# CMS libraries (QWCommon)

The aqms-cms-libs (aka QWCommon) is AQMS CMS library source code.

See the ``aqms-cms`` project for more details.
