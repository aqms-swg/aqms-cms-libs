package com.isti.quakewatch.common.qw_services;

/**
 * Interface definition: QWAcceptor.
 * 
 * @author OpenORB Compiler
 */
public interface QWAcceptor extends QWAcceptorOperations, org.omg.CORBA.Object, org.omg.CORBA.portable.IDLEntity
{
    /**
     * Constant value
     */
    public static final String ERROR_MSG_PREFIX = (String) ("Error: ");

}
