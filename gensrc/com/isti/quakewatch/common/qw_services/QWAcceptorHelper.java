package com.isti.quakewatch.common.qw_services;

/** 
 * Helper class for : QWAcceptor
 *  
 * @author OpenORB Compiler
 */ 
public class QWAcceptorHelper
{
    /**
     * Insert QWAcceptor into an any
     * @param a an any
     * @param t QWAcceptor value
     */
    public static void insert(org.omg.CORBA.Any a, com.isti.quakewatch.common.qw_services.QWAcceptor t)
    {
        a.insert_Object(t , type());
    }

    /**
     * Extract QWAcceptor from an any
     *
     * @param a an any
     * @return the extracted QWAcceptor value
     */
    public static com.isti.quakewatch.common.qw_services.QWAcceptor extract( org.omg.CORBA.Any a )
    {
        if ( !a.type().equivalent( type() ) )
        {
            throw new org.omg.CORBA.MARSHAL();
        }
        try
        {
            return com.isti.quakewatch.common.qw_services.QWAcceptorHelper.narrow( a.extract_Object() );
        }
        catch ( final org.omg.CORBA.BAD_PARAM e )
        {
            throw new org.omg.CORBA.MARSHAL(e.getMessage());
        }
    }

    //
    // Internal TypeCode value
    //
    private static org.omg.CORBA.TypeCode _tc = null;

    /**
     * Return the QWAcceptor TypeCode
     * @return a TypeCode
     */
    public static org.omg.CORBA.TypeCode type()
    {
        if (_tc == null) {
            org.omg.CORBA.ORB orb = org.omg.CORBA.ORB.init();
            _tc = orb.create_interface_tc( id(), "QWAcceptor" );
        }
        return _tc;
    }

    /**
     * Return the QWAcceptor IDL ID
     * @return an ID
     */
    public static String id()
    {
        return _id;
    }

    private final static String _id = "IDL:com/isti/quakewatch/common/qw_services/QWAcceptor:1.0";

    /**
     * Read QWAcceptor from a marshalled stream
     * @param istream the input stream
     * @return the readed QWAcceptor value
     */
    public static com.isti.quakewatch.common.qw_services.QWAcceptor read(org.omg.CORBA.portable.InputStream istream)
    {
        return(com.isti.quakewatch.common.qw_services.QWAcceptor)istream.read_Object(com.isti.quakewatch.common.qw_services._QWAcceptorStub.class);
    }

    /**
     * Write QWAcceptor into a marshalled stream
     * @param ostream the output stream
     * @param value QWAcceptor value
     */
    public static void write(org.omg.CORBA.portable.OutputStream ostream, com.isti.quakewatch.common.qw_services.QWAcceptor value)
    {
        ostream.write_Object((org.omg.CORBA.portable.ObjectImpl)value);
    }

    /**
     * Narrow CORBA::Object to QWAcceptor
     * @param obj the CORBA Object
     * @return QWAcceptor Object
     */
    public static QWAcceptor narrow(org.omg.CORBA.Object obj)
    {
        if (obj == null)
            return null;
        if (obj instanceof QWAcceptor)
            return (QWAcceptor)obj;

        if (obj._is_a(id()))
        {
            _QWAcceptorStub stub = new _QWAcceptorStub();
            stub._set_delegate(((org.omg.CORBA.portable.ObjectImpl)obj)._get_delegate());
            return stub;
        }

        throw new org.omg.CORBA.BAD_PARAM();
    }

    /**
     * Unchecked Narrow CORBA::Object to QWAcceptor
     * @param obj the CORBA Object
     * @return QWAcceptor Object
     */
    public static QWAcceptor unchecked_narrow(org.omg.CORBA.Object obj)
    {
        if (obj == null)
            return null;
        if (obj instanceof QWAcceptor)
            return (QWAcceptor)obj;

        _QWAcceptorStub stub = new _QWAcceptorStub();
        stub._set_delegate(((org.omg.CORBA.portable.ObjectImpl)obj)._get_delegate());
        return stub;

    }

}
