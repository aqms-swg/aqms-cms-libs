package com.isti.quakewatch.common.qw_services;

/**
 * Holder class for : QWAcceptor
 * 
 * @author OpenORB Compiler
 */
final public class QWAcceptorHolder
        implements org.omg.CORBA.portable.Streamable
{
    /**
     * Internal QWAcceptor value
     */
    public com.isti.quakewatch.common.qw_services.QWAcceptor value;

    /**
     * Default constructor
     */
    public QWAcceptorHolder()
    { }

    /**
     * Constructor with value initialisation
     * @param initial the initial value
     */
    public QWAcceptorHolder(com.isti.quakewatch.common.qw_services.QWAcceptor initial)
    {
        value = initial;
    }

    /**
     * Read QWAcceptor from a marshalled stream
     * @param istream the input stream
     */
    public void _read(org.omg.CORBA.portable.InputStream istream)
    {
        value = QWAcceptorHelper.read(istream);
    }

    /**
     * Write QWAcceptor into a marshalled stream
     * @param ostream the output stream
     */
    public void _write(org.omg.CORBA.portable.OutputStream ostream)
    {
        QWAcceptorHelper.write(ostream,value);
    }

    /**
     * Return the QWAcceptor TypeCode
     * @return a TypeCode
     */
    public org.omg.CORBA.TypeCode _type()
    {
        return QWAcceptorHelper.type();
    }

}
