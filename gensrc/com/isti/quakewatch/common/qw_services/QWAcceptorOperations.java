package com.isti.quakewatch.common.qw_services;

/**
 * Interface definition: QWAcceptor.
 * 
 * @author OpenORB Compiler
 */
public interface QWAcceptorOperations
{
    /**
     * Establishes a new client connection for QuakeWatch services.
     * @param  userNameStr user name string for client.
     * @param  passwordStr password string for client.
     * @param  connInfoPropsStr string of connection-information
     * properties for the client, in the following format:
     * "name1="value1","name2"="value2",...
     * @return  An "IOR:" string for the 'QWServices' CORBA object to
     * be used by the calling client, or an error message (preceded
     * by "Error: ").
     */
    public String newConnection(String userNameStr, String passwordStr, String connInfoPropsStr);

    /**
     * Returns an identifier string that uniquely identifies
     * this acceptor object.
     * @return  An identifier string that uniquely identifies
     * this acceptor object.
     */
    public String getAcceptorIDStr();

}
