package com.isti.quakewatch.common.qw_services;

/**
 * Interface definition: QWAcceptor.
 * 
 * @author OpenORB Compiler
 */
public abstract class QWAcceptorPOA extends org.omg.PortableServer.Servant
        implements QWAcceptorOperations, org.omg.CORBA.portable.InvokeHandler
{
    public QWAcceptor _this()
    {
        return QWAcceptorHelper.narrow(_this_object());
    }

    public QWAcceptor _this(org.omg.CORBA.ORB orb)
    {
        return QWAcceptorHelper.narrow(_this_object(orb));
    }

    private static String [] _ids_list =
    {
        "IDL:com/isti/quakewatch/common/qw_services/QWAcceptor:1.0"
    };

    public String[] _all_interfaces(org.omg.PortableServer.POA poa, byte [] objectId)
    {
        return _ids_list;
    }

    public final org.omg.CORBA.portable.OutputStream _invoke(final String opName,
            final org.omg.CORBA.portable.InputStream _is,
            final org.omg.CORBA.portable.ResponseHandler handler)
    {

        if (opName.equals("getAcceptorIDStr")) {
                return _invoke_getAcceptorIDStr(_is, handler);
        } else if (opName.equals("newConnection")) {
                return _invoke_newConnection(_is, handler);
        } else {
            throw new org.omg.CORBA.BAD_OPERATION(opName);
        }
    }

    // helper methods
    private org.omg.CORBA.portable.OutputStream _invoke_newConnection(
            final org.omg.CORBA.portable.InputStream _is,
            final org.omg.CORBA.portable.ResponseHandler handler) {
        org.omg.CORBA.portable.OutputStream _output;
        String arg0_in = _is.read_string();
        String arg1_in = _is.read_string();
        String arg2_in = _is.read_string();

        String _arg_result = newConnection(arg0_in, arg1_in, arg2_in);

        _output = handler.createReply();
        _output.write_string(_arg_result);

        return _output;
    }

    private org.omg.CORBA.portable.OutputStream _invoke_getAcceptorIDStr(
            final org.omg.CORBA.portable.InputStream _is,
            final org.omg.CORBA.portable.ResponseHandler handler) {
        org.omg.CORBA.portable.OutputStream _output;

        String _arg_result = getAcceptorIDStr();

        _output = handler.createReply();
        _output.write_string(_arg_result);

        return _output;
    }

}
