package com.isti.quakewatch.common.qw_services;

/**
 * Interface definition: QWAcceptor.
 * 
 * @author OpenORB Compiler
 */
public class QWAcceptorPOATie extends QWAcceptorPOA
{

    //
    // Private reference to implementation object
    //
    private QWAcceptorOperations _tie;

    //
    // Private reference to POA
    //
    private org.omg.PortableServer.POA _poa;

    /**
     * Constructor
     */
    public QWAcceptorPOATie(QWAcceptorOperations tieObject)
    {
        _tie = tieObject;
    }

    /**
     * Constructor
     */
    public QWAcceptorPOATie(QWAcceptorOperations tieObject, org.omg.PortableServer.POA poa)
    {
        _tie = tieObject;
        _poa = poa;
    }

    /**
     * Get the delegate
     */
    public QWAcceptorOperations _delegate()
    {
        return _tie;
    }

    /**
     * Set the delegate
     */
    public void _delegate(QWAcceptorOperations delegate_)
    {
        _tie = delegate_;
    }

    /**
     * _default_POA method
     */
    public org.omg.PortableServer.POA _default_POA()
    {
        if (_poa != null)
            return _poa;
        else
            return super._default_POA();
    }

    /**
     * Operation newConnection
     */
    public String newConnection(String userNameStr, String passwordStr, String connInfoPropsStr)
    {
        return _tie.newConnection( userNameStr,  passwordStr,  connInfoPropsStr);
    }

    /**
     * Operation getAcceptorIDStr
     */
    public String getAcceptorIDStr()
    {
        return _tie.getAcceptorIDStr();
    }

}
