package com.isti.quakewatch.common.qw_services;

/**
 * Interface definition: QWServices.
 * 
 * @author OpenORB Compiler
 */
public interface QWServices extends QWServicesOperations, org.omg.CORBA.Object, org.omg.CORBA.portable.IDLEntity
{
    /**
     * Constant value
     */
    public static final int CS_CONNECT_OK = (int) (0l);

    /**
     * Constant value
     */
    public static final int CS_SERVER_REDIRECT = (int) (1l);

    /**
     * Constant value
     */
    public static final int CS_INVALID_LOGIN = (int) (2l);

    /**
     * Constant value
     */
    public static final int CS_VERSION_OBSOLETE = (int) (3l);

    /**
     * Constant value
     */
    public static final int CS_BAD_DISTNAME = (int) (4l);

    /**
     * Constant value
     */
    public static final int CS_CONN_ERROR = (int) (5l);

    /**
     * Constant value
     */
    public static final int NUM_CS_VALUES = (int) (6l);

}
