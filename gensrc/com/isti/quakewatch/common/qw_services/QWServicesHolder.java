package com.isti.quakewatch.common.qw_services;

/**
 * Holder class for : QWServices
 * 
 * @author OpenORB Compiler
 */
final public class QWServicesHolder
        implements org.omg.CORBA.portable.Streamable
{
    /**
     * Internal QWServices value
     */
    public com.isti.quakewatch.common.qw_services.QWServices value;

    /**
     * Default constructor
     */
    public QWServicesHolder()
    { }

    /**
     * Constructor with value initialisation
     * @param initial the initial value
     */
    public QWServicesHolder(com.isti.quakewatch.common.qw_services.QWServices initial)
    {
        value = initial;
    }

    /**
     * Read QWServices from a marshalled stream
     * @param istream the input stream
     */
    public void _read(org.omg.CORBA.portable.InputStream istream)
    {
        value = QWServicesHelper.read(istream);
    }

    /**
     * Write QWServices into a marshalled stream
     * @param ostream the output stream
     */
    public void _write(org.omg.CORBA.portable.OutputStream ostream)
    {
        QWServicesHelper.write(ostream,value);
    }

    /**
     * Return the QWServices TypeCode
     * @return a TypeCode
     */
    public org.omg.CORBA.TypeCode _type()
    {
        return QWServicesHelper.type();
    }

}
