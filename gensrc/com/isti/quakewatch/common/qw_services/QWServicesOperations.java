package com.isti.quakewatch.common.qw_services;

/**
 * Interface definition: QWServices.
 * 
 * @author OpenORB Compiler
 */
public interface QWServicesOperations
{
    /**
     * Fetches a value indicating the status of the current
     * connection to the server.
     * @return  One of the 'CS_' values.
     */
    public int getConnectionStatusVal();

    /**
     * Fetches a message string describing the status of the
     * current connection to the server.
     * @return  A message string describing the status of the
     * current connection to the server.
     */
    public String getConnectionStatusMsg();

    /**
     * Fetches the location of the server that the client is
     * being redirected to.  The returned string may also be
     * a comma-separated list of "hostAddr:portNum" entries.
     * @return  The redirect-server location(s) string in the form
     * "hostAddr:portNum", or an empty string if the client is
     * not being redirected.
     */
    public String getRedirectedServerLoc();

    /**
     * Fetches the server ID name string (defined in the QWServer's
     * configuration file).
     * @return  The server ID name string.
     */
    public String getServerIdNameStr();

    /**
     * Fetches the host address string for this server.
     * @return  The host address string for this server.
     */
    public String getServerHostAddrStr();

    /**
     * Fetches the revision string for this server.
     * @return  The revision string for this server.
     */
    public String getServerRevisionString();

    /**
     * Fetches the type-name string used on status messages sent out
     * by the server.  When structured messages are enabled, status
     * messages will be sent with the domain name "StatusMessage"
     * and the type name set to this value.  These names can be used
     * on the client side to filter-in the status messages.
     * @return  The type-name string used on status messages sent out
     * by the server (when structured messages are enabled).
     */
    public String getStatusMsgTypeNameStr();

    /**
     * Fetches the list of alternate server IDs (defined in the
     * QWServer's configuration file).
     * @return  The list of alternate server IDs, as a string in
     * the form "hostAddr:portNum,hostAddr:portNum,...".
     */
    public String getAltServersIdsListStr();

    /**
     * Fetches the locator string for the CORBA-event-channel.
     * @return  The locator string, or an empty string if an error
     * occurred.
     */
    public String getEventChLocStr();

    /**
     * Requests that a server-alive message be sent immediately.
     */
    public void requestAliveMessage();

    /**
     * Requests that messages newer or equal to the specified time
     * value or later than the specified message number be returned.
     * @param  timeVal the time-generated value for message
     * associated with the given message number, or the requested
     * time value to be used (milliseconds since 1/1/1970).
     * @param  msgNum the message number to use, or 0 or none.
     * @return  An XML-formatted string containing the messages,
     * or an empty string if an error occurs.
     */
    public String requestMessages(long timeVal, long msgNum);

    /**
     * Requests that messages newer or equal to the specified time
     * value or later than the specified message number be returned.
     * Only messages whose event domain and type names match the
     * given list of domain and type names will be returned (unless
     * the given list is an empty string).
     * @param  timeVal the time-generated value for message
     * associated with the given message number, or the requested
     * time value to be used (milliseconds since 1/1/1970).
     * @param  msgNum the message number to use, or 0 or none.
     * @param  domainTypeListStr a list string of event domain and
     * type names in the format "domain:type,domain:type...", where
     * occurrences of the ':' and ',' characters not meant as
     * separators may be "quoted" by preceding them with the
     * backslash ('\') character and list items missing the ':'
     * character will be considered to specify only a domain name
     * (the type name will be an empty string); or an empty string
     * for none.
     * @return  An XML-formatted string containing the messages,
     * or an empty string if an error occurs.
     */
    public String requestFilteredMessages(long timeVal, long msgNum, String domainTypeListStr);

    /**
     * Requests that messages corresponding to the given time value
     * and list of feeder-data-source host-name/message-number
     * entries be returned.
     * @param  timeVal the time-generated value for message
     * associated with the given message number, or the requested
     * time value to be used (milliseconds since 1/1/1970).
     * @param  hostMsgNumListStr a list of feeder-data-source
     * host-name/message-number entries in the form:
     * "hostName"=msgNum,...
     * @return  An XML-formatted string containing the messages,
     * or an empty string if an error occurs.
     */
    public String requestSourcedMessages(long timeVal, String hostMsgNumListStr);

    /**
     * Requests that messages corresponding to the given time value
     * and list of feeder-data-source host-name/message-number
     * entries be returned.
     * Only messages whose event domain and type names match the
     * given list of domain and type names will be returned (unless
     * the given list is an empty string).
     * @param  timeVal the time-generated value for message
     * associated with the given message number, or the requested
     * time value to be used (milliseconds since 1/1/1970).
     * @param  hostMsgNumListStr a list of feeder-data-source
     * host-name/message-number entries in the form:
     * "hostName"=msgNum,...
     * @param  domainTypeListStr a list string of event domain and
     * type names in the format "domain:type,domain:type...", where
     * occurrences of the ':' and ',' characters not meant as
     * separators may be "quoted" by preceding them with the
     * backslash ('\') character and list items missing the ':'
     * character will be considered to specify only a domain name
     * (the type name will be an empty string); or an empty string
     * for none.
     * @return  An XML-formatted string containing the messages,
     * or an empty string if an error occurs.
     */
    public String requestSourcedFilteredMessages(long timeVal, String hostMsgNumListStr, String domainTypeListStr);

    /**
     * Determines the client-version status.  All clients will
     * call this method on a periodical basis while they are
     * connected.
     * @param  clientInfoStr the client-connection-information
     * properties string to use.
     * @return  true if the an updated version of the client
     * is available; false if not.
     */
    public boolean clientStatusCheck(String clientInfoStr);

    /**
     * Fetches information about available client-program upgrades.
     * @param  clientInfoStr the client-connection-information
     * properties string to use.
     * @return  An XML-formatted string containing information about
     * available client-program upgrades.
     */
    public String getClientUpgradeInfo(String clientInfoStr);

    /**
     * Fetches the timestamp value for the latest status report
     * from the server.
     * @return  The timestamp value for the latest status report
     * from the server, or 0 if no report is available.
     */
    public long getStatusReportTime();

    /**
     * Fetches the latest status-report data from the server.
     * @return  A string containing the latest status-report data,
     * or an empty string if no report is available.
     */
    public String getStatusReportData();

    /**
     * Fetches a byte-array of certificate-file data.
     * @return  A byte-array of certificate-file data, or a
     * zero-length array if no data is available.
     */
    public byte[] getCertificateFileData();

    /**
     * Disconnects the client connection.
     * @param  clientInfoStr the client-connection-information
     * properties string for the client.
     */
    public void disconnectClient(String clientInfoStr);

}
