package com.isti.quakewatch.common.qw_services;

/**
 * Interface definition: QWServices.
 * 
 * @author OpenORB Compiler
 */
public abstract class QWServicesPOA extends org.omg.PortableServer.Servant
        implements QWServicesOperations, org.omg.CORBA.portable.InvokeHandler
{
    public QWServices _this()
    {
        return QWServicesHelper.narrow(_this_object());
    }

    public QWServices _this(org.omg.CORBA.ORB orb)
    {
        return QWServicesHelper.narrow(_this_object(orb));
    }

    private static String [] _ids_list =
    {
        "IDL:com/isti/quakewatch/common/qw_services/QWServices:1.0"
    };

    public String[] _all_interfaces(org.omg.PortableServer.POA poa, byte [] objectId)
    {
        return _ids_list;
    }

    private static final java.util.Map operationMap = new java.util.HashMap();

    static {
            operationMap.put("clientStatusCheck",
                    new Operation_clientStatusCheck());
            operationMap.put("disconnectClient",
                    new Operation_disconnectClient());
            operationMap.put("getAltServersIdsListStr",
                    new Operation_getAltServersIdsListStr());
            operationMap.put("getCertificateFileData",
                    new Operation_getCertificateFileData());
            operationMap.put("getClientUpgradeInfo",
                    new Operation_getClientUpgradeInfo());
            operationMap.put("getConnectionStatusMsg",
                    new Operation_getConnectionStatusMsg());
            operationMap.put("getConnectionStatusVal",
                    new Operation_getConnectionStatusVal());
            operationMap.put("getEventChLocStr",
                    new Operation_getEventChLocStr());
            operationMap.put("getRedirectedServerLoc",
                    new Operation_getRedirectedServerLoc());
            operationMap.put("getServerHostAddrStr",
                    new Operation_getServerHostAddrStr());
            operationMap.put("getServerIdNameStr",
                    new Operation_getServerIdNameStr());
            operationMap.put("getServerRevisionString",
                    new Operation_getServerRevisionString());
            operationMap.put("getStatusMsgTypeNameStr",
                    new Operation_getStatusMsgTypeNameStr());
            operationMap.put("getStatusReportData",
                    new Operation_getStatusReportData());
            operationMap.put("getStatusReportTime",
                    new Operation_getStatusReportTime());
            operationMap.put("requestAliveMessage",
                    new Operation_requestAliveMessage());
            operationMap.put("requestFilteredMessages",
                    new Operation_requestFilteredMessages());
            operationMap.put("requestMessages",
                    new Operation_requestMessages());
            operationMap.put("requestSourcedFilteredMessages",
                    new Operation_requestSourcedFilteredMessages());
            operationMap.put("requestSourcedMessages",
                    new Operation_requestSourcedMessages());
    }

    public final org.omg.CORBA.portable.OutputStream _invoke(final String opName,
            final org.omg.CORBA.portable.InputStream _is,
            final org.omg.CORBA.portable.ResponseHandler handler)
    {

        final AbstractOperation operation = (AbstractOperation)operationMap.get(opName);

        if (null == operation) {
            throw new org.omg.CORBA.BAD_OPERATION(opName);
        }

        return operation.invoke(this, _is, handler);
    }

    // helper methods
    private org.omg.CORBA.portable.OutputStream _invoke_getConnectionStatusVal(
            final org.omg.CORBA.portable.InputStream _is,
            final org.omg.CORBA.portable.ResponseHandler handler) {
        org.omg.CORBA.portable.OutputStream _output;

        int _arg_result = getConnectionStatusVal();

        _output = handler.createReply();
        _output.write_long(_arg_result);

        return _output;
    }

    private org.omg.CORBA.portable.OutputStream _invoke_getConnectionStatusMsg(
            final org.omg.CORBA.portable.InputStream _is,
            final org.omg.CORBA.portable.ResponseHandler handler) {
        org.omg.CORBA.portable.OutputStream _output;

        String _arg_result = getConnectionStatusMsg();

        _output = handler.createReply();
        _output.write_string(_arg_result);

        return _output;
    }

    private org.omg.CORBA.portable.OutputStream _invoke_getRedirectedServerLoc(
            final org.omg.CORBA.portable.InputStream _is,
            final org.omg.CORBA.portable.ResponseHandler handler) {
        org.omg.CORBA.portable.OutputStream _output;

        String _arg_result = getRedirectedServerLoc();

        _output = handler.createReply();
        _output.write_string(_arg_result);

        return _output;
    }

    private org.omg.CORBA.portable.OutputStream _invoke_getServerIdNameStr(
            final org.omg.CORBA.portable.InputStream _is,
            final org.omg.CORBA.portable.ResponseHandler handler) {
        org.omg.CORBA.portable.OutputStream _output;

        String _arg_result = getServerIdNameStr();

        _output = handler.createReply();
        _output.write_string(_arg_result);

        return _output;
    }

    private org.omg.CORBA.portable.OutputStream _invoke_getServerHostAddrStr(
            final org.omg.CORBA.portable.InputStream _is,
            final org.omg.CORBA.portable.ResponseHandler handler) {
        org.omg.CORBA.portable.OutputStream _output;

        String _arg_result = getServerHostAddrStr();

        _output = handler.createReply();
        _output.write_string(_arg_result);

        return _output;
    }

    private org.omg.CORBA.portable.OutputStream _invoke_getServerRevisionString(
            final org.omg.CORBA.portable.InputStream _is,
            final org.omg.CORBA.portable.ResponseHandler handler) {
        org.omg.CORBA.portable.OutputStream _output;

        String _arg_result = getServerRevisionString();

        _output = handler.createReply();
        _output.write_string(_arg_result);

        return _output;
    }

    private org.omg.CORBA.portable.OutputStream _invoke_getStatusMsgTypeNameStr(
            final org.omg.CORBA.portable.InputStream _is,
            final org.omg.CORBA.portable.ResponseHandler handler) {
        org.omg.CORBA.portable.OutputStream _output;

        String _arg_result = getStatusMsgTypeNameStr();

        _output = handler.createReply();
        _output.write_string(_arg_result);

        return _output;
    }

    private org.omg.CORBA.portable.OutputStream _invoke_getAltServersIdsListStr(
            final org.omg.CORBA.portable.InputStream _is,
            final org.omg.CORBA.portable.ResponseHandler handler) {
        org.omg.CORBA.portable.OutputStream _output;

        String _arg_result = getAltServersIdsListStr();

        _output = handler.createReply();
        _output.write_string(_arg_result);

        return _output;
    }

    private org.omg.CORBA.portable.OutputStream _invoke_getEventChLocStr(
            final org.omg.CORBA.portable.InputStream _is,
            final org.omg.CORBA.portable.ResponseHandler handler) {
        org.omg.CORBA.portable.OutputStream _output;

        String _arg_result = getEventChLocStr();

        _output = handler.createReply();
        _output.write_string(_arg_result);

        return _output;
    }

    private org.omg.CORBA.portable.OutputStream _invoke_requestAliveMessage(
            final org.omg.CORBA.portable.InputStream _is,
            final org.omg.CORBA.portable.ResponseHandler handler) {
        org.omg.CORBA.portable.OutputStream _output;

        requestAliveMessage();

        _output = handler.createReply();

        return _output;
    }

    private org.omg.CORBA.portable.OutputStream _invoke_requestMessages(
            final org.omg.CORBA.portable.InputStream _is,
            final org.omg.CORBA.portable.ResponseHandler handler) {
        org.omg.CORBA.portable.OutputStream _output;
        long arg0_in = _is.read_longlong();
        long arg1_in = _is.read_longlong();

        String _arg_result = requestMessages(arg0_in, arg1_in);

        _output = handler.createReply();
        _output.write_string(_arg_result);

        return _output;
    }

    private org.omg.CORBA.portable.OutputStream _invoke_requestFilteredMessages(
            final org.omg.CORBA.portable.InputStream _is,
            final org.omg.CORBA.portable.ResponseHandler handler) {
        org.omg.CORBA.portable.OutputStream _output;
        long arg0_in = _is.read_longlong();
        long arg1_in = _is.read_longlong();
        String arg2_in = _is.read_string();

        String _arg_result = requestFilteredMessages(arg0_in, arg1_in, arg2_in);

        _output = handler.createReply();
        _output.write_string(_arg_result);

        return _output;
    }

    private org.omg.CORBA.portable.OutputStream _invoke_requestSourcedMessages(
            final org.omg.CORBA.portable.InputStream _is,
            final org.omg.CORBA.portable.ResponseHandler handler) {
        org.omg.CORBA.portable.OutputStream _output;
        long arg0_in = _is.read_longlong();
        String arg1_in = _is.read_string();

        String _arg_result = requestSourcedMessages(arg0_in, arg1_in);

        _output = handler.createReply();
        _output.write_string(_arg_result);

        return _output;
    }

    private org.omg.CORBA.portable.OutputStream _invoke_requestSourcedFilteredMessages(
            final org.omg.CORBA.portable.InputStream _is,
            final org.omg.CORBA.portable.ResponseHandler handler) {
        org.omg.CORBA.portable.OutputStream _output;
        long arg0_in = _is.read_longlong();
        String arg1_in = _is.read_string();
        String arg2_in = _is.read_string();

        String _arg_result = requestSourcedFilteredMessages(arg0_in, arg1_in, arg2_in);

        _output = handler.createReply();
        _output.write_string(_arg_result);

        return _output;
    }

    private org.omg.CORBA.portable.OutputStream _invoke_clientStatusCheck(
            final org.omg.CORBA.portable.InputStream _is,
            final org.omg.CORBA.portable.ResponseHandler handler) {
        org.omg.CORBA.portable.OutputStream _output;
        String arg0_in = _is.read_string();

        boolean _arg_result = clientStatusCheck(arg0_in);

        _output = handler.createReply();
        _output.write_boolean(_arg_result);

        return _output;
    }

    private org.omg.CORBA.portable.OutputStream _invoke_getClientUpgradeInfo(
            final org.omg.CORBA.portable.InputStream _is,
            final org.omg.CORBA.portable.ResponseHandler handler) {
        org.omg.CORBA.portable.OutputStream _output;
        String arg0_in = _is.read_string();

        String _arg_result = getClientUpgradeInfo(arg0_in);

        _output = handler.createReply();
        _output.write_string(_arg_result);

        return _output;
    }

    private org.omg.CORBA.portable.OutputStream _invoke_getStatusReportTime(
            final org.omg.CORBA.portable.InputStream _is,
            final org.omg.CORBA.portable.ResponseHandler handler) {
        org.omg.CORBA.portable.OutputStream _output;

        long _arg_result = getStatusReportTime();

        _output = handler.createReply();
        _output.write_longlong(_arg_result);

        return _output;
    }

    private org.omg.CORBA.portable.OutputStream _invoke_getStatusReportData(
            final org.omg.CORBA.portable.InputStream _is,
            final org.omg.CORBA.portable.ResponseHandler handler) {
        org.omg.CORBA.portable.OutputStream _output;

        String _arg_result = getStatusReportData();

        _output = handler.createReply();
        _output.write_string(_arg_result);

        return _output;
    }

    private org.omg.CORBA.portable.OutputStream _invoke_getCertificateFileData(
            final org.omg.CORBA.portable.InputStream _is,
            final org.omg.CORBA.portable.ResponseHandler handler) {
        org.omg.CORBA.portable.OutputStream _output;

        byte[] _arg_result = getCertificateFileData();

        _output = handler.createReply();
        com.isti.quakewatch.common.qw_services.QWServicesPackage.ByteArrayHelper.write(_output,_arg_result);

        return _output;
    }

    private org.omg.CORBA.portable.OutputStream _invoke_disconnectClient(
            final org.omg.CORBA.portable.InputStream _is,
            final org.omg.CORBA.portable.ResponseHandler handler) {
        org.omg.CORBA.portable.OutputStream _output;
        String arg0_in = _is.read_string();

        disconnectClient(arg0_in);

        _output = handler.createReply();

        return _output;
    }

    // operation classes
    private abstract static class AbstractOperation {
        protected abstract org.omg.CORBA.portable.OutputStream invoke(
                QWServicesPOA target,
                org.omg.CORBA.portable.InputStream _is,
                org.omg.CORBA.portable.ResponseHandler handler);
    }

    private static final class Operation_getConnectionStatusVal extends AbstractOperation
    {
        protected org.omg.CORBA.portable.OutputStream invoke(
                final QWServicesPOA target,
                final org.omg.CORBA.portable.InputStream _is,
                final org.omg.CORBA.portable.ResponseHandler handler) {
            return target._invoke_getConnectionStatusVal(_is, handler);
        }
    }

    private static final class Operation_getConnectionStatusMsg extends AbstractOperation
    {
        protected org.omg.CORBA.portable.OutputStream invoke(
                final QWServicesPOA target,
                final org.omg.CORBA.portable.InputStream _is,
                final org.omg.CORBA.portable.ResponseHandler handler) {
            return target._invoke_getConnectionStatusMsg(_is, handler);
        }
    }

    private static final class Operation_getRedirectedServerLoc extends AbstractOperation
    {
        protected org.omg.CORBA.portable.OutputStream invoke(
                final QWServicesPOA target,
                final org.omg.CORBA.portable.InputStream _is,
                final org.omg.CORBA.portable.ResponseHandler handler) {
            return target._invoke_getRedirectedServerLoc(_is, handler);
        }
    }

    private static final class Operation_getServerIdNameStr extends AbstractOperation
    {
        protected org.omg.CORBA.portable.OutputStream invoke(
                final QWServicesPOA target,
                final org.omg.CORBA.portable.InputStream _is,
                final org.omg.CORBA.portable.ResponseHandler handler) {
            return target._invoke_getServerIdNameStr(_is, handler);
        }
    }

    private static final class Operation_getServerHostAddrStr extends AbstractOperation
    {
        protected org.omg.CORBA.portable.OutputStream invoke(
                final QWServicesPOA target,
                final org.omg.CORBA.portable.InputStream _is,
                final org.omg.CORBA.portable.ResponseHandler handler) {
            return target._invoke_getServerHostAddrStr(_is, handler);
        }
    }

    private static final class Operation_getServerRevisionString extends AbstractOperation
    {
        protected org.omg.CORBA.portable.OutputStream invoke(
                final QWServicesPOA target,
                final org.omg.CORBA.portable.InputStream _is,
                final org.omg.CORBA.portable.ResponseHandler handler) {
            return target._invoke_getServerRevisionString(_is, handler);
        }
    }

    private static final class Operation_getStatusMsgTypeNameStr extends AbstractOperation
    {
        protected org.omg.CORBA.portable.OutputStream invoke(
                final QWServicesPOA target,
                final org.omg.CORBA.portable.InputStream _is,
                final org.omg.CORBA.portable.ResponseHandler handler) {
            return target._invoke_getStatusMsgTypeNameStr(_is, handler);
        }
    }

    private static final class Operation_getAltServersIdsListStr extends AbstractOperation
    {
        protected org.omg.CORBA.portable.OutputStream invoke(
                final QWServicesPOA target,
                final org.omg.CORBA.portable.InputStream _is,
                final org.omg.CORBA.portable.ResponseHandler handler) {
            return target._invoke_getAltServersIdsListStr(_is, handler);
        }
    }

    private static final class Operation_getEventChLocStr extends AbstractOperation
    {
        protected org.omg.CORBA.portable.OutputStream invoke(
                final QWServicesPOA target,
                final org.omg.CORBA.portable.InputStream _is,
                final org.omg.CORBA.portable.ResponseHandler handler) {
            return target._invoke_getEventChLocStr(_is, handler);
        }
    }

    private static final class Operation_requestAliveMessage extends AbstractOperation
    {
        protected org.omg.CORBA.portable.OutputStream invoke(
                final QWServicesPOA target,
                final org.omg.CORBA.portable.InputStream _is,
                final org.omg.CORBA.portable.ResponseHandler handler) {
            return target._invoke_requestAliveMessage(_is, handler);
        }
    }

    private static final class Operation_requestMessages extends AbstractOperation
    {
        protected org.omg.CORBA.portable.OutputStream invoke(
                final QWServicesPOA target,
                final org.omg.CORBA.portable.InputStream _is,
                final org.omg.CORBA.portable.ResponseHandler handler) {
            return target._invoke_requestMessages(_is, handler);
        }
    }

    private static final class Operation_requestFilteredMessages extends AbstractOperation
    {
        protected org.omg.CORBA.portable.OutputStream invoke(
                final QWServicesPOA target,
                final org.omg.CORBA.portable.InputStream _is,
                final org.omg.CORBA.portable.ResponseHandler handler) {
            return target._invoke_requestFilteredMessages(_is, handler);
        }
    }

    private static final class Operation_requestSourcedMessages extends AbstractOperation
    {
        protected org.omg.CORBA.portable.OutputStream invoke(
                final QWServicesPOA target,
                final org.omg.CORBA.portable.InputStream _is,
                final org.omg.CORBA.portable.ResponseHandler handler) {
            return target._invoke_requestSourcedMessages(_is, handler);
        }
    }

    private static final class Operation_requestSourcedFilteredMessages extends AbstractOperation
    {
        protected org.omg.CORBA.portable.OutputStream invoke(
                final QWServicesPOA target,
                final org.omg.CORBA.portable.InputStream _is,
                final org.omg.CORBA.portable.ResponseHandler handler) {
            return target._invoke_requestSourcedFilteredMessages(_is, handler);
        }
    }

    private static final class Operation_clientStatusCheck extends AbstractOperation
    {
        protected org.omg.CORBA.portable.OutputStream invoke(
                final QWServicesPOA target,
                final org.omg.CORBA.portable.InputStream _is,
                final org.omg.CORBA.portable.ResponseHandler handler) {
            return target._invoke_clientStatusCheck(_is, handler);
        }
    }

    private static final class Operation_getClientUpgradeInfo extends AbstractOperation
    {
        protected org.omg.CORBA.portable.OutputStream invoke(
                final QWServicesPOA target,
                final org.omg.CORBA.portable.InputStream _is,
                final org.omg.CORBA.portable.ResponseHandler handler) {
            return target._invoke_getClientUpgradeInfo(_is, handler);
        }
    }

    private static final class Operation_getStatusReportTime extends AbstractOperation
    {
        protected org.omg.CORBA.portable.OutputStream invoke(
                final QWServicesPOA target,
                final org.omg.CORBA.portable.InputStream _is,
                final org.omg.CORBA.portable.ResponseHandler handler) {
            return target._invoke_getStatusReportTime(_is, handler);
        }
    }

    private static final class Operation_getStatusReportData extends AbstractOperation
    {
        protected org.omg.CORBA.portable.OutputStream invoke(
                final QWServicesPOA target,
                final org.omg.CORBA.portable.InputStream _is,
                final org.omg.CORBA.portable.ResponseHandler handler) {
            return target._invoke_getStatusReportData(_is, handler);
        }
    }

    private static final class Operation_getCertificateFileData extends AbstractOperation
    {
        protected org.omg.CORBA.portable.OutputStream invoke(
                final QWServicesPOA target,
                final org.omg.CORBA.portable.InputStream _is,
                final org.omg.CORBA.portable.ResponseHandler handler) {
            return target._invoke_getCertificateFileData(_is, handler);
        }
    }

    private static final class Operation_disconnectClient extends AbstractOperation
    {
        protected org.omg.CORBA.portable.OutputStream invoke(
                final QWServicesPOA target,
                final org.omg.CORBA.portable.InputStream _is,
                final org.omg.CORBA.portable.ResponseHandler handler) {
            return target._invoke_disconnectClient(_is, handler);
        }
    }

}
