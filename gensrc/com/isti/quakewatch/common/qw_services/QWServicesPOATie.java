package com.isti.quakewatch.common.qw_services;

/**
 * Interface definition: QWServices.
 * 
 * @author OpenORB Compiler
 */
public class QWServicesPOATie extends QWServicesPOA
{

    //
    // Private reference to implementation object
    //
    private QWServicesOperations _tie;

    //
    // Private reference to POA
    //
    private org.omg.PortableServer.POA _poa;

    /**
     * Constructor
     */
    public QWServicesPOATie(QWServicesOperations tieObject)
    {
        _tie = tieObject;
    }

    /**
     * Constructor
     */
    public QWServicesPOATie(QWServicesOperations tieObject, org.omg.PortableServer.POA poa)
    {
        _tie = tieObject;
        _poa = poa;
    }

    /**
     * Get the delegate
     */
    public QWServicesOperations _delegate()
    {
        return _tie;
    }

    /**
     * Set the delegate
     */
    public void _delegate(QWServicesOperations delegate_)
    {
        _tie = delegate_;
    }

    /**
     * _default_POA method
     */
    public org.omg.PortableServer.POA _default_POA()
    {
        if (_poa != null)
            return _poa;
        else
            return super._default_POA();
    }

    /**
     * Operation getConnectionStatusVal
     */
    public int getConnectionStatusVal()
    {
        return _tie.getConnectionStatusVal();
    }

    /**
     * Operation getConnectionStatusMsg
     */
    public String getConnectionStatusMsg()
    {
        return _tie.getConnectionStatusMsg();
    }

    /**
     * Operation getRedirectedServerLoc
     */
    public String getRedirectedServerLoc()
    {
        return _tie.getRedirectedServerLoc();
    }

    /**
     * Operation getServerIdNameStr
     */
    public String getServerIdNameStr()
    {
        return _tie.getServerIdNameStr();
    }

    /**
     * Operation getServerHostAddrStr
     */
    public String getServerHostAddrStr()
    {
        return _tie.getServerHostAddrStr();
    }

    /**
     * Operation getServerRevisionString
     */
    public String getServerRevisionString()
    {
        return _tie.getServerRevisionString();
    }

    /**
     * Operation getStatusMsgTypeNameStr
     */
    public String getStatusMsgTypeNameStr()
    {
        return _tie.getStatusMsgTypeNameStr();
    }

    /**
     * Operation getAltServersIdsListStr
     */
    public String getAltServersIdsListStr()
    {
        return _tie.getAltServersIdsListStr();
    }

    /**
     * Operation getEventChLocStr
     */
    public String getEventChLocStr()
    {
        return _tie.getEventChLocStr();
    }

    /**
     * Operation requestAliveMessage
     */
    public void requestAliveMessage()
    {
        _tie.requestAliveMessage();
    }

    /**
     * Operation requestMessages
     */
    public String requestMessages(long timeVal, long msgNum)
    {
        return _tie.requestMessages( timeVal,  msgNum);
    }

    /**
     * Operation requestFilteredMessages
     */
    public String requestFilteredMessages(long timeVal, long msgNum, String domainTypeListStr)
    {
        return _tie.requestFilteredMessages( timeVal,  msgNum,  domainTypeListStr);
    }

    /**
     * Operation requestSourcedMessages
     */
    public String requestSourcedMessages(long timeVal, String hostMsgNumListStr)
    {
        return _tie.requestSourcedMessages( timeVal,  hostMsgNumListStr);
    }

    /**
     * Operation requestSourcedFilteredMessages
     */
    public String requestSourcedFilteredMessages(long timeVal, String hostMsgNumListStr, String domainTypeListStr)
    {
        return _tie.requestSourcedFilteredMessages( timeVal,  hostMsgNumListStr,  domainTypeListStr);
    }

    /**
     * Operation clientStatusCheck
     */
    public boolean clientStatusCheck(String clientInfoStr)
    {
        return _tie.clientStatusCheck( clientInfoStr);
    }

    /**
     * Operation getClientUpgradeInfo
     */
    public String getClientUpgradeInfo(String clientInfoStr)
    {
        return _tie.getClientUpgradeInfo( clientInfoStr);
    }

    /**
     * Operation getStatusReportTime
     */
    public long getStatusReportTime()
    {
        return _tie.getStatusReportTime();
    }

    /**
     * Operation getStatusReportData
     */
    public String getStatusReportData()
    {
        return _tie.getStatusReportData();
    }

    /**
     * Operation getCertificateFileData
     */
    public byte[] getCertificateFileData()
    {
        return _tie.getCertificateFileData();
    }

    /**
     * Operation disconnectClient
     */
    public void disconnectClient(String clientInfoStr)
    {
        _tie.disconnectClient( clientInfoStr);
    }

}
