package com.isti.quakewatch.common.qw_services.QWServicesPackage;

/** 
 * Helper class for : ByteArray
 *  
 * @author OpenORB Compiler
 */ 
public class ByteArrayHelper
{
    private static final boolean HAS_OPENORB;
    static
    {
        boolean hasOpenORB = false;
        try
        {
            Thread.currentThread().getContextClassLoader().loadClass( "org.openorb.orb.core.Any" );
            hasOpenORB = true;
        }
        catch ( ClassNotFoundException ex )
        {
            // do nothing
        }
        HAS_OPENORB = hasOpenORB;
    }
    /**
     * Insert ByteArray into an any
     * @param a an any
     * @param t ByteArray value
     */
    public static void insert(org.omg.CORBA.Any a, byte[] t)
    {
        a.insert_Streamable(new com.isti.quakewatch.common.qw_services.QWServicesPackage.ByteArrayHolder(t));
    }

    /**
     * Extract ByteArray from an any
     *
     * @param a an any
     * @return the extracted ByteArray value
     */
    public static byte[] extract( org.omg.CORBA.Any a )
    {
        if ( !a.type().equivalent( type() ) )
        {
            throw new org.omg.CORBA.MARSHAL();
        }
        if ( HAS_OPENORB && a instanceof org.openorb.orb.core.Any )
        {
            // streamable extraction. The jdk stubs incorrectly define the Any stub
            org.openorb.orb.core.Any any = ( org.openorb.orb.core.Any ) a;
            try
            {
                org.omg.CORBA.portable.Streamable s = any.extract_Streamable();
                if ( s instanceof com.isti.quakewatch.common.qw_services.QWServicesPackage.ByteArrayHolder )
                {
                    return ( ( com.isti.quakewatch.common.qw_services.QWServicesPackage.ByteArrayHolder ) s ).value;
                }
            }
            catch ( org.omg.CORBA.BAD_INV_ORDER ex )
            {
            }
            com.isti.quakewatch.common.qw_services.QWServicesPackage.ByteArrayHolder h = new com.isti.quakewatch.common.qw_services.QWServicesPackage.ByteArrayHolder( read( a.create_input_stream() ) );
            a.insert_Streamable( h );
            return h.value;
        }
        return read( a.create_input_stream() );
    }

    //
    // Internal TypeCode value
    //
    private static org.omg.CORBA.TypeCode _tc = null;

    /**
     * Return the ByteArray TypeCode
     * @return a TypeCode
     */
    public static org.omg.CORBA.TypeCode type()
    {
        if (_tc == null) {
            org.omg.CORBA.ORB orb = org.omg.CORBA.ORB.init();
            _tc = orb.create_alias_tc( id(), "ByteArray", orb.create_sequence_tc( 0, orb.get_primitive_tc( org.omg.CORBA.TCKind.tk_octet ) ) );
        }
        return _tc;
    }

    /**
     * Return the ByteArray IDL ID
     * @return an ID
     */
    public static String id()
    {
        return _id;
    }

    private final static String _id = "IDL:com/isti/quakewatch/common/qw_services/QWServices/ByteArray:1.0";

    /**
     * Read ByteArray from a marshalled stream
     * @param istream the input stream
     * @return the readed ByteArray value
     */
    public static byte[] read(org.omg.CORBA.portable.InputStream istream)
    {
        byte[] new_one;
        {
        int size7 = istream.read_ulong();
        new_one = new byte[size7];
        istream.read_octet_array(new_one, 0, new_one.length);
        }

        return new_one;
    }

    /**
     * Write ByteArray into a marshalled stream
     * @param ostream the output stream
     * @param value ByteArray value
     */
    public static void write(org.omg.CORBA.portable.OutputStream ostream, byte[] value)
    {
        ostream.write_ulong( value.length );
        ostream.write_octet_array( value, 0, value.length );
    }

}
