package com.isti.quakewatch.common.qw_services.QWServicesPackage;

/**
 * Holder class for : ByteArray
 * 
 * @author OpenORB Compiler
 */
final public class ByteArrayHolder
        implements org.omg.CORBA.portable.Streamable
{
    /**
     * Internal ByteArray value
     */
    public byte[] value;

    /**
     * Default constructor
     */
    public ByteArrayHolder()
    { }

    /**
     * Constructor with value initialisation
     * @param initial the initial value
     */
    public ByteArrayHolder(byte[] initial)
    {
        value = initial;
    }

    /**
     * Read ByteArray from a marshalled stream
     * @param istream the input stream
     */
    public void _read(org.omg.CORBA.portable.InputStream istream)
    {
        value = ByteArrayHelper.read(istream);
    }

    /**
     * Write ByteArray into a marshalled stream
     * @param ostream the output stream
     */
    public void _write(org.omg.CORBA.portable.OutputStream ostream)
    {
        ByteArrayHelper.write(ostream,value);
    }

    /**
     * Return the ByteArray TypeCode
     * @return a TypeCode
     */
    public org.omg.CORBA.TypeCode _type()
    {
        return ByteArrayHelper.type();
    }

}
