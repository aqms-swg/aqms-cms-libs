package com.isti.quakewatch.common.qw_services;

/**
 * Interface definition: QWAcceptor.
 * 
 * @author OpenORB Compiler
 */
public class _QWAcceptorStub extends org.omg.CORBA.portable.ObjectImpl
        implements QWAcceptor
{
    static final String[] _ids_list =
    {
        "IDL:com/isti/quakewatch/common/qw_services/QWAcceptor:1.0"
    };

    public String[] _ids()
    {
     return _ids_list;
    }

    private final static Class _opsClass = com.isti.quakewatch.common.qw_services.QWAcceptorOperations.class;

    /**
     * Operation newConnection
     */
    public String newConnection(String userNameStr, String passwordStr, String connInfoPropsStr)
    {
        while(true)
        {
            if (!this._is_local())
            {
                org.omg.CORBA.portable.InputStream _input = null;
                try
                {
                    org.omg.CORBA.portable.OutputStream _output = this._request("newConnection",true);
                    _output.write_string(userNameStr);
                    _output.write_string(passwordStr);
                    _output.write_string(connInfoPropsStr);
                    _input = this._invoke(_output);
                    String _arg_ret = _input.read_string();
                    return _arg_ret;
                }
                catch(org.omg.CORBA.portable.RemarshalException _exception)
                {
                    continue;
                }
                catch(org.omg.CORBA.portable.ApplicationException _exception)
                {
                    String _exception_id = _exception.getId();
                    throw new org.omg.CORBA.UNKNOWN("Unexpected User Exception: "+ _exception_id);
                }
                finally
                {
                    this._releaseReply(_input);
                }
            }
            else
            {
                org.omg.CORBA.portable.ServantObject _so = _servant_preinvoke("newConnection",_opsClass);
                if (_so == null)
                   continue;
                com.isti.quakewatch.common.qw_services.QWAcceptorOperations _self = (com.isti.quakewatch.common.qw_services.QWAcceptorOperations) _so.servant;
                try
                {
                    return _self.newConnection( userNameStr,  passwordStr,  connInfoPropsStr);
                }
                finally
                {
                    _servant_postinvoke(_so);
                }
            }
        }
    }

    /**
     * Operation getAcceptorIDStr
     */
    public String getAcceptorIDStr()
    {
        while(true)
        {
            if (!this._is_local())
            {
                org.omg.CORBA.portable.InputStream _input = null;
                try
                {
                    org.omg.CORBA.portable.OutputStream _output = this._request("getAcceptorIDStr",true);
                    _input = this._invoke(_output);
                    String _arg_ret = _input.read_string();
                    return _arg_ret;
                }
                catch(org.omg.CORBA.portable.RemarshalException _exception)
                {
                    continue;
                }
                catch(org.omg.CORBA.portable.ApplicationException _exception)
                {
                    String _exception_id = _exception.getId();
                    throw new org.omg.CORBA.UNKNOWN("Unexpected User Exception: "+ _exception_id);
                }
                finally
                {
                    this._releaseReply(_input);
                }
            }
            else
            {
                org.omg.CORBA.portable.ServantObject _so = _servant_preinvoke("getAcceptorIDStr",_opsClass);
                if (_so == null)
                   continue;
                com.isti.quakewatch.common.qw_services.QWAcceptorOperations _self = (com.isti.quakewatch.common.qw_services.QWAcceptorOperations) _so.servant;
                try
                {
                    return _self.getAcceptorIDStr();
                }
                finally
                {
                    _servant_postinvoke(_so);
                }
            }
        }
    }

}
