package com.isti.quakewatch.common.qw_services;

/**
 * Interface definition: QWServices.
 * 
 * @author OpenORB Compiler
 */
public class _QWServicesStub extends org.omg.CORBA.portable.ObjectImpl
        implements QWServices
{
    static final String[] _ids_list =
    {
        "IDL:com/isti/quakewatch/common/qw_services/QWServices:1.0"
    };

    public String[] _ids()
    {
     return _ids_list;
    }

    private final static Class _opsClass = com.isti.quakewatch.common.qw_services.QWServicesOperations.class;

    /**
     * Operation getConnectionStatusVal
     */
    public int getConnectionStatusVal()
    {
        while(true)
        {
            if (!this._is_local())
            {
                org.omg.CORBA.portable.InputStream _input = null;
                try
                {
                    org.omg.CORBA.portable.OutputStream _output = this._request("getConnectionStatusVal",true);
                    _input = this._invoke(_output);
                    int _arg_ret = _input.read_long();
                    return _arg_ret;
                }
                catch(org.omg.CORBA.portable.RemarshalException _exception)
                {
                    continue;
                }
                catch(org.omg.CORBA.portable.ApplicationException _exception)
                {
                    String _exception_id = _exception.getId();
                    throw new org.omg.CORBA.UNKNOWN("Unexpected User Exception: "+ _exception_id);
                }
                finally
                {
                    this._releaseReply(_input);
                }
            }
            else
            {
                org.omg.CORBA.portable.ServantObject _so = _servant_preinvoke("getConnectionStatusVal",_opsClass);
                if (_so == null)
                   continue;
                com.isti.quakewatch.common.qw_services.QWServicesOperations _self = (com.isti.quakewatch.common.qw_services.QWServicesOperations) _so.servant;
                try
                {
                    return _self.getConnectionStatusVal();
                }
                finally
                {
                    _servant_postinvoke(_so);
                }
            }
        }
    }

    /**
     * Operation getConnectionStatusMsg
     */
    public String getConnectionStatusMsg()
    {
        while(true)
        {
            if (!this._is_local())
            {
                org.omg.CORBA.portable.InputStream _input = null;
                try
                {
                    org.omg.CORBA.portable.OutputStream _output = this._request("getConnectionStatusMsg",true);
                    _input = this._invoke(_output);
                    String _arg_ret = _input.read_string();
                    return _arg_ret;
                }
                catch(org.omg.CORBA.portable.RemarshalException _exception)
                {
                    continue;
                }
                catch(org.omg.CORBA.portable.ApplicationException _exception)
                {
                    String _exception_id = _exception.getId();
                    throw new org.omg.CORBA.UNKNOWN("Unexpected User Exception: "+ _exception_id);
                }
                finally
                {
                    this._releaseReply(_input);
                }
            }
            else
            {
                org.omg.CORBA.portable.ServantObject _so = _servant_preinvoke("getConnectionStatusMsg",_opsClass);
                if (_so == null)
                   continue;
                com.isti.quakewatch.common.qw_services.QWServicesOperations _self = (com.isti.quakewatch.common.qw_services.QWServicesOperations) _so.servant;
                try
                {
                    return _self.getConnectionStatusMsg();
                }
                finally
                {
                    _servant_postinvoke(_so);
                }
            }
        }
    }

    /**
     * Operation getRedirectedServerLoc
     */
    public String getRedirectedServerLoc()
    {
        while(true)
        {
            if (!this._is_local())
            {
                org.omg.CORBA.portable.InputStream _input = null;
                try
                {
                    org.omg.CORBA.portable.OutputStream _output = this._request("getRedirectedServerLoc",true);
                    _input = this._invoke(_output);
                    String _arg_ret = _input.read_string();
                    return _arg_ret;
                }
                catch(org.omg.CORBA.portable.RemarshalException _exception)
                {
                    continue;
                }
                catch(org.omg.CORBA.portable.ApplicationException _exception)
                {
                    String _exception_id = _exception.getId();
                    throw new org.omg.CORBA.UNKNOWN("Unexpected User Exception: "+ _exception_id);
                }
                finally
                {
                    this._releaseReply(_input);
                }
            }
            else
            {
                org.omg.CORBA.portable.ServantObject _so = _servant_preinvoke("getRedirectedServerLoc",_opsClass);
                if (_so == null)
                   continue;
                com.isti.quakewatch.common.qw_services.QWServicesOperations _self = (com.isti.quakewatch.common.qw_services.QWServicesOperations) _so.servant;
                try
                {
                    return _self.getRedirectedServerLoc();
                }
                finally
                {
                    _servant_postinvoke(_so);
                }
            }
        }
    }

    /**
     * Operation getServerIdNameStr
     */
    public String getServerIdNameStr()
    {
        while(true)
        {
            if (!this._is_local())
            {
                org.omg.CORBA.portable.InputStream _input = null;
                try
                {
                    org.omg.CORBA.portable.OutputStream _output = this._request("getServerIdNameStr",true);
                    _input = this._invoke(_output);
                    String _arg_ret = _input.read_string();
                    return _arg_ret;
                }
                catch(org.omg.CORBA.portable.RemarshalException _exception)
                {
                    continue;
                }
                catch(org.omg.CORBA.portable.ApplicationException _exception)
                {
                    String _exception_id = _exception.getId();
                    throw new org.omg.CORBA.UNKNOWN("Unexpected User Exception: "+ _exception_id);
                }
                finally
                {
                    this._releaseReply(_input);
                }
            }
            else
            {
                org.omg.CORBA.portable.ServantObject _so = _servant_preinvoke("getServerIdNameStr",_opsClass);
                if (_so == null)
                   continue;
                com.isti.quakewatch.common.qw_services.QWServicesOperations _self = (com.isti.quakewatch.common.qw_services.QWServicesOperations) _so.servant;
                try
                {
                    return _self.getServerIdNameStr();
                }
                finally
                {
                    _servant_postinvoke(_so);
                }
            }
        }
    }

    /**
     * Operation getServerHostAddrStr
     */
    public String getServerHostAddrStr()
    {
        while(true)
        {
            if (!this._is_local())
            {
                org.omg.CORBA.portable.InputStream _input = null;
                try
                {
                    org.omg.CORBA.portable.OutputStream _output = this._request("getServerHostAddrStr",true);
                    _input = this._invoke(_output);
                    String _arg_ret = _input.read_string();
                    return _arg_ret;
                }
                catch(org.omg.CORBA.portable.RemarshalException _exception)
                {
                    continue;
                }
                catch(org.omg.CORBA.portable.ApplicationException _exception)
                {
                    String _exception_id = _exception.getId();
                    throw new org.omg.CORBA.UNKNOWN("Unexpected User Exception: "+ _exception_id);
                }
                finally
                {
                    this._releaseReply(_input);
                }
            }
            else
            {
                org.omg.CORBA.portable.ServantObject _so = _servant_preinvoke("getServerHostAddrStr",_opsClass);
                if (_so == null)
                   continue;
                com.isti.quakewatch.common.qw_services.QWServicesOperations _self = (com.isti.quakewatch.common.qw_services.QWServicesOperations) _so.servant;
                try
                {
                    return _self.getServerHostAddrStr();
                }
                finally
                {
                    _servant_postinvoke(_so);
                }
            }
        }
    }

    /**
     * Operation getServerRevisionString
     */
    public String getServerRevisionString()
    {
        while(true)
        {
            if (!this._is_local())
            {
                org.omg.CORBA.portable.InputStream _input = null;
                try
                {
                    org.omg.CORBA.portable.OutputStream _output = this._request("getServerRevisionString",true);
                    _input = this._invoke(_output);
                    String _arg_ret = _input.read_string();
                    return _arg_ret;
                }
                catch(org.omg.CORBA.portable.RemarshalException _exception)
                {
                    continue;
                }
                catch(org.omg.CORBA.portable.ApplicationException _exception)
                {
                    String _exception_id = _exception.getId();
                    throw new org.omg.CORBA.UNKNOWN("Unexpected User Exception: "+ _exception_id);
                }
                finally
                {
                    this._releaseReply(_input);
                }
            }
            else
            {
                org.omg.CORBA.portable.ServantObject _so = _servant_preinvoke("getServerRevisionString",_opsClass);
                if (_so == null)
                   continue;
                com.isti.quakewatch.common.qw_services.QWServicesOperations _self = (com.isti.quakewatch.common.qw_services.QWServicesOperations) _so.servant;
                try
                {
                    return _self.getServerRevisionString();
                }
                finally
                {
                    _servant_postinvoke(_so);
                }
            }
        }
    }

    /**
     * Operation getStatusMsgTypeNameStr
     */
    public String getStatusMsgTypeNameStr()
    {
        while(true)
        {
            if (!this._is_local())
            {
                org.omg.CORBA.portable.InputStream _input = null;
                try
                {
                    org.omg.CORBA.portable.OutputStream _output = this._request("getStatusMsgTypeNameStr",true);
                    _input = this._invoke(_output);
                    String _arg_ret = _input.read_string();
                    return _arg_ret;
                }
                catch(org.omg.CORBA.portable.RemarshalException _exception)
                {
                    continue;
                }
                catch(org.omg.CORBA.portable.ApplicationException _exception)
                {
                    String _exception_id = _exception.getId();
                    throw new org.omg.CORBA.UNKNOWN("Unexpected User Exception: "+ _exception_id);
                }
                finally
                {
                    this._releaseReply(_input);
                }
            }
            else
            {
                org.omg.CORBA.portable.ServantObject _so = _servant_preinvoke("getStatusMsgTypeNameStr",_opsClass);
                if (_so == null)
                   continue;
                com.isti.quakewatch.common.qw_services.QWServicesOperations _self = (com.isti.quakewatch.common.qw_services.QWServicesOperations) _so.servant;
                try
                {
                    return _self.getStatusMsgTypeNameStr();
                }
                finally
                {
                    _servant_postinvoke(_so);
                }
            }
        }
    }

    /**
     * Operation getAltServersIdsListStr
     */
    public String getAltServersIdsListStr()
    {
        while(true)
        {
            if (!this._is_local())
            {
                org.omg.CORBA.portable.InputStream _input = null;
                try
                {
                    org.omg.CORBA.portable.OutputStream _output = this._request("getAltServersIdsListStr",true);
                    _input = this._invoke(_output);
                    String _arg_ret = _input.read_string();
                    return _arg_ret;
                }
                catch(org.omg.CORBA.portable.RemarshalException _exception)
                {
                    continue;
                }
                catch(org.omg.CORBA.portable.ApplicationException _exception)
                {
                    String _exception_id = _exception.getId();
                    throw new org.omg.CORBA.UNKNOWN("Unexpected User Exception: "+ _exception_id);
                }
                finally
                {
                    this._releaseReply(_input);
                }
            }
            else
            {
                org.omg.CORBA.portable.ServantObject _so = _servant_preinvoke("getAltServersIdsListStr",_opsClass);
                if (_so == null)
                   continue;
                com.isti.quakewatch.common.qw_services.QWServicesOperations _self = (com.isti.quakewatch.common.qw_services.QWServicesOperations) _so.servant;
                try
                {
                    return _self.getAltServersIdsListStr();
                }
                finally
                {
                    _servant_postinvoke(_so);
                }
            }
        }
    }

    /**
     * Operation getEventChLocStr
     */
    public String getEventChLocStr()
    {
        while(true)
        {
            if (!this._is_local())
            {
                org.omg.CORBA.portable.InputStream _input = null;
                try
                {
                    org.omg.CORBA.portable.OutputStream _output = this._request("getEventChLocStr",true);
                    _input = this._invoke(_output);
                    String _arg_ret = _input.read_string();
                    return _arg_ret;
                }
                catch(org.omg.CORBA.portable.RemarshalException _exception)
                {
                    continue;
                }
                catch(org.omg.CORBA.portable.ApplicationException _exception)
                {
                    String _exception_id = _exception.getId();
                    throw new org.omg.CORBA.UNKNOWN("Unexpected User Exception: "+ _exception_id);
                }
                finally
                {
                    this._releaseReply(_input);
                }
            }
            else
            {
                org.omg.CORBA.portable.ServantObject _so = _servant_preinvoke("getEventChLocStr",_opsClass);
                if (_so == null)
                   continue;
                com.isti.quakewatch.common.qw_services.QWServicesOperations _self = (com.isti.quakewatch.common.qw_services.QWServicesOperations) _so.servant;
                try
                {
                    return _self.getEventChLocStr();
                }
                finally
                {
                    _servant_postinvoke(_so);
                }
            }
        }
    }

    /**
     * Operation requestAliveMessage
     */
    public void requestAliveMessage()
    {
        while(true)
        {
            if (!this._is_local())
            {
                org.omg.CORBA.portable.InputStream _input = null;
                try
                {
                    org.omg.CORBA.portable.OutputStream _output = this._request("requestAliveMessage",true);
                    _input = this._invoke(_output);
                    return;
                }
                catch(org.omg.CORBA.portable.RemarshalException _exception)
                {
                    continue;
                }
                catch(org.omg.CORBA.portable.ApplicationException _exception)
                {
                    String _exception_id = _exception.getId();
                    throw new org.omg.CORBA.UNKNOWN("Unexpected User Exception: "+ _exception_id);
                }
                finally
                {
                    this._releaseReply(_input);
                }
            }
            else
            {
                org.omg.CORBA.portable.ServantObject _so = _servant_preinvoke("requestAliveMessage",_opsClass);
                if (_so == null)
                   continue;
                com.isti.quakewatch.common.qw_services.QWServicesOperations _self = (com.isti.quakewatch.common.qw_services.QWServicesOperations) _so.servant;
                try
                {
                    _self.requestAliveMessage();
                    return;
                }
                finally
                {
                    _servant_postinvoke(_so);
                }
            }
        }
    }

    /**
     * Operation requestMessages
     */
    public String requestMessages(long timeVal, long msgNum)
    {
        while(true)
        {
            if (!this._is_local())
            {
                org.omg.CORBA.portable.InputStream _input = null;
                try
                {
                    org.omg.CORBA.portable.OutputStream _output = this._request("requestMessages",true);
                    _output.write_longlong(timeVal);
                    _output.write_longlong(msgNum);
                    _input = this._invoke(_output);
                    String _arg_ret = _input.read_string();
                    return _arg_ret;
                }
                catch(org.omg.CORBA.portable.RemarshalException _exception)
                {
                    continue;
                }
                catch(org.omg.CORBA.portable.ApplicationException _exception)
                {
                    String _exception_id = _exception.getId();
                    throw new org.omg.CORBA.UNKNOWN("Unexpected User Exception: "+ _exception_id);
                }
                finally
                {
                    this._releaseReply(_input);
                }
            }
            else
            {
                org.omg.CORBA.portable.ServantObject _so = _servant_preinvoke("requestMessages",_opsClass);
                if (_so == null)
                   continue;
                com.isti.quakewatch.common.qw_services.QWServicesOperations _self = (com.isti.quakewatch.common.qw_services.QWServicesOperations) _so.servant;
                try
                {
                    return _self.requestMessages( timeVal,  msgNum);
                }
                finally
                {
                    _servant_postinvoke(_so);
                }
            }
        }
    }

    /**
     * Operation requestFilteredMessages
     */
    public String requestFilteredMessages(long timeVal, long msgNum, String domainTypeListStr)
    {
        while(true)
        {
            if (!this._is_local())
            {
                org.omg.CORBA.portable.InputStream _input = null;
                try
                {
                    org.omg.CORBA.portable.OutputStream _output = this._request("requestFilteredMessages",true);
                    _output.write_longlong(timeVal);
                    _output.write_longlong(msgNum);
                    _output.write_string(domainTypeListStr);
                    _input = this._invoke(_output);
                    String _arg_ret = _input.read_string();
                    return _arg_ret;
                }
                catch(org.omg.CORBA.portable.RemarshalException _exception)
                {
                    continue;
                }
                catch(org.omg.CORBA.portable.ApplicationException _exception)
                {
                    String _exception_id = _exception.getId();
                    throw new org.omg.CORBA.UNKNOWN("Unexpected User Exception: "+ _exception_id);
                }
                finally
                {
                    this._releaseReply(_input);
                }
            }
            else
            {
                org.omg.CORBA.portable.ServantObject _so = _servant_preinvoke("requestFilteredMessages",_opsClass);
                if (_so == null)
                   continue;
                com.isti.quakewatch.common.qw_services.QWServicesOperations _self = (com.isti.quakewatch.common.qw_services.QWServicesOperations) _so.servant;
                try
                {
                    return _self.requestFilteredMessages( timeVal,  msgNum,  domainTypeListStr);
                }
                finally
                {
                    _servant_postinvoke(_so);
                }
            }
        }
    }

    /**
     * Operation requestSourcedMessages
     */
    public String requestSourcedMessages(long timeVal, String hostMsgNumListStr)
    {
        while(true)
        {
            if (!this._is_local())
            {
                org.omg.CORBA.portable.InputStream _input = null;
                try
                {
                    org.omg.CORBA.portable.OutputStream _output = this._request("requestSourcedMessages",true);
                    _output.write_longlong(timeVal);
                    _output.write_string(hostMsgNumListStr);
                    _input = this._invoke(_output);
                    String _arg_ret = _input.read_string();
                    return _arg_ret;
                }
                catch(org.omg.CORBA.portable.RemarshalException _exception)
                {
                    continue;
                }
                catch(org.omg.CORBA.portable.ApplicationException _exception)
                {
                    String _exception_id = _exception.getId();
                    throw new org.omg.CORBA.UNKNOWN("Unexpected User Exception: "+ _exception_id);
                }
                finally
                {
                    this._releaseReply(_input);
                }
            }
            else
            {
                org.omg.CORBA.portable.ServantObject _so = _servant_preinvoke("requestSourcedMessages",_opsClass);
                if (_so == null)
                   continue;
                com.isti.quakewatch.common.qw_services.QWServicesOperations _self = (com.isti.quakewatch.common.qw_services.QWServicesOperations) _so.servant;
                try
                {
                    return _self.requestSourcedMessages( timeVal,  hostMsgNumListStr);
                }
                finally
                {
                    _servant_postinvoke(_so);
                }
            }
        }
    }

    /**
     * Operation requestSourcedFilteredMessages
     */
    public String requestSourcedFilteredMessages(long timeVal, String hostMsgNumListStr, String domainTypeListStr)
    {
        while(true)
        {
            if (!this._is_local())
            {
                org.omg.CORBA.portable.InputStream _input = null;
                try
                {
                    org.omg.CORBA.portable.OutputStream _output = this._request("requestSourcedFilteredMessages",true);
                    _output.write_longlong(timeVal);
                    _output.write_string(hostMsgNumListStr);
                    _output.write_string(domainTypeListStr);
                    _input = this._invoke(_output);
                    String _arg_ret = _input.read_string();
                    return _arg_ret;
                }
                catch(org.omg.CORBA.portable.RemarshalException _exception)
                {
                    continue;
                }
                catch(org.omg.CORBA.portable.ApplicationException _exception)
                {
                    String _exception_id = _exception.getId();
                    throw new org.omg.CORBA.UNKNOWN("Unexpected User Exception: "+ _exception_id);
                }
                finally
                {
                    this._releaseReply(_input);
                }
            }
            else
            {
                org.omg.CORBA.portable.ServantObject _so = _servant_preinvoke("requestSourcedFilteredMessages",_opsClass);
                if (_so == null)
                   continue;
                com.isti.quakewatch.common.qw_services.QWServicesOperations _self = (com.isti.quakewatch.common.qw_services.QWServicesOperations) _so.servant;
                try
                {
                    return _self.requestSourcedFilteredMessages( timeVal,  hostMsgNumListStr,  domainTypeListStr);
                }
                finally
                {
                    _servant_postinvoke(_so);
                }
            }
        }
    }

    /**
     * Operation clientStatusCheck
     */
    public boolean clientStatusCheck(String clientInfoStr)
    {
        while(true)
        {
            if (!this._is_local())
            {
                org.omg.CORBA.portable.InputStream _input = null;
                try
                {
                    org.omg.CORBA.portable.OutputStream _output = this._request("clientStatusCheck",true);
                    _output.write_string(clientInfoStr);
                    _input = this._invoke(_output);
                    boolean _arg_ret = _input.read_boolean();
                    return _arg_ret;
                }
                catch(org.omg.CORBA.portable.RemarshalException _exception)
                {
                    continue;
                }
                catch(org.omg.CORBA.portable.ApplicationException _exception)
                {
                    String _exception_id = _exception.getId();
                    throw new org.omg.CORBA.UNKNOWN("Unexpected User Exception: "+ _exception_id);
                }
                finally
                {
                    this._releaseReply(_input);
                }
            }
            else
            {
                org.omg.CORBA.portable.ServantObject _so = _servant_preinvoke("clientStatusCheck",_opsClass);
                if (_so == null)
                   continue;
                com.isti.quakewatch.common.qw_services.QWServicesOperations _self = (com.isti.quakewatch.common.qw_services.QWServicesOperations) _so.servant;
                try
                {
                    return _self.clientStatusCheck( clientInfoStr);
                }
                finally
                {
                    _servant_postinvoke(_so);
                }
            }
        }
    }

    /**
     * Operation getClientUpgradeInfo
     */
    public String getClientUpgradeInfo(String clientInfoStr)
    {
        while(true)
        {
            if (!this._is_local())
            {
                org.omg.CORBA.portable.InputStream _input = null;
                try
                {
                    org.omg.CORBA.portable.OutputStream _output = this._request("getClientUpgradeInfo",true);
                    _output.write_string(clientInfoStr);
                    _input = this._invoke(_output);
                    String _arg_ret = _input.read_string();
                    return _arg_ret;
                }
                catch(org.omg.CORBA.portable.RemarshalException _exception)
                {
                    continue;
                }
                catch(org.omg.CORBA.portable.ApplicationException _exception)
                {
                    String _exception_id = _exception.getId();
                    throw new org.omg.CORBA.UNKNOWN("Unexpected User Exception: "+ _exception_id);
                }
                finally
                {
                    this._releaseReply(_input);
                }
            }
            else
            {
                org.omg.CORBA.portable.ServantObject _so = _servant_preinvoke("getClientUpgradeInfo",_opsClass);
                if (_so == null)
                   continue;
                com.isti.quakewatch.common.qw_services.QWServicesOperations _self = (com.isti.quakewatch.common.qw_services.QWServicesOperations) _so.servant;
                try
                {
                    return _self.getClientUpgradeInfo( clientInfoStr);
                }
                finally
                {
                    _servant_postinvoke(_so);
                }
            }
        }
    }

    /**
     * Operation getStatusReportTime
     */
    public long getStatusReportTime()
    {
        while(true)
        {
            if (!this._is_local())
            {
                org.omg.CORBA.portable.InputStream _input = null;
                try
                {
                    org.omg.CORBA.portable.OutputStream _output = this._request("getStatusReportTime",true);
                    _input = this._invoke(_output);
                    long _arg_ret = _input.read_longlong();
                    return _arg_ret;
                }
                catch(org.omg.CORBA.portable.RemarshalException _exception)
                {
                    continue;
                }
                catch(org.omg.CORBA.portable.ApplicationException _exception)
                {
                    String _exception_id = _exception.getId();
                    throw new org.omg.CORBA.UNKNOWN("Unexpected User Exception: "+ _exception_id);
                }
                finally
                {
                    this._releaseReply(_input);
                }
            }
            else
            {
                org.omg.CORBA.portable.ServantObject _so = _servant_preinvoke("getStatusReportTime",_opsClass);
                if (_so == null)
                   continue;
                com.isti.quakewatch.common.qw_services.QWServicesOperations _self = (com.isti.quakewatch.common.qw_services.QWServicesOperations) _so.servant;
                try
                {
                    return _self.getStatusReportTime();
                }
                finally
                {
                    _servant_postinvoke(_so);
                }
            }
        }
    }

    /**
     * Operation getStatusReportData
     */
    public String getStatusReportData()
    {
        while(true)
        {
            if (!this._is_local())
            {
                org.omg.CORBA.portable.InputStream _input = null;
                try
                {
                    org.omg.CORBA.portable.OutputStream _output = this._request("getStatusReportData",true);
                    _input = this._invoke(_output);
                    String _arg_ret = _input.read_string();
                    return _arg_ret;
                }
                catch(org.omg.CORBA.portable.RemarshalException _exception)
                {
                    continue;
                }
                catch(org.omg.CORBA.portable.ApplicationException _exception)
                {
                    String _exception_id = _exception.getId();
                    throw new org.omg.CORBA.UNKNOWN("Unexpected User Exception: "+ _exception_id);
                }
                finally
                {
                    this._releaseReply(_input);
                }
            }
            else
            {
                org.omg.CORBA.portable.ServantObject _so = _servant_preinvoke("getStatusReportData",_opsClass);
                if (_so == null)
                   continue;
                com.isti.quakewatch.common.qw_services.QWServicesOperations _self = (com.isti.quakewatch.common.qw_services.QWServicesOperations) _so.servant;
                try
                {
                    return _self.getStatusReportData();
                }
                finally
                {
                    _servant_postinvoke(_so);
                }
            }
        }
    }

    /**
     * Operation getCertificateFileData
     */
    public byte[] getCertificateFileData()
    {
        while(true)
        {
            if (!this._is_local())
            {
                org.omg.CORBA.portable.InputStream _input = null;
                try
                {
                    org.omg.CORBA.portable.OutputStream _output = this._request("getCertificateFileData",true);
                    _input = this._invoke(_output);
                    byte[] _arg_ret = com.isti.quakewatch.common.qw_services.QWServicesPackage.ByteArrayHelper.read(_input);
                    return _arg_ret;
                }
                catch(org.omg.CORBA.portable.RemarshalException _exception)
                {
                    continue;
                }
                catch(org.omg.CORBA.portable.ApplicationException _exception)
                {
                    String _exception_id = _exception.getId();
                    throw new org.omg.CORBA.UNKNOWN("Unexpected User Exception: "+ _exception_id);
                }
                finally
                {
                    this._releaseReply(_input);
                }
            }
            else
            {
                org.omg.CORBA.portable.ServantObject _so = _servant_preinvoke("getCertificateFileData",_opsClass);
                if (_so == null)
                   continue;
                com.isti.quakewatch.common.qw_services.QWServicesOperations _self = (com.isti.quakewatch.common.qw_services.QWServicesOperations) _so.servant;
                try
                {
                    return _self.getCertificateFileData();
                }
                finally
                {
                    _servant_postinvoke(_so);
                }
            }
        }
    }

    /**
     * Operation disconnectClient
     */
    public void disconnectClient(String clientInfoStr)
    {
        while(true)
        {
            if (!this._is_local())
            {
                org.omg.CORBA.portable.InputStream _input = null;
                try
                {
                    org.omg.CORBA.portable.OutputStream _output = this._request("disconnectClient",true);
                    _output.write_string(clientInfoStr);
                    _input = this._invoke(_output);
                    return;
                }
                catch(org.omg.CORBA.portable.RemarshalException _exception)
                {
                    continue;
                }
                catch(org.omg.CORBA.portable.ApplicationException _exception)
                {
                    String _exception_id = _exception.getId();
                    throw new org.omg.CORBA.UNKNOWN("Unexpected User Exception: "+ _exception_id);
                }
                finally
                {
                    this._releaseReply(_input);
                }
            }
            else
            {
                org.omg.CORBA.portable.ServantObject _so = _servant_preinvoke("disconnectClient",_opsClass);
                if (_so == null)
                   continue;
                com.isti.quakewatch.common.qw_services.QWServicesOperations _self = (com.isti.quakewatch.common.qw_services.QWServicesOperations) _so.servant;
                try
                {
                    _self.disconnectClient( clientInfoStr);
                    return;
                }
                finally
                {
                    _servant_postinvoke(_so);
                }
            }
        }
    }

}
