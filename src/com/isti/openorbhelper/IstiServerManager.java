//IstiServerManager.java:  Extends 'org.openorb.orb.net.ServerManagerImpl'
//                         to expose the 'getChannelsInfo()' method.
//
// 12/10/2003 -- [ET]
//

package com.isti.openorbhelper;

/**
 * Class IstiServerManager extends 'org.openorb.orb.net.ServerManagerImpl'
 * to expose the 'getChannelsInfo()' method.
 */
public class IstiServerManager extends org.openorb.orb.net.ServerManagerImpl
{
  /**
   * Construct a new server manager.
   * @param orbObj The controling orb.
   */
  public IstiServerManager(org.omg.CORBA.ORB orbObj)
  {
    super(orbObj);
  }

  /**
   * Returns an array of strings describing the active server channels.
   * @return An array of strings.
   */
  public String [] getChannelsInfo()
  {
    return super.getChannelsInfo();
  }
}
