//OpenOrbDeps.java:  Contains references to OpenORB classes needed to
//                   create a stand-alone Notification Service server
//                   application 'jar' file using JBuilder.
//
//  4/21/2003 -- [ET]  Initial version.
// 12/15/2003 -- [ET]  Added references for 'EvaluatoryUtility' to support
//                     event filtering.
// 11/29/2004 -- [ET]  Added "SocketStreamDecorationStrategy" references.
// 11/11/2005 -- [ET]  Added "UNKNOWNHelper" reference.
//

package com.isti.openorbhelper;

/**
 * Class OpenOrbDeps contains references to OpenORB classes needed to
 * create a stand-alone Notification Service server application 'jar'
 * file using JBuilder.
 */
public class OpenOrbDeps
{
    //these classes are needed by any app using the Notification Service:
  private static final org.openorb.notify.Service dummySvcObj = null;
  private static final org.openorb.orb.config.OpenORBLoader
                                                         dummyLdrObj = null;
  private static final org.openorb.util.urlhandler.HandlerFactory
                                                       dummyHFactObj = null;
  private static final org.openorb.util.urlhandler.resource.Handler
                                                        dummyHdlrObj = null;
  private static final org.openorb.util.urlhandler.classpath.Handler
                                                      dummyCPHdlrObj = null;
  private static final org.openorb.orb.iiop.IIOPProtocolInitializer
                                                      dummyPrInitObj = null;
  private static final org.openorb.orb.adapter.poa.POAInitializer
                                                     dummyPoaInitObj = null;
  private static final org.openorb.orb.adapter.fwd.ForwardInitializer
                                                     dummyFwdInitObj = null;
  private static final org.openorb.pss.Initializer dummyPInitObj = null;
  private static final org.openorb.orb.net.ServerManagerImpl
                                                      dummySvrMgrObj = null;
  private static final org.openorb.orb.net.ClientManagerImpl
                                                      dummyCltMgrObj = null;
  private static final org.openorb.orb.iiop.IIOPTransportClientInitializer
                                                    dummyTCltInitObj = null;
  private static final org.openorb.orb.messaging.MessagingInitializer
                                                     dummyMsgInitObj = null;
  private static final org.openorb.orb.pi.SimpleServerManager
                                                     dummySSvrMgrObj = null;
  private static final org.openorb.orb.pi.SimpleIORManager
                                                     dummySIorMgrObj = null;
  private static final org.openorb.orb.net.
       PriorityBoostingSocketStreamDecorationStrategy dummyPbssdsObj = null;
  private static final org.openorb.orb.net.
               BufferingSocketStreamDecorationStrategy dummyBssdsObj = null;
  private static final org.openorb.orb.net.
                  LegacySocketStreamDecorationStrategy dummyLssdsObj = null;

         //this class is required but also deprecated, so it is
         // included via the JBuilder-archive content:
//  private static final org.openorb.orb.config.OpenORBConnector
//                                                     dummyOrbConnObj = null;

    //these classes are needed to support running event channels:
  private static final memory.org.openorb.notify.persistence.EventChannelBase
                                                        dummyMECBObj = null;
  private static final memory.org.openorb.notify.persistence.EventChannelHomeBase
                                                       dummyMECHBObj = null;
  private static final file.org.openorb.notify.persistence.EventChannelBase
                                                        dummyFECBObj = null;
  private static final file.org.openorb.notify.persistence.EventChannelHomeBase
                                                       dummyFECHBObj = null;

  private static final memory.org.openorb.notify.persistence.ConsumerAdminBase
                                                        dummyMCABObj = null;
  private static final memory.org.openorb.notify.persistence.ConsumerAdminHomeBase
                                                       dummyMCAHBObj = null;
  private static final file.org.openorb.notify.persistence.ConsumerAdminBase
                                                        dummyFCABObj = null;
  private static final file.org.openorb.notify.persistence.ConsumerAdminHomeBase
                                                       dummyFCAHBObj = null;

  private static final memory.org.openorb.notify.persistence.SupplierAdminBase
                                                        dummyMSABObj = null;
  private static final memory.org.openorb.notify.persistence.SupplierAdminHomeBase
                                                       dummyMSAHBObj = null;
  private static final file.org.openorb.notify.persistence.SupplierAdminBase
                                                        dummyFSABObj = null;
  private static final file.org.openorb.notify.persistence.SupplierAdminHomeBase
                                                       dummyFSAHBObj = null;

  private static final memory.org.openorb.notify.persistence.FilterBase
                                                         dummyMFBObj = null;
  private static final memory.org.openorb.notify.persistence.FilterHomeBase
                                                        dummyMFHBObj = null;
  private static final file.org.openorb.notify.persistence.FilterBase
                                                         dummyFFBObj = null;
  private static final file.org.openorb.notify.persistence.FilterHomeBase
                                                        dummyFFHBObj = null;

  private static final memory.org.openorb.notify.persistence.MappingFilterBase
                                                        dummyMMFBObj = null;
  private static final memory.org.openorb.notify.persistence.MappingFilterHomeBase
                                                       dummyMMFHBObj = null;
  private static final file.org.openorb.notify.persistence.MappingFilterBase
                                                        dummyFMFBObj = null;
  private static final file.org.openorb.notify.persistence.MappingFilterHomeBase
                                                       dummyFMFHBObj = null;

  private static final memory.org.openorb.notify.persistence.ProxyConsumerBase
                                                        dummyMPCBObj = null;
  private static final memory.org.openorb.notify.persistence.ProxyConsumerHomeBase
                                                       dummyMPCHBObj = null;
  private static final file.org.openorb.notify.persistence.ProxyConsumerBase
                                                        dummyFPCBObj = null;
  private static final file.org.openorb.notify.persistence.ProxyConsumerHomeBase
                                                       dummyFPCHBObj = null;

  private static final memory.org.openorb.notify.persistence.ProxySupplierBase
                                                        dummyMPSBObj = null;
  private static final memory.org.openorb.notify.persistence.ProxySupplierHomeBase
                                                       dummyMPSHBObj = null;
  private static final file.org.openorb.notify.persistence.ProxySupplierBase
                                                        dummyFPSBObj = null;
  private static final file.org.openorb.notify.persistence.ProxySupplierHomeBase
                                                       dummyFPSHBObj = null;

  private static final memory.org.openorb.notify.persistence.EventBase
                                                         dummyMEBObj = null;
  private static final memory.org.openorb.notify.persistence.EventHomeBase
                                                        dummyMEHBObj = null;
  private static final file.org.openorb.notify.persistence.EventBase
                                                         dummyFEBObj = null;
  private static final file.org.openorb.notify.persistence.EventHomeBase
                                                        dummyFEHBObj = null;

  private static final memory.org.openorb.notify.persistence.EventQueueBase
                                                        dummyMEQBObj = null;
  private static final memory.org.openorb.notify.persistence.EventQueueHomeBase
                                                       dummyMEQHBObj = null;
  private static final file.org.openorb.notify.persistence.EventQueueBase
                                                        dummyFEQBObj = null;
  private static final file.org.openorb.notify.persistence.EventQueueHomeBase
                                                       dummyFEQHBObj = null;

  private static final org.openorb.constraint.Constraint
                                                       dummyConstObj = null;
  private static final org.openorb.constraint.ConstraintEvaluator
                                                       dummyCEvalObj = null;
  private static final org.openorb.constraint.InvalidValue
                                                      dummyInvValObj = null;
  private static final org.openorb.constraint.InvalidConstraint
                                                      dummyInvConObj = null;

    //Xerces classes needed for Java 1.3 support (Java 1.4 includes Xerces):
  private static final org.apache.xerces.jaxp.DocumentBuilderFactoryImpl
                                                       dummyDocBFact = null;
  private static final org.apache.xerces.parsers.XML11Configuration
                                                       dummyXml11Obj = null;
  private static final org.apache.xerces.impl.dv.dtd.DTDDVFactoryImpl
                                                   dummyDTDDVFactObj = null;

    //support classes for "CORBA.SystemException" classes
    // (needed by any app using the Notification Service):
  private static final org.omg.CORBA.UNKNOWNHelper h00obj = null;
  private static final org.omg.CORBA.BAD_PARAMHelper h01obj = null;
  private static final org.omg.CORBA.NO_MEMORYHelper h02obj = null;
  private static final org.omg.CORBA.IMP_LIMITHelper h03obj = null;
  private static final org.omg.CORBA.COMM_FAILUREHelper h04obj = null;
  private static final org.omg.CORBA.INV_OBJREFHelper h05obj = null;
  private static final org.omg.CORBA.NO_PERMISSIONHelper h06obj = null;
  private static final org.omg.CORBA.INTERNALHelper h07obj = null;
  private static final org.omg.CORBA.MARSHALHelper h08obj = null;
  private static final org.omg.CORBA.INITIALIZEHelper h09obj = null;
  private static final org.omg.CORBA.NO_IMPLEMENTHelper h10obj = null;
  private static final org.omg.CORBA.BAD_TYPECODEHelper h11obj = null;
  private static final org.omg.CORBA.BAD_OPERATIONHelper h12obj = null;
  private static final org.omg.CORBA.NO_RESOURCESHelper h13obj = null;
  private static final org.omg.CORBA.NO_RESPONSEHelper h14obj = null;
  private static final org.omg.CORBA.PERSIST_STOREHelper h15obj = null;
  private static final org.omg.CORBA.BAD_INV_ORDERHelper h16obj = null;
  private static final org.omg.CORBA.TRANSIENTHelper h17obj = null;
  private static final org.omg.CORBA.FREE_MEMHelper h18obj = null;
  private static final org.omg.CORBA.INV_IDENTHelper h19obj = null;
  private static final org.omg.CORBA.INV_FLAGHelper h20obj = null;
  private static final org.omg.CORBA.INTF_REPOSHelper h21obj = null;
  private static final org.omg.CORBA.BAD_CONTEXTHelper h22obj = null;
  private static final org.omg.CORBA.OBJ_ADAPTERHelper h23obj = null;
  private static final org.omg.CORBA.DATA_CONVERSIONHelper h24obj = null;
  private static final org.omg.CORBA.OBJECT_NOT_EXISTHelper h25obj = null;
  private static final org.omg.CORBA.TRANSACTION_REQUIREDHelper h26obj = null;
  private static final org.omg.CORBA.TRANSACTION_ROLLEDBACKHelper h27obj = null;
  private static final org.omg.CORBA.INVALID_TRANSACTIONHelper h28obj = null;
  private static final org.omg.CORBA.INV_POLICYHelper h29obj = null;
  private static final org.omg.CORBA.CODESET_INCOMPATIBLEHelper h30obj = null;
  private static final org.omg.CORBA.REBINDHelper h31obj = null;
  private static final org.omg.CORBA.TIMEOUTHelper h32obj = null;
  private static final org.omg.CORBA.TRANSACTION_UNAVAILABLEHelper h33obj = null;
  private static final org.omg.CORBA.TRANSACTION_MODEHelper h34obj = null;
  private static final org.omg.CORBA.BAD_QOSHelper h35obj = null;
}
