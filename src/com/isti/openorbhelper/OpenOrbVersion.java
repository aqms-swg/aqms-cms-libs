//OpenOrbVersion.java:  Holds the version string for the OpenORB codeset
//                      in use (plus some additional version information).
//
//  4/17/2003 -- [ET]  Version "17Apr2003".
//  4/30/2003 -- [ET]  Version "30Apr2003_mod".
//  5/15/2003 -- [ET]  Version "30Apr2003_mod2".
// 12/10/2003 -- [ET]  Version "10Dec2003".
//  2/23/2004 -- [ET]  Version "23Feb2004_mod".
// 11/29/2004 -- [ET]  Version "17Nov2004".
//  3/13/2006 -- [ET]  Version "20060309_isti1".
// 11/29/2006 -- [ET]  Version "20061129_isti2".
//  6/14/2007 -- [ET]  Version "20061129_isti3".
//

package com.isti.openorbhelper;

import org.openorb.util.ORBUtils;

/**
 * Class OpenOrbVersion holds the version string for the OpenORB codeset
 * in use (plus some additional version information).
 */
public class OpenOrbVersion
{
    /** The version string for the OpenORB codeset . */
  public static final String VERSION_STR = ORBUtils.MAJOR + "." +
                 ORBUtils.MINOR + "." + ORBUtils.BUGFIX + "_20061129_isti3";
}
