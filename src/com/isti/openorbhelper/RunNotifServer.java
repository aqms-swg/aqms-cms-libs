//RunNotifServer.java:  Creates and starts an instance of the OpenORB
//                      Notification Service.
//
//  5/22/2003 -- [ET]  Version 2.13:  Added 'connectsLogIntervalSecs' option
//                     to log active connections.
//   6/4/2003 -- [ET]  Version 2.131:  OrbManager modification.
//   6/9/2003 -- [ET]  Version 2.132:  Added 'connectsLogFileName'
//                     and new default of logging active connections
//                     to "log/NotifServerConnects.log" (if
//                     'connectsLogIntervalSecs' > 0).
//  6/10/2003 -- [ET]  Version 2.133:  Added 'logFilesMaxAgeInDays' and
//                     'connectsLogSwitchDays' and support for log files
//                     with date codes.
//   7/9/2003 -- [ET]  Version 2.134:  Added 'logFilesMaxAgeInDays' support
//                     for console-redirect-output log file.
//  7/28/2003 -- [ET]  Version 2.14:  Put memory-info output into a
//                     separate log file and added 'memoryLogMaxAgeInDays'
//                     & 'memoryLogSwitchDays' parameters.
// 10/27/2003 -- [ET]  Version 2.141:  Rebuilt with updated OpenORB
//                     libraries (cvs_20031027 + ISTI mods).
// 12/10/2003 -- [ET]  Version 2.142:  Rebuilt with updated OpenORB
//                     libraries (cvs_20031210).
//   2/4/2004 -- [ET]  Version 2.15:  Updated version number for release.
//  2/12/2004 -- [ET]  Version 2.152:  Increased 'maxConsumers' in from
//                     200 to 2000.
//  2/25/2004 -- [ET]  Version 2.164:  Rebuilt with updated and modified
//                     OpenORB libraries (cvs_20040223_mod).
//  11/9/2004 -- [ET]  Version 2.17:  Added 'publishNumericIPFlag' ("-n")
//                     parameter.
// 11/29/2004 -- [ET]  Version 2.18:  Updated OpenORB to "nightly" build
//                     from 2004-11-17 (for Java 1.5 support).
//  3/16/2005 -- [ET]  Minor modifications (imports, commented-out method
//                     'createAdminChannelViaThread()').
//  3/14/2006 -- [ET]  Version 2.19:  Updated OpenORB to "20060309_isti1"
//                     version (for "undeliveredEventsMap" memory-leak fix);
//                     added 'resourcesTimeout' ("-rt") parameter (sets
//                     number of seconds before dropped client-connections
//                     are reaped) and setup default of 120 seconds (used
//                     to be 1000 seconds).
// 11/29/2006 -- [ET]  Version 2.191:  Updated OpenORB to "20061129_isti2"
//                     version (for stuck-connections/channel-reaper fix).
//  12/8/2006 -- [ET]  Version 2.192:  Added support for status log file.
//  6/14/2007 -- [ET]  Version 2.193:  Updated OpenORB to "20061129_isti3"
//                     version (for more stuck-connections/channel-reaper
//                     fixes).
//

package com.isti.openorbhelper;

import java.util.Properties;
import java.util.Vector;
import java.io.File;
import java.io.PrintStream;
import java.io.BufferedOutputStream;
import org.apache.avalon.framework.parameters.Parameters;
import com.isti.util.Version;
import com.isti.util.UtilFns;
import com.isti.util.LogFile;
import com.isti.util.CfgProperties;
import com.isti.util.CfgPropItem;
import com.isti.util.IstiNotifyThread;
import com.isti.util.ThreadLogger;
import com.isti.util.MemoryInfoLogger;
import com.isti.quakewatch.common.QWConstants;

/**
 * Class RunNotifServer creates and starts an instance of the OpenORB
 * Notification Service.
 */
public class RunNotifServer implements Version
{
                        //name string for program:
  public static final String PROGRAM_NAME =
                                          "ISTI Notification Server Runner";
                        //revision string for program:
  public static final String REVISION_STRING = PROGRAM_NAME + ", Version " +
                                                            PROGRAM_VERSION;
  private static final String VER1_STR = "version";   //parameter names for
  private static final String VER2_STR = "v";         // version info disp
                        //name of (optional) configuration file:
  private static final String CONFIG_FILENAME = "NotificationServer.ini";
                        //name of status-logging output file:
  private static final String STATUS_LOGGER_FNAME =
                                                "log/NotifServerStatus.log";
                        //name of optional thread-logging output file:
  private static final String THREAD_LOGGER_FNAME =
                                               "log/NotifServerThreads.log";
                        //name of optional memory-info-logging output file:
  private static final String MEMINFO_LOGGER_FNAME =
                                               "log/NotifServerMemInfo.log";
                        //default name of connections-logging output file:
  private static final String CONNECTS_LOGGER_FNAME =
                                              "log/NotifServerConnects.log";
                        //header string for status-logging output:
  public static final String STATUSLOG_HEADER_STR =
                                               "Notification Server Status";
                        //"conn-count" string for status-logging output:
  public static final String STATUSLOG_CONNCOUNT_STR = "ConnCount = ";
                        //"thread-count" string for status-logging output:
  public static final String STATUSLOG_THREADCOUNT_STR = "ThreadCount = ";
                        //"memory" string for status-logging output:
  public static final String STATUSLOG_MEMORY_STR = "Memory:  ";
                        //CORBA ORB object for notification server:
  private org.omg.CORBA.ORB notifServerOrbObj = null;
                        //server-manager object:
  private IstiServerManager istiServerManagerObj = null;
                        //cache array of string for server-channels info:
  private String [] serverChannelsInfoCacheArr = null;
                        //time 'serverChannelsInfoArr' data fetched:
  private long serverChInfoArrTime = 0;
                        //thread sync object for 'getServerChannelsInfo()':
  private final Object serverChannelsInfoSyncObj = new Object();
                             //stream object for redirected console output:
  private PrintStream consoleRedirectStream = null;
                             //handle for status logger:
  private StatusInfoLogger statusLoggerObj = null;
  private ThreadLogger threadLoggerObj = null;   //handle for thread logger
  private MemoryInfoLogger memoryLoggerObj = null;    //memory info logger
                             //handle for connections-info logger:
  private ConnectsInfoLogger connectsLoggerObj = null;

         //configuration properties:
  private final CfgProperties cfgProps = new CfgProperties();
         //port number for notification server:
  private final CfgPropItem serverPortNumberProp =
                         cfgProps.add("serverPortNumber",Integer.valueOf(39988),
                                 "p","Port number for notification server");
         //host address for notification server:
  private final CfgPropItem serverHostAddressProp =
                                        cfgProps.add("serverHostAddress","",
                                "a","Host address for notification server");
         //flag to use numberic IPs instead of host names:
  private final CfgPropItem publishNumericIPFlagProp =
                          cfgProps.add("publishNumericIPFlag",Boolean.FALSE,
                              "n","Use numberic IPs instead of host names");
         //name of optional console output redirect file:
  private final CfgPropItem consoleRedirectFileNameProp =
        cfgProps.add("consoleRedirectFileName","log/NotificationServer.log",
                                        "r","Console output redirect file");
              //maximum age for log files (days, 0=infinite):
  private final CfgPropItem logFilesMaxAgeInDaysProp =
                    cfgProps.add("logFilesMaxAgeInDays",Integer.valueOf(30),
                        "x","Maximum age for log files (days, 0=infinite)");
         //flag to display IOR string for Notification Service:
  private final CfgPropItem displayServiceIORFlagProp =
        cfgProps.add("displayServiceIORFlag",Boolean.FALSE,
                                            "i","Display service IOR flag");
         //flag to display Corbaloc URL string for event channel factory:
  private final CfgPropItem displayCorbalocURLFlagProp =
        cfgProps.add("displayCorbalocURLFlag",Boolean.FALSE,
                                           "u","Display Corbaloc URL flag");
         //name of persistent storage directory:
  private final CfgPropItem pssDataDirectoryNameProp =
                              cfgProps.add("pssDataDirectoryName","pssdata",
                                        "y","Persistent storage directory");
         //name of persistent storage data-store:
  private final CfgPropItem pssDataStoreNameProp =
                                 cfgProps.add("pssDataStoreName","notifsvc",
                                  "z","Persistent storage data-store name");
         //maximum number of consumers that may be connected:
  private final CfgPropItem maxConsumersProp =
                              cfgProps.add("maxConsumers",Integer.valueOf(2000),
                                         "m","Maximum number of consumers");
         //maximum number of suppliers that may be connected:
  private final CfgPropItem maxSuppliersProp =
                                cfgProps.add("maxSuppliers",Integer.valueOf(10),
                                         "s","Maximum number of suppliers");
         //maximum number of events that may be queued in channel:
  private final CfgPropItem maxQueueLengthProp =
                             cfgProps.add("maxQueueLength",Integer.valueOf(100),
                                            "q","Maximum number of events");
         //resources timeout (secs), when event, connection, filter reaped:
  private final CfgPropItem resourcesTimeoutProp =
                           cfgProps.add("resourcesTimeout",Integer.valueOf(120),
                                           "rt","Resources timeout (secs)");
         //enables BiDirectional IIOP for event channel suppliers:
  private final CfgPropItem biDirectionalFlagProp =
                         cfgProps.add("biDirectionalFlag",Boolean.valueOf(true),
                                             "b","BiDirectional IIOP flag");
         //maximum number of threads allowed in thread pool:
  private final CfgPropItem maxThreadPoolSizeProp =
                          cfgProps.add("maxThreadPoolSize",Integer.valueOf(200),
                                          "tp","Maximum number of threads");
         //delay between reaping of unused connections:
  private final CfgPropItem reapCloseDelaySecProp =
                          cfgProps.add("reapCloseDelaySec",Integer.valueOf(120),
                                         "o","Connection reap delay (sec)");
         //flag to bind to Naming Service:
  private final CfgPropItem bindToNameServiceFlagProp =
                       cfgProps.add("bindToNamingServiceFlag",Boolean.FALSE,
                                        "bn","Bind to Naming Service flag");
         //specifies the logging trace level used for all categories;
         // legal values are "debug", "info", "warn", "error" and "fatal":
  private final CfgPropItem logPriorityLevelProp =
                                     cfgProps.add("logPriorityLevel","warn",
                                                 "l","Logging trace level");
         //enables debug logging for the event queues:
  private final CfgPropItem logEventQueueFlagProp =
                             cfgProps.add("logEventQueueFlag",Boolean.FALSE,
                                            "e","Event queue logging flag");
         //# of seconds between status log entries (0=none):
  private final CfgPropItem statusLogIntervalSecsProp =
                       cfgProps.add("statusLogIntervalSecs",Integer.valueOf(60),
                                     "si","Status logging interval (secs)");
         //maximum age for status log files (days, 0=infinite):
  private final CfgPropItem statusLogMaxAgeInDaysProp =
                        cfgProps.add("statusLogMaxAgeInDays",Integer.valueOf(3),
                            "sx","Maximum age for status log files (days)");
         //switch interval for status log files (days, 0 = no swapping):
  private final CfgPropItem statusLogSwitchDaysProp =
                          cfgProps.add("statusLogSwitchDays",Integer.valueOf(1),
                             "ss","Switch interval for status logs (days)");
         //min # of ms between "NotifServerThreads.log" entries (0=none):
  private final CfgPropItem threadLogIntervalMSecsProp =
                      cfgProps.add("threadLogIntervalMSecs",Integer.valueOf(0),
                                "t","Minimum thread logging interval (ms)");
         //# of seconds between memory-information log entries (0=none):
  private final CfgPropItem memoryLogIntervalSecsProp =
                        cfgProps.add("memoryLogIntervalSecs",Integer.valueOf(0),
                                 "f","Memory info logging interval (secs)");
         //maximum age for memory-info log files (days, 0=infinite):
  private final CfgPropItem memoryLogMaxAgeInDaysProp =
                      cfgProps.add("memoryLogMaxAgeInDays",Integer.valueOf(180),
                            "ma","Maximum age for memory-info logs (days)");
         //switch interval for mem log files (days, 0 = no swapping):
  private final CfgPropItem memoryLogSwitchDaysProp =
                    cfgProps.add("memoryLogSwitchDays",Integer.valueOf(30),
                        "ms","Switch interval for memory-info logs (days)");
         //# of seconds between current-connections log entries (0=none):
  private final CfgPropItem connectsLogIntervalSecsProp =
                      cfgProps.add("connectsLogIntervalSecs",Integer.valueOf(0),
                            "g","Connections info logging interval (secs)");
         //name of optional current-connections log file (instead of stdout):
  private final CfgPropItem connectsLogFileNameProp =
                   cfgProps.add("connectsLogFileName",CONNECTS_LOGGER_FNAME,
                                      "k","Connections info log file name");
         //switch interval for conn log files (days, 0 = no swapping):
  private final CfgPropItem connectsLogSwitchDaysProp =
               cfgProps.add("connectsLogSwitchDays",Integer.valueOf(7),
                         "w","Switch interval for connections logs (days)");

  /**
   * Creates and starts an instance of the OpenORB Notification Service.
   * @param programArgs string array of command-line arguments.
   */
  public RunNotifServer(String [] programArgs)
  {
    String str;
    final Vector vec;
    Object obj;
              //process command-line parameters and config file:
    final boolean cmdLnProcFlag = cfgProps.processCmdLnParams(programArgs,
               true,"configFileName","c",CONFIG_FILENAME,VER1_STR,VER2_STR);
    if((vec=cfgProps.getExtraCmdLnParamsVec()) != null &&
             !vec.isEmpty() && (obj=vec.firstElement()) instanceof String &&
                                                 ((String)obj).length() > 0)
    {    //extra cmd-ln params were gathered and first one is valid String
                        //remove any leading switch characters:
      str = CfgProperties.removeSwitchChars((String)obj);
      if(str.equalsIgnoreCase("help") || str.equalsIgnoreCase("h") ||
                                                        str.startsWith("?"))
      {  //parameter matches one for help screen request; show data
        System.out.println(REVISION_STRING);     //show program info
        System.out.println(getNotifSrcRevStr()); //show notif svc info
        System.out.println("Command-line parameters:");
        System.out.println(cfgProps.getHelpScreenData());
        System.exit(0);           //exit program
      }
      else if(str.equalsIgnoreCase(VER2_STR) ||
                                             str.equalsIgnoreCase(VER1_STR))
      {       //show version information
        System.out.println(REVISION_STRING);     //show program info
        System.out.println(getNotifSrcRevStr()); //show notif svc info
        System.exit(0);      //exit program
      }
      else
      {  //unexpected parameter; show error message:
        System.err.println("Invalid parameter \"" + ((String)obj) + "\"");
        System.exit(1);      //exit program
      }
    }
    if(!cmdLnProcFlag)
    {    //error processing cmd-line params or config file; show error
      System.err.println(cfgProps.getErrorMessage());
      System.exit(1);        //exit program
    }

         //setup CORBA ORB (using OpenORB) for event channel supplier:
              //start with empty properties object:
    final Properties propsObj = new Properties();
              //setup messaging initializer (all in key, value ignored);
              // needed for 'setRoundtripTimeoutPolicy()' method:
    propsObj.setProperty("org.openorb.PI.FeatureInitializerClass."
                     + "org.openorb.orb.messaging.MessagingInitializer","");

         //set queues and threads policies to maximum performance:
    propsObj.setProperty("notify.eventQueuesPolicy","MaxPerformance");
    propsObj.setProperty("notify.suppliersThreadPolicy","MaxPerformance");
    propsObj.setProperty("notify.supplierAdminsThreadPolicy","MaxPerformance");
    propsObj.setProperty("notify.consumersThreadPolicy","MaxPerformance");
    propsObj.setProperty("notify.consumerAdminsThreadPolicy","MaxPerformance");

               //if given then enter port number for notification server:
    if(serverPortNumberProp.intValue() > 0)
      propsObj.setProperty("iiop.port",serverPortNumberProp.stringValue());
              //if given then enter host address of notification server:
    if(serverHostAddressProp.stringValue().trim().length() > 0)
    {
      propsObj.setProperty("iiop.hostname",
                                       serverHostAddressProp.stringValue());
    }
              //configure use of numeric IPs instead of host names:
    propsObj.setProperty("iiop.publishIP",
                                    publishNumericIPFlagProp.stringValue());
              //enter persistent storage directory name:
    propsObj.setProperty("pss.File.DataStore.Directory",
                                    pssDataDirectoryNameProp.stringValue());
              //enter persistent storage data-store name:
    propsObj.setProperty("pss.File.DataStore.Name",
                                        pssDataStoreNameProp.stringValue());
              //enter maximum number for consumers allowed:
    propsObj.setProperty("notify.maxConsumers",
                                            maxConsumersProp.stringValue());
              //enter maximum number of suppliers allowed:
    propsObj.setProperty("notify.maxSuppliers",
                                            maxSuppliersProp.stringValue());
              //enter maximum number of events in channel:
    propsObj.setProperty("notify.maxQueueLength",
                                          maxQueueLengthProp.stringValue());
              //enter resources timeout (seconds):
    propsObj.setProperty("notify.resourcesTimeout",
                                        resourcesTimeoutProp.stringValue());
              //enter BiDirectional IIOP for event channel flag:
    propsObj.setProperty("notify.biDirectionalFlag",
                                       biDirectionalFlagProp.stringValue());
              //enter maximum number of threads allowed in thread pool:
    propsObj.setProperty("openorb.server.maxThreadPoolSize",
                                       maxThreadPoolSizeProp.stringValue());
              //enter connection reap delay value (convert seconds to ms):
    propsObj.setProperty("openorb.server.reapCloseDelay",
              Long.toString((long)(reapCloseDelaySecProp.intValue())*1000));
              //enter log priority level property:
    propsObj.setProperty("notify.logPriorityLevel",
                                        logPriorityLevelProp.stringValue());
              //enter queue-logging flag property:
    propsObj.setProperty("notify.logEventQueue",
                                       logEventQueueFlagProp.stringValue());

    final int connectsLogIntervalSecs =
                                     connectsLogIntervalSecsProp.intValue();
    final int statusLogIntervalSecs = statusLogIntervalSecsProp.intValue();
    if(statusLogIntervalSecs > 0 || connectsLogIntervalSecs > 0)
    {    //status logger or connections logger enabled
                        //setup custom server-manager class:
      propsObj.setProperty("openorb.server.serverManagerClass",
                                         IstiServerManager.class.getName());
    }

         //setup console output redirect for file (if filename given):
                                  //get maximum age for log files:
    final int logFilesMaxAgeInDays = logFilesMaxAgeInDaysProp.intValue();
                                  //get console output file name:
    final String consoleFileName = consoleRedirectFileNameProp.stringValue();
    if(consoleFileName != null && consoleFileName.length() > 0)
    {    //console output redirect filename was given
      try
      {       //create LogFile object to receive console-redirect output:
        final LogFile logFileObj;
        if(logFilesMaxAgeInDays > 0)
        {     //log file max-age value given
          logFileObj = new LogFile(    //setup to use date-code in name
               consoleFileName,LogFile.ALL_MSGS,LogFile.NO_MSGS,false,true);
              //setup log file max-age value:
          logFileObj.setMaxLogFileAge(logFilesMaxAgeInDays);
        }
        else
        {     //log file max-age value not given
          logFileObj = new LogFile(consoleFileName,
                                    LogFile.ALL_MSGS,LogFile.NO_MSGS,false);
        }
              //create output stream that writes to the log file:
        consoleRedirectStream = new PrintStream(
                 new BufferedOutputStream(logFileObj.getLogOutputStream()));
      }
      catch(Exception ex)
      {       //error opening file; show message
        System.err.println("Error opening console redirect " +
            "output file (\"" + consoleFileName + "\") for output:  " + ex);
        consoleRedirectStream = null;       //make sure handle is null
      }
      if(consoleRedirectStream != null)
      {       //output file opened OK; put in header message:
        consoleRedirectStream.println("Console output redirect file for " +
                                                  PROGRAM_NAME + " opened");
        if(!consoleRedirectStream.checkError())       //check for errors
        {     //no stream errors detected
          try
          {        //attempt to redirect console output streams to file:
            System.setOut(consoleRedirectStream);
            System.setErr(consoleRedirectStream);
          }
          catch(Exception ex)
          {        //error redirecting output streams; show message
            System.err.println("Error redirecting console " +
                    "output to file (\"" + consoleFileName + "\"):  " + ex);
            consoleRedirectStream.close();       //close output stream
            consoleRedirectStream = null;        //make sure handle is null
          }
        }
        else
        {     //error writing to file; show message
          System.err.println("Error writing to redirected " +
                       "console output file (\"" + consoleFileName + "\")");
          consoleRedirectStream.close();      //close output stream
          consoleRedirectStream = null;       //clear handle
        }
      }
    }
    System.out.println(REVISION_STRING);         //show program info
    System.out.println(getNotifSrcRevStr());     //show NotifSvc info

    try       //test if persistent storage directory exists and
    {         // create it if it does not:
      final File fileObj = new File(pssDataDirectoryNameProp.stringValue());
      if(!fileObj.exists())
      {  //persistent storage directory does not exist
        if(!fileObj.mkdirs())          //create directory
        {     //create failed; show error message
          System.err.println(
                        "Unable to create persistent storage directory \"" +
                             pssDataDirectoryNameProp.stringValue() + "\"");
        }
      }
    }
    catch(Exception ex)
    {         //exception error creating directory; show error message
      System.err.println("Error creating persistent storage directory \"" +
                     pssDataDirectoryNameProp.stringValue() + "\":  " + ex);
    }

    if(threadLogIntervalMSecsProp.intValue() > 0)
    {    //non-zero thread logging interval given; create and start logger
      try
      {
        if(logFilesMaxAgeInDays > 0)
        {     //max-age value was given; enter it
          threadLoggerObj = new ThreadLogger(THREAD_LOGGER_FNAME,
                threadLogIntervalMSecsProp.intValue(),logFilesMaxAgeInDays);
        }
        else
        {     //no max-age value
          threadLoggerObj = new ThreadLogger(THREAD_LOGGER_FNAME,
                                     threadLogIntervalMSecsProp.intValue());
        }
      }
      catch(Exception ex)
      {  //some kind of error; show message
        System.err.println("Error launching thread logger" + ex);
        ex.printStackTrace();
      }
    }
    if(memoryLogIntervalSecsProp.intValue() > 0)
    {    //non-zero memory-info logging interval given; create & start logger
      try
      {
        memoryLoggerObj = new MemoryInfoLogger(MEMINFO_LOGGER_FNAME,
                                       memoryLogIntervalSecsProp.intValue(),
                                       memoryLogMaxAgeInDaysProp.intValue(),
                                        memoryLogSwitchDaysProp.intValue());
                   //setup for no log-level indicator in log file:
        memoryLoggerObj.setLogLevelVal(LogFile.NO_LEVEL);
      }
      catch(Exception ex)
      {  //some kind of error; show message
        System.err.println("Error launching memory-info logger" + ex);
        ex.printStackTrace();
      }
    }

    try
    {         //create Notification Server object:
      org.openorb.notify.Server serverObj = new org.openorb.notify.Server();

              //add property to import Notification Service module:
      propsObj.setProperty("ImportModule.notify",
                                "${openorb.home}config/notify.xml#notify" );

              //add property to import Corbaloc Service module:
//      propsObj.setProperty("ImportModule.CorbalocService",
//                       "${openorb.home}config/default.xml#CorbalocService");

//      System.out.println("Created server object");

              //setup service parameters:
      final Parameters paramsObj = new Parameters();
                   //setup whether to bind to Naming Service:
      paramsObj.setParameter(org.openorb.notify.Service.OPT_BIND_NS,
                                   bindToNameServiceFlagProp.stringValue());
                   //setup whether to display service IOR string:
      paramsObj.setParameter(org.openorb.notify.Service.OPT_PRINT_IOR,
                                  displayServiceIORFlagProp.stringValue());
                   //setup whether to display Corbaloc URL string:
      paramsObj.setParameter(org.openorb.notify.Service.OPT_PRINT_URL,
                                  displayCorbalocURLFlagProp.stringValue());
                   //setup console debug output level:
      paramsObj.setParameter(org.openorb.notify.Service.OPT_DEBUG,
                                       logPriorityLevelProp.stringValue());

//      System.out.println("Initializing server object");

//      System.out.println("propsObj:  " +
//                                 UtilFns.enumToListString(propsObj.keys()));

      serverObj.init(new String[0],null,propsObj,paramsObj);

//      System.out.println("Initialized server object");

      notifServerOrbObj = serverObj.getORB();    //get CORBA ORB object

              //set roundtrip timeout to 10 seconds:
      setRoundtripTimeoutPolicy(notifServerOrbObj,10);

              //create event channel for admin msgs, using separate thread:
//      createAdminChannelViaThread();

      if(statusLogIntervalSecs > 0)
      {  //status logger enabled; start logging thread
        (statusLoggerObj = new StatusInfoLogger(statusLogIntervalSecs,
                     STATUS_LOGGER_FNAME,statusLogSwitchDaysProp.intValue(),
                             statusLogMaxAgeInDaysProp.intValue())).start();
      }

      if(connectsLogIntervalSecs > 0)
      {  //connections logger enabled; start logging thread
        (connectsLoggerObj = new ConnectsInfoLogger(connectsLogIntervalSecs,
                                      connectsLogFileNameProp.stringValue(),
                                       connectsLogSwitchDaysProp.intValue(),
                                             logFilesMaxAgeInDays)).start();
      }

//      System.out.println("About to run implementation");

      serverObj.run();       //run implementation (blocks until exit)

//      System.out.println("Returned from 'Server.run()' method");
      if(connectsLoggerObj != null)    //if connections logger started then
        connectsLoggerObj.terminate(); //terminate connections-logger thread
      if(statusLoggerObj != null)      //if status logger started then
        statusLoggerObj.terminate();   //terminate status-logger thread
      if(memoryLoggerObj != null)      //if memory logger started then
        memoryLoggerObj.terminate();   //terminate memory-logger thread
      if(threadLoggerObj != null)      //if thread logger started then
        threadLoggerObj.terminate();   //terminate thread-logger thread
      if(consoleRedirectStream != null)     //if console redirect open then
        consoleRedirectStream.close();      //close console-redirect stream
    }
    catch(Exception ex)
    {
      ex.printStackTrace();
      System.exit(1);
    }
  }

  /**
   * Returns an array of strings describing the active server channels.
   * If less than the amount of time specified by 'intervalSecVal' has
   * elapsed since the previous call then the array from the previous
   * call is returned.  This reduces the amount of fetching from ther
   * server manager when multiple threads are using this method.
   * @param intervalSecsVal expected number of seconds between fetches.
   * @return An array of strings describing the active server channels.
   */
  public String [] getServerChannelsInfo(int intervalSecsVal)
  {
    synchronized(serverChannelsInfoSyncObj)
    {    //only allow one thread at a time
      if(istiServerManagerObj == null)
      {    //server-manager object not yet fetched
        if(notifServerOrbObj instanceof org.openorb.orb.core.ORB)
        {  //OpenORB version of ORB class is in use
          final Object obj;
          if((obj=((org.openorb.orb.core.ORB)notifServerOrbObj).getFeature(
                            "ServerCPCManager")) instanceof IstiServerManager)
          {     //fetch 'ServerManager' object in use
            istiServerManagerObj = (IstiServerManager)obj;
          }
        }
      }
      final long currentTimeVal = System.currentTimeMillis();
      if(serverChannelsInfoCacheArr != null &&
              currentTimeVal-serverChInfoArrTime+100 < intervalSecsVal*1000)
      {  //channels info cached and time elapsed since previous
         // call (+100ms) is smaller than given interval
        return serverChannelsInfoCacheArr;
      }
      serverChInfoArrTime = currentTimeVal;      //save time of fetch

           //fetch array of channels-info strings from server manager:
      return serverChannelsInfoCacheArr =   //(save cache version of array)
                                     istiServerManagerObj.getChannelsInfo();
    }
  }

  /**
   * Creates an event channel for administrative command messages, using
   * a separate thread to allow the Notification Service ORB to get running.
   */
/*
  private void createAdminChannelViaThread()
  {
    new Thread()
        {
          public void run()
          {
            int c = 0;
            while(true)
            { //loop while trying to create admin channel
              try { sleep(1); }   //do quick sleep to help let service start
              catch(InterruptedException ex) {}
              final String serverHostAddrStr =   //get host addr for service
                                          serverHostAddressProp.stringValue();
                        //create event ch manager with log output to console:
              final EvtChManager evtChManagerObj = new EvtChManager(
                                     new OrbManager(null,serverHostAddrStr,0),
                          new LogFile(null,LogFile.NO_MSGS,LogFile.ALL_MSGS));
                        //create admin channel:
              if(evtChManagerObj.createChViaNotifService(serverHostAddrStr,
                                          serverPortNumberProp.stringValue()))
              {    //creation of admin channel successful
                break;       //exit loop (and thread)
              }
                        //error creating admin channel; show error message:
              System.err.println("Error creating admin event channel:  " +
                                          evtChManagerObj.getErrorMessage());
              if(++c >= 3)
              {    //too many attempts; show error message
                System.err.println("Unable to create admin event channel");
                break;       //exit loop (and thread)
              }
            }
          }
        }.start();      //run the thread
  }
*/

  /**
   * Returns the revision string for the OpenORB Notification Service.
   * @return The revision string for the OpenORB Notification Service.
   */
  public static String getNotifSrcRevStr()
  {
    return "OpenORB Notification Service v" + OpenOrbVersion.VERSION_STR;
  }

  /**
   * Program entry point.
   * @param programArgs string array of command-line arguments.
   */
  public static void main(String [] programArgs)
  {
    new RunNotifServer(programArgs);
  }

  /**
   * Sets the 'RelativeRoundtripTimeoutPolicy' value for the given ORB.
   * Note that for this to work the 'messaging.MessagingInitializer'
   * class must be entered as an OpenORB "FeatureInitializer".
   * @param orbObj the ORB object to use.
   * @param timeoutSecs the timeout values to use, in seconds.
   * @throws org.omg.CORBA.ORBPackage.InvalidName if the ORBPolicyManager
   * cannot be found.
   * @throws org.omg.CORBA.PolicyError if a policy error occurs.
   * @throws org.omg.CORBA.InvalidPolicies if the policy is invalid.
   */
  public static void setRoundtripTimeoutPolicy(org.omg.CORBA.ORB orbObj,
                                                            int timeoutSecs)
                                throws org.omg.CORBA.ORBPackage.InvalidName,
                     org.omg.CORBA.PolicyError,org.omg.CORBA.InvalidPolicies
  {
              //get policy manager for ORB:
    final org.omg.CORBA.PolicyManager policyMgrObj =
                                               (org.omg.CORBA.PolicyManager)
                      orbObj.resolve_initial_references("ORBPolicyManager");
              //create 'Any' object:
    final org.omg.CORBA.Any anyObj = orbObj.create_any();
              //convert time from 1ms to 100ns and insert into 'Any' object:
    org.omg.TimeBase.TimeTHelper.insert(anyObj,  //convert from sec to 100ns
                                            ((long)timeoutSecs) * 10000000);
              //create policy object and put into array:
    final org.omg.CORBA.Policy [] policyArr = new org.omg.CORBA.Policy[]
               {  orbObj.create_policy(
                    org.omg.Messaging.RELATIVE_RT_TIMEOUT_POLICY_TYPE.value,
                                                                  anyObj) };
              //enter new policy:
    policyMgrObj.set_policy_overrides(policyArr,
                                org.omg.CORBA.SetOverrideType.ADD_OVERRIDE);
  }


  /**
   * Class StatusInfoLogger creates a thread that logs status messages.
   */
  private class StatusInfoLogger extends IstiNotifyThread
  {
    private final int statusLogIntervalSecs;
    private final String statusLogFNameStr;
    private final int statusLogSwitchDays;
    private final int statusLogMaxAgeInDays;
    private final String INDENT_STR = "  ";

    /**
     * Creates a thread that logs status messages.
     * @param statusLogIntervalSecs interval between status messages.
     * @param statusLogFNameStr name of log file to write to (if none
     * given then 'stdout' is used).
     * @param statusLogSwitchDays minimum number of days between
     * log file switches via date changes.
     * @param statusLogMaxAgeInDays maximum status log file age, in days.
     */
    public StatusInfoLogger(int statusLogIntervalSecs,
                          String statusLogFNameStr, int statusLogSwitchDays,
                                                  int statusLogMaxAgeInDays)
    {
      super("StatusInfoLogger");
      this.statusLogIntervalSecs = statusLogIntervalSecs;
      this.statusLogFNameStr = statusLogFNameStr;
      this.statusLogSwitchDays = statusLogSwitchDays;
      this.statusLogMaxAgeInDays = statusLogMaxAgeInDays;
      setDaemon(true);       //make this a "daemon" (background) thread
    }

    /**
     * Executing method for logger thread.
     */
    public void run()
    {
      LogFile statLogObj = null;
      try
      {       //if filename given then setup log file:
        if(statusLogFNameStr != null && statusLogFNameStr.length() > 0)
        {     //filename was given
          if(statusLogSwitchDays > 0 || statusLogMaxAgeInDays > 0)
          {   //log file switch interval or max-age value given
                                  //setup to use date-code in name:
            statLogObj = new LogFile(statusLogFNameStr,LogFile.ALL_MSGS,
                                                LogFile.NO_MSGS,false,true);
                   //setup log file switch interval and max-age values:
            statLogObj.setLogFileSwitchIntervalDays(statusLogSwitchDays);
            statLogObj.setMaxLogFileAge(statusLogMaxAgeInDays);
          }
          else
          {   //log file switch interval and max-age value not given
            statLogObj = new LogFile(statusLogFNameStr,
                                    LogFile.ALL_MSGS,LogFile.NO_MSGS,false);
          }
        }
        else    //filename not given
          statLogObj = null;        //indicate no log file (use stdout)

        String outStr, clientCountStr;
        while(!isTerminated())
        {     //loop while thread not terminating; delay between checks
          if(!waitForNotify(statusLogIntervalSecs*1000))
            break;           //if interrupted then exit loop (and thread)
          try
          {   //get # of client-channel connections; convert to string:
            clientCountStr = Integer.toString(
                       getServerChannelsInfo(statusLogIntervalSecs).length);
          }
          catch(Exception ex)
          {   //some kind of exception error; set indicator
            clientCountStr = "<error>";
          }
              //build status output string:
          outStr = STATUSLOG_HEADER_STR +
                            UtilFns.newline + INDENT_STR + "NotifServer v" +
               PROGRAM_VERSION + ", OpenORB " + OpenOrbVersion.VERSION_STR +
                    UtilFns.newline + INDENT_STR + STATUSLOG_CONNCOUNT_STR +
                         clientCountStr + ", " + STATUSLOG_THREADCOUNT_STR +
                                             UtilFns.getTotalThreadCount() +
                       UtilFns.newline + INDENT_STR + STATUSLOG_MEMORY_STR +
                              UtilFns.getMemoryInfoStr() + UtilFns.newline +
                     "----------------------------------------------------";
          if(statLogObj != null)          //if log file setup then
            statLogObj.println(outStr);   //send output to log file
          else                            //if no log file then
            System.out.println(outStr);   //send output to stdout
        }
      }
      catch(Exception ex)
      {  //some kind of exception error; log it
        final String errStr = "StatusInfoLogger error:  " + ex;
        if(statLogObj != null)
        {     //log file setup
          statLogObj.warning(errStr);       //send output to log file
          statLogObj.warning(UtilFns.getStackTraceString(ex));
        }
        else
        {     //no log file setup
          System.err.println(errStr);       //send output to stderr
          ex.printStackTrace();
        }
      }
      if(statLogObj != null)      //if log file setup then
        statLogObj.close();       //close it
    }
  }


  /**
   * Class ConnectsInfoLogger creates a thread that logs the current
   * connections into the Notification Server.
   */
  private class ConnectsInfoLogger extends IstiNotifyThread
  {
    private final int checkIntervalSecs;
    private final String connLogFNameStr;
    private final int logFileSwitchIntervalDays;
    private final int maxLogFileAgeDays;

    /**
     * Creates a thread that logs the current connections into the
     * Notification Server.
     * @param checkIntervalSecs interval between checking for
     * changes in the connections.
     * @param connLogFNameStr name of log file to write to (if none
     * given then 'stdout' is used).
     * @param logFileSwitchIntervalDays minimum number of days between
     * log file switches via date changes.
     * @param maxLogFileAgeDays maximum log file age, in days.
     */
    public ConnectsInfoLogger(int checkIntervalSecs, String connLogFNameStr,
                       int logFileSwitchIntervalDays, int maxLogFileAgeDays)
    {
      super("ConnectsInfoLogger");
      this.checkIntervalSecs = checkIntervalSecs;
      this.connLogFNameStr = connLogFNameStr;
      this.logFileSwitchIntervalDays = logFileSwitchIntervalDays;
      this.maxLogFileAgeDays = maxLogFileAgeDays;
      setDaemon(true);       //make this a "daemon" (background) thread
    }

    /**
     * Executing method for logger thread.
     */
    public void run()
    {
      LogFile connLogObj = null;
      try
      {       //if filename given then setup log file:
        if(connLogFNameStr != null && connLogFNameStr.length() > 0)
        {     //filename was given
          if(logFileSwitchIntervalDays > 0 || maxLogFileAgeDays > 0)
          {   //log file switch interval or max-age value given
            connLogObj = new LogFile(    //setup to use date-code in name
               connLogFNameStr,LogFile.ALL_MSGS,LogFile.NO_MSGS,false,true);
                   //setup log file switch interval and max-age values:
            connLogObj.setLogFileSwitchIntervalDays(
                                                 logFileSwitchIntervalDays);
            connLogObj.setMaxLogFileAge(maxLogFileAgeDays);
          }
          else
          {   //log file switch interval and max-age value not given
            connLogObj = new LogFile(connLogFNameStr,
                                    LogFile.ALL_MSGS,LogFile.NO_MSGS,false);
          }
        }
        else    //filename not given
          connLogObj = null;        //indicate no log file (use stdout)

        String [] descStrArr;
        String outStr, dispStr, lastDispStr = "   ";
        int i;
        final StringBuffer buff = new StringBuffer();
        while(!isTerminated())
        {     //loop while thread not terminating; delay between checks
          if(!waitForNotify(checkIntervalSecs*1000))
            break;           //if interrupted then exit loop (and thread)
                        //get fetch array of channel-info strings:
          descStrArr = getServerChannelsInfo(checkIntervalSecs);
          buff.delete(0,buff.length());       //clear buffer
              //add each info string to buffer, with separators:
          for(i=0; i<descStrArr.length; ++i)
            buff.append(UtilFns.newline + "  " + descStrArr[i]);
          dispStr = buff.toString();        //convert buffer to string
          if(!dispStr.equals(lastDispStr))
          {     //new display string is different from previous
            lastDispStr = dispStr;     //save display string
                                       //build connection info string:
            outStr = "Current active connections" + ((dispStr.length() > 0) ?
                       (" ("+descStrArr.length+"):"+dispStr) : ":  <none>");
            if(connLogObj != null)          //if log file setup then
              connLogObj.println(outStr);   //send output to log file
            else                            //if no log file then
              System.out.println(outStr);   //send output to stdout
          }
        }
      }
      catch(Exception ex)
      {  //some kind of exception error; log it
        final String errStr = "ConnectsInfoLogger error:  " + ex;
        if(connLogObj != null)
        {     //log file setup
          connLogObj.warning(errStr);       //send output to log file
          connLogObj.warning(UtilFns.getStackTraceString(ex));
        }
        else
        {     //no log file setup
          System.err.println(errStr);       //send output to stderr
          ex.printStackTrace();
        }
      }
      if(connLogObj != null)      //if log file setup then
        connLogObj.close();       //close it
    }
  }
}
