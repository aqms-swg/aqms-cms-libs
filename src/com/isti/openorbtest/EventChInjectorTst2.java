//EventChInjectorTst2.java:  Test program for injecting messages into a
//                           Notification Service event channel.  Modified
//                           version for testing 2 events in a row.
//
//  2/10/2003 -- [ET]  Initial version.
// 12/22/2004 -- [ET]  Modified to set 'evtChManagerObj' to null after
//                     abort.
//

package com.isti.openorbtest;

import java.io.IOException;
import com.isti.util.UtilFns;
import com.isti.util.FileUtils;
import com.isti.util.LogFile;
import com.isti.openorbutil.*;

/**
 * Class EventChInjectorTst2 implements a test program for injecting messages
 * into a Notification Service event channel.  The first (optional)
 * command-line parameter may be the event channel name
 * (default="MainMessageService").  Version for testing 2 events in a row.
 */
public class EventChInjectorTst2
{
                             //location and name of ORB configuration file:
//  private final static String ORB_FILE_STR = "conf/orb.config";
                             //event channel manager object:
  private final EvtChManager evtChManagerObj;
    /** Filename extension string for event channel locator files. */
  public static final String LOC_EXT_STR = ".loc";
         //setup dummy reference to CORBA classes needed to create
         // a stand-alone application 'jar' file using JBuilder:
  private static final com.isti.openorbutil.OrbUtilDeps dummyDepObj = null;

  /**
   * Class EventChInjectorTst2 implements the test program.
   * @param programArgs string array of command-line parameters.
   */
  public EventChInjectorTst2(String[] programArgs)
  {
    System.out.println("Event Channel Injector Program");

    final String evtChannelNameStr = (programArgs.length > 0) ?
                                      programArgs[0] : "MainMessageService";

         //create ORB manager object:
    final OrbManager orbManagerObj = new OrbManager(programArgs);
         //initialize ORB:
    if(!orbManagerObj.initImplementation())
    {    //error initializing; show error message
      System.err.println(orbManagerObj.getErrorMessage());
      System.exit(1);
      evtChManagerObj = null;
      return;           //abort program
    }

         //create "log file" object that displays messages to console:
    final LogFile logObj = new LogFile(
                                    null,LogFile.NO_MSGS,LogFile.ALL_MSGS);
         //create event channel manager:
    evtChManagerObj = new EvtChManager(orbManagerObj,logObj);

         //attempt to open existing event channel via locator file
         // and connect supplier object:
    final boolean openFlag;
    if(!(openFlag=evtChManagerObj.openViaLocatorFile(
      evtChannelNameStr+LOC_EXT_STR)) || !evtChManagerObj.connectSupplier())
    {    //unable to join existing event channel
      if(openFlag)                          //if open succeeded then
        evtChManagerObj.closeChannel();     //close connection to bad channel
      evtChManagerObj.clearErrorMessage();  //clear connect-attempt error
         //create event channel:
      if(!evtChManagerObj.createChViaNotifService(/*evtChannelNameStr*/))
      {       //error creating event channel:
        System.err.println("\"" + evtChannelNameStr + "\":  " +
                                         evtChManagerObj.getErrorMessage());
        System.exit(2);
        return;
      }
      final String locatorStr;
              //write event channel locator string to file:
      if((locatorStr=evtChManagerObj.getLocatorString()) != null)
      {       //locator string OK
        try
        {
          FileUtils.writeStringToFile(evtChannelNameStr+LOC_EXT_STR,
                                                                locatorStr);
        }
        catch(IOException ex)
        {
          System.err.println("Error writing event channel locator file:  " +
                                                                        ex);
        }
      }
      else
      {       //locator string not available
        System.err.println("Unable to write event channel locator file; " +
                                            "locator string not available");
      }
         //connect supplier to event channel:
      if(!evtChManagerObj.connectSupplier())
      {       //error connecting supplier
        System.err.println("\"" + evtChannelNameStr + "\":  " +
                                         evtChManagerObj.getErrorMessage());
        System.exit(3);
        return;
      }
    }
//    System.out.println("Channel:  " + evtChManagerObj.getEventChannelObj());
//    System.out.println("Channel_admin:");
//    final org.omg.CosNotification.Property [] propsArr =
//                           evtChManagerObj.getEventChannelObj().get_admin();
//    for(int i=0; i<propsArr.length; ++i)
//    {
//      System.out.println("  " + propsArr[i].name + " = " + propsArr[i].value);
//    }

      //create new thread to process user input:
    (new Thread()
        {
          public void run()
          {
            System.out.println("Connected to event channel; enter " +
                               "messages to be injected (or 'Q' to quit):");
            String str;
            while(true)
            {
              if((str=UtilFns.getUserConsoleString()) != null)
              {   //user string fetched from console OK
                if(str.equalsIgnoreCase("Q"))
                  break;         //if keyword then exit loop
                try
                {          //send message to channel:
                  if(!str.equalsIgnoreCase("double"))
                    evtChManagerObj.sendMessage(str);
                  else
                  {     //if "double" then send it twice:
                    final String junkStr = "  asedifn,;aeu anbseulnbvwscanas;lcejnasl efanlcmesadnafupomseuncioamngduaonsp;p;mnegoumns;vouea.mn;gvaouemn;ugvan;ugvamen;gvauren,;gvaisremnhagv;ursemnhgv;arsngv;rsngv;ilurn";
                    (new Thread()
                        {
                          public void run()
                          {
                            evtChManagerObj.sendMessage("double1" + junkStr);
                          }
                        }).start();
                    (new Thread()
                        {
                          public void run()
                          {
                            evtChManagerObj.sendMessage("double2" + junkStr);
                          }
                        }).start();
                  }
                }
                catch(Exception ex)
                {          //exception error; show error message
                  System.err.println(
                          "Error sending message to event channel:  " + ex);
                }
              }
              else
              {   //error fetching user string from console
                try         //delay a bit before trying again:
                { Thread.sleep(3000); }
                catch(InterruptedException ex) {}
              }
            }
                   //disconnect from event channel:
            evtChManagerObj.disconnectSupplier();
            evtChManagerObj.closeChannel();           //close event channel
                   //close implementation; exit program
            orbManagerObj.closeImplementation(false);
          }
        }).start();

    orbManagerObj.runImplementation();      //run implementation
    System.out.println("Implementation has shutdown");
  }

  /**
   * Program entry point.
   * @param args string array of command-line parameters.
   */
  public static void main(String[] args)
  {
    new EventChInjectorTst2(args);
  }
}
