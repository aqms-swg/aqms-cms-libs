//EventChMonitor.java:  Notification Service event channel client test
//                      program that receives messages and echos them
//                      as raw text to the console.
//
//  2/10/2003 -- [ET]  Initial version.
// 12/15/2003 -- [ET]  Added optional support for stuctured events and
//                     event filtering.
//   1/9/2004 -- [ET]  Added optional "-l" parameter for messages-received
//                     log file generation.
// 12/22/2004 -- [ET]  Modified to set 'evtChManagerObj' to null after
//                     abort.
//

package com.isti.openorbtest;

import com.isti.util.UtilFns;
import com.isti.util.FileUtils;
import com.isti.util.LogFile;
import com.isti.openorbutil.*;

/**
 * Class EventChMonitor is a Notification Service event channel client test
 * program that receives messages and echos them as raw text to the console.
 * The "-s" parameter enables the use of structured events; otherwise the
 * first (optional) command-line parameter is taken as either a locator
 * string, the client-host IP address to use or as the event channel
 * locator file name.
 */
public class EventChMonitor
{
         //name of file containing location string for CORBA event channel:
  private String evtChLocFileNameStr = "mainMessageService.loc";
         //port number for the client ORB/POA to use:
//  private int orbPortNum = 39985;
  private String hostAddrStr = null;
  private final OrbManager orbManagerObj;
  private final EvtChManager evtChManagerObj;
  private LogFile logObj = null;
         //setup dummy reference to CORBA classes needed to create
         // a stand-alone application 'jar' file using JBuilder:
  private static final com.isti.openorbutil.OrbUtilDeps dummyDepObj = null;

  /**
   * Creates a test client object.
   * @param programArgs string array of command-line parameters.
   */
  public EventChMonitor(String [] programArgs)
  {

    System.out.println("EventChMonitor");
    boolean structuredEventsFlag = false;
    boolean testFilteringFlag = false;
         //if first cmd-line param exists and is not a switch then take
         // it as either a locator string, the client-host IP address to
         // use or as the event channel locator file name:
    String str, evtChLocStr = null;
    if(programArgs != null)
    {
      for(int i=0; i<programArgs.length; ++i)
      {
        if((str=programArgs[i]).startsWith("-"))
        {     //setup for structured events
          if("-s".equalsIgnoreCase(str))
            structuredEventsFlag = true;
          else if("-t".equalsIgnoreCase(str))
          {   //setup for structured events and event-filtering test
            structuredEventsFlag = true;
            testFilteringFlag = true;
          }
          else if("-l".equalsIgnoreCase(str))
          {   //setup log file for received messages
            logObj = new LogFile("EventChMonitor.log",LogFile.ALL_MSGS,
                                               LogFile.ALL_MSGS,false,true);
          }
        }
        else
        {
          if(str.startsWith("corbaloc:") || str.startsWith("iiop://"))
            evtChLocStr = str;             //if locator string then save it
          else if(UtilFns.isIPAddress(str))     //if param is an IP address then
            hostAddrStr = str;                  //take as client host adddress
          else                             //if param not an IP address then
            evtChLocFileNameStr = str;     //take as locator file name
        }
      }
    }
    if(evtChLocStr == null)
    {    //locator string was not given on command line
      System.out.println("Using CORBA-event-channel locator file \"" +
                                                evtChLocFileNameStr + "\"");
         //read CORBA-event-channel IOR string from file:
      try
      {
        evtChLocStr =
            FileUtils.readMultiOpenFileToString(evtChLocFileNameStr).trim();
      }
      catch(Exception ex)
      {            //error opening file; show error message and abort
        System.out.println("Unable to open event channel locator file (\"" +
                                       evtChLocFileNameStr + "\"):  " + ex);
        evtChLocStr = "corbaloc::localhost:39991/DefaultEventChannel";
        System.out.println("Using default locator string");
      }
    }
                   //fixup "iiop://" to "corbaloc::" (if present):
    evtChLocStr = EvtChManager.fixIiopString(evtChLocStr);
    System.out.println("CORBA-event-channel locator string:  " +
                                                               evtChLocStr);
         //create ORB manager object:
    orbManagerObj = new OrbManager(programArgs,hostAddrStr,0);
         //initialize ORB:
    if(!orbManagerObj.initImplementation())
    {    //error initializing; show error message
      System.err.println(orbManagerObj.getErrorMessage());
      evtChManagerObj = null;
      return;           //abort program
    }
    final String hostStr;
    if((hostStr=orbManagerObj.getEnteredHostAddrStr()) != null)
    {    //local host address was entered; show it
      System.out.println("Entered client host IP address:  " + hostStr);
    }
         //show port number used:
//    System.out.println("Entered client port value:  " + orbPortNum);

         //create event channel manager:
         //create "log file" object that displays messages to console:
    final LogFile managerLogObj = new LogFile(
                                    null,LogFile.NO_MSGS,LogFile.ALL_MSGS);
    evtChManagerObj = new EvtChManager(
                                orbManagerObj,managerLogObj,structuredEventsFlag);
    managerLogObj.debug(structuredEventsFlag ? "Using structured events" :
                                                      "Using 'Any' events");
         //open event channel via locator string:
    if(!evtChManagerObj.openViaLocatorString(evtChLocStr))
    {    //error connecting supplier
      System.err.println(
                       "Error finding event channel via locator string:  " +
                                         evtChManagerObj.getErrorMessage());
      return;
    }

    if(testFilteringFlag)
    {    //event-filtering test is enabled
              //setup to ignore all events except ones with domain & type:
//      final String domainStr = "EventChInjector";
      final String domainStr = "";
      final String typeStr = "first";
      evtChManagerObj.setupConsumerFilter(
            new EvtChEventType [] { new EvtChEventType(domainStr,typeStr) });
      managerLogObj.debug("Using event filter, domain=\"" +  domainStr +
                                            "\", type=\"" + typeStr + "\"");
    }

         //connect supplier to event channel:
    if(!evtChManagerObj.connectConsumer(structuredEventsFlag ?
                  (EvtChMsgProcIntrf)(new MonitorStructuredMsgProcessor()) :
                        (EvtChMsgProcIntrf)(new MonitorMessageProcessor())))
    {    //error connecting supplier
      System.err.println("Error connecting event channel consumer:  " +
                                         evtChManagerObj.getErrorMessage());
      return;
    }

    System.out.println("Connected to CORBA-event-channel; starting " +
                                      "implementation (Enter 'Q' to exit)");
         //run input loop in new thread so user can enter 'Q' to exit:
    (new Thread()
        {
          public void run()
          {
            doInputLoop();
          }
        }).start();
    orbManagerObj.runImplementation();      //run implementation
    System.out.println("Implementation has shutdown");
  }

  /** Loops until 'Q' is entered by user, then shuts down connection. */
  private void doInputLoop()
  {
    String str = null;
    while ( true )
    {
      if((str=UtilFns.getUserConsoleString()) != null)
      {  //user string fetched from console OK
        if(str.equalsIgnoreCase("Q"))
        {     //exit command entered by user
          System.out.println("Disconnecting from event channel");
          evtChManagerObj.disconnectConsumer();       //disconnect consumer
          evtChManagerObj.closeChannel();             //close event channel
          orbManagerObj.closeImplementation(false);   //close implementation
          return;
        }
      }
      else
      {   //error fetching user string from console
        try         //delay a bit before trying again:
        { Thread.sleep(3000); }
        catch(InterruptedException ex) {}
      }
    }
  }

  /**
   * Program entry point.
   * @param args string array of command-line parameters.
   */
  public static void main(String [] args)
  {
    new EventChMonitor(args);
    System.exit(0);
  }


  /**
   * Class MonitorMessageProcessor implements a 'processMessage()' method.
   */
  private class MonitorMessageProcessor implements MessageProcessor
  {
    /**
     * Processes a message string.
     * @param msgStr the message string to process.
     */
    public void processMessage(String msgStr)
    {
      final String str = "Received:  " + msgStr;
      System.out.println(str);
      if(logObj != null)
        logObj.println(str);
    }
  }


  /**
   * Class MonitorStructuredMsgProcessor implements a 'processMessage()'
   * method.
   */
  private class MonitorStructuredMsgProcessor
                                           implements StructuredMsgProcessor
  {
    /**
     * Processes a message string.
     * @param domainNameStr the domain name to use, or null for none.
     * @param typeNameStr the type name to use, or null for none.
     * @param eventNameStr the event name to use, or null for none.
     * @param msgStr the message string to process.
     */
    public void processMessage(String domainNameStr,String typeNameStr,
                                          String eventNameStr,String msgStr)
    {
      final String str = "Received:  domain=\"" + domainNameStr +
                               "\", type=\"" + typeNameStr + "\", name=\"" +
                                       eventNameStr + "\", msg:  " + msgStr;
      System.out.println(str);
      if(logObj != null)
        logObj.println(str);
    }
  }
}
