//EvtChCommander.java:  Defines an event channel commander that can
//                      send and receive commands.  Version for OpenORB,
//                      using Notification Service.
//
//  3/25/2003 -- [ET]
//

package com.isti.openorbutil;

import org.omg.CosNotifyChannelAdmin.EventChannelFactory;
import com.isti.util.LogFile;

/**
 * Class EvtChCommander defines an event channel commander that can
 * send and receive commands.  Version for OpenORB, using Notification
 * Service.
 */
public class EvtChCommander
{
  protected final EvtChCmdResponder cmdResponderObj;
  protected final EvtChManager evtChManagerObj;

  /**
   * Creates an event channel commander that can send and receive commands.
   * @param orbManagerObj the ORB manager to use.
   * @param logObj log file object to use, or null for none.
   * @param cmdResponderObj the responder object to use, or null for none.
   */
  public EvtChCommander(OrbManager orbManagerObj,LogFile logObj,
                                          EvtChCmdResponder cmdResponderObj)
  {
//    if(cmdResponderObj == null)
//      throw new NullPointerException("Null parameter(s)");
    this.cmdResponderObj = cmdResponderObj;

    evtChManagerObj = new EvtChManager(orbManagerObj,logObj);

    final EventChannelFactory factoryObj;
    if((factoryObj=evtChManagerObj.getEventChannelFactory(null,null)) ==
                                                                       null)
    {
      System.err.println(evtChManagerObj.getErrorMessage());
      return;
    }
    final int [] chanIdxArr = factoryObj.get_all_channels();
    if(chanIdxArr.length > 0)
    {
      for(int i=0; i<chanIdxArr.length; ++i)
      {
        try
        {
          System.out.println("chIdx=" + chanIdxArr[i] + ", IOR:  " +
                                      orbManagerObj.orbObj.object_to_string(
                              factoryObj.get_event_channel(chanIdxArr[i])));
        }
        catch(Exception ex)
        {
          System.err.println("Error fetching channel info:  " + ex);
        }
      }
    }
    else
      System.err.println("No event channels found");
  }


  public static void main(String [] args)
  {
    new EvtChCommander(new OrbManager(args),
                   new LogFile(null,LogFile.NO_MSGS,LogFile.ALL_MSGS),null);
  }
}
