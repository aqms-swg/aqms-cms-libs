//EvtChConsumer.java:  Defines a generic Notification Service event channel
//                     consumer object that receives objects as they appear
//                     in the event channel.  Version for OpenORB.
//
//  4/23/2003 -- [ET]  Initial verison.
//  1/14/2003 -- [ET]  Added optional support for stuctured events and
//                     event filtering.
//

package com.isti.openorbutil;

import org.omg.CORBA.Any;
import org.omg.CORBA.TCKind;
import org.omg.CORBA.ORBPackage.InvalidName;
import org.omg.CosEventChannelAdmin.TypeError;
import org.omg.CosEventChannelAdmin.AlreadyConnected;
import org.omg.CosNotifyChannelAdmin.EventChannel;
import org.omg.CosNotifyChannelAdmin.ProxyPushSupplier;
import org.omg.CosNotifyChannelAdmin.ConsumerAdmin;
import org.omg.CosNotifyChannelAdmin.ClientType;
import org.omg.CosNotifyChannelAdmin.ProxySupplier;
import org.omg.CosNotifyChannelAdmin.ProxyPushSupplierHelper;
import org.omg.CosNotifyChannelAdmin.StructuredProxyPushSupplier;
import org.omg.CosNotifyChannelAdmin.StructuredProxyPushSupplierHelper;
import org.omg.CosNotifyChannelAdmin.AdminLimitExceeded;
import org.omg.CosNotifyComm.PushConsumerPOA;
import org.omg.CosNotifyComm.StructuredPushConsumerPOA;
import org.omg.CosNotification.EventType;
import org.omg.CosNotification.StructuredEvent;
import org.omg.CosNotification.StructuredEventHelper;
import org.omg.CosNotifyFilter.Filter;
import org.omg.CosNotifyFilter.ConstraintExp;


/**
 * Class EvtChConsumer defines a generic Notification Service event channel
 * consumer object that receives objects as they appear in the event channel.
 * One of the 'push()' methods should be overridden to receive events.
 */
public abstract class EvtChConsumer
{
  protected org.omg.CORBA.ORB orbObj;
  protected EventChannel evtChObj;
  protected ProxyPushSupplier pushSupplierObj;
  protected StructuredProxyPushSupplier structuredPushSupplierObj;
  protected boolean structuredEventsFlag;
  protected static final String CONSTRUCTOR_ERR_STR =
                      "Error obtaining proxy push object for event channel";

  /**
   * Creates a generic Notification Service event channel consumer object.
   * @param orbObj the CORBA ORB object to be used.
   * @param evtChObj event channel object to use.
   * @param structuredEventsFlag true to use structured events, false to
   * use 'Any' events.
   * @param filterObj the Notification Service filter object to use for
   * the filtering of structured events to be received by this consumer,
   * or null for none.
   * @exception EvtChSetupException if the event channel could not be
   * setup.
   * @exception AlreadyConnected if the event channel is already
   * connected.
   * @exception TypeError if an error occurred when connecting to channel.
   * @exception AdminLimitExceeded if the consumer admin could not
   * be referenced.
   */
  public EvtChConsumer(org.omg.CORBA.ORB orbObj,EventChannel evtChObj,
                              boolean structuredEventsFlag,Filter filterObj)
                                throws EvtChSetupException,AlreadyConnected,
                                                TypeError,AdminLimitExceeded

  {
    setupConsumer(orbObj,evtChObj,structuredEventsFlag,filterObj);
  }

  /**
   * Creates a generic Notification Service event channel consumer object,
   * using structured events.
   * @param orbObj the CORBA ORB object to be used.
   * @param evtChObj event channel object to use.
   * @param filterObj the Notification Service filter object to use for
   * the filtering of structured events to be received by this consumer,
   * or null for none.
   * @exception EvtChSetupException if the event channel could not be
   * setup.
   * @exception AlreadyConnected if the event channel is already
   * connected.
   * @exception TypeError if an error occurred when connecting to channel.
   * @exception AdminLimitExceeded if the consumer admin could not
   * be referenced.
   */
  public EvtChConsumer(org.omg.CORBA.ORB orbObj,EventChannel evtChObj,
                                                           Filter filterObj)
                                throws EvtChSetupException,AlreadyConnected,
                                                TypeError,AdminLimitExceeded

  {
    this(orbObj,evtChObj,true,filterObj);
  }

  /**
   * Creates a generic Notification Service event channel consumer object.
   * An array of 'EvtChEventType' objects may be given to setup an event
   * filter, where only events that match one or more of the 'EvtChEventType'
   * objects will be received, and a match consists of both the "domain"
   * and "type" strings being the same as those in the event.  Event
   * filtering is only used with structured events.
   * @param orbObj the CORBA ORB object to be used.
   * @param evtChObj event channel object to use.
   * @param structuredEventsFlag true to use structured events, false to
   * use 'Any' events.
   * @param evtTypeArr the array of 'EvtChEventType' objects to use to
   * build the Notification Service filter object to use for
   * the filtering of structured events to be received by this consumer,
   * or null for none.
   * @exception EvtChSetupException if the event channel could not be
   * setup.
   * @exception AlreadyConnected if the event channel is already
   * connected.
   * @exception TypeError if an error occurred when connecting to channel.
   * @exception AdminLimitExceeded if the consumer admin could not
   * be referenced.
   */
  public EvtChConsumer(org.omg.CORBA.ORB orbObj,EventChannel evtChObj,
                  boolean structuredEventsFlag,EvtChEventType [] evtTypeArr)
                                throws EvtChSetupException,AlreadyConnected,
                                                TypeError,AdminLimitExceeded

  {
         //need to setup 'evtChObj' early for 'buildFilterFromTypes()':
    this.evtChObj = evtChObj;
    setupConsumer(orbObj,evtChObj,structuredEventsFlag,
                                          buildFilterFromTypes(evtTypeArr));
  }

  /**
   * Creates a generic Notification Service event channel consumer object,
   * using structured events.
   * An array of 'EvtChEventType' objects may be given to setup an event
   * filter, where only events that match one or more of the 'EvtChEventType'
   * objects will be received, and a match consists of both the "domain"
   * and "type" strings being the same as those in the event.  Event
   * filtering is only used with structured events.
   * @param orbObj the CORBA ORB object to be used.
   * @param evtChObj event channel object to use.
   * @param evtTypeArr the array of 'EvtChEventType' objects to use to
   * build the Notification Service filter object to use for
   * the filtering of structured events to be received by this consumer,
   * or null for none.
   * @exception EvtChSetupException if the event channel could not be
   * setup.
   * @exception AlreadyConnected if the event channel is already
   * connected.
   * @exception TypeError if an error occurred when connecting to channel.
   * @exception AdminLimitExceeded if the consumer admin could not
   * be referenced.
   */
  public EvtChConsumer(org.omg.CORBA.ORB orbObj,EventChannel evtChObj,
                                               EvtChEventType [] evtTypeArr)
                                throws EvtChSetupException,AlreadyConnected,
                                                TypeError,AdminLimitExceeded

  {
    this(orbObj,evtChObj,true,evtTypeArr);
  }

  /**
   * Creates a generic Notification Service event channel consumer object.
   * @param orbObj the CORBA ORB object to be used.
   * @param evtChObj event channel object to use.
   * @param structuredEventsFlag true to use structured events, false to
   * use 'Any' events.
   * @exception EvtChSetupException if the event channel could not be
   * setup.
   * @exception AlreadyConnected if the event channel is already
   * connected.
   * @exception TypeError if an error occurred when connecting to channel.
   * @exception AdminLimitExceeded if the consumer admin could not
   * be referenced.
   */
  public EvtChConsumer(org.omg.CORBA.ORB orbObj,EventChannel evtChObj,
                                               boolean structuredEventsFlag)
                                throws EvtChSetupException,AlreadyConnected,
                                                TypeError,AdminLimitExceeded

  {
    setupConsumer(orbObj,evtChObj,structuredEventsFlag,null);
  }

  /**
   * Creates a generic Notification Service event channel consumer object,
   * using 'Any' events.
   * @param orbObj the CORBA ORB object to be used.
   * @param evtChObj event channel object to use.
   * @exception EvtChSetupException if the event channel could not be
   * setup.
   * @exception AlreadyConnected if the event channel is already
   * connected.
   * @exception TypeError if an error occurred when connecting to channel.
   * @exception AdminLimitExceeded if the consumer admin could not
   * be referenced.
   */
  public EvtChConsumer(org.omg.CORBA.ORB orbObj,EventChannel evtChObj)
                                 throws EvtChSetupException,AlreadyConnected,
                                                TypeError,AdminLimitExceeded

  {
    setupConsumer(orbObj,evtChObj,false,null);
  }

  /**
   * Creates a generic Notification Service event channel consumer object.
   * @param orbObj the CORBA ORB object to be used.
   * @param serviceName the name bound to the event channel to be used.
   * @param structuredEventsFlag true to use structured events, false to
   * use 'Any' events.
   * @param filterObj the Notification Service filter object to use for
   * the filtering of structured events to be received by this consumer,
   * or null for none.
   * @exception InvalidName if the event channel name could not resolved.
   * @exception EvtChSetupException if the event channel could not be
   * setup.
   * @exception AlreadyConnected if the event channel is already
   * connected.
   * @exception TypeError if an error occurred when connecting to channel.
   * @exception AdminLimitExceeded if the consumer admin could not
   * be referenced.
   */
  public EvtChConsumer(org.omg.CORBA.ORB orbObj,String serviceName,
                              boolean structuredEventsFlag,Filter filterObj)
                    throws InvalidName,EvtChSetupException,AlreadyConnected,
                                                TypeError,AdminLimitExceeded

  {
    this(orbObj,EvtChManager.resolveChannel(orbObj,serviceName),
                                            structuredEventsFlag,filterObj);
  }

  /**
   * Creates a generic Notification Service event channel consumer object,
   * using structured events.
   * @param orbObj the CORBA ORB object to be used.
   * @param serviceName the name bound to the event channel to be used.
   * @param filterObj the Notification Service filter object to use for
   * the filtering of structured events to be received by this consumer,
   * or null for none.
   * @exception InvalidName if the event channel name could not resolved.
   * @exception EvtChSetupException if the event channel could not be
   * setup.
   * @exception AlreadyConnected if the event channel is already
   * connected.
   * @exception TypeError if an error occurred when connecting to channel.
   * @exception AdminLimitExceeded if the consumer admin could not
   * be referenced.
   */
  public EvtChConsumer(org.omg.CORBA.ORB orbObj,String serviceName,
                                                           Filter filterObj)
                    throws InvalidName,EvtChSetupException,AlreadyConnected,
                                                TypeError,AdminLimitExceeded

  {
    this(orbObj,serviceName,true,filterObj);
  }

  /**
   * Creates a generic Notification Service event channel consumer object.
   * An array of 'EvtChEventType' objects may be given to setup an event
   * filter, where only events that match one or more of the 'EvtChEventType'
   * objects will be received, and a match consists of both the "domain"
   * and "type" strings being the same as those in the event.  Event
   * filtering is only used with structured events.
   * @param orbObj the CORBA ORB object to be used.
   * @param serviceName the name bound to the event channel to be used.
   * @param structuredEventsFlag true to use structured events, false to
   * use 'Any' events.
   * @param evtTypeArr the array of 'EvtChEventType' objects to use to
   * build the Notification Service filter object to use for
   * the filtering of structured events to be received by this consumer,
   * or null for none.
   * @exception InvalidName if the event channel name could not resolved.
   * @exception EvtChSetupException if the event channel could not be
   * setup.
   * @exception AlreadyConnected if the event channel is already
   * connected.
   * @exception TypeError if an error occurred when connecting to channel.
   * @exception AdminLimitExceeded if the consumer admin could not
   * be referenced.
   */
  public EvtChConsumer(org.omg.CORBA.ORB orbObj,String serviceName,
                  boolean structuredEventsFlag,EvtChEventType [] evtTypeArr)
                    throws InvalidName,EvtChSetupException,AlreadyConnected,
                                                TypeError,AdminLimitExceeded

  {
    this(orbObj,EvtChManager.resolveChannel(orbObj,serviceName),
                                            structuredEventsFlag,evtTypeArr);
  }

  /**
   * Creates a generic Notification Service event channel consumer object,
   * using structured events.
   * An array of 'EvtChEventType' objects may be given to setup an event
   * filter, where only events that match one or more of the 'EvtChEventType'
   * objects will be received, and a match consists of both the "domain"
   * and "type" strings being the same as those in the event.  Event
   * filtering is only used with structured events.
   * @param orbObj the CORBA ORB object to be used.
   * @param serviceName the name bound to the event channel to be used.
   * @param evtTypeArr the array of 'EvtChEventType' objects to use to
   * build the Notification Service filter object to use for
   * the filtering of structured events to be received by this consumer,
   * or null for none.
   * @exception InvalidName if the event channel name could not resolved.
   * @exception EvtChSetupException if the event channel could not be
   * setup.
   * @exception AlreadyConnected if the event channel is already
   * connected.
   * @exception TypeError if an error occurred when connecting to channel.
   * @exception AdminLimitExceeded if the consumer admin could not
   * be referenced.
   */
  public EvtChConsumer(org.omg.CORBA.ORB orbObj,String serviceName,
                                               EvtChEventType [] evtTypeArr)
                    throws InvalidName,EvtChSetupException,AlreadyConnected,
                                                TypeError,AdminLimitExceeded

  {
    this(orbObj,serviceName,true,evtTypeArr);
  }

  /**
   * Creates a generic Notification Service event channel consumer object.
   * @param orbObj the CORBA ORB object to be used.
   * @param serviceName the name bound to the event channel to be used.
   * @param structuredEventsFlag true to use structured events, false to
   * use 'Any' events.
   * @exception InvalidName if the event channel name could not resolved.
   * @exception EvtChSetupException if the event channel could not be
   * setup.
   * @exception AlreadyConnected if the event channel is already
   * connected.
   * @exception TypeError if an error occurred when connecting to channel.
   * @exception AdminLimitExceeded if the consumer admin could not
   * be referenced.
   */
  public EvtChConsumer(org.omg.CORBA.ORB orbObj,String serviceName,
                                               boolean structuredEventsFlag)
                    throws InvalidName,EvtChSetupException,AlreadyConnected,
                                                TypeError,AdminLimitExceeded

  {
    this(orbObj,serviceName,structuredEventsFlag,(Filter)null);
  }

  /**
   * Creates a generic Notification Service event channel consumer object,
   * using 'Any' events.
   * @param orbObj the CORBA ORB object to be used.
   * @param serviceName the name bound to the event channel to be used.
   * @exception InvalidName if the event channel name could not resolved.
   * @exception EvtChSetupException if the event channel could not be
   * setup.
   * @exception AlreadyConnected if the event channel is already
   * connected.
   * @exception TypeError if an error occurred when connecting to channel.
   * @exception AdminLimitExceeded if the consumer admin could not
   * be referenced.
   */
  public EvtChConsumer(org.omg.CORBA.ORB orbObj,String serviceName)
                    throws InvalidName,EvtChSetupException,AlreadyConnected,
                                                TypeError,AdminLimitExceeded

  {
    this(orbObj,serviceName,false,(Filter)null);
  }

  /**
   * Creates a generic Notification Service event channel consumer object.
   * @param orbObj the CORBA ORB object to be used.
   * @param evtChObj event channel object to use.
   * @param structuredEventsFlag true to use structured events, false to
   * use 'Any' events.
   * @param filterObj the Notification Service filter object to use for
   * the filtering of structured events to be received by this consumer,
   * or null for none.
   * @exception EvtChSetupException if the event channel could not be
   * setup.
   * @exception AlreadyConnected if the event channel is already
   * connected.
   * @exception TypeError if an error occurred when connecting to channel.
   * @exception AdminLimitExceeded if the consumer admin could not
   * be referenced.
   */
  private final void setupConsumer(org.omg.CORBA.ORB orbObj,
        EventChannel evtChObj,boolean structuredEventsFlag,Filter filterObj)
                                throws EvtChSetupException,AlreadyConnected,
                                                TypeError,AdminLimitExceeded

  {
    this.orbObj = orbObj;
    this.evtChObj = evtChObj;
    this.structuredEventsFlag = structuredEventsFlag;
    final ConsumerAdmin consumerAdminObj;
    final ProxySupplier proxySupplierObj;

    if((consumerAdminObj=evtChObj.default_consumer_admin()) == null ||
       (proxySupplierObj=consumerAdminObj.obtain_notification_push_supplier(
                          (structuredEventsFlag?ClientType.STRUCTURED_EVENT:
                                                      ClientType.ANY_EVENT),
                                    new org.omg.CORBA.IntHolder())) == null)
    {    //error getting push consumer object; throw exception
      throw new EvtChSetupException(CONSTRUCTOR_ERR_STR);
    }

    if(structuredEventsFlag)
    {    //using structured events
      if((structuredPushSupplierObj=StructuredProxyPushSupplierHelper.narrow(
                                                 proxySupplierObj)) == null)
      {  //error narrowing push supplier object; throw exception
        throw new EvtChSetupException(CONSTRUCTOR_ERR_STR);
      }
      pushSupplierObj = null;     //clear 'Any' push supplier object
      if(filterObj != null)       //if filter given then add it to proxy
        structuredPushSupplierObj.add_filter(filterObj);
         //connect push supplier object to event channel:
      structuredPushSupplierObj.connect_structured_push_consumer(
                         (new StructuredEventPushCallBack())._this(orbObj));
    }
    else
    {    //using 'Any' events:
      if((pushSupplierObj=ProxyPushSupplierHelper.narrow(
                                                 proxySupplierObj)) == null)
      {  //error narrowing push supplier object; throw exception
        throw new EvtChSetupException(CONSTRUCTOR_ERR_STR);
      }
      structuredPushSupplierObj = null;     //clear structured-event object
         //connect push supplier object to event channel:
      pushSupplierObj.connect_any_push_consumer(
                                     (new AnyPushCallBack())._this(orbObj));
    }
  }

  /**
   * Disconnects this object from the event channel.
   */
  public void disconnectImpl()
  {
    try
    {         //disconnect supplier object:
      if(structuredEventsFlag)
        structuredPushSupplierObj.disconnect_structured_push_supplier();
      else
        pushSupplierObj.disconnect_push_supplier();
    }
    catch(Exception ex)
    {         //ignore any exceptions
    }
  }

  /**
   * This method is called when an object arrives over the event channel.
   * This method, or the "push(String)" method will want to be
   * overridden in a subclass of this class.  If not overridden, this
   * method will attempt to convert the received object to a String
   * and send it to the "push(String)" method.
   * @param anyObj the received object.
   */
  public void push(Any anyObj)
  {
    try
    {                             //if 'Any' object holds a string then
      final TCKind kindObj;       // extract and pass on string:
      if((kindObj=anyObj.type().kind()).equals(TCKind.tk_string))
        push(anyObj.extract_string());
      else if(kindObj.equals(TCKind.tk_struct))
      {  //'Any' object holds a structured event; pass it on
        push(StructuredEventHelper.extract(anyObj));
      }
      else    //unknown content held by 'Any' object
        push(anyObj.toString());       //use 'toString()' value
    }
    catch(Exception ex)
    {    //some kind of exception error; dump stack trace to console
      ex.printStackTrace();
    }
  }

  /**
   * This method is called when a String object arrives over the event
   * channel.  This method, or the "push(Any)" method will want to be
   * overridden in a subclass of this class.
   * @param str the string to be sent.
   */
  public void push(String str)
  {
  }

  /**
   * This method is called when a structured-event object arrives over
   * the event channel.  This method may be overridden to receive the
   * event, or, if not then the event will passed along to the
   * "push(String,String,String,Any)' method.
   * @param structuredEventObj the received structured-event object.
   */
  public void push(StructuredEvent structuredEventObj)
  {
    try
    {
      push(structuredEventObj.header.fixed_header.event_type.domain_name,
                structuredEventObj.header.fixed_header.event_type.type_name,
                          structuredEventObj.header.fixed_header.event_name,
                                      structuredEventObj.remainder_of_body);
    }
    catch(Exception ex)
    {    //some kind of exception error; dump stack trace to console
      ex.printStackTrace();
    }
  }

  /**
   * This method is called when a structured-event object arrives over
   * the event channel.  This method may be overridden to receive the
   * event, or, if not then the event will passed along to the
   * "push(Any)' method.
   * @param domainNameStr the domain name to use, or null for none.
   * @param typeNameStr the type name to use, or null for none.
   * @param eventNameStr the event name to use, or null for none.
   * @param anyObj the 'Any' data object to use.
   */
  public void push(String domainNameStr,String typeNameStr,
                                             String eventNameStr,Any anyObj)
  {
         //if 'Any' object holds a string then extract it;
         // if not then use 'toString()' value:
    final String str = (anyObj.type().kind().equals(TCKind.tk_string)) ?
                                anyObj.extract_string() : anyObj.toString();
    push(domainNameStr,typeNameStr,eventNameStr,str);
  }

  /**
   * This method is called when a structured-event object arrives over
   * the event channel.  This method may be overridden to receive the
   * event, or, if not then this method will attempt to convert the
   * received 'Any' object to a String and send it to the "push(String)"
   * method.
   * @param domainNameStr the domain name to use, or null for none.
   * @param typeNameStr the type name to use, or null for none.
   * @param eventNameStr the event name to use, or null for none.
   * @param str the string to be sent.
   */
  public void push(String domainNameStr,String typeNameStr,
                                             String eventNameStr,String str)
  {
    push(str);
  }

  /**
   * Returns the event channel object used by this object.
   * @return the event channel object used by this object.
   */
  public EventChannel getEvtChObj()
  {
    return evtChObj;
  }

  /**
   * Returns the push-supplier object for this consumer object.
   * @return if structured are in use then a 'StructuredProxyPushSupplier'
   * object; otherwise a 'ProxyPushSupplier' object.
   */
  public ProxySupplier getPushSupplierObj()
  {
    return structuredEventsFlag ? (ProxySupplier)structuredPushSupplierObj :
                                             (ProxySupplier)pushSupplierObj;
  }

  /**
   * Builds a Notification Service filter object from an array of
   * event-type objects.  Event filtering is implemented such that only
   * events that match one or more of the 'EvtChEventType' objects will
   * be received, and a match consists of both the "domain" and "type"
   * strings being the same as those in the event.  Event filtering is
   * only used with structured events.
   * @param evtTypeArr the array of 'EvtChEventType' objects to use.
   * @return A new 'Filter' object, or null if the given parameter is null.
   * @throws EvtChSetupException if an error occurred while building the
   * filter object.
   */
  public final Filter buildFilterFromTypes(EvtChEventType [] evtTypeArr)
                                                  throws EvtChSetupException
  {
    try
    {
      if(evtTypeArr == null)
        return null;
         //copy given 'EvtChEventType' array to an 'EventType' array:
      final EventType eventTypeArr [] = new EventType[evtTypeArr.length];
      for(int i=0; i<evtTypeArr.length; ++i)
      {       //for each 'EvtChEventType' item; copy contents
        eventTypeArr[i] = new EventType(evtTypeArr[i].domainStr,
                                                       evtTypeArr[i].typeStr);
      }
         //create filter object:
      final Filter filterObj = evtChObj.default_filter_factory().create_filter(
                                                              "EXTENDED_TCL");
         //create "contraint" object containing 'EvtChEventType' items,
         // put into a single-element array and add to filter object:
      filterObj.add_constraints(new ConstraintExp []
                                      { new ConstraintExp(eventTypeArr,"") });
      return filterObj;
    }
    catch(org.omg.CORBA.SystemException ex)
    {    //CORBA exception error; convert to 'EvtChSetupException'
      final String errStr;
      if((errStr=ex.getMessage()) != null &&
                                errStr.toLowerCase().indexOf("server") >= 0)
      {  //filtering not supported by event channel; build special err msg
        throw new EvtChSetupException("Message filtering not supported " +
                                         "by server's CORBA-event-channel");
      }
      throw new EvtChSetupException("Error building filter:  " + ex);
    }
    catch(Exception ex)
    {    //some kind of exception error; convert to 'EvtChSetupException'
      throw new EvtChSetupException("Error building filter:  " + ex);
    }
  }


  /**
   * Class AnyPushCallBack receives 'Any' messages from the event channel
   * and passes them back to the parent class.
   */
  protected class AnyPushCallBack extends PushConsumerPOA
  {
    /**
     * This method is called when an object arrives over the event channel.
     * The object is passed back to the parent class.
     * @param anyObj the received object.
     */
    public void push(Any anyObj)
    {
      EvtChConsumer.this.push(anyObj);
    }

    /**
     * Method called when consumer is disconnected from event channel.
     */
    public void disconnect_push_consumer()
    {
      EvtChConsumer.this.disconnectImpl();
    }

    /**
     * Method implemented to satisfy the PushConsumerPOA interface.  No
     * action is taken.
     * @param added event types to be added.
     * @param removed event types to be removed.
     */
    public void offer_change(EventType[] added,EventType[] removed)
    {
    }
  }


  /**
   * Class StructuredEventPushCallBack receives structured-event messages
   * from the event channel and passes them back to the parent class.
   */
  protected class StructuredEventPushCallBack
                                           extends StructuredPushConsumerPOA
  {
    /**
     * This method is called when an object arrives over the event channel.
     * The object is passed back to the parent class.
     * @param evtObj the received object.
     */
    public void push_structured_event(StructuredEvent evtObj)
    {
      EvtChConsumer.this.push(evtObj);
    }

    /**
     * Method called when consumer is disconnected from event channel.
     */
    public void disconnect_structured_push_consumer()
    {
      EvtChConsumer.this.disconnectImpl();
    }

    /**
     * Method implemented to satisfy the PushConsumerPOA interface.  No
     * action is taken.
     * @param added event types to be added.
     * @param removed event types to be removed.
     */
    public void offer_change(EventType[] added,EventType[] removed)
    {
    }
  }
}
