//EvtChEventType.java:  Holds "domain" and "type" strings for an event type.
//
// 12/30/2003 -- [ET]
// 5/24/2004 -- [SBH] added toListString() method, and support method.
// 5/25/2004 -- [SBH] make yesterday's mod java 1.3 compatible
//

package com.isti.openorbutil;
import com.isti.util.UtilFns;

/**
 * Class EvtChEventType holds "domain" and "type" strings for an event type.
 */
public class EvtChEventType
{
  /** Event type "domain" string. */
  public final String domainStr;
  /** Event type "type" string. */
  public final String typeStr;

  /**
   * Creates an event type information block.
   * @param domainStr the "domain" string to use.
   * @param typeStr the "type" string to use.
   */
  public EvtChEventType(String domainStr, String typeStr)
  {
    this.domainStr = domainStr;
    this.typeStr = typeStr;
  }

  /**
   * Determines if the given object is equal to this object.
   * @param obj the 'EvtChEventType' to compare to.
   * @return true if the given object is an 'EvtChEventType' object whose
   * domain and type names are equal to the ones in this object.
   */
  public boolean equals(Object obj)
  {
    if(!(obj instanceof EvtChEventType))
      return false;          //if given object wrong type then return false
    final EvtChEventType evtTypeObj = (EvtChEventType)obj;
    if(domainStr != null)
    {    //domain not null
      if(!domainStr.equals(evtTypeObj.domainStr))
        return false;        //if domains not equal then return false
    }
    else if(evtTypeObj.domainStr != null)
      return false;          //if only one domain == null then return false
    if(typeStr != null)
    {    //type not null
      if(!typeStr.equals(evtTypeObj.typeStr))
        return false;        //if types not equal then return false
    }
    else if(evtTypeObj.typeStr != null)
      return false;          //if only one type == null then return false
    return true;
  }

  /**
   * take a string and if there are any colons ":" or commas ",", insert a backslash "\"
   * in front of these characters.
   * @param tstr
   * @return the escaped string (or the original, if no escapes are necessary.
   */
  private String escapeSeparators(String tstr) {
    return UtilFns.insertQuoteChars(tstr,":,");
  }

  /**
   * creates a string suitable for using to build up the domain/type lists
   * using the format
   * "domain:type,domain:type...".  Occurrences of the ':' and ','
   * characters not meant as separators may be "quoted" by preceding
   * them with the backslash ('\') character.  List items missing the
   * ':' character will be considered to specify only a domain name
   * (the type name will be an empty string).
   * @return the final string element.
   */
  public String toListString() {

    if (domainStr == null)
      return null;

    if (typeStr == null)
      return escapeSeparators(domainStr);

    return escapeSeparators(domainStr) + ":" + escapeSeparators(typeStr);
  }

  /**
   * Returns the hash code value for this object.  The hash code value
   * is computed by adding the hash codes for the domain and type name
   * strings.
   * @return The hash code value for this object.
   */
  public int hashCode()
  {
    return (int)((long)((domainStr != null) ? domainStr.hashCode() : 0) +
                        (long)((typeStr != null) ? typeStr.hashCode() : 0));
  }
}
