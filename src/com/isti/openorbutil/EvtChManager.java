//EvtChManager.java:  Contains methods for managing a CORBA event channel.
//                    Version for OpenORB, using Notification Service.
//
//  4/23/2003 -- [ET]  Initial version.
//  5/22/2003 -- [ET]  Modified 'getLocatorString()' so that it also
//                     returns the locator string for the channel located
//                     using the 'openViaLocator...()' methods.
// 12/15/2003 -- [ET]  Added optional support for stuctured events and
//                     event filtering.
// 12/23/2003 -- [ET]  Modified 'setupConsumerFilter()' to flag error if
//                     filter given and structured events not enabled.
//  2/20/2004 -- [ET]  Added 'getErrorExceptionObj()' method.
//

package com.isti.openorbutil;

import org.omg.CORBA.ORBPackage.InvalidName;
import org.omg.CosNotifyChannelAdmin.EventChannelFactory;
import org.omg.CosNotifyChannelAdmin.EventChannelFactoryHelper;
import org.omg.CosNotifyChannelAdmin.EventChannel;
import org.omg.CosNotifyChannelAdmin.EventChannelHelper;
import com.isti.util.UtilFns;
import com.isti.util.FileUtils;
import com.isti.util.LogFile;


/**
 * Class EvtChManager contains methods for managing a CORBA event channel.
 */
public class EvtChManager
{
  protected final OrbManager orbManagerObj;
  protected final LogFile logObj;
  protected final boolean structuredEventsFlag;
  protected EventChannel eventChannelObj = null;
  protected EvtChSupplier evtChSupplierObj = null;
  protected EvtChConsumer evtChConsumerObj = null;
  protected EvtChEventType [] evtFilterTypeArr = null;
  protected int disChannelIdValue = 0;
  protected boolean disSupplierPendingFlag = false;
  protected int disSupplierIdValue = 0;
  protected boolean disConsumerPendingFlag = false;
  protected int disConsumerIdValue = 0;
  protected String locatorString = null;
  protected String errorMessage = null;     //handle for error message string
  protected Throwable errorExceptionObj = null;

  /**
   * Constructs an event channel manager.
   * @param orbManagerObj the ORB manager to use.
   * @param logObj log file object to use, or null for none.
   * @param structuredEventsFlag true to use structured events, false to
   * use 'Any' events.
   */
  public EvtChManager(OrbManager orbManagerObj,LogFile logObj,
                                               boolean structuredEventsFlag)
  {
    if(orbManagerObj == null)
      throw new NullPointerException("Null parameter");
    this.orbManagerObj = orbManagerObj;
              //setup handle to log file; if null then setup dummy log file
    this.logObj = (logObj != null) ? logObj :
                          new LogFile(null,LogFile.NO_MSGS,LogFile.NO_MSGS);
    this.structuredEventsFlag = structuredEventsFlag;
  }

  /**
   * Constructs an event channel manager (using 'Any' events).
   * @param orbManagerObj the ORB manager to use.
   * @param logObj log file object to use, or null for none.
   */
  public EvtChManager(OrbManager orbManagerObj,LogFile logObj)
  {
    this(orbManagerObj,logObj,false);
  }

  /**
   * Creates an event channel.  The event channel's locator string may be
   * fetched via the 'getLocatorString()' method.
   * @param hostNameStr host name to use, or null to use the IP of the
   * local host.
   * @param servicePortStr port number for Notification Service on host, or
   * null for 2005.
   * @return true if successful, false if error (in which case the
   * 'getErrorMessage()' method may be used to fetch the error message).
   */
  public synchronized boolean createChViaNotifService(String hostNameStr,
                                                      String servicePortStr)
  {
    if(eventChannelObj != null)
    {    //event channel already connected
      setErrorMessage("Event channel already opened");
      return false;
    }
         //get event channel factory object:
    final EventChannelFactory factoryObj;
    if((factoryObj=getEventChannelFactory(hostNameStr,servicePortStr)) ==
                                                                       null)
    {    //error fetching event channel factory object
      return false;     //return error flag
    }
    try       //create event channel; give empty array of properties for
    {         // 'initial_qos' & 'initial_admin' params, and ID holder:
      eventChannelObj = factoryObj.create_channel(
                                    new org.omg.CosNotification.Property[0],
                                    new org.omg.CosNotification.Property[0],
                                             new org.omg.CORBA.IntHolder());
    }
    catch(Exception ex)
    {
      setErrorMessage(("Unable to create event channel:  " + ex),ex);
      return false;
    }
    if(eventChannelObj == null)
    {    //null handle was returned; set error message
      logObj.debug2("EvtChManager:  'create_channel()' returned null");
      setErrorMessage("Unable to open event channel");
      return false;
    }
    logObj.debug2("EvtChManager:  Created event channel");

/*
    if(channelName != null && channelName.length() > 0)
    {
      try
      {         //enter event channel into corbaloc service:
        final CorbalocService corbalocServiceObj =
                                                 CorbalocServiceHelper.narrow(
                              orbManagerObj.orbObj.resolve_initial_references(
                                                          "CorbalocService"));
        corbalocServiceObj.put(channelName,eventChannelObj);
        logObj.debug2("EvtChManager:  Entered channel into corbaloc service");
      }
      catch(Exception ex)
      {
        setErrorMessage("Unable to enter event channel corbaloc:  " + ex);
        return false;
      }
           //create locator string for channel:
      String endpointStr;
      try
      {              //generate "endpoint" string for event channel:
        if((endpointStr=getObjectEndpoint((ObjectImpl)eventChannelObj)) ==
                   null || endpointStr.length() <= 0)
        {
          setErrorMessage(
           "Unable to generate endpoint for event channel locator string:  ");
          return false;
        }
      }
      catch(Exception ex)
      {
        setErrorMessage(
           "Unable to generate endpoint for event channel locator string:  " +
                                                                          ex);
        return false;
      }
      locatorString = "corbaloc:" + endpointStr + "/" + channelName;
    }
*/
    try
    {         //create and save "IOR:" string for channel:
      locatorString = orbManagerObj.orbObj.object_to_string(eventChannelObj);
    }
    catch(Exception ex)
    {
      setErrorMessage(
        ("Unable to generate event channel locator string:  " + ex),ex);
      return false;
    }
    logObj.debug2("EvtChManager:  Locator string:  " + locatorString);
    return true;
  }

  /**
   * Creates an event channel.  The event channel's locator string may be
   * fetched via the 'getLocatorString()' method.
   * @return true if successful, false if error (in which case the
   * 'getErrorMessage()' method may be used to fetch the error message).
   */
  public boolean createChViaNotifService()
  {
    return createChViaNotifService(null,null);
  }

  /**
   * Returns the 'EventChannelFactory' object used for event channels.
   * @param hostNameStr host name to use, or null to use the IP of the
   * local host.
   * @param servicePortStr port number for Notification Service on host, or
   * null for 2005.
   * @return The 'EventChannelFactory' object, or null if error (in which
   * case the 'getErrorMessage()' method may be used to fetch the error
   * message).
   */
  public synchronized EventChannelFactory getEventChannelFactory(
                                   String hostNameStr,String servicePortStr)
  {
    if(hostNameStr == null || hostNameStr.trim().length() <= 0)
    {    //host name not given; use IP of local host
      final String ipStr;    //if unable to get IP then use "localhost":
      hostNameStr = UtilFns.isIPAddress(ipStr=UtilFns.getLocalHostIP()) ?
                                                        ipStr : "localhost";
    }
    if(servicePortStr == null || servicePortStr.trim().length() <= 0)
      servicePortStr = "2005";    //if port # not given then set default
    locatorString = null;         //initialize locator string
    if(!orbManagerObj.isInitialized())
    {    //ORB not yet initialized; initialize it now
      if(!orbManagerObj.initImplementation())
      {  //error initializing ORB; get and set error message
        setErrorMessage(orbManagerObj.getErrorMessage());
        return null;
      }
    }
    final String serviceLocStr = "corbaloc:iiop:1.2@" + hostNameStr + ":" +
                                    servicePortStr + "/NotificationService";
    org.omg.CORBA.Object corbaObj;
    try
    {         //find default local NotificationService object:
      corbaObj = orbManagerObj.orbObj.string_to_object(serviceLocStr);
      logObj.debug2("EvtChManager:  Found Notification Service at " +
                                                             serviceLocStr);
    }
    catch(Exception ex)
    {                   //error resolving channel; set error message
      setErrorMessage(("Unable to find NotificationService:  " + ex),ex);
      return null;
    }
    if(corbaObj == null)
    {    //NotificationService object not resolved
      try
      {         //attempt to resolve NotificationService object:
        corbaObj = orbManagerObj.orbObj.resolve_initial_references(
                                                       "NotificationService");
        logObj.debug2("EvtChManager:  Resolved Notification Service");
      }
      catch(Exception ex)
      {                   //error resolving channel; set error message
        corbaObj = null;
      }
    }
    final EventChannelFactory factoryObj;
    try
    {         //narrow to an EventChannelFactory object:
      factoryObj = EventChannelFactoryHelper.narrow(corbaObj);
    }
    catch(Exception ex)
    {
      setErrorMessage(
        ("Unable to narrow 'EventChannelFactory' object:  " + ex),ex);
      return null;
    }
    logObj.debug2("EvtChManager:  Narrowed 'EventChannelFactory' object");
    return factoryObj;
  }

  /**
   * Finds the event channel using a given service name known to the ORB.
   * The service name is usually defined in an ORB configuration file.
   * @param serviceName the service name to use.
   * @return true if successful, false if error (in which case the
   * 'getErrorMessage()' method may be used to fetch the error message).
   */
  public synchronized boolean openViaServiceName(String serviceName)
  {
    if(eventChannelObj != null)
    {    //event channel already connected
      setErrorMessage("Event channel already opened");
      return false;
    }
    if(!orbManagerObj.isInitialized())
    {    //ORB not yet initialized; initialize it now
      if(!orbManagerObj.initImplementation())
      {  //error initializing ORB; get and set error message
        setErrorMessage(orbManagerObj.getErrorMessage());
        return false;
      }
    }
    try
    {         //resolve event channel using ORB and service name:
      eventChannelObj = resolveChannel(orbManagerObj.orbObj,serviceName);
    }
    catch(Exception ex)
    {                   //error resolving channel; set error message
      setErrorMessage(("Error opening event channel:  " + ex),ex);
      return false;
    }
    if(eventChannelObj == null)
    {    //null handle was returned; set error message
      setErrorMessage("Unable to open event channel");
      return false;
    }
    logObj.debug2("EvtChManager:  Found event channel:  " + getIorString());
    return true;
  }

  /**
   * Finds the event channel specified by the given locator string.
   * @param locStr the locator string to use.
   * @return true if successful, false if error (in which case the
   * 'getErrorMessage()' method may be used to fetch the error message).
   */
  public synchronized boolean openViaLocatorString(String locStr)
  {
    if(eventChannelObj != null)
    {    //event channel already connected
      setErrorMessage("Event channel already opened");
      return false;
    }
    if(!orbManagerObj.isInitialized())
    {    //ORB not yet initialized; initialize it now
      if(!orbManagerObj.initImplementation())
      {  //error initializing ORB; get and set error message
        setErrorMessage(orbManagerObj.getErrorMessage());
        return false;
      }
      logObj.debug2("EvtChManager:  Initialized ORB");
    }
                   //fixup "iiop://" to "corbaloc::" (if present) and save:
    locatorString = fixIiopString(locStr);
          //find event channel via locator string:
    logObj.debug2("EvtChManager:  Attempting to find event channel");
    try
    {
      eventChannelObj = stringToChannel(orbManagerObj.orbObj,locatorString);
    }
    catch(Exception ex)
    {          //error finding event channel; show error msg
      setErrorMessage(("Error finding event channel:  " + ex),ex);
      return false;
    }
    logObj.debug2("EvtChManager:  Found event channel:  " + getIorString());
    return true;
  }

  /**
   * Reads the locator string from the given file and then connects to the
   * event channel, locating it via the loaded string.
   * @param evtChLocFileNameStr the name of the locator file.
   * @return true if successful, false if error (in which case the
   * 'getErrorMessage()' method may be used to fetch the error message).
   */
  public boolean openViaLocatorFile(String evtChLocFileNameStr)
  {
    final String locStr;
    try
    {         //read CORBA-event-channel locator string from file:
      locStr =
            FileUtils.readMultiOpenFileToString(evtChLocFileNameStr).trim();
    }
    catch(Exception ex)
    {              //error opening file; set error message
      setErrorMessage(("Error opening event channel locator file (\"" +
                                   evtChLocFileNameStr + "\"):  " + ex),ex);
      return false;
    }
                   //connect to event channel via locator string:
    return openViaLocatorString(locStr);
  }

  /**
   * Closes the event channel.  (The reference to the event channel is
   * released.)
   */
  public synchronized void closeChannel()
  {
    eventChannelObj = null;            //clear handle to channel object
  }

  /**
   * Returns the status of the event channel connection.
   * @return true if the event channel is open (or if a close is pending),
   * false otherwise.
   */
  public synchronized boolean isOpen()
  {
    return (eventChannelObj != null);
  }

  /**
   * Returns the locator string for the event channel created via the
   * 'createChViaNotifService()' method or located using the
   * 'openViaLocator...()' methods.
   * @return The locator string, or null if none is available.
   */
  public String getLocatorString()
  {
    return locatorString;
  }

  /**
   * Returns the event channel object.
   * @return The event channel object, or null if the event channel is not
   * open.
   */
  public EventChannel getEventChannelObj()
  {
    return eventChannelObj;
  }

  /**
   * Returns the event channel supplier object.
   * @return The event channel supplier object, or null if the supplier
   * is not connected.
   */
  public EvtChSupplier getEvtChSupplierObj()
  {
    return evtChSupplierObj;
  }

  /**
   * Returns the event channel consumer object.
   * @return The event channel consumer object, or null if the consumer
   * is not connected.
   */
  public EvtChConsumer getEvtChConsumerObj()
  {
    return evtChConsumerObj;
  }

  /**
   * Creates a supplier object and connects it to the event channel.  The
   * event channel needs to have been opened using one of the 'open...'
   * methods.
   * @return true if successful, false if error (in which case the
   * 'getErrorMessage()' method may be used to fetch the error message).
   */
  public synchronized boolean connectSupplier()
  {
    if(evtChSupplierObj != null)
    {    //supplier already connected
      setErrorMessage("Supplier already connected to event channel");
      return false;
    }
    if(eventChannelObj == null)
    {    //event channel not open
      setErrorMessage("Event channel not open; can't connect supplier");
      return false;
    }
    disSupplierPendingFlag = false;         //clear disconnect-pending flag
            //create event channel supplier object:
    logObj.debug2("EvtChManager:  Connecting supplier to event channel");
         //create event channel supplier object:
    try
    {
      evtChSupplierObj = new EvtChSupplier(orbManagerObj.orbObj,
                                       eventChannelObj,structuredEventsFlag);
    }
    catch(Exception ex)
    {         //error creating event channel supplier object
      setErrorMessage(("Error creating event channel supplier object:  " +
                                                                    ex),ex);
      return false;
    }
    logObj.debug2("EvtChManager:  Connected supplier to event channel OK");
    return true;
  }

  /**
   * Sends a message via the event channel supplier.  The supplier needs
   * to have been connected using one of the 'connectSupplier()' methods.
   * @param msgStr the message string to send.
   * @return true if successful, false if error (in which case the
   * 'getErrorMessage()' method may be used to fetch the error message).
   */
  public synchronized boolean sendMessage(String msgStr)
  {
    if(evtChSupplierObj == null)
    {    //supplier not connected
      setErrorMessage("Supplier not connected to event channel");
      return false;
    }
    try       //send message string via supplier object:
    {
      evtChSupplierObj.push(msgStr);
    }
    catch(Exception ex)
    {              //error sending message string
      setErrorMessage(("Error sending message:  " + ex),ex);
      return false;
    }
    return true;
  }

  /**
   * Sends a structured-event message via the event channel supplier.
   * The supplier needs to have been connected using one of the
   * 'connectSupplier()' methods.
   * @param eventNameStr the event name to use, or null for none.
   * @param msgStr the message string to send.
   * @return true if successful, false if error (in which case the
   * 'getErrorMessage()' method may be used to fetch the error message).
   */
  public synchronized boolean sendMessage(String eventNameStr,String msgStr)
  {
    if(evtChSupplierObj == null)
    {    //supplier not connected
      setErrorMessage("Supplier not connected to event channel");
      return false;
    }
    try       //send message string via supplier object:
    {
      evtChSupplierObj.push(eventNameStr,msgStr);
    }
    catch(Exception ex)
    {              //error sending message string
      setErrorMessage(("Error sending message:  " + ex),ex);
      return false;
    }
    return true;
  }

  /**
   * Sends a structured-event message via the event channel supplier.
   * The supplier needs to have been connected using one of the
   * 'connectSupplier()' methods.
   * @param domainNameStr the domain name to use, or null for none.
   * @param typeNameStr the type name to use, or null for none.
   * @param eventNameStr the event name to use, or null for none.
   * @param msgStr the message string to send.
   * @return true if successful, false if error (in which case the
   * 'getErrorMessage()' method may be used to fetch the error message).
   */
  public synchronized boolean sendMessage(String domainNameStr,
                       String typeNameStr,String eventNameStr,String msgStr)
  {
    if(evtChSupplierObj == null)
    {    //supplier not connected
      setErrorMessage("Supplier not connected to event channel");
      return false;
    }
    try       //send message string via supplier object:
    {
      evtChSupplierObj.push(domainNameStr,typeNameStr,eventNameStr,msgStr);
    }
    catch(Exception ex)
    {              //error sending message string
      setErrorMessage(("Error sending message:  " + ex),ex);
      return false;
    }
    return true;
  }

  /**
   * Disconnects the supplier object from the event channel and discards
   * the supplier object.
   */
  public void disconnectSupplier()
  {
    final EvtChSupplier localSupplierObj;   //local handle to supplier obj
    final int localSupplierIdVal;           //local ID value for supplier
    synchronized(this)
    {
      if(evtChSupplierObj != null)
      {  //supplier is connected
              //increment supplier ID value and save in local variable:
        localSupplierIdVal = ++disSupplierIdValue;
        disSupplierPendingFlag = true;      //set disconnect-pending flag
        localSupplierObj = evtChSupplierObj;     //set local handle to obj
        evtChSupplierObj = null;            //clear handle to supplier obj
      }
      else
      {  //supplier not connected
        localSupplierObj = null;            //clear local handle to object
        disSupplierPendingFlag = false;     //clear disconnect-pending flag
        localSupplierIdVal = -1;
      }
    }
    if(localSupplierObj != null)
    {    //supplier was allocated
      localSupplierObj.disconnectImpl();    //disconnect supplier
      logObj.debug2("EvtChManager:  Supplier disconnect completed");
         //if no other disconnect requests have occurred during
         //'disconnectImpl()' then clear the disconnect-pending flag:
      if(localSupplierIdVal == disSupplierIdValue)
        disSupplierPendingFlag = false;
    }
  }

  /**
   * Returns the status of the supplier connection.
   * @return true if connected (or if a disconnect is pending), false
   * otherwise.
   */
  public synchronized boolean isSupplierConnected()
  {
    return (evtChSupplierObj != null || disSupplierPendingFlag);
  }

  /**
   * Sets up the filter to be attached to the consumer created and connected
   * to the event channel via the 'connectConsumer(EvtChMsgProcIntrf)'
   * method.  Event filtering is implemented such that only events that
   * match one or more of the 'EvtChEventType' objects will be received,
   * and a match consists of both the "domain" and "type" strings being
   * the same as those in the event.  Event filtering is only used with
   * structured events.  If this method is used, it must be called before
   * the 'connectConsumer(EvtChMsgProcIntrf)' method is called.
   * @param evtTypeArr the array of 'EvtChEventType' objects to use to
   * build the Notification Service filter object to use for
   * the filtering of structured events to be received by the consumer,
   * or null for none.
   * @return true if successful, false if error (in which case the
   * 'getErrorMessage()' method may be used to fetch the error message).
   */
  public synchronized boolean setupConsumerFilter(
                                               EvtChEventType [] evtTypeArr)
  {
    if(evtChConsumerObj != null)
    {    //consumer already connected
      setErrorMessage("Filter must be setup before consumer is " +
                                              "connected to event channel");
      return false;
    }
    if(!structuredEventsFlag && (evtTypeArr == null ||
                                                    evtTypeArr.length <= 0))
    {    //structured events disabled and array contains filter items
      setErrorMessage(
                    "Filter only supported when structured events enabled");
      return false;
    }
    evtFilterTypeArr = evtTypeArr;
    return true;
  }

  /**
   * Connects the given consumer object to the event channel.  The event
   * channel needs to have been opened using one of the 'open...' methods.
   * @param consumerObj the event-channel consumer object to use.
   * @return true if successful, false if error (in which case the
   * 'getErrorMessage()' method may be used to fetch the error message).
   */
  public synchronized boolean connectConsumer(EvtChConsumer consumerObj)
  {
    if(evtChConsumerObj != null)
    {    //consumer already connected
      setErrorMessage("Consumer already connected to event channel");
      return false;
    }
    if(eventChannelObj == null)
    {    //event channel not open
      setErrorMessage("Event channel not open; can't connect consumer");
      return false;
    }
    disConsumerPendingFlag = false;    //clear disconnect-pending flag
    evtChConsumerObj = consumerObj;    //set handle to given consumer object
    return true;
  }

  /**
   * Creates a consumer object and connects it to the event channel such
   * that incoming messages will be directed to the 'processMessage()'
   * method of the given call-back class.  The event channel needs to
   * have been opened using one of the 'open...' methods.
   * @param msgProcessorObj the 'MessageProcessor' call-back class to use.
   * @return true if successful, false if error (in which case the
   * 'getErrorMessage()' method may be used to fetch the error message).
   */
  public synchronized boolean connectConsumer(
                                    final EvtChMsgProcIntrf msgProcessorObj)
  {
            //create event channel consumer object:
    logObj.debug2(
               "EvtChManager:  Creating consumer object for event channel");
    final EvtChConsumer consumerObj;
    try
    {
      if(msgProcessorObj instanceof MessageProcessor)
      {  //using message-processor call-back for 'Any' events
        consumerObj = new EvtChConsumer(orbManagerObj.orbObj,
                      eventChannelObj,structuredEventsFlag,evtFilterTypeArr)
            {
              public void push(String msgStr)
              {              //send message to call-back method:
                ((MessageProcessor)msgProcessorObj).processMessage(msgStr);
              }
            };
      }
      else if(msgProcessorObj instanceof StructuredMsgProcessor)
      {  //using message-processor call-back for structured events
        consumerObj = new EvtChConsumer(orbManagerObj.orbObj,
                      eventChannelObj,structuredEventsFlag,evtFilterTypeArr)
            {
              public void push(String domainNameStr,String typeNameStr,
                                          String eventNameStr,String msgStr)
              {              //send message to call-back method:
                ((StructuredMsgProcessor)msgProcessorObj).processMessage(
                             domainNameStr,typeNameStr,eventNameStr,msgStr);
              }
            };
      }
      else
      {
        setErrorMessage("Error creating event channel consumer object:  " +
                           "Invalid message-processor call-back object:  " +
                                                           msgProcessorObj);
        return false;
      }
    }
    catch(Exception ex)
    {     //error creating event channel consumer object; set error msg
      setErrorMessage(
               ("Error creating event channel consumer object:  " + ex),ex);
      return false;
    }
    if(!connectConsumer(consumerObj))       //connect consumer to channel
      return false;
    logObj.debug2("EvtChManager:  Connected consumer to event channel OK");
    return true;
  }

  /**
   * Disconnects the consumer object from the event channel and discards
   * the consumer object.
   */
  public void disconnectConsumer()
  {
    final EvtChConsumer localConsumerObj;   //local handle to consumer obj
    final int localConsumerIdVal;           //local ID value for consumer
    synchronized(this)
    {
      if(evtChConsumerObj != null)
      {  //consumer is connected
              //increment consumer ID value and save in local variable:
        localConsumerIdVal = ++disConsumerIdValue;
        disConsumerPendingFlag = true;      //set disconnect-pending flag
        localConsumerObj = evtChConsumerObj;     //set local handle to obj
        evtChConsumerObj = null;            //clear handle to consumer obj
      }
      else
      {  //consumer not connected
        localConsumerObj = null;            //clear local handle to object
        disConsumerPendingFlag = false;     //clear disconnect-pending flag
        localConsumerIdVal = -1;
      }
    }
    if(localConsumerObj != null)
    {    //consumer was allocated
      localConsumerObj.disconnectImpl();    //disconnect consumer
      logObj.debug2("EvtChManager:  Consumer disconnect completed");
         //if no other disconnect requests have occurred during
         //'disconnectImpl()' then clear the disconnect-pending flag:
      if(localConsumerIdVal == disConsumerIdValue)
        disConsumerPendingFlag = false;
    }
  }

  /**
   * Returns the status of the consumer connection.
   * @return true if connected (or if a disconnect is pending), false
   * otherwise.
   */
  public synchronized boolean isConsumerConnected()
  {
    return (evtChConsumerObj != null || disConsumerPendingFlag);
  }

  /**
   * Returns an "IOR:" string for the event channel.
   * @return An "IOR:" string for the event channel, or null if none
   * could be generated.
   */
  public synchronized String getIorString()
  {
    return (eventChannelObj != null) ?
               channelToString(orbManagerObj.orbObj,eventChannelObj) : null;
  }

  /**
   * Enters error message (if none previously entered).
   * @param str the error message text.
   * @param exObj exception object for error.
   */
  protected synchronized void setErrorMessage(String str,Throwable exObj)
  {
//    logObj.debug2("EvtChManager:  " + str);
    if(errorMessage == null)
    {    //no previous error
      errorMessage = str;         //set error message
      errorExceptionObj = exObj;  //set error-exception object
    }
  }

  /**
   * Enters error message (if none previously entered).
   * @param str the error message text.
   */
  protected synchronized void setErrorMessage(String str)
  {
    setErrorMessage(str,null);
  }

  /**
   * Returns true if an error was detected.  The error message may be
   * fetched via the 'getErrorMessage()' method.
   * @return true if error; false if not.
   */
  public boolean getErrorFlag()
  {
    return (errorMessage != null);
  }

  /**
   * Returns message string for last error (or 'No error' if none) and
   * clears the error.
   * @return The error message text (or 'No error' if none).
   */
  public synchronized String getErrorMessage()
  {
                                  //save the error message:
    final String retStr = (errorMessage != null) ? errorMessage : "No error";
    errorMessage = null;          //clear the error
    return retStr;                //return the error message
  }

  /** Clears the error message string. */
  public synchronized void clearErrorMessage()
  {
    errorMessage = null;
    errorExceptionObj = null;     //clear error-exception object
  }

  /**
   * Returns the exception-object associated with the last error.
   * @return The exception-object associated with the last error,
   * or null if none is available.
   */
  public synchronized Throwable getErrorExceptionObj()
  {
    return errorExceptionObj;
  }

  /**
   * Resolves an Notification Service event channel object.
   * @param orbObj the CORBA ORB object to be used.
   * @param serviceName the name bound to the event channel.
   * @return A handle to the resolved event channel.
   * @exception InvalidName if the event channel name could not resolved.
   * @exception EvtChSetupException if the event channel could not be
   * setup.
   */
  public static EventChannel resolveChannel(
                                org.omg.CORBA.ORB orbObj,String serviceName)
                                      throws InvalidName,EvtChSetupException
  {
    try
    {
      org.omg.CORBA.Object cObj =      //resolve event channel name
                             orbObj.resolve_initial_references(serviceName);
      final EventChannel evtChObj;     //narrow to event channel object:
      if((evtChObj=EventChannelHelper.narrow(cObj)) == null)
      {  //error narrowing; throw exception
        throw new EvtChSetupException("Unable to narrow event channel \"" +
                                                        serviceName + "\"");
      }
      return evtChObj;            //return the event channel object
    }
    catch(Exception ex)
    {         //some kind of exception error occurred; report it
      throw new EvtChSetupException("Unable to setup event channel \"" +
                                                serviceName + "\":  " + ex);
    }
  }

  /**
   * Converts the given event channel object to an "IOR:" string.
   * @param orbObj the CORBA ORB object to be used.
   * @param evtChObj the event channel object to be converted.
   * @return An "IOR:" string, or null if an error occurred.
   */
  public static String channelToString(org.omg.CORBA.ORB orbObj,
                                                      EventChannel evtChObj)
  {
    final String str;
    try
    {              //convert object to string:
      str = orbObj.object_to_string(evtChObj);
    }
    catch(Exception ex)
    {
      return null;
    }
    if(str == null || str.trim().length() <= 0)
      return null;      //no data in string; return error
    return str;         //return string
  }

  /**
   * Converts the given "IOR:" or "corbaloc::" string to an event
   * channel object.
   * @param orbObj the CORBA ORB object to be used.
   * @param evtChIORStr an "IOR:" string version of the channel.
   * @return A handle to the event channel.
   * @exception EvtChSetupException if the event channel could not be
   * setup.
   */
  public static EventChannel stringToChannel(org.omg.CORBA.ORB orbObj,
                              String evtChIORStr) throws EvtChSetupException
  {
    try
    {                        //create CORBA object from "IOR:" string:
      org.omg.CORBA.Object cObj = orbObj.string_to_object(evtChIORStr);
//      System.out.println("EvtChManager.stringToChannel obj:  " + cObj);
      final EventChannel evtChObj;     //narrow to event channel object:
      if((evtChObj=EventChannelHelper.narrow(cObj)) == null)
      {  //error narrowing; throw exception
        throw new EvtChSetupException("Unable to narrow event channel");
      }
      return evtChObj;            //return the event channel object
    }
    catch(Exception ex)
    {         //some kind of exception error occurred; report it
      throw new EvtChSetupException("Unable to setup event channel" + ex);
    }
  }

  /**
   * Modifies an ORBacus v3 ("iiop://") locator string into an ORBacus
   * v4 ("corbaloc::") locator string.  If the given string does not
   * contain "iiop://" then it is returned unchanged.
   * @param iiopStr a locator string containing "iiop://".
   * @return a locator string.
   */
  public static String fixIiopString(String iiopStr)
  {
    return UtilFns.replaceSubstring(iiopStr,"iiop://","corbaloc::");
  }

  /**
   * Returns the IIOP "endpoint" string for the given implementation
   * object.
   * @param corbaObj the implementation object to use.
   * @return An "endpoint" string in the form: "iiop:1.2@ipaddress:port",
   * where 'ipaddress' is the local host IP address and 'port' is the
   * port used.
   */
/*
  public static String getObjectEndpoint(ObjectImpl corbaObj)
  {
         //this code is is lifted from
         // "org.openorb.notify.Service.getEventChannelFactoryCorbaloc()"
    org.openorb.CORBA.Delegate delegateObj =
                       (org.openorb.CORBA.Delegate)corbaObj._get_delegate();
    org.openorb.net.Address [] addrs = delegateObj.getAddresses(corbaObj);
    String endpointStr = null;
    for(int i=0; i<addrs.length; ++i )
    {
      if(addrs[i].getProtocol().equals("iiop"))
      {
        endpointStr = addrs[i].getEndpointString();
        break;
      }
    }
    if(endpointStr == null)
        endpointStr = addrs[0].getEndpointString();
    return endpointStr;
  }
*/
}
