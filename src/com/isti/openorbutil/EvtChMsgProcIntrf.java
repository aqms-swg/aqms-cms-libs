//EvtChMsgProcIntrf.java:  Defines a marker for message-processor
//                         interfaces.
//
// 12/12/2003 -- [ET]
//

package com.isti.openorbutil;

/**
 * Interface EvtChMsgProcIntrf defines a marker for message-processor
 * interfaces.
 */
public interface EvtChMsgProcIntrf
{
}
