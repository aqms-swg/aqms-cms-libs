//EvtChSetupException.java - Defines a CORBA user exception for errors
//                           while setting up an event channel.
//
//  2/10/2003 -- [ET]
//

package com.isti.openorbutil;

import org.omg.CORBA.UserException;


/**
 * Class EvtChSetupException defines a CORBA user exception for errors
 * while setting up an event channel.
 */
public class EvtChSetupException extends UserException
{
  public EvtChSetupException(String reasonStr)
  {
    super(reasonStr);
  }
}
