//EvtChSupplier.java - Defines a generic Notification Service event channel
//                     supplier object that pushes objects out to the event
//                     channel.  Version for OpenORB.
//
//  4/23/2003 -- [ET]  Initial verison.
// 12/11/2003 -- [ET]  Added optional support for structured events.
// 12/12/2006 -- [ET]  Modified to use event-channel queue (to make sure
//                     thread calling 'push()' method can never be blocked).
//

package com.isti.openorbutil;

import org.omg.CORBA.Any;
import org.omg.CORBA.ORBPackage.InvalidName;
import org.omg.CosEventComm.Disconnected;
import org.omg.CosNotifyChannelAdmin.EventChannel;
import org.omg.CosNotifyChannelAdmin.ProxyPushConsumer;
import org.omg.CosNotifyChannelAdmin.SupplierAdmin;
import org.omg.CosNotifyChannelAdmin.ClientType;
import org.omg.CosNotifyChannelAdmin.ProxyConsumer;
import org.omg.CosNotifyChannelAdmin.ProxyPushConsumerHelper;
import org.omg.CosNotifyChannelAdmin.StructuredProxyPushConsumer;
import org.omg.CosNotifyChannelAdmin.StructuredProxyPushConsumerHelper;
import org.omg.CosNotifyChannelAdmin.AdminLimitExceeded;
import org.omg.CosEventChannelAdmin.AlreadyConnected;
import org.omg.CosNotification.StructuredEvent;
import org.omg.CosNotification.EventType;
import org.omg.CosNotification.FixedEventHeader;
import org.omg.CosNotification.EventHeader;
import com.isti.util.queue.NotifyEventQueue;


/**
 * Class EvtChSupplier defines a generic Notification Service event
 * channel supplier object that pushes objects out to the event channel.
 */
public class EvtChSupplier
{
  private final org.omg.CORBA.ORB orbObj;
  private final EventChannel evtChObj;
  private final ProxyPushConsumer pushConsumerObj;
  private final StructuredProxyPushConsumer structuredPushConsumerObj;
  private final boolean structuredEventsFlag;
  private final EventChannelQueue eventChannelQueueObj =
                                                    new EventChannelQueue();
  private final Object evtChQueueWaitSyncObj = new Object();
  private static final String CONSTRUCTOR_ERR_STR =
                      "Error obtaining proxy push object for event channel";
  /** An empty string. */
  public static final String NULSTR = "";
  /** An empty 'CosNotification' property array. */
  public static final org.omg.CosNotification.Property []
             EMPTY_PROPERTY_ARRAY = new org.omg.CosNotification.Property[0];
  /** An empty event-type object (contents should not be modified). */
  public static final EventType EMPTY_EVENT_TYPE =
                                               new EventType(NULSTR,NULSTR);
  /** An empty fixed-header object (contents should not be modified). */
  public static final FixedEventHeader EMPTY_FIXED_HEADER =
                              new FixedEventHeader(EMPTY_EVENT_TYPE,NULSTR);
  /** An empty event-header object (contents should not be modified). */
  public static final EventHeader EMPTY_EVENT_HEADER =
                   new EventHeader(EMPTY_FIXED_HEADER,EMPTY_PROPERTY_ARRAY);

  /**
   * Creates a generic Notification Service event channel supplier object.
   * @param orbObj the CORBA ORB object to be used.
   * @param evtChObj the event channel to use.
   * @param structuredEventsFlag true to use structured events, false to
   * use 'Any' events.
   * @exception EvtChSetupException if the event channel could not be
   * setup.
   * @exception AlreadyConnected if the event channel is already
   * connected.
   * @exception AdminLimitExceeded if the supplier admin could not
   * be referenced.
   */
  public EvtChSupplier(org.omg.CORBA.ORB orbObj,EventChannel evtChObj,
                                               boolean structuredEventsFlag)
              throws EvtChSetupException,AlreadyConnected,AdminLimitExceeded
  {
    this.orbObj = orbObj;
    this.evtChObj = evtChObj;
    this.structuredEventsFlag = structuredEventsFlag;
    final SupplierAdmin supplierAdminObj;
    final ProxyConsumer proxyConsumerObj;
         //get push consumer object:
    if((supplierAdminObj=evtChObj.default_supplier_admin()) == null ||
       (proxyConsumerObj=supplierAdminObj.obtain_notification_push_consumer(
                          (structuredEventsFlag?ClientType.STRUCTURED_EVENT:
                                                      ClientType.ANY_EVENT),
                                    new org.omg.CORBA.IntHolder())) == null)
    {    //error getting push consumer object; throw exception
      throw new EvtChSetupException(CONSTRUCTOR_ERR_STR);
    }
    if(structuredEventsFlag)
    {    //using structured events
      if((structuredPushConsumerObj=StructuredProxyPushConsumerHelper.narrow(
                                                 proxyConsumerObj)) == null)
      {  //error narrowing push consumer object; throw exception
        throw new EvtChSetupException(CONSTRUCTOR_ERR_STR);
      }
      pushConsumerObj = null;     //clear 'Any' push supplier object
         //connect push consumer object to event channel:
      structuredPushConsumerObj.connect_structured_push_supplier(null);
    }
    else
    {    //using 'Any' events:
      if((pushConsumerObj=ProxyPushConsumerHelper.narrow(
                                                   proxyConsumerObj)) == null)
      {  //error narrowing push consumer object; throw exception
        throw new EvtChSetupException(CONSTRUCTOR_ERR_STR);
      }
      structuredPushConsumerObj = null;     //clear structured-event object
         //connect push consumer object to event channel:
      pushConsumerObj.connect_any_push_supplier(null);
    }
              //start processing for event-channel queue:
    eventChannelQueueObj.startThread();
  }

  /**
   * Creates a generic Notification Service event channel supplier object
   * using 'Any' events.
   * @param orbObj the CORBA ORB object to be used.
   * @param evtChObj the event channel to use.
   * @exception EvtChSetupException if the event channel could not be
   * setup.
   * @exception AlreadyConnected if the event channel is already
   * connected.
   * @exception AdminLimitExceeded if the supplier admin could not
   * be referenced.
   */
  public EvtChSupplier(org.omg.CORBA.ORB orbObj,EventChannel evtChObj)
              throws EvtChSetupException,AlreadyConnected,AdminLimitExceeded
  {
    this(orbObj,evtChObj,false);
  }

  /**
   * Creates a generic Notification Service event channel supplier object.
   * @param orbObj the CORBA ORB object to be used.
   * @param serviceName the name bound to the event channel to be used.
   * @param structuredEventsFlag true to use structured events, false to
   * use 'Any' events.
   * @exception InvalidName if the service name could not resolved.
   * @exception EvtChSetupException if the event channel could not be
   * setup.
   * @exception AlreadyConnected if the event channel is already
   * connected.
   * @exception AdminLimitExceeded if the supplier admin could not
   * be referenced.
   */
  public EvtChSupplier(org.omg.CORBA.ORB orbObj,String serviceName,
                                               boolean structuredEventsFlag)
                                     throws InvalidName,EvtChSetupException,
                                         AlreadyConnected,AdminLimitExceeded
  {
    this(orbObj,EvtChManager.resolveChannel(orbObj,serviceName),
                                                            structuredEventsFlag);
  }

  /**
   * Creates a generic Notification Service event channel supplier object
   * using 'Any' events.
   * @param orbObj the CORBA ORB object to be used.
   * @param serviceName the name bound to the event channel to be used.
   * @exception InvalidName if the service name could not resolved.
   * @exception EvtChSetupException if the event channel could not be
   * setup.
   * @exception AlreadyConnected if the event channel is already
   * connected.
   * @exception AdminLimitExceeded if the supplier admin could not
   * be referenced.
   */
  public EvtChSupplier(org.omg.CORBA.ORB orbObj,String serviceName)
                                     throws InvalidName,EvtChSetupException,
                                         AlreadyConnected,AdminLimitExceeded
  {
    this(orbObj,EvtChManager.resolveChannel(orbObj,serviceName),false);
  }

  /**
   * Disconnects this object from the event channel.
   */
  public void disconnectImpl()
  {
    try
    {         //disconnect consumer object:
      if(structuredEventsFlag)
        structuredPushConsumerObj.disconnect_structured_push_consumer();
      else
        pushConsumerObj.disconnect_push_consumer();
    }
    catch(Exception ex)
    {         //ignore any exceptions
    }
  }

  /**
   * Pushes an object out to the event channel.
   * @param anyObj the object to be pushed.
   * @exception Disconnected if this object is not connected to the event
   * channel.
   */
  public void push(Any anyObj) throws Disconnected
  {
    doEventPush(anyObj);
  }

  /**
   * Pushes a String out to the event channel.
   * @param str the String object to be send.
   * @exception Disconnected if this object is not connected to the event
   * channel.
   */
  public void push(String str) throws Disconnected
  {
    push(createAnyStringObj(str));          //create and send 'Any' object
  }

  /**
   * Pushes a structured event out to the event channel.
   * @param structuredEventObj the structured-event object to use.
   * @exception Disconnected if this object is not connected to the event
   * channel.
   * @exception NotStructuredModeException if this supplier was not created
   * with the 'structuredEventsFlag' parameter set to 'true'.
   */
  public void push(StructuredEvent structuredEventObj)
                             throws Disconnected, NotStructuredModeException
  {
    if(!structuredEventsFlag)
    {    //structured events not enabled; throw exception
      throw new NotStructuredModeException(
                         "Structured-events mode required but not enabled");
    }
    doEventPush(structuredEventObj);
  }

  /**
   * Pushes a structured event out to the event channel.
   * @param eventHeaderObj the event header to use, or null for none.
   * @param filterableItemsArr the filterable-items data to use, or
   * null for none.
   * @param anyDataObj the 'Any' data object to use.
   * @exception Disconnected if this object is not connected to the event
   * channel.
   * @exception NotStructuredModeException if this supplier was not created
   * with the 'structuredEventsFlag' parameter set to 'true'.
   */
  public void push(EventHeader eventHeaderObj,
      org.omg.CosNotification.Property [] filterableItemsArr,Any anyDataObj)
                             throws Disconnected, NotStructuredModeException
  {
    push(createStructuredEvent(eventHeaderObj,filterableItemsArr,
                                                               anyDataObj));
  }

  /**
   * Pushes a structured event out to the event channel.
   * @param eventTypeObj the event-type object to use, or null for none.
   * @param eventNameStr the event name to use, or null for none.
   * @param anyDataObj the 'Any' data object to use.
   * @exception Disconnected if this object is not connected to the event
   * channel.
   * @exception NotStructuredModeException if this supplier was not created
   * with the 'structuredEventsFlag' parameter set to 'true'.
   */
  public void push(EventType eventTypeObj,String eventNameStr,Any anyDataObj)
                             throws Disconnected, NotStructuredModeException
  {
    push(createStructuredEvent(createStructuredEventHeader(
                                                     createFixedEventHeader(
                           eventTypeObj,eventNameStr),EMPTY_PROPERTY_ARRAY),
                                          EMPTY_PROPERTY_ARRAY,anyDataObj));
  }

  /**
   * Pushes a structured event out to the event channel.
   * @param domainNameStr the domain name to use, or null for none.
   * @param typeNameStr the type name to use, or null for none.
   * @param eventNameStr the event name to use, or null for none.
   * @param anyDataObj the 'Any' data object to use.
   * @exception Disconnected if this object is not connected to the event
   * channel.
   * @exception NotStructuredModeException if this supplier was not created
   * with the 'structuredEventsFlag' parameter set to 'true'.
   */
  public void push(String domainNameStr,String typeNameStr,
                                         String eventNameStr,Any anyDataObj)
                             throws Disconnected, NotStructuredModeException
  {
    push(createStructuredEvent(createStructuredEventHeader(
                           createFixedEventHeader(createStructuredEventType(
                                   domainNameStr,typeNameStr),eventNameStr),
                    EMPTY_PROPERTY_ARRAY),EMPTY_PROPERTY_ARRAY,anyDataObj));
  }

  /**
   * Pushes a structured event out to the event channel.
   * @param domainNameStr the domain name to use, or null for none.
   * @param typeNameStr the type name to use, or null for none.
   * @param eventNameStr the event name to use, or null for none.
   * @param dataStr the string data to be sent.
   * @exception Disconnected if this object is not connected to the event
   * channel.
   * @exception NotStructuredModeException if this supplier was not created
   * with the 'structuredEventsFlag' parameter set to 'true'.
   */
  public void push(String domainNameStr,String typeNameStr,
                                         String eventNameStr,String dataStr)
                             throws Disconnected, NotStructuredModeException
  {
    push(createStructuredEvent(createStructuredEventHeader(
                           createFixedEventHeader(createStructuredEventType(
                                   domainNameStr,typeNameStr),eventNameStr),
                                 EMPTY_PROPERTY_ARRAY),EMPTY_PROPERTY_ARRAY,
                                              createAnyStringObj(dataStr)));
  }

  /**
   * Pushes a structured event out to the event channel.
   * @param eventNameStr the event name to use, or null for none.
   * @param anyDataObj the 'Any' data object to use.
   * @exception Disconnected if this object is not connected to the event
   * channel.
   * @exception NotStructuredModeException if this supplier was not created
   * with the 'structuredEventsFlag' parameter set to 'true'.
   */
  public void push(String eventNameStr,Any anyDataObj)
                             throws Disconnected, NotStructuredModeException
  {
    push(createStructuredEvent(createStructuredEventHeader(
                                                     createFixedEventHeader(
                       EMPTY_EVENT_TYPE,eventNameStr),EMPTY_PROPERTY_ARRAY),
                                          EMPTY_PROPERTY_ARRAY,anyDataObj));
  }

  /**
   * Pushes a structured event out to the event channel.
   * @param eventNameStr the event name to use, or null for none.
   * @param dataStr the string data to be sent.
   * @exception Disconnected if this object is not connected to the event
   * channel.
   * @exception NotStructuredModeException if this supplier was not created
   * with the 'structuredEventsFlag' parameter set to 'true'.
   */
  public void push(String eventNameStr,String dataStr)
                             throws Disconnected, NotStructuredModeException
  {
    push(createStructuredEvent(createStructuredEventHeader(
                                                     createFixedEventHeader(
                       EMPTY_EVENT_TYPE,eventNameStr),EMPTY_PROPERTY_ARRAY),
                         EMPTY_PROPERTY_ARRAY,createAnyStringObj(dataStr)));
  }

  /**
   * Returns the event channel object used by this object.
   * @returns the event channel object used by this object.
   */
  public EventChannel getEvtChObj()
  {
    return evtChObj;
  }

  /**
   * Creates an 'Any' object that contains the given string.
   * @param str the string to use.
   * @return A new 'Any' object.
   */
  public Any createAnyStringObj(String str)
  {
    final Any anyObj = orbObj.create_any();      //create 'Any' object
    anyObj.insert_string(str);                   //put in string
    return anyObj;                               //return 'Any' object
  }

  /**
   * Creates a structured-event type object.
   * @param domainNameStr the domain name to use, or null for none.
   * @param typeNameStr the type name to use, or null for none.
   * @return A new 'EventType' object.
   */
  public static EventType createStructuredEventType(
                                    String domainNameStr,String typeNameStr)
  {
    if(domainNameStr != null && domainNameStr.length() > 0)
    {
      if(typeNameStr != null && typeNameStr.length() > 0)
        return new EventType(domainNameStr,typeNameStr);
      return new EventType(domainNameStr,NULSTR);
    }
    if(typeNameStr != null && typeNameStr.length() > 0)
      return new EventType(NULSTR,typeNameStr);
    return EMPTY_EVENT_TYPE;
  }

  /**
   * Creates a fixed-header object.
   * @param eventTypeObj the event-type object to use, or null for none.
   * @param eventNameStr the event name to use, or null for none.
   * @return A new 'FixedEventHeader' object.
   */
  public static FixedEventHeader createFixedEventHeader(
                                 EventType eventTypeObj,String eventNameStr)
  {
    if(eventTypeObj != null && eventTypeObj != EMPTY_EVENT_TYPE)
    {
      if(eventNameStr != null)
        return new FixedEventHeader(eventTypeObj,eventNameStr);
      return new FixedEventHeader(eventTypeObj,NULSTR);
    }
    if(eventNameStr != null && eventNameStr.length() > 0)
      return new FixedEventHeader(EMPTY_EVENT_TYPE,eventNameStr);
    return EMPTY_FIXED_HEADER;
  }

  /**
   * Creates a structured-event header object.
   * @param fixedEventHeaderObj the fixed-header object to use, or
   * null for none.
   * @param variableHeaderArr the variable-header data to use, or
   * null for none.
   * @return A new 'EventHeader' object.
   */
  public static EventHeader createStructuredEventHeader(
                                       FixedEventHeader fixedEventHeaderObj,
                      org.omg.CosNotification.Property [] variableHeaderArr)
  {
    if(fixedEventHeaderObj != null &&
                                  fixedEventHeaderObj != EMPTY_FIXED_HEADER)
    {
      if(variableHeaderArr != null &&
                                  variableHeaderArr != EMPTY_PROPERTY_ARRAY)
      {
        return new EventHeader(fixedEventHeaderObj,variableHeaderArr);
      }
      return new EventHeader(fixedEventHeaderObj,EMPTY_PROPERTY_ARRAY);
    }
    if(variableHeaderArr != null &&
                                  variableHeaderArr != EMPTY_PROPERTY_ARRAY)
    {
      return new EventHeader(EMPTY_FIXED_HEADER,variableHeaderArr);
    }
    return EMPTY_EVENT_HEADER;
  }

  /**
   * Creates a structured event object.
   * @param eventHeaderObj the event header to use, or null for none.
   * @param filterableItemsArr the filterable-items data to use, or
   * null for none.
   * @param anyDataObj the 'Any' data object to use.
   * @return A new 'StructuredEvent' object.
   */
  public static StructuredEvent createStructuredEvent(
                                                 EventHeader eventHeaderObj,
                     org.omg.CosNotification.Property [] filterableItemsArr,
                                                             Any anyDataObj)
  {
    return new StructuredEvent(((eventHeaderObj != null) ? eventHeaderObj :
                                                        EMPTY_EVENT_HEADER),
                        ((filterableItemsArr != null) ? filterableItemsArr :
                                          EMPTY_PROPERTY_ARRAY),anyDataObj);
  }

  /**
   * Creates a structured event object with no filterable items.
   * @param eventHeaderObj the event header to use, or null for none.
   * @param anyDataObj the 'Any' data object to use.
   * @return A new 'StructuredEvent' object.
   */
  public static StructuredEvent createStructuredEvent(
                                                 EventHeader eventHeaderObj,
                                                             Any anyDataObj)
  {
    return new StructuredEvent(((eventHeaderObj != null) ? eventHeaderObj :
                       EMPTY_EVENT_HEADER),EMPTY_PROPERTY_ARRAY,anyDataObj);
  }

  /**
   * Creates a structured event object with no event header and no
   * filterable items.
   * @param anyDataObj the 'Any' data object to use.
   * @return A new 'StructuredEvent' object.
   */
  public static StructuredEvent createStructuredEvent(Any anyDataObj)
  {
    return new StructuredEvent(EMPTY_EVENT_HEADER,EMPTY_PROPERTY_ARRAY,
                                                                anyDataObj);
  }

  /**
   * Pushes an object out to the event channel.
   * @param obj the object to be pushed.
   * @exception Disconnected if this object is not connected to the event
   * channel.
   */
  private void doEventPush(Object obj) throws Disconnected
  {
    if(eventChannelQueueObj.getQueueSize() > 999)
    {    //event-channel queue contains more than 999 objects
      throw new Disconnected("Event-channel queue contains more " +
                                       "than 999 objects; event discarded");
    }
    if(!eventChannelQueueObj.isRunning())   //if thread stopped then abort
      throw new Disconnected("Event-channel queue thread not running");
    synchronized(evtChQueueWaitSyncObj)
    {   //grab thread lock for result status
      eventChannelQueueObj.pushEvent(obj);  //push event object on to queue
      try
      {       //wait for up to 10ms for event object to be dispatched:
        evtChQueueWaitSyncObj.wait(10);
      }
      catch(InterruptedException ex)
      {
      }
      if(eventChannelQueueObj.getQueueSize() <= 0 &&
                                 eventChannelQueueObj.getDisconnectedFlag())
      {  //queue empty & result was 'Disconnected' exception; throw it here
        throw eventChannelQueueObj.getDisconnectedExceptionObj();
      }
    }
  }


  /**
   * Class EventChannelQueue implements the queue by which events are
   * pushed out to the event channel.
   */
  private class EventChannelQueue extends NotifyEventQueue
  {
    private boolean disconnectedFlag = false;
    private Disconnected disconnectedExceptionObj = null;

    /**
     * Creates the queue.
     */
    public EventChannelQueue()
    {
      super("EventChannelQueue");
      setDaemonThread(true);           //set as daemon thread
    }

    /**
     * Executing method for queue.
     */
    public void run()
    {
      int errorCount = 0;
      boolean successFlag = false;
      Object obj;
      while(!finishRunning())
      {
        try
        {
          if((obj=waitForEvent()) != null)
          {   //event object received from quere
            if(obj instanceof Any)
            {      //object is of type 'Any'
              if(structuredEventsFlag)
              {    //using structured events
                             //create and send event with empty header:
                structuredPushConsumerObj.push_structured_event(
                                           createStructuredEvent((Any)obj));
              }
              else      //not using structured events
                pushConsumerObj.push((Any)obj);
            }
            else
            {      //object not of type 'Any' (must be 'StructuredEvent')
                             //send structured event object to channel:
              structuredPushConsumerObj.push_structured_event(
                                                      (StructuredEvent)obj);
            }
          }
          successFlag = true;          //indicate success
          synchronized(evtChQueueWaitSyncObj)
          {   //grab thread lock for result status
            disconnectedFlag = false;       //indicate not disconnected
                        //notify any threads waiting for result:
            evtChQueueWaitSyncObj.notifyAll();
          }
        }
        catch(Disconnected ex)
        {     //disconnected exception was thrown
          synchronized(evtChQueueWaitSyncObj)
          {   //grab thread lock for result status
            disconnectedExceptionObj = ex;  //save exception object
            disconnectedFlag = true;        //indicate disconnected
                        //notify any threads waiting for result:
            evtChQueueWaitSyncObj.notifyAll();
          }
        }
        catch(Exception ex)
        {     //some kind of exception error
          System.err.println(          //display error message
                 "Error moving object from queue to event channel (count=" +
                                                  errorCount + "):  " + ex);
          ex.printStackTrace();        //show stack trace
          if(successFlag)
          {   //previous push attempt succeeded
            successFlag = false;       //indicate failure
            errorCount = 1;            //reset error count
          }
          else if(++errorCount > 99)
          {   //previous push attempt failed and more than 99 errors
            System.err.println(        //display abort-thread message
                    "Too many errors; aborting event-channel-queue thread");
            break;                     //exit loop (and thread)
          }
        }
      }
      synchronized(evtChQueueWaitSyncObj)
      {  //grab thread lock for result status
        disconnectedExceptionObj = new Disconnected(  //create exception obj
                 "Event-channel queue thread terminated (too many errors)");
        disconnectedFlag = true;            //indicate disconnected
                        //notify any threads waiting for result:
        evtChQueueWaitSyncObj.notifyAll();
      }
      setRunning(false);          //indicate thread not running
    }

    /**
     * Determines if supplier is disconnected from event channel.
     * @return true if supplier is disconnected from event channel;
     * false if not.
     */
    public boolean getDisconnectedFlag()
    {
      return disconnectedFlag;
    }

    /**
     * Returns thrown 'Disconnected' exception object.
     * @return Thrown 'Disconnected' exception object.
     */
    public Disconnected getDisconnectedExceptionObj()
    {
      return disconnectedExceptionObj;
    }
  }
}
