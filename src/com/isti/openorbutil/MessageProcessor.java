//MessageProcessor.java:  Defines a class with a 'processMessage()'
//                        method.
//
//  2/10/2003 -- [ET]  Initial version.
// 12/12/2003 -- [ET]  Added "extends EvtChMsgProcIntrf".
//

package com.isti.openorbutil;

/**
 * Interface MessageProcessor defines a class with a 'processMessage()'
 * method.
 */
public interface MessageProcessor extends EvtChMsgProcIntrf
{
  /**
   * Processes a message string.
   * @param msgStr the message string to process.
   */
  public void processMessage(String msgStr);
}
