//NotStructuredModeException.java:  Defines a CORBA user exception thrown
//                                  when a method requires "using structured
//                                  events" mode to be enable and it is not.
//
// 12/11/2003 -- [ET]
//

package com.isti.openorbutil;

/**
 * Class NotStructuredModeException defines a CORBA user exception thrown
 * when a method requires "using structured events" mode to be enable and
 * it is not.
 */
public class NotStructuredModeException extends org.omg.CORBA.UserException
{
  public NotStructuredModeException(String reasonStr)
  {
    super(reasonStr);
  }
}
