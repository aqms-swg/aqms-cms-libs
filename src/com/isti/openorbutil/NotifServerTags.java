//NotifServerTags.java:  Holds references to public static strings for
//                       the Notification Server.
//
//  12/8/006 -- [ET]
//

package com.isti.openorbutil;

import com.isti.openorbhelper.RunNotifServer;

/**
 * Class NotifServerTags holds references public static strings for
 * the Notification Server.
 */
public class NotifServerTags
{
    /** Header string for status-logging output. */
  public static final String STATUSLOG_HEADER_STR =
                                        RunNotifServer.STATUSLOG_HEADER_STR;

    /** "client-count" string for status-logging output. */
  public static final String STATUSLOG_CONNCOUNT_STR =
                                   RunNotifServer.STATUSLOG_CONNCOUNT_STR;

    /** "thread-count" string for status-logging output. */
  public static final String STATUSLOG_THREADCOUNT_STR =
                                   RunNotifServer.STATUSLOG_THREADCOUNT_STR;

    /** "memory" string for status-logging output. */
  public static final String STATUSLOG_MEMORY_STR =
                                        RunNotifServer.STATUSLOG_MEMORY_STR;
}
