//OrbManager.java:  Contains methods for managing a CORBA ORB and POA.
//                  Version for OpenORB.
//
//  4/24/2003 -- [ET]  Initial version.
//  5/20/2003 -- [ET]  Added 'registerCorbalocObject()',
//                     'buildCorbalocString()' and 'resolveCorbalocObject()'
//                     methods.
//   6/4/2003 -- [ET]  Modified 'initImplementation()' to avoid possible
//                     null-pointer exceptions when creating ORB/POA.
//  6/11/2003 -- [ET]  Added 'waitForImplFinished()' method.
//  6/14/2003 -- [ET]  Moved "poaManager.activate()" from
//                     'runImplementation()' to 'initImplementation()';
//                     added stack trace to exception-error message
//                     returned by the 'runImplementation()' and
//                     'initImplementation()' methods.
//   2/2/2004 -- [ET]  Added 'getOpenOrbVersionStr()' method.
// 11/10/2004 -- [ET]  Added 'enterPublishNumericIPFlag()' method;
//                     modified to explictly enter a host address into
//                     the ORB only if one is given.
//  5/16/2008 -- [ET]  Modified 'closeImplementation()' to release ORB
//                     POA resources.
//  11/3/2010 -- [ET]  Added more threading to 'closeImplementation()'
//                     method to make sure it cannot block calling thread
//                     and added return flag.
//

package com.isti.openorbutil;

import java.util.Properties;
import org.omg.CORBA.Policy;
import org.omg.PortableServer.Servant;
import org.openorb.orb.corbaloc.CorbalocService;
import org.openorb.orb.corbaloc.CorbalocServiceHelper;
import org.openorb.util.NamingUtils;
import com.isti.util.UtilFns;
import com.isti.util.ModBoolean;
import com.isti.openorbhelper.OpenOrbVersion;

/**
 * Class OrbManager contains methods for managing a CORBA ORB and POA.
 * This implementation uses OpenORB.
 */
public class OrbManager
{
    /** Handle for CORBA ORB. */
  public org.omg.CORBA.ORB orbObj = null;
    /** Handle for the root portable object adapter. */
  public org.omg.PortableServer.POA poaObj = null;
    /** Handle for POA manager for the root POA. */
  public org.omg.PortableServer.POAManager poaManager = null;
    /** Command-line arguments passed in to the ORB. */
  public final String [] programArgs;
    /** Path and file name for ORB configuration file. */
  public final String orbCfgFileName;

    /** Host address string passed in to the ORB. */
  protected String orbHostAddressStr = null;
    /** Port number passed in to the ORB. */
  protected int orbPortNumber = 0;
    /** Flag for using numeric IPs instead of host names, true = enabled. */
  protected boolean publishNumericIPFlag = false;
    /** Flag for BiDirectional IIOP, true = enabled. */
  protected boolean biDirectionalFlag = true;
    /** Value for the 'RelativeRoundtripTimeoutPolicy'. */
  protected static final int ROUNDTRIP_TIMEOUT_SECS = 10;
    /** Flag for the 'BiDirectionalPolicy' value. */
  protected static final boolean BIDIRECTIONAL_BOTH_FLAG = true;
  protected boolean orbInitFlag = false;         //set true after init OK
  protected boolean closeImplFlag = false;       //true after close called
  protected boolean runningImplFlag = false;     //true while impl running
  protected String errorMessage = null;     //error message from parsing
  protected int runningOrbIdValue = 0;      //used by 'runImplementation()'
  protected boolean forceOrbDestroyFlag;    //used by 'forceOrbDestory()'
  protected boolean forcePoaDestroyFlag;    //used by 'forcePoaDestory()'
  protected boolean deactivatePoaMgrFlag;   //used by 'deactivatePoaMgr()'
                                            //handle to 'Corbaloc' service:
  protected CorbalocService corbalocServiceObj = null;

  /**
   * Creates an ORB/POA management object.
   * @param programArgs the array of command-line arguments to be passed
   * on to the CORBA ORB.
   * @param hostAddrStr a host name or IP address to be associated with
   * this ORB, or null to have the address auto-determined.
   * @param orbPortNum a port address value to be associated with this
   * ORB, or 0 to have the ORB choose the port address.
   * @param orbCfgFileName path and file name for ORB configuration file,
   * or null for none.
   */
  public OrbManager(String [] programArgs,String hostAddrStr,int orbPortNum,
                                                      String orbCfgFileName)
  {
    this.programArgs = (programArgs != null) ? programArgs : (new String[0]);
    this.orbHostAddressStr = (hostAddrStr != null &&
                      hostAddrStr.trim().length() > 0) ? hostAddrStr : null;
    this.orbPortNumber = orbPortNum;
    this.orbCfgFileName = orbCfgFileName;
  }

  /**
   * Creates an ORB/POA management object.
   * @param programArgs the array of command-line arguments to be passed
   * on to the CORBA ORB.
   * @param hostAddrStr a host name or IP address to be associated with
   * this ORB, or null to have the address auto-determined.
   * @param orbPortNum a port address value to be associated with this
   * ORB, or 0 to have the ORB choose the port address.
   */
  public OrbManager(String [] programArgs,String hostAddrStr,int orbPortNum)
  {
    this(programArgs,hostAddrStr,orbPortNum,null);
  }

  /**
   * Creates an ORB/POA management object.
   * @param programArgs the array of command-line arguments to be passed
   * on to the CORBA ORB.
   * @param orbCfgFileName path and file name for ORB configuration file,
   * or null for none.
   */
  public OrbManager(String [] programArgs,String orbCfgFileName)
  {
    this(programArgs,null,0,orbCfgFileName);
  }

  /**
   * Creates an ORB/POA management object.
   * @param programArgs the array of command-line arguments to be passed
   * on to the CORBA ORB.
   */
  public OrbManager(String [] programArgs)
  {
    this(programArgs,null,0,null);
  }

  /**
   * Creates an ORB/POA management object.
   */
  public OrbManager()
  {
    this(null,null,0,null);
  }

  /**
   * Enters connection parameters for the ORB.  The parameters cannot
   * be entered after the ORB is initialized (unless it has been closed).
   * @param hostAddrStr a host name or IP address to be associated with
   * this ORB, or null to have the address auto-determined.
   * @param orbPortNum a port address value to be associated with this
   * ORB, or 0 to have the ORB choose the port address.
   * @return true if successful, false if error (in which case the
   * 'getErrorMessage()' method may be used to fetch the error message).
   */
  public boolean enterConnectParams(String hostAddrStr,int orbPortNum)
  {
    if(orbInitFlag)
    {    //ORB already initialized; set error message
      setErrorMessage("Cannot change connect params after ORB initialized");
      return false;
    }
    this.orbHostAddressStr = (hostAddrStr != null &&
                      hostAddrStr.trim().length() > 0) ? hostAddrStr : null;
    this.orbPortNumber = orbPortNum;
    return true;
  }

  /**
   * Enters the "publishIP" IIOP flag for the ORB, which determines
   * if numeric IPs will be used instead of host names.  If this method
   * is not called then host names will be used (default=='false').
   * This method must be called before the ORB is initialized.
   * @param flgVal true for numeric IPs, false for host names.
   * @return true if successful, false if error (in which case the
   * 'getErrorMessage()' method may be used to fetch the error message).
   */
  public boolean enterPublishNumericIPFlag(boolean flgVal)
  {
    if(orbInitFlag)
    {    //ORB already initialized; set error message
      setErrorMessage(
               "Cannot change publishNumericIP flag after ORB initialized");
      return false;
    }
    publishNumericIPFlag = flgVal;
    return true;
  }

  /**
   * Enters the BiDirectional IIOP flag for the ORB.  If this method is
   * not called then the default of BiDirectional IIOP enabled is used.
   * This method must be called before the ORB is initialized.
   * @param flgVal true for BiDirectional IIOP enabled, false for
   * BiDirectional IIOP disabled.
   * @return true if successful, false if error (in which case the
   * 'getErrorMessage()' method may be used to fetch the error message).
   */
  public boolean enterBiDirectionalFlag(boolean flgVal)
  {
    if(orbInitFlag)
    {    //ORB already initialized; set error message
      setErrorMessage(
                 "Cannot change BirDirectional flag after ORB initialized");
      return false;
    }
    biDirectionalFlag = flgVal;
    return true;
  }

  /**
   * Initializes the implementation.  A relative-roundtrip-timeout
   * policy value of 10 seconds is setup and the bidirectional-IIOP
   * policy is enabled for consumers/clients.
   * @param corbalocServiceFlag if true then the "CorlocService" module
   * will be imported.
   * @return true if successful, false if error (in which case the
   * 'getErrorMessage()' method may be used to fetch the error message).
   */
  public boolean initImplementation(boolean corbalocServiceFlag)
  {
    orbInitFlag = false;          //clear ORB-initialized flag
    closeImplFlag = false;        //clear close-called flag
    runningImplFlag = false;      //clear implementation-running flag
    clearErrorMessage();          //clear any previous error message
         //setup CORBA ORB (using OpenORB) for event channel supplier:
              //start with empty properties object:
    final Properties propsObj = new Properties();
              //setup to use OpenORB versions CORBA of classes:
    propsObj.setProperty("org.omg.CORBA.ORBClass",
                                  org.openorb.orb.core.ORB.class.getName());
    propsObj.setProperty("org.omg.CORBA.ORBSingletonClass",
                         org.openorb.orb.core.ORBSingleton.class.getName());
              //setup messaging initializer (all in key, value ignored):
    propsObj.setProperty("org.openorb.PI.FeatureInitializerClass."
                     + "org.openorb.orb.messaging.MessagingInitializer","");
         //if flag then setup to import the "CorlocService" module:
    if(corbalocServiceFlag)
    {
      propsObj.setProperty("ImportModule.CorbalocService",
                       "${openorb.home}config/default.xml#CorbalocService");
    }

         //configure use of numeric IPs instead of host names:
    propsObj.setProperty("iiop.publishIP", (publishNumericIPFlag ?
                       Boolean.TRUE.toString() : Boolean.FALSE.toString()));

         //if given then put in path and file name for ORB config file:
    if(orbCfgFileName != null && orbCfgFileName.trim().length() > 0)
      propsObj.setProperty("openorb.config",orbCfgFileName);

         //if local host address variable contains data then enter it
    if(orbHostAddressStr != null)
      propsObj.setProperty("iiop.hostname",orbHostAddressStr);

         //if > 0 then enter port number for ORB/POA to use:
    if(orbPortNumber > 0)
      propsObj.setProperty("iiop.port",Integer.toString(orbPortNumber));

    try
    {         //create CORBA ORB and POA objects:
      orbObj = org.omg.CORBA.ORB.init(programArgs,propsObj);
      if(orbObj == null)
      {       //error creating object; set error message
        setErrorMessage("Error creating CORBA ORB");
        return false;
      }
              //resolve reference to root POA:
      poaObj = org.omg.PortableServer.POAHelper.narrow(
                              orbObj.resolve_initial_references("RootPOA"));
      if(poaObj == null)
      {       //error creating object; set error message
        setErrorMessage("Error resolving CORBA POA");
        return false;
      }
              //get POA manager (via root POA):
      poaManager = poaObj.the_POAManager();
      if(poaManager == null)
      {       //error fetching object; set error message
        setErrorMessage("Error fetching CORBA POA Manager");
        return false;
      }
      poaManager.activate();      //activate POA manager
    }
    catch(Exception ex)
    {    //error creating objects; set error message
      setErrorMessage("Error creating CORBA ORB or POA:  " + ex +
                         UtilFns.newline + UtilFns.getStackTraceString(ex));
      return false;
    }
    try       //set the 'RelativeRoundtripTimeoutPolicy'
    {         // and 'BiDirectionalPolicy' values:
      setRoundtripBiDirPolicies(orbObj,ROUNDTRIP_TIMEOUT_SECS,
                                                         biDirectionalFlag);
    }
    catch(Exception ex)
    {         //exception error; set error message
      setErrorMessage("Error setting 'RelativeRoundtripTimeoutPolicy':  " +
                                                                        ex);
      return false;
    }
    orbInitFlag = true;           //indicate ORB/POA initialized OK
    return true;
  }

  /**
   * Initializes the implementation.
   * @return true if successful, false if error (in which case the
   * 'getErrorMessage()' method may be used to fetch the error message).
   */
  public boolean initImplementation()
  {
    return initImplementation(false);
  }

  /**
   * Runs the implementation.  If 'initImplementation()' has not yet been
   * called then this method will call it.  This method blocks until the
   * implementation is shut down.
   * @return false if an error occurred while trying to run the
   * implementation (in which case the 'getErrorMessage()' method may
   * be used to fetch the error message).
   */
  public boolean runImplementation()
  {
    if(!orbInitFlag)
    {    //not yet initialized; do it now
      if(!initImplementation())
        return false;             //if error then return flag
    }
         //create local handle to ORB that will not be effected if
         // a new ORB is created before 'orbObj.run()' returns:
    final org.omg.CORBA.ORB localOrbObj = orbObj;
    final int localOrbIdVal = ++runningOrbIdValue;
    try
    {
      runningImplFlag = true;     //set implementation-running flag
      localOrbObj.run();          //run implementation (blocks until shutdown)
    }
    catch(Exception ex)
    {         //exception error occurred
         //if no other ORB implementations have started running then
         //clear implementation-running flag:
      if(localOrbIdVal == runningOrbIdValue)
        runningImplFlag = false;
      orbInitFlag = false;        //indicate ORB no longer initialized
      setErrorMessage("Error running implementation:  " + ex +
                         UtilFns.newline + UtilFns.getStackTraceString(ex));
      return false;
    }
//    try
//    {         //deallocate ORB resources:
//      localOrbObj.destroy();
//    }
//    catch(Exception ex)
//    {         //ignore any exceptions
//    }
         //if no other ORB implementations have started running then
         //clear implementation-running flag:
    if(localOrbIdVal == runningOrbIdValue)
      runningImplFlag = false;
    orbInitFlag = false;               //indicate ORB no longer initialized
//    orbObj = null;                     //clear handles
//    poaObj = null;
//    poaManager = null;
    if(!closeImplFlag)
    {
      setErrorMessage("Implementation ended unexpectedly");
      return false;
    }
    return true;                       //return OK flag
  }

  /**
   * Shuts downs the implementation.
   * @param waitFlag true to wait for CORBA operations to complete, false
   * to return immediately.
   * @return true if method successfully completed; false if not.
   */
  public boolean closeImplementation(boolean waitFlag)
  {
    boolean retFlag;
    try
    {
      closeImplFlag = true;            //indicate close method called
      retFlag = closePoaAndManager();  //release POA and POAManager
      orbInitFlag = false;             //indicate ORB no longer initialized
      if(!shutdownMgrOrb(waitFlag))    //shut down ORB
        retFlag = false;               //if not completed then clear flag
    }
    catch(Exception ex)
    {    //ignore any exceptions
      retFlag = false;
    }
    return retFlag;
  }

  /**
   * Closes and releases resources for the POA and the POAManager.  A
   * separate thread and a timeout are used to prevent the called
   * thread from being blocked.
   * @return true if method successfully completed; false if not.
   */
  private boolean closePoaAndManager()
  {
              //setup local handles (thread safe):
    final org.omg.PortableServer.POA localPoaObj = poaObj;
    final org.omg.PortableServer.POAManager localPoaManager = poaManager;
    final ModBoolean completeFlagObj = new ModBoolean(false);
              //create separate worker thread:
    final Thread threadObj = (new Thread("closePoaAndManager")
      {
        public void run()
        {
          try
          {         //deallocate POA and manager resources:
            if(localPoaManager != null)
              localPoaManager.deactivate(false,false);
            if(localPoaObj != null)
              localPoaObj.destroy(false,false);
            completeFlagObj.setValue(true);      //indicate completion
          }
          catch(Exception ex)
          {         //ignore any exceptions
          }
        }
      });
    threadObj.start();            //start worker thread
    try
    {         //wait for worker thread to finish (up to 3 seconds):
      threadObj.join(3000);
    }
    catch(InterruptedException ex)
    {
    }
    return completeFlagObj.getValue();      //return 'true' if completed
  }

  /**
   * Shuts down and releases resources for the ORB.  A separate thread
   * and a timeout are used to prevent the called thread from being blocked.
   * @param waitFlag true to wait for CORBA operations to complete, false
   * to return immediately.
   * @return true if method successfully completed; false if not.
   */
  private boolean shutdownMgrOrb(final boolean waitFlag)
  {
              //setup local handle to ORB (thread safe):
    final org.omg.CORBA.ORB localOrbObj = orbObj;
    if(localOrbObj == null)       //if no ORB setup then
      return true;                //just return
    final ModBoolean completeFlagObj = new ModBoolean(false);
              //create separate worker thread:
    final Thread threadObj = (new Thread("shutdownMgrOrb")
      {
        public void run()
        {
          try
          {
            localOrbObj.shutdown(waitFlag);      //perform ORB shutdown
            localOrbObj.destroy();               //release ORB resources
            completeFlagObj.setValue(true);      //indicate completion
          }
          catch(Exception ex)
          {   //ignore any exceptions
          }
        }
      });
    threadObj.start();            //start worker thread
    try
    {         //wait for worker thread to finish (up to 3 seconds):
      threadObj.join(2000);
    }
    catch(InterruptedException ex)
    {
    }
    return completeFlagObj.getValue();      //return 'true' if completed
  }

  /**
   * Returns state of ORB implementation.
   * @return true if initialized, false if not.
   */
  public boolean isInitialized()
  {
    return orbInitFlag;
  }

  /**
   * Returns the status of the running implementation.
   * @return true if the implementation is running; false if not.
   */
  public boolean isImplementationRunning()
  {
    return runningImplFlag;
  }

  /**
   * Waits for the implementation to stop running, up to the given
   * timeout.
   * @param timeOutMS the timeout to use.
   * @return true if the implementation stopped; false if the timeout
   * was reached (or if the thread was interrupted).
   */
  public boolean waitForImplFinished(int timeOutMS)
  {
    final long startTimeVal = System.currentTimeMillis();
    do
    {
      if(!isImplementationRunning())
        return true;         //if stopped then return
      try
      {       //delay between checks
        Thread.sleep(10);
      }
      catch(InterruptedException ex)
      {       //if interrupted then exit
        break;
      }
    }                        //loop if timeout not yet reached:
    while(System.currentTimeMillis() - startTimeVal < timeOutMS);
    return false;
  }

  /**
   * Forcibly destroys the POA and its resources.  This method should only
   * be used after 'closeImplementation()' has failed to make the
   * 'runImplementation()' method return.  A new thread is launched to
   * perform the work to keep things from getting blocked if the POA
   * 'destroy()' method gets "stuck".
   * @return true if the POA 'destroy()' method returned, false if a
   * timeout occurred.
   */
  public boolean forcePoaDestroy()
  {
    forcePoaDestroyFlag = false;
    (new Thread("forcePoaDestroy")
      {
        public void run()
        {
          try
          {         //deallocate POA resources:
            poaObj.destroy(false,false);
            forcePoaDestroyFlag = true;     //indicate method returned
          }
          catch(Exception ex)
          {         //ignore any exceptions
          }
        }
      }).start();

    int c = 0;
    do
    {    //wait for destroy to complete, up to 3 seconds
      try { Thread.sleep(100); }            //delay 0.1 seconds
      catch(InterruptedException ex) {}
      if(forcePoaDestroyFlag)
        return true;              //return OK flag
    }
    while(++c < 30);    //loop if timeout not reached
    return false;       //return false if timeout
  }

  /**
   * Deactives the POA manager.  A new thread is launched to
   * perform the work to keep things from getting blocked if the
   * 'deactivate()' method gets "stuck".
   * @return true if the POA 'deactivate()' method returned, false if a
   * timeout occurred.
   */
  public boolean deactivatePoaMgr()
  {
    deactivatePoaMgrFlag = false;
    (new Thread("deactivatePoaMgr")
      {
        public void run()
        {
          try
          {         //deactivate POA manager:
            poaManager.deactivate(false,false);
            deactivatePoaMgrFlag = true;    //indicate method returned
          }
          catch(Exception ex)
          {         //ignore any exceptions
          }
        }
      }).start();

    int c = 0;
    do
    {    //wait for destroy to complete, up to 3 seconds
      try { Thread.sleep(100); }            //delay 0.1 seconds
      catch(InterruptedException ex) {}
      if(deactivatePoaMgrFlag)
        return true;              //return OK flag
    }
    while(++c < 30);    //loop if timeout not reached
    return false;       //return false if timeout
  }

  /**
   * Sets the 'RelativeRoundtripTimeoutPolicy' and 'BiDirectionalPolicy'
   * values for the given ORB.
   * Note that for the 'RelativeRoundtripTimeoutPolicy' to work the
   * 'messaging.MessagingInitializer' class must be entered as an OpenORB
   * "FeatureInitializer".
   * @param orbObj the ORB object to use.
   * @param timeoutSecs the timeout value to use, in seconds.
   * @param biDirBothFlag if true then a BiDirectional Policy value of "BOTH"
   * is entered (BiDirectional IIOP enabled), otherwise a BiDirectional
   * Policy value of "NORMAL" is entered (BiDirectional IIOP disabled).
   * @throws org.omg.CORBA.ORBPackage.InvalidName if the ORBPolicyManager
   * cannot be found.
   * @throws org.omg.CORBA.PolicyError if a policy error occurs.
   * @throws org.omg.CORBA.InvalidPolicies if the policy is invalid.
   */
  public static void setRoundtripBiDirPolicies(org.omg.CORBA.ORB orbObj,
                                      int timeoutSecs,boolean biDirBothFlag)
                                throws org.omg.CORBA.ORBPackage.InvalidName,
                     org.omg.CORBA.PolicyError,org.omg.CORBA.InvalidPolicies
  {
              //create policy array:
    final Policy [] policyArr = new Policy[]
                  {  createRoundtripTimeoutPolicyObj(orbObj,timeoutSecs),
                     createBiDirectionalPolicyObj(orbObj,biDirBothFlag) };
              //enter Policy array into ORB:
    enterPoliciesArray(orbObj,policyArr);
  }

  /**
   * Creates a 'Policy' object that may be used to set the given
   * 'RelativeRoundtripTimeoutPolicy' value.
   * Note that for this to work the 'messaging.MessagingInitializer'
   * class must be entered as an OpenORB "FeatureInitializer".
   * @param orbObj the ORB object to use.
   * @param timeoutSecs the timeout value to use, in seconds.
   * @return A 'Policy' object containing the given
   * 'RelativeRoundtripTimeoutPolicy' value.
   * @throws org.omg.CORBA.PolicyError if a policy error occurs.
   */
  public static Policy createRoundtripTimeoutPolicyObj(
                                   org.omg.CORBA.ORB orbObj,int timeoutSecs)
                                            throws org.omg.CORBA.PolicyError
  {
              //create 'Any' object:
    final org.omg.CORBA.Any anyObj = orbObj.create_any();
              //convert time from 1ms to 100ns and insert into 'Any' object:
    org.omg.TimeBase.TimeTHelper.insert(anyObj,  //convert from sec to 100ns
                                            ((long)timeoutSecs) * 10000000);
              //create and return policy object:
    return orbObj.create_policy(
            org.omg.Messaging.RELATIVE_RT_TIMEOUT_POLICY_TYPE.value,anyObj);
  }

  /**
   * Creates a 'Policy' object that may be used to set a
   * 'BiDirectionalPolicy' value (for BiDirectional IIOP).
   * @param orbObj the ORB object to use.
   * @param bothFlag if true then a BiDirectional Policy value of "BOTH"
   * is entered (BiDirectional IIOP enabled), otherwise a BiDirectional
   * Policy value of "NORMAL" is entered (BiDirectional IIOP disabled).
   * @return A 'Policy' object containing the given 'BiDirectionalPolicy'
   * value.
   * @throws org.omg.CORBA.PolicyError if a policy error occurs.
   */
  public static Policy createBiDirectionalPolicyObj(
                                  org.omg.CORBA.ORB orbObj,boolean bothFlag)
                                            throws org.omg.CORBA.PolicyError
  {
              //create 'Any' object:
    final org.omg.CORBA.Any anyObj = orbObj.create_any();
              //insert policy value, based on flag:
    anyObj.insert_ushort(bothFlag ? org.omg.BiDirPolicy.BOTH.value :
                                            org.omg.BiDirPolicy.BOTH.value);
              //create and return policy object:
    return orbObj.create_policy(
            org.omg.BiDirPolicy.BIDIRECTIONAL_POLICY_TYPE.value,anyObj);
  }

  /**
   * Enters the given array of 'Policy' objects.
   * @param orbObj the ORB object to use.
   * @param policyArr the array of 'Policy' objects to use.
   * @throws org.omg.CORBA.ORBPackage.InvalidName if the ORBPolicyManager
   * cannot be found.
   * @throws org.omg.CORBA.InvalidPolicies if a policy is invalid.
   */
  public static void enterPoliciesArray(org.omg.CORBA.ORB orbObj,
                                                        Policy [] policyArr)
                               throws  org.omg.CORBA.ORBPackage.InvalidName,
                                               org.omg.CORBA.InvalidPolicies
  {
              //get policy manager for ORB:
    final org.omg.CORBA.PolicyManager policyMgrObj =
                                               (org.omg.CORBA.PolicyManager)
                      orbObj.resolve_initial_references("ORBPolicyManager");
              //enter policy array:
    policyMgrObj.set_policy_overrides(policyArr,
                                org.omg.CORBA.SetOverrideType.ADD_OVERRIDE);
  }

  /**
   * Registers a 'Corbaloc' service reference for a CORBA object.  This
   * manager object should be initialized with its 'corbalocServiceFlag'
   * set 'true' to make the Corbaloc' service available.
   * @param refNameStr the reference name to be associated with the object.
   * @param cObj the CORBA object handle to be entered.
   * @return true if successful, false if error (in which case the
   * 'getErrorMessage()' method may be used to fetch the error message).
   */
  public boolean registerCorbalocObject(String refNameStr,
                                                  org.omg.CORBA.Object cObj)
  {
    if(corbalocServiceObj == null)
    {    //handle to Corbaloc service not yet resolved
      try
      {       //resolve corbaloc service object:
        corbalocServiceObj = CorbalocServiceHelper.narrow(
                      orbObj.resolve_initial_references("CorbalocService"));
      }
      catch(Exception ex)
      {       //error resolving; set error message
        corbalocServiceObj = null;
        setErrorMessage("Error resolving 'Corbaloc' service:  " + ex);
        return false;
      }
    }
    try       //enter event object into corbaloc service:
    {                   // (encode reference to RFC2396 standard)
      corbalocServiceObj.put(NamingUtils.encodeRFC2396(refNameStr),cObj);
    }
    catch(Exception ex)
    {         //error entering object; set error message
      setErrorMessage("Error entering object (ref=\"" + refNameStr +
                                     "\") into 'Corbaloc' service:  " + ex);
      return false;
    }
    return true;
  }

  /**
   * Registers a 'Corbaloc' service reference for a CORBA Servant object.
   * This manager object should be initialized with its 'corbalocServiceFlag'
   * set 'true' to make the Corbaloc' service available.
   * @param refNameStr the reference name to be associated with the object.
   * @param servantObj the CORBA servant object handle to be entered.
   * @return true if successful, false if error (in which case the
   * 'getErrorMessage()' method may be used to fetch the error message).
   */
  public boolean registerCorbalocObject(String refNameStr,Servant servantObj)
  {
    return registerCorbalocObject(refNameStr,servantObj._this_object(orbObj));
  }

  /**
   * Builds a "Corbaloc" locator string.
   * @param hostAddress the host address to use.
   * @param portNumber the port number to use.
   * @param refNameStr the object reference name to use.
   * @return A "Corbaloc" string in the form
   * "corbaloc:iiop:1.2@hostAddress:portNumber/refNameStr".
   */
  public static String buildCorbalocString(String hostAddress,
                                           int portNumber,String refNameStr)
  {
    return "corbaloc:iiop:1.2@" + hostAddress + ":" + portNumber + "/" +
                                                                 refNameStr;
  }

  /**
   * Resolves a CORBA object via the "Corbaloc" service.
   * @param hostAddress the host address to use.
   * @param portNumber the port number to use.
   * @param refNameStr the object reference name to use.
   * @return The resolved CORBA object, or null if an error occurred (in
   * which case the 'getErrorMessage()' method may be used to fetch the
   * error message).
   */
  public org.omg.CORBA.Object resolveCorbalocObject(String hostAddress,
                                           int portNumber,String refNameStr)
  {
    try
    {         //build 'Corbaloc' string and resolve to object:
      return orbObj.string_to_object(buildCorbalocString(hostAddress,
                                                    portNumber,refNameStr));
    }
    catch(Exception ex)
    {         //error resolving; set error message
      setErrorMessage("Error resolving object (ref=\"" + refNameStr +
                                      "\") via 'Corbaloc' service:  " + ex);
      return null;
    }
  }

  /**
   * Returns the host string entered into the ORB.
   * @return the host string entered into the ORB, or null if none entered.
   */
  public String getEnteredHostAddrStr()
  {
    return orbHostAddressStr;
  }

  /**
   * Returns the port number entered into the ORB.
   * @return the port number entered into the ORB, or 0 if none entered.
   */
  public int getOrbPortNum()
  {
    return orbPortNumber;
  }

    /**
     * Enter error message (if none previously entered).
     * @param str the error message text.
     */
  protected void setErrorMessage(String str)
  {
    if(errorMessage == null)      //if no previous error then
      errorMessage = str;         //set error message
  }

    /**
     * Returns true if an error was detected.  The error message may be
     * fetched via the 'getErrorMessage()' method.
     * @return true if error; false if not.
     */
  public boolean getErrorFlag()
  {
    return (errorMessage != null);
  }

    /**
     * Returns message string for last error (or 'No error' if none) and
     * clears the error.
     * @return The error message text (or 'No error' if none).
     */
  public String getErrorMessage()
  {
                                  //save the error message:
    final String retStr = (errorMessage != null) ? errorMessage : "No error";
    errorMessage = null;          //clear the error
    return retStr;                //return the error message
  }

    /** Clears the error message string. */
  public void clearErrorMessage()
  {
    errorMessage = null;
  }

    /**
     * Returns the OpenORB version string.
     * @return The OpenORB version string.
     */
  public static String getOpenOrbVersionStr()
  {
    return OpenOrbVersion.VERSION_STR;
  }
}
