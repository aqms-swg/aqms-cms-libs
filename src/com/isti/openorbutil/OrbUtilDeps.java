//OrbUtilDeps.java:  Contains references to CORBA classes needed to
//                   create a stand-alone Notification Service (consumer
//                   or supplier) application 'jar' file using JBuilder.
//
//  4/21/2003 -- [ET]  Initial version.
// 11/29/2004 -- [ET]  Added "SocketStreamDecorationStrategy" references.
//  3/16/2005 -- [ET]  Added "UNKNOWNHelper" reference.
// 11/12/2005 -- [ET]  Added "NamingUtils", "PushConsumerPOA" and
//                     "StructuredPushConsumerPOA" references.
//

package com.isti.openorbutil;

/**
 * Class OrbUtilDeps contains references to CORBA classes needed to
 * create a stand-alone Notification Service (consumer or supplier)
 * application 'jar' file using JBuilder.
 */
public class OrbUtilDeps
{
    //these classes are needed by any app using the Notification Service:
  private static final org.openorb.notify.Service dummySvcObj = null;
  private static final org.openorb.orb.config.OpenORBLoader
                                                         dummyLdrObj = null;
  private static final org.openorb.util.urlhandler.HandlerFactory
                                                       dummyHFactObj = null;
  private static final org.openorb.util.urlhandler.resource.Handler
                                                        dummyHdlrObj = null;
  private static final org.openorb.util.urlhandler.classpath.Handler
                                                      dummyCPHdlrObj = null;
  private static final org.openorb.orb.iiop.IIOPProtocolInitializer
                                                      dummyPrInitObj = null;
  private static final org.openorb.orb.adapter.poa.POAInitializer
                                                     dummyPoaInitObj = null;
  private static final org.openorb.orb.adapter.fwd.ForwardInitializer
                                                     dummyFwdInitObj = null;
  private static final org.openorb.pss.Initializer dummyPInitObj = null;
  private static final org.openorb.orb.net.ServerManagerImpl
                                                      dummySvrMgrObj = null;
  private static final org.openorb.orb.net.ClientManagerImpl
                                                      dummyCltMgrObj = null;
  private static final org.openorb.orb.iiop.IIOPTransportClientInitializer
                                                    dummyTCltInitObj = null;
  private static final org.openorb.orb.messaging.MessagingInitializer
                                                     dummyMsgInitObj = null;
  private static final org.openorb.orb.pi.SimpleServerManager
                                                     dummySSvrMgrObj = null;
  private static final org.openorb.orb.pi.SimpleIORManager
                                                     dummySIorMgrObj = null;
  private static final org.openorb.orb.net.
       PriorityBoostingSocketStreamDecorationStrategy dummyPbssdsObj = null;
  private static final org.openorb.orb.net.
               BufferingSocketStreamDecorationStrategy dummyBssdsObj = null;
  private static final org.openorb.orb.net.
                  LegacySocketStreamDecorationStrategy dummyLssdsObj = null;
  private static final org.omg.CosNotifyComm.StructuredPushConsumerPOA
                                                        dummySpcpObj = null;
  private static final org.omg.CosNotifyComm.PushConsumerPOA
                                                         dummyPcpObj = null;
  private static final org.openorb.util.NamingUtils dummyNutilsObj = null;


         //this class is required but also deprecated, so it is
         // included via the JBuilder-archive content:
//  private static final org.openorb.orb.config.OpenORBConnector
//                                                     dummyOrbConnObj = null;

    //Xerces classes needed for Java 1.3 support (Java 1.4 includes Xerces):
  private static final org.apache.xerces.jaxp.DocumentBuilderFactoryImpl
                                                       dummyDocBFact = null;
  private static final org.apache.xerces.parsers.XML11Configuration
                                                       dummyXml11Obj = null;
  private static final org.apache.xerces.impl.dv.dtd.DTDDVFactoryImpl
                                                   dummyDTDDVFactObj = null;

    //support classes for "CORBA.SystemException" classes:
    // (needed by any app using the Notification Service):
  private static final org.omg.CORBA.UNKNOWNHelper h00obj = null;
  private static final org.omg.CORBA.BAD_PARAMHelper h01obj = null;
  private static final org.omg.CORBA.NO_MEMORYHelper h02obj = null;
  private static final org.omg.CORBA.IMP_LIMITHelper h03obj = null;
  private static final org.omg.CORBA.COMM_FAILUREHelper h04obj = null;
  private static final org.omg.CORBA.INV_OBJREFHelper h05obj = null;
  private static final org.omg.CORBA.NO_PERMISSIONHelper h06obj = null;
  private static final org.omg.CORBA.INTERNALHelper h07obj = null;
  private static final org.omg.CORBA.MARSHALHelper h08obj = null;
  private static final org.omg.CORBA.INITIALIZEHelper h09obj = null;
  private static final org.omg.CORBA.NO_IMPLEMENTHelper h10obj = null;
  private static final org.omg.CORBA.BAD_TYPECODEHelper h11obj = null;
  private static final org.omg.CORBA.BAD_OPERATIONHelper h12obj = null;
  private static final org.omg.CORBA.NO_RESOURCESHelper h13obj = null;
  private static final org.omg.CORBA.NO_RESPONSEHelper h14obj = null;
  private static final org.omg.CORBA.PERSIST_STOREHelper h15obj = null;
  private static final org.omg.CORBA.BAD_INV_ORDERHelper h16obj = null;
  private static final org.omg.CORBA.TRANSIENTHelper h17obj = null;
  private static final org.omg.CORBA.FREE_MEMHelper h18obj = null;
  private static final org.omg.CORBA.INV_IDENTHelper h19obj = null;
  private static final org.omg.CORBA.INV_FLAGHelper h20obj = null;
  private static final org.omg.CORBA.INTF_REPOSHelper h21obj = null;
  private static final org.omg.CORBA.BAD_CONTEXTHelper h22obj = null;
  private static final org.omg.CORBA.OBJ_ADAPTERHelper h23obj = null;
  private static final org.omg.CORBA.DATA_CONVERSIONHelper h24obj = null;
  private static final org.omg.CORBA.OBJECT_NOT_EXISTHelper h25obj = null;
  private static final org.omg.CORBA.TRANSACTION_REQUIREDHelper h26obj = null;
  private static final org.omg.CORBA.TRANSACTION_ROLLEDBACKHelper h27obj = null;
  private static final org.omg.CORBA.INVALID_TRANSACTIONHelper h28obj = null;
  private static final org.omg.CORBA.INV_POLICYHelper h29obj = null;
  private static final org.omg.CORBA.CODESET_INCOMPATIBLEHelper h30obj = null;
  private static final org.omg.CORBA.REBINDHelper h31obj = null;
  private static final org.omg.CORBA.TIMEOUTHelper h32obj = null;
  private static final org.omg.CORBA.TRANSACTION_UNAVAILABLEHelper h33obj = null;
  private static final org.omg.CORBA.TRANSACTION_MODEHelper h34obj = null;
  private static final org.omg.CORBA.BAD_QOSHelper h35obj = null;
}
