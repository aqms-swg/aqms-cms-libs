//StructuredMsgProcessor.java:  Defines a class with a 'processMessage()'
//                              method; for messages coming from
//                              structured events.
//
// 12/12/2003 -- [ET]
//

package com.isti.openorbutil;

/**
 * Interface StructuredMsgProcessor defines a class with a
 * 'processMessage()' method; for messages coming from structured
 * events.
 */
public interface StructuredMsgProcessor extends EvtChMsgProcIntrf
{
  /**
   * Processes a message string.
   * @param domainNameStr the domain name to use, or null for none.
   * @param typeNameStr the type name to use, or null for none.
   * @param eventNameStr the event name to use, or null for none.
   * @param msgStr the message string to process.
   */
  public void processMessage(String domainNameStr,String typeNameStr,
                                         String eventNameStr,String msgStr);
}
