//AlertMessage.java:  Defines an alert message.
//
//  6/20/2005 -- [KF]  Initial version.
//  1/19/2006 -- [KF]  Moved from 'QWCapOutClient' to 'QWCommon'.
//  6/12/2006 -- [ET]  Modified to try 'getNetID()' for network-source code
//                     if 'getDataSource()' value not matched.
//

package com.isti.quakewatch.alert;

import com.isti.quakewatch.message.QWEventMsgRecord;
import com.isti.quakewatch.util.SeismicNetworkInfo;
import com.isti.util.LogFile;
import com.isti.quakewatch.util.QWUtils;

/**
 * Class AlertMessage defines an alert message.
 */
public abstract class AlertMessage
{
    /** String containing the 'M' character. */
  public static final String MSTR = "M";
    /** String containing "N/A". */
  protected static final String NASTR = "N/A";

    /** Message type value for message. */
  public final int messageType;
    /** Event-message-record object for message. */
  public final QWEventMsgRecord eventMsgRecObj;
    /** Seismic-network information for message. */
  public final SeismicNetworkInfo networkInfoObj;
    /** Log file object. */
  public final LogFile logFileObj;
    /** Magnitude-value string. */
  public final String eventMagValStr;
    /** Magnitude-type string or null if not available. */
  public final String eventMagTypeStr;
    /** Magnitude-with-type display string. */
  public final String evtMagValTypeStr;


  /**
   * Creates an alert message.
   * @param logObj the 'LogFile' object.
   * @param messageType message type value for message.
   * @param eventMsgRecObj event-message-record object for message.
   * @param networkInfoObj seismic-network information for building
   * the message.
   */
  protected AlertMessage(
      LogFile logObj, int messageType,
      QWEventMsgRecord eventMsgRecObj, SeismicNetworkInfo networkInfoObj)
  {
    this.logFileObj = logObj;
    this.messageType = messageType;
    this.eventMsgRecObj = eventMsgRecObj;
    this.networkInfoObj = networkInfoObj;

    //get magnitude-value string:
    eventMagValStr = eventMsgRecObj.getMagnitudeValueString();
    final String magnitudeType = eventMsgRecObj.getMagnitudeType();
    if (magnitudeType != null)  //if magnitude type is available
    {
      //get magnitude-type string:
      eventMagTypeStr = QWUtils.magTypeCharToStr(magnitudeType);
      //build magnitude-with-type display string:
      evtMagValTypeStr = MSTR + eventMagTypeStr +" " + eventMagValStr;
    }
    else
    {
      //get magnitude-type string:
      eventMagTypeStr = null;
      //build magnitude-with-type display string:
      evtMagValTypeStr = MSTR + eventMagValStr;
    }
  }

  /**
   * Returns the given string (if not null) or "N/A" (if null).
   * @param str the string to use.
   * @return The given string (if not null) or "N/A" (if null).
   */
  protected static final String showValOrNA(String str)
  {
    return (str != null) ? str : NASTR;
  }

  /**
   * Returns the 'SeismicNetworkInfo' object for the given event-message-record.
   * @param eventMsgRecObj event-message-record object for message.
   * @return The 'SeismicNetworkInfo' object for the given data-source code,
   * or null if no match was found.
   */
  public static SeismicNetworkInfo getNetworkInfo(
                                            QWEventMsgRecord eventMsgRecObj)
  {
         //attempt to use 'getDataSource()' for network-source code:
    final String srcStr;
    final SeismicNetworkInfo infoObj;
    if((srcStr=eventMsgRecObj.getDataSource()) != null &&
                                               srcStr.trim().length() > 0 &&
                (infoObj=SeismicNetworkInfo.getNetworkInfo(srcStr)) != null)
    {    //source code OK and corresponding network-info found
      return infoObj;
    }
         //attempt to use 'getNetID()' for network-source code:
    return SeismicNetworkInfo.getNetworkInfo(eventMsgRecObj.getNetID());
  }

  /**
   * Returns a tag string corresponding to the message type.
   * @param messageType message type value.
   * @return a tag string corresponding to the message type.
   */
  public static String getMsgTypeTagStr(int messageType)
  {
    switch(messageType)
    {
      case AlertRecipient.MSG_NEWEVT_TYPE:
        return "New-Event";
      case AlertRecipient.MSG_UPDATE_TYPE:
        return "Update";
      case AlertRecipient.MSG_CANCEL_TYPE:
        return "Cancel";
      default:
        return "Unknown";
    }
  }

  /**
   * Returns a tag string corresponding to the message type.
   * @return "New-Event", "Update", "Cancel" or "Unknown".
   */
  public String getMsgTypeTagStr()
  {
    return getMsgTypeTagStr(messageType);
  }

  /**
   * Returns a string containing a summary of the message.
   * @return A string containing a summary of the message.
   */
  public String toString()
  {
    return "MessageType=\"" + getMsgTypeTagStr() + "\", Event=\"" +
                       eventMsgRecObj.getDisplayString();
  }
}
