//AlertMsgDateFormatter.java:  Defines an alert message date formatter.
//
//  1/19/2006 -- [KF]  Initial version.
//

package com.isti.quakewatch.alert;

import java.util.Date;
import com.isti.util.UtilFns;
import java.util.TimeZone;
import java.text.DateFormat;

/**
 * Class AlertMsgDateFormatter defines an alert message date formatter.
 */
public class AlertMsgDateFormatter implements AlertMsgDateFormatterInterface
{
    /** Date format string for long messages. */
  public static final String LONG_DATE_FMTSTR = "yyyy/MM/dd HH:mm:ss.SSS z";
    /** Date format string for short messages. */
  public static final String SHORT_DATE_FMTSTR = "yyyy/MM/dd HH:mm:ss z";
    /** Date formatter for long messages using GMT. */
  protected final DateFormat gmtLongDateFormatter =
                               UtilFns.createDateFormatObj(LONG_DATE_FMTSTR,
                                                 UtilFns.GMT_TIME_ZONE_OBJ);
    /** Date formatter for long messages using local time zone. */
  protected final DateFormat localLongDateFormatter =
                               UtilFns.createDateFormatObj(LONG_DATE_FMTSTR,
                                                     TimeZone.getDefault());
    /** Date formatter for short messages. */
  protected final DateFormat shortDateFormatter =
                              UtilFns.createDateFormatObj(SHORT_DATE_FMTSTR,
                                                     TimeZone.getDefault());
  /**
   * Updates the date formatters that use the local-time-zone configuration
   * property.
   * @param useGMTShortFlag true to use GMT for date/times in short messages.
   * @param localTimeZone the local time zone.
   * @param updateShortFlag true to always update the the date formatter
   * used for short messages.
   */
  public void updateLocalTimeZone(
      boolean useGMTShortFlag, String localTimeZone, boolean updateShortFlag)
  {
    final TimeZone timeZoneObj;
    synchronized(localLongDateFormatter)
    {    //only allow one thread at a time to access date-formatter object
      localLongDateFormatter.setTimeZone(timeZoneObj=
                  UtilFns.parseTimeZoneID(localTimeZone));
    }
    if(!useGMTShortFlag)
    {    //using local time zone for short messages; update time zone
      synchronized(shortDateFormatter)
      {  //only allow one thread at a time to access date-formatter object
        shortDateFormatter.setTimeZone(timeZoneObj);
      }
    }
    else if(updateShortFlag)
    {    //using GMT time zone for short messages and flag is set
      synchronized(shortDateFormatter)
      {  //only allow one thread at a time to access date-formatter object
        shortDateFormatter.setTimeZone(UtilFns.GMT_TIME_ZONE_OBJ);
      }
    }
  }

  /**
   * Updates the time zone for the date formatter used for short messages.
   * @param useGMTShortFlag true to use GMT for date/times in short messages.
   * @param localTimeZone the local time zone.
   */
  public void updateShortDateFmtZone(boolean useGMTShortFlag, String localTimeZone)
  {
    synchronized(shortDateFormatter)
    {    //only allow one thread at a time to access date-formatter object
              //setup GMT or local time zone, depending on flag:
      shortDateFormatter.setTimeZone(
            useGMTShortFlag ? UtilFns.GMT_TIME_ZONE_OBJ :
                  UtilFns.parseTimeZoneID(localTimeZone));
    }
  }

  /**
   * Formats a date into a string for a long-format message.
   * @param dateObj the date object to format.
   * @param gmtFlag true for GMT time zone; false for local time zone.
   * @return The formatted date string.
   */
  public String formatLongMsgDate(Date dateObj, boolean gmtFlag)
  {
    if(gmtFlag)
    {    //use GMT time zone
      synchronized(gmtLongDateFormatter)
      {  //only allow one thread at a time to access date-formatter object
        return gmtLongDateFormatter.format(dateObj);
      }
    }
    else
    {    //use local time zone
      synchronized(localLongDateFormatter)
      {  //only allow one thread at a time to access date-formatter object
        return localLongDateFormatter.format(dateObj);
      }
    }
  }

  /**
   * Formats a date into a string for a short-format message.
   * @param dateObj the date object to format.
   * @return The formatted date string.
   */
  public String formatShortMsgDate(Date dateObj)
  {
    synchronized(shortDateFormatter)
    {    //only allow one thread at a time to access date-formatter object
      return shortDateFormatter.format(dateObj);
    }
  }

}
