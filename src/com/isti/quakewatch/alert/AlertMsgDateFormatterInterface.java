//AlertMsgDateFormatterInterface.java:  Defines an alert message date formatter.
//
//  1/19/2006 -- [KF]  Initial version.
//

package com.isti.quakewatch.alert;

import java.util.Date;

/**
 * Interface AlertMsgDateFormatterInterface defines an alert message date formatter.
 */
public interface AlertMsgDateFormatterInterface
{
  /**
   * Formats a date into a string for a long-format message.
   * @param dateObj the date object to format.
   * @param gmtFlag true for GMT time zone; false for local time zone.
   * @return The formatted date string.
   */
  public String formatLongMsgDate(Date dateObj, boolean gmtFlag);

  /**
   * Formats a date into a string for a short-format message.
   * @param dateObj the date object to format.
   * @return The formatted date string.
   */
  public String formatShortMsgDate(Date dateObj);

  /**
   * Updates the date formatters that use the local-time-zone configuration
   * property.
   * @param useGMTShortFlag true to use GMT for date/times in short messages.
   * @param localTimeZone the local time zone.
   * @param updateShortFlag true to always update the the date formatter
   * used for short messages.
   */
  public void updateLocalTimeZone(
      boolean useGMTShortFlag, String localTimeZone, boolean updateShortFlag);

  /**
   * Updates the time zone for the date formatter used for short messages.
   * @param useGMTShortFlag true to use GMT for date/times in short messages.
   * @param localTimeZone the local time zone.
   */
  public void updateShortDateFmtZone(boolean useGMTShortFlag, String localTimeZone);
}
