//AlertRecipient.java:  Defines an abstract alert recipient.
//
//   6/8/2005 -- [KF]  Initial version.
//  8/31/2005 -- [KF]  Added heartbeat file.
//   9/6/2005 -- [KF]  Cleanup log messages.
//  1/19/2006 -- [KF]  Moved from 'QWCapOutClient' to 'QWCommon'.
//  1/26/2006 -- [KF]  Changed to update alert recipients when sending an alert.
//  1/31/2006 -- [KF]  Added alert recipient class name to warning string,
//                     Added log message for RecipientSetupException.
//   2/6/2006 -- [KF]  Added 'loadRecipients()' and 'saveRecipients()' methods.
//   2/6/2006 -- [KF]  Removed the 'Class' parameter from the
//                     'getAlertRecipientsList()' method.
//   2/8/2006 -- [KF]  Added 'isGlobalAlarmEnabled()' method.
//  2/13/2006 -- [KF]  Added 'setSendEnabled()' method.
//   3/7/2006 -- [KF]  Added more region options.
//  3/14/2006 -- [KF]  Changed to use global values if value is not specified.
//  3/16/2006 -- [KF]  Added debug logging for alert alarm checking.
//  3/17/2006 -- [KF]  Moved 'alarmCfgProps' to 'AlertRecipient'.
//  3/20/2006 -- [KF]  Added debug logging for alert alarm checking.
//  3/31/2006 -- [ET]  Changed log levels to 'debug' and 'debug2' for
//                     "checkAlarmEventRec" messages; minor variable
//                     name and cosmetic changes.
//   4/6/2006 -- [ET]  Renamed recipient-alarm group string from "Alarm"
//                     to "Alarms" (to differentiate from "Alarm" tab
//                     used elsewhere); removed 'globalAlarmProperties'
//                     parameter from call to QWAlarmProperties method
//                     'checkAlarmEvent()'.
//  4/17/2006 -- [ET]  Added 'setIstiRegionMgrObj()' method and
//                     implementation; added 'getAlarmRegionStrings()"
//                     method.
//  5/22/2006 -- [KF]  Changed the 'loadRecipients()' method to return an
//                     error message when an alert recipient is not created.
//  5/23/2006 -- [ET]  Modified to use QWEventMsgRecord method
//                     'get/setUtilityObject()' instead of
//                     'get/setDisplayObject()'; changed "global"
//                     to "audible-visible" in alarm item description;
//                     renamed 'ALARMS_GROUP_STR' from "Alarms" to
//                     "Email Alarms".
//   6/6/2011 -- [KF]  Changed "createAlertRecipient()" to log the stack trace.
//

package com.isti.quakewatch.alert;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;
import java.util.List;
import java.util.Properties;
import java.util.Vector;

import org.jdom.Element;

import com.isti.util.UtilFns;
import com.isti.util.LogFile;
import com.isti.util.ModIterator;
import com.isti.util.CfgProperties;
import com.isti.util.CfgPropItem;
import com.isti.util.DataChangedListener;
import com.isti.util.gis.IstiRegion;
import com.isti.util.gis.IstiRegionMgrIntf;
import com.isti.util.queue.NotifyEventQueue;

import com.isti.quakewatch.util.QWAlarmProperties;
import com.isti.quakewatch.util.QWProgramInformationInterface;
import com.isti.quakewatch.message.QWEventMsgRecord;

/**
 * Class AlertRecipient defines an abstract alert recipient.
 * NOTE: Every subclass must have a constructor with a
 * 'QWProgramInformationInterface' and 'Element' parameter.
 */
public abstract class AlertRecipient implements AlertRecipientInterface
{
         /** The class name text to be used in the log prefix text. */
  protected String classNameText =
      getClass().getName().substring(getClass().getName().lastIndexOf(".") + 1);
  /**
   * Gets the log prefix text.
   * @return the log prefix text.
   */
  protected String getLogPrefixText()
  {
    return classNameText + " (" + getRecipientName() + ") - ";
  }
         /** The default class name if one is not found in the properties. */
  public static String defaultClassName = null;
         /** Name string for "Recipient" XML elements. */
  public static final String RECIPIENT_NAMESTR = "Recipient";
         /** Class name for the 'AlertRecipient' class. */
  public static final String RECIPIENT_CLASSNAME = "recipientClassName";
         /** Name string for "General" group of configuration items. */
  protected static final String GENERAL_GROUP_STR = "General";
         /** Name string for "Email Alarms" group of configuration items. */
  protected static final String ALARMS_GROUP_STR = "Email Alarms";
         /** Message type value for new-event alert messages. */
  public static final int MSG_NEWEVT_TYPE = 0;
         /** Message type value for update-event alert messages. */
  public static final int MSG_UPDATE_TYPE = 1;
         /** Message type value for cancel-event alert messages. */
  public static final int MSG_CANCEL_TYPE = 2;
         /** 'QWProgramInformationInterface' object */
  protected final QWProgramInformationInterface progInfoObj;
         /** Log file object. */
  protected final LogFile logObj;
         /** Flag set true after item deleted. */
  protected boolean recipDeletedFlag = false;
         /** Array of alarm-region sync object. */
  private final Object alarmRegionsArrSyncObj = new Object();
         /** Array of alarm-region objects, or null for none. */
  private IstiRegion [] alarmRegionsArray = null;
         /** Handle for region-manager object. */
  private IstiRegionMgrIntf istiRegionMgrObj = null;

         /** Configuration properties object. */
  public final CfgProperties cfgProps = new CfgProperties();
         /** Alert recipient class name. */
  public final CfgPropItem alertRecipientClassNameProp = cfgProps.add(
                              RECIPIENT_CLASSNAME,getClass().getName(),null,
                                              "Alert recipient class name");
         /** Name of recipient. */
  public final CfgPropItem recipientNameProp = cfgProps.add(
                                  "recipientName",UtilFns.EMPTY_STRING,null,
                        "Recipient name").setGroupSelObj(GENERAL_GROUP_STR);
         /** Flag set true to enable sending of emails to recipient
          *  when global alarm is triggered. */
  public final CfgPropItem globalAlarmEnabledFlagProp;
         /** Set of configuration property items for alarm settings. */
  protected final QWAlarmProperties alarmCfgProps =
                           new QWAlarmProperties(cfgProps,ALARMS_GROUP_STR);

  /**
   * Creates an new recipient, with fields filled via the given
   * "Recipient" element object.
   * @param progInfoObj the 'QWProgramInformationInterface' object.
   * @param recipientElementObj the recipient object to use.
   * @throws RecipientSetupException if an error occurs while loading
   * fields from the element object.
   */
  public AlertRecipient(
      QWProgramInformationInterface progInfoObj,Element recipientElementObj)
      throws RecipientSetupException
  {
    this(progInfoObj,progInfoObj.getProgramLogFileObj());
  }

  /**
   * Creates an "empty" recipient, with all fields set to default values.
   * @param progInfoObj the 'QWProgramInformationInterface' object.
   */
  public AlertRecipient(QWProgramInformationInterface progInfoObj)
  {
    this(progInfoObj,progInfoObj.getProgramLogFileObj());
  }

  /**
   * Creates an new recipient, with all fields set equal to those in the
   * given recipient.
   * @param sourceRecipientObj the recipient object to use.
   * @throws RecipientSetupException if an error occurs.
   */
  public AlertRecipient(AlertRecipient sourceRecipientObj)
                                              throws RecipientSetupException
  {
    this(sourceRecipientObj.progInfoObj,sourceRecipientObj.logObj);
    final Element elemObj;
    if((elemObj=sourceRecipientObj.toElement()) == null)
    {
      throw new RecipientSetupException(
          "Unable to load fields from source recipient",logObj);
    }
    loadFromElement(elemObj);
  }

  /**
   * Creates an "empty" recipient, with all fields set to default values.
   * @param progInfoObj the 'QWProgramInformationInterface' object.
   * @param logObj the log file object.
   */
  private AlertRecipient(
                   QWProgramInformationInterface progInfoObj,LogFile logObj)
  {
    this.progInfoObj = progInfoObj;
    this.logObj = logObj;
    globalAlarmEnabledFlagProp = setupGlobalAlarmEnabledFlagProp(progInfoObj);
    alarmCfgProps.regionsAlarmProp.addDataChangedListener(
      new DataChangedListener()
        {
          /**
           * Called when data has changed.
           * @param sourceObj source object that called this method.
           */
          public void dataChanged(Object sourceObj)
          {
            IstiRegion [] regionsArr;
            if ( (regionsArr = IstiRegion.parseRegions(
                alarmCfgProps.regionsAlarmProp.stringValue())).length <= 0)
            { //no items in array
              regionsArr = null; //indicate no regions
            }
            synchronized (alarmRegionsArrSyncObj)
            {
              alarmRegionsArray = regionsArr;
            }
            updateIstiRegionMgrEntry();     //update region mgr entry
          }
        });
  }

  /**
   * Sets up the global-alarm-enabled-flag-property object.
   * @param progInfoObj QWProgramInformationInterface
   * @return the global-alarm-enabled-flag-property object, or null if none.
   */
  private CfgPropItem setupGlobalAlarmEnabledFlagProp(
                                  QWProgramInformationInterface progInfoObj)
  {
    if (!progInfoObj.isFunctionalitySupported(
               QWProgramInformationInterface.GLOBAL_ALARMS_SUPPORTED_VALUE))
    {
      return null;
    }
    return cfgProps.add("globalAlarmEnabledFlag",Boolean.FALSE,null,
               "Email this recipient when audible-visible alarm triggered").
                                          setGroupSelObj(GENERAL_GROUP_STR);
  }

  /**
   * Sets the region-manager object to be used by this recipient.
   * @param istiRegionMgrObj region-manger object to use.
   */
  public void setIstiRegionMgrObj(IstiRegionMgrIntf istiRegionMgrObj)
  {
    this.istiRegionMgrObj = istiRegionMgrObj;
  }

  /**
   * Updates the entry for this recipient in the regions manager.
   */
  private void updateIstiRegionMgrEntry()
  {
    if(istiRegionMgrObj != null)
    {
      final IstiRegion [] regionsArr;
      synchronized (alarmRegionsArrSyncObj)
      {
        regionsArr = alarmRegionsArray;
      }
      istiRegionMgrObj.updateAuxRegionsEntry(this,regionsArr);
    }
  }

  /**
   * Uses the constructor with the specified arguments to
   * create and initialize a new instance of specified class.
   *
   * <p>If the required access and argument checks succeed and the
   * instantiation will proceed, the constructor's declaring class
   * is initialized if it has not already been initialized.
   *
   * <p>If the constructor completes normally, returns the newly
   * created and initialized instance.
   *
   * @param classObj the class to instantiate.
   * @param parameterTypes the parameter array.
   * @param initargs array of objects to be passed as arguments to
   * the constructor call; values of primitive types are wrapped in
   * a wrapper object of the appropriate type (e.g. a <tt>float</tt>
   * in a {@link java.lang.Float Float})
   *
   * @return a new object created by calling the constructor
   * for the specified class.
   *
   * @exception NoSuchMethodException if a matching constructor is not found.
   * @exception SecurityException     if access to the information is denied.
   * @exception IllegalAccessException    if this <code>Constructor</code>
   *              object enforces Java language access control and the
   *              underlying constructor is inaccessible.
   * @exception IllegalArgumentException  if the number of actual
   *              and formal parameters differ; if an unwrapping
   *              conversion for primitive arguments fails; or if,
   *              after possible unwrapping, a parameter value
   *              cannot be converted to the corresponding formal
   *              parameter type by a method invocation conversion.
   * @exception InstantiationException    if the class that declares the
   *              underlying constructor represents an abstract class.
   * @exception InvocationTargetException if the underlying constructor
   *              throws an exception.
   * @exception ExceptionInInitializerError if the initialization provoked
   *              by this method fails.
   */
  public static Object newInstance(
      Class classObj, Class[] parameterTypes, Object[] initargs)
	throws NoSuchMethodException, SecurityException,
    InstantiationException, IllegalAccessException,
    IllegalArgumentException, InvocationTargetException
  {
    final Constructor c = classObj.getConstructor(parameterTypes);
    return c.newInstance(initargs);
  }

  /**
   * Creates an new recipient, with fields filled via the given
   * "Recipient" element object.
   * @param progInfoObj the 'QWProgramInformationInterface' object.
   * @param recipientElementObj the recipient object to use.
   * @return the new recipient or null if error.
   * @throws RecipientSetupException if an error occurs while loading
   * fields from the element object.
   */
  public static AlertRecipient createAlertRecipient(
      QWProgramInformationInterface progInfoObj, Element recipientElementObj)
      throws RecipientSetupException
  {
    String alertRecipientClassName = null;
    final LogFile logObj = progInfoObj.getProgramLogFileObj();
    try
    {
      //get the alertRecipient class name from the properties
      final Properties props = new Properties();
      props.load(new ByteArrayInputStream(
                                 recipientElementObj.getText().getBytes()));
      alertRecipientClassName = UtilFns.removeQuoteChars(
          props.getProperty(RECIPIENT_CLASSNAME),true);
      //if alertRecipient class name not found, use default (if available)
      if (alertRecipientClassName == null && defaultClassName != null)
        alertRecipientClassName = defaultClassName;
      //if no class name
      if (alertRecipientClassName == null)
      {
        logObj.warning(
          "AlertRecipient.createAlertRecipient: could not determine class name: " +
          recipientElementObj);
        return null;
      }
      //make a new instance of the the AlertRecipient
      final Class alertRecipientClass = Class.forName(alertRecipientClassName);
      final Class[] parameterTypes = new Class[] {
          QWProgramInformationInterface.class, Element.class};
      final Object[] initargs = new Object[] {
          progInfoObj, recipientElementObj};
      final Object arObj = newInstance(
      alertRecipientClass, parameterTypes, initargs);
      if (arObj instanceof AlertRecipient)
      {
        return (AlertRecipient)arObj;
      }
      logObj.warning(
          "AlertRecipient.createAlertRecipient: wrong class type: " +
          alertRecipientClassName);
    }
    catch (Exception ex)
    {
      logObj.warning(
          "AlertRecipient.createAlertRecipient: exception (" +
          alertRecipientClassName + ")\n" +
          UtilFns.getStackTraceString(ex));
    }
    return null;
  }

  /**
   * Get of configuration property items for alarm settings.
   * @return the configuration property items for alarm settings.
   */
  public final QWAlarmProperties getAlarmCfgProps()
  {
    return alarmCfgProps;
  }

  /**
   * Returns the alert-message-queue object for this recipient.
   * @return The alert-message-queue object for this recipient.
   */
  public abstract NotifyEventQueue getAlertMessageQueue();

  /**
   * Returns the status of whether a message should be sent when a global alarm
   * is triggered for this recipient.
   * @return true if sending of emails to recipient when global alarm is
   * triggered is enabled, false otherwise.
   */
  public boolean isGlobalAlarmEnabled()
  {
    return globalAlarmEnabledFlagProp != null &&
        globalAlarmEnabledFlagProp.booleanValue();
  }

  /**
   * Returns the status of whether or not sending is enabled for this
   * recipient.
   * @return true if sending is enabled; false if not.
   */
  public abstract boolean isSendEnabled();

  /**
   * Sets the status of whether a message should be sent when a global alarm
   * is triggered for this recipient.
   * @param b true if sending of emails to recipient when global alarm is
   * triggered is enabled, false otherwise.
   */
  public void setGlobalAlarmEnabled(boolean b)
  {
    if (globalAlarmEnabledFlagProp != null)
    {
      globalAlarmEnabledFlagProp.setValue(b);
    }
  }

  /**
   * Sets the status of whether or not sending is enabled for this
   * recipient.
   * @param b true if sending is enabled; false if not.
   */
  public abstract void setSendEnabled(boolean b);

  /**
   * Converts this recipient's fields into entries within an XML element.
   * @return A new 'Element' object containing the entries, or null if
   * an error occurred.
   */
  public Element toElement()
  {
         //create byte array stream to receive properties data:
    final ByteArrayOutputStream stmObj = new ByteArrayOutputStream();
    try       //store properties data in byte array stream
    {         // (save all props, no header, not cmdln-only):
      cfgProps.store(stmObj,null,false,false,false);
    }
    catch (IOException ioException)
    {
      return null;
    }
         //create a "Recipient" element:
    final Element settingsElementObj = new Element(RECIPIENT_NAMESTR);
         //set text to settings properties (insert leading newline):
    settingsElementObj.setText(UtilFns.newline + stmObj.toString());
    return settingsElementObj;
  }

  /**
   * Loads this recipient's fields from the given "Recipient" element
   * object.
   * @param recipientElementObj the recipient object to use.
   * @throws RecipientSetupException if an error occurs while loading
   * fields from the element object.
   */
  public void loadFromElement(Element recipientElementObj)
                                              throws RecipientSetupException
  {
    String msgStr;
    try
    {         //load configuration properties from the given string:
      if(cfgProps.load(recipientElementObj.getText()))
      {  //successfully loaded settings
        return;
      }
         //error loading settings; get error message
      msgStr = cfgProps.getErrorMessage();
    }
    catch(Exception ex)
    {    //some kind of exception error; build error message
      msgStr = "Error loading settings:  " + ex;
    }
         //throw feeder exception with error message:
    throw new RecipientSetupException(msgStr,logObj);
  }

    /**
     * Loads the recipients specified by the "Recipient"
     * elements in the configuration file.
     * @param progInfoObj the 'QWProgramInformationInterface' object.
     * @param configRootElement the config root element object.
     * @param alertRecipientsListObj the alert recipients list object.
     * @return an error message if an error occurred or null if successful.
     * @param istiRegionMgrObj region manager object to use, or null for
     * none.
     */
  public static String loadRecipients(
      QWProgramInformationInterface progInfoObj,Element configRootElement,
      List alertRecipientsListObj,IstiRegionMgrIntf istiRegionMgrObj)
  {
    String errStr = null;

    if (alertRecipientsListObj == null)
      return errStr;
    try  //process recipient-configuration elements:
    {
         //fetch root element and process recipient child elements:
      final Vector recipsVec = new Vector();
      if(configRootElement != null)
      {       //root element fetched OK
        final List recipElementsList =   //get list of recipient child elems
            configRootElement.getChildren(RECIPIENT_NAMESTR);
              //for each recipient child element, create 'AlertRecipient'
              // object from element and add to local Vector:
        AlertRecipient alertObj;
        //setup to iterate through recipent objects:
        final ModIterator iterObj = new ModIterator(recipElementsList,false);
        while(iterObj.hasNext())
        {
          try
          {
            alertObj =
                createAlertRecipient(progInfoObj,(Element)(iterObj.next()));
            if (alertObj != null)
            {
              if (istiRegionMgrObj != null) //if given then set region mgr
              { //region manager object was given; enter into recipient
                alertObj.setIstiRegionMgrObj(istiRegionMgrObj);
                alertObj.updateIstiRegionMgrEntry(); //enter regions
              }
              recipsVec.add(alertObj);
            }
            else
            {
              if (errStr == null)
                errStr = UtilFns.EMPTY_STRING;
              else
                errStr += "\n";
              errStr += "Error creating alert recipient";
            }
          }
          catch(Exception ex)
          {
            if (errStr == null)
              errStr = UtilFns.EMPTY_STRING;
            else
              errStr += "\n";
            errStr += "Error processing program configuration: " + ex;
          }
        }
      }
      alertRecipientsListObj.clear();  //clear any current recipient entries
                                       //enter new list of recipient objects:
      alertRecipientsListObj.addAll(recipsVec);
    }
    catch(Exception ex)
    {         //some kind of exception error; show and log message
      errStr = "Error processing program configuration: " + ex;
      LogFile logObj = progInfoObj.getProgramLogFileObj();
      logObj.warning(errStr);
      logObj.warning(UtilFns.getStackTraceString(ex));
    }
    return errStr;
  }

    /**
     * Loads the recipients specified by the "Recipient"
     * elements in the configuration file.
     * @param progInfoObj the 'QWProgramInformationInterface' object.
     * @param configRootElement the config root element object.
     * @param alertRecipientsListObj the alert recipients list object.
     * @return an error message if an error occurred or null if successful.
     */
  public static String loadRecipients(
      QWProgramInformationInterface progInfoObj,Element configRootElement,
      List alertRecipientsListObj)
  {
    return loadRecipients(progInfoObj,configRootElement,
                                               alertRecipientsListObj,null);
  }

    /**
     * Saves the recipients specified to "Recipient"
     * elements in the configuration file.
     * @param progInfoObj the 'QWProgramInformationInterface' object.
     * @param configRootElement the config root element object.
     * @param alertRecipientsListObj the alert recipients list object.
     */
  public static void saveRecipients(
      QWProgramInformationInterface progInfoObj,Element configRootElement,
      List alertRecipientsListObj)
  {
    if (alertRecipientsListObj == null)
      return;
    //setup to iterate through recipent objects:
    final ModIterator iterObj = new ModIterator(alertRecipientsListObj);
    while(iterObj.hasNext())
    {  //for each recipient; add recipient element to root element
      configRootElement.addContent(
          ((AlertRecipient)(iterObj.next())).toElement());
    }
  }

  /**
   * Sets the name value for this recipient.
   * @param nameStr the name value string to use.
   */
  public void setRecipientName(String nameStr)
  {
    recipientNameProp.setValue(
                        (nameStr != null) ? nameStr : UtilFns.EMPTY_STRING);
  }

  /**
   * Returns the name value for this recipient.
   * @returns The name value for this recipient.
   */
  public String getRecipientName()
  {
    return recipientNameProp.stringValue();
  }

  /**
   * Terminates the processing thread for the queue for transmitting
   * alert messages.
   */
  public abstract void terminateMsgQueueThread();

  /**
   * Returns a string representation of this recipient.
   * @return a string representation of this recipient.
   */
  public abstract String toString();

  /**
   * Sets the recipient-deleted flag to indicate that this recipient
   * has been removed.  This method is needed because the recipient
   * object may still be referenced in the list of alarm recipients
   * held by an event-message-record object.
   */
  public void setRecipDeletedFlag()
  {
    recipDeletedFlag = true;
  }

  /**
   * Gets the number of regions.
   * @return the number of regions.
   */
  public int getNumRegions()
  {
    final IstiRegion [] regionsArr;
    synchronized (alarmRegionsArrSyncObj)
    {
      regionsArr = alarmRegionsArray;
    }
    final int numRegions =        //get # of alarm regions for recipient
        (regionsArr != null) ? regionsArr.length : 0;
    return numRegions;
  }

  /**
   * Returns the alarm regions for this recipient, in string format.
   * @return A new array containing the alarm-region-text strings
   * for this recipient, or null if no regions exist for this recipient.
   */
  public String [] getAlarmRegionStrings()
  {
    final IstiRegion [] regionsArr;
    synchronized (alarmRegionsArrSyncObj)
    {
      regionsArr = alarmRegionsArray;
    }
    final int numRegions;         //get # of alarm regions for recipient
    if(regionsArr == null || (numRegions=regionsArr.length) <= 0)
      return null;           //if no regions then return null
    final String [] regionStrsArr = new String[numRegions];
    for(int i=0; i<numRegions; ++i)
    {    //for each region object; put string version into array
      regionStrsArr[i] = (regionsArr[i] != null) ?
                            regionsArr[i].toString() : UtilFns.EMPTY_STRING;
    }
    return regionStrsArr;         //return array of region strings
  }

  /**
   * Returns the recipient-deleted flag (which indicates that this
   * recipient has been removed).  This flag is needed because the
   * recipient object may still be referenced in the list of alarm
   * recipients held by an event-message-record object.
   * @return true if this recipient has been deleted.
   */
  public boolean getRecipDeletedFlag()
  {
    return recipDeletedFlag;
  }

  /**
   * Determines if the given event-message record would trigger an
   * alarm condition.
   * @param recObj the message-record object for the event.
   * @return true if the event would trigger an alarm; false if not.
   */
  public boolean checkAlarmEventRec(QWEventMsgRecord recObj)
  {
    final IstiRegion [] regionsArr;
    synchronized (alarmRegionsArrSyncObj)
    {
      regionsArr = alarmRegionsArray;
    }
    StringBuffer alarmReason = new StringBuffer();
    final boolean retFlag = alarmCfgProps.checkAlarmEvent(recObj,regionsArr,
                                alarmReason) != QWAlarmProperties.NO_ALARMS;
    final boolean requestedFlag = recObj.getRequestedFlag();
         //if event triggers alarm and not requested then
         // setup log level "debug"; otherwise "debug2":
    final int level = (retFlag && !requestedFlag) ? LogFile.DEBUG :
                                                             LogFile.DEBUG2;
    logObj.println(level,"checkAlarmEventRec(" + recObj.getEventIDKey() +
                  (requestedFlag?" Requested":UtilFns.EMPTY_STRING) + "  " +
                           recObj.toString().trim() + "):  " + alarmReason);
    return retFlag;
  }

  /**
   * Generates an alert message for the given event-message record.
   * @param messageType message type value for message.
   * @param recObj the message-record object for the event.
   */
  protected abstract void sendAlertMessage(QWEventMsgRecord recObj,
                                                           int messageType);

  /**
   * Generates an alert message for the given event-message record.
   * @param messageType message type value for message.
   * @param recObj the message-record object for the event.
   */
  protected void doSendAlertMessage(QWEventMsgRecord recObj, int messageType)
  {
    if (!recObj.getRequestedFlag())
    {
      sendAlertMessage(recObj, messageType);
      if (progInfoObj != null)
      {
        final List alertRecipientsList =
            progInfoObj.getAlertRecipientsList();
        if (alertRecipientsList instanceof AlertRecipientsList)
        {
          ((AlertRecipientsList)alertRecipientsList).updateAlertRecipients();
        }
      }
    }
  }

  /**
   * Generates an "new" alert message for the given event-message
   * record.
   * @param recObj the message-record object for the event.
   */
  public void sendNewAlertMessage(QWEventMsgRecord recObj)
  {
    doSendAlertMessage(recObj,MSG_NEWEVT_TYPE);
  }

  /**
   * Generates an "update" alert message for the given event-message
   * record.
   * @param recObj the message-record object for the event.
   */
  public void sendUpdateAlertMessage(QWEventMsgRecord recObj)
  {
    doSendAlertMessage(recObj,MSG_UPDATE_TYPE);
  }

  /**
   * Responds to the given deleted event-message record by generating and
   * send a "cancelled" alert message.
   * @param recObj the message-record object for the deleted event.
   */
  public void sendCancelAlertMessage(QWEventMsgRecord recObj)
  {
    doSendAlertMessage(recObj,MSG_CANCEL_TYPE);
  }

  /**
   * Adds this 'AlertRecipient' object to the list of
   * 'AlertRecipient' objects for which the given event has triggered
   * one or more alarm-alerts.
   * @param recObj the message-record object for the given event.
   */
  public void addToEventRecAlarmRecipList(QWEventMsgRecord recObj)
  {
         //attempt to fetch alarm-recipients list attached to
         // event-message record (as its "utility object");
         // if not found then create new Vector and attach it
         // to the event-message record as its "utility object":
    List listObj;
    if((listObj=getEventRecAlarmRecipList(recObj)) == null)
      recObj.setUtilityObject(listObj = new Vector());
    listObj.add(this);       //add this recipient to list
  }

  /**
   * Fetches the 'List' object containing the list of 'AlertRecipient'
   * objects for which the given event has triggered one or more
   * alarm-alerts.
   * @param recObj the message-record object for the given event.
   * @return The 'List' object, or null if none exists for the given
   * event-message record.
   */
  public static List getEventRecAlarmRecipList(QWEventMsgRecord recObj)
  {
         //attempt to fetch alarm-recipients list attached to
         // event-message record as its "utility object":
    final Object obj;
    if((obj=recObj.getUtilityObject()) instanceof List)
      return (List)obj;      //if list object found then return it
    return null;             //if not found then return null
  }

  /**
   * Fetches the 'List' object containing the list of 'AlertRecipient'
   * objects for which the given event has triggered one or more
   * alarm-alerts and removes and returns the first
   * 'AlertRecipient' item in the list.
   * @param recObj the message-record object for the given event.
   * @return The removed 'AlertRecipient' object, or null if none
   * exist for the given event-message record.
   */
  public static AlertRecipient popFirstEventRecAlarmRecip(
                                                    QWEventMsgRecord recObj)
  {
         //attempt to fetch alarm-recipients list attached to
         // event-message record (as its "utility object"):
    final List listObj;
    if((listObj=getEventRecAlarmRecipList(recObj)) == null)
      return null;      //if list not found then return null
    final Object obj;
    synchronized(listObj)
    {    //grab thread-sync for list object
      if(listObj.size() > 0)
      {  //list is not empty
        if((obj=listObj.remove(0)) instanceof AlertRecipient)
        {     //removed first object is of type 'AlertRecipient'
          if(listObj.size() <= 0)           //if list now empty then
            recObj.setUtilityObject(null);  //clear "utility object" handle
          return (AlertRecipient)obj;  //return popped object
        }
      }
      else    //list is empty
        recObj.setUtilityObject(null);      //clear "utility object" handle
      return null;
    }
  }

  /**
   * Clears the list of 'AlertRecipient' objects for which the given
   * event has triggered one or more alarm-alerts.  The
   * "utility object" for the given event is set to null.
   * @param recObj the message-record object for the given event.
   */
  public static void clearEventRecAlarmRecipList(QWEventMsgRecord recObj)
  {
    recObj.setUtilityObject(null);
  }

  /**
   * Class RecipientSetupException defines an exception thrown while
   * setting up a recipient.
   */
  public static class RecipientSetupException extends Exception
  {
    /**
     * Creates a recipient-initialization exception.
     * @param msgStr the information message string to use.
     * @param logObj the 'LogFile' object or null if none.
     */
    public RecipientSetupException(String msgStr,LogFile logObj)
    {
      super(msgStr);
      if (logObj != null)
        logObj.error(getClass().getName() + " - " + msgStr);
    }
  }
}
