//AlertRecipientInterface.java:  Defines an alert recipient.
//
//  6/22/2005 -- [KF]  Initial version.
//  1/19/2006 -- [KF]  Moved from 'QWCapOutClient' to 'QWCommon'.
//   2/8/2006 -- [KF]  Added 'isGlobalAlarmEnabled()' method.
//

package com.isti.quakewatch.alert;

import com.isti.util.queue.NotifyEventQueue;

/**
 * Interface AlertRecipientInterface defines an alert recipient.
 */
public interface AlertRecipientInterface
{
  /**
   * Returns the alert-message-queue object for this recipient.
   * @return The alert-message-queue object for this recipient.
   */
  public NotifyEventQueue getAlertMessageQueue();

  /**
   * Returns an ID string for this recipient.
   * @return An ID string for this recipient.
   */
  public String getRecipID();

  /**
   * Returns the status of whether a message should be sent when a global alarm
   * is triggered for this recipient.
   * @return true if sending of emails to recipientwhen global alarm is
   * triggered is enabled, false otherwise.
   */
  public boolean isGlobalAlarmEnabled();
}
