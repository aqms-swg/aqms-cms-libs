//AlertRecipientsList.java:  Defines an alert recipients list.
//
//  1/19/2006 -- [KF]  Initial version.
//   2/1/2006 -- [KF]  Changed 'setupGUI()' method to return a recipient panel.
//   2/8/2006 -- [KF]  Added 'sendNewAlertMessages()' method, removed
//                     'AlertEmailSenderInterface' parameter from constructor.
//  2/22/2006 -- [KF]  Added "Enable send" and "Enable global" buttons.
//   3/9/2006 -- [KF]  Fixed problems with list not being displayed.
//  3/20/2006 -- [KF]  Changed to ensure list is not empty on double click;
//                     changed to ensure changes to the JList are done on the
//                     event dispatch thread; fixed problems with list not
//                     always being displayed.
//  3/29/2006 -- [ET]  Modified methods 'doRemoveSelectedRecipients()',
//                     'showConfigDialog()' and 'showRegionsConfigDialog()'
//                     to make parent of popup be same as parent of JList;
//                     removed "Select All", "Enable" and "Disable" buttons
//                     and related combo box; renamed 'sendNewAlertMessages()'
//                     method to 'sendGlobalAlarmAlerts()'; added method
//                     'getAnyEnabledRecipsFlag()'.
//   4/7/2006 -- [ET]  Added optional 'tearOffRegionsDlgFlag' parameter to
//                     constructor; added 'getRecipientGUIPanel()' method.
//  4/17/2006 -- [ET]  Added optional 'istiRegionMgrObj' parameter to
//                     constructor; added 'getIstiRegionMgrObj()' method.
//  5/23/2006 -- [ET]  Added "recipObj.setIstiRegionMgrObj()" call to
//                     'doAddNewRecipient()' method; modified method
//                     'sendGlobalAlarmAlerts()' to check if event will
//                     already trigger alarm-alert for recipient (to
//                     prevent two alerts from begin triggered); changed
//                     label above GUI list from "Alert Recipients:" to
//                     "Email Alert Recipients:"; added "email" to "Regions"
//                     button tool-tip text.
//

package com.isti.quakewatch.alert;

import java.awt.BorderLayout;
import java.awt.Font;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.Component;
import java.awt.Window;
import java.awt.Frame;
import java.awt.Dialog;
import java.awt.event.ActionEvent;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import javax.swing.AbstractAction;
import javax.swing.BorderFactory;
import javax.swing.Box;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JPanel;
import javax.swing.SwingUtilities;
import javax.swing.JScrollPane;
import javax.swing.border.BevelBorder;
import java.util.*;

import com.isti.util.LogFile;
import com.isti.util.ModIterator;
import com.isti.util.UtilFns;
import com.isti.util.gui.IstiDialogUtil;
import com.isti.util.gui.GuiUtilFns;
import com.isti.util.gui.IstiDialogPopup;
import com.isti.util.gis.IstiRegionMgrIntf;
import com.isti.util.propertyeditor.CfgSettingsDialog;

import com.isti.quakewatch.alertemail.AlertEmailRecipient;
import com.isti.quakewatch.alertgui.AlarmRegionConfigDialog;
import com.isti.quakewatch.alertgui.AlertRecipientConfigDialog;
import com.isti.quakewatch.guiutil.ConnectionStatusPanel;
import com.isti.quakewatch.message.QWEventMsgRecord;
import com.isti.quakewatch.util.QWProgramInformationInterface;

/**
 * Class AlertRecipientsList defines an alert recipients list.
 */
public class AlertRecipientsList implements List
{
  protected final List recipientsListObj = new Vector();
    /** GUI list holding entries for recipients. */
  protected final JList recipientsJListObj = new JList();

  //the program information
  private final QWProgramInformationInterface progInfo;
  private final CfgSettingsDialog cfgSettingsDialog;
  private final boolean tearOffRegionsDlgFlag;
  private final IstiRegionMgrIntf istiRegionMgrObj;

    /** Panel setup via 'setGUI()'. */
  protected JPanel recipientGUIPanel = null;
    /** Flag set true while inside 'doRemoveSelectedRecipients()' method. */
  protected boolean inRemoveSelRecipsMethodFlag = false;
  /** Flag set true if recipient-edit via double-click on list is enabled. */
  protected boolean mouseClickEditFlag = true;


  /** Action object for the "Add" button (and menu item). */
  public final AbstractAction addButtonActionObj = new AbstractAction("Add")
      {
        public void actionPerformed(ActionEvent evtObj)
        {
          doAddNewRecipient();
        }
      };
  protected JButton addButtonObj = null;

  /** Action object for the "Edit" button (and menu item). */
  public final AbstractAction editButtonActionObj =
                                                  new AbstractAction("Edit")
      {
        public void actionPerformed(ActionEvent evtObj)
        {
          doEditSelectedRecipient();
        }
      };
  protected JButton editButtonObj = null;

//  /** Action object for the "Select All" button (and menu item). */
//  public final AbstractAction selectAllButtonActionObj =
//                                              new AbstractAction("Select All")
//      {
//        public void actionPerformed(ActionEvent evtObj)
//        {
//          doSelectAllRecipients();
//        }
//      };

  /** Action object for the "Regions" button (and menu item). */
  public final AbstractAction regionsButtonActionObj =
                                               new AbstractAction("Regions")
      {
        public void actionPerformed(ActionEvent evtObj)
        {
          doAlarmRegionsForRecipient();
        }
      };
  protected JButton regionsButtonObj = null;

  /** Action object for the "Remove" button (and menu item). */
  public final AbstractAction removeButtonActionObj =
                                                new AbstractAction("Remove")
      {
        public void actionPerformed(ActionEvent evtObj)
        {
          doRemoveSelectedRecipients();
        }
      };
  protected JButton removeButtonObj = null;


  /**
   * Creates an alert recipients list.
   * @param progInfo the program information.
   * @param cfgSettingsDialog the config settings dialog.
   * @param tearOffRegionsDlgFlag true to "tear off" the regions dialog
   * (and close the settings dialog while the regions are being edited);
   * false to use the parent dialog.
   * @param istiRegionMgrObj region manager object to use, or null for
   * none.
   */
  public AlertRecipientsList(QWProgramInformationInterface progInfo,
         CfgSettingsDialog cfgSettingsDialog, boolean tearOffRegionsDlgFlag,
                                         IstiRegionMgrIntf istiRegionMgrObj)
  {
    this.progInfo = progInfo;
    this.cfgSettingsDialog = cfgSettingsDialog;
    this.tearOffRegionsDlgFlag = tearOffRegionsDlgFlag;
    this.istiRegionMgrObj = istiRegionMgrObj;

         //setup data model for GUI list of recipients:
    recipientsJListObj.setListData(recipientsListObj.toArray());
         //setup mouse listener to do "Edit" in response to double-click:
    recipientsJListObj.addMouseListener(new MouseAdapter()
        {
          public void mouseClicked(MouseEvent evtObj)
          {
            try
            {
              if(mouseClickEditFlag && evtObj.getClickCount() >= 2)
              {    //edit enabled and mouse clicked at least twice
                final int idx =        //get index for mouse-click position
                      recipientsJListObj.locationToIndex(evtObj.getPoint());
                final Object obj;      //get recipient for index; do edit:
                if(idx >= 0 &&
                   (obj=recipientsJListObj.getModel().getElementAt(idx))
                   instanceof AlertRecipient)
                {
                  showConfigDialog((AlertRecipient)obj); //edit recipient
                }
              }
            }
            catch(Exception ex)
            {
              final LogFile logObj =
                  AlertRecipientsList.this.progInfo.getProgramLogFileObj();
              if(logObj != null)
              {
                logObj.warning("Error processing mouse click on " +
                                                 "recipients list:  " + ex);
                logObj.warning(UtilFns.getStackTraceString(ex));
              }
            }
          }
        });
  }

  /**
   * Creates an alert recipients list.
   * @param progInfo the program information.
   * @param cfgSettingsDialog the config settings dialog.
   * @param tearOffRegionsDlgFlag true to "tear off" the regions dialog
   * (and close the settings dialog while the regions are being edited);
   * false to use the parent dialog.
   */
  public AlertRecipientsList(QWProgramInformationInterface progInfo,
         CfgSettingsDialog cfgSettingsDialog, boolean tearOffRegionsDlgFlag)
  {
    this(progInfo,cfgSettingsDialog,tearOffRegionsDlgFlag,null);
  }

  /**
   * Creates an alert recipients list.
   * @param progInfo the program information.
   * @param cfgSettingsDialog the config settings dialog.
   */
  public AlertRecipientsList(QWProgramInformationInterface progInfo,
                                        CfgSettingsDialog cfgSettingsDialog)
  {
    this(progInfo,cfgSettingsDialog,false,null);
  }

  /**
   * Commits changes to the given recipient.  The GUI list of recipients
   * and the configuration file are updated.  If the recipient is not on
   * the list then it is added.
   * @param recipObj the recipient object to use.
   */
  public void commitRecipient(final AlertRecipient recipObj)
  {
    if (SwingUtilities.isEventDispatchThread())
      //current thread is the event dispatch thread
      doCommitRecipient(recipObj);
    else
      SwingUtilities.invokeLater(new Runnable()
      { //do this after other Swing work is complete
        public void run()
        {
          doCommitRecipient(recipObj);
        }
      });
  }

  /**
   * Commits changes to the given recipient.  The GUI list of recipients
   * and the configuration file are updated.  If the recipient is not on
   * the list then it is added.
   * @param recipObj the recipient object to use.
   * NOTE: This method should only be called from the event dispatch thread.
   */
  private void doCommitRecipient(final AlertRecipient recipObj)
  {
         //if recipient not in list then add it in:
    if(!contains(recipObj))
      recipientsListObj.add(recipObj);
    doUpdateListModel();  //update the list model
                                                 //select committed entry:
    doSelectAndShowListRecipient(indexOf(recipObj));
    doUpdateAlertRecipients();    //update displayed list of recipients
    progInfo.saveConfiguration(true,false);  //save configuration to file
  }

  /**
   * Called when a MenuItem event happens.
   * @param nameStr the name of the menu item.
   * @return true if the MenuItem event is for a recipient, false otherwise.
   * NOTE: This method should only be called from the event dispatch thread.
   */
  public boolean processMenuItemEvent(String nameStr)
  {
    if(nameStr.equals("AddRecip"))
      addButtonActionObj.actionPerformed(null);
    else if(nameStr.equals("EditRecip"))
      editButtonActionObj.actionPerformed(null);
    else if(nameStr.equals("RegionsRecip"))
      regionsButtonActionObj.actionPerformed(null);
    else if(nameStr.equals("RemoveRecip"))
      removeButtonActionObj.actionPerformed(null);
    else
      return false;
    return true;
  }

  /**
   * Sends new alert messages to those recipients that have
   * alert-on-global-alarm enabled.  If an alarm-alert would
   * already be triggered for the recipient then one is not
   * triggered by this method.
   * @param eventRecObj message record for event.
   */
  public void sendGlobalAlarmAlerts(QWEventMsgRecord eventRecObj)
  {
    //setup iterator for alert-recipient objects:
    final ModIterator iterObj = new ModIterator(this);
    AlertRecipient recipObj;
    while (iterObj.hasNext())
    { //for each alert-recipient object; process the event
      recipObj = (AlertRecipient) (iterObj.next());
      if (recipObj != null && recipObj.isSendEnabled() &&
                                          recipObj.isGlobalAlarmEnabled() &&
                                  !recipObj.checkAlarmEventRec(eventRecObj))
      {  //recipient send enabled, global alarm enabled
         // and not already going to send alert
        recipObj.sendNewAlertMessage(eventRecObj);
      }
    }
  }

  /**
   * Sets up the GUI.
   * @param connStatusPanel the 'ConnectionStatusPanel' or null for none.
   * @return the recipient panel.
   */
  public JPanel setupGUI(ConnectionStatusPanel connStatusPanel)
  {
         //setup label above GUI list of recipients:
    final JLabel recipientsLabelObj = new JLabel("Email Alert Recipients:");
    final Font fontObj = recipientsLabelObj.getFont();
    recipientsLabelObj.setFont(        //increase font size:
                   fontObj.deriveFont(Font.PLAIN,fontObj.getSize2D()+1.0f));
         //put empty border around label:
    recipientsLabelObj.setBorder(BorderFactory.createEmptyBorder(5,10,2,5));

         //put GUI list of recipients into a scroll pane:
    final JScrollPane listScrollPane = new JScrollPane(recipientsJListObj,
                                   JScrollPane.VERTICAL_SCROLLBAR_AS_NEEDED,
                                JScrollPane.HORIZONTAL_SCROLLBAR_AS_NEEDED);
         //put scroll pane into a panel:
    final JPanel scrollPanePanel = new JPanel(new BorderLayout());
    scrollPanePanel.add(listScrollPane,BorderLayout.CENTER);
         //put empty border around scroll-pane panel:
    scrollPanePanel.setBorder(BorderFactory.createEmptyBorder(2,10,15,5));
    recipientGUIPanel = new JPanel(new BorderLayout());
    recipientGUIPanel.add(recipientsLabelObj,BorderLayout.NORTH);
    recipientGUIPanel.add(scrollPanePanel,BorderLayout.CENTER);
    recipientGUIPanel.add(createButtonPanel(),BorderLayout.EAST);
    if (connStatusPanel != null)
    {
      //setup bottom status-message panel:
      final JPanel statusPanel = new JPanel();
      statusPanel.setLayout(new BorderLayout(5, 0));
      statusPanel.setBorder( //make it appear lowered
          BorderFactory.createBevelBorder(BevelBorder.LOWERED));
      statusPanel.add(connStatusPanel, BorderLayout.EAST);
      recipientGUIPanel.add(statusPanel, BorderLayout.SOUTH);
    }
    return recipientGUIPanel;
  }

  /**
   * Returns the panel setup via 'setupGUI()'.
   * @return The 'JPanel' object setup via 'setupGUI()', or null if
   * none has been setup.
   */
  public JPanel getRecipientGUIPanel()
  {
    return recipientGUIPanel;
  }

  /**
   * Creates the button panel.
   * @return the button panel.
   */
  protected JPanel createButtonPanel()
  {
         //create and setup buttons:
//    final JButton selectAllButton = new JButton(selectAllButtonActionObj);
//    selectAllButton.setToolTipText("Select all recipients");
    addButtonObj = new JButton(addButtonActionObj);
    addButtonObj.setToolTipText("Add new recipient");
    editButtonObj = new JButton(editButtonActionObj);
    editButtonObj.setToolTipText("Edit selected recipient");
    regionsButtonObj = new JButton(regionsButtonActionObj);
    regionsButtonObj.setToolTipText(
                       "Modify email alarm regions for selected recipient");
    removeButtonObj = new JButton(removeButtonActionObj);
    removeButtonObj.setToolTipText("Remove selected recipient(s)");
              //place buttons vertically on buttons panel:
    final JPanel buttonsPanel = new JPanel(new GridBagLayout());
    final GridBagConstraints constraintsObj = new GridBagConstraints();
    constraintsObj.gridx = constraintsObj.gridy = 0;
    constraintsObj.gridwidth = constraintsObj.gridheight = 1;
    constraintsObj.weightx = 1.0;
    constraintsObj.weighty = 0.25;
    constraintsObj.anchor = GridBagConstraints.CENTER;
    constraintsObj.fill = GridBagConstraints.HORIZONTAL;
    constraintsObj.insets = new Insets(2,0,2,0);
//    buttonsPanel.add(selectAllButton,constraintsObj);
//    ++(constraintsObj.gridy);
    buttonsPanel.add(addButtonObj,constraintsObj);
    ++(constraintsObj.gridy);
    buttonsPanel.add(editButtonObj,constraintsObj);
    ++(constraintsObj.gridy);
    buttonsPanel.add(regionsButtonObj,constraintsObj);
    ++(constraintsObj.gridy);
    buttonsPanel.add(removeButtonObj,constraintsObj);
//    ++(constraintsObj.gridy);
//    buttonsPanel.add(new EnableTypePanel(),constraintsObj);
              //add struct below buttons to push them up a little:
    ++(constraintsObj.gridy);
    constraintsObj.weighty = 0.75;     //increase spacing around strut
    constraintsObj.insets = new Insets(2,0,2,0);
    buttonsPanel.add(Box.createVerticalStrut(1),constraintsObj);
              //put empty border around buttons panel:
    buttonsPanel.setBorder(BorderFactory.createEmptyBorder(0,5,0,10));
    return buttonsPanel;
  }

  /**
   * Update the alert recipients.
   */
  public void updateAlertRecipients()
  {
    if (SwingUtilities.isEventDispatchThread())
      //current thread is the event dispatch thread
      doUpdateAlertRecipients();
    else
      SwingUtilities.invokeLater(new Runnable()
      { //do this after other Swing work is complete
        public void run()
        {
          doUpdateAlertRecipients();
        }
      });
  }

  /**
   * Update the alert recipients.
   * NOTE: This method should only be called from the event dispatch thread.
   */
  private void doUpdateAlertRecipients()
  {
    try
    {
      recipientsJListObj.repaint(); //update displayed list of recipients
    }
    catch (Exception ex)
    { //some kind of exception error; display it and move on
      System.err.println("Error with list repaint:  " + ex);
      ex.printStackTrace();
    }
  }

  /**
   * Creates, edits and adds a new recipient object.
   * NOTE: This method should only be called from the event dispatch thread.
   */
  private void doAddNewRecipient()
  {
    try
    {         //create new recipient object:
      final AlertRecipient recipObj = new AlertEmailRecipient(progInfo);
              //set region manager to be used by recipient:
      recipObj.setIstiRegionMgrObj(istiRegionMgrObj);
      showConfigDialog(recipObj);           //edit recipient
    }
    catch (Exception ex)
    {
      final LogFile logObj = progInfo.getProgramLogFileObj();
      if(logObj != null)
        logObj.warning("Error adding new recipient:  " + ex);
    }
  }

  /**
   * Edits the recipient that is currently selected on the GUI list.
   * NOTE: This method should only be called from the event dispatch thread.
   */
  private void doEditSelectedRecipient()
  {
    final Object obj;             //get currently-selected recipient object:
    if((obj=recipientsJListObj.getSelectedValue()) instanceof AlertRecipient)
      showConfigDialog((AlertRecipient)obj);  //edit recipient
  }

  /**
   * Selects all recipients that are currently on the GUI list.
   * NOTE: This method should only be called from the event dispatch thread.
   */
//  private void doSelectAllRecipients()
//  {
//    try
//    {
//      recipientsJListObj.setSelectionInterval(0, size() - 1);
//      recipientsJListObj.repaint(); //update displayed list of recipients
//    }
//    catch (Exception ex)
//    { //some kind of exception error; display it and move on
//      System.err.println("Error selecting list entry:  " + ex);
//      ex.printStackTrace();
//    }
//  }

  /**
   * Edits the alarm regions for the recipient that is currently selected
   * on the GUI list.
   * NOTE: This method should only be called from the event dispatch thread.
   */
  private void doAlarmRegionsForRecipient()
  {
    final Object obj;             //get currently-selected recipient object:
    if((obj=recipientsJListObj.getSelectedValue()) instanceof AlertRecipient)
      showRegionsConfigDialog((AlertRecipient)obj);  //edit recipient
  }

  /**
   * Removes the one or more recipients that are currently selected on
   * the GUI list.
   * NOTE: This method should only be called from the event dispatch thread.
   */
  private void doRemoveSelectedRecipients()
  {
    if(inRemoveSelRecipsMethodFlag)
      return;           //if already in method then exit
    inRemoveSelRecipsMethodFlag = true;     //indicate inside of method
    final String confirmRemovalStr = "Confirm Removal";
                        //get selected recipient objects:
    final List selObjs = GuiUtilFns.getSelectedValuesList(recipientsJListObj);
    Object obj;
    final int numSel;
    if((numSel=selObjs.size()) > 0)
    {    //at least one recipient object selected
      final IstiDialogUtil dialogUtilObj =       //setup util for popups
              new IstiDialogUtil(getWindowForComponent(recipientsJListObj));
      if(numSel > 1)
      {       //more than one recipient object selected
        if(dialogUtilObj != null && dialogUtilObj.popupYesNoConfirmMessage(
                         ("Remove the " + numSel + " selected recipients?"),
                     confirmRemovalStr,false) != IstiDialogPopup.YES_OPTION)
        {     //user selected did not select "Yes"
          inRemoveSelRecipsMethodFlag = false;   //indicate done method
          return;                                //cancel removal
        }
        for(int idx=0; idx<numSel; ++idx)
        {     //for each selected recipient entry; remove it
          if((obj=selObjs.get(idx)) instanceof AlertRecipient)
          {   //selected alert-recipient object fetched OK
            recipientsListObj.remove(obj);  //remove from list
                                                 //tag recipient as deleted:
            ((AlertRecipient)obj).setRecipDeletedFlag();
          }
        }
      }
      else
      {       //only one recipient object selected
        final AlertRecipient recipObj;
        if(!((obj=selObjs.get(0)) instanceof AlertRecipient))
        {     //unable to fetch object
          inRemoveSelRecipsMethodFlag = false;   //indicate done method
          return;                                //cancel removal
        }
        recipObj = (AlertRecipient)obj;
                 //get user confirmation:
        if(dialogUtilObj != null && dialogUtilObj.popupYesNoConfirmMessage(
                                     ("Remove recipient " + recipObj + "?"),
                     confirmRemovalStr,false) != IstiDialogPopup.YES_OPTION)
        {     //user selected "No"
          inRemoveSelRecipsMethodFlag = false;   //indicate done method
          return;                                //cancel removal
        }
        recipientsListObj.remove(recipObj); //remove from list
        recipObj.setRecipDeletedFlag();          //tag recipient as deleted
      }
      doUpdateListModel();  //update the list model
      doUpdateAlertRecipients();  //update displayed list of recipients
      doSelectAndShowListRecipient(0);   //select first recipient entry
      progInfo.saveConfiguration(true,false);  //save configuration to file
      inRemoveSelRecipsMethodFlag = false;       //indicate done method
    }
  }

  /**
   * Selects the given entry on the GUI list of recipients and ensures
   * that the entry is visible.
   * @param idx the index of the recipient entry.
   * NOTE: This method should only be called from the event dispatch thread.
   */
  private void doSelectAndShowListRecipient(final int idx)
  {
    try
    {
      recipientsJListObj.setSelectedIndex(idx);
      recipientsJListObj.ensureIndexIsVisible(idx);
      recipientsJListObj.repaint(); //update displayed list of recipients
    }
    catch (Exception ex)
    { //some kind of exception error; display it and move on
      System.err.println("Error selecting list entry:  " + ex);
      ex.printStackTrace();
    }
  }

  /**
   * Updates the list model.
   */
  protected void updateListModel()
  {
    if (SwingUtilities.isEventDispatchThread())
      //current thread is the event dispatch thread
      doUpdateListModel();
    else
      SwingUtilities.invokeLater(new Runnable()
      { //do this after other Swing work is complete
        public void run()
        {
          doUpdateListModel();
        }
      });
  }

  /**
   * Updates the list model.
   * NOTE: This method should only be called from the event dispatch thread.
   */
  private void doUpdateListModel()
  {
    try
    {
      //set data model for GUI list of recipients:
      recipientsJListObj.setListData(recipientsListObj.toArray());
      //if list is not empty and there are none selected
      if (!isEmpty() && recipientsJListObj.getSelectedIndex() < 0)
      {
        final int idx = 0; //select and display first entry
        recipientsJListObj.setSelectedIndex(idx);
        recipientsJListObj.ensureIndexIsVisible(idx);
      }
      recipientsJListObj.repaint(); //update displayed list of recipients
    }
    catch (Exception ex)
    { //some kind of exception error; display it and move on
      System.err.println("Error with update list model:  " + ex);
      ex.printStackTrace();
    }
  }

  /**
   * Shows the configuration dialog for the specified alert recipient.
   * @param recipObj the alert recipient.
   */
  public void showConfigDialog(AlertRecipient recipObj)
  {
    final AlertRecipientConfigDialog alertRecipientConfigDialog =
            new AlertRecipientConfigDialog(cfgSettingsDialog,this,recipObj);
    try
    {
      setButtonsEnabled(false);        //disable buttons
         //show dialog (give parent Frame or Dialog object):
      alertRecipientConfigDialog.showDialog(     //wait for dialog closed
                            getWindowForComponent(recipientsJListObj),true);
    }
    finally
    {
      setButtonsEnabled(true);        //re-enable buttons
    }
  }

  /**
   * Shows the regions configuration dialog for the specified alert recipient.
   * @param recipObj the alert recipient.
   */
  public void showRegionsConfigDialog(AlertRecipient recipObj)
  {
    final AlarmRegionConfigDialog alarmRegionConfigDialog =
               new AlarmRegionConfigDialog(cfgSettingsDialog,this,recipObj);
    com.isti.util.propertyeditor.CfgPropertyInspector propInspObj = null;
    String propInspTabStr = null;
    try
    {
      setButtonsEnabled(false);        //disable buttons
      if(tearOffRegionsDlgFlag)
      {  //"tear off" regions dialog
        if(cfgSettingsDialog != null &&
          (propInspObj=cfgSettingsDialog.getCfgPropertyInspector()) != null)
        {     //config-settings dialog fetched OK
                                            //get name of current tab:
          propInspTabStr = propInspObj.getCurrentTabName();
          propInspObj.closeDialog(true);    //close settings dialog (commit)
        }
                   //show dialog (make edit dialog use program frame):
        alarmRegionConfigDialog.showDialog(        //wait for dialog closed
                                   null,progInfo.getProgramFrameObj(),true);
      }
      else
      {  //setup regions-dialog parent as parent Frame or Dialog object
                   //show dialog:
        alarmRegionConfigDialog.showDialog(        //wait for dialog closed
                            getWindowForComponent(recipientsJListObj),true);
      }
    }
    finally
    {
      setButtonsEnabled(true);        //re-enable buttons
      if(propInspObj != null)
      {  //config-settings dialog was close
        if(propInspTabStr != null)     //if tab name setup then select tab
          propInspObj.setSelectedTabName(propInspTabStr);
        propInspObj.showDialog();      //re-show config-settings dialog
      }
    }
  }

  /**
   * Returns the specified component's parent Frame or Dialog object.
   * @param compObj Component to check.
   * @return The Frame or Dialog that contains the component, or the
   * given component if it does not have a valid Frame or Dialog parent.
   */
  public static Component getWindowForComponent(Component compObj)
  {
    final Component parentObj;
    return ((parentObj=doGetWindowForComponent(compObj)) != null) ?
                                                        parentObj : compObj;
  }

  /**
   * Returns the specified component's parent Frame or Dialog object.
   * @param compObj Component to check.
   * @return The Frame or Dialog that contains the component, or null
   * if the component does not have a valid Frame or Dialog parent.
   */
  private static Window doGetWindowForComponent(Component compObj)
  {
    if(compObj == null)
      return null;
    if(compObj instanceof Frame || compObj instanceof Dialog)
      return (Window)compObj;
    return doGetWindowForComponent(compObj.getParent());
  }

  /**
   * Enables or disables the button components.
   * @param flgVal true to enable, false to disable.
   */
  protected void setButtonsEnabled(boolean flgVal)
  {
    if(addButtonObj != null)
      addButtonObj.setEnabled(flgVal);
    if(editButtonObj != null)
      editButtonObj.setEnabled(flgVal);
    if(removeButtonObj != null)
      removeButtonObj.setEnabled(flgVal);
    if(regionsButtonObj != null)
      regionsButtonObj.setEnabled(flgVal);
    mouseClickEditFlag = flgVal;
  }

  /**
   * Determines whether or not this list contains any send-enabled
   * recipients.
   * @return true if this list contains any send-enabled recipients;
   * false if not.
   */
  public boolean getAnyEnabledRecipsFlag()
  {
         //setup iterator for alert-recipient objects:
    final ModIterator iterObj = new ModIterator(this);
    Object obj;
    while(iterObj.hasNext())
    { //for each alert-recipient object
      if((obj=iterObj.next()) instanceof AlertRecipient &&
                                      ((AlertRecipient)obj).isSendEnabled())
      {  //recipient found and has send enabled
        return true;
      }
    }
    return false;
  }

  /**
   * Returns the region-manager object.
   * @return The region-manager object, or null if none available.
   */
  public IstiRegionMgrIntf getIstiRegionMgrObj()
  {
    return istiRegionMgrObj;
  }


//  /**
//   * The enable type panel.
//   */
//  private class EnableTypePanel extends JPanel implements ActionListener
//  {
//    /** Enable types. */
//    private static final int ENABLE_SEND = 0;
//    private static final int ENABLE_GLOBAL = 1;
//
//    /** Enable type combo box. */
//    private final JComboBox enableTypeComboBox;
//    /** Enable button. */
//    private final JButton enableButton;
//    /** Disable button. */
//    private final JButton disableButton;
//
//    /** Action object for the "Enable" button (and menu item). */
//    public final AbstractAction enableButtonActionObj =
//        new AbstractAction("Enable")
//    {
//      public void actionPerformed(ActionEvent evtObj)
//      {
//        doEnableSelectedRecipients(true);
//      }
//    };
//
//    /** Action object for the "Disable" button (and menu item). */
//    public final AbstractAction disableButtonActionObj =
//        new AbstractAction("Disable")
//    {
//      public void actionPerformed(ActionEvent evtObj)
//      {
//        doEnableSelectedRecipients(false);
//      }
//    };
//
//    /**
//     * Creates an enable type panel.
//     */
//    public EnableTypePanel()
//    {
//      super(new GridBagLayout());
//      final Vector actionTypesVec = new Vector();
//      actionTypesVec.add("Send Alert");
//      if (progInfo.isFunctionalitySupported(QWProgramInformationInterface.GLOBAL_ALARMS_SUPPORTED_VALUE))
//        actionTypesVec.add("Global Alarm");
//      setBorder(BorderFactory.createEtchedBorder());
//      enableTypeComboBox = new JComboBox(actionTypesVec.toArray());
//      enableTypeComboBox.setToolTipText("Type for use with Enable/Disable buttons below");
//      enableTypeComboBox.addActionListener(this);
//      if (actionTypesVec.size() <= 1)
//        enableTypeComboBox.setVisible(false);
//
//      enableButton = new JButton(enableButtonActionObj);
//      disableButton = new JButton(disableButtonActionObj);
//      updateToolTipText();  //update the tool tip text
//
//      final GridBagConstraints constraintsObj = new GridBagConstraints();
//      constraintsObj.gridx = constraintsObj.gridy = 0;
//      constraintsObj.gridwidth = 2;
//      constraintsObj.gridheight = 1;
//      constraintsObj.weightx = 1.0;
//      constraintsObj.weighty = 0.25;
//      constraintsObj.anchor = GridBagConstraints.CENTER;
//      constraintsObj.fill = GridBagConstraints.HORIZONTAL;
//
//      add(enableTypeComboBox,constraintsObj);
//      constraintsObj.gridwidth = 1;
//      ++(constraintsObj.gridy);
//      add(enableButton,constraintsObj);
//      ++(constraintsObj.gridx);
//      add(disableButton,constraintsObj);
//    }
//
//    /**
//     * Invoked when an action occurs.
//     * @param e the 'ActionEvent'.
//     */
//    public void actionPerformed(ActionEvent e)
//    {
//      if (e.getSource().equals(enableTypeComboBox))
//      {
//        updateToolTipText();  //update the tool tip text
//      }
//    }
//
//    /**
//     * Enables the one or more recipients that are currently selected on
//     * the GUI list.
//     * @param enableFlag true to enable, false to disable.
//     * @see ENABLE_SEND, ENABLE_GLOBAL
//     * NOTE: This method should only be called from the event dispatch thread.
//     */
//    protected void doEnableSelectedRecipients(boolean enableFlag)
//    {
//      //get array of selected recipient objects:
//      final Object[] selObjsArr = recipientsJListObj.getSelectedValues();
//      Object obj;
//      final int numSel;
//      if ( (numSel = selObjsArr.length) > 0)
//      { //at least one recipient object selected
//        if (numSel > 1)
//        { //more than one recipient object selected
//          for (int idx = 0; idx < numSel; ++idx)
//          { //for each selected recipient entry; remove it
//            if ( (obj = selObjsArr[idx]) instanceof AlertRecipient)
//            { //selected alert-recipient object fetched OK
//              //enable recipient
//              doEnableRecipient( (AlertRecipient) obj,
//                                                 enableFlag);
//            }
//          }
//        }
//        else
//        { //only one recipient object selected
//          if (! ( (obj = selObjsArr[0]) instanceof AlertRecipient))
//          { //unable to fetch object
//            return; //cancel removal
//          }
//          //enable recipient
//          doEnableRecipient( (AlertRecipient) obj, enableFlag);
//        }
//        doUpdateAlertRecipients(); //update displayed list of recipients
//        progInfo.saveConfiguration(true, false); //save configuration to file
//      }
//    }
//
//    /**
//     * Enables the specified recipient.
//     * @param recipObj the alert email recipient object.
//     * @param enableFlag true to enable, false to disable.
//     * @see ENABLE_SEND, ENABLE_GLOBAL
//     */
//    public void enableRecipient(
//      final AlertRecipient recipObj,final boolean enableFlag)
//    {
//      if (SwingUtilities.isEventDispatchThread())
//        //current thread is the event dispatch thread
//        doEnableRecipient(recipObj,enableFlag);
//      else
//        SwingUtilities.invokeLater(new Runnable()
//        { //do this after other Swing work is complete
//          public void run()
//          {
//            doEnableRecipient(recipObj,enableFlag);
//          }
//        });
//    }
//
//    /**
//     * Enables the specified recipient.
//     * @param recipObj the alert email recipient object.
//     * @param enableFlag true to enable, false to disable.
//     * @see ENABLE_SEND, ENABLE_GLOBAL
//     * NOTE: This method should only be called from the event dispatch thread.
//     */
//    private void doEnableRecipient(
//      final AlertRecipient recipObj,final boolean enableFlag)
//    {
//      final int enableType = enableTypeComboBox.getSelectedIndex();
//      switch (enableType)
//      {
//        case ENABLE_SEND:
//          recipObj.setSendEnabled(enableFlag);
//          break;
//        case ENABLE_GLOBAL:
//          recipObj.setGlobalAlarmEnabled(enableFlag);
//          break;
//        default:
//          System.err.println("AlertRecipientsList-invalid type:  " + enableType);
//          break;
//      }
//    }
//
//    /**
//     * Updates the tool tip text.
//     */
//    private void updateToolTipText()
//    {
//      final String tipText;
//      final int enableType = enableTypeComboBox.getSelectedIndex();
//      switch (enableType)
//      {
//        case ENABLE_SEND:
//          tipText = " sending alerts to the selected recipient(s)";
//          break;
//        case ENABLE_GLOBAL:
//          tipText = " sending alerts to the selected recipient(s) when global alarm is triggered";
//          break;
//        default:
//          System.err.println("EnableTypePanel-invalid type:  " +
//                             enableType);
//          return;
//      }
//      enableButton.setToolTipText("Enable " + tipText);
//      disableButton.setToolTipText("Disable " + tipText);
//    }
//  }


  //List interface
  // Query Operations

  /**
   * Returns the number of elements in this list.  If this list contains
   * more than <tt>Integer.MAX_VALUE</tt> elements, returns
   * <tt>Integer.MAX_VALUE</tt>.
   *
   * @return the number of elements in this list.
   */
  public int size() { return recipientsListObj.size(); }

  /**
   * Returns <tt>true</tt> if this list contains no elements.
   *
   * @return <tt>true</tt> if this list contains no elements.
   */
  public boolean isEmpty() { return recipientsListObj.isEmpty(); }

  /**
   *
   * Returns <tt>true</tt> if this list contains the specified element.
   * More formally, returns <tt>true</tt> if and only if this list contains
   * at least one element <tt>e</tt> such that
   * <tt>(o==null&nbsp;?&nbsp;e==null&nbsp;:&nbsp;o.equals(e))</tt>.
   *
   * @param o element whose presence in this list is to be tested.
   * @return <tt>true</tt> if this list contains the specified element.
   * @throws ClassCastException if the type of the specified element
   * 	       is incompatible with this list (optional).
   * @throws NullPointerException if the specified element is null and this
   *         list does not support null elements (optional).
   */
  public boolean contains(Object o) { return recipientsListObj.contains(o); }

  /**
   * Returns an iterator over the elements in this list in proper sequence.
   *
   * @return an iterator over the elements in this list in proper sequence.
   */
  public Iterator iterator() { return recipientsListObj.iterator(); }

  /**
   * Returns an array containing all of the elements in this list in proper
   * sequence.  Obeys the general contract of the
   * <tt>Collection.toArray</tt> method.
   *
   * @return an array containing all of the elements in this list in proper
   *	       sequence.
   * @see Arrays#asList(Object[])
   */
  public Object[] toArray() { return recipientsListObj.toArray(); }

  /**
   * Returns an array containing all of the elements in this list in proper
   * sequence; the runtime type of the returned array is that of the
   * specified array.  Obeys the general contract of the
   * <tt>Collection.toArray(Object[])</tt> method.
   *
   * @param a the array into which the elements of this list are to
   *		be stored, if it is big enough; otherwise, a new array of the
   * 		same runtime type is allocated for this purpose.
   * @return  an array containing the elements of this list.
   *
   * @throws ArrayStoreException if the runtime type of the specified array
   * 		  is not a supertype of the runtime type of every element in
   * 		  this list.
   * @throws NullPointerException if the specified array is <tt>null</tt>.
   */
  public Object[] toArray(Object a[]) { return recipientsListObj.toArray(a); }


  // Modification Operations

  /**
   * Appends the specified element to the end of this list (optional
   * operation). <p>
   *
   * Lists that support this operation may place limitations on what
   * elements may be added to this list.  In particular, some
   * lists will refuse to add null elements, and others will impose
   * restrictions on the type of elements that may be added.  List
   * classes should clearly specify in their documentation any restrictions
   * on what elements may be added.
   *
   * @param o element to be appended to this list.
   * @return <tt>true</tt> (as per the general contract of the
   *            <tt>Collection.add</tt> method).
   *
   * @throws UnsupportedOperationException if the <tt>add</tt> method is not
   * 		  supported by this list.
   * @throws ClassCastException if the class of the specified element
   * 		  prevents it from being added to this list.
   * @throws NullPointerException if the specified element is null and this
   *           list does not support null elements.
   * @throws IllegalArgumentException if some aspect of this element
   *            prevents it from being added to this list.
   */
  public boolean add(Object o)
  {
    final boolean retVal = recipientsListObj.add(o);
    updateListModel();                 //update the list model
    return retVal;
  }

  /**
   * Removes the first occurrence in this list of the specified element
   * (optional operation).  If this list does not contain the element, it is
   * unchanged.  More formally, removes the element with the lowest index i
   * such that <tt>(o==null ? get(i)==null : o.equals(get(i)))</tt> (if
   * such an element exists).
   *
   * @param o element to be removed from this list, if present.
   * @return <tt>true</tt> if this list contained the specified element.
   * @throws ClassCastException if the type of the specified element
   * 	          is incompatible with this list (optional).
   * @throws NullPointerException if the specified element is null and this
   *            list does not support null elements (optional).
   * @throws UnsupportedOperationException if the <tt>remove</tt> method is
   *		  not supported by this list.
   */
  public boolean remove(Object o)
  {
    final boolean retFlag = recipientsListObj.remove(o);
    updateListModel();  //update the list model
    return retFlag;
  }


  // Bulk Modification Operations

  /**
   *
   * Returns <tt>true</tt> if this list contains all of the elements of the
   * specified collection.
   *
   * @param  c collection to be checked for containment in this list.
   * @return <tt>true</tt> if this list contains all of the elements of the
   * 	       specified collection.
   * @throws ClassCastException if the types of one or more elements
   *         in the specified collection are incompatible with this
   *         list (optional).
   * @throws NullPointerException if the specified collection contains one
   *         or more null elements and this list does not support null
   *         elements (optional).
   * @throws NullPointerException if the specified collection is
   *         <tt>null</tt>.
   * @see #contains(Object)
   */
  public boolean containsAll(Collection c) { return recipientsListObj.containsAll(c); }

  /**
   * Appends all of the elements in the specified collection to the end of
   * this list, in the order that they are returned by the specified
   * collection's iterator (optional operation).  The behavior of this
   * operation is unspecified if the specified collection is modified while
   * the operation is in progress.  (Note that this will occur if the
   * specified collection is this list, and it's nonempty.)
   *
   * @param c collection whose elements are to be added to this list.
   * @return <tt>true</tt> if this list changed as a result of the call.
   *
   * @throws UnsupportedOperationException if the <tt>addAll</tt> method is
   *         not supported by this list.
   * @throws ClassCastException if the class of an element in the specified
   * 	       collection prevents it from being added to this list.
   * @throws NullPointerException if the specified collection contains one
   *         or more null elements and this list does not support null
   *         elements, or if the specified collection is <tt>null</tt>.
   * @throws IllegalArgumentException if some aspect of an element in the
   *         specified collection prevents it from being added to this
   *         list.
   * @see #add(Object)
   */
  public boolean addAll(Collection c)
  {
    final boolean retFlag = recipientsListObj.addAll(c);
    updateListModel();                 //update the list model
    return retFlag;
  }

  /**
   * Inserts all of the elements in the specified collection into this
   * list at the specified position (optional operation).  Shifts the
   * element currently at that position (if any) and any subsequent
   * elements to the right (increases their indices).  The new elements
   * will appear in this list in the order that they are returned by the
   * specified collection's iterator.  The behavior of this operation is
   * unspecified if the specified collection is modified while the
   * operation is in progress.  (Note that this will occur if the specified
   * collection is this list, and it's nonempty.)
   *
   * @param index index at which to insert first element from the specified
   *	            collection.
   * @param c elements to be inserted into this list.
   * @return <tt>true</tt> if this list changed as a result of the call.
   *
   * @throws UnsupportedOperationException if the <tt>addAll</tt> method is
   *		  not supported by this list.
   * @throws ClassCastException if the class of one of elements of the
   * 		  specified collection prevents it from being added to this
   * 		  list.
   * @throws NullPointerException if the specified collection contains one
   *           or more null elements and this list does not support null
   *           elements, or if the specified collection is <tt>null</tt>.
   * @throws IllegalArgumentException if some aspect of one of elements of
   *		  the specified collection prevents it from being added to
   *		  this list.
   * @throws IndexOutOfBoundsException if the index is out of range (index
   *		  &lt; 0 || index &gt; size()).
   */
  public boolean addAll(int index, Collection c)
  {
    final boolean retFlag = recipientsListObj.addAll(index,c);
    updateListModel();                 //update the list model
    return retFlag;
  }

  /**
   * Removes from this list all the elements that are contained in the
   * specified collection (optional operation).
   *
   * @param c collection that defines which elements will be removed from
   *          this list.
   * @return <tt>true</tt> if this list changed as a result of the call.
   *
   * @throws UnsupportedOperationException if the <tt>removeAll</tt> method
   * 		  is not supported by this list.
   * @throws ClassCastException if the types of one or more elements
   *            in this list are incompatible with the specified
   *            collection (optional).
   * @throws NullPointerException if this list contains one or more
   *            null elements and the specified collection does not support
   *            null elements (optional).
   * @throws NullPointerException if the specified collection is
   *            <tt>null</tt>.
   * @see #remove(Object)
   * @see #contains(Object)
   */
  public boolean removeAll(Collection c)
  {
    final boolean retFlag = recipientsListObj.removeAll(c);
    updateListModel();                 //update the list model
    return retFlag;
  }

  /**
   * Retains only the elements in this list that are contained in the
   * specified collection (optional operation).  In other words, removes
   * from this list all the elements that are not contained in the specified
   * collection.
   *
   * @param c collection that defines which elements this set will retain.
   *
   * @return <tt>true</tt> if this list changed as a result of the call.
   *
   * @throws UnsupportedOperationException if the <tt>retainAll</tt> method
   * 		  is not supported by this list.
   * @throws ClassCastException if the types of one or more elements
   *            in this list are incompatible with the specified
   *            collection (optional).
   * @throws NullPointerException if this list contains one or more
   *            null elements and the specified collection does not support
   *            null elements (optional).
   * @throws NullPointerException if the specified collection is
   *         <tt>null</tt>.
   * @see #remove(Object)
   * @see #contains(Object)
   */
  public boolean retainAll(Collection c)
  {
    final boolean retFlag = recipientsListObj.retainAll(c);
    updateListModel();  //update the list model
    return retFlag;
  }

  /**
   * Removes all of the elements from this list (optional operation).  This
   * list will be empty after this call returns (unless it throws an
   * exception).
   *
   * @throws UnsupportedOperationException if the <tt>clear</tt> method is
   * 		  not supported by this list.
   */
  public void clear()
  {
    recipientsListObj.clear();
    updateListModel();       //update the list model
  }


  // Comparison and hashing

  /**
   * Compares the specified object with this list for equality.  Returns
   * <tt>true</tt> if and only if the specified object is also a list, both
   * lists have the same size, and all corresponding pairs of elements in
   * the two lists are <i>equal</i>.  (Two elements <tt>e1</tt> and
   * <tt>e2</tt> are <i>equal</i> if <tt>(e1==null ? e2==null :
   * e1.equals(e2))</tt>.)  In other words, two lists are defined to be
   * equal if they contain the same elements in the same order.  This
   * definition ensures that the equals method works properly across
   * different implementations of the <tt>List</tt> interface.
   *
   * @param o the object to be compared for equality with this list.
   * @return <tt>true</tt> if the specified object is equal to this list.
   */
  public boolean equals(Object o) { return recipientsListObj.equals(o); }

  /**
   * Returns the hash code value for this list.  The hash code of a list
   * is defined to be the result of the following calculation:
   * <pre>
   *  hashCode = 1;
   *  Iterator i = list.iterator();
   *  while (i.hasNext()) {
   *      Object obj = i.next();
   *      hashCode = 31*hashCode + (obj==null ? 0 : obj.hashCode());
   *  }
   * </pre>
   * This ensures that <tt>list1.equals(list2)</tt> implies that
   * <tt>list1.hashCode()==list2.hashCode()</tt> for any two lists,
   * <tt>list1</tt> and <tt>list2</tt>, as required by the general
   * contract of <tt>Object.hashCode</tt>.
   *
   * @return the hash code value for this list.
   * @see Object#hashCode()
   * @see Object#equals(Object)
   * @see #equals(Object)
   */
  public int hashCode() { return recipientsListObj.hashCode(); }


  // Positional Access Operations

  /**
   * Returns the element at the specified position in this list.
   *
   * @param index index of element to return.
   * @return the element at the specified position in this list.
   *
   * @throws IndexOutOfBoundsException if the index is out of range (index
   * 		  &lt; 0 || index &gt;= size()).
   */
  public Object get(int index) { return recipientsListObj.get(index); }

  /**
   * Replaces the element at the specified position in this list with the
   * specified element (optional operation).
   *
   * @param index index of element to replace.
   * @param element element to be stored at the specified position.
   * @return the element previously at the specified position.
   *
   * @throws UnsupportedOperationException if the <tt>set</tt> method is not
   *		  supported by this list.
   * @throws    ClassCastException if the class of the specified element
   * 		  prevents it from being added to this list.
   * @throws    NullPointerException if the specified element is null and
   *            this list does not support null elements.
   * @throws    IllegalArgumentException if some aspect of the specified
   *		  element prevents it from being added to this list.
   * @throws    IndexOutOfBoundsException if the index is out of range
   *		  (index &lt; 0 || index &gt;= size()).
   */
  public Object set(int index, Object element)
  {
    final Object retVal = recipientsListObj.set(index,element);
    updateListModel();  //update the list model
    return retVal;
  }

  /**
   * Inserts the specified element at the specified position in this list
   * (optional operation).  Shifts the element currently at that position
   * (if any) and any subsequent elements to the right (adds one to their
   * indices).
   *
   * @param index index at which the specified element is to be inserted.
   * @param element element to be inserted.
   *
   * @throws UnsupportedOperationException if the <tt>add</tt> method is not
   *		  supported by this list.
   * @throws    ClassCastException if the class of the specified element
   * 		  prevents it from being added to this list.
   * @throws    NullPointerException if the specified element is null and
   *            this list does not support null elements.
   * @throws    IllegalArgumentException if some aspect of the specified
   *		  element prevents it from being added to this list.
   * @throws    IndexOutOfBoundsException if the index is out of range
   *		  (index &lt; 0 || index &gt; size()).
   */
  public void add(int index, Object element)
  {
    recipientsListObj.add(index,element);
    updateListModel();  //update the list model
  }

  /**
   * Removes the element at the specified position in this list (optional
   * operation).  Shifts any subsequent elements to the left (subtracts one
   * from their indices).  Returns the element that was removed from the
   * list.
   *
   * @param index the index of the element to removed.
   * @return the element previously at the specified position.
   *
   * @throws UnsupportedOperationException if the <tt>remove</tt> method is
   *		  not supported by this list.
   * @throws IndexOutOfBoundsException if the index is out of range (index
   *            &lt; 0 || index &gt;= size()).
   */
  public Object remove(int index)
  {
    final Object retVal = recipientsListObj.remove(index);
    updateListModel();  //update the list model
    return retVal;
  }


  // Search Operations

  /**
   * Returns the index in this list of the first occurrence of the specified
   * element, or -1 if this list does not contain this element.
   * More formally, returns the lowest index <tt>i</tt> such that
   * <tt>(o==null ? get(i)==null : o.equals(get(i)))</tt>,
   * or -1 if there is no such index.
   *
   * @param o element to search for.
   * @return the index in this list of the first occurrence of the specified
   * 	       element, or -1 if this list does not contain this element.
   * @throws ClassCastException if the type of the specified element
   * 	       is incompatible with this list (optional).
   * @throws NullPointerException if the specified element is null and this
   *         list does not support null elements (optional).
   */
  public int indexOf(Object o) { return recipientsListObj.indexOf(o); }

  /**
   * Returns the index in this list of the last occurrence of the specified
   * element, or -1 if this list does not contain this element.
   * More formally, returns the highest index <tt>i</tt> such that
   * <tt>(o==null ? get(i)==null : o.equals(get(i)))</tt>,
   * or -1 if there is no such index.
   *
   * @param o element to search for.
   * @return the index in this list of the last occurrence of the specified
   * 	       element, or -1 if this list does not contain this element.
   * @throws ClassCastException if the type of the specified element
   * 	       is incompatible with this list (optional).
   * @throws NullPointerException if the specified element is null and this
   *         list does not support null elements (optional).
   */
  public int lastIndexOf(Object o) { return recipientsListObj.lastIndexOf(o); }


  // List Iterators

  /**
   * Returns a list iterator of the elements in this list (in proper
   * sequence).
   *
   * @return a list iterator of the elements in this list (in proper
   * 	       sequence).
   */
  public ListIterator listIterator() { return recipientsListObj.listIterator(); }

  /**
   * Returns a list iterator of the elements in this list (in proper
   * sequence), starting at the specified position in this list.  The
   * specified index indicates the first element that would be returned by
   * an initial call to the <tt>next</tt> method.  An initial call to
   * the <tt>previous</tt> method would return the element with the
   * specified index minus one.
   *
   * @param index index of first element to be returned from the
   *		    list iterator (by a call to the <tt>next</tt> method).
   * @return a list iterator of the elements in this list (in proper
   * 	       sequence), starting at the specified position in this list.
   * @throws IndexOutOfBoundsException if the index is out of range (index
   *         &lt; 0 || index &gt; size()).
   */
  public ListIterator listIterator(int index) { return recipientsListObj.listIterator(index); }

  // View

  /**
   * Returns a view of the portion of this list between the specified
   * <tt>fromIndex</tt>, inclusive, and <tt>toIndex</tt>, exclusive.  (If
   * <tt>fromIndex</tt> and <tt>toIndex</tt> are equal, the returned list is
   * empty.)  The returned list is backed by this list, so non-structural
   * changes in the returned list are reflected in this list, and vice-versa.
   * The returned list supports all of the optional list operations supported
   * by this list.<p>
   *
   * This method eliminates the need for explicit range operations (of
   * the sort that commonly exist for arrays).   Any operation that expects
   * a list can be used as a range operation by passing a subList view
   * instead of a whole list.  For example, the following idiom
   * removes a range of elements from a list:
   * <pre>
   *	    list.subList(from, to).clear();
   * </pre>
   * Similar idioms may be constructed for <tt>indexOf</tt> and
   * <tt>lastIndexOf</tt>, and all of the algorithms in the
   * <tt>Collections</tt> class can be applied to a subList.<p>
   *
   * The semantics of the list returned by this method become undefined if
   * the backing list (i.e., this list) is <i>structurally modified</i> in
   * any way other than via the returned list.  (Structural modifications are
   * those that change the size of this list, or otherwise perturb it in such
   * a fashion that iterations in progress may yield incorrect results.)
   *
   * @param fromIndex low endpoint (inclusive) of the subList.
   * @param toIndex high endpoint (exclusive) of the subList.
   * @return a view of the specified range within this list.
   *
   * @throws IndexOutOfBoundsException for an illegal endpoint index value
   *     (fromIndex &lt; 0 || toIndex &gt; size || fromIndex &gt; toIndex).
   */
  public List subList(int fromIndex, int toIndex) { return recipientsListObj.subList(fromIndex,toIndex); }
}
