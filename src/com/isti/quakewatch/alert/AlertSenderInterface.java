//AlertSenderInterface.java:  Defines an alert sender interface.
//
//   6/8/2005 -- [KF]  Initial version.
//  1/19/2006 -- [KF]  Moved from 'QWCapOutClient' to 'QWCommon'.
//

package com.isti.quakewatch.alert;

/**
 * Interface AlertSenderInterface defines an alert sender interface.
 */
public interface AlertSenderInterface
{
  /**
   * Returns a display string for the alert sender.
   * @return a display string for the alert sender.
   */
  public String getDisplayStr();

  /**
   * Returns the maximum number of retries to attempt when the sending
   * of an alert message is failing.
   * @return The maximum number of send alert message retries after failure.
   */
  public int getMaxSendMessageRetries();

  /**
   * Sends an alert message using the given parameters.
   * If the previous send attempt failed then this method may perform
   * a wait-delay before sending the message.
   * @param recipObj the alert recipient for the message.
   * @param alertMsgObj the alert message object.
   * @return true if successful; false if not (in which case a warning
   * message will be logged).
   */
  public boolean sendMessage(
      AlertRecipient recipObj, Object alertMsgObj);
}
