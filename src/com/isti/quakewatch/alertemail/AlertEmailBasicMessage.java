//AlertEmailBasicMessage.java:  Defines an alert email basic message.
//
//  6/22/2005 -- [KF]  Initial version.
//  1/19/2006 -- [KF]  Moved from 'QWCapOutClient' to 'QWCommon'.
//

package com.isti.quakewatch.alertemail;

/**
 * Class AlertEmailBasicMessage defines an alert email basic message.
 */
public class AlertEmailBasicMessage implements AlertEmailMessageInterface
{
    /** Text for the subject of the email message. */
  private final String subjectTextString;
    /** Text for the body of the email message. */
  private final String messageTextString;

  /**
   * Creates an alert email basic message.
   * @param subjectTextString the subject text string.
   * @param messageTextString the message text string.
   */
  public AlertEmailBasicMessage(String subjectTextString, String messageTextString)
  {
    this.subjectTextString = subjectTextString;
    this.messageTextString = messageTextString;
  }

  /**
   * Returns the text for the subject of the email message.
   * @return The text for the subject of the email message.
   */
  public String getSubjectTextString()
  {
    return subjectTextString;
  }

  /**
   * Returns the text for the body of the email message.
   * @return The text for the body of the email message.
   */
  public String getMessageTextString()
  {
    return messageTextString;
  }
}
