//AlertEmailMessage.java:  Defines an alert email message.
//
//  6/22/2005 -- [KF]  Initial version.
//  1/19/2006 -- [KF]  Moved from 'QWCapOutClient' to 'QWCommon'.
//   4/3/2006 -- [ET]  Added debug-log output if unable to generate
//                     distance-to-closest-locations info.
//  5/19/2008 -- [ET]  Fixed issue where miles/kilometers-distance values
//                     were wrong when a non-US locale was in use (modified
//                     to use 'LocationDistanceInformation' method
//                     'getDistMeters()' instead of 'getDistance()' method).
//   3/7/2019 -- [KF]  Modified to not include null version.
//

package com.isti.quakewatch.alertemail;

import com.isti.util.UtilFns;
import com.isti.util.LogFile;
import com.isti.util.gis.LocationPlacesInterface;
import com.isti.util.gis.LocationDistanceInformation;

import com.isti.quakewatch.util.QWUtils;
import com.isti.quakewatch.util.SeismicNetworkInfo;
import com.isti.quakewatch.message.QWEventMsgRecord;
import com.isti.quakewatch.message.QWEQEventMsgRecord;
import com.isti.quakewatch.alert.AlertMessage;
import com.isti.quakewatch.alert.AlertRecipient;
import com.isti.quakewatch.alert.AlertMsgDateFormatterInterface;

/**
 * Class AlertEmailMessage defines an alert email message.
 */
public class AlertEmailMessage extends AlertMessage
    implements AlertEmailMessageInterface
{
    /** Spacer string for "principal earthquake parameters" section. */
  private static final String HORIZ_SPACER_STR = "                    ";
    /** String containing the newline character. */
  public static final String NLSTR = "\n";
    /** String containing ", ". */
  public static final String CSSTR = ", ";

    /** "Earthquake Message" string. */
  public static final String EQ_MESSAGE_STR = "Earthquake Message";
    /** "Earthquake Deleted" string. */
  public static final String EQ_DELETED_STR = "Earthquake Deleted";
    /** "Preliminary Earthquake Report" header string; centered. */
  public static final String PRELIM_EQREP_HDRSTR =
         UtilFns.createCenteredString("== PRELIMINARY EARTHQUAKE REPORT ==",
         SeismicNetworkInfo.CENTER_FIELD_LEN);
    /** "Earthquake Report Cancellation" header string; centered. */
  public static final String CANCELLED_EQREP_HDRSTR =
        UtilFns.createCenteredString("== EARTHQUAKE REPORT CANCELLATION ==",
        SeismicNetworkInfo.CENTER_FIELD_LEN);
    /** "This report supersedes..." string. */
  public static final String REP_SUPERSEDES_STR =
             "This report supersedes any earlier reports about this event.";
    /** "This is a computer-generated message..." string. */
  public static final String NOT_REVIEWED_STR =
              "This is a computer-generated message and has not yet been " +
                "reviewed by a" + NLSTR + "seismologist.";
    /** "This event has been reviewed..." string. */
  public static final String EVENT_REVIEWED_STR =
                          "This event has been reviewed by a seismologist.";
    /** "The duty seismologist has deleted event" string. */
  public static final String EVENT_DELETED_STR =
                                 "The duty seismologist has deleted event ";
    /** "Principal Earthquake Parameters" string. */
  public static final String PRINCIPAL_EQPARAMS_STR =
                                          "PRINCIPAL EARTHQUAKE PARAMETERS";
    /** "_______________________________" string. */
  public static final String UNDERLINE_32CHARS_STR =
                                          "_______________________________";
    /** "More information about..." string. */
  public static final String MORE_INFOABOUT_STR = "More information " +
                   "about this event and other earthquakes is available at";
    /** "Additional Earthquake Parameters" string. */
  public static final String ADDITIONAL_EQPARAMS_STR =
                                         "ADDITIONAL EARTHQUAKE PARAMETERS";
    /** "Source of Information/Contacts" string. */
  public static final String SOURCE_CONTACTS_STR =
                                           "SOURCE OF INFORMATION/CONTACTS";
    /** " Quake Report" string. */
  public static final String QUAKE_REPORT_STR = " Quake Report";
    /** " Quake Deleted" string. */
  public static final String QUAKE_DELETED_STR = " Quake Deleted";

    /** Handle to the last long-format alert email message created. */
  private static AlertEmailMessage lastLongMessageObj = null;
    /** Thread-synchronization object for 'lastLongMessageObj'. */
  private static final Object lastLongMsgSyncObj = new Object();
    /** Handle to the last short-format alert email message created. */
  private static AlertEmailMessage lastShortMessageObj = null;
    /** Thread-synchronization object for 'lastShortMessageObj'. */
  private static final Object lastShortMsgSyncObj = new Object();

    /** Format flag for message; true = "long", false = "short". */
  public final boolean longMsgFormatFlag;
    /** Text for the subject of the email message. */
  private String subjectTextString;
    /** Text for the body of the email message. */
  private String messageTextString;
    /** Text for test messages or empty string if not using test messages. */
  private final String subjectTestTextString;

  /**
   * Creates an alert email message.
   * @param logObj the 'LogFile' object.
   * @param emailSenderObj 'EmailSenderInterface' object.
   * @param messageType message type value for message.
   * @param eventMsgRecObj event-message-record object for message.
   * @param longMsgFormatFlag format flag for message; true = "long",
   * false = "short".
   * @param networkInfoObj seismic-network information for building
   * the message.
   */
  protected AlertEmailMessage(
      LogFile logObj, AlertEmailSenderInterface emailSenderObj,
      int messageType, QWEventMsgRecord eventMsgRecObj,
      boolean longMsgFormatFlag,
      SeismicNetworkInfo networkInfoObj)
  {
    super(logObj,messageType,eventMsgRecObj,networkInfoObj);
    this.longMsgFormatFlag = longMsgFormatFlag;

   //get the alert message date formatter
   final AlertMsgDateFormatterInterface alertMsgDateFormat =
       emailSenderObj.getAlertMsgDateFormatter();
   //get the location places
   final LocationPlacesInterface locationPlacesObj =
      emailSenderObj.getLocationPlaces();


    if (emailSenderObj.isTestMode()) //if test messages should be used
      subjectTestTextString = " - TEST";
    else
      subjectTestTextString = UtilFns.EMPTY_STRING;

         //get event ID string (if 'QWEQEventMsgRecord' type
         // then use ID that has data-source code removed):
    final String recIDStr = (eventMsgRecObj instanceof QWEQEventMsgRecord) ?
                         ((QWEQEventMsgRecord)eventMsgRecObj).getEventID() :
                                             eventMsgRecObj.getEventIDKey();
         //build event-ID display string:
    final String eventIDStr;
    if(recIDStr != null && recIDStr.length() > 0)
    {    //event-ID-key string contains data
              //get 2-letter data-source code (convert null to ""):
      final String dSrcStr = (eventMsgRecObj.getDataSource() != null) ?
                      eventMsgRecObj.getDataSource() : UtilFns.EMPTY_STRING;
              //build event-ID string with data-source code; if
              // event ID starts with lower-case letter then make
              // data-source be upper case; otherwise lower case:
      eventIDStr = (Character.isLowerCase(recIDStr.charAt(0)) ?
                  dSrcStr.toUpperCase() : dSrcStr.toLowerCase()) + recIDStr;
    }
    else      //event-ID-key string contains no data
      eventIDStr = null;
         //build version of event-ID string that shows "???" if none:
    final String eventIDQStr = (eventIDStr != null) ? eventIDStr : "???";

    if(longMsgFormatFlag)
    {    //create long-format message
              //setup subject text for message:
      if(messageType != AlertRecipient.MSG_CANCEL_TYPE)
      {  //message type is not "cancel"
        subjectTextString = evtMagValTypeStr + " " +
                                      networkInfoObj.shortMsgDescStr + " " +
                                          EQ_MESSAGE_STR;
      }
      else
      {  //message type is "cancel"
        subjectTextString = evtMagValTypeStr + " " +
                                      networkInfoObj.shortMsgDescStr + " " +
                                          EQ_DELETED_STR;
      }

         //setup string buffer for message-body text:
      final StringBuffer buff = new StringBuffer();

              //start body text with "PRELIMINARY EARTHQUAKE REPORT" (or
              // "EARTHQUAKE REPORT CANCELLATION" if type is "cancel"):
      buff.append(((messageType != AlertRecipient.MSG_CANCEL_TYPE) ?
                                    PRELIM_EQREP_HDRSTR :
                        CANCELLED_EQREP_HDRSTR) + NLSTR);
              //add message-header information for seismic network:
      buff.append(networkInfoObj.messageHeaderStr + NLSTR + NLSTR);

      if (subjectTestTextString.length() > 0)
      {
        buff.append("== This report is for technical testing only, all recipients disregard. ==" +
            NLSTR + NLSTR);
      }
      if(messageType != AlertRecipient.MSG_CANCEL_TYPE)
      {  //message type is not "cancel"
    	String version = eventMsgRecObj.getVersion();
    	// if version is available
    	if (version != null && version.length() > 0)
    	{
            //add "Version X:  ":
    		buff.append("Version ");
    		buff.append(version);
    		buff.append(":  ");
    	}
    		  //add "This report supersedes...":
        buff.append(REP_SUPERSEDES_STR + NLSTR + NLSTR);
              //add "This event has been reviewed..." if verified event;
              // add "This is a computer-generated message..." if not:
        buff.append((eventMsgRecObj.isVerified() ?
                                     EVENT_REVIEWED_STR :
                      NOT_REVIEWED_STR) + NLSTR + NLSTR);
      }
      else
      {  //message type is "cancel"
              //add "The duty seismologist has deleted event...":
        buff.append(EVENT_DELETED_STR + eventIDQStr + "." + NLSTR + NLSTR);
      }
      if(messageType != AlertRecipient.MSG_CANCEL_TYPE)
      {  //message type is not "cancel"
              //add "PRINCIPAL EARTHQUAKE PARAMETERS" and underline:
        buff.append(PRINCIPAL_EQPARAMS_STR + NLSTR +
                  UNDERLINE_32CHARS_STR + NLSTR + NLSTR);
              //add "Magnitude" line:
        buff.append("Magnitude        :  " + eventMagValStr +
                                                    " M" + eventMagTypeStr +
             (eventMsgRecObj.isQuarry() ? " (A probable quarry explosion)" :
               QWUtils.getMagnitudeDescStr(eventMsgRecObj.getMagnitude())) +
                                                                     NLSTR);
              //add event time:
        buff.append("Event Date & Time:  " +     //local time zone
            alertMsgDateFormat.formatLongMsgDate(eventMsgRecObj.getTime(),false) +
                                                                     NLSTR);
        buff.append(HORIZ_SPACER_STR +           //GMT time zone
             alertMsgDateFormat.formatLongMsgDate(eventMsgRecObj.getTime(),true) +
                                                                     NLSTR);
              //add location coordinates:
        buff.append("Coordinates      :  " +     //decimal N/S format:
                            eventMsgRecObj.getLatitudeString(true) + CSSTR +
                           eventMsgRecObj.getLongitudeString(true) + NLSTR);
        buff.append(HORIZ_SPACER_STR +           //deg/min N/S format:
                       eventMsgRecObj.getLatitudeString(true,true) + CSSTR +
                      eventMsgRecObj.getLongitudeString(true,true) + NLSTR);
              //add event depth, in kilometers and miles:
        buff.append("Depth            :  " +
                            eventMsgRecObj.getDepthString(false) + " km (" +
                           eventMsgRecObj.getDepthString(true) + " miles)" +
                                                             NLSTR + NLSTR);
              //add distance-to-closest-locations information:
        final LocationDistanceInformation [] infoArr;
        if((infoArr=locationPlacesObj.getLocationDistInfo(
                 eventMsgRecObj.getLatitude(),eventMsgRecObj.getLongitude(),
                                        eventMsgRecObj.isQuarry())) != null)
        {     //location-distance info available
          LocationDistanceInformation distInfoObj;
          double kmVal;
          int mileVal;
          for(int i=0; i<infoArr.length; ++i)
          {
            distInfoObj = infoArr[i];
            kmVal = distInfoObj.getDistMeters() / 1000.0;
            mileVal = (int)(kmVal*QWUtils.KM_TO_MILE + 0.5);
            buff.append(mileVal + " mile" + ((mileVal!=1)?"s":"") + " (" +
                                                (int)(kmVal+0.5) + " km) " +
                                      distInfoObj.getAzimString() + " of " +
                                             distInfoObj.getName() + NLSTR);
          }
          buff.append(NLSTR);
        }
        else if(logObj != null)
        {     //location-distance info not available
          logObj.debug("AlertEmailMessage:  Unable to generate " +
                           "distance-to-closest-locations info for event " +
                                            eventMsgRecObj.getEventIDKey());
        }

        //setup to follow these rules:
        //  if (horiz_err=0 and vert_err=0) then
        //   horiz_err=N/A
        //   vert_err=N/A
        //   if (rms=0) set to N/A
        //   if (dmin=0) set to N/A
        //  else
        //    if (netcode=US and vert_err=0)
        //      vert_err='Depth fixed by seismologist'
        //    endif
        //  endif
        //  if (Number of phases=0) set to N/A  ! always
        //  if (max_gap=0) set to N/A  ! always

        final Double horizErrValObj =       //get value object for horiz err
                            (eventMsgRecObj instanceof QWEQEventMsgRecord) ?
           ((QWEQEventMsgRecord)eventMsgRecObj).getHorizontalErrorValObj() :
                                                                       null;
        final Double vertErrValObj =        //get value object for vert err:
                            (eventMsgRecObj instanceof QWEQEventMsgRecord) ?
             ((QWEQEventMsgRecord)eventMsgRecObj).getVerticalErrorValObj() :
                                                                       null;

              //set flag if vertical error not given:
        final boolean noVertErrFlag =
              (vertErrValObj == null || vertErrValObj.doubleValue() <= 0.0);
              //set flag if both horizontal and vertical error not given:
        final boolean noHorizVertErrValsFlag = (noVertErrFlag &&
           (horizErrValObj == null || horizErrValObj.doubleValue() <= 0.0));
              //if horiz/vert-error not given then setup "N/A" string:
        final String nhveNAStr = noHorizVertErrValsFlag ? NASTR : null;
              //if vertical-error not given, horizontal error given
              // and data source is "US" then setup special message:
        final String vertErrNAStr = (noVertErrFlag &&
                                                  !noHorizVertErrValsFlag &&
                    "us".equalsIgnoreCase(eventMsgRecObj.getDataSource())) ?
                                  "Depth fixed by seismologist" : nhveNAStr;

              //add "More information about..."
        buff.append(MORE_INFOABOUT_STR + NLSTR);
              //add more-info link string:
        buff.append(networkInfoObj.moreInfoLinkStr + NLSTR + NLSTR);
              //add "ADDITIONAL EARTHQUAKE PARAMETERS" and underline:
        buff.append(ADDITIONAL_EQPARAMS_STR + NLSTR +
                  UNDERLINE_32CHARS_STR + NLSTR + NLSTR);
        if(eventMsgRecObj instanceof QWEQEventMsgRecord)
        {
              //add number-of-phase value:
          buff.append("Number of phases           :  " + showValOrNA(
                   ((QWEQEventMsgRecord)eventMsgRecObj).getNumPhasesDispStr(
                                                           NASTR)) + NLSTR);
              //add RMS-time-error value:
          buff.append("RMS misfit                 :  " + showValOrNA(
                ((QWEQEventMsgRecord)eventMsgRecObj).getRMSTimeErrorDispStr(
                                                       nhveNAStr)) + NLSTR);
              //add horizontal-error value:
          buff.append("Horizontal location error  :  " + showValOrNA(
             ((QWEQEventMsgRecord)eventMsgRecObj).getHorizontalErrorDispStr(
                                                       nhveNAStr)) + NLSTR);
              //add vertical-error value:
          buff.append("Vertical location error    :  " + showValOrNA(
               ((QWEQEventMsgRecord)eventMsgRecObj).getVerticalErrorDispStr(
                                                    vertErrNAStr)) + NLSTR);
              //add azimuthal-gap value:
          buff.append("Maximum azimuthal gap      :  " + showValOrNA(
                ((QWEQEventMsgRecord)eventMsgRecObj).getAzimuthalGapDispStr(
                                                           NASTR)) + NLSTR);
              //add minimum-distance value:
          buff.append("Distance to nearest station:  " + showValOrNA(
             ((QWEQEventMsgRecord)eventMsgRecObj).getMinimumDistanceDispStr(
                                                       nhveNAStr)) + NLSTR);
        }
              //add event-ID value:
        buff.append("Event ID                   :  " +
                                   showValOrNA(eventIDStr) + NLSTR + NLSTR);
      }
              //add "SOURCE OF INFORMATION/CONTACTS" and underline:
      buff.append(SOURCE_CONTACTS_STR + NLSTR +
                  UNDERLINE_32CHARS_STR + NLSTR + NLSTR);
              //add source-of-info lines:
      buff.append(networkInfoObj.sourceOfInfoStr + NLSTR + NLSTR);
              //add underline:
      buff.append(UNDERLINE_32CHARS_STR + NLSTR + NLSTR);
              //add "You are receiving...":
      buff.append("You are receiving this \"" +
                  getMsgTypeTagStr(messageType) + "\" message via " +
                  emailSenderObj.getProgramRevisionText() +
                  ", configured to send mail from " +
                  emailSenderObj.getDisplayStr() + NLSTR);
              //set message-body-text to string version of buffer:
      messageTextString = buff.toString();
      //save handle to this message:
      synchronized (lastLongMsgSyncObj)
      { //grab thread-synchronization object for last-message handle
        lastLongMessageObj = this;
      }
    }
    else
    {    //create short-format message
              //setup "From:" string:
      final String fromStr = "From: " + networkInfoObj.shortMsgDescStr +
                                                                      NLSTR;
              //setup subject text for message:
      if(messageType != AlertRecipient.MSG_CANCEL_TYPE)
      {  //message type is not "cancel"
        subjectTextString = MSTR + eventMagValStr +
                                        QUAKE_REPORT_STR;
              //build distance-to-closest-location information:
        final String locStr;
        final LocationDistanceInformation distInfoObj;
        if((distInfoObj=locationPlacesObj.getShortMsgLocDistInfo(
                 eventMsgRecObj.getLatitude(),eventMsgRecObj.getLongitude(),
                                        eventMsgRecObj.isQuarry())) != null)
        {     //location-distance info available
          locStr = ((int)(distInfoObj.getDistMeters()/1000.0*
                                                  QWUtils.KM_TO_MILE+0.5)) +
                                      " mi " + distInfoObj.getAzimString() +
                                     " of " + distInfoObj.getName() + CSSTR;
        }
        else  //location-distance info not available
          locStr = "";
              //build message-body text:
        messageTextString = fromStr + MSTR + eventMagTypeStr +
            eventMagValStr + CSSTR + locStr +
            alertMsgDateFormat.formatShortMsgDate(eventMsgRecObj.getTime());
      }
      else
      {  //message type is "cancel"
        subjectTextString = MSTR + eventMagValStr +
                                       QUAKE_DELETED_STR;
              //add "From:" & "The duty seismologist has deleted event...":
        messageTextString = fromStr +
                         EVENT_DELETED_STR + eventIDQStr;
      }
         //save handle to this message:
      synchronized(lastShortMsgSyncObj)
      {  //grab thread-synchronization object for last-message handle
        lastShortMessageObj = this;
      }
    }
  }

  /**
   * Returns the text for the subject of the email message.
   * @return The text for the subject of the email message.
   */
  public String getSubjectTextString()
  {
    return subjectTextString + subjectTestTextString;
  }

  /**
   * Sets the text for the subject of the email message.
   * @param s The text for the subject of the email message.
   */
  protected void setSubjectTextString(String s)
  {
    subjectTextString = s;
  }

  /**
   * Returns the text for the body of the email message.
   * @return The text for the body of the email message.
   */
  public String getMessageTextString()
  {
    return messageTextString;
  }

  /**
   * Sets the text for the body of the email message.
   * @param s The text for the body of the email message.
   */
  protected void setMessageTextString(String s)
  {
    messageTextString = s;
  }

  /**
   * Returns an indicator of whether or not the given parameters match
   * those used to create this alert email message.
   * @param messageType message type value for message.
   * @param eventMsgRecObj event-message-record object for message.
   * @param longMsgFormatFlag format flag for message; true = "long",
   * false = "short".
   * @return true if the given parameters match those used to create
   * this alert email message; false if not.
   */
  public boolean isSameParams(int messageType, QWEventMsgRecord eventMsgRecObj,
                              boolean longMsgFormatFlag)
  {
    return (this.messageType == messageType &&
                               this.eventMsgRecObj.equals(eventMsgRecObj) &&
                               this.longMsgFormatFlag == longMsgFormatFlag);
  }

  /**
   * Returns a string containing a summary of the message.
   * @return A string containing a summary of the message.
   */
  public String toString()
  {
    return "MessageType=\"" + getMsgTypeTagStr() + "\", Event=\"" +
                       eventMsgRecObj.getDisplayString() + "\", Format=\"" +
                                  (longMsgFormatFlag?"long":"short") + "\"";
  }

  /**
   * Creates an alert email message.  If the parameters match those used
   * to create the previous alert email message then the previous object
   * will be reused.
   * @param logObj the 'LogFile' object.
   * @param emailSenderObj 'AlertEmailSenderInterface' object.
   * @param messageType message type value for message.
   * @param eventMsgRecObj event-message-record object for message.
   * @param longMsgFormatFlag format flag for message; true = "long",
   * false = "short".
   * @return An 'AlertEmailMessage' for the given parameters, or null
   * if the network data-source code for the given event-message record
   * is not supported.
   */
  public static AlertEmailMessage createAlertEmailMessage(
      LogFile logObj, AlertEmailSenderInterface emailSenderObj,
      int messageType, QWEventMsgRecord eventMsgRecObj,
      boolean longMsgFormatFlag)
  {
    if(longMsgFormatFlag)
    {    //using "long" format
      synchronized(lastLongMsgSyncObj)
      {  //grab thread-synchronization object for last-message handle
        if(lastLongMessageObj != null && lastLongMessageObj.isSameParams(
            messageType,eventMsgRecObj,longMsgFormatFlag))
        {     //last message exists and used same params as this one
          return lastLongMessageObj;        //return previous message object
        }
      }
    }
    else
    {    //using "short" format
      synchronized(lastShortMsgSyncObj)
      {  //grab thread-synchronization object for last-message handle
        if(lastShortMessageObj != null && lastShortMessageObj.isSameParams(
            messageType,eventMsgRecObj,longMsgFormatFlag))
        {     //last message exists and used same params as this one
          return lastShortMessageObj;       //return previous message object
        }
      }
    }
         //get message information for seismic network:
    final SeismicNetworkInfo networkInfoObj;
    if((networkInfoObj=getNetworkInfo(eventMsgRecObj)) != null)
    {    //message information fetched OK; create email-message object
      return new AlertEmailMessage(
          logObj,emailSenderObj,
          messageType,eventMsgRecObj,
          longMsgFormatFlag,networkInfoObj);
    }
         //seismic-network data-source code not supported
    return null;
  }
}
