//AlertEmailMessageInterface.java:  Defines an alert email message.
//
//  6/22/2005 -- [KF]  Initial version.
//  1/19/2006 -- [KF]  Moved from 'QWCapOutClient' to 'QWCommon'.
//

package com.isti.quakewatch.alertemail;

/**
 * Class EmailMessageInterface defines an alert email message.
 */
public interface AlertEmailMessageInterface
{
  /**
   * Returns the text for the subject of the email message.
   * @return The text for the subject of the email message.
   */
  public String getSubjectTextString();

  /**
   * Returns the text for the body of the email message.
   * @return The text for the body of the email message.
   */
  public String getMessageTextString();
}
