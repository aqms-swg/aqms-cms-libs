//AlertEmailRecipient.java:  Defines a QuakeWatch Emailer alert recipient.
//
//  6/22/2005 -- [KF]  Initial version.
//  8/25/2005 -- [KF]  Added the recipient ID to the thread names.
//   9/6/2005 -- [KF]  Cleanup log messages.
//  1/19/2006 -- [KF]  Moved from 'QWCapOutClient' to 'QWCommon'.
//  1/31/2006 -- [KF]  Added log message for RecipientSetupException,
//                     Added 'init()' method.
//  2/13/2006 -- [KF]  Added 'setSendEnabled()' method.
//  3/17/2006 -- [KF]  Moved 'alarmCfgProps' to 'AlertRecipient'.
//  3/31/2006 -- [ET]  Modified to put global-alarm-enabled-flag-property
//                     object after other items; modified method
//                     'sendAlertMessage()' to check if SMTP email
//                     server is setup.
//  5/17/2006 -- [ET]  Modified log-output level from 'info' to 'debug'
//                     in 'sendAlertMessage()' method.
//

package com.isti.quakewatch.alertemail;

import org.jdom.Element;

import com.isti.util.UtilFns;
import com.isti.util.CfgPropItem;
import com.isti.util.queue.NotifyEventQueue;

import com.isti.quakewatch.util.QWProgramInformationInterface;
import com.isti.quakewatch.message.QWEventMsgRecord;
import com.isti.quakewatch.alert.AlertSenderInterface;
import com.isti.quakewatch.alert.AlertRecipient;

/**
 * Class AlertEmailRecipient defines a QuakeWatch Emailer alert recipient.
 * NOTE: Every subclass must have a constructor with a
 * 'QWProgramInformationInterface' and 'Element' parameter.
 */
public class AlertEmailRecipient extends AlertRecipient
    implements AlertEmailRecipientInterface
{
         /** EmailSenderInterface object. */
  protected final AlertEmailSenderInterface alertEmailSenderObj;
         /** Queue for alert-email messages. */
  protected NotifyEventQueue alertEmailMessageQueue;
         /** Counter for number of alert messages have been sent. */
  protected long numberOfAlertsCount = 0;
         /** List of one or more email addresses for recipient. */
  public final CfgPropItem emailAddressListProp = cfgProps.add(
                               "emailAddressList",UtilFns.EMPTY_STRING,null,
                     "Email address(es)").setGroupSelObj(GENERAL_GROUP_STR);
         /** Flag set true to enable sending of emails to recipient. */
  public final CfgPropItem emailsEnabledFlagProp = cfgProps.add(
                                      "emailsEnabledFlag",Boolean.TRUE,null,
                                 "Enable sending emails to this recipient").
                                          setGroupSelObj(GENERAL_GROUP_STR);
         /** Flag set true to enable "long" message format for emails. */
  public final CfgPropItem longEmailFormatFlagProp = cfgProps.add(
                                     "longEmailFormatFlag",Boolean.TRUE,null,
                                       "Use \"long\" email message format").
                                          setGroupSelObj(GENERAL_GROUP_STR);

  /**
   * Creates an new recipient, with fields filled via the given
   * "Recipient" element object.
   * @param progInfoObj the 'QWProgramInformationInterface' object.
   * @param recipientElementObj the recipient object to use.
   * @throws RecipientSetupException if an error occurs while loading
   * fields from the element object.
   */
  public AlertEmailRecipient(
      QWProgramInformationInterface progInfoObj,Element recipientElementObj)
      throws RecipientSetupException
  {
    this(progInfoObj,recipientElementObj,true);
  }

  /**
   * Creates an new recipient, with fields filled via the given
   * "Recipient" element object.
   * @param progInfoObj the 'QWProgramInformationInterface' object.
   * @param recipientElementObj the recipient object to use.
   * @param initFlag true to initialize the recipient, false otherwise.
   * @throws RecipientSetupException if an error occurs while loading
   * fields from the element object.
   * NOTE: If the recipient is not intialized via the constructor (the
   * 'initFlag' is false) the 'init()' method should be called.
   * @see init
   */
  protected AlertEmailRecipient(
      QWProgramInformationInterface progInfoObj,Element recipientElementObj,
      boolean initFlag)
      throws RecipientSetupException
  {
    super(progInfoObj, recipientElementObj);
         //if global-alarm-enabled-flag property is in use then
         // move it to end of list by removing and adding it:
    if(globalAlarmEnabledFlagProp != null &&
                        cfgProps.remove(globalAlarmEnabledFlagProp) != null)
    {
      cfgProps.add(globalAlarmEnabledFlagProp);
    }
    this.alertEmailSenderObj = getAlertEmailSender(progInfoObj);
    if (initFlag)
    {
      init(recipientElementObj);
    }
  }

  /**
   * Initialize the recipient.
   * @param recipientElementObj the recipient object to use.
   * @throws RecipientSetupException if an error occurs while loading
   * fields from the element object.
   */
  protected final void init(Element recipientElementObj)
      throws RecipientSetupException
  {
    loadFromElement(recipientElementObj);
    setupMessageQueue(true);      //setup queue for alert-email messages
  }

  /**
   * Creates an "empty" recipient, with all fields set to default values.
   * @param progInfoObj the 'QWProgramInformationInterface' object.
   * @throws RecipientSetupException if an error occurs.
   */
  public AlertEmailRecipient(QWProgramInformationInterface progInfoObj)
      throws RecipientSetupException
  {
    super(progInfoObj);
         //if global-alarm-enabled-flag property is in use then
         // move it to end of list by removing and adding it:
    if(globalAlarmEnabledFlagProp != null &&
                        cfgProps.remove(globalAlarmEnabledFlagProp) != null)
    {
      cfgProps.add(globalAlarmEnabledFlagProp);
    }
    this.alertEmailSenderObj = getAlertEmailSender(progInfoObj);
         //setup queue for alert-email messages (don't start
         // message-processing thread until recipient is committed:
    setupMessageQueue(false);
  }

  /**
   * Gets the 'AlertEmailSenderInterface'.
   * @param progInfoObj QWProgramInformationInterface
   * @return the 'AlertEmailSenderInterface'.
   * @throws RecipientSetupException if an error occurs.
   */
  public static AlertEmailSenderInterface getAlertEmailSender(
                                  QWProgramInformationInterface progInfoObj)
                                              throws RecipientSetupException
  {
    final AlertSenderInterface alertSender =
        progInfoObj.getAlertSender(AlertEmailSenderInterface.class);
    if (alertSender instanceof AlertEmailSenderInterface)
      return (AlertEmailSenderInterface)alertSender;
    throw new RecipientSetupException(
                               "Unable to find 'AlertEmailSenderInterface'",
                                        progInfoObj.getProgramLogFileObj());
  }

  /**
   * Creates an new recipient, with all fields set equal to those in the
   * given recipient.
   * @param sourceRecipientObj the recipient object to use.
   * @throws RecipientSetupException if an error occurs.
   */
  public AlertEmailRecipient(
      AlertEmailRecipient sourceRecipientObj) throws RecipientSetupException
  {
    super(sourceRecipientObj);
    this.alertEmailSenderObj = sourceRecipientObj.alertEmailSenderObj;
    setupMessageQueue(true);      //setup queue for alert-email messages
  }

  /**
   * Returns the list of email addresses for this recipient.
   * @return A string containing the list of email addresses
   * for this recipient.
   */
  public String getEmailAddressListStr()
  {
    return emailAddressListProp.stringValue();
  }

  /**
   * Sets up the queue for transmitting alert-email messages.
   * @param startThreadFlag true to start the message-processing thread.
   */
  protected final void setupMessageQueue(boolean startThreadFlag)
  {
         //define queue and processing method:
    alertEmailMessageQueue = new NotifyEventQueue(
      getRecipID()+"AlertEmailMessageQueue")
        {
          /** Executing method for queue-processing thread. */
          public void run()
          {
            try
            {
              logObj.debug2(
                  getLogPrefixText() +
                  "Starting alert email message queue processing loop");
              Object obj;
              while (!finishRunning())
              {    //loop while processing messages, until terminated
                if((obj=waitForEvent()) instanceof AlertEmailMessageInterface)
                  transmitAlertEmailMessage((AlertEmailMessageInterface)obj);
              }
              logObj.debug2(
                  getLogPrefixText() +
                  "Exiting alert email message queue processing loop");
            }
            catch(Exception ex)
            {      //some kind of error; log it
              logObj.warning(
                  getLogPrefixText() +
                 "Error processing alert-email-message queue item:  " + ex);
              logObj.warning(UtilFns.getStackTraceString(ex));
            }
          }
        };
    if(startThreadFlag)                     //if flag then
      alertEmailMessageQueue.startThread(); //start the msg processing thread
  }

  /**
   * Terminates the processing thread for the queue for transmitting
   * alert-email messages.
   */
  public void terminateMsgQueueThread()
  {
    alertEmailMessageQueue.stopThread();
  }

  /**
   * Returns an ID string for this recipient.
   * @return An ID string for this recipient.
   */
  public String getRecipID()
  {
    String str;
    if((str=recipientNameProp.stringValue().trim()).length() > 0)
      return str;
    if((str=emailAddressListProp.stringValue().trim()).length() > 40)
      return str.substring(0,40).trim() + "...";
    return str;
  }

  /**
   * Returns a string representation of this recipient.
   * @return A new string in the following format:
   * "name" <email-list>.
   */
  public String toString()
  {
    int cnt = 0;
    String nameStr,emailStr;
         //get name string:
    if((nameStr=recipientNameProp.stringValue().trim()).length() > 0)
    {    //string contains data; surround value by quotes
      nameStr = "\"" + nameStr + "\"";
      ++cnt;       //increment item count
    }
         //get email-list string:
    if((emailStr=emailAddressListProp.stringValue().trim()).length() > 0)
    {    //string contains data; surround value by brackets
      emailStr = "<" + emailStr + ">";
      ++cnt;       //increment item count
    }
    //get # of alarm regions for recipient
    final int numRegions = getNumRegions();
         //return strings; if both present then insert space separator:
    return (isSendEnabled()?"":"(disabled) ") + nameStr +
                          ((cnt >= 2)?" ":UtilFns.EMPTY_STRING) + emailStr +
                                     " [" + numberOfAlertsCount + " alert" +
                 ((numberOfAlertsCount!=1)?"s":UtilFns.EMPTY_STRING) + "]" +
                                                            ((numRegions>0)?
                     (" ("+numRegions+" region"+((numRegions!=1)?"s)":")")):
                                                      UtilFns.EMPTY_STRING);
  }

  /**
   * Returns the status of whether or not sending emails is enabled for this
   * recipient.
   * @return true if sending emails is enabled; false if not.
   */
  public boolean isSendEnabled()
  {
    return !recipDeletedFlag && emailsEnabledFlagProp.booleanValue();
  }

  /**
   * Returns the alert-message-queue object for this recipient.
   * @return The alert-message-queue object for this recipient.
   */
  public NotifyEventQueue getAlertMessageQueue()
  {
    return alertEmailMessageQueue;
  }

  /**
   * Generates an alert email message for the given event-message
   * record.
   * @param messageType message type value for message.
   * @param recObj the message-record object for the event.
   */
  public void sendAlertMessage(QWEventMsgRecord recObj, int messageType)
  {
    logObj.debug(
      getLogPrefixText() +
      AlertEmailMessage.getMsgTypeTagStr(messageType) +
      " alert email triggered for event:  " + recObj.getDisplayString());
    try
    {
      final AlertEmailMessageInterface emailMsgObj;
      if((emailMsgObj=createAlertEmailMessage(recObj,messageType)) != null)
      {  //alert email message created OK
        if(alertEmailSenderObj.getJavaMailUtilObj().
                           smtpServerAddressProp.stringValue().length() > 0)
        {     //SMTP server for email is setup
          pushAlertEmailMsgToQueue(emailMsgObj);
        }
        else
        {     //SMTP server for email is not setup
          logObj.warning(getLogPrefixText() +
                                        "Unable to generate alert email; " +
                                           "SMTP server address not setup");
        }
      }
      else
      {  //unable to create alert email message
        logObj.warning(
            getLogPrefixText() +
            "Unable to generate alert email; network " +
            "data-source code not supported; event:  " +
            recObj.getDisplayString());
      }
    }
    catch(Exception ex)
    {    //some kind of exception error; log it
      logObj.warning(
          getLogPrefixText() +
          "Error generating alert email:  " + ex);
      logObj.warning(UtilFns.getStackTraceString(ex));
    }
  }

  /**
   * Sets the status of whether or not sending is enabled for this
   * recipient.
   * @param b true if sending is enabled; false if not.
   */
  public void setSendEnabled(boolean b)
  {
    emailsEnabledFlagProp.setValue(b);
  }

  /**
   * Creates an alert email message.  If the parameters match those used
   * to create the previous alert email message then the previous object
   * will be reused.
   * @param messageType message type value for message.
   * @param recObj the message-record object for the event.
   * @return An 'AlertEmailMessage' for the given parameters, or null
   * if the network data-source code for the given event-message record
   * is not supported.
   */
  protected AlertEmailMessage createAlertEmailMessage(
      QWEventMsgRecord recObj, int messageType)
  {
    return AlertEmailMessage.createAlertEmailMessage(
      logObj,alertEmailSenderObj,messageType,recObj,
      longEmailFormatFlagProp.booleanValue());
  }

  /**
   * Pushes given alert-email message onto the transmit queue.
   * @param emailMsgObj the alert-email-message object to use.
   */
  protected void pushAlertEmailMsgToQueue(
                                     AlertEmailMessageInterface emailMsgObj)
  {
    logObj.debug2(
      getLogPrefixText() +
      "Pushing msg onto queue:  " + emailMsgObj.toString());
    alertEmailMessageQueue.pushEvent(emailMsgObj);
    ++numberOfAlertsCount;        //increment # of messages counter
  }

  /**
   * Transmits the given alert-email message.
   * @param emailMsgObj the alert-email-message object to use.
   */
  protected void transmitAlertEmailMessage(
                                     AlertEmailMessageInterface emailMsgObj)
  {
    logObj.debug2(
      getLogPrefixText() +
      "Transmitting queued msg:  " + emailMsgObj.toString());
    try
    {         //get maximum number of retries allowed:
      final int maxSendMessageRetries =
                             alertEmailSenderObj.getMaxSendMessageRetries();
      int retryCount = 0;
      while(true)
      {  //loop while attempting to send email message
        if(alertEmailSenderObj.sendEmailMessage(this,emailMsgObj))
        {     //send successful
          logObj.debug2(
              getLogPrefixText() +
              "Successfully transmitted msg:  " + emailMsgObj.toString());
          break;        //exit loop
        }
        if(++retryCount > maxSendMessageRetries && maxSendMessageRetries > 0)
        {     //too many retries
          logObj.warning(
              getLogPrefixText() +
              "Too many failures (" + retryCount +
              "), unable to send message");
          logObj.debug("  msg:  " + emailMsgObj);
          break;        //exit loop
        }
      }
    }
    catch(Exception ex)
    {   //some kind of exception error
      logObj.warning(
          getLogPrefixText() +
          "Exception error transmitting msg:  " + ex);
      logObj.debug("  msg:  " + emailMsgObj);
      logObj.warning(UtilFns.getStackTraceString(ex));
    }
  }
}
