//AlertEmailRecipientInterface.java:  Defines an alert email recipient.
//
//  6/22/2005 -- [KF]  Initial version.
//  1/19/2006 -- [KF]  Moved from 'QWCapOutClient' to 'QWCommon'.
//

package com.isti.quakewatch.alertemail;

import com.isti.quakewatch.alert.AlertRecipientInterface;

/**
 * Interface AlertEmailRecipientInterface defines an alert email recipient.
 */
public interface AlertEmailRecipientInterface extends AlertRecipientInterface
{
  /**
   * Returns the list of email addresses for this recipient.
   * @return A string containing the list of email addresses
   * for this recipient.
   */
  public String getEmailAddressListStr();
}
