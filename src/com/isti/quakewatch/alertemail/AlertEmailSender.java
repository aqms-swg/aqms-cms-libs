//AlertEmailSender.java:  Defines an alert email sender.
//
//  6/22/2005 -- [KF]  Initial version.
//  1/19/2006 -- [KF]  Moved from 'QWCapOutClient' to 'QWCommon'.
//  1/27/2006 -- [KF]  Added 'getEmailPropsGroup()' method.
//  1/30/2006 -- [KF]  Added 'setProgramRevisionText()' method.
//   4/3/2006 -- [ET]  Minor variable name changes.
//   4/5/2006 -- [ET]  Removed "mail" from end of 'smtpServerAddressProp'
//                     description.
//   5/5/2006 -- [KF]  Added 'getLocalTimeZone()' method.
//   3/6/2019 -- [KF]  Added support for SSL.
//

package com.isti.quakewatch.alertemail;

import java.util.TimeZone;
import com.isti.util.UtilFns;
import com.isti.util.LogFile;
import com.isti.util.CfgProperties;
import com.isti.util.CfgPropItem;
import com.isti.util.DataChangedListener;
import com.isti.util.queue.NotifyEventQueue;
import com.isti.util.gis.LocationPlacesInterface;
import com.isti.quakewatch.util.JavaMailUtil;
import com.isti.quakewatch.alert.AlertRecipient;
import com.isti.quakewatch.alert.AlertMsgDateFormatterInterface;

/**
 * Class AlertEmailSender defines an alert email sender.
 */
public class AlertEmailSender implements AlertEmailSenderInterface
{
    /** Thread-synchronization object for 'sendEmailMessage()' method. */
  protected final Object sendEmailMsgSyncObj = new Object();
    /** Recipient tracking object for 'sendEmailMessage()' method. */
  protected AlertEmailRecipientInterface lastSendMsgRecipObj = null;
    /** Send-result tracking flag for 'sendEmailMessage()' method. */
  protected boolean lastSendSuccessfulFlag = true;
    /** Send-message retry-wait-delay for 'sendEmailMessage()' method. */
  protected int sendMsgRetryWaitMs = 0;
    /** JavaMail utility object. */
  protected final JavaMailUtil javaMailUtilObj;
    /** Local TimeZone */
  private final Object localTimeZoneSyncObj = new Object();
  private TimeZone localTimeZone = null;

         /** Maximum wait time between email-resend attempts (ms). */
  protected final int MAX_RESEND_WAITMS = 32000;

         /** Configuration properties object. */
  protected final CfgProperties settingsProps;

         /** The program revision text. */
  protected String programRevisionText = null;

         /** The alert message date formatter. */
  protected final AlertMsgDateFormatterInterface alertMsgDateFormat;

         /** The location places. */
  protected final LocationPlacesInterface locationPlacesObj;

         /** Test mode flag. */
  protected final boolean alertSenderTestModeFlag;

         /** The configuration group string. */
  protected final String emailPropsGroupStr;

         /** 'From' real-name for messages. */
  public final CfgPropItem emailFromNameProp;

         /** 'From' email-address for messages. */
  public final CfgPropItem emailFromAddressProp;

         /** Address of SMTP server for sending mail. */
  public final CfgPropItem smtpServerAddressProp;

         /** SMTP server authentication */
  private final CfgPropItem smtpServerAuthProp;

         /** Maximum number of send-mail retries after failure. */
  public final CfgPropItem maxSendMailRetriesProp;

         /** Local time zone for messages. */
  public final CfgPropItem localTimeZoneProp;

         /** Flag set true to use GMT for date/times in short messages. */
  public final CfgPropItem useGMTShortFlagProp;

         /** Flag set true to not check SMTP email server. */
  public final CfgPropItem dontCheckSMTPFlagProp;


  /**
   * Creates an alert email message sender.
   * @param settingsProps the 'CfgProperties' object.
   * @param programRevisionText the program revision text.
   * @param alertMsgDateFormat the alert message date formatter.
   * @param locationPlacesObj the location places.
   * @param alertSenderTestModeFlag true for test messages, false otherwise.
   * @param emailPropsGroupStr the group string to use for the properties.
   * @see setProgramRevisionText.
   */
  public AlertEmailSender(
      CfgProperties settingsProps,String programRevisionText,
      AlertMsgDateFormatterInterface alertMsgDateFormat,
      LocationPlacesInterface locationPlacesObj,
      boolean alertSenderTestModeFlag,String emailPropsGroupStr)
  {
    this.settingsProps = settingsProps;
    this.programRevisionText = programRevisionText;
    this.alertMsgDateFormat = alertMsgDateFormat;
    this.locationPlacesObj = locationPlacesObj;
    this.alertSenderTestModeFlag = alertSenderTestModeFlag;
    this.emailPropsGroupStr = emailPropsGroupStr;

    /** 'From' real-name for messages. */
    emailFromNameProp = settingsProps.add(
        "emailFromName", UtilFns.EMPTY_STRING, null,
        "'From' real-name for messages").
        setGroupSelObj(emailPropsGroupStr);

    /** 'From' email-address for messages. */
    emailFromAddressProp = settingsProps.add(
        "emailFromAddress", UtilFns.EMPTY_STRING, null,
        "'From' email-address for messages").
        setGroupSelObj(emailPropsGroupStr);

    /** Address of SMTP server for sending mail. */
    smtpServerAddressProp = settingsProps.add(
        "smtpServerAddress", UtilFns.EMPTY_STRING, null,
        "Address of SMTP server for sending").
        setGroupSelObj(emailPropsGroupStr);

    /** SMTP server authentication */
    smtpServerAuthProp = settingsProps.add("smtpServerAuth",
    		UtilFns.EMPTY_STRING, null,
            "SMTP server authentication").
    		setGroupSelObj(emailPropsGroupStr);

    /** Maximum number of send-mail retries after failure. */
    maxSendMailRetriesProp = settingsProps.add(
        "maxSendMailRetries", Integer.valueOf(10), null,
        "Maximum number of send-mail retries").
        setGroupSelObj(emailPropsGroupStr);

    /** Local time zone for messages. */
    localTimeZoneProp = settingsProps.add(
      "localTimeZone",
      TimeZone.getDefault().getDisplayName(false,TimeZone.SHORT),
      null,"Local time zone for messages").
        setValidator(UtilFns.timeZoneValidator).
        setGroupSelObj(emailPropsGroupStr);

    /** Flag set true to use GMT for date/times in short messages. */
    useGMTShortFlagProp = settingsProps.add(
      "useGMTShortFlag",Boolean.FALSE,null,
      "Use GMT for date/times in short messages").
        setGroupSelObj(emailPropsGroupStr);

    /** JavaMail utility object. */
    javaMailUtilObj = new JavaMailUtil(
      settingsProps, smtpServerAddressProp, smtpServerAuthProp, null);
    //configure "X-Mailer" header in messages
    javaMailUtilObj.setXMailerHeaderString(programRevisionText);

    /** Flag set true to not check SMTP email server. */
    dontCheckSMTPFlagProp = settingsProps.add(
        "dontCheckSMTPFlag", Boolean.FALSE, null,
        "Don't check SMTP email server");

         //initialize time zones for date formatters:
    updateLocalTimeZone(true);
         //setup listener to track local-time-zone config parameter:
    localTimeZoneProp.addDataChangedListener(new DataChangedListener()
        {
          public void dataChanged(Object sourceObj)
          {
            updateLocalTimeZone(false);
          }
        });
         //setup listener to track short-msg-GMT-zone-flag config parameter:
    useGMTShortFlagProp.addDataChangedListener(new DataChangedListener()
        {
          public void dataChanged(Object sourceObj)
          {
            updateShortDateFmtZone();
          }
        });
  }

  /**
   * Determines if the email-related configuration-property items have
   * been entered.
   * @return true if the email-related configuration-property items have
   * been entered; false if not.
   */
  public final boolean areEmailPropsReady()
  {
    return (emailFromAddressProp.stringValue().trim().length() > 0 &&
                   smtpServerAddressProp.stringValue().trim().length() > 0);
  }

  /**
   * Gets the alert message date formatter.
   * @return the alert message date formatter
   */
  public AlertMsgDateFormatterInterface getAlertMsgDateFormatter()
  {
    return alertMsgDateFormat;
  }

  /**
   * Returns a display string containing the 'From' email address and
   * name for messages.  The format of string is:  "name" <email>.
   * @return A display string containing the 'From' email address and
   * name for messages.
   */
  public String getDisplayStr()
  {
         //if name exists then surround it with quotes
         // and add a trailing space:
    String nameStr;
    if((nameStr=emailFromNameProp.stringValue()).length() > 0)
      nameStr = "\"" + nameStr + "\" ";
    return nameStr + "<" + emailFromAddressProp.stringValue() + ">";
  }

  /**
   * Gets the group string that are used for the properties.
   * @return the group string that are used for the properties.
   */
  public String getEmailPropsGroup()
  {
    return emailPropsGroupStr;
  }

  /**
   * Returns the JavaMail utility object.
   * @return The JavaMail utility object.
   */
  public JavaMailUtil getJavaMailUtilObj()
  {
    return javaMailUtilObj;
  }

  /**
   * Get the local time zone.
   * @return the local time zone.
   */
  public TimeZone getLocalTimeZone()
  {
    synchronized (localTimeZoneSyncObj)
    {
      if (localTimeZone == null)
      {
        localTimeZone = TimeZone.getTimeZone(localTimeZoneProp.stringValue());
      }
      return localTimeZone;
    }
  }

  /**
   * Gets the location places.
   * @return the location places.
   */
  public LocationPlacesInterface getLocationPlaces()
  {
    return locationPlacesObj;
  }

  /**
   * Returns the maximum number of retries to attempt when the sending
   * of an alert message is failing.
   * @return The maximum number of send alert message retries after failure.
   */
  public int getMaxSendMessageRetries()
  {
    return maxSendMailRetriesProp.intValue();
  }

  /**
   * Gets the program revision text.
   * @return the program revision text or null if not available.
   * @see setProgramRevisionText.
   */
  public String getProgramRevisionText()
  {
    return programRevisionText;
  }

  /**
   * Determines if in test mode and test messages are to be used.
   * @return true for test messages, false otherwise.
   */
  public boolean isTestMode()
  {
    return alertSenderTestModeFlag;
  }

  /**
   * Sends an alert message using the given parameters.
   * If the previous send attempt failed then this method may perform
   * a wait-delay before sending the message.
   * @param recipObj the alert recipient for the message.
   * @param alertMsgObj the alert message object.
   * @return true if successful; false if not (in which case a warning
   * message will be logged).
   */
  public boolean sendMessage(
      AlertRecipient recipObj, Object alertMsgObj)
  {
    if (recipObj instanceof AlertEmailRecipientInterface &&
        alertMsgObj instanceof AlertEmailMessageInterface)
    {
      return sendEmailMessage((AlertEmailRecipientInterface)recipObj,
                              (AlertEmailMessageInterface)alertMsgObj);
    }
    LogFile.getGlobalLogObj(false).warning(
      "Unable to transmit invalid message");
    return false;
  }

  /**
   * Sends an email message using the given parameters.  A "date"
   * header showing the current date will be included in the message.
   * If the previous send attempt failed then this method may perform
   * a wait-delay before sending the message.
   * @param recipObj the alert recipient for the message.
   * @param emailMsgObj the message object for holding the message.
   * @return true if successful; false if not (in which case a warning
   * message will be logged).
   */
  public boolean sendEmailMessage(AlertEmailRecipientInterface recipObj,
                                  AlertEmailMessageInterface emailMsgObj)
  {
    synchronized(sendEmailMsgSyncObj)
    {    //only allow one thread in at a time
      final LogFile logObj = LogFile.getGlobalLogObj(false);
      try
      {
        if(!lastSendSuccessfulFlag)
        {     //last attempt failed
          if(sendMsgRetryWaitMs <= 0)
          {   //retry-wait value at zero
                   //if recipient same as last time then do delay:
            if(lastSendMsgRecipObj == recipObj)
              sendMsgRetryWaitMs = 1000;
          }
          else     //retry-wait value > 0
            sendMsgRetryWaitMs *= 2;     //double the wait time
              //if too large then set to max-delay value:
          if(sendMsgRetryWaitMs > MAX_RESEND_WAITMS)
            sendMsgRetryWaitMs = MAX_RESEND_WAITMS;
          if(sendMsgRetryWaitMs > 0)
          {   //perform message-retry wait-delay
            logObj.debug2("sendEmailMessage:  Waiting " +
                  sendMsgRetryWaitMs + " ms before next send-mail attempt");
            final NotifyEventQueue queueObj =
                                       recipObj.getAlertMessageQueue();
            final long markTimeVal = System.currentTimeMillis();
            int waitMsVal = sendMsgRetryWaitMs;
            do
            { //loop through interruptions of message-retry wait-delay
                   //wait until delay time reached or interrupted:
              queueObj.waitForNotify(waitMsVal);
              if(!queueObj.isRunning())
                return false;   //if queue-processing terminated then return
            }      //calculate the amount of wait-delay time remaining:
            while((waitMsVal=sendMsgRetryWaitMs-    //loop if more remaining
                        (int)(System.currentTimeMillis()-markTimeVal)) > 0);
          }
          else
          {   //not performing message-retry wait-delay
            sendMsgRetryWaitMs = 500;  //setup wait-delay value for next time
            if(!recipObj.getAlertMessageQueue().isRunning())
              return false;  //if queue-processing terminated then return
          }
        }
        else
        {     //last attempt succeeded
          sendMsgRetryWaitMs = 0;      //reset retry-wait value
          if(!recipObj.getAlertMessageQueue().isRunning())
            return false;    //if queue-processing terminated then return
        }
        lastSendMsgRecipObj = recipObj;     //save handle to recipient
              //transmit message:
        if(lastSendSuccessfulFlag=javaMailUtilObj.sendEmailMessage(
                                         emailFromAddressProp.stringValue(),
                                            emailFromNameProp.stringValue(),
                                          recipObj.getEmailAddressListStr(),
                                         emailMsgObj.getSubjectTextString(),
                                        emailMsgObj.getMessageTextString()))
        {     //send successful
          return true;
        }
        logObj.warning("Unable to transmit msg for \"" +
                                           recipObj.getRecipID() + "\":  " +
                                   javaMailUtilObj.getErrorMessageString());
      }
      catch(Exception ex)
      {  //some kind of exception error; log it
        logObj.warning("Exception error transmitting msg for \"" +
                                      recipObj.getRecipID() + "\":  " + ex);
        logObj.debug("  msg:  " + emailMsgObj);
        logObj.warning(UtilFns.getStackTraceString(ex));
      }
      return false;
    }
  }

  /**
   * Sets the program revision text.
   * @param programRevisionText the program revision text.
   */
  public void setProgramRevisionText(String programRevisionText)
  {
    this.programRevisionText = programRevisionText;
    //configure "X-Mailer" header in messages
    javaMailUtilObj.setXMailerHeaderString(programRevisionText);
  }

  /**
   * Updates the date formatters that use the local-time-zone configuration
   * property.
   * @param updateShortFlag true to always update the the date formatter
   * used for short messages.
   */
  public void updateLocalTimeZone(boolean updateShortFlag)
  {
    synchronized (localTimeZoneSyncObj)
    {
      localTimeZone = null;
    }
    alertMsgDateFormat.updateLocalTimeZone(
      useGMTShortFlagProp.booleanValue(),localTimeZoneProp.stringValue(),
      updateShortFlag);
  }

  /**
   * Updates the time zone for the date formatter used for short messages.
   */
  public void updateShortDateFmtZone()
  {
    alertMsgDateFormat.updateShortDateFmtZone(
      useGMTShortFlagProp.booleanValue(),localTimeZoneProp.stringValue());
  }
}
