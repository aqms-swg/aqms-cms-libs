//AlertEmailSenderInterface.java:  Defines an email sender interface.
//
//  6/22/2005 -- [KF]  Initial version.
//  1/19/2006 -- [KF]  Moved from 'QWCapOutClient' to 'QWCommon'.
//

package com.isti.quakewatch.alertemail;

import com.isti.util.gis.LocationPlacesInterface;
import com.isti.quakewatch.alert.AlertSenderInterface;
import com.isti.quakewatch.alert.AlertMsgDateFormatterInterface;
import com.isti.quakewatch.util.JavaMailUtil;

/**
 * Interface SenderInterface defines an email sender interface.
 */
public interface AlertEmailSenderInterface extends AlertSenderInterface
{
  /**
   * Gets the alert message date formatter.
   * @return the alert message date formatter
   */
  public AlertMsgDateFormatterInterface getAlertMsgDateFormatter();

  /**
   * Gets the location places.
   * @return the location places.
   */
  public LocationPlacesInterface getLocationPlaces();

  /**
   * Returns the JavaMail utility object.
   * @return The JavaMail utility object.
   */
  public JavaMailUtil getJavaMailUtilObj();

  /**
   * Gets the program revision text.
   * @return the program revision text.
   */
  public String getProgramRevisionText();

  /**
   * Determines if in test mode and test messages are to be used.
   * @return true for test messages, false otherwise.
   */
  public boolean isTestMode();

  /**
   * Sends an email message using the given parameters.  A "date"
   * header showing the current date will be included in the message.
   * If the previous send attempt failed then this method may perform
   * a wait-delay before sending the message.
   * @param recipObj the alert recipient for the message.
   * @param emailMsgObj the message object for holding the message.
   * @return true if successful; false if not (in which case a warning
   * message will be logged).
   */
  public boolean sendEmailMessage(
      AlertEmailRecipientInterface recipObj, AlertEmailMessageInterface emailMsgObj);
}
