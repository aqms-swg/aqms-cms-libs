//AlarmRegionConfigDialog.java:  Defines a dialog for showing an
//                               alarm region configuration panel.
//
//  1/19/2006 -- [KF]  Initial version.
//   3/7/2006 -- [KF]  Added more region options.
//  3/17/2006 -- [KF]  Moved 'alarmCfgProps' to 'AlertRecipient'.
//  3/22/2006 -- [ET]  Added optional 'waitFlag' parameter to 'showDialog()'
//                     method.
//   4/6/2006 -- [ET]  Added optional 'regionEditParentComp' parameter to
//                     'showDialog()' method; updated tag-anchor names
//                     for "Help" button.
//  4/19/2006 -- [ET]  Modified commit-regions-panel action listener to call
//                     'commitRecipient()' method (to save recipient data)
//                     and delayed its installation until just before dialog
//                     shown (to avoid commit via 'setRegionString()' in
//                     'showDialog()' method); modified to save regions-
//                     manager object passed in via recipients list.
//  5/23/2006 -- [ET]  Modified string returned by 'getDialogTitleString()'
//                     to start with "Email".
//

package com.isti.quakewatch.alertgui;

import java.util.ArrayList;
import java.awt.Component;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

import com.isti.util.gui.IstiDialogPopup;
import com.isti.util.gui.IstiRegionPanel;
import com.isti.util.propertyeditor.CfgSettingsDialog;

import com.isti.quakewatch.alert.AlertRecipient;
import com.isti.quakewatch.alert.AlertRecipientsList;

/**
 * Class AlarmRegionConfigDialog defines a dialog for showing an
 * alarm region configuration panel.
 */
public class AlarmRegionConfigDialog extends IstiRegionPanel
{
//  private final CfgSettingsDialog cfgSettingsDialog;
//  private final AlertRecipientsList alertRecipientsListObj;
  private final AlertRecipient alertRecipientObj;  //the alert recipient
  private final ActionListener commitActionListenerObj;

  /**
   * Creates a dialog for showing an alarm region configuration panel.
   * @param cfgSettingsDialog the configuration settings dialog.
   * @param alertRecipientsListObj alert recipients list.
   * @param alertRecipientObj the alert recipient.
   */
  public AlarmRegionConfigDialog(
      final CfgSettingsDialog cfgSettingsDialog,
      final AlertRecipientsList alertRecipientsListObj,
      final AlertRecipient alertRecipientObj)
  {
    super(new ArrayList());  //do not include extra 'IstiRegion' option groups
              //change name to be pre-filled when adding new regions:
    setNewRegionNameStr("RecipRegion");

//    this.cfgSettingsDialog = cfgSettingsDialog;
//    this.alertRecipientsListObj = alertRecipientsListObj;
    this.alertRecipientObj = alertRecipientObj;
                   //save regions-manager object:
    if(alertRecipientsListObj != null)
      setIstiRegionMgrObj(alertRecipientsListObj.getIstiRegionMgrObj());

    //setup "Help" button action:
    setHelpActionListener(new ActionListener()
        {
          public void actionPerformed(ActionEvent evtObj)
          {
            final Object obj; //get component source of call:
            final Component compObj = ( (obj = evtObj.getSource()) instanceof
                                       Component) ? (Component) obj :
                AlarmRegionConfigDialog.this;
            //select tag-anchor based on whether the source
            // is the region-panel or the region-edit-panel:
            cfgSettingsDialog.showSettingsManualDialog(
                compObj,( (obj instanceof IstiRegionPanel) ?
                          "RecipientRegions" :
                          "RecipientEditRegion"));
          }
        });
    //create commit-regions-panel action-listener object:
    commitActionListenerObj = new ActionListener()
        {
          public void actionPerformed(ActionEvent evtObj)
          { //enter regions string into cfgProp:
            alertRecipientObj.getAlarmCfgProps().regionsAlarmProp.
                                   setValueString(getRegionString().trim());
                             //save recipient data and update display:
            if(alertRecipientsListObj != null)
              alertRecipientsListObj.commitRecipient(alertRecipientObj);
          }
        };
  }

  /**
   * Gets the dialog title string.
   * @return the dialog title string.
   */
  public String getDialogTitleString()
  {
    return "Email Alarm Regions for " + alertRecipientObj.toString();
  }

  /**
   * Shows an alarm region configuration dialog.
   * @param regionParentComp the parent for the region dialog, or null
   * for none.
   * @param regionEditParentComp the parent for region edit dialog, or
   * null to use the region panel.
   * @param waitFlag true to make this method block until the dialog is
   * closed.
   */
  public void showDialog(Component regionParentComp,
                           Component regionEditParentComp, boolean waitFlag)
  {
    if(isDialogVisible())
    {    //alarm-regions panel currently showing
      requestFocus();         //put focus on panel
      return;                                    //exit method
    }
         //remove any previous commit-regions-panel action listener:
    removeActionListener(commitActionListenerObj);
    setRegionString(  //put in regions string from cfgProp
       alertRecipientObj.getAlarmCfgProps().regionsAlarmProp.stringValue());
         //create alarm-regions dialog:
    final IstiDialogPopup popupObj = createDialog(
              getDialogTitleString(),regionParentComp,regionEditParentComp);
         //install commit-regions-panel action listener:
    addActionListener(commitActionListenerObj);
         //show alarm-regions dialog:
    if(waitFlag)                       //if "wait" flag set then
      popupObj.showAndWait();          //block until dialog closed
    else                               //if "wait" flag not set then
      popupObj.show();                 //show and return immediately
  }

  /**
   * Shows an alarm region configuration dialog.
   * @param regionParentComp the parent for the region dialog, or null
   * for none.
   * @param waitFlag true to make this method block until the dialog is
   * closed.
   */
  public void showDialog(Component regionParentComp, boolean waitFlag)
  {
    showDialog(regionParentComp,(Component)null,waitFlag);
  }

  /**
   * Shows an alarm region configuration dialog.  This method does
   * not wait for the dialog to be closed.
   * @param regionParentComp the parent for the region dialog, or null
   * for none.
   */
  public void showDialog(Component regionParentComp)
  {
    showDialog(regionParentComp,false);
  }
}
