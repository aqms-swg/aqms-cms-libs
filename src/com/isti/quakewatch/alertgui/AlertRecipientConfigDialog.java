//AlertRecipientConfigDialog.java:  Defines a dialog for showing an
//                                  alert recipient configuration panel.
//
//  1/19/2006 -- [KF]  Initial version.
//  3/23/2006 -- [ET]  Added optional 'waitFlag' parameter to 'showDialog()'
//                     method; modified to put focus on first text-field
//                     component when dialog shown.
//

package com.isti.quakewatch.alertgui;

import java.awt.Component;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;

import com.isti.util.propertyeditor.CfgPropertyInspector;
import com.isti.util.propertyeditor.CfgSettingsDialog;

import com.isti.quakewatch.alert.AlertRecipient;
import com.isti.quakewatch.alert.AlertRecipientsList;

/**
 * Class AlertRecipientConfigDialog defines a dialog for showing an
 * alert recipient configuration panel.
 */
public class AlertRecipientConfigDialog extends CfgPropertyInspector
{
  private final CfgSettingsDialog cfgSettingsDialog;
  private final AlertRecipientsList alertRecipientsList;

         /** Title string for configuration dialog panel. */
  public static final String RECIPIENT_CFG_STR = "Recipient Configuration";

  /**
   * Creates a dialog for showing an alert recipient configuration panel.
   * @param cfgSettingsDialog the configuration settings dialog.
   * @param alertRecipientsList alert recipients list.
   * @param alertRecipient the alert recipient.
   */
  public AlertRecipientConfigDialog(
      final CfgSettingsDialog cfgSettingsDialog,
      final AlertRecipientsList alertRecipientsList,
      final AlertRecipient alertRecipient)
  {
    super(alertRecipient.cfgProps);
    this.cfgSettingsDialog = cfgSettingsDialog;
    this.alertRecipientsList = alertRecipientsList;

    //setup "Help" button action:
    addHelpActionListener(new ActionListener()
    {
      public void actionPerformed(ActionEvent evtObj)
      {
        //get name of currently-displayed tab and show
        // "Settings Manual" dialog, referenced to the tab name:
        showSettingsManualDialog(
            AlertRecipientConfigDialog.this, getCurrentTabName());
      }
    });
    //create and add listener called to commit changes:
    addPropertyChangeListener(new PropertyChangeListener()
    {
      public void propertyChange(PropertyChangeEvent evt)
      {
        //if message-queue-processing thread not yet running
        // then start the message processing thread now:
        if (!alertRecipient.getAlertMessageQueue().isRunning())
          alertRecipient.getAlertMessageQueue().startThread();
        updateAlertRecipient(alertRecipient);  //update the alert recipient
      }
    });
         //setup to focus on first suitable component when dialog shown:
    setInitialFocusOnFirstCompFlag(true);
  }

  /**
   * Shows an alert recipient configuration dialog.
   * @param parentComp the parent component for the dialog.
   * @param waitFlag true to make this method block until the dialog is
   * closed.
   */
  public void showDialog(Component parentComp, boolean waitFlag)
  {
    showDialog(parentComp,RECIPIENT_CFG_STR,RECIPIENT_CFG_STR,
                                                            false,waitFlag);
  }

  /**
   * Shows an alert recipient configuration dialog.
   * @param parentComp the parent component for the dialog.
   */
  public void showDialog(Component parentComp)
  {
    showDialog(parentComp,RECIPIENT_CFG_STR,RECIPIENT_CFG_STR,false,false);
  }

  /**
   * Displays the "Help|SettingsManual" dialog window.
   * @param parentComponent the parent component for the dialog.
   * @param tabStr the name of the tab to use as the reference (anchor)
   * into the view.
   */
  protected void showSettingsManualDialog(
      Component parentComponent,String tabStr)
  {
    cfgSettingsDialog.showSettingsManualDialog(parentComponent,tabStr);
  }

  /**
   * Update the alert recipient.
   * @param alertRecipient the alert recipient.
   */
  protected void updateAlertRecipient(AlertRecipient alertRecipient)
  {
    alertRecipientsList.commitRecipient(alertRecipient);
  }
}
