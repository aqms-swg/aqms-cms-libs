//QWClientBase.java:  Defines base functionality for a QuakeWatch client.
//
//  12/1/2003 -- [ET]  Initial version.
//  1/23/2004 -- [ET]  Added 'loadConfigFlag' parameter to
//                     "processConfiguration()" method; made
//                     'CfgPropItem' variables "protected".
//  2/12/2004 -- [ET]  Changed "localExitCleanup()" method from "private"
//                     to "protected"; added 'getConnManagerObj()',
//                     'isEventChannelConnected()', 'isConnectionValidated()'
//                     and 'isReqServerMsgsAvailable()' methods.
//  3/10/2004 -- [ET]  Minor javadoc change.
//  3/26/2004 -- [ET]  Added 'xmlConfigLoaderObj' variable; modified to
//                     extend 'LogFileViaCfgProps'; added 'connStatusObj'
//                     and 'processingEnabledFlag' parameters to
//                     'runClient()' method; modified 'localExitCleanup()'
//                     to request connection-status popup while closing
//                     connection and to use a worker thread to complete
//                     cleanup actions; added 'setupClientInfoProps()' and
//                     'setupInitReqMsgsObjs()' methods.
//   4/8/2004 -- [ET]  Fixed reference from 'clientLogFileNameProp' to
//                     'clientLogFileLevelProp' in 'setupConfiguration()'
//                     method.
//  5/20/2004 -- [ET]  Added optional 'defaultCfgFName' parameter to
//                     'processConfiguration()' method.
//  6/16/2004 -- [ET]  Added 'getOpenOrbVersionStr()' method.
//  6/25/2004 -- [ET]  Added call to QWConnectionMgr 'performExitCleanup()'
//                     at program exit.
//  9/15/2004 -- [ET]  Added 'getCertificateFileDataArr()' method.
//  2/15/2005 -- [ET]  Added methods 'setServerLoginPropertyEditor()' and
//                     'setSaveToConfigFileCallBackObj()'.
//  2/17/2005 -- [ET]  Added "logObj==null" checks to exit cleanup methods;
//                     modified to call 'setServerLoginPropertyEditor()'
//                     in 'QWConnProperties' instead of 'QWConnectionMgr'.
//  3/21/2005 -- [ET]  Modified 'processConfiguration()' to process
//                     command-line parameter "--configFile" to specify
//                     a configuration file.
//  4/14/2005 -- [ET]  Changed 'ShutdownHookCleanupThread' log message from
//                     "warning" to "debug"; added 'isClientRunning()',
//                     'isClientTerminated()', 'stopClient()' and
//                     'clientSleepDelay()' methods.
//  5/31/2005 -- [ET]  Added versions of 'runClient()' method that accept
//                     a 'QWConnectionMgr' object as a parameter.
//   9/2/2005 -- [KF]  Added 'getClientRunningStartTime()' and
//                     'getClientRunningTime()' methods.
//  9/13/2005 -- [ET]  Modified to put default value into 'startupTimeString'
//                     instance variable; added two-string-parameter
//                     version of 'setupClientInfoProps()' method.
//  12/1/2005 -- [ET]  Changed log level of "Initializing client connection
//                     to server" message to 'debug'.
//  5/31/2007 -- [ET]  Modified to extend 'ClientLogViaCfgProps' instead
//                     of 'LogFileViaCfgProps' class.
//   2/8/2008 -- [ET]  Added config properties 'prependDeclarationFlag'
//                     and 'prependDeclarationText'; added method
//                     'getPrependDeclarationString()' and optional
//                     'addPrependDeclFlag' parameter to method
//                     'setupConfiguration()'.
//  2/28/2008 -- [KF]  Added 'getXmlConfigLoader()' method.
// 12/18/2009 -- [ET]  Modified 'localExitCleanup()' method to make it wait
//                     (up to 10 seconds) for exit-cleanup-finish thread to
//                     complete; added optional 'localEventAgePropFlag'
//                     parameter to 'setupConfiguration()' method.
//  10/4/2010 -- [ET]  Modified to remove shutdown-hook cleanup-thread
//                     object in 'localExitCleanup()' method.
//  8/21/2012 -- [ET]  Added optional 'remShutdownHookFlag' parameter to
//                     'localExitCleanup()' method; added exception catch
//                     to 'removeShutdownHookCleanupThreadObj()' method.
//  3/13/2019 -- [KF]  Modified to use the PDL Indexer.
//

package com.isti.quakewatch.clientbase;

import java.beans.PropertyEditor;
import com.isti.util.UtilFns;
import com.isti.util.LogFile;
import com.isti.util.CfgProperties;
import com.isti.util.CfgPropItem;
import com.isti.util.DataChangedListener;
import com.isti.util.XmlConfigLoader;
import com.isti.util.ClientLogViaCfgProps;
import com.isti.util.IstiDialogInterface;
import com.isti.util.IstiVersion;
import com.isti.util.ProgressIndicatorInterface;
import com.isti.quakewatch.util.QWConnProperties;
import com.isti.quakewatch.util.QWConnectionMgr;
import com.isti.quakewatch.util.ConnStatusInterface;
import com.isti.quakewatch.util.InitReqMsgsCallBack;
import com.isti.quakewatch.common.MsgTag;
import com.isti.quakewatch.common.QWConstants;
import com.isti.quakewatch.message.QWDataMsgProcessor;

/**
 * Class QWClientBase defines base functionality for a QuakeWatch client.
 * QuakeWatch client classes should extend this class.
 */
public class QWClientBase extends ClientLogViaCfgProps implements QWConstants
{
    /** Name of parameter for specifying the client configuration file. */
  public static final String CFG_PARAM_SPEC = "configFile";
  protected QWConnectionMgr connectionManagerObj = null;   //connection mgr
                             //XML-configuration-file loader:
  protected final XmlConfigLoader xmlConfigLoaderObj = new XmlConfigLoader();
  private String [] programArgsArr = null;       //command-line params
  private boolean processConfigRunFlag = false;
         //client running start time or 0 if client is not running:
  private long clientRunningStartTime = 0L;
         //flag set true if client has been terminated:
  private boolean clientTerminatedFlag = false;
         //notify object for 'clientSleepDelay()':
  private final Object clientSleepDelayNotifyObj = new Object();
         //flag to make sure that cleanup code is not run twice:
  private boolean cleanupCodeAlreadyRunFlag = false;
         //thread-synchronization object used by 'exitCleanup()':
  private final Object exitCleanupSyncObject = new Object();
         //tracking handle for 'ShutdownHookCleanupThread' object:
  private ShutdownHookCleanupThread shutdownHookCleanupThreadObj = null;
         //variables setup by 'setupClientInfoProps()':
  protected String clientNameString = null;
  protected String clientVerString = null;
  protected String startupTimeString;
         //variables setup by 'setupInitReqMsgsObjs()' method:
  protected IstiDialogInterface initReqMsgsDialogObj = null;
  protected String initReqMsgsCancelString = UtilFns.EMPTY_STRING;
  protected ProgressIndicatorInterface initReqMsgsProgIndObj = null;
  protected InitReqMsgsCallBack initReqMsgsCallBackObj = null;
         //login-dialog property editor for "serverLogin" cfgProp:
  protected PropertyEditor serverLoginPropEditorObj = null;
         //handle for save-to-config-file call-back object:
  protected DataChangedListener saveToConfigFileCallBackObj = null;
         //declaration string to be prepended to sent messages:
  protected String prependDeclarationString = null;
         //configuration properties object:
  private CfgProperties cfgProps = null;
         //connection-configuration-properties manager object:
  private QWConnProperties connPropsObj = null;
         //true to prepend declaration to generated messages:
  protected final CfgPropItem prependDeclarationFlagProp =
                     new CfgPropItem("prependDeclarationFlag",Boolean.FALSE,
                                    null,"Prepend declaration to messages");
         //declaration text to be prepended to generated messages:
  protected final CfgPropItem prependDeclarationTextProp =
       new CfgPropItem("prependDeclarationText","<?xml version=\"1.0\"?>\n",
                                 null,"Declaration text prepended to msgs");


  /**
   * Creates the client base object.  A default value is loaded into
   * the 'startupTimeString' instance variable.
   */
  public QWClientBase()
  {
    startupTimeString =
                     UtilFns.timeMillisToString(System.currentTimeMillis());
  }

  /**
   * Sets up the client configuration by adding items to the configuration-
   * properties object.  Other items may be added before and after calling
   * this method.
   * @param userPropsObj the configuration-properties object to add items
   * to, or null to create a new configuration-properties object.
   * @param connGroupSelObj the configuration-group-selection object to
   * use for connection items, or null for none.
   * @param logGroupSelObj the configuration-group-selection object to
   * use for log-file items, or null for none.
   * @param addPrependDeclFlag true to add "prependDeclaration..." items
   * to configuration.
   * @param localEventAgePropFlag if true then a local
   * 'maxServerEventAgeDays' property item is created and used.
   */
  public void setupConfiguration(CfgProperties userPropsObj,
                              Object connGroupSelObj, Object logGroupSelObj,
                  boolean addPrependDeclFlag, boolean localEventAgePropFlag)
  {
         //use given config-props object, or create new one:
    cfgProps = (userPropsObj != null) ? userPropsObj : new CfgProperties();
         //add connection items:
    connPropsObj = new QWConnProperties(cfgProps,connGroupSelObj,
                                                     localEventAgePropFlag);
    if(addPrependDeclFlag)
    {  //flag set; add prepend-declaration items to config
      cfgProps.add(prependDeclarationFlagProp);
      cfgProps.add(prependDeclarationTextProp);
                   //set prepend-declaration text if configProp flag true:
      prependDeclarationString = prependDeclarationFlagProp.booleanValue() ?
                            prependDeclarationTextProp.stringValue() : null;
                   //create listener to track changes to configProps:
      final DataChangedListener listenerObj = new DataChangedListener()
          {
            public void dataChanged(Object sourceObj)
            {
              prependDeclarationString =
                                 prependDeclarationFlagProp.booleanValue() ?
                            prependDeclarationTextProp.stringValue() : null;
            }
          };
                   //attach tracking listeners to configProps:
      prependDeclarationFlagProp.addDataChangedListener(listenerObj);
      prependDeclarationTextProp.addDataChangedListener(listenerObj);
    }
         //if previously entered then setup login-dialog property editor:
    if(serverLoginPropEditorObj != null)
      connPropsObj.setServerLoginPropertyEditor(serverLoginPropEditorObj);
         //modify default log-file name:
    clientLogFileNameProp.setDefaultValue("log/QWOutClient.log");
         //set log-file name value to be same as default:
    clientLogFileNameProp.setValue(clientLogFileNameProp.getDefaultValue());
         //modify default log-file message level to be "debug":
    clientLogFileLevelProp.setDefaultValue(LogFile.DEBUG_STR);
         //set log-file message level value to be same as default:
    clientLogFileLevelProp.setValue(clientLogFileLevelProp.getDefaultValue());
         //add log-file items to config props:
    addLogFilePropItems(cfgProps,logGroupSelObj);
  }

  /**
   * Sets up the client configuration by adding items to the configuration-
   * properties object.  Other items may be added before and after calling
   * this method.  A local 'maxServerEventAgeDays' property item is created
   * and used.
   * @param userPropsObj the configuration-properties object to add items
   * to, or null to create a new configuration-properties object.
   * @param connGroupSelObj the configuration-group-selection object to
   * use for connection items, or null for none.
   * @param logGroupSelObj the configuration-group-selection object to
   * use for log-file items, or null for none.
   * @param addPrependDeclFlag true to add "prependDeclaration..." items
   * to configuration.
   */
  public void setupConfiguration(CfgProperties userPropsObj,
                              Object connGroupSelObj, Object logGroupSelObj,
                                                 boolean addPrependDeclFlag)
  {
    setupConfiguration(userPropsObj,connGroupSelObj,logGroupSelObj,
                                                   addPrependDeclFlag,true);
  }

  /**
   * Sets up the client configuration by adding items to the configuration-
   * properties object.  Other items may be added before and after calling
   * this method.  A local 'maxServerEventAgeDays' property item is created
   * and used.
   * @param userPropsObj the configuration-properties object to add items
   * to, or null to create a new configuration-properties object.
   * @param connGroupSelObj the configuration-group-selection object to
   * use for connection items, or null for none.
   * @param logGroupSelObj the configuration-group-selection object to
   * use for log-file items, or null for none.
   */
  public void setupConfiguration(CfgProperties userPropsObj,
                              Object connGroupSelObj, Object logGroupSelObj)
  {
    setupConfiguration(userPropsObj,connGroupSelObj,logGroupSelObj,
                                                                false,true);
  }

  /**
   * Sets up the client configuration by adding items to the configuration-
   * properties object.  Other items may be added before and after calling
   * this method.  A local 'maxServerEventAgeDays' property item is created
   * and used.
   * @param userPropsObj the configuration-properties object to add items
   * to, or null to create a new configuration-properties object.
   * @param connGroupSelObj the configuration-group-selection object to
   * use for connection items, or null for none.
   * @param addPrependDeclFlag true to add "prependDeclaration..." items
   * to configuration.
   */
  public void setupConfiguration(CfgProperties userPropsObj,
                         Object connGroupSelObj, boolean addPrependDeclFlag)
  {
    setupConfiguration(userPropsObj,connGroupSelObj,null,
                                                   addPrependDeclFlag,true);
  }

  /**
   * Sets up the client configuration by adding items to the configuration-
   * properties object.  Other items may be added before and after calling
   * this method.  A local 'maxServerEventAgeDays' property item is created
   * and used.
   * @param userPropsObj the configuration-properties object to add items
   * to, or null to create a new configuration-properties object.
   * @param connGroupSelObj the configuration-group-selection object to
   * use for connection items, or null for none.
   */
  public void setupConfiguration(CfgProperties userPropsObj,
                                                     Object connGroupSelObj)
  {
    setupConfiguration(userPropsObj,connGroupSelObj,null,false,true);
  }

  /**
   * Loads settings from the configuration file and sets up the log file
   * and the console-redirect file (if enabled).
   * A shutdown hook is installed that calls 'performExitCleanup()' in
   * response to program termination ("Ctrl-C", kill, etc).  If a
   * configuration filename is specified via the command-line parameter
   * "--configFile" then it will be used instead of the filename specified
   * via the method parameter 'cfgFileName'.
   * @param cfgFileName the name of the XML file to load.
   * @param rootElementName the expected name of the root element in the
   * XML file, or null to accept any name.
   * @param settingsElementName the name of the "Settings" element whose
   * text contains the "name = value" items.
   * @param mustFlag if true and a configuration file with a "Settings"
   * element is not found then an error will be generated and the method
   * will return 'false'.
   * @param prgArgs string array of command-line parameters to be
   * processed, or null for none.
   * @param loadConfigFlag false to skip loading from the configuration
   * file and setting up the program-termination shutdown hook; true to
   * process normally.
   * @param defaultCfgFName the name of an XML file containing configuration
   * default values to be loaded before the configuration file is loaded,
   * or null for none.
   * @return 'true' if successful, false if an error occurred (in which
   * case an error message will be sent to the console and may be fetched
   * via the 'getErrorMessageString()' method).
   */
  public boolean processConfiguration(String cfgFileName,
         String rootElementName,String settingsElementName,boolean mustFlag,
            String [] prgArgs,boolean loadConfigFlag,String defaultCfgFName)
  {
    if(cfgProps == null)                    //if config not yet setup then
      setupConfiguration(null,null,null);   //setup configuration
    programArgsArr = prgArgs;
    processConfigRunFlag = true;            //indicate method has run
    boolean retFlag = true;
    if(loadConfigFlag)
    {    //load-config flag set
              //setup cleanup thread to be run at program termination:
      addShutdownHookCleanupThreadObj();

      if(defaultCfgFName != null)
      {  //"defaults" configuration filename was specified
              //load settings from "defaults" XML configuration file:
        if(!xmlConfigLoaderObj.loadConfiguration(defaultCfgFName,
                   rootElementName,settingsElementName,false,cfgProps,true))
        {     //error loading or processing defaults; show error message
          final String errStr = xmlConfigLoaderObj.getErrorMessage();
          System.err.println(errStr);
          enterErrorMessageString(errStr);
          retFlag = false;        //setup to return error flag value
        }
      }
         //check for "configFile" parameter:
      final String arg0Str;
      if(programArgsArr != null && programArgsArr.length > 0 &&
                                        (arg0Str=programArgsArr[0]) != null)
      {  //at least one command-line parameter available
        final int arg0Len = arg0Str.length();    //length of first parameter
        final int specLen = CFG_PARAM_SPEC.length();  //length of param spec
        int p = 0;      //skip past up to 2 leading dash characters:
        while(p < arg0Len && arg0Str.charAt(p) == '-' && ++p < 2);
        if(arg0Len >= p+specLen)
        {  //parameter is long enough for match
              //if equals char found then trim param; else use full length:
          final int sepPos = (arg0Len > p+specLen+1 &&
                    arg0Str.charAt(p+specLen) == '=') ? p+specLen : arg0Len;
          if(arg0Str.substring(p,sepPos).equals(CFG_PARAM_SPEC))
          {   //parameter spec matched
            final int nameSlotVal;
            if(sepPos >= arg0Len)
            { //equals char was not found; use next parameter for filename
              if(programArgsArr.length > 1 && programArgsArr[1] != null)
              {    //valid second parameter is available
                cfgFileName = programArgsArr[1]; //fetch filename parameter
                nameSlotVal = 1;       //indicate second parameter used
              }
              else
                nameSlotVal = 0;       //indicate only first parameter used
            }
            else
            {   //equals char was found; use rest of string for filename
              cfgFileName = arg0Str.substring(sepPos+1);
              nameSlotVal = 0;         //indicate only first parameter used
            }
            final int len;     //check if name surrounded by quotes:
            if((len=cfgFileName.length()) > 1 &&
                                          ((cfgFileName.charAt(0) == '\"' &&
                                       cfgFileName.charAt(len-1) == '\"') ||
                                           (cfgFileName.charAt(0) == '\'' &&
                                        cfgFileName.charAt(len-1) == '\'')))
            {      //name is surrounded by quotes; remove them
              cfgFileName = cfgFileName.substring(1,len-1);
            }
            if(programArgsArr.length > nameSlotVal+1)
            {      //more command-line parameters are available
                   //"shift down" the remaining command-line parameters:
              int newPos = 0;
              final String [] newArgsArr =  //new string array for params
                            new String[programArgsArr.length-nameSlotVal-1];
                        //for each remaining param, copy to new array:
              for(p=nameSlotVal+1; p<programArgsArr.length; ++p)
                newArgsArr[newPos++] = programArgsArr[p];
              programArgsArr = newArgsArr;     //enter new array of params
            }
            else   //no more command-line parameters are available
              programArgsArr = new String[0];  //setup empty array
          }
        }
      }
              //load settings from XML configuration file:
      if(!xmlConfigLoaderObj.loadConfiguration(cfgFileName,rootElementName,
                      settingsElementName,mustFlag,cfgProps,programArgsArr))
      {  //error loading or processing settings; show error message
        final String errStr = xmlConfigLoaderObj.getErrorMessage();
        System.err.println(errStr);
        enterErrorMessageString(errStr);
              //setup a dummy log-file object that outputs to console:
        logObj = new LogFile(null,LogFile.NO_MSGS,LogFile.INFO);
        return false;
      }
    }
    if(!createLogFile(false))     //create and setup log file
      retFlag = false;
    return retFlag;
  }

  /**
   * Loads settings from the configuration file and sets up the log file
   * and the console-redirect file (if enabled).
   * A shutdown hook is installed that calls 'performExitCleanup()' in
   * response to program termination ("Ctrl-C", kill, etc).  If a
   * configuration filename is specified via the command-line parameter
   * "--configFile" then it will be used instead of the filename specified
   * via the method parameter 'cfgFileName'.
   * @param cfgFileName the name of the XML file to load.
   * @param rootElementName the expected name of the root element in the
   * XML file, or null to accept any name.
   * @param settingsElementName the name of the "Settings" element whose
   * text contains the "name = value" items.
   * @param mustFlag if true and a configuration file with a "Settings"
   * element is not found then an error will be generated and the method
   * will return 'false'.
   * @param programArgs string array of command-line parameters to be
   * processed, or null for none.
   * @param loadConfigFlag false to skip loading from the configuration
   * file and setting up the program-termination shutdown hook; true to
   * process normally.
   * @return 'true' if successful, false if an error occurred (in which
   * case an error message will be sent to the console and may be fetched
   * via the 'getErrorMessageString()' method).
   */
  public boolean processConfiguration(String cfgFileName,
                          String rootElementName,String settingsElementName,
              boolean mustFlag,String [] programArgs,boolean loadConfigFlag)
  {
    return processConfiguration(cfgFileName,rootElementName,
              settingsElementName,mustFlag,programArgs,loadConfigFlag,null);
  }

  /**
   * Loads settings from the configuration file and sets up the log file
   * and the console-redirect file (if enabled).
   * A shutdown hook is installed that calls 'performExitCleanup()' in
   * response to program termination ("Ctrl-C", kill, etc).  If a
   * configuration filename is specified via the command-line parameter
   * "--configFile" then it will be used instead of the filename specified
   * via the method parameter 'cfgFileName'.
   * @param cfgFileName the name of the XML file to load.
   * @param rootElementName the expected name of the root element in the
   * XML file, or null to accept any name.
   * @param settingsElementName the name of the "Settings" element whose
   * text contains the "name = value" items.
   * @param mustFlag if true and a configuration file with a "Settings"
   * element is not found then an error will be generated and the method
   * will return 'false'.
   * @param programArgs string array of command-line parameters to be
   * processed, or null for none.
   * @param defaultCfgFName the name of an XML file containing configuration
   * default values to be loaded before the configuration file is loaded,
   * or null for none.
   * @return 'true' if successful, false if an error occurred (in which
   * case an error message will be sent to the console and may be fetched
   * via the 'getErrorMessageString()' method).
   */
  public boolean processConfiguration(String cfgFileName,
                          String rootElementName,String settingsElementName,
              boolean mustFlag,String [] programArgs,String defaultCfgFName)
  {
    return processConfiguration(cfgFileName,rootElementName,
             settingsElementName,mustFlag,programArgs,true,defaultCfgFName);

  }

  /**
   * Loads settings from the configuration file and sets up the log file
   * and the console-redirect files (if enabled).
   * A shutdown hook is installed that calls 'performExitCleanup()' in
   * response to program termination ("Ctrl-C", kill, etc).  If a
   * configuration filename is specified via the command-line parameter
   * "--configFile" then it will be used instead of the filename specified
   * via the method parameter 'cfgFileName'.
   * @param cfgFileName the name of the XML file to load.
   * @param rootElementName the expected name of the root element in the
   * XML file, or null to accept any name.
   * @param settingsElementName the name of the "Settings" element whose
   * text contains the "name = value" items.
   * @param mustFlag if true and the "Settings" element is not found
   * then an error will be generated; if false and the "Settings" element
   * is not found then this method will simply return 'true'.
   * @param programArgs string array of command-line parameters to be
   * processed, or null for none.
   * @return 'true' if successful, false if an error occurred (in which
   * case an error message will be sent to the console and may be fetched
   * via the 'getErrorMessageString()' method).
   */
  public boolean processConfiguration(String cfgFileName,
                          String rootElementName,String settingsElementName,
                                     boolean mustFlag,String [] programArgs)
  {
    return processConfiguration(cfgFileName,rootElementName,
                        settingsElementName,mustFlag,programArgs,true,null);
  }

  /**
   * Sets up the connection-information properties string to be sent when
   * the client connects to the server.
   * @param clientNameStr the client program name to use, or null for none.
   * @param clientVerStr the client version string to use, or null for none.
   * @param startupTimeStr the client startup time string to use, or null
   * for the default value entered via the client-base class.
   */
  public void setupClientInfoProps(String clientNameStr,String clientVerStr,
                                                      String startupTimeStr)
  {
    clientNameString = clientNameStr;
    clientVerString = clientVerStr;
    if(startupTimeStr != null)
      startupTimeString = startupTimeStr;
  }

  /**
   * Sets up the connection-information properties string to be sent when
   * the client connects to the server.
   * @param clientNameStr the client program name to use, or null for none.
   * @param clientVerStr the client version string to use, or null for none.
   */
  public void setupClientInfoProps(String clientNameStr,String clientVerStr)
  {
    setupClientInfoProps(clientNameStr,clientVerStr,null);
  }

  /**
   * Sets up dialog, progress and call-back objects used by the connection
   * manager's "initialRequestMessagesFromServer()" method.
   * @param initReqMsgsDialogObj dialog-popup object.
   * @param initReqMsgsCancelString 'Cancel' string for dialog-popup object.
   * @param initReqMsgsProgIndObj for progress-indicator object.
   * @param initReqMsgsCallBackObj call-back object.
   */
  public void setupInitReqMsgsObjs(IstiDialogInterface initReqMsgsDialogObj,
                                             String initReqMsgsCancelString,
                           ProgressIndicatorInterface initReqMsgsProgIndObj,
                                 InitReqMsgsCallBack initReqMsgsCallBackObj)
  {
    this.initReqMsgsDialogObj = initReqMsgsDialogObj;
    this.initReqMsgsCancelString = initReqMsgsCancelString;
    this.initReqMsgsProgIndObj = initReqMsgsProgIndObj;
    this.initReqMsgsCallBackObj = initReqMsgsCallBackObj;
  }

  /**
   * Enters the login-dialog property editor for the
   * 'QWServerLoginInformation' object held by the "serverLogin"
   * connection-property item.  GUI clients that want to be able to
   * display the login dialog should call this method with
   * "new LoginPropertyEditor()" as the parameter.
   * @param propertyEditorObj the property-editor object to enter.
   */
  public void setServerLoginPropertyEditor(PropertyEditor propertyEditorObj)
  {
    serverLoginPropEditorObj = propertyEditorObj;     //save handle
    if(connPropsObj != null)   //if connProps setup then enter editor
      connPropsObj.setServerLoginPropertyEditor(serverLoginPropEditorObj);
  }

  /**
   * Sets up the call-back object to be called when the connection
   * configuration-property items should be saved to the config file.
   * @param listenerObj The 'DataChangedListener' to use, or null for none.
   */
  public void setSaveToConfigFileCallBackObj(
                                            DataChangedListener listenerObj)
  {
    saveToConfigFileCallBackObj = listenerObj;        //save handle
    if(connectionManagerObj != null)
    {    //connMgr is setup; enter call-back
      connectionManagerObj.setSaveToConfigFileCallBackObj(
                                               saveToConfigFileCallBackObj);
    }
  }

  /**
   * Runs the client.  The 'processConfiguration()' method must be
   * executed before this method.
   * @param connMgrObj connection-manager object to use, or null to
   * have this method create one.
   * @param msgProcessorObj the message-processor object to use for
   * processsing received messages.
   * @param connStatusObj connection-status panel object, or null for none.
   * @param processingEnabledFlag sets the state of the initial
   * processing-enabled flag.
   * @return 'true' if successful, false if an error occurred (in which
   * case an error message will be sent to the log file and may be
   * fetched via the 'getErrorMessageString()' method).
   */
  protected boolean runClient(QWConnectionMgr connMgrObj,
       QWDataMsgProcessor msgProcessorObj,ConnStatusInterface connStatusObj,
                                              boolean processingEnabledFlag)
  {
    return runClient(connMgrObj, msgProcessorObj, connStatusObj,
        processingEnabledFlag, MsgTag.MINIMUM_VERSION);
  }

  /**
   * Runs the client.  The 'processConfiguration()' method must be
   * executed before this method.
   * @param connMgrObj connection-manager object to use, or null to
   * have this method create one.
   * @param msgProcessorObj the message-processor object to use for
   * processsing received messages.
   * @param connStatusObj connection-status panel object, or null for none.
   * @param processingEnabledFlag sets the state of the initial
   * processing-enabled flag.
   * @return 'true' if successful, false if an error occurred (in which
   * case an error message will be sent to the log file and may be
   * fetched via the 'getErrorMessageString()' method).
   * @param minVersion the minimum version or null if none.
   */
  protected boolean runClient(QWConnectionMgr connMgrObj,
       QWDataMsgProcessor msgProcessorObj,ConnStatusInterface connStatusObj,
       boolean processingEnabledFlag, IstiVersion minVersion)
  {
    if(!processConfigRunFlag)
    {    //'processConfiguration()' not yet run
      final String errStr = "Error initializing client:  " +
                                   "'processConfiguration()' method must " +
                                  "be executed before 'runClient()' method";
      logObj.warning(errStr);
      setErrorMessageString(errStr);
      return false;
    }

    try
    {         //if passed in then use given connection-manager object:
      if(connMgrObj != null)
        connectionManagerObj = connMgrObj;
      else
      {  //connection-manager object not passed in; create one
        connectionManagerObj = new QWConnectionMgr(
          programArgsArr,connPropsObj,msgProcessorObj,connStatusObj,logObj);
      }
      if (minVersion != null)
      {
        connectionManagerObj.getMsgHandlerObj().setMinVersion(minVersion);
      }
              //setup initial processing-enabled flag:
      connectionManagerObj.setMsgProcessingEnabledFlag(processingEnabledFlag);
              //setup client-connection-information properties string
              // to be sent when connecting to server:
      connectionManagerObj.setupConnInfoProps(clientNameString,clientVerString,
                                                         startupTimeString);
              //if entered then setup save-to-config-file call-back object:
      if(saveToConfigFileCallBackObj != null)
      {
        connectionManagerObj.setSaveToConfigFileCallBackObj(
                                               saveToConfigFileCallBackObj);
      }
              //setup objs for 'initialRequestMessagesFromServer()' method:
      connectionManagerObj.setupInitReqMsgsObjs(initReqMsgsDialogObj,
                              initReqMsgsCancelString,initReqMsgsProgIndObj,
                                                    initReqMsgsCallBackObj);
      logObj.debug("Initializing client connection to server");
      connectionManagerObj.initializeConnection();    //connect to server
      clientRunningStartTime = System.currentTimeMillis();  //save start time

      clientTerminatedFlag = false;         //indicate not terminated
      return true;
    }
    catch(Exception ex)
    {         //some kind of exception error; log it
      final String errStr = "Error initializing client:  " + ex;
      logObj.warning(errStr);
      logObj.warning(UtilFns.getStackTraceString(ex));
      setErrorMessageString(errStr);
      clientRunningStartTime = 0L;          //indicate client not running
      return false;
    }
  }

  /**
   * Runs the client.  The 'processConfiguration()' method must be
   * executed before this method.
   * @param msgProcessorObj the message-processor object to use for
   * processsing received messages.
   * @param connStatusObj connection-status panel object, or null for none.
   * @param processingEnabledFlag sets the state of the initial
   * processing-enabled flag.
   * @return 'true' if successful, false if an error occurred (in which
   * case an error message will be sent to the log file and may be
   * fetched via the 'getErrorMessageString()' method).
   */
  public boolean runClient(QWDataMsgProcessor msgProcessorObj,
            ConnStatusInterface connStatusObj,boolean processingEnabledFlag)
  {
    return runClient(null,msgProcessorObj,connStatusObj,
                                                     processingEnabledFlag);
  }

  /**
   * Runs the client.  The 'processConfiguration()' method must be
   * executed before this method.
   * @param msgProcessorObj the message-processor object to use for
   * processsing received messages.
   * @param connStatusObj connection-status panel object, or null for none.
   * @return 'true' if successful, false if an error occurred (in which
   * case an error message will be sent to the log file and may be
   * fetched via the 'getErrorMessageString()' method).
   */
  public boolean runClient(QWDataMsgProcessor msgProcessorObj,
                                          ConnStatusInterface connStatusObj)
  {
    return runClient(null,msgProcessorObj,connStatusObj,true);
  }

  /**
   * Runs the client.  The 'processConfiguration()' method must be
   * executed before this method.
   * @param msgProcessorObj the message-processor object to use for
   * processsing received messages.
   * @param processingEnabledFlag sets the state of the initial
   * processing-enabled flag.
   * @return 'true' if successful, false if an error occurred (in which
   * case an error message will be sent to the log file and may be
   * fetched via the 'getErrorMessageString()' method).
   */
  public boolean runClient(QWDataMsgProcessor msgProcessorObj,
                                              boolean processingEnabledFlag)
  {
    return runClient(null,msgProcessorObj,null,processingEnabledFlag);
  }

  /**
   * Runs the client.  The 'processConfiguration()' method must be
   * executed before this method.
   * @param msgProcessorObj the message-processor object to use for
   * processsing received messages.
   * @return 'true' if successful, false if an error occurred (in which
   * case an error message will be sent to the log file and may be
   * fetched via the 'getErrorMessageString()' method).
   */
  public boolean runClient(QWDataMsgProcessor msgProcessorObj)
  {
    return runClient(msgProcessorObj,null);
  }

  /**
   * Runs the client.  The 'processConfiguration()' method must be
   * executed before this method.
   * @param connMgrObj connection-manager object to use, or null to
   * have this method create one.
   * @param processingEnabledFlag sets the state of the initial
   * processing-enabled flag.
   * @return 'true' if successful, false if an error occurred (in which
   * case an error message will be sent to the log file and may be
   * fetched via the 'getErrorMessageString()' method).
   */
  public boolean runClient(QWConnectionMgr connMgrObj,
                                              boolean processingEnabledFlag)
  {
    return runClient(connMgrObj,null,null,processingEnabledFlag);
  }

  /**
   * Runs the client.  The 'processConfiguration()' method must be
   * executed before this method.
   * @param connMgrObj connection-manager object to use, or null to
   * have this method create one.
   * @return 'true' if successful, false if an error occurred (in which
   * case an error message will be sent to the log file and may be
   * fetched via the 'getErrorMessageString()' method).
   */
  public boolean runClient(QWConnectionMgr connMgrObj)
  {
    return runClient(connMgrObj,null,null,true);
  }

  /**
   * Closes and reopens the CORBA connection to the server.  The
   * connection-status popup is shown, a separate thread is used,
   * and the connection-reinit-via-user-input flag is set.
   * @return true if reinitialization attempt made, false if aborted
   * because connect or disconnect already in progress.
   */
  public boolean reinitConnection()
  {
    return connectionManagerObj.reinitConnection(true,true,true);
  }

  /**
   * Gets the client running start time in milliseconds.
   * @return the client running start time
   *  or 0 if the client is not running.
   */
  public long getClientRunningStartTime()
  {
    return clientRunningStartTime;
  }

  /**
   * Gets the amount of time the client has been running in milliseconds.
   * @return the amount of time the client has been running
   *  or 0 if the client is not running.
   */
  public long getClientRunningTime()
  {
    if (isClientRunning())
      return System.currentTimeMillis() - clientRunningStartTime;
    return 0L;
  }

  /**
   * Determines whether or not the client is running.  The client is
   * considered to be running if the 'runClient()' method has been
   * successfully executed and the 'stopClient()' method has not
   * been called.
   * @return true if the client is running; false if not.
   */
  public boolean isClientRunning()
  {
    return clientRunningStartTime != 0L;
  }

  /**
   * Determines whether or not the client has been terminated.  The
   * client is considered terminated if the 'stopClient()' method
   * has been called.
   * @return true if the client has been terminated; false if not.
   */
  public boolean isClientTerminated()
  {
    return clientTerminatedFlag;
  }

  /**
   * Returns the log file object.
   * @return The 'LogFile' object used by this class, or null if the
   * log file has not been setup.
   */
  public LogFile getLogFileObj()
  {
    return logObj;
  }

  /**
   * Returns the declaration string to be prepended to sent messages.
   * @return The declaration string to be prepended to sent messages,
   * or null if none.
   */
  public String getPrependDeclarationString()
  {
    return prependDeclarationString;
  }

  /**
   * Returns the connection-configuration-properties manager object.
   * @return The connection-configuration-properties manager object,
   * or null if it has not been created.
   */
  public QWConnProperties getConnPropsObj()
  {
    return connPropsObj;
  }

  /**
   * Returns the local 'maxServerEventAgeDays' property item (if available).
   * @return The local 'maxServerEventAgeDays' property item, or null
   * if not in use.
   */
  public CfgPropItem getMaxServerEventAgeDaysProp()
  {
    return (connPropsObj != null) ?
                         connPropsObj.getMaxServerEventAgeDaysProp() : null;
  }

  /**
   * Returns the connection-manager object.
   * @return The connection-manager object, or null if it has not been
   * created.
   */
  public QWConnectionMgr getConnManagerObj()
  {
    return connectionManagerObj;
  }

  /**
   * Returns the connection status of the server event channel.
   * @return true if the connection to the event channel is active, false
   * if not.
   */
  public boolean isEventChannelConnected()
  {
    return (connectionManagerObj != null) ?
                     connectionManagerObj.isEventChannelConnected() : false;
  }

  /**
   * Returns an indicator of whether or not the 'requestMessages()'
   * method is available via the 'QWServices' on the current server.
   * @return true if the 'requestServerMessages()' method is available,
   * false if not.
   */
  public boolean isReqServerMsgsAvailable()
  {
    return (connectionManagerObj != null) ?
                    connectionManagerObj.isReqServerMsgsAvailable() : false;
  }

  /**
   * Returns the status of whether or not the connection has been
   * "validated" via the receipt of any server-alive messages.
   * @return true if any server-alive messages have been received since
   * the last connect-to-server attempt, false if not.
   */
  public boolean isConnectionValidated()
  {
    return (connectionManagerObj != null) ?
                       connectionManagerObj.isConnectionValidated() : false;
  }

  /**
   * Returns the certificate-file data that was fetched from the server
   * at connect time.
   * @return The certificate-file data that was fetched from the server
   * at connect time, or null if none was fetched.
   */
  public byte [] getCertificateFileDataArr()
  {
    return (connectionManagerObj != null) ?
                    connectionManagerObj.getCertificateFileDataArr() : null;
  }

  /**
   * Get the XML config loader.
   * @return the XML config loader.
   */
  public XmlConfigLoader getXmlConfigLoader()
  {
    return xmlConfigLoaderObj;
  }

  /**
   * Convenience method for performing a sleep delay.  If the client is
   * terminated then the delay will be interrupted.
   * @param delayMs number of milliseconds to delay.
   * @return true if the full delay was performed; false if the client
   * was terminated.
   */
  public boolean clientSleepDelay(int delayMs)
  {
    if(!clientTerminatedFlag)
    {    //client not terminated
      synchronized(clientSleepDelayNotifyObj)
      {  //grab thread synchronization lock for notify object
        try
        {     //perform delay:
          clientSleepDelayNotifyObj.wait(delayMs);
          return true;            //indicate delay finished
        }
        catch(Exception ex)
        {
        }
      }
    }
    return false;                 //indicate delay interrupted
  }

  /**
   * Interrupts any 'clientSleepDelay()' in progress.
   */
  protected void interruptClientSleepDelay()
  {
    synchronized(clientSleepDelayNotifyObj)
    {    //grab thread synchronization lock for notify object
      clientSleepDelayNotifyObj.notifyAll();
    }
  }

  /**
   * Shuts down the client, performs exit cleanup and terminates
   * the program.  The 'System.exit()' method is called after
   * cleanup actions are performed.
   * @param exitVal the exit status code to use.
   */
  public void terminateProgram(int exitVal)
  {
    if(!cleanupCodeAlreadyRunFlag)     //if cleanup not yet performed
      localExitCleanup(exitVal);       // then do exit cleanup actions
    else                          //if exit cleanup already performed then
      System.exit(exitVal);       //terminate program
  }

  /**
   * Shuts down the client and performs exit cleanup.  The 'System.exit()'
   * method is not called.
   */
  public void stopClient()
  {
    localExitCleanup(false,0,true);
  }

  /**
   * Performs "local" exit cleanup after calling 'performExitCleanup()'.
   * @param systemExitFlag if true then 'System.exit()' will be called.
   * @param exitVal the exit status code to use.
   * @param remShutdownHookFlag if true then the shutdown-hook cleanup
   * thread will be removed.
   */
  protected void localExitCleanup(final boolean systemExitFlag,
                       final int exitVal, final boolean remShutdownHookFlag)
  {
    if(logObj != null)
      logObj.debug3("Called 'localExitCleanup()'");
    Thread finishThreadObj = null;
    synchronized(exitCleanupSyncObject)
    {    //only allow one thread access at a time
      if(!cleanupCodeAlreadyRunFlag)
      {  //cleanup not already run
        cleanupCodeAlreadyRunFlag = true;        //indicate clean performed
        if(remShutdownHookFlag)                  //if flag then
          removeShutdownHookCleanupThreadObj();  //remove shutdown hook
        try
        {     //call cleanup code for subclass:
          performExitCleanup();
        }
        catch(Exception ex)
        {     //some kind of exception error; build error message
          final String errStr = "Error executing 'performExitCleanup()':  " +
                     ex + UtilFns.newline + UtilFns.getStackTraceString(ex);
          if(logObj != null)                //if log file setup then
            logObj.warning(errStr);         //send out to log file
          else                              //if log file not setup then
            System.err.println(errStr);     //send out to console
        }
             //launch new thread to finish exit cleanup so that
             // other system activity will not be held up while waiting
             // for CORBA shutdown:
        finishThreadObj = new Thread("exitCleanupFinish")
            {
              public void run()
              {
                try
                {
                  if(logObj != null)
                    logObj.debug3("exit-cleanup-finish thread started");
                  UtilFns.sleep(100);  //slight delay to help other updates
                  clientRunningStartTime = 0L; //indicate client not running
                  clientTerminatedFlag = true; //indicate client terminated
                  interruptClientSleepDelay(); //interrupt sleep delay
                        //close connection (show popup, retry if necessary):
                  if(connectionManagerObj != null)
                    connectionManagerObj.closeConnection(true,true);
                  UtilFns.sleep(100);  //slight delay to help other updates
                  if(connectionManagerObj != null)
                    connectionManagerObj.performExitCleanup();
                }
                catch(Exception ex)
                {  //some kind of exception error; show error message
                  System.err.println("Error in 'localExitCleanup()':  " + ex);
                  ex.printStackTrace();
                }
                                       //finish exit cleanup:
                finishLocalExitCleanup(systemExitFlag,exitVal);
              }
            };
        if(logObj != null)
          logObj.debug2("Launching exit-cleanup-finish thread");
        finishThreadObj.start();
      }
      if(finishThreadObj != null)
      {  //finish thread was started
        try
        {     //wait (up to 10 seconds) for finish thread to complete:
          synchronized(finishThreadObj)
          {
            finishThreadObj.wait(10000);
          }
        }
        catch(Exception ex)
        {     //interrupted or other exception; just move on
        }
      }
    }
  }

  /**
   * Performs "local" exit cleanup after calling 'performExitCleanup()'.
   * @param systemExitFlag if true then 'System.exit()' will be called.
   * @param exitVal the exit status code to use.
   */
  protected void localExitCleanup(final boolean systemExitFlag,
                                                          final int exitVal)
  {
    localExitCleanup(systemExitFlag,exitVal,true);
  }

  /**
   * Performs "local" exit cleanup after calling 'performExitCleanup()'.
   * The 'System.exit()' method is called after the cleanup.
   * @param exitVal the exit status code to use.
   */
  protected void localExitCleanup(int exitVal)
  {
    localExitCleanup(true,exitVal,true);
  }

  /**
   * Performs "local" exit cleanup after calling 'performExitCleanup()'.
   * The 'System.exit()' method is not called.
   */
  protected void localExitCleanup()
  {
    localExitCleanup(false,0,true);
  }

  /**
   * Finishes the "local" exit cleanup.
   * @param systemExitFlag if true then 'System.exit()' will be called.
   * @param exitVal the exit status code to use.
   */
  private void finishLocalExitCleanup(boolean systemExitFlag,int exitVal)
  {
    try
    {
      if(logObj != null)
      {    //log file is OK
        logObj.debug3("Called 'finishLocalExitCleanup()'");
        closeLogFile();                  //close log file
      }
      closeConsoleRedirectStream();      //close console-redirect file
              //notify on thread object to interrupt 'wait()'
              // call at end of 'localExitCleanup()':
      final Object waitObj = Thread.currentThread();
      synchronized(waitObj)
      {
        waitObj.notifyAll();     //notify 'wait()'
      }
    }
    catch(Throwable ex)
    {  //some kind of exception error
      ex.printStackTrace();       //show stack trace
    }
    if(systemExitFlag)                 //if system-exit flag set then
      System.exit(exitVal);            //execute system-exit method
  }

  /**
   * Called in response to program termination ("Ctrl-C", kill, etc).
   * Subclasses may override this method to have custom cleanup routines
   * executed at program termination.  The version of the method in this
   * class does nothing.
   */
  protected void performExitCleanup()
  {
  }

  /**
   * Returns the OpenORB version string.
   * @return The OpenORB version string.
   */
  public static String getOpenOrbVersionStr()
  {
    return QWConnectionMgr.getOpenOrbVersionStr();
  }

  /**
   * Installs the cleanup thread to be run at program termination.
   */
  protected void addShutdownHookCleanupThreadObj()
  {
              //remove any existing cleanup thread:
    removeShutdownHookCleanupThreadObj();
              //create cleanup-thread object:
    final ShutdownHookCleanupThread shctObj =
                                            new ShutdownHookCleanupThread();
    Runtime.getRuntime().addShutdownHook(shctObj);    //install cleanup obj
    shutdownHookCleanupThreadObj = shctObj;           //indicate installed
  }

  /**
   * Removes the cleanup thread run at program termination (if installed).
   */
  protected void removeShutdownHookCleanupThreadObj()
  {
    final ShutdownHookCleanupThread shctObj;
    if((shctObj=shutdownHookCleanupThreadObj) != null)
    {  //cleanup thread was installed; remove via thread-safe local handle
      try
      {
        Runtime.getRuntime().removeShutdownHook(shctObj);
      }
      catch(Throwable ex)
      {  //ignore any exception
      }
      shutdownHookCleanupThreadObj = null;            //indicate removed
    }
  }


  /**
   * Class ShutdownHookCleanupThread is inserted as a shutdown hook, and is
   * responsible for cleaning up before a forced exit.
   */
  private class ShutdownHookCleanupThread extends Thread
  {
    /**
     * Executes the cleanup code.
     */
    public void run()
    {
      if(logObj != null && logObj.isOpen())
        logObj.debug("Program terminated via Ctrl-C, kill or abort");
      localExitCleanup(false,0);
    }
  }
}
