//QWTrackingClient.java:  Extension of 'QWClientBase' that adds the
//                        tracking of the current message number and
//                        time-generated value via a file.
//
//  12/1/2003 -- [ET]  Initial version.
//  2/16/2004 -- [ET]  Modified to use local date-format object in a
//                     thread-safe manner.
//  3/25/2004 -- [ET]  Added 'logGroupSelObj' parameter to
//                     'setupConfiguration()' method.
//   2/8/2008 -- [ET]  Added optional 'addPrependDeclFlag' parameter to
//                     'setupConfiguration()' methods.
//  5/16/2008 -- [ET]  Modified to startup with initial processing of
//                     messages disabled (until after initial fetch
//                     of requested messages completes).
//

package com.isti.quakewatch.clientbase;

import java.util.Date;
import java.text.DateFormat;
import java.io.File;
import java.io.FileNotFoundException;
import org.jdom.Element;
import com.isti.util.UtilFns;
import com.isti.util.FileUtils;
import com.isti.util.CfgProperties;
import com.isti.util.CfgPropItem;
import com.isti.quakewatch.util.QWUtils;
import com.isti.quakewatch.util.QWConnectionMgr;
import com.isti.quakewatch.common.MsgTag;
import com.isti.quakewatch.message.QWDataMsgProcessor;
import com.isti.quakewatch.message.MsgProcessorInterface;
import com.isti.quakewatch.message.QWMsgNumTimeRec;
import com.isti.quakewatch.message.QWDynMsgNumTimeRec;

/**
 * Class QWTrackingClient is an extension of 'QWClientBase' that adds the
 * tracking of the current message number and time-generated value via a
 * file.
 */
public class QWTrackingClient extends QWClientBase
{
         //record-object holding msgNum and timeGen for last message:
  private QWDynMsgNumTimeRec lastMsgNumTimeRecObj = null;
         //pathname for message-tracking file configuration property:
  private CfgPropItem trackingFileNameProp;
         //file object for tracking file:
  private File trackingFileObj;
         //thread-synchronization object for msgNum/timeGen tracking file:
  private final Object trackingFileSyncObj = new Object();

  /**
   * Sets up the client configuration by adding items to the configuration-
   * properties object.  Other items may be added before and after calling
   * this method.
   * @param userPropsObj the configuration-properties object to add items
   * to, or null to create a new configuration-properties object.
   * @param connGroupSelObj the configuration-group-selection object to
   * use for connection items, or null for none.
   * @param logGroupSelObj the configuration-group-selection object to
   * use for log-file items, or null for none.
   * @param addPrependDeclFlag true to add "prependDeclaration..." items
   * to configuration.
   */
  public void setupConfiguration(CfgProperties userPropsObj,
                              Object connGroupSelObj, Object logGroupSelObj,
                                                 boolean addPrependDeclFlag)
  {
         //use given config-props object, or create new one:
    final CfgProperties propsObj = (userPropsObj != null) ? userPropsObj :
                                                        new CfgProperties();
         //setup client-base properties:
    super.setupConfiguration(propsObj,connGroupSelObj,logGroupSelObj,
                                                        addPrependDeclFlag);
         //setup and add pathname for tracking file property:
    trackingFileNameProp = propsObj.add(
                                   "trackingFileName","qwTracking.dat",null,
              "Pathname for tracking file").setGroupSelObj(connGroupSelObj);
  }

  /**
   * Sets up the client configuration by adding items to the configuration-
   * properties object.  Other items may be added before and after calling
   * this method.
   * @param userPropsObj the configuration-properties object to add items
   * to, or null to create a new configuration-properties object.
   * @param connGroupSelObj the configuration-group-selection object to
   * use for connection items, or null for none.
   * @param logGroupSelObj the configuration-group-selection object to
   * use for log-file items, or null for none.
   */
  public void setupConfiguration(CfgProperties userPropsObj,
                              Object connGroupSelObj, Object logGroupSelObj)
  {
    setupConfiguration(userPropsObj,connGroupSelObj,logGroupSelObj,false);
  }

  /**
   * Sets up the client configuration by adding items to the configuration-
   * properties object.  Other items may be added before and after calling
   * this method.
   * @param userPropsObj the configuration-properties object to add items
   * to, or null to create a new configuration-properties object.
   * @param connGroupSelObj the configuration-group-selection object to
   * use for connection items, or null for none.
   * @param addPrependDeclFlag true to add "prependDeclaration..." items
   * to configuration.
   */
  public void setupConfiguration(CfgProperties userPropsObj,
                         Object connGroupSelObj, boolean addPrependDeclFlag)
  {
    setupConfiguration(userPropsObj,connGroupSelObj,null,addPrependDeclFlag);
  }

  /**
   * Sets up the client configuration by adding items to the configuration-
   * properties object.  Other items may be added before and after calling
   * this method.
   * @param userPropsObj the configuration-properties object to add items
   * to, or null to create a new configuration-properties object.
   * @param connGroupSelObj the configuration-group-selection object to
   * use for connection items, or null for none.
   */
  public void setupConfiguration(CfgProperties userPropsObj,
                                                     Object connGroupSelObj)
  {
    setupConfiguration(userPropsObj,connGroupSelObj,null,false);
  }

  /**
   * Runs the client.  The 'processConfiguration()' method must be
   * executed before this method.
   * @param clientMsgProcObj the client-message-processor object to use for
   * processsing received messages.
   * @return 'true' if successful, false if an error occurred (in which
   * case an error message will be sent to the console or to the log
   * file).
   */
  public boolean runClient(MsgProcessorInterface clientMsgProcObj)
  {
    try       //read message-number and time-generated values
    {         // for last message from tracking file:
                        //create file object for tracking file:
      trackingFileObj = new File(trackingFileNameProp.stringValue());
      final String str;
      synchronized(trackingFileSyncObj)
      {  //only allow one thread to access file at a time
              //read file data into string:
        str = FileUtils.readFileToString(trackingFileObj);
      }
              //parse msgNum/timeGen data and enter into tracking object:
      if(!setupLastMsgNumTimeObj(str))      //if error then log message
        logObj.warning("Error parsing tracking-file data (\"" + str + "\")");
    }
    catch(FileNotFoundException ex)
    {    //no file found; ignore
    }
    catch(Exception ex)
    {    //error accessing file; log message
      logObj.warning("Error reading tracking file:  " + ex);
    }
              //startup client (with initial processing of msgs disabled):
    return runClient(new TrackingMsgProcessor(clientMsgProcObj),false);
  }

  /**
   * Parses the given string for message-number and time-generated values
   * and enters them into the record-object holding msgNum and timeGen
   * for the last message.
   * @param dataStr a data string holding two numeric strings, in the form
   * "msgNum timeGen".
   * @return true if successful, false if an error occurred while parsing
   * the string.
   */
  private boolean setupLastMsgNumTimeObj(String dataStr)
  {
    final int p;
    final Long msgNumObj,timeGenObj;
    if(dataStr != null && (p=dataStr.indexOf(' ')) > 0 &&
       (msgNumObj=QWUtils.parseStringLong(dataStr.substring(0,p).trim())) !=
                                                                     null &&
      (timeGenObj=QWUtils.parseStringLong(dataStr.substring(p+1).trim())) !=
                                                                       null)
    {    //string parsed OK; create record
      lastMsgNumTimeRecObj = new QWDynMsgNumTimeRec(
                              msgNumObj.longValue(),timeGenObj.longValue());
      return true;
    }
    return false;
  }

  /**
   * Updates the tracking object for the last message-number and time-
   * generated values and writes the new values to the tracking file.
   * @param msgNum the message number to use.
   * @param timeGenerated the time-generated value to use.
   */
  private void updateLastMsgNumTimeObj(long msgNum, long timeGenerated)
  {
    try
    {
              //enter current msgNum/timeGen values:
      if(lastMsgNumTimeRecObj != null)
        lastMsgNumTimeRecObj.setMsgNumAndTime(msgNum,timeGenerated);
      else    //if not yet created then create message record:
        lastMsgNumTimeRecObj = new QWDynMsgNumTimeRec(msgNum,timeGenerated);
      synchronized(trackingFileSyncObj)
      {     //only allow one thread to access file at a time
                 //build string containing message-number and time-
                 // generated values and write it to the tracking file:
        FileUtils.writeStringToFile(trackingFileObj,
              (Long.toString(msgNum) + " " + Long.toString(timeGenerated)));
      }
    }
    catch(Exception ex)
    {  //some kind of exception error; log it
      logObj.warning("Error in 'updateLastMsgNumTimeObj()':  " + ex);
      logObj.debug(UtilFns.getStackTraceString(ex));
    }
  }


  /**
   * Class TrackingMsgProcessor passes along messages while tracking the
   * current message-number and time-generated values via a file.
   */
  private class TrackingMsgProcessor implements QWDataMsgProcessor
  {
    private final MsgProcessorInterface clientMsgProcObj;
              //date-formatter object for 'processDataMessage()' method:
    private final DateFormat xmlDateFormatterObj =
                                           QWUtils.getXmlDateFormatterObj();
              //tracker for received data message data:
    private String dataMessageStringTracker = "";

    /**
     * Creates a "tracking" version of the message processor.
     * @param clientMsgProcObj the 'MsgProcessorInterface' object to
     * pass messages to.
     */
    public TrackingMsgProcessor(MsgProcessorInterface clientMsgProcObj)
    {
      this.clientMsgProcObj = clientMsgProcObj;
    }

    /**
     * Processes any number of "Event" and "Product" elements in the given
     * "DataMessage" element.
     * @param qwMsgElement The "QWmessage" element.
     * @param dataMsgElement The "DataMessage" element.
     * @param xmlMsgStr the XML text message string.
     * @param requestedFlag true to indicate the the message was "requested"
     * (and that it should not be processed as a "real-time" message).
     */
    public void processDataMessage(Element qwMsgElement,
            Element dataMsgElement, String xmlMsgStr, boolean requestedFlag)
    {
      Long msgNumObj = null;
      Date timeGenObj = null;
      try
      {
           //check if message is a duplicate of the previous message:
        if(xmlMsgStr != null && xmlMsgStr.length() > 0)
        {  //given message string contains data
          if(xmlMsgStr.equals(dataMessageStringTracker))
          {     //message string data same as previous; log warning
            logObj.warning("Data message is duplicate of previous--" +
                                             "discarded; msg:  " + xmlMsgStr);
            return;
          }
                     //save message data string for next iteration:
          dataMessageStringTracker = xmlMsgStr;
        }
        else    //no data in given message string
          dataMessageStringTracker = "";      //clear message string tracker

   //     logObj.info("Received data message:  " + xmlMsgStr);

        String str;
                   //fetch and parse msgNum attribute value:
        if((str=qwMsgElement.getAttributeValue(MsgTag.MSG_NUMBER)) != null &&
                           (msgNumObj=QWUtils.parseStringLong(str)) != null)
        {  //message number found and parsed OK
                   //fetch and parse timeGen attribute value:
          if((str=qwMsgElement.getAttributeValue(MsgTag.TIME_GENERATED)) ==
                          null || (timeGenObj=parseXmlDateStr(str)) == null)
          {     //unable to find and parse time-generated value
            logObj.warning(
              "Unable to parse time-generated value from message (data=\"" +
                                                               str + "\")");
            logObj.debug("  msg:  " + xmlMsgStr);
          }
        }
        else
        {  //unable to find and parse message number
          logObj.warning(
                    "Unable to parse message number from message (data=\"" +
                                                               str + "\")");
          logObj.debug("  msg:  " + xmlMsgStr);
        }
        if(clientMsgProcObj != null)
        {  //client-message-processor OK; pass along message
          clientMsgProcObj.processDataMessage(qwMsgElement,dataMsgElement,
                              xmlMsgStr,requestedFlag,msgNumObj,timeGenObj);
        }
              //if message-number and time-generated values available then
              // enter them into the tracking object and file:
        if(msgNumObj != null && timeGenObj != null)
        {
          updateLastMsgNumTimeObj(
                                msgNumObj.longValue(),timeGenObj.getTime());
        }
      }
      catch(Exception ex)
      {  //some kind of exception error; log it
        logObj.warning("Error processing received message:  " + ex);
        logObj.debug(UtilFns.getStackTraceString(ex));
      }
    }

    /**
     * Enters the connection manager to be used by this message processor.
     * @param connMgrObj The connection-manager object to use.
     */
    public void setConnectionMgr(QWConnectionMgr connMgrObj)
    {              //connection manager not used by test code
    }

    /**
     * Returns the last event held in storage.
     * @return The 'QWMsgNumTimeRec' object for the last event held in
     * storage, or null if not available.
     */
    public QWMsgNumTimeRec getLastEventInStorage()
    {
              //return latest msgNum/timeGen values processed:
      return (lastMsgNumTimeRecObj != null) ?
                             lastMsgNumTimeRecObj.getMsgNumAndTime() : null;
    }

    /**
     * Parses the given string as an XML date string.  This method is
     * thread safe.
     * @param str the date string to parse.
     * @return A new 'Date' object, or null if the string could not be
     * parsed as an XML date string.
     */
    private Date parseXmlDateStr(String str)
    {
      synchronized(xmlDateFormatterObj)
      {
        try
        {     //parse date/time string into 'Date' object and return it:
          return xmlDateFormatterObj.parse(str);
        }
        catch(Exception ex)
        {     //error parsing
          return null;
        }
      }
    }
  }
}
