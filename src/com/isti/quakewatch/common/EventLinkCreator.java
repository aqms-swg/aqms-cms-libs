package com.isti.quakewatch.common;

import java.util.Arrays;
import java.util.regex.Pattern;

import com.isti.util.FileUtils;
import com.isti.util.StringConstants;

public class EventLinkCreator implements StringConstants
{
  /**
   * Product Link Variable<br>
   * Variables enclosed with '{' and '}' (e.g. "{SOURCE}")
   */
  public static enum ProductLinkVariable
  {
    /** Product ID Code (e.g. "ok2021eltk") */
    CODE,
    /** Product ID Source (e.g. "us") */
    SOURCE,
    /** Event source Code (e.g. "2021eltk") */
    EVCODE,
    /** Event Source (e.g. "ok") */
    EVSOURCE,
    /** Product ID update Time (epoch in milliseconds) */
    TIME,
    /** Product ID Type (e.g. "shakemap") */
    TYPE;

    private final Pattern pattern;

    ProductLinkVariable()
    {
      pattern = Pattern.compile("\\{" + toString() + "\\}");
    }

    Pattern getPattern()
    {
      return pattern;
    }
  }

  /** The default event source code */
  public static final String DEFAULT_EVENT_SOURCE_CODE = "us";

  /**
   * Get the product link.
   * 
   * @param str           the format string.
   * @param code          the product ID code.
   * @param source        the product ID source.
   * @param type          the product ID type or null if none.
   * @param updateTimeObj the product ID update time object may be a Date,
   *                      Instant, Long, Number or any class that returns the
   *                      text representation of a number using
   *                      {@code Object.toString()} or null if none.
   * @param evcode        the event source code or null to use the product ID
   *                      code.
   * @param evsource      the event source or null to use the product ID source.
   * @return the product link or null if none.
   */
  public static String getProductLink(String str, String code, String source,
      String type, Object updateTimeObj, String evcode, String evsource)
  {
    String text;
    String updateTimeText = null;
    if (str == null || (str = str.trim()).length() == 0 || source == null)
    {
      return null;
    }
    if (evcode == null)
    {
      evcode = code;
    }
    if (evsource == null)
    {
      evsource = source;
    }
    if (code == null || code.length() == 0)
    {
      code = DEFAULT_EVENT_SOURCE_CODE;
    }
    if (str.indexOf(' ') >= 0)
    {
      String linkStr = null;
      final String[] strArray = str.split(" +");
      for (int i = 0; i < strArray.length; i++)
      {
        str = strArray[i];
        linkStr = getProductLink(str, code, source, type, updateTimeObj, evcode,
            evsource);
        // if last format string or if URL exists
        if (i == strArray.length - 1
            || FileUtils.checkConnection(linkStr) != null)
        {
          break;
        }
      }
      return linkStr;
    }
    for (ProductLinkVariable var : ProductLinkVariable.values())
    {
      text = null;
      switch (var)
      {
      case CODE:
        text = code;
        break;
      case EVCODE:
        text = evcode;
        break;
      case EVSOURCE:
        text = evsource;
        break;
      case SOURCE:
        text = source;
        break;
      case TYPE:
        text = type;
        break;
      case TIME:
        if (updateTimeText == null)
        {
          updateTimeText = getUpdateTimeText(updateTimeObj);
        }
        if (!updateTimeText.isEmpty())
        {
          text = updateTimeText;
        }
        break;
      }
      if (text != null)
      {
        str = var.getPattern().matcher(str).replaceAll(text);
      }
    }
    return str;
  }

  /**
   * Get the update time text.
   * 
   * @param updateTimeObj the update time object or null if none.
   * @return the update time text or an empty string if none.
   */
  private static String getUpdateTimeText(Object updateTimeObj)
  {
    try
    {
      if (updateTimeObj != null)
      {
        final long updateTime;
        if (updateTimeObj instanceof java.util.Date)
        {
          updateTime = ((java.util.Date) updateTimeObj).getTime();
        }
        else if (updateTimeObj instanceof java.time.Instant)
        {
          updateTime = ((java.time.Instant) updateTimeObj).toEpochMilli();
        }
        else if (updateTimeObj instanceof Number)
        {
          updateTime = ((Number) updateTimeObj).longValue();
        }
        else
        {
          String updateTimeText = updateTimeObj.toString();
          updateTime = Long.parseLong(updateTimeText);
          return updateTimeText;
        }
        return Long.toString(updateTime);
      }
    }
    catch (Exception ex)
    {
    }
    return EMPTY_STRING;
  }

  /**
   * Test program.
   * 
   * @param args the program arguments. <br>
   *             <br>
   *             Space separated list of:<br>
   *             code/source/time<br>
   *             <br>
   *             For example:<br>
   *             ok2021eltk/us/1614955622378
   */
  public static void main(String[] args)
  {
    if (args.length == 0)
    {
      System.out.println("Usage : " + EventLinkCreator.class.getName()
          + " code/source/time ...");
      return;
    }

    String[] argArray;
    String code, source;
    long updateTime;
    String evcode = null;
    String evsource = "";
    final String type = "shakemap";
    String linkStr;
    String[] formatStrings;
    formatStrings = new String[]
    { "https://earthquake.usgs.gov/earthquakes/eventpage/{EVCODE}{EVSOURCE}/dyfi",
        "https://earthquake.usgs.gov/earthquakes/eventpage/{EVCODE}{EVSOURCE}/pager",
        "https://earthquake.usgs.gov/earthquakes/eventpage/{EVCODE}{EVSOURCE}/shakemap",
        "https://earthquake.usgs.gov/earthquakes/eventpage/{EVCODE}{EVSOURCE}/moment-tensor",
        "https://earthquake.usgs.gov/archive/product/shakemap/{CODE}/{SOURCE}/{TIME}/download/stationlist.json",
        "https://earthquake.usgs.gov/archive/product/{TYPE}/{CODE}/{SOURCE}/{TIME}/download/shape.zip", };
    for (String arg : args)
    {
      try
      {
        argArray = arg.split("\\/");
        code = argArray[0];
        source = argArray[1];
        updateTime = Long.parseLong(argArray[2]);
        for (String formatString : formatStrings)
        {
          linkStr = getProductLink(formatString, code, source, type, updateTime,
              evcode, evsource);
          if (FileUtils.checkConnection(linkStr) == null)
          {
            System.err.println(linkStr);
            System.err.flush();
          }
          else
          {
            System.out.println(linkStr);
            System.out.flush();
          }
        }
      }
      catch (Exception ex)
      {
        System.err.println("Invalid args (" + Arrays.asList(args) + ")");
      }
    }
  }
}
