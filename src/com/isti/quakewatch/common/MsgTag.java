//MsgTag.java:  Holds static strings containing QuakeWatch XML and
//              ANSS-EQ-XML message tag names and values.
//
//  10/7/2002 -- [ET]  Initial version.
//   6/4/2003 -- [ET]  Added "QWresend" and "NumRequested".
//  6/19/2003 -- [KF]  Added Trump event message-type.
//  8/19/2003 -- [ET]  Added tag name for "Quarry" attribute.
//  11/4/2003 -- [ET]  Added tag names for "Relay..." attributes.
// 12/19/2003 -- [ET]  Added tag names for "MsgEvtDomain", "MsgEvtType",
//                     "MsgEvtName", "ServerAddress" and "RelayServerAddr".
//   1/7/2004 -- [ET]  Added tag names for "MsgEvtMsgNum", "FilteredMsgNum",
//                     "DomainName" and "TypeName".
//   9/8/2004 -- [KF]  Added tag names and values for units.
//  1/20/2005 -- [ET]  Added tag names for "FdrSourceHost", "FdrSourceMsgNum"
//                     "OrigTimeGenerated".
//  5/10/2005 -- [ET]  Added "StationAmp" tag names.
//   8/5/2005 -- [ET]  Added "Tsumani" tag name.
//  8/16/2006 -- [ET]  Added tags for ANSS EQ XML spec.
//  9/12/2006 -- [ET]  Added 'ANSSEQ_NAMESPACE_STR'; removed 'NUM_READINGS'
//                     tag name.
//  10/2/2006 -- [ET]  Added 'CUBE_CODE' and 'INFO' tag items.
// 11/20/2006 -- [ET]  Added 'MSG_SRC' and 'MSG_IDENT' tag items.
//   3/4/2008 -- [ET]  Added 'ST_RPT_TIME' item.
// 11/16/2009 -- [ET]  Added items USAGE, SCOPE, ACTUAL, SCENARIO, TEST,
//                     PUBLIC and INTERNAL.
//  1/22/2010 -- [ET]  Added RELAY_FIRSTSVR_ID and RELAY_FIRSTSVR_ADDR
//                     tag items.
//  3/10/2010 -- [ET]  Added items for QuakeML support.
//  4/21/2010 -- [ET]  Updated items for QuakeML support.
//  5/12/2010 -- [ET]  Added 'LOCAL_STR' tag item.
//  3/13/2019 -- [KF]  Modified to use the PDL Indexer.
//  2/10/2021 -- [KF]  Added losspager and moment-tensor products.
//

package com.isti.quakewatch.common;

import com.isti.util.IstiVersion;

/**
 * Class MsgTag holds static strings containing QuakeWatch XML and
 * ANSS-EQ-XML message tag names and values.
 */
public class MsgTag
{
    /** Minimum version */
  public static final IstiVersion MINIMUM_VERSION = new IstiVersion("2.0");
    /** Version number string for XML message format. */
  public static final String VERSION_NUMBER = "2.0";

    /** Value for "Yes" on flag attributes. */
  public static final String YES = "Y";
    /** Value for "No" on flag attributes. */
  public static final String NO = "N";
    /** Value for degrees on units attributes. */
  public static final String DEGREES = "degrees";
    /** Value for KM on units attributes. */
  public static final String KM = "km";
    /** Value for seconds on units attributes. */
  public static final String SECONDS = "seconds";

    /** Tag name for Value attribute on various elements. */
  public static final String VALUE = "Value";
    /** Tag name for MsgTypeCode attribute on various elements. */
  public static final String MSG_TYPE_CODE = "MsgTypeCode";
    /** Tag name for Type attribute on various elements. */
  public static final String TYPE = "Type";
    /** Tag name for NumStations attribute on various elements. */
  public static final String NUM_STATIONS = "NumStations";
    /** Tag name for Comment attribute on various elements. */
  public static final String COMMENT = "Comment";
    /** Tag name for Name attribute on various elements. */
  public static final String NAME = "Name";

    /** Value for "New" used in DataMessage.Action. */
  public static final String NEW = "New";
    /** Value for "Update" used in DataMessage.Action. */
  public static final String UPDATE = "Update";
    /** Value for "Delete" used in DataMessage.Action. */
  public static final String DELETE = "Delete";
    /** Value for "Trump" used in DataMessage.Action. */
  public static final String TRUMP = "Trump";
    /** Value for "Earthquake" used in Event.Type. */
  public static final String EARTHQUAKE = "Earthquake";
    /** Value for "TextComment" used in Product.Type. */
  public static final String TEXT_COMMENT = "TextComment";
  /** Value for "ShakeMapURL" used in Product.Type. */
  public static final String SHAKEMAP_URL = "ShakeMapURL";
  /** Value for "FocalMech1URL" used in Product.Type. */
  public static final String FOCAL_MECH1_URL = "FocalMech1URL";
  /** Value for "FocalMech2URL" used in Product.Type. */
  public static final String FOCAL_MECH2_URL = "FocalMech2URL";
  /** Value for "RealTimeMechURL" used in Product.Type. */
  public static final String REALTIME_MECH_URL = "RealTimeMechURL";
  /** Value for "MomentTensorURL" used in Product.Type. */
  public static final String MOMENT_TENSOR_URL = "MomentTensorURL";
  /** Value for "DidYouFeelIt" used in Product.Type. */
  public static final String DIDYOU_FEELIT = "DidYouFeelIt";
  /** Value for "Tsunami" used in Product.Type. */
  public static final String TSUNAMI = "Tsunami";
  /** Value for "LinkURL" used in Product.Type. */
  public static final String LINK_URL = "LinkURL";
  /** Value for "LossPAGER" used in Product.Type. */
  public static final String LossPAGER = "LossPAGER";
  /** Value for "moment-tensor" used in Product.Type */
  public static final String MomentTensor = "moment-tensor";

                        //values for CUBIC message-type ID strings:
  public static final String EQUAKE_MSGSTR = "E ";    //Earthquake
  public static final String DELQK_MSGSTR = "DE";     //Delete Earthquake
  public static final String TRUMP_MSGSTR = "TR";     //Trump event
  public static final String TEXT_MSGSTR = "TX";      //Text Comment
  public static final String LINK_MSGSTR = "LI";      //Link to Add-on

    /** Tag name for the QWmessage element. */
  public static final String QW_MESSAGE = "QWmessage";
    /** Tag name for MsgNumber attribute of QWmessage element. */
  public static final String MSG_NUMBER = "MsgNumber";
    /** Tag name for MsgVersion attribute of QWmessage element. */
  public static final String MSG_VERSION = "MsgVersion";
    /** Tag name for ServerRevStr attribute of QWmessage element. */
  public static final String SERVER_REV_STR = "ServerRevStr";
    /** Tag name for ServerIDName attribute of QWmessage element. */
  public static final String SERVER_ID_NAME = "ServerIDName";
    /** Tag name for ServerAddress attribute of QWmessage element. */
  public static final String SERVER_ADDRESS = "ServerAddress";
    /** Tag name for TimeGenerated attribute of QWmessage element. */
  public static final String TIME_GENERATED = "TimeGenerated";
    /** Tag name for MsgSource attribute of QWmessage element. */
  public static final String MSG_SOURCE = "MsgSource";
    /** Tag name for MsgEvtDomain attribute of QWmessage element. */
  public static final String MSG_EVT_DOMAIN = "MsgEvtDomain";
    /** Tag name for MsgEvtType attribute of QWmessage element. */
  public static final String MSG_EVT_TYPE = "MsgEvtType";
    /** Tag name for MsgEvtName attribute of QWmessage element. */
  public static final String MSG_EVT_NAME = "MsgEvtName";
    /** Tag name for MsgEvtMsgNum attribute of QWmessage element. */
  public static final String MSG_EVT_MSG_NUM = "MsgEvtMsgNum";
    /** Tag name for Signature attribute of QWmessage element. */
  public static final String SIGNATURE = "Signature";
    /** Tag name for FdrSourceHost attribute of QWmessage element. */
  public static final String FDR_SOURCE_HOST = "FdrSourceHost";
    /** Tag name for FdrSourceMsgNum attribute of QWmessage element. */
  public static final String FDR_SOURCE_MSGNUM = "FdrSourceMsgNum";
    /** Tag name for OrigTimeGenerated attribute of QWmessage element. */
  public static final String ORIG_TIME_GENERATED = "OrigTimeGenerated";

    /** Tag name for the DataMessage element. */
  public static final String DATA_MESSAGE = "DataMessage";
    /** Tag name for Action attribute of DataMessage element. */
  public static final String ACTION = "Action";
    /** Tag name for TimeReceived attribute of DataMessage element. */
  public static final String TIME_RECEIVED = "TimeReceived";
    /** Tag name for FeederMsgNum attribute of DataMessage element. */
  public static final String FEEDER_MSG_NUM = "FeederMsgNum";
    /** Tag name for SrcHostMsgID attribute of DataMessage element. */
  public static final String SRC_HOST_MSG_ID = "SrcHostMsgID";
    /** Tag name for SourceHost attribute of DataMessage element. */
  public static final String SOURCE_HOST = "SourceHost";

    /** Tag name for RelayMsgNum attribute of DataMessage element. */
  public static final String RELAY_MSG_NUM = "RelayMsgNum";
    /** Tag name for RelayTimeGen attribute of DataMessage element. */
  public static final String RELAY_TIME_GEN = "RelayTimeGen";
    /** Tag name for RelayServerID attribute of DataMessage element. */
  public static final String RELAY_SERVER_ID = "RelayServerID";
    /** Tag name for RelayServerAddr attribute of DataMessage element. */
  public static final String RELAY_SERVER_ADDR = "RelayServerAddr";
    /** Tag name for RelayMsgSrc attribute of DataMessage element. */
  public static final String RELAY_MSG_SRC = "RelayMsgSrc";
    /** Tag name for RelayFirstSvrID attribute of DataMessage element. */
  public static final String RELAY_FIRSTSVR_ID = "RelayFirstSvrID";
    /** Tag name for RelayFirstSvrAddr attribute of DataMessage element. */
  public static final String RELAY_FIRSTSVR_ADDR = "RelayFirstSvrAddr";

    /** Tag name for the Identifier element. */
  public static final String IDENTIFIER = "Identifier";
    /** Tag name for EventIDKey attribute of Identifier element. */
  public static final String EVENT_ID_KEY = "EventIDKey";
    /** Tag name for DataSource attribute of Identifier element. */
  public static final String DATA_SOURCE = "DataSource";
    /** Tag name for Authoritative attribute of Identifier element. */
  public static final String AUTHORITATIVE = "Authoritative";
    /** Tag name for Associate attribute of Identifier element. */
  public static final String ASSOCIATE = "Associate";
  /** Tag name for LowPriority attribute of Identifier element. */
  public static final String LOW_PRIORITY = "LowPriority";
    /** Tag name for Version attribute of Identifier element. */
  public static final String VERSION = "Version";

    /** Tag name for the Magnitude element. */
  public static final String MAGNITUDE = "Magnitude";
    /** Tag name for NumReadings attribute of Magnitude element. */
//  public static final String NUM_READINGS = "NumReadings";
    /** Tag name for MagMethod attribute of Magnitude element. */
  public static final String MAG_METHOD = "MagMethod";
    /** Tag name for MagError attribute of Magnitude element. */
  public static final String MAG_ERROR = "MagError";

    /** Tag name for the Event element. */
  public static final String EVENT = "Event";
    /** Tag name for Time attribute of Event element. */
  public static final String TIME = "Time";
    /** Tag name for Latitude attribute of Event element. */
  public static final String LATITUDE = "Latitude";
    /** Tag name for Longitude attribute of Event element. */
  public static final String LONGITUDE = "Longitude";
    /** Tag name for Depth attribute of Event element. */
  public static final String DEPTH = "Depth";
    /** Tag name for HorizontalError attribute of Event element. */
  public static final String HORIZONTAL_ERROR = "HorizontalError";
    /** Tag name for VerticalError attribute of Event element. */
  public static final String VERTICAL_ERROR = "VerticalError";
    /** Tag name for RMSTimeError attribute of Event element. */
  public static final String RMS_TIME_ERROR = "RMSTimeError";
    /** Tag name for AzimuthalGap attribute of Event element. */
  public static final String AZIMUTHAL_GAP = "AzimuthalGap";
    /** Tag name for LocationMethod attribute of Event element. */
  public static final String LOCATION_METHOD = "LocationMethod";
    /** Tag name for Verified attribute of Event element. */
  public static final String VERIFIED = "Verified";
    /** Tag name for NumPhases attribute of Event element. */
  public static final String NUM_PHASES = "NumPhases";
    /** Tag name for MinDistance attribute of Event element. */
  public static final String MIN_DISTANCE = "MinDistance";
    /** Tag name for Quarry attribute of Event element. */
  public static final String QUARRY = "Quarry";

    /** Tag name for Lat/Lon Units attribute of Event element. */
  public static final String LAT_LON_UNITS = "LatLonUnits";
    /** Tag name for Depth Units attribute of Event element. */
  public static final String DEPTH_UNITS = "DepthUnits";
    /** Tag name for HorizontalError Units attribute of Event element. */
  public static final String HORIZONTAL_ERROR_UNITS = "HorizontalErrorUnits";
    /** Tag name for VerticalError Units attribute of Event element. */
  public static final String VERTICAL_ERROR_UNITS = "VerticalErrorUnits";
    /** Tag name for RMSTimeError Units attribute of Event element. */
  public static final String RMS_TIME_ERROR_UNITS = "RMSTimeErrorUnits";
    /** Tag name for AzimuthalGap Units attribute of Event element. */
  public static final String AZIMUTHAL_GAP_UNITS = "AzimuthalGapUnits";
    /** Tag name for MinDistance Units attribute of Event element. */
  public static final String MIN_DISTANCE_UNITS = "MinDistanceUnits";

    /** Tag name for the Product element. */
  public static final String PRODUCT = "Product";
    /** Tag name for ProdTypeCode attribute of Product element. */
  public static final String PROD_TYPE_CODE = "ProdTypeCode";

    /** Tag name for the StatusMessage element. */
  public static final String STATUS_MESSAGE = "StatusMessage";
    /** Tag name for MsgType attribute of StatusMessage element. */
  public static final String MSG_TYPE = "MsgType";
    /** Tag name for MsgData attribute of StatusMessage element. */
  public static final String MSG_DATA = "MsgData";
    /** Type value for MsgType attribute of StatusMessage element. */
  public static final String STARTUP = "Startup";
    /** Type value for MsgType attribute of StatusMessage element. */
  public static final String ALIVE = "Alive";
    /** Type value for MsgType attribute of StatusMessage element. */
  public static final String STATUS = "Status";
    /** Type value for MsgType attribute of StatusMessage element. */
  public static final String TERMINATING = "Terminating";
    /** Data-prompt value for MsgData attribute of StatusMessage element. */
  public static final String ST_RPT_TIME = "stRptTime";

    /** Tag name for the FilteredMsgNum element. */
  public static final String FILTERED_MSG_NUM = "FilteredMsgNum";
    /** Tag name for DomainName attribute of FilteredMsgNum element. */
  public static final String DOMAIN_NAME = "DomainName";
    /** Tag name for TypeName attribute of FilteredMsgNum element. */
  public static final String TYPE_NAME = "TypeName";


    /** Tag name for EQMessage element for ANSS EQ. */
  public static final String EQMESSAGE = "EQMessage";
    /** Namespace string value for ANSS-EQ 'xmlns' attribute. */
  public static final String ANSSEQ_NAMESPACE_STR =
                                            "http://www.usgs.gov/ansseqmsg";
    /** Tag name for Module child-element of ANSS-EQ EQMessage element. */
  public static final String MODULE = "Module";
    /** Tag name for MsgSrc child-element of ANSS-EQ EQMessage element. */
  public static final String MSG_SRC = "MsgSrc";
    /** Tag name for MsgIdent child-element of ANSS-EQ EQMessage element. */
  public static final String MSG_IDENT = "MsgIdent";
    /** Tag name for Sent child-element of ANSS-EQ EQMessage element. */
  public static final String SENT = "Sent";
    /** Tag name for EventID child-element of ANSS-EQ Event element. */
  public static final String EVENT_ID = "EventID";
    /** Tag name for Usage child-element of ANSS-EQ Event element. */
  public static final String USAGE = "Usage";
    /** Tag name for Scope child-element of ANSS-EQ Event element. */
  public static final String SCOPE = "Scope";
    /** Tag name for Origin child-element of ANSS-EQ Event element. */
  public static final String ORIGIN = "Origin";
    /** Tag name for ProductLink child-element of ANSS-EQ Event element. */
  public static final String PRODUCT_LINK = "ProductLink";
    /** Tag name for NumStaUsed child-element of ANSS-EQ Origin element. */
  public static final String NUM_STA_USED = "NumStaUsed";
    /** Tag name for NumPhaUsed child-element of ANSS-EQ Origin element. */
  public static final String NUM_PHA_USED = "NumPhaUsed";
    /** Tag name for MinDist child-element of ANSS-EQ Origin element. */
  public static final String MIN_DIST = "MinDist";
    /** Tag name for StdError child-element of ANSS-EQ Origin element. */
  public static final String STD_ERROR = "StdError";
    /** Tag name for Errh child-element of ANSS-EQ Origin element. */
  public static final String ERRH = "Errh";
    /** Tag name for Errz child-element of ANSS-EQ Origin element. */
  public static final String ERRZ = "Errz";
    /** Tag name for AzimGap child-element of ANSS-EQ Origin element. */
  public static final String AZIM_GAP = "AzimGap";
    /** Tag name for Method child-element of ANSS-EQ Origin element. */
  public static final String METHOD = "Method";
    /** Tag name for SourceKey elements. */
  public static final String SOURCE_KEY = "SourceKey";
    /** Tag name for TypeKey elements. */
  public static final String TYPE_KEY = "TypeKey";


    /** Tag name for 'quakeMessage' element for QuakeML. */
  public static final String QUAKE_MESSAGE = "quakeMessage";
    /** Tag name for 'quakeml' element for QuakeML. */
  public static final String QUAKE_ML = "quakeml";
    /** Namespace string value for QuakeML 'xmlns' attribute. */
  public static final String QUAKEML_NAMESPACE_STR =
                                     "http://quakeml.org/xmlns/quakeml/1.1";
    /** Namespace string value for QuakeML 'xmlns:anss' attribute. */
  public static final String ANSS_NAMESPACE_STR =
                                "http://www.usgs.gov/xmlns/anssquakeml/1.0";
    /** Tag name for 'publicID' attribute for QuakeML. */
  public static final String PUBLIC_ID = "publicID";
    /** Value string for QuakeML 'anss' prefix. */
  public static final String ANSS_STR = "anss";
    /** Value string for QuakeML 'local' value. */
  public static final String LOCAL_STR = "local";
    /** Value string for QuakeML 'smi:local/' prefix. */
  public static final String SMI_LOCAL_STR = "smi:" + LOCAL_STR + "/";
    /** Value string for QuakeML 'evt'. */
  public static final String QUAKEML_EVT_STR = "evt";
    /** Value string for QuakeML 'prd'. */
  public static final String QUAKEML_PRD_STR = "prd";
    /** Value string for QuakeML '#evt_' insert. */
  public static final String EVT_ID_STR = '#' + QUAKEML_EVT_STR + '_';
    /** Value string for QuakeML '#org_' insert. */
  public static final String ORG_ID_STR = "#org_";
    /** Value string for QuakeML '#mag_' insert. */
  public static final String MAG_ID_STR = "#mag_";
    /** Value string for QuakeML '#prd_' insert. */
  public static final String PRD_ID_STR = '#' + QUAKEML_PRD_STR + '_';
    /** Value string for QuakeML 'id' attribute. */
  public static final String ID_STR = "id";
    /** Tag name for 'eventParameters' element for QuakeML. */
  public static final String EVENT_PARAMETERS = "eventParameters";
    /** Value for 'publicID' attribute of 'eventParameters' element. */
  public static final String EVENT_PARAMS_ID = SMI_LOCAL_STR + "NEIC#evtprm";
    /** Value for prefix for 'methodID' element. */
  public static final String METHOD_ID_PREFIX = SMI_LOCAL_STR + "NEIC#";
    /** Value for 'id' attribute of 'origin' method-class comment. */
  public static final String METHOD_CLASS_ID = SMI_LOCAL_STR +
                                                             "method#class";
    /** Value string "CUBE_Code". */
  public static final String CUBECODE_STR = "CUBE_Code";
    /** Value string "locMeth". */
  public static final String LOC_METH = "locMeth";
    /** Value string "magType". */
  public static final String MAG_TYPE = "magType";
    /** Value for 'id' attribute of 'origin' "CUBE_Code" comment. */
  public static final String CUBECODE_LOCMETH_ID = SMI_LOCAL_STR +
                                              CUBECODE_STR + '#' + LOC_METH;
    /** Value for 'id' attribute of 'magnitude' "CUBE_Code" comment. */
  public static final String CUBECODE_MAGTYPE_ID = SMI_LOCAL_STR +
                                              CUBECODE_STR + '#' + MAG_TYPE;
    /** Tag name for 'event' element for QuakeML. */
  public static final String QUAKEML_EVENT = "event";
    /** Tag name for 'module' element for QuakeML. */
  public static final String QUAKEML_MODULE = "module";
    /** Tag name for 'msgSrc' element for QuakeML. */
  public static final String QUAKEML_MSG_SRC = "msgSrc";
    /** Tag name for 'msgIdent' element for QuakeML. */
  public static final String QUAKEML_MSG_IDENT = "msgIdent";
    /** Tag name for 'creationInfo' element for QuakeML. */
  public static final String CREATION_INFO = "creationInfo";
    /** Tag name for 'agencyID' element for QuakeML. */
  public static final String AGENCY_ID = "agencyID";
    /** Tag name for 'creationTime' element for QuakeML. */
  public static final String CREATION_TIME = "creationTime";
    /** Tag name for 'version' element for QuakeML. */
  public static final String QUAKEML_VERSION = "version";
    /** Tag name for 'preferredOriginID' element for QuakeML. */
  public static final String PREFERRED_ORIGIN_ID = "preferredOriginID";
    /** Tag name for 'preferredMagnitudeID' element for QuakeML. */
  public static final String PREFERRED_MAGNITUDE_ID = "preferredMagnitudeID";
    /** Tag name for 'type' element for QuakeML. */
  public static final String QUAKEML_TYPE = "type";
    /** Tag name for 'usage' element for QuakeML. */
  public static final String QUAKEML_USAGE = "usage";
    /** Tag name for 'scope' element for QuakeML. */
  public static final String QUAKEML_SCOPE = "scope";
    /** Tag name for 'origin' element for QuakeML. */
  public static final String QUAKEML_ORIGIN = "origin";
    /** Tag name for 'time' element for QuakeML. */
  public static final String QUAKEML_TIME = "time";
    /** Tag name for 'latitude' element for QuakeML. */
  public static final String QUAKEML_LATITUDE = "latitude";
    /** Tag name for 'longitude' element for QuakeML. */
  public static final String QUAKEML_LONGITUDE = "longitude";
    /** Tag name for 'depth' element for QuakeML. */
  public static final String QUAKEML_DEPTH = "depth";
    /** Tag name for 'methodID' element for QuakeML. */
  public static final String METHOD_ID = "methodID";
    /** Tag name for 'quality' element for QuakeML. */
  public static final String QUALITY = "quality";
    /** Tag name for 'usedPhaseCount' element for QuakeML. */
  public static final String USED_PHASE_COUNT = "usedPhaseCount";
    /** Tag name for 'usedStationCount' element for QuakeML. */
  public static final String USED_STATION_COUNT = "usedStationCount";
    /** Tag name for 'standardError' element for QuakeML. */
  public static final String STANDARD_ERROR = "standardError";
    /** Tag name for 'azimuthalGap' element for QuakeML. */
  public static final String QUAKEML_AZIMUTHAL_GAP = "azimuthalGap";
    /** Tag name for 'minimumDistance' element for QuakeML. */
  public static final String MINIMUM_DISTANCE = "minimumDistance";
    /** Tag name for 'errh' element for QuakeML. */
  public static final String QUAKEML_ERRH = "errh";
    /** Tag name for 'errz' element for QuakeML. */
  public static final String QUAKEML_ERRZ = "errz";
    /** Tag name for 'evaluationStatus' element for QuakeML. */
  public static final String EVALUATION_STATUS = "evaluationStatus";
    /** Tag name for 'comment' element for QuakeML. */
  public static final String QUAKEML_COMMENT = "comment";
    /** Tag name for 'text' element for QuakeML. */
  public static final String QUAKEML_TEXT = "text";
    /** Tag name for 'magnitude' element for QuakeML. */
  public static final String QUAKEML_MAGNITUDE = "magnitude";
    /** Tag name for 'mag' element for QuakeML. */
  public static final String MAG = "mag";
    /** Tag name for 'value' element for QuakeML. */
  public static final String QUAKEML_VALUE = "value";
    /** Tag name for 'uncertainty' element for QuakeML. */
  public static final String UNCERTAINTY = "uncertainty";
    /** Tag name for 'originID' element for QuakeML. */
  public static final String ORIGIN_ID = "originID";
    /** Tag name for 'stationCount' element for QuakeML. */
  public static final String STATION_COUNT = "stationCount";
    /** Tag name for 'useControl' element for QuakeML. */
  public static final String USE_CONTROL = "useControl";
    /** Tag name for 'elementID' element for QuakeML. */
  public static final String ELEMENT_ID = "elementID";
    /** Tag name for 'action' element for QuakeML. */
  public static final String QUAKEML_ACTION = "action";
    /** Tag name for 'productLink' element for QuakeML. */
  public static final String QUAKEML_PRODUCT_LINK = "productLink";
    /** Tag name for 'code' element for QuakeML. */
  public static final String QUAKEML_CODE = "code";
    /** Tag name for 'note' element for QuakeML. */
  public static final String QUAKEML_NOTE = "note";
    /** Tag name for 'link' element for QuakeML. */
  public static final String QUAKEML_LINK = "link";
    /** Value for QuakeML event type "earthquake". */
  public static final String QUAKEML_EARTHQUAKE = "earthquake";
    /** Value for QuakeML event type "quarry blast". */
  public static final String QUAKEML_QUARRY = "quarry blast";
    /** Value "preliminary" for QuakeML 'evaluationStatus' element. */
  public static final String PRELIMINARY = "preliminary";
    /** Value "confirmed" for QuakeML 'evaluationStatus' element. */
  public static final String CONFIRMED = "confirmed";
    /** Value "reviewed" for QuakeML 'evaluationStatus' element. */
  public static final String QUAKEML_REVIEWED = "reviewed";
    /** Value "final" for QuakeML 'evaluationStatus' element. */
  public static final String FINAL = "final";

    /** Value for "delete" used in QuakeML 'useControl' element. */
  public static final String QUAKEML_DELETE = "delete";
    /** Value for "trump" used in QuakeML 'useControl' element. */
  public static final String QUAKEML_TRUMP = "trump";


    /** Tag name for Error elements. */
  public static final String ERROR = "Error";
    /** Tag name for PreferredFlag elements. */
  public static final String PREFERRED_FLAG = "PreferredFlag";
    /** Tag name for Code element. */
  public static final String CODE = "Code";
    /** Tag name for Note element. */
  public static final String NOTE = "Note";
    /** Tag name for Link element. */
  public static final String LINK = "Link";
    /** Value string for "Automatic" in Origin | Status element. */
  public static final String AUTOMATIC = "Automatic";
    /** Value string for "Reviewed" in Origin | Status element. */
  public static final String QUICK_REVIEWED = "QuickReviewed";
    /** Value string for "Reviewed" in Origin | Status element. */
  public static final String REVIEWED = "Reviewed";
    /** Value string for "Published" in Origin | Status element. */
  public static final String PUBLISHED = "Published";
    /** Tag name for Class child-element of Method element. */
  public static final String CLASS = "Class";
    /** Tag name for Algorithm child-element of Method element. */
  public static final String ALGORITHM = "Algorithm";
    /** Value string for "Actual" in Event | Status element. */
  public static final String ACTUAL = "Actual";
    /** Value string for "Scenario" in Event | Status element. */
  public static final String SCENARIO = "Scenario";
    /** Value string for "Test" in Event | Status element. */
  public static final String TEST = "Test";
    /** Value string for "Public" in Event | Scope element. */
  public static final String PUBLIC = "Public";
    /** Value string for "Internal" in Event | Scope element. */
  public static final String INTERNAL = "Internal";
    /** Value string for "Text". */
  public static final String TEXT = "Text";
    /** Value string for "Info". */
  public static final String INFO = "Info";
    /** Value string for "Unknown". */
  public static final String UNKNOWN = "Unknown";
    /** Value string for "true". */
  public static final String TRUE = "true";
    /** Value string for "false". */
  public static final String FALSE = "false";
    /** Value string for "CUBE_Code ". */
  public static final String CUBE_CODE = CUBECODE_STR + ' ';


    /** Tag name for the QWresend element. */
  public static final String QW_RESEND = "QWresend";
    /** Tag name for NumRequested attribute of QWresend element. */
  public static final String NUM_REQUESTED = "NumRequested";


    /** Array of values used in Product.Type. */
  public static final String [] productTypeStrsArr =
          { TEXT_COMMENT, SHAKEMAP_URL, FOCAL_MECH1_URL, FOCAL_MECH2_URL,
            REALTIME_MECH_URL, MOMENT_TENSOR_URL, DIDYOU_FEELIT, LINK_URL };


    /** Tag name for the StationAmp element. */
  public static final String STATION_AMP = "StationAmp";

    /** Tag name for Sta attribute of StationAmp element. */
  public static final String STA = "Sta";
    /** Tag name for NetId attribute of StationAmp element. */
  public static final String NET = "Net";
    /** Tag name for Lat attribute of StationAmp element. */
  public static final String LAT = "Lat";
    /** Tag name for Lon attribute of StationAmp element. */
  public static final String LON = "Lon";
    /** Tag name for Elev attribute of StationAmp element. */
  public static final String ELEV = "Elev";
    /** Tag name for InstType attribute of StationAmp element. */
  public static final String INST_TYPE = "InstType";
    /** Tag name for Source attribute of StationAmp element. */
  public static final String SOURCE = "Source";
    /** Tag name for CommType attribute of StationAmp element. */
  public static final String COMM_TYPE = "CommType";

    /** Tag name for the Comp element. */
  public static final String COMP = "Comp";

    /** Tag name for Loc attribute of Comp element. */
  public static final String LOC = "Loc";

    /** Tag name for the Amp element. */
  public static final String AMP = "Amp";

    /** Tag name for Units attribute of Amp element. */
  public static final String UNITS = "Units";
    /** Tag name for TOffMs attribute of Amp element. */
  public static final String T_OFF_MS = "TOffMs";
    /** Tag name for Freq attribute of Amp element. */
  public static final String FREQ = "Freq";
    /** Tag name for SMFlag attribute of Amp element. */
  public static final String SM_FLAG = "SMFlag";

    /** Acceleration value for Type attribute of Amp element. */
  public static final String TYPE_ACC_VAL = "A";
    /** Velocity value for Type attribute of Amp element. */
  public static final String TYPE_VEL_VAL = "V";
    /** PSA value for Type attribute of Amp element. */
  public static final String TYPE_PSA_VAL = "P";
}
