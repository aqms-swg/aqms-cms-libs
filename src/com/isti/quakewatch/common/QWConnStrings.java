//QWConnStrings.java:  Holds static strings containing names for
//                     connection properties.
//
//   2/2/2004 -- [ET]
//  8/12/2004 -- [ET]  Added 'DISTRIB_NAME'.
//  9/30/2004 -- [ET]  Added 'CLIENT_AUTH_IP' and 'CLIENT_AUTH_HOST'.
// 12/16/2005 -- [ET]  Added 'CLIENT_USERNAME'.
//  1/30/2018 -- [KF]  Added 'CLIENT_FLAGS'.
//

package com.isti.quakewatch.common;

/**
 * Class QWConnStrings holds static strings containing names for
 * connection properties.
 */
public class QWConnStrings
{
  /** Client flags */
  public static final String CLIENT_FLAGS = "ClientFlags";
  /** Name of client program. */
  public static final String CLIENT_NAME = "ClientName";

  /** Version of client program. */
  public static final String CLIENT_VERSION = "ClientVersion";

  /** Distribution name for program. */
  public static final String DISTRIB_NAME = "DistribName";

  /** Startup time string for client program. */
  public static final String STARTUP_TIME = "StartupTime";

  /** IP address for client program. */
  public static final String CLIENT_IP = "ClientIP";

  /** Host name for client program. */
  public static final String CLIENT_HOST = "ClientHost";

  /** Version of client-communications code. */
  public static final String COMM_VERSION = "CommVersion";

  /** Version of OpenORB code. */
  public static final String OPENORB_VERSION = "OpenORBVersion";

  /** Operating system name for client program. */
  public static final String CLIENT_OSNAME = "ClientOSName";

  /** Java version string for client program. */
  public static final String JAVA_VERSION = "JavaVersion";

  /** Client IP address to be used for login authentication. */
  public static final String CLIENT_AUTH_IP = "ClientAuthIP";

  /** Client host name to be used for login authentication. */
  public static final String CLIENT_AUTH_HOST = "ClientAuthHost";

  /** Client username used for login authentication. */
  public static final String CLIENT_USERNAME = "ClientUsername";
}
