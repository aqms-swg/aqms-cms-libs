package com.isti.quakewatch.common;

import com.isti.util.Version;

public interface QWConstants extends Version
{
  /** Name of directory for log files. */
  public static final String LOG_DIR_NAME = "log";
    /** Minimum version of Java required. */
  public static final String MIN_JAVA_VERSION = "1.8";
    /** Default for server or client connection timeout in seconds */
  public static final Integer CONNECTION_TIMEOUT_SECS_DEFAULT =
      Integer.valueOf(30);
}
