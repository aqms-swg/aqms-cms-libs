//QWReportStrings.java:  Holds static strings containing prompt strings
//                       for status reports.
//
//   2/1/2008 -- [ET]
//

package com.isti.quakewatch.common;

/**
 * Class QWReportStrings holds static strings containing prompt strings
 * for status reports.
 */
public class QWReportStrings
{
  /** Header string at top of report. */
  public static final String REP_HEADER_STR = "Server Status Report";

  /** Prompt string for server-ID string. */
  public static final String REP_SERVERID_STR = "Server ID:  ";

  /** Indicator string for no tracked log messages. */
  public static final String REP_NOLOGMSGS_STR = "No tracked log messages";

  /** Prompt string for tracked-log-messages section. */
  public static final String REP_TRKLOGMSGS_STR = "Tracked log messages";

  /** Indicator string for no clients connected. */
  public static final String REP_NOCLIENTS_STR =
                                       "No clients are currently connected";

  /** Prompt string for connected-clients section. */
  public static final String REP_CONNCLIENTS_STR =
                                              "Currently connected clients";

  /** Prompt string for number-of-clients value. */
  public static final String REP_NUMCLIENTS_STR =
                                           "Number of connected clients = ";

  /** Prompt string for QWServer-memory values. */
  public static final String REP_QWSEVERMEM_STR = "QWServer memory:  ";

  /** Prompt string for QWServer-thread-count value. */
  public static final String REP_QWSTHRDCNT_STR = "QWServer thread count = ";

  /** Prompt string for notification-server-status section. */
  public static final String REP_NOTIFSTAT_STR =
                                            "Notification Server status on ";

  /** Prompt string for number-of-feeders value. */
  public static final String REP_NUMFEEDERS_STR = "Number of feeders = ";

  /** Prompt string for OpenORB version. */
  public static final String REP_ORBVERSION_STR = "OpenORB version:  ";

  /** Prompt string for Java version. */
  public static final String REP_JAVAVERSION_STR = "Java version:  ";

  /** Prompt string for OS name. */
  public static final String REP_OSNAME_STR = "OS name:  ";

  /** Prompt string for maximum-message-size-allowed value. */
  public static final String REP_MAXMSGSIZE_STR =
                                          "Maximum message size allowed:  ";
}
