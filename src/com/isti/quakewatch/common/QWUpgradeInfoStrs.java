//QWUpgradeInfoStrs.java:  Holds static strings containing names used in
//                         client-upgrade information XML-format messages.
//
//  8/31/2004 -- [ET]
//

package com.isti.quakewatch.common;

/**
 * Class QWUpgradeInfoStrs holds static strings containing names used in
 * client-upgrade information XML-format messages.
 */
public class QWUpgradeInfoStrs
{
    /** Name for outer "ClientUpgradeInfo" element. */
  public static final String CLIENT_UPGRD_INFO = "ClientUpgradeInfo";

    /** Name for "NewerVerAvail" attribute of "ClientUpgradeInfo" elem. */
  public static final String NEWER_VER_AVAIL = "NewerVerAvail";

    /** Name for "ConnectAllowed" attribute of "ClientUpgradeInfo" elem. */
  public static final String CONNECT_ALLOWED = "ConnectAllowed";

    /** Name for "NewVersionNum" attribute of "ClientUpgradeInfo" elem. */
  public static final String NEW_VERSION_NUM = "NewVersionNum";

    /** Name for "DisplayMessage" element. */
  public static final String DISPLAY_MESSAGE = "DisplayMessage";

    /** Name for "Links" element. */
  public static final String LINKS = "Links";

    /** Name for "DistZip" attribute of "Links" elem. */
  public static final String DIST_ZIP = "DistZip";

    /** Name for "DistWebsite" attribute of "Links" elem. */
  public static final String DIST_WEBSITE = "DistWebsite";

    /** Value for "Yes" on flag attributes. */
  public static final String YES = "Y";

    /** Value for "No" on flag attributes. */
  public static final String NO = "N";
}
