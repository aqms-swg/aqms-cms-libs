//ConnectionStatusPanel.java:  Defines a panel for showing the ongoing
//                             status of a connection.
//
//  3/10/2003 -- [ET]  Initial version.
// 10/22/2003 -- [ET]  Took out thread synchronization; modified to
//                     enforce paradigm that changes should only
//                     happen via the event-dispatch thread; added
//                     'isPopupDialogVisible()' method; modified so
//                     that clicking on panel will toggle popup
//                     (instead of only showing it).
// 10/28/2003 -- [ET]  Modified to implement the 'ConnStatusInterface'
//                     interface.
//  3/26/2004 -- [ET]  Moved from 'QWClient' to 'QWCommon' project.
//  2/22/2004 -- [ET]  Modified 'hidePopupDialog()' method to call
//                     IstiDialogPopup 'close()' method because it calls
//                     'dispose()' which ensures that minimized dialog
//                     won't get "stuck".
//

package com.isti.quakewatch.guiutil;

import java.awt.*;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import java.awt.event.WindowListener;
import javax.swing.*;
import com.isti.util.gui.GuiUtilFns;
import com.isti.util.gui.SymbolJLabel;
import com.isti.util.gui.IstiDialogPopup;
import com.isti.quakewatch.util.ConnStatusInterface;

/**
 * Class ConnectionStatusPanel defines a panel for showing the ongoing
 * status of a connection.
 */
public class ConnectionStatusPanel extends JPanel
                                              implements ConnStatusInterface
{
    /** Default maximum text length for text in panel. */
  public static final int DEF_MAX_TXTLEN = 75;
    /** Size of right-side margin, in pixels. */
  public static final int RMARGIN_PIXELS = 18;
    /** Default title string for popup dialog. */
  public static final String DEF_POPUP_TITLESTR = "Connection Status";
              //color object for clear:
  protected static final Color CLEAR_COLOR = new Color(255,255,255,0);
              //array to convert color-index values to Color objects:
  protected static final Color [] IDX_CLR_ARR = {
                  null, CLEAR_COLOR, Color.red, Color.yellow, Color.green };

  protected final JLabel labelObj = new JLabel();     //label for text
  protected final SymbolJLabel symbolObj =            //symbol object
                                      new SymbolJLabel("",CLEAR_COLOR,true);
  protected Component popupParentObj = null;          //parent for popup
  protected int maxTextLength = DEF_MAX_TXTLEN;       //label text max
                        //text string for popup or null for none:
  protected String popupTextString = null;
                        //title string for popup:
  protected String popupTitleString = DEF_POPUP_TITLESTR;
  protected IstiDialogPopup dialogPopupObj = null;    //popup dialog object
                        //multi-line text area for popup dialog:
  protected final JTextArea dialogTextArea = new JTextArea(3,40);
                        //button for popup dialog:
  protected final JButton dialogOkButton = new JButton("OK");
                        //flag set true for modal popup dialog:
  protected boolean popupModalFlag = false;
                        //window listener for popup dialog:
  protected WindowListener popupWindowListenerObj = null;
                        //flag set after user requested popup dialog:
  protected boolean popupRequestedFlag = false;
                        //component to receive focus after popup cleared:
  protected Component refocusComponentObj = null;

  /**
   * Creates a panel for showing the ongoing status of a connection.  A
   * colored-dot symbol appears to right of a short text message.  The
   * panel may be clicked on to display a popup showing a longer status
   * message.
   * @param popupParentObj the parent component to use when displaying
   * the popup dialog, or null for none.
   */
  public ConnectionStatusPanel(Component popupParentObj)
  {
    this.popupParentObj = popupParentObj;        //save component
              //setup flow layout, right justified and no gaps:
    setLayout(new FlowLayout(FlowLayout.RIGHT,0,0));
    add(labelObj);           //add label for text
    add(new JLabel("  "));   //add spacer after text
    add(symbolObj);          //add symbol to right of text
    add(new JLabel(          //add right-margin spacer after symbol
                 GuiUtilFns.getSpacerStrForWidth(RMARGIN_PIXELS,labelObj)));

    addMouseListener(new MouseAdapter()
        {          //setup mouse listener to toggle popup when clicked:
          public void mouseClicked(MouseEvent e)
          {
            if(!isPopupDialogVisible())
            {      //popup not currently visible
              if(popupTextString != null)        //if message available
                userRequestShowPopupDialog();    // then show popup
            }
            else   //popup is currently visible
              hidePopupDialog();       //clear popup
          }
        });
    dialogTextArea.setEditable(false);      //make text area not editable
    dialogTextArea.setLineWrap(true);       //set to break up lines
    dialogTextArea.setWrapStyleWord(true);  //set to not break up words
    dialogOkButton.addActionListener(new ActionListener()
        {          //setup "OK" button listener to hide dialog when pressed
          public void actionPerformed(ActionEvent e)
          {
            hidePopupDialog();
          }
        });
    setToolTipText("Connection status; click for detail");
  }

  /**
   * Creates a panel for showing the ongoing status of a connection.  A
   * colored-dot symbol appears to right of a short text message.  The
   * panel may be clicked on to display a popup showing a longer status
   * message.
   */
  public ConnectionStatusPanel()
  {
    this(null);
  }

  /**
   * Sets the parent component to use when displaying the popup dialog,
   * or null for none.
   * @param compObj the parent component object for the popup dialog.
   */
  public void setPopupParent(Component compObj)
  {
    popupParentObj = compObj;
  }

  /**
   * Returns the parent component to use when displaying the popup dialog,
   * or null for none.
   * @return The parent component object for the popup dialog.
   */
  public Component getPopupParent()
  {
    return popupParentObj;
  }

  /**
   * Sets the text to be displayed.
   * @param str the text to be displayed.
   */
  public void setText(final String str)
  {
              //if this is the event-dispatch thread then do it now:
    if(SwingUtilities.isEventDispatchThread())
      doSetText(str);
    else
    {         //if this is not the event-dispatch thread then do it later:
      SwingUtilities.invokeLater(new Runnable()
          {
            public void run()
            {
              doSetText(str);
            }
          });
    }
  }

  /**
   * Sets the text to be displayed.
   * @param str the text to be displayed.
   */
  protected void doSetText(String str)
  {
    if(str != null && str.length() > maxTextLength)   //if too long then trim
      str = str.substring(0,maxTextLength).trim() + "...";
    labelObj.setText(str);
  }

  /**
   * Returns the currently displayed text.
   * @return The text string.
   */
//  public String getText()
//  {
//    return labelObj.getText();
//  }

  /**
   * Sets the color of the symbol to the right of the text.
   * @param colorObj the color to use, or null for transparent.
   */
  public void setSymbolColor(final Color colorObj)
  {
              //if this is the event-dispatch thread then do it now:
    if(SwingUtilities.isEventDispatchThread())
      doSetSymbolColor(colorObj);
    else
    {         //if this is not the event-dispatch thread then do it later:
      SwingUtilities.invokeLater(new Runnable()
          {
            public void run()
            {
              doSetSymbolColor(colorObj);
            }
          });
    }
  }

  /**
   * Sets the color of the symbol to the right of the text.
   * @param colorObj the color to use, or null for transparent.
   */
  protected void doSetSymbolColor(Color colorObj)
  {
    symbolObj.setColorObj((colorObj != null) ? colorObj : CLEAR_COLOR);
  }

  /**
   * Sets the text to be displayed in the popup shown when this panel is
   * clicked on.
   * @param str text to be displayed, or null to disable the popup.
   */
  public void setPopupTextString(final String str)
  {
              //if this is the event-dispatch thread then do it now:
    if(SwingUtilities.isEventDispatchThread())
      doSetPopupTextString(str);
    else
    {         //if this is not the event-dispatch thread then do it later:
      SwingUtilities.invokeLater(new Runnable()
          {
            public void run()
            {
              doSetPopupTextString(str);
            }
          });
    }
  }

  /**
   * Sets the text to be displayed in the popup shown when this panel is
   * clicked on.
   * @param str text to be displayed, or null to disable the popup.
   */
  protected void doSetPopupTextString(String str)
  {
    popupTextString = str;
    dialogTextArea.setText(popupTextString);
                             //setup "hand" cursor for mouse pointer:
         //if message to display then setup "hand" cursor for panel:
    setCursor((popupTextString != null) ?
                            Cursor.getPredefinedCursor(Cursor.HAND_CURSOR) :
                                                 Cursor.getDefaultCursor());
  }

  /**
   * Returns the text to be displayed in the popup shown when this panel is
   * clicked on.
   * @return The text to be displayed, or null if the popup is disabled.
   */
  public String getPopupTextString()
  {
    return popupTextString;
  }

  /**
   * Sets the message strings for this panel and the popup dialog,
   * as well as the color for the symbol on the panel.
   * @param textStr text for panel, or null for no change.
   * @param popupStr text for popup, or null for no change.
   * @param symColorIdx index of color for symbol (one of the
   * '..._COLOR_IDX' values), or 'NULL_COLOR_IDX' for no change.
   */
  public void setData(final String textStr,final String popupStr,
                                                    final int symColorIdx)
  {
              //convert color-index value to Color object:
    final Color symColorObj = (symColorIdx >= 0 &&
        symColorIdx < IDX_CLR_ARR.length) ? IDX_CLR_ARR[symColorIdx] : null;
              //if this is the event-dispatch thread then do it now:
    if(SwingUtilities.isEventDispatchThread())
      doSetData(textStr,popupStr,symColorObj);
    else
    {         //if this is not the event-dispatch thread then do it later:
      SwingUtilities.invokeLater(new Runnable()
          {
            public void run()
            {
              doSetData(textStr,popupStr,symColorObj);
            }
          });
    }
  }

  /**
   * Sets the message strings for this panel and the popup dialog.
   * @param textStr text for panel, or null for no change.
   * @param popupStr text for popup, or null for no change.
   */
  public void setData(String textStr,String popupStr)
  {
    setData(textStr,popupStr,NULL_COLOR_IDX);
  }

  /**
   * Sets the message strings for this panel and the popup dialog,
   * as well as the color for the symbol on the panel.
   * @param textStr text for panel, or null for no change.
   * @param popupStr text for popup, or null for no change.
   * @param symColorObj color for symbol, or null for no change.
   */
  protected void doSetData(String textStr,String popupStr,Color symColorObj)
  {
    if(textStr != null)                //if given then set panel text
      doSetText(textStr);
    if(popupStr != null)               //if given then set popup text
      doSetPopupTextString(popupStr);
    if(symColorObj != null)            //if given then set symbol color
      doSetSymbolColor(symColorObj);
  }

  /**
   * Sets the title to be displayed in the popup shown when this panel is
   * clicked on.
   * @param str title to be displayed, or null for the default title.
   */
  public void setTitleTextString(final String str)
  {
              //if this is the event-dispatch thread then do it now:
    if(SwingUtilities.isEventDispatchThread())
      doSetTitleTextString(str);
    else
    {         //if this is not the event-dispatch thread then do it later:
      SwingUtilities.invokeLater(new Runnable()
          {
            public void run()
            {
              doSetTitleTextString(str);
            }
          });
    }
  }

  /**
   * Sets the title to be displayed in the popup shown when this panel is
   * clicked on.
   * @param str title to be displayed, or null for the default title.
   */
  protected void doSetTitleTextString(String str)
  {
    popupTitleString = (str != null) ? str : DEF_POPUP_TITLESTR;
    if(dialogPopupObj != null)                        //if popup allocated
      dialogPopupObj.setTitleStr(popupTitleString);   // then set title str
  }

  /**
   * Returns the title to be displayed in the popup shown when this panel is
   * clicked on.
   * @return The title to be displayed, or null if the default will be
   * displayed.
   */
  public String getTitleTextString()
  {
    return popupTitleString;
  }

  /**
   * Sets the maximum length allowed for the text in the panel.
   * @param len the length value.
   */
  public void setMaxTextLength(int len)
  {
    maxTextLength = len;
  }

  /**
   * Returns the maximum length allowed for the text in the panel.
   * @return The length value.
   */
  public int getMaxTextLength()
  {
    return maxTextLength;
  }

  /**
   * Sets the modal flag for the popup dialog.  The popup dialog defaults
   * to non-modal.
   * @param modalFlag true for modal, false for non-modal (allows other
   * windows to run).
   */
  public void setPopupModal(boolean modalFlag)
  {
    popupModalFlag = modalFlag;
    if(dialogPopupObj != null)
      dialogPopupObj.setModal(popupModalFlag);
  }

  /**
   * Adds the given window listener to the popup dialog.  Only one listener
   * may be added before the dialog has been displayed.
   * @param listenerObj the window listener to add.
   */
  public void addPopupWindowListener(WindowListener listenerObj)
  {
    if(dialogPopupObj != null)    //if dialog created then add listener
      dialogPopupObj.addWindowListener(listenerObj);
    else           //if dialog not created then save listener object
      popupWindowListenerObj = listenerObj;
  }

  /**
   * Removes the given window listener from the popup dialog.
   * @param listenerObj the window listener to remove.
   */
  public void removePopupWindowListener(
                                                 WindowListener listenerObj)
  {
    if(dialogPopupObj != null)    //if dialog created then remove listener
      dialogPopupObj.removeWindowListener(listenerObj);
    else           //if dialog not created then clear listener object
      popupWindowListenerObj = null;
  }

  /**
   * Sets the component that is to receive focus after the popup dialog
   * is cleared.
   * @param compObj the component to receive focus.
   */
  public void setRefocusComponent(Object compObj)
  {
    if(compObj instanceof Component)
      refocusComponentObj = (Component)compObj;
  }

  /**
   * Displays the popup dialog showing extra status information.
   * The dialog is non-modal.
   */
  public void showPopupDialog()
  {
              //if this is the event-dispatch thread then do it now:
    if(SwingUtilities.isEventDispatchThread())
      doShowPopupDialog();
    else
    {         //if this is not the event-dispatch thread then do it later:
      SwingUtilities.invokeLater(new Runnable()
          {
            public void run()
            {
              doShowPopupDialog();
            }
          });
    }
  }

  /**
   * Displays the popup dialog showing extra status information.
   * The dialog is non-modal.
   */
  protected void doShowPopupDialog()
  {
    if(dialogPopupObj == null)
    {    //dialog not yet created; create it now
                 //set text area font to same as label except a bit larger:
      final Font fontObj = labelObj.getFont();
      dialogTextArea.setFont(fontObj.deriveFont(fontObj.getSize2D()+2.0f));
                                     //put text area into a scroll pane:
      final JScrollPane paneObj = new JScrollPane(dialogTextArea,
                                 JScrollPane.VERTICAL_SCROLLBAR_AS_NEEDED,
                                  JScrollPane.HORIZONTAL_SCROLLBAR_NEVER);
                 //get rid of scroll pane border & put extra space on top:
      paneObj.setBorder(BorderFactory.createEmptyBorder(10,2,0,2));
      dialogPopupObj = new IstiDialogPopup(popupParentObj,paneObj,
                          popupTitleString,dialogOkButton,popupModalFlag);
      if(popupWindowListenerObj != null)  //if given then add listener
        dialogPopupObj.addWindowListener(popupWindowListenerObj);
                 //set text area background to match dialog background:
      dialogTextArea.setBackground(dialogPopupObj.getBackground());
    }
    dialogPopupObj.show();           //show the dialog
  }

  /**
   * Displays the popup dialog showing extra status information in response
   * to a user request.  The dialog is non-modal.
   */
  public void userRequestShowPopupDialog()
  {
    popupRequestedFlag = true;         //indicate user requested popup
    showPopupDialog();                 //show popup dialog
  }

  /**
   * Hides the popup dialog.
   */
  public void hidePopupDialog()
  {
              //if this is the event-dispatch thread then do it now:
    if(SwingUtilities.isEventDispatchThread())
      doHidePopupDialog();
    else
    {         //if this is not the event-dispatch thread then do it later:
      SwingUtilities.invokeLater(new Runnable()
          {
            public void run()
            {
              doHidePopupDialog();
            }
          });
    }
  }

  /**
   * Hides the popup dialog.
   */
  protected void doHidePopupDialog()
  {
    if(dialogPopupObj != null)
    {    //dialog created
      popupRequestedFlag = false;           //clear user-requested flag
      if(dialogPopupObj.isVisible())
      {  //dialog is currently visible; hide it
              //use 'close()' method because it calls 'dispose()'
              // which ensures that minimized dialog won't get "stuck":
        dialogPopupObj.close();
        if(refocusComponentObj != null)
        {       //refocus component is setup
          SwingUtilities.invokeLater(new Runnable()
              {           //do this after other Swing business is done
                public void run()
                {         //put focus on given component
                  refocusComponentObj.requestFocus();
                }
              });
        }
      }
    }
  }

  /**
   * Clears any displayed popup dialog if the user has not requested
   * that it be shown.
   */
  public void autoClearPopupDialog()
  {
    if(!popupRequestedFlag)            //if not user requested then
      hidePopupDialog();               //hide popup dialog
  }

  /**
   * Returns the status of whether or not the popup dialolg is visible.
   * @return true if the popup dialolg is visible, false if not.
   */
  public boolean isPopupDialogVisible()
  {
    return (dialogPopupObj != null) ? dialogPopupObj.isVisible() : false;
  }
}
