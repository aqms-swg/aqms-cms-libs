/**
 * EventTreeTable.java
 *
 *   3/18/2004 -- [ET]  Changed "com.isti.quakewatch.client.message"
 *                      references to "com.isti.quakewatch.message".
 *  12/13/2005 -- [KF]  Moved from 'QWClient' to 'QWCommon' project.
 */

package com.isti.quakewatch.guiutil;

import com.isti.quakewatch.message.QWEventMsgRecord;
import javax.swing.*;
import javax.swing.table.*;
import java.awt.FontMetrics;
import com.isti.util.gui.JTreeTable;

/**
 * Assembles the UI. The UI consists of a JTreeTable.
 */
public class EventTreeTable extends JTreeTable
{
  public EventTreeTable(QWEventMsgRecord eventMsgRecord)
  {
    super(new EventTreeTableModel(eventMsgRecord));

    //turn off auto-resize
    this.setAutoResizeMode(AUTO_RESIZE_OFF);

    try
    {
      //size the columns based on largest name/value sizes
      TableColumnModel cm = getColumnModel();
      TableColumn nameColumn = cm.getColumn(0);
      TableColumn valueColumn = cm.getColumn(1);
      EventTreeTableModel ttm = (EventTreeTableModel)getTreeTableModel();

      //get the font metrics to find the size
      FontMetrics fm = getFontMetrics(getFont());
      //get the width of the view port
      int vpWidth = getPreferredScrollableViewportSize().width;
      //determine the name width using the current value and add
      // the length of the largest name
      int nameWidth = nameColumn.getPreferredWidth()+
                      fm.stringWidth(ttm.getLongestName());
      //make sure the name width is at least half the viewport width
      if (nameWidth < vpWidth/2)
        nameWidth = vpWidth/2;
      //determine the value width using the length of the largest value and
      // add extra for the border
      int valueWidth = fm.stringWidth(ttm.getLongestValue()) + 20;
      //determine the total width of name and value columns
      int totalWidth = nameWidth + valueWidth;
      //if there is extra space in the viewport add it to the value column
      if (totalWidth < vpWidth)
        valueWidth += vpWidth - totalWidth;

      //update the name and value preferred width values
      nameColumn.setPreferredWidth(nameWidth);
      valueColumn.setPreferredWidth(valueWidth);

      //change the position of the text on the header
      final JTableHeader th = getTableHeader();
      TableCellRenderer hr = th.getDefaultRenderer();
      if (hr instanceof JLabel)
      {
        JLabel label = (JLabel)hr;
        //use the center position of the name header for both columns
        int gap = (nameColumn.getPreferredWidth()-fm.stringWidth(
            ttm.getColumnName(0)))/2;
        //create dummy icon to get gap for text
        byte[] imageData = {0};
        label.setIcon(new ImageIcon(imageData));
        label.setIconTextGap(gap);
        //change horizontal alignment and text position
        label.setHorizontalAlignment(SwingConstants.LEADING);
        label.setHorizontalTextPosition(SwingConstants.TRAILING);
      }
    }
    catch (Exception ex) {}
  }
}
