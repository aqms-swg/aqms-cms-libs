//EventTreeTableModel.java:  A TreeTableModel representing events.
//
//  3/18/2004 -- [ET]  Changed "com.isti.quakewatch.client.message"
//                     references to "com.isti.quakewatch.message".
//  5/17/2005 -- [ET]  Changed to use modified 'QWEventMsgRecord' interface.
//   8/3/2005 -- [ET]  Modified to display any text data held by elements
//                     (added "value = node.getTextNormalize()" to
//                     'getValue(Element)' method.
// 12/13/2005 -- [KF]  Moved from 'QWClient' to 'QWCommon' project.
//  9/22/2006 -- [ET]  Modified to recognize ANSS-EQ-XML tag names.
//  4/13/2010 -- [ET]  Modified to recognize QuakeML tag names.
//  5/12/2010 -- [ET]  Modified to display QuakeML elements at same level
//                     as 'event' element (if any).
//  7/23/2010 -- [ET]  Modified to not show product element (again) if
//                     included under event element (change to EventNode
//                     'getChildren()' method).
//

package com.isti.quakewatch.guiutil;

import java.util.List;
import java.util.Vector;
import org.jdom.Element;
import org.jdom.Attribute;
import com.isti.util.FifoHashtable;
import com.isti.util.gui.AbstractTreeTableModel;
import com.isti.util.gui.TreeTableModel;
import com.isti.quakewatch.common.MsgTag;
import com.isti.quakewatch.message.QWEventMsgRecord;
import com.isti.quakewatch.message.QWProductMsgRecord;

/**
 * EventTreeTableModel is a TreeTableModel representing events.
 */
public class EventTreeTableModel extends AbstractTreeTableModel
{
  // Names of the columns.
  static protected String[] cNames = {"Name", "Value"};

  // Types of the columns.
  static protected Class[] cTypes =
  {
    TreeTableModel.class,
    String.class
  };

  /** longest name string. */
  private String longestName = "";
  /** longest value string. */
  private String longestValue = "";

  /**
  * Creates an EventTreeTableModel.
  * @param eventMsgRecord the event message record for the event.
  */
  public EventTreeTableModel(QWEventMsgRecord eventMsgRecord)
  {
    super(null);
    if (eventMsgRecord != null)
    {
      final EventNode node = new EventNode(eventMsgRecord);
      root = node;
      longestName = node.getLongestName();
      longestValue = node.getLongestValue();
    }
  }

  /**
   * @return the longest name string.
   */
  public String getLongestName()
  {
    return longestName;
  }

  /**
   * @return the longest value string.
   */
  public String getLongestValue()
  {
    return longestValue;
  }

  //
  // The TreeModel interface
  //

  /**
   * Returns the number of children of <code>node</code>.
   * @param node the particular node.
   * @return the number of children.
   */
  public int getChildCount(Object node)
  {
    Object[] children = getChildren(node);
    return (children == null) ? 0 : children.length;
  }

  /**
   * Returns the child of <code>node</code> at index <code>i</code>.
   * @param node the particular node.
   * @param i the index of the child.
   * @return the child.
   */
  public Object getChild(Object node, int i)
  {
    return getChildren(node)[i];
  }

  //
  //  The TreeTableNode interface.
  //

  /**
   * Returns the number of columns.
   * @return the number of columns.
   */
  public int getColumnCount()
  {
    return cNames.length;
  }

  /**
   * Returns the name for a particular column.
   * @param column the particular column.
   * @return the name for a particular column.
   */
  public String getColumnName(int column)
  {
    return cNames[column];
  }

  /**
   * Returns the class for the particular column.
   * @param column the particular column.
   * @return the Class.
   */
  public Class getColumnClass(int column)
  {
    return cTypes[column];
  }

  /**
   * Returns the value of the particular column.
   * @param node the particular node.
   * @param column the particular column.
   * @return the value of the particular column.
   */
  public Object getValueAt(Object node, int column)
  {
    final EventNode eventNode;
    try
    {
      eventNode = (EventNode)node;
    }
    catch (java.lang.ClassCastException e)
    {
      return null;
    }

    switch(column)
    {
      case 0:
        return eventNode.toString();
      case 1:
        return eventNode.getValue();
    }

    return null;
  }

  //
  // Some convenience methods.
  //

  /**
   * Returns the object for the particular node.
   * @param node the particular node.
   * @return the object for the particular node.
   */
  protected Object getObject(Object node)
  {
    final EventNode eventNode;
    try
    {
      eventNode = (EventNode)node;
    }
    catch (java.lang.ClassCastException e)
    {
      return null;
    }
    return eventNode.getObject();
  }

  /**
   * Returns the children for the particular node.
   * @param node the particular node.
   * @return the children for the particular node.
   */
  protected Object[] getChildren(Object node)
  {
    final EventNode eventNode;
    try
    {
      eventNode = (EventNode)node;
    }
    catch (java.lang.ClassCastException e)
    {
      return null;
    }
    return eventNode.getChildren();
  }


  /**
   * A EventNode is used to maintain a cache of an events's children.
   */
  protected static class EventNode
  {
    /** object the node represents. */
    protected final Object            node;
    /** Parent EventNode of the node. */
    private final EventNode           parent;
    /** Children of the node. */
    protected final EventNode[]       children;
    /** longest name string. */
    private String longestName = "";
    /** longest value string. */
    private String longestValue = "";

    protected EventNode(Object node)
    {
      this(null, node);
    }

    protected EventNode(EventNode parent, Object node)
    {
      String name, value;
      this.parent = parent;
      this.node = node;

      name = toString();
      if (name.length() > longestName.length())
        longestName = name;
      value = getValue();
      if (value.length() > longestValue.length())
        longestValue = value;

      final List childrenList = getChildren(node);
      if (childrenList == null || childrenList.size() <= 0)
      {
        this.children = null;
        return;
      }

      this.children = new EventNode[childrenList.size()];
      try
      {
        for (int i = 0; i < this.children.length; i++)
        {
          this.children[i] = new EventNode(this, childrenList.get(i));
          name = this.children[i].longestName;
          if (name.length() > longestName.length())
            longestName = name;
          value = this.children[i].longestValue;
          if (value.length() > longestValue.length())
            longestValue = value;
        }
      }
      catch (java.lang.ClassCastException e)
      {
        return;
      }
    }

    /**
     * @return the longest name string.
     */
    public String getLongestName()
    {
      return longestName;
    }

    /**
     * @return the longest value string.
     */
    public String getLongestValue()
    {
      return longestValue;
    }

    /**
     * Returns the children for the particular node.
     * @param node the particular node.
     * @return the children for the particular node.
     */
    public List getChildren(Object node)
    {
      final List childrenList;
      if(node instanceof QWEventMsgRecord)
      {
        final QWEventMsgRecord eventMsgRecord = (QWEventMsgRecord)node;
        final Element evtElemObj = eventMsgRecord.getMsgRecChildElement();
        childrenList = new Vector();
        childrenList.add(evtElemObj);

        final FifoHashtable table = eventMsgRecord.getProductRecTable();
        if(table != null)
        {  //table of product objects not empty
                   //get list of child elements for event record:
          final List evtElemList = evtElemObj.getChildren();
          final Vector vector = table.getValuesVector();
          for(int i = 0; i < vector.size(); i++)
          {
            final Object obj = vector.get(i);
            if(obj instanceof QWProductMsgRecord)
            {
              final QWProductMsgRecord productMsgRecord =
                  (QWProductMsgRecord)obj;
                             //if product element not under event record
                             // (already included) then add to list:
              if(!evtElemList.contains(productMsgRecord.productElement))
                childrenList.add(productMsgRecord.productElement);
            }
          }
        }
      }
      else if (node instanceof Element)
      {
        childrenList = new Vector();
        Element elemObj = (Element)node;
        final List elementList = elemObj.getChildren();
        final List attributeList = elemObj.getAttributes();
        final int attribListSize = (attributeList != null) ?
                                                   attributeList.size() : 0;
              //if only one attribute then add it before elements:
        if(attribListSize == 1)
          childrenList.addAll(attributeList);
              //add elements (if any):
        if(elementList != null && elementList.size() > 0)
          childrenList.addAll(elementList);
              //if more than one attribute then add them after elements:
        if(attribListSize > 1)
          childrenList.addAll(attributeList);
              //if QuakeML 'event' element with 'eventParameters' parent
              // and additional elements at same level as 'event' element
              // are present then include them in list:
        if(MsgTag.QUAKEML_EVENT.equalsIgnoreCase(elemObj.getName()) &&
                                    (elemObj=elemObj.getParent()) != null &&
                MsgTag.EVENT_PARAMETERS.equalsIgnoreCase(elemObj.getName()))
        {  //element is named 'event' and parent is named 'eventParameters'
          List peersListObj;
          if((peersListObj=elemObj.getChildren()) != null &&
                                                    peersListObj.size() > 1)
          {  //additional elements at same level as 'event' are present
                   //use clone (can be modified independent of element tree):
            peersListObj = new Vector(peersListObj);
            peersListObj.remove(node);           //don't add node
            childrenList.addAll(peersListObj);   //add found elements
          }
        }
      }
      else
      {
        childrenList = null;
      }
      return childrenList;
    }

    /**
     * Returns the the string to be used to display this leaf in the JTree.
     * @return the the string to be used to display this leaf in the JTree.
     */
    public String toString()
    {
      if (node instanceof QWEventMsgRecord)                //if event then
        return ((QWEventMsgRecord)node).getEventIDKey();   //show ID key
      if (node instanceof Element)
      {  //node is 'Element' type
        final Element elemObj = (Element)node;
        final String elemNameStr = elemObj.getName();
        String str;
        if(MsgTag.QUAKEML_PRODUCT_LINK.equals(elemNameStr))
        {  //node name is "productLink"
          if((str=elemObj.getChildTextTrim(MsgTag.QUAKEML_TYPE,
                     elemObj.getNamespace())) != null && str.length() > 0 &&
                                               !str.equals(MsgTag.LINK_URL))
          {  //child element "type" found and is not "LinkURL"
            return str;        //return type value for node
          }
          if((str=elemObj.getChildTextTrim(MsgTag.QUAKEML_CODE,
                       elemObj.getNamespace())) != null && str.length() > 0)

          {  //child element "code" found
            return str;        //return value for node
          }
        }
        else if(MsgTag.PRODUCT_LINK.equals(elemNameStr))
        {  //node name is "ProductLink"
          if((str=elemObj.getChildTextTrim(MsgTag.TYPE_KEY,
                     elemObj.getNamespace())) != null && str.length() > 0 &&
                                               !str.equals(MsgTag.LINK_URL))
          {  //child element "TypeKey" found and is not "LinkURL"
            return str;        //return type value for node
          }
          if((str=elemObj.getChildTextTrim(MsgTag.CODE,
                       elemObj.getNamespace())) != null && str.length() > 0)

          {  //child element "Code" found
            return str;        //return value for node
          }
        }
        else if(MsgTag.PRODUCT.equals(elemNameStr) &&
                     (str=elemObj.getAttributeValue(MsgTag.TYPE)) != null &&
                                                    str.trim().length() > 0)
        {  //node is a product element with a valid type value
          return str;        //return type value for node
        }
        return elemNameStr;
      }
      if(node instanceof Attribute)
        return ((Attribute)node).getName();
      else
        return "";
    }

    /**
     * Returns the the value to be used to display this leaf in the JTree.
     * @return the the value to be used to display this leaf in the JTree.
     */
    public String getValue()
    {
      if (node instanceof Attribute)
        return ((Attribute)node).getValue();
      else if (node instanceof Element)
        return getValue((Element)node);
      else
        return "";
    }

    /**
     * Returns the the value to be used to display this leaf in the JTree.
     * @param node the node to get the value of.
     * @return the the value to be used to display this leaf in the JTree.
     */
    public String getValue(Element node)
    {
      String valueStr;
      if((valueStr=node.getAttributeValue(MsgTag.VALUE)) != null &&
                                               valueStr.trim().length() > 0)
      {  //"Value" attribute found
        return valueStr;
      }
      final String nodeNameStr = node.getName();
      if(MsgTag.IDENTIFIER.equals(nodeNameStr) &&
           (valueStr=node.getAttributeValue(MsgTag.EVENT_ID_KEY)) != null &&
                                               valueStr.trim().length() > 0)
      {  //node is "Identifier" and event ID OK
        return valueStr;
      }
         //if element text data found then return it:
      if((valueStr=node.getTextNormalize()).length() > 0)
        return valueStr;
      if((valueStr=node.getChildTextTrim(
                       MsgTag.QUAKEML_VALUE,node.getNamespace())) != null &&
                                                      valueStr.length() > 0)
      {  //"value" child-element with data found
        return valueStr;
      }
      if((valueStr=node.getChildTextTrim(
                               MsgTag.VALUE,node.getNamespace())) != null &&
                                                      valueStr.length() > 0)
      {  //"Value" child-element with data found
        return valueStr;
      }
      if(MsgTag.QUAKEML_MAGNITUDE.equals(nodeNameStr))
      {  //node is QuakeML "magnitude"
        final Element elemObj;
        if((elemObj=node.getChild(MsgTag.MAG,node.getNamespace())) != null &&
                                         (valueStr=elemObj.getChildTextTrim(
                    MsgTag.QUAKEML_VALUE,elemObj.getNamespace())) != null &&
                                                      valueStr.length() > 0)
        {  //"mag"|"value" child-element with data found
          return valueStr;
        }
      }
      if(MsgTag.QUAKEML_PRODUCT_LINK.equals(nodeNameStr) &&
                        (valueStr=node.getChildTextTrim(MsgTag.QUAKEML_LINK,
                                            node.getNamespace())) != null &&
                                                      valueStr.length() > 0)
      {  //node is "productLink" and "link" child-element found
        return valueStr;
      }
      if(MsgTag.PRODUCT_LINK.equals(nodeNameStr) &&
                                (valueStr=node.getChildTextTrim(MsgTag.LINK,
                                            node.getNamespace())) != null &&
                                                      valueStr.length() > 0)
      {  //node is "ProductLink" and "Link" child-element found
        return valueStr;
      }
      if(MsgTag.QUAKEML_COMMENT.equals(nodeNameStr) &&
                        (valueStr=node.getChildTextTrim(MsgTag.QUAKEML_TEXT,
                                            node.getNamespace())) != null &&
                                                      valueStr.length() > 0)
      {  //node is "comment" and "text" child-element found
        return valueStr;
      }
      if(MsgTag.COMMENT.equals(nodeNameStr) &&
                                (valueStr=node.getChildTextTrim(MsgTag.TEXT,
                                            node.getNamespace())) != null &&
                                                      valueStr.length() > 0)
      {  //node is "Comment" and "Text" child-element found
        return valueStr;
      }
      return "";
    }

    /**
     * Returns the object the node represents.
     * @return the object the node represents.
     */
    public Object getObject()
    {
      return node;
    }

    /**
     * Returns the parent of the node.
     * @return the parent of the node.
     */
    public EventNode getParent()
    {
      return parent;
    }

    /**
     * Returns the children for this event.
     * @return the children for this event.
     */
    protected Object[] getChildren()
    {
      return children;
    }
  }
}
