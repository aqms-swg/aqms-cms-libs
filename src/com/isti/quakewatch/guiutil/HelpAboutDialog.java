//HelpAboutDialog.java:  Defines a dialog for showing help.
//
//  9/17/2004 -- [KF]  Initial version, previously in 'QWClient'.
//

package com.isti.quakewatch.guiutil;

import java.awt.Component;
import javax.swing.JPanel;
import com.isti.util.gui.IstiDialogPopup;

/**
 * Class HelpAboutDialog defines a dialog for showing help.
 */
public class HelpAboutDialog extends IstiDialogPopup
{
  /**
   * Creates a dialog for showing help.
   * @param parentComp the parent component for the popup.
   * @param helpAboutPanel the panel for showing help.
   * @param programName the program name.
   */
  public HelpAboutDialog(
      Component parentComp,JPanel helpAboutPanel,String programName)
  {
    super(parentComp,helpAboutPanel,"About " + programName,"OK",false);
  }
}