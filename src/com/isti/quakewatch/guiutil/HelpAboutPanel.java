//HelpAboutPanel.java:  Defines a panel for showing program information.
//
//  9/17/2004 -- [KF]  Initial version, previously in 'QWClient'.
//   1/7/2009 -- [ET]  Changed "Written by ISTI" text to
//                     "Developed by ISTI".
//  7/26/2010 -- [ET]  Added optional 'extraTextStr' parameter.
//  3/13/2019 -- [KF]  Modified to use the PDL Indexer.
//

package com.isti.quakewatch.guiutil;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.FlowLayout;
import java.awt.Font;
import java.net.URL;
import javax.swing.*;
import com.isti.util.FileUtils;
import com.isti.util.LaunchBrowser;
import com.isti.util.gui.LaunchBrowserButton;
import com.isti.util.gui.IstiSmallLogo;
import com.isti.util.gui.FocusUtils;
import com.isti.quakewatch.util.QWConnectionMgr;

/**
 * Class HelpAboutPanel defines a panel for showing program information.
 */
public class HelpAboutPanel extends JPanel
{
    /** URL link string for CISN graphic. */
  public static final String CISN_URL_LINKSTR = "http://www.cisn.org";

  /**
   * Creates a panel for showing help.
   * @param launchBrowserObj the launch browser object.
   * @param revisionString the revision string.
   * @param javaVersionString the Java version string.
   * @param cisnGraphicFname the CISN Graphic filename or null if none.
   * @param openMapText the OpenMap Copyright Message or null if none.
   * @param extraTextStr extra text for bottom of panel, or null for none.
   */
  public HelpAboutPanel(LaunchBrowser launchBrowserObj,
                            String revisionString, String javaVersionString,
                                String cisnGraphicFname, String openMapText,
                                                        String extraTextStr)
  {
    setLayout(new BoxLayout(this,BoxLayout.Y_AXIS));
    //put extra space at bottom above OK button:
    setBorder(BorderFactory.createEmptyBorder(0,0,10,0));

    final Color helpBackground = Color.white;
    final JLabel revLabelObj = new JLabel(revisionString);
    revLabelObj.setFont(new Font("Serif",Font.BOLD,28));
    //need to put label into panel to get background color:
    final JPanel revisionPanel = new JPanel();
    revisionPanel.add(revLabelObj);
    revisionPanel.setBackground(helpBackground);
    revisionPanel.setAlignmentX(0.5f);      //align panel in box
    add(revisionPanel);

    //create ISTI link "button":
    final JButton istiButton = LaunchBrowserButton.createButton(
        "Developed by ISTI", new ImageIcon(IstiSmallLogo.dataArray),
        launchBrowserObj, "http://www.isti.com");
    istiButton.setFont(new Font("Serif",Font.BOLD,26));
    istiButton.setBackground(helpBackground);
    FocusUtils.setComponentFocusDisabled(istiButton);    //disable focus
    //put button into panel for centering:
    final JPanel istiPanel = new JPanel();
    istiPanel.setBackground(helpBackground);
    istiPanel.add(istiButton);
    istiPanel.setAlignmentX(0.5f);          //align panel in box
    add(istiPanel);

    final JLabel forLabelObj = new JLabel("for");
    forLabelObj.setFont(new Font("Dialog",Font.BOLD,14));
    //need to put label into panel to get background color:
    final JPanel forPanel = new JPanel();
    forPanel.add(forLabelObj);
    forPanel.setBackground(helpBackground);
    forPanel.setAlignmentX(0.5f);           //align panel in box
    add(forPanel);

    if (cisnGraphicFname != null)
    {
      //create CISN link "button":
      final URL urlObj = FileUtils.fileMultiOpenInputURL(cisnGraphicFname);
      final JButton cisnButton = LaunchBrowserButton.createButton(
          "the California Integrated Seismic Network",
          ((urlObj!=null)?(new ImageIcon(urlObj)):null),launchBrowserObj,
          CISN_URL_LINKSTR);
      //setup to show text centered beneath graphic:
      cisnButton.setVerticalTextPosition(SwingConstants.BOTTOM);
      cisnButton.setHorizontalTextPosition(SwingConstants.CENTER);
      cisnButton.setFont(new Font("Serif",Font.BOLD,20));
      cisnButton.setBackground(helpBackground);
      FocusUtils.setComponentFocusDisabled(cisnButton);  //disable focus
      //put button into panel for centering:
      final JPanel cisnPanel = new JPanel();
      cisnPanel.setBackground(helpBackground);
      cisnPanel.add(cisnButton);
      cisnPanel.setAlignmentX(0.5f);        //align panel in box
      add(cisnPanel);
    }

    if (openMapText != null)
    {
      final JTextArea openMapTextObj = new JTextArea(
          "\n\nMapping Engine:  " + openMapText);
      openMapTextObj.setEditable(false);      //text area not editable
      //need to put text area into panel to get background color:
      final JPanel openMapPanel = new JPanel();
      openMapPanel.add(openMapTextObj);
      openMapPanel.setBackground(helpBackground);
      openMapPanel.setAlignmentX(0.5f);           //align panel in box
      add(openMapPanel);
    }

    final JTextArea libLabelTextObj = new JTextArea("Libraries:  ");
    libLabelTextObj.setEditable(false);        //text area not editable
    final JPanel libLabelPanel = new JPanel(new BorderLayout());
    libLabelPanel.add(libLabelTextObj,BorderLayout.NORTH);
    libLabelPanel.setBackground(helpBackground);
    final JTextArea librariesTextObj = new JTextArea("OpenORB, Version " +
        QWConnectionMgr.getOpenOrbVersionStr() +
        "\n  http://openorb.sourceforge.net\n" +
        "JDOM, http://www.jdom.org\nXerces, http://xml.apache.org\n");
    librariesTextObj.setEditable(false);       //text area not editable
    //need to put text area into panel to get background color:
    final JPanel librariesPanel = new JPanel(new BorderLayout());
    librariesPanel.add(libLabelPanel,BorderLayout.WEST);
    librariesPanel.add(librariesTextObj,BorderLayout.CENTER);
    librariesPanel.setBackground(helpBackground);
    librariesPanel.setBorder(BorderFactory.createEmptyBorder(0,5,0,0));
    librariesPanel.setAlignmentX(0.5f);        //align panel in box
    add(librariesPanel);

    String dispTextStr = "Using Java " + javaVersionString + " on " +
                                     launchBrowserObj.getOsNamePropString();
              //if extra text given then add to text area:
    if(extraTextStr != null && extraTextStr.length() > 0)
      dispTextStr += '\n' + extraTextStr;
    final JTextArea javaVerTextObj = new JTextArea(dispTextStr);
    javaVerTextObj.setEditable(false);      //text area not editable
         //need to put text area into panel to get background color:
    final JPanel javaVerPanel = new JPanel(new FlowLayout(FlowLayout.LEFT));
    javaVerPanel.add(javaVerTextObj);
    javaVerPanel.setBackground(helpBackground);
    javaVerPanel.setAlignmentX(0.5f);            //align panel in box
    add(javaVerPanel);
  }

  /**
   * Creates a panel for showing help.
   * @param launchBrowserObj the launch browser object.
   * @param revisionString the revision string.
   * @param javaVersionString the Java version string.
   * @param cisnGraphicFname the CISN Graphic filename or null if none.
   * @param openMapText the OpenMap Copyright Message or null if none.
   */
  public HelpAboutPanel(LaunchBrowser launchBrowserObj,
                            String revisionString, String javaVersionString,
                                String cisnGraphicFname, String openMapText)
  {
    this(launchBrowserObj,revisionString,javaVersionString,
                                         cisnGraphicFname,openMapText,null);
  }
}
