//MailServerTest.java:  Defines a mail server test object.
//
//  1/19/2006 -- [KF]  Initial version.
//  3/31/2006 -- [ET]  Added optional 'checkServerFlag' parameter to
//                     constructor and added 'doInitialServerCheck()'
//                     and 'getInitialServerTestDoneFlag()' methods;
//                     modified 'doTestMailServerConnection()' method
//                     to also check 'from' email address and have
//                     debug-log output.
//  5/23/2007 -- [ET]  Fixed spelling in 'doTestMailServerConnection()'
//                     method ("STMP" -> "SMTP").
//

package com.isti.quakewatch.guiutil;

import com.isti.util.UtilFns;
import com.isti.util.LogFile;
import com.isti.util.DataChangedListener;
import com.isti.util.ProgramInformationInterface;
import com.isti.util.menu.IstiMenuLoader;
import com.isti.util.gui.EntryFieldPanel;
import com.isti.util.gui.IstiDialogPopup;
import com.isti.util.gui.IstiDialogUtil;
import com.isti.quakewatch.alertemail.AlertEmailSender;

/**
 * Class MailServerTest defines a mail server test object.
 */
public class MailServerTest
{
  /** "TestMailServerConnection" name string. */
  private final static String testMailServerConnectionNameStr =
      "TestMailServerConnection";
  /** "SendTestEmailMessage" name string. */
  private final static String sendTestEmailMessageNameStr =
      "SendTestEmailMessage";
  private final static String[] testNameStrArray = {
      testMailServerConnectionNameStr,
      sendTestEmailMessageNameStr
  };
  private final QWCfgSettingsDialog settingsDialog;
  private final IstiMenuLoader menuLoader;
  private final LogFile logObj;
  //the program information
  private final ProgramInformationInterface progInfo;
  private final AlertEmailSender alertEmailSenderObj;

  /** Flag set true while inside 'sendTestEmailMessage()' method. */
  protected boolean inSendTestEmailMessageFlag = false;
  /** Flag set true after initial email-server test completed. */
  protected boolean initialServerTestDoneFlag = false;

  /**
   * Creates a mail server test object.  Note that if this constructor
   * performs an email-server test (via 'checkServerFlag'==true) then
   * it will block until the test is complete.
   * @param settingsDialog the settings dialog.
   * @param menuLoader the menu loader.
   * @param checkServerFlag true to check if email-server configuation
   * properties are setup and test email server; false to not (in which
   * case the 'doInitialServerCheck()' will usually want to be called
   * later).
   */
  public MailServerTest(QWCfgSettingsDialog settingsDialog,
                          IstiMenuLoader menuLoader, boolean checkServerFlag)
  {
    this.settingsDialog = settingsDialog;
    this.menuLoader = menuLoader;
    progInfo = settingsDialog.getProgramInformation();
    final LogFile lObj;      //get log-file obj (or create dummy if none):
    logObj = ((lObj=progInfo.getProgramLogFileObj()) != null) ? lObj :
                                                    LogFile.getNullLogObj();
    alertEmailSenderObj = settingsDialog.getAlertEmailSender();
    if(checkServerFlag &&
                  !alertEmailSenderObj.dontCheckSMTPFlagProp.booleanValue())
    {    //check-server param is set and don't-check config flag is clear
         //test conn to SMTP server (don't show success, wait for finish,
         // show popup dialog while test in progress, if email-server
         // properties not setup then show modal dialog):
      testMailServerConnection(false,true,true,true,true);
    }
         //setup listener to check connection after SMTP server changed:
    alertEmailSenderObj.smtpServerAddressProp.addDataChangedListener(
      new DataChangedListener()
        {
          public void dataChanged(Object sourceObj)
          {
            if(!alertEmailSenderObj.dontCheckSMTPFlagProp.booleanValue() &&
                    alertEmailSenderObj.smtpServerAddressProp.stringValue().
                                                               length() > 0)
            {      //don't-check config flag is clear and
                   // SMTP-address not empty; test conn to SMTP server
                   // (don't show success, don't wait, don't check props)
              testMailServerConnection(false,false,true,false);
            }
            updateMenuItemShown();     //update the menu items shown
          }
        });
    updateMenuItemShown();        //update the menu items shown
  }

  /**
   * Creates a mail server test object.
   * @param settingsDialog the settings dialog.
   * @param menuLoader the menu loader.
   */
  public MailServerTest(QWCfgSettingsDialog settingsDialog,
                                                  IstiMenuLoader menuLoader)
  {
    this(settingsDialog,menuLoader,true);
  }

  /**
   * Performs the initial test of the connection to the SMTP server (if
   * it has not yet been performed).
   * @param showSuccessFlag true to show a "success" dialog; false to not.
   * @param waitFlag true to block until the test finishes; false to use
   * a worker thread.
   * @param showTestDlgFlag true to show popup dialog while email-server
   * test is in progress; false to not.
   * @param checkPropsFlag true to check email-server configuration
   * properties and show a dialog if they are not setup; false to not.
   * @param chkPropsModalFlag true for dialog shown via 'checkPropsFlag' ==
   * true to be modal; false for non-modal.
   */
  public void doInitialServerCheck(boolean showSuccessFlag,
                                  boolean waitFlag, boolean showTestDlgFlag,
                          boolean checkPropsFlag, boolean chkPropsModalFlag)
  {
    if(!initialServerTestDoneFlag &&
                  !alertEmailSenderObj.dontCheckSMTPFlagProp.booleanValue())
    {    //test not yet done and don't-check config flag is clear
      testMailServerConnection(showSuccessFlag,waitFlag,showTestDlgFlag,
                                          checkPropsFlag,chkPropsModalFlag);
    }
  }

  /**
   * Performs the initial test of the connection to the SMTP server (if
   * it has not yet been performed).
   * @param showSuccessFlag true to show a "success" dialog; false to not.
   * @param waitFlag true to block until the test finishes; false to use
   * a worker thread.
   * @param showTestDlgFlag true to show popup dialog while email-server
   * test is in progress; false to not.
   * @param checkPropsFlag true to check email-server configuration
   * properties and show a (modal) dialog if they are not setup; false
   * to not.
   */
  public void doInitialServerCheck(boolean showSuccessFlag,
           boolean waitFlag, boolean showTestDlgFlag,boolean checkPropsFlag)
  {
    doInitialServerCheck(showSuccessFlag,waitFlag,showTestDlgFlag,
                                                       checkPropsFlag,true);
  }

  /**
   * Update the menu items shown.
   */
  protected void updateMenuItemShown()
  {
    if(menuLoader == null)  //exit if no menu loader
      return;
    final boolean showItemFlag =
       alertEmailSenderObj.smtpServerAddressProp.stringValue().length() > 0;
    String nameStr;
    for (int i = 0; i < testNameStrArray.length; i++)
    {
      nameStr = testNameStrArray[i];
      menuLoader.updateMenuItemShown(nameStr, showItemFlag);
    }
  }

  /**
   * Called when a MenuItem event happens.
   * @param nameStr the name of the menu item.
   * @return true if the MenuItem event is for a mail test, false otherwise.
   */
  public boolean processMenuItemEvent(String nameStr)
  {
    if(nameStr.equals(testMailServerConnectionNameStr))
      testMailServerConnection(true);
    else if(nameStr.equals(sendTestEmailMessageNameStr))
      sendTestEmailMessage();
    else
      return false;
    return true;
  }

  /**
   * Queries the user and sends a test email message.
   */
  public void sendTestEmailMessage()
  {
    if(inSendTestEmailMessageFlag)
      return;      //if already in method then exit
    inSendTestEmailMessageFlag = true;      //indicate inside method
         //if necessary email properties not setup then show dialog:
    while(!alertEmailSenderObj.areEmailPropsReady())
    {
      inSendTestEmailMessageFlag = false;   //indicate not inside method
      if(!settingsDialog.showEmailPropsNotReadyDialog(true))
        return;         //if user hit 'Cancel' then abort
      inSendTestEmailMessageFlag = true;    //indicate inside method
    }
         //get user to enter recipient email address:
    final String recipStr;
    final EntryFieldPanel entryPanelObj = new EntryFieldPanel(
        "Enter the email address to which to send a test message","Email:");
    if((recipStr=entryPanelObj.showPanelInDialog(progInfo.getProgramFrameObj(),
                                "Send Test Email Message",false)) == null ||
                                              recipStr.trim().length() <= 0)
    {    //user hit cancel or didn't enter an email address
      inSendTestEmailMessageFlag = false;   //indicate done method
      return;
    }
    (new Thread("sendTestEmail")       //use worker thread to send email
      {
        public void run()
        {
          IstiDialogPopup dialogObj = null;
          try
          {                  //show dialog while sending mail:
            dialogObj = showWaitingDialog(
                                       "Sending out test email message...");
            final boolean retFlag =
                alertEmailSenderObj.getJavaMailUtilObj().sendEmailMessage(
                alertEmailSenderObj.emailFromAddressProp.stringValue(),
                alertEmailSenderObj.emailFromNameProp.stringValue(),recipStr,
                ("Test Message from " + progInfo.getProgramName()),
                ("This is a test email message from the " +
                progInfo.getProgramName() + " program (Version " +
                progInfo.getProgramVersion() + ")"));
            dialogObj.close();         //clear dialog
            if(!retFlag)
            { //error sending email; log and show message to user
              final IstiDialogUtil dialogUtilObj =
                                               progInfo.getIstiDialogUtil();
              if(dialogUtilObj != null)
                dialogUtilObj.popupErrorMessage(logObj,
                    ("Unable to send email message:  " +
                     alertEmailSenderObj.getJavaMailUtilObj().
                                                   getErrorMessageString()),
                    false,true);
            }
          }
          catch(Exception ex)
          {   //some kind of exception error
            final String errStr = "Exception error sending test email:  " +
                                                                         ex;
            logObj.warning(errStr);         //log error message
            logObj.warning(UtilFns.getStackTraceString(ex));
            if(dialogObj != null)      //if waiting dialog was created then
              dialogObj.close();       //close dialog
                                       //show error message to user:
            final IstiDialogUtil dialogUtilObj = progInfo.getIstiDialogUtil();
            if(dialogUtilObj != null)
              dialogUtilObj.popupErrorMessage(errStr,false,true);
          }
        }
      }).start();
      inSendTestEmailMessageFlag = false;   //indicate done method
  }

  /**
   * Displays the given message in a popup dialog with no title and no
   * buttons.  The returned dialog object may be used to close the dialog.
   * This method does not block.
   * @param msgStr the message to be displayed.
   * @return A new 'IstiDialogPopup' object.
   */
  public IstiDialogPopup showWaitingDialog(String msgStr)
  {
         //create dialog with no title and no buttons:
    final IstiDialogPopup dialogObj = new IstiDialogPopup(
      progInfo.getProgramFrameObj(),
      msgStr,UtilFns.EMPTY_STRING,false,
      IstiDialogPopup.NO_AUTOBUTTONS_OPTION);
         //setup to dispose dialog after it is closed:
    dialogObj.setDefaultCloseOperation(IstiDialogPopup.DISPOSE_ON_CLOSE);
    dialogObj.setVisibleViaInvokeLater(true);    //show dialog
    return dialogObj;
  }

  /**
   * Tests the connection to the SMTP server.
   * @param showSuccessFlag true to show a "success" dialog; false to not.
   * @param waitFlag true to block until the test finishes; false to use
   * a worker thread.
   * @param showTestDlgFlag true to show popup dialog while email-server
   * test is in progress; false to not.
   * @param checkPropsFlag true to check email-server configuration
   * properties and show a dialog if they are not setup; false to not.
   * @param chkPropsModalFlag true for dialog shown via 'checkPropsFlag' ==
   * true to be modal; false for non-modal.
   */
  public void testMailServerConnection(final boolean showSuccessFlag,
                      final boolean waitFlag, final boolean showTestDlgFlag,
              final boolean checkPropsFlag, final boolean chkPropsModalFlag)
  {
    initialServerTestDoneFlag = true;       //indicate initial test done
    if(checkPropsFlag)
    {    //check email-server properties
              //if necessary email properties not setup then show dialog:
      while (!alertEmailSenderObj.areEmailPropsReady())
      {
        if(!settingsDialog.showEmailPropsNotReadyDialog(
                                                    true,chkPropsModalFlag))
        {     //user hit 'Cancel'; abort method
          return;
        }
      }
    }
    if(alertEmailSenderObj.smtpServerAddressProp.stringValue().length() > 0)
    { //SMTP server address cfgProp is not empty
      if(!waitFlag)
      { //not waiting; use worker thread
        (new Thread("testMailServerConnection")
        {
          public void run()
          {
            doTestMailServerConnection(
                                  showSuccessFlag,waitFlag,showTestDlgFlag);
          }
        }).start();
      }
      else //waiting for finish; use calling thread
        doTestMailServerConnection(showSuccessFlag,waitFlag,showTestDlgFlag);
    }
  }

  /**
   * Tests the connection to the SMTP server.
   * @param showSuccessFlag true to show a "success" dialog; false to not.
   * @param waitFlag true to block until the test finishes; false to use
   * a worker thread.
   * @param showTestDlgFlag true to show popup dialog while email-server
   * test is in progress; false to not.
   * @param checkPropsFlag true to check email-server configuration
   * properties and show a (modal) dialog if they are not setup; false
   * to not.
   */
  public void testMailServerConnection(boolean showSuccessFlag,
          boolean waitFlag, boolean showTestDlgFlag, boolean checkPropsFlag)
  {
    testMailServerConnection(showSuccessFlag,waitFlag,showTestDlgFlag,
                                                       checkPropsFlag,true);
  }

  /**
   * Tests the connection to the SMTP server.  This method does not wait
   * for the test to finish (a worker thread is used) and a popup dialog is
   * shown while the test is in progress.
   * @param showSuccessFlag true to check email-server configuration
   * properties and show a (modal) dialog if they are not setup and to
   * show a "success" dialog; false to not.
   */
  public void testMailServerConnection(boolean showSuccessFlag)
  {
    testMailServerConnection(
                           showSuccessFlag,false,true,showSuccessFlag,true);
  }

  /**
   * Performs the work of testing the connection to the SMTP server.  This
   * method blocks until the test is complete.
   * @param showSuccessFlag true to show a "success" dialog; false to not.
   * @param modalFlag true to make the "success" or "error" dialog modal;
   * false for non-modal dialog.
   * @param showTestDlgFlag true to show popup dialog while email-server
   * test is in progress; false to not.
   */
  protected void doTestMailServerConnection(boolean showSuccessFlag,
                                 boolean modalFlag, boolean showTestDlgFlag)
  {
    IstiDialogPopup waitPopupObj = null;
    logObj.debug("Testing connection to SMTP server \"" +
            alertEmailSenderObj.smtpServerAddressProp.stringValue() + "\"");
    if(showTestDlgFlag)
    {    //show-dialog flag is set; show dialog while connecting
      waitPopupObj = showWaitingDialog(
                              "Testing connection to SMTP email server...");
    }
    boolean resultFlag =     //check validity of 'from' email address:
            alertEmailSenderObj.getJavaMailUtilObj().validateFromAddress(
                     alertEmailSenderObj.emailFromAddressProp.stringValue(),
                       alertEmailSenderObj.emailFromNameProp.stringValue());
    final String errMsgPrefixStr;
    if(resultFlag)
    {    //'from' address validated OK
      resultFlag =                //perform email-server test
            alertEmailSenderObj.getJavaMailUtilObj().testServerConnection();
      errMsgPrefixStr = UtilFns.EMPTY_STRING;
    }
    else      //'from' address invalid; setup error-message prefix
      errMsgPrefixStr = "Error accessing email server:  ";
    if(showTestDlgFlag)
      waitPopupObj.close();       //if show-dialog flag then clear dialog
    if(resultFlag)
    { //success flag returned
      logObj.debug("Successfully tested connection to SMTP server \"" +
            alertEmailSenderObj.smtpServerAddressProp.stringValue() + "\"");
      if(showSuccessFlag)
      { //show-success flag is set; show dialog
        final IstiDialogUtil dialogUtilObj = progInfo.getIstiDialogUtil();
        if(dialogUtilObj != null)
        {
          dialogUtilObj.popupMessage("Successfully tested " +
                               "connection to SMTP email server", "Success",
                                  IstiDialogPopup.PLAIN_MESSAGE,modalFlag);
        }
      }
    }
    else
    { //error flag returned; show and log message:
      final IstiDialogUtil dialogUtilObj = progInfo.getIstiDialogUtil();
      if(dialogUtilObj != null)
      {
        dialogUtilObj.popupErrorMessage(logObj,(errMsgPrefixStr +
                                   alertEmailSenderObj.getJavaMailUtilObj().
                                                   getErrorMessageString()),
                                                            modalFlag,true);
      }
    }
  }

  /**
   * Determines if the initial server test has been performed.
   * @return true if the initial server test has been performed; false
   * if not.
   */
  public boolean getInitialServerTestDoneFlag()
  {
    return initialServerTestDoneFlag;
  }
}
