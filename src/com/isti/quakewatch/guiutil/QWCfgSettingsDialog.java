//QWCfgSettingsDialog.java:  Manages a QuakeWatch configuration-settings
//                           dialog.
//
//  1/17/2006 -- [KF]  Initial version.
//  1/27/2006 -- [KF]  Removed 'getConfigGroupName()' method.
//  3/27/2006 -- [ET]  Added optional 'modalFlag' parameter to method
//                     'showEmailPropsNotReadyDialog()'.
//   4/3/2006 -- [ET]  Added optional 'addListenerFlag' parameter to
//                     'createSettingsDialog()' method.
//  8/18/2006 -- [ET]  Made property-change listener check for
//                     'CHANGE_ALL_PROPSTR' event (and ignore "ancestor"
//                     event generated when dialog is first shown).
//

package com.isti.quakewatch.guiutil;

import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import javax.swing.SwingUtilities;

import com.isti.util.ProgramInformationInterface;
import com.isti.util.propertyeditor.CfgSettingsDialog;
import com.isti.util.propertyeditor.CfgPropertyInspector;
import com.isti.util.gui.IstiDialogPopup;
import com.isti.util.gui.IstiDialogUtil;

import com.isti.quakewatch.alertemail.AlertEmailSender;

/**
 * Class QWCfgSettingsDialog manages a QuakeWatch configuration-settings
 * dialog.
 */
public class QWCfgSettingsDialog extends CfgSettingsDialog
{
  private final AlertEmailSender alertEmailSender;

    /** Flag used by 'showSettingsDialog()' method. */
  private boolean emailPropsReadyFlag = false;

  /**
   * Creates a manager for a QuakeWatch configuration-settings dialog.
   * @param progInfo the program information.
   * @param alertEmailSender the alert email sender.
   */
  public QWCfgSettingsDialog(ProgramInformationInterface progInfo,
                                          AlertEmailSender alertEmailSender)
  {
    super(progInfo);
    this.alertEmailSender = alertEmailSender;
  }

  /**
   * Creates the "Settings" dialog.
   * @param addListenerFlag true to add listener for saving configuration
   * and checking if email props not ready.
   */
  public void createSettingsDialog(boolean addListenerFlag)
  {
    super.createSettingsDialog();
    if(addListenerFlag)
    {
         //create and add listener called to save program settings:
      final CfgPropertyInspector propInspObj;
      if((propInspObj=getCfgPropertyInspector()) != null)
      {       //cfg-prop-inspector object is OK
        propInspObj.addPropertyChangeListener(new PropertyChangeListener()
            {
              public void propertyChange(PropertyChangeEvent evt)
              {
                if(CfgPropertyInspector.CHANGE_ALL_PROPSTR.equals(
                                                     evt.getPropertyName()))
                {  //event is "ChangeAll" via 'commitConfigSettings()'
                        //save settings to config file:
                  getProgramInformation().saveConfiguration(true, false);
                  if(!alertEmailSender.dontCheckSMTPFlagProp.booleanValue()
                                && !alertEmailSender.areEmailPropsReady() &&
                                                        emailPropsReadyFlag)
                  {     //don't-check flag is clear, email props are
                        // clear and they existed before
                        //show email-props-not-ready dialog (via
                        // 'invokeLater()' to try and keep things
                        // from being too recursive):
                    SwingUtilities.invokeLater(new Runnable()
                        {
                          public void run()
                          {
                            showEmailPropsNotReadyDialog(false);
                          }
                        });
                  }
                }
              }
            });
      }
    }
  }

  /**
   * Creates the "Settings" dialog.  A listener for saving configuration
   * and checking if email props not ready is added.
   */
  public void createSettingsDialog()
  {
    createSettingsDialog(true);
  }

  /**
   * Gets the 'AlertEmailSender' object.
   * @return the 'AlertEmailSender' object.
   */
  public AlertEmailSender getAlertEmailSender()
  {
    return alertEmailSender;
  }

  /**
   * Sets up the "Settings" dialog.
   */
  public void setupSettingsDialog()
  {
    super.setupSettingsDialog();
         //setup email-props-Ready flag (checked by listener):
    emailPropsReadyFlag = alertEmailSender.areEmailPropsReady();
  }

  /**
   * Displays the email-settings-not-ready dialog and then, if the user
   * hits the 'OK' button, the configuration-settings dialog.
   * @param waitFlag true to make this method block until the
   * configuration-settings dialog is closed (even if 'modalFlag'==false).
   * @param modalFlag true for a modal email-settings-not-ready dialog,
   * false for non-modal.
   * @return true if the user hit the 'OK' button; false if not.
   */
  public final boolean showEmailPropsNotReadyDialog(boolean waitFlag,
                                                          boolean modalFlag)
  {
    final String msgStr = "This program cannot send alert email messages " +
                                 "until the \"'From' email address\" and " +
                    "\"Address of SMTP server\" settings are filled in.  " +
                                        "Hit 'OK' to enter these settings.";
    final IstiDialogUtil dialogUtilObj =
                                getProgramInformation().getIstiDialogUtil();
    if(dialogUtilObj == null || dialogUtilObj.popupOKConfirmMessage(
            msgStr,"Email Settings",modalFlag) != IstiDialogPopup.OK_OPTION)
    {    //user did not hit 'OK'
      return false;
    }
    showSettingsDialog(alertEmailSender.getEmailPropsGroup(),waitFlag);
    return true;
  }

  /**
   * Displays the email-settings-not-ready dialog and then, if the user
   * hits the 'OK' button, the configuration-settings dialog.  The
   * email-settings-not-ready dialog is modal.
   * @param waitFlag true to make this method block until the
   * configuration-settings dialog is closed.
   * @return true if the user hit the 'OK' button; false if not.
   */
  public final boolean showEmailPropsNotReadyDialog(boolean waitFlag)
  {
    return showEmailPropsNotReadyDialog(waitFlag,true);
  }
}
