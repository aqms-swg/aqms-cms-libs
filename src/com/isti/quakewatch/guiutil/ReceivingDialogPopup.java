//ReceivingDialogPopup.java:  Defines the "Receiving" popup dialog
//                            shown while receiving messages from
//                            the server.
//
//  3/26/2004 -- [ET]
//   6/3/2004 -- [KF]  Move base logic into new 'ProgressDialog' class.
//

package com.isti.quakewatch.guiutil;

import java.awt.Component;
import com.isti.util.gui.ProgressDialog;

/**
 * Class ReceivingDialogPopup defines the "Receiving" popup dialog
 * shown while receiving messages from the server.
 */
public class ReceivingDialogPopup extends ProgressDialog
{
  /**
   * Creates a "Receiving" popup dialog.
   * @param parentComp the parent component for the popup.
   */
  public ReceivingDialogPopup(Component parentComp)
  {
    super(parentComp,"Receiving","Receiving event-messages from server...");
  }
}
