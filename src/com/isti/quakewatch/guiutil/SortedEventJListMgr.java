//SortedEventJListMgr.java:  Manages a JList GUI object containing a
//                           list of sorted event objects.
//
//  9/13/2004 -- [KF]  Module created by pulling OpenMap-related code from
//                     'QWEventListManager'.
//  5/26/2005 -- [ET]  Renamed class from "EventJListManager" to
//                     "SortedEventJListMgr" and moved from "message"
//                     to "guiutil" package; removed code to set fixed
//                     cell width.
//  5/27/2005 -- [ET]  Modified 'fillAndEnableQuakeList()' to make it
//                     thread safe; removed 'quakeListFont' parameter
//                     from constructor.
//   8/5/2005 -- [ET]  Modified to use QWEventMsgRecord method
//                     'containsShakeMapProduct()' instead of
//                     'getShakeMapInfo()'.
//  12/7/2005 -- [KF]  Added support for 'ExtendedComparable'.
//  12/8/2005 -- [KF]  Added support to select next/previous Tsunami.
//  1/10/2006 -- [ET]  Added 'getQuakeListObj()' method.
//   6/7/2006 -- [ET]  Added 'getLastManualSelectTime()' method and
//                     implementation; added optional 'autoSelFlag' to
//                     methods 'setSelectedEventKeyStr()' and
//                     'selectLastEventInList()'; modified method
//                     'setSelectedEventKeyStr()' to set last-selected
//                     time in selected event-message-record object.
//  8/14/2006 -- [ET]  Added notes to javadoc for class.
//  1/24/2008 -- [ET]  Added thread synchronization for instance variable
//                     'lastManualSelectTime'.
//  5/12/2011 -- [ET]  Modified to use case-insensitive event-ID keys.
//

package com.isti.quakewatch.guiutil;

import java.awt.Component;
import java.awt.Color;
import java.awt.Point;
import java.awt.event.MouseListener;
import java.awt.event.KeyListener;
import javax.swing.ListSelectionModel;
import javax.swing.ListCellRenderer;
import com.isti.util.ExtendedComparable;
import com.isti.util.LogFile;
import com.isti.util.FifoHashtable;
import com.isti.util.gui.SortedValuesJList;
import com.isti.quakewatch.message.QWEventMsgRecord;
import com.isti.quakewatch.message.EventListManager;

/**
 * Class SortedEventJListMgr manages a JList GUI object containing a
 * list of sorted event objects.  The events are held in two parallel
 * lists--the 'eventsListTable' in the parent class and the 'quakeListObj'
 * in this class.  This is done so the GUI 'quakeListObj' component is
 * always updated via the event-dispatch thread, while the event objects
 * in the 'eventsListTable' are always directly available to any thread.
 * The methods in this class ensure that the two lists are kept in sync.
 * The event objects in the 'eventsListTable' are always sorted by value,
 * ascending.  The GUI 'quakeListObj' component defaults to being sorted
 * by ascending values, but may be re-sorted via the 'reSortList()' method
 * (in 'SortedValuesJList').  After this is done, the event-index values
 * of the two lists will no longer correspond.
 */
public class SortedEventJListMgr extends EventListManager
{
  protected final SortedValuesJList quakeListObj;
  protected boolean quakeListEnabledFlag = false;
  protected QWEventMsgRecord selectedRecordObj = null;
  protected final Object selectedRecordObjSyncObj = new Object();
  protected final Object setSelEventMethodSyncObj = new Object();
  protected boolean inSetSelectedEventFlag = false;
  protected EventSelectionChangeListener eventSelChangeListener = null;
  protected long lastManualSelectTime = 0;
  protected final Object lastManSelTimeSyncObj = new Object();

  /**
   * Creates an event list manager.
   * @param quakeListCellRenderer the list-cell renderer to use with
   * the quake JList.
   * @param logFileObj log file object to use.
   * @param sortDirFlag true for ascending sort order, false for
   * descending.
   */
  public SortedEventJListMgr(ListCellRenderer quakeListCellRenderer,
                                    LogFile logFileObj, boolean sortDirFlag)
  {
    this(quakeListCellRenderer,logFileObj,sortDirFlag,
                                           ExtendedComparable.NO_SORT_TYPE);
  }

  /**
   * Creates an event list manager.
   * @param quakeListCellRenderer the list-cell renderer to use with
   * the quake JList.
   * @param logFileObj log file object to use.
   * @param sortDirFlag true for ascending sort order, false for
   * descending.
   * @param sortType the sort type.
   */
  public SortedEventJListMgr(ListCellRenderer quakeListCellRenderer,
                      LogFile logFileObj, boolean sortDirFlag, int sortType)
  {
    super(logFileObj);
    quakeListObj = new SortedValuesJList(
                           quakeListCellRenderer,null,sortDirFlag,sortType);
         //setup JList to only allow one item to be selected at a time:
    quakeListObj.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
         //setup JList listener to process changes in selection:
    quakeListObj.setListSelKeyChangeCallBack(
                            new SortedValuesJList.ListSelKeyChangeCallBack()
        {
          public void newSelectedKeyObj(Object keyObj)
          {
            processQuakeListSelChange(keyObj);
          }
        });
  }

  /**
   * Creates a "dummy" list manager.
   */
  public SortedEventJListMgr()
  {
    super();
    quakeListObj = new SortedValuesJList();
  }

  /**
   * Sets the callback listener to be used when a new event is selected.
   * Only one callback listener may be installed at a time.  Calling
   * this method with a 'null' parameter will clear the listener.  This
   * method should not be called at any time that a selection change may
   * be in progress.
   * @param listenerObj the callback listener to use.
   */
  public void setEventSelectionChangeListener(
                                   EventSelectionChangeListener listenerObj)
  {
    eventSelChangeListener = listenerObj;
  }

  /**
   * Sets up the quake list to use the list model held by this object.
   */
  public void fillAndEnableQuakeList()
  {
    quakeListEnabledFlag = true;            //enable quake list updates
              //fill quake list from table (use clone to be thread safe):
    quakeListObj.putAll((FifoHashtable)(eventsListTable.clone()));
  }

  /**
   * Adds the given event message record object to the table of events,
   * putting it into sorted position.
   * @param recObj the event message record object to add.
   */
  public void addEventToList(QWEventMsgRecord recObj)
  {
    super.addEventToList(recObj);      //add event to local table
    if(quakeListEnabledFlag)     //if enabled then add event to quake JList:
      quakeListObj.put(recObj.getLCEventIDKey(),recObj);
    synchronized(selectedRecordObjSyncObj)
    {    //grab thread lock for selected record object
      if(selectedRecordObj != null &&
                         selectedRecordObj.getEventIDKey().equalsIgnoreCase(
                                                    recObj.getEventIDKey()))
      {  //new record's event-ID is same as currently-selected object's
        selectedRecordObj = recObj;    //update currently-selected object
      }
    }
  }

  /**
   * Removes the given event message record object from the table of events.
   * @param keyStr the event-ID string for the message record to be removed.
   */
  public void removeEventFromList(String keyStr)
  {
    if(keyStr != null)
    {  //given key string not null
      super.removeEventFromList(keyStr);    //remove event from local table
              //remove event from quake JList (use lower-cased event ID):
      quakeListObj.removeKey(keyStr.toLowerCase());
      synchronized(selectedRecordObjSyncObj)
      {  //grab thread lock for selected record object
        if(selectedRecordObj != null &&
                 selectedRecordObj.getEventIDKey().equalsIgnoreCase(keyStr))
        {  //record's event-ID is same as currently-selected object's
          selectedRecordObj = null;         //clear selected record object
        }
      }
    }
 }

  /**
   * Removes the event message record object for the given index value
   * from the table of events.
   * @param idx the index of the event message record object to be removed.
   * @return The "key" object for the removed record, or null if a record
   * for the given index could not be found.
   */
  public Object removeEventForIndex(int idx)
  {
    final Object keyObj;
    if((keyObj=super.removeEventForIndex(idx)) != null)
      quakeListObj.removeKey(keyObj);  //remove event from quake JList
    return keyObj;
  }

  /**
   * Sets the selected event message record object.
   * @param keyStr the ID key string of event message record to select,
   * or null to have none selected.
   * @param autoSelFlag true if event is being "automatically" selected;
   * false if event is being "manually" selected.
   */
  public void setSelectedEventKeyStr(String keyStr, boolean autoSelFlag)
  {
    setSelectedEventKeyStr(keyStr,autoSelFlag,false);
  }

  /**
   * Sets the selected event message record object.
   * @param keyStr the ID key string of event message record to select,
   * or null to have none selected.
   */
  public void setSelectedEventKeyStr(String keyStr)
  {
    setSelectedEventKeyStr(keyStr,false,false);
  }

  /**
   * Sets the selected event message record object.
   * @param keyStr the ID key string of event message record to select,
   * or null to have none selected.
   * @param autoSelFlag true if event is being "automatically" selected;
   * false if event is being "manually" selected.
   * @param inProcListSelChangeFlag true if method called from
   * 'processQuakeListSelChange()'.
   */
  protected void setSelectedEventKeyStr(String keyStr, boolean autoSelFlag,
                                            boolean inProcListSelChangeFlag)
  {
    synchronized(setSelEventMethodSyncObj)
    {    //only allow one thread in at a time
      inSetSelectedEventFlag = true;        //indicate method in progress
      logObj.debug3("SortedEventJListMgr:  setSelectedEventKeyStr(\"" +
                                        keyStr + "\"," + autoSelFlag + "," +
                                             inProcListSelChangeFlag + ")");
      if(keyStr != null)
      {  //event ID key string given
        final QWEventMsgRecord recObj;
        if((recObj=getRecordObjForKeyStr(keyStr)) != null)
        {     //new ID key matched to record on list
          logObj.debug2("SortedEventJListMgr:  Setting selected event to:  " +
                                                 recObj.getDisplayString());
          final long currentTimeVal = System.currentTimeMillis();
                        //set time that event was last selected:
          recObj.setSelectedTimeVal(currentTimeVal);
          final String oldKeyStr;
          synchronized(selectedRecordObjSyncObj)
          {   //grab thread lock for selected record object
            if(selectedRecordObj != null)
            { //previously-selected event record exists
                                  //update its "selected" time:
              selectedRecordObj.setSelectedTimeVal(currentTimeVal);
                                  //get its event-ID key:
              oldKeyStr = selectedRecordObj.getEventIDKey();
            }
            else   //previously-selected event record does not exist
              oldKeyStr = null;   //no "previous" event-ID key
            selectedRecordObj = recObj;       //save selected record
          }
                   //if "manual" selection and selected-key string
                   // changing then update tracking time:
          if(!autoSelFlag && !keyStr.equalsIgnoreCase(oldKeyStr))
          {
            synchronized(lastManSelTimeSyncObj)
            {  //only allow one thread at a time to access time variable
              lastManualSelectTime = currentTimeVal;
            }
          }
          if(!inProcListSelChangeFlag)
          {   //not called from 'processQuakeListSelChange()'
            logObj.debug3("SortedEventJListMgr:  " +
                     "Setting quake JList selection to \"" + keyStr + "\"");
                        //select event-record with given key string
                        //(ensure visible):
            quakeListObj.setSelectedKey(keyStr.toLowerCase(),true);
          }
              //if selection change listener installed then
              // call change listener with selected record object:
          if(eventSelChangeListener != null)
            eventSelChangeListener.newSelectedEventObj(recObj);
        }
        else
        {
          logObj.debug2("SortedEventJListMgr:  Requested event ID key not " +
                                            "matched (\"" + keyStr + "\")");
        }
      }
      else
      {    //event ID key string not given
        logObj.debug2("SortedEventJListMgr:  Clearing selected event");
        synchronized(selectedRecordObjSyncObj)
        {  //grab thread lock for selected record object
          selectedRecordObj = null;           //clear selection object
        }
        if(!inProcListSelChangeFlag)
        {  //not called from 'processQuakeListSelChange()'
          quakeListObj.clearSelection();      //clear selection on JList
        }
              //if selection change listener installed then
              // call change listener with null to clear selection:
        if(eventSelChangeListener != null)
          eventSelChangeListener.newSelectedEventObj(null);
      }
      inSetSelectedEventFlag = false;       //indicate method complete
    }
  }

  /**
   * Selects the last event on the list.
   * @param autoSelFlag true if event is being "automatically" selected;
   * false if event is being "manually" selected.
   */
  public void selectLastEventInList(boolean autoSelFlag)
  {
    final QWEventMsgRecord recObj;
    if((recObj=getLastEventInList()) != null)    //get last event record
    {
      logObj.debug3("SortedEventJListMgr:  Selecting last event in list:  " +
                            recObj.getDisplayString() + ", " + autoSelFlag);
      setSelectedEventKeyStr(recObj.getEventIDKey(),autoSelFlag);
    }
  }

  /**
   * Selects the last event on the list.
   */
  public void selectLastEventInList()
  {
    selectLastEventInList(false);
  }

  /**
   * Returns the selected event message record object.
   * @returns The selected event message record object, or null if none
   * if selected.
   */
  public QWEventMsgRecord getSelectedRecordObj()
  {
    synchronized(selectedRecordObjSyncObj)
    {    //grab thread lock for selected record object
      return selectedRecordObj;
    }
  }

  /**
   * Selects the previous or next event with products.
   * @param name the name of the menu item.
   * @return true if a different event with products was
   * selected, false if none was found.
   */
  public boolean selectProductsFromList(String name)
  {
    //true to find next product, false to find previous product
    final boolean dirFlag = name.indexOf("Next") >= 0;
    //true to find next ShakeMap, false otherwise
    final boolean shakeMapFlag = name.indexOf("ShakeMap") >= 0;
    //true to find next Tsunami, false otherwise
    final boolean tsunamiFlag = name.indexOf("Tsunami") >= 0;
    int i;
    Object obj;
    QWEventMsgRecord recObj;
    final int selListIndex;
    synchronized(eventsListTable)
    {    //grab thread lock for list table object
                   //get index of current selected event record:
      selListIndex = getSelectedIndex();
      final int lastIndex;        //get index of last item in list:
      if((lastIndex=eventsListTable.size()-1) < 0)
        return false;        //if list empty then return non-found flag
         //get index of initial selected item (if any):
      int initialIndex = (selListIndex >= 0) ? selListIndex : 0;
      if(initialIndex > lastIndex)   //if past last index then
        initialIndex = lastIndex;    //use last index instead
      i = initialIndex;
      while(true)
      {       //for each list item processed
        if(dirFlag)
        {     //searching for next event; increment index
          if(++i > lastIndex)     //if index value is out of bounds then
            i = 0;                //wrap to beginning
        }
        else
        {     //searching for previous event; decrement index
          if(--i < 0)             //if index value is out of bounds then
            i = lastIndex;        //wrap to end
        }
        if ((obj = eventsListTable.elementAt(i)) instanceof QWEventMsgRecord)
        {
          recObj = (QWEventMsgRecord)obj;
          try
          { //get list item for index:
            if (recObj.getProductCount() > 0)
            { //event contains products
              //if not looking for Tsunami or if Tsunami is available and
              // not looking for ShakeMap or if ShakeMap is available
              if ((!tsunamiFlag || recObj.containsTsunamiProduct()) &&
                  (!shakeMapFlag || recObj.containsShakeMapProduct()))
              {
                break; //exit loop (and select index)
              }
            }
          }
          catch (Exception ex)
          {} //ignore any unexpected errors
        }
        if(i == initialIndex)        //if just processed initial item
          return false;              // then return none-found flag
      }
    }
         //(make sure that 'setSelectedEventKeyStr()' is done outside of
         // the "synchronized(eventsListTable)" block to prevent a JList
         // selection operation from happening inside of the block:
    if(i != selListIndex)
    {    //new index value; select its record
      setSelectedEventKeyStr(((QWEventMsgRecord)obj).getEventIDKey(),false);
      return true;
    }
    return false;
  }

  /**
   * Updates the quake JList object.
   */
  public void fireQuakeListUpdate()
  {
    quakeListObj.repaint();            //repaint list
  }

  /**
   * Returns the index for the current selected event record.
   * @return The index for the current selected event record, or -1 if
   * no event is selected.
   */
  protected int getSelectedIndex()
  {
    synchronized(selectedRecordObjSyncObj)
    {    //grab thread lock for selected record object
      return (selectedRecordObj != null) ?
                 getIndexOfEventKey(selectedRecordObj.getEventIDKey()) : -1;
    }
  }

  /**
   * Processes a selection-change event from the JList.
   * @param keyObj the key for the newly selected event.
   */
  protected void processQuakeListSelChange(Object keyObj)
  {
    logObj.debug3("SortedEventJListMgr:  processQuakeListSelChange(), " +
                        "inSetSelectedEventFlag=" + inSetSelectedEventFlag);
         //if 'setSelectedEventKeyStr()' in progress then abort because
         // either it caused the change event or another selection
         // action is happening that should take priority:
    if(inSetSelectedEventFlag)
      return;
    if(keyObj instanceof String)
    {    //selection OK; forward event selection
      final String keyStr = (String)keyObj;
      logObj.debug3("SortedEventJListMgr:  processQuakeListSelChange() " +
                                                  "forwarding \"" + keyStr +
                                "\" selection to setSelectedEventKeyStr()");
      setSelectedEventKeyStr(keyStr,false,true); //(indicate source of call)
    }
  }

  /**
   * Sets whether or not 'ListDataListener' objects attached to the list
   * model for the quake list are called when changes occur.
   * @param flgVal true to enable calling of 'ListDataListener' objects,
   * false to disable.
   */
  public void setQuakeListListenersEnabled(boolean flgVal)
  {
    quakeListObj.setListenersEnabled(flgVal);
  }

  /**
   * Fires a contents-changed event to the list model for the quake list.
   */
  public void fireQuakeListContentsChanged()
  {
    quakeListObj.fireContentsChanged();
  }

  /**
   * Ensures that the current quake list selection is visible.
   */
  public void ensureQuakeListSelectionVisible()
  {
    quakeListObj.ensureSelectionVisible();
  }

  /**
   * Requests that the quake list have the keyboard focus.
   */
  public void requestQuakeListFocus()
  {
    quakeListObj.requestFocus();
  }

  /**
   * Updates the quake list colors.
   * @param foregroundColor the foreground color to set, or null to leave
   * the color unchanged.
   * @param backgroundColor the background color to set, or null to leave
   * the color unchanged.
   * @param selForegroundColor the selection foreground color to set, or
   * null to leave the color unchanged.
   * @param selBackgroundColor the selection background color to set, or
   * null to leave the color unchanged.
   * @param directFlag if true then the color setting is always done
   * immediately; if false and the thread calling this method is not
   * the event-dispatch thread then the action is queued and invoked
   * later by the event-dispatch thread.
   */
  public void updateQuakeListColors(Color foregroundColor,
                            Color backgroundColor, Color selForegroundColor,
                               Color selBackgroundColor, boolean directFlag)
  {
    logObj.debug2("SortedEventJListMgr.updateQuakeListColors:  " +
               "foregroundColor=" + foregroundColor + ", backgroundColor=" +
            backgroundColor + ", selForegroundColor=" + selForegroundColor +
            ", selBackgroundColor=" + selBackgroundColor + ", directFlag=" +
                                                                directFlag);
              //update colors on the quake list:
    quakeListObj.setColors(foregroundColor,backgroundColor,
                          selForegroundColor,selBackgroundColor,directFlag);
  }

  /**
   * Returns the quake list object associated with the cell closest to
   * the given location.
   * @param pointObj the coordinates to use, relative to the quake list.
   * @return The quake list object associated with the cell closest to
   * the given location, or null if none found.
   */
  public Object quakeListlocationToValue(Point pointObj)
  {
    return quakeListObj.locationToValue(pointObj);
  }

  /**
   * Returns the currently selected value on the quake list.
   * @return The currently selected value on the quake list, or null
   * if none currently selected.
   */
  public Object getQuakeListSelectedValue()
  {
    return quakeListObj.getSelectedValue();
  }

  /**
   * Adds the given mouse listener to the quake list.
   * @param listenerObj the mouse listener object to use.
   */
  public void addQuakeListMouseListener(MouseListener listenerObj)
  {
    quakeListObj.addMouseListener(listenerObj);
  }

  /**
   * Adds the given key listener to the quake list.
   * @param listenerObj the key listener object to use.
   */
  public void addQuakeListKeyListener(KeyListener listenerObj)
  {
    quakeListObj.addKeyListener(listenerObj);
  }

  /**
   * Returns the quake-list 'JList' component object.
   * @return The quake-list 'JList' component object.
   */
  public Component getQuakeListObj()
  {
    return quakeListObj;
  }

  /**
   * Returns the quake-list display component object.  Sub-classes may
   * override this method to return a hosting panel instead of the
   * 'JList' object itself.
   * @return The quake-list display component object.
   */
  public Component getQuakeListComponent()
  {
    return quakeListObj;
  }

  /**
   * Returns the time of the last "manual" selection change to the list.
   * @return The time of the last "manual" selection change to the list,
   * or 0 if none have occurred.
   */
  public long getLastManualSelectTime()
  {
    synchronized(lastManSelTimeSyncObj)
    {  //only allow one thread at a time to access time variable
      return lastManualSelectTime;
    }
  }


  /**
   * Interface EventSelectionChangeListener defines a callback listener
   * used when a new event is selected.
   */
  public interface EventSelectionChangeListener
  {
    /**
     * Method called with the new selected event.
     * @param recObj the new selected event message record object.
     */
    public void newSelectedEventObj(QWEventMsgRecord recObj);
  }
}
