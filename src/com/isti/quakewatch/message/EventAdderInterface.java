//EventAdderInterface.java:  Defines a method used to add an
//                           event-message record.
//
// 11/17/2003 -- [ET]  Initial version.
//  3/18/2004 -- [ET]  Moved from 'QWClient' to 'QWCommon' project.
//

package com.isti.quakewatch.message;

/**
 * Interface EventAdderInterface defines a method used to add an
 * event-message record.
 */
public interface EventAdderInterface
{
  /**
   * Adds the given event message record object.
   * @param recObj the event message record object to add.
   */
  void addEventToList(QWEventMsgRecord recObj);
}
