//EventListManager.java:  Manages a list of event objects.
//
//   3/7/2003 -- [ET]  Initial version.
//  6/19/2003 -- [ET]  Added 'selectLastEventInList()' method.
//   7/2/2003 -- [ET]  Modified to use a fixed width on the quake list
//                     (rather than letting the layout manager fit panel
//                     width to content).
//   7/8/2003 -- [ET]  Modified 'fireQuakeListUpdate()' method to fire
//                     contents-changed event to the list model.
// 10/22/2003 -- [ET]  Modified to use an internal 'FifoHashtable' to
//                     hold the master events list and to use a
//                     'SortedValuesJList' object for the displayed quake
//                     list (operated in a "write-only" fashion); fixed
//                     quake-list selection issues when list updated
//                     (via 'SortedValuesJList' improvements).
// 11/18/2003 -- [ET]  Added "implements EventAdderInterface"; took
//                     "Unable to fetch..." debug-log message out of
//                     'processQuakeListSelChange()' method.
//  1/20/2004 -- [ET]  Fixed so that if the currently-selected record
//                     object is modified (via the 'addEventToList()'
//                     method) the proper record object is returned
//                     via the 'getSelectedRecordObj()' method.
//  3/18/2004 -- [ET]  Removed JList-related code; moved from 'QWClient'
//                     to 'QWCommon' project.
//  5/17/2005 -- [ET]  Modified to use QWEventMsgRecord 'getEventIDKey()'
//                     method instead of directly accessing variable.
//  5/12/2011 -- [ET]  Modified to use case-insensitive event-ID keys.
//

package com.isti.quakewatch.message;

import java.util.Vector;
import java.util.Iterator;
import com.isti.util.FifoHashtable;
import com.isti.util.LogFile;

/**
 * Class EventListManager manages a list of event objects.
 */
public class EventListManager implements EventAdderInterface
{
                                       //table of event objects:
  protected final FifoHashtable eventsListTable = new FifoHashtable();
  protected final LogFile logObj;
  protected boolean eventsListChangedFlag = false;

  /**
   * Creates an event list manager.
   * @param logFileObj log file object to use, or null for none.
   */
  public EventListManager(LogFile logFileObj)
  {
         //setup log file object; use dummy log file obj if none given:
    logObj = (logFileObj != null) ? logFileObj :
                          new LogFile(null,LogFile.NO_MSGS,LogFile.NO_MSGS);
  }

  /**
   * Creates a "dummy" list manager.
   */
  public EventListManager()
  {
    logObj = new LogFile(null,LogFile.NO_MSGS,LogFile.NO_MSGS);
  }

  /**
   * Returns the thread-synchronized object for the events list.
   * @return The thread-synchronized object for the events list.
   */
  public Object getEventsListSyncObj()
  {
    return eventsListTable;
  }

  /**
   * Adds the given event message record object to the table of events,
   * putting it into sorted position.
   * @param recObj the event message record object to add.
   */
  public void addEventToList(QWEventMsgRecord recObj)
  {
                                       //add event to local table:
    eventsListTable.putSortByValue(recObj.getLCEventIDKey(),recObj,true);
    eventsListChangedFlag = true;      //indicate events list changed
  }

  /**
   * Removes the given event message record object from the table of events.
   * @param keyStr the event-ID string for the message record to be removed.
   */
  public void removeEventFromList(String keyStr)
  {
    if(keyStr != null)
    {  //given key string not null; use lower-cased version
      eventsListTable.remove(keyStr.toLowerCase());
      eventsListChangedFlag = true;    //indicate events list changed
    }
  }

  /**
   * Removes the event message record object for the given index value
   * from the table of events.
   * @param idx the index of the event message record object to be removed.
   * @return The "key" object for the removed record, or null if a record
   * for the given index could not be found.
   */
  public Object removeEventForIndex(int idx)
  {
    final Object keyObj;
    synchronized(eventsListTable)
    {    //grab thread lock for list table object
      if(idx >= 0 && idx < eventsListTable.size())
      {  //index value is OK
        keyObj = eventsListTable.keyAt(idx);     //get key object for index
        eventsListTable.removeElementAt(idx);    //remove object at index
      }
      else    //index value OK
        keyObj = null;       //indicate no key object
    }
    eventsListChangedFlag = true;      //indicate events list changed
    return keyObj;
  }

  /**
   * Returns the events-list-changed flag that is set by the
   * 'addEventToList()' and 'removeEventFromList()' methods.
   * @return true if the events list has changed since the last
   * call to 'clearEventsListChangedFlag()', false otherwise.
   */
  public boolean getEventsListChangedFlag()
  {
    return eventsListChangedFlag;
  }

  /**
   * Clears the events-list-changed flag that is set by the
   * 'addEventToList()' and 'removeEventFromList()' methods.
   */
  public void clearEventsListChangedFlag()
  {
    eventsListChangedFlag = false;
  }

  /**
   * Returns the index value for the given event message record ID key
   * string value.
   * @param keyStr the event ID key string value to use.
   * @return The associated index value, or -1 if none matched.
   */
  public int getIndexOfEventKey(String keyStr)
  {
         //if given key string not null then use lower-cased version:
    return (keyStr != null) ?
                      eventsListTable.indexOfKey(keyStr.toLowerCase()) : -1;
  }

  /**
   * Returns the event message record object with the given event ID key
   * string in the table of events.
   * @param keyStr the event ID string to use.
   * @return The event message record object, or null if no match could
   * be found.
   */
  public QWEventMsgRecord getRecordObjForKeyStr(String keyStr)
  {
    if(keyStr != null)
    {  //given key string not null; use lower-cased version
      final Object obj;
      return ((obj=eventsListTable.get(keyStr.toLowerCase())) instanceof
                           QWEventMsgRecord) ? (QWEventMsgRecord)obj : null;
    }
    return null;
  }

  /**
   * Returns the event message record object at the given index in the
   * table of events.
   * @param indexVal the index value to use.
   * @return The event message record object, or null if no match could
   * be found.
   */
  public QWEventMsgRecord getRecordObjForIndex(int indexVal)
  {
    try
    {
      final Object obj;      //return record object for index:
      return ((obj=eventsListTable.elementAt(indexVal)) instanceof
                           QWEventMsgRecord) ? (QWEventMsgRecord)obj : null;
    }
    catch(ArrayIndexOutOfBoundsException ex)
    {         //if bad index then return null
      return null;
    }
  }

  /**
   * Returns the event key ID string for the event message record
   * associated with the given index.
   * @param indexVal the index value to use.
   * @returns The event key ID string, or null if the index value is out
   * of range.
   */
//  public String getEventKeyForIndex(int indexVal)
//  {
//    final QWEventMsgRecord recObj;
//    return ((recObj=getRecordObjForIndex(indexVal)) != null) ?
//                                                   recObj.eventIDKey : null;
//  }

  /**
   * Returns true if the given event ID key string is in the events list.
   * @param keyStr the event ID key string value to use.
   * @return true if the given event ID key string is in the events list,
   * otherwise false.
   */
  public boolean isEventKeyInList(String keyStr)
  {
    return (keyStr != null) ?     //use lower-cased version of key string
                  eventsListTable.containsKey(keyStr.toLowerCase()) : false;
  }

  /**
   * Returns the last event message record object on the list.
   * @return The event message record object, or null if none could
   * be found.
   */
  public QWEventMsgRecord getLastEventInList()
  {
    try
    {
      final Object obj;
      final int numItems;
      synchronized(eventsListTable)
      {  //grab thread lock for list table
        if((numItems=eventsListTable.size()) > 0)
        {     //list contains items; return last item
          return ((obj=eventsListTable.elementAt(numItems-1)) instanceof
                           QWEventMsgRecord) ? (QWEventMsgRecord)obj : null;
        }
      }
    }
    catch(ArrayIndexOutOfBoundsException ex)
    {         //if bad index then return null
    }
    return null;
  }

  /**
   * Returns the size of the events list.
   * @return The number of events in the list.
   */
  public int getEventsListSize()
  {
    return eventsListTable.size();
  }

  /**
   * Returns an iterator that operates over a read-only collection view
   * of the values contained in the table of event message record objects.
   * The iterator does not support the remove operation.  The table
   * should not be modified while the iterator is in use.
   * @return An iterator that operates over a the values contained in the
   * table of event message record objects.
   */
  public Iterator getEventsListIterator()
  {
    return eventsListTable.values().iterator();
  }

  /**
   * Returns a copy of the Vector of values from the table of event
   * message record objects.
   * @return A 'Vector' object of 'QWEventMsgRecord' objects.
   */
  public Vector getEventsListValuesVec()
  {
    return eventsListTable.getValuesVector();
  }

  /**
   * Sets whether or not 'ListDataListener' objects attached to the list
   * model for the quake list are called when changes occur.
   * This version of the method does nothing; subclasses will probably
   * want to override it.
   * @param flgVal true to enable calling of 'ListDataListener' objects,
   * false to disable.
   */
  public void setQuakeListListenersEnabled(boolean flgVal)
  {
  }

  /**
   * Fires a contents-changed event to the list model for the quake list.
   * This version of the method does nothing; subclasses will probably
   * want to override it.
   */
  public void fireQuakeListContentsChanged()
  {
  }

  /**
   * Ensures that the current quake list selection is visible.
   * This version of the method does nothing; subclasses will probably
   * want to override it.
   */
  public void ensureQuakeListSelectionVisible()
  {
  }
}
