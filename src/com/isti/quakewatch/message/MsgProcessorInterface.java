//MsgProcessorInterface.java:  Defines the QuakeWatch client data-message
//                             processing method.
//
// 11/26/2003 -- [ET]
//

package com.isti.quakewatch.message;

import java.util.Date;
import org.jdom.Element;

/**
 * Interface MsgProcessorInterface defines the QuakeWatch client data-message
 * processing method.
 */
public interface MsgProcessorInterface
{
  /**
   * Processes any number of "Event" and "Product" elements in the given
   * "DataMessage" element.
   * @param qwMsgElement The "QWmessage" element.
   * @param dataMsgElement The "DataMessage" element.
   * @param xmlMsgStr the XML text message string.
   * @param requestedFlag true to indicate the the message was "requested"
   * (and that it should not be processed as a "real-time" message).
   * @param msgNumObj a 'Long' object holding the message number for the
   * message, or null if a message number is not available.
   * @param timeGenObj a 'Date' object holding the time-generated value
   * for the message, or null if a time-generated value is not available.
   */
  void processDataMessage(Element qwMsgElement,
              Element dataMsgElement,String xmlMsgStr,boolean requestedFlag,
                                            Long msgNumObj,Date timeGenObj);
}
