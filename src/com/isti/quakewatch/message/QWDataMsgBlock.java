//DataMessageBlock.java:  Contains the objects needed to process a
//                        QuakeWatch XML data message.
//
// 10/27/2003 -- [ET]
//

package com.isti.quakewatch.message;

import org.jdom.Element;

/**
 * Class DataMessageBlock contains the objects needed to process a
 * QuakeWatch XML data message.
 */
public class QWDataMsgBlock
{
  public final Element qwMsgElement;
  public final Element dataMsgElement;
  public final String xmlMsgStr;
  public final boolean requestedFlag;

  /**
   * Constructs a data block of the objects needed to process an
   * XML data message.
   * @param qwMsgElement The "QWmessage" element.
   * @param dataMsgElement The "DataMessage" element.
   * @param xmlMsgStr the XML text message string.
   * @param requestedFlag true to set the "requested" flag on the generated
   * data-message objects (to indicate that they should not be processed as
   * a "real-time" message).
   */
  public QWDataMsgBlock(Element qwMsgElement,Element dataMsgElement,
                                   String xmlMsgStr,boolean requestedFlag)
  {
    this.qwMsgElement = qwMsgElement;
    this.dataMsgElement = dataMsgElement;
    this.xmlMsgStr = xmlMsgStr;
    this.requestedFlag = requestedFlag;
  }
}
