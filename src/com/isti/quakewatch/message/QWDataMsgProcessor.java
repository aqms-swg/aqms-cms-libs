//QWDataMsgProcessor.java:  Defines methods for data-message processing.
//
// 10/30/2003 -- [ET]
//

package com.isti.quakewatch.message;

import org.jdom.Element;
import com.isti.quakewatch.util.QWConnectionMgr;

/**
 * Interface QWDataMsgProcessor defines methods for data-message processing.
 */
public interface QWDataMsgProcessor
{
  /**
   * Enters the connection manager to be used by this message processor.
   * @param connMgrObj The connection-manager object to use.
   */
  public void setConnectionMgr(QWConnectionMgr connMgrObj);

  /**
   * Processes any number of "Event" and "Product" elements in the given
   * "DataMessage" element.
   * @param qwMsgElement The "QWmessage" element.
   * @param dataMsgElement The "DataMessage" element.
   * @param xmlMsgStr the XML text message string.
   * @param requestedFlag true to set the "requested" flag on the generated
   * data-message objects (to indicate that they should not be processed as
   * a "real-time" message).
   */
  public void processDataMessage(Element qwMsgElement,
             Element dataMsgElement,String xmlMsgStr,boolean requestedFlag);

  /**
   * Returns the last event held in storage.
   * @return The 'QWMsgNumTimeRec' object for the last event held in
   * storage.
   */
  public QWMsgNumTimeRec getLastEventInStorage();
}
