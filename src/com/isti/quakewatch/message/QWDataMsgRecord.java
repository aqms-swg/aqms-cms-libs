//QWDataMsgRecord.java:  A record of data from a QuakeWatch XML DataMessage.
//
//  9/20/2002 -- [ET]  Initial version.
//  6/19/2003 -- [ET]  Changed 'msgNumber' references from 'int' to 'long'.
// 10/28/2003 -- [ET]  Moved from 'QWClient' to 'QWCommon' project;
//                     modified to implement 'ArchivableMsgObjEntry'.
// 11/17/2003 -- [ET]  Moved back from 'QWCommon' to 'QWClient' project;
//                     took out 'ArchivableMsgObjEntry' implementation.
//  3/18/2004 -- [ET]  Moved from 'QWClient' to 'QWCommon' project.
//  5/17/2005 -- [ET]  Moved "set/getRequestedFlag()" methods from
//                     'QWIdentDataMsgRecord' to this class; moved
//                     "get/setUpdateFlag()", "get/setAlarmTriggeredFlag()",
//                     "get/setLocationNote()" and "get/setDisplayObject()"
//                     methods from 'QWEventMsgRecord' to this class;
//                     made DataMessage "Action" attribute optional.
//  5/31/2005 -- [ET]  Added 'relayTimeGen' instance variable.
//   6/1/2005 -- [ET]  Added 'get/setShowMapSymbolFlag()' methods.
//  5/17/2006 -- [ET]  Added 'get/setUtilityObject()' methods.
//

package com.isti.quakewatch.message;

import java.util.Date;
import java.util.List;
import org.jdom.Element;
import com.isti.util.Archivable;
import com.isti.quakewatch.common.MsgTag;

/**
 * QWDataMsgRecord defines a record of data from a QuakeWatch XML DataMessage.
 */
public class QWDataMsgRecord extends QWMsgRecord
{
    /**
     * Handle to the DataMessage 'Element' object used to construct this
     * object.
     */
  public final Element dataMsgElement;
    /** Action string for message ("Update", "Delete"). */
  public final String action;
    /** Time that message was received at server (or null if not given). */
  public final Date timeReceived;
    /** Time generated received by QWRelay feeder (or null if not given). */
  public final Date relayTimeGen;
    /** Flag set true to indicate that record was "requested". */
  protected boolean requestedFlag = false;
    /** Flag set true if this is an update to an existing record. */
  protected boolean updateFlag = false;
    /** Flag set true if an alarm has been triggered for this record. */
  protected boolean alarmTriggeredFlag = false;
    /** Flag set true if symbol for message should be shown on map. */
  protected boolean showMapSymbolFlag = true;
    /** Location note text for message. */
  protected String locationNote = "";
    /** Handle for a display object associated with this message record. */
  protected Object displayObject;
    /** Handle for a utility object associated with this message record. */
  protected Object utilityObject;

  /**
   * Creates a record of data from a QuakeWatch XML DataMessage.
   * @param qwMsgElement the XML "QWmessage" element object.
   * @param dataMsgElement the XML "DataMessage" element object.
   * @throws QWRecordException if an error occurs while creating
   * the data record.
   */
  public QWDataMsgRecord(Element qwMsgElement,Element dataMsgElement)
                                                    throws QWRecordException
  {
    super(qwMsgElement);                    //construct parent object
    this.dataMsgElement = dataMsgElement;   //save handle to element
    if(dataMsgElement == null)
      throw new QWRecordException("Null element object handle");
    if(!MsgTag.DATA_MESSAGE.equalsIgnoreCase(dataMsgElement.getName()))
    {    //root element name not "DataMessage"
      throw new QWRecordException("Element tag name \"" +
                                      MsgTag.DATA_MESSAGE + "\" not found");
    }
    currentElement = dataMsgElement;     //set current element
         //fetch and save action string for message
    action = getOptAttribStr(MsgTag.ACTION);
         //fetch and save time message received at server:
    timeReceived = getOptAttribTime(MsgTag.TIME_RECEIVED);
         //fetch and save relay-time-generated value:
    relayTimeGen = getOptAttribTime(MsgTag.RELAY_TIME_GEN);
  }

  /**
   * ***Not implemented.***
   * Creates a record of data from the archived-string data for an XML
   * QWmessage.  This constructor is needed to have this class implement
   * the 'Archivable' interface.
   * @param dataStr the archived-string data to use.
   * @param mkrObj Archivable.Marker parameter to indicate that this
   * constructor is used to dearchive a record.
   * @throws QWRecordException if an error occurs while creating
   * the data record.
   */
  public QWDataMsgRecord(String dataStr,Archivable.Marker mkrObj)
                                                    throws QWRecordException
  {
    super(dataStr,mkrObj);                 //construct parent
    throw new QWRecordException("Archivable constructor not implemented");

//    currentElement = dataMsgElement;     //set current element
//         //fetch and save action string for message
//    action = getAttribStr(MsgTag.ACTION);
//         //fetch and save time message received at server:
//    timeReceived = getOptAttribTime(MsgTag.TIME_RECEIVED);
  }

  /**
   * Creates a record of data from existing data items.
   * @param qwMsgElement the XML "QWmessage" element object.
   * @param dataMsgElement the XML "DataMessage" element object.
   * @param msgNumber message number from the 'QWmessage' element for
   * this message.
   * @param timeGenerated time that message was created by server (or
   * null if not given).
   * @param action "Action" string for message ("Update", "Delete").
   * @param timeReceived time that message was received at server (or
   * null if not given).
   * @throws QWRecordException if an error occurs while creating
   * the data record.
   */
  public QWDataMsgRecord(Element qwMsgElement,Element dataMsgElement,
          long msgNumber,Date timeGenerated,String action,Date timeReceived)
                                                    throws QWRecordException
  {
    super(qwMsgElement,msgNumber,timeGenerated);      //construct parent
    this.dataMsgElement = dataMsgElement;        //save handle to element
    this.action = action;
    this.timeReceived = timeReceived;
    relayTimeGen = null;
  }

  /**
   * Indicates whether some other object is "equal to" this one.
   *
   * @param   obj   the reference object with which to compare.
   * @return  <code>true</code> if this object is the same as the obj
   *          argument; <code>false</code> otherwise.
   */
  public boolean equals(QWDataMsgRecord obj)
  {
    return super.equals(obj) &&
        ((action == null) ? obj.action == null : action.equals(obj.action));
    //ignore time received
    //(timeReceived == null ? obj.timeReceived == null : timeReceived.equals(obj.timeReceived));
  }

  /**
   * Indicates whether some other object is "equal to" this one.
   *
   * @param   obj   the reference object with which to compare.
   * @return  <code>true</code> if this object is the same as the obj
   *          argument; <code>false</code> otherwise.
   */
  public boolean equals(Object obj)
  {
    return obj instanceof QWDataMsgRecord && equals((QWDataMsgRecord)obj);
  }

  /**
   * Fetches the "Identifier" element under the given search element.
   * @param searchElement the element to search.
   * @param searchElemNameStr the display name of the search element
   * (for error messages).
   * @return The "Identifier" element object.
   * @throws QWRecordException if something other than a single
   * "Identifier" element if found.
   */
  protected Element getIdentifierElement(Element searchElement,
                          String searchElemNameStr) throws QWRecordException
  {
         //get list of "Identifier" elements under the search element:
    final List listObj = searchElement.getChildren(MsgTag.IDENTIFIER);
    if(listObj == null || listObj.size() <= 0)
    {    //no "Identifier" elements found
      throw new QWRecordException("No \"" + MsgTag.IDENTIFIER +
              "\" element found for \"" + searchElemNameStr + "\" element");
    }
    if(listObj.size() > 1)
    {    //more than one "Identifier" element found
      throw new QWRecordException("More than one \"" + MsgTag.IDENTIFIER +
              "\" element found for \"" + searchElemNameStr + "\" element");
    }
    final Element elementObj;
    final Object obj;   //set current-element handle to "Identifier" element:
    if(!((obj=listObj.get(0)) instanceof Element) ||
                                       !(MsgTag.IDENTIFIER.equalsIgnoreCase(
                                      (elementObj=(Element)obj).getName())))
    {    //element is not an "Identifier" element (shouldn't happen)
      throw new QWRecordException("Error fetching \"" + MsgTag.IDENTIFIER +
                    "\" element for \"" + searchElemNameStr + "\" element");
    }
    return elementObj;       //return "Identifier" element
  }

  /**
   * Sets the "requested" flag.
   * @param flgVal the flag value to set.
   */
  public void setRequestedFlag(boolean flgVal)
  {
    requestedFlag = flgVal;
  }

  /**
   * Returns the "requested" flag.
   * @return the value of the flag.
   */
  public boolean getRequestedFlag()
  {
    return requestedFlag;
  }

  /**
   * Sets the 'update' flag to indicate whether or not this record is an
   * update to a previously existing message record.
   * @param flgVal true if this record is an update, false if it is a new
   * message record.
   */
  public void setUpdateFlag(boolean flgVal)
  {
    updateFlag = flgVal;
  }

  /**
   * Returns true if this record is an update to a previously existing
   * message record, false if it is a new message record.
   * @return true if this record is an update.
   */
  public boolean getUpdateFlag()
  {
    return updateFlag;
  }

  /**
   * Sets the 'alarmTriggered' flag to indicate whether or not an alarm
   * has been triggered for this record.
   * @param flgVal true if an alarm has been triggered for this record,
   * false if not.
   */
  public void setAlarmTriggeredFlag(boolean flgVal)
  {
    alarmTriggeredFlag = flgVal;
  }

  /**
   * Returns the 'alarmTriggered' flag to indicate whether or not an alarm
   * has been triggered for this record.
   * @return true if an alarm has been triggered for this record,
   * false if not.
   */
  public boolean getAlarmTriggeredFlag()
  {
    return alarmTriggeredFlag;
  }

  /**
   * Sets the 'showMapSymbolFlag' flag to indicate whether or not the
   * symbol for this message should be shown on the map.  If this
   * method is not called then then 'showMapSymbolFlag' flag will
   * default to 'true' (symbol shown on map).
   * @param flgVal true if the symbol for this message should be shown
   * on the map; false if not.
   */
  public void setShowMapSymbolFlag(boolean flgVal)
  {
    showMapSymbolFlag = flgVal;
  }

  /**
   * Returns the 'showMapSymbolFlag' flag to indicate whether or not the
   * symbol for this message should be shown on the map.
   * @return true if the symbol for this message should be shown
   * on the map; false if not.
   */
  public boolean getShowMapSymbolFlag()
  {
    return showMapSymbolFlag;
  }

  /**
   * Sets the "Location Note" value for this message record.
   * @param str the "Location Note" value to use.
   */
  public void setLocationNote(String str)
  {
    locationNote = str;
  }

  /**
   * Returns the "Location Note" value for this message record.
   * @return A string containing the "Location Note" value.
   */
  public String getLocationNote()
  {
    return locationNote;
  }

  /**
   * Sets the display object associated with this message record.
   * @param obj the object to use.
   */
  public void setDisplayObject(Object obj)
  {
    displayObject = obj;
  }

  /**
   * Returns the display object associated with this message record.
   * @return The display object.
   */
  public Object getDisplayObject()
  {
    return displayObject;
  }

  /**
   * Sets the utility object associated with this event record.
   * @param obj the object to use.
   */
  public void setUtilityObject(Object obj)
  {
    utilityObject = obj;
  }

  /**
   * Returns the utility object associated with this event record.
   * @return The utility object.
   */
  public Object getUtilityObject()
  {
    return utilityObject;
  }
}
