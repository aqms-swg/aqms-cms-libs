//QWDelEventMsgRecord.java:  Defines a record of data for one event that is
//                           to be deleted.
//
//  9/20/2002 -- [ET]  Initial version.
// 10/24/2003 -- [ET]  Modified (via import) to reference 'QWCommon'
//                     version of  'QWRecordException' instead of
//                     local version.
// 12/22/2003 -- [ET]  Added support for a table of products.
//  3/18/2004 -- [ET]  Moved from 'QWClient' to 'QWCommon' project.
//  5/16/2005 -- [ET]  Changed 'QWEventMsgRecord' references to
//                     'QWEQeventMsgRecord'.
//   9/8/2006 -- [ET]  Added support for for ANSS-EQ-XML format; removed
//                     'createQWDelEventMsgRecord()' method and related
//                     constructor.
//  9/29/2006 -- [ET]  Added 'getCommentStr()' method.
//  3/31/2010 -- [ET]  Added support for QuakeML format.
//

package com.isti.quakewatch.message;

import org.jdom.Element;
import com.isti.util.UtilFns;
import com.isti.util.FifoHashtable;
import com.isti.quakewatch.common.MsgTag;

/**
 * QWDelEventMsgRecord defines a record of data for one event that is to be
 * deleted.
 */
public class QWDelEventMsgRecord extends QWIdentDataMsgRecord
{
    /** Handle to 'Element' object used to construct this object. */
  public final Element eventElement;
    /** Message type string set by QWServer. */
  public final String type;
    /** Message type code string received from feeder module. */
  public final String msgTypeCode;
    /** Table of 'QWProductMsgRecord' objects for event (or null). */
  protected FifoHashtable productRecTable = null;

  /**
   * Creates a data record for one event that is to be deleted, built
   * from an XML "Event" message element.
   * @param qwMsgElement the XML "QWmessage" element object.
   * @param dataMsgElement the XML "DataMessage" element object.
   * @param eventElement the XML "Event" message element.
   * @param messageFormatSpec specifier for message-format type (one of
   * the 'QWIdentDataMsgRecord.MFMT_...' values).
   * @throws QWRecordException if an error occurs while creating
   * the data record.
   */
  public QWDelEventMsgRecord(Element qwMsgElement,Element dataMsgElement,
                                Element eventElement, int messageFormatSpec)
                                                    throws QWRecordException
  {                                                   //construct parents:
    super(qwMsgElement,dataMsgElement,eventElement,messageFormatSpec);
    this.eventElement = eventElement;       //save handle to element

    switch(messageFormatSpec)
    {  //process data depending on format specification
      case MFMT_QUAKEML:     //data is QuakeML message format
        if(!MsgTag.USE_CONTROL.equalsIgnoreCase(eventElement.getName()))
        {  //root element name not "useControl"
          throw new QWRecordException("Element tag name \"" +
                                       MsgTag.USE_CONTROL + "\" not found");
        }
              //get optional "type" child-element of "useControl" element:
        type = getOptElementStr(MsgTag.QUAKEML_TYPE);
              //set no "MsgTypeCode" value:
        msgTypeCode = UtilFns.EMPTY_STRING;
        break;

      case MFMT_ANSSEQXML:   //data is ANSS-EQ-XML message format
        if(!MsgTag.EVENT.equalsIgnoreCase(eventElement.getName()))
        {  //root element name not "Event"
          throw new QWRecordException("Element tag name \"" +
                                             MsgTag.EVENT + "\" not found");
        }
              //get optional "Type" child-element of "Event" element:
        type = getOptElementStr(MsgTag.TYPE);
              //set no "MsgTypeCode" value:
        msgTypeCode = UtilFns.EMPTY_STRING;
        break;

      default:               //data is QuakeWatch message format
        if(!MsgTag.EVENT.equalsIgnoreCase(eventElement.getName()))
        {  //root element name not "Event"
          throw new QWRecordException("Element tag name \"" +
                                             MsgTag.EVENT + "\" not found");
        }
              //get "Type" attribute:
        type = getAttribStr(MsgTag.TYPE);
              //get optional "MsgTypeCode" attribute:
        msgTypeCode = getNonNullOptAttribStr(MsgTag.MSG_TYPE_CODE);
    }
  }

  /**
   * Creates a data record for one event that is to be deleted, built
   * from an XML "Event" message element.
   * @param qwMsgElement the XML "QWmessage" element object.
   * @param dataMsgElement the XML "DataMessage" element object.
   * @param eventElement the XML "Event" message element.
   * @param anssEQMsgFormatFlag true for ANSS-EQ-XML message format;
   * false for QuakeWatch message format.
   * @throws QWRecordException if an error occurs while creating
   * the data record.
   * @deprecated Use version with 'messageFormatSpec' parameter.
   */
  public QWDelEventMsgRecord(Element qwMsgElement,Element dataMsgElement,
                          Element eventElement, boolean anssEQMsgFormatFlag)
                                                    throws QWRecordException
  {                                                   //construct parents:
    this(qwMsgElement,dataMsgElement,eventElement,
                   (anssEQMsgFormatFlag ? MFMT_ANSSEQXML : MFMT_QWMESSAGE));
  }

  /**
   * Creates a data record for one event that is to be deleted, built
   * from a QuakeWatch format XML "Event" message element.
   * @param qwMsgElement the XML "QWmessage" element object.
   * @param dataMsgElement the XML "DataMessage" element object.
   * @param eventElement the XML "Event" message element.
   * @throws QWRecordException if an error occurs while creating
   * the data record.
   */
  public QWDelEventMsgRecord(Element qwMsgElement,Element dataMsgElement,
                              Element eventElement) throws QWRecordException
  {
    this(qwMsgElement,dataMsgElement,eventElement,MFMT_QWMESSAGE);
  }

  /**
   * Creates a data record for one event that is to be deleted, built
   * from an existing event message record object.
   * @param recObj the event message record object to use.
   * @throws QWRecordException if an error occurs while creating
   * the data record.
   */
//  public QWDelEventMsgRecord(QWEQEventMsgRecord recObj)
//                                                    throws QWRecordException
//  {
//         //construct parents using items from event message record object:
//    super(recObj.qwMsgElement,recObj.dataMsgElement,recObj.eventElement,
//                        recObj.msgNumber,recObj.timeGenerated,recObj.action,
//                                      recObj.timeReceived,recObj.eventIDKey,
//                                          recObj.dataSource,recObj.version);
//    eventElement = recObj.eventElement;
//    type = recObj.type;
//    msgTypeCode = "";
//  }

  /**
   * Indicates whether some other object is "equal to" this one.
   *
   * @param   obj   the reference object with which to compare.
   * @return  <code>true</code> if this object is the same as the obj
   *          argument; <code>false</code> otherwise.
   */
  public boolean equals(QWDelEventMsgRecord obj)
  {
    return super.equals(obj) &&
              ((type == null) ? obj.type == null : type.equals(obj.type)) &&
                          ((msgTypeCode == null) ? obj.msgTypeCode == null :
                                       msgTypeCode.equals(obj.msgTypeCode));
  }

  /**
   * Indicates whether some other object is "equal to" this one.
   *
   * @param   obj   the reference object with which to compare.
   * @return  <code>true</code> if this object is the same as the obj
   *          argument; <code>false</code> otherwise.
   */
  public boolean equals(Object obj)
  {
    return obj instanceof QWDelEventMsgRecord && equals((QWDelEventMsgRecord)obj);
  }

  /**
   * Returns a string representation of this class.
   * @return String object.
   */
  public String toString()
  {
    return "eventIDKey=" + this.eventIDKey +
        ", type=" + this.type +
        ", eventElement=" + this.eventElement;
  }

  /**
   * Enters a new table of 'QWProductMsgRecord' objects for products
   * associated with the event to be deleted.
   * @param tableObj the new table to use.
   */
  public void setProductRecTable(FifoHashtable tableObj)
  {
    productRecTable = tableObj;
  }

  /**
   * Returns the table of 'QWProductMsgRecord' objects for products
   * associated with the event to be deleted.
   * @return A FifoHashtable of 'QWProductMsgRecord' objects, or null
   * if none are available.
   */
  public FifoHashtable getProductRecTable()
  {
    return productRecTable;
  }

  /**
   * Returns a 'QWProductMsgRecord' object for the event to be deleted.
   * @param typeStr the 'type' value of the 'QWProductMsgRecord' object
   * to be returned.
   * @return A 'QWProductMsgRecord' object, or null if a matching object
   * is not found.
   */
  public QWProductMsgRecord getProductMsgRecord(String typeStr)
  {
    Object obj;
    if(productRecTable != null &&
           (obj=productRecTable.get(typeStr)) instanceof QWProductMsgRecord)
    {    //table exists and matching product was found
      return (QWProductMsgRecord)obj;       //return product object
    }
    return null;
  }

  /**
   * Returns the number of products associated with the event to be deleted.
   * @return The number of products associated with the event to be deleted.
   */
  public int getProductCount()
  {
    return (productRecTable != null) ? productRecTable.size() : 0;
  }

  /**
   * Returns the comment text from this message.
   * @return The comment text string from this message, or null if none
   * available.
   */
  public String getCommentStr()
  {
    switch(messageFormatSpec)
    {  //process data depending on format specification
      case MFMT_QUAKEML:     //data is QuakeML message format
        return getChildElementStr(     //return value of 'comment'|'text'
                                MsgTag.QUAKEML_COMMENT,MsgTag.QUAKEML_TEXT);

      case MFMT_ANSSEQXML:   //data is ANSS-EQ-XML message format
        final Element commentElem;
        if((commentElem=currentElement.getChild(
                     MsgTag.COMMENT,currentElement.getNamespace())) == null)
        {  //no "Comment" child-element found
          return null;
        }
        return commentElem.getChildTextTrim(
                                    MsgTag.TEXT,commentElem.getNamespace());

      default:               //data is QuakeWatch message format
        return getOptAttribStr(MsgTag.COMMENT);
    }
  }

  /**
   * Factory method that creates a data record for one event that is to
   * be deleted, built from an existing event message record object.
   * @param recObj the event message record object to use.
   * @return A new 'QWDelEventMsgRecord' object, or null if an error
   * occurred while creating the data record.
   */
//  public static QWDelEventMsgRecord createQWDelEventMsgRecord(
//                                                  QWEQEventMsgRecord recObj)
//  {
//    try
//    {
//      return new QWDelEventMsgRecord(recObj);
//    }
//    catch(QWRecordException ex) {}
//    return null;
//  }
}
