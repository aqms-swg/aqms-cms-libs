//QWDelProductMsgRecord.java:  Defines a record of data for one
//                             event-product that is to be deleted.
//
//  9/13/2002 -- [ET]  Initial version.
//  9/19/2003 -- [ET]  Added 'value' field.
// 10/24/2003 -- [ET]  Modified (via import) to reference 'QWCommon'
//                     version of  'QWRecordException' instead of
//                     local version.
//  3/18/2004 -- [ET]  Moved from 'QWClient' to 'QWCommon' project.
//  9/27/2006 -- [ET]  Added support for for ANSS-EQ-XML format; added
//                     'prodTypeCode' field; added 'isSameProductType()'
//                     method.
//  10/2/2006 -- [ET]  Modified 'msgTypeCode' to be "TX" for Comment
//                     messages and "LI" for ProductLink messages
//                     (ANSS-EQ-XML format); added 'commentElemFlag'
//                     parameter to constructor.
//   4/1/2010 -- [ET]  Added support for QuakeML format.
//  6/19/2013 -- [ET]  Modified to explicit set product-type value
//                     to "ShakeMapURL" if product determined to be
//                     ShakeMap product.
//

package com.isti.quakewatch.message;

import java.util.Iterator;
import org.jdom.Element;
import com.isti.quakewatch.common.MsgTag;
import com.isti.quakewatch.util.ResourceIdentifier;
import com.isti.util.UtilFns;

/**
 * QWDelProductMsgRecord defines a record of data for one event-product
 * that is to be deleted.
 */
public class QWDelProductMsgRecord extends QWIdentDataMsgRecord
{
    /** Handle to 'Element' object used to construct this object. */
  public final Element productElement;
    /** Message type string set by QWServer. */
  public final String type;
    /** Message type code string received from feeder module. */
  public final String msgTypeCode;
    /** Product type code string received from feeder module. */
  public final String prodTypeCode;
    /** Value string for product, or null if none. */
  public final String value;

  /**
   * Creates a data record for one event product that is to be deleted,
   * built from an XML "Product" message element.
   * @param qwMsgElement the XML "QWmessage" element object.
   * @param dataMsgElement the XML "DataMessage" element object.
   * @param eventElement the XML "Event" element object if ANSS-EQ-XML
   * message format, or null if QuakeWatch message format.
   * @param productElement the "ProductLink" message element if ANSS-EQ-XML
   * message format, or the "Product" message element (containing the
   * "Identifier") if QuakeWatch message format.
   * @param messageFormatSpec specifier for message-format type (one of
   * the 'QWIdentDataMsgRecord.MFMT_...' values).
   * @param commentElemFlag true if Comment element in use in ANSS-EQ-XML
   * message; false if ProductLink element in use in ANSS-EQ-XML message
   * (or if QuakeWatch message).
   * @throws QWRecordException if an error occurs while creating
   * the data record.
   */
  public QWDelProductMsgRecord(Element qwMsgElement, Element dataMsgElement,
                               Element eventElement, Element productElement,
                             int messageFormatSpec, boolean commentElemFlag)
                                                    throws QWRecordException
  {
         //call 'QWIdentDataMsgRecord' constructor with 'Event' element
         // and 'Version' from 'ProductLink' element (if ANSS-EQ-XML),
         // or 'Product' element (if other format):
    super(qwMsgElement,dataMsgElement,
                                    ((messageFormatSpec == MFMT_ANSSEQXML) ?
                                             eventElement : productElement),
                                                          messageFormatSpec,
       ((messageFormatSpec == MFMT_ANSSEQXML) ? productElement.getChildText(
                     MsgTag.VERSION,productElement.getNamespace()) : null));
    this.productElement = productElement;   //save handle to element

    String parsedTypeStr,parsedCodeStr;
    switch(messageFormatSpec)
    {  //process data depending on format specification
      case MFMT_QUAKEML:     //data is QuakeML message format
              //setup to get data-fields from "productLink" element:
        currentElement = productElement;
              //get "code" and "value" strings from 'comment' attributes:
        final Iterator iterObj = currentElement.getChildren(
           MsgTag.QUAKEML_COMMENT,currentElement.getNamespace()).iterator();
        Object obj;
        Element elemObj;
        ResourceIdentifier resIdObj;
        parsedCodeStr = UtilFns.EMPTY_STRING;
        String valStr = null;
        while(iterObj.hasNext())
        {  //for each item in list
          if((obj=iterObj.next()) instanceof Element)
          {  //'comment' element fetched OK
            elemObj = (Element)obj;
                   //get 'id' attribute and parse as resource identifier:
            resIdObj = new ResourceIdentifier(
                                  elemObj.getAttributeValue(MsgTag.ID_STR));
            if(MsgTag.QUAKEML_PRODUCT_LINK.equalsIgnoreCase(
                                               resIdObj.getResourceIDStr()))
            {  //'id' attribute resource-id is "productLink"
              if(MsgTag.QUAKEML_CODE.equalsIgnoreCase(
                                                  resIdObj.getLocalIDStr()))
              {  //'id' attribute local-id is "code"; get 'text' string
                if((parsedCodeStr=elemObj.getChildText(
                       MsgTag.QUAKEML_TEXT,elemObj.getNamespace())) == null)
                {  //no 'text' child-element found
                  parsedCodeStr = UtilFns.EMPTY_STRING;    //don't allow null
                }
              }
              else if(MsgTag.QUAKEML_LINK.equalsIgnoreCase(
                                                  resIdObj.getLocalIDStr()))
              {  //'id' attribute local-id is "link"; get 'text' string
                valStr = elemObj.getChildText(
                                MsgTag.QUAKEML_TEXT,elemObj.getNamespace());
              }
            }
          }
        }
        value = valStr;           //set "value" string (if any)
        if(commentElemFlag)
        {  //delete-comment message element
          parsedTypeStr = MsgTag.TEXT_COMMENT;   //set type to "TextComment"
          msgTypeCode = MsgTag.TEXT_MSGSTR;      //set message-code to "TX"
        }
        else
        {  //delete-product element message
                        //parse 'elementID' value as resource identifier:
          resIdObj = new ResourceIdentifier(
                       currentElement.getAttributeValue(MsgTag.ELEMENT_ID));
                        //use "code" part of local-id as 'type' value:
          parsedTypeStr = resIdObj.getProductCodeStr();
          msgTypeCode = MsgTag.LINK_MSGSTR;      //set message-code to "LI"
        }
        break;

      case MFMT_ANSSEQXML:   //data is ANSS-EQ-XML message format
              //setup to get data-fields from "ProductLink" element:
        currentElement = productElement;
              //get "Code" child-element for "ProductLink" element:
        parsedCodeStr = getNonNullOptElementStr(MsgTag.CODE);
              //get "TypeKey" child-element of "ProductLink" element:
        parsedTypeStr = getOptElementStr(MsgTag.TYPE_KEY);
        if(commentElemFlag)
        {  //Comment element
          if(!MsgTag.COMMENT.equalsIgnoreCase(productElement.getName()))
          {     //element name not "Comment"
            throw new QWRecordException("Element tag name \"" +
                                           MsgTag.COMMENT + "\" not found");
          }
                   //set message-code to "TX":
          msgTypeCode = MsgTag.TEXT_MSGSTR;
                   //set 'value' to optional Text child-element:
          value = getOptElementStr(MsgTag.TEXT);
        }
        else
        {  //ProductLink element
          if(!MsgTag.PRODUCT_LINK.equalsIgnoreCase(productElement.getName()))
          {     //element name not "ProductLink"
            throw new QWRecordException("Element tag name \"" +
                                      MsgTag.PRODUCT_LINK + "\" not found");
          }
                   //set message-code to "LI":
          msgTypeCode = MsgTag.LINK_MSGSTR;
                   //set 'value' to Link child-element:
          value = getOptElementStr(MsgTag.LINK);
        }
        break;

      default:               //data is QuakeWatch message format
        if(!MsgTag.PRODUCT.equalsIgnoreCase(productElement.getName()))
        {  //element name not "Product"
          throw new QWRecordException("Element tag name \"" +
                                           MsgTag.PRODUCT + "\" not found");
        }
              //get "Type" attribute:
        parsedTypeStr = getAttribStr(MsgTag.TYPE);
              //get optional "MsgTypeCode" attribute:
        msgTypeCode = getNonNullOptAttribStr(MsgTag.MSG_TYPE_CODE);
              //get optional "ProdTypeCode" attribute:
        parsedCodeStr = getNonNullOptAttribStr(MsgTag.PROD_TYPE_CODE);
              //get Value attribute for delete-product element:
        value = getOptAttribStr(MsgTag.VALUE);
    }
    if(!MsgTag.SHAKEMAP_URL.equals(parsedTypeStr))
    {  //message type is not "ShakeMapURL"
              //if determined to be ShakeMap product then
              // set explicit product-type value:
      if(isShakeMapProduct(parsedTypeStr,parsedCodeStr,value))
        parsedTypeStr = MsgTag.SHAKEMAP_URL;
      else if(MsgTag.LINK_URL.equals(parsedTypeStr) &&
                                          parsedCodeStr.trim().length() > 0)
      {  //message type is "LinkURL" and "Code" value exists
        parsedTypeStr += '(' + parsedCodeStr + ')';   //append "Code" value
      }
    }
    type = parsedTypeStr;
    prodTypeCode = parsedCodeStr;
  }

  /**
   * Creates a data record for one event product that is to be deleted,
   * built from an XML "Product" message element.
   * @param qwMsgElement the XML "QWmessage" element object.
   * @param dataMsgElement the XML "DataMessage" element object.
   * @param eventElement the XML "Event" element object if ANSS-EQ-XML
   * message format, or null if QuakeWatch message format.
   * @param productElement the "ProductLink" message element if ANSS-EQ-XML
   * message format, or the "Product" message element (containing the
   * "Identifier") if QuakeWatch message format.
   * @param anssEQMsgFormatFlag true for ANSS-EQ-XML message format;
   * false for QuakeWatch message format.
   * @param commentElemFlag true if Comment element in use in ANSS-EQ-XML
   * message; false if ProductLink element in use in ANSS-EQ-XML message
   * (or if QuakeWatch message).
   * @throws QWRecordException if an error occurs while creating
   * the data record.
   * @deprecated Use version with 'messageFormatSpec' parameter.
   */
  public QWDelProductMsgRecord(Element qwMsgElement, Element dataMsgElement,
                               Element eventElement, Element productElement,
                       boolean anssEQMsgFormatFlag, boolean commentElemFlag)
                                                    throws QWRecordException
  {
    this(qwMsgElement,dataMsgElement,eventElement,productElement,
                    (anssEQMsgFormatFlag ? MFMT_ANSSEQXML : MFMT_QWMESSAGE),
                                                           commentElemFlag);
  }

//  /**
//   * Creates a data record for one event product that is to be deleted,
//   * built from a QuakeWatch format XML "Product" message element.
//   * @param qwMsgElement the XML "QWmessage" element object.
//   * @param dataMsgElement the XML "DataMessage" element object.
//   * @param productElement the XML "Product" message element.
//   * @throws QWRecordException if an error occurs while creating
//   * the data record.
//   */
//  public QWDelProductMsgRecord(Element qwMsgElement,Element dataMsgElement,
//                            Element productElement) throws QWRecordException
//  {
//    this(qwMsgElement,dataMsgElement,null,productElement,MFMT_QWMESSAGE,false);
//  }

  /**
   * Indicates whether some other object is "equal to" this one.
   * @param   obj   the reference object with which to compare.
   * @return  <code>true</code> if this object is the same as the obj
   *          argument; <code>false</code> otherwise.
   */
  public boolean equals(QWDelProductMsgRecord obj)
  {
    return super.equals(obj) &&
              ((type == null) ? obj.type == null : type.equals(obj.type)) &&
                          ((msgTypeCode == null) ? obj.msgTypeCode == null :
                                     msgTypeCode.equals(obj.msgTypeCode)) &&
            ((value == null) ? obj.value == null : value.equals(obj.value));
  }

  /**
   * Indicates whether some other object is "equal to" this one.
   * @param   obj   the reference object with which to compare.
   * @return  <code>true</code> if this object is the same as the obj
   *          argument; <code>false</code> otherwise.
   */
  public boolean equals(Object obj)
  {
    return obj instanceof QWDelProductMsgRecord && equals((QWDelProductMsgRecord)obj);
  }

  /**
   * Indicates if the product type of the given record is equal to the
   * product type for this record.  The 'prodTypeCode' values are also
   * compared.
   * @param recObj QWProductMsgRecord given record to compare.
   * @return true if the product type of the given record is equal to
   * the product type for this record; false if not.
   */
  public boolean isSameProductType(QWProductMsgRecord recObj)
  {
    if(recObj == null)       //if given record is null then
      return false;          //indicate no match
    if(type != null)
    {    //this record's type is not null
      if(recObj.type == null || type.equalsIgnoreCase(recObj.type))
        return false;   //if other type is null or not equal then indicate
    }
    else if(recObj.type != null)
      return false;     //if this type null and other not then indicate
         //record types match
    if(prodTypeCode != null)
    {    //this record's prodTypeCode is not null
      if(recObj.prodTypeCode == null ||
                         prodTypeCode.equalsIgnoreCase(recObj.prodTypeCode))
      {  //other prodTypeCode is null or not equal
        return false;
      }
    }
    else if(recObj.prodTypeCode != null)
      return false;     //if this prodTypeCode null and other not then ind
         //record prodTypeCodes match
    return true;
  }

  /**
   * Returns a string representation of this class.
   * @return String object.
   */
  public String toString()
  {
    return "eventIDKey=" + this.eventIDKey +
        ", type=" + this.type +
        ", msgTypeCode=" + this.msgTypeCode +
        ", value=" + this.value;
  }
}
