//QWDynMsgNumTimeRec.java:  Defines a dynamic message record holding
//                          message-record and time-generated values.
//
//  12/1/2003 -- [ET]  Initial version.
// 12/31/2003 -- [ET]  Added 'incMsgNumAndSetTime()' method.
//

package com.isti.quakewatch.message;

/**
 * Class QWDynMsgNumTimeRec defines a dynamic message record
 * holding message-record and time-generated values.
 */
public class QWDynMsgNumTimeRec implements QWMsgNumTimeRec
{
  private long msgNumVal = 0;
  private long timeGeneratedVal = 0;
  private final Object msgRecordSyncObject;

  /**
   * Creates a dynamic message record holding message-record and
   * time-generated values.
   * @param msgNum the message number to use.
   * @param timeGenerated the time-generated value to use (ms since
   * 1/1/1970).
   * @param msgRecordSyncObject the thread-synchronization object to use,
   * or null to create a new one.
   */
  public QWDynMsgNumTimeRec(long msgNum,long timeGenerated,
                                                 Object msgRecordSyncObject)
  {
    msgNumVal = msgNum;
    timeGeneratedVal = timeGenerated;
                   //save thread-sync object or create new one:
    this.msgRecordSyncObject = (msgRecordSyncObject != null) ?
                                       msgRecordSyncObject : (new Object());
  }

  /**
   * Creates a dynamic message record holding message-record and
   * time-generated values.
   * @param msgNum the message number to use.
   * @param timeGenerated the time-generated value to use (ms since
   * 1/1/1970).
   */
  public QWDynMsgNumTimeRec(long msgNum,long timeGenerated)
  {
    this(msgNum,timeGenerated,null);
  }

  /**
   * Creates a dynamic message record holding message-record and
   * time-generated values.
   * @param msgRecordSyncObject the thread-synchronization object to use,
   * or null to create a new one.
   */
  public QWDynMsgNumTimeRec(Object msgRecordSyncObject)
  {
    this(0,0,msgRecordSyncObject);
  }

  /**
   * Creates a dynamic message record holding message-record and
   * time-generated values.
   */
  public QWDynMsgNumTimeRec()
  {
    this(0,0,null);
  }

  /**
   * Enters the given message-number and time-generated values.
   * @param msgNum the message number to use.
   * @param timeGenerated the time-generated value to use (ms since
   * 1/1/1970).
   */
  public void setMsgNumAndTime(long msgNum,long timeGenerated)
  {
    synchronized(msgRecordSyncObject)
    {     //grab thread-synchronization lock for object
      msgNumVal = msgNum;
      timeGeneratedVal = timeGenerated;
    }
  }

  /**
   * Increments the current message number and sets the time-generated
   * value.
   * @param timeGenerated the time-generated value to use (ms since
   * 1/1/1970).
   */
  public void incMsgNumAndSetTime(long timeGenerated)
  {
    synchronized(msgRecordSyncObject)
    {     //grab thread-synchronization lock for object
      ++msgNumVal;
      timeGeneratedVal = timeGenerated;
    }
  }

  /**
   * Returns the message number value.  This method is needed to
   * implemented the 'QWMsgNumTimeRec' interface.
   * @return The message number value.
   */
  public long getMsgNum()
  {
    synchronized(msgRecordSyncObject)
    {     //grab thread-synchronization lock for object
      return msgNumVal;
    }
  }

  /**
   * Returns the time-generated value.  This method is needed to
   * implemented the 'QWMsgNumTimeRec' interface.
   * @return The time-generated value, in milliseconds since 1/1/1970.
   */
  public long getTimeGenerated()
  {
    synchronized(msgRecordSyncObject)
    {     //grab thread-synchronization lock for object
      return timeGeneratedVal;
    }
  }

  /**
   * Returns the thread-synchronization object in use.
   * @return The thread-synchronization object in use.
   */
  public Object getMsgRecordSyncObject()
  {
    return msgRecordSyncObject;
  }

  /**
   * Returns the current message-number and time-generated values.
   * @param recObj a message record to compare message-number and time-
   * generated values against (same object returned if equal), or null
   * for none.
   * @return A 'QWStaticMsgNumTimeRec' object containing the current
   * message-number and time-generated values.
   */
  public QWStaticMsgNumTimeRec getMsgNumAndTime(QWStaticMsgNumTimeRec recObj)
  {
    synchronized(msgRecordSyncObject)
    {     //grab thread-synchronization lock for object
      if(recObj != null && recObj.getMsgNum() == msgNumVal &&
                              recObj.getTimeGenerated() == timeGeneratedVal)
      {
        return recObj;
      }
      return new QWStaticMsgNumTimeRec(msgNumVal,timeGeneratedVal);
    }
  }

  /**
   * Returns the current message-number and time-generated values.
   * @return A 'QWStaticMsgNumTimeRec' object containing the current
   * message-number and time-generated values.
   */
  public QWStaticMsgNumTimeRec getMsgNumAndTime()
  {
    return getMsgNumAndTime(null);
  }
}
