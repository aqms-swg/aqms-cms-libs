//QWEQEventMsgRecord.java:  Defines a record of data for one earthquake
//                          event.
//
// 10/23/2002 -- [ET]
//  6/19/2003 -- [KF]  Added Trump event message-type.
//  8/15/2003 -- [ET]  Added 'addToProductRecTable()' method.
//  8/19/2003 -- [ET]  Added 'quarry' flag and support.
// 10/24/2003 -- [ET]  Modified (via import) to reference 'QWCommon'
//                     version of  'QWRecordException' instead of
//                     local version.
// 11/17/2003 -- [ET]  Added 'set/getAlarmTriggeredFlag()' methods.
//  2/16/2004 -- [ET]  Modified to make usage of date and number-format
//                     objects thread safe.
//  3/18/2004 -- [ET]  Moved from 'QWClient' to 'QWCommon' project.
//   5/7/2004 -- [KF]  Added 'getShakeMapProductMsgRecord()' method.
//   5/7/2004 -- [ET]  Added 'magnitudeType' field.
//  5/10/2004 -- [ET]  Added options to 'getLat/LonString()' and
//                     'getDepthString()' methods; added thread
//                     sychronization to 'updateTimeZones()' method.
//  5/11/2004 -- [ET]  Added 'get...DispStr()' methods.
//  5/12/2004 -- [ET]  Modified 'getAzimuthalGapDispStr()' to read
//                     floating point and convert to integer value.
//  5/18/2004 -- [ET]  Fixed 'getLongitudeString(boolean ewFormatFlag)'
//                     method to not call itself; fixed method
//                     'getAzimuthalGapDispStr()'.
//  5/21/2004 -- [ET]  Added 'getVerticalErrorValObj()',
//                     'getHorizontalErrorValObj()' and
//                     'buildDistanceDispStr()' methods;
//                     added optional 'zeroValStr' parameter
//                     to 'get...DispStr()' methods.
//   7/2/2004 -- [ET]  Changed reference to QDM 'util' source files to
//                     updated external 'qdmutil' library.
//  5/19/2005 -- [ET]  Renamed from 'QWEventMsgRecord' to
//                     'QWEQEventMsgRecord' and modified to
//                     implement 'QWEventMsgRecord' interface;
//                     moved 'getEventID()' and 'getEventIDKey()'
//                     methods to 'QWIdentDataMsgRecord' class; moved
//                     "get/setUpdateFlag()", "get/setAlarmTriggeredFlag()",
//                     "get/setLocationNote()" and "get/setDisplayObject()"
//                     methods to 'QWDataMsgRecord' class.
//   6/2/2005 -- [ET]  Added 'get/setSupersededRecordObj()' methods.
//   6/6/2005 -- [KF]  Added 'getNumStationsObj()' method.
//   8/5/2005 -- [ET]  Added methods 'containsShakeMapProduct()',
//                     'containsTsunamiProduct()' and
//                     'matchProductMsgRecType()'.
//  12/7/2005 -- [KF]  Added support for 'ExtendedComparable'.
// 12/23/2005 -- [KF]  Moved 'createFloatNumberFormat' methods to "UtilFns",
//                     Added change listener for default locale.
//   6/7/2006 -- [ET]  Moved "..._SORT_TYPE" definitions from this class
//                     to 'QWEventMsgRecord' class; modified method
//                     'compareTo(obj,int)' to return 0 if both records
//                     have missing magnitudes and to use date comparison
//                     as a tie-breaker if magnitudes are equal (when
//                     sort type is 'VALUE_SORT_TYPE'); added methods
//                     "get/setValueSortRankSpecifier()"; modified method
//                     'timeEquals()' to work with a 'QWEventMsgRecord'
//                     object (instead of only 'QWEQEventMsgRecord'); added
//                     not-implemented versions of "get/setSelectedTimeVal()"
//                     methods.
//  9/19/2006 -- [ET]  Added support for ANSS-EQ-XML format; removed method
//                     'getmagRecordVec()'; fixed usage of 'zeroValStr'
//                     parameter in 'getHorizontalErrorDispStr()',
//                     'getVerticalErrorDispStr()' and
//                     'getMinimumDistanceDispStr()' methods;
//                     modified to use QWProductMsgRecord
//                     'isMatchingProductType()' method; modified
//                     'getShakeMapProductMsgRecord()' method to detect
//                     ShakeMap product records with "LinkURL" type
//                     value.
//  10/4/2006 -- [ET]  Added 'getMagNumStationsObj()', 'getNumPhasesObj()',
//                     'getMinimumDistanceObj()', 'getRMSTimeErrorObj()',
//                     'getAzimuthalGapObj()', 'getMagStandardErrorObj()'
//                     'getLocationMethod()', 'getMethodCommentText()'
//                     and 'getFirstMagCommentText()' methods; changed
//                     'getNumStationsObj()' method to make it return
//                     the number-of-stations value for the event instead
//                     of the magnitude.
//  4/18/2008 -- [ET]  Added optional 'oneSpaceFlag' parameter to
//                     'getDateTimeString()' method.
// 11/16/2009 -- [ET]  Added 'getEventUsage()' and 'getEventScope()' methods.
//  3/30/2010 -- [ET]  Added support for QuakeML format.
//  5/12/2010 -- [ET]  Modified QuakeML processing to allow 'magnitude'
//                     element to be at same level as 'event' element.
//  8/25/2010 -- [ET]  Updated package names for QDM classes.
//  5/19/2011 -- [ET]  Modified 'getShakeMapProductMsgRecord()' method to
//                     recognize more variants of a ShakeMap-product
//                     message; added 'isEventDataValid()' method.
//  6/19/2013 -- [ET]  Moved ShakeMap-product-detection code from
//                     'getShakeMapProductMsgRecord()' method to
//                     'isShakeMapProduct()' method in 'QWIdentDataMsgRecord'
//                     class.
//  1/26/2017 -- [KF]  Modified to support two installers, one for
//                     Tsunami/Emergency centers and another for other users
//  3/13/2019 -- [KF]  Modified to use the PDL Indexer.
//

package com.isti.quakewatch.message;

import java.util.List;
import java.util.Iterator;
import java.util.Date;
import java.util.TimeZone;
import java.util.Locale;
import java.text.NumberFormat;
import java.text.DateFormat;
import java.text.DecimalFormatSymbols;
import java.text.DateFormatSymbols;
import org.jdom.Element;
import com.isti.util.UtilFns;
import com.isti.util.FifoHashtable;
import com.isti.util.LogFile;
import com.isti.util.ExtendedComparable;
import com.isti.util.DataChangedListener;
import com.isti.util.gis.GisUtils;
import com.isti.quakewatch.util.QWUtils;
import com.isti.quakewatch.util.ResourceIdentifier;
import com.isti.quakewatch.common.MsgTag;

/**
 * QWEQEventMsgRecord defines a record of data for one event.
 */
public class QWEQEventMsgRecord extends QWIdentDataMsgRecord
           implements QWEventMsgRecord,Comparable,ExtendedComparable
{
    /** Handle to 'Element' object used to construct this object. */
  public final Element eventElement;
  /** The event scope */
  private final String eventScope;
    /** Message type string set by QWServer. */
  public final String type;
    /** Message type code string received from feeder module. */
  public final String msgTypeCode;
    /** Primary magnitude value for event, or null if none given. */
  public final Double magnitudeVal;
    /** Number-of-stations value, or null if none given. */
  public final Integer magNumStationsVal;
    /** Standard-error value for primary magnitude, or null if none given. */
  public final Double magStandardErrorVal;
    /** Contents of first "Comment|Text" for magnitude, or null if none. */
  public final String firstMagCommentText;
    /** Primary magnitude type for event, or null if none given. */
  public final String magnitudeType;
    /** Date object specifying date/time for event. */
  public final Date time;
    /** Latitude value for event. */
  public final double latitude;
    /** Longitude value for event. */
  public final double longitude;
    /** Depth value for event, or null if none given. */
  public final Double depth;
    /** Authoritative flag value for event, or null if none given. */
  public final Boolean authoritative;
    /** Associate value for event, or null if none given. */
  public final String associate;
    /** LowPriority value for the event */
  private final Boolean lowPriority;
    /** Verified flag value for event, or false if none given. */
  public final boolean verified;
    /** Quarry flag value for event, or false if none given. */
  public final boolean quarry;
    /** Trump flag value for event. */
  private boolean trump = false;
    /** Flag set true if event contains a ShakeMap product. */
  private boolean containsShakeMapProductFlag = false;
    /** Flag set true if event contains a Tsunami product. */
  private boolean containsTsunamiProductFlag = false;

    /** Table of 'QWProductMsgRecord' objects for this event (or null). */
  protected FifoHashtable productRecTable = null;
    /** Handle to record that this record superseded, or null for none. */
  protected QWEventMsgRecord supersededRecordObj = null;
    /** Value-sort range specifier set by 'setValueSortRankSpecifier()'. */
  private static int valueSortRankSpecifier = 2;

    /** Text to display when there is no value */
  public static final String NO_VALUE_STR = "??";

    //tag strings for 'getLat/LonString()' methods:
  private static final String N_STR = " N";
  private static final String S_STR = " S";
  private static final String E_STR = " E";
  private static final String W_STR = " W";

    //units strings for 'get...DispStr()' methods:
  private static final String KM_DSTR = " km";
  private static final String SECS_DSTR = " seconds";
  private static final String DEG_DSTR = " degree";
    //number formatter for 'getRMSTimeErrorDispStr()' method:
  private static NumberFormat rmsTimeErrorNumberFormat = null;

    //number formatters to convert numbers to string for class objects:
  private static final NumberFormat oneDigitNumberFormat =
                                         UtilFns.createFloatNumberFormat(1);
  private static final NumberFormat latLonDigitNumberFormat =
                                         UtilFns.createFloatNumberFormat(3);

    //date format object for creating date/time strings (two spaces):
  private static final DateFormat dateTimeFormatterObj =
                        UtilFns.createDateFormatObj("yyyy/MM/dd  HH:mm:ss");
    //date format object for creating date/time strings (one space):
  private static final DateFormat sp1DateTimeFormatterObj =
                         UtilFns.createDateFormatObj("yyyy/MM/dd HH:mm:ss");
    //date formatter object for creating date string:
  private static final DateFormat dateFormatterObj =
                                 UtilFns.createDateFormatObj("MMM d, yyyy");
    //time formatter object for creating time string:
  private static final DateFormat timeFormatterObj =
                              UtilFns.createDateFormatObj("HH:mm:ss.SSS z");

  private static final long maxEventTimeValue;

  static
  {
    //add data-change listener to handle default-locale changes
    UtilFns.addDataChangedListener(new DataChangedListener()
      {
        /**
         * Called when data has changed.
         * @param sourceObj source object that called this method.
         */
        public void dataChanged(Object sourceObj)
        {
          //if there was a default locale change
          final Locale localeObj = UtilFns.getDefaultLocale();
          if(sourceObj.equals(localeObj))
          {
            //update the float number formatters
            final DecimalFormatSymbols newSymbols =
                                        new DecimalFormatSymbols(localeObj);
            UtilFns.setDecimalFormatSymbols(
                                       rmsTimeErrorNumberFormat,newSymbols);
            UtilFns.setDecimalFormatSymbols(
                                           oneDigitNumberFormat,newSymbols);
            UtilFns.setDecimalFormatSymbols(
                                        latLonDigitNumberFormat,newSymbols);

            //update the date formatters
            final DateFormatSymbols newFormatSymbols =
                                           new DateFormatSymbols(localeObj);
            UtilFns.setDateFormatSymbols(
                                    dateTimeFormatterObj, newFormatSymbols);
            UtilFns.setDateFormatSymbols(
                                 sp1DateTimeFormatterObj, newFormatSymbols);
            UtilFns.setDateFormatSymbols(
                                        dateFormatterObj, newFormatSymbols);
            UtilFns.setDateFormatSymbols(
                                        timeFormatterObj, newFormatSymbols);
          }
        }
      });
         //setup maximum time value for 'isEventDataValid()' method:
      long timeVal;
      try
      {       //get current time:
        timeVal = System.currentTimeMillis();
      }
      catch(Exception ex)
      {       //use default time value if error
        timeVal = 1500000000000L;
      }       //add 100 years to current time value for limit:
      maxEventTimeValue = timeVal + 100*365*UtilFns.MS_PER_DAY;
  }


  /**
   * Creates a data record for one event, built from the given XML elements.
   * @param qwMsgElement the XML "QWmessage" element object.
   * @param dataMsgElement the XML "DataMessage" element object.
   * @param eventElement the XML "Event" message element.
   * @param originElement the "Origin" message element if ANSS-EQ-XML
   * message format, or null if QuakeWatch message format.
   * @param messageFormatSpec specifier for message-format type (one of
   * the 'QWIdentDataMsgRecord.MFMT_...' values).
   * @throws QWRecordException if an error occurs while creating
   * the data record.
   */
  public QWEQEventMsgRecord(Element qwMsgElement, Element dataMsgElement,
                                Element eventElement, Element originElement,
                             int messageFormatSpec) throws QWRecordException
  {                                                   //construct parents:
    super(qwMsgElement,dataMsgElement,eventElement,messageFormatSpec);
    this.eventElement = eventElement;       //save handle to element
    if(!MsgTag.EVENT.equalsIgnoreCase(eventElement.getName()))
    {    //root element name not "Event"
      throw new QWRecordException("Element tag name \"" +
                                             MsgTag.EVENT + "\" not found");
    }
    List listObj;
    Object obj;
    String str;
    Element elemObj;
    switch(messageFormatSpec)
    {  //process data depending on format specification
      case MFMT_QUAKEML:     //data is QuakeML message format
        if(originElement == null)
          throw new QWRecordException("Null 'origin' element object");
        //get value of 'quakeMessage'|'scope' element:
        if((elemObj=dataMsgElement.getChild(
        	MsgTag.QUAKE_MESSAGE,eventElement.getNamespace())) != null)
        {  //'quakeMessage' element fetched OK; get value of 'scope'
          eventScope = elemObj.getChildTextTrim(
                         MsgTag.QUAKEML_SCOPE,elemObj.getNamespace());
        }
        else
        {
          eventScope = null;
        }
              //get "type" child-element of "event" element:
        type = getOptElementStr(MsgTag.QUAKEML_TYPE);
              //get "preferredMagnitudeID" child-element of "event" element:
        final String prefMagIdStr =
                            getOptElementStr(MsgTag.PREFERRED_MAGNITUDE_ID);
              //set no "MsgTypeCode" value:
        msgTypeCode = UtilFns.EMPTY_STRING;
              //setup to get data-fields from "origin" element:
        currentElement = originElement;
              //get "time" value-child-element of "origin" element:
        time = getElementValueTime(MsgTag.QUAKEML_TIME);
              //get "latitude" value-child-element of "origin" element:
        latitude = getElementValueDouble(MsgTag.QUAKEML_LATITUDE);
              //get "longitude" value-child-element of "origin" element:
        longitude = getElementValueDouble(MsgTag.QUAKEML_LONGITUDE);
              //get optional "depth" value-child-element of "origin" element:
        final Double dblObj;           //convert meters to kilometers:
        depth = ((dblObj=getOptElementValueDouble(MsgTag.QUAKEML_DEPTH)) !=
                   null) ? (Double.valueOf(dblObj.doubleValue()/1000.0)) : null;
              //get optional "Authoritative" value-child-element of "origin" element:
        authoritative = getOptAttribBoolean(MsgTag.AUTHORITATIVE);
              //get optional "Associate" attribute of "origin" element:
        associate = getOptAttribStr(MsgTag.ASSOCIATE);
              //get optional "LowPriority" value-child-element of "origin" element:
        lowPriority = getOptAttribBoolean(MsgTag.LOW_PRIORITY);
        if (lowPriority != null && associate == null)
        {
        	LogFile.getGlobalLogObj().warning("Low priority without associate");
        }
              //convert "origin|evaluationStatus" to "Verified" flag:
        verified = (str=getOptElementStr(MsgTag.EVALUATION_STATUS)) != null
                        && (str.equalsIgnoreCase(MsgTag.QUAKEML_REVIEWED) ||
                                   str.equalsIgnoreCase(MsgTag.CONFIRMED) ||
                                        str.equalsIgnoreCase(MsgTag.FINAL));
              //set "Quarry" flag if "event|type" is "quarry blast":
        quarry = (type != null &&
                              type.equalsIgnoreCase(MsgTag.QUAKEML_QUARRY));

              //get list of "magnitude" elements under "event" element
              // or list of "magnitude" elements at same level as "event":
        if(((listObj=eventElement.getChildren(MsgTag.QUAKEML_MAGNITUDE,
             eventElement.getNamespace())) != null && listObj.size() > 0) ||
                              ((elemObj=eventElement.getParent()) != null &&
                      (listObj=elemObj.getChildren(MsgTag.QUAKEML_MAGNITUDE,
                    elemObj.getNamespace())) != null && listObj.size() > 0))
        {  //at least one "magnitude" element found
                   //determine which "magnitude" element should be used:
          Element selMagElem = null;
          String idStr;
          final Iterator iterObj = listObj.iterator();
          while(iterObj.hasNext())
          {  //for each "magnitude" element in list
            if((obj=iterObj.next()) instanceof Element)
            {  //'Element' fetched OK
              if((idStr=((Element)obj).getAttributeValue(
                                               MsgTag.PUBLIC_ID)) != null &&
                                                 idStr.equals(prefMagIdStr))
              {  //magnitude's 'publicID' matches 'preferredMagnitudeID'
                selMagElem = (Element)obj;  //select magnitude element obj
                break;                      //exit loop
              }
              if(selMagElem == null)        //if first magnitude object then
                selMagElem = (Element)obj;  //select magnitude element obj
            }
            else
            {  //not 'Element' object in list (shouldn't happen)
              throw new QWRecordException("Error fetching \"" +
                            MsgTag.QUAKEML_MAGNITUDE + "\" element for \"" +
                                      MsgTag.QUAKEML_ORIGIN + "\" element");
            }
          }
              //create magnitude-record object for element (if found):
          final QWMagnitudeRecord selMagRecObj = (selMagElem != null) ?
                       new QWMagnitudeRecord(selMagElem,messageFormatSpec) :
                                                                       null;
              //if available then save value from selected magnitude object:
          magnitudeVal = (selMagRecObj != null) ?
                                  Double.valueOf(selMagRecObj.magnitude) : null;
              //if available then save num stations from sel mag object:
          magNumStationsVal = (selMagRecObj != null) ?
                                            selMagRecObj.numStations : null;
              //if available then save standard error from sel mag object:
          magStandardErrorVal = (selMagRecObj != null) ?
                                               selMagRecObj.magError : null;
              //if available then save "comment|text" from sel mag object:
          firstMagCommentText = (selMagRecObj != null) ?
                                    selMagRecObj.firstMagCommentText : null;
              //if available then save type from selected magnitude object:
          magnitudeType = (selMagRecObj != null) ? selMagRecObj.type : null;
        }
        else  //no "Magnitude" elements found
        {
          magnitudeVal = null;         //indicate no magnitude value
          magNumStationsVal = null;    //indicate no num stations value
          magStandardErrorVal = null;  //indicate no mag std error value
          firstMagCommentText = null;  //indicate no mag "Comment|Text"
          magnitudeType = null;        //indicate no magnitude type
        }
        break;

      case MFMT_ANSSEQXML:   //data is ANSS-EQ-XML message format
        if(originElement == null)
          throw new QWRecordException("Null 'Origin' element object");
        eventScope = eventElement.getChildTextTrim(
            MsgTag.SCOPE,eventElement.getNamespace());
              //get "Type" child-element of "Event" element:
        type = getOptElementStr(MsgTag.TYPE);
              //set no "MsgTypeCode" value:
        msgTypeCode = UtilFns.EMPTY_STRING;
              //setup to get data-fields from "Origin" element:
        currentElement = originElement;
              //get "Time" child-element of "Origin" element:
        time = getElementTime(MsgTag.TIME);
              //get "Latitude" child-element of "Origin" element:
        latitude = getElementDouble(MsgTag.LATITUDE);
              //get "Longitude" child-element of "Origin" element:
        longitude = getElementDouble(MsgTag.LONGITUDE);
              //get optional "Depth" child-element of "Origin" element:
        depth = getOptElementDouble(MsgTag.DEPTH);
              //get optional "Authoritative" child-element of "Origin" element:
        authoritative = getOptAttribBoolean(MsgTag.AUTHORITATIVE);
        	  //get optional "Associate" attribute of "Origin" element:
        associate = getOptAttribStr(MsgTag.ASSOCIATE);
              //get optional "LowPriority" value-child-element of "origin" element:
        lowPriority = getOptAttribBoolean(MsgTag.LOW_PRIORITY);
        if (lowPriority != null && associate == null)
        {
        	LogFile.getGlobalLogObj().warning("Low priority without associate");
        }
              //convert "Origin|Status" to "Verified" flag:
        verified = (str=getOptElementStr(MsgTag.STATUS)) != null &&
                             (str.equalsIgnoreCase(MsgTag.QUICK_REVIEWED) ||
                                    str.equalsIgnoreCase(MsgTag.REVIEWED) ||
                                    str.equalsIgnoreCase(MsgTag.PUBLISHED));
              //set "Quarry" flag if "Event|Type" is "Quarry":
        quarry = (type != null && type.equalsIgnoreCase(MsgTag.QUARRY));

              //get list of "Magnitude" elements under "Origin" element:
        if((listObj=originElement.getChildren(
                  MsgTag.MAGNITUDE,originElement.getNamespace())) != null &&
                                                         listObj.size() > 0)
        {  //at least one "Magnitude" element found
          QWMagnitudeRecord magRecObj, selMagRecObj = null;
          final Iterator iterObj = listObj.iterator();
          while(iterObj.hasNext())
          {     //for each "Magnitude" element in list
            if((obj=iterObj.next()) instanceof Element)
            {   //'Element' fetched OK; create object
              magRecObj = new QWMagnitudeRecord((Element)obj,
                                                         messageFormatSpec);
              if(selMagRecObj == null || (magRecObj.preferredFlag != null &&
                                   magRecObj.preferredFlag.booleanValue() &&
                                      (selMagRecObj.preferredFlag == null ||
                               !selMagRecObj.preferredFlag.booleanValue())))
              {    //first mag record found, or has 'preferredFlag'==true
                   // and previously-selected record did not
                selMagRecObj = magRecObj;   //select magnitude record object
              }
            }
            else
            {  //not 'Element' object in list (shouldn't happen)
              throw new QWRecordException("Error fetching \"" +
                                    MsgTag.MAGNITUDE + "\" element for \"" +
                                              MsgTag.ORIGIN + "\" element");
            }
          }
              //if available then save value from selected magnitude object:
          magnitudeVal = (selMagRecObj != null) ?
                                  Double.valueOf(selMagRecObj.magnitude) : null;
              //if available then save num stations from sel mag object:
          magNumStationsVal = (selMagRecObj != null) ?
                                            selMagRecObj.numStations : null;
              //if available then save standard error from sel mag object:
          magStandardErrorVal = (selMagRecObj != null) ?
                                               selMagRecObj.magError : null;
              //if available then save "Comment|Text" from sel mag object:
          firstMagCommentText = (selMagRecObj != null) ?
                                    selMagRecObj.firstMagCommentText : null;
              //if available then save type from selected magnitude object:
          magnitudeType = (selMagRecObj != null) ? selMagRecObj.type : null;
        }
        else  //no "Magnitude" elements found
        {
          magnitudeVal = null;         //indicate no magnitude value
          magNumStationsVal = null;    //indicate no num stations value
          magStandardErrorVal = null;  //indicate no mag std error value
          firstMagCommentText = null;  //indicate no mag "Comment|Text"
          magnitudeType = null;        //indicate no magnitude type
        }
        break;

      default:               //data is QuakeWatch message format
    	eventScope = null;
              //get "Type" attribute:
        type = getAttribStr(MsgTag.TYPE);
              //get optional "MsgTypeCode" attribute:
        msgTypeCode = getNonNullOptAttribStr(MsgTag.MSG_TYPE_CODE);
              //get "Time" attribute:
        time = getAttribTime(MsgTag.TIME);
              //get "Latitude" attribute:
        latitude = getAttribDouble(MsgTag.LATITUDE);
              //get "Longitude" attribute:
        longitude = getAttribDouble(MsgTag.LONGITUDE);
              //get optional "Depth" attribute:
        depth = getOptAttribDouble(MsgTag.DEPTH);
              //get optional "Authoritative" attribute:
        authoritative = getOptAttribBoolean(MsgTag.AUTHORITATIVE);
              //get optional "Associate" attribute:
        associate = getOptAttribStr(MsgTag.ASSOCIATE);
             //get optional "LowPriority" attribute:
        lowPriority = getOptAttribBoolean(MsgTag.LOW_PRIORITY);
        if (lowPriority != null && associate == null)
        {
        	LogFile.getGlobalLogObj().warning("Low priority without associate");
        }
              //get optional "Verified" attribute:
        verified = getDefAttribBoolean(MsgTag.VERIFIED,false);
              //get optional "Quarry" attribute:
        quarry = getDefAttribBoolean(MsgTag.QUARRY,false);

              //get list of "Magnitude" elements under "Event" element:
        if((listObj=eventElement.getChildren(MsgTag.MAGNITUDE)) != null &&
                                                         listObj.size() > 0)
        {  //at least one "Magnitude" element found
          QWMagnitudeRecord magRecObj, firstMagRecObj = null;
          final Iterator iterObj = listObj.iterator();
          while(iterObj.hasNext())
          {     //for each "Magnitude" element in list
            if((obj=iterObj.next()) instanceof Element)
            {   //'Element' fetched OK; create object
              magRecObj = new QWMagnitudeRecord((Element)obj);
              if(firstMagRecObj == null)    //if first one then
                firstMagRecObj = magRecObj; //save magnitude record object
            }
            else
            {   //not 'Element' object in list (shouldn't happen)
              throw new QWRecordException("Error fetching \"" +
                                    MsgTag.MAGNITUDE + "\" element for \"" +
                                               MsgTag.EVENT + "\" element");
            }
          }
              //if available then save value from first magnitude object:
          magnitudeVal = (firstMagRecObj != null) ?
                                Double.valueOf(firstMagRecObj.magnitude) : null;
              //if available then save num stations from first mag object:
          magNumStationsVal = (firstMagRecObj != null) ?
                                          firstMagRecObj.numStations : null;
              //if available then save standard error from first mag object:
          magStandardErrorVal = (firstMagRecObj != null) ?
                                             firstMagRecObj.magError : null;
              //if available then save "Comment|Text" from sel mag object:
          firstMagCommentText = (firstMagRecObj != null) ?
                                  firstMagRecObj.firstMagCommentText : null;
              //if available then save type from first magnitude object:
          magnitudeType = (firstMagRecObj != null) ? firstMagRecObj.type :
                                                                       null;
        }
        else  //no "Magnitude" elements found
        {
          magnitudeVal = null;         //indicate no magnitude value
          magNumStationsVal = null;    //indicate no mag num stations value
          magStandardErrorVal = null;  //indicate no mag std error value
          firstMagCommentText = null;  //indicate no mag "Comment|Text"
          magnitudeType = null;        //indicate no magnitude type
        }
    }
  }

  /**
   * Creates a data record for one event, built from the given XML elements.
   * @param qwMsgElement the XML "QWmessage" element object.
   * @param dataMsgElement the XML "DataMessage" element object.
   * @param eventElement the XML "Event" message element.
   * @param originElement the "Origin" message element if ANSS-EQ-XML
   * message format, or null if QuakeWatch message format.
   * @param anssEQMsgFormatFlag true for ANSS-EQ-XML message format;
   * false for QuakeWatch message format.
   * @throws QWRecordException if an error occurs while creating
   * the data record.
   * @deprecated Use version with 'messageFormatSpec' parameter.
   */
  public QWEQEventMsgRecord(Element qwMsgElement, Element dataMsgElement,
                                Element eventElement, Element originElement,
                       boolean anssEQMsgFormatFlag) throws QWRecordException
  {                                                   //construct parents:
    this(qwMsgElement,dataMsgElement,eventElement,originElement,
                   (anssEQMsgFormatFlag ? MFMT_ANSSEQXML : MFMT_QWMESSAGE));
  }

  /**
   * Creates a data record for one event, built from a "QuakeWatch"
   * format XML "Event" message element.
   * @param qwMsgElement the XML "QWmessage" element object.
   * @param dataMsgElement the XML "DataMessage" element object.
   * @param eventElement the XML "Event" message element.
   * @throws QWRecordException if an error occurs while creating
   * the data record.
   */
  public QWEQEventMsgRecord(Element qwMsgElement,Element dataMsgElement,
                              Element eventElement) throws QWRecordException
  {
    this(qwMsgElement,dataMsgElement,eventElement,null,MFMT_QWMESSAGE);
  }

  /**
   * Returns the child "Event" element used to build this message record.
   * @return The child "Event" element used to build this message record.
   */
  public Element getMsgRecChildElement()
  {
    return eventElement;
  }

  /**
   * Sets the value-sort rank specifier.  This is used by the
   * 'compareTo(obj,int)' method to determine the result when
   * two different types of objects that implement 'QWEventMsgRecord'
   * are compared (using 'VALUE_SORT_TYPE').
   * @param specVal numeric specifier to use.
   */
  public static void setValueSortRankSpecifier(int specVal)
  {
    valueSortRankSpecifier = specVal;
  }

  /**
   * Returns the value-sort rank specifier.  This is used by the
   * 'compareTo(obj,int)' method to determine the result when
   * two different types of objects that implement 'QWEventMsgRecord'
   * are compared (using 'VALUE_SORT_TYPE').
   * @return The numeric value-sort rank specifier.
   */
  public int getValueSortRankSpecifier()
  {
    return valueSortRankSpecifier;
  }

  /**
   * Returns the ShakeMap information for the event.
   * @return ShakeMap info or null if not available.
   */
  public String getShakeMapInfo()
  {
    final QWProductMsgRecord prodRecObj;
    return ((prodRecObj=getShakeMapProductMsgRecord()) != null) ?
                                                    prodRecObj.value : null;
  }

  /**
   * Gets the shake map product message record for the event.
   * @return the shake map product message record or null if not available.
   */
  public QWProductMsgRecord getShakeMapProductMsgRecord()
  {
    return getProductMsgRecord(MsgTag.SHAKEMAP_URL);
  }

  /**
   * Gets the associate string.
   * @return the associate string or null if none.
   */
  public String getAssociateString()
  {
    return associate;
  }

  /**
   * Get the authoritative value.
   * @return the authoritative value or null if none.
   */
  public Boolean getAuthoritative()
  {
    return authoritative;
  }

  /**
   * Returns true if the event is authoritative.
   * @return  true if the event is authoritative.
   */
  public boolean isAuthor()
  {
    return (authoritative != null && authoritative.booleanValue());
  }

  /**
   * Gets the authoritative flag as a string.
   * @return  string representing the authoritative flag.
   */
  public String getAuthoritativeString()
  {
    if(authoritative == null)
      return NO_VALUE_STR;
    return isAuthor()?MsgTag.YES:MsgTag.NO;
  }
  
  /**
   * Return true if event is low priority.
   * @return true if event is low priority.
   */
  public boolean isLowPriority()
  {
	  return lowPriority != null && lowPriority.booleanValue();
  }

  /**
   * Returns true if the event is verified.
   * @return true if the event is verified.
   */
  public boolean isVerified()
  {
    return verified;
  }

  /**
   * Returns true if the "quarry" flag is set.
   * @return true if the "quarry" flag is set.
   */
  public boolean isQuarry()
  {
    return quarry;
  }

  /**
   * Adds a 'QWProductMsgRecord' object to this event record, to be stored
   * in a table using the record's 'type' as the key value.
   * @param productMsgObj the 'QWProductMsgRecord' object to add.
   */
  public void addProductMsgRecord(QWProductMsgRecord productMsgObj)
  {
    if(productRecTable == null)               //if not yet created then
      productRecTable = new FifoHashtable();  //create table
    productRecTable.put(productMsgObj.type,productMsgObj);
    updateProductIndicatorFlags();     //update contains-product flags
  }

  /**
   * Returns a 'QWProductMsgRecord' object for this event record.
   * A product is considered matched if its type string is identical to
   * the  given type string.
   * @param typeStr the 'type' value of the 'QWProductMsgRecord' object
   * to be returned.
   * @return A 'QWProductMsgRecord' object, or null if a matching object
   * is not found.
   */
  public QWProductMsgRecord getProductMsgRecord(String typeStr)
  {
    Object obj;
    if(productRecTable != null &&
           (obj=productRecTable.get(typeStr)) instanceof QWProductMsgRecord)
    {    //table exists and matching product was found
      return (QWProductMsgRecord)obj;       //return product object
    }
    return null;
  }

  /**
   * Removes a 'QWProductMsgRecord' object from this event record.
   * @param prodTypeStr the 'type' string of the 'QWProductMsgRecord'
   * object to remove.
   * @return The 'QWProductMsgRecord' object that was removed, or null
   * if a matching 'QWProductMsgRecord' was not found.
   */
  public QWProductMsgRecord removeProductMsgRecord(String prodTypeStr)
  {
    final Object obj;
    if(productRecTable != null && (obj=productRecTable.remove(prodTypeStr))
                                              instanceof QWProductMsgRecord)
    {    //table exists and matching product was removed
      updateProductIndicatorFlags();        //update contains-product flags
      return (QWProductMsgRecord)obj;       //return product object
    }
    return null;
  }

  /**
   * Returns a matching 'QWProductMsgRecord' object for this event record.
   * A product is considered a match if its type string contains the
   * given type string.
   * @param typeStr the 'type' value of the 'QWProductMsgRecord' object
   * to be matched.
   * @return A 'QWProductMsgRecord' object, or null if a matching object
   * is not found.
   */
  public QWProductMsgRecord matchProductMsgRecType(String typeStr)
  {
    if(productRecTable != null)
    {    //product record table exists; get iterator for product records
      final Iterator iterObj = productRecTable.getValuesVector().iterator();
      Object obj;
      QWProductMsgRecord prodRecObj;
      while(iterObj.hasNext())
      {  //for each product record
        if((obj=iterObj.next()) instanceof QWProductMsgRecord)
        {     //product record object fetched OK
          prodRecObj = (QWProductMsgRecord)obj;
          if(prodRecObj.isMatchingProductType(typeStr))
          {   //product record type string contains given type string
            return prodRecObj;         //return matched product
          }
        }
      }
    }
    return null;
  }

  /**
   * Returns the table of 'QWProductMsgRecord' objects for products
   * associated with this event.
   * @return A FifoHashtable of 'QWProductMsgRecord' objects, or null
   * if none are available.
   */
  public FifoHashtable getProductRecTable()
  {
    return productRecTable;
  }

  /**
   * Returns the number of products associated with this event.
   * @return The number of products associated with this event.
   */
  public int getProductCount()
  {
    return (productRecTable != null) ? productRecTable.size() : 0;
  }

  /**
   * Enters a new table of 'QWProductMsgRecord' objects for products
   * associated with this event.
   * @param tableObj the new table to use.
   */
  public void setProductRecTable(FifoHashtable tableObj)
  {
    productRecTable = tableObj;
    updateProductIndicatorFlags();     //update contains-product flags
  }

  /**
   * Appends the given table of 'QWProductMsgRecord' objects for products
   * associated with this event.
   * @param tableObj the table to use.
   * @param keepCurrentFlag if true then any matching items in the current
   * products table will be retained; if false then any matching items in
   * the given table will overwrite current ones.
   */
  public void addToProductRecTable(FifoHashtable tableObj,
                                                    boolean keepCurrentFlag)
  {
    if(tableObj != null && tableObj.size() > 0)
    {    //given table contains items
      if(productRecTable == null || productRecTable.size() <= 0)
        productRecTable = tableObj;    //if no current then take new table
      else
      {       //current products table exists
        if(keepCurrentFlag)
        {     //don't overwrite matching items in current products table
          final FifoHashtable newTableObj =      //duplicate given table
                                          (FifoHashtable)(tableObj.clone());
                   //add in current items (old items take priority):
          newTableObj.putAll(productRecTable);
          productRecTable = newTableObj;         //save new table
        }
        else  //overwrite matching items in current products table
          productRecTable.putAll(tableObj);      //add new table items
      }
      updateProductIndicatorFlags();        //update contains-product flags
    }
  }

  /**
   * Updates the contains-product indicator flags.
   */
  protected void updateProductIndicatorFlags()
  {
    containsShakeMapProductFlag = (getShakeMapProductMsgRecord() != null);
    containsTsunamiProductFlag =
                           (matchProductMsgRecType(MsgTag.TSUNAMI) != null);
  }

  /**
   * Returns a flag indicating whether or not this event contains a
   * ShakeMap product.
   * @return true if this event contains a ShakeMap product; false
   * if not.
   */
  public boolean containsShakeMapProduct()
  {
    return containsShakeMapProductFlag;
  }

  /**
   * Returns a flag indicating whether or not this event contains a
   * Tsunami product.
   * @return true if this event contains a Tsunami product; false
   * if not.
   */
  public boolean containsTsunamiProduct()
  {
    return containsTsunamiProductFlag;
  }

  /**
   * Indicates whether some other object is "equal to" this one.
   * @param obj the reference object with which to compare.
   * @return <code>true</code> if this object is the same as the obj
   * argument; <code>false</code> otherwise.
   */
  public boolean equals(QWEventMsgRecord obj)
  {
    final QWEQEventMsgRecord recObj;
    if(!(obj instanceof QWEQEventMsgRecord))
      return false;     //if not a 'QWEQEventMsgRecord' then return false
    recObj = (QWEQEventMsgRecord)obj;
    return super.equals(obj) &&
        ((type == null) ? recObj.type == null : type.equals(recObj.type)) &&
        ((msgTypeCode == null) ? recObj.msgTypeCode == null :
                                     msgTypeCode.equals(recObj.msgTypeCode)) &&
        ((magnitudeVal == null) ? recObj.magnitudeVal == null :
                                   magnitudeVal.equals(recObj.magnitudeVal)) &&
        ((time == null) ? recObj.time == null : time.equals(recObj.time)) &&
        (latitude == recObj.latitude) &&
        (longitude == recObj.longitude) &&
        ((depth == null) ? recObj.depth == null : depth.equals(recObj.depth)) &&
        ((authoritative == null) ? recObj.authoritative == null :
                                 authoritative.equals(recObj.authoritative)) &&
        ((associate == null) ? recObj.associate == null :
        	associate.equals(recObj.associate)) &&
        (verified == recObj.verified) &&
        (trump == recObj.trump);
  }

  /**
   * Indicates whether some other object is "equal to" this one.
   * @param obj the reference object with which to compare.
   * @return <code>true</code> if this object is the same as the obj
   * argument; <code>false</code> otherwise.
   */
  public boolean equals(Object obj)
  {
    return obj instanceof QWEventMsgRecord && equals((QWEventMsgRecord)obj);
  }

  /**
   * Returns true if the given object is a 'QWEventMsgRecord' object whose
   * event date/time equals the one for this record; otherwise returns
   * false.
   * @param obj the 'QWEventMsgRecord' object to use.
   * @return true if the event date/times are equal.
   */
  public boolean timeEquals(QWEventMsgRecord obj)
  {
    final QWEventMsgRecord recObj;
    if(!(obj instanceof QWEventMsgRecord))
      return false;     //if not a 'QWEventMsgRecord' then return false
    recObj = (QWEventMsgRecord)obj;
    return (time == null) ? (recObj.getTime() == null) :
                                              time.equals(recObj.getTime());
  }

  /**
   * Returns true if the given object is a 'QWEventMsgRecord' object whose
   * event date/time equals the one for this record; otherwise returns
   * false.
   * @param obj the 'QWEventMsgRecord' object to use.
   * @return true if the event date/times are equal.
   */
  public boolean timeEquals(Object obj)
  {
    return obj instanceof QWEventMsgRecord &&
                                          timeEquals((QWEventMsgRecord)obj);
  }

  /**
   * Compares this object with the specified object and index for order.
   * This method implements the ExtendedComparable interface.
   * @param   obj the Object to be compared.
   * @param   sortType the sort type.
   * @return  a negative integer, zero, or a positive integer as this object
   *		is less than, equal to, or greater than the specified object.
   * @throws ClassCastException if the specified object's type prevents it
   *         from being compared to this Object.
   */
  public int compareTo(Object obj, int sortType) throws ClassCastException
  {
    if(sortType == QWEventMsgRecord.VALUE_SORT_TYPE)
    {       //sort-type is "value" (magnitude value)
                      //get magnitude value for given record object:
      final Double givenMagObj;
      if(obj != null)
      {  //given object not null
        if(!(obj instanceof QWEventMsgRecord))
          throw new ClassCastException();   //if wrong type then exception
              //get value-sort rank specifier for given object:
        final int givenRankSpec =
                        ((QWEventMsgRecord)obj).getValueSortRankSpecifier();
              //if value-sort rank specifiers different then return result:
        if(valueSortRankSpecifier != givenRankSpec)
          return (valueSortRankSpecifier < givenRankSpec) ? -1 : 1;
        givenMagObj = ((QWEventMsgRecord)obj).getMagnitudeObj();
      }
      else    //given object is null
        givenMagObj = null;       //indicate no magnitude value
      final int retVal;
            //if this record has no magnitude then return 0 if given
            // record also has no magnitude or -1 if it does:
      if(magnitudeVal == null)
        retVal = (givenMagObj == null) ? 0 : -1;
      else if(givenMagObj == null)  //if given record has no magnitude
        retVal = 1;                  // then return 1
      else            //compare magnitude-value objects
        retVal = magnitudeVal.compareTo(givenMagObj);
           //if "not equal" then return result; otherwise
           // return result of "date" comparison:
      return (retVal != 0) ? retVal : compareTo(obj);
    }
            //sort-type not "value" (magnitude value)
    return compareTo(obj);           //return result of "date" comparison
  }

  /**
   * Compares the given 'QWEventMsgRecord' object to this one.
   * The event date/time is used as the sorting key.
   * @param obj the 'QWEventMsgRecord' object to use.
   * @return A negative integer, zero, or a positive integer as this
   * object is less than, equal to, or greater than the specified object.
   * @throws ClassCastException if the specified object's type prevents it
   * from being compared to this Object.
   */
  public int compareTo(Object obj) throws ClassCastException
  {
    try
    {         //return comparison of date objects:
      return time.compareTo(((QWEventMsgRecord)obj).getTime());
    }
    catch(NullPointerException ex)
    {         //if this object's date object is null then return -1;
              // otherwise other object or date was null so return 1:
      return (time == null) ? -1 : 1;
    }
  }

  /**
   * Returns a string representation of this record.  (Same as
   * 'getToolTipString()' method.)
   * @return A string representation of date, time and magnitude for
   * this record, with 2-space separators surrounding each item.
   */
  public String toString()
  {
    return "  " + getToolTipString() + "  ";
  }

  /**
   * Returns a string representation of this record to be used for
   * its tooltip text.  (Same as 'toString()' method.)
   * @return A string representation of date, time and magnitude for
   * this record.
   */
  public String getToolTipString()
  {
    return getDateTimeString() + "  " + getMagnitudeValueString();
  }

  /**
   * Returns a display string representation of this object (not
   * including the location note string).
   * @return A new String object.
   */
  public String getDisplayString()
  {
    return "ID#=" + eventID + " " + dataSource + " " + version +
        " " + getDateTimeString() +
        " mag=" + getMagnitudeValueString() +
        " depth=" + getDepthString() +
        " " + getLatitudeString() +
        " " + getLongitudeString();
  }

  /**
   * Returns a display string representation of the date.
   * @return A display string.
   */
  public String getDateString()
  {
    if(time == null)
      return NO_VALUE_STR;
    synchronized(dateFormatterObj)
    {    //only allow thread at a time to use date-format object
      return dateFormatterObj.format(time);
    }
  }

  /**
   * Returns a display string representation of the date/time.
   * @param oneSpaceFlag true for one space between date and time;
   * false for two spaces.
   * @return A display string.
   */
  public String getDateTimeString(boolean oneSpaceFlag)
  {
    if(time == null)
      return NO_VALUE_STR;
    if(oneSpaceFlag)
    {  //one space between date and time
      synchronized(sp1DateTimeFormatterObj)
      {    //only allow thread at a time to use date-format object
        return sp1DateTimeFormatterObj.format(time);
      }
    }
    else
    {  //two spaces between date and time
      synchronized(dateTimeFormatterObj)
      {    //only allow thread at a time to use date-format object
        return dateTimeFormatterObj.format(time);
      }
    }
  }

  /**
   * Returns a display string representation of the date/time.
   * @return A display string.
   */
  public String getDateTimeString()
  {
    return getDateTimeString(false);
  }

  /**
   * Returns a display string representation of the time.
   * @return A display string.
   */
  public String getTimeString()
  {
    if(time == null)
      return NO_VALUE_STR;
    final String str;
    synchronized(timeFormatterObj)
    {    //only allow thread at a time to use date-format object
      str = timeFormatterObj.format(time);
    }
         //trim so that only 2 digits of fractional seconds are returned:
    final int len = str.length();
    int p;              //make sure that format is as expected:
    if((p=str.lastIndexOf('.')) > 0 && p + 3 < len)
      return str.substring(0,p+3) + str.substring(p+4);
    return str;
  }

  /**
   * Returns a display string representation of the latitude.
   * @param nsFormatFlag true for N/S format; false for +/- format.
   * @param degMinFlag true for degrees/minutes format; false for
   * decimal format.
   * @return A display string.
   */
  public String getLatitudeString(boolean nsFormatFlag, boolean degMinFlag)
  {
    if(degMinFlag)
    {    //use degrees/minutes format
      if(nsFormatFlag)
      {  //use N/S format
        return (latitude >= 0.0) ?
                                  (QWUtils.getDegMinStr(latitude) + N_STR) :
                                  (QWUtils.getDegMinStr(-latitude) + S_STR);
      }
      else  //use +/- format
        return QWUtils.getDegMinStr(latitude);
    }
    else
    {    //use decimal format
      synchronized(latLonDigitNumberFormat)
      {  //only allow thread at a time to use number-format object
        if(nsFormatFlag)
        {     //use N/S format
          return (latitude >= 0.0) ?
                        (latLonDigitNumberFormat.format(latitude) + N_STR) :
                        (latLonDigitNumberFormat.format(-latitude) + S_STR);
        }
        else  //use +/- format
          return latLonDigitNumberFormat.format(latitude);
      }
    }
  }

  /**
   * Returns a display string representation of the latitude.  The
   * decimal format is used.
   * @param nsFormatFlag true for N/S format; false for +/- format.
   * @return A display string.
   */
  public String getLatitudeString(boolean nsFormatFlag)
  {
    return getLatitudeString(nsFormatFlag,false);
  }

  /**
   * Returns a display string representation of the latitude.  The decimal
   * +/- format is used.
   * @return A display string.
   */
  public String getLatitudeString()
  {
    return getLatitudeString(false,false);
  }

  /**
   * Returns a display string representation of the longitude.
   * @param ewFormatFlag true for E/W format; false for +/- format.
   * @param degMinFlag true for degrees/minutes format; false for
   * decimal format.
   * @return A display string.
   */
  public String getLongitudeString(boolean ewFormatFlag, boolean degMinFlag)
  {
    if(degMinFlag)
    {    //use degrees/minutes format
      if(ewFormatFlag)
      {  //use E/W format
        return (longitude >= 0.0) ?
                                 (QWUtils.getDegMinStr(longitude) + E_STR) :
                                 (QWUtils.getDegMinStr(-longitude) + W_STR);
      }
      else  //use +/- format
        return QWUtils.getDegMinStr(longitude);
    }
    else
    {    //use decimal format
      synchronized(latLonDigitNumberFormat)
      {  //only allow thread at a time to use number-format object
        if(ewFormatFlag)
        {     //use E/W format
          return (longitude >= 0.0) ?
                       (latLonDigitNumberFormat.format(longitude) + E_STR) :
                       (latLonDigitNumberFormat.format(-longitude) + W_STR);
        }
        else  //use +/- format
          return latLonDigitNumberFormat.format(longitude);
      }
    }
  }

  /**
   * Returns a display string representation of the longitude.  The
   * decimal format is used.
   * @param ewFormatFlag true for E/W format; false for +/- format.
   * @return A display string.
   */
  public String getLongitudeString(boolean ewFormatFlag)
  {
    return getLongitudeString(ewFormatFlag,false);
  }

  /**
   * Returns a display string representation of the longitude.  The
   * decimal +/- format is used.
   * @return A display string.
   */
  public String getLongitudeString()
  {
    return getLongitudeString(false,false);
  }

  /**
   * Returns the event depth value.
   * @return A 'Double' containing the event depth value, or null if
   * none available.
   */
  public Double getDepthObj()
  {
    return depth;
  }

  /**
   * Returns a display string representation of the depth.
   * @param milesFlag true to convert the depth value from kilometers
   * to miles; false to leave the depth value unchanged.
   * @return A display string.
   */
  public String getDepthString(boolean milesFlag)
  {
    if(depth == null)
      return NO_VALUE_STR;
    synchronized(oneDigitNumberFormat)
    {    //only allow thread at a time to use number-format object
      return oneDigitNumberFormat.format(milesFlag ?
              depth.doubleValue()*QWUtils.KM_TO_MILE : depth.doubleValue());
    }
  }

  /**
   * Returns a display string representation of the depth.
   * @return A display string.
   */
  public String getDepthString()
  {
    return getDepthString(false);
  }

  /**
   * Returns a display string representation of the magnitude.
   * @return A display string.
   */
  public String getMagnitudeValueString()
  {
    if(magnitudeVal == null)
        return NO_VALUE_STR;
    synchronized(oneDigitNumberFormat)
    {    //only allow thread at a time to use number-format object
      return oneDigitNumberFormat.format(magnitudeVal.doubleValue());
    }
  }

  /**
   * Returns a string representing the type code for the primary
   * magnitude.
   * @return A string representing the type code for the primary
   * magnitude, or null if none available.
   */
  public String getMagnitudeType()
  {
    return magnitudeType;
  }

  //EQEvent interface

  /**
   * Returns the event network ID in lower case.
   * @return the event network ID in lower case.
   */
  private String netId = null;
  public String getNetID()
  {
    if(netId == null)
    {
      netId = dataSource.toLowerCase();
      if(netId.equals(dataSource))
        netId = dataSource;
    }
    return netId;
  }

//  /**
//   * Returns the event ID code.  This is the same as the event-ID key
//   * except that any leading data-source code is removed.
//   * @return The event ID code.
//   */
//  public String getEventID()    //now defined in 'QWIdentDataMsgRecord'
//  {
//    return eventID;
//  }
//
//  /**
//   * Returns the event ID key (usually including a leading data-source
//   * code).
//   * @return The event ID key.
//   */
//  public String getEventIDKey() //now defined in 'QWIdentDataMsgRecord'
//  {
//    return eventIDKey;
//  }

  /**
   * Returns the event latitude.
   * @return the event latitude.
   */
  public double getLatitude()
  {
    return latitude;
  }

  /**
   * Returns the event longitude.
   * @return the event longitude.
   */
  public double getLongitude()
  {
    return longitude;
  }

  /**
   * Returns the event magnitude.
   * @return The event magnitude, or 0.0 if none available.
   */
  public double getMagnitude()
  {
    if(magnitudeVal == null)
      return 0.0;
    return magnitudeVal.doubleValue();
  }

  /**
   * Returns a 'Double' object containing the event magnitude.
   * @return A 'Double' object containing the event magnitude, or null
   * if none available.
   */
  public Double getMagnitudeObj()
  {
    return magnitudeVal;
  }

  /**
   * Returns an 'Integer' object containing the number of stations for
   * the current magnitude.
   * @return An 'Integer' object containing the number of stations for
   * the current magnitude, or null if none available.
   */
  public Integer getMagNumStationsObj()
  {
    return magNumStationsVal;
  }

  /**
   * Returns an 'Double' object containing the standard error for the
   * current magnitude.
   * @return An 'Double' object containing the standard error for the
   * current magnitude, or null if none available.
   */
  public Double getMagStandardErrorObj()
  {
    return magStandardErrorVal;
  }

  /**
   * Returns a the contents of the first "Comment|Text" child-element
   * for the current magnitude.
   * @return The contents of the first "Comment|Text" child-element
   * for the current magnitude, or null if none available.
   */
  public String getFirstMagCommentText()
  {
    return firstMagCommentText;
  }

  /**
   * Returns the event time.
   * @return the event time.
   */
  public Date getTime()
  {
    return time;
  }

  /**
   * Returns true if the event should trump other events.
   * @return true if the event should trump other events.
   */
  public boolean getTrump()
  {
    return trump;
  }

  /**
   * Sets the trump flag.
   * @param b the flag value to use.
   */
  public void setTrump(boolean b)
  {
    trump = b;
  }

  /**
   * Returns the event's horizontal-error value (kilometers).
   * @return A 'Double' object containing the event's horizontal-error
   * value, or null if not available.
   */
  public Double getHorizontalErrorValObj()
  {
    switch(messageFormatSpec)
    {  //get value depending on format specification
      case MFMT_QUAKEML:     //QuakeML message format
                   //return 'quality'|'anss:errh' value:
        final Double dObj;             //(convert meters to km)
        return ((dObj=getChildElementDouble(
             MsgTag.QUALITY,MsgTag.QUAKEML_ERRH,MsgTag.ANSS_STR)) != null) ?
                               Double.valueOf(dObj.doubleValue()/1000.0) : null;
      case MFMT_ANSSEQXML:   //ANSS-EQ-XML message format
        return getOptElementDouble(MsgTag.ERRH);
      default:               //QuakeWatch message format
        return getOptAttribDouble(MsgTag.HORIZONTAL_ERROR);
    }
  }

  /**
   * Returns a display string for the event's horizontal-error value,
   * including the units tag.
   * @param zeroValStr a string to be returned if the value is less
   * than or equal to zero, or null if a display string for the value
   * should be returned.
   * @return A display string for the event's horizontal-error value,
   * or null if not available.
   */
  public String getHorizontalErrorDispStr(String zeroValStr)
  {
    return buildDistanceDispStr(getHorizontalErrorValObj(),zeroValStr);
  }

  /**
   * Returns a display string for the event's horizontal-error value,
   * including the units tag.
   * @return A display string for the event's horizontal-error value,
   * or null if not available.
   */
  public String getHorizontalErrorDispStr()
  {
    return getHorizontalErrorDispStr(null);
  }

  /**
   * Returns the event's vertical-error value (kilometers).
   * @return A 'Double' object containing the event's vertical-error
   * value, or null if not available.
   */
  public Double getVerticalErrorValObj()
  {
    switch(messageFormatSpec)
    {  //get value depending on format specification
      case MFMT_QUAKEML:     //QuakeML message format
                   //return 'quality'|'anss:errz' value:
        final Double dObj;             //(convert meters to km)
        return ((dObj=getChildElementDouble(
             MsgTag.QUALITY,MsgTag.QUAKEML_ERRZ,MsgTag.ANSS_STR)) != null) ?
                               Double.valueOf(dObj.doubleValue()/1000.0) : null;
      case MFMT_ANSSEQXML:   //ANSS-EQ-XML message format
        return getOptElementDouble(MsgTag.ERRZ);
      default:               //QuakeWatch message format
        return getOptAttribDouble(MsgTag.VERTICAL_ERROR);
    }
  }

  /**
   * Returns a display string for the event's vertical-error value,
   * including the units tag.
   * @param zeroValStr a string to be returned if the value is less
   * than or equal to zero, or null if a display string for the value
   * should be returned.
   * @return A display string for the event's vertical-error value,
   * or null if not available.
   */
  public String getVerticalErrorDispStr(String zeroValStr)
  {
    return buildDistanceDispStr(getVerticalErrorValObj(),zeroValStr);
  }

  /**
   * Returns a display string for the event's vertical-error value,
   * including the units tag.
   * @return A display string for the event's vertical-error value,
   * or null if not available.
   */
  public String getVerticalErrorDispStr()
  {
    return getVerticalErrorDispStr(null);
  }

  /**
   * Returns a 'Double' object containing the event's RMS-time-error value.
   * @return A 'Double' object containing the event's RMS-time-error value,
   * or null if not available.
   */
  public Double getRMSTimeErrorObj()
  {
    switch(messageFormatSpec)
    {  //get value depending on format specification
      case MFMT_QUAKEML:     //QuakeML message format
                   //return 'quality'|'standardError' value:
        return getChildElementDouble(MsgTag.QUALITY,MsgTag.STANDARD_ERROR);
      case MFMT_ANSSEQXML:   //ANSS-EQ-XML message format
        return getOptElementDouble(MsgTag.STD_ERROR);
      default:               //QuakeWatch message format
        return getOptAttribDouble(MsgTag.RMS_TIME_ERROR);
    }
  }

  /**
   * Returns a display string for the event's RMS-time-error value,
   * including the units tag.
   * @param zeroValStr a string to be returned if the value is less
   * than or equal to zero, or null if a display string for the value
   * should be returned.
   * @return A display string for the event's RMS-time-error value,
   * or null if not available.
   */
  public String getRMSTimeErrorDispStr(String zeroValStr)
  {
    final Double valObj;
    if((valObj=getRMSTimeErrorObj()) == null)
      return null;      //if no matching value then return null
    final double val = valObj.doubleValue();
    if(zeroValStr != null && val <= 0.0)    //if str given and <= 0 then
      return zeroValStr;                    //return 'zero' str
    if(rmsTimeErrorNumberFormat == null)    //create formatter if nec
      rmsTimeErrorNumberFormat = UtilFns.createFloatNumberFormat(2);
    synchronized(rmsTimeErrorNumberFormat)
    {    //only allow thread at a time to use number-format object
      return rmsTimeErrorNumberFormat.format(val) + SECS_DSTR;
    }
  }

  /**
   * Returns a display string for the event's RMS-time-error value,
   * including the units tag.
   * @return A display string for the event's RMS-time-error value,
   * or null if not available.
   */
  public String getRMSTimeErrorDispStr()
  {
    return getRMSTimeErrorDispStr(null);
  }

  /**
   * Returns a 'Double' containing the event's azimuthal-gap value.
   * @return A 'Double' containing the event's azimuthal-gap value,
   * or null if not available.
   */
  public Double getAzimuthalGapObj()
  {
    switch(messageFormatSpec)
    {  //get value depending on format specification
      case MFMT_QUAKEML:     //QuakeML message format
                   //return 'quality'|'azimuthalGap' value:
        return getChildElementDouble(
                               MsgTag.QUALITY,MsgTag.QUAKEML_AZIMUTHAL_GAP);
      case MFMT_ANSSEQXML:   //ANSS-EQ-XML message format
        return getOptElementDouble(MsgTag.AZIM_GAP);
      default:               //QuakeWatch message format
        return getOptAttribDouble(MsgTag.AZIMUTHAL_GAP);
    }
  }

  /**
   * Returns a display string for the event's azimuthal-gap value,
   * including the units tag.
   * @param zeroValStr a string to be returned if the value is less
   * than or equal to zero, or null if a display string for the value
   * should be returned.
   * @return A display string for the event's azimuthal-gap value,
   * or null if not available.
   */
  public String getAzimuthalGapDispStr(String zeroValStr)
  {
    final Double valObj;
    if((valObj=getAzimuthalGapObj()) == null)
      return null;      //if no matching value then return null
    final int val = (int)(valObj.doubleValue() + 0.5);
    if(zeroValStr != null && val <= 0)      //if str given and <= 0 then
      return zeroValStr;                    //return 'zero' str
    return Integer.toString(val) + DEG_DSTR + ((val != 1) ? "s" : "");
  }

  /**
   * Returns a display string for the event's azimuthal-gap value,
   * including the units tag.
   * @return A display string for the event's azimuthal-gap value,
   * or null if not available.
   */
  public String getAzimuthalGapDispStr()
  {
    return getAzimuthalGapDispStr(null);
  }

  /**
   * Returns an 'Integer' object containing the event's number-of-phases
   * value.
   * @return An 'Integer' object containing the event's number-of-phases
   * value, or null if none available.
   */
  public Integer getNumPhasesObj()
  {
    switch(messageFormatSpec)
    {  //get value depending on format specification
      case MFMT_QUAKEML:     //QuakeML message format
                   //return 'quality'|'usedPhaseCount' value:
        return getChildElementInteger(
                                    MsgTag.QUALITY,MsgTag.USED_PHASE_COUNT);
      case MFMT_ANSSEQXML:   //ANSS-EQ-XML message format
        return getOptElementInteger(MsgTag.NUM_PHA_USED);
      default:               //QuakeWatch message format
        return getOptAttribInteger(MsgTag.NUM_PHASES);
    }
  }

  /**
   * Returns a display string for the event's number-of-phases value.
   * @param zeroValStr a string to be returned if the value is less
   * than or equal to zero, or null if a display string for the value
   * should be returned.
   * @return A display string for the event's number-of-phases value,
   * or null if not available.
   */
  public String getNumPhasesDispStr(String zeroValStr)
  {
    final Integer valObj;
    if((valObj=getNumPhasesObj()) == null)
      return null;      //if no matching value then return null
    final int val = valObj.intValue();
    if(zeroValStr != null && val <= 0.0)    //if str given and <= 0 then
      return zeroValStr;                    //return 'zero' str
    return Integer.toString(val);
  }

  /**
   * Returns a display string for the event's number-of-phases value.
   * @return A display string for the event's number-of-phases value,
   * or null if not available.
   */
  public String getNumPhasesDispStr()
  {
    return getNumPhasesDispStr(null);
  }

  /**
   * Returns an 'Integer' object containing the event's number-of-stations
   * value.
   * @return An 'Integer' object containing the event's number-of-stations
   * value, or null if none available.
   */
  public Integer getNumStationsObj()
  {
    switch(messageFormatSpec)
    {  //get value depending on format specification
      case MFMT_QUAKEML:     //QuakeML message format
                   //return 'quality'|'usedStationCount' value:
        return getChildElementInteger(
                                  MsgTag.QUALITY,MsgTag.USED_STATION_COUNT);
      case MFMT_ANSSEQXML:   //ANSS-EQ-XML message format
        return getOptElementInteger(MsgTag.NUM_STA_USED);
      default:               //QuakeWatch message format
        return getOptAttribInteger(MsgTag.NUM_STATIONS);
    }
  }

  /**
   * Returns a display string for the event's number-of-stations value.
   * @param zeroValStr a string to be returned if the value is less
   * than or equal to zero, or null if a display string for the value
   * should be returned.
   * @return A display string for the event's number-of-stations value,
   * or null if not available.
   */
  public String getNumStationsDispStr(String zeroValStr)
  {
    final Integer valObj;
    if((valObj=getNumStationsObj()) == null)
      return null;      //if no matching value then return null
    final int val = valObj.intValue();
    if(zeroValStr != null && val <= 0.0)    //if str given and <= 0 then
      return zeroValStr;                    //return 'zero' str
    return Integer.toString(val);
  }

  /**
   * Returns a display string for the event's number-of-stations value.
   * @return A display string for the event's number-of-stations value,
   * or null if not available.
   */
  public String getNumStationsDispStr()
  {
    return getNumStationsDispStr(null);
  }

  /**
   * Returns a 'Double' object containing the event's minimum-distance
   * value (in kilometers).
   * @return A 'Double' object containing the event's minimum-distance
   * value (in kilometers), or null if not available.
   */
  public Double getMinimumDistanceObj()
  {
    final Double doubleObj;
    switch(messageFormatSpec)
    {  //get value depending on format specification
      case MFMT_QUAKEML:     //QuakeML message format
                   //get 'quality'|'minimumDistance' value (in degrees):
        if((doubleObj=getChildElementDouble(
                     MsgTag.QUALITY,MsgTag.MINIMUM_DISTANCE)) == null)
        {  //'quality'|'minimumDistance' element not found
          return null;
        }
        break;
      case MFMT_ANSSEQXML:   //ANSS-EQ-XML message format
                                  //get min-distance value (in degrees):
        if((doubleObj=getOptElementDouble(MsgTag.MIN_DIST)) == null)
          return null;
        break;
      default:               //QuakeWatch message format
        return getOptAttribDouble(MsgTag.MIN_DISTANCE);
    }
              //convert degrees to kilometers and return value:
    return Double.valueOf(GisUtils.degreesToKm(doubleObj.doubleValue()));
  }

  /**
   * Returns a display string for the event's minimum-distance value
   * (in kilometers), including the units tag.
   * @param zeroValStr a string to be returned if the value is less
   * than or equal to zero, or null if a display string for the value
   * should be returned.
   * @return A display string for the event's minimum-distance value,
   * or null if not available.
   */
  public String getMinimumDistanceDispStr(String zeroValStr)
  {
    final Double doubleObj;
    return ((doubleObj=getMinimumDistanceObj()) != null) ?
            buildDistanceDispStr(doubleObj.doubleValue(),zeroValStr) : null;
  }

  /**
   * Returns a display string for the event's minimum-distance value
   * (in kilometers), including the units tag.
   * @return A display string for the event's minimum-distance value,
   * or null if not available.
   */
  public String getMinimumDistanceDispStr()
  {
    return getMinimumDistanceDispStr(null);
  }

  /**
   * Returns the location-method string for the event.
   * @return The location-method string for the event, or null if not
   * available.
   */
  public String getLocationMethod()
  {
    switch(messageFormatSpec)
    {  //get value depending on format specification
      case MFMT_QUAKEML:     //QuakeML message format
        final String mIdStr;      //get value of 'methodID' element:
        if((mIdStr=getOptElementStr(MsgTag.METHOD_ID)) != null)
        {  //'methodID' element value exists; parse as resource identifier
          final ResourceIdentifier resIdObj = new ResourceIdentifier(mIdStr);
                             //if 'local-id' parsed out then return it,
          final String str;  // otherwise return entire value:
          return ((str=resIdObj.getLocalIDStr()) != null) ? str : mIdStr;
        }
        return null;
      case MFMT_ANSSEQXML:   //ANSS-EQ-XML message format
        final Element methElem;
        if((methElem=currentElement.getChild(
                      MsgTag.METHOD,currentElement.getNamespace())) == null)
        {  //no "Method" child element found
          return null;
        }
                //return value of "Method" | "Algorithm" element:
        return methElem.getChildTextTrim(
                                  MsgTag.ALGORITHM,methElem.getNamespace());
      default:               //QuakeWatch message format
        return getOptAttribStr(MsgTag.LOCATION_METHOD);
    }
  }

  /**
   * Returns the contents of the QuakeML "origin | comment | text" element
   * (with 'id' attribute ending with "CUBE_Code#locMeth" or the first
   * ANSS-EQ-XML first "Origin | Method | Comment | Text" element for
   * the event.
   * @return The method-comment text, or null if not found (or not QuakeML
   * or ANSS-EQ-XML format).
   */
  public String getMethodCommentText()
  {
    switch(messageFormatSpec)
    {  //get value depending on format specification
      case MFMT_QUAKEML:     //QuakeML message format
                   //get list of 'comment' elements:
        final Iterator iterObj = currentElement.getChildren(
           MsgTag.QUAKEML_COMMENT,currentElement.getNamespace()).iterator();
        Object obj;
        Element elemObj;
        ResourceIdentifier resIdObj;
        while(iterObj.hasNext())
        {  //for each item in list
          if((obj=iterObj.next()) instanceof Element)
          {  //'comment' element fetched OK
            elemObj = (Element)obj;
                   //get 'id' attribute and parse as resource identifier:
            resIdObj = new ResourceIdentifier(
                                  elemObj.getAttributeValue(MsgTag.ID_STR));
            if(MsgTag.CUBECODE_STR.equalsIgnoreCase(
                                             resIdObj.getResourceIDStr()) &&
                                           MsgTag.LOC_METH.equalsIgnoreCase(
                                                  resIdObj.getLocalIDStr()))
            {  //'id' attribute ends with "CUBE_Code#locMeth"
                        //return value of 'comment'|'text' element:
              return elemObj.getChildText(
                                MsgTag.QUAKEML_TEXT,elemObj.getNamespace());
            }
          }
        }
        return null;
      case MFMT_ANSSEQXML:   //ANSS-EQ-XML message format
        final Element methodElem,commentElem;
        if((methodElem=currentElement.getChild(
                    MsgTag.METHOD,currentElement.getNamespace())) != null &&
                                           (commentElem=methodElem.getChild(
                         MsgTag.COMMENT,methodElem.getNamespace())) != null)
        {    //child elements found
          return commentElem.getChildText(
                                    MsgTag.TEXT,commentElem.getNamespace());
        }
        return null;
      default:               //QuakeWatch message format
        return null;
    }
  }

  /**
   * Returns the 'Event'|'Usage' value for the event.
   * @return The 'Event'|'Usage' value for the event, or null if not
   * available.
   */
  public String getEventUsage()
  {
    switch(messageFormatSpec)
    {  //get value depending on format specification
      case MFMT_QUAKEML:     //QuakeML message format
              //return value of 'quakeMessage'|'usage' element:
        final Element elemObj;    // (use namespace from 'event' element)
        if((elemObj=dataMsgElement.getChild(
                 MsgTag.QUAKE_MESSAGE,eventElement.getNamespace())) != null)
        {  //'quakeMessage' element fetched OK; return value of 'usage'
          return elemObj.getChildTextTrim(
                               MsgTag.QUAKEML_USAGE,elemObj.getNamespace());
        }
        return null;
      case MFMT_ANSSEQXML:   //ANSS-EQ-XML message format
              //return value of 'Event'|'Usage' element:
        return eventElement.getChildTextTrim(
                                   MsgTag.USAGE,eventElement.getNamespace());
      default:               //QuakeWatch message format
        return null;
    }
  }

  /**
   * Returns the 'Event'|'Scope' value for the event.
   * @return The 'Event'|'Scope' value for the event, or null if not
   * available.
   */
  public String getEventScope()
  {
	return eventScope;
  }

  /**
   * Determines if the event's basic data values are valid.  The event
   * time, latitude, longitude, and magnitude (if present) are checked.
   * @return true if the event's basic data values are valid; false if not.
   */
  public boolean isEventDataValid()
  {
    if(time == null || time.getTime() < 0 ||
                                         time.getTime() > maxEventTimeValue)
    {  //event-time value missing or out of range
      return false;
    }
    if(latitude < -360 || latitude > 360 ||
                                        longitude < -360 || longitude > 360)
    {  //latitude or longitude value out of range
      return false;
    }
    if(magnitudeVal != null && (magnitudeVal.doubleValue() < -100.0 ||
                                        magnitudeVal.doubleValue() > 100.0))
    {  //magnitude value exists and is out of range
      return false;
    }
    return true;
  }

  /**
   * Sets the handle to the record that this record superseded.
   * @param recObj handle to record that this record superseded, or
   * null for none.
   */
  public void setSupersededRecordObj(QWEventMsgRecord recObj)
  {
    supersededRecordObj = recObj;
  }

  /**
   * Returns the handle to the record that this record superseded.
   * @return The handle to the record that this record superseded, or
   * null for none.
   */
  public QWEventMsgRecord getSupersededRecordObj()
  {
    return supersededRecordObj;
  }

  /**
   * Sets the time that this message record was last selected.  This
   * method is not implemented in this class.
   * @param timeVal time value, in milliseconds since 1/1/1970.
   */
  public void setSelectedTimeVal(long timeVal)
  {
  }

  /**
   * Returns the time that this message record was last selected.  This
   * method is not implemented in this class.
   * @return 0.
   */
  public long getSelectedTimeVal()
  {
    return 0;
  }

  /**
   * Updates time zone used for date/time values for all
   * event-message-record objects.
   * @param zone time zone.
   */
  public static void updateDateTimeZones(TimeZone zone)
  {
         //set time zone for date formatters:
    synchronized(dateTimeFormatterObj)
    {
      dateTimeFormatterObj.setTimeZone(zone);
    }
    synchronized(sp1DateTimeFormatterObj)
    {
      sp1DateTimeFormatterObj.setTimeZone(zone);
    }
    synchronized(dateFormatterObj)
    {
      dateFormatterObj.setTimeZone(zone);
    }
    synchronized(timeFormatterObj)
    {
      timeFormatterObj.setTimeZone(zone);
    }
  }

  /**
   * Returns a display string for the given distance value, including
   * the units tag.
   * @param val the given value to use.
   * @param zeroValStr a string to be returned if the value is less
   * than or equal to zero, or null if a display string for the value
   * should be returned.
   * @return A display string for the event's distance
   * error value, or null if not available.
   */
  public static String buildDistanceDispStr(double val, String zeroValStr)
  {
    if(zeroValStr != null && val <= 0.0)    //if str given and <= 0 then
      return zeroValStr;                    //return 'zero' str
    synchronized(oneDigitNumberFormat)
    {    //only allow thread at a time to use number-format object
      return oneDigitNumberFormat.format(val) + KM_DSTR;
    }
  }

  /**
   * Returns a display string for the given distance value, including
   * the units tag.
   * @param valObj the given value object to use.
   * @param zeroValStr a string to be returned if the value is less
   * than or equal to zero, or null if a display string for the value
   * should be returned.
   * @return A display string for the event's distance
   * error value, or null if not available.
   */
  public static String buildDistanceDispStr(Double valObj, String zeroValStr)
  {
    return (valObj != null) ?
               buildDistanceDispStr(valObj.doubleValue(),zeroValStr) : null;
  }

  /**
   * Returns a display string for the given distance value, including
   * the units tag.
   * @param val the given value to use.
   * @return A display string for the event's distance
   * error value, or null if not available.
   */
  public static String buildDistanceDispStr(double val)
  {
    return buildDistanceDispStr(val,null);
  }

  /**
   * Returns a display string for the given distance value, including
   * the units tag.
   * @param valObj the given value object to use.
   * @return A display string for the event's distance
   * error value, or null if not available.
   */
  public static String buildDistanceDispStr(Double valObj)
  {
    return buildDistanceDispStr(valObj,null);
  }
}
