//QWEventMsgRecord.java:  Defines the interface to a record of data
//                        for one event.
//
//  5/19/2005 -- [ET]  Initial version.
//   6/2/2005 -- [ET]  Added methods 'get/setSupersededRecordObj()' and
//                     'get/setShowMapSymbolFlag()'.
//   8/5/2005 -- [ET]  Added methods 'containsShakeMapProduct()' and
//                     'containsTsunamiProduct()'.
//  5/17/2006 -- [ET]  Added 'get/setUtilityObject()' methods.
//   6/7/2006 -- [ET]  Moved "..._SORT_TYPE" definitions to this class
//                     (from 'QWEQEventMsgRecord' class) and renamed
//                     'MAG_SORT_TYPE' to 'VALUE_SORT_TYPE'; added
//                     "get/setSelectedTimeVal()" methods.
//  5/12/2011 -- [ET]  Added 'getLCEventIDKey()' method.
//

package com.isti.quakewatch.message;

import java.util.Date;
import org.jdom.Element;
import com.isti.util.FifoHashtable;

/**
 * QWEventMsgRecord defines a record of data for one event.
 */
public interface QWEventMsgRecord extends Comparable,QWMsgNumTimeRec
{
    /** The products sort type. */
  public static final int PROD_SORT_TYPE = 1;
    /** The date sort type. */
  public static final int DATE_SORT_TYPE = 2;
    /** The value sort type. */
  public static final int VALUE_SORT_TYPE = 3;
    /** The default sort type. */
  public static final int DEF_SORT_TYPE = DATE_SORT_TYPE;
    /** The maximum sort type. */
  public static final int MAX_SORT_TYPE = 3;

  /**
   * Returns the child element used to build the message record.
   * @return The child element used to build the message record.
   */
  public Element getMsgRecChildElement();

  /**
   * Indicates whether some other object is "equal to" this one.
   *
   * @param   obj   the reference object with which to compare.
   * @return  <code>true</code> if this object is the same as the obj
   *          argument; <code>false</code> otherwise.
   */
  public boolean equals(QWEventMsgRecord obj);

  /**
   * Indicates whether some other object is "equal to" this one.
   *
   * @param   obj   the reference object with which to compare.
   * @return  <code>true</code> if this object is the same as the obj
   *          argument; <code>false</code> otherwise.
   */
  public boolean equals(Object obj);

  /**
   * Returns true if the given object is a 'QWEventMsgRecord' object whose
   * event date/time equals the one for this record; otherwise returns
   * false.
   * @param obj the 'QWEventMsgRecord' object to use.
   * @return true if the event date/times are equal.
   */
  public boolean timeEquals(QWEventMsgRecord obj);

  /**
   * Returns true if the given object is a 'QWEventMsgRecord' object whose
   * event date/time equals the one for this record; otherwise returns
   * false.
   * @param obj the 'QWEventMsgRecord' object to use.
   * @return true if the event date/times are equal.
   */
  public boolean timeEquals(Object obj);

  /**
   * Compares the given 'QWEventMsgRecord' object to this one.
   * @param obj the 'QWEventMsgRecord' object to use.
   * @return A negative integer, zero, or a positive integer as this
   * object is less than, equal to, or greater than the specified object.
   * The event date/time is used as the sorting key.
   * @throws ClassCastException if the specified object's type prevents it
   * from being compared to this Object.
   */
  public int compareTo(Object obj) throws ClassCastException;

  /**
   * Returns the value-sort rank specifier.  This is used by the
   * 'compareTo(obj,int)' method to determine the result when
   * two different types of objects that implement 'QWEventMsgRecord'
   * are compared (using 'VALUE_SORT_TYPE').
   * @return The numeric value-sort rank specifier.
   */
  public int getValueSortRankSpecifier();

  /**
   * Returns a string representation of this record.
   * @returns A string representation of this record.
   */
  public String toString();

  /**
   * Returns a string representation of this record to be used for
   * its tooltip text.
   * @returns A string representation of this record to be used for
   * its tooltip text.
   */
  public String getToolTipString();

  /**
   * Returns a display string representation of this object (not
   * including the location note string).
   * @return A new String object.
   */
  public String getDisplayString();

  /**
   * Returns a display string representation of the date.
   * @return A display string.
   */
  public String getDateString();

  /**
   * Returns a display string representation of the date/time.
   * @return A display string.
   */
  public String getDateTimeString();

  /**
   * Returns a display string representation of the time.
   * @return A display string.
   */
  public String getTimeString();

  /**
   * Returns a display string representation of the latitude.
   * @param nsFormatFlag true for N/S format; false for +/- format.
   * @param degMinFlag true for degrees/minutes format; false for
   * decimal format.
   * @return A display string.
   */
  public String getLatitudeString(boolean nsFormatFlag, boolean degMinFlag);

  /**
   * Returns a display string representation of the latitude.  The
   * decimal format is used.
   * @param nsFormatFlag true for N/S format; false for +/- format.
   * @return A display string.
   */
  public String getLatitudeString(boolean nsFormatFlag);

  /**
   * Returns a display string representation of the latitude.  The decimal
   * +/- format is used.
   * @return A display string.
   */
  public String getLatitudeString();

  /**
   * Returns a display string representation of the longitude.
   * @param ewFormatFlag true for E/W format; false for +/- format.
   * @param degMinFlag true for degrees/minutes format; false for
   * decimal format.
   * @return A display string.
   */
  public String getLongitudeString(boolean ewFormatFlag, boolean degMinFlag);

  /**
   * Returns a display string representation of the longitude.  The
   * decimal format is used.
   * @param ewFormatFlag true for E/W format; false for +/- format.
   * @return A display string.
   */
  public String getLongitudeString(boolean ewFormatFlag);

  /**
   * Returns a display string representation of the longitude.  The
   * decimal +/- format is used.
   * @return A display string.
   */
  public String getLongitudeString();

  /**
   * Sets the "Location Note" value for this event record.
   * @param str the "Location Note" value to use.
   */
  public void setLocationNote(String str);

  /**
   * Returns the "Location Note" value for this event record.
   * @return A string containing the "Location Note" value.
   */
  public String getLocationNote();

  /**
   * Sets the "requested" flag.
   * @param flgVal the flag value to set.
   */
  public void setRequestedFlag(boolean flgVal);

  /**
   * Returns the "requested" flag.
   * @return the value of the flag.
   */
  public boolean getRequestedFlag();

  /**
   * Sets the display object associated with this event record.
   * @param obj the object to use.
   */
  public void setDisplayObject(Object obj);

  /**
   * Returns the display object associated with this event record.
   * @return The display object.
   */
  public Object getDisplayObject();

  /**
   * Sets the utility object associated with this event record.
   * @param obj the object to use.
   */
  public void setUtilityObject(Object obj);

  /**
   * Returns the utility object associated with this event record.
   * @return The utility object.
   */
  public Object getUtilityObject();

  /**
   * Returns a 'QWProductMsgRecord' object for this event record.
   * @param typeStr the 'type' value of the 'QWProductMsgRecord' object
   * to be returned.
   * @return A 'QWProductMsgRecord' object, or null if a matching object
   * is not found.
   */
  public QWProductMsgRecord getProductMsgRecord(String typeStr);

  /**
   * Returns the table of 'QWProductMsgRecord' objects for products
   * associated with this event.
   * @return A FifoHashtable of 'QWProductMsgRecord' objects, or null
   * if none are available.
   */
  public FifoHashtable getProductRecTable();

  /**
   * Returns the number of products associated with this event.
   * @return The number of products associated with this event.
   */
  public int getProductCount();

  /**
   * Returns the ShakeMap information for the event.
   * @return ShakeMap info or null if not available.
   */
  public String getShakeMapInfo();

  /**
   * Returns a flag indicating whether or not this event contains a
   * ShakeMap product.
   * @return true if this event contains a ShakeMap product; false
   * if not.
   */
  public boolean containsShakeMapProduct();

  /**
   * Returns a flag indicating whether or not this event contains a
   * Tsunami product.
   * @return true if this event contains a Tsunami product; false
   * if not.
   */
  public boolean containsTsunamiProduct();

  /**
   * Sets the 'update' flag to indicate whether or not this record is an
   * update to a previously existing event record.
   * @param flgVal true if this record is an update, false if it is a new
   * event record.
   */
  public void setUpdateFlag(boolean flgVal);

  /**
   * Returns true if this record is an update to a previously existing
   * event record, false if it is a new event record.
   * @return true if this record is an update.
   */
  public boolean getUpdateFlag();

  /**
   * Sets the 'alarmTriggered' flag to indicate whether or not an alarm
   * has been triggered for this record.
   * @param flgVal true if an alarm has been triggered for this record,
   * false if not.
   */
  public void setAlarmTriggeredFlag(boolean flgVal);

  /**
   * Returns the 'alarmTriggered' flag to indicate whether or not an alarm
   * has been triggered for this record.
   * @return true if an alarm has been triggered for this record,
   * false if not.
   */
  public boolean getAlarmTriggeredFlag();

  /**
   * Sets the 'showMapSymbolFlag' flag to indicate whether or not the
   * symbol for this message should be shown on the map.  If this
   * method is not called then then 'showMapSymbolFlag' flag will
   * default to 'true' (symbol shown on map).
   * @param flgVal true if the symbol for this message should be shown
   * on the map; false if not.
   */
  public void setShowMapSymbolFlag(boolean flgVal);

  /**
   * Returns the 'showMapSymbolFlag' flag to indicate whether or not the
   * symbol for this message should be shown on the map.
   * @return true if the symbol for this message should be shown
   * on the map; false if not.
   */
  public boolean getShowMapSymbolFlag();

  //EQEvent interface

  /**
   * Returns the event network ID in lower case.
   * @return the event network ID in lower case.
   */
  public String getNetID();

  /**
   * Returns the event ID key (usually including a leading data-source
   * code).
   * @return The event ID key.
   */
  public String getEventIDKey();

  /**
   * Returns a lower-cased version of the event ID key (usually including
   * a leading data-source code).
   * @return The lower-cased event ID key.
   */
  public String getLCEventIDKey();

  /**
   * Returns the event data-source code.
   * @return The event data-source code.
   */
  public String getDataSource();

  /**
   * Returns the version code string for the event.
   * @return The version code string for the event, or null if none given.
   */
  public String getVersion();

  /**
   * Returns the event latitude.
   * @return the event latitude.
   */
  public double getLatitude();

  /**
   * Return the event longitude.
   * @return the event longitude.
   */
  public double getLongitude();

  /**
   * Return the event magnitude.
   * @return the event magnitude.
   */
  public double getMagnitude();

  /**
   * Returns a 'Double' object containing the event magnitude.
   * @return A 'Double' object containing the event magnitude, or null
   * if none available.
   */
  public Double getMagnitudeObj();

  /**
   * Returns a display string representation of the magnitude.
   * @return A display string.
   */
  public String getMagnitudeValueString();

  /**
   * Returns a string representing the type code for the primary
   * magnitude.
   * @return A string representing the type code for the primary
   * magnitude, or null if none available.
   */
  public String getMagnitudeType();

  /**
   * Returns the event depth value.
   * @return A 'Double' containing the event depth value, or null if
   * none available.
   */
  public Double getDepthObj();

  /**
   * Returns a display string representation of the depth.
   * @param milesFlag true to convert the depth value from kilometers
   * to miles; false to leave the depth value unchanged.
   * @return A display string.
   */
  public String getDepthString(boolean milesFlag);

  /**
   * Returns a display string representation of the depth.
   * @return A display string.
   */
  public String getDepthString();

  /**
   * Return the event time.
   * @return the event time.
   */
  public Date getTime();

  /**
   * Returns true if the event is verified.
   * @return  true if the event is verified.
   */
  public boolean isVerified();

  /**
   * Returns true if the "quarry" flag is set.
   * @return true if the "quarry" flag is set.
   */
  public boolean isQuarry();

  /**
   * Sets the handle to the record that this record superseded.
   * @param recObj handle to record that this record superseded, or
   * null for none.
   */
  public void setSupersededRecordObj(QWEventMsgRecord recObj);

  /**
   * Returns the handle to the record that this record superseded.
   * @return The handle to the record that this record superseded, or
   * null for none.
   */
  public QWEventMsgRecord getSupersededRecordObj();

  /**
   * Sets the time that this message record was last selected.
   * @param timeVal time value, in milliseconds since 1/1/1970.
   */
  public void setSelectedTimeVal(long timeVal);

  /**
   * Returns the time that this message record was last selected.
   * @return The time that this message record was last selected (in
   * milliseconds since 1/1/1970), or 0 if this record was never
   * selected or if this method is not implemented.
   */
  public long getSelectedTimeVal();
}
