//QWIdentDataMsgRecord.java:  Defines a base-record of functionality for
//                            records that use an "Identifier" element
//                            (or an ANSS-EQ-XML-format "Event" element).
//
//  9/20/2002 -- [ET]  Initial version.
//  6/18/2003 -- [ET]  Added 'set/getRequestedFlag()' methods.
//  6/19/2003 -- [ET]  Changed 'msgNumber' references from 'int' to 'long';
//                     modified 'equals()' methods to ignore case of
//                     event ID and data source.
// 10/24/2003 -- [ET]  Modified (via imports) to reference 'QWCommon'
//                     versions of  'QWDataMsgRecord' and
//                     'QWRecordException' instead of local versions.
// 11/17/2003 -- [ET]  Deleted import to 'QWCommon' version of
//                     'QWDataMsgRecord' to use local version instead.
//  3/18/2004 -- [ET]  Moved from 'QWClient' to 'QWCommon' project.
//  5/16/2005 -- [ET]  Moved 'set/getRequestedFlag()' methods from this
//                     class to 'QWDataMsgRecord'; moved 'getEventID()'
//                     and 'getEventIDKey()' methods from 'QWEventMsgRecord'
//                     to this class; added 'getVersion()' method; added
//                     'getDataSource()' method.
//  9/13/2006 -- [ET]  Added support for ANSS-EQ-XML format.
//  3/30/2010 -- [ET]  Added support for QuakeML format.
//  5/12/2010 -- [ET]  Modified to recognize event 'publicID' format
//                     associated with non-'quakeMessage' messages.
//  5/12/2011 -- [ET]  Added 'getLCEventIDKey()' method.
//  6/19/2013 -- [ET]  Added 'isShakeMapProduct()' method.
//

package com.isti.quakewatch.message;

import java.util.Date;
import org.jdom.Element;
import com.isti.util.UtilFns;
import com.isti.quakewatch.common.MsgTag;
import com.isti.quakewatch.util.ResourceIdentifier;

/**
 * QWIdentDataMsgRecord defines a base-record of functionality for
 * records that use an "Identifier" element (or an ANSS-EQ-XML-format
 * "Event" element)..
 */
public abstract class QWIdentDataMsgRecord extends QWDataMsgRecord
{
    /** Identification key string for event (includes data source code). */
  public final String eventIDKey;
    /** Identification string for event (not including data source code). */
  public final String eventID;
    /** Data source code string for event. */
  public final String dataSource;
    /** Version code string for event, or null if none given. */
  public final String version;
    /** Handle/indicator for lower-cased version of event-ID string. */
  protected final String lcEventIDKeyStr;

    /** Specifier for message-format type (one of 'MFMT_...' values). */
  public final int messageFormatSpec;

    /** Specifier index for "QWmessage" format for 'messageFormatSpec'. */
  public static final int MFMT_QWMESSAGE = 0;
    /** Specifier index for ANSS-EQ-XML format for 'messageFormatSpec'. */
  public static final int MFMT_ANSSEQXML = 1;
    /** Specifier index for QuakeML format for 'messageFormatSpec'. */
  public static final int MFMT_QUAKEML = 2;


  /**
   * Creates a data record that contains event-identification information,
   * loading data from the given elements.
   * @param qwMsgElement the XML "QWmessage" element object.
   * @param dataMsgElement the XML "DataMessage" element object.
   * @param eventElement the XML "Event" element object or the XML
   * element that contains the "Identifier" element.
   * @param messageFormatSpec specifier for message-format type (one of
   * the 'MFMT_...' values).
   * @param versionStr the version string to be used, or null to retrieve
   * version string from the 'eventElement' object.
   * @throws QWRecordException if an error occurs while creating the
   * data record.
   */
  public QWIdentDataMsgRecord(Element qwMsgElement, Element dataMsgElement,
                                Element eventElement, int messageFormatSpec,
                                 String versionStr) throws QWRecordException
  {
    super(qwMsgElement,dataMsgElement);     //construct parent objects
    if(eventElement == null)
      throw new QWRecordException("Null element object handle");
    this.messageFormatSpec = messageFormatSpec;
    switch(messageFormatSpec)
    {  //process data depending on format specification
      case MFMT_QUAKEML:     //data is QuakeML message format
              //setup to get data-fields from "event" element:
        currentElement = eventElement;
        final ResourceIdentifier resIdObj;
        if(MsgTag.USE_CONTROL.equalsIgnoreCase(currentElement.getName()))
        {  //given element is named "useControl"
                   //create resource-id object from 'elementID' value:
          resIdObj = new ResourceIdentifier(getElementStr(
                                                        MsgTag.ELEMENT_ID));
              //if 'versionStr' not given then get 'version'
              // child-element from 'useControl' elem (null if none):
          version = (versionStr != null) ? versionStr :
                                   getOptElementStr(MsgTag.QUAKEML_VERSION);
        }
        else
        {  //given element is not named "useControl"
                   //if element named "comment" then use 'id' attribute;
                   // otherwise use "publicID" attribute:
          final String nameStr = (MsgTag.QUAKEML_COMMENT.equalsIgnoreCase(
              currentElement.getName())) ? MsgTag.ID_STR : MsgTag.PUBLIC_ID;
                   //create resource-id object from "id" attribute:
          resIdObj = new ResourceIdentifier(getAttribStr(nameStr));
          if(versionStr != null)       //if 'versionStr' given then
            version = versionStr;      //use 'versionStr' value
          else
          {  //'versionStr' value not given
            final Element elemObj;     //get 'creationInfo' child element:
            elemObj = currentElement.getChild(
                        MsgTag.CREATION_INFO,currentElement.getNamespace());
                   //use 'version' elem of 'creationInfo' (or null if none):
            version = (elemObj != null) ? elemObj.getChildText(
                      MsgTag.QUAKEML_VERSION,elemObj.getNamespace()) : null;
          }
        }
                   //parse event-ID from resource-id value:
        String str;
        if((str=resIdObj.getResourceIDStr()) == null)
        {  //no event-ID found
          throw new QWRecordException("Unable to parse event-ID from '" +
              currentElement.getName() + "' element resource-id value (\"" +
                                        resIdObj.getSourceURIStr() + "\")");
        }
        if(str.length() > 6 && "event/".equalsIgnoreCase(str.substring(0,6)))
        {  //event-ID format in resource is "smi:SSSS/event/#######"
          str = str.substring(6);
        }
        else
        {
          int pos;
          if((pos=str.indexOf("/event")) > 7)
          {  //event-ID format is "smi:SSSS/out.######/###########/event/#"
            str = str.substring(0,pos);     //remove trailing text
            if((pos=str.indexOf('/')) > 9)  //if slash found after data then
              str = str.substring(0,pos);   //remove slash and data after it
            if(str.startsWith("out."))      //if found then
              str = str.substring(4);       //remove leading "out." text
          }
        }
        eventID = str;
              //parse dataSrc from resource-id; if not found then
              // use authority-id if not "local" (empty string if none):
        dataSource = ((str=resIdObj.getDataSourceStr()) != null ||
                              ((str=resIdObj.getAuthorityIDStr()) != null &&
                                 !str.equalsIgnoreCase(MsgTag.LOCAL_STR))) ?
                                                 str : UtilFns.EMPTY_STRING;
              //create EventIDKey from other data-fields:
        eventIDKey = dataSource.toLowerCase() + eventID;
        break;

      case MFMT_ANSSEQXML:   //data is ANSS-EQ-XML message format
              //setup to get data-fields from "Event" element:
        currentElement = eventElement;
              //get EventID child-element from "Event" element:
        eventID = getElementStr(MsgTag.EVENT_ID);
              //get DataSource child-element from "Event" element:
        dataSource = getElementStr(MsgTag.DATA_SOURCE);
              //create EventIDKey from other data-fields:
        eventIDKey = dataSource.toLowerCase() + eventID;
              //if 'versionStr' not given then get Version
              // child-element from "Event" elem (null if none):
        version = (versionStr != null) ? versionStr :
                                           getOptElementStr(MsgTag.VERSION);
        break;

      default:               //data is QuakeWatch message format
              //get "Identifier" element under given element:
        currentElement = getIdentifierElement(
                                       eventElement,eventElement.getName());
              //get EventIDKey attribute from "Identifier" element:
        eventIDKey = getAttribStr(MsgTag.EVENT_ID_KEY);
              //get DataSource attribute from "Identifier" element:
        dataSource = getAttribStr(MsgTag.DATA_SOURCE);
              //if 'versionStr' not given then get Version
              // child-element from "Event" elem (null if none):
        version = (versionStr != null) ? versionStr :
                                            getOptAttribStr(MsgTag.VERSION);
              //set current-element handle to given element:
        currentElement = eventElement;
              //determine the event ID:
        eventID = determineEventID();
    }
         //if to-lower-case does not change event ID then set
         // 'lcEventIDKeyStr' to event-ID string; otherwise set
         // to null so lower-case version is generated on demand
         // (and record doesn't need to store extra string):
    lcEventIDKeyStr = (eventIDKey.equals(eventIDKey.toLowerCase())) ?
                                                          eventIDKey : null;
  }

  /**
   * Creates a data record that contains event-identification information,
   * loading data from the given elements.
   * @param qwMsgElement the XML "QWmessage" element object.
   * @param dataMsgElement the XML "DataMessage" element object.
   * @param eventElement the XML "Event" element object or the XML
   * element that contains the "Identifier" element.
   * @param messageFormatSpec specifier for message-format type (one of
   * the 'MFMT_...' values).
   * @throws QWRecordException if an error occurs while creating the
   * data record.
   */
  public QWIdentDataMsgRecord(Element qwMsgElement, Element dataMsgElement,
                                Element eventElement, int messageFormatSpec)
                                                    throws QWRecordException
  {
    this(qwMsgElement,dataMsgElement,eventElement,messageFormatSpec,null);
  }

  /**
   * Creates a data record that contains event-identification information,
   * loading data from the given elements.
   * @param qwMsgElement the XML "QWmessage" element object.
   * @param dataMsgElement the XML "DataMessage" element object.
   * @param eventElement the XML "Event" element object or the XML
   * element that contains the "Identifier" element.
   * @param anssEQMsgFormatFlag true for ANSS-EQ-XML message format;
   * false for QuakeWatch message format.
   * @param versionStr the version string to be used, or null to retrieve
   * version string from the 'eventElement' object.
   * @throws QWRecordException if an error occurs while creating the
   * data record.
   * @deprecated Use version with 'messageFormatSpec' parameter.
   */
  public QWIdentDataMsgRecord(Element qwMsgElement, Element dataMsgElement,
                          Element eventElement, boolean anssEQMsgFormatFlag,
                                 String versionStr) throws QWRecordException
  {
    this(qwMsgElement,dataMsgElement,eventElement,
                    (anssEQMsgFormatFlag ? MFMT_ANSSEQXML : MFMT_QWMESSAGE),
                                                                versionStr);
  }

  /**
   * Creates a data record that contains event-identification information,
   * loading data from the given elements.
   * @param qwMsgElement the XML "QWmessage" element object.
   * @param dataMsgElement the XML "DataMessage" element object.
   * @param eventElement the XML "Event" element object or the XML
   * element that contains the "Identifier" element.
   * @param anssEQMsgFormatFlag true for ANSS-EQ-XML message format;
   * false for QuakeWatch message format.
   * @throws QWRecordException if an error occurs while creating the
   * data record.
   * @deprecated Use version with 'messageFormatSpec' parameter.
   */
  public QWIdentDataMsgRecord(Element qwMsgElement, Element dataMsgElement,
                          Element eventElement, boolean anssEQMsgFormatFlag)
                                                    throws QWRecordException
  {
    this(qwMsgElement,dataMsgElement,eventElement,
                    (anssEQMsgFormatFlag ? MFMT_ANSSEQXML : MFMT_QWMESSAGE),
                                                                      null);
  }

  /**
   * Creates a data record that uses a QuakeWatch message format
   * "Identifier" element and loads attributes from the element.
   * @param qwMsgElement the XML "QWmessage" element object.
   * @param dataMsgElement the XML "DataMessage" element object.
   * @param eventElement the XML "Event" element object or the XML
   * element that contains the "Identifier" element.
   * @throws QWRecordException if an error occurs while creating the
   * data record.
   */
  public QWIdentDataMsgRecord(Element qwMsgElement, Element dataMsgElement,
                              Element eventElement) throws QWRecordException
  {
    this(qwMsgElement,dataMsgElement,eventElement,MFMT_QWMESSAGE,null);
  }

  /**
   * Creates a data record that uses an "Identifier" element and loads
   * attributes from the element.
   * @param qwMsgElement the XML "QWmessage" element object.
   * @param dataMsgElement the XML "DataMessage" element object.
   * @param eventElement the XML element that contains the "Identifier"
   * element.
   * @param msgNumber message number from the 'QWmessage' element for
   * this message.
   * @param timeGenerated time that message was created by server (or
   * null if not given).
   * @param action "Action" string for message ("Update", "Delete").
   * @param timeReceived time that message was received at server (or
   * null if not given).
   * @param eventIDKey identification key string for event.
   * @param dataSource data source code string for event.
   * @param version version code string for event, or null if none given.
   * @throws QWRecordException if an error occurs while creating the
   * data record.
   */
  public QWIdentDataMsgRecord(Element qwMsgElement, Element dataMsgElement,
                   Element eventElement, long msgNumber, Date timeGenerated,
                        String action, Date timeReceived, String eventIDKey,
                 String dataSource, String version) throws QWRecordException
  {
    super(qwMsgElement,dataMsgElement,      //construct parent objects
                               msgNumber,timeGenerated,action,timeReceived);
    this.eventIDKey = eventIDKey;
    this.dataSource = dataSource;
    this.version = version;
    currentElement = eventElement;
    eventID = determineEventID();           //determine event-ID value
    messageFormatSpec = MFMT_QWMESSAGE;     //default to "QWmessage" format
         //if to-lower-case does not change event ID then set
         // 'lcEventIDKeyStr' to event-ID string; otherwise set
         // to null so lower-case version is generated on demand
         // (and record doesn't need to store extra string):
    lcEventIDKeyStr = (eventIDKey.equals(eventIDKey.toLowerCase())) ?
                                                          eventIDKey : null;
  }

  /**
   * @return event ID
   */
  private String determineEventID()
  {
    //if event ID key contains the data source skip it
    if (eventIDKey != null && dataSource != null &&
        eventIDKey.toLowerCase().startsWith(dataSource.toLowerCase()))
      return eventIDKey.substring(dataSource.length());
    else
      return eventIDKey;
  }

  /**
   * Indicates whether some other object is "equal to" this one.
   *
   * @param   obj   the reference object with which to compare.
   * @return  <code>true</code> if this object is the same as the obj
   *          argument; <code>false</code> otherwise.
   */
  public boolean equals(QWIdentDataMsgRecord obj)
  {
    return super.equals(obj) &&
        ((eventIDKey == null) ? obj.eventIDKey == null :
                             eventIDKey.equalsIgnoreCase(obj.eventIDKey)) &&
        ((eventID == null) ? obj.eventID == null :
                                   eventID.equalsIgnoreCase(obj.eventID)) &&
        ((dataSource == null) ? obj.dataSource == null :
                             dataSource.equalsIgnoreCase(obj.dataSource)) &&
        ((version == null) ? obj.version == null :
                                               version.equals(obj.version));
  }

  /**
   * Indicates whether some other object is "equal to" this one.
   *
   * @param   obj   the reference object with which to compare.
   * @return  <code>true</code> if this object is the same as the obj
   *          argument; <code>false</code> otherwise.
   */
  public boolean equals(Object obj)
  {
    return obj instanceof QWIdentDataMsgRecord &&
                                          equals((QWIdentDataMsgRecord)obj);
  }

  /**
   * Returns the event ID code.  This is the same as the event-ID key
   * except that any leading data-source code is removed.
   * @return The event ID code.
   */
  public String getEventID()
  {
    return eventID;
  }

  /**
   * Returns the event ID key (usually including a leading data-source
   * code).
   * @return The event ID key.
   */
  public String getEventIDKey()
  {
    return eventIDKey;
  }

  /**
   * Returns a lower-cased version of the event ID key (usually including
   * a leading data-source code).
   * @return The lower-cased event ID key.
   */
  public String getLCEventIDKey()
  {
         //if 'lcEventIDKeyStr' not null then return it; otherwise
         // generate and return lower-cased event ID on demand:
    return (lcEventIDKeyStr != null) ? lcEventIDKeyStr :
                                                   eventIDKey.toLowerCase();
  }

  /**
   * Returns the event data-source code.
   * @return The event data-source code.
   */
  public String getDataSource()
  {
    return dataSource;
  }

  /**
   * Returns the version code string for the event.
   * @return The version code string for the event, or null if none given.
   */
  public String getVersion()
  {
    return version;
  }
  
  /**
   * Determines if the given attributes indicate a ShakeMap product.
   * @param typeStr product-type value.
   * @param codeStr product-code value.
   * @param linkStr URL-link string for product, or null for none.
   * @return true if the given attributes indicate a ShakeMap product;
   * false if not.
   */
  public static boolean isShakeMapProduct(String typeStr, String codeStr,
                                                             String linkStr)
  {
    if(MsgTag.SHAKEMAP_URL.equalsIgnoreCase(typeStr) ||
                                       "ShakeMap".equalsIgnoreCase(typeStr))
    {  //record type is "ShakeMapURL" or "ShakeMap" (case insensitive)
      return true;
    }
    if(codeStr != null && UtilFns.isURLAddress(linkStr))
    {  //record has 'prodTypeCode' and record's value is a URL
      if(codeStr.equalsIgnoreCase(MsgTag.SHAKEMAP_URL) ||
                                       codeStr.equalsIgnoreCase("ShakeMap"))
      {  //'prodTypeCode' is "ShakeMapURL" or "ShakeMap"
        return true;
      }
      if(codeStr.toLowerCase().startsWith("sm") &&
                                           linkStr.indexOf("intensity") > 0)
      {  //record looks like ShakeMap product
        return true;
      }
    }
    return false;
  }
}
