//QWMagnitudeRecord.java:  A record of magnitude data.
//
//  9/18/2002 -- [ET]  Initial version.
// 10/24/2003 -- [ET]  Modified (via imports) to reference 'QWCommon'
//                     versions of  'QWMsgRecord' and 'QWRecordException'
//                     instead of local versions.
//  3/18/2004 -- [ET]  Moved from 'QWClient' to 'QWCommon' project.
//  9/12/2006 -- [ET]  Added support for ANSS-EQ-XML format; removed
//                     'numReadings' field.
//  10/4/2006 -- [ET]  Added 'firstMagCommentText' field.
//  3/29/2010 -- [ET]  Added support for QuakeML format.
//  3/13/2019 -- [KF]  Modified to use the PDL Indexer.
//

package com.isti.quakewatch.message;

import org.jdom.Element;
import com.isti.quakewatch.common.MsgTag;

/**
 * QWMagnitudeRecord defines a record of magnitude data.
 */
public class QWMagnitudeRecord extends QWRecord
{
    /** Magnitude value. */
  public final double magnitude;
    /** Type code string, or null if none given. */
  public final String type;
    /** Number-of-stations value, or null if none given. */
  public final Integer numStations;
    /** Magnitude method code string, or null if none given. */
  public final String magMethod;
    /** Standard-error value, or null if none given. */
  public final Double magError;
    /** Contents of first "Comment|Text" element, or null if none. */
  public final String firstMagCommentText;
    /** 'Version' value for ANSS-EQ-XML element, or null if none. */
  public final String version;
    /** 'PreferredFlag' value for ANSS-EQ-XML element, or null if none. */
  public final Boolean preferredFlag;

  /**
   * Creates a magnitude data record, built from an XML "Magnitude"
   * message element.
   * @param elementObj the XML "Magnitude" message element.
   * @param messageFormatSpec specifier for message-format type (one of
   * the 'QWIdentDataMsgRecord.MFMT_...' values).
   * @throws QWRecordException if an error occurs while creating
   * the data record.
   */
  public QWMagnitudeRecord(Element elementObj, int messageFormatSpec)
                                                    throws QWRecordException
  {
    super(elementObj);
    if(!MsgTag.MAGNITUDE.equalsIgnoreCase(elementObj.getName()))
    {  //element name not "Magnitude" or "magnitude"
      throw new QWRecordException("Bad element tag name (was \"" +
                                 elementObj.getName() + "\", should be \"" +
                                                  MsgTag.MAGNITUDE + "\")");
    }
    Element commentElemObj;
    switch(messageFormatSpec)
    {  //process data depending on format specification
      case QWIdentDataMsgRecord.MFMT_QUAKEML:    //QuakeML message format
              //get magnitude value from 'mag'|'value' element:
        final Element magElem;
        String str;
        if((magElem=elementObj.getChild(
                           MsgTag.MAG,elementObj.getNamespace())) == null ||
                                                  (str=magElem.getChildText(
                   MsgTag.QUAKEML_VALUE,elementObj.getNamespace())) == null)
        {
          throw new QWRecordException("Magnitude child element '" +
                 MsgTag.MAG + "'|'" + MsgTag.QUAKEML_VALUE + "' not found");

        }
              //parse magnitude from 'mag'|'value' element text:
        magnitude = parseDataDouble(MsgTag.MAG,str).doubleValue();
              //get type code string:
        type = getOptElementStr(MsgTag.QUAKEML_TYPE);
              //get number-of-stations ('stationCount') value:
        numStations = getOptElementInteger(MsgTag.STATION_COUNT);
              //get magnitude error from 'mag'|'uncertainty' element:
        magError = ((str=magElem.getChildText(
                      MsgTag.UNCERTAINTY,magElem.getNamespace())) != null) ?
                             parseDataDouble(MsgTag.UNCERTAINTY,str) : null;
              //get version from 'creationInfo'|'version' element:
        final Element mCreElem;   //get 'creationInfo' child element:
        mCreElem = elementObj.getChild(
                            MsgTag.CREATION_INFO,elementObj.getNamespace());
              //get 'version' elem of 'creationInfo' (or null if none):
        version = (mCreElem != null) ? mCreElem.getChildText(
                     MsgTag.QUAKEML_VERSION,mCreElem.getNamespace()) : null;
              //clear ANSS-EQ-XML PreferredFlag value:
        preferredFlag = null;
              //get magnitude method-id string:
        magMethod = getOptElementStr(MsgTag.METHOD_ID);
              //get contents of 'comment'|'text':
        firstMagCommentText = getChildElementStr(
                                MsgTag.QUAKEML_COMMENT,MsgTag.QUAKEML_TEXT);
        break;

      case QWIdentDataMsgRecord.MFMT_ANSSEQXML:  //ANSS-EQ-XML message format
              //get magnitude value:
        magnitude = getElementDouble(MsgTag.VALUE);
              //get type code string:
        type = getOptElementStr(MsgTag.TYPE_KEY);
              //get number-of-stations value:
        numStations = getOptElementInteger(MsgTag.NUM_STATIONS);
              //get standard error for magnitude value:
        magError = getOptElementDouble(MsgTag.ERROR);
              //get version string:
        version = getOptElementStr(MsgTag.VERSION);
              //get PreferredFlag value:
        preferredFlag = getOptAttribBoolean(MsgTag.PREFERRED_FLAG);
              //get magnitude method algorithm string:
        final Element methElemObj;
        magMethod = ((methElemObj=elementObj.getChild(
                        MsgTag.METHOD,elementObj.getNamespace())) != null) ?
                                                   methElemObj.getChildText(
                        MsgTag.ALGORITHM,methElemObj.getNamespace()) : null;
              //get contents of "Comment|Text":
        firstMagCommentText = ((commentElemObj=elementObj.getChild(
                       MsgTag.COMMENT,elementObj.getNamespace())) != null) ?
                                                commentElemObj.getChildText(
                          MsgTag.TEXT,commentElemObj.getNamespace()) : null;
        break;

      default:               //data is QuakeWatch message format
              //get magnitude value:
        magnitude = getAttribDouble(MsgTag.VALUE);
              //get type code string:
        type = getOptAttribStr(MsgTag.TYPE);
              //get number-of-stations value:
        numStations = getOptAttribInteger(MsgTag.NUM_STATIONS);
              //get magnitude method code string:
        magMethod = getOptAttribStr(MsgTag.MAG_METHOD);
              //get standard error for magnitude value:
        magError = getOptAttribDouble(MsgTag.MAG_ERROR);
              //clear ANSS-EQ-XML variables:
        version = null;
        preferredFlag = null;
        firstMagCommentText = null;
    }
  }

  /**
   * Creates a magnitude data record, built from an XML "Magnitude"
   * message element.
   * @param elementObj the XML "Magnitude" message element.
   * @param anssEQFlag true to interpret message element as ANSS-EQ-XML
   * format; false interpret message element as "QuakeWatch" format.
   * @throws QWRecordException if an error occurs while creating
   * the data record.
   * @deprecated Use version with 'messageFormatSpec' parameter.
   */
  public QWMagnitudeRecord(Element elementObj, boolean anssEQFlag)
                                                    throws QWRecordException
  {
    this(elementObj,(anssEQFlag ? QWIdentDataMsgRecord.MFMT_ANSSEQXML :
                                      QWIdentDataMsgRecord.MFMT_QWMESSAGE));
  }

  /**
   * Creates a magnitude data record, built from an XML "Magnitude"
   * message element.
   * @param elementObj the XML "Magnitude" message element.
   * @throws QWRecordException if an error occurs while creating
   * the data record.
   */
  public QWMagnitudeRecord(Element elementObj) throws QWRecordException
  {
    this(elementObj,QWIdentDataMsgRecord.MFMT_QWMESSAGE);
  }

  /**
   * Indicates whether some other object is "equal to" this one.
   * @param   obj   the reference object with which to compare.
   * @return  <code>true</code> if this object is the same as the obj
   *          argument; <code>false</code> otherwise.
   */
  public boolean equals(QWMagnitudeRecord obj)
  {
    return super.equals(obj) && (magnitude == obj.magnitude) &&
              ((type == null) ? obj.type == null : type.equals(obj.type)) &&
                          ((numStations == null) ? obj.numStations == null :
                                     numStations.equals(obj.numStations)) &&
                              ((magMethod == null) ? obj.magMethod == null :
                                         magMethod.equals(obj.magMethod)) &&
                                ((magError == null) ? obj.magError == null :
                                             magError.equals(obj.magError));
  }

  /**
   * Indicates whether some other object is "equal to" this one.
   * @param   obj   the reference object with which to compare.
   * @return  <code>true</code> if this object is the same as the obj
   *          argument; <code>false</code> otherwise.
   */
  public boolean equals(Object obj)
  {
    return obj instanceof QWMagnitudeRecord && equals((QWMagnitudeRecord)obj);
  }
}
