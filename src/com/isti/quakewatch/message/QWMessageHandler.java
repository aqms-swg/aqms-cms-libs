//QWMessageHandler.java:  Manages the request-resend-from-server aspects
//                        of message handling.
//
//  11/5/2003 -- [ET]  Initial version.
// 11/26/2003 -- [ET]  Modified 'processFetchedMessage()' to create
//                     normalized XML-element string.
//  1/14/2004 -- [ET]  Removed 'setLastMsgRecordObj()' and modified to
//                     handle last-received-message tracking internally;
//                     modified message-number tracking to deal with
//                     event-message filtering; added support for
//                     'debugMissedMsgTestValue' parameter.
//  1/29/2004 -- [ET]  Added call to 'clearLastReceivedValues()' in
//                     'fetchAndProcInitMessagesFromServer()' method to
//                     prevent false missed messages because msgNums
//                     changed via change to connected server.
//  2/16/2004 -- [ET]  Modified to use local date-format objects in a
//                     thread-safe manner.
//  3/31/2004 -- [ET]  Replaced 'setAliveMsgListener()' with add/remove
//                     alive-msg-listener methods and enhanced
//                     implementation; extracted 'QWMessageHandler' class
//                     into a separate file.
//  8/24/2004 -- [ET]  Modified 'setProcessingEnabledFlag()' method's
//                     processing of queued messages to discard obsolete
//                     messages (ones with "old" message numbers).
//   9/1/2004 -- [KF]  Added authentication.
//  9/17/2004 -- [KF]  Modified to get the certificate from the server
//                     instead of the 'certFile' property.
//  9/21/2004 -- [KF]  Modified to skip messages with invalid/missing
//                     signatures at the start of message processing
//                     and also skip status messages.
//  9/21/2004 -- [ET]  Improved signature-validation log messages and
//                     lowered log level on per-validated-message log
//                     outputs.
// 10/22/2004 -- [ET]  Modified invoking of alive-message listeners in
//                     'processStatusMessage()' to use 'ModIterator'
//                     to allow listeners to remove themselves.
// 10/25/2004 -- [ET]  Modified 'fetchAndProcessMessagesFromServer()' to
//                     always refresh "user" error message in dialog popup.
//  1/27/2004 -- [ET]  Added tracking of feeder-data-source information for
//                     received messages and use of "requestSourced...()"
//                     QWServices methods when fetching messages from a
//                     "new" server.
//   6/27/2005 - [ET]  Modified 'fetchAndProcInitMessagesFromServer()'
//                     method to fix issue where switching to new server
//                     could result in already-received non-event-update
//                     messages being re-requested because they are not held
//                     in memory as discrete messages (modified to only
//                     clear last-received values when changing servers).
//  9/13/2005 -- [ET]  Added methods 'isProcessingEnabled()',
//                     'getLastReceivedMsgValues()' and public version
//                     of 'doFetchAndProcessMessagesFromServer()';
//                     increased debug log level on log outputs generated
//                     during message fetch and process; added methods
//                     'fireConnectionStatusChanged()' and
//                     'isConnectionValidated()'.
//  9/20/2005 -- [ET]  Removed time check vs. "maxServerEventAge" from
//                     'doFetchAndProcessMessagesFromServer()' method
//                     (because it interfered with message polling from
//                     web-services server when 'maxServerEventAge'==0).
//  12/1/2005 -- [ET]  Modified to never clear 'lastReceivedTimeGenerated'
//                     value (to keep it available for fetching messages,
//                     even after server switches); modified to that
//                     consecutive message fetches with no messages ever
//                     received will each use time value of previous
//                     fetch; renamed method 'clearLastReceivedValues()'
//                     to 'clearLastReceivedMsgNum()'; added method
//                     'getLastFetchMsgsFromServerTime()'.
//   1/9/2006 -- [ET]  Removed calls to 'clearLastReceivedMsgNum()' from
//                     'doFetchAndProcessMessagesFromServer()' method to
//                     prevent connection-close during method from clearing
//                     the last-received message number (resulting in
//                     next fetch from server using time value only); added
//                     log-debug2 output to 'terminateFetchAndProcessMsgs()'
//                     method.
// 10/23/2006 -- [ET]  Modified 'queueProcessDataMessage()' to check for
//                     duplicate messages in waiting-messages queue and
//                     remove them if found.
// 11/13/2006 -- [ET]  Modified default value of 'processingEnabledFlag'
//                     to be 'false'.
//  8/29/2007 -- [ET]  Modified to use IstiXmlUtils 'elementToFixedString()'
//                     method (which processes incoming message-elements so
//                     that whitespace between elements is removed and
//                     control characters within elements are encoded to
//                     "&##;" strings).
//  2/29/2008 -- [ET]  Added 'invokeAliveMsgListeners()' method.
//  5/28/2008 -- [ET]  Modified 'queueProcessDataMessage()' method to make
//                     it skip missed-message checking if message is queued
//                     (check will happen when message is unqueued); added
//                     'clearWaitingMsgsQueueTable()' method.
//   6/4/2008 -- [ET]  Added method 'isLastReceivedMsgNumZero()' and
//                     optional 'setMsgTimeCurrentFlag' parameter to
//                     method 'clearLastReceivedMsgNum()'.
//  8/23/2010 -- [ET]  Added 'processMessage(String,boolean)' method.
// 12/10/2021 -- [KF]  Modified TimeGenerated check to be < instead of <=.
//

package com.isti.quakewatch.message;

import java.util.List;
import java.util.Vector;
import java.util.Iterator;
import java.util.Date;
import java.text.DateFormat;
import java.security.cert.X509Certificate;
import java.security.cert.X509CRL;
import org.jdom.Element;
import com.isti.openorbutil.MessageProcessor;
import com.isti.util.UtilFns;
import com.isti.util.LogFile;
import com.isti.util.FifoHashtable;
import com.isti.util.IstiXmlUtils;
import com.isti.util.IstiDialogInterface;
import com.isti.util.ProgressIndicatorInterface;
import com.isti.util.ModIterator;
import com.isti.util.TwoObjectMatcher;
import com.isti.util.IstiEncryptionUtils;
import com.isti.util.IstiVersion;
import com.isti.util.TagValueTable;
import com.isti.quakewatch.common.MsgTag;
import com.isti.quakewatch.util.QWUtils;
import com.isti.quakewatch.util.QWAliveMsgListener;
import com.isti.quakewatch.util.QWConnectionMgr;
import com.isti.quakewatch.util.DomainTypeInfoTable;
import com.isti.quakewatch.util.QWConnProperties;

/**
 * Class QWMessageHandler manages the request-resend-from-server aspects
 * of message handling.
 */
public class QWMessageHandler implements MessageProcessor
{
    //indicator values returned by 'checkMessageNumber()' method:
  public static final int CHKNUM_MESSAGE_OK = 0;
  public static final int CHKNUM_MISSED_MSGS = 1;
  public static final int CHKNUM_OBSOLETE_MSG = 2;

  protected final QWConnectionMgr connMgrObj;    //connection manger object
  protected final QWDataMsgProcessor dataMsgProcObj;
  protected final LogFile logObj;
  protected final QWConnProperties cfgObj;  //handle to conn properties
  protected final int debugMissedMsgTestValue;
  protected final boolean debugMissedMsgTestFlag;

    //Vector of 'QWAliveMsgListener' objects:
  protected final Vector aliveMsgListenersVec = new Vector();
    //matcher for domain and type names being subscribed to:
  protected TwoObjectMatcher domainTypeMatcherObj = null;
    //true if any domain and type names are being subscribed to:
  protected boolean domainTypeFilteringFlag = false;
    //processing enabled flag; initially false (disabled):
  private boolean processingEnabledFlag = false;
    //queue of data messages waiting to be processed:
  private final FifoHashtable waitingMsgsQueueTable = new FifoHashtable();
    //message number from last "Alive" message from server:
  protected long lastAliveMsgNum = 0;
    //message countered used if 'debugMissedMsgTestValue' > 0:
  protected int debugMessageCounter = 0;
    //date-formatter for 'doFetchAndProcessMessagesFromServer()':
  private final DateFormat xmlDateFormatterObj =
                                           QWUtils.getXmlDateFormatterObj();
    //date-formatter for 'getMessageTimeGeneratedDate()':
  private static final DateFormat getMsgXmlDateFormatterObj =
                                           QWUtils.getXmlDateFormatterObj();
    //commonly-used string for debug outputs:
  private static final String MSG_COLON_STR = "  msg:  ";

    //flag set true during 'fetchAndProcessMessagesFromServer()':
  protected boolean fetchAndProcessMessagesFlag = false;
    //thread sync obj for 'fetchAndProcessMessagesFromServer()':
  protected final Object fetchAndProcessMessagesSyncObject = new Object();
    //time of last successful fetch of messages from the server:
  protected long lastFetchMsgsFromServerTime = 0;
    //terminate flag for 'fetchAndProcessMessagesFromServer()':
  protected boolean fetchAndProcMsgsTerminateFlag = false;
    //message-number value for last received message:
  protected long lastReceivedMessageNumber = 0;
    //time-generated value for last received message:
  protected long lastReceivedTimeGenerated = 0;
    //flag set true if last-received msgNum came from status message:
  protected boolean lastReceivedNumViaStatMsgFlag = false;
    //thread-synchronization object for last-received variables:
  protected final Object lastRecvdValuesSyncObj = new Object();
    //flag set 'true' after each fetched message received:
  protected boolean fetchAndProcMsgRecvdFlag = false;
    //check for missed messages flag; initially false:
  protected boolean checkMissedEnabledFlag = false;
    //flag set 'true' to have missed-message checking always disabled:
  protected boolean disableMissedMsgCheckingFlag = false;
    //table of feeder-data-source host-name/message-number entries:
  protected final TagValueTable fdrSrcHostMsgNumTableObj =
                                                        new TagValueTable();
    //table of msgNum/timeGen values for domain/type names:
  protected DomainTypeInfoTable domainTypeInfoTableObj = null;
    //thread-synchronization object for domain/type table:
  protected Object domainTypeTableSyncObj = new Object();
    //array of data from certificate file:
  protected byte [] certificateFileDataArr = null;
    //certificate:
  protected X509Certificate certificateObj = null;
  private IstiVersion minVersion = null;

  /**
   * Creates a message-handler object.
   * @param connMgrObj the connection manager object to use.
   * @param logObj log-file object to be used, or null for none.
   */
  public QWMessageHandler(QWConnectionMgr connMgrObj,LogFile logObj)
  {
    this.connMgrObj = connMgrObj;    //save handle to connection manager
    //get handle to data-message processor:
    this.dataMsgProcObj = connMgrObj.getDataMsgProcObj();
    //setup handle to log file; if null then setup dummy log file
    this.logObj = (logObj != null) ? logObj :
                  new LogFile(null,LogFile.NO_MSGS,LogFile.NO_MSGS);
    cfgObj = connMgrObj.getConnProperties();  //save handle to conn properties
    //get 'debugMissedMsgTestValue' parameter value:
    debugMissedMsgTestValue = connMgrObj.getDebugMissedMsgTestValue();
    //true if any messages are to be ignored for debug testing:
    if(debugMissedMsgTestFlag = (debugMissedMsgTestValue > 0))
    {    //missed-message debug testing is enabled; log message
      logObj.info("Missed-message debug testing enabled, " +
                  ((debugMissedMsgTestValue > 1) ? ("only 1 out of every " +
         debugMissedMsgTestValue + " incoming messages will be processed") :
                                  "all incoming messages will be ignored"));
    }
  }

  /**
   * Get the minimum version.
   * @return the minimum version or null if none.
   */
  public IstiVersion getMinVersion()
  {
    return minVersion;
  }

  /**
   * Set the minimum version.
   * @param minVersion the minimum version or null if none.
   */
  public void setMinVersion(IstiVersion minVersion)
  {
    this.minVersion = minVersion;
  }

  /**
   * Adds the given 'QWAliveMsgListener' object.  This object's
   * method will be called when an alive message is received.
   * @param listenerObj the listener object to add.
   */
  public void addAliveMsgListener(QWAliveMsgListener listenerObj)
  {
    if(listenerObj != null)
      aliveMsgListenersVec.add(listenerObj);
  }

  /**
   * Removes the given 'QWAliveMsgListener' object.
   * @param listenerObj the listener object to remove.
   */
  public void removeAliveMsgListener(QWAliveMsgListener listenerObj)
  {
    if(listenerObj != null)
      aliveMsgListenersVec.remove(listenerObj);
  }

  /**
   * Sets the list of event domain and type names being subscribed to
   * for event-message filtering.
   * @param listStr the list string to use.
   */
  public void setDomainTypeListStr(String listStr)
  {
    //set flag if list string contains any domain:name entries:
    domainTypeFilteringFlag = (listStr != null &&
                               listStr.trim().length() > 0);
    if(domainTypeFilteringFlag)
    {    //list string contains domain:name entries
      if(domainTypeInfoTableObj == null)
      {       //domain/type info table not yet created; create it now
        domainTypeInfoTableObj = new DomainTypeInfoTable();
        //setup thread-synchronization object for domain/type table:
        domainTypeTableSyncObj = domainTypeInfoTableObj.getThreadSyncObj();
        //create matcher object from domain/type list string:
        domainTypeMatcherObj = QWUtils.listStringToMatcher(listStr);
      }
      else    //domain/type info table already created
        domainTypeInfoTableObj.clear();     //clear table
    }
    else      //list string contains no entries
      domainTypeMatcherObj = null;     //discard any matcher object
  }

  /**
   * Processes a "QWmessage" XML text message string.  This method
   * implements the 'MessageProcessor' interface.
   * @param xmlMsgStr the XML text message string.
   * @param allowQueuingFlag true to allow queuing of the message (if
   * message proccessing disabled).
   */
  public void processMessage(String xmlMsgStr, boolean allowQueuingFlag)
  {
//    logObj.debug4("processMessage(String) called");
    try
    {         //convert string to tree of JDOM 'Element' and process:
      processMessage(IstiXmlUtils.stringToElement(xmlMsgStr),xmlMsgStr,
                                                    false,allowQueuingFlag);
    }
    catch(Exception ex)
        {         //error converting string; log error message
      logObj.warning("Error converting XML message string:  " + ex);
      logObj.warning(MSG_COLON_STR + xmlMsgStr);
    }
  }

  /**
   * Processes a "QWmessage" XML text message string.  This method
   * implements the 'MessageProcessor' interface.
   * @param xmlMsgStr the XML text message string.
   */
  public void processMessage(String xmlMsgStr)
  {
    processMessage(xmlMsgStr,true);
  }

  /**
   * Processes a "QWmessage" XML message.
   * @param qwMsgElement "QWmessage" element containing the message.
   * @param xmlMsgStr XML-text-string version of the message.
   * @param requestedFlag true to set the "requested" flag on the generated
   * data-message objects (to indicate that they should not be processed as
   * a "real-time" message).
   * @param allowQueuingFlag true to allow queuing of the message (if
   * message proccessing disabled).
   */
  public void processMessage(Element qwMsgElement,String xmlMsgStr,
                             boolean requestedFlag,boolean allowQueuingFlag)
  {
    if (!isValidMessage(qwMsgElement,xmlMsgStr))
      return;           //if invalid message then reject it

//    logObj.debug4("processMessage(Element,String,boolean,boolean):  " +
//                  "requestedFlag=" + requestedFlag + ", allowQueuingFlag=" +
//                                                          allowQueuingFlag);

//    if(msgStrListenerObj != null)      //if listener installed then send str
//      msgStrListenerObj.newMessageString(xmlMsgStr);
    int qwMsgCount = 0;      //track # of messages processed
         //get list of "DataMessage" elements in message:
                             // (use modified iterator so that list can be
    Iterator iterObj =       //  modified while being iterated over)
             new ModIterator(qwMsgElement.getChildren(MsgTag.DATA_MESSAGE));
    Object obj;
    Element elemObj;
    while(iterObj.hasNext())
    {    //for each "DataMessage" element; fetch 'Element' object from list
      if((obj=iterObj.next()) instanceof Element &&
                                       MsgTag.DATA_MESSAGE.equalsIgnoreCase(
                                          (elemObj=(Element)obj).getName()))
      {  //element fetched OK; process it
        queueProcessDataMessage(qwMsgElement,elemObj,xmlMsgStr,
                                requestedFlag,allowQueuingFlag);
        ++qwMsgCount;             //increment message count
      }
      else
      {  //error fetching element (shouldn't happen)
        logObj.warning("Error fetching \"" + MsgTag.DATA_MESSAGE +
                       "\" element from list object");
      }
    }
         //get list of "StatusMessage" elements in message:
                           // (use modified iterator so that list can be
    iterObj =              //  modified while being iterated over)
           new ModIterator(qwMsgElement.getChildren(MsgTag.STATUS_MESSAGE));
    while(iterObj.hasNext())
    {    //for each "StatusMessage" element; fetch 'Element' object from list
      if((obj=iterObj.next()) instanceof Element &&
                                     MsgTag.STATUS_MESSAGE.equalsIgnoreCase(
                                          (elemObj=(Element)obj).getName()))
      {  //element fetched OK; process it
        processStatusMessage(qwMsgElement,elemObj,xmlMsgStr);
        ++qwMsgCount;             //increment message count
      }
      else
      {  //error fetching element (shouldn't happen)
        logObj.warning("Error fetching \"" + MsgTag.STATUS_MESSAGE +
                       "\" element from list object");
      }
    }
    if(qwMsgCount == 0)
    {    //no messages found; log warning
      logObj.warning("No data or status elements found in QWmessage");
      logObj.warning(MSG_COLON_STR + xmlMsgStr);
    }
  }

  /**
   * Processes a "QWmessage" XML message.
   * @param qwMsgElement "QWmessage" element containing the message.
   * @param requestedFlag true to set the "requested" flag on the generated
   * data-message objects (to indicate that they should not be processed as
   * a "real-time" message).
   */
  private void processFetchedMessage(
      Element qwMsgElement,boolean requestedFlag)
  {
//    logObj.debug4("processFetchedMessage:  requestedFlag=" + requestedFlag);
    final String xmlMsgStr;
    try
    {         //convert JDOM 'Element' to a string:
      xmlMsgStr = IstiXmlUtils.elementToFixedString(qwMsgElement);
    }
    catch(Exception ex)
    {         //error converting string; log error message
      logObj.warning("Error converting XML message element to string:  " +
                     ex);
      return;
    }
    try
    {
      //process message:
      processMessage(qwMsgElement,xmlMsgStr,requestedFlag,false);
    }
    catch(Exception ex)
    {         //error converting string; log error message
      logObj.warning("Error processing XML message:  " + ex);
      logObj.warning(UtilFns.getStackTraceString(ex));
    }
  }

  /**
   * Processes or queues any number of "Event" and "Product" elements in
   * the given "DataMessage" element.
   * @param qwMsgElement The "QWmessage" element.
   * @param dataMsgElement The "DataMessage" element.
   * @param xmlMsgStr the XML text message string.
   * @param requestedFlag true to set the "requested" flag on the generated
   * data-message objects (to indicate that they should not be processed as
   * a "real-time" message).
   * @param allowQueuingFlag true to allow queuing of the message (if
   * message proccessing disabled).
   */
  private void queueProcessDataMessage(Element qwMsgElement,
                                    Element dataMsgElement,String xmlMsgStr,
                             boolean requestedFlag,boolean allowQueuingFlag)
  {
//    logObj.debug4("queueProcessDataMessage:  requestedFlag=" +
//                  requestedFlag + ", allowQueuingFlag=" + allowQueuingFlag);
//    logObj.debug4("processMessage, msg:  " + xmlMsgStr);
    if(!debugMissedMsgTestFlag || !allowQueuingFlag ||
                                             (debugMissedMsgTestValue > 1 &&
                      ++debugMessageCounter % debugMissedMsgTestValue == 0))
    {    //message is not being ignored (for missed-message debug testing)
                             //fetch message number from XML element:
      final long msgNum = getMessageNumberValue(qwMsgElement);
      if(allowQueuingFlag)
      {  //queuing allowed for this message
        synchronized(waitingMsgsQueueTable)
        {     //get thread synchronization lock for message queuing
          if(!processingEnabledFlag)
          {     //processing not enabled; add msg to waiting-messages queue
            logObj.debug2("QWMessageHandler:  Message added to " +
                          "waiting-messages queue (msgNum=" + msgNum + ")");
            logObj.debug3(MSG_COLON_STR + xmlMsgStr);
                   //add message to waiting-messages queue:
            waitingMsgsQueueTable.put(xmlMsgStr,
                             new QWDataMsgBlock(qwMsgElement,dataMsgElement,
                                                  xmlMsgStr,requestedFlag));
            return;                              //exit method
          }
        }
      }
         //message not queued; check if duplicate of message
         // in waiting-messages queue and remove it if so:
      if(waitingMsgsQueueTable.size() > 0 &&
                            waitingMsgsQueueTable.remove(xmlMsgStr) != null)
      {  //waiting-messages queue not empty and duplicate found
        logObj.debug("QWMessageHandler:  Duplicate message removed " +
                                              "from waiting-message queue");
        logObj.debug(MSG_COLON_STR + xmlMsgStr);
      }
      int retVal;
              //check message number to see if any messages have been
              // missed (and request the missed messages if so) and
              // check message number and time-generated date:
      if((retVal=checkMessageNumber(msgNum-1,
                                     qwMsgElement)) == CHKNUM_MISSED_MSGS)
      {
        logObj.debug2("QWMessageHandler:  Missed messages were detected " +
                      "while processing received data message");
      }
      else if(retVal == CHKNUM_OBSOLETE_MSG)
      {     //message is obsolete; log info message
        logObj.info("Obsolete message received and discarded (msgNum=" +
                    msgNum + ")");
        logObj.debug(MSG_COLON_STR + xmlMsgStr);
        return;       //discard and exit method
      }
         //process message now:
      doProcessDataMessage(qwMsgElement,dataMsgElement,xmlMsgStr,
                                                             requestedFlag);
    }
    else
    {    //message is beinging ignored (for missed-message debug testing)
      logObj.info("Incoming message ignored for debug testing, " +
                  "debugMessageCounter=" + debugMessageCounter);
      logObj.debug2(MSG_COLON_STR + xmlMsgStr);
    }
  }

  /**
   * Processes the given "StatusMessage" element.
   * @param qwMsgElement The "QWmessage" element.
   * @param statusMsgElement The "StatusMessage" element.
   * @param xmlMsgStr the XML text message string.
   */
  private void processStatusMessage(Element qwMsgElement,
                                    Element statusMsgElement,String xmlMsgStr)
  {
    try
    {                   //create 'QWStatusMsgRecord' object from elements:
      final QWStatusMsgRecord statusRecObj = new QWStatusMsgRecord(
          System.currentTimeMillis(),qwMsgElement,statusMsgElement);
      //debug-log receipt of status message:
      logObj.debug5("Received status message (msg#" +
           statusRecObj.msgNumber + "), MsgType=\"" + statusRecObj.msgType +
                                                          "\", MsgData=\"" +
              ((statusRecObj.msgData!=null)?statusRecObj.msgData:"<none>") +
                                                                      "\"");
      if(MsgTag.ALIVE.equalsIgnoreCase(statusRecObj.msgType))
      {  //message is server-alive message type
//        lastAliveTime = System.currentTimeMillis();   //mark time
        if(checkMissedEnabledFlag &&
           statusRecObj.msgNumber != lastAliveMsgNum)
        {     //checking of missed messages enabled and message number
          // is different from last checked alive message
          lastAliveMsgNum = statusRecObj.msgNumber;   //save new number
          // check message number to see if any messages have been
          // missed (and request the missed messages if so):
          if(checkMessageNumber(statusRecObj.msgNumber,statusRecObj) ==
             CHKNUM_MISSED_MSGS)
          {
            logObj.debug2("QWMessageHandler:  Missed messages were detected " +
                          "while processing server-alive message");
          }
        }
              //call alive-message listeners with status-record object:
        invokeAliveMsgListeners(statusRecObj);
      }
    }
    catch(Exception ex)
    {         //some kind of exception error; log it
      logObj.warning("Error processing \"" + MsgTag.STATUS_MESSAGE +
                     "\" element:  " + ex);
      logObj.debug(MSG_COLON_STR + xmlMsgStr);
      logObj.warning(UtilFns.getStackTraceString(ex));
    }
  }

  /**
   * Invokes all registered server-alive-message listeners using the
   * given record object.
   * @param recObj the 'QWStatusMsgRecord' object for the received message,
   * or null if none available.
   */
  public void invokeAliveMsgListeners(QWStatusMsgRecord recObj)
  {
    final Iterator iterObj;
    synchronized(aliveMsgListenersVec)
    {  //grab thread lock for alive-message-listeners vector
                   //get iterator for copy of vector:
      iterObj = new ModIterator(aliveMsgListenersVec);
    }
        //for each listener in vector; invoke call-back method:
    while(iterObj.hasNext())
      ((QWAliveMsgListener)(iterObj.next())).aliveMsgReceived(recObj);
  }

  /**
   * Returns an indicator of whether or not message processing is enabled.
   * @return true if message processing is enabled; false if not.
   */
  public boolean isProcessingEnabled()
  {
    return processingEnabledFlag;
  }

  /**
   * Sets the message-processing-enabled and check-missed-msgs-enabled
   * flags.
   * @param flgVal true specifies that queued and incoming messages should
   * now be processed and that missed-message checking should be enabled;
   * false specifies that incoming messages should be put into the
   * waiting-messages queue and that missed-message checking should be
   * disabled.
   */
  public void setProcessingEnabledFlag(boolean flgVal)
  {
//    logObj.debug4("setProcessingEnabledFlag:  flgVal=" + flgVal);
    if(flgVal)
    {    //message processing enable requested
      //check if resend-request of messages from server available:
      final boolean isReqServerMsgsAvailFlag =
          connMgrObj.isReqServerMsgsAvailable();
      if(!processingEnabledFlag)
      {  //message processing not currently enabled
        if(fetchAndProcessMessagesFlag)
        {     //fetch-and-process running; exit with processing disabled
          logObj.debug("QWMessageHandler:  Fetch-and-process thread " +
                       "running when message-processing enable requested");
          return;
        }
              //process any messages waiting in the queue:
        Object obj;
        QWDataMsgBlock msgBlockObj;
        int msgChkVal;
        while(true)
        {     //for each event message in the queue:
          synchronized(waitingMsgsQueueTable)
          {  //get thread synchronization lock for message queuing
            if(waitingMsgsQueueTable.size() <= 0)
            {     //no more objects in messages queue:
              processingEnabledFlag = true; //enable message processing
              logObj.debug2("QWMessageHandler:  Message processing enabled");
              break;
            }
                                  //get first object in queue:
            if((obj=waitingMsgsQueueTable.elementAt(0))
                                                  instanceof QWDataMsgBlock)
            {      //object is a QWDataMsgBlock
              msgBlockObj = (QWDataMsgBlock)obj;
              //if resend-request of messages from server avail and
              // missed messages were detected and a fetch-and-process
              // worker thread started then stop processing queue:
              if(isReqServerMsgsAvailFlag &&
                 connMgrObj.getMaxServerEventAgeMs() > 0)
              {    //resend-request of messages from server is available
                if((msgChkVal=checkMessageNumber(getMessageNumberValue(
                    msgBlockObj.qwMsgElement)-1,msgBlockObj.qwMsgElement)) ==
                    CHKNUM_MISSED_MSGS)
                {  //missed messages were detected
                  if(fetchAndProcessMessagesFlag)
                  {     //fetch-and-process worker thread started
                    logObj.debug2("QWMessageHandler:  Missed messages " +
                       "were detected while processing waiting-msgs queue");
                    break;
                  }
                }
                else if(msgChkVal == CHKNUM_OBSOLETE_MSG)
                {  //obsolete message (message number is "old")
                  logObj.debug3("QWMessageHandler:  Obsolete message " +
                                "discarded while processing waiting-msgs queue");
                  msgBlockObj = null;       //discard message
                }
              }
            }
            else   //object not a QWDataMsgBlock
              msgBlockObj = null;
            //remove first object from queue:
            waitingMsgsQueueTable.removeElementAt(0);
          }
          //process data message:
          if(msgBlockObj != null)
          {   //data block OK; process data block
//            logObj.debug4("setProcessingEnabledFlag:  " +
//                          "sending queued msg to 'doProcessDataMessage()'");
//            logObj.debug4(MSG_COLON_STR + msgBlockObj.xmlMsgStr);
            doProcessDataMessage(msgBlockObj.qwMsgElement,
                           msgBlockObj.dataMsgElement,msgBlockObj.xmlMsgStr,
                                                 msgBlockObj.requestedFlag);
          }
          if(fetchAndProcessMessagesFlag)
          {   //fetch-and-process started; exit with processing disabled
            logObj.debug("QWMessageHandler:  Fetch-and-process thread " +
                      "started while processing waiting-messages in queue");
            break;
          }
        }
      }
      //if resend-request of messages from server available then
      // enable missed-message checking:
      setupCheckMissedEnabledFlag(isReqServerMsgsAvailFlag);
    }
    else
    {    //message processing disable requested
      if(processingEnabledFlag)
      {  //message processing currently enabled
        synchronized(waitingMsgsQueueTable)
        {     //get thread synchronization lock for message queuing
          processingEnabledFlag = false;    //disable message processing
          logObj.debug2("QWMessageHandler:  Message processing disabled");
        }
      }
      checkMissedEnabledFlag = false;  //disable missed-message checking
    }
  }

  /**
   * Sets up the 'checkMissedEnabledFlag' to be equal to the given
   * flag if the max-server-event-age-property is greater than zero,
   * otherwise sets the 'checkMissedEnabledFlag' to false.
   * @param isReqServerMsgsAvailFlag true if the server connection
   * supports resend-requests of messages.
   */
  private void setupCheckMissedEnabledFlag(boolean isReqServerMsgsAvailFlag)
  {
    checkMissedEnabledFlag = (!disableMissedMsgCheckingFlag) &&
                                ((connMgrObj.getMaxServerEventAgeMs() > 0) ?
                                          isReqServerMsgsAvailFlag : false);
  }

  /**
   * Updates the 'checkMissedEnabledFlag' to enable/disable missed-message
   * checking.
   */
  public void updateCheckMissedEnabledFlag()
  {
    if(processingEnabledFlag)
    {    //message processing is enabled; update flag
      setupCheckMissedEnabledFlag(connMgrObj.isReqServerMsgsAvailable());
    }
  }

  /**
   * Configures whether or not missed-message check will always be
   * disabled.
   * @param flgVal true to have missed-message checking always disabled;
   * false to allow missed-message checking.
   */
  public void setDisableMissedMsgCheckingFlag(boolean flgVal)
  {
    disableMissedMsgCheckingFlag = flgVal;
    updateCheckMissedEnabledFlag();
  }

  /**
   * Returns a flag indicator of whether or not missed-message checking
   * is always disabled.
   * @return true if missed-message checking is always disabled;
   * false if missed-message checking is allowed.
   */
  public boolean getDisableMissedMsgCheckingFlag()
  {
    return disableMissedMsgCheckingFlag;
  }

  /**
   * Processes the given data message by entering its message-number
   * and time-generated values into the tracking system and then
   * passing the message on to the 'QWDataMsgProcessor' call-back
   * object.
   * @param qwMsgElement The "QWmessage" element.
   * @param dataMsgElement The "DataMessage" element.
   * @param xmlMsgStr the XML text message string.
   * @param requestedFlag true to set the "requested" flag on the generated
   * data-message objects (to indicate that they should not be processed as
   * a "real-time" message).
   */
  private void doProcessDataMessage(Element qwMsgElement,
              Element dataMsgElement,String xmlMsgStr,boolean requestedFlag)
  {
    try
    {
      final long msgNum;
      final Date dateObj;    //extract msgNum and timeGen from msg element:
      if((msgNum=getMessageNumberValue(qwMsgElement)) > 0 &&
                (dateObj=getMessageTimeGeneratedDate(qwMsgElement)) != null)
      {  //msgNum and timeGen values extracted OK
//        logObj.debug4("doProcessDataMessage; old:  " +
//                  "lastReceivedMessageNumber=" + lastReceivedMessageNumber +
//                ", lastReceivedTimeGenerated=" + lastReceivedTimeGenerated +
//                                        ", lastReceivedNumViaStatMsgFlag=" +
//                                             lastReceivedNumViaStatMsgFlag);
        synchronized(lastRecvdValuesSyncObj)
        {     //only allow one thread to access values at a time
          lastReceivedMessageNumber = msgNum;              //set values
          lastReceivedTimeGenerated = dateObj.getTime();
                   //indicate last-received msgNum not from status msg:
          lastReceivedNumViaStatMsgFlag = false;
        }
//        logObj.debug4("doProcessDataMessage; new:  " +
//                  "lastReceivedMessageNumber=" + lastReceivedMessageNumber +
//                ", lastReceivedTimeGenerated=" + lastReceivedTimeGenerated);
              //extract feeder-data-source attributes from 'QWmessage' elem
        final String fdrSrcHostStr;
        final long fdrSrcMsgNum;
        if((fdrSrcHostStr=getFeederSourceHostStr(qwMsgElement)) != null &&
                (fdrSrcMsgNum=getFeederSrcMsgNumberValue(qwMsgElement)) > 0)
        {     //feeder-data-source host name and msgNum values found
                                       //put in tracking table:
          fdrSrcHostMsgNumTableObj.put(fdrSrcHostStr,fdrSrcMsgNum);
        }
        if(domainTypeFilteringFlag)
        {     //domain/type event-message filtering is in use
          final String domainStr = getMessageEventDomainStr(qwMsgElement);
          final String typeStr = getMessageEventTypeStr(qwMsgElement);
          if((domainStr != null && domainStr.length() > 0) ||
                                  (typeStr != null && typeStr.length() > 0))
          {   //"MsgEvtDomain"/"MsgEvtType" attributes fetched OK
                   //get value for "MsgEvtMsgNum" attribute:
            final String msgNumStr = getMessageEventMsgNumStr(qwMsgElement);
            final Long longObj;
            if((longObj=QWUtils.parseStringLong(msgNumStr)) != null)
            {      //"MsgEvtMsgNum" value parsed OK
                        //enter "MsgEvtMsgNum" value into table:
              domainTypeInfoTableObj.setMessageNumberValue(
                                     domainStr,typeStr,longObj.longValue());
            }
            else
            {      //error parsing "MsgEvtMsgNum" value; log warning msg
              if(msgNumStr != null)
              {
                logObj.warning("Unable to parse '" +
                     MsgTag.MSG_EVT_MSG_NUM + "' attribute in message (\"" +
                                                         msgNumStr + "\")");
              }
              else
              {
                logObj.warning("Unable to find '" +
                         MsgTag.MSG_EVT_MSG_NUM + "' attribute in message");
              }
              logObj.debug(MSG_COLON_STR + xmlMsgStr);
            }
          }
        }
      }
      else
      {       //unable to extract msgNum and timeGen values
        synchronized(lastRecvdValuesSyncObj)
        {  //only allow one thread to access values at a time
          lastReceivedMessageNumber = 0;    //clear last-rec'd-msgNum value
        }
        logObj.debug4("doProcessDataMessage:  " +
                             "Unable to extract msgNum and timeGen values");
      }

      if(dataMsgProcObj != null)
      {  //data-message-processor OK; process data message
        dataMsgProcObj.processDataMessage(
                       qwMsgElement,dataMsgElement,xmlMsgStr,requestedFlag);
      }
    }
    catch(Exception ex)
    {
      logObj.warning("Error processing message:  " + ex);
      logObj.warning(UtilFns.getStackTraceString(ex));
    }
  }

  /**
   * Returns the message number and time-generated values for the last
   * message received from the server (if available).
   * @return A new 'QWMsgNumTimeRec' object containing the message number
   * and time-generated values for the last messaged received from the
   * server (if not available then the values will be zeroes).
   */
  public QWMsgNumTimeRec getLastReceivedMsgValues()
  {
    long msgNum,timeVal;
    synchronized(lastRecvdValuesSyncObj)
    {    //only allow one thread to access values at a time
              //if msgNum came from status msg then don't use it;
              // otherwise use msgNum from last msg:
      msgNum = lastReceivedNumViaStatMsgFlag ? 0 : lastReceivedMessageNumber;
      timeVal = lastReceivedTimeGenerated;     //use time from last msg
    }
    if(timeVal <= 0)
    {    //last-message values are not available or bad time value
      try
      {            //get last message record in stored event list:
        final QWMsgNumTimeRec recObj;
        if(dataMsgProcObj != null &&
                    (recObj=dataMsgProcObj.getLastEventInStorage()) != null)
        {     //last message record fetched OK
          timeVal = recObj.getTimeGenerated();   //use time from record
          msgNum = recObj.getMsgNum();           //use msg # from record
        }
        else
        {     //unable to fetch last message record
          msgNum = 0;             //indicate no message number
          timeVal = 0;            //indicate no time value found
        }
      }
      catch(Exception ex)
      {       //some kind of exception error happened
        msgNum = 0;               //indicate no message number
        timeVal = 0;              //indicate no time value found
        logObj.warning("getLastReceivedMsgValues error:  " + ex);
        logObj.warning(UtilFns.getStackTraceString(ex));
      }
      if(timeVal <= 0)
      {  //no time value for last message record was found
              //generate request-time from value for
              // "max age of events requested from server"
              // (add 1/2 second so when process-and-fetch
              //  checks time it's still within max-age):
        long maxServerAgeMs;
        if((maxServerAgeMs=connMgrObj.getMaxServerEventAgeMs()) > 500)
          maxServerAgeMs -= 500;
        timeVal = System.currentTimeMillis() - maxServerAgeMs;
      }
    }
    return new QWStaticMsgNumTimeRec(msgNum,timeVal);
  }

  /**
   * Requests, fetches and processes event message records from the server.
   * A worker thread is created and run to perform the work.
   * @param timeVal the time-generated value for message associated with
   * the given message number, or the requested time value to be used
   * (milliseconds since 1/1/1970).
   * @param msgNum the message number to use, or 0 or none.
   * @param requestedFlag true to set the "requested" flag on the generated
   * data-message objects (to indicate that they should not be processed as
   * a "real-time" message).
   * @param popupObj dialog popup object to be closed when the loading
   * of messages is complete, or null for none.
   * @param progressIndObj progress bar widget to be updated during
   * processing, or null for none.
   * @param serverChangedFlag true if this is the initial fetch from a
   * "new" server (one that was not connected to previously) and the
   * "requestSourced...()" methods should be used.
   * @return true if the worker thread was successfully launched, false
   * if the worker thread is already running.
   */
  public boolean fetchAndProcessMessagesFromServer(final long timeVal,
                              final long msgNum,final boolean requestedFlag,
                                         final IstiDialogInterface popupObj,
                            final ProgressIndicatorInterface progressIndObj,
                                            final boolean serverChangedFlag)
  {
//    logObj.debug4("fetchAndProcessMessagesFromServer:  timeVal=" +
//       timeVal + ", msgNum=" + msgNum + ", requestedFlag=" + requestedFlag);
    synchronized(fetchAndProcessMessagesSyncObject)
    {    //only allow one thread access at a time
      if(fetchAndProcessMessagesFlag)
        return false;   //if worker thread running then return false
      fetchAndProcessMessagesFlag = true;   //indicate thread running
      setProcessingEnabledFlag(false);      //disable msg proc
         //create and run worker thread:
      (new Thread("fetchAndProcessMessagesFromServer")
          {
            public void run()
            {
              String errMsgStr;
              try
              {         //do the fetching and processing work:
                errMsgStr = doFetchAndProcessMessagesFromServer(
                                timeVal,msgNum,requestedFlag,progressIndObj,
                                                         serverChangedFlag);
              }
              catch(Exception ex)
              {         //some kind of exception error
                errMsgStr =            //build error message
                   "Error in 'fetchAndProcessMessagesFromServer()':  " + ex;
                logObj.warning(errMsgStr);     //log warning msg & stack
                logObj.warning(UtilFns.getStackTraceString(ex));
              }
              if(popupObj != null)
              {    //popup dialog object was given
                            //enter error message (or null for none):
                popupObj.setUserMessageString(errMsgStr);
                if(!fetchAndProcMsgsTerminateFlag || popupObj.isVisible())
                {  //fetch-and-proc not terminated or popup currently showing
                        //wait (if nec) for dialog to show & then close it:
                  popupObj.closeWithWait(1000);    // (wait up to 1 second)
                }
              }
                   //indicate this fetch-and-process thread done:
              fetchAndProcessMessagesFlag = false;
              if(!fetchAndProcMsgsTerminateFlag) //if not terminated then
                setProcessingEnabledFlag(true);  //enable message processing
            }
          }).start();
      return true;
    }
  }

  /**
   * Requests, fetches and processes the initial set of event message
   * records from the server.  The request is for all message records
   * newer than the latest message record held by this client (if
   * available).  A worker thread is created and run to perform the
   * work.
   * @param popupObj dialog popup object to be closed when the loading
   * of messages is complete, or null for none.
   * @param progressIndObj progress bar widget to be updated during
   * processing, or null for none.
   * @param serverChangedFlag true if this is the initial fetch from a
   * "new" server (one that was not connected to previously) and the
   * "requestSourced...()" methods should be used.
   * @return true if the worker thread was successfully launched, false
   * if the worker thread is already running.
   */
  public boolean fetchAndProcInitMessagesFromServer(
                                         final IstiDialogInterface popupObj,
                            final ProgressIndicatorInterface progressIndObj,
                                                  boolean serverChangedFlag)
  {
//    logObj.debug4("fetchAndProcInitMessagesFromServer called");
         //get msgNum and timGen values for last-received message:
    final QWMsgNumTimeRec msgNumTimeRecObj = getLastReceivedMsgValues();
         //if server changed then clear last-received-msgNum to
         // prevent false missed messages because msgNums changed:
    if(serverChangedFlag)
      clearLastReceivedMsgNum();
    final long timeVal = msgNumTimeRecObj.getTimeGenerated();
    final long msgNum = msgNumTimeRecObj.getMsgNum();
    logObj.debug("QWMessageHandler:  Fetching initial messages from " +
                       "server, timeVal=" + timeVal + ", msgNum=" + msgNum +
                                    ", serverChanged=" + serverChangedFlag);
         //fetch and process messages:
    return fetchAndProcessMessagesFromServer(timeVal,msgNum,true,
                                 popupObj,progressIndObj,serverChangedFlag);
  }

  /**
   * Returns the status of the 'fetchAndProcessMessagesFromServer()' method.
   * @return true if the 'fetchAndProcessMessagesFromServer()' method's
   * worker thread is running; false if not.
   */
  public boolean isFetchAndProcessMessagesRunning()
  {
    return fetchAndProcessMessagesFlag;
  }

  /**
   * Performs the work of requesting, fetching and processing event message
   * records from the server.
   * @param timeVal the time-generated value for message associated with
   * the given message number, or the requested time value to be used
   * (milliseconds since 1/1/1970).
   * @param msgNum the message number to use, or 0 or none.
   * @param requestedFlag true to set the "requested" flag on the generated
   * data-message objects (to indicate that they should not be processed as
   * a "real-time" message).
   * @param progressIndObj progress indicator object to be updated during
   * processing, or null for none.
   * @param serverChangedFlag true if this is the initial fetch from a
   * "new" server (one that was not connected to previously) and the
   * "requestSourced...()" methods should be used.
   * @return null if successful; an error message string if an error
   * occurred.
   */
  protected String doFetchAndProcessMessagesFromServer(long timeVal,
                                          long msgNum,boolean requestedFlag,
                                  ProgressIndicatorInterface progressIndObj,
                                                  boolean serverChangedFlag)
  {
//    logObj.debug4("doFetchAndProcessMessagesFromServer:  timeVal=" +
//       timeVal + ", msgNum=" + msgNum + ", requestedFlag=" + requestedFlag);
    boolean pIndFlag;
    if(pIndFlag=(progressIndObj != null))
    {  //progress bar object was given (flag set)
      try
      {            //setup progress indicator to use values from 0 to 99:
        progressIndObj.setMinimum(0);
        progressIndObj.setMaximum(99);
      }
      catch(Exception ex)
      {     //some kind of error occurred
        pIndFlag = false;       //disable progress bar updates
      }
    }
         //generate minimum allowed request-time from value
         // for "max age of events requested from server":
//    final long minAllowedTimeVal = System.currentTimeMillis() -
//                                   connMgrObj.getMaxServerEventAgeMs();
//    if(timeVal < minAllowedTimeVal)
//    {    //given time value is before minimum allowed
//      logObj.debug("QWMessageHandler:  Message-request-from-server time " +
//          "value too old (timeVal=" + timeVal + ", msgNum=" + msgNum + ")");
//      timeVal = minAllowedTimeVal;     //use minimum time
//      msgNum = 0;                      //make sure message number not used
//    }

    final String ERR_PRESTR =          //prefix for error messages
                            "Error parsing requested XML message string:  ";
    String reqMsgsStr, str, retStr = null;
    Element elementObj;
    int numReqVal, msgListSize, cnt, totalMsgCount = 0, totalNumReqVal = 0;
    List msgElementsList;
    Object obj;
    Iterator iterObj;
    Date timeGenDateObj;
    long newTimeVal;
                             //mark start time for fetch:
    final long fetchStartTime = System.currentTimeMillis();
         //clear 'terminate" and "message-received" flags:
    fetchAndProcMsgsTerminateFlag = fetchAndProcMsgRecvdFlag = false;
    while(true)
    {    //for each set of 'QWMessage' elements received
      logObj.debug3("Requesting messages from server (timeVal=" + timeVal +
                   ", msgNum=" + msgNum + ")");
         //request messages from server (if server changed and
         // feeder-data-source table entries exist then supply them):
      reqMsgsStr = connMgrObj.requestServerMessages(timeVal,msgNum,
                                                      ((serverChangedFlag &&
                             fdrSrcHostMsgNumTableObj.getNumEntries() > 0) ?
                      fdrSrcHostMsgNumTableObj.getEntriesListStr() : null));
                   //save time of last successful fetch:
      lastFetchMsgsFromServerTime = System.currentTimeMillis();
      if(fetchAndProcMsgsTerminateFlag)     //if flag set then
        return null;                        //terminate thread
      if(reqMsgsStr == null || reqMsgsStr.length() <= 0)
      {       //no data returned; setup error message
        retStr = "Communications error fetching messages from server";
        break;          //abort method
      }
         //indicate fetched message received:
      fetchAndProcMsgRecvdFlag = true;
      try
      {       //convert received string to XML element:
        elementObj = IstiXmlUtils.stringToElement(reqMsgsStr);
      }
      catch(Exception ex)
      {       //error converting; setup error message
        retStr = "Error converting requested XML message string:  " + ex;
        break;          //abort method
      }
         //check if element named "QWresend":
      if(!MsgTag.QW_RESEND.equalsIgnoreCase(elementObj.getName()))
      {       //element name not "QWresend"; setup error message
        retStr = ERR_PRESTR + "Lead element not named \"" +
                 MsgTag.QW_RESEND + "\"";
        break;          //abort method
      }
         //get number of messages requested attribute:
      if((str=elementObj.getAttributeValue(MsgTag.NUM_REQUESTED)) !=  null)
      {       //found "numRequested" attribute
        try
        {          //convert numeric string to integer:
          numReqVal = Integer.parseInt(str);
          logObj.debug3("  Received number-requested value:  " + numReqVal);
        }
        catch(NumberFormatException ex)
        {          //error converting value
          logObj.warning("Error parsing num-requested value (\"" + str +
                             "\") in request message received from server");
          numReqVal = 0;          //clear value
        }
      }
      else
      {       //unable to find "numRequested" attribute
        logObj.debug("  Missing \"" + MsgTag.NUM_REQUESTED +
                       "\" attribute in lead element received from server");
        numReqVal = 0;            //clear value
      }
         //get list of 'QWmessage'-element objects:
      msgElementsList = elementObj.getChildren(MsgTag.QW_MESSAGE);
         //get size of list:
      msgListSize = (msgElementsList != null) ? msgElementsList.size() : 0;
      logObj.debug3("  Number of event-messages received:  " + msgListSize);
      if(msgListSize <= 0)
      {  //no message element objects received via fetch
        if(totalMsgCount <= 0)
        {     //no message element objects received in previous fetches
          synchronized(lastRecvdValuesSyncObj)
          {   //only allow one thread to access values at a time
                   //if msgNum came from status msg or msgNum not
                   // valid then setup to use fetch-start-time as
                   // time value for next fetch from server:
            if(lastReceivedNumViaStatMsgFlag ||
                                             lastReceivedMessageNumber <= 0)
            {
              lastReceivedTimeGenerated = fetchStartTime;
            }
          }
        }
        return null;              //exit method
      }
      if(numReqVal > totalNumReqVal)   //if larger than current then
        totalNumReqVal = numReqVal;    //save total # requested

         //process list of 'QWmessage'-element objects:
         // (use modified iterator so that list can be
         //  modified while being iterated over)
      iterObj = new ModIterator(msgElementsList);
      while(iterObj.hasNext())
      {    //for each 'QWMessage' element in list
        if((obj=iterObj.next()) instanceof Element)
        {     //'Element' object fetched OK
                   //process mesage (with "requested" flag passed along):
          processFetchedMessage((Element)obj,requestedFlag);
          ++totalMsgCount;        //increment count
        }
        if(fetchAndProcMsgsTerminateFlag)   //if flag set then
          return null;                      //terminate thread
        if(pIndFlag && (totalMsgCount % 10) == 0 && totalNumReqVal > 0)
        {     //progress bar enabled and 1 out of 10 iterations reached
                   //calculate percentage of total messages processed:
          if((cnt=totalMsgCount*100/totalNumReqVal) > 0 && cnt < 100)
            progressIndObj.setValue(cnt);   //if value OK then update bar
        }
      }

         //log information on messages received:
      synchronized(lastRecvdValuesSyncObj)
      {  //only allow one thread to access values at a time
        str = (lastReceivedMessageNumber > 0) ? ("QWMessageHandler:  " +
                  "lastReceivedMessageNumber=" + lastReceivedMessageNumber +
               ", lastReceivedTimeGenerated=" + lastReceivedTimeGenerated) :
                                                                       null;
      }
      if(str != null)
        logObj.debug3(str);
      if(fdrSrcHostMsgNumTableObj.getNumEntries() > 0)
      {  //feeder-data-source table contains entries; log them
        logObj.debug3("QWMessageHandler:  Feeder-data-source info:  " +
                               fdrSrcHostMsgNumTableObj.getEntriesListStr());
      }

      if(msgListSize >= numReqVal)      //if all requested msgs received then
        return null;                    //exit method

         //get last element-object received:
      if(!((obj=msgElementsList.get(msgListSize-1)) instanceof Element))
      {       //unable to fetch last element; setup error message
        retStr = ERR_PRESTR + "Unable to fetch last event message";
        break;          //abort method
      }
      elementObj = (Element)obj;
         //get "MsgNumber" attribute:
      if((str=elementObj.getAttributeValue(MsgTag.MSG_NUMBER)) == null)
      {       //error fetching attribute; setup error message
        retStr = ERR_PRESTR + "Unable to find \"" +
                 MsgTag.MSG_NUMBER + "\" attribute in last event message";
        break;          //abort method
      }
      try
      {       //convert numeric string to integer:
        msgNum = Long.parseLong(str);       //set new message-number value
      }
      catch(NumberFormatException ex)
      {       //error converting; setup error message
        retStr = ERR_PRESTR + "Error parsing \"" +
                 MsgTag.MSG_NUMBER + "\" attribute value (\"" +
                 str + "\") in last event message";
        break;          //abort method
      }
         //get "TimeGenerated" attribute:
      if((str=elementObj.getAttributeValue(MsgTag.TIME_GENERATED)) == null)
      {       //error fetching attribute; setup error message
        retStr = ERR_PRESTR + "Unable to find \"" +
               MsgTag.TIME_GENERATED + "\" attribute in last event message";
        break;          //abort method
      }
      if((timeGenDateObj=parseStringXmlDate(str)) == null)
      {       //error converting; setup error message
        retStr = ERR_PRESTR + "Error parsing \"" +
                          MsgTag.TIME_GENERATED + "\" attribute value (\"" +
                                          str + "\") in last event message";
        break;          //abort method
      }
              //get and check new time value:
      if((newTimeVal=timeGenDateObj.getTime()) < timeVal)
      {  //new time value is less than old time value; log message
        logObj.debug(
            "QWMessageHandler:  Next iteration of server-messages fetch aborted because new time value ("
                + newTimeVal + ") is < old time value (" + timeVal + ")");
        return null;                   //terminate thread
      }
      timeVal = newTimeVal;            //set new time value
      if(fetchAndProcMsgsTerminateFlag)     //if flag set then
        return null;                        //terminate thread
      serverChangedFlag = false;       //clear flag for next iteration
    }
         //aborting message after error; log warning message
    logObj.warning(retStr);
         //debug-log received message string:
    logObj.debug(MSG_COLON_STR + reqMsgsStr);
    return retStr;           //return error message string
  }

  /**
   * Performs the work of requesting, fetching and processing new event
   * message records from the server.  This method may be called on a
   * regular periodic basis to poll the server for new messages.
   * @param requestedFlag true to set the "requested" flag on the generated
   * data-message objects (to indicate that they should not be processed as
   * a "real-time" message).
   * @param progressIndObj progress indicator object to be updated during
   * processing, or null for none.
   * @param serverChangedFlag true if this is the initial fetch from a
   * "new" server (one that was not connected to previously) and the
   * "requestSourced...()" methods should be used.
   * @return null if successful; an error message string if an error
   * occurred (the error message string is logged).
   */
  public String doFetchAndProcessMessagesFromServer(boolean requestedFlag,
                                  ProgressIndicatorInterface progressIndObj,
                                                  boolean serverChangedFlag)
  {
    try
    {
         //get msgNum and timGen values for last-received message:
      final QWMsgNumTimeRec msgNumTimeRecObj = getLastReceivedMsgValues();
         //fetch and process messages:
      return doFetchAndProcessMessagesFromServer(
           msgNumTimeRecObj.getTimeGenerated(),msgNumTimeRecObj.getMsgNum(),
                            requestedFlag,progressIndObj,serverChangedFlag);
    }
    catch(Exception ex)
    {         //some kind of exception error
      final String errMsgStr =              //build error message
                   "Error in 'fetchAndProcessMessagesFromServer()':  " + ex;
      logObj.warning(errMsgStr);     //log warning msg & stack
      logObj.warning(UtilFns.getStackTraceString(ex));
      return errMsgStr;
    }
  }

  /**
   * Checks if any fetched messages have been received since the last
   * call to this method.
   * @return true if any fetched messages have been received, false if not.
   */
  public boolean checkFetchAndProcMsgReceived()
  {
    if(fetchAndProcMsgRecvdFlag)
    {    //flag set to indicate message received
      fetchAndProcMsgRecvdFlag = false;     //clear flag
      return true;
    }
    return false;
  }

  /**
   * Terminates any 'fetchAndProcessMessages' thread that is running.
   */
  public void terminateFetchAndProcessMsgs()
  {
    logObj.debug2("QWMessageHandler:  Setting " +
                               "'fetchAndProcMsgsTerminateFlag' to 'true'");
    fetchAndProcMsgsTerminateFlag = true;
  }

  /**
   * Clears the held message-number value for the last-received message.
   * This reinitializes the missed-message tracking mechanism.
   * @param setMsgTimeCurrentFlag true to set the time value for the
   * last-received message to the current time (to prevent old messages
   * from being fetched via 'doFetchAndProcessMessagesFromServer()'
   * method).
   */
  public void clearLastReceivedMsgNum(boolean setMsgTimeCurrentFlag)
  {
    synchronized(lastRecvdValuesSyncObj)
    {  //only allow one thread to access values at a time
      lastReceivedMessageNumber = 0;        //clear last-rec'd-msgNum value
                   //indicate last-received msgNum not from status msg:
      lastReceivedNumViaStatMsgFlag = false;
      if(setMsgTimeCurrentFlag)   //if flag then set timeVal to current time
        lastReceivedTimeGenerated = System.currentTimeMillis();
    }
    if(domainTypeInfoTableObj != null)      //if created then
      domainTypeInfoTableObj.clear();       //clear domain/type info table
  }

  /**
   * Clears the held message-number value for the last-received message.
   * This reinitializes the missed-message tracking mechanism.
   */
  public void clearLastReceivedMsgNum()
  {
    clearLastReceivedMsgNum(false);
  }

  /**
   * Determines if the held message-number value for the
   * last-received message is zero.  This usually happens
   * via the 'clearLastReceivedMsgNum()' method.
   * @return true if the held message-number value for the
   * last-received message is zero.
   */
  public boolean isLastReceivedMsgNumZero()
  {
    synchronized(lastRecvdValuesSyncObj)
    {  //only allow one thread to access values at a time
      return (lastReceivedMessageNumber == 0);
    }
  }

  /**
   * Clears the queue of data messages waiting to be processed.
   */
  public void clearWaitingMsgsQueueTable()
  {
    waitingMsgsQueueTable.clear();
  }

  /**
   * Returns time of last successful fetch of messages from the server.
   * @return time of last successful fetch of messages from the server,
   * in milliseconds since 1/1/1970.
   */
  public long getLastFetchMsgsFromServerTime()
  {
    return lastFetchMsgsFromServerTime;
  }

  /**
   * Parses the given string as an XML date string.  This method is
   * thread safe.
   * @param str the date string to parse.
   * @return A new 'Date' object, or null if the string could not be
   * parsed as an XML date string.
   */
  private Date parseStringXmlDate(String str)
  {
    synchronized(xmlDateFormatterObj)
    {
      try
      {       //parse date/time string into 'Date' object and return it:
        return xmlDateFormatterObj.parse(str);
      }
      catch(Exception ex)
      {       //error parsing
        return null;
      }
    }
  }

  /**
   * Checks to see if the given message number is greater than the message
   * number of the last event message received, and if so, requests the
   * missing messages from the server (via a worker thread).  Also checks
   * to see if the given message number and time-generated date are older
   * than those of the last-received message.
   * @param checkMsgNum the message number to use.
   * @param messageObj the 'Element' object to be checked if the
   * message number is smaller then the current message number, or
   * the 'QWStatusMsgRecord' object to be used, or null for no object.
   * @return The value 'CHKNUM_MESSAGE_OK' if the message number is as
   * expected; 'CHKNUM_MISSED_MSGS' if missed messages were detected;
   * or 'CHKNUM_OBSOLETE_MSG' if the given message number and message
   * date indicate that the message is obsolete and should be discarded.
   */
  protected int checkMessageNumber(long checkMsgNum,Object messageObj)
  {
//    logObj.debug4("checkMessageNumber:  checkMsgNum=" + checkMsgNum +
//                                              ", messageObj=" + messageObj);
    try
    {
      long lastMsgNum,lastTimeVal;
                               //get msgNum/timeGen for last received msg:
      synchronized(lastRecvdValuesSyncObj)
      {  //only allow one thread to access values at a time
        lastMsgNum = lastReceivedMessageNumber;  //use msgNum from last msg
              //if msgNum came from status msg then don't use
              // timeVal; otherwise use timeVal from last msg:
        lastTimeVal = lastReceivedNumViaStatMsgFlag ? 0 :
                                                  lastReceivedTimeGenerated;
      }
      //check if given message number is greater than the message
      // number of the last event message received:
      if(lastMsgNum > 0)
      {  //last-received-message-number value is valid
        if(checkMissedEnabledFlag && checkMsgNum > lastMsgNum)
        {  //check-missed checking enabled and gap in (main) message numbers
          if(domainTypeFilteringFlag)
          {   //domain/type event-message filtering is in use
            if(messageObj instanceof Element)
            {      //given message object is data-message 'Element'
              //check msgNum for domain/type name:
              final Element elementObj = (Element)messageObj;
              final String domainStr = getMessageEventDomainStr(elementObj);
              final String typeStr = getMessageEventTypeStr(elementObj);
              final Long longObj;
              final long tableMsgNumVal;
              if(((domainStr != null && domainStr.length() > 0) ||
                  (typeStr != null && typeStr.length() > 0)) &&
                  (longObj=QWUtils.parseStringLong(
                  getMessageEventMsgNumStr(elementObj))) != null &&
                  (tableMsgNumVal=domainTypeInfoTableObj.getMessageNumberValue(
                  domainStr,typeStr)) > 0)
              {    //"MsgEvtDomain"/"MsgEvtType" attributes fetched OK and
                // value for "MsgEvtMsgNum" attribute fetched and parsed
                // OK and msgNum for domain/type fetched from table OK
                //check if "MsgEvtMsgNum" value from message is
                // beyond next value in message-number sequence:
                if(longObj.longValue() <= tableMsgNumVal+1)
                  return CHKNUM_MESSAGE_OK;   //return if msgNum not too large
                logObj.debug("Missed messages detected (domain=\"" +
                             domainStr + "\", type=\"" + typeStr +
                             "\"); last received message was " +
                             tableMsgNumVal + ", server is at " + longObj);
                //call 'fetchAndProcessMessagesFromServer()' below
              }
              else      //unable to check msgNum for domain/type name
                return CHKNUM_MESSAGE_OK;
            }
            else if(messageObj instanceof QWStatusMsgRecord)
            {      //given message object is status-message record
              //check message numbers for domain/types
              // in status message (log any missing msgs):
              if(!checkStatusMessageObj((QWStatusMsgRecord)messageObj,true))
                return CHKNUM_MESSAGE_OK;   //return if no missing messages
              //call 'fetchAndProcessMessagesFromServer()' below
            }
          }
          else
          {   //domain/type event-message filtering not in use
            logObj.debug("Missed messages detected; last received " +
                         "message was " + lastMsgNum +
                         ", server is at " + checkMsgNum);
          }
          //request msgs from server (via worker thread):
          fetchAndProcessMessagesFromServer(lastTimeVal,
                                          lastMsgNum,false,null,null,false);
          return CHKNUM_MISSED_MSGS;        //return value for missed msgs
        }
        else  //given message-number not larger than current message-number
        {          //check if message-number is before current and if given
          // message-time is more than 1 second older than current:
          final Date dateObj;
          if(checkMsgNum < lastMsgNum && messageObj instanceof Element &&
             lastTimeVal > 0 && (dateObj=getMessageTimeGeneratedDate(
             (Element)messageObj)) != null &&
             lastTimeVal - dateObj.getTime() > 1000)
          {   //given message number and time-generated are too old
            return CHKNUM_OBSOLETE_MSG;     //return value for obs msg
          }
        }
      }
      else if(messageObj instanceof QWStatusMsgRecord)
      {  //last-received-message values not valid and
        // given message object is a status-message record
//        logObj.debug4("checkMessageNumber; old:  " +
//                  "lastReceivedMessageNumber=" + lastReceivedMessageNumber +
//                ", lastReceivedTimeGenerated=" + lastReceivedTimeGenerated +
//                                        ", lastReceivedNumViaStatMsgFlag=" +
//                                             lastReceivedNumViaStatMsgFlag);
        synchronized(lastRecvdValuesSyncObj)
        {     //only allow one thread to access values at a time
                   //enter given msgNum as value for last-received message:
          lastReceivedMessageNumber = checkMsgNum;
                   //indicate last-received msgNum came from status msg:
          lastReceivedNumViaStatMsgFlag = true;
        }
//        logObj.debug4("checkMessageNumber; new:  " +
//                  "lastReceivedMessageNumber=" + lastReceivedMessageNumber +
//                ", lastReceivedTimeGenerated=" + lastReceivedTimeGenerated);
        //if domain/type event-message filtering in use then
        // enter message numbers for domain/types from status msg:
        if(domainTypeFilteringFlag)
          checkStatusMessageObj((QWStatusMsgRecord)messageObj,false);
      }
    }
    catch(Exception ex)
    {
      logObj.warning("Error checking message number:  " + ex);
      logObj.warning(UtilFns.getStackTraceString(ex));
    }
    return CHKNUM_MESSAGE_OK;               //return value for msg OK
  }

  /**
   * Checks the "FilteredMsgNum" child-elements of the given status message
   * record for any gap in the message-number sequence for each domain/type
   * name.  If a matching entry for any domain/type name is not found in
   * the 'DomainTypeInfoTable' then a new one will be created with the
   * message-number value from the "FilteredMsgNum" element.
   * @param statusRecObj the 'QWStatusMsgRecord' object to use.
   * @param logFlag true to generate a log message for each message-number
   * gap found; false to generate no log messages.
   * @return true if any gaps in message numbers wre found; false if not.
   */
  protected boolean checkStatusMessageObj(QWStatusMsgRecord statusRecObj,
      boolean logFlag)
  {
    boolean missedMsgsFlag = false;
    if(statusRecObj.statusMsgElement != null)
    {    //"StatusMessage" element is OK
      final List fMsgNumElems;    //get list of "FilteredMsgNum" elements:
      if((fMsgNumElems=statusRecObj.statusMsgElement.getChildren(
          MsgTag.FILTERED_MSG_NUM)) != null)
      {  //list of "FilteredMsgNum" elements fetched OK
        final Iterator iterObj = fMsgNumElems.iterator();
        Object obj;
        Element elemObj;
        String domainStr,typeStr;
        Long valueObj;
        long tableMsgNumVal;
        synchronized(domainTypeTableSyncObj)
        { //only allow 1 thread to access 'domainTypeInfoTableObj' at a time
          while(iterObj.hasNext())
          {   //for each "FilteredMsgNum" element
            if((obj=iterObj.next()) instanceof Element)
            {      //Element object fetched OK
              elemObj = (Element)obj;  //get "DomainName"/"TypeName" attribs:
              domainStr = elemObj.getAttributeValue(MsgTag.DOMAIN_NAME);
              typeStr = elemObj.getAttributeValue(MsgTag.TYPE_NAME);
              if(((domainStr != null && domainStr.length() > 0) ||
                  (typeStr != null && typeStr.length() > 0)) &&
                  (valueObj=QWUtils.parseStringLong(
                  elemObj.getAttributeValue(MsgTag.VALUE))) != null)
              {    //"DomainName"/"TypeName" attributes fetched OK and
                // numeric "Value" attribute fetched and parsed OK
                if((tableMsgNumVal=
                    domainTypeInfoTableObj.getMessageNumberValue(
                    domainStr,typeStr)) > 0)
                {  //matching domain/type name entry found in table
                  if(valueObj.longValue() > tableMsgNumVal)
                  {     //received msgNum value greater than value in table
                    missedMsgsFlag = true;       //indicate msgs missed
                    if(logFlag)
                    {   //logging of missed messages is enabled
                      logObj.debug("Missed messages detected (domain=\"" +
                                   domainStr + "\", type=\"" + typeStr +
                                   "\"); last received message was " +
                                   tableMsgNumVal + ", server is at " + valueObj);
                    }
                  }
                }
                else if(domainTypeMatcherObj != null &&
                        domainTypeMatcherObj.contains(domainStr,typeStr))
                {  //no matching domain/type name entry found in table
                  // and domain/type is among those being subscribed to
                  //create new table entry for domain/type:
                  domainTypeInfoTableObj.setMessageNumberValue(
                      domainStr,typeStr,valueObj.longValue());

                }
              }
            }
          }
        }
      }
    }
    return missedMsgsFlag;
  }

  /**
   * Returns the certificate that was fetched from the server at connect time.
   * @return The certificate that was fetched from the server
   * at connect time, or null if none was fetched or error with certificate.
   */
  public X509Certificate getCertificate()
  {
    //if the certificate file data has changed
    if (connMgrObj.getCertificateFileDataArr() != certificateFileDataArr)
    {
      generateCertificate();  //generate the certificate
    }

    return certificateObj;  //return the certificate
  }

  /**
   * Generates the certificate.
   */
  public void generateCertificate()
  {
    certificateFileDataArr = connMgrObj.getCertificateFileDataArr();

    try
    {
      //if the data was fetched
      if (certificateFileDataArr != null && certificateFileDataArr.length > 0)
      {
        //generate the certificate
        certificateObj = IstiEncryptionUtils.generateX509Certificate(
            certificateFileDataArr);
        if (certificateObj != null)  //if the certificate was generated
        {
          //get COA
          final X509Certificate certificateOfAuthority =
              IstiEncryptionUtils.generateX509Certificate(
              cfgObj.coaFileProp.stringValue());
          //get CRL
          final X509CRL crl = IstiEncryptionUtils.generateX509CRL(
              cfgObj.crlFileProp.stringValue());
          if (IstiEncryptionUtils.verifyX509Information(
              certificateObj, certificateOfAuthority, crl))
          {
            logObj.info("Authentication information verified.");
            return;  //we have a valid certificate so we are done
          }
          logObj.warning("Authentication information is not valid.");
        }
        else
        {
          logObj.warning("Certificate data is not valid.");
        }
      }
      else
      {
        logObj.warning("Certificate data is not available.");
      }
    }
    catch (Exception ex)
    {
      logObj.warning("Error generating certificate:  " + ex);
      logObj.warning(UtilFns.getStackTraceString(ex));
    }
    certificateObj = null;  //valid certificate is not available
  }

  /**
   * Validates the message.
   * @param qwMsgElement "QWmessage" element containing the message.
   * @param xmlMsgStr the XML text message string.
   * @return true if the message is valid
   * (signature is valid or missing if allowed),false otherwise.
   */
  public boolean isValidMessage(Element qwMsgElement,String xmlMsgStr)
  {
    String text;
    if (getCertificate() != null)  //if authentication should be done
    {
      text = qwMsgElement.getAttributeValue(MsgTag.SIGNATURE);
      if (text != null && text.length() > 0)  //if signature was found
      {  //message contains signature
        final String msgText = QWUtils.getTextForSignature(qwMsgElement);
        final boolean validFlag = IstiEncryptionUtils.isValidSignatureText(
            msgText, UtilFns.removeQuoteChars(text), certificateObj);
        if (!validFlag)
        {
          logObj.warning(
                        "Rejected received message with invalid signature");
          logObj.debug(MSG_COLON_STR + xmlMsgStr);
          return false;
        }
        logObj.debug3(
                 "QWMessageHandler:  Received message signature validated");
      }
      else    //message does not contain a signature
      {            //reject unless processing messages without signatures
        if (!cfgObj.processMessageWithoutSigFlagProp.booleanValue())
        {
          logObj.warning(
                        "Rejected received message with missing signature");
          logObj.debug(MSG_COLON_STR + xmlMsgStr);
          return false;
        }
        logObj.debug3("QWMessageHandler:  " +
                         "Allowed received message with missing signature");
      }
    }
    if (minVersion != null)
    {
      text = qwMsgElement.getAttributeValue(MsgTag.MSG_VERSION);
      if (new IstiVersion(text).compareTo(minVersion) < 0)
      {
        return false;
      }
    }
    return true;
  }

  /**
   * Returns true if any event domain and type names are being
   * subscribed to.
   * @return true if any event domain and type names are being
   * subscribed to.
   */
  public boolean getDomainTypeFilteringFlag()
  {
    return domainTypeFilteringFlag;
  }

  /**
   * Returns the status of whether or not the connection has been
   * "validated" via the receipt of any server-alive messages.
   * @return true if any server-alive messages have been received since
   * the last connect-to-server attempt, false if not.
   */
  public boolean isConnectionValidated()
  {
    return connMgrObj.isConnectionValidated();
  }

  /**
   * Runs the 'connectionStatusChanged()' method on the status-checking
   * thread to get the client check-in to be performed immediately.  This
   * is used by the 'QWWebSvcsConnector' module to make a client check-in
   * happen right after a successful connect to the server.
   */
  public void fireConnectionStatusChanged()
  {
    connMgrObj.fireConnectionStatusChanged();
  }

  /**
   * Returns the value for the "MsgNumber" attribute of the given 'Element'
   * object.
   * @param elemObj the 'Element' object to use.
   * @return The "MsgNumber" value, or 0 if none could be found and
   * successfully parsed.
   */
  public static long getMessageNumberValue(Element elemObj)
  {
    try
    {
      return Long.parseLong(elemObj.getAttributeValue(MsgTag.MSG_NUMBER));
    }
    catch(Exception ex)
    {
      return 0;
    }
  }

  /**
   * Returns the Date object for the "TimeGenerated" attribute of the
   * given 'Element' object.
   * @param elemObj the 'Element' object to use.
   * @return A new Date object, or null if none could be found and
   * successfully parsed.
   */
  public static Date getMessageTimeGeneratedDate(Element elemObj)
  {
    synchronized(getMsgXmlDateFormatterObj)
    {    //only allow one thread at a time to use date-format object
      try
      {
        return getMsgXmlDateFormatterObj.parse(
            elemObj.getAttributeValue(MsgTag.TIME_GENERATED));
      }
      catch(Exception ex)
      {
        return null;
      }
    }
  }

  /**
   * Returns the value of the "FdrSourceHost" attribute of the given
   * 'Element' object.
   * @param elemObj the 'Element' object to use.
   * @return The string value of the "FdrSourceHost" attribute, or null
   * if the attribute was not found.
   */
  public static String getFeederSourceHostStr(Element elemObj)
  {
    return elemObj.getAttributeValue(MsgTag.FDR_SOURCE_HOST);
  }

  /**
   * Returns the value for the "FdrSourceMsgNum" attribute of the given
   * 'Element' object.
   * @param elemObj the 'Element' object to use.
   * @return The "FdrSourceMsgNum" value, or 0 if none could be found and
   * successfully parsed.
   */
  public static long getFeederSrcMsgNumberValue(Element elemObj)
  {
    try
    {
      return Long.parseLong(
                       elemObj.getAttributeValue(MsgTag.FDR_SOURCE_MSGNUM));
    }
    catch(Exception ex)
    {
      return 0;
    }
  }

  /**
   * Returns the value of the "MsgEvtDomain" attribute of the given
   * 'Element' object.
   * @param elemObj the 'Element' object to use.
   * @return The string value of the "MsgEvtDomain" attribute, or null
   * if the attribute was not found.
   */
  public static String getMessageEventDomainStr(Element elemObj)
  {
    return elemObj.getAttributeValue(MsgTag.MSG_EVT_DOMAIN);
  }

  /**
   * Returns the value of the "MsgEvtType" attribute of the given
   * 'Element' object.
   * @param elemObj the 'Element' object to use.
   * @return The string value of the "MsgEvtType" attribute, or null
   * if the attribute was not found.
   */
  public static String getMessageEventTypeStr(Element elemObj)
  {
    return elemObj.getAttributeValue(MsgTag.MSG_EVT_TYPE);
  }

  /**
   * Returns the value of the "MsgEvtMsgNum" attribute of the given
   * 'Element' object.
   * @param elemObj the 'Element' object to use.
   * @return The string value of the "MsgEvtMsgNum" attribute, or null
   * if the attribute was not found.
   */
  public static String getMessageEventMsgNumStr(Element elemObj)
  {
    return elemObj.getAttributeValue(MsgTag.MSG_EVT_MSG_NUM);
  }
}
