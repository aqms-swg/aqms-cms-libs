//QWMsgManager.java:  Message manager for QuakeWatch XML messages.
//
//   3/7/2003 -- [ET]  Initial version.
//  6/17/2003 -- [ET]  Renamed 'processEventMsgRecord()' to
//                     'checkFilterEventMsgRecord()'.
//  6/19/2003 -- [KF]  Added Trump event message-type.
//  6/24/2003 -- [KF]  Add 'evtRecObj' to 'newProductMsgRecord' and
//                     'newDelProductMsgRecord' methods.
//  6/25/2003 -- [ET]  Modified so that all received data messages are
//                     sent to the storage manager (instead of just those
//                     that were accepted using the current filter and
//                     storge-max-age parameters); modified to use
//                     'IstiDialogPopup.closeWithWait()' method.
//  6/26/2003 -- [ET]  Added methods for requesting, receiving and
//                     processing event messages from the server.
//   7/8/2003 -- [ET]  Made 'clearLastMsgRecordObj()' method public.
//  7/22/2003 -- [ET]  Added check to see if incoming message out of
//                     sequence and too old; moved 'MS_PER_HOUR' to
//                     "UtilFns".
//  7/31/2003 -- [ET]  Modified 'doFetchAndProcessMessagesFromServer()' to
//                     squelch unnecessary message-resend requests.
//   8/4/2003 -- [ET]  Modified 'fetchAndProcInitMessagesFromServer()' to
//                     always do initial resend-request from last message
//                     in current event list; modified so that the initial
//                     number of day's worth of events requested from the
//                     server will not exceed the maximum-loaded age.
//  8/15/2003 -- [ET]  Modified 'addEvent()' so that if an existing event-
//                     message record is superceded (via QDM), any existing
//                     products are copied to the new event-message record.
//  8/19/2003 -- [ET]  Modified so that when an event is removed via QDM-
//                     merge it is added to the filtered table and to
//                     direct later-arriving products to superceding
//                     events.
//  9/16/2003 -- [ET]  Added division-by-zero check ('totalNumReqVal') and
//                     changed progress bar to react to num-requested
//                     increases in 'doFetchAndProcessMessagesFromServer()'.
//  9/19/2003 -- [ET]  Added check for duplicate data message.
// 10/16/2003 -- [ET]  Changed 'JProgressBar' to 'ManagedJProgressBar'.
// 10/22/2003 -- [ET]  Modified to make sure that currently-selected event
//                     stays visible on quake list after reapply of events
//                     filter is performed.
// 10/30/2003 -- [ET]  Modified (via import) to reference 'QWCommon'
//                     version of 'QWMsgRecord' instead of local version
//                     and implement the 'QWDataMsgProcessor' interface;
//                     modified to work with 'QWMessageHandler'.
//  11/3/2003 -- [ET]  Improved 'setupMaxServerEventAgeMs()' method.
// 11/17/2003 -- [ET]  Added 'FilteredEventRecordTable' class and support
//                     for 'alarmTriggeredFlag' in 'QWEventMsgRecord'
//                     objects.
// 12/22/2003 -- [ET]  Modified to copy product table to delete-event record
//                     sent via 'msgObjsListenerObj.newDelEventMsgRecord()'
//                     method.
//   1/6/2004 -- [ET]  Took out calls to 'MsgHandler.setLastMsgRecordObj()'.
//  1/15/2004 -- [ET]  Modified latitude/longitude filtering so that if
//                     both min and max are set to zero then filtering is
//                     disabled.
//  3/18/2004 -- [ET]  Moved from 'QWClient' to 'QWCommon' project;
//                     modified constructor parameters.
//  3/23/2004 -- [ET]  Modified to use 'QWFilterProperties'; changed
//                     'setStorageMinDate()' to 'updateStorageMinimumDate()'.
//   4/2/2004 -- [ET]  Modified log level of duplicate-message output from
//                     "warning" to "info".
//   4/5/2004 -- [ET]  Added 'applyStorageAndFilterParams()' and
//                     'setupApplyParamsObjs()' methods.
//   4/6/2004 -- [ET]  Added 'performNextEventsCheck()' method,
//                     'EventsCheckThread' class and associated
//                     start/stop methods.
//   5/5/2004 -- [KF]  Added 'evtRecObj' to 'newDelEventMsgRecord' method.
//   5/5/2004 -- [ET]  Modified to load QDM "regions.xml" file in
//                     constructor; added 'MSG_COLON_STR'.
//   5/6/2004 -- [KF]  Made the 'compVersionStrs' method public and static.
//  5/12/2004 -- [ET]  Fixed so that QDM "regions.xml" file may be read
//                     from the program jar.
//  5/14/2004 -- [ET]  Modified 'applyStorageAndFilterParams()' to make
//                     sure that popup gets cleared even when reapply
//                     finishes very quickly.
//  5/20/2004 -- [ET]  Modified 'updateEventInTable()' to copy over the
//                     event-message record's display object when updating
//                     an existing event (QWEmailer uses the object to hold
//                     a list of alert recipients).
//   6/1/2004 -- [ET]  Added product-message cache to handle product
//                     messages that arrive before their associated
//                     event message.
//   6/4/2004 -- [ET]  Fixed 'setupMaxServerEventAgeMs()' so that a
//                     max-server-event-age config-prop value of zero
//                     is not modified.
//  6/18/2004 -- [ET]  Fixed issue where a product message linked to an
//                     event via a QDM merge could overwrite an existing
//                     product with the same event-ID as the event;
//                     modified to allow event update to result in
//                     removal of QDM-low-priority event; modified
//                     to put products from deleted events into the
//                     product-messages cache.
//   7/6/2004 -- [ET]  Modified so that if an event is deleted and a
//                     previously-received QDM-low-priority duplicate
//                     of the event exists then the duplicate event
//                     is pulled in to replace the deleted event; changed
//                     reference to QDM 'util' source files to updated
//                     external 'qdmutil' library; swapped order of
//                     parameters to 'EQEventsUtils.lowpriority()' so
//                     that if all else equal older event will lower
//                     priority.
//  7/16/2004 -- [ET]  Improved product-update debug-log message;
//                     added 'getQDMVersionStr()' method.
// 11/22/2004 -- [ET]  Modified 'processDataMessage()' delete-event to
//                     cache products before pulling QDM-duplicate event
//                     from filtered table.
//  5/18/2005 -- [ET]  Changed to use modified 'QWEventMsgRecord' interface;
//                     added 'updateTimeZones()' method.
//   6/2/2005 -- [ET]  Modified to save "superseded" message records via
//                     'setSupersededRecordObj()' method in new records.
//  1/31/2006 -- [KF]  Added 'addMsgObjsListener()' and
//                     'removeMsgObjsListener()' methods.
//  2/21/2006 -- [KF]  Changed to make 'addMsgObjsListener()' and
//                     'removeMsgObjsListener()' methods thread safe.
//  5/17/2006 -- [ET]  Modified 'updateEventInTable()' method to transfer
//                     utility object.
//  9/26/2006 -- [ET]  Added support for for ANSS-EQ-XML format.
//  10/2/2006 -- [ET]  Added 'commentElemFlag' parameter to call to
//                     'QWDelProductMsgRecord' constructor.
//  5/21/2007 -- [ET]  Changed log level on "Product with unexpected type
//                     code" messages from 'info' to 'debug2'.
//  6/11/2007 -- [ET]  Fixed issue where if an existing event became
//                     low QDM-priority via an update the event would
//                     not be removed.
//  7/30/2007 -- [ET]  Added 'setUseProductCacheFlag()' method.
//  8/15/2007 -- [ET]  Added optional 'storeInvalidMsgsFlag' parameter to
//                     'setStorageProcessor()' method.
//  4/21/2008 -- [ET]  Added methods 'getFilteredEventsList()',
//                     'addFilteredEvtsListChgListener()',
//                     'removeFilteredEvtsListChgListener()',
//                     'getFilterProperties()' and 'getLogObj()'.
// 12/18/2009 -- [ET]  Increased debug level on "New event record discarded,
//                     does not pass storage-minimum date" log outputs.
//  4/12/2010 -- [ET]  Added support for QuakeML format.
//  5/12/2010 -- [ET]  Modified 'getOriginElement()' to make it check for
//                     'origin' element at same level as given element
//                     (if QuakeML format).
//  8/26/2010 -- [ET]  Updated package names for QDM classes; modified
//                     'processDataMessage()' method to change log-level
//                     of "No valid elements found..." message from
//                     'Warning' to 'Debug'.
//  10/8/2010 -- [ET]  Modified 'processDataMessage()' method to log
//                     'debug2' messages in response to errors when
//                     creating message-record objects for incoming
//                     messages (instead of logging 'warning' messages).
//  5/20/2011 -- [ET]  Modified to use case-insensitive event-ID keys;
//                     modified to retain products when new non-filtered
//                     event matches event in filtered-events table.
//  2/15/2012 -- [ET]  Modified 'addEvent()' method to retain early-arriving
//                     products for events that are immediately discarded
//                     via QDM merge when they arrive.
//   5/3/2012 -- [KF]  Added 'isProcessUpdates' and 'setProcessUpdatesFlag'
//                     methods.
//  5/25/2012 -- [ET]  Changed 'setProductRecTable()' call to
//                     'addToProductRecTable()' in 'addEvent()' method
//                     to fix issue where products could be dropped from
//                     an event if the event was previously removed
//                     (i.e., via QDM merge).
//   7/9/2013 -- [ET]  Changed log level of "Removed x product message from
//                     cache" message from 'info' to 'debug'; changed log
//                     level of "Product with unexpected type code" message
//                     from 'info' to 'debug2'; added method
//                     'getFilteredEvtMsgRecObj()'; modified method
//                     'compVersionStrs()' so if numeric version strings
//                     given then a numeric comparison is performed (so,
//                     for example, "10" is greater than "9") and to
//                     support 'bothNullVal' parameter.
//  3/13/2019 -- [KF]  Modified to use the PDL Indexer.
//

package com.isti.quakewatch.message;

import java.util.*;
import org.jdom.Element;
import org.jdom.Namespace;
import com.isti.util.UtilFns;
import com.isti.util.LogFile;
import com.isti.util.FifoHashtable;
import com.isti.util.CfgPropItem;
import com.isti.util.DataChangedListener;
import com.isti.util.DataChgdListenerSupport;
import com.isti.util.IstiDialogInterface;
import com.isti.util.IstiNotifyThread;
import com.isti.util.KeyedTreeTable;
import com.isti.quakewatch.common.MsgTag;
import com.isti.quakewatch.util.QWConnectionMgr;
import com.isti.quakewatch.util.QWFilterProperties;
import com.isti.quakewatch.util.ResourceIdentifier;
import com.isti.quakewatch.util.QWUtils;

/**
 * Class QWMsgManager is a message manager for QuakeWatch XML messages.
 */
public class QWMsgManager implements QWDataMsgProcessor
{
  private final EventListManager eventListManagerObj;
              //property item for max age of events requested from server:
  private final CfgPropItem maxServerEventAgeDaysProp;
              //property item for max age for events to be loaded and held:
  private final CfgPropItem maxLoadEventAgeDaysProp;
  private final QWFilterProperties filterPropertiesObj;
  private final LogFile logObj;
  private final Object eventsListSyncObj;
  private QWConnectionMgr connectionMgrObj = null;
  private static final int MAX_PRODUCTCACHE_AGEMS =  //product-cache max-age
                                          10 * (int)(UtilFns.MS_PER_MINUTE);
//  private QWMessageHandler msgHandlerObj = null;
                   //table of event record objs that have been filtered out:
  private final FilteredEventRecordTable filteredEventRecordTable =
                                             new FilteredEventRecordTable();
                        //convert array of product-type strings to a Vector:
  private static final Vector productTypeStrsVec =
                       new Vector(Arrays.asList(MsgTag.productTypeStrsArr));
  private Date storageMinDateObj = null;    //minimum date for event storage
              //list of listeners for event messages:
  private Vector msgObjsListenerListObj = new Vector();
              //listener for event messages:
  private QWMsgObjsListener msgObjsListenerObj = null;
              //tracker for 'maxLoadEventAgeDays' configuration property:
  private long maxLdEvtAgeMsTracker = 7*UtilFns.MS_PER_DAY;
         //dialog-popup interface used by 'applyStorageAndFilterParams()':
  private IstiDialogInterface applyParamsDialogInterfaceObj = null;
         //listener used by 'applyStorageAndFilterParams()':
  private DataChangedListener applyParamsCompletedListenerObj = null;
         //flag true while in 'applyStorageAndFilterParams()' method:
  private boolean applyStorageAndFilterParamsFlag = false;
              //index value used by 'processNextEventMsgRecord()':
  private int nextEventRecordIndex = -1;
              //index value used by 'getNextFilteredEventMsgRecord()':
  private int nextFilteredRecordIndex = -1;
              //storage processor object:
  private QWStorageProcessor storageProcObj = null;
              //set true to send invalid messages to storage processor:
  private boolean storeInvalidMessagesFlag = false;
              //flag to track changes for 'reapplyEventsFilter()' method:
//  private boolean filteredEventTableChangedFlag = false;
              //tracker for received data message data:
  private String dataMessageStringTracker = "";
              //trump record object tracker:
  private QWTrumpMsgRecord trumpRecObj = null;
         //handle for 'EventsCheckThread' object:
  private EventsCheckThread eventsCheckThreadObj = null;
         //table of product messages with no matching events:
  private final KeyedTreeTable productCacheTableObj =
                                                   new KeyedTreeTable(true);
         //flag set false to disable product cache:
  private boolean useProductCacheFlag = true;
         //commonly-used string for debug outputs:
  private static final String MSG_COLON_STR = "  msg:  ";
        // flag set to also process updates to events
  private boolean processUpdatesFlag = false;


  /**
   * Creates a message manager for QuakeWatch XML messages.
   * @param eventListManagerObj the event-list manager object to use.
   * @param maxServerEventAgeDaysProp property item for maximum age of
   * events requested from server.
   * @param maxLoadEventAgeDaysProp property item for maximum age in
   * days for events to be loaded and held.
   * @param filterPropertiesObj the filter-properties object to use, or
   * null for none.
   * @param resourceDir the resource directory or null for none.
   * @param logFileObj log file object to use, or null for none.
   */
  public QWMsgManager(EventListManager eventListManagerObj,
                                      CfgPropItem maxServerEventAgeDaysProp,
                                        CfgPropItem maxLoadEventAgeDaysProp,
                                     QWFilterProperties filterPropertiesObj,
                                String resourceDir,LogFile logFileObj)
  {
    this.eventListManagerObj = eventListManagerObj;
    this.filterPropertiesObj = filterPropertiesObj;
              //get thread-sync object for events-list table:
    eventsListSyncObj = eventListManagerObj.getEventsListSyncObj();
         //setup log file object; use dummy log file obj if none given:
    logObj = (logFileObj != null) ? logFileObj :
                          new LogFile(null,LogFile.NO_MSGS,LogFile.NO_MSGS);
              //enter max-loaded-event-age property object:
    this.maxLoadEventAgeDaysProp = maxLoadEventAgeDaysProp;
              //initialize 'maxLoadEventAgeDays' tracker:
    maxLdEvtAgeMsTracker =
                         (long)(this.maxLoadEventAgeDaysProp.doubleValue() *
                                                        UtilFns.MS_PER_DAY);
              //setup change listener to track max-loaded-event-age value
              // and call 'applyStorageAndFilterParams()' if decreased:
    this.maxLoadEventAgeDaysProp.addDataChangedListener(
      new DataChangedListener()
        {
          public void dataChanged(Object sourceObj)
          {
            final long newLoadAgeVal;
                   //check if max-server-event-age value is effected and
                   // get max-loaded-event-age value in milliseconds:
            if((newLoadAgeVal=setupMaxServerEventAgeMs()) <
                                                       maxLdEvtAgeMsTracker)
            {      //value of max-loaded-event-age has decreased
              maxLdEvtAgeMsTracker = newLoadAgeVal;   //save new value
              applyStorageAndFilterParams();          //apply new value
            }
          }
        });
              //enter max-server-event-age property object:
    this.maxServerEventAgeDaysProp = maxServerEventAgeDaysProp;
              //setup change listener to track max-server-event-age
              // value and (possibly) enable check-missed flag if
              // max-server-event-age goes from 0 to greater than 0:
    this.maxServerEventAgeDaysProp.addDataChangedListener(
      new DataChangedListener()
        {
          public void dataChanged(Object sourceObj)
          {
            setupMaxServerEventAgeMs();        //save current value in ms
          }
        });
    if(filterPropertiesObj != null)
    {    //filter-properties object was given
              //setup change listener to apply new values to all events:
      filterPropertiesObj.addDataChangedListener(
        new DataChangedListener()
          {
            public void dataChanged(Object sourceObj)
            {
              applyStorageAndFilterParams();          //apply new values
            }
          });
    }
  }

  /**
   * Sets up the value for 'maxServerEventAgeMs'.
   * @return The maximum-loaded-event-age property value, converted to
   * milliseconds.
   */
  private long setupMaxServerEventAgeMs()
  {
    final long loadedAgeVal =          //convert loaded-age value to ms
           (long)(maxLoadEventAgeDaysProp.doubleValue()*UtilFns.MS_PER_DAY);
    if(connectionMgrObj != null)
    {    //connection-manager object has been entered
              //check if loaded-age value is less than server-age value;
              // use the lesser of the two values:
      long serverAgeVal =         //convert server-age value to ms
         (long)(maxServerEventAgeDaysProp.doubleValue()*UtilFns.MS_PER_DAY);
      if(serverAgeVal > loadedAgeVal)       //if larger then loaded age then
        serverAgeVal = loadedAgeVal;        // use loaded-age value instead
                                  //enter value into connection manager:
      connectionMgrObj.setMaxServerEventAgeMs(serverAgeVal);
    }
    return loadedAgeVal;
  }

  /**
   * Enters the connection manager to be used by this message manager.
   * @param connMgrObj The connection-manager object to use.
   */
  public void setConnectionMgr(QWConnectionMgr connMgrObj)
  {
    connectionMgrObj = connMgrObj;
//    msgHandlerObj = (connMgrObj != null) ?
//                                 connectionMgrObj.getMsgHandlerObj() : null;
    setupMaxServerEventAgeMs();        //setup 'maxServerEventAgeMs' value
  }

  /**
   * Sets up the dialog-popup and completion call-back objects to
   * be used by the 'applyStorageAndFilterParams()' method.  The
   * 'applyStorageAndFilterParams()' method will be called
   * automatically when any filter parameters are changed or
   * when the maximum-loaded-event-age parameter is decreased.
   * @param popupObj popup dialog to be shown during reapply, or null
   * for none.
   * @param listenerObj call-back listener to be invoked after reapply
   * is complete, or null for none.
   */
  public void setupApplyParamsObjs(IstiDialogInterface popupObj,
                                            DataChangedListener listenerObj)
  {
    applyParamsDialogInterfaceObj = popupObj;
    applyParamsCompletedListenerObj = listenerObj;
  }

  /**
   * Configures whether or not the product cache is used.
   * @param flgVal true for product cache used; false for not.
   */
  public void setUseProductCacheFlag(boolean flgVal)
  {
    useProductCacheFlag = flgVal;
  }

  /**
   * Creates and starts a thread that calls 'performNextEventsCheck()'
   * once every 100 milliseconds.  This will filter-out and remove
   * events as they age.
   */
  public void startEventsCheckThread()
  {
    if(eventsCheckThreadObj != null)        //if thread already allocated
      eventsCheckThreadObj.terminate();     // then terminate thread
    eventsCheckThreadObj = new EventsCheckThread();   //create thread
    eventsCheckThreadObj.start();           //start thread
  }

  /**
   * Terminates the events-check thread created by the
   * 'startEventsCheckThread()' method.
   */
  public void stopEventsCheckThread()
  {
    if(eventsCheckThreadObj != null)
    {    //thread has been allocated
      eventsCheckThreadObj.terminate();     //terminate thread
      eventsCheckThreadObj = null;          //release thread object
    }
  }

  /**
   * Processes any number of "Event", "Product" and "StationAmp" elements
   * in the given "DataMessage" element.  This method is needed to
   * implement the 'QWDataMsgProcessor' interface.
   * @param qwMsgElement The "QWmessage" element.
   * @param dataMsgElement The "DataMessage" element.
   * @param xmlMsgStr the XML text message string.
   * @param requestedFlag true to set the "requested" flag on the generated
   * data-message objects (to indicate that they should not be processed as
   * a "real-time" message).
   */
  public void processDataMessage(Element qwMsgElement,
              Element dataMsgElement,String xmlMsgStr,boolean requestedFlag)
  {
         //check if message is a duplicate of the previous message:
    if(xmlMsgStr != null && xmlMsgStr.length() > 0)
    {    //given message string contains data
      if(xmlMsgStr.equals(dataMessageStringTracker))
      {  //message string data same as previous; log message
        logObj.info("Data message is duplicate of previous--" +
                                           "discarded; msg:  " + xmlMsgStr);
        return;
      }
                   //save message data string for next iteration:
      dataMessageStringTracker = xmlMsgStr;
    }
    else      //no data in given message string
      dataMessageStringTracker = "";        //clear message string tracker

              //determine message format and lead element:
    final QWUtils.ElemAndMsgFmtSpec elemAndMsgFmtSpecObj =
                             QWUtils.determineMessageFormat(dataMsgElement);
    final Element msgTopElemObj = elemAndMsgFmtSpecObj.elementObj;
    final int messageFormatSpec = elemAndMsgFmtSpecObj.messageFormatSpec;
    final Iterator eventsIterObj,productsIterObj;
    Element elemObj;
    if(messageFormatSpec == QWIdentDataMsgRecord.MFMT_QUAKEML)
    {  //message is QuakeML format
              //setup namespace object from 'quakeMessage' element:
      final Namespace nSpaceObj = msgTopElemObj.getNamespace();
              //dig down through 'quakeml' and 'eventParameters' elements
              // to get to 'event' elements (if any):
      if(MsgTag.QUAKE_ML.equalsIgnoreCase(msgTopElemObj.getName()))
        elemObj = msgTopElemObj;  //if top element 'quakeml' then use it
      else    //find 'quakeml' element under top element:
        elemObj = msgTopElemObj.getChild(MsgTag.QUAKE_ML,nSpaceObj);
      if(elemObj == null || (elemObj=elemObj.getChild(
                   MsgTag.EVENT_PARAMETERS,elemObj.getNamespace())) == null)
      {  //'quakeml' and 'eventParameters' elements not found
        elemObj = msgTopElemObj;       //just use top element
      }
              //build list of 'event' elements in msg (wrap in 'ArrayList'
              // to allow adding of various elements from tree):
      final List evtList = new ArrayList(
          elemObj.getChildren(MsgTag.QUAKEML_EVENT,elemObj.getNamespace()));
              //build list of 'productLink' elements (wrap in 'ArrayList'
              // to allow adding of various elements from tree):
      final List prdList = new ArrayList(
          msgTopElemObj.getChildren(MsgTag.QUAKEML_PRODUCT_LINK,nSpaceObj));
      final List uctList =        //build list of 'useControl' elements:
                    msgTopElemObj.getChildren(MsgTag.USE_CONTROL,nSpaceObj);
      final List cmtList =        //build list of 'comment' elements in msg:
                msgTopElemObj.getChildren(MsgTag.QUAKEML_COMMENT,nSpaceObj);
              //add 'useControl' elements to list of 'event' elements:
      evtList.addAll(uctList);
              //add 'useControl' elements to list of 'productLink' elements:
      prdList.addAll(uctList);
              //add 'comment' elements to list of 'productLink' elements:
      prdList.addAll(cmtList);
              //setup to scan elements for origins and products:
      eventsIterObj = evtList.iterator();
      productsIterObj = prdList.iterator();
    }
    else if(messageFormatSpec == QWIdentDataMsgRecord.MFMT_ANSSEQXML)
    {  //message data is ANSS-EQ-XML format
              //setup to scan 'Event' elements for origins and products:
      final List evtList = msgTopElemObj.getChildren(MsgTag.EVENT,
                                              msgTopElemObj.getNamespace());
      eventsIterObj = evtList.iterator();
      productsIterObj = evtList.iterator();
    }
    else
    {    //top element of message data is not 'quakeMessage' or 'EQMessage'
              //setup to scan 'Event' & 'Product' elems under 'DataMessage'
      eventsIterObj = dataMsgElement.getChildren(MsgTag.EVENT).iterator();
      productsIterObj =
                      dataMsgElement.getChildren(MsgTag.PRODUCT).iterator();
    }
    int msgCount = 0;        //track # of messages processed
    Object obj;
    boolean msgStoredFlag = false;
    Element eventElement,originElement;
    QWEQEventMsgRecord newEventRecObj;
    QWEventMsgRecord evtRecObj,hpRecObj;
    QWDelEventMsgRecord delEventRecObj;
    String tableTagStr;
    boolean delActionFlag,mainTableFlag,passesFilterFlag;
    while(eventsIterObj.hasNext())
    {  //for each "event" element; fetch element object from list
      try
      {
        eventElement = (Element)(eventsIterObj.next());
        if(isActionTrump(messageFormatSpec,eventElement,dataMsgElement))
        {     //value of "action" child-element is "trump"
                   //create trump message record from XML elements:
          trumpRecObj = new QWTrumpMsgRecord(qwMsgElement,
                             dataMsgElement,eventElement,messageFormatSpec);
          if(storageProcObj != null && !msgStoredFlag)
          {   //storage manager is setup and msg not yet stored
            storageProcObj.storeRecord(trumpRecObj);
            msgStoredFlag = true;      //indicate 'qwMsgElement' msg stored
          }
          ++msgCount;        //increment message count
          continue;          //re-loop to check for more msg elements
        }
              //get 'origin' child-element of 'event' element:
        originElement = getOriginElement(messageFormatSpec,eventElement);
              //determine if value of "action" child-element is "delete":
        delActionFlag = isActionDelete(
                             messageFormatSpec,eventElement,dataMsgElement);
        if(delActionFlag || originElement != null ||
                   messageFormatSpec == QWIdentDataMsgRecord.MFMT_QWMESSAGE)
        {  //delete action or 'Origin' elem found or QuakeWatch-XML format
           // (skip if non-delete non-QWXML without any 'Origin' elements)
          synchronized(eventsListSyncObj)
          {  //grab thread lock for events list
            if(!delActionFlag)
            {  //action is not "delete"; process as "update"
                     //create event message record from XML elements:
              newEventRecObj = new QWEQEventMsgRecord(
                                   qwMsgElement,dataMsgElement,eventElement,
                                           originElement,messageFormatSpec);
                     //enter last event message:
//              if(msgHandlerObj != null)     //if msg-handler OK then enter
//                msgHandlerObj.setLastMsgRecordObj(newEventRecObj);

              if (trumpRecObj != null)  //if trump record was found
              {
                //get identification key string for event.
                final String trumpIDKey = trumpRecObj.eventIDKey;

                //if event ID keys match
                if (trumpIDKey != null &&
                    trumpIDKey.equalsIgnoreCase(newEventRecObj.eventIDKey))
                {
                  logObj.debug(
                      "Setting trump flag for event (ID=\"" +
                      newEventRecObj.eventIDKey + "\")");
                  newEventRecObj.setTrump(true);  //set the trump flag
                }
                else
                {
                  logObj.warning(
                      "Event for trump (ID=\"" + trumpIDKey +
                      "\") does not match (ID=\"" + newEventRecObj.eventIDKey +
                      "\")");
                }
              }

              if(requestedFlag)        //if flag then set "requested" flag
                newEventRecObj.setRequestedFlag(true);
              if(storageProcObj != null && !msgStoredFlag)
              {    //storage manager is setup and msg not yet stored
                storageProcObj.storeRecord(newEventRecObj);
                msgStoredFlag = true;  //indicate 'qwMsgElement' msg stored
              }
              if(storageMinDateObj == null || (newEventRecObj.time != null &&
                          !(newEventRecObj.time.before(storageMinDateObj))))
              { //event record date/time not before minimum storage date/time
                       //check if record with same key already in table:
                if((evtRecObj=eventListManagerObj.getRecordObjForKeyStr(
                                        newEventRecObj.eventIDKey)) != null)
                {      //record with same key already in table
                       //update event in table (if version code is newer):
                  if(updateEventInTable(eventListManagerObj,evtRecObj,
                                                              newEventRecObj))
                  {    //version is newer
                    if(!checkFilterEventMsgRecord(newEventRecObj))
                    {   //updated record passes filter
                      logObj.debug2("Updated event record in table (ID=\"" +
                               newEventRecObj.eventIDKey + "\", newVer=\"" +
                                  newEventRecObj.version + "\", oldVer=\"" +
                                            evtRecObj.getVersion() + "\")");
                      logObj.debug3(MSG_COLON_STR + xmlMsgStr);
                        //check if update results in any existing event
                        // becoming QDM low priority or vice-versa:
                      checkRemoveLowPriorityEvent(newEventRecObj);
                    }
                    else
                    {   //updated record was filtered out
                      logObj.debug2("Updated event record no longer " +
                                                    "passes filter (ID=\"" +
                               newEventRecObj.eventIDKey + "\", newVer=\"" +
                                  newEventRecObj.version + "\", oldVer=\"" +
                                            evtRecObj.getVersion() + "\")");
                      logObj.debug3(MSG_COLON_STR + xmlMsgStr);
                    }
                    fireNewEventMsgRecord(newEventRecObj);
                  }
                  else
                  {    //version is not newer; ignore message
                    logObj.debug2("Ignoring event message with same or lower " +
                         "version code (ID=\"" + newEventRecObj.eventIDKey +
                              "\", currentVer=\"" + evtRecObj.getVersion() +
                       "\", newMsgVer=\"" + newEventRecObj.version + "\")");
                    logObj.debug3(MSG_COLON_STR + xmlMsgStr);
                  }
                }
                else    //record with same key not already in table
                {            //check if any products for event in
                             // product-message cache and add if so:
                  chkFetchFromProductCache(newEventRecObj);
                  if((passesFilterFlag=(filterPropertiesObj == null ||
                       filterPropertiesObj.passesFilter(newEventRecObj))) &&
                                                   addEvent(newEventRecObj))
                  {     //record passes filter and PDL-merge OK
                    logObj.debug2("Added new event record to table (ID=\"" +
                                  newEventRecObj.eventIDKey + "\", ver=\"" +
                                            newEventRecObj.version + "\")");
                    logObj.debug3(MSG_COLON_STR + xmlMsgStr);
                    fireNewEventMsgRecord(newEventRecObj);
                  }
                  else      //record does not pass filter or PDL-merge
                  {         //add record to table of filtered event records:
                    addOrUpdateToFilteredEventTable(newEventRecObj);
                    logObj.debug2("New event record does not pass " +
                      ((passesFilterFlag)?"PDL-merge":"filter parameters") +
                                 ":  " + newEventRecObj.getDisplayString());
                    logObj.debug3(MSG_COLON_STR + xmlMsgStr);
                  }
                }
              }
              else
              { //event record date/time is before minimum storage date/time
                logObj.debug3("New event record discarded, does not pass " +
                                                "storage-minimum date:  " +
                                        newEventRecObj.getDisplayString());
                logObj.debug4(MSG_COLON_STR + xmlMsgStr);
              }
              trumpRecObj = null;      //clear the trump
              ++msgCount;              //increment message count
            }
            else if(isDeleteEventMsg(messageFormatSpec,eventElement))
            {   //action is "Delete", and message has no product
                // child-elements or is not 'QWmessage' format
                     //create delete-event message record from XML elements:
              delEventRecObj = new QWDelEventMsgRecord(qwMsgElement,
                             dataMsgElement,eventElement,messageFormatSpec);
                     //enter last event message:
//              if(msgHandlerObj != null)     //if msg-handler OK then enter
//                msgHandlerObj.setLastMsgRecordObj(delEventRecObj);
              if(requestedFlag)        //if flag then set "requested" flag
                delEventRecObj.setRequestedFlag(true);
              if(storageProcObj != null && !msgStoredFlag)
              {    //storage manager is setup and msg not yet stored
                storageProcObj.storeRecord(delEventRecObj);
                msgStoredFlag = true;  //indicate 'qwMsgElement' msg stored
              }
                     //check if event record with same key exists in tables:
              if((evtRecObj=eventListManagerObj.getRecordObjForKeyStr(
                                      delEventRecObj.eventIDKey)) instanceof
                                                         QWEQEventMsgRecord)
              {      //event record exists in main event table
                tableTagStr = UtilFns.EMPTY_STRING;   //no tag str for msgs
                mainTableFlag = true;    //indicate found in main table
              }
              else if((obj=filteredEventRecordTable.get(
                  delEventRecObj.eventIDKey)) instanceof QWEQEventMsgRecord)
              {      //event record exists in filtered event table
                evtRecObj = (QWEventMsgRecord)obj;
                tableTagStr = "filtered ";    //set tag string for log msgs
                mainTableFlag = false;   //indicate not found in main table
              }
              else   //matching event record not found
              {
                tableTagStr = null;      //indicate not found
                mainTableFlag = false;   //indicate not found in main table
              }
              if(tableTagStr != null)
              {      //record with same key exists in table
                if(compVersionStrs(         //compare version codes
                    delEventRecObj.version,evtRecObj.getVersion(),1,1) >= 0)
                {    //delete-message version is empty or >= current version
                  eventListManagerObj.removeEventFromList(
                                                 evtRecObj.getEventIDKey());
                  logObj.debug2("Removed event record from " + tableTagStr +
                                "table (ID=\"" + delEventRecObj.eventIDKey +
                              "\", deletedVer=\"" + evtRecObj.getVersion() +
                       "\", delMsgVer=\"" + delEventRecObj.version + "\")");
                  logObj.debug3(MSG_COLON_STR + xmlMsgStr);
                  if(mainTableFlag)
                  {     //matching record was found in main table
                             //copy any products into delete-record object:
                    delEventRecObj.setProductRecTable(
                                            evtRecObj.getProductRecTable());
                    fireNewDelEventMsgRecord(delEventRecObj,evtRecObj);
                  }
                        //move products to product-message cache:
                  final FifoHashtable tableObj;
                  final int sizeVal;
                  if((tableObj=evtRecObj.getProductRecTable()) != null &&
                                              (sizeVal=tableObj.size()) > 0)
                  {
                    logObj.debug2("Moving " + sizeVal + " product" +
                            ((sizeVal!=1)?"s":"") + " from deleted event " +
                                      "(ID=\"" + delEventRecObj.eventIDKey +
                                            "\") to product-message cache");
                        //add product messages from deleted event to cache:
                    addMsgsToProductCache(tableObj,LogFile.DEBUG2);
                  }
                        //check if PDL-matching record exists in filtered
                        // table; if so then pull it into the main table:
                  if((evtRecObj=pullFilteredDupEvent(
                                    (QWEQEventMsgRecord)evtRecObj)) != null)
                  {     //matching rec exists & removed from filtered table
                             //check if any products for event in
                             // product-message cache and add if so:
                    chkFetchFromProductCache(evtRecObj);
                    if(addEvent(evtRecObj))
                    {   //pulled record passes PDL-merge
                      logObj.debug("Pulled and added PDL-matching record" +
                                             " from filtered table (ID=\"" +
                                     evtRecObj.getEventIDKey() + "\", ver=\"" +
                                               evtRecObj.getVersion() + "\")");
                      fireNewEventMsgRecord(evtRecObj);
                    }
                    else      //record does not pass PDL-merge
                    {         //put back in table of filtered event records:
                      addOrUpdateToFilteredEventTable(evtRecObj);
                      logObj.debug("Pulled PDL-matching record from " +
                               "filtered table does not pass PDL-merge:  " +
                                                 evtRecObj.getDisplayString());
                    }
                  }
                }
                else
                {    //delete-message version is less than current version
                  logObj.debug2("Ignoring delete-event message, version " +
                              "too low (ID=\"" + delEventRecObj.eventIDKey +
                              "\", currentVer=\"" + evtRecObj.getVersion() +
                       "\", delMsgVer=\"" + delEventRecObj.version + "\")");
                  logObj.debug3(MSG_COLON_STR + xmlMsgStr);
                }
              }
              else
              {      //record with same key does not exist in table
                logObj.debug2("Ignoring delete-event message, no matching " +
                       "event in table (ID=\"" + delEventRecObj.eventIDKey +
                       "\", delMsgVer=\"" + delEventRecObj.version + "\")");
                logObj.debug3(MSG_COLON_STR + xmlMsgStr);
              }
              ++msgCount;              //increment message count
            }
          }
        }
      }
      catch(QWRecordException ex)
      {  //error creating message-record obect; log it
        logObj.debug2("Error creating record for \"" + MsgTag.EVENT +
                                                    "\" message:  " + ex);
        logObj.debug3(MSG_COLON_STR + xmlMsgStr);
        logObj.debug3(UtilFns.getStackTraceString(ex));
      }
      catch(Exception ex)
      {  //some kind of exception error; log it
        logObj.warning("Error processing \"" + MsgTag.EVENT +
                                                    "\" message:  " + ex);
        logObj.debug(MSG_COLON_STR + xmlMsgStr);
        logObj.debug(UtilFns.getStackTraceString(ex));
      }
    }

    Element productElement;
    QWProductMsgRecord newProductRecObj,prodRecObj;
    QWDelProductMsgRecord delProductRecObj;
    boolean msgDeleteFlag,commentElemFlag;
    String str;
    while(productsIterObj.hasNext())
    {    //for each product element; fetch 'Element' object from list
      try
      {
        switch(messageFormatSpec)
        {  //process data depending on format specification
          case QWIdentDataMsgRecord.MFMT_QUAKEML:     //QuakeML format
            eventElement = null;       //no "Event" element
                        //get 'product', 'comment' or 'useControl' element:
            productElement = (Element)(productsIterObj.next());
            if(MsgTag.QUAKEML_PRODUCT_LINK.equalsIgnoreCase(
                                                  productElement.getName()))
            {  //element name is "productLink"
              msgDeleteFlag = false;        //indicate not "delete" message
              commentElemFlag = false;      //indicate not comment element
            }
            else if(MsgTag.USE_CONTROL.equalsIgnoreCase(
                                                  productElement.getName()))
            {  //element name is "useControl" (probably a "delete" message)
                   //process 'useControl'|'elementID' value as resource-id:
              final ResourceIdentifier resIdObj = new ResourceIdentifier(
                              productElement.getChildText(MsgTag.ELEMENT_ID,
                                            productElement.getNamespace()));
              if(MsgTag.QUAKEML_PRD_STR.equalsIgnoreCase(
                                           resIdObj.getResourceTypeStr()) &&
                                     MsgTag.QUAKEML_DELETE.equalsIgnoreCase(
                          productElement.getChildText(MsgTag.QUAKEML_ACTION,
                                            productElement.getNamespace())))
              {  //'useControl'|'elementID' value is resource-id with
                 // resource type "prd" and 'useControl'|'action'=="delete"
                msgDeleteFlag = true;       //indicate "delete" message
                commentElemFlag = false;    //indicate not comment element
              }
              else
              {  //not product-delete message
                productElement = null;      //ignore message element
                msgDeleteFlag = false;      //indicate not "delete" message
                commentElemFlag = false;    //indicate not comment element
              }
            }
            else if(MsgTag.QUAKEML_COMMENT.equalsIgnoreCase(
                                                  productElement.getName()))
            {  //element name is "comment"
              if((str=productElement.getChildText(MsgTag.QUAKEML_TEXT,
                                  productElement.getNamespace())) != null &&
                                                    str.trim().length() > 0)
              {  //child-element 'text' contains data
                commentElemFlag = true;     //indicate comment element
                msgDeleteFlag = false;      //indicate not "delete" message
              }
              else
              {  //child-element 'text' empty (is a "delete" message)
                commentElemFlag = true;     //indicate comment element
                msgDeleteFlag = true;       //indicate "delete" message
              }
            }
            else
            {  //no element-name matches for product message
              productElement = null;        //ignore message element
              msgDeleteFlag = false;        //indicate not "delete" message
              commentElemFlag = false;      //indicate not comment element
            }
            break;
          case QWIdentDataMsgRecord.MFMT_ANSSEQXML:   //ANSS-EQ-XML format
                        //get "Event" element:
            eventElement = (Element)(productsIterObj.next());
                        //get "ProductLink" child-element of "Event":
            if((productElement=eventElement.getChild(MsgTag.PRODUCT_LINK,
                                      eventElement.getNamespace())) != null)
            {  //"ProductLink" child-element found
              commentElemFlag = false;      //indicate not Comment element
            }
            else
            {  //"ProductLink" child-element not found
                        //get "Comment" child-element of "Event" (if any):
              productElement = eventElement.getChild(
                                MsgTag.COMMENT,eventElement.getNamespace());
              commentElemFlag = true;       //indicate Comment element
            }
                        //determine if "delete" message:
            msgDeleteFlag = isActionElementDelete(productElement);
            break;
          default:      //QuakeWatch message format
                                       //get "Product" element:
            productElement = (Element)(productsIterObj.next());
            eventElement = null;       //no "Event" element
            commentElemFlag = false;   //indicate not Comment element
                        //determine if "delete" message:
            msgDeleteFlag = isActionAttribDelete(dataMsgElement);
        }
        if(productElement != null)
        {  //element fetched OK; process it
          synchronized(eventsListSyncObj)
          {   //grab thread lock for events list
            if(!msgDeleteFlag)
            {  //not "Delete" message; process as "Update"
                     //create product message record from XML elements:
              newProductRecObj = new QWProductMsgRecord(qwMsgElement,
                                 dataMsgElement,eventElement,productElement,
                                         messageFormatSpec,commentElemFlag);
                     //enter last event message:
//              if(msgHandlerObj != null)     //if msg-handler OK then enter
//                msgHandlerObj.setLastMsgRecordObj(newProductRecObj);
              if(requestedFlag)        //if flag then set "requested" flag
                newProductRecObj.setRequestedFlag(true);
              if(storageProcObj != null && !msgStoredFlag)
              {    //storage manager is setup and msg not yet stored
                storageProcObj.storeRecord(newProductRecObj);
                msgStoredFlag = true;  //indicate 'qwMsgElement' msg stored
              }
                     //check if event record with same key exists in tables:
              if((evtRecObj=eventListManagerObj.getRecordObjForKeyStr(
                                    newProductRecObj.eventIDKey)) instanceof
                                                       QWEQEventMsgRecord)
              {      //event record exists in main event table
                tableTagStr = UtilFns.EMPTY_STRING;   //no tag str for msgs
                mainTableFlag = true;    //indicate found in main table
              }
              else if((obj=filteredEventRecordTable.get(
                                    newProductRecObj.eventIDKey)) instanceof
                                                         QWEQEventMsgRecord)
              {    //event record exists in filtered event table
                evtRecObj = (QWEventMsgRecord)obj;    //set handle to record
                        //check if there's a higher-priority duplicate
                        // event in the main table (if so then it should
                        // receive the product record instead);
                if((hpRecObj=getHigherPriorityEvent(
                                    (QWEQEventMsgRecord)evtRecObj)) == null)
                {  //higher-priority duplicate event not found
                  tableTagStr = "filtered ";   //set tag string for log msgs
                  mainTableFlag = false;    //ind not found in main table
                }
                else
                {  //higher-priority duplicate event was found
                  logObj.debug2("Product message (\"" +
                                newProductRecObj.type + "\") for event \"" +
                                                 evtRecObj.getEventIDKey() +
                            "\" will be added to higher-priority event \"" +
                                           hpRecObj.getEventIDKey() + "\"");
                  evtRecObj = hpRecObj;     //use duplicate record instead
                  tableTagStr = UtilFns.EMPTY_STRING;  //no tag str for msgs
                  mainTableFlag = true;     //indicate found in main table
                }
              }
              else   //matching event record not found
              {
                evtRecObj = null;
                tableTagStr = null;      //indicate not found
                mainTableFlag = false;   //indicate not found in main table
              }
              if(tableTagStr != null)
              {    //matching event record was found
                if((prodRecObj=evtRecObj.getProductMsgRecord(
                                            newProductRecObj.type)) != null)
                {  //previous product of same type exists
                        //check if event-ID in product message is same as
                        // event-ID in event msg (could be diff via QDM):
                  if(newProductRecObj.eventIDKey.equalsIgnoreCase(
                                               evtRecObj.getEventIDKey()) ||
                                    !prodRecObj.eventIDKey.equalsIgnoreCase(
                                                 evtRecObj.getEventIDKey()))
                  {     //event-IDs are same or event-ID of existing
                        // product message is different from event msg
                             //if event-IDs of new and existing product
                             // message are different then accept new
                             // product message; if they are the same
                             // then compare the version codes:
                    final int result;
                    if(!newProductRecObj.eventIDKey.equalsIgnoreCase(
                          prodRecObj.eventIDKey) || (result=compVersionStrs(
                           newProductRecObj.version,prodRecObj.version)) > 0
                                                          || (result == 0 &&
                                      !newProductRecObj.equals(prodRecObj)))
                    {   //event-IDs of new and old product are different or
                        //version code of new product is greater than current
                        // or they are the same & the products are different
                                       //update product in event record:
                      ((QWEQEventMsgRecord)evtRecObj).addProductMsgRecord(
                                                          newProductRecObj);
                      logObj.debug2("Updated \"" + newProductRecObj.type +
                                             "\" product for event (ID=\"" +
                                     evtRecObj.getEventIDKey() + "\") in " +
                                          tableTagStr + "table (new ID=\"" +
                                               newProductRecObj.eventIDKey +
                                    "\" ver=\"" + newProductRecObj.version +
                                   "\", old ID=\"" + prodRecObj.eventIDKey +
                                  "\" ver=\"" + prodRecObj.version + "\")");
                      logObj.debug3(MSG_COLON_STR + xmlMsgStr);
                      if(mainTableFlag)
                      { //matching record was found in main table
                        fireNewProductMsgRecord(newProductRecObj,evtRecObj);
                      }
                    }
                    else
                    {   //version code of new product is <= current
                      logObj.debug2("Ignoring \"" + newProductRecObj.type +
                                        "\" product message with same or " +
                                    "lower version code in " + tableTagStr +
                              "table (ID=\"" + newProductRecObj.eventIDKey +
                                  "\", curProdVer=\"" + prodRecObj.version +
                                                       "\", newProdVer=\"" +
                                          newProductRecObj.version + "\")");
                      logObj.debug3(MSG_COLON_STR + xmlMsgStr);
                    }
                  }
                  else
                  {     //event-ID in product msg is different from event
                    logObj.debug2("Ignoring \"" + newProductRecObj.type +
                                "\" product message with different ID (\"" +
                                               newProductRecObj.eventIDKey +
                                                   "\") than event (ID=\"" +
                                                 evtRecObj.getEventIDKey() +
                                               "\") with existing product");
                    logObj.debug3(MSG_COLON_STR + xmlMsgStr);
                  }
                }
                else      //no previous product of same type in event record
                {                   //add product to event record:
                  ((QWEQEventMsgRecord)evtRecObj).addProductMsgRecord(
                                                          newProductRecObj);
                     //check if product type code string is known:
                  if(productTypeStrsVec.indexOf(newProductRecObj.type) < 0 &&
                                            newProductRecObj.type != null &&
                         !newProductRecObj.type.startsWith(MsgTag.LINK_URL))
                  {  //product-type string not known and not "LinkURL()"
                    logObj.debug2("Product with unexpected type code (\"" +
                                                     newProductRecObj.type +
                          "\") added to to event record in " + tableTagStr +
                              "table (ID=\"" + newProductRecObj.eventIDKey +
                                                       "\", productVer=\"" +
                                          newProductRecObj.version + "\")");
                    logObj.debug2(MSG_COLON_STR + xmlMsgStr);
                  }
                  else
                  {  //product-type string OK
                    logObj.debug2("Added \"" + newProductRecObj.type +
                            "\" product to event record in " + tableTagStr +
                              "table (ID=\"" + newProductRecObj.eventIDKey +
                                                       "\", productVer=\"" +
                                          newProductRecObj.version + "\")");
                    logObj.debug3(MSG_COLON_STR + xmlMsgStr);
                  }
                  fireNewProductMsgRecord(newProductRecObj,evtRecObj);
                }
              }
              else      //no matching event record
              {                   //add product message to cache table:
                addMsgToProductCache(newProductRecObj,LogFile.DEBUG);
                logObj.debug3(MSG_COLON_STR + xmlMsgStr);
              }
            }
            else   //message action is "Delete"
            {   //create delete-product message record from XML elements:
              delProductRecObj = new QWDelProductMsgRecord(qwMsgElement,
                                 dataMsgElement,eventElement,productElement,
                                         messageFormatSpec,commentElemFlag);
                     //enter last event message:
//              if(msgHandlerObj != null)     //if msg-handler OK then enter
//                msgHandlerObj.setLastMsgRecordObj(delProductRecObj);
              if(requestedFlag)        //if flag then set "requested" flag
                delProductRecObj.setRequestedFlag(true);
              if(storageProcObj != null && !msgStoredFlag)
              {    //storage manager is setup and msg not yet stored
                storageProcObj.storeRecord(delProductRecObj);
                msgStoredFlag = true;  //indicate 'qwMsgElement' msg stored
              }
                     //check if event record with same key exists in tables:
              if((evtRecObj=eventListManagerObj.getRecordObjForKeyStr(
                                    delProductRecObj.eventIDKey)) instanceof
                                                         QWEQEventMsgRecord)
              {      //event record exists in main event table
                tableTagStr = UtilFns.EMPTY_STRING;   //no tag str for msgs
                mainTableFlag = true;    //indicate found in main table
              }
              else if((obj=filteredEventRecordTable.get(
                                    delProductRecObj.eventIDKey)) instanceof
                                                         QWEQEventMsgRecord)
              {      //event record exists in filtered event table
                evtRecObj = (QWEventMsgRecord)obj;    //set handle to record
                        //check if there's a higher-priority duplicate
                        // event in the main table (if so then it should
                        // have the product record instead):
                if((hpRecObj=getHigherPriorityEvent(
                                    (QWEQEventMsgRecord)evtRecObj)) == null)
                {  //higher-priority duplicate event not found
                  mainTableFlag = false;   //indicate not found in main table
                  tableTagStr = "filtered ";    //set tag string for log msgs
                }
                else
                {  //higher-priority duplicate event was found
                  logObj.debug2("Product message (\"" +
                                delProductRecObj.type + "\") for event \"" +
                                                 evtRecObj.getEventIDKey() +
                        "\" will be deleted from higher-priority event \"" +
                                           hpRecObj.getEventIDKey() + "\"");
                  evtRecObj = hpRecObj;     //use duplicate record instead
                  tableTagStr = UtilFns.EMPTY_STRING; //no tag str for msgs
                  mainTableFlag = true;     //indicate found in main table
                }
              }
              else   //matching event record not found
              {
                evtRecObj = null;
                tableTagStr = null;      //indicate not found
                mainTableFlag = false;   //indicate not found in main table
              }
              if(tableTagStr != null)
              {    //matching event record was found
                if((prodRecObj=evtRecObj.getProductMsgRecord(
                                            delProductRecObj.type)) != null)
                {  //product of same type exists
                        //check if event-IDs are the same
                        // (could be different via QDM):
                  if(delProductRecObj.eventIDKey.equalsIgnoreCase(
                                                     prodRecObj.eventIDKey))
                  {     //event-IDs are the same
                    if(compVersionStrs(       //compare version codes
                        delProductRecObj.version,prodRecObj.version,1,1) >= 0)
                    {  //delete-message version is empty or >= current version
                                           //remove product from event record:
                      ((QWEQEventMsgRecord)evtRecObj).removeProductMsgRecord(
                                                     delProductRecObj.type);
                      logObj.debug2("Removed \"" + delProductRecObj.type +
                            "\" product from event record in " + tableTagStr +
                                "table (ID=\"" + delProductRecObj.eventIDKey +
                                    "\", delProdVer=\"" + prodRecObj.version +
                                                         "\", msgProdVer=\"" +
                                            delProductRecObj.version + "\")");
                      logObj.debug3(MSG_COLON_STR + xmlMsgStr);
                            //if matching record was found in main table
                      if(mainTableFlag)
                      {
                        fireNewDelProductMsgRecord(delProductRecObj, evtRecObj);
                      }
                    }
                    else
                    {  //version code of new product is < current
                      logObj.debug2("Ignoring delete \"" +
                                                       delProductRecObj.type +
                                "\" product message, version too low (ID=\"" +
                           delProductRecObj.eventIDKey + "\", curProdVer=\"" +
                                 prodRecObj.version + "\", delMsgProdVer=\"" +
                                            delProductRecObj.version + "\")");
                      logObj.debug3(MSG_COLON_STR + xmlMsgStr);
                    }
                  }
                  else
                  {     //event-IDs are not the same
                    logObj.debug2("Ignoring delete \"" +
                                                     delProductRecObj.type +
                                "\" product message with different ID (\"" +
                                               delProductRecObj.eventIDKey +
                                        "\") than existing product (ID=\"" +
                                             prodRecObj.eventIDKey + "\")");
                    logObj.debug3(MSG_COLON_STR + xmlMsgStr);
                  }
                }
                else
                {    //no current product of same type in event record
                  logObj.debug2("Ignoring delete \"" +
                         delProductRecObj.type + "\" product message, no " +
                                 "matching product in event record (ID=\"" +
                                               delProductRecObj.eventIDKey +
                                                    "\", delMsgProdVer=\"" +
                                          delProductRecObj.version + "\")");
                  logObj.debug3(MSG_COLON_STR + xmlMsgStr);
                }
              }
              else      //no matching event record for product message
              {              //check if matching product message is cached:
                if(!chkRemoveProductCache(delProductRecObj))
                {  //matching product message was not cached
                  logObj.debug2("Ignoring delete \"" +
                         delProductRecObj.type + "\" product message, no " +
                                            "matching event record (ID=\"" +
                                               delProductRecObj.eventIDKey +
                                                    "\", delMsgProdVer=\"" +
                                          delProductRecObj.version + "\")");
                }
                logObj.debug3(MSG_COLON_STR + xmlMsgStr);
              }
            }
            ++msgCount;           //increment message count
          }
        }
      }
      catch(QWRecordException ex)
      {  //error creating message-record obect; log it
        logObj.debug2("Error creating record for product message:  " + ex);
        logObj.debug3(MSG_COLON_STR + xmlMsgStr);
        logObj.debug3(UtilFns.getStackTraceString(ex));
      }
      catch(Exception ex)
      {  //some kind of exception error; log it
        logObj.warning("Error processing product message:  " + ex);
        logObj.debug(MSG_COLON_STR + xmlMsgStr);
        logObj.debug(UtilFns.getStackTraceString(ex));
      }
    }

    QWStationAmpMsgRecord stationAmpRecObj;
         //get iterator for list of "StationAmp" elements in message:
    final Iterator elemIterObj =
                  dataMsgElement.getChildren(MsgTag.STATION_AMP).iterator();
    Element stationAmpElement;
    while(elemIterObj.hasNext())
    {    //for each "StationAmp" element; fetch 'Element' object from list
      if((obj=elemIterObj.next()) instanceof Element &&
                                        MsgTag.STATION_AMP.equalsIgnoreCase(
                                (stationAmpElement=(Element)obj).getName()))
      {  //element fetched OK; process it
        try
        {
          synchronized(eventsListSyncObj)
          {   //grab thread lock for events list

              //test if "Action" attribute of "DataMessage" is "Delete":
            if(!isActionAttribDelete(dataMsgElement))
            {   //action is not "Delete"; process as "Update"
                     //create event message record from XML elements:
              stationAmpRecObj = new QWStationAmpMsgRecord(
                             qwMsgElement,dataMsgElement,stationAmpElement);
                     //enter last event message:
//              if(msgHandlerObj != null)     //if msg-handler OK then enter
//                msgHandlerObj.setLastMsgRecordObj(newEventRecObj);
              if(requestedFlag)        //if flag then set "requested" flag
                stationAmpRecObj.setRequestedFlag(true);
              if(storageProcObj != null && !msgStoredFlag)
              {    //storage manager is setup and msg not yet stored
                storageProcObj.storeRecord(stationAmpRecObj);
                msgStoredFlag = true;  //indicate 'qwMsgElement' msg stored
              }
              if(storageMinDateObj == null ||
                                           (stationAmpRecObj.time != null &&
                        !(stationAmpRecObj.time.before(storageMinDateObj))))
              { //msg record date/time not before minimum storage date/time
                         //check if record with same key already in table:
                if((evtRecObj=eventListManagerObj.getRecordObjForKeyStr(
                                 stationAmpRecObj.getEventIDKey())) != null)
                {      //record with same key already in table
                       //update in table (if version newer or msg different):
                  if(updateEventInTable(eventListManagerObj,evtRecObj,
                                                          stationAmpRecObj))
                  {    //version is newer (or message is different)
                    logObj.debug2(
                                "Updated amplitude record in table (ID=\"" +
                        stationAmpRecObj.getEventIDKey() + "\", newVer=\"" +
                           stationAmpRecObj.getVersion() + "\", oldVer=\"" +
                                            evtRecObj.getVersion() + "\")");
                    logObj.debug3(MSG_COLON_STR + xmlMsgStr);
                    fireNewEventMsgRecord(stationAmpRecObj);
                  }
                  else
                  {    //version is not newer; ignore message
                    logObj.debug2("Ignoring amplitude message with same " +
                                            "or lower version code (ID=\"" +
                                          stationAmpRecObj.getEventIDKey() +
                              "\", currentVer=\"" + evtRecObj.getVersion() +
                        "\", newMsgVer=\"" + stationAmpRecObj.getVersion() +
                                                                     "\")");
                    logObj.debug3(MSG_COLON_STR + xmlMsgStr);
                  }
                }
                else    //record with same key not already in table
                {            //check if any products for event in
                             // product-message cache and add if so:
                  chkFetchFromProductCache(stationAmpRecObj);
                  if((passesFilterFlag=(filterPropertiesObj == null ||
                     filterPropertiesObj.passesFilter(stationAmpRecObj))) &&
                                                 addEvent(stationAmpRecObj))
                  {     //record passes filter and PDL-merge OK
                    logObj.debug2(
                              "Added new amplitude record to table (ID=\"" +
                           stationAmpRecObj.getEventIDKey() + "\", ver=\"" +
                                     stationAmpRecObj.getVersion() + "\")");
                    logObj.debug3(MSG_COLON_STR + xmlMsgStr);
                    fireNewEventMsgRecord(stationAmpRecObj);
                  }
                  else      //record does not pass filter or PDL-merge
                  {         //add record to table of filtered event records:
                    addOrUpdateToFilteredEventTable(stationAmpRecObj);
                    logObj.debug2("New amplitude record does not pass " +
                      ((passesFilterFlag)?"PDL-merge":"filter parameters") +
                               ":  " + stationAmpRecObj.getDisplayString());
                    logObj.debug3(MSG_COLON_STR + xmlMsgStr);
                  }
                }
              }
              else
              { //msg record date/time is before minimum storage date/time
                logObj.debug3("New amplitude record discarded, does not " +
                                              "pass storage-minimum date:  " +
                                         stationAmpRecObj.getDisplayString());
                logObj.debug4(MSG_COLON_STR + xmlMsgStr);
              }
            }
            else
            { //data-message action is "Delete"
              logObj.warning("Delete amplitude record ignored");
              logObj.debug(MSG_COLON_STR + xmlMsgStr);
            }
            ++msgCount;           //increment message count
          }
        }
        catch(QWRecordException ex)
        {  //error creating message-record obect; log it
          logObj.debug2("Error creating record for \"" +
                                 MsgTag.STATION_AMP + "\" message:  " + ex);
          logObj.debug3(MSG_COLON_STR + xmlMsgStr);
          logObj.debug3(UtilFns.getStackTraceString(ex));
        }
        catch(Exception ex)
        {  //some kind of exception error; log it
          logObj.warning("Error processing \"" + MsgTag.STATION_AMP +
                                                      "\" message:  " + ex);
          logObj.debug(MSG_COLON_STR + xmlMsgStr);
          logObj.debug(UtilFns.getStackTraceString(ex));
        }
      }
      else
      {  //error fetching element (shouldn't happen)
        logObj.warning("Error fetching \"" + MsgTag.STATION_AMP +
                                             "\" element from list object");
      }
    }

    if(msgCount == 0)
    {  //no valid message-elements found
      if(storeInvalidMessagesFlag)
      {  //flag set to store invalid messages
        logObj.debug2(
                "No valid elements found in data message; storing message");
        logObj.debug2(MSG_COLON_STR + xmlMsgStr);
        if(!msgStoredFlag && storageProcObj != null)
        {  //message was not previously stored and storage processor setup
          try           //create message-record object from given elements
          {             // and give to storage processor:
            storageProcObj.storeRecord(
                          new QWDataMsgRecord(qwMsgElement,dataMsgElement));
//          msgStoredFlag = true;  //indicate 'qwMsgElement' msg stored
          }
          catch(Exception ex)
          {  //some kind of exception error; log it
            logObj.warning(
               "Error storing data message with no valid elements:  " + ex);
            logObj.warning(MSG_COLON_STR + xmlMsgStr);
          }
        }
      }
      else
      {  //not storing invalid messages or storage processor not setup
        logObj.debug("No valid elements found in data message");
        logObj.debug(MSG_COLON_STR + xmlMsgStr);
      }
    }
  }

  /**
   * Returns the "origin" child-element of the given element (if available).
   * An "origin" element at the same level as the given element may also
   * be return (if found).
   * @param messageFormatSpec specifier for message-format type (one of
   * the 'QWIdentDataMsgRecord.MFMT_...' values).
   * @param eventElement "event" element object.
   * @return The "origin" child-element of the given element, or null if
   * not available.
   */
  private Element getOriginElement(int messageFormatSpec,
                                                       Element eventElement)
  {
    if(eventElement == null)
      return null;
    switch(messageFormatSpec)
    {  //process data depending on format specification
      case QWIdentDataMsgRecord.MFMT_QUAKEML:    //QuakeML message format
              //check name 'event'; return child-element 'origin' (if any):
        if(MsgTag.QUAKEML_EVENT.equalsIgnoreCase(eventElement.getName()))
        {  //element name is 'event'
          Element elemObj;
          if((elemObj=eventElement.getChild(
                MsgTag.QUAKEML_ORIGIN,eventElement.getNamespace())) != null)
          {  //child-element 'origin' found under 'event' element
            return elemObj;
          }
          if((elemObj=eventElement.getParent()) != null &&
                            (elemObj=elemObj.getChild(MsgTag.QUAKEML_ORIGIN,
                                           elemObj.getNamespace())) != null)
          {  //child-element 'origin' found at same level as 'event' element
            return elemObj;
          }
        }
        return null;

      case QWIdentDataMsgRecord.MFMT_ANSSEQXML:  //ANSS-EQ-XML message format
              //return child-element 'origin' (if any):
        return eventElement.getChild(
                                 MsgTag.ORIGIN,eventElement.getNamespace());

      default:          //QuakeWatch message format
        return null;
    }
  }

  /**
   * Determines if the given message-element object contains an "action"
   * element or attribute for "trump".
   * @param messageFormatSpec specifier for message-format type (one of
   * the 'QWIdentDataMsgRecord.MFMT_...' values).
   * @param eventElement "event" or "useControl" element object.
   * @param dataMsgElement "DataMessage" element object.
   * @return true if the given message-element object contains an "action"
   * element or attribute for "trump"; false if not.
   */
  private boolean isActionTrump(int messageFormatSpec, Element eventElement,
                                                     Element dataMsgElement)
  {
    final String str;
    switch(messageFormatSpec)
    {  //process data depending on format specification
      case QWIdentDataMsgRecord.MFMT_QUAKEML:    //QuakeML message format
                   //test for 'useControl'|'action' value of "trump":
        return (eventElement != null &&
              MsgTag.USE_CONTROL.equalsIgnoreCase(eventElement.getName()) &&
                                             (str=eventElement.getChildText(
              MsgTag.QUAKEML_ACTION,eventElement.getNamespace())) != null &&
                                str.equalsIgnoreCase(MsgTag.QUAKEML_TRUMP));

      case QWIdentDataMsgRecord.MFMT_ANSSEQXML:  //ANSS-EQ-XML message format
                   //test for 'action' child-element value of "trump":
        return (eventElement != null && (str=eventElement.getChildText(
                      MsgTag.ACTION,eventElement.getNamespace())) != null &&
                                        str.equalsIgnoreCase(MsgTag.TRUMP));

      default:                                   //QuakeWatch message format
                   //test for 'action' attribute value of "trump":
        return (dataMsgElement != null &&
            (str=dataMsgElement.getAttributeValue(MsgTag.ACTION)) != null &&
                                        str.equalsIgnoreCase(MsgTag.TRUMP));
    }
  }

  /**
   * Determines if the given message-element object contains an "action"
   * element or attribute for "delete".
   * @param messageFormatSpec specifier for message-format type (one of
   * the 'QWIdentDataMsgRecord.MFMT_...' values).
   * @param eventElement "event" or "useControl" element object.
   * @param dataMsgElement "DataMessage" element object.
   * @return true if the given message-element object contains an "action"
   * element or attribute for "delete"; false if not.
   */
  private boolean isActionDelete(int messageFormatSpec, Element eventElement,
                                                     Element dataMsgElement)
  {
    final String str;
    switch(messageFormatSpec)
    {  //process data depending on format specification
      case QWIdentDataMsgRecord.MFMT_QUAKEML:    //QuakeML message format
                   //test for 'useControl'|'action' value of delete":
        return (eventElement != null &&
              MsgTag.USE_CONTROL.equalsIgnoreCase(eventElement.getName()) &&
                                             (str=eventElement.getChildText(
              MsgTag.QUAKEML_ACTION,eventElement.getNamespace())) != null &&
                               str.equalsIgnoreCase(MsgTag.QUAKEML_DELETE));

      case QWIdentDataMsgRecord.MFMT_ANSSEQXML:  //ANSS-EQ-XML message format
                   //test for 'action' child-element value of "Delete":
        return (eventElement != null && (str=eventElement.getChildText(
                      MsgTag.ACTION,eventElement.getNamespace())) != null &&
                                       str.equalsIgnoreCase(MsgTag.DELETE));

      default:                                   //QuakeWatch message format
                   //test for 'action' attribute value of "Delete":
        return (dataMsgElement != null &&
            (str=dataMsgElement.getAttributeValue(MsgTag.ACTION)) != null &&
                                       str.equalsIgnoreCase(MsgTag.DELETE));
    }
  }

  /**
   * Determines if the given delete message references an event record.
   * @param messageFormatSpec specifier for message-format type (one of
   * the 'QWIdentDataMsgRecord.MFMT_...' values).
   * @param elemObj "event" or "useControl" element object.
   * @return true if the given delete message references an event record or
   * is in "QWmessage" format; false if not.
   */
  private boolean isDeleteEventMsg(int messageFormatSpec,
                                                       Element elemObj)
  {
    final String str;
    switch(messageFormatSpec)
    {  //process data depending on format specification
      case QWIdentDataMsgRecord.MFMT_QUAKEML:    //QuakeML message format
                   //get 'useControl'|'elementID' value:
        if(MsgTag.USE_CONTROL.equalsIgnoreCase(elemObj.getName()) &&
                                                  (str=elemObj.getChildText(
                         MsgTag.ELEMENT_ID,elemObj.getNamespace())) != null)
       {  //'useControl'|'elementID' value found; process as resource-id
         final ResourceIdentifier resIdObj = new ResourceIdentifier(str);
                   //return true if resource type is "evt":
         return MsgTag.QUAKEML_EVT_STR.equalsIgnoreCase(
                                             resIdObj.getResourceTypeStr());
       }
       return false;

      case QWIdentDataMsgRecord.MFMT_ANSSEQXML:  //ANSS-EQ-XML message format
                   //test for 'ProductLink' child-element value of 'Event':
        return (elemObj.getChild(
                       MsgTag.PRODUCT_LINK,elemObj.getNamespace()) == null);

      default:          //QuakeWatch message format
        return true;
    }
  }

  /**
   * Tests if the given 'Element' contains an "Action" child-element that
   * is equal to "Delete".
   * @param elemObj the 'Element' object to use.
   * @return true if the given 'Element' contains an "Action" child-element
   * that is equal to "Delete", otherwise returns false.
   */
  private boolean isActionElementDelete(Element elemObj)
  {
    final String str;
    return (elemObj != null && (str=elemObj.getChildText(
                           MsgTag.ACTION,elemObj.getNamespace())) != null &&
                                       str.equalsIgnoreCase(MsgTag.DELETE));
  }

  /**
   * Tests if the given 'Element' contains an "Action" attribute that is
   * equal to "Delete".
   * @param elemObj the 'Element' object to use.
   * @return true if the given 'Element' contains an "Action" attribute
   * that is equal to "Delete", otherwise returns false.
   */
  private boolean isActionAttribDelete(Element elemObj)
  {
    final String str;
    return (elemObj != null &&
                   (str=elemObj.getAttributeValue(MsgTag.ACTION)) != null &&
                                       str.equalsIgnoreCase(MsgTag.DELETE));
  }

  /**
   * Compares version strings.  If numeric version strings given then a
   * numeric comparison is performed (so, for example, "10" is greater
   * than "9").
   * @param ver1Str first version string.
   * @param ver2Str second version string.
   * @param def1Val default value to be returned if first version string
   * contains no data (null or empty) and second version string contains
   * data.
   * @param bothNullVal value to be returned if both version strings
   * contain no data (null or empty).
   * @return An integer that is less than, equal to or greater than zero
   * as the first string is less than, equal to or greater than the second
   * string (or one of the given values).
   */
  public static int compVersionStrs(String ver1Str, String ver2Str,
                                               int def1Val, int bothNullVal)
  {
    if(ver1Str == null || (ver1Str=ver1Str.trim()).length() <= 0)
    {  //first version string is null or empty
      if(bothNullVal == def1Val)       //if return values same then
        return def1Val;                // return it
              //if both strings null/empty then return both-null value:
      return (ver2Str == null || (ver2Str=ver2Str.trim()).length() <= 0) ?
                                                      bothNullVal : def1Val;
    }
    if(ver2Str == null || (ver2Str=ver2Str.trim()).length() <= 0)
      return -1;
    try       //if numeric version strings then do numeric comparison
    {         // (so, for example, "10" is greater than "9"):
      if(Character.isDigit(ver1Str.charAt(0)))
      {  //looks like numeric strings in use
        return Float.compare(     //return comparison if valid float values:
                       Float.parseFloat(ver1Str),Float.parseFloat(ver2Str));
      }
    }
    catch(Exception ex)
    {  //unable to convert as float values; use comparison below
    }
    return ver1Str.compareTo(ver2Str);
  }

  /**
   * Compares version strings.  If numeric version strings given then a
   * numeric comparison is performed (so, for example, "10" is greater
   * than "9").
   * @param ver1Str first version string.
   * @param ver2Str second version string.
   * @return An integer that is less than, equal to or greater than zero
   * as the first string is less than, equal to or greater than the second
   * string.
   */
  public static int compVersionStrs(String ver1Str, String ver2Str)
  {
    return compVersionStrs(ver1Str,ver2Str,-1,0);
  }

  /**
   * Adds the 'QWMsgObjsListener' object to the list being used.  This object's
   * methods will be called when new event-message record objects are
   * created in response to XML messages received from the QWServer.
   * @param listenerObj the listener object to add.
   */
  public void addMsgObjsListener(QWMsgObjsListener listenerObj)
  {
    msgObjsListenerListObj.add(listenerObj);
  }

  /**
   * Removes the 'QWMsgObjsListener' object from the list being used.
   * @param listenerObj the listener object to remove.
   */
  public void removeMsgObjsListener(QWMsgObjsListener listenerObj)
  {
    msgObjsListenerListObj.remove(listenerObj);
  }

  /**
   * Sets the 'QWMsgObjsListener' object to be used.  This object's
   * methods will be called when new event-message record objects are
   * created in response to XML messages received from the QWServer.
   * @param listenerObj the listener object to use.
   */
  public void setMsgObjsListener(QWMsgObjsListener listenerObj)
  {
    msgObjsListenerObj = listenerObj;
  }

  /**
   * Updates the minimum-storage date from the 'maxLoadEventAgeDays'
   * property.  New events before this date will be discarded, and
   * when 'reapplyEventsFilter()' is performed, events before this
   * date/time will be deleted.
   */
  public void updateStorageMinimumDate()
  {
         //calculate minimum date/time for events:
    long val;
                   //get value for 'maxLoadEventAgeDays' property:
    if((val=(long)(maxLoadEventAgeDaysProp.doubleValue()*
                                                   UtilFns.MS_PER_DAY)) > 0)
    {    //maximum age-in-days value is greater than zero
              //create Date object at current time minus age-in-hours:
      storageMinDateObj = new Date(System.currentTimeMillis() - val);
    }
    else      //no minumim age value
      storageMinDateObj = null;        //indicate no date/time limit
//    logObj.debug3("QWMsgManager.updateStorageMinimumDate:  " + storageMinDateObj);
  }

  /**
   * Reapplies the storage and filter parameters to the table of events
   * held by this message manager, removing any event record objects that
   * no longer pass the parameters.
   * @param reloadFilteredFlag true to have the table of filtered events
   * reprocessed into the main table, false to not.
   * @return true if the main table of events has been changed, false if
   * not.
   */
  public boolean reapplyEventsFilter(boolean reloadFilteredFlag)
  {
    final int pauseLimit = 100;        //cycle count for thread "pausing"
    final int pauseDelayMs = 50;       //thread "pausing" time in ms
    boolean changeFlag = false;        //initialize "change" flag
    try
    {
      int idx = 0, pauseCounter;
      Object obj;
      QWEventMsgRecord recObj;
              //do "pauses" during processing so as not to monopolize
              // the synchronization lock on the table or take up
              // too much CPU time:
                        //disable list-change listeners while reapplying:
      eventListManagerObj.setQuakeListListenersEnabled(false);
      nextEventRecordIndex = 0;        //reset next-record index value
                                       //clear events-list-changed flag
      eventListManagerObj.clearEventsListChangedFlag();
      checkEventsOuterLoop:       //label for break statement below
      while(true)
      {       //loop through iterations and pauses
        pauseCounter = 0;         //initialize pause counter
        synchronized(eventsListSyncObj)
        {     //don't allow other threads to access list data during reapply
                   //if events-list table was modified during last pause
                   // then restart at beginning of table (need to do
                   // this inside of the 'synchronized' block):
          if(eventListManagerObj.getEventsListChangedFlag())
            idx = 0;           //if table changed then restart at beginning
          Date dateObj;
          int eventsListSize = eventListManagerObj.getEventsListSize();
          while(true)
          {   //for each event record object in main table
            if(idx >= eventsListSize)
            { //done processing all events in main table
                   //if flag set then go through filtered-out events and see
                   // which should be put back into the main table:
                   // (do it here while lock for events-list is held)
              if(reloadFilteredFlag)
              {    //flag set; reload filtered-out events into main table
//                addEventsVec(filteredEventRecordTable.values());
                logObj.debug4("reapplyEventsFilter:  Reloading and " +
                                        "processing filtered-events table");
                synchronized(filteredEventRecordTable)
                { //grab thread lock for filtered-events table
                        //copy filtered-event records to new Vector:
                  final Vector vec =
                                 filteredEventRecordTable.getValuesVector();
                  filteredEventRecordTable.clear();   //clear filtered table
                  final Iterator iterObj = vec.iterator();
                  while(iterObj.hasNext())
                  {     //for each filtered-event record
                    if((obj=iterObj.next()) instanceof QWEventMsgRecord)
                    {   //event record object fetched OK
                      recObj = (QWEventMsgRecord)obj;   //set handle to obj
                      if(storageMinDateObj == null ||
                                      ((dateObj=recObj.getTime()) != null &&
                                      !(dateObj.before(storageMinDateObj))))
                      {      //filtered event passes storage filter OK
                        if(filterPropertiesObj == null ||
                                   filterPropertiesObj.passesFilter(recObj))
                        {    //record passes filter parameters
                             //if evt with same ID not already in table then
                             // add event record to table, via event merge:
                          if(!eventListManagerObj.isEventKeyInList(
                                                    recObj.getEventIDKey()))
                          {
                            addEvent(recObj);    //add event (via merge)
                            changeFlag = true;   //ind events table changed
                          }
                          if(++pauseCounter >= pauseLimit)   //inc pause ctr
                          {  //pause count limit reached; do a delay
                            pauseCounter = 0;         //reset counter
                            try { Thread.sleep(pauseDelayMs); }
                            catch(InterruptedException ex) {}
                          }
                        }
                        else      //record does not pass filter parameters
                        {              //put back into filtered table:
                          filteredEventRecordTable.addEventToList(recObj);
                        }
                      }
                      else
                      {      //filtered event does not pass storage filter
                        logObj.debug2("Filtered event record deleted via " +
                                  "reapply:  " + recObj.getDisplayString());

                      }
                    }
                  }
                }
              }
              break checkEventsOuterLoop;        //exit outer loop
            }
            if((recObj=eventListManagerObj.getRecordObjForIndex(idx++)) !=
                                                                       null)
            {  //record fetched OK
              if(storageMinDateObj == null ||
                                      ((dateObj=recObj.getTime()) != null &&
                                      !(dateObj.before(storageMinDateObj))))
              {    //event passes storage filter OK
                if(filterPropertiesObj != null &&
                                  !filterPropertiesObj.passesFilter(recObj))
                {  //event does not pass filter
                          //decrement index and remove event record:
                  eventListManagerObj.removeEventForIndex(--idx);
                  --eventsListSize;    //decrement table size
                                       //put record into filtered table:
                  filteredEventRecordTable.addEventToList(recObj);
                  changeFlag = true;       //indicate events table changed
                  logObj.debug2("Event record filtered out via reapply:  " +
                                                 recObj.getDisplayString());
                }
              }
              else
              {   //event record does not pass storage minimum
                          //decrement index and delete event record:
                eventListManagerObj.removeEventForIndex(--idx);
                --eventsListSize;           //decrement table size
                changeFlag = true;          //indicate events table changed
                logObj.debug2("Event record deleted via reapply:  " +
                                                 recObj.getDisplayString());
              }
            }
            if(++pauseCounter >= pauseLimit)     //increment pause counter
            {      //pause count limit reached
              pauseCounter = 0;        //reset counter
                                       //clear change flag:
              eventListManagerObj.clearEventsListChangedFlag();
              break;                   //exit inner loop
            }
          }
        }
              //do pause delay outside of synchronized block:
        try { Thread.sleep(pauseDelayMs); }
        catch(InterruptedException ex) {}
      }
    }
    catch(Exception ex)
    {    //some kind of exception error; log warning message
      logObj.warning("QWMsgManager.reapplyEventsFilter():  " + ex);
      logObj.debug(UtilFns.getStackTraceString(ex));
    }
                        //re-enable list-change listeners:
    eventListManagerObj.setQuakeListListenersEnabled(true);
    if(changeFlag)      //if events table changed then fire change-event
      eventListManagerObj.fireQuakeListContentsChanged();
                        //keep selected event in view on quake list:
    eventListManagerObj.ensureQuakeListSelectionVisible();
    nextEventRecordIndex = 0;          //reset next-record index value
    return changeFlag;       //return change flag
  }

  /**
   * Reapplies the storage and filter parameters to all events.  The
   * 'setupApplyParamsObjs()' method may be used to setup the dialog-popup
   * and completion call-back objects for this method.
   */
  public void applyStorageAndFilterParams()
  {
    logObj.debug4("entered QWMsgManager.applyStorageAndFilterParams(), " +
      "applyStorageAndFilterParamsFlag=" + applyStorageAndFilterParamsFlag);
         //need to ignore reentry attempts into method that can be caused
         // by modifying the "keep/dropDataSources" properties:
    if(applyStorageAndFilterParamsFlag)
      return;           //if already performing action then just return
    applyStorageAndFilterParamsFlag = true;      //indicate in method
    try
    {
      if(eventListManagerObj.getEventsListSize() > 0 ||
                                      getFilteredEventRecordTableSize() > 0)
      {     //events list or filtered-events list contains items
                 //create thread that will reapply storage-age
                 // and filter to event records:
        final Thread applyParamsThread =
                                 new Thread("applyStorageAndFilterParams")
            {
              public void run()
              {
                logObj.debug4("started QWMsgManager." +
                                  "applyStorageAndFilterParams() thread");
                try     //delay in case multiple settings are changing and
                {       // to give the popup dialog a chance to display:
                  UtilFns.sleep(250);
                  updateStorageMinimumDate();    //setup min-storage date
                  reapplyEventsFilter(true);     //perform reapply
                  if(applyParamsCompletedListenerObj != null)
                  {     //listener was given; invoke call-back method
                    applyParamsCompletedListenerObj.dataChanged(
                                                         QWMsgManager.this);
                  }
                }
                catch(Exception ex)
                {     //some kind of exception error; log warning message
                  logObj.warning(
                            "QWMsgManager.applyStorageAndFilterParams()" +
                                                       " thread:  " + ex);
                  logObj.debug(UtilFns.getStackTraceString(ex));
                }
                                                 //indicate complete:
                applyStorageAndFilterParamsFlag = false;
                if(applyParamsDialogInterfaceObj != null)
                {  //popup was given; close popup (wait for visible)
                  applyParamsDialogInterfaceObj.closeWithWait(500);
                }
                logObj.debug4(
                           "finished applyStorageAndFilterParams thread");
              }
            };
              //if popup object given then show (via event-dispatch thread)
        if(applyParamsDialogInterfaceObj != null)
          applyParamsDialogInterfaceObj.setVisibleViaInvokeLater(true);
        applyParamsThread.start();          //launch reapply worker thread
      }
      else
      {     //no events loaded
        updateStorageMinimumDate();               //setup min-storage date
        applyStorageAndFilterParamsFlag = false;  //indicate complete
      }
    }
    catch(Exception ex)
    {    //some kind of exception error; log warning message
      logObj.warning("QWMsgManager.applyStorageAndFilterParams():  " + ex);
      logObj.debug(UtilFns.getStackTraceString(ex));
      applyStorageAndFilterParamsFlag = false;   //indicate complete
    }
  }

  /**
   * Returns event records from the event table one at time in a
   * round-robin fashion.
   * @return The next 'QWEventMsgRecord' object in the event table,
   * or null if the table is empty.
   */
  public QWEventMsgRecord getNextEventMsgRecord()
  {
    try
    {
      synchronized(eventsListSyncObj)
      {  //get thread lock so data can't change between getSize and getElem
        final int len;
        if((len=eventListManagerObj.getEventsListSize()) <= 0)
          return null;         //if table empty then return null
        if(++nextEventRecordIndex >= len)   //increment to next record
          nextEventRecordIndex = 0;    //if reached end then wrap around
        final Object obj;
        return ((obj=eventListManagerObj.getRecordObjForIndex(
                       nextEventRecordIndex)) instanceof QWEventMsgRecord) ?
                                               (QWEventMsgRecord)obj : null;
      }
    }
    catch(Exception ex)
    {         //some kind of error; just return null
      return null;
    }
  }

  /**
   * Checks the given event message record object, testing if the
   * event date/time is before the minimum storage date/time and if the
   * record passes the filter, removing or moving if so.
   * @param recObj the event message record to use.
   * @return true if the event message record was removed from the
   * main table, false if not.
   */
  public boolean checkFilterEventMsgRecord(QWEventMsgRecord recObj)
  {
    if(storageMinDateObj != null &&
                                 recObj.getTime().before(storageMinDateObj))
    {    //minimum storage date set and event time is before it
      logObj.debug2("Deleting displayed event (too old):  " +
                                                 recObj.getDisplayString());
                                                 //remove event from table:
      eventListManagerObj.removeEventFromList(recObj.getEventIDKey());
      return true;
    }
    if(filterPropertiesObj != null &&
                                  !filterPropertiesObj.passesFilter(recObj))
    {    //record does not pass filter
      logObj.debug2("Filtering-out displayed event:  " +
                                                 recObj.getDisplayString());
                                                 //remove event from table:
      eventListManagerObj.removeEventFromList(recObj.getEventIDKey());
                                      //put record into filtered table:
      filteredEventRecordTable.addEventToList(recObj);
//      filteredEventTableChangedFlag = true;      //indicate change
      return true;
    }
    return false;
  }

  /**
   * Processes event records in the filtered table one at time in a
   * round-robin fashion, testing each to see if the event date/time
   * is before the minimum storage date/time and removing if so.
   * @return true if the event message record was removed from the
   * filtered table, false if not.
   */
  public boolean processNextFilteredEventMsgRecord()
  {
    if(storageMinDateObj == null)      //if no minimum storage date then
      return false;                    //just return
    try
    {
      synchronized(filteredEventRecordTable)
      {  //get thread lock so table can't change between getSize and getElem
        final int len;
        if((len=filteredEventRecordTable.size()) <= 0)
          return false;      //if table empty then return null
        if(++nextFilteredRecordIndex >= len)     //increment to next record
          nextFilteredRecordIndex = 0;      //if reached end then wrap around
        final Object obj;
        final QWEventMsgRecord recObj;
        if((obj=filteredEventRecordTable.elementAt(
                      nextFilteredRecordIndex)) instanceof QWEventMsgRecord)
        {  //next record fetched OK
          recObj = (QWEventMsgRecord)obj;          //set handle to record
          if(recObj.getTime().before(storageMinDateObj))
          {    //event time is before minimum storage time
            logObj.debug2("Deleting filtered event (too old):  " +
                                                 recObj.getDisplayString());
                                       //remove event from filtered table:
            filteredEventRecordTable.remove(recObj.getEventIDKey());
//            filteredEventTableChangedFlag = true;     //indicate change
            return true;
          }
        }
      }
    }
    catch(Exception ex)
    {         //some kind of error; just return false
    }
    return false;
  }

  /**
   * This method may be called on a periodic basis to filter-out and
   * remove events as they age.
   */
  public void performNextEventsCheck()
  {
         //update the minimum storage date:
    updateStorageMinimumDate();
    QWEventMsgRecord recObj;
         //check next event and filter-out if no longer passes:
    if((recObj=getNextEventMsgRecord()) != null)
      checkFilterEventMsgRecord(recObj);
         //check next filtered event and delete if too old:
    processNextFilteredEventMsgRecord();
  }

  /**
   * This method should be called on a periodic basis to remove old
   * messages from the product-message cache.
   */
  public void delOldProductCacheMsgs()
  {
    if(!useProductCacheFlag)      //if product cache not in use then
      return;                     //abort method
    final Iterator iterObj;
         //remove entries that are too old (up to 10 at a time):
    if((iterObj=productCacheTableObj.removeOldEntries(
        System.currentTimeMillis()-MAX_PRODUCTCACHE_AGEMS,10,true)) != null)
    {    //entries were removed
      Object obj;
      QWProductMsgRecord prodRecObj;
      while(iterObj.hasNext())
      {  //for each removed product-message record
        if((obj=iterObj.next()) instanceof QWProductMsgRecord)
        {     //product-message-record object fetched OK
          prodRecObj = (QWProductMsgRecord)obj;
          logObj.debug("Removed \"" + prodRecObj.type + "\" product " +
                                  "message from cache, no matching event " +
                         "record received (ID=\"" +  prodRecObj.eventIDKey +
                          "\", productVer=\"" + prodRecObj.version + "\")");
        }
        else
        {     //unable to fetch product-message-record object
          logObj.info(
              "Unknown object removed from product-message cache:  " + obj);
        }
      }
    }
  }

  /**
   * Returns the table of event message record objects that have been
   * filtered out.
   * @return A 'FifoHashtable' object of 'QWEventMsgRecord' objects.
   */
//  public FifoHashtable getFilteredEventRecordTable()
//  {
//    return filteredEventRecordTable;
//  }

  /**
   * Returns the 'QWFilterProperties' object in use.
   * @return The 'QWFilterProperties' object in use, or null if none.
   */
  public QWFilterProperties getFilterProperties()
  {
    return filterPropertiesObj;
  }

  /**
   * Returns a list of event message record objects that have been
   * filtered out.  Events not displayed because of PDL-low-priority
   * are also included in the list.
   * @return A new list of event message record objects that have been
   * filtered out.
   */
  public List getFilteredEventsList()
  {
    return filteredEventRecordTable.getValuesVector();
  }

  /**
   * Returns the event message record object for the given key string.
   * @param keyStr event-ID key string.
   * @return The event message record object for the given key string,
   * or null if no match.
   */
  public QWEventMsgRecord getFilteredEvtMsgRecObj(String keyStr)
  {
    final Object obj;
    if((obj=filteredEventRecordTable.get(keyStr)) instanceof QWEventMsgRecord)
      return (QWEventMsgRecord)obj;
    return null;
  }

  /**
   * Registers the given 'DataChangedListener' object to be notified
   * when the filtered-events table is changed.
   * @param listenerObj the 'DataChangedListener' object.
   */
  public void addFilteredEvtsListChgListener(DataChangedListener listenerObj)
  {
    filteredEventRecordTable.addDataChangedListener(listenerObj);
  }

  /**
   * Unregisters the given 'DataChangedListener' object from the list of
   * listeners for the filtered-events table.
   * @param listenerObj the 'DataChangedListener' object.
   */
  public void removeFilteredEvtsListChgListener(
                                            DataChangedListener listenerObj)
  {
    filteredEventRecordTable.removeDataChangedListener(listenerObj);
  }

  /**
   * Returns the size of the table of event message record objects that
   * have been filtered out.
   * @return The number of event message records in the filtered table.
   */
  public int getFilteredEventRecordTableSize()
  {
    return filteredEventRecordTable.size();
  }

  /**
   * Returns the time of the last "Alive" message received from the
   * server, in milliseconds since 1/1/1970.
   * @return The time of the last "Alive" message received from the
   * server, or 0 if none have been received.
   */
//  public long getLastAliveTime()
//  {
//    return lastAliveTime;
//  }

  /**
   * Returns the last event held in storage.  This method is needed
   * to implement the 'QWDataMsgProcessor' interface.
   * @return The 'QWMsgNumTimeRec' object for the last event held in
   * storage, or null if none available.
   */
  public QWMsgNumTimeRec getLastEventInStorage()
  {
    return eventListManagerObj.getLastEventInList();
  }

  /**
   * Updates an event already in the given list-table (if the version
   * code is newer or the same and the message is different).
   * @param listObj the list manager containing the table of events.
   * @param oldRecObj the current event record in the table.
   * @param newRecObj the new event record.
   * @return true if the event was updated, false if not.
   */
  private boolean updateEventInTable(EventAdderInterface listObj,
                      QWEventMsgRecord oldRecObj,QWEventMsgRecord newRecObj)
  {
    final int result = compVersionStrs(
                             newRecObj.getVersion(),oldRecObj.getVersion());
    if(result > 0 || (result == 0 && !newRecObj.equals(oldRecObj)))
    {    //version code is newer or same and message is different
      if(newRecObj instanceof QWEQEventMsgRecord)
      {  //record is 'QWEQEventMsgRecord' type; transfer table of products:
        ((QWEQEventMsgRecord)newRecObj).addToProductRecTable(
                                       oldRecObj.getProductRecTable(),true);
      }
                   //transfer alarm-triggered flag:
      newRecObj.setAlarmTriggeredFlag(oldRecObj.getAlarmTriggeredFlag());
                   //transfer display object:
      newRecObj.setDisplayObject(oldRecObj.getDisplayObject());
                   //transfer utility object:
      newRecObj.setUtilityObject(oldRecObj.getUtilityObject());
                   //set flag to indicate that this is an update:
      newRecObj.setUpdateFlag(true);
                   //set handle to record being replaced in table:
      newRecObj.setSupersededRecordObj(oldRecObj);
                   //update entry in table, sorted by time:
      listObj.addEventToList(newRecObj);
      return true;
    }
    return false;
  }

  /**
   * Adds or updates an event-message record to the filtered-events table.
   * @param recObj the event-message record to add.
   */
  private void addOrUpdateToFilteredEventTable(QWEventMsgRecord recObj)
  {
    final Object obj;
    if((obj=filteredEventRecordTable.get(
                       recObj.getEventIDKey())) instanceof QWEventMsgRecord)
    {    //record with same key already in table
              //update event in table (if version code is newer):
      if(updateEventInTable(filteredEventRecordTable,
                                              (QWEventMsgRecord)obj,recObj))
      {
//        filteredEventTableChangedFlag = true;    //indicate change
      }
    }
    else
    {       //record with same key not already in table; add rec
      filteredEventRecordTable.addEventToList(recObj);
//      filteredEventTableChangedFlag = true;      //indicate change
    }
  }
  
  	/**
	 * Determines if the event is low priority.
	 * 
	 * @param recObj
	 *            the event.
	 * @return true if the event is low priority, otherwise false.
	 */
  private boolean lowpriority(QWEventMsgRecord recObj)
  {
	  boolean lowPriority = false;
	  if (recObj instanceof QWEQEventMsgRecord)
	  {
		  QWEQEventMsgRecord event = (QWEQEventMsgRecord) recObj;
		  if (event.isLowPriority())
		  {
			  lowPriority = true;
		  }
	  }
	  return lowPriority;
  }
  
  	/**
	 * Gets the duplicate of the specified event.
	 * 
	 * @param recObj
	 *            the event.
	 * @return the duplicate event or null if none.
	 */
  private QWEventMsgRecord getDuplicate(QWEventMsgRecord recObj)
  {
	  String s;
	  QWEventMsgRecord dupRecObj = null;
	  if (recObj instanceof QWEQEventMsgRecord &&
			  (s = ((QWEQEventMsgRecord) recObj).getAssociateString()) != null)
	  {
		  synchronized (eventListManagerObj.getEventsListSyncObj()) {
			  dupRecObj = eventListManagerObj.getRecordObjForKeyStr(s);
		  }
		  logObj.info("getDuplicate (" + recObj.getEventIDKey() + ") = " +
				  s + " (" + dupRecObj + ")");
	  }
	  return dupRecObj;
  }

  /**
   * Attempts to add the given 'QWEventMsgRecord' object to the table.
   * The thread-synchronization lock for the events-list table should be
   * held by the thread calling this method.
   * @param recObj 'QWEventMsgRecord' object to be added.
   * @return true if the record object was added.
   */
  private boolean addEvent(QWEventMsgRecord recObj)
  {
    if(recObj instanceof QWEQEventMsgRecord)
    {    //given record is 'QWEQEventMsgRecord' type
      final QWEQEventMsgRecord eqEvtRecObj = (QWEQEventMsgRecord)recObj;
         //check if the event has a duplicate:
      final QWEventMsgRecord dupRecObj = getDuplicate(eqEvtRecObj);
      if(dupRecObj != null)
      {
        logObj.debug("Event " + eqEvtRecObj.getEventIDKey()+
             " is a PDL-duplicate of event " + dupRecObj.getEventIDKey());
        // if new event is low priority
        if(lowpriority(eqEvtRecObj))
        {
          logObj.debug("New event (" + eqEvtRecObj.getEventIDKey() +
                                 ") is low priority, not adding the event");
                   //if new event has products then transfer them
                   // to existing higher-priority event:
          final int prodCount;
          if((prodCount=eqEvtRecObj.getProductCount()) > 0)
          {  //event object contains products
            if(dupRecObj instanceof QWEQEventMsgRecord)
            {  //record is 'QWEQEventMsgRecord' type; copy products to
               // new event record object (keep any current products):
              ((QWEQEventMsgRecord)dupRecObj).addToProductRecTable(
                                     eqEvtRecObj.getProductRecTable(),true);
              logObj.debug2("Copied " + prodCount + " product" +
                                                   ((prodCount!=1)?"s":"") +
                                         " from new low-priority event \"" +
                                               eqEvtRecObj.getEventIDKey() +
                                                 "\" to existing event \"" +
                                                 dupRecObj.getEventIDKey() + "\"");
              if (processUpdatesFlag)
              {
                logObj.debug("Processing updated existing event " +
                    recObj.getEventIDKey());
                fireNewEventMsgRecord(recObj);
              }
            }
          }
          return false;
        }
        logObj.debug("Existing event \"" + dupRecObj.getEventIDKey() +
                                  "\" is low priority, removing the event");
              //existing event is low priority; remove lower-priority event
        removeLowPriorityEvent(eqEvtRecObj,dupRecObj);
      }
         //check if event with same ID in filtered table; remove if so:
      Object obj;
      if((obj=filteredEventRecordTable.remove(
                eqEvtRecObj.getEventIDKey())) instanceof QWEQEventMsgRecord)
      {  //event record existed in and removed from filtered event table
//        filteredEventTableChangedFlag = true;    //indicate change
              //transfer any products held by filtered version of event:
        eqEvtRecObj.addToProductRecTable(
                       ((QWEQEventMsgRecord)obj).getProductRecTable(),true);
      }
    }
              //add new event to main table:
    eventListManagerObj.addEventToList(recObj);
    return true;
  }

  /**
   * Removes the given lower-priority event.
   * @param recObj the higher-priority event.
   * @param dupRecObj the lower-priority event.
   */
  private void removeLowPriorityEvent(QWEQEventMsgRecord recObj,
		  QWEventMsgRecord dupRecObj)
  {
          //copy products table from existing event to new one:
    if(dupRecObj != null)
    {
      final int prodCount;
      if((prodCount=dupRecObj.getProductCount()) > 0)
      {   //event object contains products
        if(recObj instanceof QWEQEventMsgRecord)
        {     //record is 'QWEQEventMsgRecord' type; copy products to
              // new event record object (keep any current products):
          ((QWEQEventMsgRecord)recObj).addToProductRecTable(
                                       dupRecObj.getProductRecTable(),true);
        }
        logObj.debug("Copied " + prodCount + " product" +
                    ((prodCount!=1)?"s":"") + " from PDL-removed event \"" +
                          dupRecObj.getEventIDKey() + "\" to new event \"" +
                                             recObj.getEventIDKey() + "\"");
      }
      addOrUpdateToFilteredEventTable(dupRecObj);
      logObj.debug2("Moved event \"" + dupRecObj.getEventIDKey() +
                                             "\" to filtered-events table");
                   //set handle to record being removed:
      recObj.setSupersededRecordObj(dupRecObj);
    }
          //remove lower-priority event from main table:
    eventListManagerObj.removeEventFromList(dupRecObj.getEventIDKey());
  }

  /**
   * Checks if there is an existing PDL-duplicate for the given event
   * and removes the lower-priority event if so.
   * The thread-synchronization lock for the events-list table should
   * be held by the thread calling this method.
   * @param recObj the given event object to use.
   */
  private void checkRemoveLowPriorityEvent(QWEQEventMsgRecord recObj)
  {
         //check if the event has a PDL-duplicate:
    final QWEventMsgRecord dupRecObj = getDuplicate(recObj);
    if(dupRecObj != null)
    {    //PDL-duplicate event founds
      logObj.debug("Event " + recObj.getEventIDKey()+
             " is a PDL-duplicate of event " + dupRecObj.getEventIDKey());
      if(lowpriority(dupRecObj))
      {  //existing event ('dupEventObj') is low priority; remove it
        logObj.debug("Existing event \"" + dupRecObj.getEventIDKey() +
                                  "\" is low priority, removing the event");
        removeLowPriorityEvent(recObj,dupRecObj);
      }
      else if (dupRecObj instanceof QWEQEventMsgRecord)
      {  //updated event ('recObj') is low priority; remove it
        logObj.debug("Updated event \"" + recObj.getEventIDKey() +
                                  "\" is low priority, removing the event");
        removeLowPriorityEvent((QWEQEventMsgRecord) dupRecObj,recObj);
      }
    }
  }

  /**
   * Checks to see if a higher-priority duplicate event-message
   * record exists.  The QDM merge is used to check the records.
   * @param recObj 'QWEventMsgRecord' object to check.  The
   * thread-synchronization lock for the events-list table should
   * be held by the thread calling this method.
   * @return The higher-priority duplicate event-message record, or
   * null if none was found.
   */
  private QWEventMsgRecord getHigherPriorityEvent(QWEQEventMsgRecord recObj)
  {
         //check if the event has a duplicate:
    final QWEventMsgRecord dupRecObj = getDuplicate(recObj);
    if(dupRecObj != null && lowpriority(recObj))
    {    //higher-priority duplicate 'QWEventMsgRecord' found
      return dupRecObj;      //return record
    }
    return null;
  }

  /**
   * Checks if there is a PDL-matching event in the filtered-events table
   * and, if so, removes it from the table and returns it.
   * @param recObj the given event object to use.
   * @return The PDL-matching event in the filtered-events table,
   * or null if none found.
   */
  private QWEventMsgRecord pullFilteredDupEvent(QWEQEventMsgRecord recObj)
  {
    synchronized(filteredEventRecordTable)
    {    //hold thread-sync lock for filtered table while searching it
      final QWEventMsgRecord dupRecObj = getDuplicate(recObj);
      if(dupRecObj != null)
      {  //PDL-matching record was found
        if(filterPropertiesObj == null ||
                                filterPropertiesObj.passesFilter(dupRecObj))
        {     //no filter or event passes filter; remove from filtered table
          filteredEventRecordTable.remove(dupRecObj.getEventIDKey());
          return dupRecObj;
        }
        logObj.debug2("PDL-duplicate (" + dupRecObj +
                           ") of deleted event (" + recObj.getEventIDKey() +
                            ") not pulled because it does not pass filter");
      }
    }
    return null;
  }

  /**
   * Adds the given product message to the product-message cache (if
   * a higher-version message of the same product type is not already
   * in the cache).
   * @param newProductRecObj the product-message-record object to be added.
   * @param logMsgLevelVal the log-message-level value to use for the
   * log message generated when the message is added.
   */
  private void addMsgToProductCache(QWProductMsgRecord newProductRecObj,
                                                         int logMsgLevelVal)
  {
    if(!useProductCacheFlag)      //if product cache not in use then
      return;                     //abort method
    boolean addMsgFlag = true;         //initialize add-to-cache flag
    final Iterator iterObj;
    if(newProductRecObj.type != null &&
                                    (iterObj=productCacheTableObj.getValues(
                               newProductRecObj.getLCEventIDKey())) != null)
    {    //product type OK and entries for event key in cache
      Object obj;
      QWProductMsgRecord prodRecObj;
      while(iterObj.hasNext())
      {  //for each cached product message for event key
        if((obj=iterObj.next()) instanceof QWProductMsgRecord &&
                                         newProductRecObj.isSameProductType(
                                        prodRecObj=(QWProductMsgRecord)obj))
        {     //matching product message of same type in cache
          final int result = compVersionStrs(
                               newProductRecObj.version,prodRecObj.version);
          if(result > 0 || (result == 0 &&
                                      !newProductRecObj.equals(prodRecObj)))
          {   //version code of new product is greater than current
              // or they are the same & the products are different
                        //remove existing product message from cache:
            productCacheTableObj.removeEntry(
                                   prodRecObj.getLCEventIDKey(),prodRecObj);
            logObj.debug2("Removed duplicate \"" + newProductRecObj.type +
                                            "\" product from cache (ID=\"" +
                                               newProductRecObj.eventIDKey +
                                  "\", delProdVer=\"" + prodRecObj.version +
                                                       "\", msgProdVer=\"" +
                                          newProductRecObj.version + "\")");
          }
          else
          {   //version code not new enough
            addMsgFlag = false;        //clear add-to-cache flag
            logObj.debug2("Ignoring new \"" + newProductRecObj.type +
                         "\" product message, cache version higher (ID=\"" +
                         newProductRecObj.eventIDKey + "\", curProdVer=\"" +
                               prodRecObj.version + "\", newMsgProdVer=\"" +
                                          newProductRecObj.version + "\")");
          }
          break;           //exit loop
        }
      }
    }
    if(addMsgFlag)
    {    //product message should be added to cache
      if(productCacheTableObj.addEntry(newProductRecObj.getLCEventIDKey(),
                                                          newProductRecObj))
      {  //product message added to cache OK
        logObj.println(logMsgLevelVal,"Caching \"" + newProductRecObj.type +
                                    "\" product message with no matching " +
                                            "event record in table (ID=\"" +
                         newProductRecObj.eventIDKey + "\", productVer=\"" +
                                          newProductRecObj.version + "\")");
      }
      else
      {  //product message is duplicate, not added to cache
        logObj.println(logMsgLevelVal,"Duplicate \"" +
                                                     newProductRecObj.type +
                                        "\" product message not added to " +
                                            "product-message cache (ID=\"" +
                         newProductRecObj.eventIDKey + "\", productVer=\"" +
                                          newProductRecObj.version + "\")");
      }
    }
  }

  /**
   * Adds the given table of product messages to the product-message
   * cache (if, for each message, a higher-version message of the same
   * product type is not already in the cache).
   * @param msgsTableObj the table of product-message-record objects
   * to be added, or null for none.
   * @param logMsgLevelVal the log-message-level value to use for the
   * log message generated when the messages are added.
   */
  private void addMsgsToProductCache(FifoHashtable msgsTableObj,
                                                         int logMsgLevelVal)
  {
    if(!useProductCacheFlag)      //if product cache not in use then
      return;                     //abort method
    if(msgsTableObj != null)
    {    //table given
      final Iterator iterObj = msgsTableObj.values().iterator();
      Object obj;
      while(iterObj.hasNext())
      {    //for each product message in list
        if((obj=iterObj.next()) instanceof QWProductMsgRecord)
          addMsgToProductCache((QWProductMsgRecord)obj,logMsgLevelVal);
      }
    }
  }

  /**
   * Checks to see if a product message matching the given
   * delete-product message exists in the product-message cache
   * and removes it (if the version number is high enough).
   * @param delProductRecObj the delete-product-message-record
   * object specifying the product-message to be removed.
   * @return true if a matching product message was removed from
   * the product-message cache; false if not.
   */
  private boolean chkRemoveProductCache(
                                     QWDelProductMsgRecord delProductRecObj)
  {
    if(!useProductCacheFlag)      //if product cache not in use then
      return false;               //abort method
    boolean retFlag = false;
    final Iterator iterObj;
    Object obj;
    QWProductMsgRecord prodRecObj;
    if(delProductRecObj.type != null &&
                                    (iterObj=productCacheTableObj.getValues(
                               delProductRecObj.getLCEventIDKey())) != null)
    {    //del-product type OK and entries for event key in cache
      while(iterObj.hasNext())
      {       //for each cached product message for event key
        if((obj=iterObj.next()) instanceof QWProductMsgRecord &&
                                         delProductRecObj.isSameProductType(
                                        prodRecObj=(QWProductMsgRecord)obj))
        {     //matching product message of same type in cache
          if(compVersionStrs(delProductRecObj.version,
                                               prodRecObj.version,1,1) >= 0)
          {   //del-msg version is empty or >= cache version
            productCacheTableObj.removeEntry(
                                   prodRecObj.getLCEventIDKey(),prodRecObj);
            logObj.debug2("Removed \"" + delProductRecObj.type +
                                            "\" product from cache (ID=\"" +
                                               delProductRecObj.eventIDKey +
                                  "\", delProdVer=\"" + prodRecObj.version +
                                                       "\", msgProdVer=\"" +
                                          delProductRecObj.version + "\")");
          }
          else
          {   //version code of new product is < current
            logObj.debug2("Ignoring delete \"" + delProductRecObj.type +
                         "\" product message, cache version higher (ID=\"" +
                         delProductRecObj.eventIDKey + "\", curProdVer=\"" +
                               prodRecObj.version + "\", delMsgProdVer=\"" +
                                          delProductRecObj.version + "\")");
          }
          retFlag = true;         //indicate matching msg was in cache
        }
      }
    }
    return retFlag;
  }

  /**
   * Checks to see if any product messages for the given event message
   * exist in the product-message cache and fetches and adds them
   * to the event message if so.
   * @param newEventRecObj the event-message-record object to use.
   */
  private void chkFetchFromProductCache(QWEventMsgRecord newEventRecObj)
  {
    if(!useProductCacheFlag)      //if product cache not in use then
      return;                     //abort method
    final Iterator iterObj;
    if(newEventRecObj instanceof QWEQEventMsgRecord &&
                                    (iterObj=productCacheTableObj.getValues(
                                 newEventRecObj.getLCEventIDKey())) != null)
    {    //given record is 'QWEQEventMsgRecord' type and matching
         // product-message entries found for event
      Object obj;
      QWProductMsgRecord prodRecObj;
      while(iterObj.hasNext())
      {  //for each product-message entry
        if((obj=iterObj.next()) instanceof QWProductMsgRecord)
        {     //product-message-record object fetched OK
          prodRecObj = (QWProductMsgRecord)obj;
          productCacheTableObj.removeEntry(      //remove product from cache
                                   prodRecObj.getLCEventIDKey(),prodRecObj);
                   //add product to event record:
          ((QWEQEventMsgRecord)newEventRecObj).addProductMsgRecord(
                                                                prodRecObj);
                   //check if product type code string is known:
          if(productTypeStrsVec.indexOf(prodRecObj.type) < 0 &&
                                                  prodRecObj.type != null &&
                               !prodRecObj.type.startsWith(MsgTag.LINK_URL))
          {   //product-type string not known and not "LinkURL()"
            logObj.debug2("Product with unexpected type code (\"" +
                                                           prodRecObj.type +
                             "\") added from cache to event record (ID=\"" +
                                                     prodRecObj.eventIDKey +
                          "\", productVer=\"" + prodRecObj.version + "\")");
          }
          else
          {   //product-type string OK
            logObj.debug2("Added \"" + prodRecObj.type +
                            "\" product from cache to event record (ID=\"" +
                               prodRecObj.eventIDKey + "\", productVer=\"" +
                                                prodRecObj.version + "\")");
          }
          fireNewProductMsgRecord(prodRecObj,newEventRecObj);
        }
      }
    }
  }

  /**
   * Determines if the message manager will process updates caused by filtered
   * lower-priority events.
   * @return true if processing updates, false otherwise.
   */
  public boolean isProcessUpdates()
  {
    return processUpdatesFlag;
  }

  /**
   * Sets if the message manager will process updates caused by filtered
   * lower-priority events.
   * @param b true to process updates, false otherwise.
   */
  public void setProcessUpdatesFlag(boolean b)
  {
    processUpdatesFlag = b;
  }

  /**
   * Sets the storage processor object to be used for storing messages
   * processed by this object.  Invalid messages will not be sent
   * to the storage processor.
   * @param storageProcObj the storage processor object to use.
   */
  public void setStorageProcessor(QWStorageProcessor storageProcObj)
  {
    this.storageProcObj = storageProcObj;
  }

  /**
   * Sets the storage processor object to be used for storing messages
   * processed by this object.
   * @param storeInvalidMsgsFlag true to have invalid messages sent
   * to the storage processor (if the message is well-formed XML);
   * false to have all invalid messages discarded (default).
   * @param storageProcObj the storage processor object to use.
   */
  public void setStorageProcessor(QWStorageProcessor storageProcObj,
                                               boolean storeInvalidMsgsFlag)
  {
    this.storageProcObj = storageProcObj;
    storeInvalidMessagesFlag = storeInvalidMsgsFlag;
  }

  /**
   * Returns the log-file object used by the message manager.
   * @return The 'LogFile' object in use.
   */
  public LogFile getLogObj()
  {
    return logObj;
  }

  /**
   * Updates time zone used for date/time values for all
   * event-message and "StationAmp" record objects.
   * @param zone time zone.
   */
  public static void updateTimeZones(TimeZone zone)
  {
    QWEQEventMsgRecord.updateDateTimeZones(zone);
    QWStationAmpMsgRecord.updateDateTimeZones(zone);
  }


  /**
   * Class FilteredEventRecordTable defines the table of filtered event-
   * message record.
   */
  private class FilteredEventRecordTable extends FifoHashtable
                                              implements EventAdderInterface
  {
    private final DataChgdListenerSupport dataChgdListenerSupObj =
                 new DataChgdListenerSupport(FilteredEventRecordTable.this);

    /**
     * Adds the given event message record object to the table,
     * putting it into sorted position.
     * @param recObj the event message record object to add.
     */
    public void addEventToList(QWEventMsgRecord recObj)
    {
      putSortByValue(recObj.getLCEventIDKey(),recObj,true);
      dataChgdListenerSupObj.fireListeners();    //fire change listeners
    }

    /**
     * Returns the value to which this hashtable maps the specified key.
     * Returns null if the hashtable contains no mapping for this
     * key.
     * @param keyObj key whose associated value is to be returned.
     * @return The value to which this hashtable maps the specified key.
     */
    public Object get(Object keyObj)
    {
                   //use lower-cased key string:
      return (keyObj instanceof String) ?
                                 super.get(((String)keyObj).toLowerCase()) :
                                                          super.get(keyObj);
    }

    /**
     * Removes the mapping for this key from the table (if present).
     * @param keyObj key object whose mapping is to be removed.
     * @return previous value associated with specified key, or null
     * if there was no mapping for key.
     */
    public Object remove(Object keyObj)
    {
                   //use lower-cased key string:
      final Object retObj = (keyObj instanceof String) ?
                              super.remove(((String)keyObj).toLowerCase()) :
                                                       super.remove(keyObj);
      dataChgdListenerSupObj.fireListeners();    //fire change listeners
      return retObj;
    }

    /**
     * Clears the table so that it contains no keys.
     */
    public synchronized void clear()
    {
      super.clear();
      dataChgdListenerSupObj.fireListeners();    //fire change listeners
    }

    /**
     * Registers the given 'DataChangedListener' object to be notified
     * when this table is changed.
     * @param listenerObj the 'DataChangedListener' object.
     */
    public void addDataChangedListener(DataChangedListener listenerObj)
    {
      dataChgdListenerSupObj.addListener(listenerObj);
    }

    /**
     * Unregisters the given 'DataChangedListener' object from the list of
     * listeners for this table.
     * @param listenerObj the 'DataChangedListener' object.
     */
    public void removeDataChangedListener(DataChangedListener listenerObj)
    {
      dataChgdListenerSupObj.removeListener(listenerObj);
    }
  }

  /**
   * Creates a thread that calls 'performNextEventsCheck()' once every
   * 100 milliseconds.  This will filter-out and remove events as they
   * age.  The 'delOldProductCacheMsgs()' method is also called to
   * removed old messages from the product-message cache.
   */
  private class EventsCheckThread extends IstiNotifyThread
  {
    private int prodCacheCounter = 0;

    /**
     * Creates an events-check thread object.
     */
    public EventsCheckThread()
    {
      super("EventsCheckThread");
    }

    /**
     * Executing method for the thread.
     */
    public void run()
    {
      logObj.debug("QWMsgManager:  EventsCheckThread started");
      while(true)
      {
        waitForNotify(100);            //delay between checks
        if(isTerminated())        //if 'terminate()' called then
          break;                  //exit loop (and thread)
        performNextEventsCheck();      //do checking operations
        if(++prodCacheCounter > 10)
        {     //1 out of 10 times reached
          delOldProductCacheMsgs();    //delete old product-msg cache entries
          prodCacheCounter = 0;        //reset counter
        }
      }
      logObj.debug("QWMsgManager:  EventsCheckThread stopped");
    }
  }

  /**
   * Called when a new 'QWEventMsgRecord' object has been created in
   * response to an XML message.
   * @param evtRecObj event record object
   */
  private void fireNewEventMsgRecord(QWEventMsgRecord evtRecObj)
  {
    //if listener installed then send new event record:
    if(msgObjsListenerObj != null)
    {
      msgObjsListenerObj.newEventMsgRecord(evtRecObj);
    }

    final Object[] msgObjsListenerArray = msgObjsListenerListObj.toArray();
    for (int i = 0; i < msgObjsListenerArray.length; i++)
    {
      ((QWMsgObjsListener)msgObjsListenerArray[i]).newEventMsgRecord(
                                                                 evtRecObj);
    }
  }

  /**
   * Called when a new 'QWDelEventMsgRecord' object has been created in
   * response to an XML message.
   * @param delEventRecObj delete event record object
   * @param evtRecObj event record object
   */
  private void fireNewDelEventMsgRecord(
      QWDelEventMsgRecord delEventRecObj, QWEventMsgRecord evtRecObj)
  {
    if (msgObjsListenerObj != null)
    { //listener installed; send new delete-event record
      msgObjsListenerObj.newDelEventMsgRecord(delEventRecObj,evtRecObj);
    }

    final Object[] msgObjsListenerArray = msgObjsListenerListObj.toArray();
    for (int i = 0; i < msgObjsListenerArray.length; i++)
    {
      ((QWMsgObjsListener)msgObjsListenerArray[i]).newDelEventMsgRecord(
          delEventRecObj,evtRecObj);
    }
  }

  /**
   * Called when a new 'QWProductMsgRecord' object has been created in
   * response to an XML message.
   * @param prodRecObj product record object
   * @param evtRecObj event record object
   */
  private void fireNewProductMsgRecord(
      QWProductMsgRecord prodRecObj, QWEventMsgRecord evtRecObj)
  {
    if(msgObjsListenerObj != null)
    {    //listener installed; send new product record
      msgObjsListenerObj.newProductMsgRecord(prodRecObj,evtRecObj);
    }

    final Object [] msgObjsListenerArray = msgObjsListenerListObj.toArray();
    for (int i = 0; i < msgObjsListenerArray.length; i++)
    {
      ((QWMsgObjsListener)msgObjsListenerArray[i]).newProductMsgRecord(
          prodRecObj,evtRecObj);
    }
  }

  /**
   * Called when a new 'QWDelProductMsgRecord' object has been created in
   * response to an XML message.
   * @param delProductRecObj delete product record object
   * @param evtRecObj event record object
   */
  private void fireNewDelProductMsgRecord(
      QWDelProductMsgRecord delProductRecObj, QWEventMsgRecord evtRecObj)
  {
    if (msgObjsListenerObj != null)
    { //listener installed; send new delete record
      msgObjsListenerObj.newDelProductMsgRecord(delProductRecObj,evtRecObj);
    }

    final Object[] msgObjsListenerArray = msgObjsListenerListObj.toArray();
    for (int i = 0; i < msgObjsListenerArray.length; i++)
    {
      ((QWMsgObjsListener)msgObjsListenerArray[i]).newDelProductMsgRecord(
          delProductRecObj,evtRecObj);
    }
  }
}
