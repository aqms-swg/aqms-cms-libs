//QWMsgNumTimeRec.java:  Defines methods for fetching message-number and
//                       time-generated values.
//
//  12/1/2003 -- [ET]
//

package com.isti.quakewatch.message;


/**
 * Interface QWMsgNumTimeRec defines methods for fetching message-number
 * and time-generated values.
 */
public interface QWMsgNumTimeRec
{
  /**
   * Returns the message number value.
   * @return The message number value.
   */
  long getMsgNum();

    /**
     * Returns the time-generated value.
     * @return The time-generated value, in milliseconds since 1/1/1970.
     */
  long getTimeGenerated();
}
