//QWMsgObjsListener.java:  Defines a call-back listener interface used
//                         with event-message record objects.
//
//  9/19/2002 -- [ET]
//  6/24/2003 -- [KF]  Add 'evtRecObj' to 'newProductMsgRecord' and
//                     'newDelProductMsgRecord' methods.
//  3/18/2004 -- [ET]  Moved from 'QWClient' to 'QWCommon' project.
//   5/5/2004 -- [KF]  Add 'evtRecObj' to 'newDelEventMsgRecord' method.
//

package com.isti.quakewatch.message;

/**
 * QWMsgObjsListener defines a call-back listener interface used with
 * event-message record objects.
 */
public interface QWMsgObjsListener
{
  /**
   * Called when a new 'QWEventMsgRecord' object has been created in
   * response to an XML message.
   * @param evtRecObj event record object
   */
  public void newEventMsgRecord(QWEventMsgRecord evtRecObj);

  /**
   * Called when a new 'QWDelEventMsgRecord' object has been created in
   * response to an XML message.
   * @param delEventRecObj delete event record object
   * @param evtRecObj event record object
   */
  public void newDelEventMsgRecord(
      QWDelEventMsgRecord delEventRecObj, QWEventMsgRecord evtRecObj);

  /**
   * Called when a new 'QWProductMsgRecord' object has been created in
   * response to an XML message.
   * @param prodRecObj product record object
   * @param evtRecObj event record object
   */
  public void newProductMsgRecord(
      QWProductMsgRecord prodRecObj, QWEventMsgRecord evtRecObj);

  /**
   * Called when a new 'QWDelProductMsgRecord' object has been created in
   * response to an XML message.
   * @param delProductRecObj delete product record object
   * @param evtRecObj event record object
   */
  public void newDelProductMsgRecord(
      QWDelProductMsgRecord delProductRecObj, QWEventMsgRecord evtRecObj);
}
