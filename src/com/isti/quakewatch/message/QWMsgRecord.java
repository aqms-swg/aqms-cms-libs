//QWMsgRecord.java:  A record of data from an XML QWmessage.
//
//  9/20/2002 -- [ET]  Initial version.
//  6/19/2003 -- [ET]  Changed 'msgNumber' type from 'int' to 'long.
//  7/15/2003 -- [ET]  Added implementation for 'Archivable' interface.
//  10/1/2003 -- [ET]  Made member variables 'final'.
// 10/28/2003 -- [ET]  Moved from 'QWClient' to 'QWCommon' project;
//                     modified to implement 'QWMsgNumTimeRec'.
// 11/26/2003 -- [ET]  Modified 'elemToStr()' method to normalize
//                     returned string.
//  5/31/2005 -- [ET]  Added 'origTimeGenerated' instance variable.
//  12/5/2005 -- [ET]  Added 'getServerIDName()', 'getServerRevStr()' and
//                     'getServerAddress()' methods.
//  8/29/2007 -- [ET]  Modified to use IstiXmlUtils 'elementToFixedString()'
//                     method (which processes incoming message-elements so
//                     that whitespace between elements is removed and
//                     control characters within elements are encoded to
//                     "&##;" strings).
//

package com.isti.quakewatch.message;

import java.util.Date;
import org.jdom.Element;
import com.isti.util.IstiXmlUtils;
import com.isti.util.Archivable;
import com.isti.quakewatch.common.MsgTag;

/**
 * QWMsgRecord defines a record of data from an XML QWmessage.
 */
public class QWMsgRecord extends QWRecord
                                       implements Archivable,QWMsgNumTimeRec
{
    /**
     * Handle to the QWmessage 'Element' object used to construct this
     * object.
     */
  public final Element qwMsgElement;
    /** Message number from the 'QWmessage' element for this message. */
  public final long msgNumber;
    /** Time that message was created by server (or null if not given). */
  public final Date timeGenerated;
    /** Original time generated if changed (or null if not given). */
  public final Date origTimeGenerated;
    /** Archive-string form of record data. */
  protected String archiveFormStr = null;

  /**
   * Creates a record of data from an XML QWmessage.
   * @param qwMsgElement the XML "QWmessage" element object.
   * @throws QWRecordException if an error occurs while creating
   * the data record.
   */
  public QWMsgRecord(Element qwMsgElement) throws QWRecordException
  {
    super(qwMsgElement);                   //construct parent
    this.qwMsgElement = qwMsgElement;      //save handle to element
    checkQWMsgRecordName(qwMsgElement);    //check element name
         //fetch and save message number:
    msgNumber = getAttribLong(MsgTag.MSG_NUMBER);
         //fetch and save time of message creation at server:
    timeGenerated = getOptAttribTime(MsgTag.TIME_GENERATED);
         //fetch and save original time-generated time:
    origTimeGenerated = getOptAttribTime(MsgTag.ORIG_TIME_GENERATED);
  }

  /**
   * Creates a record of data from the archived-string data for an XML
   * QWmessage.  This constructor is needed to have this class implement
   * the 'Archivable' interface.
   * @param dataStr the archived-string data to use.
   * @param mkrObj Archivable.Marker parameter to indicate that this
   * constructor is used to dearchive a record.
   * @throws QWRecordException if an error occurs while creating
   * the data record.
   */
  public QWMsgRecord(String dataStr,Archivable.Marker mkrObj)
                                                    throws QWRecordException
  {
    super(dataStr,mkrObj);                 //construct parent
    qwMsgElement = currentElement;         //save handle to element
    checkQWMsgRecordName(qwMsgElement);    //check element name
         //fetch and save message number:
    msgNumber = getAttribLong(MsgTag.MSG_NUMBER);
         //fetch and save time of message creation at server:
    timeGenerated = getOptAttribTime(MsgTag.TIME_GENERATED);
         //fetch and save original time-generated time:
    origTimeGenerated = getOptAttribTime(MsgTag.ORIG_TIME_GENERATED);
  }

  /**
   * Checks if the root element of the given message element object is
   * named "QWmessage".
   * @param qwMsgElement message element object to check.
   * @throws QWRecordException if the root element of the given message
   * element is not named "QWmessage".
   */
  protected final void checkQWMsgRecordName(Element qwMsgElement)
                                                   throws QWRecordException
  {
    if(!MsgTag.QW_MESSAGE.equalsIgnoreCase(qwMsgElement.getName()))
    {    //root element name not "QWmessage"
      throw new QWRecordException("Element tag name \"" +
                                        MsgTag.QW_MESSAGE + "\" not found");
    }
  }

  /**
   * Creates a record of data from existing data items.
   * @param qwMsgElement the XML "QWmessage" element object.
   * @param msgNumber message number from the 'QWmessage' element for
   * this message.
   * @param timeGenerated time that message was created by server (or
   * null if not given).
   * @throws QWRecordException if an error occurs while creating
   * the data record.
   */
  public QWMsgRecord(Element qwMsgElement,long msgNumber,Date timeGenerated)
                                                    throws QWRecordException
  {
    super(qwMsgElement);                   //construct parent
    this.qwMsgElement = qwMsgElement;      //save handle to element
    this.msgNumber = msgNumber;
    this.timeGenerated = timeGenerated;
    origTimeGenerated = null;
  }

  /**
   * Indicates whether some other object is "equal to" this one.
   *
   * @param   obj   the reference object with which to compare.
   * @return  <code>true</code> if this object is the same as the obj
   *          argument; <code>false</code> otherwise.
   */
  public boolean equals(QWMsgRecord obj)
  {
    return super.equals(obj);
        //ignore message number and time generated
        //(msgNumber == obj.msgNumber) &&
        //(timeGenerated == null ? obj.timeGenerated == null : timeGenerated.equals(obj.timeGenerated));
  }

  /**
   * Indicates whether some other object is "equal to" this one.
   *
   * @param   obj   the reference object with which to compare.
   * @return  <code>true</code> if this object is the same as the obj
   *          argument; <code>false</code> otherwise.
   */
  public boolean equals(Object obj)
  {
    return obj instanceof QWMsgRecord && equals((QWMsgRecord)obj);
  }

  /**
   * Returns the date that this object should be archived under.  This
   * method is needed to have this class implement the 'Archivable'
   * interface.
   * @return the date we should be archived under
   */
  public Date getArchiveDate()
  {
    return timeGenerated;
  }

  /**
   * Returns the archive-data representation of this object.  This
   * method is needed to have this class implement the 'Archivable'
   * interface.
   * @return A string containing the archive-data representation of
   * this object.
   * @throws QWRecordException if an error occurs while converting
   * the XML data to string form.
   */
  public String toArchivedForm()
  {
         //if archive form not yet created then create it now:
    if(archiveFormStr == null)
      archiveFormStr = elemToStr(qwMsgElement);
    return archiveFormStr;        //return archive form
  }

  /**
   * Returns the message number from the 'QWmessage' element for this
   * message.  This method is needed to have this class implement the
   * 'QWMsgNumTimeRec' interface.
   * @return The message number from the 'QWmessage' element for this
   * message.
   */
  public long getMsgNum()
  {
    return msgNumber;
  }

  /**
   * Returns the time-generated value for this record.  This method is
   * needed to have this class implement the 'QWMsgNumTimeRec' interface.
   * @return The long-integer epoch time-generated values, in milliseconds
   * since 1/1/1970.
   */
  public long getTimeGenerated()
  {
    return timeGenerated.getTime();
  }

  /**
   * Returns the contents of the Server-ID Name attribute for this
   * message record.
   * @return The contents of the 'ServerIDName' attribute for this
   * message record, or null if not available.
   */
  public String getServerIDName()
  {
    return qwMsgElement.getAttributeValue(MsgTag.SERVER_ID_NAME);
  }

  /**
   * Returns the contents of the Server Revision String attribute for this
   * message record.
   * @return The contents of the 'ServerRevStr' attribute for this
   * message record, or null if not available.
   */
  public String getServerRevStr()
  {
    return qwMsgElement.getAttributeValue(MsgTag.SERVER_REV_STR);
  }

  /**
   * Returns the contents of the Server Address attribute for this
   * message record.
   * @return The contents of the 'ServerAddress' attribute for this
   * message record, or null if not available.
   */
  public String getServerAddress()
  {
    return qwMsgElement.getAttributeValue(MsgTag.SERVER_ADDRESS);
  }

  /**
   * Converts an Element object to a string.
   * @param elementObj the Element object to use.
   * @return A new converted string.
   * @throws QWRecordException if an error occurred.
   */
  public static String elemToStr(Element elementObj)
  {
    try
    {              //convert Element object to a string and return it:
      return IstiXmlUtils.elementToFixedString(elementObj);
    }
    catch(Exception ex)
    {              //if error then convert to 'QWRecordException':
      throw new QWRecordException(
                              "Error converting element to string:  " + ex);
    }
  }
}
