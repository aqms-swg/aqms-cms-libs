//QWMessageListener.java:  Defines a call-back listener interface used with
//                         a "QWMessage" XML text message.
//
//  9/19/2002 -- [ET]  Initial version.
//  3/18/2004 -- [ET]  Moved from 'QWClient' to 'QWCommon' project.
//

package com.isti.quakewatch.message;

/**
 * QWMessageListener defines a call-back listener interface used with a
 * "QWMessage" XML text message.
 */
public interface QWMsgStrListener
{
  /**
   * Called when a new "QWMessage" XML text message string has been
   * received.
   * @param msgStr message string
   */
  public void newMessageString(String msgStr);
}
