//QWProductMsgRecord.java:  Defines a record of data for one
//                          event-product.
//
//  9/18/2002 -- [ET]  Initial version.
// 10/24/2003 -- [ET]  Modified (via import) to reference 'QWCommon'
//                     version of  'QWRecordException' instead of
//                     local version.
//  3/18/2004 -- [ET]  Moved from 'QWClient' to 'QWCommon' project.
//  9/27/2006 -- [ET]  Added support for for ANSS-EQ-XML format; added
//                     'prodTypeCode' field; added 'isSameProductType()'
//                     and 'isMatchingProductType()' methods.
//  10/2/2006 -- [ET]  Modified 'msgTypeCode' to be "TX" for Comment
//                     messages and "LI" for ProductLink messages
//                     (ANSS-EQ-XML format).
//  3/31/2010 -- [ET]  Added support for QuakeML format.
//  6/19/2013 -- [ET]  Modified to explicit set product-type value
//                     to "ShakeMapURL" if product determined to be
//                     ShakeMap product.
//

package com.isti.quakewatch.message;

import org.jdom.Element;
import com.isti.quakewatch.common.MsgTag;

/**
 * QWProductMsgRecord defines a record of data for one event-product.
 */
public class QWProductMsgRecord extends QWIdentDataMsgRecord
{
    /** Handle to 'Element' object used to construct this object. */
  public final Element productElement;
    /** Message type string set by QWServer. */
  public final String type;
    /** Message type code string received from feeder module. */
  public final String msgTypeCode;
    /** Product type code string received from feeder module. */
  public final String prodTypeCode;
    /** Value string for product, or null if none. */
  public final String value;
    /** Comment string for product, or null if none. */
  public final String comment;
    /** Handle to 'QWEventMsgRecord' parent, or null if none. */
  public QWEventMsgRecord parentEventMsgRecord = null;

  /**
   * Creates a data record for one event product, built from an XML
   * product-message element.
   * @param qwMsgElement the XML "QWmessage" element object.
   * @param dataMsgElement the XML "DataMessage" element object.
   * @param eventElement the XML "Event" element object if ANSS-EQ-XML
   * message format, or null if other message format.
   * @param productElement the "Product" message element.
   * @param messageFormatSpec specifier for message-format type (one of
   * the 'QWIdentDataMsgRecord.MFMT_...' values).
   * @param commentElemFlag true if Comment element in use; false if
   * ProductLink element in use (or if QuakeWatch message).
   * @throws QWRecordException if an error occurs while creating
   * the data record.
   */
  public QWProductMsgRecord(Element qwMsgElement, Element dataMsgElement,
                               Element eventElement, Element productElement,
                             int messageFormatSpec, boolean commentElemFlag)
                                                    throws QWRecordException
  {
         //call 'QWIdentDataMsgRecord' constructor with 'Event' element
         // and 'Version' from 'ProductLink' element (if ANSS-EQ-XML),
         // or 'Product' element (if other format):
    super(qwMsgElement,dataMsgElement,
                                    ((messageFormatSpec == MFMT_ANSSEQXML) ?
                                             eventElement : productElement),
                                                          messageFormatSpec,
       ((messageFormatSpec == MFMT_ANSSEQXML) ? productElement.getChildText(
                     MsgTag.VERSION,productElement.getNamespace()) : null));
    this.productElement = productElement;   //save handle to element

    String parsedTypeStr,parsedCodeStr;
    switch(messageFormatSpec)
    {  //process data depending on format specification
      case MFMT_QUAKEML:     //data is QuakeML message format
              //setup to get data-fields from "productLink" element:
        currentElement = productElement;
              //get "code" child-element for "productLink" element:
        parsedCodeStr = getNonNullOptElementStr(MsgTag.QUAKEML_CODE);
              //get "type" child-element of "productLink" element:
        parsedTypeStr = getOptElementStr(MsgTag.QUAKEML_TYPE);
        if(commentElemFlag)
        {  //comment element
          if(!MsgTag.QUAKEML_COMMENT.equalsIgnoreCase(
                                                  productElement.getName()))
          {  //element name not "comment"
            throw new QWRecordException("Element tag name \"" +
                                   MsgTag.QUAKEML_COMMENT + "\" not found");
          }
                   //if no 'type' string then put in "TextComment":
          if(parsedTypeStr == null || parsedTypeStr.trim().length() > 0)
            parsedTypeStr = MsgTag.TEXT_COMMENT;
                   //set message-code to "TX":
          msgTypeCode = MsgTag.TEXT_MSGSTR;
                   //set 'value' to 'text' child-element:
          value = getOptElementStr(MsgTag.QUAKEML_TEXT);
                   //set no value for 'comment':
          comment = null;
        }
        else
        {  //productLink element
          if(!MsgTag.QUAKEML_PRODUCT_LINK.equalsIgnoreCase(
                                                  productElement.getName()))
          {  //element name not "productLink"
            throw new QWRecordException("Element tag name \"" +
                              MsgTag.QUAKEML_PRODUCT_LINK + "\" not found");
          }
                   //set message-code to "LI":
          msgTypeCode = MsgTag.LINK_MSGSTR;
                   //set 'value' to Link child-element:
          value = getOptElementStr(MsgTag.QUAKEML_LINK);
                   //set 'comment' to 'comment'|'text' child-element:
          comment = getChildElementStr(
                                MsgTag.QUAKEML_COMMENT,MsgTag.QUAKEML_TEXT);
        }
        break;

      case MFMT_ANSSEQXML:   //data is ANSS-EQ-XML message format
              //setup to get data-fields from "ProductLink" element:
        currentElement = productElement;
              //get "Code" child-element for "ProductLink" element:
        parsedCodeStr = getNonNullOptElementStr(MsgTag.CODE);
              //get "TypeKey" child-element of "ProductLink" element:
        parsedTypeStr = getOptElementStr(MsgTag.TYPE_KEY);
        if(commentElemFlag)
        {  //Comment element
          if(!MsgTag.COMMENT.equalsIgnoreCase(productElement.getName()))
          {  //element name not "Comment"
            throw new QWRecordException("Element tag name \"" +
                                           MsgTag.COMMENT + "\" not found");
          }
                   //set message-code to "TX":
          msgTypeCode = MsgTag.TEXT_MSGSTR;
                   //set 'value' to Text child-element:
          value = getOptElementStr(MsgTag.TEXT);
                   //set 'comment' to Link child-element:
          comment = getOptElementStr(MsgTag.LINK);
        }
        else
        {  //ProductLink element
          if(!MsgTag.PRODUCT_LINK.equalsIgnoreCase(productElement.getName()))
          {  //element name not "ProductLink"
            throw new QWRecordException("Element tag name \"" +
                                      MsgTag.PRODUCT_LINK + "\" not found");
          }
                   //set message-code to "LI":
          msgTypeCode = MsgTag.LINK_MSGSTR;
                   //set 'value' to Link child-element:
          value = getOptElementStr(MsgTag.LINK);
                   //set 'comment' to Note child-element:
          comment = getOptElementStr(MsgTag.NOTE);
        }
        break;

      default:               //data is QuakeWatch message format
        if(!MsgTag.PRODUCT.equalsIgnoreCase(productElement.getName()))
        {  //element name not "Product"
          throw new QWRecordException("Element tag name \"" +
                                           MsgTag.PRODUCT + "\" not found");
        }
              //get "Type" attribute:
        parsedTypeStr = getAttribStr(MsgTag.TYPE);
              //get optional "MsgTypeCode" attribute:
        msgTypeCode = getNonNullOptAttribStr(MsgTag.MSG_TYPE_CODE);
              //get optional "parsedCodeStr" attribute:
        parsedCodeStr = getNonNullOptAttribStr(MsgTag.PROD_TYPE_CODE);
              //get Value attribute for Product element:
        value = getOptAttribStr(MsgTag.VALUE);
              //get Comment attribute for Product element:
        comment = getOptAttribStr(MsgTag.COMMENT);
    }
    if(!MsgTag.SHAKEMAP_URL.equals(parsedTypeStr))
    {  //message type is not "ShakeMapURL"
              //if determined to be ShakeMap product then
              // set explicit product-type value:
      if(isShakeMapProduct(parsedTypeStr,parsedCodeStr,value))
        parsedTypeStr = MsgTag.SHAKEMAP_URL;
      else if(MsgTag.LINK_URL.equals(parsedTypeStr) &&
                                          parsedCodeStr.trim().length() > 0)
      {  //message type is "LinkURL" and "Code" value exists
        parsedTypeStr += '(' + parsedCodeStr + ')';   //append "Code" value
      }
    }
    type = parsedTypeStr;              //set message type
    prodTypeCode = parsedCodeStr;      //set message product code
  }

  /**
   * Creates a data record for one event product, built from an XML
   * product-message element.
   * @param qwMsgElement the XML "QWmessage" element object.
   * @param dataMsgElement the XML "DataMessage" element object.
   * @param eventElement the XML "Event" element object if ANSS-EQ-XML
   * message format, or null if other message format.
   * @param productElement the "Product" message element.
   * @param anssEQMsgFormatFlag true to interpret message element as
   * ANSS-EQ-XML format; false to interpret message element as "QuakeWatch"
   * format.
   * @param commentElemFlag true if Comment element in use in ANSS-EQ-XML
   * message; false if ProductLink element in use in ANSS-EQ-XML message
   * (or if QuakeWatch message).
   * @throws QWRecordException if an error occurs while creating
   * the data record.
   * @deprecated Use version with 'messageFormatSpec' parameter.
   */
  public QWProductMsgRecord(Element qwMsgElement, Element dataMsgElement,
                               Element eventElement, Element productElement,
                       boolean anssEQMsgFormatFlag, boolean commentElemFlag)
                                                    throws QWRecordException
  {
    this(qwMsgElement,dataMsgElement,eventElement,productElement,
                    (anssEQMsgFormatFlag ? MFMT_ANSSEQXML : MFMT_QWMESSAGE),
                                                           commentElemFlag);
  }

//  /**
//   * Creates a data record for one event product, built from a QuakeWatch
//   * format XML "Product" message element.
//   * @param qwMsgElement the XML "QWmessage" element object.
//   * @param dataMsgElement the XML "DataMessage" element object.
//   * @param productElement the XML "Product" message element.
//   * @throws QWRecordException if an error occurs while creating
//   * the data record.
//   */
//  public QWProductMsgRecord(Element qwMsgElement, Element dataMsgElement,
//                            Element productElement) throws QWRecordException
//  {
//    this(qwMsgElement,dataMsgElement,productElement,null,
//                                                      MFMT_QWMESSAGE,false);
//  }

  /**
   * Indicates whether some other object is "equal to" this one.
   * @param   obj   the reference object with which to compare.
   * @return  <code>true</code> if this object is the same as the obj
   *          argument; <code>false</code> otherwise.
   */
  public boolean equals(QWProductMsgRecord obj)
  {
    return super.equals(obj) &&
              ((type == null) ? obj.type == null : type.equals(obj.type)) &&
                          ((msgTypeCode == null) ? obj.msgTypeCode == null :
                                     msgTypeCode.equals(obj.msgTypeCode)) &&
          ((value == null) ? obj.value == null : value.equals(obj.value)) &&
                                  ((comment == null) ? obj.comment == null :
                                             comment.equals(obj.comment)) &&
        ((parentEventMsgRecord == null) ? obj.parentEventMsgRecord == null :
                     parentEventMsgRecord.equals(obj.parentEventMsgRecord));
  }

  /**
   * Indicates whether some other object is "equal to" this one.
   * @param   obj   the reference object with which to compare.
   * @return  <code>true</code> if this object is the same as the obj
   *          argument; <code>false</code> otherwise.
   */
  public boolean equals(Object obj)
  {
    return obj instanceof QWProductMsgRecord &&
                                            equals((QWProductMsgRecord)obj);
  }

  /**
   * Indicates if the product type of the given record is equal to the
   * product type for this record.  The 'prodTypeCode' values are also
   * compared.
   * @param recObj QWProductMsgRecord given record to compare.
   * @return true if the product type of the given record is equal to
   * the product type for this record; false if not.
   */
  public boolean isSameProductType(QWProductMsgRecord recObj)
  {
    if(recObj == null)       //if given record is null then
      return false;          //indicate no match
    if(type != null)
    {    //this record's type is not null
      if(recObj.type == null || type.equalsIgnoreCase(recObj.type))
        return false;   //if other type is null or not equal then indicate
    }
    else if(recObj.type != null)
      return false;     //if this type null and other not then indicate
         //record types match
    if(prodTypeCode != null)
    {    //this record's prodTypeCode is not null
      if(recObj.prodTypeCode == null ||
                         prodTypeCode.equalsIgnoreCase(recObj.prodTypeCode))
      {  //other prodTypeCode is null or not equal
        return false;
      }
    }
    else if(recObj.prodTypeCode != null)
      return false;     //if this prodTypeCode null and other not then ind
         //record prodTypeCodes match
    return true;
  }

  /**
   * Indicates if the given string appears in this product's 'type' or
   * 'prodTypeCode' fields.  A case-insensitive comparison is used.
   * @param typeStr given string to match.
   * @return true if the given string appears in this product's 'type'
   * or 'prodTypeCode' fields; false if not.
   */
  public boolean isMatchingProductType(String typeStr)
  {
    if(typeStr != null)
    {    //given string not null; convert to lower case
      typeStr = typeStr.toLowerCase();
      if((type != null && type.toLowerCase().indexOf(typeStr) >= 0) ||
                                                    (prodTypeCode != null &&
                          prodTypeCode.toLowerCase().indexOf(typeStr) >= 0))
      {  //given string matched
        return true;
      }
    }
    return false;
  }

  /**
   * Returns a string representation of this class.
   * @return String object.
   */
  public String toString()
  {
    return "eventIDKey=" + this.eventIDKey + ", type=" + this.type +
             ", msgTypeCode=" + this.msgTypeCode + ", value=" + this.value +
                                                ", comment=" + this.comment;
  }
}
