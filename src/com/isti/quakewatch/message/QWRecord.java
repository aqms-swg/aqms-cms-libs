//QWRecord.java:  Provides methods for extracting XML attribute values.
//
//  9/16/2002 -- [ET]  Initial version.
//  6/19/2003 -- [ET]  Added 'parseAttribLong()' and 'getAttribLong()'
//                     methods.
//  7/15/2003 -- [ET]  Added support for 'Archivable' interface.
// 10/24/2003 -- [ET]  Moved from 'QWClient' to 'QWCommon' project.
//  11/5/2003 -- [ET]  Modified to use QWUtil 'parseStringXmlDate()' method.
//  2/16/2004 -- [ET]  Modified to use local date-format object in a
//                     thread-safe manner.
//  5/16/2005 -- [ET]  Added 'getOptTextDouble()' method; fixed method
//                     'parseAttribTime()' to deal properly with 'null'
//                     parameter.
//  9/12/2006 -- [ET]  Added "getElement...()" methods and support.
//  3/29/2010 -- [ET]  Added methods 'getElementValueStr()',
//                     'getElementValueDouble()', 'getElementValueTime()'
//                     'getOptElementValueDouble()', 'getChildElementStr()',
//                     'getChildElementInteger()' and
//                     'getChildElementDouble()'.
//  5/12/2010 -- [ET]  Modified 'parseDataTime()' to make it handle
//                     date/time values without trailing 'Z'.
//  3/13/2019 -- [KF]  Modified to use the PDL Indexer.
//

package com.isti.quakewatch.message;

import java.util.Date;
import java.text.DateFormat;
import java.text.ParseException;
import org.jdom.Element;
import org.jdom.Namespace;
import com.isti.util.Archivable;
import com.isti.util.IstiXmlUtils;
import com.isti.quakewatch.common.MsgTag;
import com.isti.quakewatch.util.QWUtils;

/**
 * Class QWRecord provides methods for extracting XML attribute values.
 */
public abstract class QWRecord
{
    /** Handle to current message 'Element' object being processed. */
  protected Element currentElement;
              //date-formatter object for 'parseDataTime()' method:
  private static final DateFormat xmlDateFormatterObj =
                                           QWUtils.getXmlDateFormatterObj();
              //date-formatter object for 'parseDataTime()' method:
  private static final DateFormat xmlDateFormatter2Obj =
                                          QWUtils.getXmlDateFormatter2Obj();

  /**
   * Creates a record object and sets the current element to be used.
   * @param elementObj the XML element object to be used.
   * @throws QWRecordException if the given object is null.
   */
  public QWRecord(Element elementObj) throws QWRecordException
  {
    if(elementObj == null)
      throw new QWRecordException("Null element object handle");
    currentElement = elementObj;
  }

  /**
   * Creates a record object from the archived-string data for an XML
   * record.
   * @param dataStr the archived-string data to use.
   * @param mkrObj Archivable.Marker parameter to indicate that this
   * constructor is used to dearchive a record.
   * @throws QWRecordException if an error occurs while creating
   * the data record.
   */
  public QWRecord(String dataStr, Archivable.Marker mkrObj)
                                                    throws QWRecordException
  {
    try
    {              //convert string to XML element and save it:
      currentElement = IstiXmlUtils.stringToElement(dataStr);
    }
    catch(Exception ex)
    {              //if error then convert to 'QWRecordException':
      throw new QWRecordException(ex.toString());
    }
  }

  /**
   * Indicates whether some other object is "equal to" this one.
   *
   * @param   obj   the reference object with which to compare.
   * @return  <code>true</code> if this object is the same as the obj
   *          argument; <code>false</code> otherwise.
   */
  public boolean equals(Object obj)
  {
    return obj instanceof QWRecord;
  }

  /**
   * Returns the current XML element for this record.
   * @return The current 'Element' object for this record.
   */
  public Element getCurrentElement()
  {
    return currentElement;
  }

  /**
   * Returns the value of an attribute in the current element.
   * @param nameStr the name of the attribute.
   * @return The value string.
   * @throws QWRecordException if the named attribute was not found.
   */
  protected String getAttribStr(String nameStr) throws QWRecordException
  {
    final String str;
         //get attribute:
    if((str=currentElement.getAttributeValue(nameStr)) == null)
    {    //attribute not found
      throw new QWRecordException("Element attribute \"" +
                                              nameStr + "\" not found");
    }
    return str;
  }

  /**
   * Returns the floating-point value of an attribute in the current
   * element.
   * @param nameStr the name of the attribute.
   * @return The numeric value from the attribute.
   * @throws QWRecordException if the named attribute was not found or if
   * its value could not be converted.
   */
  protected double getAttribDouble(String nameStr) throws QWRecordException
  {
    return parseDataDouble(nameStr,getAttribStr(nameStr)).doubleValue();
  }

  /**
   * Returns the integer value of an attribute in the current element.
   * @param nameStr the name of the attribute.
   * @return The numeric value from the attribute.
   * @throws QWRecordException if the named attribute was not found or if
   * its value could not be converted.
   */
  protected int getAttribInteger(String nameStr) throws QWRecordException
  {
    return parseDataInteger(nameStr,getAttribStr(nameStr)).intValue();
  }

  /**
   * Returns the long integer value of an attribute in the current element.
   * @param nameStr the name of the attribute.
   * @return The numeric value from the attribute.
   * @throws QWRecordException if the named attribute was not found or if
   * its value could not be converted.
   */
  protected long getAttribLong(String nameStr) throws QWRecordException
  {
    return parseDataLong(nameStr,getAttribStr(nameStr)).longValue();
  }

  /**
   * Returns the date/time value of an attribute in the current element.
   * @param nameStr the name of the attribute.
   * @return A new 'Date' object holding the date/time value.
   * @throws QWRecordException if the named attribute was not found or if
   * its value could not be converted.
   */
  protected Date getAttribTime(String nameStr) throws QWRecordException
  {
    return parseDataTime(nameStr,getAttribStr(nameStr));
  }

  /**
   * Returns the value of an attribute in the current element.
   * @param nameStr the name of the attribute.
   * @return The value string, or null if the named attribute was not found.
   */
  protected String getOptAttribStr(String nameStr)
  {
    return currentElement.getAttributeValue(nameStr);
  }

  /**
   * Returns the value of an attribute in the current element.
   * @param nameStr the name of the attribute.
   * @return The value string, or an empty string if the named attribute
   * was not found.
   */
  protected String getNonNullOptAttribStr(String nameStr)
  {
    final String str;
    return ((str=currentElement.getAttributeValue(nameStr)) != null) ?
                                                                   str : "";
  }

  /**
   * Returns the floating-point value of an optional attribute in the
   * current element.
   * @param nameStr the name of the attribute.
   * @return A new Double object holding the value, or null if the attribute
   * was not found.
   * @throws QWRecordException if the attribute was found but its value
   * could not be converted.
   */
  protected Double getOptAttribDouble(String nameStr)
                                                    throws QWRecordException
  {
    return parseDataDouble(nameStr,
                                 currentElement.getAttributeValue(nameStr));
  }

  /**
   * Returns the integer value of an optional attribute in the current
   * element.
   * @param nameStr the name of the attribute.
   * @return A new Integer object holding the value, or null if the attribute
   * was not found.
   * @throws QWRecordException if the attribute was found but its value
   * could not be converted.
   */
  protected Integer getOptAttribInteger(String nameStr)
                                                    throws QWRecordException
  {
    return parseDataInteger(nameStr,
                                 currentElement.getAttributeValue(nameStr));
  }

  /**
   * Returns the boolean value of an optional attribute in the current
   * element, or a default value if the attribute is not found.
   * @param nameStr the name of the attribute.
   * @param defaultValueFlag the default value to be returned if the given
   * attribute is not found.
   * @return the boolean value.
   * @throws QWRecordException if the attribute was found but its value
   * could not be converted.
   */
  protected boolean getDefAttribBoolean(String nameStr,
                          boolean defaultValueFlag) throws QWRecordException
  {
    final Boolean booleanObj = getOptAttribBoolean(nameStr);
    if (booleanObj == null)
      return defaultValueFlag;         //if not found then return default
    return booleanObj.booleanValue();
  }

  /**
   * Returns the boolean value of an optional attribute in the current
   * element, or null if the attribute is not found.
   * @param nameStr the name of the attribute.
   * @return A new Boolean object holding the value, or null if the attribute
   * was not found.
   * @throws QWRecordException if the attribute was found but its value
   * could not be converted.
   */
  protected Boolean getOptAttribBoolean(String nameStr) throws QWRecordException
  {
    return parseDataBoolean(nameStr,
                              currentElement.getAttributeValue(nameStr));
  }

  /**
   * Returns the date/time value of an optional attribute in the current
   * element.
   * @param nameStr the name of the attribute.
   * @return A new Date object holding the date/time value, or null if
   * the attribute was not found.
   * @throws QWRecordException if the attribute was found but its value
   * could not be converted.
   */
  protected Date getOptAttribTime(String nameStr) throws QWRecordException
  {
    return parseDataTime(nameStr,currentElement.getAttributeValue(nameStr));
  }

  /**
   * Parses a floating-point numeric string.
   * @param nameStr the name of the item in use (for error message
   * generation).
   * @param str the floating-point numeric string to parse (or null for
   * none).
   * @return A new Double object holding the value, or null if the 'str'
   * parameter is null.
   * @throws QWRecordException if the string could not be converted.
   */
  protected Double parseDataDouble(String nameStr, String str)
                                                    throws QWRecordException
  {
    if(str == null)
      return null;
    final Double doubleObj;
    if((doubleObj=QWUtils.parseStringDouble(str)) == null)
    {    //error parsing numeric value
      throw new QWRecordException("Error parsing floating-point \"" +
                                   nameStr + "\" data value (" + str + ")");
    }
    return doubleObj;
  }

  /**
   * Parses an integer numeric string.
   * @param nameStr the name of the item in use (for error message
   * generation).
   * @param str the floating-point numeric string to parse (or null for
   * none).
   * @return A new Integer object holding the value, or null if the 'str'
   * parameter is null.
   * @throws QWRecordException if the string could not be converted.
   */
  protected Integer parseDataInteger(String nameStr, String str)
                                                    throws QWRecordException
  {
    if(str == null)
      return null;
    final Integer integerObj;
    if((integerObj=QWUtils.parseStringInteger(str)) == null)
    {    //error parsing numeric value
      throw new QWRecordException("Error parsing integer \"" +
                                   nameStr + "\" data value (" + str + ")");
    }
    return integerObj;
  }

  /**
   * Parses a long integer numeric string.
   * @param nameStr the name of the item in use (for error message
   * generation).
   * @param str the floating-point numeric string to parse (or null for
   * none).
   * @return A new Long object holding the value, or null if the 'str'
   * parameter is null.
   * @throws QWRecordException if the string could not be converted.
   */
  protected Long parseDataLong(String nameStr, String str)
                                                    throws QWRecordException
  {
    if(str == null)
      return null;
    final Long longObj;
    if((longObj=QWUtils.parseStringLong(str)) == null)
    {    //error parsing numeric value
      throw new QWRecordException("Error parsing long integer \"" +
                                   nameStr + "\" data value (" + str + ")");
    }
    return longObj;
  }

  /**
   * Parses a date/time specification string.
   * @param nameStr the name of the item in use (for error message
   * generation).
   * @param str the date/time string to parse (or null for none).
   * @return A new Date object holding the date/time, or null if the 'str'
   * parameter is null.
   * @throws QWRecordException if the string could not be converted.
   */
  protected Date parseDataTime(String nameStr, String str)
                                                    throws QWRecordException
  {
    if(str == null)
      return null;
    final Date dateObj;           //parse string as XML date:
    try
    {
      int sLen;
      char ch;
      if((sLen=str.length()) > 0 && ((ch=str.charAt(sLen-1)) == 'Z' ||
                                                                 ch == 'z'))
      {  //date/time value ends with trailing 'Z'
        synchronized(xmlDateFormatterObj)
        {  //only allow one thread at a time to use date-format object
          if((dateObj=xmlDateFormatterObj.parse(str)) != null)
            return dateObj;
        }
      }
      else
      {  //date/time value does not end with trailing 'Z' (use alt format)
        synchronized(xmlDateFormatter2Obj)
        {  //only allow one thread at a time to use date-format object
          if((dateObj=xmlDateFormatter2Obj.parse(str)) != null)
            return dateObj;
        }
      }
    }
    catch(ParseException ex)
    {
    }
              //if unable to parse then throw exception:
    throw new QWRecordException("Error parsing \"" + nameStr +
                          "\" data date/time value (\"" + str + "\")");
  }

  /**
   * Parses a boolean string.
   * @param nameStr the name of the item in use (for error message
   * generation).
   * @param str the boolean string to parse (or null for
   * none).
   * @return A new Boolean object holding the value, or null if the 'str'
   * parameter is null.
   * @throws QWRecordException if the string could not be converted.
   */
  protected Boolean parseDataBoolean(String nameStr, String str)
                                                    throws QWRecordException
  {
    if(str == null)
      return null;
    if(str.equalsIgnoreCase(MsgTag.TRUE) ||
                        str.equalsIgnoreCase(MsgTag.YES) || str.equals("1"))
    {    //value is "true", "Y" or "1"
      return Boolean.TRUE;
    }
    if(str.equalsIgnoreCase(MsgTag.FALSE) ||
                         str.equalsIgnoreCase(MsgTag.NO) || str.equals("0"))
    {    //value is "false", "N" or "0"
      return Boolean.FALSE;
    }
    throw new QWRecordException("Error parsing boolean \"" +
                                   nameStr + "\" data value (" + str + ")");

  }

  /**
   * Returns the floating-point value of the text for the current element.
   * @return A new Double object holding the value, or null if the element
   * text is empty.
   * @throws QWRecordException if the string could not be converted.
   */
  protected final Double getOptTextDouble() throws QWRecordException
  {
    final String str;
    if((str=currentElement.getTextTrim()).length() <= 0)
      return null;
    final Double doubleObj;
    if((doubleObj=QWUtils.parseStringDouble(str)) == null)
    {    //error parsing numeric value
      throw new QWRecordException("Error parsing floating-point \"" +
                currentElement.getName() + "\" element text (" + str + ")");
    }
    return doubleObj;
  }

  /**
   * Returns the value of a child element in the current element.
   * @param nameStr the name of the child element.
   * @return The value string.
   * @throws QWRecordException if the child element was not found.
   */
  protected String getElementStr(String nameStr) throws QWRecordException
  {
    final String str;
         //get child element:
    if((str=currentElement.getChildText(
                            nameStr,currentElement.getNamespace())) == null)
    {    //child element not found
      throw new QWRecordException("Child element \"" +
                                              nameStr + "\" not found");
    }
    return str;
  }

  /**
   * Returns the floating-point value of a child-element of the current
   * element.
   * @param nameStr the name of the element.
   * @return The numeric value from the element.
   * @throws QWRecordException if the named element was not found or if
   * its value could not be converted.
   */
  protected double getElementDouble(String nameStr) throws QWRecordException
  {
    return parseDataDouble(nameStr,getElementStr(nameStr)).doubleValue();
  }

  /**
   * Returns the integer value of a child-element of the current element.
   * @param nameStr the name of the element.
   * @return The numeric value from the element.
   * @throws QWRecordException if the named element was not found or if
   * its value could not be converted.
   */
  protected int getElementInteger(String nameStr) throws QWRecordException
  {
    return parseDataInteger(nameStr,getElementStr(nameStr)).intValue();
  }

  /**
   * Returns the long integer value of a child-element of the current element.
   * @param nameStr the name of the element.
   * @return The numeric value from the element.
   * @throws QWRecordException if the named element was not found or if
   * its value could not be converted.
   */
  protected long getElementLong(String nameStr) throws QWRecordException
  {
    return parseDataLong(nameStr,getElementStr(nameStr)).longValue();
  }

  /**
   * Returns the date/time value of a child-element of the current element.
   * @param nameStr the name of the element.
   * @return A new 'Date' object holding the date/time value.
   * @throws QWRecordException if the named element was not found or if
   * its value could not be converted.
   */
  protected Date getElementTime(String nameStr) throws QWRecordException
  {
    return parseDataTime(nameStr,getElementStr(nameStr));
  }

  /**
   * Returns the value of a child element in the current element.
   * @param nameStr the name of the child element.
   * @return The value string, or null if the named child element was
   * not found.
   */
  protected String getOptElementStr(String nameStr)
  {
    return currentElement.getChildText(
                                     nameStr,currentElement.getNamespace());
  }

  /**
   * Returns the value of a child element in the current element.
   * @param nameStr the name of the child element.
   * @return The value string, or an empty string if the named
   * child element was not found.
   */
  protected String getNonNullOptElementStr(String nameStr)
  {
    final String str;
    return ((str=currentElement.getChildText(
                nameStr,currentElement.getNamespace())) != null) ? str : "";
  }

  /**
   * Returns the floating-point value of an optional child-element of the
   * current element.
   * @param nameStr the name of the element.
   * @return A new Double object holding the value, or null if the element
   * was not found.
   * @throws QWRecordException if the element was found but its value
   * could not be converted.
   */
  protected Double getOptElementDouble(String nameStr)
                                                    throws QWRecordException
  {
    return parseDataDouble(nameStr,currentElement.getChildText(
                                    nameStr,currentElement.getNamespace()));
  }

  /**
   * Returns the integer value of an optional child-element of the current
   * element.
   * @param nameStr the name of the element.
   * @return A new Integer object holding the value, or null if the element
   * was not found.
   * @throws QWRecordException if the element was found but its value
   * could not be converted.
   */
  protected Integer getOptElementInteger(String nameStr)
                                                    throws QWRecordException
  {
    return parseDataInteger(nameStr,currentElement.getChildText(
                                    nameStr,currentElement.getNamespace()));
  }

  /**
   * Returns the date/time value of an optional child-element of the current
   * element.
   * @param nameStr the name of the element.
   * @return A new Date object holding the date/time value, or null if
   * the element was not found.
   * @throws QWRecordException if the element was found but its value
   * could not be converted.
   */
  protected Date getOptElementTime(String nameStr) throws QWRecordException
  {
    return parseDataTime(nameStr,currentElement.getChildText(
                                    nameStr,currentElement.getNamespace()));
  }

  /**
   * Returns the text of the child element 'value' in a child element
   * in the current element.
   * @param nameStr the name of the child element.
   * @return The value string.
   * @throws QWRecordException if the child element or 'value' element
   * was not found.
   */
  protected String getElementValueStr(String nameStr)
                                                    throws QWRecordException
  {
    final Element elemObj;
    if((elemObj=currentElement.getChild(
                            nameStr,currentElement.getNamespace())) == null)
    {    //named child element not found
      throw new QWRecordException("Child element \"" +
                                                  nameStr + "\" not found");
    }
    final String str;
         //get child element:
    if((str=elemObj.getChildText(
                      MsgTag.QUAKEML_VALUE,elemObj.getNamespace())) == null)
    {    //child element 'value' not found
      throw new QWRecordException("Child element '" + MsgTag.QUAKEML_VALUE +
                              "' in element \"" + nameStr + "\" not found");
    }
    return str;
  }

  /**
   * Returns the floating-point value of the child element 'value' in a
   * child-element of the current element.
   * @param nameStr the name of the element.
   * @return The numeric value from the element.
   * @throws QWRecordException if the named element or 'value' element
   * was not found or if its value could not be converted.
   */
  protected double getElementValueDouble(String nameStr) throws QWRecordException
  {
    return parseDataDouble(nameStr,getElementValueStr(nameStr)).doubleValue();
  }

  /**
   * Returns the date/time value of the child element 'value' in a
   * child-element of the current element.
   * @param nameStr the name of the element.
   * @return A new 'Date' object holding the date/time value.
   * @throws QWRecordException if the named element or 'value' element
   * was not found or if its value could not be converted.
   */
  protected Date getElementValueTime(String nameStr) throws QWRecordException
  {
    return parseDataTime(nameStr,getElementValueStr(nameStr));
  }

  /**
   * Returns the floating-point value of the child element 'value' in an
   * optional child-element of the current element.
   * @param nameStr the name of the element.
   * @return A new Double object holding the value, or null if the element
   * was not found.
   * @throws QWRecordException if the element was found but its value
   * could not be converted.
   */
  protected Double getOptElementValueDouble(String nameStr)
                                                    throws QWRecordException
  {
    final Element elemObj;
    if((elemObj=currentElement.getChild(
                            nameStr,currentElement.getNamespace())) == null)
    {  //name child element not found
      return null;           //indicate not found
    }
    return parseDataDouble(nameStr,elemObj.getChildText(
                              MsgTag.QUAKEML_VALUE,elemObj.getNamespace()));
  }

  /**
   * Returns the value of a sub-child element under a child element in
   * the current element.
   * @param elemNameStr name of child element.
   * @param childNameStr name of sub-child element.
   * @param nsPrefixStr namespace-prefix string for sub-child element,
   * or null to use same namespace as parent.
   * @return The value string, or null if the named child elements were
   * not found.
   */
  public String getChildElementStr(String elemNameStr, String childNameStr,
                                                         String nsPrefixStr)
  {
    final Element elemObj;
    if((elemObj=currentElement.getChild(
                        elemNameStr,currentElement.getNamespace())) != null)
    {  //child element found; setup namespace for sub-child
      final Namespace childNsObj = (nsPrefixStr != null) ?
                 elemObj.getNamespace(nsPrefixStr) : elemObj.getNamespace();
                        //return text from sub-child element:
      return elemObj.getChildText(childNameStr,childNsObj);
    }
    return null;
  }

  /**
   * Returns the value of a sub-child element under a child element in
   * the current element.
   * @param elemNameStr name of child element.
   * @param childNameStr name of sub-child element.
   * @return The value string, or null if the named child elements were
   * not found.
   */
  public String getChildElementStr(String elemNameStr, String childNameStr)
  {
    return getChildElementStr(elemNameStr,childNameStr,null);
  }

  /**
   * Returns the integer value of a sub-child element under a child element
   * in the current element.
   * @param elemNameStr name of child element.
   * @param childNameStr name of sub-child element.
   * @param nsPrefixStr namespace-prefix string for sub-child element,
   * or null to use same namespace as parent.
   * @return The value string, or null if the named child elements were
   * not found.
   * @return A new Integer object holding the value, or null if the named
   * child elements were not found.
   * @throws QWRecordException if the element was found but its value
   * could not be converted.
   */
  public Integer getChildElementInteger(String elemNameStr,
                                    String childNameStr, String nsPrefixStr)
                                                    throws QWRecordException
  {
    return parseDataInteger(childNameStr,
                  getChildElementStr(elemNameStr,childNameStr,nsPrefixStr));
  }

  /**
   * Returns the integer value of a sub-child element under a child element
   * in the current element.
   * @param elemNameStr name of child element.
   * @param childNameStr name of sub-child element.
   * @return The value string, or null if the named child elements were
   * not found.
   * @return A new Integer object holding the value, or null if the named
   * child elements were not found.
   * @throws QWRecordException if the element was found but its value
   * could not be converted.
   */
  public Integer getChildElementInteger(String elemNameStr,
                               String childNameStr) throws QWRecordException
  {
    return getChildElementInteger(elemNameStr,childNameStr,null);
  }

  /**
   * Returns the double value of a sub-child element under a child element
   * in the current element.
   * @param elemNameStr name of child element.
   * @param childNameStr name of sub-child element.
   * @param nsPrefixStr namespace-prefix string for sub-child element,
   * or null to use same namespace as parent.
   * @return The value string, or null if the named child elements were
   * not found.
   * @return A new Double object holding the value, or null if the named
   * child elements were not found.
   * @throws QWRecordException if the element was found but its value
   * could not be converted.
   */
  public Double getChildElementDouble(String elemNameStr,
                                    String childNameStr, String nsPrefixStr)
                                                    throws QWRecordException
  {
    return parseDataDouble(childNameStr,
                  getChildElementStr(elemNameStr,childNameStr,nsPrefixStr));
  }

  /**
   * Returns the double value of a sub-child element under a child element
   * in the current element.
   * @param elemNameStr name of child element.
   * @param childNameStr name of sub-child element.
   * @return The value string, or null if the named child elements were
   * not found.
   * @return A new Double object holding the value, or null if the named
   * child elements were not found.
   * @throws QWRecordException if the element was found but its value
   * could not be converted.
   */
  public Double getChildElementDouble(String elemNameStr,
                               String childNameStr) throws QWRecordException
  {
    return getChildElementDouble(elemNameStr,childNameStr,null);
  }
}
