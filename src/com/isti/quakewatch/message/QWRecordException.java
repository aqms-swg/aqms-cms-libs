//QWRecordException.java:  Defines an exception thrown while constructing
//                         a QuakeWatch data record.
//
//   9/9/2002 -- [ET]  Initial version.
// 10/24/2003 -- [ET]  Moved from 'QWClient' to 'QWCommon' project;
//                     removed "org.jdom.Element" import; changed base
//                     class from 'Exception' to 'RuntimeException'.
//

package com.isti.quakewatch.message;

/**
 * QWRecordException defines an exception thrown while constructing
 * a QuakeWatch data record.
 */
public class QWRecordException extends RuntimeException
{
  /**
   * Creates an exception thrown while constructing a 'QWEventRecord'.
   * @param msgStr a descriptive message for the exception.
   */
  public QWRecordException(String msgStr)
  {
    super(msgStr);
  }
}
