//QWStaticMsgNumTimeRec.java:  Defines an immutable record holding
//                             message-record and time-generated values.
//
// 11/26/2003 -- [ET]
//

package com.isti.quakewatch.message;

/**
 * Class QWStaticMsgNumTimeRec defines an immutable record holding
 * holding message-record and time-generated values.
 */
public class QWStaticMsgNumTimeRec implements QWMsgNumTimeRec
{
  private final long msgNumVal;
  private final long timeGeneratedVal;

  /**
   * Creates a dynamic message record holding message-record and
   * time-generated values.
   * @param msgNum the message number to use.
   * @param timeGenerated the time-generated value to use.
   */
  public QWStaticMsgNumTimeRec(long msgNum,long timeGenerated)
  {
    msgNumVal = msgNum;
    timeGeneratedVal = timeGenerated;
  }

  /**
   * Returns the message number value.  This method is needed to
   * implemented the 'QWMsgNumTimeRec' interface.
   * @return The message number value.
   */
  public long getMsgNum()
  {
    return msgNumVal;
  }

  /**
   * Returns the time-generated value.  This method is needed to
   * implemented the 'QWMsgNumTimeRec' interface.
   * @return The time-generated value, in milliseconds since 1/1/1970.
   */
  public long getTimeGenerated()
  {
    return timeGeneratedVal;
  }
}
