//QWStationAmpMsgRecord.java:  Defines a record of amplitude data.
//
//  5/19/2005 -- [ET]  Initial version.
//   6/2/2005 -- [ET]  Added 'get/setSupersededRecordObj()' methods.
//  6/17/2005 -- [ET]  Modified to select component with largest amplitude
//                     value.
//  6/27/2005 -- [ET]  Added 'get/setPlaybackTimeDateObj()' methods.
//   8/5/2005 -- [ET]  Added methods 'containsShakeMapProduct()' and
//                     'containsTsunamiProduct()'.
// 12/23/2005 -- [KF]  Moved 'createFloatNumberFormat' methods to "UtilFns".
//                     Added change listener for default locale.
//   6/7/2006 -- [ET]  Fixed potential null-pointer exception in constructor;
//                     added 'compareTo(obj,int)' method; added methods
//                     "get/setValueSortRankSpecifier()"; modified method
//                     'timeEquals()' to work with a 'QWEventMsgRecord'
//                     object (instead of only 'QWStationAmpMsgRecord');
//                     added "get/setSelectedTimeVal()" methods.
//  5/12/2011 -- [ET]  Added 'getLCEventIDKey()' method.
//

package com.isti.quakewatch.message;

import java.util.Date;
import java.util.Calendar;
import java.util.List;
import java.util.Iterator;
import java.util.TimeZone;
import java.util.Locale;
import java.text.NumberFormat;
import java.text.DateFormat;
import java.text.DecimalFormatSymbols;
import java.text.DateFormatSymbols;
import org.jdom.Element;
import com.isti.util.UtilFns;
import com.isti.util.FifoHashtable;
import com.isti.util.DataChangedListener;
import com.isti.util.ExtendedComparable;
import com.isti.quakewatch.util.QWUtils;
import com.isti.quakewatch.common.MsgTag;

/**
 * QWStationAmpMsgRecord defines a record of amplitude data.  A selector
 * is used to determine whether the acceleration or velocity amplitude
 * is represented.  In each case, the component with the largest amplitude
 * value is used.
 */
public class QWStationAmpMsgRecord extends QWDataMsgRecord
                   implements Comparable,QWEventMsgRecord,ExtendedComparable
{
  /** Selection value for "acceleration" (for 'selectAmplitudeType()'). */
  public static final int AMP_ACC_TYPE = 0;
  /** Selection value for "velocity" (for 'selectAmplitudeType()'). */
  public static final int AMP_VEL_TYPE = 1;
  /** Default amplitude selection value ("velocity"). */
  public static final int AMP_DEFAULT_TYPE = AMP_VEL_TYPE;

    /** Handle to 'Element' object used to construct this object. */
  public final Element stationAmpElement;
    /** Station ID code. */
  public final String sta;
    /** Network ID code for station. */
  public final String net;
    /** Latitude value for event. */
  public final double lat;
    /** Longitude value for event. */
  public final double lon;
    /** Date object specifying date/time for message. */
  public final Date time;
    /** Verbose station name, or null if none specified. */
  public final String stationName;
    /** Station elevation, or null if none specified. */
  public final Double elev;
    /** Instrument type, or null if none specified. */
  public final String instType;
    /** Description of data source, or null if none specified. */
  public final String source;
    /** Communications interface type, or null if none specified. */
  public final String commType;
    /** Acceleration record object. */
  public final AmplitudeRecord accAmplitudeRecObj;
    /** Velocity record object. */
  public final AmplitudeRecord velAmplitudeRecObj;
    /** "Station, channel, network, location" string for acceleration amp. */
  public final String accSCNLString;
    /** "Station, channel, network, location" string for velocity amp. */
  public final String velSCNLString;
    /** "Station, channel, network, location, time, acc, vel" string. */
  protected final String keyNscltValsString;
    /** Handle to record that this record superseded, or null for none. */
  protected QWEventMsgRecord supersededRecordObj = null;
    /** Last time that message record was selected. */
  protected long selectedTimeVal = 0;
    /** Playback time for amplitude record, or null for none. */
  protected Date playbackTimeDateObj = null;
    /** Thread-synchronization object for 'playbackTimeDateObj'. */
  protected final Object playbackTimeSyncObj = new Object();
    /** Selector value set by 'selectAmplitudeType()' method. */
  private static int amplitudeTypeSelector = AMP_DEFAULT_TYPE;
    /** Value-sort range specifier set by 'setValueSortRankSpecifier()'. */
  private static int valueSortRankSpecifier = 1;

    /** Text to display when there is no value. */
  public static final String NO_VALUE_STR = "??";

    //tag strings for 'getLat/LonString()' methods:
  private static final String N_STR = " N";
  private static final String S_STR = " S";
  private static final String E_STR = " E";
  private static final String W_STR = " W";

  private static final String CSEP_STR = ", ";

         //number formatter to convert lat/lon values to strings:
  private static final NumberFormat latLonDigitNumberFormatter =
      UtilFns.createFloatNumberFormat(3);
         //number formatter to convert amplitude values to strings:
  private static final NumberFormat amplitudeNumberFormatter =
      UtilFns.createFloatNumberFormat(1,4);

    //date format object for creating XML-format date/time strings:
  private static final DateFormat xmlDateTimeFormatterObj =
                                           QWUtils.getXmlDateFormatterObj();
    //date format object for creating date/time strings:
  private static final DateFormat dateTimeFormatterObj =
      UtilFns.createDateFormatObj("yyyy/MM/dd  HH:mm:ss");
    //date formatter object for creating date string:
  private static final DateFormat dateFormatterObj =
      UtilFns.createDateFormatObj("MMM d, yyyy");
    //time formatter object for creating time string:
  private static final DateFormat timeFormatterObj =
      UtilFns.createDateFormatObj("HH:mm:ss.SSS z");

  static
  {
    //add data-change listener to handle default-locale changes
    UtilFns.addDataChangedListener(new DataChangedListener()
        {
          /**
           * Called when data has changed.
           * @param sourceObj source object that called this method.
           */
          public void dataChanged(Object sourceObj)
          {
            //if there was a default locale change
            final Locale localeObj = UtilFns.getDefaultLocale();
            if(sourceObj.equals(localeObj))
            {
              //update the float number formatters
              final DecimalFormatSymbols newSymbols =
                                        new DecimalFormatSymbols(localeObj);
              UtilFns.setDecimalFormatSymbols(
                                     latLonDigitNumberFormatter,newSymbols);
              UtilFns.setDecimalFormatSymbols(
                                       amplitudeNumberFormatter,newSymbols);

              //update the date formatters
              final DateFormatSymbols newFormatSymbols =
                                           new DateFormatSymbols(localeObj);
              UtilFns.setDateFormatSymbols(
                                    dateTimeFormatterObj, newFormatSymbols);
              UtilFns.setDateFormatSymbols(
                                        dateFormatterObj, newFormatSymbols);
              UtilFns.setDateFormatSymbols(
                                        timeFormatterObj, newFormatSymbols);
            }
          }
        });
  }

  /**
   * Creates a data record for one event, built from an XML "Event"
   * message element.
   * @param qwMsgElement the XML "QWmessage" element object.
   * @param dataMsgElement the XML "DataMessage" element object.
   * @param stationAmpElement the XML "StationAmp" message element.
   * @throws QWRecordException if an error occurs while creating
   * the data record.
   */
  public QWStationAmpMsgRecord(Element qwMsgElement,Element dataMsgElement,
                         Element stationAmpElement) throws QWRecordException
  {
    super(qwMsgElement,dataMsgElement);  //construct parents
    this.stationAmpElement = stationAmpElement;  //save handle to element
    if(!MsgTag.STATION_AMP.equalsIgnoreCase(
                                               stationAmpElement.getName()))
    {    //root element name not "Event"
      throw new QWRecordException("Element tag name \"" +
                             MsgTag.STATION_AMP + "\" not found");
    }
    currentElement = stationAmpElement;     //save handle to element

         //get station "Sta" attribute:
    sta = getAttribStr(MsgTag.STA);
         //get optional "Net" attribute:
    net = getAttribStr(MsgTag.NET);
         //get "Latitude" attribute:
    lat = getAttribDouble(MsgTag.LAT);
         //get "Longitude" attribute:
    lon = getAttribDouble(MsgTag.LON);
         //get message "Time" attribute:
    time = getAttribTime(MsgTag.TIME);

         //get optional station "Name" attribute:
    stationName = getOptAttribStr(MsgTag.NAME);
         //get optional "Elev" attribute:
    elev = getOptAttribDouble(MsgTag.ELEV);
         //get optional "InstType" attribute:
    instType = getOptAttribStr(MsgTag.INST_TYPE);
         //get optional "Source" attribute:
    source = getOptAttribStr(MsgTag.SOURCE);
         //get optional "CommType" attribute:
    commType = getOptAttribStr(MsgTag.COMM_TYPE);

         //process "Comp" elements:
    Double curAccAmpValObj,curVelAmpValObj;
    Element curAccElemObj,curVelElemObj;
    Double ampValObj, maxAccAmpValObj = null, maxVelAmpValObj = null;
    Element maxAccCompElemObj = null, maxVelCompElemObj = null;
    Element maxAccAmpElemObj = null, maxVelAmpElemObj = null;
    final List compListObj;  //get list of "Comp" elems under "StationTag":
    if((compListObj=stationAmpElement.getChildren(MsgTag.COMP)) != null
                                                  && compListObj.size() > 0)
    {    //at least one "Comp" element available
      final Iterator compIterObj = compListObj.iterator();
      Object obj;
      Element compElem;
      List ampListObj;
      Iterator ampIterObj;
      String typeStr;
      do
      {  //for each "Comp" element
        if((obj=compIterObj.next()) instanceof Element &&
             (ampListObj=(compElem=(Element)obj).getChildren(MsgTag.AMP)) !=
                                              null && ampListObj.size() > 0)
        {     //"Comp" element OK and contains "Amp" child elements
          ampIterObj = ampListObj.iterator();
                   //initialize "current" handles:
          curAccAmpValObj = curVelAmpValObj = null;
          curAccElemObj = curVelElemObj = null;
          while(ampIterObj.hasNext())
          {   //for each "Amp" child element
            if((obj=ampIterObj.next()) instanceof Element)
            {      //'Element' object fetched OK
                   //setup 'currentElement' handle for "get...()" methods:
              currentElement = (Element)obj;
              if((typeStr=getOptAttribStr(MsgTag.TYPE)) != null &&
                                     (ampValObj=getOptTextDouble()) != null)
              {    //"Type" attribute found and amplitude value found
                if(typeStr.equalsIgnoreCase(MsgTag.TYPE_ACC_VAL))
                {  //type is "acceleration"
                  if(curAccAmpValObj == null ||
                                    ampValObj.compareTo(curAccAmpValObj) > 0)
                  {     //no previous current value or new value is larger
                    curAccAmpValObj = ampValObj;      //save new value
                    curAccElemObj = currentElement;   //save Amp element
                  }
                }
                else if(typeStr.equalsIgnoreCase(MsgTag.TYPE_VEL_VAL))
                {    //type is "velocity"
                  if(curVelAmpValObj == null ||
                                    ampValObj.compareTo(curVelAmpValObj) > 0)
                  {     //no previous current value or new value is larger
                    curVelAmpValObj = ampValObj;      //save new value
                    curVelElemObj = currentElement;   //save Amp element
                  }
                }
              }
            }
          }
                   //check if comp has max acc amp value and save if so:
          if(curAccAmpValObj != null && (maxAccAmpValObj == null ||
                            curAccAmpValObj.compareTo(maxAccAmpValObj) > 0))
          {   //no previous max or current value is new maximum
            maxAccAmpValObj = curAccAmpValObj;   //save new max value
            maxAccCompElemObj = compElem;        //save Comp element
            maxAccAmpElemObj = curAccElemObj;    //save Acc element
          }
                   //check if comp has max vel amp value and save if so:
          if(curVelAmpValObj != null && (maxVelAmpValObj == null ||
                            curVelAmpValObj.compareTo(maxVelAmpValObj) > 0))
          {   //no previous max or current value is new maximum
            maxVelAmpValObj = curVelAmpValObj;   //save new max value
            maxVelCompElemObj = compElem;        //save Comp element
            maxVelAmpElemObj = curVelElemObj;    //save Vel element
          }
        }
      }
      while(compIterObj.hasNext());    //loop if more "comp" elements
      currentElement = stationAmpElement;   //restore "current" element
    }
         //setup amplitude record for maximum acceleration value:
    accAmplitudeRecObj = (maxAccAmpElemObj != null) ?
                 createAmplitudeRecordObj(maxAccAmpValObj,maxAccCompElemObj,
                    maxAccAmpElemObj) : AmplitudeRecord.emptyAmpCompInfoObj;
         //setup amplitude record for maximum velocity value:
    velAmplitudeRecObj = (maxVelAmpElemObj != null) ?
                 createAmplitudeRecordObj(maxVelAmpValObj,maxVelCompElemObj,
                    maxVelAmpElemObj) : AmplitudeRecord.emptyAmpCompInfoObj;

         //setup SCNL strings:
    accSCNLString = ((sta != null) ? sta : UtilFns.EMPTY_STRING) +
                           ',' + ((accAmplitudeRecObj.compNameStr != null) ?
                    accAmplitudeRecObj.compNameStr : UtilFns.EMPTY_STRING) +
                        ',' + ((net != null) ? net : UtilFns.EMPTY_STRING) +
                                  ((accAmplitudeRecObj.compLocStr != null) ?
              (',' + accAmplitudeRecObj.compLocStr) : UtilFns.EMPTY_STRING);
    velSCNLString = ((sta != null) ? sta : UtilFns.EMPTY_STRING) +
                           ',' + ((velAmplitudeRecObj.compNameStr != null) ?
                    velAmplitudeRecObj.compNameStr : UtilFns.EMPTY_STRING) +
                        ',' + ((net != null) ? net : UtilFns.EMPTY_STRING) +
                                  ((velAmplitudeRecObj.compLocStr != null) ?
              (',' + velAmplitudeRecObj.compLocStr) : UtilFns.EMPTY_STRING);
         //setup "key" string for record:
    synchronized(xmlDateTimeFormatterObj)
    {    //only allow thread at a time to use date-format object
      keyNscltValsString = ((net != null) ? net : UtilFns.EMPTY_STRING) +
                  ',' + ((sta != null) ? sta : UtilFns.EMPTY_STRING) + ',' +
                                 ((accAmplitudeRecObj.compNameStr != null) ?
              accAmplitudeRecObj.compNameStr : UtilFns.EMPTY_STRING) + ',' +
                                  ((accAmplitudeRecObj.compLocStr != null) ?
               accAmplitudeRecObj.compLocStr : UtilFns.EMPTY_STRING) + ',' +
                                 ((velAmplitudeRecObj.compNameStr != null) ?
              velAmplitudeRecObj.compNameStr : UtilFns.EMPTY_STRING) + ',' +
                                  ((velAmplitudeRecObj.compLocStr != null) ?
               velAmplitudeRecObj.compLocStr : UtilFns.EMPTY_STRING) + ',' +
                    ((time != null) ? xmlDateTimeFormatterObj.format(time) :
                                               UtilFns.EMPTY_STRING) + ',' +
                                      accAmplitudeRecObj.ampValueStr + ',' +
                                             velAmplitudeRecObj.ampValueStr;
    }
  }

  /**
   * Creates an amplitude record based on the given parameters.
   * @param ampValueObj amplitude value.
   * @param compElemObj component Element for amplitude.
   * @param ampElemObj amplitude Element for amplitude.
   * @return A new 'AmplitudeRecord' object.
   */
  protected final AmplitudeRecord createAmplitudeRecordObj(
                Double ampValueObj, Element compElemObj, Element ampElemObj)
  {
    final Calendar calObj = Calendar.getInstance(QWUtils.gmtTimeZone);
    Integer tOffMsIntObj;
    final Date ampTimeObj;
    final String ampUnitsStr;
    int tOffsMsVal = 0;
    final Element oldCurElemObj = currentElement;   //save value
    if(ampElemObj != null)
    {    //element object exists for amplitude
                   //setup 'currentElement' handle for "get...()" methods:
      currentElement = ampElemObj;
                                  //fetch "TOffMs" attribute:
      if((tOffMsIntObj=getOptAttribInteger(MsgTag.T_OFF_MS)) != null &&
                               (tOffsMsVal=tOffMsIntObj.intValue()) != 0)
      {  //time-offset value is non-zero
        calObj.setTime(time);     //setup cal obj and add offset
        calObj.add(Calendar.MILLISECOND,tOffsMsVal);
        ampTimeObj = calObj.getTime();      //save value
      }
      else    //time-offset value is not present or is zero
        ampTimeObj = time;
      ampUnitsStr = getOptAttribStr(MsgTag.UNITS);    //fetch "Units" attrib
    }
    else
    {    //no element object for amplitude
      ampTimeObj = time;
      ampUnitsStr = null;
    }
    currentElement = oldCurElemObj;         //restore current-elem handle
              //create and return amp-component-info object:
    return new AmplitudeRecord(ampValueObj,tOffsMsVal,ampTimeObj,ampUnitsStr,
                                 compElemObj.getAttributeValue(MsgTag.NAME),
                                 compElemObj.getAttributeValue(MsgTag.LOC));
  }

  /**
   * Selects which type of amplitude values ("acceleration" or "velocity")
   * will be returned by "getAmp...()" and several other methods.
   * This static method affects all 'QWStationAmpMsgRecord' instances.
   * If this method is not called then the default "acceleration" will
   * be used.
   * @param selVal the selector value 'AMP_ACC_TYPE' or 'AMP_VEL_TYPE'.
   */
  public static void selectAmplitudeType(int selVal)
  {
    amplitudeTypeSelector = selVal;
  }

  /**
   * Returns the selection value set via the 'selectAmplitudeType()'
   * method.
   * @returns The selector value 'AMP_ACC_TYPE' or 'AMP_VEL_TYPE'.
   */
  public static int getAmplitudeTypeValue()
  {
    return amplitudeTypeSelector;
  }

  /**
   * Sets the value-sort rank specifier.  This is used by the
   * 'compareTo(obj,int)' method to determine the result when
   * two different types of objects that implement 'QWEventMsgRecord'
   * are compared (using 'VALUE_SORT_TYPE').
   * @param specVal numeric specifier to use.
   */
  public static void setValueSortRankSpecifier(int specVal)
  {
    valueSortRankSpecifier = specVal;
  }

  /**
   * Returns the value-sort rank specifier.  This is used by the
   * 'compareTo(obj,int)' method to determine the result when
   * two different types of objects that implement 'QWEventMsgRecord'
   * are compared (using 'VALUE_SORT_TYPE').
   * @return The numeric value-sort rank specifier.
   */
  public int getValueSortRankSpecifier()
  {
    return valueSortRankSpecifier;
  }

  /**
   * Returns the child "StationAmp" element used to build this message
   * record.
   * @return The child "StationAmp" element used to build this message
   * record.
   */
  public Element getMsgRecChildElement()
  {
    return stationAmpElement;
  }

  /**
   * Returns the acceleration or velocity amplitude value (determined
   * via the 'selectAmplitudeType()' method).
   * @return The acceleration or velocity amplitude value, or 0.0 if
   * not available.
   */
  public double getAmpValue()
  {
    if(amplitudeTypeSelector == AMP_ACC_TYPE &&
                                     accAmplitudeRecObj.ampValueObj != null)
    {
      return accAmplitudeRecObj.ampValueObj.doubleValue();
    }
    if(amplitudeTypeSelector == AMP_VEL_TYPE &&
                                     velAmplitudeRecObj.ampValueObj != null)
    {
      return velAmplitudeRecObj.ampValueObj.doubleValue();
    }
    return 0.0;
  }

  /**
   * Returns a 'Double' object containing the acceleration or velocity
   * amplitude value (determined via the 'selectAmplitudeType()' method).
   * @return A 'Double' object containing the acceleration or velocity
   * amplitude value, or 'null' if not available.
   */
  public Double getAmpValueObj()
  {
    if(amplitudeTypeSelector == AMP_ACC_TYPE)
      return accAmplitudeRecObj.ampValueObj;
    if(amplitudeTypeSelector == AMP_VEL_TYPE)
      return velAmplitudeRecObj.ampValueObj;
    return null;
  }

  /**
   * Returns a display string representation of the acceleration or
   * velocity amplitude value (determined via the 'selectAmplitudeType()'
   * method).
   * @return A display string representation of the acceleration or
   * velocity amplitude value, or "??" if not available.
   */
  public String getAmpValueString()
  {
    if(amplitudeTypeSelector == AMP_ACC_TYPE)
      return accAmplitudeRecObj.ampValueStr;
    if(amplitudeTypeSelector == AMP_VEL_TYPE)
      return velAmplitudeRecObj.ampValueStr;
    return NO_VALUE_STR;
  }

  /**
   * Returns the component name for the acceleration or velocity
   * amplitude value (determined via the 'selectAmplitudeType()'
   * method).  The component with the maximum value is used.
   * @return The component name for the acceleration or velocity
   * amplitude value, or 'null' if none is available.
   */
  public String getCompName()
  {
    if(amplitudeTypeSelector == AMP_ACC_TYPE)
      return accAmplitudeRecObj.compNameStr;
    if(amplitudeTypeSelector == AMP_VEL_TYPE)
      return velAmplitudeRecObj.compNameStr;
    return null;
  }

  /**
   * Returns the component location for the acceleration or velocity
   * amplitude value (determined via the 'selectAmplitudeType()'
   * method).  The component with the maximum value is used.
   * @return The component location for the acceleration or velocity
   * amplitude value, or 'null' if none is available.
   */
  public String getCompLoc()
  {
    if(amplitudeTypeSelector == AMP_ACC_TYPE)
      return accAmplitudeRecObj.compLocStr;
    if(amplitudeTypeSelector == AMP_VEL_TYPE)
      return velAmplitudeRecObj.compLocStr;
    return null;
  }

  /**
   * Indicates whether some other object is "equal to" this one.
   * @param obj the reference object with which to compare.
   * @return <code>true</code> if this object is the same as the obj
   * argument; <code>false</code> otherwise.
   */
  public boolean equals(QWEventMsgRecord obj)
  {
    final QWStationAmpMsgRecord recObj;
    if(!(obj instanceof QWStationAmpMsgRecord))
      return false;     //if not a 'QWStationAmpMsgRecord' then return false
    recObj = (QWStationAmpMsgRecord)obj;
    return super.equals(obj) &&
        ((sta == null) ? (recObj.sta == null) : sta.equals(recObj.sta)) &&
        ((net == null) ? (recObj.net == null) : net.equals(recObj.net)) &&
        lat == recObj.lat && lon == recObj.lon &&
        ((time == null) ? (recObj.time == null) : time.equals(recObj.time)) &&
        ((stationName == null) ? (recObj.stationName == null) :
                                  stationName.equals(recObj.stationName)) &&
        ((elev == null) ? (recObj.elev == null) : elev.equals(recObj.elev)) &&
        ((instType == null) ? (recObj.instType == null) :
                                        instType.equals(recObj.instType)) &&
        ((source == null) ? (recObj.source == null) :
                                            source.equals(recObj.source)) &&
        ((commType == null) ? (recObj.commType == null) :
                                        commType.equals(recObj.commType)) &&
        ((accAmplitudeRecObj == null) ? (recObj.accAmplitudeRecObj == null) :
                    accAmplitudeRecObj.equals(recObj.accAmplitudeRecObj)) &&
        ((velAmplitudeRecObj == null) ? (recObj.velAmplitudeRecObj == null) :
                      velAmplitudeRecObj.equals(recObj.velAmplitudeRecObj));
  }

  /**
   * Indicates whether some other object is "equal to" this one.
   * @param obj the reference object with which to compare.
   * @return <code>true</code> if this object is the same as the obj
   * argument; <code>false</code> otherwise.
   */
  public boolean equals(Object obj)
  {
    return obj instanceof QWEventMsgRecord && equals((QWEventMsgRecord)obj);
  }

  /**
   * Returns true if the given object is a 'QWEventMsgRecord'
   * object whose message date/time equals the one for this record;
   * otherwise returns false.  The time values used are associated
   * with the acceleration or velocity amplitude value (determined
   * via the 'selectAmplitudeType()' method).
   * @param obj the 'QWEventMsgRecord' object to use.
   * @return true if the message date/times are equal.
   */
  public boolean timeEquals(QWEventMsgRecord obj)
  {
    final QWEventMsgRecord recObj;
    if(!(obj instanceof QWEventMsgRecord))
      return false;     //if not a 'QWEventMsgRecord' then return false
    recObj = (QWEventMsgRecord)obj;
    final Date timeObj;
    return recObj != null && (((timeObj=getTime()) == null) ?
               recObj.getTime() == null : timeObj.equals(recObj.getTime()));
  }

  /**
   * Returns true if the given object is a 'QWEventMsgRecord' object
   * whose message date/time equals the one for this record; otherwise
   * returns false.  The time values used are associated with the
   * acceleration or velocity amplitude value (determined via the
   * 'selectAmplitudeType()' method).
   * @param obj the 'QWEventMsgRecord' object to use.
   * @return true if the message date/times are equal.
   */
  public boolean timeEquals(Object obj)
  {
    return obj instanceof QWEventMsgRecord &&
                                          timeEquals((QWEventMsgRecord)obj);
  }

  /**
   * Compares this object with the specified object and index for order.
   * This method implements the ExtendedComparable interface.
   * @param   obj the Object to be compared.
   * @param   sortType the sort type.
   * @return  a negative integer, zero, or a positive integer as this object
   *		is less than, equal to, or greater than the specified object.
   * @throws ClassCastException if the specified object's type prevents it
   *         from being compared to this Object.
   */
  public int compareTo(Object obj, int sortType) throws ClassCastException
  {
    if(sortType == QWEventMsgRecord.VALUE_SORT_TYPE)
    {       //sort-type is "value" (amplitude value)
                      //get amplitude value for given record object:
      final Double givenAmpObj;
      if(obj != null)
      {  //given object not null
        if(!(obj instanceof QWEventMsgRecord))
          throw new ClassCastException();   //if wrong type then exception
              //get value-sort rank specifier for given object:
        final int givenRankSpec =
                        ((QWEventMsgRecord)obj).getValueSortRankSpecifier();
              //if value-sort rank specifiers different then return result:
        if(valueSortRankSpecifier != givenRankSpec)
          return (valueSortRankSpecifier < givenRankSpec) ? -1 : 1;
              //if amp object type then get amplitude value:
        givenAmpObj = (obj instanceof QWStationAmpMsgRecord) ?
                       ((QWStationAmpMsgRecord)obj).getAmpValueObj() : null;
      }
      else
        givenAmpObj = null;       //indicate no value available
      final int retVal;
            //if this record has no amplitude then return 0 if given
            // record also has no amplitude or -1 if it does:
      final Double ampValObj;
      if((ampValObj=getAmpValueObj()) == null)
        retVal = (givenAmpObj == null) ? 0 : -1;
      else if(givenAmpObj == null)  //if given record has no amplitude
        retVal = 1;                  // then return 1
      else            //compare amplitude-value objects
        retVal = ampValObj.compareTo(givenAmpObj);
           //if "not equal" then return result; otherwise
           // return result of "date" comparison:
      return (retVal != 0) ? retVal : compareTo(obj);
    }
            //sort-type not "value" (amplitude value)
    return compareTo(obj);           //return result of "date" comparison
  }

  /**
   * Compares the given 'QWEventMsgRecord' object to this one.
   * The event date/time is used as the sorting key.  The time values
   * used are associated with the acceleration or velocity amplitude
   * value (determined via the 'selectAmplitudeType()' method).
   * @param obj the 'QWEventMsgRecord' object to use.
   * @return A negative integer, zero, or a positive integer as this
   * object is less than, equal to, or greater than the specified object.
   * @throws ClassCastException if the specified object's type prevents it
   * from being compared to this Object.
   */
  public int compareTo(Object obj) throws ClassCastException
  {
    try
    {         //return comparison of date objects:
      return getTime().compareTo(((QWEventMsgRecord)obj).getTime());
    }
    catch(NullPointerException ex)
    {         //if this object's date object is null then return -1;
              // otherwise other object or date was null so return 1:
      return (getTime() == null) ? -1 : 1;
    }
  }

  /**
   * Returns a string representation of this record.  The acceleration
   * or velocity amplitude is used (determined via the
   * 'selectAmplitudeType()' method).
   * @return A string representation of date, time and amplitude for
   * this record, with 2-space separators surrounding each item.
   */
  public String toString()
  {
    return "  " + getDateTimeString() + "  " + getAmpValueString() + "  ";
  }

  /**
   * Returns a string representation of this record to be used for
   * its tooltip text.
   * @return A string representation of the SCNL, date, time and
   * amplitude for this record.
   */
  public String getToolTipString()
  {
    final String str;
    if(amplitudeTypeSelector == AMP_ACC_TYPE)
      str = "Acc=" + getAccAmpValueString();
    else if(amplitudeTypeSelector == AMP_VEL_TYPE)
      str = "Vel=" + getVelAmpValueString();
    else
      str = getAmpValueString();
    return getSCNLString() + "  " + getDateTimeString() + "  " + str;
  }

  /**
   * Returns a display string representation of this object.
   * @return A new String object.
   */
  public String getDisplayString()
  {
    final String compName = getCompName();
    final String compLoc = getCompLoc();
    return ((sta != null) ? sta : UtilFns.EMPTY_STRING) + CSEP_STR +
         ((compName != null) ? compName : UtilFns.EMPTY_STRING) + CSEP_STR +
                   ((net != null) ? net : UtilFns.EMPTY_STRING) + CSEP_STR +
           ((compLoc != null) ? compLoc : UtilFns.EMPTY_STRING) + CSEP_STR +
                     getLatitudeString() + CSEP_STR + getLongitudeString() +
                                            CSEP_STR + getDateTimeString() +
                                CSEP_STR + "acc=" + getAccAmpValueString() +
                                     ((accAmplitudeRecObj.timeOffsMs != 0) ?
                           (" (toff="+accAmplitudeRecObj.timeOffsMs+"ms)") :
                                                     UtilFns.EMPTY_STRING) +
                                CSEP_STR + "vel=" + getVelAmpValueString() +
                                     ((velAmplitudeRecObj.timeOffsMs != 0) ?
                           (" (toff="+velAmplitudeRecObj.timeOffsMs+"ms)") :
                                                      UtilFns.EMPTY_STRING);
  }

  /**
   * Returns a string containing the station, channel, network,
   * and location codes for the record.
   * @return A String in the form:  "sta,cha,net,loc".
   */
  public String getSCNLString()
  {
    if(amplitudeTypeSelector == AMP_ACC_TYPE)
      return accSCNLString;
    if(amplitudeTypeSelector == AMP_VEL_TYPE)
      return velSCNLString;
    return UtilFns.EMPTY_STRING;
  }

  /**
   * Returns a string containing the network, station, channel,
   * location and time codes for the record along with the
   * acceleration and velocity amplitude values.  This string
   * may be used as a unique "key" for the amplitude record.
   * @return A String in the form:  sta,cha,net,loc,time,acc,vel.
   */
  public String getEventIDKey()
  {
    return keyNscltValsString;
  }

  /**
   * Returns a lower-cased string containing the network, station,
   * channel, location and time codes for the record along with the
   * acceleration and velocity amplitude values.  This string
   * may be used as a unique "key" for the amplitude record.
   * @return A String in the form:  sta,cha,net,loc,time,acc,vel.
   */
  public String getLCEventIDKey()
  {
    return keyNscltValsString.toLowerCase();
  }

  /**
   * Returns the data source description string.
   * @return The data source description string.
   */
  public String getDataSource()
  {
    return source;
  }

  /**
   * Returns the network ID.
   * @return the network ID.
   */
  public String getNetID()
  {
    return net;
  }

  /**
   * Returns a display string representation of the date (not including
   * the time) associated with the acceleration or velocity amplitude
   * value (determined via the 'selectAmplitudeType()' method).
   * @return A display string representation of the date (not including
   * the time) associated with the acceleration or velocity amplitude
   * value.
   */
  public String getDateString()
  {
    final Date timeObj;
    if((timeObj=getTime()) == null)
      return NO_VALUE_STR;
    synchronized(dateFormatterObj)
    {    //only allow thread at a time to use date-format object
      return dateFormatterObj.format(timeObj);
    }
  }

  /**
   * Returns a display string representation of the date/time
   * associated with the acceleration or velocity amplitude
   * value (determined via the 'selectAmplitudeType()' method).
   * @return A display string representation of the date/time
   * associated with the acceleration or velocity amplitude
   * value.
   */
  public String getDateTimeString()
  {
    final Date timeObj;
    if((timeObj=getTime()) == null)
      return NO_VALUE_STR;
    synchronized(dateTimeFormatterObj)
    {    //only allow thread at a time to use date-format object
      return dateTimeFormatterObj.format(timeObj);
    }
  }

  /**
   * Returns a display string representation of the time (not including
   * the date) associated with the acceleration or velocity amplitude
   * value (determined via the 'selectAmplitudeType()' method).
   * @return A display string representation of the time (not including
   * the date) associated with the acceleration or velocity amplitude
   * value.
   */
  public String getTimeString()
  {
    final Date timeObj;
    if((timeObj=getTime()) == null)
      return NO_VALUE_STR;
    final String str;
    synchronized(timeFormatterObj)
    {    //only allow thread at a time to use date-format object
      str = timeFormatterObj.format(timeObj);
    }
         //trim so that only 2 digits of fractional seconds are returned:
    final int len = str.length();
    int p;              //make sure that format is as expected:
    if((p=str.lastIndexOf('.')) > 0 && p + 3 < len)
      return str.substring(0,p+3) + str.substring(p+4);
    return str;
  }

  /**
   * Returns the time value associated with the acceleration or velocity
   * amplitude value (determined via the 'selectAmplitudeType()'
   * method).
   * @return A 'Date' object containing the time value associated with
   * the acceleration or velocity amplitude value.
   */
  public Date getTime()
  {
    if(amplitudeTypeSelector == AMP_ACC_TYPE &&
                                      accAmplitudeRecObj.ampTimeObj != null)
    {
      return accAmplitudeRecObj.ampTimeObj;
    }
    else if(amplitudeTypeSelector == AMP_VEL_TYPE &&
                                      velAmplitudeRecObj.ampTimeObj != null)
    {
      return velAmplitudeRecObj.ampTimeObj;
    }
    return time;
  }

  /**
   * Returns the time value for the "StationAmp" message.
   * @return A 'Date' object containing the time value for the
   * "StationAmp" message.
   */
  public Date getMessageTime()
  {
    return time;
  }

  /**
   * Returns the latitude.
   * @return the latitude.
   */
  public double getLatitude()
  {
    return lat;
  }

  /**
   * Returns the longitude.
   * @return the longitude.
   */
  public double getLongitude()
  {
    return lon;
  }

  /**
   * Returns a display string representation of the latitude.
   * @param nsFormatFlag true for N/S format; false for +/- format.
   * @param degMinFlag true for degrees/minutes format; false for
   * decimal format.
   * @return A display string.
   */
  public String getLatitudeString(boolean nsFormatFlag, boolean degMinFlag)
  {
    if(degMinFlag)
    {    //use degrees/minutes format
      if(nsFormatFlag)
      {  //use N/S format
        return (lat >= 0.0) ? (QWUtils.getDegMinStr(lat) + N_STR) :
                                  (QWUtils.getDegMinStr(-lat) + S_STR);
      }
      else  //use +/- format
        return QWUtils.getDegMinStr(lat);
    }
    else
    {    //use decimal format
      synchronized(latLonDigitNumberFormatter)
      {  //only allow thread at a time to use number-format object
        if(nsFormatFlag)
        {     //use N/S format
          return (lat >= 0.0) ?
                          (latLonDigitNumberFormatter.format(lat) + N_STR) :
                          (latLonDigitNumberFormatter.format(-lat) + S_STR);
        }
        else  //use +/- format
          return latLonDigitNumberFormatter.format(lat);
      }
    }
  }

  /**
   * Returns a display string representation of the latitude.  The
   * decimal format is used.
   * @param nsFormatFlag true for N/S format; false for +/- format.
   * @return A display string.
   */
  public String getLatitudeString(boolean nsFormatFlag)
  {
    return getLatitudeString(nsFormatFlag,false);
  }

  /**
   * Returns a display string representation of the latitude.  The decimal
   * +/- format is used.
   * @return A display string.
   */
  public String getLatitudeString()
  {
    return getLatitudeString(false,false);
  }

  /**
   * Returns a display string representation of the longitude.
   * @param ewFormatFlag true for E/W format; false for +/- format.
   * @param degMinFlag true for degrees/minutes format; false for
   * decimal format.
   * @return A display string.
   */
  public String getLongitudeString(boolean ewFormatFlag, boolean degMinFlag)
  {
    if(degMinFlag)
    {    //use degrees/minutes format
      if(ewFormatFlag)
      {  //use E/W format
        return (lon >= 0.0) ? (QWUtils.getDegMinStr(lon) + E_STR) :
                                 (QWUtils.getDegMinStr(-lon) + W_STR);
      }
      else  //use +/- format
        return QWUtils.getDegMinStr(lon);
    }
    else
    {    //use decimal format
      synchronized(latLonDigitNumberFormatter)
      {  //only allow thread at a time to use number-format object
        if(ewFormatFlag)
        {     //use E/W format
          return (lon >= 0.0) ?
                          (latLonDigitNumberFormatter.format(lon) + E_STR) :
                          (latLonDigitNumberFormatter.format(-lon) + W_STR);
        }
        else  //use +/- format
          return latLonDigitNumberFormatter.format(lon);
      }
    }
  }

  /**
   * Returns a display string representation of the longitude.  The
   * decimal format is used.
   * @param ewFormatFlag true for E/W format; false for +/- format.
   * @return A display string.
   */
  public String getLongitudeString(boolean ewFormatFlag)
  {
    return getLongitudeString(ewFormatFlag,false);
  }

  /**
   * Returns a display string representation of the longitude.  The
   * decimal +/- format is used.
   * @return A display string.
   */
  public String getLongitudeString()
  {
    return getLongitudeString(false,false);
  }

  /**
   * Returns a display string representation of the amplitude
   * acceleration value.
   * @return A display string representation of the amplitude
   * acceleration value, or "??" if not available.
   */
  public String getAccAmpValueString()
  {
    return accAmplitudeRecObj.ampValueStr;
  }

  /**
   * Returns a display string representation of the velocity
   * acceleration value.
   * @return A display string representation of the amplitude
   * velocity value, or "??" if not available.
   */
  public String getVelAmpValueString()
  {
    return velAmplitudeRecObj.ampValueStr;
  }

  /**
   * Sets the handle to the record that this record superseded.
   * @param recObj handle to record that this record superseded, or
   * null for none.
   */
  public void setSupersededRecordObj(QWEventMsgRecord recObj)
  {
    supersededRecordObj = recObj;
  }

  /**
   * Returns the handle to the record that this record superseded.
   * @return The handle to the record that this record superseded, or
   * null for none.
   */
  public QWEventMsgRecord getSupersededRecordObj()
  {
    return supersededRecordObj;
  }

  /**
   * Sets the time that this message record was last selected.
   * @param timeVal time value, in milliseconds since 1/1/1970.
   */
  public void setSelectedTimeVal(long timeVal)
  {
    selectedTimeVal = timeVal;
  }

  /**
   * Returns the time that this message record was last selected.
   * @return The time that this message record was last selected (in
   * milliseconds since 1/1/1970), or 0 if this record was never
   * selected or if this method is not implemented.
   */
  public long getSelectedTimeVal()
  {
    return selectedTimeVal;
  }

  /**
   * Sets the "playback time" for this amplitude record.
   * @param dateObj time/date object to use.
   */
  public void setPlaybackTimeDateObj(Date dateObj)
  {
    synchronized(playbackTimeSyncObj)
    {    //only allow one thread at a time
      playbackTimeDateObj = dateObj;
    }
  }

  /**
   * Returns the "playback time" for this amplitude record.
   * @return The "playback time" for this amplitude record.
   */
  public Date getPlaybackTimeDateObj()
  {
    synchronized(playbackTimeSyncObj)
    {    //only allow one thread at a time
      return playbackTimeDateObj;
    }
  }

  /**
   * Returns the version code string.  For 'QWStationAmpMsgRecord' the
   * value 'null' is always returned.
   * @return null.
   */
  public String getVersion()
  {
    return null;
  }

  /**
   * Returns a 'QWProductMsgRecord' object for this record.  For
   * 'QWStationAmpMsgRecord' the value 'null' is always returned.
   * @param typeStr the 'type' value of the 'QWProductMsgRecord' object
   * to be returned.
   * @return null.
   */
  public QWProductMsgRecord getProductMsgRecord(String typeStr)
  {
    return null;
  }

  /**
   * Returns the table of 'QWProductMsgRecord' objects for products
   * associated with this record.  For 'QWStationAmpMsgRecord' the
   * value 'null' is always returned.
   * @return null.
   */
  public FifoHashtable getProductRecTable()
  {
    return null;
  }

  /**
   * Returns the number of products associated with this record.  For
   * 'QWStationAmpMsgRecord' the value 0 is always returned.
   * @return 0.
   */
  public int getProductCount()
  {
    return 0;
  }

  /**
   * Returns the ShakeMap information.  For 'QWStationAmpMsgRecord' the
   * value 'null' is always returned.
   * @return null.
   */
  public String getShakeMapInfo()
  {
    return null;
  }

  /**
   * Returns a flag indicating whether or not this event contains a
   * ShakeMap product.  For 'QWStationAmpMsgRecord' the value 'false'
   * is always returned.
   * @return false.
   */
  public boolean containsShakeMapProduct()
  {
    return false;
  }

  /**
   * Returns a flag indicating whether or not this event contains a
   * Tsunami product.  For 'QWStationAmpMsgRecord' the value 'false'
   * is always returned.
   * @return false.
   */
  public boolean containsTsunamiProduct()
  {
    return false;
  }

  /**
   * Returns the event magnitude.  For 'QWStationAmpMsgRecord' the value
   * 0.0 is always returned.
   * @return 0.0.
   */
  public double getMagnitude()
  {
    return 0.0;
  }

  /**
   * Returns a 'Double' object containing the event magnitude.  For
   * 'QWStationAmpMsgRecord' the value 'null' is always returned.
   * @return null.
   */
  public Double getMagnitudeObj()
  {
    return null;
  }

  /**
   * Returns a display string representation of the magnitude.  For
   * 'QWStationAmpMsgRecord' the value "??" is always returned.
   * @return "??".
   */
  public String getMagnitudeValueString()
  {
    return NO_VALUE_STR;
  }

  /**
   * Returns a string representing the type code for the primary
   * magnitude.  For 'QWStationAmpMsgRecord' the value 'null' is
   * always returned.
   * @return null.
   */
  public String getMagnitudeType()
  {
    return null;
  }

  /**
   * Returns the event depth value.  For 'QWStationAmpMsgRecord' the
   * value 'null' is always returned.
   * @return null.
   */
  public Double getDepthObj()
  {
    return null;
  }

  /**
   * Returns a display string representation of the depth.  For
   * 'QWStationAmpMsgRecord' the value "??" is always returned.
   * @param milesFlag true to convert the depth value from kilometers
   * to miles; false to leave the depth value unchanged.
   * @return "??".
   */
  public String getDepthString(boolean milesFlag)
  {
    return NO_VALUE_STR;
  }

  /**
   * Returns a display string representation of the depth.  For
   * 'QWStationAmpMsgRecord' the value "??" is always returned.
   * @return "??".
   */
  public String getDepthString()
  {
    return getDepthString(false);
  }

  /**
   * Returns true if the message is verified.  For 'QWStationAmpMsgRecord'
   * the value 'false' is always returned.
   * @return false.
   */
  public boolean isVerified()
  {
    return false;
  }

  /**
   * Returns true if the "quarry" flag is set.  For 'QWStationAmpMsgRecord'
   * the value 'false' is always returned.
   * @return false.
   */
  public boolean isQuarry()
  {
    return false;
  }

  /**
   * Updates time zone used for date/time values for all
   * event-message-record objects.
   * @param zone time zone.
   */
  public static void updateDateTimeZones(TimeZone zone)
  {
         //set time zone for date formatters:
    synchronized(dateTimeFormatterObj)
    {
      dateTimeFormatterObj.setTimeZone(zone);
    }
    synchronized(dateFormatterObj)
    {
      dateFormatterObj.setTimeZone(zone);
    }
    synchronized(timeFormatterObj)
    {
      timeFormatterObj.setTimeZone(zone);
    }
  }


  /**
   * Class AmplitudeRecord contains a record of amplitude information.
   */
  public static class AmplitudeRecord
  {
    public final Double ampValueObj;
    public final String ampValueStr;
    public final int timeOffsMs;
    public final Date ampTimeObj;
    public final String ampUnitsStr;
    public final String compNameStr;
    public final String compLocStr;
         /** Empty amplitude information object. */
    public static final AmplitudeRecord emptyAmpCompInfoObj =
                                                      new AmplitudeRecord();

    /**
     * Creates an amplitude record.
     * @param ampValueObj amplitude value.
     * @param timeOffsMs time offset for amplitude, in milliseconds.
     * @param ampTimeObj time value for amplitude.
     * @param ampUnitsStr units string for amplitude, or null for none.
     * @param compNameStr component name for amplitude.
     * @param compLocStr component location for amplitude, or null for none.
     */
    public AmplitudeRecord(Double ampValueObj,int timeOffsMs,
                                        Date ampTimeObj, String ampUnitsStr,
                                      String compNameStr, String compLocStr)
    {
      this.ampValueObj = ampValueObj;
      this.timeOffsMs = timeOffsMs;
      this.ampTimeObj = ampTimeObj;
      this.ampUnitsStr = ampUnitsStr;
      this.compNameStr = compNameStr;
      this.compLocStr = compLocStr;
         //setup string version value:
      synchronized(amplitudeNumberFormatter)
      {  //only allow thread at a time to use number-format object
        ampValueStr = (ampValueObj != null) ?    //set str ver of value
                amplitudeNumberFormatter.format(ampValueObj) : NO_VALUE_STR;
      }
    }

    /**
     * Creates an empty amplitude record object.
     */
    public AmplitudeRecord()
    {
      this(null,0,null,null,null,null);
    }

    /**
     * Indicates whether some other object is "equal to" this one.
     * @param ampRecObj the reference object with which to compare.
     * @return <code>true</code> if this object is equal to the obj
     * argument; <code>false</code> otherwise.
     */
    public boolean equals(AmplitudeRecord ampRecObj)
    {
      return ((ampValueObj == null) ? (ampRecObj.ampValueObj == null) :
                               ampValueObj.equals(ampRecObj.ampValueObj)) &&
                                       timeOffsMs == ampRecObj.timeOffsMs &&
                    ((ampTimeObj == null) ? (ampRecObj.ampTimeObj == null) :
                                 ampTimeObj.equals(ampRecObj.ampTimeObj)) &&
                  ((ampUnitsStr == null) ? (ampRecObj.ampUnitsStr == null) :
                               ampUnitsStr.equals(ampRecObj.ampUnitsStr)) &&
                  ((compNameStr == null) ? (ampRecObj.compNameStr == null) :
                               compNameStr.equals(ampRecObj.compNameStr)) &&
                    ((compLocStr == null) ? (ampRecObj.compLocStr == null) :
                                   compLocStr.equals(ampRecObj.compLocStr));
    }

    /**
     * Indicates whether some other object is "equal to" this one.
     * @param obj the reference object with which to compare.
     * @return <code>true</code> if this object is equal to the obj
     * argument; <code>false</code> otherwise.
     */
    public boolean equals(Object obj)
    {
      return (obj instanceof AmplitudeRecord) ?
                                       equals((AmplitudeRecord)obj) : false;
    }
  }
}
