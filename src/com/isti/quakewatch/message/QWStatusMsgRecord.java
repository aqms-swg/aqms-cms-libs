//QWStatusMsgRecord.java:  Manages a status-message record.
//
//  3/31/2004 -- [ET]  Extracted from 'QWMessageHandler' class; added
//                     'receivedTimeMs' field.
//

package com.isti.quakewatch.message;

import java.util.Date;
import org.jdom.Element;
import com.isti.quakewatch.common.MsgTag;

/**
 * Class QWStatusMsgRecord manages a status-message record.
 */
public class QWStatusMsgRecord extends QWRecord
{
    /** QWmessage 'Element' object used to construct this object. */
  public final Element qwMsgElement;
    /** Message number from the 'QWmessage' element for this message. */
  public final long msgNumber;
    /** Time that message was created by server (or null if not given). */
  public final Date timeGenerated;
    /** StatusMessage 'Element' object used to construct this object. */
  public final Element statusMsgElement;
    /** Type string for message. */
  public final String msgType;
    /** Optional Data string for message, or null if no data. */
  public final String msgData;
    /** Local time that the message was received. */
  public final long receivedTimeMs;

  /**
   * Creates a record of data from a QuakeWatch XML StatusMessage.
   * @param receivedTimeMs local time that the message was received.
   * @param qwMsgElement the XML "QWmessage" element object.
   * @param statusMsgElement the XML "StatusMessage" element object.
   * @throws QWRecordException if an error occurs while creating
   * the data record.
   */
  public QWStatusMsgRecord(long receivedTimeMs,Element qwMsgElement,
                        Element statusMsgElement) throws QWRecordException
  {
    super(qwMsgElement);                       //construct parent
    this.receivedTimeMs = receivedTimeMs;      //save local received time
    this.qwMsgElement = qwMsgElement;          //save handle to element
    this.statusMsgElement = statusMsgElement;  //save handle to element
    if(statusMsgElement == null)
      throw new QWRecordException("Null element object handle");
    if(!MsgTag.QW_MESSAGE.equalsIgnoreCase(qwMsgElement.getName()))
    {    //root element name not "QWmessage"
      throw new QWRecordException("Element tag name \"" +
                                      MsgTag.QW_MESSAGE + "\" not found");
    }
    if(!MsgTag.STATUS_MESSAGE.equalsIgnoreCase(statusMsgElement.getName()))
    {    //root element name not "StatusMessage"
      throw new QWRecordException("Element tag name \"" +
                                  MsgTag.STATUS_MESSAGE + "\" not found");
    }
            //fetch and save message number:
    msgNumber = getAttribLong(MsgTag.MSG_NUMBER);
            //fetch and save time of message creation at server:
    timeGenerated = getOptAttribTime(MsgTag.TIME_GENERATED);

    currentElement = statusMsgElement;     //set current element
         //fetch and save MsgType string for message:
    msgType = getAttribStr(MsgTag.MSG_TYPE);
         //fetch and save optional MsgData string for message:
    msgData = getOptAttribStr(MsgTag.MSG_DATA);
  }

  /**
   * Returns the difference between the given local time that the
   * message was received and the time-generated value for the message.
   * @return The time difference, in milliseconds.
   */
  public long getReceivedTimeOffsetMs()
  {
    return receivedTimeMs - timeGenerated.getTime();
  }
}
