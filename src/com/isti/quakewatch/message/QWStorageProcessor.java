//QWStorageProcessor.java:  Defines a storage-processing method.
//
//  3/18/2003 -- [ET]
//

package com.isti.quakewatch.message;

/**
 * Interface QWStorageProcessor defines a storage-processing method.
 */
public interface QWStorageProcessor
{
  /**
   * Stores the given "QWmessage" element object.
   * @param recObj the record object holding the "QWmessage" element.
   */
  public void storeRecord(QWMsgRecord recObj);
}
