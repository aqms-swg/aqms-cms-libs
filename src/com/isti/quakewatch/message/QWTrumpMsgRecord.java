//QWTrumpMsgRecord.java:  Defines a record of data for one
//                          trump-event.
//
//  6/18/2003 -- [KF]  Initial version.
// 10/24/2003 -- [ET]  Modified (via import) to reference 'QWCommon'
//                     version of  'QWRecordException' instead of
//                     local version.
//  3/18/2004 -- [ET]  Moved from 'QWClient' to 'QWCommon' project.
//   9/7/2006 -- [ET]  Added support for for ANSS-EQ-XML format.
//  9/29/2006 -- [ET]  Added 'getCommentStr()' method.
//   4/2/2010 -- [ET]  Added support for QuakeML format.
//

package com.isti.quakewatch.message;

import org.jdom.Element;
import com.isti.util.UtilFns;
import com.isti.quakewatch.common.MsgTag;

/**
 * QWTrumpMsgRecord defines a record of data for one trump-event.
 */
public class QWTrumpMsgRecord extends QWIdentDataMsgRecord
{
    /** Handle to 'Element' object used to construct this object. */
  public final Element eventElement;
    /** Message type string set by QWServer. */
  public final String type;
    /** Message type code string received from feeder module. */
  public final String msgTypeCode;

  /**
   * Creates a data record for one trump-event, built from an XML
   * "Trump" message element.
   * @param qwMsgElement the XML "QWmessage" element object.
   * @param dataMsgElement the XML "DataMessage" element object.
   * @param eventElement the XML "Event" message element.
   * @param messageFormatSpec specifier for message-format type (one of
   * the 'QWIdentDataMsgRecord.MFMT_...' values).
   * @throws QWRecordException if an error occurs while creating
   * the data record.
   */
  public QWTrumpMsgRecord(Element qwMsgElement, Element dataMsgElement,
                                Element eventElement, int messageFormatSpec)
                                                    throws QWRecordException
  {
    super(qwMsgElement,dataMsgElement,eventElement,messageFormatSpec);
    this.eventElement = eventElement;   //save handle to element
    switch(messageFormatSpec)
    {  //process data depending on format specification
      case MFMT_QUAKEML:     //data is QuakeML message format
        if(!MsgTag.USE_CONTROL.equalsIgnoreCase(eventElement.getName()))
        {  //root element name not "useControl"
          throw new QWRecordException("Element tag name \"" +
                                       MsgTag.USE_CONTROL + "\" not found");
        }
              //get optional "type" child-element of "useControl" element:
        type = getOptElementStr(MsgTag.QUAKEML_TYPE);
              //set no "MsgTypeCode" value:
        msgTypeCode = UtilFns.EMPTY_STRING;
        break;

      case MFMT_ANSSEQXML:   //data is ANSS-EQ-XML message format
        if(!MsgTag.EVENT.equalsIgnoreCase(eventElement.getName()))
        {  //root element name not "Event"
          throw new QWRecordException("Element tag name \"" +
                                             MsgTag.EVENT + "\" not found");
        }
              //get "Type" child-element of "Event" element:
        type = getOptElementStr(MsgTag.TYPE);
              //set no "MsgTypeCode" value:
        msgTypeCode = UtilFns.EMPTY_STRING;
        break;

      default:               //data is QuakeWatch message format
        if(!MsgTag.EVENT.equalsIgnoreCase(eventElement.getName()))
        {  //root element name not "Event"
          throw new QWRecordException("Element tag name \"" +
                                             MsgTag.EVENT + "\" not found");
        }
              //get "Type" attribute:
        type = getAttribStr(MsgTag.TYPE);
              //get optional "MsgTypeCode" attribute:
        msgTypeCode = getNonNullOptAttribStr(MsgTag.MSG_TYPE_CODE);
    }
  }

  /**
   * Creates a data record for one trump-event, built from an XML
   * "Trump" message element.
   * @param qwMsgElement the XML "QWmessage" element object.
   * @param dataMsgElement the XML "DataMessage" element object.
   * @param eventElement the XML "Event" message element.
   * @param anssEQMsgFormatFlag true for ANSS-EQ-XML message format;
   * false for QuakeWatch message format.
   * @throws QWRecordException if an error occurs while creating
   * the data record.
   * @deprecated Use version with 'messageFormatSpec' parameter.
   */
  public QWTrumpMsgRecord(Element qwMsgElement, Element dataMsgElement,
                          Element eventElement, boolean anssEQMsgFormatFlag)
                                                    throws QWRecordException
  {
    this(qwMsgElement,dataMsgElement,eventElement,
                   (anssEQMsgFormatFlag ? MFMT_ANSSEQXML : MFMT_QWMESSAGE));
  }

  /**
   * Creates a data record for one trump-event, built from a QuakeWatch
   * XML format "Trump" message element.
   * @param qwMsgElement the XML "QWmessage" element object.
   * @param dataMsgElement the XML "DataMessage" element object.
   * @param eventElement the XML "Event" message element.
   * @throws QWRecordException if an error occurs while creating
   * the data record.
   */
  public QWTrumpMsgRecord(Element qwMsgElement, Element dataMsgElement,
                            Element eventElement) throws QWRecordException
  {
    this(qwMsgElement,dataMsgElement,eventElement,false);
  }

  /**
   * Indicates whether some other object is "equal to" this one.
   * @param   obj   the reference object with which to compare.
   * @return  <code>true</code> if this object is the same as the obj
   *          argument; <code>false</code> otherwise.
   */
  public boolean equals(QWTrumpMsgRecord obj)
  {
    return super.equals(obj) &&
              ((type == null) ? obj.type == null : type.equals(obj.type)) &&
                          ((msgTypeCode == null) ? obj.msgTypeCode == null :
                                       msgTypeCode.equals(obj.msgTypeCode));
  }

  /**
   * Indicates whether some other object is "equal to" this one.
   * @param   obj   the reference object with which to compare.
   * @return  <code>true</code> if this object is the same as the obj
   *          argument; <code>false</code> otherwise.
   */
  public boolean equals(Object obj)
  {
    return obj instanceof QWTrumpMsgRecord && equals((QWTrumpMsgRecord)obj);
  }

  /**
   * Returns a string representation of this class.
   * @return String object.
   */
  public String toString()
  {
    return "eventIDKey=" + this.eventIDKey + ", type=" + this.type +
                                        ", msgTypeCode=" + this.msgTypeCode;
  }

  /**
   * Returns the comment text from this message.
   * @return The comment text string from this message, or null if none
   * available.
   */
  public String getCommentStr()
  {
    switch(messageFormatSpec)
    {  //process data depending on format specification
      case MFMT_QUAKEML:     //data is QuakeML message format
        return getChildElementStr(     //return value of 'comment'|'text'
                                MsgTag.QUAKEML_COMMENT,MsgTag.QUAKEML_TEXT);

      case MFMT_ANSSEQXML:   //data is ANSS-EQ-XML message format
        final Element commentElem;
        if((commentElem=currentElement.getChild(
                     MsgTag.COMMENT,currentElement.getNamespace())) == null)
        {  //no "Comment" child-element found
          return null;
        }
        return commentElem.getChildTextTrim(
                                    MsgTag.TEXT,commentElem.getNamespace());

      default:               //data is QuakeWatch message format
        return getOptAttribStr(MsgTag.COMMENT);
    }
  }
}
