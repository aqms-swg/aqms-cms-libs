package com.isti.quakewatch.util;

/**
 * Interface ConnLoginRejectCallBack defines the call-back method invoked
 * when a connection login attempt is rejected by the server.
 */
public interface ConnLoginRejectCallBack
{

  /**
   * Method invoked when a connection login attempt is
   * rejected by the server.  This method implements the
   * 'ConnLoginRejectCallBack' interface.
   * @param msgStr a message-string describing the status
   * of the current connection to the server.
   */
  public void connLoginAttemptRejected(String msgStr);
}
