//ConnStatusInterface.java:  Defines access to a panel for showing the
//                           ongoing status of a connection.
//
//  9/26/2003 -- [ET]  Converted from 'ConnectionStatusPanel.java'.
//  11/9/2010 -- [ET]  Removed unused 'import java.awt.Color'.
//

package com.isti.quakewatch.util;

/**
 * Class ConnStatusInterface defines access to a panel for showing the
 * ongoing status of a connection.
 */
public interface ConnStatusInterface
{
  public static final int GREEN_COLOR_IDX = 4;   //clrs for 'setData()'
  public static final int YELLOW_COLOR_IDX = 3;
  public static final int RED_COLOR_IDX = 2;
  public static final int CLEAR_COLOR_IDX = 1;
  public static final int NULL_COLOR_IDX = 0;

  /**
   * Sets the message strings for this panel and the popup dialog,
   * as well as the color for the symbol on the panel.
   * @param textStr text for panel, or null for no change.
   * @param popupStr text for popup, or null for no change.
   * @param symColorIdx index of color for symbol (one of the
   * '..._COLOR_IDX' values), or 'NULL_COLOR_IDX' for no change.
   */
  public void setData(String textStr,String popupStr,int symColorIdx);

  /**
   * Sets the message strings for this panel and the popup dialog.
   * @param textStr text for panel, or null for no change.
   * @param popupStr text for popup, or null for no change.
   */
  public void setData(String textStr,String popupStr);

  /**
   * Sets the component that is to receive focus after the popup dialog
   * is cleared.
   * @param compObj the 'Component' object to receive focus.
   */
  public void setRefocusComponent(Object compObj);

  /**
   * Displays the popup dialog showing extra status information.
   * The dialog is non-modal.
   */
  public void showPopupDialog();

  /**
   * Displays the popup dialog showing extra status information in response
   * to a user request.  The dialog is non-modal.
   */
  public void userRequestShowPopupDialog();

  /**
   * Hides the popup dialog.
   */
  public void hidePopupDialog();

  /**
   * Clears any displayed popup dialog if the user has not requested
   * that it be shown.
   */
  public void autoClearPopupDialog();
}
