//ConnectRejectCallBack.java:  Defines the call-back method invoked
//                             when a connect attempt is rejected by
//                             the server.
//
//  11/3/2004 -- [ET]
//

package com.isti.quakewatch.util;

/**
 * Interface ConnectRejectCallBack defines the call-back method invoked
 * when a connect attempt is rejected by the server.
 */
public interface ConnectRejectCallBack
{
  /**
   * Call-back method invoked when a connect attempt is rejected by
   * the server.
   * @param connStatusVal a value indicating the status of the current
   * connection to the server, one of the 'QWServices.CS_' values.
   * @param connStatusStr a message-string describing the status of
   * the current connection to the server.
   */
  public void connectAttemptRejected(int connStatusVal,
                                                      String connStatusStr);
}
