//CubeCodeTranslator.java:  Translates message-codes for the CUBE
//                          message format.
//
//  8/10/2006 -- [ET]  Initial version.
//  10/2/2006 -- [ET]  Changed order of items in 'locMethodCodeXlatArr[]'
//                     array so that 'toCubeLocMethod("Hypoinverse")'
//                     will return 'H' (not 'L'); changed order of items
//                     in 'magTypeCodeXlatArr[]' array so that
//                     'toCubeMagType("Mb")' will return 'B' (not 'P').
// 11/11/2009 -- [ET]  Added 'CUBE_LOCMETHZ_STR' and 'CUBE_LOCMETHQ_STR'
//                     fields.
//

package com.isti.quakewatch.util;

import java.util.HashMap;
import com.isti.util.TwoStringBlock;

/**
 * Class CubeCodeTranslator translates message-codes for the CUBE
 * message format.
 */
public class CubeCodeTranslator
{
         //magnitude type strings:
  public static final String MB_STR = "Mb";
  public static final String MD_STR = "Md";
  public static final String ME_STR = "Me";
  public static final String ML_STR = "Ml";
  public static final String MI_STR = "Mi";
  public static final String MBLG_STR = "MbLg";
  public static final String MW_STR = "Mw";
  public static final String MS_STR = "Ms";
  public static final String MT_STR = "Mt";

         //location-method Algorithm strings:
  public static final String BINDER_STR = "Binder";
  public static final String ANTELOPE_STR = "Antelope";
  public static final String NONNEIC_STR = "NonNEIC-furnished";
  public static final String HYPO_STR = "Hypoinverse";
  public static final String MACRO_STR = "Macroseismic";
  public static final String HYDRA_STR = "Hydra";

         //location-method Class strings:
  public static final String UNKNOWN_STR = "Unknown";
  public static final String JOHNSON_STR = "Carol Johnson stack";
  public static final String GEIGER_STR =
                                        "Geiger's Method and Least Squares";
  public static final String REPORTED_STR = "Reported";

    /** Magnitude-type code translation array. */
  public static final String [][] magTypeCodeXlatArr = new String [][]
                   { {"C", MD_STR },
                     {"D", MD_STR },
                     {"E", ME_STR },
                     {"G", ML_STR },
                     {"I", MI_STR },
                     {"L", ML_STR },
                     {"N", MBLG_STR },
                     {"O", MW_STR },
                     {"P", MB_STR },
                     {"B", MB_STR },
                     {"S", MS_STR },
                     {"T", MT_STR },
                     {"W", MW_STR } };

    /** Location-method code translation array. */
  public static final String [][] locMethodCodeXlatArr = new String [][]
                   { {"A", BINDER_STR, JOHNSON_STR },
                     {"D", ANTELOPE_STR, GEIGER_STR },
                     {"F", NONNEIC_STR, UNKNOWN_STR },
                     {"L", HYPO_STR, GEIGER_STR },
                     {"H", HYPO_STR, GEIGER_STR },
                     {"M", MACRO_STR, REPORTED_STR },
                     {"R", HYDRA_STR, GEIGER_STR } };

    /** CUBE location-method code 'Z', for 'Scope'=="Internal". */
  public static final String CUBE_LOCMETHZ_STR = "Z";

    /** CUBE location-method code 'Q', for "possible quarry explosion". */
  public static final String CUBE_LOCMETHQ_STR = "Q";

    /** From-CUBE translation table for magnitude type. */
  public static final HashMap fromCubeMagTypeXlatTable =
                                           createFromCubeMagTypeXlatTable();

    /** To-CUBE translation table for magnitude type. */
  public static final HashMap toCubeMagTypeXlatTable =
                                             createToCubeMagTypeXlatTable();

    /** From-CUBE translation table for location method. */
  public static final HashMap fromCubeLocMethodXlatTable =
                                         createFromCubeLocMethodXlatTable();

    /** To-CUBE translation table for location method. */
  public static final HashMap toCubeLocMethodXlatTable =
                                           createToCubeLocMethodXlatTable();


    //private constructor so that no object instances may be created
    // (static access only)
  private CubeCodeTranslator()
  {
  }

  /**
   * Translates a CUBE-format magnitude-type code to a magnitude-type
   * string.
   * @param magCodeStr CUBE-format magnitude-type code.
   * @return Translated magnitude-type string, or null if code not matched.
   */
  public static String fromCubeMagType(String magCodeStr)
  {
    return (String)(fromCubeMagTypeXlatTable.get(magCodeStr.toLowerCase()));
  }

  /**
   * Translates a magnitude-type string to a CUBE-format magnitude-type
   * code.
   * @param magTypeStr magnitude-type string.
   * @return Translated CUBE-format magnitude-type code, or null if type
   * string not matched.
   */
  public static String toCubeMagType(String magTypeStr)
  {
    return (String)(toCubeMagTypeXlatTable.get(magTypeStr.toLowerCase()));
  }

  /**
   * Translates a CUBE-format location-method code to a location-method
   * string.
   * @param locMethCodeStr CUBE-format location-method code.
   * @return A two-string-block object containing the translated
   * 'Algorithm' and 'Class' strings, or null if code not matched.
   */
  public static TwoStringBlock fromCubeLocMethod(String locMethCodeStr)
  {
    return (TwoStringBlock)(fromCubeLocMethodXlatTable.get(
                                             locMethCodeStr.toLowerCase()));
  }

  /**
   * Translates a location-method 'Algorithm' string to a CUBE-format
   * location-method code.
   * @param locMethAlgStr location-method string.
   * @return Translated CUBE-format location-method code, or null if given
   * string not matched.
   */
  public static String toCubeLocMethod(String locMethAlgStr)
  {
    return (String)(toCubeLocMethodXlatTable.get(
                                              locMethAlgStr.toLowerCase()));
  }


    //Creates from-CUBE translation table for magnitude type.
  private static HashMap createFromCubeMagTypeXlatTable()
  {
    final HashMap mapObj = new HashMap();
    for(int i=0; i<magTypeCodeXlatArr.length; ++i)
    {
      mapObj.put(magTypeCodeXlatArr[i][0].toLowerCase(),
                                                  magTypeCodeXlatArr[i][1]);
    }
    return mapObj;
  }

    //Creates to-CUBE translation table for magnitude type.
  private static HashMap createToCubeMagTypeXlatTable()
  {
    final HashMap mapObj = new HashMap();
    for(int i=0; i<magTypeCodeXlatArr.length; ++i)
    {
      mapObj.put(magTypeCodeXlatArr[i][1].toLowerCase(),
                                                  magTypeCodeXlatArr[i][0]);
    }
    return mapObj;
  }

    //Creates from-CUBE translation table for location method.
  private static HashMap createFromCubeLocMethodXlatTable()
  {
    final HashMap mapObj = new HashMap();
    for(int i=0; i<locMethodCodeXlatArr.length; ++i)
    {
      mapObj.put(locMethodCodeXlatArr[i][0].toLowerCase(),
                              new TwoStringBlock(locMethodCodeXlatArr[i][1],
                                               locMethodCodeXlatArr[i][2]));
    }
    return mapObj;
  }

    //Creates to-CUBE translation table for location method.
  private static HashMap createToCubeLocMethodXlatTable()
  {
    final HashMap mapObj = new HashMap();
    for(int i=0; i<locMethodCodeXlatArr.length; ++i)
    {
      mapObj.put(locMethodCodeXlatArr[i][1].toLowerCase(),
                                                locMethodCodeXlatArr[i][0]);
    }
    return mapObj;
  }
}
