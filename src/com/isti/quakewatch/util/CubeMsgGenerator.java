//CubeMsgGenerator.java:  Generates CUBE-format text messages from
//                        QuakeWatch message-record objects.
//
//  10/5/2006 -- [ET]  Initial version.
//  7/16/2007 -- [ET]  Fixed issue where generated 'delete' and 'trump'
//                     messages would end with "null" if no comment
//                     element was found.
//  8/15/2007 -- [ET]  Added 'acceptXmlMsgsFlag' parameter and
//                     implementation to 'createMessage()' method.
//  8/30/2007 -- [ET]  Modified to use IstiXmlUtils 'getChildDataStr()'
//                     and 'ctrlCharsFromEscapedCodes()' methods to decode
//                     escaped ("&##;") strings for control characters
//                     in element-content text (to restore CR/LF/tab
//                     characters) when XML messages are passed through
//                     via 'acceptXmlMsgsFlag' setting.
//  2/11/2008 -- [ET]  Modified to use 'getPrependDeclarationString()' for
//                     optional insertion of declaration in generated XML
//                     messages.
//  5/23/2008 -- [ET]  Modified to put in all nines ("9999") if a value
//                     would be larger than what a given field could hold.
//  7/23/2008 -- [ET]  Modified use a default location-method character of
//                     'U' when the XML location-method algorithm string
//                     is not recognized or not found.
// 11/16/2009 -- [ET]  Modified 'translateLocMethod()' method to generate
//                     the 'Z' location-method code when the ANSS-EQ-XML
//                     'Event'|'Scope' element value is "Internal"; added
//                     'acceptNonActualFlag' parameter to 'createMessage()'
//                     method.
// 11/20/2009 -- [ET]  Moved class to 'QWCommon' project.
// 12/17/2009 -- [ET]  Added "get/setPrependDeclarationString()" methods
//                     and removed constructor with 'clientObj' parameter.
//

package com.isti.quakewatch.util;

import java.text.DateFormat;
import com.isti.util.UtilFns;
import com.isti.util.IstiXmlUtils;
import com.isti.quakewatch.common.MsgTag;
import com.isti.quakewatch.message.QWMsgRecord;
import com.isti.quakewatch.message.QWDataMsgRecord;
import com.isti.quakewatch.message.QWEQEventMsgRecord;
import com.isti.quakewatch.message.QWDelEventMsgRecord;
import com.isti.quakewatch.message.QWTrumpMsgRecord;
import com.isti.quakewatch.message.QWProductMsgRecord;
import com.isti.quakewatch.message.QWDelProductMsgRecord;

/**
 * Class CubeMsgGenerator generates CUBE-format text messages from
 * QuakeWatch message-record objects.
 */
public class CubeMsgGenerator
{
  protected static final String SPACE_STR = " ";
              //date format object for formatting date/time strings:
  protected final DateFormat cubeDateFormatterObj =
       UtilFns.createDateFormatObj("yyyyMMddHHmmssSSS",QWUtils.gmtTimeZone);
  protected static final String POS_OVERFLOW_STR = "99999999999999999999";
  protected static final String NEG_OVERFLOW_STR = "-99999999999999999999";
              //default location-method char for 'translateLocMethod()':
  protected static final String DEF_LOCMETHOD_STR = "U";
              //string to be prepended when XML messages are generated:
  protected String prependDeclarationString = null;

  /**
   * Sets the string to be prepended when XML-format messages are generated.
   * This only applies to non-standard/invalid XML messages that may be
   * outputted when the 'acceptXmlMsgFlag' parameter to the 'createMessage()'
   * method is set to 'true'.
   * @param pDeclStr string to be prepended, or null or an emptry string
   * for none.
   */
  public void setPrependDeclarationString(String pDeclStr)
  {
    prependDeclarationString =
       (pDeclStr != null && pDeclStr.trim().length() > 0) ? pDeclStr : null;
  }

  /**
   * Returns the string to be prepended when XML-format messages are
   * generated.  This only applies to non-standard/invalid XML messages
   * that may be outputted when the 'acceptXmlMsgFlag' parameter to the
   * 'createMessage()' method is set to 'true'.
   * @return The string to be prepended, or null for none.
   */
  public String getPrependDeclarationString()
  {
    return prependDeclarationString;
  }

  /**
   * Creates a CUBE-format text message from the given message-record
   * object.
   * @param recObj message-record object to use.
   * @param acceptXmlMsgsFlag true to accept non-standard/invalid messages
   * and return the XML-message text.
   * @param acceptNonActualFlag true to accept messages with Event|Usage
   * set to non-Actual value.
   * @return A new CUBE-format text message string, an empty string if
   * 'acceptNonActualFlag'==false and the message contained an Event|Usage
   * element with a value other than "Actual", or null if a message string
   * could not be created.
   */
  public String createMessage(QWMsgRecord recObj, boolean acceptXmlMsgsFlag,
                                                boolean acceptNonActualFlag)
  {
    if(recObj instanceof QWEQEventMsgRecord)
    {    //event-message record
      final QWEQEventMsgRecord evtMsgRecObj = (QWEQEventMsgRecord)recObj;
      final String uStr;
      if(acceptNonActualFlag || (uStr=evtMsgRecObj.getEventUsage()) == null
                                    || uStr.equalsIgnoreCase(MsgTag.ACTUAL))
      {  //accepting all msgs or no Event|Usage value or value is "Actual"
        synchronized(cubeDateFormatterObj)
        {  //only allow one thread at a time to use date formatter
          return calcAppendCheckChar(MsgTag.EQUAKE_MSGSTR +
                         UtilFns.fixStringLen(evtMsgRecObj.getEventID(),8) +
                      UtilFns.fixStringLen(evtMsgRecObj.getDataSource(),2) +
                         UtilFns.fixStringLen(evtMsgRecObj.getVersion(),1) +
                           UtilFns.fixStringLen(cubeDateFormatterObj.format(
                                               evtMsgRecObj.getTime()),15) +
                                 doubleToCubeStr(evtMsgRecObj.getLatitude(),
                                                      10000,7,false,false) +
                                doubleToCubeStr(evtMsgRecObj.getLongitude(),
                                                      10000,8,false,false) +
                                 doubleToCubeStr(evtMsgRecObj.getDepthObj(),
                                                         10,4,false,false) +
                             doubleToCubeStr(evtMsgRecObj.getMagnitudeObj(),
                                                         10,2,false,false) +
                          intToCubeStr(evtMsgRecObj.getNumStationsObj(),3) +
                            intToCubeStr(evtMsgRecObj.getNumPhasesObj(),3) +
                       doubleToCubeStr(evtMsgRecObj.getMinimumDistanceObj(),
                                                         10,4,false,false) +
                          doubleToCubeStr(evtMsgRecObj.getRMSTimeErrorObj(),
                                                        100,4,false,false) +
                    doubleToCubeStr(evtMsgRecObj.getHorizontalErrorValObj(),
                                                         10,4,false,false) +
                      doubleToCubeStr(evtMsgRecObj.getVerticalErrorValObj(),
                                                         10,4,false,false) +
                          doubleToCubeStr(evtMsgRecObj.getAzimuthalGapObj(),
                                                              (1.0/3.6),2) +
                                            translateMagType(evtMsgRecObj) +
                       intToCubeStr(evtMsgRecObj.getMagNumStationsObj(),2) +
                      doubleToCubeStr(evtMsgRecObj.getMagStandardErrorObj(),
                                                         10,2,false,false) +
                                          translateLocMethod(evtMsgRecObj));
        }
      }
      else    //Event|Usage value preset and value is not "Actual"
        return UtilFns.EMPTY_STRING;        //indicate not message
    }
    if(recObj instanceof QWDelEventMsgRecord)
    {    //delete-event-message record
      final QWDelEventMsgRecord delEvtRecObj = (QWDelEventMsgRecord)recObj;
      return MsgTag.DELQK_MSGSTR +
                         UtilFns.fixStringLen(delEvtRecObj.getEventID(),8) +
                      UtilFns.fixStringLen(delEvtRecObj.getDataSource(),2) +
             UtilFns.fixStringLen(delEvtRecObj.getVersion(),1) + SPACE_STR +
                                   nonNullStr(delEvtRecObj.getCommentStr());
    }
    if(recObj instanceof QWTrumpMsgRecord)
    {    //trump-message record
      final QWTrumpMsgRecord trumpEvtRecObj = (QWTrumpMsgRecord)recObj;
      return MsgTag.TRUMP_MSGSTR +
                       UtilFns.fixStringLen(trumpEvtRecObj.getEventID(),8) +
                    UtilFns.fixStringLen(trumpEvtRecObj.getDataSource(),2) +
           UtilFns.fixStringLen(trumpEvtRecObj.getVersion(),1) + SPACE_STR +
                                 nonNullStr(trumpEvtRecObj.getCommentStr());
    }
    if(recObj instanceof QWProductMsgRecord)
    {    //product-message record
      final QWProductMsgRecord prodRecObj = (QWProductMsgRecord)recObj;
      if(MsgTag.LINK_MSGSTR.equalsIgnoreCase(prodRecObj.msgTypeCode))
      {  //message is ProductLink ("LI")
        return prodRecObj.msgTypeCode +
                           UtilFns.fixStringLen(prodRecObj.getEventID(),8) +
                        UtilFns.fixStringLen(prodRecObj.getDataSource(),2) +
          UtilFns.fixStringLen(prodRecObj.getVersion(),2,true) + SPACE_STR +
                    prodRecObj.prodTypeCode + SPACE_STR + prodRecObj.value +
                                             ((prodRecObj.comment != null &&
                                   prodRecObj.comment.trim().length() > 0) ?
                   (SPACE_STR + prodRecObj.comment) : UtilFns.EMPTY_STRING);
      }
      else
      {  //message is Comment ("TX")
        return prodRecObj.msgTypeCode +
                           UtilFns.fixStringLen(prodRecObj.getEventID(),8) +
                        UtilFns.fixStringLen(prodRecObj.getDataSource(),2) +
          UtilFns.fixStringLen(prodRecObj.getVersion(),2,true) + SPACE_STR +
                                               nonNullStr(prodRecObj.value);
      }
    }
    if(recObj instanceof QWDelProductMsgRecord)
    {    //delete-product-message record
      final QWDelProductMsgRecord delProdObj = (QWDelProductMsgRecord)recObj;
      if(MsgTag.LINK_MSGSTR.equalsIgnoreCase(delProdObj.msgTypeCode))
      {  //message is ProductLink ("LI")
        return delProdObj.msgTypeCode +
                           UtilFns.fixStringLen(delProdObj.getEventID(),8) +
                        UtilFns.fixStringLen(delProdObj.getDataSource(),2) +
          UtilFns.fixStringLen(delProdObj.getVersion(),2,true) + SPACE_STR +
                    delProdObj.prodTypeCode + SPACE_STR + delProdObj.value +
                                                       SPACE_STR + "delete";
      }
      else
      {  //message is Comment ("TX")
        return delProdObj.msgTypeCode +
                           UtilFns.fixStringLen(delProdObj.getEventID(),8) +
                        UtilFns.fixStringLen(delProdObj.getDataSource(),2) +
           UtilFns.fixStringLen(delProdObj.getVersion(),2,true) + SPACE_STR;
      }
    }
    if(acceptXmlMsgsFlag && recObj instanceof QWDataMsgRecord)
    {  //accepting non-standard msgs and record is plain data-message record
      final QWDataMsgRecord dataRecObj = (QWDataMsgRecord)recObj;
      if(dataRecObj.dataMsgElement != null)
      {  //'DataMessage' element OK
        try        //fetch all child elements of 'DataMessage' element
        {          // and convert them to strings (appended together);
                   // convert any escaped strings to control chars:
          String retStr = IstiXmlUtils.ctrlCharsFromEscapedCodes(
                   IstiXmlUtils.getChildDataStr(dataRecObj.dataMsgElement));
          if(retStr.length() > 0)
          {  //data was generated
                   //if prepend-declaration string setup
                   // then insert into return string:
            if(prependDeclarationString != null)
              retStr = prependDeclarationString + retStr;
            return retStr;        //return generated string
          }
        }
        catch(Exception ex)
        {  //some kind of exception error; just return null
        }
      }
    }
    return null;
  }

  /**
   * Converts the given value to a CUBE-format numeric string.
   * @param val value to convert.
   * @param multiplier power-of-ten multiplier to apply to value.
   * @param outLen desired length of output string.
   * @param addZerosFlag true to add leading zero(s) to make string look
   * like original floating-point value without the decimal point.
   * @param showPosFlag true to show leading plus sign ('+') if positive
   * value.
   * @return Numeric-string representation of value.
   */
  public static String doubleToCubeStr(double val, int multiplier,
                      int outLen, boolean addZerosFlag, boolean showPosFlag)
  {
              //apply multiplier and convert to integer:
    final int iVal = (int)
                     (val * (((val < 0) ? -multiplier : multiplier)) + 0.5);
              //add leading zero(s) to make string look like original
              // floating-point value without the decimal point:
    String pStr = UtilFns.EMPTY_STRING;
    if(addZerosFlag && multiplier >= 10)
    {
      if(iVal < 10)
        pStr = '0' + pStr;
      if(multiplier >= 100)
      {
        if(iVal < 100)
          pStr = '0' + pStr;
        if(multiplier >= 1000)
        {
          if(iVal < 1000)
            pStr = '0' + pStr;
          if(multiplier >= 10000)
          {
            if(iVal < 10000)
              pStr = '0' + pStr;
          }
        }
      }
    }
    if(val < 0.0)                 //if negative then
      pStr = '-' + pStr;          //show minus sign
    else if(showPosFlag && val > 0.0)  //if showing positive sign then
      pStr = '+' + pStr;               //show plus sign
    String valStr = pStr+Integer.toString(iVal);
    if(valStr.length() > outLen)
    {  //too large for desired length; fill string with nines;
       // will be truncated to length by 'fixStringLen()' below:
      valStr = (val < 0.0) ? NEG_OVERFLOW_STR : POS_OVERFLOW_STR;
    }
              //return string, padded or truncated to length:
    return UtilFns.fixStringLen(valStr,outLen,true);
  }

  /**
   * Converts the given value to a CUBE-format numeric string.
   * @param valObj value object to convert, or null if none.
   * @param multiplier power-of-ten multiplier to apply to value.
   * @param outLen desired length of output string.
   * @param addZerosFlag true to add leading zero(s) to make string look
   * like original floating-point value without the decimal point.
   * @param showPosFlag true to show leading plus sign ('+') if positive
   * value.
   * @return Numeric-string representation of value, or a string
   * containing all spaces if no value given.
   */
  public static String doubleToCubeStr(Double valObj, int multiplier,
                      int outLen, boolean addZerosFlag, boolean showPosFlag)
  {
    return (valObj != null) ? doubleToCubeStr(valObj.doubleValue(),
                               multiplier,outLen,addZerosFlag,showPosFlag) :
                                            UtilFns.getSpacerString(outLen);
  }

  /**
   * Converts the given value to a CUBE-format numeric string.
   * @param val value to convert.
   * @param multiplier multiplier to apply to value.
   * @param outLen desired length of output string.
   * @return Numeric-string representation of value.
   */
  public static String doubleToCubeStr(double val, double multiplier,
                                                                 int outLen)
  {
    return UtilFns.fixStringLen(Integer.toString(
                         (int)(val*multiplier + ((val < 0) ? -0.5 : 0.5))),
                                                               outLen,true);
  }

  /**
   * Converts the given value to a CUBE-format numeric string.
   * @param valObj value object to convert, or null if none.
   * @param multiplier multiplier to apply to value.
   * @param outLen desired length of output string.
   * @return Numeric-string representation of value, or a string
   * containing all spaces if no value given.
   */
  public static String doubleToCubeStr(Double valObj, double multiplier,
                                                                 int outLen)
  {
    return (valObj != null) ?
        doubleToCubeStr(valObj.doubleValue(),multiplier,outLen) :
                                            UtilFns.getSpacerString(outLen);
  }

  /**
   * Converts the given value to a CUBE-format numeric string.
   * @param valObj value object to convert, or null if none.
   * @param outLen desired length of output string.
   * @param noValStr string to return if no value was given, or
   * null for none.
   * @return Numeric-string representation of value, or a string
   * containing all spaces (or 'noValStr') if no value given.
   */
  public static String intToCubeStr(Integer valObj, int outLen,
                                                            String noValStr)
  {
    final String retStr;
    if(valObj != null)
    {  //value object was given
      String valStr = valObj.toString();    //convert value to string
      if(valStr.length() > outLen)
      {  //too large for desired length; fill string with nines;
         // will be truncated to length by 'fixStringLen()' below:
        valStr = (valObj.intValue() < 0) ? NEG_OVERFLOW_STR :
                                                           POS_OVERFLOW_STR;
      }
              //generate return string, padded or truncated to length:
      retStr = UtilFns.fixStringLen(valStr,outLen,true);
    }
    else      //value object not given; create spaces string
      retStr = UtilFns.getSpacerString(outLen);
              //if no-value str given and return string is blank
              // then return no-value str:
    if(noValStr != null && retStr.trim().length() <= 0)
      return noValStr;
    return retStr;
  }

  /**
   * Converts the given value to a CUBE-format numeric string.
   * @param valObj value object to convert, or null if none.
   * @param outLen desired length of output string.
   * @return Numeric-string representation of value, or a string
   * containing all spaces if no value given.
   */
  public static String intToCubeStr(Integer valObj, int outLen)
  {
    return intToCubeStr(valObj,outLen,null);
  }

  /**
   * Translates the given message-record's magnitude-type string to a
   * CUBE-format code.
   * @param recObj event record containing magnitude-type string to use.
   * @return The single-character CUBE-format magnitude-type string, or
   * a string containing a single space if none.
   */
  public static String translateMagType(QWEQEventMsgRecord recObj)
  {
    final String magTypeStr;
    if((magTypeStr=recObj.getMagnitudeType()) == null)
    {    //no data; return space
      return SPACE_STR;
    }
    if(magTypeStr.trim().length() == 1)     //if single char then return it
      return magTypeStr;                    // (is already CUBE code)
         //check if event message has "Magnitude|Comment|Text" child-element
         // containing "CUBE_Code X", where 'X' is magnitude-type code:
    String commentStr;
    if((commentStr=recObj.getFirstMagCommentText()) != null &&
                                  (commentStr=commentStr.trim()).length() ==
                                            MsgTag.CUBE_CODE.length() + 1 &&
                                    commentStr.startsWith(MsgTag.CUBE_CODE))
    {    //"CUBE_Code X" comment found; return code character
      return commentStr.substring(MsgTag.CUBE_CODE.length());
    }
    return UtilFns.fixStringLen(
                            CubeCodeTranslator.toCubeMagType(magTypeStr),1);
  }

  /**
   * Translates the given message-record's location-method string to a
   * CUBE-format code.
   * @param recObj event record containing location-method string to use.
   * @return The single-character CUBE-format location-method-code string,
   * or a string containing a the default character ('U') if none.
   */
  public static String translateLocMethod(QWEQEventMsgRecord recObj)
  {
    final String retStr,locMethStr,scopeStr;
         //check if event has "Event|Scope" element with value "Internal":
    if((scopeStr=recObj.getEventScope()) != null &&
                                 scopeStr.equalsIgnoreCase(MsgTag.INTERNAL))
    {  //event has "Event|Scope" element with value "Internal"
      retStr = CubeCodeTranslator.CUBE_LOCMETHZ_STR;       //return 'Z'
    }
    else if((locMethStr=recObj.getLocationMethod()) == null)
      retStr = DEF_LOCMETHOD_STR;      //if no data then return default char
    else if(locMethStr.trim().length() != 1)
    {    //not single character
         //check if event msg has "Origin|Method|Comment|Text" child-element
         // containing "CUBE_Code X", where 'X' is location-method code:
      String commentStr;
      if((commentStr=recObj.getMethodCommentText()) != null &&
                                  (commentStr=commentStr.trim()).length() ==
                                            MsgTag.CUBE_CODE.length() + 1 &&
                                    commentStr.startsWith(MsgTag.CUBE_CODE))
      {  //"CUBE_Code X" comment found; return code character
        return commentStr.substring(MsgTag.CUBE_CODE.length());
      }
                   //convert location-method string to CUBE code char:
      final String cubeStr =
                             CubeCodeTranslator.toCubeLocMethod(locMethStr);
                   //if CUBE code char returned then make sure string
                   // length is 1; otherwise use default character:
      retStr = (cubeStr != null && cubeStr.trim().length() > 0) ?
                        UtilFns.fixStringLen(cubeStr,1) : DEF_LOCMETHOD_STR;
    }
    else      //single character (is already CUBE code)
      retStr = locMethStr;
         //make code lower-case if event "verified"; upper-case if not:
    return recObj.isVerified() ? retStr.toLowerCase() : retStr.toUpperCase();
  }

  /**
   * Calculates the Menlo Park check char and appends it to the given
   * string.
   * @param str string to use.
   * @return The given string with the Menlo Park check char apppended
   * to the end.
   */
  public static String calcAppendCheckChar(String str)
  {
    return str + calcMenloCheckChar(str);
  }

  /**
   * Calculates and returns the "Menlo Park(USGS) check char (Nov.94)"
   * code for the given String (excess 36 modulo 91).
   * @param str the string to use.
   * @return the check character.
   */
  public static char calcMenloCheckChar(String str)
  {
    final byte [] byteArr = str.getBytes();
    final int len = byteArr.length;
    int sum = 0;
    for(int p=0; p<len; ++p)
      sum = ((((sum&1)!=0)?0x8000:0) + (sum>>1) + byteArr[p]) & 0xFFFF;
    return (char)(36 + (sum % 91));
  }

  /**
   * If the given string is null then returns an empty string, otherwise
   * returns the given string.
   * @param str given string.
   * @return The given string, or an empty string if the given string is
   * null.
   */
  public static String nonNullStr(String str)
  {
    return (str != null) ? str : UtilFns.EMPTY_STRING;
  }
}
