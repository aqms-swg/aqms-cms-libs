//DomainTypeInfoRec.java:  Manages an information record containing
//                         a message-number value associated with an
//                         event domain and type name.
//
//  1/14/2004 -- [ET]
//

package com.isti.quakewatch.util;

/**
 * Class DomainTypeInfoRec manages an information record containing
 * a message-number value associated with an event domain and type name.
 */
public class DomainTypeInfoRec
{
    /** Event domain name string. */
  public final String domainNameStr;
    /** Event type name string. */
  public final String typeNameStr;
  protected long messageNumberValue = 0;
//  protected final boolean DEBUG_FLAG = true;

  /**
   * Creates an information record for the given event domain and type
   * name.
   * @param domainStr the domain name to use.
   * @param typeStr the type name to use.
   * @param msgNum the mesage-number value to use.
   */
  public DomainTypeInfoRec(String domainStr, String typeStr, long msgNum)
  {
    domainNameStr = domainStr;
    typeNameStr = typeStr;
    messageNumberValue = msgNum;
//    if(DEBUG_FLAG)
//    {
//      LogFile.getGlobalLogObj().debug3("DomainTypeInfoRec:  " +
//               "created record, domain=\"" + domainNameStr + "\", type=\"" +
//                          typeNameStr + "\", msgNum=" + messageNumberValue);
//    }
  }

  /**
   * Creates an information record for the given event domain and type
   * name.  The message-number value is set to zero.
   * @param domainStr the domain name to use.
   * @param typeStr the type name to use.
   */
  public DomainTypeInfoRec(String domainStr, String typeStr)
  {
    this(domainStr,typeStr,0);
  }

  /**
   * Sets the current message-number value.
   * @param msgNum the mesage-number value to use.
   */
  public void setMessageNumberValue(long msgNum)
  {
    messageNumberValue = msgNum;
//    if(DEBUG_FLAG)
//    {
//      LogFile.getGlobalLogObj().debug3("DomainTypeInfoRec:  " +
//                   "set msgNum, domain=\"" + domainNameStr + "\", type=\"" +
//                          typeNameStr + "\", msgNum=" + messageNumberValue);
//    }
  }

  /**
   * Increments the current message number value.
   */
  public void incMessageNumberValue()
  {
    ++messageNumberValue;
//    if(DEBUG_FLAG)
//    {
//      LogFile.getGlobalLogObj().debug3("DomainTypeInfoRec:  " +
//                   "inc msgNum, domain=\"" + domainNameStr + "\", type=\"" +
//                          typeNameStr + "\", msgNum=" + messageNumberValue);
//    }
  }

  /**
   * Returns the current message number value.
   * @return The message number value.
   */
  public long getMessageNumberValue()
  {
    return messageNumberValue;
  }
}
