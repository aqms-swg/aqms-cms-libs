//DomainTypeInfoTable.java:  Manages a table of information records, each
//                           containing a message-number value associated
//                           with an event domain and type name.
//
//  1/14/2003 -- [ET]
//

package com.isti.quakewatch.util;

import java.util.Collection;
import com.isti.util.UtilFns;
import com.isti.util.FifoHashtable;

/**
 * Class DomainTypeInfoTable manages a table of information records, each
 * containing a message-number value associated with an event domain and
 * type name.
 */
public class DomainTypeInfoTable
{
//  protected final boolean DEBUG_FLAG = true;
  protected final FifoHashtable hashtableObj = new FifoHashtable();
                   //separator for domain/type names in key strings:
  protected static final char TABLE_KEY_SEPCHAR = ':';
                   //characters that should be "quoted" in key strings:
  protected static final String TABLEKEY_SPECIAL_CHARS = TABLE_KEY_SEPCHAR +
                                              UtilFns.DEF_SPECIAL_CHARS_STR;


  /**
   * Sets the current message-number value associated with the given
   * event domain and type names.
   * @param domainStr the domain name to use.
   * @param typeStr the type name to use.
   * @param msgNum the mesage-number value to use.
   */
  public void setMessageNumberValue(String domainStr, String typeStr,long msgNum)
  {
    final String keyStr = buildTableKeyStr(domainStr,typeStr);
    final Object obj;
    synchronized(hashtableObj)
    {    //grab thread-synchronization lock for table
      if((obj=hashtableObj.get(keyStr)) instanceof DomainTypeInfoRec)
      {  //matching record found; set values
        ((DomainTypeInfoRec)obj).setMessageNumberValue(msgNum);
//        if(DEBUG_FLAG)
//        {
//          LogFile.getGlobalLogObj().debug3(
//                "DomainTypeInfoTable.setMessageNumberValue:  update, domain=\"" +
//              domainStr + "\", type=\"" + typeStr + "\", msgNum=" + msgNum);
//        }
      }
      else    //matching record not found
      {            //create and add record with given values:
        hashtableObj.put(keyStr,new DomainTypeInfoRec(
                                                 domainStr,typeStr,msgNum));
//        if(DEBUG_FLAG)
//        {
//          LogFile.getGlobalLogObj().debug3(
//             "DomainTypeInfoTable.setMessageNumberValue:  new entry, domain=\"" +
//              domainStr + "\", type=\"" + typeStr + "\", msgNum=" + msgNum);
//        }
      }
    }
  }

  /**
   * Returns the information-record object associated with the given
   * event domain and type names.  If a matching record does not exist
   * in the table then a new one (with its message-number value set to
   * zero) is created, added to the table and returned.
   * @param domainStr the domain name to use.
   * @param typeStr the type name to use.
   * @return The 'DomainTypeInfoRec' object from the table associated
   * with the given event domain and type names.
   */
  public DomainTypeInfoRec getInfoRecObj(String domainStr, String typeStr)
  {
    final String keyStr = buildTableKeyStr(domainStr,typeStr);
    final Object obj;
    synchronized(hashtableObj)
    {  //grab thread-synchronization lock for table
      if((obj=hashtableObj.get(keyStr)) instanceof DomainTypeInfoRec)
        return (DomainTypeInfoRec)obj;
      final DomainTypeInfoRec recObj =
                                   new DomainTypeInfoRec(domainStr,typeStr);
      hashtableObj.put(keyStr,recObj);
      return recObj;
    }
  }

  /**
   * Returns the current message-number value associated with the given
   * event domain and type names.
   * @param domainStr the domain name to use.
   * @param typeStr the type name to use.
   * @return The current message-number value associated with the given
   * event domain and type names, or 0 if no entry matching the given
   * event domain and type names exists in the table.
   */
  public long getMessageNumberValue(String domainStr, String typeStr)
  {
    final String keyStr = buildTableKeyStr(domainStr,typeStr);
    final Object obj;
    if((obj=hashtableObj.get(keyStr)) instanceof DomainTypeInfoRec)
      return ((DomainTypeInfoRec)obj).getMessageNumberValue();
    return 0;
  }

  /**
   * Returns a read-only collection view of the 'DomainTypeInfoRec'
   * objects contained in this table.  The collection does not support
   * any add or remove operations.  The 'values().iterator()' method
   * may be used to iterate over the values in this table (as long as
   * the table is not modified between iterations).
   * @return A collection view of the 'DomainTypeInfoRec' objects
   * contained in this table
   */
  public Collection values()
  {
    return hashtableObj.values();
  }

  /**
   * Clears the table.
   */
  public void clear()
  {
    hashtableObj.clear();
//    if(DEBUG_FLAG)
//    {
//      LogFile.getGlobalLogObj().debug3(
//                                     "DomainTypeInfoTable:  table cleared");
//    }
  }

  /**
   * Returns the thread-synchronization object for the table.
   * @return The thread-synchronization object for the table.
   */
  public Object getThreadSyncObj()
  {
    return hashtableObj;
  }

  /**
   * Creates a table-key string containing the given domain and type
   * name strings.  Instances of ':' and other special characters in
   * the given strings will be "quoted" by inserting a backslash ('\')
   * in front of them.
   * @param domainStr the domain name to use.
   * @param typeStr the type name to use.
   * @return A new table-key string.
   */
  public static String buildTableKeyStr(String domainStr,String typeStr)
  {
    if(domainStr == null)                   //if null string then
      domainStr = UtilFns.EMPTY_STRING;     //use empty string
    if(typeStr == null)                     //if null string then
      typeStr = UtilFns.EMPTY_STRING;       //use empty string
    return UtilFns.insertQuoteChars(domainStr,TABLEKEY_SPECIAL_CHARS) +
                                                         TABLE_KEY_SEPCHAR +
                   UtilFns.insertQuoteChars(typeStr,TABLEKEY_SPECIAL_CHARS);
  }
}
