//EncryptDecryptIntf.java:  Interface for encrypt/decrypt methods.
//
// 11/21/2005 -- [ET]
//

package com.isti.quakewatch.util;

/**
 * Interface EncryptDecryptIntf describes encrypt/decrypt methods.
 */
public interface EncryptDecryptIntf
{
  /**
   * Encrypts the given string.
   * @param dataStr data string to use.
   * @return encrypted string, or null if the data string is invalid.
   */
  public String encrypt(String dataStr);

  /**
   * Decrypts the given string.
   * @param dataStr data string to use.
   * @return decrypted string, or null if the data string is invalid.
   */
  public String decrypt(String dataStr);

  /**
   * Determines if the given data string is in the format of a string that
   * has been encrypted by this class.
   * @param dataStr data strign to use.
   * @return true if the given data string is in the format of a string
   * that has been encrypted by this class.
   */
  public boolean isEncryptStr(String dataStr);

}
