//EncryptDecryptUtil.java:  Utility encrypt/decrypt methods for QuakeWatch.
//
//  3/18/2008 -- [ET]
//

package com.isti.quakewatch.util;

/**
 * Class EncryptDecryptUtil defines utility encrypt/decrypt methods for
 * QuakeWatch
 */
public class EncryptDecryptUtil implements EncryptDecryptIntf
{
  private static final String miscKeyString =
                      "kemel;al4lkmklm2mck;lm*mk;lmavg3$nmlfa*&%ma;kklsk4" +
                      "e8ckil;a6;m;*7665rBiNmu:<UmnUYurtylVv##vgfEAgRESgk" +
                      "aeomcursnsaunun3upnmpcasnmo4i4wi4m;fksl;mvkdsv(&bj" +
                      "2asenjsun4u4ndrvfdsn23r8$@#((NvtctviVt5cxlklmyud7n" +
                      "6ibbyt3#@#78nubrc3#;oi:JnBYT54542xril;nnjunbutivyt" +
                      "nu^435wi7nbiubxCYE32@&IbOybonnoMIopiuonjdrtvm7mcql";
  private final int [] generatedKeyArray;

  /**
   * Creates an encrypt/decrypt utility.
   * @param key1Str key string to use.
   * @param key2Str key string to use.
   */
  public EncryptDecryptUtil(String key1Str, String key2Str)
  {
    if(key1Str == null || key1Str.length() <= 0)
      key1Str = "_";
    if(key2Str == null || key2Str.length() <= 0)
      key2Str = " ";
    final int key1StrLen = key1Str.length();
    final int key2StrLen = key2Str.length();
    final int keyLen = miscKeyString.length();
    generatedKeyArray = new int[keyLen];
    int idx1,idx2;
    for(int i=0; i<keyLen; ++i)
    {
      idx1 = i % key1StrLen;
      idx2 = i % key2StrLen;
      if(i % 7 == 0)
      {
        generatedKeyArray[i] = Math.abs((int)(miscKeyString.charAt(i)) +
           (int)(key1Str.charAt(idx1)) + (int)(key2Str.charAt(idx2))) % 256;
      }
      else if(i % 5 == 0)
      {
        generatedKeyArray[i] = Math.abs((int)(miscKeyString.charAt(i)) -
           (int)(key1Str.charAt(idx1)) + (int)(key2Str.charAt(idx2))) % 256;
      }
      else if(i % 2 == 0)
      {
        generatedKeyArray[i] = Math.abs((int)(miscKeyString.charAt(i))*2 +
           (int)(key1Str.charAt(idx1)) - (int)(key2Str.charAt(idx2))) % 256;
      }
      else
      {
        generatedKeyArray[i] = Math.abs((int)(miscKeyString.charAt(i)) +
          (int)(key1Str.charAt(idx1)) - (int)(key2Str.charAt(idx2))) % 256;;
      }
    }
  }

  /**
   * Encrypts the given string.
   * @param dataStr data string to use.
   * @return encrypted string, or null if the data string is invalid.
   */
  public String encrypt(String dataStr)
  {
    try
    {
      int dataStrLen;
      if((dataStrLen=dataStr.length()) <= 0 || dataStrLen > 9999999)
        return null;
      final char [] dataCharArr = new char[dataStrLen+1];
      char chVal;
      int cTotal = 0;
      for(int i=0; i<dataStrLen; ++i)
      {
        if((chVal=dataStr.charAt(i)) < (char)0 || chVal > (char)255)
          chVal = '?';         //if char out of range then use '?'
        dataCharArr[i] = chVal;
        cTotal += chVal;
      }
      dataCharArr[dataStrLen] = (char)((cTotal * 3) % 256);
      ++dataStrLen;
      final StringBuffer buff = new StringBuffer();
      int val;
      String str;
      for(int i=0; i<dataStrLen; ++i)
      {
        val = (int)(dataCharArr[i]);
        if(i % 3 == 0)
          val += generatedKeyArray[i%256];
        else if(i % 2 == 0)
          val = val*2 + generatedKeyArray[i%256];
        else
          val += generatedKeyArray[i%256]*2;
        if(val < 10)
          str = "00" + val;
        else if(val < 100)
          str = "0" + val;
        else
          str = "" + val;
        buff.append(str.charAt(1));
        buff.append(str.charAt(0));
        buff.append(str.charAt(2));
      }
      dataStr = buff.toString();
      dataStrLen = buff.length();
      buff.delete(0,dataStrLen);
      final StringBuffer buff2 = new StringBuffer();
      int i = 0;
      do
      {
        buff.append(dataStr.charAt(i));
        if(i + 1 < dataStrLen)
          buff2.append(dataStr.charAt(i+1));
      }
      while((i+=2) < dataStrLen);
      buff.append(buff2.toString());
      return "000" + buff.reverse().toString() + "000";
    }
    catch(Exception ex)
    {
      ex.printStackTrace();
      return null;
    }
  }

  /**
   * Decrypts the given string.
   * @param dataStr data string to use.
   * @return decrypted string, or null if the data string is invalid.
   */
  public String decrypt(String dataStr)
  {
    try
    {
      int dataStrLen;
      if(!isEncryptStr(dataStr) || (dataStrLen=dataStr.length()) <= 6)
        return null;
      final StringBuffer buff =
                          new StringBuffer(dataStr.substring(3,dataStrLen-3));
      dataStr = buff.reverse().toString();
      dataStrLen -= 6;
      buff.delete(0,dataStrLen);
      final int halfLen = (dataStrLen + 1) / 2;
      for(int i=0; i<halfLen; ++i)
      {
        buff.append(dataStr.charAt(i));
        if(i+halfLen < dataStrLen)
          buff.append(dataStr.charAt(i+halfLen));
      }
      dataStr = buff.toString();
      buff.delete(0,buff.length());
      int i = 0, sPos = 0;
      String str;
      int val;
      do
      {
        str = new String(new char [] { dataStr.charAt(sPos+1),
                              dataStr.charAt(sPos), dataStr.charAt(sPos+2) });
        try
        {
          val = Integer.parseInt(str);
        }
        catch(NumberFormatException ex)
        {
          throw new NumberFormatException("Data value format invalid");
        }
        if(i % 3 == 0)
          val -= generatedKeyArray[i%256];
        else if(i % 2 == 0)
          val = (val - generatedKeyArray[i%256]) / 2;
        else
          val -= generatedKeyArray[i%256]*2;
        buff.append((char)val);
        ++i;
      }
      while((sPos+=3) < dataStrLen);
      if((dataStrLen=buff.length()) > 1)
      {
        char ch = buff.charAt(--dataStrLen);
        buff.deleteCharAt(dataStrLen);
        int tot = 0;
        for(i=0; i<dataStrLen; ++i)
          tot += buff.charAt(i);
        if((char)((tot * 3) % 256) == ch)
          return buff.toString();
      }
    }
    catch(Exception ex)
    {
      ex.printStackTrace();
    }
    return null;
  }

  /**
   * Determines if the given data string is in the format of a string that
   * has been encrypted by this class.
   * @param dataStr data strign to use.
   * @return true if the given data string is in the format of a string
   * that has been encrypted by this class.
   */
  public boolean isEncryptStr(String dataStr)
  {
    if(dataStr != null && dataStr.length() >= 9 &&
         dataStr.startsWith("000") && dataStr.endsWith("000"))
    {
      final int endLen = dataStr.length() - 3;
      char ch;
      for(int i=3; i<endLen; ++i)
      {
        if((ch=dataStr.charAt(i)) < '0' || ch > '9')
          return false;
      }
      return true;
    }
    return false;
  }


/*
  public static void main(String[] args) throws java.io.IOException
  {
    final EncryptDecryptUtil encryptDecryptObj =
                               new EncryptDecryptUtil("testkeyOne","tstk2");
    String inStr,encStr,outStr;
    int inStrLen,randCount = 0;
    java.util.Random randomObj = null;
    char [] charArr;
    while(true)
    {
      if(randCount <= 0)
      {
        System.out.print("in:   ");
        inStr = (new java.io.BufferedReader(new java.io.InputStreamReader(
                    System.in))).readLine();
        if(inStr == null || inStr.length() <= 0 ||
                                                inStr.equalsIgnoreCase("Q"))
        {
          return;
        }
        if(inStr.startsWith("DoRand"))
        {
          if(inStr.length() > 6)
          {
            try
            {
              randCount = Integer.parseInt(inStr.substring(6));
              continue;
            }
            catch(NumberFormatException ex)
            {
            }
          }
          else
          {
            randCount = 1;
            continue;
          }
        }
        if((encStr=encryptDecryptObj.encrypt(inStr)) != null)
        {
          System.out.println("enc:  " + encStr);
          if((outStr=encryptDecryptObj.decrypt(encStr)) != null)
            System.out.println("out:  " + outStr);
          else
            System.err.println("Error decrypting data string");
        }
        else
          System.err.println("Error encrypting data string");
      }
      else
      {
        if(randomObj == null)
          randomObj = new java.util.Random(System.currentTimeMillis());
        inStrLen = randomObj.nextInt(255) + 1;
        charArr = new char[inStrLen];
        for(int i=0; i<inStrLen; ++i)
          charArr[i] = (char)(randomObj.nextInt(127-32) + 32);
        inStr = new String(charArr);
        if((encStr=encryptDecryptObj.encrypt(inStr)) == null)
        {
          System.err.println("Error encrypting data string:  " + inStr);
          randCount = 0;
          continue;
        }
        if((outStr=encryptDecryptObj.decrypt(encStr)) == null)
        {
          System.err.println("Error decrypting data string, inStr:  " +
                                                                     inStr);
          randCount = 0;
          continue;
        }
        if(!inStr.equals(outStr))
        {
          System.out.println("Mismatch, count = " + randCount);
          randCount = 1;
        }
        if(--randCount <= 4)
        {
          System.out.println("in:   " + inStr);
          System.out.println("enc:  " + encStr);
          System.out.println("out:  " + outStr);
        }
      }
    }
  }
*/
}
