//InitReqMsgsCallBack.java:  Defines methods called by 'QWConnectionMgr'
//                           before and after its initial fetch of messages
//                           from the server.
//
//  9/30/2003 -- [ET]
//

package com.isti.quakewatch.util;

/**
 * Interface InitReqMsgsCallBack defines methods called by 'QWConnectionMgr'
 * before and after its initial fetch of messages from the server.
 */
public interface InitReqMsgsCallBack
{
  /**
   * Called just before the 'fetchAndProcInitMessagesFromServer()'
   * method is called.
   */
  public void fetchMessagesPrepare();

  /**
   * Called after the 'fetchAndProcInitMessagesFromServer()' has
   * completed or has been canceled.
   * @param errMsgStr an error message string, or null if no error.
   */
  public void fetchMessagesComplete(String errMsgStr);
}
