//JavaMailTransport:  Defines transport methods for
//                    using Sun's JavaMail package to send mail.
//
//   6/7/2011 -- [KF]  Initial version.
//

package com.isti.quakewatch.util;

import javax.mail.Authenticator;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.NoSuchProviderException;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;

/**
 * Class JavaMailTransport defines transport methods for using Sun's JavaMail
 * package to send mail.
 */
public class JavaMailTransport {
  /** The protocol. */
  private final String protocol = "smtp";

  /** The authenticator or null if none. */
  private Authenticator authenticator = null;

  /** The password authentication. */
  private PasswordAuthentication passwordAuthentication = new PasswordAuthentication(
      null, null);

  /** The host or null for the default. */
  private String host;

  /** The port or -1 for the default. */
  private int port = -1;

  /**
   * Connect to the server.
   * @param transportObj the transport.
   * @throws MessagingException if failure.
   */
  protected void connect(Transport transportObj) throws MessagingException {
    transportObj.connect(host, port, passwordAuthentication.getUserName(),
        passwordAuthentication.getPassword());
  }

  /**
   * Get the authenticator.
   * @return the authenticator or null if none.
   */
  public Authenticator getAuthenticator() {
    return authenticator;
  }

  /**
   * Send a message.
   * @param msgObj the message.
   * @param sessionObj the mail session or null if none.
   * @throws MessagingException if the message could not be sent to some or any
   *           of the recipients.
   */
  public void sendMessage(Message msgObj, Session sessionObj)
      throws MessagingException {
    // if no authentication or no mail session
    if (authenticator == null || sessionObj == null) {
      Transport.send(msgObj); // send email message
    } else {
      Transport transportObj = sessionObj.getTransport(protocol);
      try {
        connect(transportObj); // connect to the server
        transportObj.sendMessage(msgObj, msgObj.getAllRecipients());
      } finally {
        transportObj.close();
      }
    }
  }

  /**
   * Set the authentication flag.
   * @param b true for authentication, false otherwise.
   */
  public void setAuthentication(boolean b) {
    if (b) {
      authenticator = new Authenticator() {
        protected PasswordAuthentication getPasswordAuthentication() {
          return passwordAuthentication;
        }
      };
    } else {
      authenticator = null;
    }
  }

  /**
   * Set the host.
   * @param host the host.
   */
  public void setHost(String host) {
    this.host = host;
  }

  /**
   * Set the password.
   * @param password the password.
   */
  public void setPassword(String password) {
    passwordAuthentication = new PasswordAuthentication(
        passwordAuthentication.getUserName(), password);
  }

  /**
   * Set the port.
   * @param port the port.
   */
  public void setPort(int port) {
    this.port = port;
  }

  /**
   * Set the user.
   * @param user the user.
   */
  public void setUser(String user) {
    passwordAuthentication = new PasswordAuthentication(user,
        passwordAuthentication.getPassword());
  }

  /**
   * Tests the connection to the mail server.
   * @param sessionObj the mail session.
   * @throws NoSuchProviderException if transport provider is not found.
   * @throws MessagingException if failure.
   */
  public void testServerConnection(Session sessionObj)
      throws MessagingException {
    // get Transport object for session:
    Transport transportObj = sessionObj.getTransport(new InternetAddress());
    try {
      connect(transportObj); // connect to the server
    } finally {
      transportObj.close();
    }
  }
}
