//JavaMailUtil.java:  Defines configuration and utility methods for
//                    using Sun's JavaMail package to send mail via
//                    an SMTP server.
//
//  4/22/2004 -- [ET]  Initial version for 'QWEmailer'.
//   6/8/2005 -- [KF]  Copied to 'QWCapOutClient'.
//  1/19/2006 -- [KF]  Moved from 'QWCapOutClient' to 'QWCommon'.
//  3/31/2006 -- [ET]  Added 'validateFromAddress()' method; added the
//                     word "email" to several error messages.
// 12/21/2006 -- [ET]  Added "all-in-one program jar" notes to comments.
//   6/7/2011 -- [KF]  Added support for specifying the port and authentication.
//   3/6/2019 -- [KF]  Added support for SSL.
//

package com.isti.quakewatch.util;

import java.util.ArrayList;
import java.util.Date;
import java.util.Properties;

import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.NoSuchProviderException;
import javax.mail.Session;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;

import com.isti.util.CfgPropItem;
import com.isti.util.CfgProperties;
import com.isti.util.DataChangedListener;
import com.isti.util.ErrorMessageMgr;
import com.isti.util.UtilFns;

/**
 * Class JavaMailUtil defines configuration and utility methods for
 * using Sun's JavaMail package to send mail via an SMTP server.
 * If an all-in-one program jar is being built then the following
 * files need to be included:  "META-INF/javamail.providers" &
 * "META-INF/javamail.address.map" from "smtp.jar", and
 * "META-INF/javamail.charset.map" & "META-INF/javamail.mailcap"
 * from "mailapi.jar".
 */
public class JavaMailUtil extends ErrorMessageMgr
{
  private static final char QUOTE_CHAR = '\'';
    /** Flag set true to indicate session config-properties change. */
  protected boolean sessionPropsChangedFlag = true;
    /** Session object for sending SMTP mail. */
  protected Session smtpMailSessionObj = null;
    /** String for "X-Mailer" header in messages. */
  protected String xMailerHeaderString = null;

    /** Configuration-properties object; may be passed in. */
  public final CfgProperties configPropsObj;
    /** Config-properties object containing only mail items. */
  public final CfgProperties localPropsObj;

    /** Address of SMTP server for sending mail ("URL[:port]"). */
  public final CfgPropItem smtpServerAddressProp;
    /** Timeout for initial connection to SMTP server, in seconds. */
  public final CfgPropItem initSMTPTimeoutSecProp;
    /** Timeout for send-connections to SMTP server, in seconds. */
  public final CfgPropItem sendSMTPTimeoutSecProp;
    /** Flag set true for no-send if some recipient addresses are bad. */
  public final CfgPropItem dontSendPartialFlagProp;
    /** Flag for JavaMail session debug messages, true = debug on. */
  public final CfgPropItem mailDebugFlagProp;
    /**
     * SMTP server authentication ("username,password[,STARTTLS][,SSL]").
     * Authentication options such as STARTTLS require a recent version of JavaMail.
     */
  public final CfgPropItem smtpServerAuthProp;
    /** The Java mail transport. */
  private final JavaMailTransport transportObj = new JavaMailTransport();


  /**
   * Constructs a mail utilities object.
   * @param propsObj the configuration-properties object to add item to,
   * or null to create a new configuration-properties object.
   * @param smtpServerAddressProp the SMTP-server-address property object
   * to use, or null to create one.
   * @param smtpServerAuthProp the SMTP server authentication property object
   * to use, or null to create one.
   * @param groupSelObj the configuration-group-selection object to use,
   * or null for none.
   */
  public JavaMailUtil(CfgProperties propsObj,
                      CfgPropItem smtpServerAddressProp,
                      CfgPropItem smtpServerAuthProp,
                      Object groupSelObj)
  {
         //setup local configuration-properties object:
    localPropsObj = new CfgProperties();

         //setup data-changed listener that sets the changed flag:
    final DataChangedListener propsChangedListenerObj =
                                                   new DataChangedListener()
        {
          public void dataChanged(Object sourceObj)
          {
            sessionPropsChangedFlag = true;
          }
        };

         //if no SMTP-server-address property object
    if(smtpServerAddressProp == null)
    {
      smtpServerAddressProp = localPropsObj.add(
                              "smtpServerAddress",UtilFns.EMPTY_STRING,null,
                                 "Address of SMTP server for sending mail").
                                                setGroupSelObj(groupSelObj);
    }
    this.smtpServerAddressProp = smtpServerAddressProp;
    if (smtpServerAuthProp == null)
    {
    	smtpServerAuthProp = localPropsObj.add("smtpServerAuth", UtilFns.EMPTY_STRING,null,
                "SMTP server authentication").
                               setGroupSelObj(groupSelObj);
    }
	this.smtpServerAuthProp = smtpServerAuthProp;

         //add session-properties-changed listener to server-address prop:
    this.smtpServerAddressProp.addDataChangedListener(
                                                   propsChangedListenerObj);

         //timeout for initial connection to SMTP server, in seconds:
    initSMTPTimeoutSecProp = localPropsObj.add("initSMTPTimeoutSec",
                                                       Integer.valueOf(10),null,
                                  "Timeout for SMTP server init (seconds)").
                                                setGroupSelObj(groupSelObj);

         //timeout for send-connections to SMTP server, in seconds:
    sendSMTPTimeoutSecProp = localPropsObj.add("sendSMTPTimeoutSec",
                                                      Integer.valueOf(120),null,
                              "Timeout for sends to SMTP server (seconds)").
                                                setGroupSelObj(groupSelObj).
                            addDataChangedListener(propsChangedListenerObj);

         //flag set true for no-send if some recipient addresses are bad:
    dontSendPartialFlagProp = localPropsObj.add("dontSendPartialFlag",
                                                    Boolean.valueOf(false),null,
                               "Don't send to rest if some recipients bad").
                                                setGroupSelObj(groupSelObj).
                            addDataChangedListener(propsChangedListenerObj);

         //flag for JavaMail session debug messages, true = debug on:
    mailDebugFlagProp = localPropsObj.add("mailDebugFlag",
                                                    Boolean.valueOf(false),null,
                                  "Enable JavaMail session debug messages").
                                                setGroupSelObj(groupSelObj).
                            addDataChangedListener(propsChangedListenerObj);

    if(propsObj != null)
    {    //external config-props object was given
      propsObj.putAll(localPropsObj);       //add items to external props
      configPropsObj = propsObj;            //set handle to ext props obj
    }
    else      //external config-props object not given
      configPropsObj = localPropsObj;       //set handle to local props obj
  }

  /**
   * Constructs a mail utilities object.
   */
  public JavaMailUtil()
  {
    this(null,null,null,null);
  }

  /**
   * Sets the string for the "X-Mailer" header in messages.
   * @param str the string to use, or null for none.
   */
  public void setXMailerHeaderString(String str)
  {
    xMailerHeaderString = str;
  }

  /**
   * Returns the string for the "X-Mailer" header in messages.
   * @return The "X-Mailer" header string to use, or null if none.
   */
  public String getXMailerHeaderString()
  {
    return xMailerHeaderString;
  }

  /**
   * Tests the connection to the SMTP mail server.
   * @return true if successful; false if not (in which case an error
   * message may be fetched via the 'getErrorMessageString()' method).
   */
  public boolean testServerConnection()
  {
    final Session sessionObj;
    if((sessionObj=createMailSession(true)) == null)
      return false;
    try
    {
      transportObj.testServerConnection(sessionObj);
    }
    catch(NoSuchProviderException ex)
    {         //no provider configuration exception; setup error message
      setErrorMessageString("JavaMail transport provider config error " +
                            "(check \"javamail.providers\" file):  " +  ex);
      return false;
    }
    catch(Exception ex)
    {         //some kind of error
      if(getLastMessagingException(ex) instanceof
                                              java.net.UnknownHostException)
      {  //nested exception is "unknown host"; setup error message
        setErrorMessageString("Unable to connect to SMTP email server:  " +
            "Unknown host \"" + smtpServerAddressProp.stringValue() + "\"");
      }
      else
      {  //not "unknown host" nested exception; setup error message
        setErrorMessageString("Unable to connect to SMTP email server (\"" +
                       smtpServerAddressProp.stringValue() + "\"):  " + ex);
      }
      return false;
    }
    return true;
  }

  /**
   * Checks the validity of the given "from" email address.
   * @param fromAddrStr the "from" address to use.
   * @param fromNameStr the personal-name to associated with the
   * "from" address, or null for none.
   * @return true if the given email address is valid; false if
   * not (in which case an error message may be fetched via the
   * 'getErrorMessageString()' method).
   */
  public boolean validateFromAddress(String fromAddrStr, String fromNameStr)
  {
    if(fromAddrStr == null || fromAddrStr.length() <= 0)
    {    //no data in 'from' address
      setErrorMessageString("Empty 'from' address");
      return false;
    }
         //parse "from" address string:
    final InternetAddress fromAddrObj;
    try
    {         //parse "from" address into object:
      fromAddrObj = new InternetAddress(fromAddrStr);
    }
    catch(Exception ex)
    {
      setErrorMessageString("Invalid 'from' address:  " + ex);
      return false;
    }
         //if personal-name string given then add it in:
    if(fromNameStr != null && fromNameStr.length() > 0)
    {    //"from" personal-name given
      try
      {       //add personal-name to address object:
        fromAddrObj.setPersonal(fromNameStr);
      }
      catch(Exception ex)
      {
        setErrorMessageString("Invalid 'from' personal-name:  " + ex);
        return false;
      }
    }
    return true;
  }

  /**
   * Checks the validity of the given "from" email address.
   * @param fromAddrStr the "from" address to use.
   * @return true if the given email address is valid; false if
   * not (in which case an error message may be fetched via the
   * 'getErrorMessageString()' method).
   */
  public boolean validateFromAddress(String fromAddrStr)
  {
    return validateFromAddress(fromAddrStr,null);
  }

  /**
   * Sends an email message using the given parameters.  This method
   * blocks until the send attempt is completed, and thread-synchronizes
   * using the mail-session object.
   * @param fromAddrStr the "from" address to use.
   * @param fromNameStr the personal-name to associated with the "from"
   * address, or null for none.
   * @param recipientsStr the list of recipient email addresses.
   * @param subjectStr the subject text to use, or null for none.
   * @param bodyTextStr the message-body text to use.
   * @param sentDateObj the "sent" date to use, or null for no date in
   * message.
   * @param xMailerHdrFlag true to include the "X-Mailer" header text
   * setup via the 'setXMailerHeaderString()' method; false for no
   * "X-Mailer" header.
   * @return true if successful; false if not (in which case an error
   * message may be fetched via the 'getErrorMessageString()' method).
   */
  public boolean sendEmailMessage(String fromAddrStr, String fromNameStr,
                String recipientsStr, String subjectStr, String bodyTextStr,
                                   Date sentDateObj, boolean xMailerHdrFlag)
  {
    if(fromAddrStr == null || fromAddrStr.length() <= 0)
    {    //no data in 'from' address
      setErrorMessageString("Empty 'from' address");
      return false;
    }
         //parse "from" address string:
    final InternetAddress fromAddrObj;
    try
    {         //parse "from" address into object:
      fromAddrObj = new InternetAddress(fromAddrStr);
    }
    catch(Exception ex)
    {
      setErrorMessageString("Error parsing 'from' address:  " + ex);
      return false;
    }
         //if personal-name string given then add it in:
    if(fromNameStr != null && fromNameStr.length() > 0)
    {    //"from" personal-name given
      try
      {       //add personal-name to address object:
        fromAddrObj.setPersonal(fromNameStr);
      }
      catch(Exception ex)
      {
        setErrorMessageString("Error adding 'from' personal-name:  " + ex);
        return false;
      }
    }
         //parse recipient addresses string:
    final InternetAddress [] recipsAddrArr;
    try
    {         //parse recipient addresses into array of address objects:
      recipsAddrArr = InternetAddress.parse(recipientsStr);
    }
    catch(Exception ex)
    {
      setErrorMessageString("Error parsing recipient addresses:  " + ex);
      return false;
    }
         //if session properties changed or mail-session object
         // not setup then create mail-session object:
    if(sessionPropsChangedFlag || smtpMailSessionObj == null)
    {
      if((smtpMailSessionObj=createMailSession(false)) == null)
        return false;
      sessionPropsChangedFlag = false;
    }
    synchronized(smtpMailSessionObj)
    {    //thread-synchronize on session object
      try
      {                   //create message object:
        final Message msgObj = new MimeMessage(smtpMailSessionObj);
        msgObj.setFrom(fromAddrObj);     //enter the "from" address
                          //enter the "to" addresses:
        msgObj.setRecipients(Message.RecipientType.TO,recipsAddrArr);
        if(subjectStr != null)                //if subject string given then
          msgObj.setSubject(subjectStr);      //enter subject text
        if(sentDateObj != null)               //if sent-date given then
          msgObj.setSentDate(sentDateObj);    //set given date
        if(xMailerHdrFlag && xMailerHeaderString != null &&
                                           xMailerHeaderString.length() > 0)
        {  //flag set and "X-Mailer" header given; enter string for header
          msgObj.setHeader("X-Mailer",xMailerHeaderString);
        }
        msgObj.setText(bodyTextStr);          //enter message text
        transportObj.sendMessage(msgObj,smtpMailSessionObj); //send email message
      }
      catch(NoSuchProviderException ex)
      {         //no provider configuration exception; setup error message
        setErrorMessageString("JavaMail transport provider config error " +
                            "(check \"javamail.providers\" file):  " +  ex);
        return false;
      }
      catch(Exception ex)
      {         //some kind of error
        if(getLastMessagingException(ex) instanceof
                                              java.net.UnknownHostException)
        {  //nested exception is "unknown host"; setup error message
          setErrorMessageString("Unable to connect to SMTP email server:  " +
            "Unknown host \"" + smtpServerAddressProp.stringValue() + "\"");
        }
        else    //not "unknown host" nested exception; setup error message
          setErrorMessageString("Error sending email:  " + ex);
        return false;
      }
    }
    return true;
  }

  /**
   * Sends an email message using the given parameters.
   * @param fromAddrStr the "from" address to use.
   * @param fromNameStr the personal-name to associated with the "from"
   * address, or null for none.
   * @param recipientsStr the list of recipient email addresses.
   * @param subjectStr the subject text to use, or null for none.
   * @param bodyTextStr the message-body text to use.
   * @param sentDateObj the "sent" date to use, or null for no date in
   * message.
   * @return true if successful; false if not (in which case an error
   * message may be fetched via the 'getErrorMessageString()' method).
   */
  public boolean sendEmailMessage(String fromAddrStr, String fromNameStr,
                                    String recipientsStr, String subjectStr,
                                       String bodyTextStr, Date sentDateObj)
  {
    return sendEmailMessage(fromAddrStr,fromNameStr,recipientsStr,
                                   subjectStr,bodyTextStr,sentDateObj,true);
  }

  /**
   * Sends an email message using the given parameters.  A "date" header
   * showing the current date will be included in the message.
   * @param fromAddrStr the "from" address to use.
   * @param fromNameStr the personal-name to associated with the "from"
   * address, or null for none.
   * @param recipientsStr the list of recipient email addresses.
   * @param subjectStr the subject text to use, or null for none.
   * @param bodyTextStr the message-body text to use.
   * @return true if successful; false if not (in which case an error
   * message may be fetched via the 'getErrorMessageString()' method).
   */
  public boolean sendEmailMessage(String fromAddrStr, String fromNameStr,
                String recipientsStr, String subjectStr, String bodyTextStr)
  {
    return sendEmailMessage(fromAddrStr,fromNameStr,recipientsStr,
                                  subjectStr,bodyTextStr,(new Date()),true);
  }

  /**
   * Sends an email message using the given parameters.
   * @param fromAddrStr the "from" address to use.
   * @param recipientsStr the list of recipient email addresses.
   * @param subjectStr the subject text to use, or null for none.
   * @param bodyTextStr the message-body text to use.
   * @param sentDateObj the "sent" date to use, or null for no date in
   * message.
   * @param xMailerHdrFlag true to include the "X-Mailer" header text
   * setup via the 'setXMailerHeaderString()' method; false for no
   * "X-Mailer" header.
   * @return true if successful; false if not (in which case an error
   * message may be fetched via the 'getErrorMessageString()' method).
   */
  public boolean sendEmailMessage(String fromAddrStr, String recipientsStr,
                                      String subjectStr, String bodyTextStr,
                                   Date sentDateObj, boolean xMailerHdrFlag)
  {
    return sendEmailMessage(fromAddrStr,null,recipientsStr,subjectStr,
                                    bodyTextStr,sentDateObj,xMailerHdrFlag);
  }

  /**
   * Sends an email message using the given parameters.
   * @param fromAddrStr the "from" address to use.
   * @param recipientsStr the list of recipient email addresses.
   * @param subjectStr the subject text to use, or null for none.
   * @param bodyTextStr the message-body text to use.
   * @param sentDateObj the "sent" date to use, or null for no date in
   * message.
   * @return true if successful; false if not (in which case an error
   * message may be fetched via the 'getErrorMessageString()' method).
   */
  public boolean sendEmailMessage(String fromAddrStr, String recipientsStr,
                    String subjectStr, String bodyTextStr, Date sentDateObj)
  {
    return sendEmailMessage(fromAddrStr,null,recipientsStr,subjectStr,
                                              bodyTextStr,sentDateObj,true);
  }

  /**
   * Sends an email message using the given parameters.  A "date" header
   * showing the current date will be included in the message.
   * @param fromAddrStr the "from" address to use.
   * @param recipientsStr the list of recipient email addresses.
   * @param subjectStr the subject text to use, or null for none.
   * @param bodyTextStr the message-body text to use.
   * @return true if successful; false if not (in which case an error
   * message may be fetched via the 'getErrorMessageString()' method).
   */
  public boolean sendEmailMessage(String fromAddrStr, String recipientsStr,
                                      String subjectStr, String bodyTextStr)
  {
    return sendEmailMessage(fromAddrStr,null,recipientsStr,subjectStr,
                                             bodyTextStr,(new Date()),true);
  }

  /**
   * Creates an email session object.
   * @param initialFlag true to use the "initial" SMTP-timeout configuration
   * value; false to use the "sending" SMTP-timeout configuration value.
   * @return A new 'Session' object, or null if an error occurred
   * (in which case an error message may be fetched via the
   * 'getErrorMessageString()' method).
   */
  protected Session createMailSession(boolean initialFlag)
  {
    final Properties propsObj = new Properties();     //create props obj
                                  //put in name of SMTP server:
    String host = smtpServerAddressProp.stringValue();
    int port = -1;
    // check if port was specified
    int portIndex = host.lastIndexOf(":");
    if (portIndex > 0)
    {
      try
      {
        String s = host.substring(portIndex + 1);
        port = Integer.parseInt(s);
        propsObj.setProperty("mail.smtp.port", s);
        host = host.substring(0, portIndex);
      }
      catch (Exception ex)
      {
        setErrorMessageString("Error setting up email session object:  " + ex);
        return null;
      }
    }
    propsObj.setProperty("mail.smtp.host", host);
              //setup timeout value, "initial" or "sending", based on flag:
    final String timeoutValStr = Integer.toString((initialFlag ?
                                         initSMTPTimeoutSecProp.intValue() :
                                 sendSMTPTimeoutSecProp.intValue()) * 1000);
              //enter timeout values:
    propsObj.setProperty("mail.smtp.connectiontimeout",timeoutValStr);
    propsObj.setProperty("mail.smtp.timeout",timeoutValStr);
    propsObj.setProperty("mail.smtp.sendpartial",    //set send-partial flag
         (Boolean.valueOf(!dontSendPartialFlagProp.booleanValue())).toString());
    // check if authentication should be used
    final String authText = smtpServerAuthProp.stringValue();
    if (authText.length() > 0)
    {
      final ArrayList authList;
      if (authText.charAt(0) == QUOTE_CHAR)
      {
    	  authList = UtilFns.parseSeparatedSubstrsToList(
    			  authText, ",", QUOTE_CHAR, false, true);
      }
      else
      {
    	  authList = UtilFns.parseSeparatedSubstrsToList(authText, ",");
      }
      transportObj.setAuthentication(true);
      transportObj.setUser(authList.get(0).toString());
      if (authList.size() > 1)
      {
        transportObj.setPassword(authList.get(1).toString());
      }
      transportObj.setPort(port);
      transportObj.setHost(host);
      propsObj.setProperty("mail.smtp.auth", "true");
      if (authList.contains("STARTTLS"))
      {
        propsObj.setProperty("mail.smtp.starttls.enable", "true");
        propsObj.setProperty("mail.smtp.starttls.required", "true");
      }
      if (authList.contains("SSL"))
      {
        propsObj.setProperty("mail.smtp.ssl.enable", "true");
        propsObj.setProperty("mail.smtp.socketFactory.class", "javax.net.ssl.SSLSocketFactory");
        propsObj.setProperty("mail.smtp.socketFactory.fallback", "false");
        propsObj.setProperty("mail.smtp.socketFactory.port", Integer.toString(port));
      }
    }
    final Session sessionObj;
    try
    {                           //create session object:
      sessionObj = Session.getInstance(propsObj,transportObj.getAuthenticator());
    }
    catch(Exception ex)
    {            //some kind of error; setup error message
      setErrorMessageString("Error setting up email session object:  " + ex);
      return null;
    }
                                       //setup session-debug flag:
    sessionObj.setDebug(mailDebugFlagProp.booleanValue());
    return sessionObj;
  }


  /**
   * Returns the 'CfgProperties' object holding the configuration-property
   * items.  If a 'CfgProperties' object parameter is passed in to the
   * constructor then this method will return it.
   * @return The 'CfgProperties' object holding the configuration-property
   * items.
   */
  public CfgProperties getConfigProps()
  {
    return configPropsObj;
  }

  /**
   * Returns the 'CfgProperties' object holding the local
   * configuration-property items (related to mail settings).
   * @return The 'CfgProperties' object holding the local
   * configuration-property items (related to mail settings).
   */
  public CfgProperties getLocalProps()
  {
    return localPropsObj;
  }

  /**
   * Returns the last nested exception for the given 'MessagingException'.
   * @param throwableObj the 'MessagingException' object to use.
   * @return The last nested exception, or null if none found.
   */
  public static Exception getLastMessagingException(Throwable throwableObj)
  {
    if(!(throwableObj instanceof MessagingException))
      return null;           //if wrong type then return null
    Exception ex = (Exception)throwableObj, nextExObj = null;
    while(true)
    {    //for each level of nesting; get next exception object
      if((ex=((MessagingException)ex).getNextException()) == null)
        break;               //if no more then exit loop
      nextExObj = ex;        //save nested exception object
      if(!(ex instanceof MessagingException))
        break;               //if not 'MessagingException' then exit loop
    }
    return nextExObj;
  }


  /**
   * Test program.
   * @param args string array of command-line parameters.
   */
//  public static void main(String [] args)
//  {
//    final JavaMailUtil mailUtilObj = new JavaMailUtil();
//    final String hostStr = (args.length > 1) ? args[1] : "smtp.gmail.com:587";
//    System.out.println("Testing SMTP connection to \"" + hostStr + "\"");
//    mailUtilObj.mailDebugFlagProp.setValue(true);
//    mailUtilObj.smtpServerAddressProp.setValue(hostStr);
//    mailUtilObj.smtpServerAuthProp.setValue("isti.test@gmail.com,sta3?x@J,STARTTLS");
//    mailUtilObj.setXMailerHeaderString("JavaMailUtilTester");
//
//    if(mailUtilObj.testServerConnection())
//      System.out.println("Connection test successful");
//    else
//      System.err.println("Error:  " + mailUtilObj.getErrorMessageString());
//
//    final String recipStr = (args.length > 0) ? args[0] : "name@example.com";
//    System.out.println("Sending a test email message to <" + recipStr + ">");
//    if(!mailUtilObj.sendEmailMessage(recipStr,"JavaMailUtilTest",
//                                  ("\"Test Recipient\" <" + recipStr + ">"),
//                                                "JavaMailUtil Test Message",
//                                     "This is a JavaMailUtil test message"))
//    {
//      System.err.println("Error:  " + mailUtilObj.getErrorMessageString());
//    }
//  }
}
