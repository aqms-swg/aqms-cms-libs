//LoginInfoFileProc.java:  Processes a file containing server-login
//                         information.
//
//  11/1/2004 -- [ET]  Initial version.
//   2/7/2005 -- [ET]  Changed log level of "info file not found" message
//                     from 'debug2' to 'debug'.
//  12/1/2005 -- [ET]  Modified to copy login-information resources from
//                     'QWServerLoginInformation' object held by config-prop
//                     item given in constructor to the newly-created info
//                     object (to help with web-services server support).
//

package com.isti.quakewatch.util;

import java.io.FileOutputStream;
import java.io.FileNotFoundException;
import com.isti.util.UtilFns;
import com.isti.util.FileUtils;
import com.isti.util.LogFile;
import com.isti.util.CfgProperties;
import com.isti.util.CfgPropItem;
import com.isti.util.propertyeditor.LoginInformation;

/**
 * Class LoginInfoFileProc processes a file containing server-login
 * information.  If a 'password' or 'stdEncPassword' item is given
 * then it is converted to an 'encryptedPassword' item and the
 * file is updated.
 */
public class LoginInfoFileProc
{
    /** Flag set true if login-info file processed successfully. */
  protected boolean processSuccessFlag = false;

    /** Table of config-prop items for the login-information file. */
  protected final CfgProperties infoCfgProps = new CfgProperties();
    /** Username property for login-information file. */
  protected final CfgPropItem usernameProp =
                          infoCfgProps.add("username",UtilFns.EMPTY_STRING);
    /** Plain-text-password property for login-information file. */
  protected final CfgPropItem passwordProp =
                          infoCfgProps.add("password",UtilFns.EMPTY_STRING);
    /** Standard-encoded-password property for login-information file. */
  protected final CfgPropItem stdEncPasswordProp =
                    infoCfgProps.add("stdEncPassword",UtilFns.EMPTY_STRING);
    /** Encrypted-password property for login-information file. */
  protected final CfgPropItem encryptedPasswordProp =
                 infoCfgProps.add("encryptedPassword",UtilFns.EMPTY_STRING);

  /**
   * Creates a login-information-file processor.
   * @param fNameStr the name of the login-information file to use.
   * @param serverLoginProp the server-login-cfg-prop item to update,
   * or null for none.
   * @param logObj the 'LogFile' object to use, or null for no log output.
   */
  public LoginInfoFileProc(String fNameStr,
                                CfgPropItem serverLoginProp, LogFile logObj)
  {
    if(fNameStr == null || fNameStr.trim().length() <= 0)
    {    //filename not specified
      logObj.debug2(
            "LoginInfoFileProc:  Login-information filename not specified");
      return;
    }
    try
    {
      if(logObj == null)
        logObj = LogFile.getNullLogObj();
      if(infoCfgProps.load(FileUtils.readFileToString(fNameStr)))
      {  //login-info file loaded OK
        if(usernameProp.getLoadedFlag())
        {     //'username' item was specified
          boolean encPwdPropFlag = false;
          final QWServerLoginInformation loginInfoObj;
          if(passwordProp.stringValue().length() <= 0)
          {   //'password' (plain-text) item not given
            if(stdEncPasswordProp.stringValue().length() <= 0)
            {      //'stdEncPassword' item not given
              final String encPwdPropStr =       //get enc-pwd prop value:
                                        encryptedPasswordProp.stringValue();
                        //set flag if enc-pwd prop string not empty:
              if(encPwdPropFlag = (encPwdPropStr.length() > 0))
              {
                logObj.debug2("LoginInfoFileProc:  Using encrypted " +
                                "password from login-information file (\"" +
                                                          fNameStr + "\")");
              }
              else
              {
                logObj.debug("LoginInfoFileProc:  Empty password data " +
                                  "loaded from login-information file (\"" +
                                                          fNameStr + "\")");
              }
                        //if enc-pwd prop string given then use it as the
                        // encoded password; otherwise use blank password:
              loginInfoObj = new QWServerLoginInformation(
                                                 usernameProp.stringValue(),
                                              encPwdPropStr,encPwdPropFlag);
                        //force flag to indicate enc-pwd prop given
                        // so that if no password was specified the
                        // login-info file will remain unchanged:
              encPwdPropFlag = true;
            }
            else
            {      //'stdEncPassword' item was given
              logObj.debug2("LoginInfoFileProc:  Using standard " +
                                "password from login-information file (\"" +
                                                          fNameStr + "\")");
              loginInfoObj = new QWServerLoginInformation(
                                            usernameProp.stringValue(),true,
                                          stdEncPasswordProp.stringValue());
            }
          }
          else
          {   //'password' (plain-text) item was given
            logObj.debug2("LoginInfoFileProc:  Using plain-text " +
                                "password from login-information file (\"" +
                                                          fNameStr + "\")");
            loginInfoObj = new QWServerLoginInformation(
                                                 usernameProp.stringValue(),
                                                passwordProp.stringValue());
          }
                   //enter new login-info into given config-prop object:
          if(serverLoginProp != null)
          {   //config-prop object OK
            final Object obj;     //copy resources into new login-info obj:
            if((obj=serverLoginProp.getValue()) instanceof LoginInformation)
              loginInfoObj.copyLoginInfoResources((LoginInformation)obj);
            serverLoginProp.setValue(loginInfoObj);   //enter new info obj
          }
          if(!encPwdPropFlag)
          {   //encrypted password was not loaded from login-info file
            logObj.debug2("LoginInfoFileProc:  Encrypting password and " +
                                     "updating login-information file (\"" +
                                                          fNameStr + "\")");
                   //fetch encrypted pwd and set 'encryptedPassword' prop:
            encryptedPasswordProp.setValue(
                                   loginInfoObj.getEncryptedPasswordText());
                   //create cfg-props object with only 'username'
                   // and 'encryptedPassword' items:
            final CfgProperties tempCfgProps = new CfgProperties();
            tempCfgProps.add(usernameProp);
            tempCfgProps.add(encryptedPasswordProp);
            final FileOutputStream outStm = new FileOutputStream(fNameStr);
                   //store properties data to login-info file
                   // (save all props, no header, not cmdln-only):
            tempCfgProps.store(outStm,null,false,false,false);
            outStm.close();            //close output file
          }
          processSuccessFlag = true;   //indicate processed successfully
        }
        else
        {     //'username' item not specified; log error message
          logObj.warning("No '" + usernameProp.getName() +
                 "' item in login-information file (\"" + fNameStr + "\")");
        }
      }
      else
      {  //error loading login-info file; log error message
        logObj.warning("Error loading login-information file (\"" +
                      fNameStr + "\"):  " + infoCfgProps.getErrorMessage());
      }
    }
    catch(FileNotFoundException ex)
    {    //file not found; log debug message
      logObj.debug("LoginInfoFileProc:  Login-information file (\"" +
                                                fNameStr + "\") not found");
    }
    catch(Exception ex)
    {    //some kind of exception error; log warning message
      logObj.warning("Error processing login-information file (\"" +
                                                  fNameStr + "\"):  " + ex);
      logObj.warning(UtilFns.getStackTraceString(ex));
    }
  }

  /**
   * Returns an indicator of whether or data was successfully
   * loaded and processed from the login-information file.
   * @return true if data was successfully loaded and processed
   * from the login-information file; false if not.
   */
  public boolean getProcessSuccessFlag()
  {
    return processSuccessFlag;
  }

  /**
   * Process a login-information file.
   * @param fNameStr the name of the login-information file to use.
   * @param serverLoginProp the server-login-cfg-prop item to update,
   * or null for none.
   * @param logObj the 'LogFile' object to use, or null for no log output.
   * @return true if data was successfully loaded and processed
   * from the login-information file; false if not.
   */
  public static boolean process(String fNameStr,
                                CfgPropItem serverLoginProp, LogFile logObj)
  {
    return (new LoginInfoFileProc(
                  fNameStr,serverLoginProp,logObj)).getProcessSuccessFlag();
  }


//  /**
//   * Test program.
//   * @param args string array of command-line parameters.
//   */
//  public static void main(String [] args)
//  {
//    final LogFile logObj = new LogFile(
//                                     null,LogFile.NO_MSGS,LogFile.ALL_MSGS);
//    final CfgPropItem serverLoginProp = new CfgPropItem("TestServerLogin",
//            new QWServerLoginInformation()).setEmptyStringDefaultFlag(true);
//    if(LoginInfoFileProc.process("TestLoginInfo.dat",serverLoginProp,logObj))
//      logObj.info("LoginInfoFileProc Test:  Data processed successfully");
//    else
//      logObj.warning("LoginInfoFileProc Test:  Error processing data");
//    logObj.info("serverLoginProp:  " + serverLoginProp);
//    logObj.close();
//  }
}
