//QWAbstractConnector.java:  Defines the interface for the connector
//                           modules used by the 'QWConnectionMgr', and
//                           some common functionality.
//
//   9/9/2005 -- [ET]  Initial version.
//   3/5/2008 -- [ET]  Added methods 'isStatusReportDataAvail()',
//                     'getStatusReportTime()', 'getStatusReportData()'
//                     and 'getServerRepHostAddrStr()'.
//  4/14/2008 -- [ET]  Added 'StatusCheckFailedException' class and added
//                     it to 'performClientStatusCheck()' method
//                     declaration.
//  8/30/2010 -- [ET]  Added optional 'logLevelVal' parameter to method
//                     'setConnPanelInitError()'.
//  10/6/2010 -- [ET]  Added 'causeExObj' parameter to constructor for
//                     'StatusCheckFailedException' class.
//  11/9/2010 -- [ET]  Changed 'setConnStatusRefocusComponent()' parameter
//                     type from 'Component' to 'Object'.
//

package com.isti.quakewatch.util;

import com.isti.openorbutil.EvtChEventType;
import com.isti.util.UtilFns;
import com.isti.util.LogFile;
import com.isti.quakewatch.common.MsgTag;

/**
 * Class QWAbstractConnector defines the interface for the connector
 * modules used by the 'QWConnectionMgr', and some common functionality.
 */
public abstract class QWAbstractConnector
{
    //maximum number of retries for request-messages methods:
  protected static final int REQMSGS_NUM_RETRIES = 2;
  protected final ConnStatusInterface connStatusObj;
  protected final LogFile logObj;
  protected String filterDomainTypeListStr = null;
  protected EvtChEventType [] filterEventTypeArr = null;
  protected boolean connectDoneFlag = false;     //true after connect done
  protected boolean disconnectDoneFlag = false;  //true after disconnect done
  protected boolean connInvalidatedFlag = false; //true if connection bad
  protected boolean connAttemptStartedFlag = false;   //true if conn started
  protected boolean connAttemptFailedFlag = false;    //true if conn failed
  protected boolean connAttemptRejectedFlag = false;  //true if conn rejected
              //flag set true after client disconn'd from server services:
  protected boolean disconnectClientServicesFlag = false;
              //user-name string for client connection:
  protected String connectionUserNameString = null;
              //password string for client connection:
  protected String connectionPasswordString = null;
              //information-properties string for client connection:
  protected String connectionInfoPropsString = null;
              //name ID string for connected server:
  protected String serverIdNameString = null;
              //host address reported by connected server:
  protected String serverRepHostAddrString = null;
              //name ID string for connected server:
  protected String serverRevisionString = null;
  protected final ServerHostInfo serverHostInfoObj = new ServerHostInfo();
              //QWServer 'Acceptor' ID that should be rejected:
  protected String acceptorRejectIDString = null;
              //reason QWServer 'Acceptor' ID should be rejected:
  protected String acceptorRejectReasonStr = null;
              //type-name string for status messages:
  protected String statusMsgTypeNameStr = null;
              //"previous" value for 'statusMsgTypeNameStr':
  protected String prevStatusMsgTypeNameStr = null;
              //array of data from certificate file:
  protected byte [] certificateFileDataArr = null;
              //set true if get-status-report methods available on server:
  protected boolean statusReportDataAvailFlag = false;
              //utility object for decrypting received status reports:
  protected EncryptDecryptUtil encryptDecryptUtilObj = null;


  /**
   * Creates an abstract connector object.
   * @param connStatusObj connection-status panel object, or null for none.
   * @param logObj log-file object to be used, or null for none.
   */
  public QWAbstractConnector(ConnStatusInterface connStatusObj,
                                                             LogFile logObj)
  {
    this.connStatusObj = connStatusObj;
              //setup handle to log file; if null then setup dummy log file
    this.logObj = (logObj != null) ? logObj :
                          new LogFile(null,LogFile.NO_MSGS,LogFile.NO_MSGS);
  }

  /**
   * Configures this connector's event-message consumer to filter-out
   * (reject) all messages except those with event domain and type
   * names matching the ones specified in the given list string.  The
   * list string is also used when resend-requesting messages from the
   * server to filter which messages are returned.  The list string must
   * be in the format "domain:type,domain:type...".  Occurrences of the
   * ':' and ',' characters not meant as separators may be "quoted" by
   * preceding them with the backslash ('\') character.  List items
   * missing the ':' character will be considered to specify only a
   * domain name (the type name will be an empty string).  If this
   * method is not called then all incoming message will be accepted.
   * If the list string contains data then an entry for the event domain
   * and type name values for status messages ("StatusMessage:serverID/Addr")
   * is appended to the list.
   * @param listStr the list string to use.
   */
  public void setFilterDomainTypeList(String listStr)
  {
    if(filterDomainTypeListStr == null || listStr == null ||
                                   !listStr.equals(filterDomainTypeListStr))
    {    //first time called or list string is different from last time
      if(listStr != null && listStr.trim().length() > 0)
      {  //given list string contains data
        filterDomainTypeListStr = listStr;       //save list string
                   //convert list string to array of event-type items:
        updateFilterDomainTypeList();
      }
      else
      {       //list string contains no data
        filterDomainTypeListStr = null;
        filterEventTypeArr = null;
      }
    }
  }

  /**
   * Builds the contents of the array of event-type objects for message
   * filtering.  This method is called when the list is initially setup
   * and again after the 'statusMsgTypeNameStr' value is retrieved from
   * the server.
   */
  protected void updateFilterDomainTypeList()
  {
    if(filterDomainTypeListStr != null)
    {    //list of domain/type names contains data
      if(prevStatusMsgTypeNameStr == null ||
                     !prevStatusMsgTypeNameStr.equals(statusMsgTypeNameStr))
      {  //no previous 'statusMsgTypeNameStr' value or value is changing
                   //build array of event-type objects from list-string:
        filterEventTypeArr = QWUtils.listStringToEventTypeArray(
                              filterDomainTypeListStr,MsgTag.STATUS_MESSAGE,
                    ((statusMsgTypeNameStr!=null)?statusMsgTypeNameStr:""));
        prevStatusMsgTypeNameStr = statusMsgTypeNameStr;   //track value
      }
    }
    else      //list of domain/type names does not contain data
      filterEventTypeArr = null;
  }

  /**
   * Updates the connection parameters to be used on the next initialization.
   * @param clientHostAddress an IP address to be associated with this
   * client, or null to have the address auto-determined.
   * @param clientPortNum a port address value to be associated with this
   * client, or 0 to have the port address auto-determined.
   */
  public abstract void updateConnectParams(String clientHostAddress,
                                                         int clientPortNum);

  /**
   * Enters the connection-information values to be sent when the client
   * connects to the server.
   * @param userNameStr the user name string to use.
   * @param passwordStr the password string to use.
   * @param connPropsStr the connection-information properties string to
   * use.
   */
  public void enterConnectionInfo(String userNameStr,
                                    String passwordStr, String connPropsStr)
  {
    connectionUserNameString = userNameStr;
    connectionPasswordString = passwordStr;
    connectionInfoPropsString = connPropsStr;
  }

  /**
   * Sets the identifier for a QWServer 'Acceptor' that should be
   * rejected when a connection attempt is made.  This identifier
   * is used by the QWRelayFeeder to determine if the connected
   * server is its own QWServer.
   * @param accIDStr the QWServer 'Acceptor' identifer to use, or
   * null for none.
   * @param reasonStr a message string describing the reason why
   * the QWServer 'Acceptor' should be rejected.
   */
  public void setAcceptorRejectIDStr(String accIDStr, String reasonStr)
  {
    acceptorRejectIDString = accIDStr;
    acceptorRejectReasonStr = reasonStr;
    logObj.debug2(
            "QWAbstractConnector:  Entered 'Acceptor'-reject-ID string \"" +
                                             acceptorRejectIDString + "\"");
  }

  /**
   * Starts a new thread that establishes a connection to a QWServer.
   * @param serverHostAddrStr the host address used to locate the server.
   * @param serverHostPortNum the port number used to locate the server.
   * @param showPopupFlag true to force the connection status popup
   * dialog to be shown.
   */
  public abstract void startConnectViaHostInfo(String serverHostAddrStr,
                               int serverHostPortNum,boolean showPopupFlag);

  /**
   * Starts a new thread that connects to the event channel via a
   * connection to a QWServer.
   * @param serverHostAddrStr the host address used to locate the server.
   * @param serverHostPortNum the port number used to locate the server.
   */
  public void startConnectViaHostInfo(String serverHostAddrStr,
                                                      int serverHostPortNum)
  {
    startConnectViaHostInfo(serverHostAddrStr,serverHostPortNum,false);
  }

  /**
   * Shuts down the connection and waits for the disconnect to
   * complete or timeout.
   * @param showPopupFlag true to force the connection status popup
   * dialog to be shown.
   */
  public abstract void shutdownConnection(boolean showPopupFlag);

  /**
   * Disconnects this client from the server services.
   * @param sepThreadFlag true to use a separate thread and enforce a
   * one second timeout; false to use the calling thread.
   */
  public void disconnectClientServices(boolean sepThreadFlag)
  {
    final String dcsStr = "QWAbstractConnector.disconnectClientServices:  ";
    disconnectClientServicesFlag = false;        //initialize flag
    if(sepThreadFlag)
    {    //use separate thread
      if(!getInitializedFlag())        //if connection not initialized then
        return;                        //abort method
      (new Thread("disconnectClientServices")
        {
          public void run()
          {
            logObj.debug3(dcsStr + "Starting disconnect thread");
            doDisconnectClientServices();
            logObj.debug3(dcsStr + "Finished disconnect thread");
          }
        }).start();
      int c = 0;
      while(true)
      {  //wait for disconnect to complete, up to 1 second
        try { Thread.sleep(10); }           //delay 10 milliseconds
        catch(InterruptedException ex) {}
        if(disconnectClientServicesFlag)
          break;        //if disconnect finished then exit loop
        if(++c >= 100)
        {     //timeout reached; log result message
          logObj.debug(dcsStr +
              "Timeout waiting for client disconnect from server services");
          break;        //exit loop
        }
      }
    }
    else      //use same thread
      doDisconnectClientServices();
  }

  /**
   * Disconnects this client from the server services.  A separate
   * thread is used and a one second timeout is enforced.
   */
  public void disconnectClientServices()
  {
    disconnectClientServices(true);
  }

  /**
   * Performs the work of disconnecting this client from the server
   * services.
   * @return true if successful; false if an error occurred.
   */
  protected abstract boolean doDisconnectClientServices();

  /**
   * Returns the server ID name string (defined in the QWServer's
   * configuration file).
   * @return The server ID name string, or null if not available.
   */
  public abstract String getServerIDNameStr();

  /**
   * Returns the host address reported by the server.
   * @return The host address reported by the server, or null if not
   * available.
   */
  public abstract String getServerRepHostAddrStr();

  /**
   * Returns the server revision string.
   * @return The server revision string, or null if not available.
   */
  public abstract String getServerRevisionStr();

  /**
   * Requests that the server send an 'Alive' message immediately.
   * This allows the connection to the event channel to be verified more
   * quickly.
   */
  public abstract void requestServerAliveMsg();

  /**
   * Returns an indicator of whether or not the 'requestMessages()'
   * method is available via the 'QWServices' on the current server.
   * @return true if the 'requestServerMessages()' method is available,
   * false if not.
   */
  public abstract boolean isReqServerMsgsAvailable();

  /**
   * Clears the connection-attempt-started flag.  This flag is set when
   * the connection thread starts up.
   */
  public void clearConnAttemptStartedFlag()
  {
    connAttemptStartedFlag = false;
  }

  /**
   * Returns the connection-attempt-started flag.  This flag is set when
   * the connection thread starts up.
   * @return The connection-attempt-started flag.
   */
  public boolean getConnAttemptStartedFlag()
  {
    return connAttemptStartedFlag;
  }

  /**
   * Clears the connection-attempt-failed flag.  This flag is set after
   * a connection attempt has failed.
   */
  public void clearConnAttemptFailedFlag()
  {
    connAttemptFailedFlag = false;
  }

  /**
   * Returns the connection-attempt-failed flag.  This flag is set after
   * a connection attempt has failed.
   * @return The connection-attempt-failed flag.
   */
  public boolean getConnAttemptFailedFlag()
  {
    return connAttemptFailedFlag;
  }

  /**
   * Clears the connection-attempt-rejected flag.  This flag is set after
   * a connection attempt was rejected (probably because of invalid login).
   */
  public void clearConnAttemptRejectedFlag()
  {
    connAttemptRejectedFlag = false;
  }

  /**
   * Returns the connection-attempt-rejected flag.  This flag is set after
   * a connection attempt was rejected (probably because of invalid login).
   * @return The connection-attempt-rejected flag.
   */
  public boolean getConnAttemptRejectedFlag()
  {
    return connAttemptRejectedFlag;
  }

  /**
   * Requests that messages newer or equal to the specified time value or
   * later than the specified message number be returned from the server.
   * If a 'hostMsgNumListStr' value is not given and a non-zero message
   * number is given and the given time value matches then messages with
   * message numbers greater than the given message number are returned;
   * otherwise messages newer or equal to the specified time value are
   * returned (within a tolerance value).  If a 'hostMsgNumListStr' value
   * is given then it is used with the QWServices "requestSourced...()"
   * methods (and any given message number is ignored).  An XML message
   * containing the messages is returned, using the following format: <pre>
   * [QWresend NumRequested="10"]
   *   [QWmessage ...] [/QWmessage]
   *   [QWmessage ...] [/QWmessage]
   *   ...
   * [/QWresend]
   * </pre> (Substitute angle brackets for square brackets.)
   * The number of messages that would be required to fill the request
   * is returned in the "NumRequested" attribute.  However, no more than
   * the oldest 'maxResendNumMsgs' number of messages will be returned
   * in any single call to this method (additional calls may be needed to
   * fetch all of the desired messages).
   * @param timeVal the time-generated value for message associated with
   * the given message number, or the requested time value to be used
   * (milliseconds since 1/1/1970).
   * @param msgNum the message number to use, or 0 or none.
   * @param hostMsgNumListStr a list of feeder-data-source
   * host-name/message-number entries, or null or empty string
   * if the "requestSourced...()" methods should not be used.
   * @return An XML-formatted string containing the messages, or an empty
   * string if an error occurs.
   */
  public abstract String requestServerMessages(long timeVal,long msgNum,
                                                  String hostMsgNumListStr);

  /**
   * Calls the QWServices 'clientStatusCheck()' method.
   * @return true if the an updated version of the client is available;
   * false if not.
   * @throws NoSuchMethodException if the 'clientStatusCheck()' method
   * is not implemented on the server.
   * @throws StatusCheckFailedException if the call to 'clientStatusCheck()'
   * failed with an exception other than 'NoSuchMethodException'.
   */
  public abstract boolean performClientStatusCheck()
                   throws NoSuchMethodException, StatusCheckFailedException;

  /**
   * Returns information about available client-program upgrades via
   * the QWServices 'getClientUpgradeInfo()' method.
   * @return An XML-formatted string containing information about
   * available client-program upgrades, or null if an error occurred.
   */
  public abstract String fetchClientUpgradeInfoFromServer();

  /**
   * Returns the location of the server that the client is
   * being redirected to.  The returned string may also be
   * a comma-separated list of "hostAddr:portNum" entries.
   * @return The redirect-server location string in the form
   * "hostAddr:portNum", an empty string if the client is
   * not being redirected, or null if an error occurred.
   */
  public abstract String fetchRedirectedServerLoc();

  /**
   * Returns the "initialized" status of the connection.
   * @return true if the connection is initialized.
   */
  public abstract boolean getInitializedFlag();

  /**
   * Returns the connection status.  If 'invalidateConnection()' has been
   * called then this method will return false until the connection is
   * reinitialized.
   * @return true if the connection is active, false if not.
   */
  public abstract boolean getConnectedFlag();

  /**
   * Marks the connection as "invalidated", causing the
   * 'getConnectedFlag()' method to return 'false'.
   */
  public abstract void invalidateConnection();

  /**
   * Determines if get-status-report methods are available on the server.
   * @return true if get-status-report methods are available on the server;
   * false if not.
   */
  public abstract boolean isStatusReportDataAvail();

  /**
   * Fetches the timestamp value for the latest status report from the
   * server.
   * @return The timestamp value for the latest status report from the
   * server, or 0 if no report is available or if an error occurred.
   */
  public abstract long getStatusReportTime();

  /**
   * Fetches the latest status-report data from the server.
   * @return A string containing the latest status-report data from
   * the server, an empty string if no report is available, or null
   * if an error occurred.
   */
  public abstract String getStatusReportData();

  /**
   * Fetches and decodes the latest status report from the server.
   * @return A string containing the latest status report from
   * the server, an empty string if no report is available, or null
   * if an error occurred.
   */
  public String getDecodedStatusReport()
  {
    if(encryptDecryptUtilObj == null)
    {  //decrypter object not yet created; create one now
      encryptDecryptUtilObj =
         new EncryptDecryptUtil(serverIdNameString,serverRepHostAddrString);
    }
    final String dataStr = getStatusReportData();
    final String retStr;     //if decrypt OK return result; else orig str
    return ((retStr=encryptDecryptUtilObj.decrypt(dataStr)) != null) ?
                                                           retStr : dataStr;
  }

  /**
   * Returns an identification string for the connected server.  This
   * string is the server ID name (if available), or the host address
   * and port number for the server.
   * @return An identification string for the connected server, or an
   * empty string if not available.
   */
  public String getServerIDString()
  {
    final String svrIdStr;   //get server ID name:
    if((svrIdStr=getServerIDNameStr()) != null && svrIdStr.length() > 0)
    {
      return svrIdStr;
    }
    return serverHostInfoObj.getServerAddrPortString();
  }

  /**
   * Returns a more-detailed identification string for the configured
   * server.  This string contains the server ID name and the "address:
   * port" ID for the server.
   * @return An identification string for the configured server, or an
   * empty string if not available.
   */
  public String getDetailedServerIDString()
  {
                             //get configured host address:
    final String addrStr = serverHostInfoObj.getServerAddrPortString();
    final String svrIdStr;   //get server ID name:
    if((svrIdStr=getServerIDNameStr()) != null && svrIdStr.length() > 0)
    {    //ID name exists; append configured host "addr:port" (if given)
      return (addrStr.length() > 0) ?
                                ("\"" + svrIdStr + "\" (" + addrStr + ")") :
                                                   ("\"" + svrIdStr + "\"");
    }
    return addrStr;     //if no server ID name then return "addr:port"
  }

  /**
   * Returns the certificate-file data that was fetched from the server
   * at connect time.
   * @return The certificate-file data that was fetched from the server
   * at connect time, or null if none was fetched.
   */
  public byte [] getCertificateFileDataArr()
  {
    return certificateFileDataArr;
  }

  /**
   * Returns the connection status interface object.
   * @return An object that implements 'ConnStatusInterface', or null if
   * none available.
   */
  public ConnStatusInterface getConnPanelObj()
  {
    return connStatusObj;
  }

  /**
   * Sets the component that is to receive focus after the connection
   * status popup dialog is cleared.
   * @param compObj the compoment to receive focus.
   */
  public void setConnStatusRefocusComponent(Object compObj)
  {
    if(connStatusObj != null)
      connStatusObj.setRefocusComponent(compObj);
  }

  /**
   * Displays the popup dialog showing extra connection status information
   * in response to a user request.
   */
  public void userRequestShowConnStatusPopupDialog()
  {
    if(connStatusObj != null)
      connStatusObj.userRequestShowPopupDialog();
  }

  /**
   * Clears any displayed connection status popup dialog if the user
   * has not requested that it be shown.
   */
  public void autoClearConnStatusPopupDialog()
  {
    if(connStatusObj != null)
      connStatusObj.autoClearPopupDialog();
  }

  /**
   * Shows the popup dialog displaying extra status information for the
   * connection status panel.
   */
  public void showConnPanelPopup()
  {
    if(connStatusObj != null)
      connStatusObj.showPopupDialog();
  }

  /**
   * Sets the message strings for the connection status panel and popup,
   * as well as the color for the symbol on the panel.
   * @param textStr text for panel, or null for no change.
   * @param popupStr text for popup, or null for no change.
   * @param symColorIdx index of color for symbol (one of the
   * ConnStatusInterface '..._COLOR_IDX' values), or 'NULL_COLOR_IDX'
   * for no change.
   */
  public void setConnPanelData(String textStr,String popupStr,
                                                            int symColorIdx)
  {
    if(connStatusObj != null)
      connStatusObj.setData(textStr,popupStr,symColorIdx);
  }

  /**
   * Sets the message strings for the connection status panel and popup.
   * @param textStr text for panel, or null for no change.
   * @param popupStr text for popup, or null for no change.
   */
  public void setConnPanelData(String textStr,String popupStr)
  {
    if(connStatusObj != null)
      connStatusObj.setData(textStr,popupStr);
  }

  /**
   * Sets connection status panel to show an initialization error.
   * @param popupTextStr message to show in the popup panel.
   * @param logLevelVal log level for message sent to log file.
   */
  protected void setConnPanelInitError(String popupTextStr, int logLevelVal)
  {
    int ePos;      //find newline in text; use end-of-string pos if none:
    if((ePos=popupTextStr.indexOf(UtilFns.newline)) < 0)
      ePos = popupTextStr.length();
                   //only show first line of text in popup message:
    setConnPanelData(QWConnectionMgr.NOTCONN_CLICKFDET_STR,
          popupTextStr.substring(0,ePos),ConnStatusInterface.RED_COLOR_IDX);
                   //log message (with full text):
    logObj.println(logLevelVal,"Connection init error:  " + popupTextStr);
  }

  /**
   * Sets connection status panel to show an initialization error.
   * @param popupTextStr message to show in the popup panel.
   */
  protected void setConnPanelInitError(String popupTextStr)
  {
    setConnPanelInitError(popupTextStr,LogFile.WARNING);
  }


  /**
   * Class ServerHostInfo holds host address string and port number
   * values and provides thread-safe methods for setting and fetching
   * the values.
   */
  protected static class ServerHostInfo
  {
    private String hostAddr = "";
    private int portNum = 0;

    /**
     * Sets the host values.
     * @param hostAddr the host address string.
     * @param portNum the host port number string.
     */
    public synchronized void setValues(String hostAddr,int portNum)
    {
      this.hostAddr = (hostAddr != null) ? hostAddr : "";
      this.portNum = portNum;
    }

    /**
     * Returns the host address string.
     * @return The host address string.
     */
    public synchronized String getHostAddr()
    {
      return hostAddr;
    }

    /**
     * Returns the host port number.
     * @return The host port number.
     */
    public synchronized int getPortNum()
    {
      return portNum;
    }

    /**
     * Returns a string representation of the host address and port number.
     * @return A string representation of the host address and port number
     * in the form "addr:port", or an empty string if no host address is
     * set.
     */
    public synchronized String getServerAddrPortString()
    {
      if(hostAddr.length() > 0)             //if host address exists then
        return hostAddr + ":" + portNum;    //add port number
      return "";        //if no host address then return empty string
    }
  }


  /**
   * Class StatusCheckFailedException is thrown by the
   * 'performClientStatusCheck()' method when the call to
   * 'clientStatusCheck()' fails with an exception other
   * than 'NoSuchMethodException'.
   */
  public static class StatusCheckFailedException extends Exception
  {
    /**
     * Creates a client-status-check-failed exception.
     * @param msgStr message string for exception.
     * @param causeExObj cause exception (or null for none).
     */
    public StatusCheckFailedException(String msgStr, Throwable causeExObj)
    {
      super(msgStr,causeExObj);
    }

    /**
     * Creates a client-status-check-failed exception.
     * @param msgStr message string for exception.
     */
    public StatusCheckFailedException(String msgStr)
    {
      super(msgStr);
    }
  }
}
