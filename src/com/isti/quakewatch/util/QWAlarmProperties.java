//QWAlarmProperties.java:  Manages the QuakeWatch client alarm configuration.
//
//   3/4/2004 -- [ET]  Moved items for alarm-triggering from
//                     'ClientAlarmConfig' into this class.
//   3/9/2004 -- [KF]  Moved logic for matching items for alarm-triggering from
//                     'ClientAlarmConfig' into this class.
//  3/12/2004 -- [ET]  Added constructor with parameters.
//  3/19/2004 -- [KF]  Moved 'checkAlarmEvent(QWEventMsgRecord)' from
//                     'ClientAlarmConfig' into this class,
//                     add optional region parameter to 'checkAlarmEvent'.
//  3/22/2004 -- [KF]  Added 'addAlarmPropertyItem' method.
//  3/24/2004 -- [ET]  Restored 'addAlarmPropertyItems()' method.
//  3/30/2004 -- [KF]  Fix order of parameters to the region 'contains' method.
//   4/5/2004 -- [KF]  Added support for alarm region magnitude.
//   4/8/2004 -- [KF]  Added 'regionsAlarmProp' property.
//  4/12/2004 -- [KF]  Do not put 'regionsAlarmProp' property in group.
//  4/27/2004 -- [ET]  Fixed bug in 'checkAlarmEvent()' that would make it
//                     trigger alarms on events with no magnitude value.
//   5/3/2004 -- [ET]  Added 'minAlarmDepth' property; fixed bug where if
//                     an alarm region was configured with no magnitude
//                     then events in the region of any magnitude would
//                     trigger the alarm.
//  6/10/2004 -- [ET]  Changed 'IstiRegion' reference from "com.isti.util"
//                     to "com.isti.util.gis" package.
//  7/15/2004 -- [KF]  Added 'alarmReason' parameter to 'checkAlarmEvent'.
//  7/16/2004 -- [KF]  Change max for 'alarmMagnitudeProp'from 10 to 9.9.
//  5/17/2005 -- [ET]  Changed to use modified 'QWEventMsgRecord' interface.
//   3/7/2006 -- [KF]  Added more region options.
//  3/14/2006 -- [KF]  Changed to use global values if value is not specified.
//  3/20/2006 -- [KF]  Added debug logging for alert alarm checking,
//                     Fixed problem with the maximum alarm age check.
//  3/31/2006 -- [ET]  Modified 'checkAlarmEvent()' to do "append(String)"
//                     instead of "append(StringBuffer)" because second
//                     version doesn't exist in Java 1.3 (resulting in
//                     'NoSuchMethodError' if compiled with Java 1.4 and
//                     run with Java 1.3); cosmetic changes.
//   4/7/2006 -- [ET]  Removed 'globalAlarmProperties' parameter from
//                     'checkAlarmEvent()' method; modified 'checkRegion()'
//                     method to only use alarm items from region (and not
//                     default to alarm items from property settings).
//   6/8/2006 -- [ET]  Added 'minAlarmAmplitude' property and support for
//                     amplitude events.
//

package com.isti.quakewatch.util;

import java.util.Date;
import com.isti.util.UtilFns;
import com.isti.util.CfgPropItem;
import com.isti.util.CfgProperties;
import com.isti.util.DataChangedListener;
import com.isti.util.gis.IstiRegion;
import com.isti.quakewatch.message.QWEventMsgRecord;
import com.isti.quakewatch.message.QWStationAmpMsgRecord;

/**
 * Class QWAlarmProperties manages the QuakeWatch client alarm property
 * items.
 */
public class QWAlarmProperties
{
  /** The bit flag for no alarm. */
  public final static int NO_ALARMS = 0x00;
  /** The bit flag for the visual alarm. */
  public final static int VISUAL_ALARM = 0x01;
  /** The bit flag for the audible alarm. */
  public final static int AUDIBLE_ALARM = 0x02;
  /** The bit flag for all alarm. */
  public final static int ALL_ALARMS = VISUAL_ALARM | AUDIBLE_ALARM;

  /** Thread-synchronization object for alarm configuration. */
  protected final Object alarmConfigSyncObject = new Object();

  /** Minimum magnitude for alarm. */
  public final CfgPropItem alarmMagnitudeProp =
                     (new CfgPropItem("alarmMagnitude",Double.valueOf(3.0),null,
                               "Minimum magnitude for alarm")).setValidator(
                                                   IstiRegion.magValidator);

  /** Minimum depth for alarm. */
  public final CfgPropItem minAlarmDepthProp =
                      (new CfgPropItem("minAlarmDepth",Double.valueOf(0.0),null,
                             "Minimum depth for alarm (km)")).setValidator(
                                                IstiRegion.depthValidator);

  /** Maximum depth for alarm (0.0 = no maximum). */
  public final CfgPropItem maxAlarmDepthProp =
                      (new CfgPropItem("maxAlarmDepth",Double.valueOf(0.0),null,
                              "Maximum depth for alarm (km)")).setValidator(
                                                 IstiRegion.depthValidator);

  /** Maximum alarm event age in hours (0=infinite). */
  public final CfgPropItem maxAlarmEventHoursProp =
                  (new CfgPropItem("maxAlarmEventHours",Integer.valueOf(4),null,
                                 "Maximum event age (hours)")).setValidator(
                                                   IstiRegion.ageValidator);

  /** Verified (reviewed) events trigger alarm. */
  public final CfgPropItem verifiedEventsAlarmFlagProp =
               new CfgPropItem("verifiedEventsAlarmFlag",Boolean.FALSE,null,
                                      "Only verified events trigger alarm");

  /** Regions for alarm. */
  public final CfgPropItem regionsAlarmProp =
                  new CfgPropItem("alarmRegions",UtilFns.EMPTY_STRING, null,
                                                           "Alarm regions");

  /** Minimum amplitude for alarm (not in alarm group). */
  public final CfgPropItem minAlarmAmplitudeProp =
                 (new CfgPropItem("minAlarmAmplitude",Double.valueOf(10.0),null,
              "Minimum amplitude for alarm")).setValidator(0.0,999999999.0);

  /** Obsolete:  Maximum depth for alarm ("alarmDepth"). */
  protected final CfgPropItem alarmDepthPropObs =
                         new CfgPropItem("alarmDepth",UtilFns.EMPTY_STRING).
                                                    setIgnoreItemFlag(true);

  //maximum alarm event age in milliseconds (0=infinite):
  private long maxAlarmEventMS = 0;

  /**
   * Creates an object for the QuakeWatch client alarm configuration.
   * Alarm configuration property items are added to the given
   * configuration properties object (if given).
   * @param settingsProps the configuration-properties object to use,
   * or null for none.
   * @param groupSelObj the configuration-group-selection object to use,
   * or null for none.
   */
  public QWAlarmProperties(CfgProperties settingsProps, Object groupSelObj)
  {
         //set local version of max alarm event age:
    maxAlarmEventMS = (long)(maxAlarmEventHoursProp.intValue()) *
                                                        UtilFns.MS_PER_HOUR;
         //setup change listener for "max alarm event age" property:
    maxAlarmEventHoursProp.addDataChangedListener(new DataChangedListener()
        {
          public void dataChanged(Object obj)
          {
            //update local version of variable
            synchronized(alarmConfigSyncObject)
            {
              maxAlarmEventMS = (long)(maxAlarmEventHoursProp.intValue()) *
                                                        UtilFns.MS_PER_HOUR;
            }
          }
        });
    if(settingsProps != null)     //if CfgProps obj given then add items
      addAlarmPropertyItems(settingsProps,groupSelObj);
  }

  /**
   * Creates an object for the QuakeWatch client alarm configuration.
   */
  public QWAlarmProperties()
  {
    this(null,null);
  }

  /**
   * Adds alarm-property items to the given configuration-properties
   * object.
   * @param settingsProps the configuration-properties object to use.
   * @param groupSelObj the configuration-group-selection object to use,
   * or null for none.
   */
  public final void addAlarmPropertyItems(CfgProperties settingsProps,
                                                         Object groupSelObj)
  {
    settingsProps.add(alarmMagnitudeProp).setGroupSelObj(groupSelObj);
    settingsProps.add(minAlarmDepthProp).setGroupSelObj(groupSelObj);
    settingsProps.add(maxAlarmDepthProp).setGroupSelObj(groupSelObj);
    settingsProps.add(maxAlarmEventHoursProp).setGroupSelObj(groupSelObj);
    settingsProps.add(verifiedEventsAlarmFlagProp).
                                                setGroupSelObj(groupSelObj);
         //add region alarms property but do not put in group:
    settingsProps.add(regionsAlarmProp);

         //add obsolete "alarmDepth" property to prevent error messages:
    settingsProps.add(alarmDepthPropObs);
  }

  /**
   * Returns the "Minimum amplitude for alarm" configuration
   * property item object.  This item object is created and held by
   * this alarm-properties object but is not added to the configuration
   * properties object or the alarm group.  Client programs that use
   * this configuration property item will fetch it via this method,
   * add it to the configuration properties and setup its group-select
   * object.
   * @return The "Minimum amplitude for alarm" configuration
   * property item object.
   */
  public CfgPropItem getMinAlarmAmplitudeProp()
  {
    return minAlarmAmplitudeProp;
  }

  /**
   * Checks if an event should start alarm.
   * @param recObj the event message record.
   * @return the bit flag for the alarm.
   */
  public int checkAlarmEvent(QWEventMsgRecord recObj)
  {
    return checkAlarmEvent(recObj, null);
  }

  /**
   * Checks if an event should start alarm.
   * @param recObj the event message record.
   * @param regionsArr the regions for the alarm or null if none.
   * @return the bit flag for the alarm.
   */
  public int checkAlarmEvent(QWEventMsgRecord recObj,
                                                   IstiRegion [] regionsArr)
  {
    return checkAlarmEvent(recObj,regionsArr,null);
  }

  /**
   * Checks if an event should start alarm.
   * @param recObj the event message record.
   * @param regionsArr the regions for the alarm or null if none.
   * @param alarmReason if supplied the reason the alarm should or should
   * not start be started.
   * @return the bit flag for the alarm.
   */
  public int checkAlarmEvent(
      QWEventMsgRecord recObj,IstiRegion [] regionsArr,StringBuffer alarmReason)
  {
    final double eventMagAmpVal;
    final Date eventTime;
    final boolean eventVerifedFlag;
    final double eventDepth;
    final double minAlarmDepth;
    final double maxAlarmDepth;
    final double minAlarmMagAmp;
    final long maxAlarmAgeMillis;
    final boolean verifiedEventsAlarmFlag;
    final boolean amplitudeFlag;
    final boolean audibleAlarmFlag;
    final boolean visualAlarmFlag;

         //get local copies of "global" alarm configuration parameters:
    synchronized(alarmConfigSyncObject)
    {
      if(amplitudeFlag = (recObj instanceof QWStationAmpMsgRecord))
      {  //amplitude event
        final Double amplitudeValObj =
                           ((QWStationAmpMsgRecord)recObj).getAmpValueObj();
        if (amplitudeValObj == null)
        {     //no amplitude for event
          if (alarmReason != null)
            addAlarmReason(alarmReason,"no amplitude");
          return NO_ALARMS; //do not start the alarm
        }
        eventMagAmpVal = amplitudeValObj.doubleValue();   //get amp val
      }
      else
      {  //earthquake magnitude event
        final Double magnitudeValObj = recObj.getMagnitudeObj();
        if (magnitudeValObj == null)
        {     //no magnitude for event
          if (alarmReason != null)
            addAlarmReason(alarmReason,"no magnitude");
          return NO_ALARMS; //do not start the alarm
        }
        eventMagAmpVal = magnitudeValObj.doubleValue();   //get mag val
      }
      eventTime = recObj.getTime();
      eventVerifedFlag = recObj.isVerified();
      final Double eventDepthObj = recObj.getDepthObj();
      if (eventDepthObj == null)
        eventDepth = 0.0;
      else
        eventDepth = eventDepthObj.doubleValue();

      //get values from alarm properties
      minAlarmMagAmp = amplitudeFlag ? minAlarmAmplitudeProp.doubleValue() :
                                           alarmMagnitudeProp.doubleValue();
      minAlarmDepth = minAlarmDepthProp.doubleValue();
      maxAlarmDepth = maxAlarmDepthProp.doubleValue();
      maxAlarmAgeMillis = maxAlarmEventMS;
      verifiedEventsAlarmFlag = verifiedEventsAlarmFlagProp.booleanValue();
      audibleAlarmFlag = true;
      visualAlarmFlag = true;
    }

    //check the global alarm settings
    int alarmFlags = NO_ALARMS;
    if ((alarmFlags=checkAlarm(eventMagAmpVal,eventTime,eventVerifedFlag,
                      eventDepth,minAlarmDepth,maxAlarmDepth,minAlarmMagAmp,
                 maxAlarmAgeMillis,verifiedEventsAlarmFlag,audibleAlarmFlag,
                  visualAlarmFlag,alarmReason,amplitudeFlag)) == ALL_ALARMS)
    {
      return alarmFlags;              //start the alarm
    }

    if (!amplitudeFlag && regionsArr != null)
    {    //not amplitude event and alarm regions exist
      final int numRegions = regionsArr.length;
      IstiRegion regionObj;
      StringBuffer regionAlarmReason = null;
      int regionAlarmFlags;

      for (int i=0; i<numRegions && alarmFlags != ALL_ALARMS; i++)
      {  //for each alarm region configured
        regionObj = regionsArr[i];
        if (regionObj.contains(recObj.getLatitude(), recObj.getLongitude()))
        {     //event located within alarm region
          if (alarmReason != null)
          {
            addAlarmReason(alarmReason,"region (flgs=" + alarmFlags + ") " +
                                                 regionObj.getName() + "-");
          }
          regionAlarmReason = new StringBuffer();
          if ((regionAlarmFlags=checkRegion(
              eventMagAmpVal,eventTime,eventVerifedFlag,eventDepth,
              regionAlarmReason,regionObj)) != NO_ALARMS)  //if region checks
          {
            alarmFlags |= regionAlarmFlags;  //add the flags
          }
          if (regionAlarmReason != null)
          {
                   //do "append(String)" instead of "append(StringBuffer)"
                   // below because second version doesn't exist in Java 1.3
                   // (resulting in 'NoSuchMethodError' under Java 1.3)
            if (alarmReason != null)
              alarmReason.append(regionAlarmReason.toString());
            regionAlarmReason = null;
          }
        }
      }
    }
    return alarmFlags;
  }

  /**
   * Adds the message to the alarm reason.
   * @param alarmReason the alarm reason.
   * @param msg the message.
   */
  protected void addAlarmReason(StringBuffer alarmReason,String msg)
  {
    if (alarmReason.length() > 0)
      alarmReason.append("; " + msg);
    else
      alarmReason.append(msg);
  }

  /**
   * Checks the alarm settings.
   * @param eventMagAmpVal the event magnitude or amplitude.
   * @param eventTime the event time.
   * @param eventVerifedFlag the event verified flag.
   * @param eventDepth the event depth.
   * @param minAlarmDepth the minimum alarm depth.
   * @param maxAlarmDepth the maximum alarm depth.
   * @param minAlarmMagAmp the alarm magnitude or amplitude.
   * @param maxAlarmAgeMillis the maximum alarm age.
   * @param verifiedEventsAlarmFlag the verified events alarm flag.
   * @param audibleAlarmFlag true for audible alarm.
   * @param visualAlarmFlag true for visual alarm.
   * @param alarmReason if supplied the reason the alarm should or should
   * not start be started.
   * @param amplitudeFlag true if amplitude event, false if earthquake
   * magnitude event.
   * @return the bit flag for the alarm.
   */
  protected int checkAlarm(
      final double eventMagAmpVal,
      final Date eventTime,
      final boolean eventVerifedFlag,
      final double eventDepth,
      final double minAlarmDepth,
      final double maxAlarmDepth,
      final double minAlarmMagAmp,
      final long maxAlarmAgeMillis,
      final boolean verifiedEventsAlarmFlag,
      final boolean audibleAlarmFlag,
      final boolean visualAlarmFlag,
      StringBuffer alarmReason,
      boolean amplitudeFlag)
  {

    if(amplitudeFlag)
    {    //amplitude event
      if (eventMagAmpVal < minAlarmMagAmp)
      {  //event amplitude < alarm amplitude
        if (alarmReason != null)
        {
          addAlarmReason(alarmReason,"alarm amplitude is less than min " +
                             UtilFns.floatNumberToString(minAlarmMagAmp));
        }
        return NO_ALARMS;          //do not start the alarm
      }
    }
    else
    {    //earthquake magnitude event
      if (eventMagAmpVal < minAlarmMagAmp)
      {  //event magnitude < alarm magnitude
        if (alarmReason != null)
        {
          addAlarmReason(alarmReason,"alarm magnitude is less than min " +
                             UtilFns.floatNumberToString(minAlarmMagAmp));
        }
        return NO_ALARMS;          //do not start the alarm
      }

      if (eventDepth != 0.0)
      {    //depth value exists for event
        if(eventDepth < minAlarmDepth)
        {  //event depth less than min
          if (alarmReason != null)
          {
            addAlarmReason(alarmReason,"depth is less than min " +
                               UtilFns.floatNumberToString(minAlarmDepth));
          }
          return NO_ALARMS;        //do not start the alarm
        }
        if(maxAlarmDepth > 0.0 && eventDepth > maxAlarmDepth)
        {  //event depth greater than max (if specified)
          if (alarmReason != null)
          {
            addAlarmReason(alarmReason,"depth is greater than max " +
                               UtilFns.floatNumberToString(maxAlarmDepth));
          }
          return NO_ALARMS;        //do not start the alarm
        }
      }
      else if (maxAlarmDepth > 0.0 || minAlarmDepth > 0.0)
      {    //no depth value and at least 1 alarm-depth criteria specified
        if (alarmReason != null)
          addAlarmReason(alarmReason,"no depth value");
        return NO_ALARMS;          //do not start the alarm
      }

      //if only verified events trigger and the event is not verified
      if (verifiedEventsAlarmFlag && !eventVerifedFlag)
      {
        if (alarmReason != null)
          addAlarmReason(alarmReason,"not verified");
        return NO_ALARMS;          //do not start the alarm
      }
    }

    if (maxAlarmAgeMillis > 0)  //maximum alarm event age limit is set
    {
      if (eventTime == null)
      {
        // age of event is unknown
        if (alarmReason != null)
          addAlarmReason(alarmReason,"age is unknown");
        return NO_ALARMS;          //do not start the alarm
      }
      if (eventTime.getTime() < (System.currentTimeMillis() - maxAlarmAgeMillis))
      {
        // age of event is older than the limit
        if (alarmReason != null)
        {
          addAlarmReason(alarmReason,"age is older than the limit " +
                             maxAlarmAgeMillis);
        }
        return NO_ALARMS;          //do not start the alarm
      }
    }

    int alarmFlags = NO_ALARMS;
    if (audibleAlarmFlag)
      alarmFlags |= AUDIBLE_ALARM;
    if (visualAlarmFlag)
      alarmFlags |= VISUAL_ALARM;

    if (alarmReason != null)
    {
      addAlarmReason(alarmReason,"alarm (flgs=" + alarmFlags +
                          ") " + (amplitudeFlag?"amplitude ":"magnitude ") +
                               UtilFns.floatNumberToString(minAlarmMagAmp));
    }
    return alarmFlags;  //start the alarm
  }

  /**
   * Checks if an event should start alarm.
   * @param eventMagVal the event magnitude.
   * @param eventTime the event time.
   * @param eventVerifedFlag the event verified flag.
   * @param eventDepth the event depth.
   * @param alarmReason if supplied the reason the alarm should or should
   * not start be started.
   * @param regionObj the region object.
   * @return the bit flag for the alarm.
   */
  protected int checkRegion(
      final double eventMagVal,
      final Date eventTime,
      final boolean eventVerifedFlag,
      final double eventDepth,
      StringBuffer alarmReason,
      IstiRegion regionObj)
  {
         //fetch alarm items for region:
    final Number minDepthObj =
                   regionObj.getOptionNumberValue(IstiRegion.OPID_MINDEPTH);
    final double minAlarmDepth = (minDepthObj != null) ?
                                            minDepthObj.doubleValue() : 0.0;
    final Number maxDepthObj =
                   regionObj.getOptionNumberValue(IstiRegion.OPID_MAXDEPTH);
    final double maxAlarmDepth = (maxDepthObj != null) ?
                                            maxDepthObj.doubleValue() : 0.0;
    final Number regionMagObj =
                        regionObj.getOptionNumberValue(IstiRegion.OPID_MAG);
    final double alarmMag = (regionMagObj != null) ?
                                           regionMagObj.doubleValue() : 0.0;
    final Number maxAgeObj =
                     regionObj.getOptionNumberValue(IstiRegion.OPID_MAXAGE);
    final long maxAlarmAgeMillis = (maxAgeObj != null) ?
                              maxAgeObj.longValue()*UtilFns.MS_PER_HOUR : 0;
    final Object verifiedObj =
                         regionObj.getOptionValue(IstiRegion.OPID_VERIFIED);
    final boolean verifiedEventsAlarmFlag = (verifiedObj instanceof Boolean)
                            ? ((Boolean)verifiedObj).booleanValue() : false;
    final Object audibleObj =
                          regionObj.getOptionValue(IstiRegion.OPID_AUDIBLE);
    final boolean audibleAlarmFlag = (verifiedObj instanceof Boolean) ?
                                ((Boolean)audibleObj).booleanValue() : true;
    final Object visualObj =
                           regionObj.getOptionValue(IstiRegion.OPID_VISUAL);
    final boolean visualAlarmFlag = (verifiedObj instanceof Boolean) ?
                                 ((Boolean)visualObj).booleanValue() : true;
         //check the region alarm settings:
    return checkAlarm(
      eventMagVal,eventTime,eventVerifedFlag,eventDepth,minAlarmDepth,
      maxAlarmDepth,alarmMag,maxAlarmAgeMillis,verifiedEventsAlarmFlag,
      audibleAlarmFlag,visualAlarmFlag,alarmReason,false);
  }
}
