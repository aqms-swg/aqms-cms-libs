//QWAliveMsgListener.java:  Defines a call-back listener interface used
//                          with server-alive status messages.
//
//  10/2/2002 -- [ET]  Initial version.
//  9/26/2003 -- [ET]  Moved from 'QWClient' to 'QWCommon' project.
//  3/31/2004 -- [ET]  Added 'QWStatusMsgRecord' parameter to method.
//  2/29/2008 -- [ET]  Modified 'aliveMsgReceived()' parameter comment.
//

package com.isti.quakewatch.util;

import com.isti.quakewatch.message.QWStatusMsgRecord;

/**
 * QWAliveMsgListener defines a call-back listener interface used with
 * server-alive status messages.
 */
public interface QWAliveMsgListener
{
  /**
   * Called when a new server-alive message has been received.
   * @param recObj the 'QWStatusMsgRecord' object for the received message,
   * or null if none available.
   */
  public void aliveMsgReceived(QWStatusMsgRecord recObj);
}
