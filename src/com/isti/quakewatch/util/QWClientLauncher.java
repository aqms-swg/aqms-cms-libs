//QWClientLauncher.java:  Launches the QuakeWatch client.
//
//  9/16/2004 -- [KF]  Initial version.
//  9/29/2004 -- [KF]  Added 'getLaunchClientCommand()' methods.
//  2/22/2005 -- [ET]  Changed 'DEFAULT_RUN_CLIENT_JAR_FILE_DIR' to "."
//                     and added "../CISN_Display" to alternates.
// 10/27/2008 -- [ET]  Added optional 'progSingletonDirStr' parameter to
//                     constructor.
//  11/7/2008 -- [ET]  Added methods 'detDirsAndCreateSingleton()',
//                     'getProgramRunDirStr()' and 'getBaseWriteDirStr()'.
//   3/5/2019 -- [KF]  Modified to use 'getOsName' method.
//  3/20/2019 -- [KF]  Modified to use the user-home directory if the
//                     program-run directory is not writable.
//

package com.isti.quakewatch.util;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import com.isti.util.ProgramSingleton;
import com.isti.util.LogFile;
import com.isti.util.UtilFns;
import com.isti.util.DupJarLauncher;
import com.isti.util.FileUtils;

/**
 * Class QWClientLauncher launches the QuakeWatch client.
 */
public class QWClientLauncher
{
  /** Name for the "program singleton" object. */
  public static final String CLIENT_PROGRAM_SINGLETON_NAME = "QWClient";

  /** Name for the client jar file. */
  public static final String CLIENT_JAR_FILE = "QWClient.jar";

  /** Default client jar file directory. */
  public static final String DEFAULT_RUN_CLIENT_JAR_FILE_DIR = ".";
         //Note:  If 'DEFAULT_RUN_CLIENT_JAR_FILE_DIR' != "." then the
         //       'getRunClientJarFile()' method needs to be modified.

  /** Alternate client jar file directories. */
  public static final String [] ALTERNATE_RUN_CLIENT_JAR_FILE_DIRS =
                                       { "../CISN_Display", "../QWClient" };

         //directory that program runs out of:
  protected static String programRunDirStr = UtilFns.EMPTY_STRING;
         //base directory for write-access files:
  protected static String baseWriteDirStr = UtilFns.EMPTY_STRING;

  //create client "program singleton" object:
  private final ProgramSingleton clientProgramSingletonObj;

  //parameters
//  private final File runClientJarFile;
  private final LogFile logObj;

  private final String cmdStr;
  private final File parentFile;

  /**
   * Constructs a client launcher.
   * @param logObj the log object.
   * @throws FileNotFoundException if the client jar file could not be found.
   */
  public QWClientLauncher(LogFile logObj) throws FileNotFoundException
  {
    this(logObj,getRunClientJarFile(null));
  }

  /**
   * Constructs a client launcher.
   * @param logObj the log object.
   * @param clientDirectory the client directory or empty string for default.
   * @param progSingletonDirStr directory for program-singleton file, or
   * null or empty string for default directory.
   * @throws FileNotFoundException if the client jar file could not be found.
   */
  public QWClientLauncher(LogFile logObj, String clientDirectory,
                    String progSingletonDirStr) throws FileNotFoundException
  {
    this(logObj,getRunClientJarFile(clientDirectory),progSingletonDirStr);
  }

  /**
   * Constructs a client launcher.
   * @param logObj the log object.
   * @param clientDirectory the client directory or empty string for default.
   * @throws FileNotFoundException if the client jar file could not be found.
   */
  public QWClientLauncher(LogFile logObj, String clientDirectory)
                                                throws FileNotFoundException
  {
    this(logObj,getRunClientJarFile(clientDirectory),null);
  }

  /**
   * Constructs a client launcher.
   * @param logObj the log object.
   * @param runClientJarFile the client jar file.
   * @param progSingletonDirStr directory for program-singleton file, or
   * null or empty string for default directory.
   * @throws FileNotFoundException if the client jar file could not be found.
   */
  public QWClientLauncher(LogFile logObj, File runClientJarFile,
                    String progSingletonDirStr) throws FileNotFoundException
  {
    if (runClientJarFile == null || !runClientJarFile.exists())
    {
      throw new FileNotFoundException(
          "The client jar file \"" + runClientJarFile +
          "\" does not exist");
    }

         //save parameters:
//    this.runClientJarFile = runClientJarFile;
    this.logObj = logObj;

         //get the parent file:
    parentFile = runClientJarFile.getParentFile();

    cmdStr = getLaunchClientCommand(parentFile);  //save the command string

         //create the program singleton object (if 'progSingletonDirStr'
         // given then use it; else use program-run directory):
    clientProgramSingletonObj = (progSingletonDirStr != null &&
                                  progSingletonDirStr.trim().length() > 0) ?
                                   new ProgramSingleton(progSingletonDirStr,
                                            CLIENT_PROGRAM_SINGLETON_NAME) :
                                            new ProgramSingleton(parentFile,
                                             CLIENT_PROGRAM_SINGLETON_NAME);
  }

  /**
   * Constructs a client launcher.
   * @param logObj the log object.
   * @param runClientJarFile the client jar file.
   * @throws FileNotFoundException if the client jar file could not be found.
   */
  public QWClientLauncher(LogFile logObj ,File runClientJarFile)
                                                throws FileNotFoundException
  {
    this(logObj,runClientJarFile,null);
  }

  /**
   * Runs the client.
   * @return an error string or null if no error.
   */
  public String runClient()
  {
    return runClient("client");
  }

  /**
   * Runs the client.
   * @param clientNameStr the client name string.
   * @return an error string or null if no error.
   */
  public String runClient(String clientNameStr)
  {
    //if the client is already running
    if (clientProgramSingletonObj.isRunning())
    {
      return clientNameStr + " is already running";
    }

    logObj.debug("Starting " + clientNameStr + " process ('" + cmdStr + "')");
    try
    {
      final Process p = Runtime.getRuntime().exec(cmdStr, null, parentFile);
      //see if the process terminated
      try
      {
        p.exitValue();
      }
      catch (IllegalThreadStateException ex)
      {
        //process is still running, we are done
        logObj.debug(clientNameStr + " process started successfully");
        return null;
      }
    }
    catch(Exception ex)  //exception while trying to run the client
    {
      //show stack trace
      logObj.debug(UtilFns.getStackTraceString(ex));
      return clientNameStr + "could not be launched" ;
    }
    return clientNameStr + " process terminated";
  }

  /**
   * Returns the launch command for the QWClient program.  If possible
   * the command is fetched from a batch/script file; if not then
   * a default launch command is returned.
   * @return The launch command for the QWClient program.
   */
  public static String getLaunchClientCommand()
  {
    return getLaunchClientCommand(null);
  }

  /**
   * Returns the launch command for the QWClient program.  If possible
   * the command is fetched from a batch/script file; if not then
   * a default launch command is returned.
   * @param parent the parent for the files.
   * @return The launch command for the QWClient program.
   */
  public static String getLaunchClientCommand(File parent)
  {
    String cmdStr;           //attempt to fetch launch command from files:
    String javaCmdStr = DupJarLauncher.getJavaCmd();
    if (!javaCmdStr.equals("java"))
    {
        cmdStr = javaCmdStr + " -Xmx512m -jar " + CLIENT_JAR_FILE;
    }
    else if((cmdStr=fetchJavaCmdFromFile(new File(parent,"runClient.bat"))) == null &&
       (cmdStr=fetchJavaCmdFromFile(new File(parent,"runClient"))) == null)

    {    //unable to fetch launch command from 'runClient' batch or script
              //if "Windows" OS then attempt to use "runClientW.bat":
      if(UtilFns.getOsName().toLowerCase().indexOf(
          "windows") < 0 || (cmdStr=fetchJavaCmdFromFile(new File(parent,
          "runClientW.bat"))) == null)
      {  //not "Windows" or no "runClientW.bat"; use default
        cmdStr = "java -Xmx512m -jar " + CLIENT_JAR_FILE;
      }
    }
    return cmdStr;
  }

  /**
   * Returns the first "java" command from the given file.  The command
   * should be in the form "java ... -jar JarName.jar" and may have
   * characters before the command, additional parameters, and lines of
   * data after the command.  If a "%*" or "$*" string is found right
   * after the command it is removed.
   * @param fileObj the input file object.
   * @return The "java" command string, or null if a valid command string
   * could not be fetched from the file.
   */
  protected static String fetchJavaCmdFromFile(File fileObj)
  {
    try
    {
      String dataStr = FileUtils.readFileToString(fileObj);
      int p;
      if(dataStr != null && (p=dataStr.indexOf("java")) >= 0)
      {  //'java' command found
        if(p > 0)       //remove any characters before 'java' command
          dataStr = dataStr.substring(p);
        final int dotJarPos;
        if((p=dataStr.indexOf("-jar")) > 3 &&
                                    (dotJarPos=dataStr.indexOf(".jar")) > p)
        {     //"-jar" and ".jar" substrings found
          if((p=dataStr.indexOf('\n')) > 0)
          {   //trailing newline found
            if(p <= dotJarPos)    //if before ".jar' then abort
              return null;
                   //remove trailing newline and any characters after it:
            dataStr = dataStr.substring(0,p);
          }
          if((p=dataStr.indexOf("%*")) > 0)
          {   //trailing "%*" found
            if(p <= dotJarPos)    //if before ".jar' then abort
              return null;
            dataStr = dataStr.substring(0,p);    //remove trailing "%*"
          }
          if((p=dataStr.indexOf("$*")) > 0)
          {   //trailing "$*" found
            if(p <= dotJarPos)    //if before ".jar' then abort
              return null;
            dataStr = dataStr.substring(0,p);    //remove trailing "%*"
          }
          dataStr = dataStr.trim();         //remove any trailing whitespace
          return dataStr;
        }
      }
    }
    catch(IOException ex)
    {         //error reading file
    }
    return null;
  }

  /**
   * Returns the client jar file for the specified client directory.
   * @param clientDirectory the client directory or empty string for default.
   * @return the client jar file or null if not found.
   */
  protected static File getRunClientJarFile(String clientDirectory)
  {
    //if client directory was specified and is not the default
    if (clientDirectory != null && clientDirectory.length() > 0 &&
        !DEFAULT_RUN_CLIENT_JAR_FILE_DIR.equals(clientDirectory))
    {
      //return the jar file
      return new File(clientDirectory,CLIENT_JAR_FILE);
    }

    //start with the default
         //Note:  If 'DEFAULT_RUN_CLIENT_JAR_FILE_DIR' != "." then the
         //       commented-out line below should be used instead:
    final File defaultRunClientJarFile = new File(CLIENT_JAR_FILE);
//                new File(DEFAULT_RUN_CLIENT_JAR_FILE_DIR,CLIENT_JAR_FILE);

    //if the default jar file does not exist
    if (!defaultRunClientJarFile.exists())
    {
      File runClientJarFile = null;
      //for each alternate client jar file directory
      for (int i = 0; i < ALTERNATE_RUN_CLIENT_JAR_FILE_DIRS.length; i++)
      {
        //return the jar file if it exists
        runClientJarFile =
                   new File(ALTERNATE_RUN_CLIENT_JAR_FILE_DIRS[i],
                   CLIENT_JAR_FILE);
        if (runClientJarFile.exists())
          return runClientJarFile;
      }
    }

    //return the default jar file
    return defaultRunClientJarFile;
  }

  public static void main(String[] args)
  {
    final LogFile logObj = LogFile.initGlobalLogObj(LogFile.getConsoleLogObj());
    final QWClientLauncher clientLauncher;

    try
    {
      clientLauncher = new QWClientLauncher(logObj);
    }
    catch (Exception ex)
    {
      logObj.warning(ex.toString());
      return;
    }

    //run the client
    final String errorStr = clientLauncher.runClient();
    if (errorStr != null)  //if error running the client
    {
      logObj.warning(errorStr);
      return;
    }
  }

  /**
   * Determines the program-run directory and the base directory for
   * write-access files, and creates a program singleton object.
   * This method enters values into the static 'programRunDirStr' and
   * 'baseWriteDirStr' variables.
   * @param sFileNameStr program name to use with singleton file.
   * @param prgRunDirFlag true if program-run directory should be used
   * for write-access files; false if user-home directory should be used.
   * @param userHomeSubDirStr user-home subdirectory name to use.
   * @param userHomeDirPrefixStr prefix for secondary user-home directory
   * name.
   * @return A new program singleton object.
   */
  public static ProgramSingleton detDirsAndCreateSingleton(
                                 String sFileNameStr, boolean prgRunDirFlag,
                      String userHomeSubDirStr, String userHomeDirPrefixStr)
  {
    try
    {              //determine directory program is running out of:
      programRunDirStr = (new File(".")).getAbsolutePath();
      if(programRunDirStr.endsWith("."))
      {  //string ends with "."
        programRunDirStr =        //remove trailing "."
                  programRunDirStr.substring(0,programRunDirStr.length()-1);
      }
    }
    catch(Exception ex)
    {  //some kind of exception error; show error message
      System.err.println("Error determining program-run directory:  " + ex);
    }
    if(!prgRunDirFlag)
    {  //'useProgramRunDir' cmd-line parameter was not given
      try
      {            //determine user-home directory:
        baseWriteDirStr = System.getProperty(
                                          "user.home",UtilFns.EMPTY_STRING);
        if(!programRunDirStr.startsWith(baseWriteDirStr) ||
        		!new File(programRunDirStr).canWrite())
        {
        	//program-run directory is not under user-home directory
        	// or program-run directory is not writable
          final int len;
          final char ch;          //check if path ends with name separator:
          if((len=baseWriteDirStr.length()) > 0 &&
                 (ch=baseWriteDirStr.charAt(len-1)) != File.separatorChar &&
                                                    ch != '/' && ch != '\\')
          {  //path does not end with name separator
            baseWriteDirStr += File.separatorChar;    //append separator
          }
                   //calculate hash-code value based on program paths:
          final int iHashVal = (programRunDirStr+baseWriteDirStr).hashCode();
                   //build path to be "homedir/.cisndisplay/client####":
          baseWriteDirStr += userHomeSubDirStr + File.separatorChar +
                                                         userHomeDirPrefixStr +
                       ((iHashVal < 0) ? 0x100000000L+iHashVal : iHashVal) +
                                                             File.separator;
        }
        else
        {
          baseWriteDirStr = UtilFns.EMPTY_STRING;     //use program-run dir
        }
      }
      catch(Exception ex)
      {  //some kind of exception error; show error message
        System.err.println("Error determining user-home directory:  " + ex);
      }
    }
         //create and return program singleton, using base dir (if present):
    return (baseWriteDirStr != null && baseWriteDirStr.trim().length() > 0) ?
                        new ProgramSingleton(baseWriteDirStr,sFileNameStr) :
                                         new ProgramSingleton(sFileNameStr);
  }

  /**
   * Returns the 'programRunDirStr' value entered by the method
   * 'detDirsAndCreateSingleton()'.
   * @return The 'programRunDirStr' value entered by the method
   * 'detDirsAndCreateSingleton()', or an empty string if none
   * available.
   */
  public static String getProgramRunDirStr()
  {
    return programRunDirStr;
  }

  /**
   * Returns the 'baseWriteDirStr' value entered by the method
   * 'detDirsAndCreateSingleton()'.
   * @return The 'baseWriteDirStr' value entered by the method
   * 'detDirsAndCreateSingleton()', or an empty string if none
   * available.
   */
  public static String getBaseWriteDirStr()
  {
    return baseWriteDirStr;
  }
}
