//QWConnProperties.java:  Manages the configuration property items for
//                        a connection to the QuakeWatch host.
//
//  11/6/2003 -- [ET]  Initial version.
// 12/19/2003 -- [ET]  Added 'subscribeDomainTypeList' property.
//  1/12/2004 -- [ET]  Added 'debugMissedMsgTestValue' property.
//  1/21/2004 -- [ET]  Added 'getItemsDisplayString()' and 'getLocalProps()'
//                     methods.
//   2/2/2004 -- [ET]  Changed default host from 'qws1' to 'qws2'.
//   2/9/2004 -- [ET]  Modified javadoc  for 'maxServerEventAgeDaysProp'.
//  6/29/2004 -- [ET]  Added 'statusCheckIntervalSecProp',
//                     'lastUpgradeAvailTimeProp' and
//                     'callUpgradeAvailHoursProp' properties.
//   8/5/2004 -- [ET]  Fixed descriptions on 'lastUpgradeAvailTime' and
//                     'callUpgradeAvailHours' properties.
//   8/5/2004 -- [KF]  Added 'serverLogin' property.
//   9/2/2004 -- [KF]  Added 'coaFile', 'crlFile', 'certFile' and
//                     'processMessageWithoutSigFlag' properties.
//  9/16/2004 -- [ET]  Moved down "serverLogin" item.
//  9/17/2004 -- [KF]  Modified to get the certificate from the server
//                     instead of the 'certFile' property.
//  10/1/2004 -- [ET]  Added methods 'getServerLoginInfoObj()' and
//                     'enableServerLoginDialog()'; restored 'group'
//                     setting to "Server login" property.
//  10/5/2004 -- [ET]  Changed 'enableServerLoginDialog()' method to
//                     'setServerLoginPropertyEditor()'.
// 10/13/2004 -- [ET]  Added 'getServerLoginPropertyEditor()' method.
//  11/1/2004 -- [ET]  Added 'loginInfoFileName' item.
// 11/15/2004 -- [ET]  Made 'eventChLocFile' item obsolete.
//  12/3/2004 -- [ET]  Changed 'DEF_SERVER_HOSTADDR' to an empty string
//                     to make default behavior be to select an alternate
//                     server at random.
//  12/8/2004 -- [ET]  Modified set of initial alternate servers to be
//                     the four "quakewatchX.cisn.org:39977" servers.
//  2/18/2005 -- [ET]  Added methods 'isServerLoginInfoEmpty()' and
//                     'setServerLoginInfoViaCfgProp()'.
//   4/8/2005 -- [ET]  Changed 'DEF_ALTSERVERS_LIST' to empty string.
//  9/16/2005 -- [ET]  Added 'webServicesServerFlag' item.
//  10/6/2010 -- [ET]  Changed default 'maxConnRetryWaitMinutes' value from
//                     5 to 2 minutes.
//

package com.isti.quakewatch.util;

import java.beans.PropertyEditor;
import com.isti.util.UtilFns;
import com.isti.quakewatch.common.QWConstants;
import com.isti.util.CfgPropItem;
import com.isti.util.CfgProperties;
import com.isti.util.DataChangedListener;

/**
 * Class QWConnectionMgr manages the configuration property items for
 * a connection to the QuakeWatch host.
 */
public class QWConnProperties implements QWConstants
{
    /** Default setting for server host address ("" == select alternate). */
  public static final String DEF_SERVER_HOSTADDR = UtilFns.EMPTY_STRING;
    /** Default setting for server port number. */
  public static final int DEF_SERVER_PORTNUM = 39977;
    /** Default setting for alternate-servers list string. */
  public static final String DEF_ALTSERVERS_LIST = UtilFns.EMPTY_STRING;
    /** Default setting for Certificate of Authority (CoA) file. */
  public static final String DEF_COA_FILE = "http://www.isti.com/qw/coa.pem";
    /** Default setting for certificate revocation list (CRL) file. */
  public static final String DEF_CRL_FILE =
                                           "http://www.isti.com/qw/crl.pem";
    /** Default setting for Certificate file. */
  public static final String DEF_CERT_FILE = UtilFns.EMPTY_STRING;
         //handle for empty server-login-information object:
  protected QWServerLoginInformation emptyServerLoginInfoObj = null;

         /** Configuration-properties object; may be passed in. */
  public final CfgProperties configPropsObj;

         /** Config-properties object containing only connection items. */
  public final CfgProperties localPropsObj;

         /** Host address of QWServer to connect to. */
  public final CfgPropItem serverHostAddressProp;

         /** Port number of QWServer to connect to. */
  public final CfgPropItem serverPortNumberProp;

         /** Server login username and password. */
  public final CfgPropItem serverLoginProp;

         /** Server connection is via web services flag. */
  public final CfgPropItem webServicesServerFlagProp;

         /** Max seconds to allow between server alive msgs (0=infinite). */
  public final CfgPropItem maxServerAliveSecProp;

         /** Server connection-retry timeout in seconds (0=infinite). */
  private final CfgPropItem connTimeoutSecProp;
  private int connTimeoutSec = CONNECTION_TIMEOUT_SECS_DEFAULT;

         /** Maximum wait between reconnect-to-server attempts (minutes). */
  public final CfgPropItem maxConnRetryWaitMinutesProp;

         /** Set true to enable fail-over reconnect to alternate servers. */
  public final CfgPropItem altServersEnabledFlagProp;

         /** List of alternate servers for fail-over reconnect. */
  public final CfgPropItem alternateServersListProp;

         /** Set true to allow only alternate servers specified in config. */
  public final CfgPropItem keepDefaultAltServersFlagProp;

         /** Optional host address specification for this client. */
  public final CfgPropItem clientHostAddressProp;

         /** Optional ORB/BOA port number specification for this client. */
  public final CfgPropItem clientPortNumProp;

         /** List of event domain and type names to subscribe to. */
  public final CfgPropItem subscribeDomainTypeListProp;

         /** Seconds between calls to 'clientStatusCheck()' (0=disable). */
  public final CfgPropItem statusCheckIntervalSecProp;

         /** Timestamp of last time upgrade-avail call-back was called. */
  public final CfgPropItem lastUpgradeAvailTimeProp;

         /** # of hrs between calls to upgrade-avail call-back (0=never). */
  public final CfgPropItem callUpgradeAvailHoursProp;

         /** Optional filename of file holding server-login information. */
  public final CfgPropItem loginInfoFileNameProp;

         /** Certificate of Authority (CoA) file. */
  public final CfgPropItem coaFileProp;

         /** certificate revocation list (CRL) file. */
  public final CfgPropItem crlFileProp;

         /** Process message without signature flag. */
  public final CfgPropItem processMessageWithoutSigFlagProp;

         /** Debug-test value, ignore all but 1 out of given # of msgs. */
  public final CfgPropItem debugMissedMsgTestValueProp;

         /** Max age for events requested from server (days, 0=none). */
  public final CfgPropItem maxServerEventAgeDaysProp;

         /** Obsolete:  Optional locator file for main event channel. */
  public final CfgPropItem eventChLocFileProp;


  /**
   * Constructs a connection-properties manager object.
   * @param propsObj the configuration-properties object to add item to,
   * or null to create a new configuration-properties object.
   * @param groupSelObj the configuration-group-selection object to use,
   * or null for none.
   * @param localEventAgePropFlag if true then a local
   * 'maxServerEventAgeDays' property item is created and used.
   */
  public QWConnProperties(CfgProperties propsObj,Object groupSelObj,
                                              boolean localEventAgePropFlag)
  {
         //setup local configuration-properties object:
    localPropsObj = new CfgProperties();

         //Host address of QWServer to connect to.
    serverHostAddressProp = localPropsObj.add(
       "serverHostAddress",DEF_SERVER_HOSTADDR).setGroupSelObj(groupSelObj);

         //Port number of QWServer to connect to.
    serverPortNumberProp = localPropsObj.add(
                        "serverPortNumber",Integer.valueOf(DEF_SERVER_PORTNUM)).
                          setValidator(0,65535).setGroupSelObj(groupSelObj);

         //Server login username and password.
    serverLoginProp = localPropsObj.add(
        "serverLogin", new QWServerLoginInformation()
        ).setEmptyStringDefaultFlag(true).setGroupSelObj(groupSelObj);

         //Server connection is via web services flag.
    webServicesServerFlagProp = localPropsObj.add(
                                  "webServicesServerFlag",Boolean.FALSE,null,
                                    "Server connection is via web services");
              //if 'QWWebSvcsManager' class can be successfully loaded then
              // set group-select obj (to show item on GUI property editor):
    if(QWWebSvcsConnector.loadQWWebSvcsManagerClassObj())
      webServicesServerFlagProp.setGroupSelObj(groupSelObj);
    else
    {    //unable to load 'QWWebSvcsManager' class
              //attach data-changed listener to set group-select object
              // (and show item on GUI property editor) if item is set
              // 'true' later on via config-file load:
      final Object finalGroupSelObj = groupSelObj;
      webServicesServerFlagProp.addDataChangedListener(
        new DataChangedListener()
        {
          public void dataChanged(Object sourceObj)
          {
            if(webServicesServerFlagProp.booleanValue())
            {      //item is 'true'; check if already has group-select obj
              if(webServicesServerFlagProp.getGroupSelObj() == null)
                webServicesServerFlagProp.setGroupSelObj(finalGroupSelObj);
                   //remove this data-changed listener from item:
              webServicesServerFlagProp.removeDataChangedListener(this);
            }
          }
        });
    }

         //Max seconds to allow between server alive msgs (0=infinite).
    maxServerAliveSecProp = localPropsObj.add(
                                   "maxServerAliveSec",Integer.valueOf(15),null,
                                          "Max secs for server alive msgs").
                           setValidator(0,9999).setGroupSelObj(groupSelObj);

         //Server connection-retry timeout in seconds (0=infinite).
    connTimeoutSecProp = localPropsObj
        .add("connTimeoutSec", CONNECTION_TIMEOUT_SECS_DEFAULT, null,
            "Server connection timeout (seconds)")
        .setValidator(0, 9999).setGroupSelObj(groupSelObj);
    connTimeoutSecProp.addDataChangedListener(new DataChangedListener()
    {
      public void dataChanged(Object sourceObj)
      {
        synchronized (connTimeoutSecProp)
        {
          connTimeoutSec = connTimeoutSecProp.intValue();
          // if the timeout exists and it is less than the server's default
          // value
          if (connTimeoutSec > 0
              && connTimeoutSec < CONNECTION_TIMEOUT_SECS_DEFAULT)
          {
            // use the server's default value
            connTimeoutSec = CONNECTION_TIMEOUT_SECS_DEFAULT;
            connTimeoutSecProp.setValue(connTimeoutSec);
          }
        }
      }
    });

         //Maximum wait between reconnect-to-server attempts (minutes).
    maxConnRetryWaitMinutesProp = localPropsObj.add(
                              "maxConnRetryWaitMinutes",Integer.valueOf(2),null,
                                     "Max connection-retry wait (minutes)").
                            setValidator(1,999).setGroupSelObj(groupSelObj);

         //Set true to enable fail-over reconnect to alternate servers.
    altServersEnabledFlagProp = localPropsObj.add(
                                  "altServersEnabledFlag",Boolean.TRUE,null,
                                   "Enable fail-over to alternate servers").
                                                setGroupSelObj(groupSelObj);

         //List of alternate servers for fail-over reconnect.
    alternateServersListProp =
              localPropsObj.add("alternateServersList",DEF_ALTSERVERS_LIST);

         //Set true to allow only alternate servers specified in config.
    keepDefaultAltServersFlagProp = localPropsObj.add(
                             "keepDefaultAltServersFlag",Boolean.FALSE,null,
                                    "Allow only default alternate servers");

         //Optional host address specification for this client.
    clientHostAddressProp = localPropsObj.add("clientHostAddress",
                                                      UtilFns.EMPTY_STRING);

         //Optional ORB/BOA port number specification for this client.
    clientPortNumProp = localPropsObj.add(
                  "clientPortNum",Integer.valueOf(0),null,"Client port number").
                                                      setValidator(0,65535);

         //List of event domain and type names to subscribe to
         // using format "domain:type,domain:type...":
    subscribeDomainTypeListProp = localPropsObj.add(
                        "subscribeDomainTypeList",UtilFns.EMPTY_STRING,null,
                                    "List of domain:types to subscribe to");

         //Seconds between calls to 'clientStatusCheck()' (0=disable):
    statusCheckIntervalSecProp = localPropsObj.add(
                              "statusCheckIntervalSec",Integer.valueOf(60),null,
                                      "Seconds between status-check calls");

         //Timestamp of last time upgrade-avail call-back was called:
    lastUpgradeAvailTimeProp = localPropsObj.add(
                              "lastUpgradeAvailTime",Long.valueOf(0),null,
                                    "Time of last upgrade-avail call-back");

         //# of hrs between calls to upgrade-avail call-back (0=never):
    callUpgradeAvailHoursProp = localPropsObj.add(
                               "callUpgradeAvailHours",Integer.valueOf(72),null,
                                  "Hours between upgrade-avail call-backs");

         //Optional filename of file holding server-login information:
    loginInfoFileNameProp = localPropsObj.add(
                              "loginInfoFileName",UtilFns.EMPTY_STRING,null,
                                   "File holding server-login information");

         //Certificate of Authority (CoA) file
    coaFileProp = localPropsObj.add(
        "coaFile",DEF_COA_FILE).setEmptyStringDefaultFlag(
        true);

         //Certificate Revocation List (CRL) file
    crlFileProp = localPropsObj.add(
        "crlFile",DEF_CRL_FILE).setEmptyStringDefaultFlag(
        true);

         // Process message without signature flag
    processMessageWithoutSigFlagProp = localPropsObj.add(
        "processMessageWithoutSigFlag",Boolean.TRUE).setEmptyStringDefaultFlag(
        true);

         //Debug-test value, ignore all but 1 out of given # of msgs.
    debugMissedMsgTestValueProp = localPropsObj.add(
                              "debugMissedMsgTestValue",Integer.valueOf(0),null,
                                                      null,true,false,true);

         //Obsolete:  Optional locator file for main event channel.
    eventChLocFileProp = localPropsObj.add(
             "eventChLocFile",UtilFns.EMPTY_STRING).setIgnoreItemFlag(true);

         //if flag then create local copy of 'maxServerEventAgeDays' item:
    if(localEventAgePropFlag)
    {    //flag is set; create and add property item:
      maxServerEventAgeDaysProp =
                  localPropsObj.add(getServerEventAgePropObj(groupSelObj));
    }
    else
      maxServerEventAgeDaysProp = null;     //indicate not in use

    if(propsObj != null)
    {    //external config-props object was given
      propsObj.putAll(localPropsObj);       //add items to external props
      configPropsObj = propsObj;            //set handle to ext props obj
    }
    else      //external config-props object not given
      configPropsObj = localPropsObj;       //set handle to local props obj
  }

  /**
   * Constructs a connection-properties manager object.
   * @param propsObj the configuration-properties object to add item to,
   * or null to create a new configuration-properties object.
   * @param groupSelObj the configuration-group-selection object to use,
   * or null for none.
   */
  public QWConnProperties(CfgProperties propsObj,Object groupSelObj)
  {
    this(propsObj,groupSelObj,false);
  }

  /**
   * Constructs a connection-properties manager object, using a newly-
   * created configuration-properties object.  A local
   * 'maxServerEventAgeDays' property item is created and used.
   */
  public QWConnProperties()
  {
    this(null,null,true);
  }

  /**
   * Returns the 'CfgProperties' object holding the configuration-property
   * items.  If a 'CfgProperties' object parameter is passed in to the
   * constructor then this method will return it.
   * @return The 'CfgProperties' object holding the configuration-property
   * items.
   */
  public CfgProperties getConfigProps()
  {
    return configPropsObj;
  }

  /**
   * @return the server connection-retry timeout in seconds (0=infinite)
   */
  public int getConnTimeoutSec()
  {
    synchronized (connTimeoutSecProp)
    {
      return connTimeoutSec;
    }
  }

  /**
   * Returns the 'CfgProperties' object holding the local
   * configuration-property items (related to connection settings).
   * @return The 'CfgProperties' object holding the local
   * configuration-property items (related to connection settings).
   */
  public CfgProperties getLocalProps()
  {
    return localPropsObj;
  }

  /**
   * Returns the 'QWServerLoginInformation' object held by the
   * "serverLogin" property item.
   * @return The 'QWServerLoginInformation' object held by the
   * "serverLogin" property item, or an empty 'QWServerLoginInformation'
   * object if one could not be fetched.
   */
  public QWServerLoginInformation getServerLoginInfoObj()
  {
    final Object obj;
    if((obj=serverLoginProp.getValue()) instanceof
                                               QWServerLoginInformation)
    {     //server-login-info object fetched OK
      return (QWServerLoginInformation)obj;
    }
    if(emptyServerLoginInfoObj == null)
      emptyServerLoginInfoObj = new QWServerLoginInformation();
    return emptyServerLoginInfoObj;
  }

  /**
   * Determines if the 'QWServerLoginInformation' object held by the
   * "serverLogin" property item contains empty username and password
   * strings.
   * @return true if the 'QWServerLoginInformation' object held by the
   * "serverLogin" property item is empty; false if not.
   */
  public boolean isServerLoginInfoEmpty()
  {
    final Object obj;
    if((obj=serverLoginProp.getValue()) instanceof
                                                   QWServerLoginInformation)
    {    //server-login-info object fetched OK
      final QWServerLoginInformation infoObj = (QWServerLoginInformation)obj;
      String str;       //return true if username and pwd are empty:
      return ((str=infoObj.getUsernameText()) == null ||
                                                str.trim().length() <= 0) &&
                                 ((str=infoObj.getPasswordText()) == null ||
                                                  str.trim().length() <= 0);
    }
    return true;
  }

  /**
   * Fetches the 'QWServerLoginInformation' object from the given
   * configuration-property item and enters it into the "serverLogin"
   * property item.
   * @param itemObj a configuration-property item holding a
   * 'QWServerLoginInformation' object.
   */
  public void setServerLoginInfoViaCfgProp(CfgPropItem itemObj)
  {
    final Object obj;
    if(itemObj != null && (obj=itemObj.getValue()) instanceof
                                                   QWServerLoginInformation)
    {    //given cfgProp holds a 'QWServerLoginInformation' object
      serverLoginProp.setValue(obj);        //enter server-login info obj
    }
  }

  /**
   * Enters the login-dialog property editor for the
   * 'QWServerLoginInformation' object held by the "serverLogin"
   * property item.  GUI clients that want to be able to
   * display the login dialog should call this method with
   * "new LoginPropertyEditor()" as the parameter.
   * @param propertyEditorObj the property-editor object to enter.
   */
  public void setServerLoginPropertyEditor(PropertyEditor propertyEditorObj)
  {
    getServerLoginInfoObj().setPropertyEditor(propertyEditorObj);
  }

  /**
   * Returns the login-dialog property editor for the
   * 'QWServerLoginInformation' object held by the "serverLogin"
   * property item.
   * @return The login-dialog property editor for the
   * 'QWServerLoginInformation' object held by the "serverLogin"
   * property item.
   */
  public PropertyEditor getServerLoginPropertyEditor()
  {
    return getServerLoginInfoObj().getPropertyEditor();
  }

  /**
   * Returns a string containing a display of the names and values for
   * the connection property items.
   * @param equalsStr a String containing the characters to be placed
   * between each name and value; or if null then the characters " = "
   * will be used.
   * @param newlineStr a String containing the characters to be used to
   * separate items; or if null then the system default newline
   * character(s) will be used.
   * @return A string containing a display of the names and values for
   * the connection property items.
   */
  public String getItemsDisplayString(String equalsStr,String newlineStr)
  {
    return localPropsObj.getDisplayString(equalsStr,newlineStr);
  }

  /**
   * Returns a string containing a display of the names and values for
   * the connection property items.
   * @return A string containing a display of the names and values for
   * the connection property items.
   */
  public String getItemsDisplayString()
  {
    return localPropsObj.getDisplayString();
  }

  /**
   * Returns the local 'maxServerEventAgeDays' property item (if available).
   * @return The local 'maxServerEventAgeDays' property item, or null
   * if not in use.
   */
  public CfgPropItem getMaxServerEventAgeDaysProp()
  {
    return maxServerEventAgeDaysProp;
  }

  /**
   * Creates and returns a 'maxServerEventAgeDays' property item object.
   * @param groupSelObj the configuration-group-selection object to use,
   * or null for none.
   * @return A new 'maxServerEventAgeDays' property item object.
   */
  public static CfgPropItem getServerEventAgePropObj(Object groupSelObj)
  {
    return new CfgPropItem("maxServerEventAgeDays",Double.valueOf(3.0),null,
                                        "Max event from server age (days)").
                     setValidator(0.0,999999.0).setGroupSelObj(groupSelObj);
  }
}
