//QWConnectionMgr.java:  Manages the configuration, initialization and
//                       closing of the connection to the QuakeWatch host.
//
//  11/6/2003 -- [ET]  Initial version.
// 11/13/2003 -- [ET]  Modified 'initialRequestMessagesFromServer()' to
//                     always perform request.
// 12/19/2003 -- [ET]  Added support for "subscribeDomainTypeList" config
//                     parameter and event-message filtering.
//  1/21/2004 -- [ET]  Added debug-log of connection settings at each
//                     connection-initialization.
//   2/2/2004 -- [ET]  Added 'inhibitReinitFlag' to prevent changing of
//                     server connection property-values from resulting
//                     in reinit being called with 'userInitFlag'==true
//                     via data-changed listeners; added support for
//                     setting up a connection-information properties
//                     string to be sent when connecting to a server.
//   2/4/2004 -- [ET]  Fixed bug where if a user-entered server was the
//                     only server on the alternate-servers list then
//                     the connection could repeatedly retry the server
//                     as an "alternate" with no wait delays; modified
//                     to make sure that when switching servers
//                     the last-used server ends up on the local
//                     alternate-servers list.
//  3/26/2004 -- [ET]  Modified to add listeners that will reinitialize
//                     connection after host-address or port-number
//                     properties are changed.
//  3/29/2004 -- [ET]  Modified 'doReinitConnection()' to do call
//                     'fireListCommit()' after change to alternate-servers
//                     list to save configuration.
//  3/31/2004 -- [ET]  Added try/catch to 'initialRequestMessagesFromServer'
//                     thread 'run()' method; replaced
//                     'setHdlrAliveMsgListener()' with add/remove
//                     alive-msg-listener methods.
//  6/16/2004 -- [ET]  Added 'getOpenOrbVersionStr()' method.
//  7/15/2004 -- [ET]  Added 'performClientStatusCheck()' and
//                     'performExitCleanup()' methods; added support
//                     for status-check thread.
//  7/28/2004 -- [ET]  Added methods 'getLocalHostIPString()' and
//                     'getLocalHostNameString()'; added support for
//                     upgrade-info and connect-attempt-failed call-backs.
//  8/11/2004 -- [ET]  Removed 'setAltServersListChangeListenerObj()';
//                     added 'setSaveToConfigFileCallBackObj()' and
//                     'fireSaveToConfigFileCallBack()' methods; added
//                     code to save 'lastUpgradeAvailTime' to config file
//                     when changed; added optional 'distribNameStr'
//                     parameter to 'setupConnInfoProps()' method.
//  9/14/2004 -- [ET]  Added 'updateLastUpgradeAvailTime()' method and
//                     modified so that 'lastUpgradeAvailTime' is updated
//                     after upgrade-info call-back is invoked in response
//                     to connect attempt failed.
//  9/15/2004 -- [ET]  Added 'getCertificateFileDataArr()' method.
//  9/21/2004 -- [ET]  Incremented COMM_VERSION_STR to "1.1".
//  9/24/2004 -- [ET]  Fixed 'invokeUpgradeInfoCallBack()' method to make
//                     it pass along 'notConnFlag' value; added calls to
//                     'StatusCheckingThread.connectionStatusChanged()'.
//  10/1/2004 -- [ET]  Modified to fetch server-login information and
//                     pass it into the QWCorbaManager; added method
//                     'enableServerLoginDialog()'.
//  10/5/2004 -- [ET]  Changed 'enableServerLoginDialog()' method to
//                     'setServerLoginPropertyEditor()'.
// 10/12/2004 -- [ET]  Fixed so that "login-authorization" IP and host
//                     name are only added to connection-properties
//                     table when available; added reinit-connection
//                     change listener to server-login config-property
//                     item; added 'indicateLoginAttemptFailed()' method;
//                     removed 'localHostIPString'/'localHostNameString'
//                     variables.
// 10/13/2004 -- [ET]  Modified 'indicateLoginAttemptFailed()' method to
//                     make it clear the connection-status popup dialog.
// 10/15/2004 -- [ET]  Modified 'connectAttemptFailed()' method to show
//                     connection-status popup dialog after connection
//                     error or client distribution-name rejected.
// 10/22/2004 -- [ET]  Modified 'getAlternateServersAvailFlag()' to make
//                     it check the 'altServersEnabledFlag' cfgProp item;
//                     renamed 'ConnectFailedCallBack' interface to
//                     'ConnectRejectCallBack'.
// 10/29/2004 -- [ET]  Modified 'initializeConnection()' method to detect
//                     change in encoded login password value due to local
//                     host name or IP address change and fire a save to
//                     configuration file.
//  11/3/2004 -- [ET]  Added option of using data from login-information
//                     file (via "loginInfoFileName" config-prop); added
//                     null checks for 'connStatusObj'; added methods
//                     'setConnectAttemptRejectCallBackObj()' and
//                     'fireConnectAttemptRejectCallBack()'.
// 11/12/2004 -- [ET]  Added 'setAcceptorRejectIDStr()' method.
// 11/15/2004 -- [ET]  Modified so that if the server-address config-prop
//                     is empty and alternate servers are enabled and
//                     available then one is chosen at random and used;
//                     removed support for 'eventChLocFile' connection
//                     parameter.
// 11/16/2004 -- [ET]  Added support for server redirect.
//  1/26/2005 -- [ET]  Added 'hostMsgNumListStr' parameter to
//                     'requestServerMessages()' method.
//  9/19/2005 -- [ET]  Modified to use 'QWAbstractConnector' instead
//                     of 'QWCorbaManager' and support for connecting
//                     to a QW web services server; added method
//                     'fireConnectionStatusChanged()'; updated
//                     'COMM_VERSION_STR' from "1.1" to "1.2".
//  12/5/2005 -- [ET]  Modified to enable two-way encryption on login
//                     information when connecting to a web-services
//                     server; added 'connLoginAttemptRejected()'
//                     method and modified to implement interface
//                     'ConnLoginRejectCallBack'; changed references
//                     from 'clearLastReceivedValues()' to
//                     'clearLastReceivedMsgNum()'; added method
//                     'isLastMsgFromCurrentServer()'; modified method
//                     'initialRequestMessagesFromServer()' to "force"
//                     server-changed flag if last message in storage
//                     did not come from connected server.
// 12/16/2005 -- [ET]  Modified to declare 'connInfoPropsTableObj' final.
//  3/24/2006 -- [ET]  Added methods 'addInitReqMsgsDoneListener()' and
//                     'removeInitReqMsgsDoneListener()'.
//   4/5/2006 -- [ET]  Removed "QWClient" from several log outputs.
//  6/12/2006 -- [ET]  Modified to prevent empty host-name entries (like
//                     ":39977" from getting into 'alternateServersList'
//                     configuration parameter.
//  8/28/2006 -- [ET]  Added listener (in constructor) to make sure that
//                     value of two-way encryption flag is retained after
//                     login-info changed.
// 11/13/2006 -- [ET]  Added methods 'getServerIDNameStr()' and
//                     'getServerRevisionStr()'; modified method
//                     'isLastMsgFromCurrentServer()' to send
//                     exception-error message to log file.
//  2/29/2008 -- [ET]  Removed calls to QWWebSvcsConnector method
//                     'setAliveMsgListenerObj()'.
//  4/14/2008 -- [ET]  Modified 'performClientStatusCheck()' method to
//                     throw 'StatusCheckFailedException' on error.
//  5/21/2008 -- [ET]  Modified to call QWMessageHandler method
//                     'clearWaitingMsgsQueueTable()' when initializing
//                     or closing connection to server.
// 10/22/2008 -- [ET]  Modified IP-address reporting to give preference
//                     to "valid" IPv4 addresses (via parameter to
//                     'getLocalHostAddrObj()' method); using updated
//                     'QWServerLoginInformation' class; added additional
//                     debug-log outputs for login/host-address information;
//                     updated 'COMM_VERSION_STR' from "1.2" to "1.3".
//  8/30/2010 -- [ET]  Modified 'initializeConnection()' method to make it
//                     log "No server configuration available" message at
//                     'debug' level (instead of 'warning').
//  1/30/2018 -- [KF]  Added 'CLIENT_FLAGS'.
//   3/5/2019 -- [KF]  Modified to use 'getOsName' method.
//  3/15/2021 -- [KF]  Replaced ConnectAttemptFailedCallBack with
//                     ConnectAttemptFinishedCallBack.
//  3/17/2021 -- [KF]  Modified to ensure alternate server list is the default
//                     value when 'keepDefaultAltServersFlag' is set to true.
//

package com.isti.quakewatch.util;

import java.net.InetAddress;
import java.beans.PropertyEditor;
import com.isti.util.UtilFns;
import com.isti.util.FifoHashtable;
import com.isti.util.CfgPropItem;
import com.isti.util.LogFile;
import com.isti.util.AddrPortListMgr;
import com.isti.util.DataChangedListener;
import com.isti.util.IstiDialogInterface;
import com.isti.util.DataChgdListenerSupport;
import com.isti.util.ProgressIndicatorInterface;
import com.isti.util.propertyeditor.ShowDialogPropertyEditor;
import com.isti.quakewatch.common.QWConnStrings;
import com.isti.quakewatch.common.qw_services.QWServices;
import com.isti.quakewatch.message.QWDataMsgProcessor;
import com.isti.quakewatch.message.QWMessageHandler;
import com.isti.quakewatch.message.QWMsgRecord;

/**
 * Class QWConnectionMgr manages the configuration, initialization and
 * closing of the connection to the QuakeWatch host.
 */
public class QWConnectionMgr implements
                               ConnectRejectCallBack,ConnLoginRejectCallBack
{
    /** Version number of communications code. */
  public static final String COMM_VERSION_STR = "1.3";
  protected final String [] programArgs;         //program args
  protected final QWConnProperties cfgObj;       //connection config props
  protected final QWDataMsgProcessor dataMsgProcObj;
  protected final ConnStatusInterface connStatusObj;
  protected final LogFile logObj;
  protected QWAbstractConnector qwConnectorObj = null;     //connector obj
  protected final Object qwConnectorSyncObj = new Object();
  protected final QWMessageHandler msgHandlerObj;     //message handler obj
  protected boolean connectingFlag = false;      //true while connecting
  protected boolean closingFlag = false;         //true while disconnecting
  protected boolean reinitingFlag = false;       //true while reiniting conn
  protected boolean terminatingFlag = false;     //true while term program
  protected boolean inhibitReinitFlag = false;   //true to disable reinit
                             //handle for server tracking thread object:
  protected ServerTrackingThread serverTrackingThreadObj = null;
              //object for connect/disconnect method synchronization:
  protected final Object connectionSyncObj = new Object();
              //handle for save-to-config-file call-back object:
  protected DataChangedListener saveToConfigFileCallBackObj = null;
              //alt server host-address/port-number list mgr:
  protected final AddrPortListMgr altServersListMgr = new AddrPortListMgr();
              //dlg-interface used by 'initialRequestMessagesFromServer()':
  protected IstiDialogInterface initReqMsgsDialogObj = null;
              //'Cancel' str used by 'initialRequestMessagesFromServer()':
  protected String initReqMsgsCancelString = UtilFns.EMPTY_STRING;
              //prog-interface used by 'initialRequestMessagesFromServer()':
  protected ProgressIndicatorInterface initReqMsgsProgIndObj = null;
              //call-back obj used by 'initialRequestMessagesFromServer()':
  protected InitReqMsgsCallBack initReqMsgsCallBackObj = null;
              //support for 'DataChangedListener' objects invoked after
              // 'initialRequestMessagesFromServer()' work is complete:
  protected final DataChgdListenerSupport initReqMsgsDoneLstnrSupportObj =
                                          new DataChgdListenerSupport(this);
              //information-properties table for client connection:
  protected final FifoHashtable connInfoPropsTableObj = new FifoHashtable();
              //list of event domain and type names being subscribed to:
  protected String subscribeDomainTypeListStr = null;
              //handle for status-checking-thread object:
  protected StatusCheckingThread statusCheckingThreadObj = null;
              //handle for upgrade-info call-back object:
  protected UpgradeInfoCallBack upgradeInfoCallBackObj = null;
              //set true after popup shown in 'connectAttemptRejected()':
  protected boolean errorStatusPopupShownFlag = false;
              //handle for connect-attempted-finished call-back object:
  protected ConnectAttemptFinishedCallBack connAttemptFinishedCallBackObj = null;
              //thread sync object for 'connAttemptFinishedCallBackObj':
  protected final Object connAttFailCallBackSyncObj = new Object();
              //handle for connect-attempted-finished call-back object:
  protected ConnectRejectCallBack connAttemptRejectCallBackObj = null;
              //thread sync object for 'connAttemptRejectCallBackObj':
  protected final Object connAttRejectCallBackSyncObj = new Object();
              //QWServer 'Acceptor' ID that should be rejected:
  protected String acceptorRejectIDString = null;
              //reason QWServer 'Acceptor' ID should be rejected:
  protected String acceptorRejectReasonStr = null;
              //local variable holding 'maxServerEventAgeDays' value in ms:
  private long maxServerEventAgeMs = 0;
              //thread synchronization object for 'maxServerEventAgeMs':
  private final Object maxSrvrEvtAgeSyncObj = new Object();
              //host/port entry-block object for tracking last server:
  private AddrPortListMgr.EntryBlock lastServerHostPortObj = null;
              //string used with connection-status panel:
  public static final String NOTCONN_CLICKFDET_STR =
                                          "Not Connected; click for detail";


  /**
   * Creates a connection-manager object.
   * @param programArgs the array of command-line arguments to be passed
   * on to the CORBA ORB.
   * @param connPropsObj a 'QWConnProperties' object holding the configuration
   * property items for the connection, or null to use defaults.
   * @param dataMsgProcObj the 'QWDataMsgProcessor' object to be used,
   * or null for none.
   * @param connStatusObj connection-status panel object, or null for none.
   * @param logObj log-file object to be used, or null for none.
   */
  public QWConnectionMgr(String [] programArgs,QWConnProperties connPropsObj,
        QWDataMsgProcessor dataMsgProcObj,ConnStatusInterface connStatusObj,
                                                             LogFile logObj)
  {
    this.programArgs = programArgs;
                                       //connection props (or defaults):
    cfgObj = (connPropsObj != null) ? connPropsObj : new QWConnProperties();
    this.dataMsgProcObj = dataMsgProcObj;
    this.connStatusObj = connStatusObj;
              //setup handle to log file; if null then setup dummy log file
    this.logObj = (logObj != null) ? logObj :
                          new LogFile(null,LogFile.NO_MSGS,LogFile.NO_MSGS);
              //create message handler object:
    msgHandlerObj = new QWMessageHandler(this,logObj);

         //setup to use two-way encryption with login information
         // if connection is to a web-services server:
    updateLoginInfoTwoWayEncFlag();
         //setup change listener to track future changes to property:
    cfgObj.webServicesServerFlagProp.addDataChangedListener(
                                                   new DataChangedListener()
        {
          public void dataChanged(Object sourceObj)
          {
            updateLoginInfoTwoWayEncFlag();      //enter new flag value
          }
        });
         //setup change listener to make sure that value of two-way
         // encryption flag is retained after login-info changed:
    cfgObj.serverLoginProp.addDataChangedListener(new DataChangedListener()
      {
        public void dataChanged(Object sourceObj)
        {
          updateLoginInfoTwoWayEncFlag();        //enter new flag value
        }
      });

         //fetch and process data from login-info file (if given):
    final String loginInfoFileName;
    if((loginInfoFileName=connPropsObj.loginInfoFileNameProp.stringValue())
                           != null && loginInfoFileName.trim().length() > 0)
    {    //login-information file specified; attempt to process it
      if(LoginInfoFileProc.process(loginInfoFileName,
                                       connPropsObj.serverLoginProp,logObj))
      {  //fetch and process of login info succeeded; log message
        logObj.info("Using login information from file \"" +
                                                  loginInfoFileName + "\"");
      }
    }

         //set 'keepDefaultServersFlag' value in the alt-servers list
         // manager (will be used later by the QW connector):
    altServersListMgr.setKeepDefaultServersFlag(
                       cfgObj.keepDefaultAltServersFlagProp.booleanValue());
         //setup change listener to track future changes to property:
    cfgObj.keepDefaultAltServersFlagProp.addDataChangedListener(
                                                   new DataChangedListener()
        {
          public void dataChanged(Object sourceObj)
          {                                 //enter new flag value:
            boolean b = cfgObj.keepDefaultAltServersFlagProp.booleanValue(); 
            altServersListMgr.setKeepDefaultServersFlag(b);
            if (b) // if allow only alternate servers specified in configuration
            {
              // reset the alternate servers to the default
              cfgObj.alternateServersListProp.reset();
            }
          }
        });

         //setup "commit" listener to respond when the alternate-servers
         // list is changed and "committed":
    altServersListMgr.setListCommitListenerObj(new DataChangedListener()
        {
          public void dataChanged(Object sourceObj)
          {
                   //enter new alt-servers list string into config property:
            cfgObj.alternateServersListProp.setValue(
                                     altServersListMgr.getEntriesListStr());
          }
        });
         //setup initial last-used server addr/port object:
    final String hostStr;
    if((hostStr=cfgObj.serverHostAddressProp.stringValue()) != null &&
                                                hostStr.trim().length() > 0)
    {    //current host-address string not empty; enter as last-used
      lastServerHostPortObj = new AddrPortListMgr.EntryBlock(hostStr,
                                    cfgObj.serverPortNumberProp.intValue());
    }
         //load alt-servers-list manager from config property item:
    final String altServersListStr =
                              cfgObj.alternateServersListProp.stringValue();
    if(altServersListStr != null && altServersListStr.length() > 0)
    {    //alt-servers-list config property not empty; add to list mgr
      if(!altServersListMgr.setEntriesListStr(altServersListStr))
      {  //syntax error in alt-servers-list string
        logObj.info("Error in '" +
                                 cfgObj.alternateServersListProp.getName() +
                                                     "' config setting:  " +
                                 altServersListMgr.getErrorMessageString());
      }
      logObj.debug2("Alternate-servers list read from config:  \"" +
                              altServersListMgr.getEntriesListStr() + "\"");
         //check if current server in alternate servers list:
      if(altServersListMgr.removeEntry(lastServerHostPortObj))
      {  //current server was in alternate servers list and removed
        logObj.debug("Removed current server \"" + lastServerHostPortObj +
                                          "\" from alternate servers list");
              //fire commit to save to config item and out to file:
        altServersListMgr.fireListCommit(this);
      }
    }
    else      //alt-servers-list config property not empty
      logObj.debug2("Empty alternate-servers list in config");

         //if the max-server-event-age property object held by the
         // connection-properties object then setup to use it:
    final CfgPropItem evtAgePropObj;
    if((evtAgePropObj=cfgObj.getMaxServerEventAgeDaysProp()) != null)
    {    //max-server-event-age property object exist in connMgr
              //fetch and enter current max-server-event-age value in ms:
      setMaxServerEventAgeMs((long)(evtAgePropObj.doubleValue()*
                                                       UtilFns.MS_PER_DAY));
              //setup change listener to track max-server-event-age
              // value and (possibly) enable check-missed flag if
              // max-server-event-age goes from 0 to greater than 0:
      evtAgePropObj.addDataChangedListener(
        new DataChangedListener()
          {
            public void dataChanged(Object sourceObj)
            {
              setMaxServerEventAgeMs((long)(evtAgePropObj.doubleValue()*
                                                       UtilFns.MS_PER_DAY));
            }
          });
    }

         //enter this connection-manager object into the message processor:
    if(dataMsgProcObj != null)                   //if msg-proc obj given
      dataMsgProcObj.setConnectionMgr(this);     // then enter conn-mgr obj

         //create and start status-checking thread:
    statusCheckingThreadObj = new StatusCheckingThread(this);

         //setup data-changed listener to reinitialize connection after
         // host-addr, port-num or login config properties are modified:
    addConfigChangedListeners(new DataChangedListener()
        {
          public void dataChanged(Object obj)
          {      //reinitialize connection (don't show
                 // popup, use separate thread, user initiated):
            reinitConnection(false,true,true);
          }
        });
  }

  /**
   * Creates a connection-manager object.
   * @param programArgs the array of command-line arguments to be passed
   * on to the CORBA ORB.
   * @param connPropObj a 'QWConnProperties' object holding the configuration
   * property items for the connection, or null to use defaults.
   * @param dataMsgProcObj the 'QWDataMsgProcessor' object to be used,
   * or null for none.
   * @param logObj log-file object to be used, or null for none.
   */
  public QWConnectionMgr(String [] programArgs,QWConnProperties connPropObj,
                           QWDataMsgProcessor dataMsgProcObj,LogFile logObj)
  {
    this(programArgs,connPropObj,dataMsgProcObj,null,logObj);
  }

  /**
   * Creates a connection-manager object.
   * @param programArgs the array of command-line arguments to be passed
   * on to the CORBA ORB.
   * @param dataMsgProcObj the 'QWDataMsgProcessor' object to be used,
   * or null for none.
   * @param logObj log-file object to be used, or null for none.
   */
  public QWConnectionMgr(String [] programArgs,
                           QWDataMsgProcessor dataMsgProcObj,LogFile logObj)
  {
    this(programArgs,null,dataMsgProcObj,null,logObj);
  }

  /**
   * Enters the login-dialog property editor for the
   * 'QWServerLoginInformation' object held by the "serverLogin"
   * connection-property item.  GUI clients that want to be able to
   * display the login dialog should call this method with
   * "new LoginPropertyEditor()" as the parameter.
   * @param propertyEditorObj the property-editor object to enter.
   */
  public void setServerLoginPropertyEditor(PropertyEditor propertyEditorObj)
  {
    cfgObj.setServerLoginPropertyEditor(propertyEditorObj);
  }

  /**
   * Sets up the call-back object to be called when the connection
   * configuration-property items should be saved to the config file.
   * @param listenerObj The 'DataChangedListener' to use, or null for none.
   */
  public void setSaveToConfigFileCallBackObj(
                                            DataChangedListener listenerObj)
  {
    if(saveToConfigFileCallBackObj != null)
    {    //previous call-back existed; remove it from config-prop item
      cfgObj.lastUpgradeAvailTimeProp.removeDataChangedListener(
                                               saveToConfigFileCallBackObj);
    }

    saveToConfigFileCallBackObj = listenerObj;

    if(saveToConfigFileCallBackObj != null)
    {    //call-back given; enter it into config-prop item
      cfgObj.lastUpgradeAvailTimeProp.addDataChangedListener(
                                               saveToConfigFileCallBackObj);
    }
  }

  /**
   * Adds the given data-changed listener to the host-address,
   * port-number, server-login and web services server configuration
   * properties.
   * @param connCfgListener the 'DataChangedListener' object to add.
   */
  protected final void addConfigChangedListeners(
                                        DataChangedListener connCfgListener)
  {
              //add listener to track changes to server host address:
    cfgObj.serverHostAddressProp.addDataChangedListener(connCfgListener);
              //add listener to track changes to server port number:
    cfgObj.serverPortNumberProp.addDataChangedListener(connCfgListener);
              //add listener to track changes to client host address:
    cfgObj.clientHostAddressProp.addDataChangedListener(connCfgListener);
              //add listener to track changes to client port number:
    cfgObj.clientPortNumProp.addDataChangedListener(connCfgListener);
              //add listener to track changes to server login information:
    cfgObj.serverLoginProp.addDataChangedListener(connCfgListener);
              //add listener to track changes to web services server flag:
    cfgObj.webServicesServerFlagProp.addDataChangedListener(connCfgListener);
  }

  /**
   * Sets up dialog, progress and call-back objects used by the
   * 'initialRequestMessagesFromServer()' method.
   * @param initReqMsgsDialogObj dialog-popup object.
   * @param initReqMsgsCancelString 'Cancel' string for dialog-popup object.
   * @param initReqMsgsProgIndObj for progress-indicator object.
   * @param initReqMsgsCallBackObj call-back object.
   */
  public void setupInitReqMsgsObjs(IstiDialogInterface initReqMsgsDialogObj,
                                             String initReqMsgsCancelString,
                           ProgressIndicatorInterface initReqMsgsProgIndObj,
                                 InitReqMsgsCallBack initReqMsgsCallBackObj)
  {
    this.initReqMsgsDialogObj = initReqMsgsDialogObj;
    this.initReqMsgsCancelString = initReqMsgsCancelString;
    this.initReqMsgsProgIndObj = initReqMsgsProgIndObj;
    this.initReqMsgsCallBackObj = initReqMsgsCallBackObj;
  }

  /**
   * Sets up the connection-information properties string to be sent when
   * the client connects to the server.
   * @param clientNameStr the client program name to use, or null for none.
   * @param clientVerStr the client version string to use, or null for none.
   * @param startupTimeStr the client startup time string to use, or null
   * for none.
   * @param distribNameStr the distribution name to use, or null for none.
   */
  public void setupConnInfoProps(String clientNameStr,String clientVerStr,
                                String startupTimeStr,String distribNameStr)
  {
	    setupConnInfoProps(
	    		clientNameStr,clientVerStr,startupTimeStr,distribNameStr,null);
  }

  /**
   * Sets up the connection-information properties string to be sent when
   * the client connects to the server.
   * @param clientNameStr the client program name to use, or null for none.
   * @param clientVerStr the client version string to use, or null for none.
   * @param startupTimeStr the client startup time string to use, or null
   * for none.
   * @param distribNameStr the distribution name to use, or null for none.
   * @param clientFlags the client flags or null for none.
   */
  public void setupConnInfoProps(
		  String clientNameStr,String clientVerStr, String startupTimeStr,
		  String distribNameStr, String clientFlags)
  {
    if(clientNameStr != null)     //if given then enter program name
      connInfoPropsTableObj.put(QWConnStrings.CLIENT_NAME,clientNameStr);
    if(clientVerStr != null)      //if given then enter program version
      connInfoPropsTableObj.put(QWConnStrings.CLIENT_VERSION,clientVerStr);
    if(distribNameStr != null)     //if given then enter distribution name
      connInfoPropsTableObj.put(QWConnStrings.DISTRIB_NAME,distribNameStr);
    if (clientFlags != null)
        connInfoPropsTableObj.put(QWConnStrings.CLIENT_FLAGS,clientFlags);
    if(startupTimeStr != null)    //if given then enter program startup time
      connInfoPropsTableObj.put(QWConnStrings.STARTUP_TIME,startupTimeStr);
              //enter version number for client-communications code:
    connInfoPropsTableObj.put(QWConnStrings.COMM_VERSION,COMM_VERSION_STR);
              //enter OpenORB version string:
    connInfoPropsTableObj.put(QWConnStrings.OPENORB_VERSION,
                                                    getOpenOrbVersionStr());
              //determine and enter operating-system name:
    connInfoPropsTableObj.put(QWConnStrings.CLIENT_OSNAME,UtilFns.getOsName());
              //determine and enter Java version:
    connInfoPropsTableObj.put(QWConnStrings.JAVA_VERSION,UtilFns.getJavaVersion());
  }

  /**
   * Sets up the connection-information properties string to be sent when
   * the client connects to the server.
   * @param clientNameStr the client program name to use, or null for none.
   * @param clientVerStr the client version string to use, or null for none.
   * @param startupTimeStr the client startup time string to use, or null
   * for none.
   */
  public void setupConnInfoProps(String clientNameStr,String clientVerStr,
                                                      String startupTimeStr)
  {
    setupConnInfoProps(clientNameStr,clientVerStr,startupTimeStr,null,null);
  }

  /**
   * Sets the upgrade-information call-back object.  When client-upgrade
   * information is available from the QWServer, it is fetched and
   * then returned as a string via this call-back.  The call-back
   * method should return quickly.
   * @param callBackObj the call-back object to use, or null for none.
   */
  public void setUpgradeInfoCallBackObj(UpgradeInfoCallBack callBackObj)
  {
    upgradeInfoCallBackObj = callBackObj;
  }

  /**
   * Sets the value for the maximum age for events fetched from the
   * server.
   * @param ageVal the value for the maximum age for events fetched
   * from the server, in milliseconds.
   */
  public final void setMaxServerEventAgeMs(long ageVal)
  {
    synchronized(maxSrvrEvtAgeSyncObj)
    {    //only allow thread at a time to access 'maxServerEventAgeMs'
      maxServerEventAgeMs = ageVal;
    }
         //update missed-message checking enabled flag:
    msgHandlerObj.updateCheckMissedEnabledFlag();
  }

  /**
   * Returns the value for 'maxServerEventAgeMs'.
   * @return The value for 'maxServerEventAgeMs'.
   */
  public long getMaxServerEventAgeMs()
  {
    synchronized(maxSrvrEvtAgeSyncObj)
    {    //only allow thread at a time to access 'maxServerEventAgeMs'
      return maxServerEventAgeMs;
    }
  }

  /**
   * Initializes the connection to the server.
   * @param initialFlag true if this is the first connection to server
   * for the program.
   * @return true if a connect attempt was made, false if not (because
   * of no server-locator information).
   */
  public boolean initializeConnection(boolean initialFlag)
  {
    logObj.debug4("Called 'initializeConnection(" + initialFlag + ")'");
    if(connectingFlag || closingFlag || terminatingFlag)
    {    //connect or disconnect in progress or program terminating
      logObj.debug2("Aborted 'initializeConnection()', connectingFlag=" +
                           connectingFlag + ", closingFlag=" + closingFlag +
                                    ", terminatingFlag=" + terminatingFlag);
      return false;                    //abort method
    }
    synchronized(connectionSyncObj)
    {    //only allow one thread in at a time; recheck flags after synch
      if(connectingFlag || closingFlag || terminatingFlag)
      {  //connect or disconnect in progress or program terminating
      logObj.debug2("Aborted 'initializeConnection()', connectingFlag=" +
                         connectingFlag + ", closingFlag=" + closingFlag +
                                  ", terminatingFlag=" + terminatingFlag);
        return false;                  //abort method
      }
      connectingFlag = true;           //indicate connecting in progress
         //if not initial connection then do slight delay
         // in case multiple config items are changing:
      if(!initialFlag)
      {
        try { Thread.sleep(50); }
        catch(InterruptedException ex) {}
      }
                   //clear any queued messages from previous connection:
      msgHandlerObj.clearWaitingMsgsQueueTable();
      boolean retFlag;                 //save server-host address:
      String serverHostAddrStr = cfgObj.serverHostAddressProp.stringValue();
      subscribeDomainTypeListStr =     //list of domain/types subscribed to
                           cfgObj.subscribeDomainTypeListProp.stringValue();
         //enter domain/type list string into message handler:
      msgHandlerObj.setDomainTypeListStr(subscribeDomainTypeListStr);
         //if no server address then select random alternate server:
      int serverHostAddrStrLen;
      if((serverHostAddrStrLen=serverHostAddrStr.length()) <= 0 &&
                          cfgObj.altServersEnabledFlagProp.booleanValue() &&
                                               altServersListMgr.size() > 0)
      {  //no server address, alt servers enabled and available
        final AddrPortListMgr.EntryBlock blkObj;
        if((blkObj=altServersListMgr.popRandomEntry()) != null)
        {     //random alternate server popped OK
                   //set new current server to popped-server entry:
          setServerAddressPortValues(
                                 blkObj.hostAddrStr,blkObj.portNumber,true);
                   //update local host-address string and length:
          serverHostAddrStr = cfgObj.serverHostAddressProp.stringValue();
          serverHostAddrStrLen = serverHostAddrStr.length();
                   //setup new last-used server addr/port object:
          lastServerHostPortObj = new AddrPortListMgr.EntryBlock(
                  serverHostAddrStr,cfgObj.serverPortNumberProp.intValue());
          logObj.info("No server host address configured; " +
                     "selected random alternate server \"" + blkObj + "\"");
          fireSaveToConfigFileCallBack();   //save changes to config file
        }
      }
         //if server address available then initialize access:
      if(serverHostAddrStrLen > 0)
      {  //server address given
        String str;
        if((str=cfgObj.clientHostAddressProp.stringValue()) != null &&
                                                           str.length() > 0)
        {  //host address was specified; log it
          logObj.debug("Local host address from config:  " + str);
        }
        int val;          //if port number specified then log it:
        if((val=cfgObj.clientPortNumProp.intValue()) > 0)
          logObj.debug("Local port number from config:  " + val);
              //debug-log connection settings:
        logObj.debug("Connection settings:  " +
                                    cfgObj.getItemsDisplayString("=",", "));
        if(cfgObj.webServicesServerFlagProp.booleanValue())
        {     //connecting to QW web services server
          if(!(qwConnectorObj instanceof QWWebSvcsConnector))
          {   //QW Web Services connector obj not yet created; create it now
            logObj.debug3("Creating 'QWWebSvcsConnector' object");
            synchronized(qwConnectorSyncObj)
            { //grab thread-synchronization object for 'qwConnectorObj'
              qwConnectorObj = new QWWebSvcsConnector(this,msgHandlerObj,
                             cfgObj.maxServerAliveSecProp,altServersListMgr,
                                                      connStatusObj,logObj);
            }
                   //disable missed-message checking in the handler:
            msgHandlerObj.setDisableMissedMsgCheckingFlag(true);
                   //enter Acceptor-reject-ID strings:
            qwConnectorObj.setAcceptorRejectIDStr(
                            acceptorRejectIDString,acceptorRejectReasonStr);
          }
          else
          {   //CORBA connector obj already created; update connect params
            logObj.debug3("Reusing 'QWWebSvcsConnector' object");
          }
        }
        else
        {     //connecting to QW CORBA server
          if(!(qwConnectorObj instanceof QWCorbaConnector))
          {   //QW CORBA connector object not yet created; create it now
            logObj.debug3("Creating 'QWCorbaConnector' object");
            synchronized(qwConnectorSyncObj)
            { //grab thread-synchronization object for 'qwConnectorObj'
              qwConnectorObj = new QWCorbaConnector(this,programArgs,
                       msgHandlerObj,altServersListMgr,connStatusObj,logObj,
                                 cfgObj.clientHostAddressProp.stringValue(),
                                       cfgObj.clientPortNumProp.intValue());
            }
                   //enable missed-message checking in the handler:
            msgHandlerObj.setDisableMissedMsgCheckingFlag(false);
                   //enter Acceptor-reject-ID strings:
            qwConnectorObj.setAcceptorRejectIDStr(
                            acceptorRejectIDString,acceptorRejectReasonStr);
          }
          else
          {   //CORBA connector obj already created; update connect params
            logObj.debug3("Reusing 'QWCorbaConnector' object");
            qwConnectorObj.updateConnectParams(
                                 cfgObj.clientHostAddressProp.stringValue(),
                                       cfgObj.clientPortNumProp.intValue());
          }
        }
              //setup subscribe-filter for consumer:
        qwConnectorObj.setFilterDomainTypeList(subscribeDomainTypeListStr);
              //if conn-info props table not setup then do it now:
        if(connInfoPropsTableObj.size() <= 0)
          setupConnInfoProps(null,null,null);
              //enter current local-host IP and host name into conn-props:
        final InetAddress addrObj = UtilFns.getLocalHostAddrObj(true);
        connInfoPropsTableObj.put(
                   QWConnStrings.CLIENT_IP,UtilFns.getLocalHostIP(addrObj));
        connInfoPropsTableObj.put(
               QWConnStrings.CLIENT_HOST,UtilFns.getLocalHostName(addrObj));
              //fetch server-login-info object from config-prop object:
        final QWServerLoginInformation loginInfoObj =
                                             cfgObj.getServerLoginInfoObj();
              //get "auth" host name & IP before call to 'getTransEncPwd()':
        final String oldHstIPStr = loginInfoObj.getLocalHostIPString();
        final String oldHstNameStr = loginInfoObj.getLocalHostNameString();
              //get username from login-info object:
        final String usernameTextStr = loginInfoObj.getUsernameText();
              //get pwd from login-info object (may update host-name/IP):
        final String transEncPwdStr = loginInfoObj.getTransEncPwd();
              //get "auth" host name & IP after call to 'getTransEncPwd()':
        final String newHstIPStr = loginInfoObj.getLocalHostIPString();
        final String newHstNameStr = loginInfoObj.getLocalHostNameString();
        if(newHstIPStr != null && newHstNameStr != null)
        {     //fetched host name and IP address strings OK
          if(!newHstIPStr.equals(oldHstIPStr) ||
                                       !newHstNameStr.equals(oldHstNameStr))
          {   //host name and/or IP address changed
            if(!newHstIPStr.equals(oldHstIPStr))
            {  //host IP address changed; log debug message
              logObj.debug("Connection auth-IP value changed from \"" +
                             oldHstIPStr + "\" to \"" + newHstIPStr + "\"");
            }
            if(!newHstNameStr.equals(oldHstNameStr))
            {  //host name changed; log debug message
              logObj.debug("Connection auth-host value changed from \"" +
                         oldHstNameStr + "\" to \"" + newHstNameStr + "\"");
            }
            fireSaveToConfigFileCallBack();      //save updated pwd value
          }
          if(newHstIPStr.length() > 0)
          {   //"login-authorization" IP-string contains data; enter it
            connInfoPropsTableObj.put(QWConnStrings.CLIENT_AUTH_IP,
                                                               newHstIPStr);
          }
          if(newHstNameStr.length() > 0)
          {   //"login-authorization" host name contains data; enter it
            connInfoPropsTableObj.put(QWConnStrings.CLIENT_AUTH_HOST,
                                                             newHstNameStr);
          }
        }
        logObj.debug("Connection local-host info:  " +
                                    loginInfoObj.getLocalHostAddrInfoStr());
              //convert table of conn-props to quoted strings:
        final String connPropsStr = connInfoPropsTableObj.toQuotedStrings();
        logObj.debug("Connection-info-props string:  " + connPropsStr);
              //fetch username and password from server-login-info object
              // and enter them and conn-info props string into connector:
        qwConnectorObj.enterConnectionInfo(usernameTextStr,
                                               transEncPwdStr,connPropsStr);
                   //initialize connection via separate thread,
                   // if first attempt then show connection status popup:
        qwConnectorObj.startConnectViaHostInfo(
                   serverHostAddrStr,cfgObj.serverPortNumberProp.intValue(),
                                                               initialFlag);
        retFlag = true;           //indicate connection attempt made
        if(serverTrackingThreadObj == null ||
                                         !serverTrackingThreadObj.isAlive())
        {     //server-tracking thread not yet started
          (serverTrackingThreadObj =        //create and start thread
                                    new ServerTrackingThread(this)).start();
        }
      }
      else
      {  //server address not given
        if(serverTrackingThreadObj != null)
        {     //server tracking thread is running
          serverTrackingThreadObj.terminate();        //terminate thread
          serverTrackingThreadObj = null;             //indicate terminated
        }
                   //setup not-connected messages:
        if(connStatusObj != null)
        {     //connection-status object is available
          connStatusObj.setData("Not Connected",
                           "No server information available; not connected",
                                         ConnStatusInterface.RED_COLOR_IDX);
        }
        if(initialFlag)
        {     //first connect attempt; log message
          logObj.debug("No server configuration available; not connected");
          if(connStatusObj != null)              //if status obj avail then
            connStatusObj.showPopupDialog();     //show status popup
        }
        retFlag = false;       //indicate connection attempt not made
      }
      connectingFlag = false;  //indicate connecting no longer in progress
      return retFlag;
    }
  }

  /**
   * Initializes the connection to the server.
   * @return true if a connect attempt was made, false if not (because
   * of no server-locator information).
   */
  public boolean initializeConnection()
  {
    return initializeConnection(true);
  }

  /**
   * Closes the connection to the server.
   * @param showPopupFlag true to force the connection status popup
   * dialog to be shown.
   * @param waitFlag if true and a close-connection attempt is already
   * in progress then this method waits until the attempt completes
   * and repeats the close-connection attempt if necessary.
   */
  public void closeConnection(boolean showPopupFlag,boolean waitFlag)
  {
    if(waitFlag)
    {    //wait for close to complete
      try
      {
        if(!closingFlag)
        {     //connection not already closing
          doCloseConnection(showPopupFlag);      //close connection
        }
        else
        {     //connection close already in progress; wait for it
          int count = 50;    //5 second timeout
          do
          {   //loop while waiting for connection to close
            UtilFns.sleep(100);        //delay 0.1 seconds between checks
            if(!closingFlag)
            {      //connection close attempt complete
                        //if connection not closed then try again:
              if(qwConnectorObj != null &&
                                        qwConnectorObj.getInitializedFlag())
              {
                doCloseConnection(showPopupFlag);  //close connection
              }
              break;         //exit loop
            }
          }
          while(--count > 0);
        }
      }
      catch(Exception ex)
      {
        logObj.warning("Exception in 'closeConnection()':  " + ex);
      }
    }
    else      //don't wait for close to complete
      doCloseConnection(showPopupFlag);
  }

  /**
   * Closes the connection to the server.
   */
  public void closeConnection()
  {
    doCloseConnection(false);
  }

  /**
   * Closes the connection to the server.
   * @param showPopupFlag true to force the connection status popup
   * dialog to be shown.
   */
  protected void doCloseConnection(boolean showPopupFlag)
  {
    logObj.debug4("Called 'closeConnection(" + showPopupFlag + ")'");
    if(connectingFlag || closingFlag)
    {    //connect or disconnect in progress
      logObj.debug2("Aborted 'closeConnection()', connectingFlag=" +
                           connectingFlag + ", closingFlag=" + closingFlag);
      return;                     //abort method
    }
    synchronized(connectionSyncObj)
    {    //only allow one thread in at a time
              //if not reinitializing connection to server then
              // terminate server tracking thread:
      if(!reinitingFlag && serverTrackingThreadObj != null)
      {  //not reinitialzing and server tracking thread is running
        serverTrackingThreadObj.terminate();     //terminate thread
        serverTrackingThreadObj = null;          //indicate terminated
      }
                        //make sure any msg-fetch in progress is terminated:
      msgHandlerObj.terminateFetchAndProcessMsgs();
                        //disable message processing:
      msgHandlerObj.setProcessingEnabledFlag(false);
                        //clear any queued messages:
      msgHandlerObj.clearWaitingMsgsQueueTable();
      if(connectingFlag || closingFlag)  //recheck flags after synchronize
      {  //connect or disconnect in progress
        logObj.debug2("Aborted 'closeConnection()', connectingFlag=" +
                           connectingFlag + ", closingFlag=" + closingFlag);
        return;                   //abort method
      }
      closingFlag = true;    //indicate disconnecting in progress
      if(qwConnectorObj != null && qwConnectorObj.getInitializedFlag())
      {    //QW connector was initialized OK
        qwConnectorObj.shutdownConnection(showPopupFlag);  //close connection
      }
      closingFlag = false;   //indicate disconnecting no longer in progress
    }
  }

  /**
   * Closes and reopens the connection to the server.
   * @param showPopupFlag true to force the connection status popup
   * dialog to be shown.
   * @param sepThreadFlag true to use a separate thread (to prevent
   * menu item or dialog that called this from being blocked during
   * reinitialization), false to use same thread (i.e. if called from
   * server tracking thread).
   * @param userInitFlag true to indicate that the connnection-reinit
   * was signaled via user input.
   * @return true if reinitialization attempt made, false if aborted
   * because connect or disconnect already in progress or "inhibit" flag
   * set.
   */
  public boolean reinitConnection(final boolean showPopupFlag,
                           boolean sepThreadFlag,final boolean userInitFlag)
  {
    logObj.debug4("Called 'reinitConnection()', showPopupFlag=" +
                        showPopupFlag + ", sepThreadFlag=" + sepThreadFlag +
                                          ", userInitFlag=" + userInitFlag);
    if(inhibitReinitFlag)
    {    //inhibit flag set
      logObj.debug2("Aborted 'reinitConnection()', inhibitReinitFlag=true");
      return false;               //abort method
    }
    if(reinitingFlag || connectingFlag || closingFlag)
    {    //connect or disconnect in progress
      logObj.debug2("Aborted 'reinitConnection()', reinitingFlag=" +
                      reinitingFlag + ", connectingFlag=" + connectingFlag +
                                            ", closingFlag=" + closingFlag);
      return false;               //abort method
    }
    synchronized(connectionSyncObj)
    {    //only allow one thread in at a time; recheck flags after sync
      if(reinitingFlag || connectingFlag || closingFlag)
      {       //connect or disconnect in progress
        logObj.debug2("Aborted 'reinitConnection()', reinitingFlag=" +
                      reinitingFlag + ", connectingFlag=" + connectingFlag +
                                            ", closingFlag=" + closingFlag);
        return false;             //abort method
      }
      reinitingFlag = true;       //indicate connection reinit in progress
      try
      {
        logObj.info("Reinitializing connection to server");
        if(sepThreadFlag)
        {  //separate-thread flag is set
           //reinitialize in separate thread to prevent menu item or dialog
           // that called this from being blocked during reinitialization:
          (new Thread("reinitConnection")
              {
                public void run()
                {
                  try
                  {                      //slight delay to allow menu item or
                    try { sleep(100); }  // dialog that called this to clear
                    catch(InterruptedException ex) {}
                    doReinitConnection(showPopupFlag,userInitFlag);
                    reinitingFlag = false;  //indicate conn reinit complete
                  }
                  catch(Exception ex)
                  {     //some kind of exception error; log warning message
                    logObj.warning("reinitConnection() thread:  " + ex);
                    logObj.debug(UtilFns.getStackTraceString(ex));
                    reinitingFlag = false;  //indicate conn reinit complete
                  }
                }
              }).start();
        }
        else
        {          //do reinitialization in same thread
          doReinitConnection(showPopupFlag,userInitFlag);
          reinitingFlag = false;       //indicate connection reinit complete
        }
      }
      catch(Exception ex)
      {  //some kind of exception error; log warning message
        logObj.warning("reinitConnection() error:  " + ex);
        logObj.debug(UtilFns.getStackTraceString(ex));
        reinitingFlag = false;         //indicate connection reinit complete
      }
    }
    return true;
  }

  /**
   * Closes and reopens the connection to the server.
   * @return true if reinitialization attempt made, false if aborted
   * because connect or disconnect already in progress.
   */
  public boolean reinitConnection()
  {
    return reinitConnection(false,false,false);
  }

  /**
   * Performs the actual work of closing and reopening the connection
   * to the server.
   * @param showPopupFlag true to force the connection status popup
   * dialog to be shown.
   * @param userInitFlag true to indicate that the connnection-reinit
   * was signaled via user input.
   */
  protected void doReinitConnection(boolean showPopupFlag,
                                                       boolean userInitFlag)
  {
    if(qwConnectorObj != null)                 //if manager OK then
      qwConnectorObj.invalidateConnection();   //invalidate conn
         //if server tracker running then indicate reconnecting:
    if(serverTrackingThreadObj != null)
      serverTrackingThreadObj.connReinitialized(userInitFlag);
         //if status tracker running then indicate connection changed:
    if(statusCheckingThreadObj != null)
      statusCheckingThreadObj.connectionStatusChanged();
         //if event channel connected OK then clear error-conn-status-shown
         // flag so that popup may appear after next failed connect:
    if(isEventChannelConnected())
      errorStatusPopupShownFlag = false;
    doCloseConnection(showPopupFlag);       //close connection
         //if flag set and manager OK then show status popup:
    if(showPopupFlag && qwConnectorObj != null)
      qwConnectorObj.showConnPanelPopup();
         //if the server is being changed then make sure that an entry
         // for the last-used server exists in the alternate-servers list:
    final String serverHostAddrStr =   //get addr/port for specified server
                                 cfgObj.serverHostAddressProp.stringValue();
    final int serverPortNumVal = cfgObj.serverPortNumberProp.intValue();
    boolean diffServerFlag = true, altListChangedFlg = false;
    if(lastServerHostPortObj != null &&
                                lastServerHostPortObj.hostAddrStr != null &&
                      lastServerHostPortObj.hostAddrStr.trim().length() > 0)
    {    //last-used server object exists and has valid hostname
      if(!lastServerHostPortObj.equals(serverHostAddrStr,serverPortNumVal))
      {  //specified server is different from last-used server
              //add last-used server to alt-servers list (if missing):
        altListChangedFlg =
                          altServersListMgr.addEntry(lastServerHostPortObj);
      }
      else    //specified server is same as last-used server
        diffServerFlag = false;
    }
    if(diffServerFlag)
    {    //specified server is different from last-used server
                   //setup last-used server object for next time:
      lastServerHostPortObj = new AddrPortListMgr.EntryBlock(
                                        serverHostAddrStr,serverPortNumVal);
         //if alt-servers list contains specified server then remove it:
      if(altServersListMgr.removeEntry(lastServerHostPortObj) ||
                                                        altListChangedFlg)
      {       //alt-servers list was changed
                   //fire alt-servers list commit to save configuration:
        altServersListMgr.fireListCommit(this);
        logObj.debug2("doReinitConnection:  " +       //log debug message
                                         "New alternate servers list:  \"" +
                              altServersListMgr.getEntriesListStr() + "\"");
      }
    }
    initializeConnection(false);       //initialize connnection
  }

  /**
   * Sets the server-address and port-number configuration
   * properties to the given values.
   * @param addrStr the server-address value to use.
   * @param portNum the port-number value to use.
   * @param inhibitFlag true to set "inhibit" flag so that
   * 'reinitConnection()' will not be called.
   */
  protected void setServerAddressPortValues(String addrStr,int portNum,
                                                        boolean inhibitFlag)
  {
    final boolean saveInhibitFlag = inhibitReinitFlag;
         //if flag param then set "inhibit" flag so that changing server
         // connection property-values won't result in reinit being called:
    if(inhibitFlag)
      inhibitReinitFlag = inhibitFlag;
    cfgObj.serverHostAddressProp.setValue(addrStr);
    cfgObj.serverPortNumberProp.setValue(portNum);
    if(inhibitFlag)          //if flag param then restore value
      inhibitReinitFlag = saveInhibitFlag;
  }

  /**
   * Reinitializes the connection, using an alternate server (if available).
   * @param compAddrStr host address to compare to.
   * @param compPortNum port number to compare to.
   * @param sepThreadFlag true to use a separate thread (to prevent
   * menu item or dialog that called this from being blocked during
   * reinitialization), false to use same thread (if called from server
   * tracking thread).
   * @return true if the current server was switched to one other than
   * the one specified by the given host address and port number.
   */
  public boolean reinitConnWithAlternateServer(String compAddrStr,
                                      int compPortNum,boolean sepThreadFlag)
  {
    if(cfgObj.altServersEnabledFlagProp.booleanValue())
    {    //fail-over reconnect to alternate servers enabled
      if(altServersListMgr.size() > 0)
      {    //alternate-servers list is not empty
        AddrPortListMgr.EntryBlock blkObj,bk2Obj;
           //pop the first entry off the list of alternate servers:
        if((blkObj=altServersListMgr.popFirstEntry()) != null)
        {  //first entry popped off OK, get current-server addr/port#
          final String serverHostAddrStr =
                                 cfgObj.serverHostAddressProp.stringValue();
          final int serverPortNumVal =
                                     cfgObj.serverPortNumberProp.intValue();
              //if popped entry is same as current server then
              // try next entry, repeatedly (up to 100 times):
          int c = 100;
          boolean equalsFlag;
          while((equalsFlag=blkObj.equals(
                                     serverHostAddrStr,serverPortNumVal)) &&
                         (bk2Obj=altServersListMgr.popFirstEntry()) != null)
          {
            blkObj = bk2Obj;
            if(--c <= 0)
              break;
          }
          if(!equalsFlag)
          {   //popped entry is not same as current server
            logObj.debug("Switching to alternate server \"" + blkObj +
                                        "\" and reinitializing connection");
                   //add current-server entry to end of alt-servers list:
            altServersListMgr.addEntry(serverHostAddrStr,serverPortNumVal);
            logObj.debug2("reinitConnWithAlternateServer:  " +
                                         "New alternate servers list:  \"" +
                              altServersListMgr.getEntriesListStr() + "\"");
                //set new current server to popped-server entry:
            setServerAddressPortValues(
                                 blkObj.hostAddrStr,blkObj.portNumber,true);
                //reinitialize connection (with 'userInitFlag'==false):
            reinitConnection(false,sepThreadFlag,false);
                //fire alt-servers list commit to save configuration:
            altServersListMgr.fireListCommit(this);
                //compare new server's host addr and port # to given values:
            return !blkObj.equals(compAddrStr,compPortNum);
          }
          else
          {   //popped entry same as current server
            logObj.debug("reinitConnWithAlternateServer:  " +
                                 "Alt-server entry same as current server");
          }
        }
        else
        {  //unable to pop off first entry; log message
          logObj.debug("reinitConnWithAlternateServer:  " +
                                         "'popFirstEntry()' returned null");
        }
      }
      else
      {  //alternate-servers list is empty
        logObj.debug2("reinitConnWithAlternateServer:  " +
                             "No alternate servers available to switch to");
      }
    }
    else
    {    //fail-over reconnect to alternate servers not enabled
      logObj.debug2("reinitConnWithAlternateServer:  " +
                    "Fail-over reconnect to alternate servers not enabled");
    }
         //no alternate servers are available
    reinitConnection(false,sepThreadFlag,false); //reinitialize connection
    return false;                                //indicate no switch
  }

  /**
   * Invokes the call-back object setup via the
   * 'setSaveToConfigFileCallBackObj()' method.
   */
  public void fireSaveToConfigFileCallBack()
  {
    if(saveToConfigFileCallBackObj != null)
    {    //save-to-config-file call-back is setup
      logObj.debug2(
                  "QWConnectionMgr invoking save-to-config-file call-back");
      try
      {            //invoke call-back method:
        saveToConfigFileCallBackObj.dataChanged(this);
      }
      catch(Exception ex)
      {  //some kind of exception error; log it
        logObj.warning(
            "QWConnectionMgr.fireSaveToConfigFileCallBack() error:  " + ex);
        logObj.warning(UtilFns.getStackTraceString(ex));
      }
    }
  }

  /**
   * Performs the initial request of messages from the server, via a
   * worker thread.  This method also enables message processing in
   * the message manager (after the message request is completed).
   * @param serverChangedFlag true if this is the initial fetch from a
   * "new" server (one that was not connected to previously) and the
   * "requestSourced...()" methods should be used.
   */
  public void initialRequestMessagesFromServer(
                                            final boolean serverChangedFlag)
  {
         //if resend-request of messages from server not available or if
         // "max age of events requested from server" is 0 then just return:
    if(!qwConnectorObj.isReqServerMsgsAvailable() ||
                                              getMaxServerEventAgeMs() <= 0)
    {                        //clear last-received-msgNum value:
      msgHandlerObj.clearLastReceivedMsgNum();
      msgHandlerObj.setProcessingEnabledFlag(true);    //enable msg proc
      return;
    }
         //create and run worker thread:
    (new Thread("initialRequestMessagesFromServer")
        {
          public void run()
          {
            try
            {
              String errMsgStr = null;   //error message via dialog popup
                     //if call-back object setup then call "prepare" method:
              if(initReqMsgsCallBackObj != null)
                initReqMsgsCallBackObj.fetchMessagesPrepare();
                     //request initial set of messages from server via
                     // separate thread (if last msg in storage from a
                     // different server then force server-changed flag):
              if(msgHandlerObj.fetchAndProcInitMessagesFromServer(
                                initReqMsgsDialogObj,initReqMsgsProgIndObj,
                        serverChangedFlag || !isLastMsgFromCurrentServer()))
              {      //fetch-and-process worker thread launched OK
                if(initReqMsgsDialogObj != null)
                {    //dialog-popup object given; show dialog
                  final Object retObj = initReqMsgsDialogObj.showAndWait();
                  logObj.debug2("Object returned by receiving-events " +
                                              "'popup.show()':  " + retObj);
                  if(retObj == null || retObj == initReqMsgsCancelString)
                  {  //popup closed or "Cancel" button pressed by user
                          //terminate message-request-receive thread:
                    msgHandlerObj.terminateFetchAndProcessMsgs();
                          //clear last-received-msgNum value so that
                          // resend-requests triggered via the alive-msg
                          // will not try to request the rest of the messages:
                    msgHandlerObj.clearLastReceivedMsgNum();
                    logObj.debug("Initial request of messages from server " +
                                                  "canceled by user input");
                  }
                  else
                  {  //popup not closed by user
                    if((errMsgStr=
                         initReqMsgsDialogObj.getUserMessageString()) != null)
                    { //message string was entered into the popup object
                          //clear last-received-msgNum value so that
                          // resend-requests triggered via the alive-msg
                          // will not try to request the rest of the messages:
                      msgHandlerObj.clearLastReceivedMsgNum();
                    }
                  }
                }
                else
                {    //dialog-popup object not given
                  if(msgHandlerObj.isFetchAndProcessMessagesRunning())
                  {  //fetch-and-process thread still running
                          //wait for fetch-and-process thread to complete;
                          // if more than 30 seconds elapses without a
                          // message received then timeout and abort:
                    int cnt = 0;
                    while(true)
                    {     //wait up to 30 seconds for message or complete
                      UtilFns.sleep(100);
                      if(!msgHandlerObj.isFetchAndProcessMessagesRunning())
                        break;           //if thread complete then exit loop
                      if(msgHandlerObj.checkFetchAndProcMsgReceived())
                        cnt = 0;       //if msg received then restart timeout
                      else if(++cnt >= 300)
                      {   //timeout reached
                        errMsgStr =      //setup error message
                               "initialRequestMessagesFromServer() error:" +
                             "  Timeout reached before fetch-and-process " +
                                                         "thread completed";
                        logObj.warning(errMsgStr);      //log message
                        break;                          //exit loop
                      }
                    }
                  }
                }
              }
              else
              {      //fetch-and-process worker thread already running
                logObj.warning("Unable to perform initial request of " +
                    "messages from server; request-thread already running");
              }
                     //if call-back object setup then call "complete" method:
              if(initReqMsgsCallBackObj != null)
                initReqMsgsCallBackObj.fetchMessagesComplete(errMsgStr);
              if(initReqMsgsDialogObj != null &&
                           msgHandlerObj.isFetchAndProcessMessagesRunning())
              { //popup was given and fetch-and-process thread still running
                int cnt = 50;
                while(true)
                {         //wait up to 5 seconds for thread to finish
                  UtilFns.sleep(100);
                  if(!msgHandlerObj.isFetchAndProcessMessagesRunning())
                    break;          //if thread complete then exit loop
                  if(--cnt <= 0)
                  {                 //if timeout reached then exit loop
                    logObj.debug("initialRequestMessagesFromServer():" +
                             "  Timeout reached before fetch-and-process " +
                                                        "thread completed");
                    break;
                  }
                }
              }
            }
            catch(Exception ex)
            {      //some kind of exception error; log it
              logObj.warning("Error in 'initialRequestMessagesFromServer'" +
                                                         " thread:  " + ex);
              logObj.warning(UtilFns.getStackTraceString(ex));
            }
                   //if not closing or reinitializing connection then
                   // make sure message processing is enabled:
            if(!closingFlag && !reinitingFlag)
              msgHandlerObj.setProcessingEnabledFlag(true);
                   //if status tracker running then get client
                   // check-in to be performed immediately:
            if(statusCheckingThreadObj != null)
              statusCheckingThreadObj.connectionStatusChanged();
                   //fire work-complete listeners:
            initReqMsgsDoneLstnrSupportObj.fireListeners();
          }
        }).start();
  }

  /**
   * Requests that messages newer or equal to the specified time value or
   * later than the specified message number be returned from the server.
   * If a 'hostMsgNumListStr' value is not given and a non-zero message
   * number is given and the given time value matches then messages with
   * message numbers greater than the given message number are returned;
   * otherwise messages newer or equal to the specified time value are
   * returned (within a tolerance value).  If a 'hostMsgNumListStr' value
   * is given then it is used with the QWServices "requestSourced...()"
   * methods (and any given message number is ignored).
   * @param timeVal the time-generated value for message associated with
   * the given message number, or the requested time value to be used
   * (milliseconds since 1/1/1970).
   * @param msgNum the message number to use, or 0 or none.
   * @param hostMsgNumListStr a list of feeder-data-source
   * host-name/message-number entries, or null or empty string
   * if the "requestSourced...()" methods should not be used.
   * @return An XML-formatted string containing the messages, or an empty
   * string if an error occurs.
   */
  public String requestServerMessages(long timeVal,long msgNum,
                                                   String hostMsgNumListStr)
  {
    return (qwConnectorObj != null) ? qwConnectorObj.requestServerMessages(
                                     timeVal,msgNum,hostMsgNumListStr) : "";
  }

  /**
   * Calls the QWServices 'clientStatusCheck()' method.  If client-upgrade
   * information is available, an upgrade-info call-back object has
   * been setup (via the 'setUpgradeInfoCallBackObj()' method), the
   * update-info call-back is enabled and enough time has elapsed
   * since the last time it was called then the upgrade information string
   * is fetched from the server and the upgrade-info call-back method is
   * invoked with the information string.
   * @throws NoSuchMethodException if the 'clientStatusCheck()' method
   * is not implemented on the server.
   * @throws StatusCheckFailedException if the call to 'clientStatusCheck()'
   * failed with an exception other than 'NoSuchMethodException'.
   */
  public void performClientStatusCheck() throws NoSuchMethodException,
                              QWAbstractConnector.StatusCheckFailedException
  {
    if(qwConnectorObj != null)
    {    //QW connector object is OK
      if(qwConnectorObj.performClientStatusCheck() &&
                                             upgradeInfoCallBackObj != null)
      {  //upgrade information is available and call-back obj is setup
        final long currentTimeVal = System.currentTimeMillis();
        final long callUpgAvailMS;
        final long lastTimeVal;
        if((callUpgAvailMS=cfgObj.callUpgradeAvailHoursProp.longValue()*
                                                 UtilFns.MS_PER_HOUR) > 0 &&
          ((lastTimeVal=cfgObj.lastUpgradeAvailTimeProp.longValue()) <= 0 ||
                            currentTimeVal >= lastTimeVal + callUpgAvailMS))
        {     //upgrade call-back is enabled and it has never been called or
              // enough time has elapsed since the last time it was called
          final String infoStr;        //fetch upgrade info from server:
          if((infoStr=qwConnectorObj.fetchClientUpgradeInfoFromServer()) !=
                                                                       null)
          {   //upgrade info fetched OK; enter time-of-last-call-back value
            cfgObj.lastUpgradeAvailTimeProp.setValue(currentTimeVal);
                        //return info string via call-back method
                        // (indicate that client is connected):
            invokeUpgradeInfoCallBack(infoStr,false);
          }
        }
      }
    }
  }

  /**
   * Returns information about available client-program upgrades via
   * the QWServices 'getClientUpgradeInfo()' method.
   * @return An XML-formatted string containing information about
   * available client-program upgrades, or null if an error occurred.
   */
  public String fetchClientUpgradeInfoFromServer()
  {
    return (qwConnectorObj != null) ?
                   qwConnectorObj.fetchClientUpgradeInfoFromServer() : null;
  }

  /**
   * Invokes the call-back method used to return the upgrade-information
   * string fetched from the server.
   * @param xmlInfoStr XML upgrade-information string to be parsed
   * and used.
   * @param notConnFlag true if the client was unable to connect
   * because its version was too old; false if not.
   */
  protected void invokeUpgradeInfoCallBack(String xmlInfoStr,
                                                        boolean notConnFlag)
  {
    try       //return info string via call-back method
    {         // (indicate that client is connected):
      upgradeInfoCallBackObj.showUpgradeInfo(xmlInfoStr,notConnFlag);
    }
    catch(Exception ex)
    {    //some kind of exception error; log it
      logObj.warning("Error in upgrade-info call-back method:  " + ex);
      logObj.warning(UtilFns.getStackTraceString(ex));
    }
  }

  /**
   * Updates timestamp value for last time upgrade-avail call-back
   * was called.
   */
  public void updateLastUpgradeAvailTime()
  {
         //if current value is >= 10 seconds old then update it:
    final long curTimeVal = System.currentTimeMillis();
    if(curTimeVal - cfgObj.lastUpgradeAvailTimeProp.longValue() >= 10000)
      cfgObj.lastUpgradeAvailTimeProp.setValue(curTimeVal);
  }

  /**
   * Adds the given 'QWAliveMsgListener' object (to be used by the
   * message handler).  This object's method will be called when an
   * alive message is received.
   * @param listenerObj the listener object to add.
   */
  public void addHdlrAliveMsgListener(QWAliveMsgListener listenerObj)
  {
    msgHandlerObj.addAliveMsgListener(listenerObj);
  }

  /**
   * Removes the given 'QWAliveMsgListener' object (used by the
   * message handler).
   * @param listenerObj the listener object to remove.
   */
  public void removeHdlrAliveMsgListener(QWAliveMsgListener listenerObj)
  {
    msgHandlerObj.removeAliveMsgListener(listenerObj);
  }

  /**
   * Adds an 'initialRequestMessagesFromServer'-done listener.  The
   * 'dataChanged()' method on the listener is invoked after the
   * 'initialRequestMessagesFromServer()' method/thread has completed
   * its work.
   * @param listenerObj 'DataChangedListener' object to add.
   */
  public void addInitReqMsgsDoneListener(DataChangedListener listenerObj)
  {
    initReqMsgsDoneLstnrSupportObj.addListener(listenerObj);
  }

  /**
   * Removes an 'initialRequestMessagesFromServer'-done listener.
   * @param listenerObj 'DataChangedListener' object to remove.
   */
  public void removeInitReqMsgsDoneListener(DataChangedListener listenerObj)
  {
    initReqMsgsDoneLstnrSupportObj.removeListener(listenerObj);
  }

  /**
   * Sets the message-processing-enabled and check-missed-msgs-enabled
   * flags.
   * @param flgVal true specifies that queued and incoming messages should
   * now be processed and that missed-message checking should be enabled;
   * false specifies that incoming messages should be put into the
   * waiting-messages queue and that missed-message checking should be
   * disabled.
   */
  public void setMsgProcessingEnabledFlag(boolean flgVal)
  {
    msgHandlerObj.setProcessingEnabledFlag(flgVal);
  }

  /**
   * Terminates any 'fetchAndProcessMessages' thread that is running via
   * the message handler.
   */
  public void terminateHdlrFetchAndProcessMsgs()
  {
    msgHandlerObj.terminateFetchAndProcessMsgs();
  }

  /**
   * Performs exit-cleanup needed for a clean program exit.  The
   * status-checking thread is terminated.
   */
  public void performExitCleanup()
  {
    statusCheckingThreadObj.terminate();
  }

  /**
   * Runs the 'connectionStatusChanged()' method on the status-checking
   * thread to get the client check-in to be performed immediately.  This
   * is used by the 'QWWebSvcsConnector' module to make a client check-in
   * happen right after a successful connect to the server.
   */
  public void fireConnectionStatusChanged()
  {
         //if status tracker running then get client
         // check-in to be performed immediately:
    if(statusCheckingThreadObj != null)
      statusCheckingThreadObj.connectionStatusChanged();
  }

  /**
   * Returns the status of the message handler's
   * 'fetchAndProcessMessagesFromServer()' method.
   * @return true if the 'fetchAndProcessMessagesFromServer()' method's
   * worker thread is running; false if not.
   */
  public boolean isFetchAndProcessMessagesRunning()
  {
    return msgHandlerObj.isFetchAndProcessMessagesRunning();
  }

  /**
   * Sets the identifier for a QWServer 'Acceptor' that should be
   * rejected when a connection attempt is made.  This identifier
   * is used by the QWRelayFeeder to determine if the connected
   * server is its own QWServer.
   * @param accIDStr the QWServer 'Acceptor' identifer to use, or
   * null for none.
   * @param reasonStr a message string describing the reason why
   * the QWServer 'Acceptor' should be rejected.
   */
  public void setAcceptorRejectIDStr(String accIDStr, String reasonStr)
  {
    acceptorRejectIDString = accIDStr;
    acceptorRejectReasonStr = reasonStr;
    if(qwConnectorObj != null)  //if QW connector setup then enter strings
      qwConnectorObj.setAcceptorRejectIDStr(accIDStr,reasonStr);
  }

  /**
   * Returns the connection status to the server event channel.
   * @return true if the connection to the event channel is active, false
   * if not.
   */
  public boolean isEventChannelConnected()
  {
    return (qwConnectorObj != null) ? qwConnectorObj.getConnectedFlag() :
                                                                      false;
  }

  /**
   * Returns an indicator of whether or not the 'requestMessages()'
   * method is available via the 'QWServices' on the current server.
   * @return true if the 'requestServerMessages()' method is available,
   * false if not.
   */
  public boolean isReqServerMsgsAvailable()
  {
    return (qwConnectorObj != null) ?
                          qwConnectorObj.isReqServerMsgsAvailable() : false;
  }

  /**
   * Returns the certificate-file data that was fetched from the server
   * at connect time.
   * @return The certificate-file data that was fetched from the server
   * at connect time, or null if none was fetched.
   */
  public byte [] getCertificateFileDataArr()
  {
    return (qwConnectorObj != null) ?
                          qwConnectorObj.getCertificateFileDataArr() : null;
  }

  /**
   * Returns the status of whether or not the connection has been
   * "validated" via the receipt of any server-alive messages.
   * @return true if any server-alive messages have been received since
   * the last connect-to-server attempt, false if not.
   */
  public boolean isConnectionValidated()
  {
    return (serverTrackingThreadObj != null) ?
                    serverTrackingThreadObj.isConnectionValidated() : false;
  }

  /**
   * Returns the QW connector object held by this object.
   * @return A 'QWAbstractConnector' object.
   */
  public QWAbstractConnector getQWConnectorObj()
  {
    synchronized(qwConnectorSyncObj)
    {    //grab thread-synchronization object for 'qwConnectorObj'
      return qwConnectorObj;
    }
  }

  /**
   * Returns the ID name string for the currently-connected QWServer
   * (defined in the QWServer's configuration file).
   * @return The server ID name string, or null if not available.
   */
  public String getServerIDNameStr()
  {
    return (qwConnectorObj != null) ?
                                 qwConnectorObj.getServerIDNameStr() : null;
  }

  /**
   * Returns the revision string for the currently-connected QWServer.
   * @return The server revision string, or null if not available.
   */
  public String getServerRevisionStr()
  {
    return (qwConnectorObj != null) ?
                               qwConnectorObj.getServerRevisionStr() : null;
  }

  /**
   * Returns the message-handler object.
   * @return A 'QWMessageHandler' object.
   */
  public QWMessageHandler getMsgHandlerObj()
  {
    return msgHandlerObj;
  }

  /**
   * Returns the data-message-processor object.
   * @return An object that implements the 'QWDataMsgProcessor' interface.
   */
  public QWDataMsgProcessor getDataMsgProcObj()
  {
    return dataMsgProcObj;
  }

  /**
   * Returns true if alternate servers are enabled and alternate server
   * entries are available.
   * @return true if alternate servers are enabled and alternate server
   * entries are available, false if not.
   */
  public boolean getAlternateServersAvailFlag()
  {
    return (cfgObj.altServersEnabledFlagProp.booleanValue() &&
                                              altServersListMgr.size() > 0);
  }

  /**
   * Returns the alternate-servers list manager object.
   * @return A 'AddrPortListMgr' object.
   */
  public AddrPortListMgr getAltServersListMgr()
  {
    return altServersListMgr;
  }

  /**
   * Called to indicate that program termination is in progress.
   */
  public void setTerminatingFlag()
  {
    terminatingFlag = true;
  }

  /**
   * Returns the 'QWConnProperties' object holding the connection
   * configuration property items.
   * @return The 'QWConnProperties' object holding the connection
   * configuration property items.
   */
  public QWConnProperties getConnProperties()
  {
    return cfgObj;
  }

  /**
   * Returns the value of the 'debugMissedMsgTestValue' parameter.
   * If this parameter is non-zero then all except 1 out of every
   * 'debugMissedMsgTestValue' messages will be ignored.  This is
   * used to debug-test the detect-missed-messages mechanism.
   * @return The value of the 'debugMissedMsgTestValue' parameter.
   */
  public int getDebugMissedMsgTestValue()
  {
    return cfgObj.debugMissedMsgTestValueProp.intValue();
  }

  /**
   * Returns the log-file object held by this object.
   * @return A 'LogFile' object.
   */
  public LogFile getLogFileObj()
  {
    return logObj;
  }

  /**
   * Returns the OpenORB version string.
   * @return The OpenORB version string.
   */
  public static String getOpenOrbVersionStr()
  {
    return QWCorbaConnector.getOpenOrbVersionStr();
  }

  /**
   * Call-back method invoked when a connect attempt is
   * rejected by the server.  This method implements the
   * 'ConnectRejectCallBack' interface.
   * @param connStatusVal a value indicating the status of the
   * current connection to the server, one of the 'QWServices.CS_'
   * values.
   * @param connStatusStr a message-string describing the status
   * of the current connection to the server.
   */
  public void connectAttemptRejected(int connStatusVal,String connStatusStr)
  {
    if(connStatusVal == QWServices.CS_SERVER_REDIRECT)
    {    //connection being redirected to another server
      if(doServerRedirectFetchAndSwitch())
        return;         //if redirect OK then exit method
      if(connStatusObj != null)
      {  //connection-status object is available; update status message
        connStatusObj.setData(NOTCONN_CLICKFDET_STR,
               "Unable to connect to server:  Error during server redirect",
                                         ConnStatusInterface.RED_COLOR_IDX);
      }
    }
         //invoke connect-attempt-rejected call-back (if set):
    fireConnectAttemptRejectCallBack(connStatusVal,connStatusStr);
    switch(connStatusVal)
    {
      case QWServices.CS_VERSION_OBSOLETE:       //client version too old
              //terminate server-tracking thread:
        synchronized(connectionSyncObj)
        {     //only allow one thread at a time
          if(serverTrackingThreadObj != null)
          {   //server tracking thread is running
            serverTrackingThreadObj.terminate();      //terminate thread
            serverTrackingThreadObj = null;           //indicate terminated
          }
        }
        final String infoStr;
        if(upgradeInfoCallBackObj != null &&
                       (infoStr=fetchClientUpgradeInfoFromServer()) != null)
        {     //upgrade-info call-back obj is setup and info fetched OK
                        //enter time-of-last-call-back value:
          cfgObj.lastUpgradeAvailTimeProp.setValue(
                                                System.currentTimeMillis());
                        //return info string via call-back method
                        // (indicate that client is not connected):
          invokeUpgradeInfoCallBack(infoStr,true);
        }
        break;

      case QWServices.CS_INVALID_LOGIN:          //client login rejected
              //indicate login-attempt failed (return message string):
        connLoginAttemptRejected(connStatusStr);
        break;

      case QWServices.CS_SERVER_REDIRECT:   //redirect to different server
      case QWServices.CS_BAD_DISTNAME:      //client dist-name rejected
      case QWServices.CS_CONN_ERROR:        //server-side connection error
        if(!errorStatusPopupShownFlag)
        {     //error-conn-status popup not shown since last connect:
          errorStatusPopupShownFlag = true;      //indicate shown
          if(connStatusObj != null)              //if conn-status obj avail
            connStatusObj.showPopupDialog();     // then show popup
        }
        break;

      default:
        logObj.warning("Unexpected connection-status value in " +
             "QWConnectionMgr.connectAttemptRejected():  " + connStatusVal);
    }
  }

  /**
   * Method invoked when a connection login attempt is
   * rejected by the server.  This method implements the
   * 'ConnLoginRejectCallBack' interface.
   * @param msgStr a message-string describing the status
   * of the current connection to the server.
   */
  public void connLoginAttemptRejected(String msgStr)
  {
         //terminate server-tracking thread:
    synchronized(connectionSyncObj)
    {    //only allow one thread at a time
      if(serverTrackingThreadObj != null)
      {  //server tracking thread is running
        serverTrackingThreadObj.terminate();     //terminate thread
        serverTrackingThreadObj = null;          //indicate terminated
      }
    }
    final String idStr;
    if(msgStr != null && qwConnectorObj != null &&
                       (idStr=qwConnectorObj.getServerIDString()) != null &&
                                                  idStr.trim().length() > 0)
    {    //conn-status string given and server-ID name fetched OK
      msgStr += " (on '" + idStr + "')";    //append server-ID name
    }
         //indicate login-attempt failed (return message string):
    indicateLoginAttemptRejected(msgStr);
  }

  /**
   * Indicates that the login-to-server attempt failed.  If a
   * 'LoginPropertyEditor' object was entered via the
   * server-login-info object's 'setPropertyEditor()'
   * method then the login dialog will be shown.
   * @param msgStr a descriptive error message string.
   */
  protected void indicateLoginAttemptRejected(String msgStr)
  {
    if(connStatusObj != null && cfgObj.getServerLoginPropertyEditor()
                                        instanceof ShowDialogPropertyEditor)
    {    //connection-status object is available and login property
         // editor in use supports 'showEditorDialog()' method
      connStatusObj.hidePopupDialog();      //clear conn-status popup
    }
              //call method to indicate login failed:
    if(cfgObj.getServerLoginInfoObj().loginAttemptFailed(msgStr))
    {    //user hit "OK" on login dialog
              //get property editor from login-information object:
      final PropertyEditor propEditorObj;
      if((propEditorObj=cfgObj.getServerLoginInfoObj().getPropertyEditor())
                                                                    != null)
      {  //property editor fetched OK
              //get value from property editor and check if changed:
        final Object newInfoValueObj;
        if((newInfoValueObj=propEditorObj.getValue()) != null &&
                 !newInfoValueObj.equals(cfgObj.serverLoginProp.getValue()))
        {    //value changed; enter new value into cfgProp
          cfgObj.serverLoginProp.setValue(newInfoValueObj);
          fireSaveToConfigFileCallBack();   //make sure new value saved
        }
        else  //value not changed; fire listener to trigger reconnect
          cfgObj.serverLoginProp.fireDataChanged();
      }
    }
  }

  /**
   * Fetches the redirect server from the currently-connected server
   * and switches the connection to it.
   * @return true if a redirect-server entry was successfully fetched
   * and processed; false if not.
   */
  protected boolean doServerRedirectFetchAndSwitch()
  {
    final String redirStr;
    if(qwConnectorObj != null &&
               (redirStr=qwConnectorObj.fetchRedirectedServerLoc()) != null)
    {    //QW connector is OK and redirected-server string fetched OK
      final int redirStrlen;
      if((redirStrlen=redirStr.length()) > 0)
      {  //fetched redirected-server string contains data
        logObj.debug("QWConnectionMgr:  Fetched redirect-server " +
                                           "string:  \"" + redirStr + "\"");
        final int colonPos;
        if((colonPos=redirStr.indexOf(':')) > 0 && colonPos < redirStrlen-1)
        {     //colon found in string and is not at end of string
          int commaPos;
          if((commaPos=redirStr.indexOf(',',colonPos+1)) > colonPos+1 &&
                                                   commaPos < redirStrlen-1)
          {   //comma found and is not at end of string
                        //get entries after comma for alternate servers:
            String altStr = redirStr.substring(commaPos+1).trim();
            if(altStr.length() > 0)
            {      //extra entries in redirect-servers string
              logObj.debug("QWConnectionMgr:  Prepending extra fetched " +
                                    "redirect-server entries (\"" + altStr +
                                           "\") to alternate-servers list");
                        //pop-out all alternate-server entries:
              final String oldAltStr = altServersListMgr.popAllEntries();
              if(!altStr.endsWith(","))     //if fetched entries don't end
                altStr += ",";              // with comma then append one
                        //prepend fetched entries and enter new list:
              if(!altServersListMgr.addEntriesListStr(altStr+oldAltStr))
              {    //error entering new list
                logObj.warning("Error updating alternate-servers list:  " +
                                 altServersListMgr.getErrorMessageString());
                altServersListMgr.clearAllEntries();  //clear list
                             //restore previous list:
                if(!altServersListMgr.addEntriesListStr(oldAltStr))
                {  //error restoring previous list
                  logObj.warning("Error restoring previous " +
                                               "alternate-servers list:  " +
                                 altServersListMgr.getErrorMessageString());
                }
              }
            }
          }
          else     //comma not found
            commaPos = redirStrlen;    //use end-of-string position
                   //fetch port-number part of string:
          final String portStr = redirStr.substring(colonPos+1,commaPos);
          try
          {
            logObj.info("Redirected to new server:  " +
                                            redirStr.substring(0,commaPos));
                   //enter server-address and port-number values
                   // (will trigger reconnect to server):
            setServerAddressPortValues(redirStr.substring(0,colonPos),
                                    Integer.parseInt(portStr.trim()),false);
            fireSaveToConfigFileCallBack();      //save new settings
            return true;               //indicate fetched and processed OK
          }
          catch(NumberFormatException ex)
          {   //error parsing port-number value; log warning message
            logObj.warning("Fetched redirect-server string (\"" + redirStr +
                                     "\") port number not a valid integer");
          }
        }
        else
        {     //colon not found in string; log warning message
          logObj.warning("Fetched redirect-server string (\"" + redirStr +
                                      "\") does not contain a port number");
        }
      }
      else    //fetched redirected-server string empty; log warning message
        logObj.warning("Fetched redirect-server string is empty");
    }
    return false;
  }

  /**
   * Compares the given host-address/port-number to those held by the
   * first entry in the list of alternate servers.
   * @param hostAddr the host address to use.
   * @param portNum the port number to use.
   * @return true if the given host-address/port-number equals the
   * host address string and port number fields held by the first
   * entry in the list of alternate servers; false if not.
   */
  public boolean equalsFirstAlternateServer(String hostAddr, int portNum)
  {
    return altServersListMgr.equalsFirstEntry(hostAddr,portNum);
  }

  /**
   * Sets the connect-attempted-finished call-back object.  The call-back
   * method will be invoked when a connect attempt fails to reach the
   * server.  The next connect attempt will not begin until the
   * call-back method returns.
   * @param callBackObj the call-back object to use, or null for none.
   */
  public void setConnectAttemptFinishedCallBackObj(
                                   ConnectAttemptFinishedCallBack callBackObj)
  {
    synchronized(connAttFailCallBackSyncObj)
    {    //only allow one thread at a time to access call-back object
      connAttemptFinishedCallBackObj = callBackObj;
    }
  }

  /**
   * Determines whether or not a connect-attempted-finished call-back object
   * has been set.  The call-back is set via the method
   * 'setConnectAttemptFinishedCallBackObj()'.
   * @return true if a connect-attempted-finished call-back object has been
   * set; false if not.
   */
  public boolean isConnectAttemptFinishedCallBackSet()
  {
    synchronized(connAttFailCallBackSyncObj)
    {    //only allow one thread at a time to access call-back object
      return (connAttemptFinishedCallBackObj != null);
    }
  }

  /**
   * Determines if the server ID for the last message in storage matches
   * the server ID for the currently-connected server.
   * @return true if the server ID for the last message in storage matches
   * the server ID for the currently-connected server; false if not or if
   * the server ID(s) could not be fetched.
   */
  public boolean isLastMsgFromCurrentServer()
  {
    try
    {
      String curSrvrIdStr;                  //fetch server ID from current
      final String msgSrvrIdStr;            // server and from last msg in
      Object obj;                           // storage and compare:
      return (qwConnectorObj != null &&
                (curSrvrIdStr=qwConnectorObj.getServerIDString()) != null &&
                          (curSrvrIdStr=curSrvrIdStr.trim()).length() > 0 &&
                                             dataMsgProcObj != null && (obj=
           dataMsgProcObj.getLastEventInStorage()) instanceof QWMsgRecord &&
              (msgSrvrIdStr=((QWMsgRecord)obj).getServerIDName()) != null &&
                                  curSrvrIdStr.equals(msgSrvrIdStr.trim()));
    }
    catch(Throwable ex)
    {
      logObj.warning("QWConnectionMgr.isLastMsgFromCurrentServer():  " +
                                                                        ex);
      logObj.warning(UtilFns.getStackTraceString(ex));
    }
    return false;
  }

  /**
   * Invokes the connect-attempted-finished call-back method.  This is
   * done after a connect attempt finished to reach the server.  The
   * call-back is set via the method 'setConnectAttemptFinishedCallBackObj()'.
   * The next connect attempt will not begin until the call-back method
   * returns.
   * @param failureFlag true if failure, false otherwise.
   * @param allServersFlag true if connections to all servers have been
   * tried and have failed at least once.
   * @param anyConnectedFlag true if any previous connection-attempts
   * have been successful.
   */
  public void fireConnectAttemptFinishedCallBack(
      boolean failureFlag, boolean allServersFlag,
      boolean anyConnectedFlag)
  {
    try
    {
      final ConnectAttemptFinishedCallBack callBackObj;
      synchronized(connAttFailCallBackSyncObj)
      {  //only allow one thread at a time to access call-back object
        callBackObj = connAttemptFinishedCallBackObj;   //setup local handle
      }
      if (callBackObj == null)
      {
        return;       //if call-back object not set then return
      }
              //execute call-back method outside of thread-sync block:
      callBackObj.connectAttemptFinished(failureFlag,allServersFlag,anyConnectedFlag);
    }
    catch(Exception ex)
    {    //some kind of exception error; log warning
      logObj.warning("QWConnectionMgr:  Error invoking " +
                                "connect-attempted-finished call-back:  " + ex);
      logObj.warning(UtilFns.getStackTraceString(ex));
    }
  }

  /**
   * Sets the connect-attempt-rejected call-back object.  The call-back
   * method will be invoked when a connect attempt reaches the server
   * but is rejected because of obsolete version, invalid login, or
   * some other error.
   * @param callBackObj the call-back object to use, or null for none.
   */
  public void setConnectAttemptRejectCallBackObj(
                                          ConnectRejectCallBack callBackObj)
  {
    synchronized(connAttRejectCallBackSyncObj)
    {    //only allow one thread at a time to access call-back object
      connAttemptRejectCallBackObj = callBackObj;
    }
  }

  /**
   * Invokes the connect-attempt-rejected call-back method.  This is
   * done after a connect attempt reaches the server but is rejected
   * because of obsolete version, invalid login, or some other error.  The
   * call-back is set via the method 'setConnectAttemptRejectCallBackObj()'.
   * @param connStatusVal a value indicating the status of the current
   * connection to the server, one of the 'QWServices.CS_' values.
   * @param connStatusStr a message-string describing the status of
   * the current connection to the server.
   */
  protected void fireConnectAttemptRejectCallBack(int connStatusVal,
                                                      String connStatusStr)
  {
    try
    {
      final ConnectRejectCallBack callBackObj;
      synchronized(connAttRejectCallBackSyncObj)
      {  //only allow one thread at a time to access call-back object
        if(connAttemptRejectCallBackObj == null)
          return;       //if call-back object not set then return
        callBackObj = connAttemptRejectCallBackObj;   //setup local handle
      }
              //execute call-back method outside of thread-sync block:
      callBackObj.connectAttemptRejected(connStatusVal,connStatusStr);
    }
    catch(Exception ex)
    {    //some kind of exception error; log warning
      logObj.warning("QWConnectionMgr:  Error invoking " +
                              "connect-attempt-rejected call-back:  " + ex);
      logObj.warning(UtilFns.getStackTraceString(ex));
    }
  }

  /**
   * Updates the two-way encryption flag for the current
   * server-login-information object.
   */
  public void updateLoginInfoTwoWayEncFlag()
  {
    if(!cfgObj.getServerLoginInfoObj().setTwoWayEncryptionFlag(
                           cfgObj.webServicesServerFlagProp.booleanValue()))
    {
      logObj.warning(
              "Unable to update login-information two-way encryption flag");
    }
  }


  /**
   * Interface UpgradeInfoCallBack defines the method called after an
   * upgrade-information string has been fetched from the server.
   */
  public interface UpgradeInfoCallBack
  {
    /**
     * Call-back method invoked after an upgrade-information string has
     * been fetched from the server.  This method should return quickly.
     * @param xmlInfoStr XML upgrade-information string to be parsed
     * and used.
     * @param notConnFlag true if the client was unable to connect
     * because its version was too old; false if not.
     */
    public void showUpgradeInfo(String xmlInfoStr,boolean notConnFlag);
  }


  /**
   * Interface ConnectAttemptCompletedCallBack defines the method called after a
   * connect attempt finished to reach the server.
   */
  public interface ConnectAttemptFinishedCallBack
  {
    /**
     * Call-back method invoked after a connect attempt finished to reach
     * the server.
     * @param failureFlag true if failure, false otherwise.
     * @param allServersFlag true if connections to all servers have been
     * tried and have failed at least once.
     * @param anyConnectedFlag true if any previous connection-attempts
     * have been successful.
     */
    public void connectAttemptFinished(boolean failureFlag,
        boolean allServersFlag, boolean anyConnectedFlag);
  }
}
