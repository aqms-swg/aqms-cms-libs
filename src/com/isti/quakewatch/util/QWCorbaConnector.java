//QWCorbaConnector.java:  Manages the QuakeWatch host connection and the
//                        CORBA-event-channel used to receive XML text
//                        messages.
//
//  2/18/2003 -- [ET]  Initial version.
//  5/28/2003 -- [ET]  Added support for connecting via a QWServer.
//   6/2/2003 -- [ET]  Implemented client request of immediate 'Alive'
//                     message from server; connection status messages
//                     now show server address info.
//  6/12/2003 -- [ET]  Modified to clear handle to QWServices when
//                     connection closed; added QWServices thread
//                     synchronization; added 'requestServerMessages()'
//                     method.
//  6/13/2003 -- [ET]  Modified 'setConnPanelInitError()' to only show
//                     first line of error message in popup window text.
//  6/18/2003 -- [ET]  Modified 'requestServerMessages()' to log warning
//                     message if empty string returned.
//  6/19/2003 -- [ET]  Added 'isReqServerMsgsAvailable()' method.
//  7/22/2003 -- [ET]  Added retries to 'requestServerMessages()' method.
//  7/30/2003 -- [ET]  Added debug stack trace to 'connectToHost()'
//                     exception-catch; added second-attempt workaround
//                     to 'connectToHost()'; improved connection-failed
//                     error messages.
//  8/11/2003 -- [ET]  Added support for alternate servers.
//  10/2/2003 -- [ET]  Moved from 'QWClient' to 'QWCommon' project;
//                     modified to work outside of 'QWClient' project.
//  11/6/2003 -- [ET]  Added support for "keepDefaultAltServersFlag" (via
//                     'AddrPortListMgr.getKeepDefaultServersFlag()'
//                     method).
//  1/14/2004 -- [ET]  Added optional support for event-message filtering.
//  1/16/2004 -- [ET]  Improved connect/disconnect-thread control.
//  1/29/2004 -- [ET]  Added 'clearConnAttemptStartedFlag()' and
//                     'getConnAttemptStartedFlag()' methods; modified
//                     login username from "qwuser" to "qwuser2".
//   2/4/2004 -- [ET]  Added support for sending a connection-information
//                     properties string when connecting to a server;
//                     modified to show "[host not found]" instead of
//                     CORBA-exception text when server is unreachable.
//  3/29/2004 -- [ET]  Removed 'getConnectDoneFlag()' method and added
//                     'getConnAttemptFailedFlag()' method and support.
//  6/25/2004 -- [ET]  Added 'performClientStatusCheck()' method.
//  7/28/2004 -- [ET]  Added 'fetchClientUpgradeInfoFromServer()' method;
//                     added 'ConnectFailCallBack' and support; modified
//                     to reference 'QWAcceptor.ERROR_MSG_PREFIX' instead
//                     of 'QWServicesStrings.ERROR_MSG_PREFIX'.
//  8/12/2004 -- [ET]  Reduced delay in 'notifyConnectAttemptFailed()'.
//   9/8/2004 -- [ET]  Added "Unable to connect to server:" prefix
//                     to displayed (failed) connection status text.
//  9/15/2004 -- [ET]  Added fetch of certifigate-file data from server
//                     and 'getCertificateFileDataArr()' method.
//  10/1/2004 -- [ET]  Modified to have 'enterConnectionInfo()' method
//                     with username and password parameters.
//  10/4/2004 -- [ET]  Modified to prevent null parameters from being
//                     passed to 'qwAcceptorObj.newConnection()'.
// 10/13/2004 -- [ET]  Modified 'connectToHost()' to fetch server name
//                     before fetching connection-status value.
// 10/22/2004 -- [ET]  Renamed 'ConnectFailedCallBack' interface to
//                     'ConnectRejectCallBack'; renamed method
//                     'notifyConnectAttemptFailed()' to
//                     'notifyConnectAttemptRejected()'; added
//                     'connAttemptRejectedFlag'.
//  11/3/2004 -- [ET]  Moved 'ConnectRejectCallBack' interface to a
//                     separate module.
// 11/12/2004 -- [ET]  Added 'setAcceptorRejectIDStr()' method and
//                     implementation.
// 11/15/2004 -- [ET]  Modified to make ORB use numeric IPs instead of host
//                     names (seems to be needed for bi-directional IIOP).
// 11/18/2004 -- [ET]  Added 'fetchRedirectedServerLoc()' method; added
//                     local timeout to 'shutdownConnection()' method.
// 11/24/2004 -- [ET]  Added methods 'getServerRevisionStr()' and
//                     'disconnectClientServices()'; modified to
//                     call 'disconnectClientServices()' when closing
//                     connection; improved error response to receipt
//                     of empty event-channel locator string from server;
//                     modified to convert 'CORBA.OBJECT_NOT_EXIST'
//                     exception to "service not found" message.
//  12/3/2004 -- [ET]  Modified 'fetchClientUpgradeInfoFromServer()' to log
//                     debug message if method not implemented on server.
//  1/26/2005 -- [ET]  Added 'hostMsgNumListStr' parameter to
//                     'requestServerMessages()' method.
//  9/15/2005 -- [ET]  Renamed from 'QWCorbaManager' to 'QWCorbaConnector'
//                     and modified to extend 'QWAbstractConnector'.
// 12/12/2005 -- [ET]  Added 'logObj.info()' output to 'connect()' method
//                     in 'EventChannelConnectThread' class.
//  8/18/2006 -- [ET]  Modified "setConnPanelData("Disconnected"...)" call
//                     in disconnect thread to explicitly set status color
//                     to 'red'.
//   3/5/2008 -- [ET]  Added methods 'isStatusReportDataAvail()',
//                     'getStatusReportTime()', 'getStatusReportData()'
//                     and 'getServerRepHostAddrStr()'.
//  4/14/2008 -- [ET]  Modified 'performClientStatusCheck()' method to
//                     throw 'StatusCheckFailedException' on error.
//  5/28/2008 -- [ET]  Changed output log level in 'requestServerMessages()'
//                     method from 'debug2' to 'debug'.
//  8/30/2010 -- [ET]  Modfified 'connectToHost()' method to make it use
//                     log level 'debug' (instead of 'warning') for
//                     login-not-valid messages.
//  9/24/2010 -- [ET]  Improved connection-disconnect threading (to make
//                     sure disconnect thread terminates before next connect
//                     thread starts).
//  10/6/2010 -- [ET]  Changed logged connection-shutdown-timeout and
//                     disconnect-client-service-method warnings to debug
//                     messages; modified to log connect-fail messages
//                     at debug level, and then at warning level after
//                     two minutes with errors have elapsed.
//  11/3/2010 -- [ET]  Added debug-log message in response to timeout
//                     flagged by 'orbManagerObj.closeImplementation()'
//                     method.
//  11/5/2010 -- [ET]  Modified server-connect thread to clear util
//                     object used by 'getDecodedStatusReport()' method
//                     so it will work correctly after reconnect.
//  8/21/2012 -- [ET]  Changed output log level for "Timeout waiting for
//                     close-ORB-implementation to complete" message in
//                     'EventChannelDisconnectThread' from 'debug' to
//                     'debug2'.
//

package com.isti.quakewatch.util;

import com.isti.util.UtilFns;
import com.isti.util.LogFile;
import com.isti.util.AddrPortListMgr;
import com.isti.openorbutil.OrbManager;
import com.isti.openorbutil.OrbUtilDeps;
import com.isti.openorbutil.EvtChManager;
import com.isti.openorbutil.MessageProcessor;
import com.isti.quakewatch.common.qw_services.QWAcceptor;
import com.isti.quakewatch.common.qw_services.QWAcceptorHelper;
import com.isti.quakewatch.common.qw_services.QWServices;
import com.isti.quakewatch.common.qw_services.QWServicesHelper;

/**
 * Class QWCorbaConnector manages the QuakeWatch host connection and the
 * CORBA-event-channel used to receive XML text messages.
 */
public class QWCorbaConnector extends QWAbstractConnector
{
    /** Reference string for QWAcceptor object on server ("QWAcceptor"). */
  public static final String QWACCEPTOR_REFSTR = "QWAcceptor";
    /** Time elapsed before logging connect-fail msgs at warning level. */
  protected static final long CONNFAIL_WARN_TIMEMS =
                                                   2L*UtilFns.MS_PER_MINUTE;
  protected final String [] programArgs;
  protected final MessageProcessor msgProcessorObj;
  protected String clientHostAddress = null;
  protected int clientPortNum = 0;
  protected final AddrPortListMgr altServersListMgr;
  protected String errorMessage = null;          //handle for error message
  protected OrbManager orbManagerObj = null;
  protected QWServices qwServicesObj = null;
  protected final Object qwServicesSyncObj = new Object();
  protected EvtChManager evtChManagerObj = null;
              //handle for connect-attempt-rejected call-back:
  protected ConnectRejectCallBack connectRejectCallBackObj = null;
              //false if "requestSourced...()" methods not avail on server:
  protected boolean reqSourcedMsgsAvailFlag = true;
              //flag set true if 'requestServerMessages()' available:
  protected boolean reqServerMsgsAvailFlag = false;
              //handle for current event channel connect thread object:
  protected EventChannelConnectThread evtChConnectThreadObj = null;
              //thread-sync object for 'evtChConnectThreadObj':
  protected final Object evtChConnThreadSyncObj = new Object();
              //handle for current event channel disconnect thread object:
  protected EventChannelDisconnectThread evtChDisconnectThreadObj = null;
              //thread-sync object for 'evtChDisconnectThreadObj':
  protected final Object evtChDisconnThreadSyncObj = new Object();
              //time that last connect-to-server attempt started:
  protected long lastConnectAttemptTime = 0;
              //time of last successful connect-to-server attempt:
  protected long lastConnectSuccessTime = 0;
              //time of start of last connect-attempt "session":
  protected long lastConnectSessionTime = 0;
  //setup dummy reference to CORBA classes needed to create
  // a stand-alone application 'jar' file using ant "classfileset":
  protected static final OrbUtilDeps dummyDepObj = null;


  /**
   * Creates a manager for the CORBA-event-channel used to receive
   * QuakeWatch XML text messages.
   * @param connectRejectCallBackObj call-back object whose method is
   * invoked when a connect-attempt cannot be completed, or null for
   * none.
   * @param programArgs the array of command-line arguments to be passed
   * on to the CORBA ORB.
   * @param msgProcessorObj 'MessageProcessor' object to be used by the
   * CORBA event-channel consumer, or null for none.
   * @param altServersListMgr alternate servers list manager object, or
   * null for none.
   * @param connStatusObj connection-status panel object, or null for none.
   * @param logObj log-file object to be used, or null for none.
   * @param clientHostAddress an IP address to be associated with this
   * client, or null to have the address auto-determined.
   * @param clientPortNum a port address value to be associated with this
   * client, or 0 to have the ORB choose the port address.
   */
  public QWCorbaConnector(ConnectRejectCallBack connectRejectCallBackObj,
                    String [] programArgs, MessageProcessor msgProcessorObj,
                                          AddrPortListMgr altServersListMgr,
                          ConnStatusInterface connStatusObj, LogFile logObj,
                                String clientHostAddress, int clientPortNum)
  {
    super(connStatusObj,logObj);
    this.connectRejectCallBackObj = connectRejectCallBackObj;
    this.programArgs = programArgs;
    this.clientHostAddress = clientHostAddress;
    this.clientPortNum = clientPortNum;
    this.msgProcessorObj = msgProcessorObj;
    this.altServersListMgr = altServersListMgr;
  }

  /**
   * Creates a manager for the CORBA-event-channel used to receive
   * QuakeWatch XML text messages.
   * @param connectRejectCallBackObj call-back object whose method is
   * invoked when a connect-attempt cannot be completed, or null for
   * none.
   * @param programArgs the array of command-line arguments to be passed
   * on to the CORBA ORB.
   * @param msgProcessorObj 'MessageProcessor' object to be used by the
   * CORBA event-channel consumer, or null for none.
   * @param altServersListMgr alternate servers list manager object, or
   * null for none.
   * @param connStatusObj connection-status panel object, or null for none.
   * @param logObj log-file object to be used, or null for none.
   */
  public QWCorbaConnector(ConnectRejectCallBack connectRejectCallBackObj,
                    String [] programArgs, MessageProcessor msgProcessorObj,
                                          AddrPortListMgr altServersListMgr,
                          ConnStatusInterface connStatusObj, LogFile logObj)
  {
    this(connectRejectCallBackObj,programArgs,msgProcessorObj,
                             altServersListMgr,connStatusObj,logObj,null,0);
  }

  /**
   * Updates the connection parameters to be used on the next ORB
   * initialization.
   * @param clientHostAddress an IP address to be associated with this
   * client, or null to have the address auto-determined.
   * @param clientPortNum a port address value to be associated with this
   * client, or 0 to have the ORB choose the port address.
   */
  public synchronized void updateConnectParams(String clientHostAddress,
                                                          int clientPortNum)
  {
    this.clientHostAddress = clientHostAddress;
    this.clientPortNum = clientPortNum;
  }

  /**
   * Starts a new thread that connects to the event channel via a
   * connection to a QWServer.
   * @param serverHostAddrStr the host address used to locate the server.
   * @param serverHostPortNum the port number used to locate the server.
   * @param showPopupFlag true to force the connection status popup
   * dialog to be shown.
   */
  public synchronized void startConnectViaHostInfo(String serverHostAddrStr,
                               int serverHostPortNum, boolean showPopupFlag)
  {
         //log connection parameters in use:
    logObj.debug("QWServer host address = \"" + serverHostAddrStr +
                                  "\", port number = " + serverHostPortNum);
         //make sure any current connect thread is terminated:
    terminateConnectChThread(3);
         //init connect done, attempt-failed and attempt-rejected flags:
    connectDoneFlag = connAttemptFailedFlag = connAttemptRejectedFlag =
                                                                      false;
                             //enter values for host addr and port #:
    serverHostInfoObj.setValues(serverHostAddrStr,serverHostPortNum);
    synchronized(evtChConnThreadSyncObj)
    {    //grab thread-sync object for 'evtChConnectThreadObj'
                             //create new thread for connection attempt:
      evtChConnectThreadObj = new EventChannelConnectThread(
                                       serverHostAddrStr,serverHostPortNum);
         //if flag and connection status object given then show panel:
      if(showPopupFlag && connStatusObj != null)
        connStatusObj.showPopupDialog();    //show status popup panel
      reqSourcedMsgsAvailFlag = true;       //initialize methods-avail flag
      evtChConnectThreadObj.start();        //start connection thread
    }
  }

  /**
   * Starts a new thread that connects to the event channel, locating it
   * via the given string.
   * @param evtChLocStr the locator string to use.
   * @param showPopupFlag true to force the connection status popup
   * dialog to be shown.
   */
//  public synchronized void startConnectViaLocatorString(String evtChLocStr,
//                                                      boolean showPopupFlag)
//  {
//         //log connection parameters in use:
//    if(evtChLocStr != null)
//    {    //locator string not null; log it
//      logObj.debug("CORBA-event-channel locator string:  " + evtChLocStr);
//    }
//         //make sure any current connect thread is terminated:
//    terminateConnectChThread(3);
//         //init connect done, attempt-failed and attempt-rejected flags:
//    connectDoneFlag = connAttemptFailedFlag = connAttemptRejectedFlag =
//                                                                      false;
//                             //enter empty values for host addr/port:
//    serverHostInfoObj.setValues("",0);
//    synchronized(evtChConnThreadSyncObj)
//    {    //grab thread-sync object for 'evtChConnectThreadObj'
//                             //create new thread for connection attempt:
//      evtChConnectThreadObj = new EventChannelConnectThread(evtChLocStr);
//         //if flag and connection status object given then show panel:
//      if(showPopupFlag && connStatusObj != null)
//        connStatusObj.showPopupDialog();    //show status popup panel
//      evtChConnectThreadObj.start();        //start connection thread
//    }
//  }

  /**
   * Starts a new thread that connects to the event channel, locating it
   * via the given string.
   * @param evtChLocStr the locator string to use.
   */
//  public void startConnectViaLocatorString(String evtChLocStr)
//  {
//    startConnectViaLocatorString(evtChLocStr,false);
//  }

  /**
   * Reads the locator string from the given file and then starts a new
   * thread that connects to the event channel, locating it via the loaded
   * string.
   * @param evtChLocFileNameStr the name of the locator file.
   * @param showPopupFlag true to force the connection status popup
   * dialog to be shown.
   * @return true if the locator string was read from the file successfully,
   * false if an error occurred while fetching the locator string.
   */
//  public boolean startConnectViaLocatorFile(String evtChLocFileNameStr,
//                                                      boolean showPopupFlag)
//  {
//    final String locStr;
//    try
//    {         //read CORBA-event-channel locator string from file:
//      locStr =
//            FileUtils.readMultiOpenFileToString(evtChLocFileNameStr).trim();
//    }
//    catch(Exception ex)
//    {              //error opening file; set error message
//      setConnPanelInitError("Error opening event channel locator file (\"" +
//                                       evtChLocFileNameStr + "\"):  " + ex);
//         //if flag and connection status object given then show panel:
//      if(showPopupFlag && connStatusObj != null)
//        connStatusObj.showPopupDialog();    //show status popup panel
//      return false;
//    }
//                   //connect to event channel via locator string:
//    startConnectViaLocatorString(locStr,showPopupFlag);
//    return true;
//  }

  /**
   * Reads the locator string from the given file and then starts a new
   * thread that connects to the event channel, locating it via the loaded
   * string.
   * @param evtChLocFileNameStr the name of the locator file.
   * @return true if the locator string was read from the file successfully,
   * false if an error occurred while fetching the locator string.
   */
//  public boolean startConnectViaLocatorFile(String evtChLocFileNameStr)
//  {
//    return startConnectViaLocatorFile(evtChLocFileNameStr,false);
//  }

  /**
   * Terminates and disposes any event channel connect thread that is
   * running.
   * @param numWaitSecs number of seconds to wait for thread to finish
   * terminating.
   */
  public synchronized void terminateConnectChThread(int numWaitSecs)
  {
    final boolean runningFlag;
    synchronized(evtChConnThreadSyncObj)
    {    //grab thread-sync object for 'evtChConnectThreadObj'
              //set flag if connect thread is running:
      if(runningFlag = (evtChConnectThreadObj != null))
      {       //connect thread is running
        evtChConnectThreadObj.terminate();    //terminate it
        evtChConnectThreadObj = null;         //release thread object
      }
    }
    if(runningFlag)
    {    //previous connect was running
      int count = 0;    //wait for terminate complete, up to given # of secs:
      while(!connectDoneFlag && ++count <= numWaitSecs &&
                                                       UtilFns.sleep(1000));
    }
  }

  /**
   * Shuts down the CORBA connection and waits for the disconnect to
   * complete or timeout.
   * @param showPopupFlag true to force the connection status popup
   * dialog to be shown.
   */
  public void shutdownConnection(boolean showPopupFlag)
  {
    try
    {
      startDisconnectThread();         //start new thread to do disconnect
      if(showPopupFlag)                //if flag then show popup
        showConnPanelPopup();
      logObj.debug3("QWCorbaConnector.shutdownConnection:  " +
                                                  "Waiting for disconnect");
      int cnt = 0;
      while(!disconnectDoneFlag)       //loop until complete or timeout
      {
        if(++cnt > 100)
        {     //local timeout reached
          logObj.debug("QWCorbaConnector:  Timeout waiting for " +
                                       "shutdown-connection to complete; " +
                                   "aborting connection-disconnect thread");
          terminateDisconnectChThread(2);        //abort thread
          if(orbManagerObj != null)
          {  //ORB manager handle OK; force ORB shutdown
            if(!orbManagerObj.closeImplementation(false))
            {  //timeout waiting for close to complete
              logObj.debug("QWCorbaConnector:  Timeout waiting for " +
                             "forced close-ORB-implementation to complete");
            }
            UtilFns.sleep(2000);         //pause to let close complete
          }
          break;
        }
        UtilFns.sleep(100);            //delay between checks
      }
      logObj.debug3("QWCorbaConnector.shutdownConnection:  " +
                                         "Finished waiting for disconnect");
    }
    catch(Exception ex)
    {         //some kind of exception error; log it and exit method
      logObj.warning("Exception in 'shutdownConnection()':  " + ex);
    }
  }

  /**
   * Starts a new thread that disconnects the CORBA connection.
   */
  public synchronized void startDisconnectThread()
  {
         //make sure any current connect thread is terminated:
    terminateConnectChThread(1);
         //make sure any current disconnect thread is terminated:
    terminateDisconnectChThread(3);    // (wait for up to 3 seconds)
    invalidateConnection();       //indicate connection "invalidated"
         //init connect done, attempt-failed and attempt-rejected flags:
    connectDoneFlag = connAttemptFailedFlag = connAttemptRejectedFlag =
                                                                      false;
    synchronized(evtChDisconnThreadSyncObj)
    {    //grab thread-sync object for 'evtChDisconnectThreadObj'
      disconnectDoneFlag = false;           //initial done flag
                             //create new thread for connection attempt:
      evtChDisconnectThreadObj = new EventChannelDisconnectThread();
      evtChDisconnectThreadObj.start();     //start connection thread
    }
  }

  /**
   * Terminates and disposes any event channel disconnect thread that is
   * running.
   * @param numWaitSecs number of seconds to wait for thread to finish
   * terminating.
   */
  public synchronized void terminateDisconnectChThread(int numWaitSecs)
  {
    final boolean runningFlag;
    synchronized(evtChDisconnThreadSyncObj)
    {    //grab thread-sync object for 'evtChDisconnectThreadObj'
              //set flag if disconnect thread is running:
      if(runningFlag = (evtChDisconnectThreadObj != null))
      {       //disconnect thread is running
        evtChDisconnectThreadObj.terminate();    //terminate it
        evtChDisconnectThreadObj = null;         //release thread object
      }
    }
    if(runningFlag)
    {    //previous disconnect was running
      int count = 0;    //wait for terminate complete, up to given # of secs:
      while(!disconnectDoneFlag && ++count <= numWaitSecs &&
                                                       UtilFns.sleep(1000));
    }
  }

  /**
   * Disconnects the consumer implementation from the event channel (if
   * connected).  A new thread is launched to perform the work to keep
   * things from getting blocked if the channel disconnect gets "stuck".
   * @return true if successful (or if consumer not setup), false if
   * timeout occurred before disconnected was confirmed.
   */
  public synchronized boolean disconnectConsumerFromChannel()
  {
    logObj.debug2("disconnectConsumerFromChannel:  Starting disconnect");
    if(evtChManagerObj != null && evtChManagerObj.isConsumerConnected())
    {    //event channel consumer is connected
      (new Thread("disconnectConsumerFromChannel")
        {
          public void run()
          {
            evtChManagerObj.disconnectConsumer();  //disconnect from channel
          }
        }).start();

      int c = 0;
      while(true)
      {  //wait for disconnect to complete, up to 5 seconds
        try { Thread.sleep(100); }          //delay 0.1 seconds
        catch(InterruptedException ex) {}
        if(!evtChManagerObj.isConsumerConnected())
        {     //disconnect successful; log result message
          logObj.debug("Disconnected consumer from event channel OK");
          return true;            //return OK flag
        }
        if(++c >= 50)
        {     //timeout reached; log result message
          logObj.debug(
              "Timeout waiting for consumer disconnect from event channel");
          return false;           //return error flag
        }
      }
    }
    else
    {
      logObj.debug2(
                  "disconnectConsumerFromChannel:  Consumer not connected");
    }
    return true;
  }

  /**
   * Close the CORBA event channel.
   */
  public synchronized void closeEventChannel()
  {
    if(evtChManagerObj != null && evtChManagerObj.isOpen())
    {    //event channel is open
      evtChManagerObj.closeChannel();      //close event channel
      logObj.debug2("closeEventChannel:  Event channel closed");
    }
    else
      logObj.debug2("closeEventChannel:  Event channel was not open");
  }

  /**
   * Performs the work of disconnecting this client from the server
   * services.
   * @return true if successful; false if an error occurred.
   */
  protected boolean doDisconnectClientServices()
  {
    try
    {
      synchronized(qwServicesSyncObj)
      {  //only allow one thread access to QWServices at a time
        if(qwServicesObj != null)
        {     //QWServices available
          logObj.debug3("QWCorbaConnector:  Calling 'disconnectClient()'");
          qwServicesObj.disconnectClient(
                                       (connectionInfoPropsString != null) ?
                          connectionInfoPropsString : UtilFns.EMPTY_STRING);
          logObj.debug3(
                     "QWCorbaConnector:  Returned from 'disconnectClient()'");
          logObj.debug("QWCorbaConnector:  " +
                             "Disconnected client from server services OK");
        }
        disconnectClientServicesFlag = true;     //indicate finished
        return true;
      }
    }
    catch(org.omg.CORBA.BAD_OPERATION ex)
    {  //'disconnectClient()' not implemented on server
      logObj.debug2("QWCorbaConnector:  'disconnectClient()' method not " +
                                                   "implemented on server");
      disconnectClientServicesFlag = true;       //indicate finished
      return true;
    }
    catch(org.omg.CORBA.SystemException ex)
    {  //communications exception (log message at debug level)
      logObj.debug("QWCorbaConnector:  CORBA error calling " +
                                "disconnect-client service method:  " + ex);
      disconnectClientServicesFlag = true;       //indicate finished
      return false;
    }
    catch(Exception ex)
    {  //unexpected exception; log warning message
      logObj.warning("Error calling disconnect-client service method:  " +
                                                                        ex);
      disconnectClientServicesFlag = true;       //indicate finished
      return false;
    }
  }

  /**
   * Returns the server ID name string (defined in the QWServer's
   * configuration file).
   * @return The server ID name string, or null if not available.
   */
  public String getServerIDNameStr()
  {
    synchronized(qwServicesSyncObj)
    {    //only allow one thread access to QWServices at a time
      return (qwServicesObj != null && serverIdNameString != null &&
               serverIdNameString.length() > 0) ? serverIdNameString : null;
    }
  }

  /**
   * Returns the host address reported by the server.
   * @return The host address reported by the server, or null if not
   * available.
   */
  public String getServerRepHostAddrStr()
  {
    synchronized(qwServicesSyncObj)
    {    //only allow one thread access to QWServices at a time
      return (qwServicesObj != null && serverRepHostAddrString != null &&
                                     serverRepHostAddrString.length() > 0) ?
                                             serverRepHostAddrString : null;
    }
  }

  /**
   * Returns the server revision string.
   * @return The server revision string, or null if not available.
   */
  public String getServerRevisionStr()
  {
    synchronized(qwServicesSyncObj)
    {    //only allow one thread access to QWServices at a time
      return (qwServicesObj != null && serverRevisionString != null &&
           serverRevisionString.length() > 0) ? serverRevisionString : null;
    }
  }

  /**
   * Returns the type-name string used on status messages sent out
   * by the server.  When structured messages are enabled, status
   * messages will be sent with the domain name "StatusMessage"
   * and the type name set to this value.  These names can be used
   * on the client side to filter-in the status messages.
   * @return The type-name string used on status messages sent out
   * by the server (when structured messages are enabled), or null
   * if not available.
   */
  public String getStatusMsgTypeNameStr()
  {
    synchronized(qwServicesSyncObj)
    {    //only allow one thread access to QWServices at a time
      return (qwServicesObj != null && statusMsgTypeNameStr != null &&
           statusMsgTypeNameStr.length() > 0) ? statusMsgTypeNameStr : null;
    }
  }

  /**
   * Requests that the server send an 'Alive' message immediately.
   * This allows the connection to the event channel to be verified more
   * quickly.
   */
  public void requestServerAliveMsg()
  {
    try
    {
      synchronized(qwServicesSyncObj)
      {  //only allow one thread access to QWServices at a time
        if(qwServicesObj != null)
        {     //QWServices available
          logObj.debug2("Requesting 'Alive' message from server");
          qwServicesObj.requestAliveMessage();   //request alive message
        }
      }
    }
    catch(Exception ex)
    {
      logObj.warning("Error requesting server 'Alive' message:  " + ex);
    }
  }

  /**
   * Returns an indicator of whether or not the 'requestMessages()'
   * method is available via the 'QWServices' on the current server.
   * @return true if the 'requestServerMessages()' method is available,
   * false if not.
   */
  public boolean isReqServerMsgsAvailable()
  {
    synchronized(qwServicesSyncObj)
    {    //only allow one thread access to QWServices at a time
      return qwServicesObj != null && reqServerMsgsAvailFlag;
    }
  }

  /**
   * Requests that messages newer or equal to the specified time value or
   * later than the specified message number be returned from the server.
   * If a 'hostMsgNumListStr' value is not given and a non-zero message
   * number is given and the given time value matches then messages with
   * message numbers greater than the given message number are returned;
   * otherwise messages newer or equal to the specified time value are
   * returned (within a tolerance value).  If a 'hostMsgNumListStr' value
   * is given then it is used with the QWServices "requestSourced...()"
   * methods (and any given message number is ignored).  An XML message
   * containing the messages is returned, using the following format: <pre>
   * [QWresend NumRequested="10"]
   *   [QWmessage ...] [/QWmessage]
   *   [QWmessage ...] [/QWmessage]
   *   ...
   * [/QWresend]
   * </pre> (Substitute angle brackets for square brackets.)
   * The number of messages that would be required to fill the request
   * is returned in the "NumRequested" attribute.  However, no more than
   * the oldest 'maxResendNumMsgs' number of messages will be returned
   * in any single call to this method (additional calls may be needed to
   * fetch all of the desired messages).
   * @param timeVal the time-generated value for message associated with
   * the given message number, or the requested time value to be used
   * (milliseconds since 1/1/1970).
   * @param msgNum the message number to use, or 0 or none.
   * @param hostMsgNumListStr a list of feeder-data-source
   * host-name/message-number entries, or null or empty string
   * if the "requestSourced...()" methods should not be used.
   * @return An XML-formatted string containing the messages, or an empty
   * string if an error occurs.
   */
  public String requestServerMessages(long timeVal, long msgNum,
                                                   String hostMsgNumListStr)
  {
                   //prefix string for error messages:
    final String ERR_PRESTR = "Error requesting messages from server:  ";
         //set flag if QWServices "requestSourced...()" methods
         // are available and feeder-data-source info was given:
    boolean reqSrcdFlag = reqSourcedMsgsAvailFlag &&
                                               (hostMsgNumListStr != null &&
                                     hostMsgNumListStr.trim().length() > 0);
    int retryCount = 0;
    while(true)
    {    //loop while retrying request
      try
      {
        synchronized(qwServicesSyncObj)
        {  //only allow one thread access to QWServices at a time
          if(qwServicesObj != null)
          {     //QWServices available
            logObj.debug("QWCorbaConnector:  Requesting resend of messages" +
                 " from server, timeVal=" + timeVal + ", msgNum=" + msgNum);
            logObj.debug2("  hostMsgNumListStr:  " +
                                              ((hostMsgNumListStr != null) ?
                                             hostMsgNumListStr : "<null>"));
            final String retStr;
            if(reqSrcdFlag)
            {      //use "requestSourced...()" methods
              try
              {
                if(filterDomainTypeListStr != null)
                {       //filtering enabled; pass in filter-string
                  logObj.debug3("QWCorbaConnector:  Calling " +
                             "QWServices.requestSourcedFilteredMessages()");
                  retStr = qwServicesObj.requestSourcedFilteredMessages(
                         timeVal,hostMsgNumListStr,filterDomainTypeListStr);
                }
                else
                {       //filtering not enabled
                  logObj.debug3("QWCorbaConnector:  Calling " +
                                     "QWServices.requestSourcedMessages()");
                  retStr = qwServicesObj.requestSourcedMessages(
                                                 timeVal,hostMsgNumListStr);
                }
              }
              catch(org.omg.CORBA.BAD_OPERATION ex)
              {    //method not implemented on server
                        //clear use-"requestSourced...()" flags:
                reqSrcdFlag = reqSourcedMsgsAvailFlag = false;
                logObj.debug("QWCorbaConnector:  \"requestSourced...()\" " +
                                        "method not implemented on server");
                continue;         //restart at top of loop
              }
            }
            else
            {      //don't use "requestSourced...()" methods
              if(filterDomainTypeListStr != null)
              {    //filtering enabled; pass in filter-string
                logObj.debug3("QWCorbaConnector:  Calling " +
                                    "QWServices.requestFilteredMessages()");
                retStr = qwServicesObj.requestFilteredMessages(
                                    timeVal,msgNum,filterDomainTypeListStr);
              }
              else
              {         //filtering not enabled
                logObj.debug3("QWCorbaConnector:  Calling " +
                                            "QWServices.requestMessages()");
                retStr = qwServicesObj.requestMessages(timeVal,msgNum);
              }
            }
                   //if non-empty string received then return it:
            if(retStr.length() > 0)
              return retStr;
            logObj.warning(ERR_PRESTR + "Empty string returned");
          }
          else
            logObj.warning(ERR_PRESTR + "QWServices not initialized");
          return UtilFns.EMPTY_STRING;
        }
      }
      catch(Exception ex)
      {
        logObj.warning(ERR_PRESTR + ex);
      }
      if(++retryCount > REQMSGS_NUM_RETRIES)
        return UtilFns.EMPTY_STRING;
      logObj.info("  Retrying request (#-attempts=" + retryCount + ")");
    }
  }

  /**
   * Calls the QWServices 'clientStatusCheck()' method.
   * @return true if the an updated version of the client is available;
   * false if not.
   * @throws NoSuchMethodException if the 'clientStatusCheck()' method
   * is not implemented on the server.
   * @throws StatusCheckFailedException if the call to 'clientStatusCheck()'
   * failed with an exception other than 'NoSuchMethodException'.
   */
  public boolean performClientStatusCheck()
                    throws NoSuchMethodException, StatusCheckFailedException
  {
    try
    {
      synchronized(qwServicesSyncObj)
      {  //only allow one thread access to QWServices at a time
        if(qwServicesObj != null)
        {     //QWServices is available
          logObj.debug4("QWCorbaConnector:  Calling 'clientStatusCheck()'");
          final boolean retFlag = qwServicesObj.clientStatusCheck(
                                       (connectionInfoPropsString != null) ?
                          connectionInfoPropsString : UtilFns.EMPTY_STRING);
          logObj.debug4("QWCorbaConnector:  Return value from " +
                                      "'clientStatusCheck()' = " + retFlag);
          return retFlag;
        }
      }
    }
    catch(org.omg.CORBA.BAD_OPERATION ex)
    {    //'clientStatusCheck()' not implemented on server
      throw new NoSuchMethodException();
    }
    catch(Exception ex)
    {    //some kind of exception error; rethrow with error message
      throw new StatusCheckFailedException(
                       ("Error calling 'clientStatusCheck()':  " + ex), ex);
    }
    return false;
  }

  /**
   * Returns information about available client-program upgrades via
   * the QWServices 'getClientUpgradeInfo()' method.
   * @return An XML-formatted string containing information about
   * available client-program upgrades, or null if an error occurred.
   */
  public String fetchClientUpgradeInfoFromServer()
  {
    try
    {
      synchronized(qwServicesSyncObj)
      {  //only allow one thread access to QWServices at a time
        if(qwServicesObj != null)
        {     //QWServices is available
          logObj.debug3("QWCorbaConnector:  Calling 'getClientUpgradeInfo()'");
          final String retStr = qwServicesObj.getClientUpgradeInfo(
                                       (connectionInfoPropsString != null) ?
                          connectionInfoPropsString : UtilFns.EMPTY_STRING);
          logObj.debug4(
            "QWCorbaConnector:  Returned from 'getClientUpgradeInfo()' OK");
          return retStr;
        }
      }
    }
    catch(org.omg.CORBA.BAD_OPERATION ex)
    {    //'getClientUpgradeInfo()' not implemented on server
      logObj.debug("QWCorbaConnector:  " +
               "Method 'getClientUpgradeInfo()' not implemented on server");
    }
    catch(Exception ex)
    {    //some kind of exception error; log warning message
      logObj.warning("Error calling 'getClientUpgradeInfo()':  " + ex);
    }
    return null;
  }

  /**
   * Returns the location of the server that the client is
   * being redirected to.  The returned string may also be
   * a comma-separated list of "hostAddr:portNum" entries.
   * @return The redirect-server location string in the form
   * "hostAddr:portNum", an empty string if the client is
   * not being redirected, or null if an error occurred.
   */
  public String fetchRedirectedServerLoc()
  {
    try
    {
      synchronized(qwServicesSyncObj)
      {  //only allow one thread access to QWServices at a time
        if(qwServicesObj != null)
        {     //QWServices is available
          logObj.debug3(
                     "QWCorbaConnector:  Calling 'getRedirectedServerLoc()'");
          final String retStr = qwServicesObj.getRedirectedServerLoc();
          logObj.debug4(
            "QWCorbaConnector:  Returned from 'getRedirectedServerLoc()' OK");
          return retStr;
        }
      }
    }
    catch(Exception ex)
    {    //some kind of exception error; log warning message
      logObj.warning("Error calling 'getRedirectedServerLoc()':  " + ex);
    }
    return null;
  }

  /**
   * Determines if get-status-report methods are available on the server.
   * @return true if get-status-report methods are available on the server;
   * false if not.
   */
  public boolean isStatusReportDataAvail()
  {
    return statusReportDataAvailFlag;
  }

  /**
   * Fetches the timestamp value for the latest status report from the
   * server.
   * @return The timestamp value for the latest status report from the
   * server, or 0 if no report is available or if an error occurred.
   */
  public long getStatusReportTime()
  {
    try
    {
      synchronized(qwServicesSyncObj)
      {  //only allow one thread access to QWServices at a time
        if(qwServicesObj != null)
        {  //QWServices is available
          logObj.debug3(
                      "QWCorbaConnector:  Calling 'getStatusReportTime()'");
          final long timeVal = qwServicesObj.getStatusReportTime();
          logObj.debug4(
             "QWCorbaConnector:  Returned from 'getStatusReportTime()' OK");
          return timeVal;
        }
      }
    }
    catch(Exception ex)
    {  //some kind of exception error; log warning message
      logObj.warning("Error calling 'getStatusReportTime()':  " + ex);
    }
    return 0;
  }

  /**
   * Fetches the latest status-report data from the server.
   * @return A string containing the latest status-report data from
   * the server, an empty string if no report is available, or null
   * if an error occurred.
   */
  public String getStatusReportData()
  {
    try
    {
      synchronized(qwServicesSyncObj)
      {  //only allow one thread access to QWServices at a time
        if(qwServicesObj != null)
        {  //QWServices is available
          logObj.debug3(
                      "QWCorbaConnector:  Calling 'getStatusReportData()'");
          final String retStr = qwServicesObj.getStatusReportData();
          logObj.debug4(
             "QWCorbaConnector:  Returned from 'getStatusReportData()' OK");
          return retStr;
        }
      }
    }
    catch(Exception ex)
    {  //some kind of exception error; log warning message
      logObj.warning("Error calling 'getStatusReportData()':  " + ex);
    }
    return null;
  }

  /**
   * Method called when a connect-to-server attempt was rejected.
   * A worker thread and a slight delay are used to give the 'connect'
   * thread a chance to terminate cleanly.
   * @param connStatusVal a value indicating the status of the current
   * connection to the server, one of the 'QWServices.CS_' values.
   * @param connStatusStr a message-string describing the status of
   * the current connection to the server.
   */
  protected void notifyConnectAttemptRejected(final int connStatusVal,
                                                 final String connStatusStr)
  {
    connAttemptRejectedFlag = true;         //indicate rejected
    if(connectRejectCallBackObj != null)
    {    //call-back object setup OK
      (new Thread("ConnectRejectCallBack")
          {                       //invoke call-back via worker thread
            public void run()     // and perform a short delay to allow
            {                     // the 'connect' thread to terminate
              try { sleep(250); }
              catch(InterruptedException ex) {}
              try
              {         //invoke call-back method:
                connectRejectCallBackObj.connectAttemptRejected(connStatusVal,
                                                             connStatusStr);
              }
              catch(Exception ex)
              {    //some kind of exception error; log it
                logObj.warning("ConnectRejectCallBack:  " + ex);
                logObj.warning(UtilFns.getStackTraceString(ex));
              }
            }
          }).start();
    }
  }

  /**
   * Returns the "initialized" status of the CORBA connection.
   * @return true if the ORB is initialized.
   */
  public synchronized boolean getInitializedFlag()
  {
    return (orbManagerObj != null && orbManagerObj.isInitialized());
  }

  /**
   * Returns the event channel locator string.
   * @return The locator string value.
   */
//  public synchronized String getEvtChLocStr()
//  {
//    return evtChLocStr;
//  }

  /**
   * Returns the client host address given to this manager.
   * @return The client host address (possibly null).
   */
  public synchronized String getClientHostAddress()
  {
    return clientHostAddress;
  }

  /**
   * Returns the client port number given to this manager.
   * @return The client port number (possibly 0).
   */
  public synchronized int getClientPortNum()
  {
    return clientPortNum;
  }

  /**
   * Returns the host address entered into the CORBA ORB.
   * @return The host address entered into the CORBA ORB, or null if none.
   */
  public synchronized String getEnteredHostAddress()
  {
    return (orbManagerObj != null) ? orbManagerObj.getEnteredHostAddrStr() :
                                                                       null;
  }

  /**
   * Returns the port number entered into the CORBA ORB.
   * @return The port number entered into the CORBA ORB, or 0 if none.
   */
  public synchronized int getOrbPortNum()
  {
    return (orbManagerObj != null) ? orbManagerObj.getOrbPortNum() : 0;
  }

  /**
   * Returns the completion status of the attempt to connect to the
   * server.
   * @return true if the connection attempt is complete, false is not.
   */
//  public boolean getConnectDoneFlag()
//  {
//    return connectDoneFlag;
//  }

  /**
   * Returns the connection status of the event channel.  If
   * 'invalidateConnection()' has been called then this method will
   * return false until the connection is reinitialized.
   * @return true if the connection to the event channel is active, false
   * if not.
   */
  public boolean getConnectedFlag()
  {
    return (!connInvalidatedFlag && evtChManagerObj != null &&
                                     evtChManagerObj.isConsumerConnected());
  }

  /**
   * Marks the connection as "invalidated", causing the
   * 'getConnectedFlag()' method to return 'false'.
   */
  public void invalidateConnection()
  {
    logObj.debug3("QWCorbaConnector:  Invalidating connection");
    connInvalidatedFlag = true;
  }

  /**
   * Returns the OpenORB version string.
   * @return The OpenORB version string.
   */
  public static String getOpenOrbVersionStr()
  {
    return OrbManager.getOpenOrbVersionStr();
  }


  /**
   * Class EventChannelConnectThread defines a thread used to connect to
   * the CORBA event channel.
   */
  protected class EventChannelConnectThread extends Thread
  {
    private String evtChLocStr = null;
    private final String serverHostAddrStr;
    private final int serverHostPortNum;
    private final String hostIDString;
    private boolean terminateFlag = false;

    /**
     * Creates a thread used to connect to the CORBA event channel.
     * @param serverHostAddrStr the host address used to locate the server.
     * @param serverHostPortNum the port number used to locate the server.
     */
    public EventChannelConnectThread(String serverHostAddrStr,
                                                      int serverHostPortNum)
    {
      super("EventChannelConnectThread");        //set thread name
      this.serverHostAddrStr = (serverHostAddrStr != null) ?
                                            serverHostAddrStr.trim() : null;
      this.serverHostPortNum = serverHostPortNum;
      hostIDString = " (" +       //create host-ID string for use in msgs
                          serverHostAddrStr + ":" + serverHostPortNum + ")";
    }

    /**
     * Creates a thread used to connect to the CORBA event channel.
     * @param evtChLocStr the locator string to use.
     */
    public EventChannelConnectThread(String evtChLocStr)
    {
      super("EventChannelConnectThread");        //set thread name
      this.evtChLocStr = evtChLocStr;
      serverHostAddrStr = null;
      serverHostPortNum = 0;
      hostIDString = UtilFns.EMPTY_STRING;
    }

    /**
     * Runs the connect thread.
     */
    public void run()
    {
      logObj.debug3("QWCorbaConnector:  EventChannelConnectThread started");
      try
      {
        connAttemptStartedFlag = true;      //indicate thread started
        serverIdNameString = null;          //init server ID name string
        encryptDecryptUtilObj = null;       //clear any previous util object
        if((serverHostAddrStr != null && serverHostAddrStr.length() > 0) ||
                                                     (evtChLocStr != null &&
                             (evtChLocStr=evtChLocStr.trim()).length() > 0))
        {     //server address or event ch locator string contains data
                   //attempt to initialize ORB and connect to event channel:
          final boolean connectFlag = connect();
          if(terminateFlag)
          {   //thread is terminating
            logObj.debug2(
                     "QWCorbaConnector:  CORBA connection thread terminated");
            connectDoneFlag = true;    //indicate connection attempt complete
            return;     //(thread object handle should already be null)
          }
          if(connectFlag)
          {   //connected OK; show success message in connection status panel
            final String connectedIdStr = (serverIdNameString != null &&
                                        serverIdNameString.length() > 0) ?
                      (" \"" + serverIdNameString + "\"" + hostIDString) :
                                                             hostIDString;
            setConnPanelData("Connected","Connected to server" +
                           connectedIdStr + "; waiting for first message",
                                    ConnStatusInterface.YELLOW_COLOR_IDX);
            logObj.debug("Connected to CORBA-event-channel" +
                          connectedIdStr + "; waiting for first message");
            if(msgProcessorObj != null)
            { //message processor object OK
                   //clear popup dialog (if not requested by user):
              autoClearConnStatusPopupDialog();
            }
          }
          if(msgProcessorObj == null)
          {  //no message processor object; set error message
            setConnPanelInitError("Message processor object handle is null");
          }
              //create local handle to ORB manager that will not be
              // affected if 'orbManagerObj' is cleared to 'null':
          final OrbManager localOrbMgrObj = orbManagerObj;
          synchronized(evtChConnThreadSyncObj)
          {   //grab thread-sync object for 'evtChConnectThreadObj'
            if(evtChConnectThreadObj != null)    //if not yet cleared then
              evtChConnectThreadObj = null;      //release thread object
            connectDoneFlag = true;    //indicate connection attempt complete
          }
          if(!connectFlag)
          {   //connect attempt failed
            connAttemptFailedFlag = true;        //indicate failed
            return;                              //abort method
          }
              //start the CORBA implementation; this method blocks until
              // the ORB is shutdown (closed) unless an error occurs:
          if(!localOrbMgrObj.runImplementation())
          {   //error running CORBA implementation; set error message
            setConnPanelInitError(localOrbMgrObj.getErrorMessage());
//              orbInitFlag = false;       //indicate ORB not initialized
//              orbClosedFlag = true;      //indicate connection closed
            return;
          }
          logObj.debug2("Returned from CORBA implementation run method");
//            orbClosedFlag = true;       //indicate connection closed
          return;
        }
        else  //no data in locator string; set error message
          setConnPanelInitError("Server configuration data is missing");
      }
      catch(Throwable ex)
      {       //some kind of error; log it
        setConnPanelInitError("Exception error:  " + ex +
                         UtilFns.newline + UtilFns.getStackTraceString(ex));
      }
         //ORB not initialized
      synchronized(evtChConnThreadSyncObj)
      {  //grab thread-sync object for 'evtChConnectThreadObj'
        if(evtChConnectThreadObj != null)        //if not yet cleared then
          evtChConnectThreadObj = null;          //release thread object
        connectDoneFlag = true;        //indicate connection attempt complete
      }
    }

    /**
     * Connects to the server and event channel.
     * @return true if successful, false if error.
     */
    private boolean connect()
    {
      logObj.info("Connecting to server" +
        ((serverHostAddrStr != null && serverHostAddrStr.length() > 0) ?
             (" at '" + serverHostAddrStr + ":" + serverHostPortNum + "'") :
                                                     UtilFns.EMPTY_STRING));
         //set initial state for connection status panel:
      final String startStr = "Initializing connection";
      setConnPanelData(startStr,(startStr + "; initializing ORB"),
                                         ConnStatusInterface.RED_COLOR_IDX);
      try
      {
        if(!getInitializedFlag())
        {     //ORB not initialized
          if(orbManagerObj == null)
          {   //ORB manager not yet created
            logObj.debug2("QWCorbaConnector:  Creating ORB manager object");
            orbManagerObj =
                new OrbManager(programArgs,clientHostAddress,clientPortNum);
                   //set ORB to use numeric IPs instead of host names
                   // (seems to be needed for bi-directional IIOP):
            orbManagerObj.enterPublishNumericIPFlag(true);
          }
          else
          {   //ORB manager already created
            logObj.debug2(
                     "QWCorbaConnector:  Reusing current ORB manager object");
            if(!orbManagerObj.enterConnectParams(     //update connect params
                                             clientHostAddress,clientPortNum))
            {   //error updating parameters; fetch and enter error message
              setConnPanelInitError(orbManagerObj.getErrorMessage());
              return false;              //abort thread
            }
          }
          if(terminateFlag)       //if thread terminating then
            return false;         //abort method
              //initialize ORB:
          if(!orbManagerObj.initImplementation())
          {   //initialize failed; fetch and enter error message
            setConnPanelInitError(orbManagerObj.getErrorMessage());
            return false;              //abort thread
          }
          if(terminateFlag)       //if thread terminating then
            return false;         //abort method
          logObj.debug2(
                     "QWCorbaConnector:  Successfully initialized CORBA ORB");
        }
      }
      catch(Exception ex)
      {       //some kind of exception error occurred
        setConnPanelInitError("Error initializing CORBA ORB:  " + ex);
        return false;             //abort thread
      }
      final String str;
              //if host address entered into ORB then log it:
      if((str=getEnteredHostAddress()) != null)
        logObj.debug("Using local host address:  " + str);
      final int val;
              //if port number entered into ORB then log it:
      if((val=getOrbPortNum()) > 0)
        logObj.debug("Using local port number:  " + val);
              //manage "lastConnect..." timer variables so that
              // connection-fail messages are logged at debug level
              // until the failure lasts 2+ minutes:
      final long curTimeVal = System.currentTimeMillis();
      if(lastConnectSessionTime <= 0)            //if new "session" then
        lastConnectSessionTime = curTimeVal;     //set new session start time
      logObj.debug("QWCorbaConnector:  lastConnectAttemptTime = " +
                                             ((lastConnectAttemptTime > 0) ?
            UtilFns.timeMillisToString(lastConnectAttemptTime) : "(none)"));
      logObj.debug("QWCorbaConnector:  lastConnectSuccessTime = " +
                                             ((lastConnectSuccessTime > 0) ?
            UtilFns.timeMillisToString(lastConnectSuccessTime) : "(none)"));
      final int failLogLevel;
      if(lastConnectAttemptTime <= 0 ||
                         (lastConnectAttemptTime > lastConnectSuccessTime &&
                curTimeVal > lastConnectSessionTime + CONNFAIL_WARN_TIMEMS))
      {  //is first connect, or last attempt failed and no recent warning
        failLogLevel = LogFile.WARNING;     //log conn fail at warning level
        if(lastConnectAttemptTime > 0)
        {  //not first connect attempt
          lastConnectSessionTime = 0;       //reset "session" time
          logObj.debug("QWCorbaConnector:  Unable to connect for " +
                  "greater than " + CONNFAIL_WARN_TIMEMS/1000 + " seconds");
        }
      }
      else
        failLogLevel = LogFile.DEBUG;       //log conn fail at debug level
      lastConnectAttemptTime = curTimeVal;       //set new "attempt" time
      if(serverHostAddrStr != null)
      {  //server address given
        if(!connectToHost(failLogLevel))    //connect to server
          return false;           //if error then return error flag
      }
         //attempt connection to event channel:
      if(connectEventChannel(failLogLevel))
      {  //connect to event channel succeeded
                                       //set new "success" time:
        lastConnectSuccessTime = System.currentTimeMillis();
        lastConnectSessionTime = 0;    //reset "session" time
        return true;                   //return success flag
      }
      return false;
    }

    /**
     * Connects to the host.
     * @param failLogLevel log level for connect-fail messages.
     * @return true if successful, false if error.
     */
    private boolean connectToHost(int failLogLevel)
    {
         //set next state for connection status panel:
      setConnPanelData("Connecting to server",
                       "Initializing; connecting to server" + hostIDString);
      try
      {
        org.omg.CORBA.Object cObj;
        String qwServicesIorStr;
        QWAcceptor qwAcceptorObj;
        final int ATTEMPT_LIMIT = 2;
        int attemptCount = 0;
        while(true)
        {
          if(++attemptCount > 1)
            logObj.debug("Retrying initial connect-to-host connection");
              //use server host address and port number to build Corbaloc
              // address and use it to resolve a CORBA object:
          logObj.debug3("QWCorbaConnector:  Resolving Corbaloc object");
          if((cObj=orbManagerObj.resolveCorbalocObject(serverHostAddrStr,
                                serverHostPortNum,QWACCEPTOR_REFSTR)) == null)
          {   //error resolving Corbaloc
            if(terminateFlag)     //if thread terminating then
              return false;       //abort method
                   //setup error message
            final String errStr = "Unable to connect to server" +
                     hostIDString + "; error resolving Corbaloc object:  " +
                                            orbManagerObj.getErrorMessage();
            if(attemptCount < ATTEMPT_LIMIT)
            {      //not too many attempts
              logObj.debug(errStr);         //log error message
              continue;                     //loop and retry
            }
            setConnPanelInitError(errStr,failLogLevel);
            return false;
          }
              //narrow CORBA object into a 'QWAcceptor' object:
          logObj.debug3("QWCorbaConnector:  Narrowing QWAcceptor object");
          try
          {
            qwAcceptorObj = QWAcceptorHelper.narrow(cObj);
          }
          catch(Exception ex)
          {   //error narrowing 'QWAcceptor'
            if(terminateFlag)          //if thread terminating then
              return false;            //abort method
            if(attemptCount < ATTEMPT_LIMIT)
            {      //not too many attempts; log error message
              logObj.debug("Error narrowing QWAcceptor object:  " + ex);
              continue;                     //loop and retry
            }
            throw ex;
          }
          if(terminateFlag)       //if thread terminating then
            return false;         //abort method
          if(acceptorRejectIDString != null)
          {   //an 'Acceptor' reject ID string is setup
            try
            {   //fetch Acceptor-ID string from server:
              final String idStr;
              logObj.debug3(
                  "QWCorbaConnector:  Calling QWServices.getAcceptorIDStr()");
              idStr = qwAcceptorObj.getAcceptorIDStr();
              logObj.debug("QWCorbaConnector:  Fetched Acceptor-ID " +
                                                       "string:  " + idStr);
              if(acceptorRejectIDString.equals(idStr))
              {    //reject Acceptor-ID same as fetched ID string
                                  //setup error message:
                setConnPanelInitError("Aborting connection to server" +
                            hostIDString + ":  " + acceptorRejectReasonStr);
                return false;     //terminate connection attempt
              }
            }
            catch(org.omg.CORBA.BAD_OPERATION ex)
            {   //method not implemented on server
              logObj.debug("QWCorbaConnector:  'getAcceptorIDStr()' method " +
                                               "not implemented on server");
            }
            catch(Exception ex)
            {   //error fetching Acceptor-ID string from server
              logObj.warning("Error fetching Acceptor-ID string " +
                                                     "from server:  " + ex);
            }
          }
              //fetch QWServices-object IOR (or error message) from server:
          logObj.debug3(
                     "QWCorbaConnector:  Calling QWAcceptor.newConnection()");
          try
          {        //enter username/pwd and conn-info props (or IP if none):
            qwServicesIorStr = qwAcceptorObj.newConnection(
                 ((connectionUserNameString!=null)?connectionUserNameString:
                                                      UtilFns.EMPTY_STRING),
                 ((connectionPasswordString!=null)?connectionPasswordString:
                                                      UtilFns.EMPTY_STRING),
                                      ((connectionInfoPropsString != null) ?
                                                 connectionInfoPropsString :
                                                 UtilFns.getLocalHostIP()));
          }
          catch(Exception ex)
          {   //error narrowing 'QWAcceptor'
            if(terminateFlag)          //if thread terminating then
              return false;            //abort method
            if(attemptCount < ATTEMPT_LIMIT)
            {      //not too many attempts; log error message
              logObj.debug("Error calling QWAcceptor.newConnection():  " +
                                                                        ex);
              continue;                     //loop and retry
            }
            throw ex;
          }
          if(terminateFlag)       //if thread terminating then
            return false;         //abort method
          if(qwServicesIorStr == null || qwServicesIorStr.length() <= 0)
          {   //no data in returned string; set error message
            final String errStr = "Unable to connect to server" +
                           hostIDString + ":  (No error message available)";
            if(attemptCount < ATTEMPT_LIMIT)
            {      //not too many attempts
              logObj.debug(errStr);         //log error message
              continue;                     //loop and retry
            }
            setConnPanelInitError(errStr,failLogLevel);
            return false;
          }
          if(qwServicesIorStr.startsWith(QWAcceptor.ERROR_MSG_PREFIX))
          {   //error message returned; pass it on
            setConnPanelInitError("Unable to connect to server" +
                          hostIDString + ":  " + qwServicesIorStr.substring(
                        QWAcceptor.ERROR_MSG_PREFIX.length()),failLogLevel);
            return false;
          }
          break;
        }
        logObj.debug("Received QWServices IOR string from server:  " +
                                                          qwServicesIorStr);
        logObj.debug3(
           "QWCorbaConnector:  Converting and narrowing QWServices object");
        synchronized(qwServicesSyncObj)
        {     //only allow one thread access to QWServices at a time
                   //convert IOR string to 'QWServices' object:
          qwServicesObj = QWServicesHelper.narrow(
                   orbManagerObj.orbObj.string_to_object(qwServicesIorStr));
              //if thread terminating then throw exception caught below:
          if(terminateFlag)
            throw new RuntimeException("Thread terminated");

          try
          {        //fetch server ID name from server:
            logObj.debug3(
                "QWCorbaConnector:  Calling QWServices.getServerIdNameStr()");
            serverIdNameString = qwServicesObj.getServerIdNameStr();
          }
          catch(Exception ex)
          {   //error fetching ID name
            serverIdNameString = null;      //indicate no name
            logObj.warning("Error fetching server ID name from server:  " +
                                                                        ex);
          }
              //if thread terminating then throw exception caught below:
          if(terminateFlag)
            throw new RuntimeException("Thread terminated");

          try
          {        //fetch host address from server:
            logObj.debug3("QWCorbaConnector:  Calling " +
                                       "QWServices.getServerHostAddrStr()");
            serverRepHostAddrString = qwServicesObj.getServerHostAddrStr();
          }
          catch(Exception ex)
          {   //error host address
            serverRepHostAddrString = null;      //indicate no name
            logObj.warning("Error fetching host address from server:  " +
                                                                        ex);
          }
              //if thread terminating then throw exception caught below:
          if(terminateFlag)
            throw new RuntimeException("Thread terminated");

          try
          {        //fetch server revision string from server:
            logObj.debug3("QWCorbaConnector:  Calling " +
                                    "QWServices.getServerRevisionString()");
            serverRevisionString = qwServicesObj.getServerRevisionString();
            logObj.debug("QWCorbaConnector:  Fetched revision string " +
                          "from server:  \"" + serverRevisionString + "\"");
          }
          catch(org.omg.CORBA.BAD_OPERATION ex)
          {   //method not implemented on server
            serverRevisionString = null;    //indicate revision string
            logObj.debug("QWCorbaConnector:  'getServerRevisionString()' " +
                                        "method not implemented on server");
          }
          catch(Exception ex)
          {   //error fetching server revision string
            serverRevisionString = null;    //indicate revision string
            logObj.warning("Error fetching server revision string " +
                                                     "from server:  " + ex);
          }
              //if thread terminating then throw exception caught below:
          if(terminateFlag)
            throw new RuntimeException("Thread terminated");

          int connStatusVal;
          try
          {   //fetch connection-status value from server:
            logObj.debug3("QWCorbaConnector:  Calling " +
                                     "QWServices.getConnectionStatusVal()");
            connStatusVal = qwServicesObj.getConnectionStatusVal();
            logObj.debug("Fetched connection-status value:  " +
                                                             connStatusVal);
          }
          catch(org.omg.CORBA.BAD_OPERATION ex)
          {   //method not implemented on server
            connStatusVal = QWServices.CS_CONNECT_OK;
            logObj.debug("QWCorbaConnector:  'getConnectionStatusVal()' " +
                                        "method not implemented on server");
          }
          catch(Exception ex)
          {   //error fetching connection-status value from server
            connStatusVal = QWServices.CS_CONNECT_OK;
            logObj.warning("Error fetching connection-status value " +
                                                     "from server:  " + ex);
          }
          if(connStatusVal != QWServices.CS_CONNECT_OK)
          {   //connection not successful
            String connStatusStr;
            try
            {      //fetch connection-status value from server:
              logObj.debug3("QWCorbaConnector:  Calling " +
                                     "QWServices.getConnectionStatusMsg()");
              connStatusStr = qwServicesObj.getConnectionStatusMsg();
              logObj.debug2("Fetched connection-status message:  \"" +
                                                      connStatusStr + "\"");
            }
            catch(Exception ex)
            {      //error fetching connection-status value from server
              connStatusStr = null;
              logObj.warning("Error fetching connection-status string " +
                                                     "from server:  " + ex);
            }
            if(connStatusStr == null || connStatusStr.trim().length() <= 0)
            {      //no error-status message; enter generic message
              connStatusStr = "(Unable to fetch error message)";
            }
            if(connStatusVal != QWServices.CS_SERVER_REDIRECT)
            {      //status is not server-redirect
                        //log and show status message on panel (if invalid
                        // login then log level 'debug', else 'warning'):
              setConnPanelInitError(
                         ("Unable to connect to server:  " + connStatusStr),
                           ((connStatusVal == QWServices.CS_INVALID_LOGIN) ?
                                             LogFile.DEBUG : failLogLevel));
            }
            else
            {      //status is server-redirect
              setConnPanelData("Redirected",connStatusStr,
                                      ConnStatusInterface.YELLOW_COLOR_IDX);
              logObj.info("Connection status:  " + connStatusStr);
            }
                   //fire notify of connect rejected (via call-back):
            notifyConnectAttemptRejected(connStatusVal,connStatusStr);
            return false;
          }
              //if thread terminating then throw exception caught below:
          if(terminateFlag)
            throw new RuntimeException("Thread terminated");

          if(filterDomainTypeListStr != null)
          {    //list of domain/type names contains data
            try
            {   //fetch type-name for status messages from server:
              logObj.debug3("QWCorbaConnector:  Calling " +
                                    "QWServices.getStatusMsgTypeNameStr()");
              statusMsgTypeNameStr = qwServicesObj.getStatusMsgTypeNameStr();
              logObj.debug2("Received type-name for status messages:  \"" +
                                               statusMsgTypeNameStr + "\"");
                   //update 'filterEventTypeArr' with new type-name value:
              updateFilterDomainTypeList();
            }
            catch(Exception ex)
            {   //error fetching ID name
              statusMsgTypeNameStr = null;    //indicate no type-name
              logObj.warning("Error fetching type-name for " +
                                     "status messages from server:  " + ex);
            }
          }
              //if thread terminating then throw exception caught below:
          if(terminateFlag)
            throw new RuntimeException("Thread terminated");

          if(altServersListMgr != null &&
                             !altServersListMgr.getKeepDefaultServersFlag())
          {   //alternate servers list manager OK and flag for
              // keeping only default alt-server entries not set
            String idsListStr;
            try
            {   //fetch list of alternate server IDs:
              logObj.debug3("QWCorbaConnector:  Calling " +
                                    "QWServices.getAltServersIdsListStr()");
              idsListStr = qwServicesObj.getAltServersIdsListStr();
              logObj.debug("Received alternate server IDs list:  \"" +
                                                         idsListStr + "\"");
            }
            catch(Exception ex)
            {   //error fetching list of alternate server IDs
              idsListStr = null;              //indicate no list
              logObj.warning("Error fetching list of alternate server " +
                                                 "IDs from server:  " + ex);
            }
              //if thread terminating then throw exception caught below:
            if(terminateFlag)
              throw new RuntimeException("Thread terminated");
            if(idsListStr != null && idsListStr.length() > 0)
            {   //non-empty list fetched OK
                               //save current contents of list:
              final String oldListStr = altServersListMgr.getEntriesListStr();
              if(!idsListStr.equals(oldListStr))
              {      //fetched alt-servers list different from current one
                               //enter fetched alt-servers list string:
                if(altServersListMgr.setEntriesListStr(idsListStr))
                {    //new list OK; indicate that list should be "committed"
                  altServersListMgr.fireListCommit(this);
                }
                else
                {    //error setting list; log warning message
                  logObj.warning("Error validating fetched list of " +
                                                 "alternate server IDs:  " +
                                 altServersListMgr.getErrorMessageString());
                               //put back old list entries:
                  altServersListMgr.setEntriesListStr(oldListStr);
                }
              }
            }
          }

                   //indicate that request-msgs-from-server is available:
          reqServerMsgsAvailFlag = true;

              //fetch event-channel IOR from server:
          logObj.debug3(
                  "QWCorbaConnector:  Calling QWServices.getEventChLocStr()");
          if((evtChLocStr=qwServicesObj.getEventChLocStr()) == null ||
                                                  evtChLocStr.length() <= 0)
          {   //null or empty string received; set error message
            setConnPanelInitError("Unable to connect to server" +
               hostIDString + ":  No message-channel locator data received",
                                                              failLogLevel);
            return false;
          }
              //if thread terminating then throw exception caught below:
          if(terminateFlag)
            throw new RuntimeException("Thread terminated");
          logObj.debug("Received event-channel IOR string from server" +
                                        hostIDString + ":  " + evtChLocStr);
          try
          {   //fetch certificate-file data from server:
            logObj.debug3("QWCorbaConnector:  Calling " +
                                     "QWServices.getCertificateFileData()");
            certificateFileDataArr = qwServicesObj.getCertificateFileData();
          }
          catch(org.omg.CORBA.BAD_OPERATION ex)
          {   //method not implemented on server
            certificateFileDataArr = null;
            logObj.debug("QWCorbaConnector:  'getCertificateFileData()' " +
                                        "method not implemented on server");
          }
          catch(Exception ex)
          {   //error fetching certificate-file data from server
            certificateFileDataArr = null;
            logObj.warning("Error fetching certificate-file data " +
                                                     "from server:  " + ex);
          }
          if(certificateFileDataArr != null)
          {   //certificate data was fetched
            if(certificateFileDataArr.length > 0)
            {      //certificate data array not empty; log message
              logObj.debug("QWCorbaConnector:  Fetched certificate-file " +
                                               "data from server (length=" +
                                       certificateFileDataArr.length + ")");
            }
            else   //certificate data is a zero-length array
              certificateFileDataArr = null;     //clear handle to array
          }
          try
          {   //test if get-status-report methods available on server:
            logObj.debug3("QWCorbaConnector:  Calling " +
                                     "QWServices.getStatusReportTime()");
            qwServicesObj.getStatusReportTime();
            statusReportDataAvailFlag = true;    //indicate available
          }
          catch(org.omg.CORBA.BAD_OPERATION ex)
          {   //method not implemented on server
            statusReportDataAvailFlag = false;   //indicate not available
            logObj.debug("QWCorbaConnector:  get-status-report " +
                                       "methods not implemented on server");
          }
          catch(Exception ex)
          {   //error invoking method on server
            statusReportDataAvailFlag = false;   //indicate not available
            logObj.warning("Error invoking 'getStatusReportTime()' " +
                                                "method on server:  " + ex);
          }
          return true;
        }
      }
      catch(Exception ex)
      {       //some kind of exception error occurred
        synchronized(qwServicesSyncObj)
        {     //only allow one thread access to QWServices at a time
          qwServicesObj = null;   //clear 'QWServices' object
        }
        if(terminateFlag)         //if thread terminating then
          return false;           //abort method
              //convert CORBA transient, timeout, etc errors to "friendlier"
              // descriptions; otherwise show exception string:
        final String errStr;
        if(ex instanceof org.omg.CORBA.TRANSIENT)
          errStr = ":  [transient]";
        else if(ex instanceof org.omg.CORBA.TIMEOUT)
          errStr = ":  [timeout]";
        else if(ex instanceof org.omg.CORBA.OBJECT_NOT_EXIST)
          errStr = ":  [service not found]";
        else if(ex instanceof org.omg.CORBA.COMM_FAILURE &&
                                                  ex.getMessage() != null &&
               ex.getMessage().toLowerCase().indexOf("host not found") >= 0)
        {
          errStr = ":  [host not found]";
        }
        else
          errStr = "; connection error:  " + ex;
        setConnPanelInitError(("Unable to connect to server" +
                                       hostIDString + errStr),failLogLevel);
//        if(errStr.length() <= 0)       //make sure exception is logged
//          logObj.debug("  Connection error:  " + ex);
                                       //debug-log stack trace:
              //if not a CORBA exception then log stack trace:
        if(!(ex instanceof org.omg.CORBA.SystemException))
          logObj.debug(UtilFns.getStackTraceString(ex));
        return false;
      }
    }

    /**
     * Connects to the event channel.
     * @param failLogLevel log level for connect-fail messages.
     * @return true if successful, false if error.
     */
    private boolean connectEventChannel(int failLogLevel)
    {
         //set next state for connection status panel:
      setConnPanelData("Connecting to channel",
                       "Initializing; connecting to message-event-channel");
      try
      {
              //if not yet created then create event channel manager:
        if(evtChManagerObj == null)
          evtChManagerObj = new EvtChManager(orbManagerObj,logObj,true);
              //open connection to event channel:
        final boolean openFlag =
                          evtChManagerObj.openViaLocatorString(evtChLocStr);
        if(terminateFlag)         //if thread terminating then
          return false;           //abort method
        if(!openFlag)
        {     //event channel open failed; set error message
          setConnPanelInitError(evtChManagerObj.getErrorMessage(),
                                                              failLogLevel);
          return false;
        }
              //setup consumer filter:
        if(!evtChManagerObj.setupConsumerFilter(filterEventTypeArr))
        {     //error setting up filter; set error message
          setConnPanelInitError(evtChManagerObj.getErrorMessage(),
                                                              failLogLevel);
          return false;
        }
              //create and connect event channel consumer:
        if(!evtChManagerObj.connectConsumer(msgProcessorObj))
        {     //error connecting consumer; set error message
          setConnPanelInitError(evtChManagerObj.getErrorMessage(),
                                                              failLogLevel);
          return false;
        }
        connInvalidatedFlag = false;   //clear "invalidated" flag
        return true;
      }
      catch(Exception ex)
      {       //some kind of exception error occurred
        if(terminateFlag)         //if thread terminating then
          return false;           //abort method
        setConnPanelInitError(("Error connecting to event channel:  " + ex),
                                                              failLogLevel);
        return false;
      }
    }

    /**
     * Sets flag to indicate that this thread is "terminated".  Any
     * in-progress connection activities will be aborted (where possible).
     */
    public void terminate()
    {
      logObj.debug3(
          "QWCorbaConnector:  EventChannelConnectThread terminate requested");
      terminateFlag = true;
    }
  }

  /**
   * Class EventChannelDisconnectThread defines a thread used to disconnect
   * from the CORBA event channel.
   */
  protected class EventChannelDisconnectThread extends Thread
  {
    private boolean terminateFlag = false;

    /**
     * Runs the disconnect thread.
     */
    public void run()
    {
      logObj.debug3("QWCorbaConnector:  EventChannelDisconnectThread started");
      try
      {
        if(!getInitializedFlag())
        {     //ORB not initialized
          synchronized(evtChDisconnThreadSyncObj)
          {   //grab thread-sync object for 'evtChDisconnectThreadObj'
            if(evtChDisconnectThreadObj != null) //if not yet cleared then
              evtChDisconnectThreadObj = null;   //release thread object
            disconnectDoneFlag = true;           //indicate disconnect done
          }
          return;                                //abort thread
        }
              //show status in panel (color to red):
        setConnPanelData("Disconnecting","Closing message-event-channel",
                                         ConnStatusInterface.RED_COLOR_IDX);
        disconnectConsumerFromChannel();    //disconnect from event channel
        if(terminateFlag)
        {  //thread is terminating
          logObj.debug("QWCorbaConnector:  Channel disconnect " +
                                                          "thread aborted");
          disconnectDoneFlag = true;        //indicate disconnect done
          return;     //(thread object handle should already be null)

        }
              //show status in panel:
        setConnPanelData(null,"Disconnecting from server services",
                                         ConnStatusInterface.RED_COLOR_IDX);
        disconnectClientServices();         //disconnect from QWServices
        if(terminateFlag)
        {  //thread is terminating
          logObj.debug("QWCorbaConnector:  Channel disconnect " +
                                                          "thread aborted");
          disconnectDoneFlag = true;        //indicate disconnect done
          return;     //(thread object handle should already be null)

        }
              //set color to red after event channel closed:
        setConnPanelData(null,"Shutting down connection",
                                         ConnStatusInterface.RED_COLOR_IDX);
        closeEventChannel();                //close CORBA event channel
        if(terminateFlag)
        {  //thread is terminating
          logObj.debug("QWCorbaConnector:  Channel disconnect " +
                                                          "thread aborted");
          disconnectDoneFlag = true;        //indicate disconnect done
          return;     //(thread object handle should already be null)

        }
        synchronized(qwServicesSyncObj)
        {     //only allow one thread access to QWServices at a time
          qwServicesObj = null;     //clear handle to QWServices
        }
        logObj.debug2("QWCorbaConnector:  Closing CORBA ORB");
        if(!orbManagerObj.closeImplementation(false))      //do ORB shutdown
        {  //timeout waiting for close to complete
          logObj.debug2("QWCorbaConnector:  Timeout waiting for " +
                                    "close-ORB-implementation to complete");
        }
        int c = 0;
        while(true)
        {  //wait for CORBA manager to indicate closed, up to 5 seconds
          if(terminateFlag)
          {   //thread is terminating
            setConnPanelData(null,"Connection shutdown aborted");
            logObj.debug("QWCorbaConnector:  CORBA ORB shutdown aborted");
            disconnectDoneFlag = true;           //indicate disconnect done
            return;     //(thread object handle should already be null)
          }
          try { Thread.sleep(100); }          //delay 0.1 seconds
          catch(InterruptedException ex) {}
          if(!orbManagerObj.isImplementationRunning())
          {     //implementation shutdown OK
            setConnPanelData("Disconnected","Connection shutdown complete",
                                         ConnStatusInterface.RED_COLOR_IDX);
            logObj.debug("QWCorbaConnector:  CORBA implementation " +
                                                  "shut down successfully");
            break;
          }
          if(++c >= 50)
          {     //too much time elapsed
//            setConnPanelData(null,
//                             "Shutting down connection (forcing shutdown)");
            logObj.debug("QWCorbaConnector:  Timeout waiting for CORBA " +
                                             "implementation to shut down");
                        //forcibly destroy ORB and its resources:
//            logObj.debug2("Deactivating CORBA POA manager");
//            if(!orbManagerObj.deactivatePoaMgr())
//            {
//              logObj.debug2(
//                       "Timeout waiting for POAmgr.deactivate() to return");
//            }
//            logObj.debug2("Forcibly destroying CORBA POA");
//            if(!orbManagerObj.forcePoaDestroy())
//              logObj.debug2("Timeout waiting for POA.destroy() to return");
//            logObj.debug2("Forcibly destroying CORBA ORB");
//            if(!orbManagerObj.forceOrbDestroy())
//              logObj.debug2("Timeout waiting for ORB.destroy() to return");
            break;
          }
        }
      }
      catch(Exception ex)
      {       //some kind of exception error occurred
        final String errStr = "Error during connection shutdown:  " + ex;
        setConnPanelData(null,errStr);
        logObj.info(errStr);
      }
      synchronized(evtChDisconnThreadSyncObj)
      {  //grab thread-sync object for 'evtChDisconnectThreadObj'
        if(evtChDisconnectThreadObj != null)     //if not yet cleared then
          evtChDisconnectThreadObj = null;       //release thread object
        disconnectDoneFlag = true;               //indicate disconnect done
      }
    }

    /**
     * Sets flag to indicate that this thread is "terminated".  Any
     * in-progress connection activities will be aborted (where possible).
     */
    public void terminate()
    {
      logObj.debug3("QWCorbaConnector:  EventChannelDisconnectThread " +
                                                     "terminate requested");
      terminateFlag = true;
    }
  }
}
