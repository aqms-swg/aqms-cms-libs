//QWFilterProperties.java:  Manages the QuakeWatch client event-filter
//                          configuration.
//
//  3/19/2004 -- [ET]  Initial version.
//   4/5/2004 -- [ET]  Added 'add/removeDataChangedListener()' methods.
//  7/16/2004 -- [KF]  Changed max for 'dispMinMagnitudeProp'from 10 to 9.9.
//   9/8/2004 -- [KF]  Changed filter min/max lat/lon values.
//  9/27/2004 -- [ET]  Set default filter min/max lat/lon values to zero.
//  5/17/2005 -- [ET]  Changed to use modified 'QWEventMsgRecord' interface.
//  6/24/2005 -- [ET]  Added 'getMinValAmpsShownProp()' method and
//                     implementation.
//  4/24/2008 -- [ET]  Added items 'dispMaxMagError', 'dispMaxHorizError',
//                     'dispMaxVertError', 'dispMaxStandError',
//                     'dispMinNumPhaUsed', 'dispMaxAzimGapProp',
//                     'dispMinDepthProp' and 'dispMaxDepthProp';
//                     changed lowest-valid value on 'dispMinMagnitude'
//                     from 0.0 to -9.9.
//  5/18/2011 -- [ET]  Added optional "Drop events with invalid data values"
//                     item; added 'setKeepDataSourcesDefault()' method.
//  1/26/2017 -- [KF]  Modified to support two installers, one for
//                     Tsunami/Emergency centers and another for other users
// 10/31/2019 -- [KF]  Added expert mode to allow setting emergency mode.
//

package com.isti.quakewatch.util;

import java.util.Vector;
import java.util.Iterator;
import com.isti.util.UtilFns;
import com.isti.util.CfgPropItem;
import com.isti.util.CfgProperties;
import com.isti.util.CfgPropValidator;
import com.isti.util.DataChangedListener;
import com.isti.util.gis.IstiRegion;
import com.isti.quakewatch.message.QWEventMsgRecord;
import com.isti.quakewatch.common.MsgTag;
import com.isti.quakewatch.message.QWEQEventMsgRecord;
import com.isti.quakewatch.message.QWStationAmpMsgRecord;

/**
 * Class QWFilterProperties manages the QuakeWatch client event-filter
 * configuration.
 */
public class QWFilterProperties
{
         /** Default description-suffix string. */
  public final static String DEF_DESC_SUFFIX = " displayed";
         /** Default maximum age (in days) for events. */
  public final static double DEF_MAXAGE_DAYS = 7.0;
          /** Validator for latitude/longitude items. */
  public final static CfgPropValidator latLonValidator =
                 new CfgPropValidator(Double.valueOf(-180.0),Double.valueOf(180.0));

         //local copies of cfgProps, set to default values:
  protected double filterMinMagVal = 0.0;   //minimum magnitude for filtering
  protected long filterMaxAgeMs =           //maximum age (ms) for filtering
                               (long)(DEF_MAXAGE_DAYS * UtilFns.MS_PER_DAY);
//  protected double filterMinLatVal = 10.0;  //minimum latitude for filtering
//  protected double filterMaxLatVal = 70.0;  //maximum latitude for filtering
//  protected double filterMinLonVal = -180.0;  //min longitude for filtering
//  protected double filterMaxLonVal = -50.0; //maximum longitude for filtering
  protected double filterMinLatVal = 0.0;   //minimum latitude for filtering
  protected double filterMaxLatVal = 0.0;   //maximum latitude for filtering
  protected double filterMinLonVal = 0.0;   //min longitude for filtering
  protected double filterMaxLonVal = 0.0;   //maximum longitude for filtering
  protected boolean filterLatEnabledFlag =  //true for lat filtering
                                        (filterMinLatVal < filterMaxLatVal);
  protected boolean filterLonEnabledFlag =  //true for lon filtering
                                        (filterMinLonVal < filterMaxLonVal);
  protected Vector keepDataSourcesVec = null;    //Vector of codes to keep
  protected Vector dropDataSourcesVec = null;    //Vector of codes to drop
  protected double filterMinDepthVal = 0.0;      //minimum depth
  protected double filterMaxDepthVal = 0.0;      //maximum depth
  protected double filterMaxMagErrorVal = 0.0;   //maximum magnitude error
  protected double filterMaxHorizErrorVal = 0.0; //maximum horizontal error
  protected double filterMaxVertErrorVal = 0.0;  //maximum vertical error
  protected double filterMaxStandErrorVal = 0.0; //maximum standard error
  protected double filterMaxAzimGapVal = 0.0;    //maximum azimuthal gap
  protected int filterMinNumPhaUsedVal = 0;      //minimum # of phases used
  private boolean filterDropInternalEventsFlag = false;
  protected boolean filterDropInvalidEventsFlag = false;
  protected double filterMinAmpsShownVal = 1.0;  //min amplitude value shown
  protected boolean filterMinAmpsValSetFlag = true;  //true if min amp > 0
         //thread-synchronization object for local values:
  protected final Object localValuesSyncObj = new Object();
         //flag set true while inside 'keepDataSourcesProp' change method:
  protected boolean inKeepSrcsChgdMethodFlag = false;
         //flag set true while inside 'dropDataSourcesProp' change method:
  protected boolean inDropSrcsChgdMethodFlag = false;
         //tracker for time of last cfgProp item change:
  protected long lastPropertyChangeTime = 0;

         /** Configuration-properties object; may be passed in. */
  public final CfgProperties configPropsObj;

         /** Config-properties object containing only filter items. */
  public final CfgProperties localPropsObj;

         /** Minimum magnitude value for events. */
  public final CfgPropItem dispMinMagnitudeProp;

         /** Maximum age (in days) for events (0 = show all). */
  public final CfgPropItem dispMaxAgeDaysProp;

         /** Keep events from these data sources. */
  public final CfgPropItem keepDataSourcesProp;

         /** Drop events from these data sources. */
  public final CfgPropItem dropDataSourcesProp;

         /** Minimum latitude value allowed for an event. */
  public final CfgPropItem evtMinLatitudeProp;

         /** Maximum latitude value allowed for an event. */
  public final CfgPropItem evtMaxLatitudeProp;

         /** Minimum longitude value allowed for an event. */
  public final CfgPropItem evtMinLongitudeProp;

         /** Maximum longitude value allowed for an event. */
  public final CfgPropItem evtMaxLongitudeProp;

         /** Minimum depth for events. */
  public final CfgPropItem dispMinDepthProp;

         /** Maximum depth for events. */
  public final CfgPropItem dispMaxDepthProp;

         /** Maximum magnitude error for events. */
  public final CfgPropItem dispMaxMagErrorProp;

         /** Maximum horizontal error for events (km). */
  public final CfgPropItem dispMaxHorizErrorProp;

         /** Maximum vertical error for events (km). */
  public final CfgPropItem dispMaxVertErrorProp;

         /** Maximum standard (RMS time) error for events (seconds). */
  public final CfgPropItem dispMaxStandErrorProp;

         /** Maximum azimuthal gap for events. */
  public final CfgPropItem dispMaxAzimGapProp;

         /** Minimum number of phases used for events. */
  public final CfgPropItem dispMinNumPhaUsedProp;

         /** Drop events with internal scope. */
  private final CfgPropItem dropInternalEventsFlagProp;

         /** Drop events with invalid data values. */
  public final CfgPropItem dropInvalidEventsFlagProp;

         /** Minimum value for amplitudes shown.  (Not in filter group.) */
  public final CfgPropItem minValAmpsShownProp;


  /**
   * Constructs an event-filter-properties manager object.
   * @param propsObj the configuration-properties object to add item to,
   * or null to create a new configuration-properties object.
   * @param groupSelObj the configuration-group-selection object to use,
   * or null for none.
   * @param descSuffixStr description suffix string to be appended to
   * several of the items' descriptions, or null for none.
   * @param showDropInvEvtsItemFlag true to show the "Drop events with
   * invalid data values" item (by setting its group-select object to
   * the given 'groupSelObj') and set its default value to 'true'; false
   * to not show the item and set its default value to 'false'.
   */
  public QWFilterProperties(CfgProperties propsObj, Object groupSelObj,
                      String descSuffixStr, boolean showDropInvEvtsItemFlag)
  {
    if(descSuffixStr == null)                    //if suffix is null then
      descSuffixStr = UtilFns.EMPTY_STRING;      //use emtpy string
         //setup local configuration-properties object:
    localPropsObj = new CfgProperties();

         //Minimum magnitude value for events:
    dispMinMagnitudeProp = localPropsObj.add(
                        "dispMinMagnitude",Double.valueOf(filterMinMagVal),null,
                               ("Minimum event magnitude" + descSuffixStr)).
                         setValidator(-9.9,9.9).setGroupSelObj(groupSelObj).
                            addDataChangedListener(new DataChangedListener()
        {     //setup data-changed listener to keep local value in sync
          public void dataChanged(Object sourceObj)
          {
            synchronized(localValuesSyncObj)
            { //only allow one thread access to values at a time
              updateLastPropertyChangeTime();    //update change time
              filterMinMagVal = dispMinMagnitudeProp.doubleValue();
            }
          }
        });

         //Maximum age (in days) for events (0 = show all):
    dispMaxAgeDaysProp = localPropsObj.add(
                          "dispMaxAgeDays",Double.valueOf(DEF_MAXAGE_DAYS),null,
                         ("Maximum event age" + descSuffixStr + " (days)")).
                     setValidator(0.0,999999.0).setGroupSelObj(groupSelObj).
                            addDataChangedListener(new DataChangedListener()
        {     //setup data-changed listener to keep local value in sync
          public void dataChanged(Object sourceObj)
          {
            synchronized(localValuesSyncObj)
            { //only allow one thread access to values at a time
              updateLastPropertyChangeTime();    //update change time
              filterMaxAgeMs = (long)(dispMaxAgeDaysProp.doubleValue() *
                                                        UtilFns.MS_PER_DAY);
            }
          }
        });

         //Keep events from these data sources:
    keepDataSourcesProp = localPropsObj.add(
                                "keepDataSources",UtilFns.EMPTY_STRING,null,
                                     "Keep events from these data sources").
                                                setGroupSelObj(groupSelObj).
                            addDataChangedListener(new DataChangedListener()
        {     //setup data-changed listener to keep local value in sync
          public void dataChanged(Object sourceObj)
          {
            synchronized(localValuesSyncObj)
            { //only allow one thread access to values at a time
              if(!inKeepSrcsChgdMethodFlag)
              {    //not recursive call
                inKeepSrcsChgdMethodFlag = true;      //indicate in method
                updateLastPropertyChangeTime();       //update change time
                        //setup "keep" list of data source codes:
                String str = keepDataSourcesProp.stringValue();
                Object obj;
                if(keepDataSourcesProp.getEmptyStringDefaultFlag() &&
                                                        str.length() <= 0 &&
                      (obj=keepDataSourcesProp.getDefaultValue()) instanceof
                                       String && ((String)obj).length() > 0)
                {  //empty-string-default flag set, current value empty and
                   // default value not empty
                  str = (String)obj;        //use default value
                }
                if((str=setKeepDataSourcesStr(str)) != null)
                {  //at least 1 code in list; enter reconverted list
                  keepDataSourcesProp.setValue(str);
                }
                inKeepSrcsChgdMethodFlag = false;     //indicate done method
              }
            }
          }
        });

         //Drop events from these data sources:
    dropDataSourcesProp = localPropsObj.add(
                                "dropDataSources",UtilFns.EMPTY_STRING,null,
                                     "Drop events from these data sources").
                                                setGroupSelObj(groupSelObj).
                            addDataChangedListener(new DataChangedListener()
        {     //setup data-changed listener to keep local value in sync
          public void dataChanged(Object sourceObj)
          {
            synchronized(localValuesSyncObj)
            { //only allow one thread access to values at a time
              if(!inDropSrcsChgdMethodFlag)
              {    //not recursive call
                inDropSrcsChgdMethodFlag = true;      //indicate in method
                updateLastPropertyChangeTime();       //update change time
                        //setup "drop" list of data source codes:
                final String str;
                if((str=setDropDataSourcesStr(
                                dropDataSourcesProp.stringValue())) != null)
                {  //at least 1 code in list; enter reconverted list
                  dropDataSourcesProp.setValue(str);
                }
                inDropSrcsChgdMethodFlag = false;     //indicate done method
              }
            }
          }
        });

         //Minimum latitude value allowed for an event:
    evtMinLatitudeProp = localPropsObj.add(
                          "evtMinLatitude",Double.valueOf(filterMinLatVal),null,
                                ("Minimum event latitude" + descSuffixStr)).
                  setValidator(latLonValidator).setGroupSelObj(groupSelObj).
                            addDataChangedListener(new DataChangedListener()
        {     //setup data-changed listener to keep local value in sync
          public void dataChanged(Object sourceObj)
          {
            synchronized(localValuesSyncObj)
            { //only allow one thread access to values at a time
              updateLastPropertyChangeTime();    //update change time
              filterMinLatVal = evtMinLatitudeProp.doubleValue();
                             //if min < max then set filtering-enabled flag:
              filterLatEnabledFlag = (filterMinLatVal < filterMaxLatVal);
            }
          }
        });

         //Maximum latitude value allowed for an event:
    evtMaxLatitudeProp = localPropsObj.add(
                          "evtMaxLatitude",Double.valueOf(filterMaxLatVal),null,
                                ("Maximum event latitude" + descSuffixStr)).
                  setValidator(latLonValidator).setGroupSelObj(groupSelObj).
                            addDataChangedListener(new DataChangedListener()
        {     //setup data-changed listener to keep local value in sync
          public void dataChanged(Object sourceObj)
          {
            synchronized(localValuesSyncObj)
            { //only allow one thread access to values at a time
              updateLastPropertyChangeTime();    //update change time
              filterMaxLatVal = evtMaxLatitudeProp.doubleValue();
                             //if min < max then set filtering-enabled flag:
              filterLatEnabledFlag = (filterMinLatVal < filterMaxLatVal);
            }
          }
        });

         //Minimum longitude value allowed for an event:
    evtMinLongitudeProp = localPropsObj.add(
                         "evtMinLongitude",Double.valueOf(filterMinLonVal),null,
                               ("Minimum event longitude" + descSuffixStr)).
                  setValidator(latLonValidator).setGroupSelObj(groupSelObj).
                            addDataChangedListener(new DataChangedListener()
        {     //setup data-changed listener to keep local value in sync
          public void dataChanged(Object sourceObj)
          {
            synchronized(localValuesSyncObj)
            { //only allow one thread access to values at a time
              updateLastPropertyChangeTime();    //update change time
              filterMinLonVal = evtMinLongitudeProp.doubleValue();
                             //if min < max then set filtering-enabled flag:
              filterLonEnabledFlag = (filterMinLonVal < filterMaxLonVal);
            }
          }
        });

         //Maximum longitude value allowed for an event:
    evtMaxLongitudeProp = localPropsObj.add(
                         "evtMaxLongitude",Double.valueOf(filterMaxLonVal),null,
                               ("Maximum event longitude" + descSuffixStr)).
                  setValidator(latLonValidator).setGroupSelObj(groupSelObj).
                            addDataChangedListener(new DataChangedListener()
        {     //setup data-changed listener to keep local value in sync
          public void dataChanged(Object sourceObj)
          {
            synchronized(localValuesSyncObj)
            { //only allow one thread access to values at a time
              updateLastPropertyChangeTime();    //update change time
              filterMaxLonVal = evtMaxLongitudeProp.doubleValue();
                             //if min < max then set filtering-enabled flag:
              filterLonEnabledFlag = (filterMinLonVal < filterMaxLonVal);
            }
          }
        });

         //Minimum depth for events:
    dispMinDepthProp = localPropsObj.add(
                    "dispMinDepth",Double.valueOf(filterMinDepthVal),null,
                              ("Minimum event depth (km)" + descSuffixStr)).
                                    setValidator(IstiRegion.depthValidator).
                                                setGroupSelObj(groupSelObj).
                            addDataChangedListener(new DataChangedListener()
        {     //setup data-changed listener to keep local value in sync
          public void dataChanged(Object sourceObj)
          {
            synchronized(localValuesSyncObj)
            { //only allow one thread access to values at a time
              updateLastPropertyChangeTime();    //update change time
              filterMinDepthVal = dispMinDepthProp.doubleValue();
            }
          }
        });

         //Maximum depth for events:
    dispMaxDepthProp = localPropsObj.add(
                    "dispMaxDepth",Double.valueOf(filterMaxDepthVal),null,
                              ("Maximum event depth (km)" + descSuffixStr)).
                                    setValidator(IstiRegion.depthValidator).
                                                setGroupSelObj(groupSelObj).
                            addDataChangedListener(new DataChangedListener()
        {     //setup data-changed listener to keep local value in sync
          public void dataChanged(Object sourceObj)
          {
            synchronized(localValuesSyncObj)
            { //only allow one thread access to values at a time
              updateLastPropertyChangeTime();    //update change time
              filterMaxDepthVal = dispMaxDepthProp.doubleValue();
            }
          }
        });

         //Maximum magnitude error for events:
    dispMaxMagErrorProp = localPropsObj.add(
                    "dispMaxMagError",Double.valueOf(filterMaxMagErrorVal),null,
                         ("Maximum event magnitude error" + descSuffixStr)).
                       setValidator(0.0,9999.9).setGroupSelObj(groupSelObj).
                            addDataChangedListener(new DataChangedListener()
        {     //setup data-changed listener to keep local value in sync
          public void dataChanged(Object sourceObj)
          {
            synchronized(localValuesSyncObj)
            { //only allow one thread access to values at a time
              updateLastPropertyChangeTime();    //update change time
              filterMaxMagErrorVal = dispMaxMagErrorProp.doubleValue();
            }
          }
        });

         //Maximum horizontal error for events (km):
    dispMaxHorizErrorProp = localPropsObj.add(
                "dispMaxHorizError",Double.valueOf(filterMaxHorizErrorVal),null,
                        ("Maximum event horizontal error" + descSuffixStr)).
                       setValidator(0.0,9999.9).setGroupSelObj(groupSelObj).
                            addDataChangedListener(new DataChangedListener()
        {     //setup data-changed listener to keep local value in sync
          public void dataChanged(Object sourceObj)
          {
            synchronized(localValuesSyncObj)
            { //only allow one thread access to values at a time
              updateLastPropertyChangeTime();    //update change time
              filterMaxHorizErrorVal = dispMaxHorizErrorProp.doubleValue();
            }
          }
        });

         //Maximum vertical error for events (km):
    dispMaxVertErrorProp = localPropsObj.add(
                  "dispMaxVertError",Double.valueOf(filterMaxVertErrorVal),null,
                          ("Maximum event vertical error" + descSuffixStr)).
                       setValidator(0.0,9999.9).setGroupSelObj(groupSelObj).
                            addDataChangedListener(new DataChangedListener()
        {     //setup data-changed listener to keep local value in sync
          public void dataChanged(Object sourceObj)
          {
            synchronized(localValuesSyncObj)
            { //only allow one thread access to values at a time
              updateLastPropertyChangeTime();    //update change time
              filterMaxVertErrorVal = dispMaxVertErrorProp.doubleValue();
            }
          }
        });

         //Maximum standard (RMS time) error for events (seconds):
    dispMaxStandErrorProp = localPropsObj.add(
                "dispMaxStandError",Double.valueOf(filterMaxStandErrorVal),null,
                          ("Maximum event standard error" + descSuffixStr)).
                       setValidator(0.0,9999.9).setGroupSelObj(groupSelObj).
                            addDataChangedListener(new DataChangedListener()
        {     //setup data-changed listener to keep local value in sync
          public void dataChanged(Object sourceObj)
          {
            synchronized(localValuesSyncObj)
            { //only allow one thread access to values at a time
              updateLastPropertyChangeTime();    //update change time
              filterMaxStandErrorVal = dispMaxStandErrorProp.doubleValue();
            }
          }
        });

         //Maximum azimuthal gap for events (degrees):
    dispMaxAzimGapProp = localPropsObj.add(
                      "dispMaxAzimGap",Double.valueOf(filterMaxAzimGapVal),null,
                           ("Maximum event azimuthal gap" + descSuffixStr)).
                      setValidator(0.0,999.999).setGroupSelObj(groupSelObj).
                            addDataChangedListener(new DataChangedListener()
        {     //setup data-changed listener to keep local value in sync
          public void dataChanged(Object sourceObj)
          {
            synchronized(localValuesSyncObj)
            { //only allow one thread access to values at a time
              updateLastPropertyChangeTime();    //update change time
              filterMaxAzimGapVal = dispMaxAzimGapProp.doubleValue();
            }
          }
        });

         //Minimum number of phases used for events:
    dispMinNumPhaUsedProp = localPropsObj.add(
               "dispMinNumPhaUsed",Integer.valueOf(filterMinNumPhaUsedVal),null,
                        ("Minimum event number of phases" + descSuffixStr)).
                         setValidator(0,999999).setGroupSelObj(groupSelObj).
                            addDataChangedListener(new DataChangedListener()
        {     //setup data-changed listener to keep local value in sync
          public void dataChanged(Object sourceObj)
          {
            synchronized(localValuesSyncObj)
            { //only allow one thread access to values at a time
              updateLastPropertyChangeTime();    //update change time
              filterMinNumPhaUsedVal = dispMinNumPhaUsedProp.intValue();
            }
          }
        });

    //Drop events with internal scope:
    dropInternalEventsFlagProp = localPropsObj.add("dropInternalEventsFlag",
        Boolean.valueOf(filterDropInternalEventsFlag),null,
        "Drop events with internal scope").setGroupSelObj(groupSelObj).
        addDataChangedListener(new DataChangedListener()
    		{     //setup data-changed listener to keep local value in sync
          public void dataChanged(Object sourceObj)
          {
            synchronized(localValuesSyncObj)
            { //only allow one thread access to values at a time
              updateLastPropertyChangeTime();    //update change time
              filterDropInternalEventsFlag =
                  dropInternalEventsFlagProp.booleanValue();
              // do not save if true
              dropInternalEventsFlagProp.setNoSaveItemFlag(filterDropInternalEventsFlag);
            }
          }
    		});

         //Drop events with invalid data values:
    dropInvalidEventsFlagProp = localPropsObj.add(
               "dropInvalidEventsFlag",Boolean.valueOf(showDropInvEvtsItemFlag),
                               null,"Drop events with invalid data values").
                            addDataChangedListener(new DataChangedListener()
        {     //setup data-changed listener to keep local value in sync
          public void dataChanged(Object sourceObj)
          {
            synchronized(localValuesSyncObj)
            { //only allow one thread access to values at a time
              updateLastPropertyChangeTime();    //update change time
              filterDropInvalidEventsFlag =
                                   dropInvalidEventsFlagProp.booleanValue();
            }
          }
        });
    if(showDropInvEvtsItemFlag)   //if showing then set group-select object
      dropInvalidEventsFlagProp.setGroupSelObj(groupSelObj);
              //setup initial value for drop-invalid flag:
    filterDropInvalidEventsFlag = dropInvalidEventsFlagProp.booleanValue();

         //Minimum value for amplitudes shown (not in filter group
         // and not added to 'CfgProperties' object):
    minValAmpsShownProp = (new CfgPropItem("minValAmpsShown",
                                     Double.valueOf(filterMinAmpsShownVal),null,
                                     "Minimum value for amplitudes shown")).
                                              setValidator(0.0,999999999.0).
                            addDataChangedListener(new DataChangedListener()
        {     //setup data-changed listener to keep local value in sync
          public void dataChanged(Object sourceObj)
          {
            synchronized(localValuesSyncObj)
            { //only allow one thread access to values at a time
              updateLastPropertyChangeTime();    //update change time
                                  //enter new value:
              filterMinAmpsShownVal = minValAmpsShownProp.doubleValue();
                                  //set flag if positive value:
              filterMinAmpsValSetFlag = (filterMinAmpsShownVal > 0.0);
            }
          }
        });

    if(propsObj != null)
    {    //external config-props object was given
      propsObj.putAll(localPropsObj);       //add items to external props
      configPropsObj = propsObj;            //set handle to ext props obj
    }
    else      //external config-props object not given
      configPropsObj = localPropsObj;       //set handle to local props obj
  }

  /**
   * Constructs an event-filter-properties manager object.
   * @param propsObj the configuration-properties object to add item to,
   * or null to create a new configuration-properties object.
   * @param groupSelObj the configuration-group-selection object to use,
   * or null for none.
   * @param descSuffixStr description suffix string to be appended to
   * several of the items' descriptions, or null for none.
   */
  public QWFilterProperties(CfgProperties propsObj, Object groupSelObj,
                                                       String descSuffixStr)
  {
    this(propsObj,groupSelObj,descSuffixStr,false);
  }

  /**
   * Constructs an event-filter-properties manager object.  The default
   * description-suffix string (" displayed") is appended to several of
   * the items' descriptions.
   * @param propsObj the configuration-properties object to add item to,
   * or null to create a new configuration-properties object.
   * @param groupSelObj the configuration-group-selection object to use,
   * or null for none.
   * @param showDropInvEvtsItemFlag true to show the "Drop events with
   * invalid data values" item (by setting its group-select object to
   * the given 'groupSelObj') and set its default value to 'true'; false
   * to not show the item and set its default value to 'false'.
   */
  public QWFilterProperties(CfgProperties propsObj, Object groupSelObj,
                                            boolean showDropInvEvtsItemFlag)
  {
    this(propsObj,groupSelObj,DEF_DESC_SUFFIX,showDropInvEvtsItemFlag);
  }

  /**
   * Constructs an event-filter-properties manager object.  The default
   * description-suffix string (" displayed") is appended to several of
   * the items' descriptions.
   * @param propsObj the configuration-properties object to add item to,
   * or null to create a new configuration-properties object.
   * @param groupSelObj the configuration-group-selection object to use,
   * or null for none.
   */
  public QWFilterProperties(CfgProperties propsObj, Object groupSelObj)
  {
    this(propsObj,groupSelObj,DEF_DESC_SUFFIX,false);
  }

  /**
   * Constructs an event-filter-properties manager object, using a newly-
   * created configuration-properties object.  The default description-suffix
   * string (" displayed") is appended to several of the items' descriptions.
   */
  public QWFilterProperties()
  {
    this(null,null,DEF_DESC_SUFFIX,false);
  }

  /**
   * Adds the given data-changed listener to each of the filter items.
   * @param listenerObj the data-changed listener object to use.
   */
  public void addDataChangedListener(DataChangedListener listenerObj)
  {
    if(listenerObj != null)
    {    //given listener object not null; setup iterator for filter items
      final Iterator iterObj = localPropsObj.values().iterator();
      Object obj;
      while(iterObj.hasNext())
      {  //for each filter item; add listener
        if((obj=iterObj.next()) instanceof CfgPropItem)
          ((CfgPropItem)obj).addDataChangedListener(listenerObj);
      }
         //add listener to min-amps-show cfgProp (not in filter group):
      minValAmpsShownProp.addDataChangedListener(listenerObj);
    }
  }

  /**
   * Removes the given data-changed listener from each of the filter items.
   * @param listenerObj the data-changed listener object to use.
   */
  public void removeDataChangedListener(DataChangedListener listenerObj)
  {
    if(listenerObj != null)
    {    //given listener object not null; setup iterator for filter items
      final Iterator iterObj = localPropsObj.values().iterator();
      Object obj;
      while(iterObj.hasNext())
      {  //for each filter item; remove listener
        if((obj=iterObj.next()) instanceof CfgPropItem)
          ((CfgPropItem)obj).removeDataChangedListener(listenerObj);
      }
         //add listener to min-amps-show cfgProp (not in filter group):
      minValAmpsShownProp.removeDataChangedListener(listenerObj);
    }
  }

  /**
   * Returns the "Minimum value for amplitudes shown" configuration
   * property item object.  This item object is created and held by
   * this filter-properties object but is not added to the configuration
   * properties object or the filter group.  Client programs that use
   * this configuration property item will fetch it via this method,
   * add it to the configuration properties and setup its group-select
   * object.
   * @return The "Minimum value for amplitudes shown" configuration
   * property item object.
   */
  public CfgPropItem getMinValAmpsShownProp()
  {
    return minValAmpsShownProp;
  }

  /**
   * Determines if in emergency mode.
   * @return true if emergency mode, false otherwise.
   */
  public boolean isEmergency()
  {
    return !dropInternalEventsFlagProp.booleanValue();
  }

  /**
   * Set the emergency mode.
   * @param b true for emergency mode, false otherwise.
   */
  public void setEmergency(boolean b)
  {
    if (b)
    {
      dropInternalEventsFlagProp.setValue(false);
    }
    else
    {
      dropInternalEventsFlagProp.setDefaultAndValue(true);
    }
  }

  /**
   * Set the expert mode.
   * @param b true for expert mode, false otherwise.
   */
  public void setExpertMode(boolean b)
  {
    dropInternalEventsFlagProp.setIgnoreItemFlag(!b);
  }

  /**
   * Sets the default value for the 'keepDataSources' item, and sets up
   * the empty-string-default flag for the item.
   * @param str default value for item.
   */
  public void setKeepDataSourcesDefault(String str)
  {
    keepDataSourcesProp.setDefaultValue(str);
    keepDataSourcesProp.setEmptyStringDefaultFlag(true);
         //if current value empty then enter default value:
    if(keepDataSourcesProp.stringValue().length() <= 0)
      keepDataSourcesProp.setValue(str);
  }

  /**
   * Tests if the given event passes the filter parameters.
   * @param recObj the message record object for the given event.
   * @return true if the event passes; false if the event should be
   * filtered out.
   */
  public boolean passesFilter(QWEventMsgRecord recObj)
  {
//    logObj.debug5("passesFilter:  filterMaxAgeMs=" + filterMaxAgeMs +
//                                                          ", recObj.time=" +
//                com.isti.quakewatch.util.QWUtils.xmlDateFormatterObj.format(
//                     recObj.time) + ", filterMinMagVal=" + filterMinMagVal +
//     ", recObj.mag=" + recObj.magnitudeVal + ", latMin=" + filterMinLatVal +
//             ", latMax=" + filterMaxLatVal + ", lonMin=" + filterMinLonVal +
//             ", lonMax=" + filterMaxLonVal + ", recLat=" + recObj.latitude +
//                                            ", recLon=" + recObj.longitude);

    long currentTimeMs;
    int cnt = 0;
    while(true)
    {    //loop while waiting for cfgProp item modifications to be finished
         // (this is to deal which the case of when several configuration-
         //  property items are being modified via a single commit and a
         //  new event arrives in the middle of the modifications)
      synchronized(localValuesSyncObj)
      {  //only allow one thread access to values at a time
        if((currentTimeMs=System.currentTimeMillis()) -
                                lastPropertyChangeTime >= 100 || ++cnt > 10)
        {     //enough time has elapsed since last cfgProp change
              // or too many loops have happened
                   //if a minimum age filter exists then the event's
                   // age must be <= the filter age:
          if(filterMaxAgeMs > 0 && (recObj.getTime() == null ||
                               currentTimeMs - recObj.getTime().getTime() >
                                                         filterMaxAgeMs))
          {   //event record does not pass
            return false;         //return flag to indicate not-pass
          }
              //check magnitude or amplitude value:
          if(recObj instanceof QWEQEventMsgRecord)
          {  //message type is EQ event record
            Double dblValObj;
                   //if a minimum magnitude filter limit exists then event
                   // must have a magnitude value >= filter limit value:
            if(filterMinMagVal != 0.0 &&
                            ((dblValObj=recObj.getMagnitudeObj()) == null ||
                                 dblValObj.doubleValue() < filterMinMagVal))
            {      //event record does not pass
              return false;       //return flag to indicate not-pass
            }
                   //if a minimum depth filter limit exists then event
                   // must have a depth value >= filter limit value:
            Double depthValObj;
            if(filterMinDepthVal > 0.0)
            {  //minimum depth filter limit exists
              if((depthValObj=recObj.getDepthObj()) == null ||
                              depthValObj.doubleValue() < filterMinDepthVal)
              {    //event record does not pass
                return false;     //return flag to indicate not-pass
              }
            }
            else   //no minimum depth filter limit
              depthValObj = null;
                   //if a maximum depth filter limit exists and event has
                   // a depth value then check vs filter limit value:
            if(filterMaxDepthVal > 0.0)
            {  //maximum depth filter limit exists
                        //(if depth-value object not already fetched from
                        // message record then try to fetch it now)
              if((depthValObj != null ||
                              (depthValObj=recObj.getDepthObj()) != null) &&
                              depthValObj.doubleValue() > filterMaxDepthVal)
              {    //event record does not pass
                return false;     //return flag to indicate not-pass
              }
            }
            final QWEQEventMsgRecord eqRecObj = (QWEQEventMsgRecord)recObj;
                   //if a maximum magnitude-error filter limit exists and
                   // event has magnitude-error value then check vs limit:
            if(filterMaxMagErrorVal > 0.0 &&
                    (dblValObj=eqRecObj.getMagStandardErrorObj()) != null &&
                             dblValObj.doubleValue() > filterMaxMagErrorVal)
            {      //event record does not pass
              return false;       //return flag to indicate not-pass
            }
                   //if a maximum horizontal-error filter limit exists and
                   // event has horizontal-error value then check vs limit:
            if(filterMaxHorizErrorVal > 0.0 &&
                  (dblValObj=eqRecObj.getHorizontalErrorValObj()) != null &&
                           dblValObj.doubleValue() > filterMaxHorizErrorVal)
            {      //event record does not pass
              return false;       //return flag to indicate not-pass
            }
                   //if a maximum vertical-error filter limit exists and
                   // event has vertical-error value then check vs limit:
            if(filterMaxVertErrorVal > 0.0 &&
                   (dblValObj=eqRecObj.getVerticalErrorValObj()) != null &&
                            dblValObj.doubleValue() > filterMaxVertErrorVal)
            {      //event record does not pass
              return false;       //return flag to indicate not-pass
            }
                   //if a maximum standard-error filter limit exists and
                   // event has standard-error value then check vs limit:
            if(filterMaxStandErrorVal > 0.0 &&
                        (dblValObj=eqRecObj.getRMSTimeErrorObj()) != null &&
                           dblValObj.doubleValue() > filterMaxStandErrorVal)
            {      //event record does not pass
              return false;       //return flag to indicate not-pass
            }
                   //if a maximum azimuthal-gap filter limit exists and
                   // event has azimuthal-gap value then check vs limit:
            if(filterMaxAzimGapVal > 0.0 &&
                        (dblValObj=eqRecObj.getAzimuthalGapObj()) != null &&
                              dblValObj.doubleValue() > filterMaxAzimGapVal)
            {      //event record does not pass
              return false;       //return flag to indicate not-pass
            }
                   //if a min-#-of-phases-used filter exists then event
                   // must have a #-of-phases-used value >= limit value:
            final Integer intValObj;
            if(filterMinNumPhaUsedVal > 0 &&
                          ((intValObj=eqRecObj.getNumPhasesObj()) == null ||
                             intValObj.intValue() < filterMinNumPhaUsedVal))
            {      //event record does not pass
              return false;       //return flag to indicate not-pass
            }
                   //if dropping events with invalid data then check:
            if(filterDropInvalidEventsFlag && !eqRecObj.isEventDataValid())
              return false;       //return flag to indicate not-pass
			if (filterDropInternalEventsFlag
				&& MsgTag.INTERNAL.equalsIgnoreCase(eqRecObj.getEventScope()))
			{
              return false;       //return flag to indicate not-pass
            }
          }
          else if(recObj instanceof QWStationAmpMsgRecord)
          {  //message type is amplitude record
            if(filterMinAmpsValSetFlag &&
                             ((QWStationAmpMsgRecord)recObj).getAmpValue() <
                                                      filterMinAmpsShownVal)
            {      //min-amplitude limit exists and value is below it
              return false;       //return flag to indicate not-pass
            }
          }
          else
          {  //message type not determined
                   //if a minimum magnitude filter exists then the event
                   // must have a magnitude value >= the filter value:
            if(filterMinMagVal > 0.0 && (recObj.getMagnitudeObj() == null ||
                                   recObj.getMagnitude() < filterMinMagVal))
            {      //event record does not pass
              return false;       //return flag to indicate not-pass
            }
          }
              //if latitude filtering enabled, value must be within bounds:
          if(filterLatEnabledFlag &&
                                  (recObj.getLatitude() < filterMinLatVal ||
                                    recObj.getLatitude() > filterMaxLatVal))
          {
            return false;         //return flag to indicate not-pass
          }
              //if longitude filtering enabled, value must be within bounds:
          if(filterLonEnabledFlag &&
                                 (recObj.getLongitude() < filterMinLonVal ||
                                   recObj.getLongitude() > filterMaxLonVal))
          {
            return false;         //return flag to indicate not-pass
          }
              //check vs. "keep" data source code list:
          String dataSrcStr = null;
          if(keepDataSourcesVec != null)
          {   //Vector of "keep" codes exists
                   //get data-source string (use net-ID if amplitude msg):
            dataSrcStr = (recObj instanceof QWStationAmpMsgRecord) ?
                                 recObj.getNetID() : recObj.getDataSource();
              //return true if code exists & matched to "keep" Vector entry:
            return dataSrcStr != null && keepDataSourcesVec.contains(
                                                  dataSrcStr.toUpperCase());
          }
          if(dropDataSourcesVec != null)
          {   //Vector of "drop" codes exists
            if(dataSrcStr == null)
            { //data-source string not yet setup
                   //get data-source string (use net-ID if amplitude msg):
              dataSrcStr = (recObj instanceof QWStationAmpMsgRecord) ?
                                 recObj.getNetID() : recObj.getDataSource();
            }
              //return true if code not matched to entry in "drop" Vector:
            return (dataSrcStr == null || !dropDataSourcesVec.contains(
                                                 dataSrcStr.toUpperCase()));
          }
          return true;
        }
      }
      UtilFns.sleep(25);     //delay to allow any cfgProp changes to finish
    }
  }

  /**
   * Returns the 'CfgProperties' object holding the configuration-property
   * items.  If a 'CfgProperties' object parameter is passed in to the
   * constructor then this method will return it.
   * @return The 'CfgProperties' object holding the configuration-property
   * items.
   */
  public CfgProperties getConfigProps()
  {
    return configPropsObj;
  }

  /**
   * Returns the 'CfgProperties' object holding the local
   * configuration-property items (related to filter settings).
   * @return The 'CfgProperties' object holding the local
   * configuration-property items (related to filter settings).
   */
  public CfgProperties getLocalProps()
  {
    return localPropsObj;
  }

  /**
   * Returns a string containing a display of the names and values for
   * the filter property items.
   * @param equalsStr a String containing the characters to be placed
   * between each name and value; or if null then the characters " = "
   * will be used.
   * @param newlineStr a String containing the characters to be used to
   * separate items; or if null then the system default newline
   * character(s) will be used.
   * @return A string containing a display of the names and values for
   * the connection property items.
   */
  public String getItemsDisplayString(String equalsStr, String newlineStr)
  {
    return localPropsObj.getDisplayString(equalsStr,newlineStr);
  }

  /**
   * Returns a string containing a display of the names and values for
   * the filter property items.
   * @return A string containing a display of the names and values for
   * the connection property items.
   */
  public String getItemsDisplayString()
  {
    return localPropsObj.getDisplayString();
  }

  /**
   * Sets the list of data source codes for events that are to be kept.
   * @param codeListStr a list of data source codes, separated by spaces,
   * commas or semicolons; or null for no codes.
   * @return The list of data source codes with each item separated by ", "
   * and made all upper-case, or null if no codes.
   */
  protected String setKeepDataSourcesStr(String codeListStr)
  {
                             //trim any leading or trailing spaces:
    if(codeListStr == null || (codeListStr=codeListStr.trim()).length() <= 0
                                                 || codeListStr.equals("*"))
    {    //given string contains no data or contains match-all ("*")
      keepDataSourcesVec = null;       //setup for no items
      return null;                     //return null for no items
    }
    codeListStr = codeListStr.toUpperCase();     //convert to all upper-case
         //convert string list to Vector, default separators, not strict:
    keepDataSourcesVec = UtilFns.removeEmptyStrings(
                     UtilFns.quotedStringsToVector(codeListStr,null,false));
         //return reconverted string list:
    return UtilFns.enumToListString(keepDataSourcesVec.elements());
  }

  /**
   * Returns the list string of data source codes for events that are
   * to be kept.
   * @return The list string of data source codes, or null if no codes.
   */
//  public String getKeepDataSourcesStr()
//  {
//    return UtilFns.enumToString(keepDataSourcesVec.elements());
//  }

  /**
   * Sets the list of data source codes for events that are to be dropped.
   * @param codeListStr a list of data source codes, separated by spaces,
   * commas or semicolons; or null for no codes.
   * @return The list of data source codes with each item separated by ", "
   * and made all upper-case, or null if no codes.
   */
  protected String setDropDataSourcesStr(String codeListStr)
  {
                             //trim any leading or trailing spaces:
    if(codeListStr == null || (codeListStr=codeListStr.trim()).length() <= 0)
    {    //given string contains no data
      dropDataSourcesVec = null;       //setup for no items
      return null;                     //return null for no items
    }
    codeListStr = codeListStr.toUpperCase();     //convert to all upper-case
         //convert string list to Vector, default separators, not strict:
    dropDataSourcesVec = UtilFns.removeEmptyStrings(
                     UtilFns.quotedStringsToVector(codeListStr,null,false));
         //return reconverted string list:
    return UtilFns.enumToListString(dropDataSourcesVec.elements());
  }

  /**
   * Returns the list string of data source codes for events that are
   * to be dropped.
   * @return The list string of data source codes, or null if no codes.
   */
//  public String getDropDataSourcesStr()
//  {
//    return UtilFns.enumToString(dropDataSourcesVec.elements());
//  }

  /**
   * Updates the time of last change to configuration-property item.
   */
  protected void updateLastPropertyChangeTime()
  {
    lastPropertyChangeTime = System.currentTimeMillis();
  }
}
