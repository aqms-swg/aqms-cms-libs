//QWPasswordUtils.java:  A collection of static utility fields and
//                       methods for QuakeWatch passwords.
//
//   8/9/2004 -- [KF]  Initial version.
//  10/3/2004 -- [ET]  Modified 'encode()' to convert null parameters
//                     to empty strings.
//  10/5/2004 -- [ET]  Removed 'newUsernamePwd()' method.
// 10/12/2004 -- [ET]  Removed extraneous null-parameter check from
//                     'createTransEncPwd()' method; modified 'encrypt()'
//                     method to log caught exceptions.
//

package com.isti.quakewatch.util;

import java.security.MessageDigest;
import com.isti.util.UtilFns;
import com.isti.util.LogFile;
import com.isti.util.IstiEncryptionUtils;
import com.isti.util.JCrypt;

/**
 * Class QWPasswordUtils contains a collection of static utility fields and
 * methods for QuakeWatch passwords.
 */
public class QWPasswordUtils
{
  //private constructor so that no object instances may be created
  // (static access only)
  private QWPasswordUtils()
  {
  }

  /**
   * Creates an encoded password.
   * @param memEncKey the memory encryption key or null if none.
   * @param standardPwd the standard password.
   * @return the encoded password.
   */
  public static String createEncPwd(String memEncKey, String standardPwd)
  {
    return encode(memEncKey, standardPwd);
  }

  /**
   * Creates a memory encryption key.
   * @param localHostNameString the host name.
   * @param localHostIPString the IP Address.
   * @return the memory encryption key.
   */
  public static String createMemEncKey(
      String localHostNameString, String localHostIPString)
  {
    return encode(localHostNameString, localHostIPString);
  }

  /**
   * Creates a standard password.
   * @param usernameText the username text.
   * @param passwordText the password text to encrypt.
   * @return the standard password.
   */
  public static String createStandardPwd(String usernameText, String passwordText)
  {
    return JCrypt.crypt(usernameText, passwordText);
  }

  /**
   * Creates a transmit encoded password.
   * @param memEncKey the memory encryption key or null if none.
   * @param encPwd the encoded password.
   * @return the transmit encoded password.
   */
  public static String createTransEncPwd(String memEncKey, String encPwd)
  {
    return encode(memEncKey, encPwd);
  }

  /**
   * Encodes the specified key and text.
   * @param keyText the key text.
   * @param original the text to encrypt.
   * @return the key and text encoded.
   */
  public static String encode(String keyText, String original)
  {
    if(keyText == null)      //if null then change to empty string
      keyText = UtilFns.EMPTY_STRING;
    if(original == null)     //if null then change to empty string
      original = UtilFns.EMPTY_STRING;
    return encrypt(keyText + original);
  }

  /**
   * Encrypts the specified text.
   * @param original the text to encrypt.
   * @return the text encrypted.
   */
  public static String encrypt(String original)
  {
    try
    {
      final MessageDigest md = MessageDigest.getInstance("SHA");
      md.update(original.getBytes(IstiEncryptionUtils.DEFAULT_CHARACTER_ENCODING));
      return IstiEncryptionUtils.encode(md.digest());
    }
    catch(Exception ex)
    {    //exception error; log it
      LogFile.getGlobalLogObj(true).warning("QWPasswordUtils error:  " + ex);
      LogFile.getGlobalLogObj(true).warning(UtilFns.getStackTraceString(ex));
    }
    return UtilFns.EMPTY_STRING;
  }
}
