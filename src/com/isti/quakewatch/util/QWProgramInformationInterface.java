//QWProgramInformationInterface.java: Defines methods for program information.
//
//  1/19/2006 -- [KF]  Initial version.
//  1/26/2006 -- [KF]  Added 'getAlertRecipientsList()' method.
//   2/6/2006 -- [KF]  Removed the 'Class' parameter from the
//                     'getAlertRecipientsList()' method.
//   2/8/2006 -- [KF]  Added 'isFunctionalitySupported()' method.
//  3/14/2006 -- [KF]  Added 'getGlobalAlarmProperties()' method.
//

package com.isti.quakewatch.util;

import java.util.List;
import com.isti.util.ProgramInformationInterface;
import com.isti.quakewatch.alert.AlertSenderInterface;

/**
 * Interface QWProgramInformationInterface defines methods for program information.
 */
public interface QWProgramInformationInterface extends ProgramInformationInterface
{
  /**
   * Gets the alert recipients list.
   * The list may be a simple 'List' or an 'AlertRecipientsList' object.
   * @return the alert recipients list or null if not available.
   */
  public List getAlertRecipientsList();

  /**
   * Gets the message sender of the specified type.
   * @param alertSenderClassObj the alert sender class.
   * @return the message sender or null if not available.
   */
  public AlertSenderInterface getAlertSender(Class alertSenderClassObj);

  /**
   * Gets the global 'QWAlarmProperties' object.
   * @return the global 'QWAlarmProperties' object or null if none.
   */
  public QWAlarmProperties getGlobalAlarmProperties();


  /**
   * The global alarms supported value.
   * @see isSupported
   */
  public final static int GLOBAL_ALARMS_SUPPORTED_VALUE = 0x0001;

  /**
   * Determines if the specified functionality is supported.
   * @param functionalityValue the functionality value.
   * @see GLOBAL_ALARMS_SUPPORTED_VALUE
   * @return true if the specified functionality is supported, false otherwise.
   */
  public boolean isFunctionalitySupported(int functionalityValue);
}
