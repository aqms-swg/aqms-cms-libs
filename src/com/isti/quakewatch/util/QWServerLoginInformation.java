//QWServerLoginInformation.java:  Manages QWServer login information.
//
//    8/5/2004 -- [KF]  Initial version.
//  10/12/2004 -- [ET]  Modified methods to generate values "on demand".
//  10/25/2004 -- [ET]  Modified 'getTransEncPwd()' to regenerate its
//                      value if the plain-text password is available
//                      and the local host name or IP address changes.
//   11/1/2004 -- [ET]  Added constructors with 'isEncryptedFlag' and
//                      'standEncPwdFlag' parameters.
//   11/8/2004 -- [ET]  Modified so that if the local IP address is a
//                      "fake" address (like 192.168...) then it is not
//                      used when encrypting the password.
//   2/23/2005 -- [ET]  Fixed bug where incorrect transmit-encoded password
//                      would be returned from existing object (made
//                      'getTransEncPwd()' use 'getFixedLocalHostIPStr()'
//                      method).
//  11/22/2005 -- [ET]  Modified to support optional two-way encryption;
//                      added 'copyLoginInfoResources()' method.
//   8/18/2006 -- [ET]  Modified to clear cached password data when
//                      encryption method changes between one-way and
//                      two-way (to prevent wrong transmit-encoded
//                      password from being carried over).
//   8/28/2006 -- [ET]  Added 'getTwoWayEncryptionFlag()' method.
//  10/22/2008 -- [ET]  Modified 'createMemEncKey()' method to make it
//                      always update 'memEncKeyStr' value; modified
//                      'getFixedLocalHostIPStr()' method to make it
//                      support IPv6 addresses; modified to give
//                      preference to "valid" IPv4 addresses (via
//                      parameter to 'getLocalHostAddrObj()' method);
//                      added 'getLocalHostAddrInfoStr()' method.
//   6/23/2011 -- [ET]  Added null-parameter check to method
//                      'getFixedLocalHostIPStr()'; added try/catch
//                      to 'setupLocalHostStrings()' method.
//

package com.isti.quakewatch.util;

import java.lang.reflect.Constructor;
import java.net.InetAddress;
import com.isti.util.UtilFns;
import com.isti.util.propertyeditor.LoginInformation;

/**
 * Class QWServerLoginInformation manages QWServer login information.
 */
public class QWServerLoginInformation extends LoginInformation
{
  //the local host name
  private String localHostNameString = UtilFns.EMPTY_STRING;
  //the local host IP
  private String localHostIPString = UtilFns.EMPTY_STRING;
  //optional two-way encryption object
  private EncryptDecryptIntf twoWayEncryptDecryptObj = null;
  //flag set true for two-way encryption
  private boolean twoWayEncryptionFlag = false;
  //the memory encryption key
  private String memEncKeyStr = null;
  //the standard encoded password
  private String standEncPwdStr = null;
  //the transmit encoded password
  private String transEncPwdStr = null;
  //flag set true if the standard encoded password given via contructor:
  private final boolean standEncPwdGivenFlag;
  //buffer for local-host-address info via 'UtilFns.getLocalHostAddrObj()':
  private final StringBuffer localHostAddrInfoBuff = new StringBuffer();

  /**
   * Creates empty login information.
   */
  public QWServerLoginInformation()
  {
    setupLocalHostStrings();           //setup local-host-string variables
    standEncPwdGivenFlag = false;      //indicate standard-enc pwd not given
  }

  /**
   * Creates login information.
   * @param loginInfoText the text the represents the login information.
   */
  public QWServerLoginInformation(String loginInfoText)
  {
    super(loginInfoText);
    setupLocalHostStrings();           //setup local-host-string variables
    standEncPwdGivenFlag = false;      //indicate standard-enc pwd not given
  }

  /**
   * Creates login information.
   * @param usernameText the user name text.
   * @param passwordText the password text (not encrypted).
   */
  public QWServerLoginInformation(String usernameText, String passwordText)
  {
    super(usernameText,passwordText);
    setupLocalHostStrings();           //setup local-host-string variables
    standEncPwdGivenFlag = false;      //indicate standard-enc pwd not given
  }

  /**
   * Creates login information.
   * @param usernameText the user name text.
   * @param passwordText the password text.
   * @param isEncryptedFlag true if the password text is encrypted.
   */
  public QWServerLoginInformation(String usernameText, String passwordText,
                                                    boolean isEncryptedFlag)
  {
    super(usernameText,passwordText,isEncryptedFlag);
    setupLocalHostStrings();           //setup local-host-string variables
    standEncPwdGivenFlag = false;      //indicate standard-enc pwd not given
  }

  /**
   * Creates login information.
   * @param usernameText the user name text.
   * @param standEncPwdFlag true if the standard encoded password is given;
   * false if the plain text password is given.
   * @param passwordText the password text.
   */
  public QWServerLoginInformation(String usernameText,
                               boolean standEncPwdFlag, String passwordText)
  {
    super(usernameText,      //if std-enc pwd given then enter blank pwd:
            (standEncPwdFlag ? UtilFns.EMPTY_STRING : passwordText), false);
    setupLocalHostStrings();           //setup local-host-string variables
    if(standEncPwdFlag)
    {    //standard encoded password was given
      standEncPwdStr = passwordText;   //save standard encoded password
      standEncPwdGivenFlag = true;     //indicate standard-enc pwd was given
    }
    else
      standEncPwdGivenFlag = false;    //indicate standard-enc pwd not given
  }

  /**
   * Enables or disables two-way encryption.  If enabled and the
   * currently-held password is encrypted and can be decrypted
   * then it is converted to clear text.
   * @param flgVal true to enabled two-way encryption; false to disable.
   * @return true if successful, false if an error occurs.
   */
  public boolean setTwoWayEncryptionFlag(boolean flgVal)
  {
    if(flgVal)
    {    //two-way encryption enabled
      try
      {
        if(!twoWayEncryptionFlag)
        {     //two-way encryption not already enabled
          transEncPwdStr = null;  //clear pwd so new one will be generated
                             //setup encrypt/decrypt object:
          if(!createTwoWayEncryptDecryptObj(
                                       localHostNameString,localHostIPString))
          {
            return false;
          }
          twoWayEncryptionFlag = true;      //indicate enabled
          final String pwdStr;
          if(isEncrypted() &&
             twoWayEncryptDecryptObj.isEncryptStr(pwdStr=getPasswordText()))
          {   //password is encrypted and can be decrypted
            final String retStr;       //decrypt password text:
            if((retStr=twoWayEncryptDecryptObj.decrypt(pwdStr)) == null)
              return false;
                   //clear any previously-generated enc pwd and info text:
            clearEncryptedPassword();
            clearLoginInfoText();
                   //enter decrypted password; indicate not encrypted:
            setPasswordText(retStr,false);
          }
          else
          {   //password not encrypted or can't be decrypted
                   //clear any previously-generated enc pwd and info text:
            clearEncryptedPassword();
            clearLoginInfoText();
          }
        }
      }
      catch(Throwable ex)
      {       //some kind of exception error
        ex.printStackTrace();          //show stack trace
        return false;                  //return error flag
      }
    }
    else
    {    //two-way encryption disabled
      twoWayEncryptDecryptObj = null;       //make sure handle cleared
      if(twoWayEncryptionFlag)
      {  //two-way encryption was enabled
        twoWayEncryptionFlag = false;       //indicate disabled
        memEncKeyStr = null;      //clear key so new one will be generated
        transEncPwdStr = null;    //clear pwd so new one will be generated
        clearEncryptedPassword(); //clear pwd so new one will be generated
        clearLoginInfoText();     //clear text so new text will be generated
      }
    }
    return true;
  }

  /**
   * Returns an indicator of whether or not two-way encryption is enabled.
   * @return true if two-way encryption is enabled; false if disabled.
   */
  public boolean getTwoWayEncryptionFlag()
  {
    return twoWayEncryptionFlag;
  }

  /**
   * Sets up the 'localHostNameString' and 'localHostIPString' variables.
   */
  private void setupLocalHostStrings()
  {
    localHostAddrInfoBuff.setLength(0);     //clear previous address info
    try
    {
         //get local-host address (IPv4 preferred, save address info):
      final InetAddress addrObj =
                    UtilFns.getLocalHostAddrObj(true,localHostAddrInfoBuff);
      localHostNameString = UtilFns.getLocalHostName(addrObj);
      localHostIPString = getFixedLocalHostIPStr(addrObj);
    }
    catch(Throwable ex)
    {  //some kind of exception error; show message
      System.err.println("QWSErverLoginInformation error setting " +
                                           "up local host strings:  " + ex);
      ex.printStackTrace();
    }
  }

  /**
   * Fetches the local-host IP address string, converting the value to
   * "(local)" if it is a "fake" IP address or IPv6 address.  The
   * address is considered "fake" if it begins with one of the following:
   * "192.168.", "172.16.", "10.","255.", "127.0.0.0", "127.0.0.1",
   * "0.0.0.0", "fec", "fed", "fee" or "fef".
   * @param addrObj the 'InetAddress' object to use, or null for none.
   * @return The local-host IP address string.
   */
  private String getFixedLocalHostIPStr(InetAddress addrObj)
  {
    final String str;
    return (addrObj == null || addrObj.isSiteLocalAddress() ||
             UtilFns.isFakeIPAddress(str=UtilFns.getLocalHostIP(addrObj))) ?
                                                            "(local)" : str;
  }

  /**
   * Returns the local host IP string.  The value returned by this
   * method may be updated by the 'getTransEncPwd()' method.
   * @return the local host IP string.
   */
  public String getLocalHostIPString()
  {
    return localHostIPString;
  }

  /**
   * Returns the local host name string.  The value returned by this
   * method may be updated by the 'getTransEncPwd()' method.
   * @return the local host name string.
   */
  public String getLocalHostNameString()
  {
    return localHostNameString;
  }

  /**
   * Returns address information about the local host.
   * @return A new string containing address information about the local
   * host.
   */
  public String getLocalHostAddrInfoStr()
  {
    return localHostAddrInfoBuff.toString();
  }

  /**
   * Returns the transmit encoded password.  This method may update the
   * local host name and IP address values returned via the methods
   * 'getLocalHostNameString()' and 'getLocalHostIPString()'.
   * @return the transmit encoded password.
   */
  public String getTransEncPwd()
  {
    if(transEncPwdStr != null)
    {    //current value for transmit encoded password exists
         //if no plain-text password available then return current value:
      if(isEncrypted() || standEncPwdGivenFlag)
        return transEncPwdStr;
      final StringBuffer iBuff = new StringBuffer();  //buffer for host info
              //fetch current values for local host name and IP address
              // (IPv4 preferred, save address info):
      final InetAddress addrObj = UtilFns.getLocalHostAddrObj(true,iBuff);
      final String hstNameStr = UtilFns.getLocalHostName(addrObj);
      final String hstIPStr = getFixedLocalHostIPStr(addrObj);
      if((hstNameStr.equals(localHostNameString) &&
              hstIPStr.equals(localHostIPString)) || hstIPStr.length() <= 0)
      {  //local host name & IP address not changed or new IP not valid
        return transEncPwdStr;       //return current value
      }
              //save new host name and IP address & generate new pwd value:
      localHostNameString = hstNameStr;
      localHostIPString = hstIPStr;
      localHostAddrInfoBuff.setLength(0);   //clear previous address info
      localHostAddrInfoBuff.append(iBuff);  //enter new host address info
      memEncKeyStr = null;        //clear key so new one will be generated
      clearEncryptedPassword();   //clear pwd so new one will be generated
      clearLoginInfoText();       //clear text so new text will be generated
      if(twoWayEncryptionFlag)
      {  //using two-way encryption; create new encrypt/decrypt object
        createTwoWayEncryptDecryptObj(
                                     localHostNameString,localHostIPString);
      }
    }
              //generate transmit encoded password (if using two-way
              // encryption then return clear-text password):
    transEncPwdStr = twoWayEncryptionFlag ? getPasswordText() :
                                          QWPasswordUtils.createTransEncPwd(
                              createMemEncKey(),getEncryptedPasswordText());

    return transEncPwdStr;
  }

  /**
   * Creates a memory encryption key.
   * @return The memory encryption key text.
   */
  protected String createMemEncKey()
  {
    memEncKeyStr = QWPasswordUtils.createMemEncKey(
                                     localHostNameString,localHostIPString);
    return memEncKeyStr;
  }

  /**
   * Creates the encrypted password.
   * @return the encrypted password.
   */
  protected String createEncryptedPassword()
  {
    if(!twoWayEncryptionFlag)
    {    //not using two-way encryption
      if(standEncPwdStr == null)
      {  //standard password not yet generated; do it now
        standEncPwdStr = QWPasswordUtils.createStandardPwd(
                                       getUsernameText(),getPasswordText());
      }
         //create and return the encoded password:
      return QWPasswordUtils.createEncPwd(createMemEncKey(),standEncPwdStr);
    }
         //using two-way encryption
    final String retStr;
    return ((retStr=twoWayEncryptDecryptObj.encrypt(getPasswordText()))
                                   != null) ? retStr : UtilFns.EMPTY_STRING;
  }

  /**
   * Copies resources from the given login-information object into
   * this object.
   * @param srcInfoObj the 'LoginInformation' object from which resources
   * should be copied.
   */
  public void copyLoginInfoResources(LoginInformation srcInfoObj)
  {
    if(srcInfoObj instanceof QWServerLoginInformation)
    {    //given object is same type as this one; copy 2-way encryption flag
      setTwoWayEncryptionFlag(
               ((QWServerLoginInformation)srcInfoObj).twoWayEncryptionFlag);
    }
    super.copyLoginInfoResources(srcInfoObj);   //call parent method
  }

  /**
   * Creates an instance of the two-way encryption object.
   * @param key1Str key string to use.
   * @param key2Str key string to use.
   * @return true if successful; false if an error occurred.
   */
  private boolean createTwoWayEncryptDecryptObj(String key1Str,
                                                             String key2Str)
  {
    final Class encryptDecryptClassObj;
    try
    {       //load class object for 'QWEncryptDecrypt' class:
      encryptDecryptClassObj = Class.forName(
                     "com.isti.quakewatch.encryptdecrypt.QWEncryptDecrypt");
    }
    catch(ClassNotFoundException ex)
    {       //unable to load class; show error message
      System.err.println("QWServerLoginInformation:  " +
                      "Unable to load needed class for two-way encryption");
      return false;
    }
    catch(Throwable ex)
    {       //exception error loading class
      ex.printStackTrace();
      return false;
    }
    try
    {         //get two-string constructor for class:
      final Constructor constObj = encryptDecryptClassObj.getConstructor(
                               new Class [] { String.class, String.class });
              //construct new instance of class:
      twoWayEncryptDecryptObj = (EncryptDecryptIntf)(constObj.newInstance(
                                       new Object [] { key1Str, key2Str }));
    }
    catch(Throwable ex)
    {         //exception error instantiating object
      ex.printStackTrace();
      return false;
    }
    return true;
  }
}
