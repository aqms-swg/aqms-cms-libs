//QWStorageManager.java:  Manages the persistent storage of QuakeWatch
//                        XML text messages.
//
//   3/7/2003 -- [ET]  Initial version.
//  6/11/2003 -- [ET]  Modified so log-level tags are no longer put into
//                     the message-store file.
//  6/25/2003 -- [ET]  Modified to use 'IstiDialogPopup.waitForDialogShowing()'
//                     method.
//   7/1/2003 -- [ET]  Modified to use 'IstiDialogPopup.waitForDialogVisible()'
//                     method instead of 'waitForDialogShowing()' and to
//                     attempt to close the dialog even if it's not visible.
//   7/2/2003 -- [ET]  Fixed bug in 'run()' where listeners attached to the
//                     quake list model where not getting re-enabled.
//  7/18/2003 -- [ET]  Initial working implementation using 'ArchiveManager'.
//  7/25/2003 -- [ET]  Parameter 'maxLoadEventAgeDaysProp' changed
//                     to 'maxLoadEventAgeHoursProp'.
//  7/30/2003 -- [ET]  Changed 'maxLoadEventAgeHours' parameter to
//                     floating-point 'maxLoadEventAgeDays'.
//  9/19/2003 -- [ET]  Added message number sequence check to
//                     'storeRecord()'.
// 10/15/2003 -- [ET]  Renamed QWMsgManager 'setListModelListenersEnabled()'
//                     method to 'setQuakeListListenersEnabled()';
//                     renamed QWMsgManager 'fireListModelContentsChanged()'
//                     method to 'fireQuakeListContentsChanged()'.
// 10/16/2003 -- [ET]  Changed 'JProgressBar' to 'ManagedJProgressBar'.
// 10/29/2003 -- [ET]  Modified (via import) to reference 'QWCommon'
//                     version of  'QWMsgRecord' instead of local version;
//                     modified to work with 'QWMessageHandler'.
//   1/9/2004 -- [ET]  Modified 'storeRecord()' to quelch message-number-
//                     sequence warning if event filter is in use; added
//                     null-parameter check to constructor.
//  3/18/2004 -- [ET]  Moved to "com.isti.quakewatch.client.utils" package;
//                     added "implements QWStorageProcessor".
//  3/31/2004 -- [ET]  Modified to be a server-alive-message listener and
//                     to determine when the archive manager time-check
//                     offset needs to be increased (to fix issue where
//                     new event messages could be rejected when the server
//                     clock is ahead of the local clock).
//   6/3/2004 -- [KF]  Changed 'ManagedJProgressBar' to
//                     'ProgressIndicatorInterface'.
//  7/12/2004 -- [KF]  Make sure delay time value is not negative.
//   6/1/2005 -- [KF]  Moved from 'QWClient' to 'QWCommon' project.
//  2/29/2008 -- [ET]  Modified 'aliveMsgReceived()' to allow null
//                     parameter.
// 10/23/2008 -- [ET]  Added optional parameters 'archiveRootDirName' and
//                     'archiveBaseFName' to constructor.
//

package com.isti.quakewatch.util;

import java.util.Date;
import java.util.Calendar;
import com.isti.util.UtilFns;
import com.isti.util.LogFile;
import com.isti.util.CfgPropItem;
import com.isti.util.ArchiveManager;
import com.isti.util.FlatDailyArchiveManager;
import com.isti.util.Archivable;
import com.isti.util.ProgressIndicatorInterface;
import com.isti.util.IstiDialogInterface;
import com.isti.quakewatch.util.QWAliveMsgListener;
import com.isti.quakewatch.common.MsgTag;
import com.isti.quakewatch.message.QWMsgRecord;
import com.isti.quakewatch.message.QWStatusMsgRecord;
import com.isti.quakewatch.message.EventListManager;
import com.isti.quakewatch.message.QWMessageHandler;
import com.isti.quakewatch.message.QWStorageProcessor;


/**
 * Class QWStorageManager manages the persistent storage of QuakeWatch
 * XML text messages.
 */
public class QWStorageManager extends Thread
                           implements QWStorageProcessor, QWAliveMsgListener
{
    /** Value returned via 'popupObj.show()' if events loaded OK. */
  public static final Object eventFileLoadedValue = new Object();
    /** Default name of root directory for archive ("storage"). */
  public static final String ARCHIVE_ROOT_DIRNAME = "storage";
    /** Default "base" filename for archive files ("QWEvents.txt"). */
  public static final String ARCHIVE_BASE_FNAME = "QWEvents.txt";
  private final QWMessageHandler msgHandlerObj;
  private final EventListManager eventListManagerObj;
  private final IstiDialogInterface popupObj;
  private final ProgressIndicatorInterface progressBarObj;
  private final CfgPropItem maxLoadEventAgeDaysProp;
  private final CfgPropItem maxArchiveAgeDaysProp;
  private final LogFile logObj;
  private final static String MSG_START_STR = "<" + MsgTag.QW_MESSAGE;
  private boolean loadDoneFlag = false;
  private final FlatDailyArchiveManager archiveManagerObj;
  private boolean progressBarFlag = false;
  private long storeRecordMsgNumTracker = 0;
  private long aliveMsgRecTimeOffs = 0;
  private final Object aliveMsgTimeOffsSyncObj = new Object();


  /**
   * Creates a manager for the persistent storage of QuakeWatch XML text
   * messages.  This constructor spawns a separate thread that loads
   * "QWmessage" XML text messages from the storage file, sending them
   * to the message manager.  Each line of data in the file is expected
   * to contain a single, complete "QWmessage" XML text message, but other
   * characters may precede the beginning of the message text.
   * @param msgHandlerObj message handler object to use.
   * @param eventListManagerObj event-list manager object to use.
   * @param popupObj dialog popup object to be closed when the loading
   * of messages is complete, or null for none.
   * @param progressBarObj progress bar widget to be updated during initial
   * load of data from storage file, or null for none.
   * @param maxLoadEventAgeDaysProp a 'CfgPropItem' that specifies the
   * floating-point number-of-days' worth of events to be loaded.
   * @param maxArchiveAgeDaysProp a 'CfgPropItem' that specifies the
   * maximum age in days before event-archive files are deleted.
   * @param logObj LogFile object to use.
   * @param archiveRootDirName name of root directory for archive.
   * @param archiveBaseFName "base" filename for archive files.
   * @throws NoSuchMethodException if an error occurs while setting up
   * the archive manager object (shouldn't happen).
   */
  public QWStorageManager(QWMessageHandler msgHandlerObj,
         EventListManager eventListManagerObj, IstiDialogInterface popupObj,
                                  ProgressIndicatorInterface progressBarObj,
                                        CfgPropItem maxLoadEventAgeDaysProp,
                          CfgPropItem maxArchiveAgeDaysProp, LogFile logObj,
                         String archiveRootDirName, String archiveBaseFName)
                                                throws NoSuchMethodException
  {
    super("storageManager");
    if(msgHandlerObj == null || eventListManagerObj == null ||
         maxLoadEventAgeDaysProp == null || maxArchiveAgeDaysProp == null ||
                                                             logObj == null)
    {    //null parameter
      throw new NullPointerException("Null parameter(s)");
    }
    this.msgHandlerObj = msgHandlerObj;
    this.eventListManagerObj = eventListManagerObj;
    this.popupObj = popupObj;
    this.progressBarObj = progressBarObj;
    this.maxLoadEventAgeDaysProp = maxLoadEventAgeDaysProp;
    this.maxArchiveAgeDaysProp = maxArchiveAgeDaysProp;

    this.logObj = logObj;
              //create archive manager:
    archiveManagerObj = new FlatDailyArchiveManager(QWMsgRecord.class,
                                       archiveRootDirName,archiveBaseFName);
              //configure to look for leading "<QWmessage" prefix:
    archiveManagerObj.setArchivedFormPrefixStr(MSG_START_STR);
              //setup to have this object receive server-alive messages:
    msgHandlerObj.addAliveMsgListener(this);

    start();                      //start new thread to load messages
  }

  /**
   * Creates a manager for the persistent storage of QuakeWatch XML text
   * messages.  This constructor spawns a separate thread that loads
   * "QWmessage" XML text messages from the storage file, sending them
   * to the message manager.  Each line of data in the file is expected
   * to contain a single, complete "QWmessage" XML text message, but other
   * characters may precede the beginning of the message text.  The 'base'
   * filename "QWEvents.txt" is used.
   * @param msgHandlerObj message handler object to use.
   * @param eventListManagerObj event-list manager object to use.
   * @param popupObj dialog popup object to be closed when the loading
   * of messages is complete, or null for none.
   * @param progressBarObj progress bar widget to be updated during initial
   * load of data from storage file, or null for none.
   * @param maxLoadEventAgeDaysProp a 'CfgPropItem' that specifies the
   * floating-point number-of-days' worth of events to be loaded.
   * @param maxArchiveAgeDaysProp a 'CfgPropItem' that specifies the
   * maximum age in days before event-archive files are deleted.
   * @param logObj LogFile object to use.
   * @param archiveRootDirName name of root directory for archive.
   * @throws NoSuchMethodException if an error occurs while setting up
   * the archive manager object (shouldn't happen).
   */
  public QWStorageManager(QWMessageHandler msgHandlerObj,
         EventListManager eventListManagerObj, IstiDialogInterface popupObj,
                                  ProgressIndicatorInterface progressBarObj,
                                        CfgPropItem maxLoadEventAgeDaysProp,
                          CfgPropItem maxArchiveAgeDaysProp, LogFile logObj,
                                                  String archiveRootDirName)
                                                throws NoSuchMethodException
  {
    this(msgHandlerObj,eventListManagerObj,popupObj,progressBarObj,
                       maxLoadEventAgeDaysProp,maxArchiveAgeDaysProp,logObj,
                                     archiveRootDirName,ARCHIVE_BASE_FNAME);
  }

  /**
   * Creates a manager for the persistent storage of QuakeWatch XML text
   * messages.  This constructor spawns a separate thread that loads
   * "QWmessage" XML text messages from the storage file, sending them
   * to the message manager.  Each line of data in the file is expected
   * to contain a single, complete "QWmessage" XML text message, but other
   * characters may precede the beginning of the message text.  The root
   * directory name "storage" and 'base' filename "QWEvents.txt" are used.
   * @param msgHandlerObj message handler object to use.
   * @param eventListManagerObj event-list manager object to use.
   * @param popupObj dialog popup object to be closed when the loading
   * of messages is complete, or null for none.
   * @param progressBarObj progress bar widget to be updated during initial
   * load of data from storage file, or null for none.
   * @param maxLoadEventAgeDaysProp a 'CfgPropItem' that specifies the
   * floating-point number-of-days' worth of events to be loaded.
   * @param maxArchiveAgeDaysProp a 'CfgPropItem' that specifies the
   * maximum age in days before event-archive files are deleted.
   * @param logObj LogFile object to use.
   * @throws NoSuchMethodException if an error occurs while setting up
   * the archive manager object (shouldn't happen).
   */
  public QWStorageManager(QWMessageHandler msgHandlerObj,
         EventListManager eventListManagerObj, IstiDialogInterface popupObj,
                                  ProgressIndicatorInterface progressBarObj,
                                        CfgPropItem maxLoadEventAgeDaysProp,
                          CfgPropItem maxArchiveAgeDaysProp, LogFile logObj)
                                                throws NoSuchMethodException
  {
    this(msgHandlerObj,eventListManagerObj,popupObj,progressBarObj,
                       maxLoadEventAgeDaysProp,maxArchiveAgeDaysProp,logObj,
                                   ARCHIVE_ROOT_DIRNAME,ARCHIVE_BASE_FNAME);
  }

  /**
   * Closes the persistent storage archive.
   */
  public void close()
  {
    interrupt();        //send signal to terminate purge-archive thread
    archiveManagerObj.closeArchive();       //close archive manager
    logObj.debug2("Closed storage/archive manager");
  }

  /**
   * Stores the given "QWmessage" element object into the persistent
   * storage archive.
   * @param recObj the record object holding the "QWmessage" element.
   */
  public void storeRecord(QWMsgRecord recObj)
  {
    if(!msgHandlerObj.getDomainTypeFilteringFlag() && recObj.msgNumber > 0)
    {    //event filtering not in use and record contains a valid message #
      if(storeRecordMsgNumTracker > 0)
      {  //this is not the first message number checked
        if(recObj.msgNumber != storeRecordMsgNumTracker + 1)
        {     //message number is out of sequence; log debug message
          logObj.debug("Stored record message number out of sequence, " +
                    "prevMsgNum=" + storeRecordMsgNumTracker + ", msgNum=" +
                    recObj.msgNumber + ", timeGen=" + recObj.timeGenerated);
        }
      }
                   //store message number for next iteration:
      storeRecordMsgNumTracker = recObj.msgNumber;
    }
    try
    {
      archiveManagerObj.archiveItem(recObj);
    }
    catch(Exception ex)
    {
      logObj.warning("Error storing message-record to archive:  " + ex);
    }
  }

  /**
   * Called each time an "Alive" message is received from the QWServer.
   * If the time-generated time for the message is ahead of local time
   * then the archive manager time-check offset value may be adjusted to
   * make sure that new incoming event messages will not be rejected.
   * This method implements the 'QWAliveMsgListener' interface.
   * @param recObj the 'QWStatusMsgRecord' object for the received message,
   * or null if none available.
   */
  public void aliveMsgReceived(QWStatusMsgRecord recObj)
  {
    if(recObj != null)
    {  //given record object not null
      synchronized(aliveMsgTimeOffsSyncObj)
      {  //grab thread-sync lock and set received-time-offset value
        aliveMsgRecTimeOffs = recObj.getReceivedTimeOffsetMs();
      }
      if(aliveMsgRecTimeOffs < 0 &&
            -aliveMsgRecTimeOffs > archiveManagerObj.getTimeCheckOffsetMs())
      {  //alive-msg timeGen time is ahead of local time and difference is
         // larger than current archive manager time-check offset value
              //calculate offset value that is twice as large:
        final int offsVal = (-aliveMsgRecTimeOffs < Integer.MAX_VALUE/2) ?
                          ((int)-aliveMsgRecTimeOffs)*2 : Integer.MAX_VALUE;
              //configure archive manager to use time-check offset value:
        archiveManagerObj.setTimeCheckOffsetMs(offsVal);
        logObj.debug("QWStorageManager:  Alive message time offset = " +
           aliveMsgRecTimeOffs + ", archive mgr time-check offset set to " +
                                                                   offsVal);
      }
      else
      {
        logObj.debug5("QWStorageManager:  Alive message time offset = " +
                                                       aliveMsgRecTimeOffs);
      }
    }
  }

  /**
   * Returns the received-time-offset value for the last-received
   * server-alive message.  This value indicates the difference between
   * the local clock and the server clock, with a negative value
   * indicating that the server clock is ahead of the local clock.
   * @return The received-time-offset value, in milliseconds.
   */
  public long getLastAliveMsgRecTimeOffs()
  {
    synchronized(aliveMsgTimeOffsSyncObj)
    {    //grab thread-sync lock for value
       return aliveMsgRecTimeOffs;
    }
  }

  /**
   * Returns the status of loading XML messages from the storage file.
   * @return true if the load is complete, false is not.
   */
  public boolean getLoadDoneFlag()
  {
    return loadDoneFlag;
  }

  /**
   * Closes the dialog window (if given) and sets 'loadDoneFlag' to
   * true.
   * @param closeObj the object to be returned via 'popupObj.close()',
   * or null for none.
   */
  private void closeDialog(Object closeObj)
  {
    if(popupObj != null)
    {    //popup dialog object was given
      popupObj.waitForDialogVisible(1000);  //wait for dialog visible
      if(closeObj != null)             //if object given then
        popupObj.close(closeObj);      //close with object
      else                             //if object not given then
        popupObj.close();              //just close
    }
    loadDoneFlag = true;               //indicate loading complete
  }

  /**
   * Separate thread method that loads event objects from the archive,
   * sending them to the message manager.
   */
  public void run()
  {
    long lastArchiveItemTime = 0;
    if(progressBarFlag=(progressBarObj != null))
    {  //progress bar object was given (flag set)
      try
      {       //setup progress bar to use values from 0 to 99:
        progressBarObj.setMinimum(0);
        progressBarObj.setMaximum(99);

              //get newest item and use it's date with the progress bar:
//        final long sTimeVal = System.currentTimeMillis();
        final Archivable itemObj =
                           archiveManagerObj.getNewestItemInEntireArchive();
//        logObj.debug5("getNewestItemInEntireArchive() time = " +
//                              (System.currentTimeMillis()-sTimeVal) + "ms");
        final Date dateObj;
        if(itemObj != null && (dateObj=itemObj.getArchiveDate()) != null)
        {     //item and date fetched OK
          lastArchiveItemTime = dateObj.getTime();   //save date/time in ms
          logObj.debug3("QWStorageManager:  Last msg-generated date/time " +
                                                 "in archive:  " + dateObj);
        }
        else  //unable to get date of last item; use current date instead
          lastArchiveItemTime = System.currentTimeMillis();
      }
      catch(Exception ex)
      {     //some kind of error occurred
        progressBarFlag = false;       //disable progress bar updates
        logObj.debug(
                "QWStorageManager:  Error setting up progress bar:  " + ex);
      }
    }

         //disable listeners attached to the quake list model:
    eventListManagerObj.setQuakeListListenersEnabled(false);
         //process items in archive:
    Object closeDialogObj = null;
    try
    {       //setup start date for events items to be fetched from archive:
      final Date startDateObj =  new Date(System.currentTimeMillis() -
          (long)(maxLoadEventAgeDaysProp.doubleValue()*UtilFns.MS_PER_DAY));
            //fetch and process event items from archive:
      archiveManagerObj.processArchivedItems(startDateObj,null,
                          new QWMsgRecProcessCallBack(lastArchiveItemTime));
      int n = eventListManagerObj.getEventsListSize();
      if (n == 0) // if no events loaded
      {
        logObj.info("QWStorageManager:  No events loaded, purging archive");
        archiveManagerObj.purgeArchive(null, null);
      }
            //setup to close dialog with "success" value:
      closeDialogObj = eventFileLoadedValue;
    }
    catch(Exception ex)
    {       //error reading messages; build error message
      final String msgStr = "Error loading events from archive:  " + ex;
      logObj.warning(msgStr);        //log error message
      if(popupObj != null)                     //if dialog obj OK then
        popupObj.setUserMessageString(msgStr); //enter error message
    }
                 //reenable listeners attached to the quake list model:
    eventListManagerObj.setQuakeListListenersEnabled(true);
                 //indicate that list model contents have changed:
    eventListManagerObj.fireQuakeListContentsChanged();
    closeDialog(closeDialogObj);       //close dialog window (if given)

//    logObj.debug2("QWStorageManager:  Performing test purge");
//    archiveManagerObj.setPurgeIntoArchiveFlag(true);
//    if(!archiveManagerObj.purgeArchive(new Date(System.currentTimeMillis()-
//                    maxArchiveAgeDaysProp.intValue()*UtilFns.MS_PER_DAY)))
//    {  //error flag returned; log message
//      logObj.warning("Error detected while purging event archive");
//    }
//    archiveManagerObj.setPurgeIntoArchiveFlag(false);
//    logObj.debug2("QWStorageManager:  Test purge complete");

         //use thread to purge archive at 00:00:01 GMT each day:
                                            //create GMT Calendar object:
    final Calendar calObj = Calendar.getInstance(UtilFns.GMT_TIME_ZONE_OBJ);
    calObj.set(Calendar.HOUR_OF_DAY,0);     //set time to 00:00:01.000
    calObj.set(Calendar.MINUTE,0);
    calObj.set(Calendar.SECOND,1);
    calObj.set(Calendar.MILLISECOND,0);
    Date calDateObj;
    long delayTimeVal;
    while(!archiveManagerObj.isArchiveClosed())
    {    //loop until storage/archive manager closed
      calObj.add(Calendar.DATE,1);     //add 1 day to target time
      calDateObj = calObj.getTime();   //get Date object for Calendar
              //calculate delay to reach target time, in milliseconds:
      delayTimeVal = calDateObj.getTime() - System.currentTimeMillis();
      if (delayTimeVal <= 0)  //if time alread reached
      {
        delayTimeVal = 0;
        logObj.debug2(
            "QWStorageManager:  Purge-archive thread already reached target " +
            "time: " + calDateObj);
      }
      else
      {
        logObj.debug2("QWStorageManager:  Purge-archive thread delaying" +
                      " for " + delayTimeVal + "ms, until " + calDateObj);
      }
      try
      {
        sleep(delayTimeVal);      //delay until target time
      }
      catch(InterruptedException ex)
      {       //thread-terminate signal received via 'closeArchive()' method
        break;     //exit loop (and thread)
      }
      if(archiveManagerObj.isArchiveClosed())
        break;     //if archive closed then exit loop (and thread)
              //calculate cutoff date/time and do purge of archive:
      if(!archiveManagerObj.purgeArchive(new Date(System.currentTimeMillis()-
                      maxArchiveAgeDaysProp.intValue()*UtilFns.MS_PER_DAY)))
      {  //error flag returned; log message
        logObj.warning("Error detected while purging event archive");
      }
    }
    logObj.debug("QWStorageManager:  Purge-archive thread terminated");
  }


  /**
   * Class QWMsgRecProcessCallBack implements the call-back to process
   * 'QWMsgRecord' items from the archive.
   */
  protected class QWMsgRecProcessCallBack
                                   implements ArchiveManager.ProcessCallBack
  {
    private final long lastArchiveItemTime;
    private int checkInterval = 1;
    private long diffVal, msgCount = 0;
    private long firstArchiveItemTime = 0, totalArchiveTime = 0;
    private int val, prevPBarVal = -1;

    /**
     * Creates a call-back to process 'QWMsgRecord' items from the
     * archive.
     * @param lastArchiveItemTime the date/time for the last item in
     * the archive (in ms since 1/1/1970).
     */
    public QWMsgRecProcessCallBack(long lastArchiveItemTime)
    {
      this.lastArchiveItemTime = lastArchiveItemTime;
    }

    /**
     * Processes the QWMsgRecord item from the archive.
     * @param itemObj the QWMsgRecord item to be processed.
     * @param archivedFormStr the archived-form data string for the item.
     * @return true if processing is to continue, false if processing
     * should be terminated.
     */
    public boolean procArchivedItem(Archivable itemObj,
                                                    String archivedFormStr)
    {
      if(itemObj instanceof QWMsgRecord)
      {  //item is 'QWMsgRecord'; process its XML element:
        msgHandlerObj.processMessage(((QWMsgRecord)itemObj).qwMsgElement,
                                                archivedFormStr,true,false);
               //check if progress bar should be updated:
        if(progressBarFlag && (++msgCount % checkInterval) == 0)
        {      //flag set and msg count interval reached
          try
          {                            //get item date/time value:
            final long timeVal = itemObj.getArchiveDate().getTime();
            if(firstArchiveItemTime <= 0)
            {      //time of first archive item not yet setup
              firstArchiveItemTime = timeVal;    //save date/time value
                                       //set total time for archive:
              totalArchiveTime = lastArchiveItemTime - firstArchiveItemTime;
              if((checkInterval=       //set check-interval for progress bar
                            (int)(totalArchiveTime/UtilFns.MS_PER_DAY)) < 1)
              {
                checkInterval = 1;     //if too small then use 1
              }
              else if(checkInterval > 100)
                checkInterval = 100;   //if too large then use 100
              logObj.debug3("QWStorageManager:  checkInterval = " +
                                                             checkInterval);
            }
                  //convert time-value ratio to 0-99 value:
            if(totalArchiveTime > 0 && (diffVal=(timeVal-
                      firstArchiveItemTime)*100/totalArchiveTime) > 0 &&
                                                              diffVal < 100)
            {      //ratio converted OK
              val = (int)diffVal;      //set interger version of ratio val
              if(val != prevPBarVal)
              {    //progress bar value is changing
                progressBarObj.setValue((int)val);    //update progress bar
                logObj.debug3("QWStorageManager:  Progress bar from " +
                             prevPBarVal + " to " + val + ", date/time = " +
                                       UtilFns.timeMillisToString(timeVal));
                prevPBarVal = val;     //save value as "previous"
              }
            }
          }
          catch(Exception ex)
          {       //some kind of error occurred
            progressBarFlag = false;        //disable progress bar updates
            logObj.debug(
                  "QWStorageManager:  Error managing progress bar:  " + ex);
          }
        }
      }
      return true;
    }
  }
}
