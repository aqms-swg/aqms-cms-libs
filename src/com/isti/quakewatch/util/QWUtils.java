//QWUtils.java:  A collection of static utility fields and methods for
//               QuakeWatch.
//
// 12/23/2002 -- [ET]  Initial version.
//  6/19/2003 -- [ET]  Added 'parseStringLong()' method.
//  11/5/2003 -- [ET]  Added 'parseStringXmlDate()' method.
//   1/5/2004 -- [ET]  Added 'listStringToEventTypeArray()' methods.
//  2/16/2004 -- [ET]  Replaced public 'xmlDateFormatterObj' variable
//                     with 'getXmlDateFormatterObj()' method; made
//                     'parseStringXmlDate()' method thread safe.
//  5/10/2004 -- [ET]  Added 'getDegMinStr()' method; added 'KM_TO_MILE'
//                     constant.
//  5/11/2004 -- [ET]  Added 'MILE_TO_KM' constant.
//  5/24/2004 -- [SBH] Added 'typeArraytoListSTring()' method
//   6/4/2004 -- [ET]  Added methods 'getMagnitudeDescStr()' and
//                     'magTypeCharToStr()'.
//  6/11/2004 -- [ET]  Modified km/miles constants to reference 'GisUtils'.
//   9/1/2004 -- [KF]  Added 'getTextForSignature()' method.
//  1/14/2005 -- [ET]  Changed 'FLOAT_ZERO_VAL' from "1.0e-25" to "0.00001".
// 12/20/2005 -- [KF]  Added support for setting the default locale.
// 12/23/2005 -- [KF]  Moved 'createFloatNumberFormat' methods to "UtilFns",
//                     Changed XML format parsers to use the US locale,
//                     Added change listener for default locale.
//  3/12/2010 -- [ET]  Modified "floating-point number formatter object"
//                     to not use commas in output.
//  4/20/2010 -- [ET]  Removed "floating-point number formatter object"
//                     (because not thread synchronized); added method
//                     'determineMessageFormat()'.
//  5/12/2010 -- [ET]  Modified 'determineMessageFormat()' method to make
//                     it recognize QuakeML-format messages with lead
//                     element 'quakeml'; added 'getXmlDateFormatter2Obj()'
//                     method.
//   3/5/2019 -- [KF]  Added 'checkJavaVersion' method.
// 11/06/2019 -- [KF]  Modified for OpenMap 5.1.15.
//

package com.isti.quakewatch.util;

import java.util.*;
import java.io.*;
import java.text.DateFormat;
import java.text.NumberFormat;
import java.text.DecimalFormatSymbols;
import org.jdom.Element;
import com.isti.openorbutil.EvtChEventType;
import com.isti.util.UtilFns;
import com.isti.util.TwoObjectMatcher;
import com.isti.util.gis.GisUtils;
import com.isti.util.IstiXmlUtils;
import com.isti.util.DataChangedListener;
import com.isti.quakewatch.common.MsgTag;
import com.isti.quakewatch.common.QWConstants;
import com.isti.quakewatch.message.QWIdentDataMsgRecord;

/**
 * Class QWUtils contains a collection of static utility fields and methods
 * for QuakeWatch.
 */
public class QWUtils implements QWConstants
{
    /** Date format string for parsing and formatting date/time values. */
  public static final String xmlDateFormatString =
                                             "yyyy-MM-dd'T'HH:mm:ss.SSS'Z'";
    /** Alternate date format string for parsing (no trailing 'Z'). */
  public static final String xmlDateFormat2String =
                                                "yyyy-MM-dd'T'HH:mm:ss.SSS";
    /** Timezone object set to "GMT". */
  public static final TimeZone gmtTimeZone = TimeZone.getTimeZone("GMT");
    /** Constant for converting kilometers to miles. */
  public static final double KM_TO_MILE = GisUtils.KM_TO_MILE;
    /** Constant for converting miles to kilometers. */
  public static final double MILE_TO_KM = GisUtils.MILE_TO_KM;
//    /** Floating-point number formatter object. */
//  public static final NumberFormat decValFormatObj =
//                                                 NumberFormat.getInstance();
//    /** Maximum # of digits after decimal point for 'decValFormatObj'. */
//  public static final int MAX_DECFMT_DIGITS = 8;
    /** Floating-point value below which is "nearly" zero. */
  public static final double DOUBLE_ZERO_VAL = 0.00001;
  public static final float FLOAT_ZERO_VAL = 0.00001f;
    /** Single-space string. */
  public static final String SPACE_STR = " ";
              //date formatter for 'parseStringXmlDate()' method:
  private static final DateFormat parseXmlDateFormatterObj =
      UtilFns.createDateFormatObj(xmlDateFormatString,gmtTimeZone,Locale.US);
              //handle for 2-fractional-digit number formatter:
  private static NumberFormat twoDigitFormatter = null;

  static      //static initialization block; executes only once
  {                     //set # of digits after DP for lat/lon formatter:
//    decValFormatObj.setMinimumFractionDigits(1);
//    decValFormatObj.setMaximumFractionDigits(MAX_DECFMT_DIGITS);
//    decValFormatObj.setGroupingUsed(false);      //no commas in output

    //add data change listener
    UtilFns.addDataChangedListener(new DataChangedListener()
        {
          /**
           * Called when data has changed.
           * @param sourceObj source object that called this method.
           */
          public void dataChanged(Object sourceObj)
          {
            //if there was a default locale change
            final Locale localeObj = UtilFns.getDefaultLocale();
            if (sourceObj.equals(localeObj))
            {
              //update the float number formatters
              final DecimalFormatSymbols newSymbols =
                                        new DecimalFormatSymbols(localeObj);
              UtilFns.setDecimalFormatSymbols(twoDigitFormatter,newSymbols);
            }
          }
        });
  }

    //private constructor so that no object instances may be created
    // (static access only)
  private QWUtils()
  {
  }

  /**
   * Check the Java version.
   * @return the java version or null if error.
   */
  public static String checkJavaVersion()
  {
  	return UtilFns.checkJavaVersion(MIN_JAVA_VERSION);
  }

  /**
   * Loads a set of properties from a file.
   * @param fileNameStr the name of the input file.
   * @return A new 'Properties' object containing the properties loaded
   * from the file.
   * @throws FileNotFoundException if the given file was not found.
   * @throws IOException if an error occurs while reading from the file.
   */
  public static Properties loadPropsFromFile(String fileNameStr)
                                    throws FileNotFoundException,IOException
  {
    final Properties propsObj = new Properties();     //create properties obj
                                       //open file for input:
    final BufferedInputStream stmObj = new BufferedInputStream(
                                          new FileInputStream(fileNameStr));
    try
    {
      propsObj.load(stmObj);           //load properties from file
    }
    catch(IOException ex)
    {              //error loading properties
      stmObj.close();             //make sure input file is closed
      throw ex;                   //rethrow exception
    }
    stmObj.close();                    //close input file
    return propsObj;                   //return Properties object
  }

  /**
   * Returns the text to be used for a signature.
   * @param qwMsgElement "QWmessage" element containing the message.
   * @return the text to be used for a signature.
   */
  public static String getTextForSignature(Element qwMsgElement)
  {
    final StringBuffer buff = new StringBuffer();
    final Iterator itr = (qwMsgElement.getChildren()).iterator();

    while(itr.hasNext())
    {
      try
      {
        buff.append(IstiXmlUtils.elementToString((Element)itr.next()));
      }
      catch (Exception ex) {}
    }
    return buff.toString();
  }

  /**
   * Creates and returns a new date-formatter object that may be used
   * to parse and format XML-date/time strings.  The format string
   * is "yyyy-MM-dd'T'HH:mm:ss.SSS'Z'".
   * @return A new 'DateFormat' object.
   */
  public static DateFormat getXmlDateFormatterObj()
  {
    return UtilFns.createDateFormatObj(xmlDateFormatString,gmtTimeZone,
                                                                 Locale.US);
  }

  /**
   * Creates and returns a new date-formatter object that may be used
   * to parse and format XML-date/time strings.  The format string
   * is "yyyy-MM-dd'T'HH:mm:ss.SSS" (no trailing 'Z').
   * @return A new 'DateFormat' object.
   */
  public static DateFormat getXmlDateFormatter2Obj()
  {
    return UtilFns.createDateFormatObj(xmlDateFormat2String,gmtTimeZone,
                                                                 Locale.US);
  }

  /**
   * Parses the given string into a floating-point number.  The numeric
   * string may have a leading '+' or '-' sign and will have leading
   * and trailing whitespace trimmed before being parsed.
   * @param str numeric string to parse.
   * @return A 'Double' object containing the parsed value, or null
   * if the value could not be parsed.
   */
  public static Double parseStringDouble(String str)
  {
    try
    {         //create 'Double' wrapper object and return it:
      int sPos = 0;
      if(str.startsWith("+"))     //if begins with '+' then
        ++sPos;                   //start with next character
      return Double.valueOf(str.substring(sPos).trim());
    }
    catch(Exception ex)
    {
    }
    return null;        //if error then return null
  }

  /**
   * Parses the given string into an integer number.  The numeric
   * string may have a leading '+' or '-' sign and will have leading
   * and trailing whitespace trimmed before being parsed.
   * @param str numeric string to parse.
   * @return A 'Integer' object containing the parsed value, or null
   * if the value could not be parsed.
   */
  public static Integer parseStringInteger(String str)
  {
    try
    {         //create 'Integer' wrapper object and return it:
      int sPos = 0;
      if(str.startsWith("+"))     //if begins with '+' then
        ++sPos;                   //start with next character
      return Integer.valueOf(str.substring(sPos).trim());
    }
    catch(Exception ex)
    {
    }
    return null;        //if error then return null
  }

  /**
   * Parses the given string into an long integer number.  The numeric
   * string may have a leading '+' or '-' sign and will have leading
   * and trailing whitespace trimmed before being parsed.
   * @param str numeric string to parse.
   * @return A 'Long' object containing the parsed value, or null
   * if the value could not be parsed.
   */
  public static Long parseStringLong(String str)
  {
    try
    {         //create 'Integer' wrapper object and return it:
      int sPos = 0;
      if(str.startsWith("+"))     //if begins with '+' then
        ++sPos;                   //start with next character
      return Long.valueOf(str.substring(sPos).trim());
    }
    catch(Exception ex)
    {
    }
    return null;        //if error then return null
  }

  /**
   * Parses the given string as an XML date string.  This method is
   * thread safe.
   * @param str the date string to parse.
   * @return A new 'Date' object, or null if the string could not be
   * parsed as an XML date string.
   */
  public static Date parseStringXmlDate(String str)
  {
    synchronized(parseXmlDateFormatterObj)
    {
      try
      {       //parse date/time string into 'Date' object and return it:
        return parseXmlDateFormatterObj.parse(str);
      }
      catch(Exception ex)
      {       //error parsing
        return null;
      }
    }
  }

  /**
   * Formats an number to produce a string.
   * @param number    The number to format.
   * @param numDigits the minimum and maximum number of fraction
   * digits to be shown.
   * @return Formatted string.
   */
  public static String convertFloatToString(double number, int numDigits)
  {
    return UtilFns.createFloatNumberFormat(numDigits).format(number);
  }

  /**
   * Formats an number to produce a string.
   * @param number    The number to format.
   * @param minDigits the minimum number of fraction digits to be shown.
   * @param maxDigits the maximum number of fraction digits to be shown.
   * @return Formatted string.
   */
  public static String convertFloatToString(double number, int minDigits,
                                                              int maxDigits)
  {
    return UtilFns.createFloatNumberFormat(minDigits, maxDigits).format(number);
  }

  /**
   * Compares two floating-point values for near equality.
   * @param val1 first value.
   * @param val2 second value.
   * @return true if nearly equal, false if not.
   */
  public static boolean isNearlyEqual(double val1, double val2)
  {
    return (Math.abs(val2-val1) < DOUBLE_ZERO_VAL);
  }

  /**
   * Compares two floating-point values for near equality.
   * @param val1 first value.
   * @param val2 second value.
   * @return true if nearly equal, false if not.
   */
  public static boolean isNearlyEqual(float val1, float val2)
  {
    return (Math.abs(val2-val1) < FLOAT_ZERO_VAL);
  }

  /**
   * Converts the given list string of event domain and type names to an
   * array of event-type objects.  The list string must be in the format
   * "domain:type,domain:type...".  Occurrences of the ':' and ','
   * characters not meant as separators may be "quoted" by preceding
   * them with the backslash ('\') character.  List items missing the
   * ':' character will be considered to specify only a domain name
   * (the type name will be an empty string).
   * @param listStr the list string to use.
   * @param extraDomainStr extra domain-name string to be appended to
   * list (if given), or null for none.
   * @param extraTypeStr extra type-name string to be appended to
   * list (if given), or null for none.
   * @return A new 'EvtChEventType' array, or null if no domain/type
   * names were found in the list string.
   */
  public static EvtChEventType [] listStringToEventTypeArray(String listStr,
                                 String extraDomainStr, String extraTypeStr)
  {
    try
    {
      if(listStr == null || (listStr=listStr.trim()).length() <= 0)
        return null;         //no data in list string
      boolean extraFlag;          //setup extra-domain-type-given flag:
      if(extraFlag=(extraDomainStr != null || extraTypeStr != null))
      {  //extra domain or type string given
        if(extraDomainStr == null)               //if extra-domain is null
          extraDomainStr = UtilFns.EMPTY_STRING; // then set it to empty str
        if(extraTypeStr == null)                 //if extra-type is null
          extraTypeStr = UtilFns.EMPTY_STRING;   // then set it to empty str
      }
              //convert comma-separated list to a Vector of item strings:
      final Vector itemsVec = UtilFns.listStringToVector(listStr,',',true);
      final Iterator iterObj = itemsVec.iterator();
      final Vector eventTypeVec = new Vector();
      String itemStr,domainStr,typeStr;
      int p;
      while(iterObj.hasNext())
      {  //for each "domain:type" list item string
        itemStr = (String)(iterObj.next());
        if(itemStr.length() > 0)
        {     //item string contains data; find ':' separator
          if((p=UtilFns.findCharPos(itemStr,':')) >= 0)
          {   //':' separator found; parse strings
            domainStr = UtilFns.removeQuoteChars(
                                        itemStr.substring(0,p),true).trim();
            typeStr = UtilFns.removeQuoteChars(
                                        itemStr.substring(p+1),true).trim();
          }
          else
          {   //':' separator not found; take item string as domain name
            domainStr = UtilFns.removeQuoteChars(itemStr,true).trim();
            typeStr = "";         //no type name
          }
                   //create event-type object and add to Vector:
          eventTypeVec.add(new EvtChEventType(domainStr,typeStr));
              //check domain/type vs. extra domain/type names:
          if(extraFlag && domainStr.equals(extraDomainStr) &&
                                               typeStr.equals(extraTypeStr))
          {   //domain/type same as extra domain/type names
            extraFlag = false;    //cancel adding of extra domain/type names
          }
        }
      }
      if(extraFlag)          //if flag then add extra domain/type names:
        eventTypeVec.add(new EvtChEventType(extraDomainStr,extraTypeStr));
      if((p=eventTypeVec.size()) > 0)
      {  //Vector contains data; convert to array and return
        return (EvtChEventType [])(eventTypeVec.toArray(
                                                    new EvtChEventType[p]));
      }
    }
    catch(Exception ex)
    {         //some kind of exception error; just return null
    }
    return null;
  }

  /**
   * Converts the given list string of event domain and type names to an
   * array of event-type objects.  The list string must be in the format
   * "domain:type,domain:type...".  Occurrences of the ':' and ','
   * characters not meant as separators may be "quoted" by preceding
   * them with the backslash ('\') character.  List items missing the
   * ':' character will be considered to specify only a domain name
   * (the type name will be an empty string).
   * @param listStr the list string to use.
   * @return A new 'EvtChEventType' array, or null if no domain/type
   * names were found in the list string.
   */
  public static EvtChEventType [] listStringToEventTypeArray(String listStr)
  {
    return listStringToEventTypeArray(listStr,null,null);
  }

  /**
   * Takes an array of EvtChEventType objects and returns a string of the
   * array in list format. The list string will be in the format
   * "domain:type,domain:type...".  Occurrences of the ':' and ','
   * characters not meant as separators may be "quoted" by preceding
   * them with the backslash ('\') character.  List items missing the
   * ':' character will be considered to specify only a domain name
   * (the type name will be an empty string).
   * @param list the EvtChEventType list.
   * @return The string for archiving.
   */
  public static String typeArraytoListString (EvtChEventType[] list)
  {
    final StringBuffer sb = new StringBuffer();
    final int len;
    if((len=list.length) > 0)
    {    //array not empty
      int i = 0;
      while(true)
      {  //for each domain/type object in array
        sb.append(list[i].toListString());
        if(++i >= len)
          break;
        sb.append(",");
      }
    }
    return sb.toString();
  }

  /**
   * Converts the given list string of event domain and type names to a
   * matcher object that may be used to match pairs of domain and type
   * names against those in the list.  The list string must be in the format
   * "domain:type,domain:type...".  Occurrences of the ':' and ','
   * characters not meant as separators may be "quoted" by preceding
   * them with the backslash ('\') character.  List items missing the
   * ':' character will be considered to specify only a domain name
   * (the type name will be an empty string).
   * @param listStr the list string to use.
   * @return A new 'TwoObjectMatcher' built from the list string items,
   * or null if no domain/type names were found in the list string.
   */
  public static TwoObjectMatcher listStringToMatcher(String listStr)
  {
    final EvtChEventType [] typeArr;
    final int typeArrLen;    //convert list to array of event-type objs:
    if((typeArr=listStringToEventTypeArray(listStr,null,null)) != null &&
                                            (typeArrLen=typeArr.length) > 0)
    {    //convert to array OK; create matcher object to be filled
      final TwoObjectMatcher matcherObj = new TwoObjectMatcher();
              //load matcher object with items from list:
      for(int i=0; i<typeArrLen; ++i)
        matcherObj.add(typeArr[i].domainStr,typeArr[i].typeStr);
      return matcherObj;          //return matcher object
    }
    return null;        //no items found in list
  }

  /**
   * Returns a string showing the given value in degrees and minutes.
   * @param value the value to use.
   * @param degStr the tag string to use for the degrees value.
   * @param minStr the tag string to use for the minutes value.
   * @return The degrees/minutes string.
   */
  public static String getDegMinStr(double value, String degStr,
                                                              String minStr)
  {
    final int degVal = (int)value;                    //integer degrees
    final double minVal = (value - degVal) * 60.0;    //fractional minutes
    if(twoDigitFormatter == null)      //if no formatter then create one
      twoDigitFormatter = UtilFns.createFloatNumberFormat(2);
    synchronized(twoDigitFormatter)
    {    //only allow one thread at a time use formatter
              //build string (make sure minutes value is not negative):
      return degVal + SPACE_STR + degStr + SPACE_STR +
                    twoDigitFormatter.format((minVal>=0.0)?minVal:-minVal) +
                                                         SPACE_STR + minStr;
    }
  }

  /**
   * Returns a string showing the given value in degrees and minutes.
   * The tags "deg" and "min" are used.
   * @param value the value to use.
   * @return The degrees/minutes string.
   */
  public static String getDegMinStr(double value)
  {
    return getDegMinStr(value,"deg","min");
  }

  /**
   * Returns a descriptive string for the given magnitude.
   * @param magVal the magnitude value to use.
   * @return A descriptive string for the given magnitude, in the
   * form " (A xxx quake)".
   */
  public static String getMagnitudeDescStr(double magVal)
  {
    final String descStr;
    if(magVal >= 8.0)
      descStr = "great";
    else if(magVal >= 7.0)
      descStr = "major";
    else if(magVal >= 6.0)
      descStr = "strong";
    else if(magVal >= 5.0)
      descStr = "moderate";
    else if(magVal >= 4.0)
      descStr = "light";
    else if(magVal >= 3.0)
      descStr = "minor";
    else if(magVal > 0.0)
      descStr = "micro";
    else
      return "";
    return " (A " + descStr + " earthquake)";
  }

  /**
   * Translates the given magnitude-type character to a descriptive
   * 1-3 character string.  The following characters are expected: <pre>
   *     B = body magnitude (Mb)
   *     C = duration magnitude (Md)
   *     D = duration magnitude (Md)
   *     E = energy magnitude (Me)
   *     G = local magnitude (Ml)
   *     I = "Tsuboi" moment magnitude (Mi)
   *     L = local magnitude (Ml)
   *     N = "Nuttli" surface wave magnitude (MbLg)
   *     O = moment magnitude (Mw)
   *     P = body magnitude (Mb)
   *     S = surface wave magnitude (Ms)
   *     T = teleseismic moment magnitude (Mt)
   *     W = regional moment magnitude (Mw)
   * </pre>
   * @param magCh the magnitude-character to use.
   * @return A 1-3 character magnitude-type string, or an empty string
   * if the magnitude type is not recognized.
   */
  public static String magTypeCharToStr(char magCh)
  {
    switch(Character.toUpperCase(magCh))
    {
      case 'B':              //body magnitude (Mb)
        return "b";
      case 'C':              //duration magnitude (Md)
        return "d";
      case 'D':              //duration magnitude (Md)
        return "d";
      case 'E':              //energy magnitude (Me)
        return "e";
      case 'G':              //local magnitude (Ml)
        return "l";
      case 'I':              //"Tsuboi" moment magnitude (Mi)
        return "i";
      case 'L':              //local magnitude (Ml)
        return "L";
      case 'N':              //"Nuttli" surface wave magnitude (MbLg)
        return "bLg";
      case 'O':              //moment magnitude (Mw)
        return "w";
      case 'P':              //body magnitude (Mb)
        return "b";
      case 'S':              //surface wave magnitude (Ms)
        return "s";
      case 'T':              //teleseismic moment magnitude (Mt)
        return "t";
      case 'W':              //regional moment magnitude (Mw)
        return "w";
      default:
        return "";
    }
  }

  /**
   * Translates the given magnitude-type character to a descriptive
   * 1-3 character string.  The following characters are expected: <pre>
   *     B = body magnitude (Mb)
   *     C = duration magnitude (Md)
   *     D = duration magnitude (Md)
   *     E = energy magnitude (Me)
   *     G = local magnitude (Ml)
   *     I = "Tsuboi" moment magnitude (Mi)
   *     L = local magnitude (Ml)
   *     N = "Nuttli" surface wave magnitude (MbLg)
   *     O = moment magnitude (Mw)
   *     P = body magnitude (Mb)
   *     S = surface wave magnitude (Ms)
   *     T = teleseismic moment magnitude (Mt)
   *     W = regional moment magnitude (Mw)
   * </pre>
   * @param magCharStr a string containing the magnitude-character to use.
   * @return A 1-3 character magnitude-type string, or an empty string
   * if the magnitude type is not recognized.
   */
  public static String magTypeCharToStr(String magCharStr)
  {
    return magTypeCharToStr((magCharStr.length() > 0) ?
                                                magCharStr.charAt(0) : ' ');
  }

  /**
   * Determines the format of the given message element and returns
   * the lead element for the message.
   * @param dataMsgElemObj 'DataMessage' element for message.
   * @return A new 'ElemAndMsgFmtSpec' object containing the lead
   * element and the message-format specifier (one of the
   * 'QWIdentDataMsgRecord.MFMT_...' values).
   */
  public static ElemAndMsgFmtSpec determineMessageFormat(
                                                     Element dataMsgElemObj)
  {
    final List listObj;
    if((listObj=dataMsgElemObj.getChildren()) != null)
    {  //list of child elements fetched OK
      final Iterator iterObj = listObj.iterator();
      Object obj;
      Element elemObj;
      String nameStr;
      while(iterObj.hasNext())
      {  //for each child element fetched
        if((obj=iterObj.next()) instanceof Element)
        {  //element object fetched OK
          elemObj = (Element)obj;
          nameStr = elemObj.getName();
          if(MsgTag.QUAKE_MESSAGE.equals(nameStr) ||
                                            MsgTag.QUAKE_ML.equals(nameStr))
          {  //element name is 'quakeMessage' or 'quakeml'
            return new ElemAndMsgFmtSpec(   //indicate QuakeML format
                                 elemObj,QWIdentDataMsgRecord.MFMT_QUAKEML);
          }
          if(MsgTag.EQMESSAGE.equals(nameStr))
          {  //element name is 'EQMessage'
            return new ElemAndMsgFmtSpec(   //indicate ANSS-EQ-XML format
                               elemObj,QWIdentDataMsgRecord.MFMT_ANSSEQXML);
          }
        }
      }
    }
                   //default to "QuakeWatch" format:
    return new ElemAndMsgFmtSpec(dataMsgElemObj,
                                       QWIdentDataMsgRecord.MFMT_QWMESSAGE);
  }


  /**
   * Class ElemAndMsgFmtSpec holds an element object and a message-format
   * specifier (one of the 'QWIdentDataMsgRecord.MFMT_...' values).
   */
  public static class ElemAndMsgFmtSpec
  {
    public final Element elementObj;
    public final int messageFormatSpec;

    /**
     * Creates a holder object.
     * @param elementObj element object.
     * @param messageFormatSpec message-format specifier (one of the
     * 'QWIdentDataMsgRecord.MFMT_...' values).
     */
    public ElemAndMsgFmtSpec(Element elementObj, int messageFormatSpec)
    {
      this.elementObj = elementObj;
      this.messageFormatSpec = messageFormatSpec;
    }
  }
}
