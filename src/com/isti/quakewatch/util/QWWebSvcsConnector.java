//QWWebSvcsConnector.java:  Manages a connection to a QuakeWatch Web
//                          Services Server.

package com.isti.quakewatch.util;

import java.net.URL;
import com.isti.util.UtilFns;
import com.isti.util.LogFile;
import com.isti.util.IstiNotifyThread;
import com.isti.util.CfgPropItem;
import com.isti.util.AddrPortListMgr;
import com.isti.quakewatch.common.QWConnStrings;
import com.isti.quakewatch.message.QWMessageHandler;
import com.isti.util.ProgressIndicatorInterface;

/**
 * Class QWWebSvcsConnector manages a connection to a QuakeWatch Web
 * Services Server.
 */
public class QWWebSvcsConnector extends QWAbstractConnector
{
    /** Default polling interval (used if unable to fetch from server). */
  protected static final int DEF_POLLINGITVL_MS = 10000;
  protected final QWWebSvcsInterface webSvcsManagerObj;
  protected final QWMessageHandler msgHandlerObj;
  protected final CfgPropItem maxServerAliveSecProp;
  protected final AddrPortListMgr altServersListMgr;
  protected static Class webSvcsManagerClassObj = null;
  protected static String webSvcsMgrClassErrMsgStr = null;
  protected String webSvcsInitErrorMsgStr = null;
  protected boolean connectedToServerFlag = false;
              //handle for connect-login-attempt-rejected call-back:
  protected ConnLoginRejectCallBack connLoginRejectCallBackObj = null;
              //handle for current connect thread object:
  protected WebServicesConnectThread webSvcsConnectThreadObj = null;
              //thread-sync object for 'webSvcsConnectThreadObj':
  protected final Object webSvcsConnThreadSyncObj = new Object();
              //handle for current disconnect thread object:
  protected WebServicesDisconnectThread webSvcsDisconnectThreadObj = null;
              //thread-sync object for 'webSvcsDisconnectThreadObj':
  protected final Object webSvcsDisconnThreadSyncObj = new Object();
              //handle for current message-fetching thread object:
  protected MessageFetchingThread messageFetchingThreadObj = null;
              //thread-sync object for 'messageFetchingThreadObj':
  protected final Object msgFetchingThreadSyncObj = new Object();
              //recommended polling interval from server:
  protected int messagePollingIntervalMS = DEF_POLLINGITVL_MS;
              //flag set true after 'requestServerAliveMsg()' called:
  protected boolean aliveMessageRequestedFlag = false;


  /**
   * Creates a QW Web Services Server connector object.
   * @param webSvcsManagerObj QW Web Services Server manager to use, or
   * null to dynamically instantiate 'QWWebSvcsManager'.
   * @param connLoginRejectCallBackObj call-back object whose method is
   * invoked when a connect-login-attempt cannot be completed, or null for
   * none.
   * @param msgHandlerObj 'QWMessageHandler' object to use, or null for
   * none.
   * @param maxServerAliveSecProp configuration property object specifying
   * maximum # of seconds between server-alive messages, or null to have
   * default value used.
   * @param altServersListMgr alternate servers list manager object, or
   * null for none.
   * @param connStatusObj connection-status panel object, or null for none.
   * @param logObj log-file object to be used, or null for none.
   */
  public QWWebSvcsConnector(QWWebSvcsInterface webSvcsManagerObj,
                         ConnLoginRejectCallBack connLoginRejectCallBackObj,
                                             QWMessageHandler msgHandlerObj,
                                          CfgPropItem maxServerAliveSecProp,
                                          AddrPortListMgr altServersListMgr,
                          ConnStatusInterface connStatusObj, LogFile logObj)
  {
    super(connStatusObj,logObj);
    if(webSvcsManagerObj == null)
    {    //web services manager object not given
      if(loadQWWebSvcsManagerClassObj())
      {  //loaded class object for 'QWWebSvcsManager' class OK
        try
        {     //dynamically instantiate the 'QWWebSvcsManager' class:
          webSvcsManagerObj = (QWWebSvcsInterface)(
            webSvcsManagerClassObj.getDeclaredConstructor().newInstance());
        }
        catch(Throwable ex)
        {     //unable to instantiate
                   //set error message shown during connection attempt:
          webSvcsInitErrorMsgStr =
                         "Unable to instantiate 'QWWebSvcsManager':  " + ex;
        }
      }
      else    //unable to load 'QWWebSvcsManager' class; set error message
        webSvcsInitErrorMsgStr = webSvcsMgrClassErrMsgStr;
    }
    this.webSvcsManagerObj = webSvcsManagerObj;
    this.connLoginRejectCallBackObj = connLoginRejectCallBackObj;
    this.msgHandlerObj = msgHandlerObj;
    this.maxServerAliveSecProp = (maxServerAliveSecProp != null) ?
                                                     maxServerAliveSecProp :
            (new CfgPropItem("default_maxServerAliveSec",(Integer.valueOf(0))));
    this.altServersListMgr = altServersListMgr;
  }

  /**
   * Creates a QW Web Services Server connector object.  The QW web services
   * manager class "com.isti.quakewatch.qwwebsvcs.QWWebSvcsManager" is
   * dynamically instantiated.
   * @param connLoginRejectCallBackObj call-back object whose method is
   * invoked when a connect-login-attempt cannot be completed, or null for
   * none.
   * @param msgHandlerObj 'QWMessageHandler' object to use, or null for
   * none.
   * @param maxServerAliveSecProp configuration property object specifying
   * maximum # of seconds between server-alive messages, or null to have
   * default value used.
   * @param altServersListMgr alternate servers list manager object, or
   * null for none.
   * @param connStatusObj connection-status panel object, or null for none.
   * @param logObj log-file object to be used, or null for none.
   */
  public QWWebSvcsConnector(
                         ConnLoginRejectCallBack connLoginRejectCallBackObj,
           QWMessageHandler msgHandlerObj,CfgPropItem maxServerAliveSecProp,
                                          AddrPortListMgr altServersListMgr,
                          ConnStatusInterface connStatusObj, LogFile logObj)
  {
    this(null,connLoginRejectCallBackObj,msgHandlerObj,
              maxServerAliveSecProp,altServersListMgr,connStatusObj,logObj);
  }

  /**
   * Loads the class object for the 'QWWebSvcsManager' class.
   * @return true if successful; false if not.
   */
  public static boolean loadQWWebSvcsManagerClassObj()
  {
    if(webSvcsManagerClassObj == null)
    {    //'QWWebSvcsManager' class object not yet loaded
      if(webSvcsMgrClassErrMsgStr != null)       //if previous error then
        return false;                            //return error flag
      try
      {       //load class object for 'QWWebSvcsManager' class:
        webSvcsManagerClassObj = Class.forName(
                          "com.isti.quakewatch.qwwebsvcs.QWWebSvcsManager");
      }
      catch(ClassNotFoundException ex)
      {       //unable to load class; enter error message
        webSvcsMgrClassErrMsgStr = "Unable to load QW Web Services Server" +
                          " support (QWWebSvcsMgr jar file may be missing)";
        return false;
      }
      catch(Throwable ex)
      {       //exception error loading class
        webSvcsMgrClassErrMsgStr =          //enter error message
                          "Unable to load 'QWWebSvcsManager' class:  " + ex;
        return false;
      }
    }
    return true;
  }

  /**
   * Updates the connection parameters to be used on the next initialization.
   * @param clientHostAddress an IP address to be associated with this
   * client, or null to have the address auto-determined.
   * @param clientPortNum a port address value to be associated with this
   * client, or 0 to have the port address auto-determined.
   */
  public void updateConnectParams(String clientHostAddress,
                                                         int clientPortNum)
  {
  }

  /**
   * Enters the connection-information values to be sent when the client
   * connects to the server.  This overridden version calls the parent
   * method and then adds a username entry to the connection-information
   * properties string.
   * @param userNameStr the user name string to use.
   * @param passwordStr the password string to use.
   * @param connPropsStr the connection-information properties string to
   * use.
   */
  public void enterConnectionInfo(String userNameStr,
                                    String passwordStr, String connPropsStr)
  {
    super.enterConnectionInfo(userNameStr,passwordStr,connPropsStr);
    connectionInfoPropsString += ",\"" + UtilFns.insertQuoteChars(
                               QWConnStrings.CLIENT_USERNAME,"\"\r\n\t\\") +
                                               "\"=\"" + userNameStr + '\"';
  }

  /**
   * Starts a new thread that establishes a connection to a QWServer.
   * @param serverHostAddrStr the host address used to locate the server.
   * @param serverHostPortNum the port number used to locate the server.
   * @param showPopupFlag true to force the connection status popup
   * dialog to be shown.
   */
  public void startConnectViaHostInfo(String serverHostAddrStr,
                               int serverHostPortNum, boolean showPopupFlag)
  {
         //log connection parameters in use:
    logObj.debug("QW Web Services Server host address = \"" +
              serverHostAddrStr + "\", port number = " + serverHostPortNum);
         //make sure any current connect thread is terminated:
    terminateConnectThread(3);
         //init connect done, attempt-failed and attempt-rejected flags:
    connectDoneFlag = connAttemptFailedFlag = false;
                             //enter values for host addr and port #:
    serverHostInfoObj.setValues(serverHostAddrStr,serverHostPortNum);
    synchronized(webSvcsConnThreadSyncObj)
    {    //grab thread-sync object for 'webSvcsConnectThreadObj'
                             //create new thread for connection attempt:
      webSvcsConnectThreadObj = new WebServicesConnectThread(
                                       serverHostAddrStr,serverHostPortNum);
         //if flag and connection status object given then show panel:
      if(showPopupFlag && connStatusObj != null)
        connStatusObj.showPopupDialog();    //show status popup panel
      webSvcsConnectThreadObj.start();        //start connection thread
    }
  }

  /**
   * Terminates and disposes any event channel connect thread that is
   * running.
   * @param numWaitSecs number of seconds to wait for thread to finish
   * terminating.
   */
  public void terminateConnectThread(int numWaitSecs)
  {
    final boolean runningFlag;
    synchronized(webSvcsConnThreadSyncObj)
    {    //grab thread-sync object for 'webSvcsConnectThreadObj'
              //set flag if connect thread is running:
      if(runningFlag = (webSvcsConnectThreadObj != null))
      {       //connect thread is running
        webSvcsConnectThreadObj.terminate();    //terminate it
        webSvcsConnectThreadObj = null;         //release thread object
      }
    }
    if(runningFlag)
    {    //previous connect was running
      int count = 0;    //wait for terminate complete, up to given # of secs:
      while(!connectDoneFlag && ++count <= numWaitSecs &&
                                                       UtilFns.sleep(1000));
    }
  }

  /**
   * Shuts down the connection and waits for the disconnect to
   * complete or timeout.
   * @param showPopupFlag true to force the connection status popup
   * dialog to be shown.
   */
  public void shutdownConnection(boolean showPopupFlag)
  {
    try
    {
      startDisconnectThread();         //start new thread to do disconnect
      if(showPopupFlag)                //if flag then show popup
        showConnPanelPopup();
      logObj.debug3("QWWebSvcsConnector.shutdownConnection:  " +
                                                  "Waiting for disconnect");
      int cnt = 0;
      while(!disconnectDoneFlag)       //loop until complete or timeout
      {
        if(++cnt > 100)
        {     //local timeout reached
          logObj.debug("QWWebSvcsConnector:  " +
                     "Timeout waiting for shutdown-connection to complete");
          break;
        }
        try { Thread.sleep(100); }          //delay between checks
        catch(InterruptedException ex) {}
      }
      logObj.debug3("QWWebSvcsConnector.shutdownConnection:  " +
                                         "Finished waiting for disconnect");
    }
    catch(Exception ex)
    {         //some kind of exception error; log it and exit method
      logObj.warning("Exception in 'shutdownConnection()':  " + ex);
    }
  }

  /**
   * Starts a new thread that disconnects the QW web services connection.
   */
  public void startDisconnectThread()
  {
         //make sure any current connect thread is terminated:
    terminateConnectThread(1);
         //make sure any current disconnect thread is terminated:
    terminateDisconnectThread(3);    // (wait for up to 3 seconds)
    invalidateConnection();       //indicate connection "invalidated"
         //init connect done, attempt-failed and attempt-rejected flags:
    connectDoneFlag = connAttemptFailedFlag = connAttemptRejectedFlag =
                                                                      false;
    synchronized(webSvcsDisconnThreadSyncObj)
    {    //grab thread-sync object for 'webSvcsDisconnectThreadObj'
      disconnectDoneFlag = false;           //initial done flag
                             //create new thread for connection attempt:
      webSvcsDisconnectThreadObj = new WebServicesDisconnectThread();
      webSvcsDisconnectThreadObj.start();        //start connection thread
    }
  }

  /**
   * Terminates and disposes any event channel disconnect thread that is
   * running.
   * @param numWaitSecs number of seconds to wait for thread to finish
   * terminating.
   */
  public void terminateDisconnectThread(int numWaitSecs)
  {
    final boolean runningFlag;
    synchronized(webSvcsDisconnThreadSyncObj)
    {    //grab thread-sync object for 'webSvcsDisconnectThreadObj'
              //set flag if disconnect thread is running:
      if(runningFlag = (webSvcsDisconnectThreadObj != null))
      {       //disconnect thread is running
        webSvcsDisconnectThreadObj.terminate();  //terminate it
        webSvcsDisconnectThreadObj = null;       //release thread object
      }
    }
    if(runningFlag)
    {    //previous disconnect was running
      int count = 0;    //wait for terminate complete, up to given # of secs:
      while(!disconnectDoneFlag && ++count <= numWaitSecs &&
                                                       UtilFns.sleep(1000));
    }
  }

  /**
   * Performs the work of disconnecting this client from the server
   * services.
   * @return true if successful; false if an error occurred.
   */
  protected boolean doDisconnectClientServices()
  {
    try
    {
      if(webSvcsManagerObj != null && connectedToServerFlag)
      {    //connected to server
        final Boolean retFlagObj;
        retFlagObj = webSvcsManagerObj.disconnectClient(
                                       (connectionInfoPropsString != null) ?
                          connectionInfoPropsString : UtilFns.EMPTY_STRING);
        logObj.debug3(
                 "QWWebSvcsConnector:  Returned from 'disconnectClient()'");
        if(retFlagObj != null)
        {     //method call returned a flag value
          if(retFlagObj.booleanValue())
          {   //method returned 'true'
            logObj.debug("QWWebSvcsConnector:  " +
                           "Disconnected client from server services OK");
          }
          else
          {   //method returned 'false'
            logObj.debug("QWWebSvcsConnector:  " +
                              "'disconnectClient()' method return 'false'");
          }
        }
      }
      disconnectClientServicesFlag = true;       //indicate finished
      return true;
    }
    catch(Exception ex)
    {
      logObj.warning("Error calling disconnect-client service method:  " +
                                                                        ex);
      disconnectClientServicesFlag = true;       //indicate finished
      return false;
    }
  }

  /**
   * Returns the server ID name string (defined in the QWServer's
   * configuration file).
   * @return The server ID name string, or null if not available.
   */
  public String getServerIDNameStr()
  {
    return (connectedToServerFlag && serverIdNameString != null &&
               serverIdNameString.length() > 0) ? serverIdNameString : null;
  }

  /**
   * Returns the host address reported by the server.
   * @return The host address reported by the server, or null if not
   * available.
   */
  public String getServerRepHostAddrStr()
  {
    return (connectedToServerFlag && serverRepHostAddrString != null &&
                                     serverRepHostAddrString.length() > 0) ?
                                             serverRepHostAddrString : null;
  }

  /**
   * Returns the server revision string.
   * @return The server revision string, or null if not available.
   */
  public String getServerRevisionStr()
  {
    return (connectedToServerFlag && serverRevisionString != null &&
           serverRevisionString.length() > 0) ? serverRevisionString : null;
  }

  /**
   * Requests that the server send an 'Alive' message immediately.
   */
  public void requestServerAliveMsg()
  {
    aliveMessageRequestedFlag = true;
  }

  /**
   * Returns an indicator of whether or not the 'requestMessages()'
   * method is available on the current server.
   * @return true.
   */
  public boolean isReqServerMsgsAvailable()
  {
    return true;
  }

  /**
   * Requests that messages newer or equal to the specified time value or
   * later than the specified message number be returned from the server.
   * If a 'hostMsgNumListStr' value is not given and a non-zero message
   * number is given and the given time value matches then messages with
   * message numbers greater than the given message number are returned;
   * otherwise messages newer or equal to the specified time value are
   * returned (within a tolerance value).  If a 'hostMsgNumListStr' value
   * is given then it is used with the server "requestSourced...()"
   * methods (and any given message number is ignored).  An XML message
   * containing the messages is returned, using the following format: <pre>
   * [QWresend NumRequested="10"]
   *   [QWmessage ...] [/QWmessage]
   *   [QWmessage ...] [/QWmessage]
   *   ...
   * [/QWresend]
   * </pre> (Substitute angle brackets for square brackets.)
   * The number of messages that would be required to fill the request
   * is returned in the "NumRequested" attribute.  However, no more than
   * the oldest 'maxResendNumMsgs' number of messages will be returned
   * in any single call to this method (additional calls may be needed to
   * fetch all of the desired messages).
   * @param timeVal the time-generated value for message associated with
   * the given message number, or the requested time value to be used
   * (milliseconds since 1/1/1970).
   * @param msgNum the message number to use, or 0 or none.
   * @param hostMsgNumListStr a list of feeder-data-source
   * host-name/message-number entries, or null or empty string
   * if the "requestSourced...()" methods should not be used.
   * @return An XML-formatted string containing the messages, or an empty
   * string if an error occurs.
   */
  public String requestServerMessages(long timeVal, long msgNum,
                                                   String hostMsgNumListStr)
  {
                   //prefix string for error messages:
    final String ERR_PRESTR = "Error requesting messages from server:  ";
         //set flag if feeder-data-source info was given:
    boolean reqSrcdFlag = (hostMsgNumListStr != null &&
                                     hostMsgNumListStr.trim().length() > 0);
    int retryCount = 0;
    while(true)
    {    //loop while retrying request
      try
      {
        if(webSvcsManagerObj != null && connectedToServerFlag)
        {     //connected to server
          logObj.debug3("QWWebSvcsConnector:  Requesting send of messages" +
               " from server, timeVal=" + timeVal + ", msgNum=" + msgNum);
          logObj.debug3("  hostMsgNumListStr:  " +
                                            ((hostMsgNumListStr != null) ?
                                           hostMsgNumListStr : "<null>"));
          final String retStr;
          if(reqSrcdFlag)
          {      //use "requestSourced...()" methods
            if((retStr=webSvcsManagerObj.requestSourcedMsgsStr(
                timeVal,hostMsgNumListStr,filterDomainTypeListStr)) == null)
            {
              logObj.warning(ERR_PRESTR + "'requestSourcedMsgsStr':  " +
                                 webSvcsManagerObj.getErrorMessageString());
            }
          }
          else
          {      //don't use "requestSourced...()" methods
            if(filterDomainTypeListStr != null)
            {    //filtering enabled; pass in filter-string
              if((retStr=webSvcsManagerObj.requestFilteredMessages(
                           timeVal,msgNum,filterDomainTypeListStr)) == null)
              {
                logObj.warning(ERR_PRESTR + "'requestFilteredMessages':  " +
                                 webSvcsManagerObj.getErrorMessageString());
              }
            }
            else
            {         //filtering not enabled
              if((retStr=webSvcsManagerObj.requestMessages(timeVal,msgNum))
                                                                    == null)
              {
                logObj.warning(ERR_PRESTR + "'requestMessages':  " +
                                 webSvcsManagerObj.getErrorMessageString());
              }
            }
          }
                 //if non-empty string received then return it:
          if(retStr != null)
          {
            if(retStr.length() > 0)
              return retStr;
            else
              logObj.warning(ERR_PRESTR + "Empty string returned");
          }
        }
        else
        {
          logObj.warning(ERR_PRESTR + "Not connected to server");
          return UtilFns.EMPTY_STRING;
        }
      }
      catch(Exception ex)
      {
        logObj.warning(ERR_PRESTR + ex);
      }
      if(++retryCount > REQMSGS_NUM_RETRIES)
        return UtilFns.EMPTY_STRING;
      logObj.info("  Retrying request (#-attempts=" + retryCount + ")");
    }
  }

  /**
   * Calls the server 'clientStatusCheck()' method.
   * @return true if the an updated version of the client is available;
   * false if not.
   * @throws NoSuchMethodException if the 'clientStatusCheck()' method
   * is not implemented on the server.
   * @throws StatusCheckFailedException if the call to 'clientStatusCheck()'
   * failed with an exception other than 'NoSuchMethodException'.
   */
  public boolean performClientStatusCheck()
                    throws NoSuchMethodException, StatusCheckFailedException
  {
    if(webSvcsManagerObj != null && connectedToServerFlag)
    {    //connected to server
      final Boolean retFlagObj;
      if((retFlagObj=webSvcsManagerObj.clientStatusCheck(
                                       (connectionInfoPropsString != null) ?
                 connectionInfoPropsString : UtilFns.EMPTY_STRING)) != null)
      {  //method call succeeded
        final boolean retFlag = retFlagObj.booleanValue();
        logObj.debug4("QWWebSvcsConnector:  Return value from " +
                                    "'clientStatusCheck()' = " + retFlag);
        return retFlag;
      }
         //method call failed; check if method not implemented on server
      String errMsgStr;
      if((errMsgStr=webSvcsManagerObj.getErrorMessageString()) != null &&
                                errMsgStr.indexOf("No such operation") >= 0)
      {  //method not implemented on server; indicate via exception
        throw new NoSuchMethodException();
      }
      throw new StatusCheckFailedException(   //indicate error via exception
                      "Error calling 'clientStatusCheck()':  " + errMsgStr);
    }
    return false;
  }

  /**
   * Returns information about available client-program upgrades via
   * the server 'getClientUpgradeInfo()' method.
   * @return An XML-formatted string containing information about
   * available client-program upgrades, or null if an error occurred.
   */
  public String fetchClientUpgradeInfoFromServer()
  {
    if(webSvcsManagerObj != null && connectedToServerFlag)
    {    //connected to server
      final String retStr;
      if((retStr=webSvcsManagerObj.getClientUpgradeInfo(
                                       (connectionInfoPropsString != null) ?
                 connectionInfoPropsString : UtilFns.EMPTY_STRING)) != null)
      {  //method call succeeded
        logObj.debug4("QWWebSvcsConnector:  Returned from " +
                                             "'getClientUpgradeInfo()' OK");
        return retStr;
      }
         //method call failed; log error message
      logObj.warning("Error calling 'getClientUpgradeInfo()':  " +
                                 webSvcsManagerObj.getErrorMessageString());
    }
    return null;
  }

  /**
   * Returns the location of the server that the client is
   * being redirected to.  The returned string may also be
   * a comma-separated list of "hostAddr:portNum" entries.
   * @return The redirect-server location string in the form
   * "hostAddr:portNum", an empty string if the client is
   * not being redirected, or null if an error occurred.
   */
  public String fetchRedirectedServerLoc()
  {
    return UtilFns.EMPTY_STRING;
  }

  /**
   * Determines if get-status-report methods are available on the server.
   * @return true if get-status-report methods are available on the server;
   * false if not.
   */
  public boolean isStatusReportDataAvail()
  {
    return statusReportDataAvailFlag;
  }

  /**
   * Fetches the timestamp value for the latest status report from the
   * server.
   * @return The timestamp value for the latest status report from the
   * server, or 0 if no report is available or if an error occurred.
   */
  public long getStatusReportTime()
  {
    if(webSvcsManagerObj != null && connectedToServerFlag)
    {    //connected to server
      final long timeVal = webSvcsManagerObj.getStatusReportTime();
      logObj.debug4("QWWebSvcsConnector:  Returned from " +
                                              "'getStatusReportTime()' OK");
      return timeVal;
    }
    return 0;
  }

  /**
   * Fetches the latest status-report data from the server.
   * @return A string containing the latest status-report data from
   * the server, an empty string if no report is available, or null
   * if an error occurred.
   */
  public String getStatusReportData()
  {
    if(webSvcsManagerObj != null && connectedToServerFlag)
    {    //connected to server
      final String retStr;
      if((retStr=webSvcsManagerObj.getStatusReportData()) != null)
      {  //method call succeeded
        logObj.debug4("QWWebSvcsConnector:  Returned from " +
                                              "'getStatusReportData()' OK");
        return retStr;
      }
         //method call failed; log error message
      logObj.warning("Error calling 'getStatusReportData()':  " +
                                 webSvcsManagerObj.getErrorMessageString());
    }
    return null;
  }

  /**
   * Method called when a connect-to-server login attempt was rejected.
   * A worker thread and a slight delay are used to give the 'connect'
   * thread a chance to terminate cleanly.
   * @param connStatusStr a message-string describing the status of
   * the current connection to the server.
   */
  protected void notifyConnLoginAttemptRejected(final String connStatusStr)
  {
    connAttemptRejectedFlag = true;         //indicate rejected
    if(connLoginRejectCallBackObj != null)
    {    //call-back object setup OK
      (new Thread("ConnLoginRejectCallBack")
          {                       //invoke call-back via worker thread
            public void run()     // and perform a short delay to allow
            {                     // the 'connect' thread to terminate
              try { sleep(250); }
              catch(InterruptedException ex) {}
              try
              {         //invoke call-back method:
                connLoginRejectCallBackObj.connLoginAttemptRejected(
                                                             connStatusStr);
              }
              catch(Exception ex)
              {    //some kind of exception error; log it
                logObj.warning("ConnLoginRejectCallBack:  " + ex);
                logObj.warning(UtilFns.getStackTraceString(ex));
              }
            }
          }).start();
    }
  }

  /**
   * Returns the "initialized" status of the connection.
   * @return true if the connection is initialized.
   */
  public boolean getInitializedFlag()
  {
    return (webSvcsManagerObj != null && webSvcsManagerObj.isConnected());
  }

  /**
   * Returns the connection status.  If 'invalidateConnection()' has been
   * called then this method will return false until the connection is
   * reinitialized.
   * @return true if the connection is active, false if not.
   */
  public boolean getConnectedFlag()
  {
    return !connInvalidatedFlag && connectedToServerFlag;
  }

  /**
   * Marks the connection as "invalidated", causing the
   * 'getConnectedFlag()' method to return 'false'.
   */
  public void invalidateConnection()
  {
    connInvalidatedFlag = true;
  }


  /**
   * Class WebServicesConnectThread defines a thread used to connect to
   * a QuakeWatch Web Services Server.
   */
  protected class WebServicesConnectThread extends IstiNotifyThread
  {
    private final String serverHostAddrStr;
    private final int serverHostPortNum;
    private String hostIDString;

    /**
     * Creates a thread used to connect to a QuakeWatch Web Services Server.
     * @param serverHostAddrStr the host address used to locate the server.
     * @param serverHostPortNum the port number used to locate the server.
     */
    public WebServicesConnectThread(String serverHostAddrStr,
                                                      int serverHostPortNum)
    {
      super("WebServicesConnectThread");        //set thread name
      this.serverHostAddrStr = (serverHostAddrStr != null) ?
                                            serverHostAddrStr.trim() : null;
      this.serverHostPortNum = serverHostPortNum;
      hostIDString = " (" +       //create host-ID string for use in msgs
                          serverHostAddrStr + ":" + serverHostPortNum + ")";
    }

    /**
     * Runs the connect thread.
     */
    public void run()
    {
      logObj.debug3("QWWebSvcConnector:  WebServicesConnectThread started");
      try
      {
        connAttemptStartedFlag = true;      //indicate thread started
        serverIdNameString = null;          //init server ID name string
        serverRepHostAddrString = null;     //init host address string
        aliveMessageRequestedFlag = false;  //clear indicator flag
        encryptDecryptUtilObj = null;       //clear any previous util object
        if(serverHostAddrStr != null && serverHostAddrStr.length() > 0)
        {     //server address string contains data
          if(webSvcsManagerObj != null)
          {   //web services manager OK
                   //attempt to initialize connection to server:
            final boolean connectFlag = connect();
            if(isTerminated())
            {   //thread is terminating
              logObj.debug2(
                       "QWWebSvcsConnector:  Connection thread terminated");
              connectDoneFlag = true;  //indicate connection attempt complete
              return;   //(thread object handle should already be null)
            }
            if(connectFlag)
            { //connected OK; show success message in connection status panel
              if(webSvcsManagerObj.getRedirectedEndpointFlag())
              {    //connection was redirected to new endpoint by server
                final URL urlObj;
                if((urlObj=webSvcsManagerObj.getEndpointUrlObj()) != null)
                {  //redirected endpoint URL fetched OK
                  final String redirHostStr = urlObj.getHost();
                  int redirPortNum = urlObj.getPort();
                  if(redirPortNum == -1)
                  {     //default port number; enter value if known protocol
                    final String protocolStr;
                    if("https".equals(protocolStr=urlObj.getProtocol()))
                      redirPortNum = 443;
                    else if("http".equals(protocolStr))
                      redirPortNum = 80;
                    else if("ftp".equals(protocolStr))
                      redirPortNum = 21;
                  }
                  if(serverHostAddrStr == null ||
                                  !serverHostAddrStr.equals(redirHostStr) ||
                                          serverHostPortNum != redirPortNum)
                  {     //different host address or port number for redir
                             //update host-ID string for use in msgs:
                    hostIDString = " (" + redirHostStr + ":" +
                                                         redirPortNum + ")";
                             //update host information values:
                    serverHostInfoObj.setValues(redirHostStr,redirPortNum);
                  }
                }
              }
              final String connectedIdStr = (serverIdNameString != null &&
                                          serverIdNameString.length() > 0) ?
                        (" \"" + serverIdNameString + "\"" + hostIDString) :
                                                               hostIDString;
              setConnPanelData("Connected","Connected to server" +
                             connectedIdStr + "; waiting for first message",
                                      ConnStatusInterface.YELLOW_COLOR_IDX);
              logObj.debug("Connected to server" +
                            connectedIdStr + "; waiting for first message");
                   //clear popup dialog (if not requested by user):
              autoClearConnStatusPopupDialog();
              connectedToServerFlag = true;        //indicate connected
              connInvalidatedFlag = false;         //clear "invalidated" flag
                     //launch message fetching thread:
              synchronized(msgFetchingThreadSyncObj)
              {      //grab thread-sync object for 'messageFetchingThreadObj'
                if(messageFetchingThreadObj != null)    //if previous thread
                  messageFetchingThreadObj.terminate(); //then terminate it
                               //create new message-fetching thread:
                messageFetchingThreadObj = new MessageFetchingThread();
                messageFetchingThreadObj.start();       //start thread
              }
            }
            else
            {      //connection attempt failed
              final String errStr;
              if(webSvcsManagerObj.getUnfetchedMessageFlag())
              {    //unfetched error msg available; enter into status panel
                setConnPanelInitError("Unable to connect:  " +
                        (errStr=webSvcsManagerObj.getErrorMessageString()));
              }
              else
                errStr = webSvcsManagerObj.getErrorMessageString();
              connAttemptFailedFlag = true;      //indicate failed
                   //fire notify of connect-login rejected (via call-back):
              if(webSvcsManagerObj.getConnInvalidLoginFlag())
                notifyConnLoginAttemptRejected(errStr);
            }
          }
          else
          {     //web services manager not initialized; set error message
            setConnPanelInitError("Unable to connect to server:  " +
                                         ((webSvcsInitErrorMsgStr != null &&
                               webSvcsInitErrorMsgStr.trim().length() > 0) ?
                                                    webSvcsInitErrorMsgStr :
                                "Error initializing web services manager"));
            connAttemptFailedFlag = true;        //indicate failed
          }
        }
        else
        {     //no data in locator string; set error message
          setConnPanelInitError("Server configuration data is missing");
          connAttemptFailedFlag = true;          //indicate failed
        }
      }
      catch(Throwable ex)
      {       //some kind of error; log it
        setConnPanelInitError("Unable to connect; exception error:  " + ex +
                         UtilFns.newline + UtilFns.getStackTraceString(ex));
        connAttemptFailedFlag = true;            //indicate failed
      }
      synchronized(webSvcsConnThreadSyncObj)
      {  //grab thread-sync object for 'webSvcsConnectThreadObj'
        if(webSvcsConnectThreadObj != null)      //if not yet cleared then
          webSvcsConnectThreadObj = null;        //release thread object
        connectDoneFlag = true;        //indicate connection attempt complete
      }
      logObj.debug3("QWWebSvcConnector:  WebServicesConnectThread finished");
    }

    /**
     * Connects to the server.
     * @return true if successful, false if error.
     */
    private boolean connect()
    {
         //set initial state for connection status panel:
      final String startStr = "Initializing connection";
      setConnPanelData(startStr,
                   (startStr + " to QW Web Services Server" + hostIDString),
                                         ConnStatusInterface.RED_COLOR_IDX);
      try
      {       //initialize connection:
        if(!webSvcsManagerObj.connect(serverHostAddrStr,serverHostPortNum,
                  logObj,connectionUserNameString,connectionPasswordString))
        {     //initialize failed
          return false;                //abort thread
        }
        if(isTerminated())        //if thread terminating then
          return false;           //abort method
        logObj.debug2(
                "QWWebSvcsConnector:  Successfully initialized connection");

              //fetch server ID name from server:
        if((serverIdNameString=webSvcsManagerObj.getServerIdNameStr()) ==
                                                                       null)
        {
          return false;
        }
        if(isTerminated())        //if thread terminating then
          return false;           //abort method

              //fetch host address from server:
        if((serverRepHostAddrString=webSvcsManagerObj.
                                            getServerHostAddrStr()) == null)
        {
          return false;
        }
        if(isTerminated())        //if thread terminating then
          return false;           //abort method

        if(acceptorRejectIDString != null)
        {     //an 'Acceptor' reject ID string is setup
          final String idStr;     //fetch Acceptor-ID string from server:
          if((idStr=webSvcsManagerObj.getAcceptorIDStr()) != null)
          {   //fetch 'Acceptor' reject ID string OK
            logObj.debug("QWWebSvcsConnector:  Fetched Acceptor-ID " +
                                                       "string:  " + idStr);
            if(acceptorRejectIDString.equals(idStr))
            {    //reject Acceptor-ID same as fetched ID string
                                //setup error message:
              setConnPanelInitError("Aborting connection to server" +
                            hostIDString + ":  " + acceptorRejectReasonStr);
                        //clear any existing message to make sure
                        // 'run()' method doesn't overwrite error:
              webSvcsManagerObj.clearErrorMessageString();
              return false;     //terminate connection attempt
            }
          }
          else
          {   //error fetching 'Acceptor' reject ID string
            logObj.warning(
                        "Error fetching Acceptor-ID string from server:  " +
                                 webSvcsManagerObj.getErrorMessageString());
          }
          if(isTerminated())      //if thread terminating then
            return false;         //abort method
        }

              //fetch server revision string from server:
        if((serverRevisionString=
                       webSvcsManagerObj.getServerRevisionString()) != null)
        {     //revision string fetched OK
          logObj.debug("QWWebSvcsConnector:  Fetched revision string " +
                          "from server:  \"" + serverRevisionString + "\"");
        }
        else
        {     //error fetching revision string
          logObj.warning("Error fetching revision string from server:  " +
                                 webSvcsManagerObj.getErrorMessageString());
        }
        if(isTerminated())        //if thread terminating then
          return false;           //abort method

              //fetch recommended polling interval from server:
        messagePollingIntervalMS = DEF_POLLINGITVL_MS;     //init default val
        String str;
        if((str=webSvcsManagerObj.getRecommendedPollingInterval()) != null)
        {     //recommended polling interval numeric string fetched OK
          try
          {        //convert numeric string to integer:
            messagePollingIntervalMS = Integer.parseInt(str);
            logObj.debug("QWWebSvcsConnector:  Fetched recommended " +
                                   "polling interval value from server:  " +
                                          messagePollingIntervalMS + " ms");
            if(messagePollingIntervalMS < 100)
            {      //value too small; log warning and revert to default
              logObj.warning("Invalid recommended polling " +
                                    "interval value fetched from server (" +
                    messagePollingIntervalMS + "); reverting to default (" +
                                                  DEF_POLLINGITVL_MS + ")");
              messagePollingIntervalMS = DEF_POLLINGITVL_MS;
            }
          }
          catch(NumberFormatException ex)
          {   //error converting numeric string to integer
            logObj.warning("Error parsing recommended polling " +
                    "interval value fetched from server (\"" + str + "\")");
          }
        }
        else
        {     //error fetching revision string
          logObj.warning("Error fetching recommended polling interval " +
                                                    "value from server:  " +
                                 webSvcsManagerObj.getErrorMessageString());
        }
        if(isTerminated())        //if thread terminating then
          return false;           //abort method

        if(altServersListMgr != null &&
                             !altServersListMgr.getKeepDefaultServersFlag())
        {     //alternate servers list manager OK and flag for
              // keeping only default alt-server entries not set
          String idsListStr;
                        //fetch list of alternate server IDs:
          if((idsListStr=webSvcsManagerObj.getAltServersIdsListStr()) != null)
          {   //list fetched OK
            logObj.debug("Received alternate server IDs list:  \"" +
                                                         idsListStr + "\"");
          }
          else
          {   //error fetching list of alternate server IDs
            logObj.warning("Error fetching list of alternate server " +
                                                      "IDs from server:  " +
                                 webSvcsManagerObj.getErrorMessageString());
          }
          if(isTerminated())      //if thread terminating then
            return false;         //abort method
          if(idsListStr != null && idsListStr.length() > 0)
          {   //non-empty list fetched OK
                             //save current contents of list:
            final String oldListStr = altServersListMgr.getEntriesListStr();
            if(!idsListStr.equals(oldListStr))
            {      //fetched alt-servers list different from current one
                             //enter fetched alt-servers list string:
              if(altServersListMgr.setEntriesListStr(idsListStr))
              {    //new list OK; indicate that list should be "committed"
                altServersListMgr.fireListCommit(this);
              }
              else
              {    //error setting list; log warning message
                logObj.warning("Error validating fetched list of " +
                                               "alternate server IDs:  " +
                               altServersListMgr.getErrorMessageString());
                             //put back old list entries:
                altServersListMgr.setEntriesListStr(oldListStr);
              }
            }
          }
        }
        if(isTerminated())        //if thread terminating then
          return false;           //abort method

                   //fetch certificate-file data from server:
        if((certificateFileDataArr=
                        webSvcsManagerObj.getCertificateFileData()) != null)
        {
          if(certificateFileDataArr.length > 0)
          {      //certificate data array not empty; log message
            logObj.debug("QWWebSvcsConnector:  Fetched certificate-file " +
                                               "data from server (length=" +
                                       certificateFileDataArr.length + ")");
          }
          else   //certificate data is a zero-length array
            certificateFileDataArr = null;     //clear handle to array
        }
        else
        {   //error fetching certificate-file data from server
          logObj.warning("Error fetching certificate-file data " +
              "from server:  " + webSvcsManagerObj.getErrorMessageString());
        }
        if(isTerminated())        //if thread terminating then
          return false;           //abort method

                   //test if get-status-report methods available on server:
                                       //clear any previous error:
        webSvcsManagerObj.clearErrorMessageString();
        webSvcsManagerObj.getStatusReportTime();
        if(webSvcsManagerObj.getErrorMessageFlag())
        {  //get-status-report method call resulted in an error
          statusReportDataAvailFlag = false;     //indicate not available
          logObj.debug("Error invoking get-status-report method " +
                "on server:  " + webSvcsManagerObj.getErrorMessageString());
        }
        else  //get-status-report method call succeeded
          statusReportDataAvailFlag = true;      //indicate available

        return true;
      }
      catch(Exception ex)
      {       //some kind of exception error occurred
        setConnPanelInitError("Error initializing connection:  " + ex);
        return false;             //abort thread
      }
    }
  }


  /**
   * Class WebServicesDisconnectThread defines a thread used to disconnect
   * from a QuakeWatch Web Services Server.
   */
  protected class WebServicesDisconnectThread extends IstiNotifyThread
  {
    /**
     * Creates a thread used to disconnect from a QuakeWatch Web Services
     * Server.
     */
    public WebServicesDisconnectThread()
    {
      super("WebServicesDisconnectThread");           //set thread name
    }

    /**
     * Runs the disconnect thread.
     */
    public void run()
    {
      logObj.debug3(
                 "QWWebSvcConnector:  WebServicesDisconnectThread started");
      try
      {
              //terminate message-fetching thread:
        synchronized(msgFetchingThreadSyncObj)
        {     //grab thread-sync object for 'messageFetchingThreadObj'
          if(messageFetchingThreadObj != null)        //if thread exists
            messageFetchingThreadObj.terminate();     //then terminate it
          messageFetchingThreadObj = null;       //clear thread handle
        }
              //disconnect from server:
        if(webSvcsManagerObj.isConnected())
        {     //server connection is active
                   //show status in panel (color to red):
          setConnPanelData("Disconnecting","Disconnecting from server",
                                         ConnStatusInterface.RED_COLOR_IDX);
          disconnectClientServices();       //notify server of disconnect
          setConnPanelData(null,"Shutting down connection",
                                         ConnStatusInterface.RED_COLOR_IDX);
          webSvcsManagerObj.disconnect();
          setConnPanelData("Disconnected","Connection shutdown complete",
                                         ConnStatusInterface.RED_COLOR_IDX);
          logObj.debug("QWWebSvcConnector:  Server connection " +
                                                  "shut down successfully");
        }
        else  //server connection not active
          logObj.debug("QWWebSvcConnector:  Not connected to server");
      }
      catch(Exception ex)
      {       //some kind of exception error occurred
        final String errStr = "Error during connection shutdown:  " + ex;
        setConnPanelData(null,errStr);
        logObj.info(errStr);
      }
      connectedToServerFlag = false;             //indicate not connected
      synchronized(webSvcsDisconnThreadSyncObj)
      {  //grab thread-sync object for 'webSvcsDisconnectThreadObj'
        if(webSvcsDisconnectThreadObj != null)   //if not yet cleared then
          webSvcsDisconnectThreadObj = null;     //release thread object
        disconnectDoneFlag = true;               //indicate disconnect done
      }
      logObj.debug3(
                "QWWebSvcConnector:  WebServicesDisconnectThread finished");
    }
  }


  /**
   * Class MessageFetchingThread defines a thread used to continuously
   * fetch messages from a QuakeWatch Web Services Server.  The thread
   * also simulates server-alive-message events.
   */
  protected class MessageFetchingThread extends IstiNotifyThread
  {
    private long nextAliveMsgTime = 0;
    private boolean fetchSuccessFlag = true;

    /**
     * Creates a message-fetching thread.
     */
    public MessageFetchingThread()
    {
      super("MessageFetchingThread");
    }

    /**
     * Runs the message-fetching thread.
     */
    public void run()
    {
      logObj.debug3("QWWebSvcConnector:  MessageFetchingThread started");
      try
      {       //wait for 'requestServerAliveMsg()' to be called:
        int waitCount = 0;
        do
        {
          if(!waitForNotify(100) || aliveMessageRequestedFlag)
            break;      //if interrupted or method called then exit loop
        }
        while(++waitCount < 30);       //wait for up to 3 seconds
        aliveMessageRequestedFlag = false;       //clear indicator flag
        waitForNotify(100);       //delay to let server-track thread setup
        long currentTimeVal, intvlVal;
        long nextMsgFetchTime = 0;
        boolean prevProcEnabledFlag = false, initialCheckinFiredFlag = false,
                checkInitialMsgNumDoneFlag = false;
                   //create "progress-indicator" object to be invoked by
                   // 'doFetchAndProcessMessagesFromServer()' method while
                   // messages are being downloaded, so that simulated
                   // "server-alive" messages are still generated during
                   // large, time-consuming fetches of data messages:
        final ProgressIndicatorInterface fetchProgressObj =
          new ProgressIndicatorInterface()
            {
              public void setValue(int val)
              {    //check if the time for sending simulated "server-alive"
                   // message reached and send it if so:
                checkSendAliveMsg(System.currentTimeMillis());
              }
              public void setMinimum(int val) {}
              public void setMaximum(int val) {}
              public int getMinimum() { return 0; }
              public int getMaximum() { return 0; }
              public int getValue() { return 0; }
              public void setOrientation(int newOrientation) {}
              public int getOrientation() { return 0; }
            };
        while(!isTerminated())
        {     //loop until thread is terminated
          if(msgHandlerObj != null)
          {   //message handler object OK
            currentTimeVal = System.currentTimeMillis();
                   //check if the time for sending a simulated
                   // "server-alive" message reached and send it if so:
            checkSendAliveMsg(currentTimeVal);
                   //if not yet done and connection has been "validated"
                   // then call 'connectionStatusChanged()' to get
                   // client check-in to be performed immediately:
            if(!initialCheckinFiredFlag &&
                                      msgHandlerObj.isConnectionValidated())
            {
              initialCheckinFiredFlag = true;
              msgHandlerObj.fireConnectionStatusChanged();
            }
            if(msgHandlerObj.isProcessingEnabled())
            {      //processing is enabled
              if(prevProcEnabledFlag)
              {    //processing was enabled previously
                if(currentTimeVal >= nextMsgFetchTime)
                {  //next message-fetch time reached
                  try
                  {          //fetch new messages from server (if any):
                    if(msgHandlerObj.doFetchAndProcessMessagesFromServer(
                                      false,fetchProgressObj,false) == null)
                    {   //message fetch successful
                      fetchSuccessFlag = true;   //indicate fetch succeeded
                                       //setup next message-fetch time:
                      nextMsgFetchTime = System.currentTimeMillis() +
                                                   messagePollingIntervalMS;
                    }
                    else
                    {   //error fetching messages
                      fetchSuccessFlag = false;  //indicate fetch failed
                             //setup message-fetch interval time to be
                             // 1/10 alive-message time so as to retry
                             // before alive-message time expires:
                      if((intvlVal=
                                maxServerAliveSecProp.intValue()*100L) <= 0)
                      {    //interval value not positive
                        intvlVal = 100;     //use 100ms minimum time
                      }
                                       //setup next message-fetch time:
                      nextMsgFetchTime = System.currentTimeMillis() +
                                                                   intvlVal;
                    }
                  }
                  catch(Exception ex)
                  {     //some kind of exception error
                    fetchSuccessFlag = false;    //indicate fetch failed
                                       //setup next message-fetch time:
                    nextMsgFetchTime = System.currentTimeMillis() +
                                                   messagePollingIntervalMS;
                    logObj.warning("Error fetching messages from server:  "
                                                                      + ex);
                    logObj.warning(UtilFns.getStackTraceString(ex));
                  }
                }
              }
              else
              {    //processing was not enabled previously
                if(!checkInitialMsgNumDoneFlag)
                {  //initial message-number check not yet done
                        //if message handler OK and last-received message
                        // number is zero then set last-received time value
                        // to current time so if completion of initial fetch
                        // was canceled by user only new messages will be
                        // fetched by this thread:
                  if(msgHandlerObj.isLastReceivedMsgNumZero())
                    msgHandlerObj.clearLastReceivedMsgNum(true);
                  checkInitialMsgNumDoneFlag = true;  //set "done" flag
                }
                        //setup next message-fetch time; if any previous
                        // fetches of messages from server have occurred
                        // then add polling interval; if not then
                        // setup to do message fetch immediately:
                nextMsgFetchTime = currentTimeVal +
                                                  ((nextMsgFetchTime == 0 &&
                       msgHandlerObj.getLastFetchMsgsFromServerTime() > 0) ?
                                              messagePollingIntervalMS : 0);
              }
              prevProcEnabledFlag = true;        //set tracking flag
            }
            else   //processing not enabled
              prevProcEnabledFlag = false;       //clear tracking flag
          }
          waitForNotify(100);     //delay 100ms between iterations
        }
      }
      catch(Throwable ex)
      {       //some kind of error; log it
        setConnPanelInitError("Exception error:  " + ex +
                         UtilFns.newline + UtilFns.getStackTraceString(ex));
      }
      logObj.debug3("QWWebSvcConnector:  MessageFetchingThread finished");
    }

    /**
     * Checks if the time for sending a simulated "server-alive" message
     * has been reached and sends it if so.
     * @param currentTimeVal current time, in ms.
     */
    private void checkSendAliveMsg(long currentTimeVal)
    {
      long aliveIntervalMS;
      if(currentTimeVal >= nextAliveMsgTime)
      {  //listener OK and next (or first) 'alive' msg time reached
        if(fetchSuccessFlag)
        {  //last message fetch was successful
                        //invoke registered alive-message listeners:
          msgHandlerObj.invokeAliveMsgListeners(null);
                   //calculate # of milliseconds before next alive
                   // msg as half of 'maxServerAliveSec' time:
          if((aliveIntervalMS=
                          maxServerAliveSecProp.intValue()*500L) <= 0)
          {  //interval value not positive
            aliveIntervalMS = 500;     //use 1/2 second minimum time
          }
          nextAliveMsgTime = currentTimeVal + aliveIntervalMS;
        }
        else      //last message fetch failed
          nextAliveMsgTime = 0;        //setup to send after next success
      }
    }
  }
}
