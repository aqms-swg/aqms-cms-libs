//QWWebSvcsInterface.java:  Defines methods for interacting with a
//                          QuakeWatch Web Services Server.
//
//   9/9/2005 -- [ET]  Initial version.
//  12/8/2005 -- [ET]  Added log file and optional username/password
//                     parameters to 'connect()' method; removed
//                     'setLogFileObj()' method; added methods
//                     'getConnInvalidLoginFlag()', 'getEndpointUrlObj()',
//                     'getRedirectedEndpointFlag()' and
//                     'getRedirectedEndpointUrlStr()'.
//   3/5/2008 -- [ET]  Added methods 'getStatusReportTime()' and
//                     'getStatusReportData()'.
//

package com.isti.quakewatch.util;

import java.net.URL;
import com.isti.util.LogFile;
import com.isti.util.ErrorMsgMgrIntf;

/**
 * Interface QWWebSvcsInterface defines methods for interacting with a
 * QuakeWatch Web Services Server.
 */
public interface QWWebSvcsInterface extends ErrorMsgMgrIntf
{
  /**
   * Establishes a connection to a QuakeWatch Web Services Server and
   * verifies that the server is reachable and running.
   * @param hostNameStr host name to use, or null for default value
   * ("localhost").
   * @param hostPortNum host port number to use, or 0 for default
   * value (8080).
   * @param logObj log file object to use, or null for none.
   * @param usernameStr username string to use, or null for none.
   * @param passwordStr password string to use, or null for none.
   * @return true if successful; false if not (in which case an error
   * message may be fetched via the 'getErrorMessageString()' method).
   */
  public boolean connect(String hostNameStr, int hostPortNum,
                    LogFile logObj, String usernameStr, String passwordStr);

  /**
   * Establishes a connection to a QuakeWatch Web Services Server and
   * verifies that the server is reachable and running.
   * @param hostNameStr host name to use, or null for default value
   * ("localhost").
   * @param hostPortNum host port number to use, or 0 for default
   * value (8080).
   * @param logObj log file object to use, or null for none.
   * @return true if successful; false if not (in which case an error
   * message may be fetched via the 'getErrorMessageString()' method).
   */
  public boolean connect(String hostNameStr, int hostPortNum,
                                                            LogFile logObj);

  /**
   * Closes the connection to the QuakeWatch Web Services Server.
   */
  public void disconnect();

  /**
   * Returns a flag indicating whether or not the connection is active.
   * @return true if connected; false is not.
   */
  public boolean isConnected();

  /**
   * Returns an identifier string that uniquely identifies the server.
   * This identifier is used by the QWRelayFeeder to determine
   * if the connected server is its own QWServer.
   * @return An identifier string that uniquely identifies the server.
   */
  public String getAcceptorIDStr();

  /**
   * Fetches the list of alternate web service server IDs (defined in the
   * QWServer's configuration file).
   * @return The list of alternate server IDs, as a string in
   * the form "hostAddr:portNum,hostAddr:portNum,...", or null if an
   * error occurred (in which case an error message may be fetched via
   * the 'getErrorMessageString()' method).
   */
  public String getAltServersIdsListStr();

  /**
   * Returns the recommended polling interval time for web-services clients,
   * in milliseconds.
   * @return The recommended polling interval time, in milliseconds, or null
   * if an error occurred (in which case an error message may be fetched via
   * the 'getErrorMessageString()' method).
   */
  public String getRecommendedPollingInterval();

  /**
   * Returns a string of information property values for the QWServer's
   * cache and the server itself.  The string will contain a set of
   * comma-separated entries in the form:  name="value".  The names
   * for the entries will come from the strings found in the
   * 'ServerCacheInfoStrings' class.
   * @return A string containing information property values, or null if an
   * error occurred (in which case an error message may be fetched via
   * the 'getErrorMessageString()' method).
   */
  public String getServerCacheSummary();

  /**
   * Fetches the server ID name string (defined in the QWServer's
   * configuration file).
   * @return The server ID name string, or null if an error occurred
   * (in which case an error message may be fetched via the
   * 'getErrorMessageString()' method).
   */
  public String getServerIdNameStr();

  /**
   * Fetches the server host address string for the QWServer.
   * @return The server host address string, or null if an error
   * occurred (in which case an error message may be fetched via
   * the 'getErrorMessageString()' method).
   */
  public String getServerHostAddrStr();

  /**
   * Returns the revision string for the QuakeWatch server.
   * @return The revision string for the QuakeWatch server, or null if an
   * error occurred (in which case an error message may be fetched via
   * the 'getErrorMessageString()' method).
   */
  public String getServerRevisionString();

  /**
   * Returns the web services version string.
   * @return the web services version string, or null if an error
   * occurred (in which case an error message may be fetched via
   * the 'getErrorMessageString()' method).
   */
  public String getWebServiceVersion();

  /**
   * Fetches the timestamp value for the latest status report from the
   * QWServer.
   * @return The timestamp value for the latest status report from the
   * QWServer, or 0 if no report is available or if an error occurred.
   */
  public long getStatusReportTime();

  /**
   * Fetches the latest status-report data from the QWServer.
   * @return A string containing the latest status-report data from the
   * QWServer, an empty string if no report is available, or null if an
   * error occurred (in which case an error message may be fetched via
   * the 'getErrorMessageString()' method).
   */
  public String getStatusReportData();

  /**
   * Fetches a byte-array of certificate-file data from the QWServer.
   * @return A byte-array of certificate-file data, a zero-length array
   * if no data is available, or null if an error occurred (in which
   * case an error message may be fetched via the 'getErrorMessageString()'
   * method).
   */
  public byte [] getCertificateFileData();

  /**
   * Determines the client-version status.  Clients should
   * call this method on a periodic basis while they are
   * connected.
   * @param clientInfoStr the client-connection-information
   * properties string to use.
   * @return A 'Boolean' object set to true if the an updated version of
   * the client is available, a 'Boolean' object set to false if not, or
   * null if an error occurred (in which case an error message may be
   * fetched via the 'getErrorMessageString()' method).
   */
  public Boolean clientStatusCheck(String clientInfoStr);

  /**
   * Returns information about available client-program upgrades.
   * @param clientInfoStr the client-connection-information
   * properties string to use.
   * @return An XML-formatted string containing information about
   * available client-program upgrades, an empty string if a clients
   * manager is not available, or null if an error occurred (in which
   * case an error message may be fetched via the 'getErrorMessageString()'
   * method).
   */
  public String getClientUpgradeInfo(String clientInfoStr);

  /**
   * Disconnects the client connection.
   * @param clientInfoStr the client-connection-information
   * properties string for the client.
   * @return A 'Boolean' object set to true if the client was removed
   * successfully; a 'Boolean' object set to false if no matching client
   * entry was found, or null if an error occurred (in which case an error
   * message may be fetched via the 'getErrorMessageString()' method).
   */
  public Boolean disconnectClient(String clientInfoStr);

  /**
   * Requests that messages newer or equal to the specified time value or
   * later than the specified message number be returned.  If a non-zero
   * message number is given and the given time value matches then
   * messages with message numbers greater than the given message number
   * are returned; otherwise messages newer or equal to the specified time
   * value are returned (within a tolerance value).  An XML message
   * containing the messages is built and returned, using the following
   * format: <pre>
   * [QWresend NumRequested="10"]
   *   [QWmessage ...] [/QWmessage]
   *   [QWmessage ...] [/QWmessage]
   *   ...
   * [/QWresend]
   * </pre> (Substitute angle brackets for square brackets.)
   * The number of messages that would be required to fill the request
   * is returned in the "NumRequested" attribute.  However, no more than
   * the oldest 'maxResendNumMsgs' number of messages will be returned
   * in any single call to this method (additional calls may be needed to
   * fetch all of the desired messages).
   * @param timeVal the time-generated value for message associated with
   * the given message number, or the requested time value to be used
   * (milliseconds since 1/1/1970).
   * @param msgNum the message number to use, or 0 or none.
   * @return An XML-formatted string containing the messages, an empty
   * string if a server-side error occurred, or 'null' if a client-side
   * error occurred (in which case an error message may be fetched via
   * the 'getErrorMessageString()'  method).
   */
  public String requestMessages(long timeVal, long msgNum);

  /**
   * Requests that messages newer or equal to the specified time value or
   * later than the specified message number be returned.  If a non-zero
   * message number is given and the given time value matches then
   * messages with message numbers greater than the given message number
   * are returned; otherwise messages newer or equal to the specified time
   * value are returned (within a tolerance value).  Only messages whose
   * event domain and type names match the given list of domain and type
   * names will be returned (unless the given list is an empty string).
   * An XML message containing the messages is built and returned, using
   * the following format: <pre>
   * [QWresend NumRequested="10"]
   *   [QWmessage ...] [/QWmessage]
   *   [QWmessage ...] [/QWmessage]
   *   ...
   * [/QWresend]
   * </pre> (Substitute angle brackets for square brackets.)
   * The number of messages that would be required to fill the request
   * is returned in the "NumRequested" attribute.  However, no more than
   * the oldest 'maxResendNumMsgs' number of messages will be returned
   * in any single call to this method (additional calls may be needed to
   * fetch all of the desired messages).
   * @param timeVal the time-generated value for message associated with
   * the given message number, or the requested time value to be used
   * (milliseconds since 1/1/1970).
   * @param msgNum the message number to use, or 0 or none.
   * @param domainTypeListStr a list string of event domain and
   * type names in the format "domain:type,domain:type...", where
   * occurrences of the ':' and ',' characters not meant as
   * separators may be "quoted" by preceding them with the
   * backslash ('\') character and list items missing the ':'
   * character will be considered to specify only a domain name
   * (the type name will be an empty string); or an empty string
   * for none.
   * @return An XML-formatted string containing the messages, an empty
   * string if a server-side error occurred, or 'null' if a client-side
   * error occurred (in which case an error message may be fetched via
   * the 'getErrorMessageString()'  method).
   */
  public String requestFilteredMessages(long timeVal,
                                     long msgNum, String domainTypeListStr);

  /**
   * Requests that messages corresponding to the given time value and
   * list of feeder-data-source host-name/message-number entries be
   * returned.  If a list of domain and type names is given then only
   * messages whose event domain and type names match the given list
   * will be returned.  An XML message containing the messages is built
   * and returned, using the following format: <pre>
   * [QWresend NumRequested="10"]
   *   [QWmessage ...] [/QWmessage]
   *   [QWmessage ...] [/QWmessage]
   *   ...
   * [/QWresend]
   * </pre> (Substitute angle brackets for square brackets.)
   * The number of messages that would be required to fill the request
   * is returned in the "NumRequested" attribute.  However, no more than
   * the oldest 'maxResendNumMsgs' number of messages will be returned
   * in any single call to this method (additional calls may be needed to
   * fetch all of the desired messages).
   * @param requestTimeVal the time-generated value for message associated
   * with the given message number, or the requested time value to be used
   * (milliseconds since 1/1/1970).
   * @param hostMsgNumListStr a list of feeder-data-source
   * host-name/message-number entries in the form:
   * "hostName"=msgNum,...
   * @param domainTypeListStr a list string of event domain and
   * type names in the format "domain:type,domain:type...", where
   * occurrences of the ':' and ',' characters not meant as
   * separators may be "quoted" by preceding them with the
   * backslash ('\') character and list items missing the ':'
   * character will be considered to specify only a domain name
   * (the type name will be an empty string); null or an empty string
   * for none.
   * @return An XML-formatted string containing the messages, an empty
   * string if a server-side error occurred, or 'null' if a client-side
   * error occurred (in which case an error message may be fetched via
   * the 'getErrorMessageString()'  method).
   */
  public String requestSourcedMsgsStr(long requestTimeVal,
                        String hostMsgNumListStr, String domainTypeListStr);


  /**
   * Determines if the last connect attempt was rejected due to invalid
   * login.
   * @return true if last connect attempt was rejected due to invalid
   * login; false if not.
   */
  public boolean getConnInvalidLoginFlag();

  /**
   * Returns the endpoint URL for the current connection.  The endpoint
   * will be different from the original one specified if the connection
   * has been redirected.
   * @return The URL object specifying the endpoint for the current
   * connection, or null if no connection is active.
   */
  public URL getEndpointUrlObj();

  /**
   * Returns an indicator of whether or not the current connection
   * was redirected (by the server) to a different endpoint.
   * @return true if the connection was redirected; false if not.
   */
  public boolean getRedirectedEndpointFlag();

  /**
   * Returns the URL string for the endpoint that the connection was
   * redirected to (by the server), if any.
   * @return The URL string for the endpoint that the connection was
   * redirected to (by the server), or null if the connection has not
   * been redirected.
   */
  public String getRedirectedEndpointUrlStr();

}
