//ResourceIdentifier.java:  Defines a resource identifier parsed from a
//                          URI string.
//
//  3/23/2010 -- [ET]  Initial version.
//

package com.isti.quakewatch.util;

/**
 * Class ResourceIdentifier defines a resource identifier parsed from a
 * URI string, as defined in the QuakeML specification.  The URI format is:
 *   smi:(authority-id)/(resource-id)[#(local-id)]
 * The "local-id" is subdivided into "typ_SS_code", where 'typ' is the
 * resource-type code (i.e., "evt", "prd", "mag"), 'SS' is the data-source
 * specified, and 'code' is an optional product code.  Examples:
 *   smi:local/00688138#evt_AT
 *   smi:local/00688138#prd_AT_Tsunami
 *   smi:local/00688138#mag_AT_Ml
 */
public class ResourceIdentifier
{
  protected final String sourceURIStr;      //given source URI
  protected final int authorityIDPos;       //position after 'authority-id'
  protected final int resourceIDPos;        //position after 'resource-id'
  protected final int resourceTypePos;      //position after 'typ'
  protected final int dataSourcePos;        //position after 'SS'
  protected final boolean productCodeFlag;  //true if 'code' present
    /** Schema name "smi" string. */
  public static final String SMI_PRE_STR = "smi:";
    /** Length of schema name "smi" string. */
  public static final int SMI_STR_LEN = SMI_PRE_STR.length();

  /**
   * Creates a resource identifier parsed from the given URI string.
   * @param sourceURIStr source URI string.
   */
  public ResourceIdentifier(String sourceURIStr)
  {
    this.sourceURIStr = sourceURIStr;
    if(sourceURIStr != null && sourceURIStr.startsWith(SMI_PRE_STR))
    {  //source string not null and starts with "smi:"
      int p;
      if((p=sourceURIStr.indexOf('/',SMI_STR_LEN)) > 0)
      {  //slash separator found
        authorityIDPos = p;       //save authority-ID position
        if((p=sourceURIStr.indexOf('#',authorityIDPos)) > 0)
        {  //stop character found
          resourceIDPos = p;      //save resource-ID position
          if((p=sourceURIStr.indexOf('_',resourceIDPos)) > 0)
          {  //first underscore separator found
            resourceTypePos = p;      //save resource-type position
            if((p=sourceURIStr.indexOf('_',resourceTypePos+1)) > 0)
            {  //second underscore separator found
              dataSourcePos = p;            //save data-source position
              productCodeFlag = true;       //product-code value available
            }
            else
            {  //second underscore separator not found; use rest of string
              dataSourcePos = sourceURIStr.length();
              productCodeFlag = false;        //no product-code value
            }
          }
          else
          {  //first underscore separator not found; use rest of string
            resourceTypePos = sourceURIStr.length();
            dataSourcePos = 0;              //no more ID strings
            productCodeFlag = false;        //no product-code value
          }
        }
        else
        {  //stop character not found; use rest of string
          resourceIDPos = sourceURIStr.length();
          resourceTypePos = dataSourcePos = 0;   //no more ID strings
          productCodeFlag = false;               //no product-code value
        }
      }
      else
      {  //slash separator not found; use rest of string
        authorityIDPos = sourceURIStr.length();
                             //no more ID strings
        resourceIDPos = resourceTypePos = dataSourcePos = 0;
        productCodeFlag = false;            //no product-code value
      }
    }
    else
    {  //source string is null; clear position variables
      authorityIDPos = resourceIDPos = resourceTypePos = dataSourcePos = 0;
      productCodeFlag = false;              //no product-code value
    }
  }

  /**
   * Returns the source URI string used to create this object.
   * @return The source URI string used to create this object.
   */
  public String getSourceURIStr()
  {
    return sourceURIStr;
  }

  /**
   * Returns the 'authority-id' string.
   * @return A new string containing the 'authority-id' value, or null
   * if not available.
   */
  public String getAuthorityIDStr()
  {
    return (authorityIDPos > 0) ?
                  sourceURIStr.substring(SMI_STR_LEN,authorityIDPos) : null;
  }

  /**
   * Returns the 'resource-id' string.
   * @return A new string containing the 'resource-id' value, or null
   * if not available.
   */
  public String getResourceIDStr()
  {
    return (authorityIDPos > 0 && resourceIDPos > authorityIDPos) ?
              sourceURIStr.substring(authorityIDPos+1,resourceIDPos) : null;
  }

  /**
   * Returns the 'local-id' string.
   * @return A new string containing the 'local-id' value, or null
   * if not available.
   */
  public String getLocalIDStr()
  {
                   //return source string contents after stop character:
    return (resourceIDPos > 0 && resourceTypePos > resourceIDPos) ?
                             sourceURIStr.substring(resourceIDPos+1) : null;
  }

  /**
   * Returns the 'resource-type' ('typ') string.
   * @return A new string containing the 'resource-type' value, or null
   * if not available.
   */
  public String getResourceTypeStr()
  {
    return (resourceIDPos > 0 && resourceTypePos > resourceIDPos) ?
             sourceURIStr.substring(resourceIDPos+1,resourceTypePos) : null;
  }

  /**
   * Returns the 'data-source' ('SS') string.
   * @return A new string containing the 'data-source' value, or null
   * if not available.
   */
  public String getDataSourceStr()
  {
    return (resourceTypePos > 0 && dataSourcePos > resourceTypePos) ?
             sourceURIStr.substring(resourceTypePos+1,dataSourcePos) : null;
  }

  /**
   * Returns the 'product-code' ('code') string.
   * @return A new string containing the 'product-code' value, or null
   * if not available.
   */
  public String getProductCodeStr()
  {
    return (productCodeFlag && dataSourcePos > 0 &&
                                    dataSourcePos < sourceURIStr.length()) ?
                             sourceURIStr.substring(dataSourcePos+1) : null;
  }


//  public static void main(String [] args)
//  {
//    final String [] uriStrsArr =
//        {
//          "smi:local/00688138#evt_AT",
//          "smi:local/00688138#prd_AT_Tsunami",
//          "smi:local/00688138#mag_AT_Ml",
//          "smi:ch.ethz.sed/magnitude/generic/surface_wave_magnitude"
//        };
//    ResourceIdentifier resIdObj;
//    for(int i=0; i<uriStrsArr.length; ++i)
//    {
//      resIdObj = new ResourceIdentifier(uriStrsArr[i]);
//      System.out.println();
//      System.out.println('\"' + uriStrsArr[i] + "\":");
//      System.out.println("  AuthorityID:  " + resIdObj.getAuthorityIDStr());
//      System.out.println("  ResourceID:  " + resIdObj.getResourceIDStr());
//      System.out.println("  LocalID:  " + resIdObj.getLocalIDStr());
//      System.out.println("  ResourceType:  " +
//                                             resIdObj.getResourceTypeStr());
//      System.out.println("  DataSource:  " + resIdObj.getDataSourceStr());
//      System.out.println("  ProductCode:  " + resIdObj.getProductCodeStr());
//    }
//  }
}
