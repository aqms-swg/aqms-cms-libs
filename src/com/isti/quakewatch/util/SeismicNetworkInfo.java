//SeismicNetworkInfo.java:  Holds message-building information for
//                          seismic networks.
//
//   6/4/2004 -- [ET]
//   9/3/2004 -- [KF]  Moved from QWEmailer to QWCommon project.
//  9/28/2004 -- [KF]  Added support for coordinates.
//  9/29/2004 -- [KF]  Changed to use upper case for 'dataSourceStr'.
//  5/18/2011 -- [ET]  Added methods 'getDataSourceNamesList()' and
//                     'getDataSourceNamesString()'.
//

package com.isti.quakewatch.util;

import java.util.List;
import java.util.Iterator;
import java.util.Vector;
import org.jdom.Element;
import com.isti.util.UtilFns;
import com.isti.util.LogFile;
import com.isti.util.IstiXmlUtils;
import com.isti.util.FifoHashtable;

/**
 * Class SeismicNetworkInfo holds message-building information for
 * seismic networks.
 */
public class SeismicNetworkInfo
{
    /** Field length for centered lines of text in headers. */
  public static final int CENTER_FIELD_LEN = 78;

    /** 2-letter data-source code. */
  public final String dataSourceStr;
    /** Top-of-message header string. */
  public final String messageHeaderStr;
    /** More-information link string. */
  public final String moreInfoLinkStr;
    /** Source-of-information string shown at bottom of message. */
  public final String sourceOfInfoStr;
    /** Source descriptor to be used with short-format messages. */
  public final String shortMsgDescStr;
    /** Coordinates array */
  public final Double[] coordinatesArray;

    /** Table of seismic-network-information blocks. */
  protected static final FifoHashtable networkInfoTable = new FifoHashtable();
  private static boolean tableLoadedFlag = false;
  private static SeismicNetworkInfo[] seismicNetworkInfo = null;

  /**
   * Creates an information block of message-building data for a
   * seismic network.
   * @param dataSourceStr 2-letter data-source code.
   * @param messageHeaderStr top-of-message header string.
   * @param moreInfoLinkStr more-information link string.
   * @param sourceOfInfoStr source-of-information string shown
   * at bottom of message.
   * @param shortMsgDescStr source descriptor to be used with
   * short-format messages.
   * @param coordinatesArray the array of coordinates.
   */
  public SeismicNetworkInfo(
      String dataSourceStr, String messageHeaderStr, String moreInfoLinkStr,
      String sourceOfInfoStr, String shortMsgDescStr,
      Double[] coordinatesArray)
  {
    this.dataSourceStr = dataSourceStr;
    this.messageHeaderStr = messageHeaderStr;
    this.moreInfoLinkStr = moreInfoLinkStr;
    this.sourceOfInfoStr = sourceOfInfoStr;
    this.shortMsgDescStr = shortMsgDescStr;
    this.coordinatesArray = coordinatesArray;
  }

  /**
   * Loads the seismic-network-information table from the given
   * "regions.xml" file.
   * @param regionsXmlFName the pathname for the "regions.xml" file to
   * use.
   * @param logObj the LogFile object to use for outputting warning
   * messages.
   * @return null if successful; an error-message string if not.
   */
  public static String loadNetworkRegionsXmlFile(String regionsXmlFName,
                                                             LogFile logObj)
  {
    synchronized (networkInfoTable)
    {
      if (tableLoadedFlag)  //exit if table is already loaded
        return null;

      try
      {
        final IstiXmlUtils istiXmlUtilsObj = new IstiXmlUtils();
        //load data from "regions.xml" file:
        if(!istiXmlUtilsObj.loadFile(regionsXmlFName,"regions"))
          return istiXmlUtilsObj.getErrorMessageString();
        //get "regions" element from data:
        final Element rootElemObj;
        if((rootElemObj=istiXmlUtilsObj.getRootElement()) == null)
          return "Unable to find 'regions' root element";
        //get list of "net" elements under "regions" element:
        final List netElementsList = rootElemObj.getChildren("net");
        if(netElementsList.size() <= 0)
          return "Unable to find any 'net' elements under 'regions'";
        final Iterator iterObj = netElementsList.iterator();
        Element netElemObj,infoElemObj,regionElemObj;
        String dataSourceStr,messageHeaderStr,moreInfoLinkStr,
        sourceOfInfoStr,shortMsgDescStr;
        while(iterObj.hasNext())
            {  //for each "net" element
          netElemObj = (Element)(iterObj.next());
          //get "code" attribute from "net" element:
          if((dataSourceStr=netElemObj.getAttributeValue("code")) == null)
          {
            logObj.warning("Unable to find 'code' attribute of 'net' element");
            continue;
          }
          //convert code to upper case and check if all whitespace:
          if((dataSourceStr=dataSourceStr.trim().toUpperCase()).length() <= 0)
          {
            logObj.warning("Found empty 'code' attribute of 'net' element");
            continue;
          }
          //get "MsgHeaderText" child element text:
          if((messageHeaderStr=netElemObj.getChildText("MsgHeaderText")) !=
             null)
          {
            //trim multi-line string, with centering:
            messageHeaderStr = UtilFns.trimMultiLineString(messageHeaderStr,
                CENTER_FIELD_LEN);
            //get "MsgMoreInfo" child element:
            if((infoElemObj=netElemObj.getChild("MsgMoreInfo")) == null)
            {
              logObj.warning("Unable to find 'MsgMoreInfo' element for '" +
                             dataSourceStr + "' element");
              continue;
            }
            //get "link" attribute from "MsgMoreInfo" child element:
            if((moreInfoLinkStr=infoElemObj.getAttributeValue("link")) == null)
            {
              logObj.warning("Unable to find 'link' attribute in " +
                             "'MsgMoreInfo' element for '" + dataSourceStr + "' element");
              continue;
            }
            //get "MsgSourceOfInfo" child element text:
            if((sourceOfInfoStr=netElemObj.getChildText("MsgSourceOfInfo")) ==
               null)
            {
              logObj.warning("Unable to find 'MsgSourceOfInfo' element for '" +
                             dataSourceStr + "' element");
              continue;
            }
            //trim multi-line string, without centering:
            sourceOfInfoStr = UtilFns.trimMultiLineString(sourceOfInfoStr);
            //get "MsgShortInfo" child element:
            if((infoElemObj=netElemObj.getChild("MsgShortInfo")) != null)
            {
              //get "desc" attribute from "MsgShortInfo" child element:
              if((shortMsgDescStr=infoElemObj.getAttributeValue("desc")) ==
                 null)
              {
                logObj.warning("Unable to find 'desc' attribute in " +
                               "'MsgShortInfo' element for '" +
                               dataSourceStr + "' element");
              }
            }
            else
            {   //unable to find "MsgShortInfo" child element
              shortMsgDescStr = null;
              logObj.warning("Unable to find 'MsgShortInfo' element for '" +
                             dataSourceStr + "' element");
            }
            logObj.debug2("Loaded seismic-network message info for '" +
                          dataSourceStr + "', header:" + UtilFns.newline +
                          messageHeaderStr);
            logObj.debug2("More-info link:  \"" + moreInfoLinkStr +
                          "\", source-of-info:" + UtilFns.newline +
                          sourceOfInfoStr);
            logObj.debug2("Short-msg source desc:  " +
                          ((shortMsgDescStr != null) ? ("\"" + shortMsgDescStr + "\"") :
                          "<none>"));

            //get coordinates
            final Vector coordinatesVec = new Vector();
            final List coordElemList;
            if ((regionElemObj = netElemObj.getChild("region")) == null)
            {
              logObj.warning("Unable to find 'region' element for '" +
                             dataSourceStr + "' element");
            }
            else if ((coordElemList = regionElemObj.getChildren("coordinate")
                      ).size() > 0)
            {
              final Iterator coordIterObj = coordElemList.iterator();
              Element coordElemObj;
              String latitude, longitude;
              Double dLat, dLon;

              while(coordIterObj.hasNext())  //for each coordinate
              {
                coordElemObj = (Element)(coordIterObj.next());
                latitude = coordElemObj.getAttributeValue("latitude");
                longitude = coordElemObj.getAttributeValue("longitude");
                if (latitude == null || longitude == null)
                {
                  break;
                }
                dLat = Double.valueOf(latitude);
                dLon = Double.valueOf(longitude);
                coordinatesVec.add(dLat);
                coordinatesVec.add(dLon);
              }
            }

            //create the coordinates array
            Double[] coordinatesArray = new Double[0];
            coordinatesArray = (Double[])coordinatesVec.toArray(coordinatesArray);

            //add entry to seismic-network-information table:
            addTableEntry(
                dataSourceStr,messageHeaderStr,moreInfoLinkStr,
                sourceOfInfoStr,shortMsgDescStr,coordinatesArray);
          }
          else
          {
            logObj.debug2("No 'MsgHeaderText' child found for '" +
                          dataSourceStr + "' element");
          }
        }
        tableLoadedFlag = true;  //the table is loaded
        return null;
      }
      catch(Exception ex)
      {
        return "Error loading regions XML file:  " + ex;
      }
    }
  }

  /**
   * Returns an array of the 'SeismicNetworkInfo' objects
   * from the seismic-network-information table.
   * @return An array of 'SeismicNetworkInfo' objects.
   */
  public static SeismicNetworkInfo[] getNetworkInfo()
  {
    synchronized (networkInfoTable)
    {
      //if the array was not created and the table was loaded
      if (seismicNetworkInfo == null && tableLoadedFlag)
      {
        //create the array
        seismicNetworkInfo = new SeismicNetworkInfo[0];
        seismicNetworkInfo = (SeismicNetworkInfo[])networkInfoTable.values(
            ).toArray(seismicNetworkInfo);
      }
      return seismicNetworkInfo;
    }
  }

  /**
   * Returns the 'SeismicNetworkInfo' object for the given data-source code
   * from the seismic-network-information table.
   * @param dataSourceStr 2-letter data-source code.
   * @return The 'SeismicNetworkInfo' object for the given data-source code,
   * or null if no match was found.
   */
  public static SeismicNetworkInfo getNetworkInfo(String dataSourceStr)
  {
    if(dataSourceStr != null)
    {    //data-source code given
      Object obj;            //convert to upper case and look up:
      if((obj=networkInfoTable.get(dataSourceStr.toUpperCase())) instanceof
                                                           SeismicNetworkInfo)
      {  //match found
        return (SeismicNetworkInfo)obj;
      }
    }
    return null;
  }

  /**
   * Returns a list of seismic-network names from the current table.
   * @return A list of strings, or an empty list if no names are available.
   */
  public static List getDataSourceNamesList()
  {
    final Vector retList = new Vector();
    final SeismicNetworkInfo [] infoArr;
    if((infoArr=getNetworkInfo()) != null && infoArr.length > 0)
    {  //array contains items; add each data-source string to list
      for(int i=0; i<infoArr.length; ++i)
        retList.add(infoArr[i].dataSourceStr);
    }
    return retList;
  }

  /**
   * Returns a comma-separated list string of seismic-network names from
   * the current table.
   * @return A comma-separated list string, or an empty string if no names
   * are available.
   */
  public static String getDataSourceNamesString()
  {
    return UtilFns.listObjToListStr(getDataSourceNamesList(),", ");
  }

  /**
   * Adds an entry to the seismic-network-information table.
   * @param dataSourceStr 2-letter data-source code.
   * @param messageHeaderStr top-of-message header string.
   * @param moreInfoLinkStr more-information link string.
   * @param sourceOfInfoStr source-of-information string shown
   * at bottom of message.
   * @param shortMsgDescStr source descriptor to be used with
   * short-format messages.
   * @param coordinatesArray the array of coordinates or null if not available.
   */
  protected static void addTableEntry(
      String dataSourceStr, String messageHeaderStr, String moreInfoLinkStr,
      String sourceOfInfoStr, String shortMsgDescStr,
      Double[] coordinatesArray)
  {
    networkInfoTable.put(dataSourceStr, new SeismicNetworkInfo(
        dataSourceStr,messageHeaderStr,moreInfoLinkStr,
        sourceOfInfoStr,shortMsgDescStr,coordinatesArray));
  }
}
