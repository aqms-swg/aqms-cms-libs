//ServerTrackingThread.java:  Defines a thread that tracks the status of
//                            the QuakeWatch server.
//
//  2/18/2003 -- [ET]  Initial version.
//   6/2/2003 -- [ET]  Improved reconnect strategy (will no longer give
//                     up); implemented client request of immediate
//                     'Alive' message from server; connection status
//                     messages now show server address info.
//  6/12/2003 -- [ET]  Improved handling of alive/change flags; added
//                     server ID to connection status panel text.
//  6/13/2003 -- [ET]  Added 250ms delay between event channel ready and
//                     call to 'requestServerAliveMsg()'.
//  6/25/2003 -- [ET]  If reconnected after being disconnected for more
//                     than an hour then a repeat of the initial
//                     resend-request is requested.
//   7/1/2003 -- [ET]  Changed "logObj.debug3()" outs to "logObj.debug5()".
//  7/16/2003 -- [ET]  Moved 'MS_PER_HOUR' to "UtilFns".
//  8/12/2003 -- [ET]  Added support for alternate servers.
//  8/13/2003 -- [ET]  Added 'userInitFlag' parameter to
//                     'connReinitialized()' method.
// 10/29/2003 -- [ET]  Moved from 'QWClient' to 'QWCommon' project;
//                     modified to work outside of 'QWClient' project.
// 11/13/2003 -- [ET]  Updated call to 'initialRequestMessagesFromServer()'
//                     (no flag parameter).
//   1/6/2004 -- [ET]  Fixed to prevent exception when server name not
//                     available; modified to log exceptions in 'run()'
//                     method.
//  1/21/2004 -- [ET]  Fixed to react properly to the 'connTimeoutSec'
//                     parameter being set to zero (never timeout);
//                     modified log level of two missed-alive-messages
//                     outputs from "warning" to "debug".
//  1/28/2004 -- [ET]  Fixed problem where first alternate-server-connect
//                     attempt would fail because done-flag was checked
//                     before attempt began (problem introduced via
//                     'QWCorbaManager' changes on 1/16/2004).
//   2/2/2004 -- [ET]  Added extra debug-log outputs.
//  3/29/2004 -- [ET]  Changed  'getConnectDoneFlag()' reference to
//                     'getConnAttemptFailedFlag()' to fix issue where
//                     manual reconnect would always fall-over to a
//                     server on the alternate-servers list.
//  3/31/2004 -- [ET]  Modified to call 'addHdlrAliveMsgListener()'
//                     instead of 'setHdlrAliveMsgListener()' and added
//                     parameter to 'aliveMsgReceived()' method.
//   8/4/2004 -- [ET]  Modified to save connection config props (if changed)
//                     to config file after connection verified.
// 10/22/2004 -- [ET]  Modified to call QWConnectionManager method
//                     'fileConnectAttemptFailedCallBack()'.
//  11/3/2004 -- [ET]  Fixed name on QWConnectionManager method
//                     'fireConnectAttemptFailedCallBack()'.
//  1/14/2005 -- [ET]  Modified constructor to initialize alternate-servers
//                     list tracker ('altServersListTracker').
//  1/27/2005 -- [ET]  Modified to supply 'serverChangedFlag' parameter to
//                     'initialRequestMessagesFromServer()' method.
//  9/15/2005 -- [ET]  Modified to use 'QWAbstractConnector' instead of
//                     'QWCorbaManager'; modified 'terminate()' method to
//                     remove object from list of alive-message listeners.
//  3/15/2021 -- [KF]  Replaced ConnectAttemptFailedCallBack with
//                     ConnectAttemptFinishedCallBack.
//  3/16/2021 -- [KF]  Modified to invoke connect-attempted-finished
//                     call-back method with no failure when connected
//

package com.isti.quakewatch.util;

import java.util.Random;
import com.isti.util.UtilFns;
import com.isti.util.LogFile;
import com.isti.util.DataChangedListener;
import com.isti.quakewatch.message.QWStatusMsgRecord;

/**
 * Class ServerTrackingThread defines a thread that tracks the status of
 * the QuakeWatch server.
 */
public class ServerTrackingThread extends Thread
                                               implements QWAliveMsgListener
{
  private final QWConnectionMgr connMgrObj;      //connection-manager object
  private final QWConnProperties cfgObj;         //connection properties obj
  private QWAbstractConnector qwConnectorObj;    //QW connector object
  private final LogFile logObj;                  //log file object
  private String serverHostAddrTracker = "";     //server host addr tracker
  private int serverPortNumTracker = 0;          //server port # tracker
  private String altServersListTracker = "";     //alt-servers list tracker
         //flag set true while attempting connection to alternate server:
  private boolean attemptingAltServerFlag = false;
         //flag set true to delay increase in retry-wait time:
  private boolean delayRetryWaitIncFlag = false;
  private boolean aliveMsgFlag = false;     //true after alive msg received
  private boolean connReinitFlag = false;   //true after connection redone
  private boolean connReinitUserInitFlag = false;
  private boolean terminateFlag = false;    //true if thread terminating
  private boolean aliveMsgsRecvdFlag = false;    //true if any alive msgs
         //flag set true if 'maxServerAliveSec' property changed:
  private boolean maxAliveTimeChgFlag = false;
  private long connectionCheckTime = 0;     //"check" time for connection
  private Random randomObj = null;

  /**
   * Creates the thread that tracks the status of the QuakeWatch server.
   * @param connMgrObj the connection-manager object to use.
   */
  public ServerTrackingThread(QWConnectionMgr connMgrObj)
  {
    super("ServerTrackingThread");          //set thread name
    this.connMgrObj = connMgrObj;
    cfgObj = connMgrObj.getConnProperties();
    this.logObj = connMgrObj.getLogFileObj();    //setup log-file object
              //get QW connector object from connection manager:
    qwConnectorObj = connMgrObj.getQWConnectorObj();
              //init trackers for server host addr, port # and alt servers:
    serverHostAddrTracker = cfgObj.serverHostAddressProp.stringValue();
    serverPortNumTracker = cfgObj.serverPortNumberProp.intValue();
    altServersListTracker = cfgObj.alternateServersListProp.stringValue();
              //setup to have this object receive server-alive messages:
    connMgrObj.addHdlrAliveMsgListener(this);
  }

  /** Runs this thread. */
  public void run()
  {
    logObj.debug3("ServerTrackingThread:  Started");
    try
    {
         //add listener for when the 'maxServerAliveSec' property has
         // changed to interrupt a 'sleep()' that may be in progress
         // with an old (possibly large) value:
      cfgObj.maxServerAliveSecProp.addDataChangedListener(
        new DataChangedListener()
          {
            public void dataChanged(Object obj)
            {
              maxServerAliveSecChanged();
            }
          });
      int missedCount;    //number of alive messages missed in a row
      int maxServerAliveSec,connTimeoutSec,maxConnRetryWaitSec,offsetVal;
      long startCheckTimeVal,retryWaitMillisecs;
      boolean anyConnSuccessFlag = false;   //true after event ch connected
      boolean showedSuccessFlag,connAttFailedFlag,retryTimeoutFlag;
      int attemptCount = 0, retryWaitSecs = 0;
      outerLoop:          //label for named 'break' statements below
      while(!terminateFlag)
      {    //loop while event-channel-connected status changing
        if(connReinitUserInitFlag)
        {  //connection-reinit-via-user-input flag is set
          logObj.debug4("ServerTrackingThread:  " +
                                 "Connection reinitialized via user input");
          retryWaitSecs = 0;                  //reset retry-connect delay val
          connReinitUserInitFlag = false;     //reset flag
        }
                //clear alive-messages-received and connection-reinit flags:
        aliveMsgsRecvdFlag = connReinitFlag = false;
        if(qwConnectorObj.getConnectedFlag())
        {  //connection active; save connection-start time
          connectionCheckTime = System.currentTimeMillis();
          anyConnSuccessFlag = true;        //indicate event ch connected
          showedSuccessFlag = false;        //reset showed-success flag
          attemptCount = 0;    //init consecutive connect attempt count
          missedCount = 0;     //init # of alive messages missed in a row
                               //get server-ID string:
          final String serverIDStr = qwConnectorObj.getServerIDString();
                               //create "basic" server-ID display string:
          final String srv1Str = (serverIDStr.length() > 0) ?
                                                (" to " + serverIDStr) : "";
                               //get "detailed" server-ID string:
          final String detServerIDStr =
                                 qwConnectorObj.getDetailedServerIDString();
                               //get length of "detailed" server-ID string:
          final int len = (serverIDStr != null) ? serverIDStr.length() : 0;
                               //create "detailed" server-ID display strings:
          final String srv2Str = (len > 0) ? (":  " + detServerIDStr) : "";
          final String srv3Str = (len > 0) ? (" " + detServerIDStr + "") : "";
          try { sleep(250); }  //delay to help let connection get setup
          catch(InterruptedException ex) {}
          logObj.debug5("ServerTrackingThread:  Starting alive-check loop");
          aliveMsgFlag = false;          //clear alive-msg-received flag
          interrupted();                 //make sure interrupted flag clear
          if(terminateFlag)         //if thread terminating then
            break;                  //exit thread
          // invoke connect-attempted-finished call-back method with no failure
          connMgrObj.fireConnectAttemptFinishedCallBack(false, false, false);
          if(terminateFlag)         //if thread terminating then
            break;                  //exit thread
                //request 'Alive' message from server (to speed-up verify):
          qwConnectorObj.requestServerAliveMsg();
          while(true)
          {     //loop while checking for server-alive messages
            try
            {        //wait until interrupted or timeout:
              while(!terminateFlag)
              {      //loop until intterupted or timeout
                if((maxServerAliveSec=
                               cfgObj.maxServerAliveSecProp.intValue()) > 0)
                {    //maximum server alive time is not zero
                  sleep(maxServerAliveSec*1000);   //delay up to time value
                  logObj.debug5(
                    "ServerTrackingThread:  Alive message timeout reached");
                  break;            //exit wait loop
                }
                else //maximum server alive time is zero
                  sleep(1000);      //delay between rechecks of value
              }
            }
            catch(InterruptedException ex)
            {
              logObj.debug5(
                     "ServerTrackingThread:  Alive-check wait interrupted");
            }
            if(terminateFlag)      //if thread terminating then
              break outerLoop;     //exit thread
            if(connReinitFlag)
            {   //connection reinitializing
              logObj.debug5("ServerTrackingThread:  Flag set for " +
                                                 "connection reinitializing");
              break;               //exit inner loop
            }
            if(aliveMsgFlag)
            {   //server-alive message received
                          //save time that server-alive message received:
              connectionCheckTime = System.currentTimeMillis();
              aliveMsgFlag = false;           //clear alive-msg-received flag
              aliveMsgsRecvdFlag = true;      //indicate alive msgs received
              logObj.debug5("ServerTrackingThread:  Alive message received");
              if(!showedSuccessFlag)
              {      //this is the first alive message received
                qwConnectorObj.setConnPanelData(("Connected" + srv1Str),
                                            ("Connected to server" + srv2Str),
                                         ConnStatusInterface.GREEN_COLOR_IDX);
                logObj.info("Connection verified to server" + srv2Str);
                showedSuccessFlag = true;     //indicate first msg received
                missedCount = 0;              //reset missed count
                retryWaitSecs = 0;            //reset retry-connect delay value
                attemptingAltServerFlag = false;   //clear alt-server flag
                        //set flag if connected server has changed:
                final boolean serverChangedFlag =
                                             (!serverHostAddrTracker.equals(
                              cfgObj.serverHostAddressProp.stringValue()) ||
                                                     serverPortNumTracker !=
                                    cfgObj.serverPortNumberProp.intValue());
                if(serverChangedFlag || !altServersListTracker.equals(
                             cfgObj.alternateServersListProp.stringValue()))
                {  //server host addr/portNum or alt-server list changed
                        //invoke save-to-config-file call-back:
                  connMgrObj.fireSaveToConfigFileCallBack();
                        //update trackers for server host address & port #:
                  serverHostAddrTracker =
                                 cfgObj.serverHostAddressProp.stringValue();
                  serverPortNumTracker =
                                     cfgObj.serverPortNumberProp.intValue();
                        //update tracker for alternate-servers list:
                  altServersListTracker =
                              cfgObj.alternateServersListProp.stringValue();
                }
                          //do initial resend-request of messages and
                          // enable msg-processing in the msg manager:
                connMgrObj.initialRequestMessagesFromServer(
                                                         serverChangedFlag);
              }
              else if(missedCount > 0)
              {     //one or more server-alive messages missed
                qwConnectorObj.setConnPanelData(("Connected" + srv1Str),
                           ("Connection reestablished to server" + srv2Str),
                                       ConnStatusInterface.GREEN_COLOR_IDX);
                logObj.info("Connection to server " + srv3Str +
                                     " reestablished after " + missedCount +
                     " missed 'Alive' message" + ((missedCount!=1)?"s":""));
                missedCount = 0;         //reset missed count
              }
            }
            else if(maxAliveTimeChgFlag)
            {   //'maxAliveTimeSec' property changing
                          //clear 'maxAliveTimeSec' change flag:
              maxAliveTimeChgFlag = false;
              logObj.debug5("ServerTrackingThread:  'maxAliveTimeSec' " +
                                                       "change registered");
              continue;                  //restart loop
            }
            else
            {  //server-alive message not received
              logObj.debug5(
                       "ServerTrackingThread:  Alive message not received");
              ++missedCount;             //increment missed count
                          //check if too much time without alive messages:
              if((connTimeoutSec=
                               cfgObj.getConnTimeoutSec()) <= 0 ||
                            System.currentTimeMillis()-connectionCheckTime <
                                                        connTimeoutSec*1000)
              {      //no connection-retry timeout or timeout not reached
                if(aliveMsgsRecvdFlag)
                {    //previous alive messages were received
                  qwConnectorObj.setConnPanelData(
                                        "Lost connection; click for detail",
                                                       ("Lost connection;" +
                         " waiting for next message from server" + srv2Str),
                                      ConnStatusInterface.YELLOW_COLOR_IDX);
                  logObj.info("Missed 'alive' message (" +
                                   missedCount + ") from server" + srv2Str);
                }
                else
                {    //no previous alive messages received
                  qwConnectorObj.setConnPanelData(
                                          "Not receiving; click for detail",
                             ("Not receiving 'alive' messages from server" +
                                srv2Str),ConnStatusInterface.RED_COLOR_IDX);
                  logObj.warning(
                              "No 'alive' messages received from server (" +
                                               missedCount + ")" + srv2Str);
                }
              }
              else
              {   //connection timeout reached
                if(aliveMsgsRecvdFlag)
                {    //previous alive messages were received
                  qwConnectorObj.setConnPanelData(
                                           "Disconnected; click for detail",
                                ("Lost connection; reconnecting to server" +
                                srv2Str),ConnStatusInterface.RED_COLOR_IDX);
                  logObj.info("Not receiving 'alive' messages (" +
                             missedCount + "); reinitializing connection " +
                                                     "to server" + srv2Str);
                }
                else
                {    //no previous alive messages received
                  qwConnectorObj.setConnPanelData(
                                           "Disconnected; click for detail",
                               ("Unable to establish connection to server" +
                                srv2Str),ConnStatusInterface.RED_COLOR_IDX);
                  logObj.warning("No 'alive' messages received (" +
                             missedCount + "); reinitializing connection " +
                                                     "to server" + srv2Str);
                }
                     //reinitialize connection (don't force popup,
                     // don't use separate thread):
                connMgrObj.reinitConnection(false,false,false);
                break;            //exit inner loop
              }
            }
            interrupted();               //make sure interrupted flag clear
          }
        }
        else
        {  //event channel not connected
          logObj.debug5(
                   "ServerTrackingThread:  Starting wait-for-connect loop");

          if((connTimeoutSec=cfgObj.getConnTimeoutSec()) > 0)
          {   //connection-retry timeout is non-zero
            startCheckTimeVal = System.currentTimeMillis();
                  //if random number generator not yet created then
                  // create one now, using the current time as a seed:
            if(randomObj == null)
              randomObj = new Random(startCheckTimeVal);
             //setup maximum length of time to wait for server connect:
            if(retryWaitSecs <= 0)
            {     //wait time was reset
                       //setup initial wait equal to server timeout value:
              retryWaitSecs = connTimeoutSec;
              logObj.debug4("ServerTrackingThread:  " +
                       "Setting initial retryWaitSecs to " + retryWaitSecs);
                       //init alt-server flag to true if alt-servers avail
                       // to allow fast switch-over to alternate server
                       // (also setup flag to delay increase in retry-wait
                       //  time by one iteration if alt-servers available
                       //  to allow initial retry-wait time value to be used
                       //  once before any increase):
              delayRetryWaitIncFlag = attemptingAltServerFlag =
                                  connMgrObj.getAlternateServersAvailFlag();
            }
            else if(!attemptingAltServerFlag)
            {     //wait time was not reset and not attempting alternate server
              if(!delayRetryWaitIncFlag)
              {   //not delaying increase in retry-wait time
                            //get max-wait-time property:
                maxConnRetryWaitSec =            // (convert to seconds)
                         cfgObj.maxConnRetryWaitMinutesProp.intValue() * 60;
                if(retryWaitSecs < maxConnRetryWaitSec)
                {   //wait time currently below maximum
                         //double the wait time, up to maximum value:
                  if((retryWaitSecs*=2) > maxConnRetryWaitSec)
                    retryWaitSecs = maxConnRetryWaitSec;
                  logObj.debug4("ServerTrackingThread:  " +
                        "Doubled retryWaitSecs (to " + retryWaitSecs + ")");
                }
                else
                {
                  logObj.debug4("ServerTrackingThread:  " +
                       "retryWaitSecs not doubled; not smaller than max (" +
                                             maxConnRetryWaitSec + " sec)");
                }
              }
              else     //delaying increase in retry-wait time
              {
                delayRetryWaitIncFlag = false;       //clear flag
                logObj.debug4("ServerTrackingThread:  " +
                               "retryWaitSecs not doubled; delay flag set");
              }
            }
            else
            {
              logObj.debug4("ServerTrackingThread:  " +
                  "retryWaitSecs not doubled; attemptingAltServerFlag set");
            }
            retryWaitMillisecs = (long)retryWaitSecs * 1000;  //convert to ms
                       //setup an "offset" time value to be added to the
                       // wait, with a bit of randomization so that all
                       // clients are not trying to reconnect at once;
                       // start with 10% of value (2 second minimum):
            if((offsetVal=(int)(retryWaitMillisecs/10)) < 2000)
              offsetVal = 2000;
                     //add random value (0 to offset) to wait:
            retryWaitMillisecs += randomObj.nextInt(offsetVal);
            logObj.debug3("ServerTrackingThread:  Waiting for up to " +
                                          (double)retryWaitMillisecs/1000.0 +
                                        " seconds before reconnect attempt");
          }
          else
          {   //no connection-retry timeout
            startCheckTimeVal = 0;
            retryWaitMillisecs = 0;    //setup to wait indefinitely
            logObj.debug3("ServerTrackingThread:  " +
                           "Waiting indefinitely before reconnect attempt");
          }
          while(true)
          {     //loop while waiting for connection or retry timeout
            try { sleep(1000); }    //delay between checks
            catch(InterruptedException ex) {}
            if(terminateFlag)       //if thread terminating then
              break outerLoop;      //exit thread
                   //reload QW connector obj from conMgr (in case changed):
            qwConnectorObj = connMgrObj.getQWConnectorObj();
            if(connReinitFlag)
            {   //connection is being reinitialized
              logObj.debug5(
                      "ServerTrackingThread:  Connection reinit signalled");
              break;         //exit inner loop
            }
            if(qwConnectorObj.getConnectedFlag())
            {   //connection is active
              logObj.debug5("ServerTrackingThread:  Connection active");
              break;         //exit inner loop
            }
            if(qwConnectorObj.getConnAttemptRejectedFlag())
            {   //connect attempt rejected
              logObj.debug5(
                         "ServerTrackingThread:  Connect attempt rejected");
              continue;      //go back to top of loop
            }
                   //set flag if connect-attempt failed:
            if((connAttFailedFlag=qwConnectorObj.getConnAttemptFailedFlag())
                                                 && attemptingAltServerFlag)
            {      //connect-attempt failed and attempting alternate server
              retryTimeoutFlag = true;      //set flag to skip delay
            }
            else if(retryTimeoutFlag = (retryWaitMillisecs > 0 &&
                             System.currentTimeMillis()-startCheckTimeVal >=
                                                        retryWaitMillisecs))
            {      //enough time has elapsed for retry attempt
              connAttFailedFlag = true;     //set flag for connect failed
            }
            if(connAttFailedFlag &&
                             connMgrObj.isConnectAttemptFinishedCallBackSet())
            {      //connect-attempt failed and call-back object is set
                        //invoke call-back method (second flag true if
                        // no alternate servers or next server to be
                        // attempted is same as "original" server):
              connMgrObj.fireConnectAttemptFinishedCallBack(
                  true,
                  (!connMgrObj.getAlternateServersAvailFlag() ||
                      connMgrObj.equalsFirstAlternateServer(
                          serverHostAddrTracker,serverPortNumTracker)),
                  anyConnSuccessFlag);
                        //delay to let 'connReinitialized()' be called:
              try { sleep(1000); }
              catch(InterruptedException ex) {}
              if(terminateFlag)        //if thread terminating then
                break outerLoop;       //exit thread
                        //if re-initd or conn active then exit inner loop:
              if(connReinitFlag || qwConnectorObj.getConnectedFlag())
                break;
              if(qwConnectorObj.getConnAttemptRejectedFlag())
                continue;    //if connect rejected, go back to top of loop
              if(!retryTimeoutFlag)
              {    //retry timeout not previously reached
                        //re-check timeout after dialog displayed
                retryTimeoutFlag = (retryWaitMillisecs > 0 &&
                             System.currentTimeMillis()-startCheckTimeVal >=
                                                        retryWaitMillisecs);
              }
            }
            if(retryTimeoutFlag)
            {   //connecting to alternate server and connect attempt complete
                // (and failed) or enough time has elapsed for retry attempt
                                       //clear connect-failed flag:
              qwConnectorObj.clearConnAttemptFailedFlag();
                                       //clear connect-rejected flag:
              qwConnectorObj.clearConnAttemptRejectedFlag();
              ++attemptCount;          //increment number of attempts
              logObj.debug3("ServerTrackingThread:  Reinitializing " +
                            "connection (# attempts = " + attemptCount + ")");
                   //clear connection-attempt-started flag:
              qwConnectorObj.clearConnAttemptStartedFlag();
                   //reinitialize connection using a server
                   // from the alternate-servers list (if available),
                   // don't use separate thread; set flag if alternate-
                   // server entry was found and attempted:
              attemptingAltServerFlag =
                                   connMgrObj.reinitConnWithAlternateServer(
                          serverHostAddrTracker,serverPortNumTracker,false);
              logObj.debug3("ServerTrackingThread:  Connecting to host \"" +
                  cfgObj.serverHostAddressProp.stringValue() + "\", port " +
                                    cfgObj.serverPortNumberProp.intValue());
              logObj.debug3("ServerTrackingThread:  " +
                        "serverHostAddrTracker=\"" + serverHostAddrTracker +
                        "\", serverPortNumTracker=" + serverPortNumTracker +
                    ", attemptingAltServerFlag=" + attemptingAltServerFlag);
                   //wait for connection attempt to begin (up to timeout):
              int count = 8000 / 250;       //setup eight-second timeout
              while(!qwConnectorObj.getConnAttemptStartedFlag() &&
                                                                --count > 0)
              {
                UtilFns.sleep(250);
              }
              break;      //exit inner loop
            }
          }
        }
      }
    }
    catch(Exception ex)
    {         //some kind of exception error; log it
      logObj.warning("ServerTrackingThread error:  " + ex);
      logObj.warning(UtilFns.getStackTraceString(ex));
    }
    logObj.debug3("ServerTrackingThread:  Ended");
  }

  /**
   * Called each time an "Alive" message is received from the QWServer.
   * This method implements the 'QWAliveMsgListener' interface.
   * @param recObj the 'QWStatusMsgRecord' object for the received message.
   */
  public synchronized void aliveMsgReceived(QWStatusMsgRecord recObj)
  {
    aliveMsgFlag = true;          //indicate alive message received
    interrupt();                  //interrupt the 'sleep()' in progress
  }

  /**
   * Called when the connection is about to be reinitialized.
   * @param userInitFlag true to indicate that the connnection-reinit
   * was signaled via user input.
   */
  public synchronized void connReinitialized(boolean userInitFlag)
  {
    connReinitFlag = true;        //indicate connection reinitializing
    if(userInitFlag)
      connReinitUserInitFlag = true;
    interrupt();                  //interrupt the 'sleep()' in progress
  }

  /**
   * Called when the 'maxServerAliveSec' property has changed.  This
   * interrupts the 'sleep()' that may be in progress with an old
   * (possibly large) value.
   */
  public synchronized void maxServerAliveSecChanged()
  {
    maxAliveTimeChgFlag = true;   //indicate value changed
    interrupt();                  //interrupt the 'sleep()' in progress
  }

  /**
   * Returns the status of whether or not the connection has been
   * "validated" via the receipt of any server-alive messages.
   * @return true if any server-alive messages have been received since
   * the last connect-to-server attempt, false if not.
   */
  public boolean isConnectionValidated()
  {
    return aliveMsgsRecvdFlag;
  }

  /**
   * Terminates this thread.
   */
  public synchronized void terminate()
  {
    logObj.debug5("ServerTrackingThread:  Terminate requested");
    terminateFlag = true;         //set terminate flag
                        //remove this object from list of listeners:
    connMgrObj.removeHdlrAliveMsgListener(this);
    interrupt();                  //interrupt any 'sleep()' in progress
    try
    {         //wait for thread to terminate, up to time limit
      join(100);
    }
    catch(InterruptedException ex) {}
  }
}
