//StatusCheckingThread.java:  Defines a thread that periodically calls the
//                            QWServices 'clientStatusCheck()' method.
//
//  7/27/2004 -- [ET]  Initial version.
//  9/24/2004 -- [ET]  Added 'connectionStatusChanged()' method.
//  4/14/2008 -- [ET]  Modified to reinitialize connection to server if
//                     status-check calls fail (for 10x connection-timeout
//                     time or 5 minutes if none).
//  10/4/2010 -- [ET]  Changed logged "Error calling 'clientStatusCheck()'"
//                     warning debug message.
//

package com.isti.quakewatch.util;

import com.isti.util.UtilFns;
import com.isti.util.LogFile;
import com.isti.util.IstiNotifyThread;

/**
 * Class StatusCheckingThread defines a thread that periodically calls the
 * QWServices 'clientStatusCheck()' method.
 */
public class StatusCheckingThread extends IstiNotifyThread
{
  protected final QWConnectionMgr connMgrObj;
  protected final QWConnProperties connPropsObj;
  protected final LogFile logObj;

  /**
   * Creates a status-checking thread object.
   * @param connMgrObj the connection-manager object to use.
   */
  public StatusCheckingThread(QWConnectionMgr connMgrObj)
  {
    super("StatusCheckingThread");
    this.connMgrObj = connMgrObj;
    connPropsObj = connMgrObj.getConnProperties();
    logObj = connMgrObj.getLogFileObj();
    start();            //start the checking thread
  }

  /**
   * Executing method for thread.
   */
  public void run()
  {
    try
    {
      logObj.debug2("StatusCheckingThread started");
         //setup timeout for status-check failures to cause conn reinit:
      final int ctoSecs;               // (setup as 10x connection timeout
      final int checkTimeoutMS =       //  or 5 minutes if none)
                ((ctoSecs=connPropsObj.getConnTimeoutSec()) > 0) ?
                                                     ctoSecs*10000 : 300000;
                   //mark initial time for status-check-failures timeout:
      long statusCheckTimeVal = System.currentTimeMillis();
      boolean statusCheckEnabledFlag = true;
      boolean lastCheckSuccessFlag = true;
      while(!isTerminated())
      {  //loop while thread is not terminated
        waitForNotify(
                   connPropsObj.statusCheckIntervalSecProp.intValue()*1000);
        if(isTerminated())
          break;
        if(connMgrObj.isConnectionValidated())
        {     //connection validated
          if(!connMgrObj.isFetchAndProcessMessagesRunning())
          {   //message-fetch from server not in progress
            if(statusCheckEnabledFlag)
            {      //call to status-check method enabled
              try
              {
                connMgrObj.performClientStatusCheck();
                lastCheckSuccessFlag = true;     //indicate success
                   //update time for status-check-failures timeout:
                statusCheckTimeVal = System.currentTimeMillis();
              }
              catch(NoSuchMethodException ex)
              {  //'clientStatusCheck()' method not implemented on server
                        //clear status-check method enabled flag
                        // (until connection is reset):
                statusCheckEnabledFlag = false;
                logObj.debug("StatusCheckingThread:  Calls to " +
                                 "'clientStatusCheck()' disabled because " +
                                        "method not implemented on server");
              }
              catch(Exception ex)
              {  //call to 'clientStatusCheck()' method failed
                        //log message (debug if comm, warning if other):
                if(ex.getCause() instanceof org.omg.CORBA.SystemException ||
                                ex instanceof org.omg.CORBA.SystemException)
                {
                  logObj.debug("StatusCheckingThread:  " + ex.getMessage());
                }
                else
                  logObj.warning(ex.getMessage());
                if(lastCheckSuccessFlag)              //if previous OK then
                  lastCheckSuccessFlag = false;       //indicate failure
                else if(System.currentTimeMillis() - statusCheckTimeVal >
                                                             checkTimeoutMS)
                {  //previous attempts also failed and timeout has elapsed
                             //reinitialize connection (don't force popup,
                             // don't use separate thread):
                  connMgrObj.reinitConnection(false,false,false);
                }
              }
            }
          }
          else
          {   //message-fetch from server is in progress
                   //update time for status-check-failures timeout:
            statusCheckTimeVal = System.currentTimeMillis();
            logObj.debug4("StatusCheckingThread:  Call to " +
                                  "'clientStatusCheck()' skipped because " +
                                               "message-fetch in progress");
          }
        }
        else
        {     //connection not validated
                   //update time for status-check-failures timeout:
          statusCheckTimeVal = System.currentTimeMillis();
          if(statusCheckEnabledFlag)
          {     //call to status-check method enabled
            logObj.debug4("StatusCheckingThread:  Call to " +
                                  "'clientStatusCheck()' skipped because " +
                                                "connection not validated");
          }
          else
          {   //call to status-check method enabled
            statusCheckEnabledFlag = true;  //re-enable (because conn reset)
            logObj.debug4("StatusCheckingThread:  Calls to " +
                                        "'clientStatusCheck()' re-enabled");
          }
        }
      }
      logObj.debug2("StatusCheckingThread terminated");
    }
    catch(Exception ex)
    {         //some kind of exception error; log it
      logObj.warning("StatusCheckingThread error:  " + ex);
      logObj.warning(UtilFns.getStackTraceString(ex));
    }
  }

  /**
   * Called after the connection status has changed.  This interrupts
   * any sleep-delay in progress, allowing the state of the connection
   * to be detected and the status check-in to be re-enabled or performed
   * immediately.
   */
  public void connectionStatusChanged()
  {
    notifyThread();
  }
}
