//CommTest.java:  Test program for client-side QuakeWatch communications.
//
//  11/3/2003 -- [ET]
//

package com.isti.quakewatch.utiltest;

import org.jdom.Element;
import com.isti.util.UtilFns;
import com.isti.util.LogFile;
import com.isti.util.CfgPropItem;
import com.isti.quakewatch.util.QWConnectionMgr;
import com.isti.quakewatch.message.QWDataMsgProcessor;
import com.isti.quakewatch.message.QWMsgNumTimeRec;
import com.isti.quakewatch.util.QWConnProperties;

/**
 * Class CommTest is a test program for client-side QuakeWatch
 * communications.
 */
public class CommTest
{
  protected final LogFile logObj = new LogFile("CommTest.log",
                                LogFile.ALL_MSGS,LogFile.DEBUG3,false,true);


  public CommTest(String [] args)
  {
    test1(args);
  }

  public void test1(String [] args)
  {
              //setup configuration for connection:
    final QWConnProperties connPropsObj = new QWConnProperties();
              //use host "www.isti.com:39977":
    connPropsObj.serverHostAddressProp.setValue("www.isti.com");
    connPropsObj.serverPortNumberProp.setValue(39977);
              //don't switch to alternate if server connection fails:
    connPropsObj.altServersEnabledFlagProp.setValue(false);

              //if max-server-event-age property object fetched OK then
              // set max age of events to fetch from server:
    final CfgPropItem evtAgePropObj;
    if((evtAgePropObj=connPropsObj.getMaxServerEventAgeDaysProp()) != null)
      evtAgePropObj.setValue(8.0/UtilFns.HOURS_PER_DAY);   //set to 8 hours

              //create message-processor object:
    final CommTestMsgProcessor msgProcessorObj = new CommTestMsgProcessor();
              //create connection-manager object:
    final QWConnectionMgr connMgrObj =
              new QWConnectionMgr(args,connPropsObj,msgProcessorObj,logObj);

    logObj.info("CommTest:  Initializing connection to server");
    connMgrObj.initializeConnection();      //connect to server
//    while(!connMgrObj.isConnectionValidated())
      UtilFns.sleep(100);                   //wait for connection validated
//    UtilFns.sleep(2000);

//    logObj.info("CommTest:  Reinitializing connection to server");
//    connMgrObj.reinitConnection();          //close and reconnect to server
//    while(!connMgrObj.isConnectionValidated())
//      UtilFns.sleep(100);                   //wait for connection validated
//    UtilFns.sleep(2000);

              //wait for user to enter "terminate":
    System.out.println("CommTest:  Enter 'q' to quit program");
    String str;
    while(true)
    {
      if((str=UtilFns.getUserConsoleString()) != null)
      {   //user string fetched from console OK
        if(str.equalsIgnoreCase("q"))
          break;         //if keyword then exit loop
      }
      else
      {   //error fetching user string from console
        try         //delay a bit before trying again:
        { Thread.sleep(3000); }
        catch(InterruptedException ex) {}
      }
    }

    logObj.info("CommTest:  closing connection to server");
    connMgrObj.closeConnection();           //close connection to server
  }

  /**
   * Test program entry point.
   * @param args command-line parameters.
   */
  public static void main(String [] args)
  {
    new CommTest(args);
  }


  protected class CommTestMsgProcessor implements QWDataMsgProcessor
  {
              //tracker for received data message data:
    private String dataMessageStringTracker = "";

    /**
     * Processes any number of "Event" and "Product" elements in the given
     * "DataMessage" element.
     * @param qwMsgElement The "QWmessage" element.
     * @param dataMsgElement The "DataMessage" element.
     * @param xmlMsgStr the XML text message string.
     * @param requestedFlag true to set the "requested" flag on the generated
     * data-message objects (to indicate that they should not be processed as
     * a "real-time" message).
     */
    public void processDataMessage(Element qwMsgElement,
              Element dataMsgElement,String xmlMsgStr,boolean requestedFlag)
    {
         //check if message is a duplicate of the previous message:
      if(xmlMsgStr != null && xmlMsgStr.length() > 0)
      {  //given message string contains data
        if(xmlMsgStr.equals(dataMessageStringTracker))
        {     //message string data same as previous; log warning
          logObj.warning("Data message is duplicate of previous--" +
                                           "discarded; msg:  " + xmlMsgStr);
          return;
        }
                   //save message data string for next iteration:
        dataMessageStringTracker = xmlMsgStr;
      }
      else    //no data in given message string
        dataMessageStringTracker = "";      //clear message string tracker

      logObj.info("Received data message:  " + xmlMsgStr);
    }

    /**
     * Enters the connection manager to be used by this message processor.
     * @param connMgrObj The connection-manager object to use.
     */
    public void setConnectionMgr(QWConnectionMgr connMgrObj)
    {              //connection manager not used by test code
    }

    /**
     * Returns the last event held in storage.
     * @return The 'QWMsgNumTimeRec' object for the last event held in
     * storage.
     */
    public QWMsgNumTimeRec getLastEventInStorage()
    {
      return null;
    }
  }
}
