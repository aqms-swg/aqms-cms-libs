//AbstractQueryStringProcessor:  Process the query string.
//
//   2/29/2012 -- [KF]  Initial version.
//
//
//=====================================================================
// Copyright (C) 2012 Instrumental Software Technologies, Inc.
//
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions
// are met:
// 1. Redistributions of source code, or portions of this source code,
//    must retain the above copyright notice, this list of conditions
//    and the following disclaimer.
// 2. Redistributions in binary form must reproduce the above copyright
//    notice, this list of conditions and the following disclaimer in
//    the documentation and/or other materials provided with the
//    distribution.
// 3. All advertising materials mentioning features or use of this
//    software must display the following acknowledgment:
//    "This product includes software developed by Instrumental
//    Software Technologies, Inc. (http://www.isti.com)"
// 4. If the software is provided with, or as part of a commercial
//    product, or is used in other commercial software products the
//    customer must be informed that "This product includes software
//    developed by Instrumental Software Technologies, Inc.
//    (http://www.isti.com)"
// 5. The names "Instrumental Software Technologies, Inc." and "ISTI"
//    must not be used to endorse or promote products derived from
//    this software without prior written permission. For written
//    permission, please contact "info@isti.com".
// 6. Products derived from this software may not be called "ISTI"
//    nor may "ISTI" appear in their names without prior written
//    permission of Instrumental Software Technologies, Inc.
// 7. Redistributions of any form whatsoever must retain the following
//    acknowledgment:
//    "This product includes software developed by Instrumental
//    Software Technologies, Inc. (http://www.isti.com/)."
// THIS SOFTWARE IS PROVIDED BY INSTRUMENTAL SOFTWARE
// TECHNOLOGIES, INC. "AS IS" AND ANY EXPRESSED OR IMPLIED
// WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES
// OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
// DISCLAIMED.  IN NO EVENT SHALL INSTRUMENTAL SOFTWARE TECHNOLOGIES,
// INC. OR ITS CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
// INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
// (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
// SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
// HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
// STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
// ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED
// OF THE POSSIBILITY OF SUCH DAMAGE.
//=====================================================================
//  A current version of the software can be found at
//                http://www.isti.com
//  Bug reports and comments should be directed to
//  Instrumental Software Technologies, Inc. at info@isti.com
//=====================================================================
//

package com.isti.util;

import java.net.URL;
import java.util.StringTokenizer;

/**
 * Abstract class AbstractQueryStringProcessor processes the query string in a
 * URL.
 * <p>
 * A URL containing a query string is as follows:
 * </p>
 * <dl>
 * <dd><code>scheme://domain/path?query_string</code></dd>
 * </dl>
 * <ul>
 * <li>The query string is composed of a series of name-value pairs.</li>
 * <li>The name-value pairs are each separated by an equals sign '='. The equals
 * sign and value may be omitted if the value is an empty string.</li>
 * <li>The series of pairs is separated by the ampersand '&amp;' or semicolon
 * ';'.</li>
 * </ul>
 * <p>
 * The 'processNameValue' method must be implemented to process the name-value
 * pairs.
 * @see #processNameValue(String, String)
 */
public abstract class AbstractQueryStringProcessor implements StringConstants {
  /** The default name value pair series separator. */
  public final static String DEFAULT_NAME_VALUE_PAIR_SERIES_SEPARATOR = "&;";

  /** The default name value separator. */
  public final static String DEFAULT_NAME_VALUE_SEPARATOR = "=";

  /** The default query string separator. */
  public final static String DEFAULT_QUERY_STRING_SEPARATOR = "?";

  /** True to decode the values, false otherwise. */
  private boolean decodeFlag = true;

  /** The name value pair series separator. */
  private String nameValuePairSeriesSeparator = DEFAULT_NAME_VALUE_PAIR_SERIES_SEPARATOR;

  /** The name value separator. */
  private String nameValueSeparator = DEFAULT_NAME_VALUE_SEPARATOR;

  /** The query string separator. */
  private String queryStringSeparator = DEFAULT_QUERY_STRING_SEPARATOR;

  /**
   * Get the name value pair series separator.
   * @return the name value pair series separator.
   */
  public String getNameValuePairSeriesSeparator() {
    return nameValuePairSeriesSeparator;
  }

  /**
   * Get the name value separator.
   * @return the name value separator.
   */
  public String getNameValueSeparator() {
    return nameValueSeparator;
  }

  /**
   * Get the query string separator.
   * @return the query string separator.
   */
  public String getParamsStartText() {
    return queryStringSeparator;
  }

  /**
   * Determines if the code flag is set.
   * @return true if decoding, false otherwise.
   */
  public boolean isDecodeFlag() {
    return decodeFlag;
  }

  /**
   * Process the name value.
   * @param name the name.
   * @param value the value.
   * @return true if the processing should continue, false otherwise.
   */
  public abstract boolean processNameValue(String name, String value);

  /**
   * Process the query string in the URL text.
   * @param urlText the URL text.
   */
  public void processQueryString(String urlText) {
    String name, value;
    if (queryStringSeparator != null) {
      int index = urlText.lastIndexOf(queryStringSeparator);
      if (index >= 0) {
        urlText = urlText.substring(index + 1);
      }
    }
    StringTokenizer queryStringTokenizer = new StringTokenizer(urlText,
        nameValuePairSeriesSeparator);
    while (queryStringTokenizer.hasMoreTokens()) {
      StringTokenizer nameValueTokenizer = new StringTokenizer(
          queryStringTokenizer.nextToken(), nameValueSeparator);
      if (!nameValueTokenizer.hasMoreTokens()) {
        continue;
      }
      name = nameValueTokenizer.nextToken();
      value = EMPTY_STRING;
      if (nameValueTokenizer.hasMoreTokens()) {
        value = nameValueTokenizer.nextToken();
        if (decodeFlag) {
          value = URLRefDecoder.decode(value);
        }
      }
      if (!processNameValue(name, value)) {
        break;
      }
    }
  }

  /**
   * Process the query string in the URL.
   * @param url the URL.
   */
  public void processQueryString(URL url) {
    processQueryString(url.toString());
  }

  /**
   * Set the decode flag.
   * @param decodeFlag true if decoding, false otherwise.
   */
  public void setDecodeFlag(boolean decodeFlag) {
    this.decodeFlag = decodeFlag;
  }

  /**
   * Set the name value pair series separator.
   * @param the name value pair series separator.
   */
  public void setNameValuePairSeriesSeparator(
      String nameValuePairSeriesSeparator) {
    this.nameValuePairSeriesSeparator = nameValuePairSeriesSeparator;
  }

  /**
   * Set the name value separator.
   * @param the name value separator.
   */
  public void setNameValueSeparator(String nameValueSeparator) {
    this.nameValueSeparator = nameValueSeparator;
  }

  /**
   * Set the query string separator.
   * @param the query string separator.
   */
  public void setParamsStartText(String queryStringSeparator) {
    this.queryStringSeparator = queryStringSeparator;
  }
}
