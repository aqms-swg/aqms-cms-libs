//AddrPortListMgr.java:  Manages a list of host-address/port-number entries.
//
//  8/12/2003 -- [ET]  Initial version.
//  11/6/2003 -- [ET]  Added 'get/setKeepDefaultServersFlag()' methods.
// 10/20/2004 -- [ET]  Added 'equalsFirstEntry()' method.
// 11/15/2004 -- [ET]  Added 'popRandomEntry()' method.
// 11/18/2004 -- [ET]  Added optional 'randomFlag' parameter to
//                     'getEntriesListStr()' method; added 'hashCode()'
//                     method to 'EntryBlock' class.
//

package com.isti.util;

import java.util.Vector;
import java.util.Iterator;
import java.util.Random;

/**
 * Class AddrPortListMgr manages a list of host-address/port-number entries.
 */
public class AddrPortListMgr extends ErrorMessageMgr
{
         //host-address/port-number separator character (default is ':'):
  protected static char sepCh = ':';
         //Vector of 'EntryBlock' objects:
  protected final Vector entriesVec = new Vector();
         //call-back for 'fireListCommit()' method:
  protected DataChangedListener listCommitListenerObj = null;
         //utility flag used via get/set methods:
  protected boolean keepDefaultServersFlag = false;
         //random-number generator object:
  protected final Random randomObj = new Random(System.currentTimeMillis());

  /**
   * Creates a manager for a list of host-address/port-number entries,
   * with the list initially empty.
   */
  public AddrPortListMgr()
  {
  }

  /**
   * Creates a manager for a list of host-address/port-number entries,
   * interprets the given data list string into host-address/port-number
   * entries and adds them to the end of the list.  The 'getErrorMessage()'
   * method may be used to determine if an error occurred while interpreting
   * the data list string (in which case the 'getErrorMessageString()'
   * method may be used to fetch the error message).
   * @param dataListStr a data list string in the form
   * "hostAddr:portNum,hostAddr:portNum,...", where ':' is the
   * separator character defined via the 'setSeparatorChar()' method
   * (default is ':') and ',' is the item-separator character defined
   * by the 'itemSepChar' parameter.
   * @param itemSepChar the item separator character.
   */
  public AddrPortListMgr(String dataListStr,char itemSepChar)
  {
    addEntriesListStr(dataListStr,itemSepChar);
  }

  /**
   * Creates a manager for a list of host-address/port-number entries,
   * interprets the given data list string into host-address/port-number
   * entries and adds them to the end of the list.  The 'getErrorMessage()'
   * method may be used to determine if an error occurred while interpreting
   * the data list string (in which case the 'getErrorMessageString()'
   * method may be used to fetch the error message).
   * @param dataListStr a data list string in the form
   * "hostAddr:portNum,hostAddr:portNum,...", where ':' is the
   * separator character defined via the 'setSeparatorChar()' method
   * (default is ':').
   */
  public AddrPortListMgr(String dataListStr)
  {
    addEntriesListStr(dataListStr);
  }

  /**
   * Interprets the given data string into a host address and port number
   * and creates and entry data block containing the values.
   * @param dataStr a data string in the form "hostAddr:portNum", where
   * ':' is the separator character defined via the  'setSeparatorChar()'
   * method (default is ':').
   * @return A new 'EntryBlock' object, or null if an error occurred
   * while interpreting the data string (in which case the
   * 'getErrorMessageString()' method may be used to fetch the
   * error message).
   */
  public EntryBlock parseEntry(String dataStr)
  {
    clearErrorMessageString();    //clear any previous error
    final int sepPos;
    if(dataStr == null ||         //trim whitespace; find separator
                       (sepPos=(dataStr=dataStr.trim()).indexOf(sepCh)) < 0)
    {    //null handle or separator not found; set error message
      setErrorMessageString("Separator '" + sepCh + "' not found");
      return null;
    }
    if(sepCh <= 0)
    {    //no host-address data; set error message
      setErrorMessageString("Host-address data not found");
      return null;
    }
    final int portNum;
    try
    {         //parse port number integer value:
      portNum = Integer.parseInt(dataStr.substring(sepPos+1));
    }
    catch(NumberFormatException ex)
    {         //error parsing value; set error message
      setErrorMessageString("Unable to parse port number value (\"" +
                                       dataStr.substring(sepPos+1) + "\")");
      return null;
    }
                   //create and return data block for entry:
    return new EntryBlock(dataStr.substring(0,sepPos),portNum);
  }

  /**
   * Determines whether or not an entry with the same host-address and
   * port-number exists in the list.
   * @param blkObj the host-address/port-number data-block to add.
   * @return true if an an entry with the same host-address and port-number
   * exists in the list.
   */
  public boolean contains(EntryBlock blkObj)
  {
    return entriesVec.contains(blkObj);
  }

  /**
   * Adds a host-address/port-number entry to the end of the list.  If
   * an entry with the same host address and port number already exists
   * in the list then the list is left unchanged.
   * @param blkObj the host-address/port-number data-block to use.
   * @return true if the new entry was added to the end of the list;
   * false if an entry with the same host address and port number already
   * existed in the list and the list was left unchanged.
   */
  public boolean addEntry(EntryBlock blkObj)
  {
    synchronized(entriesVec)
    {    //grab thread-synchronization lock for Vector object
      if(entriesVec.contains(blkObj))  //if already contains equal entry then
        return false;                  //indicate not added
      entriesVec.add(blkObj);     //add entry to end
      return true;                //indicate added
    }
  }

  /**
   * Adds a host-address/port-number entry to the end of the list.  If
   * an entry with the same host address and port number already exists
   * in the list then the list is left unchanged.
   * @param addrStr the host address to use.
   * @param portNum the port number to use.
   * @return true if the new entry was added to the end of the list;
   * false if an entry with the same host address and port number already
   * existed in the list and the list was left unchanged.
   */
  public boolean addEntry(String addrStr,int portNum)
  {
    return addEntry(new EntryBlock(addrStr,portNum));
  }

  /**
   * Interprets the given data string into a host address and port number
   * and adds a new entry with the values to the end of the list.
   * @param dataStr a data string in the form "hostAddr:portNum", where
   * ':' is the separator character defined via the  'setSeparatorChar()'
   * method (default is ':').
   * @return true if successful, false if an error occurred
   * while interpreting the data string (in which case the
   * 'getErrorMessageString()' method may be used to fetch the
   * error message).
   */
  public boolean addEntry(String dataStr)
  {
    final EntryBlock blkObj;
    if((blkObj=parseEntry(dataStr)) == null)     //parse data
      return false;     //if error then return false (err msg already setup)
    addEntry(blkObj);             //add entry to end of list
    return true;
  }

  /**
   * Interprets the given data list string into host-address/port-number
   * entries and adds them to the end of the list.  If the given data
   * list string contains no data then this method just returns 'true'.
   * @param dataListStr a data list string in the form
   * "hostAddr:portNum,hostAddr:portNum,...", where ':' is the
   * separator character defined via the 'setSeparatorChar()' method
   * (default is ':') and ',' is the item-separator character defined
   * by the 'itemSepChar' parameter.
   * @param itemSepChar the item separator character.
   * @return true if successful, false if an error occurred
   * while interpreting the data list string (in which case the
   * 'getErrorMessageString()' method may be used to fetch the
   * error message).
   */
  public final boolean addEntriesListStr(String dataListStr,char itemSepChar)
  {
    synchronized(entriesVec)
    {    //grab thread-synchronization lock for Vector object
      final int dataListStrLen;
      if(dataListStr == null ||          //trim whitespace and get length
            (dataListStrLen=(dataListStr=dataListStr.trim()).length()) <= 0)
      {    //no data given
        return true;           //just return
      }
      String itemStr;
      int itemSepPos, sPos = 0;
      do
      {    //for each item in list string; find separator character:
        if((itemSepPos=dataListStr.indexOf(itemSepChar,sPos)) < 0)
          itemSepPos = dataListStrLen;   //if not found then use end pos
                //add item string to end of list:
        if(!addEntry(itemStr=dataListStr.substring(sPos,itemSepPos)))
        {  //error parsing item; insert data into error message
          setErrorMessageString("Error in item \"" + itemStr +
                   "\" (at " + (sPos+1) + "):  " + getErrorMessageString());
          return false;
        }
      }         //set next position after separator; loop if data remaining
      while((sPos=itemSepPos+1) < dataListStrLen);
      return true;
    }
  }

  /**
   * Interprets the given data list string into host-address/port-number
   * entries and adds them to the end of the list.  If the given data
   * list string contains no data then this method just returns 'true'.
   * @param dataListStr a data list string in the form
   * "hostAddr:portNum,hostAddr:portNum,...", where ':' is the
   * separator character defined via the 'setSeparatorChar()' method
   * (default is ':').
   * @return true if successful, false if an error occurred
   * while interpreting the data list string (in which case the
   * 'getErrorMessageString()' method may be used to fetch the
   * error message).
   */
  public final boolean addEntriesListStr(String dataListStr)
  {
    return addEntriesListStr(dataListStr,',');
  }

  /**
   * Clears any current entries; then interprets the given data list
   * string into host-address/port-number entries and adds them to the
   * end of the list.  If the given data list string contains no data
   * then this method just returns 'true'.
   * @param dataListStr a data list string in the form
   * "hostAddr:portNum,hostAddr:portNum,...", where ':' is the
   * separator character defined via the 'setSeparatorChar()' method
   * (default is ':') and ',' is the item-separator character defined
   * by the 'itemSepChar' parameter.
   * @param itemSepChar the item separator character.
   * @return true if successful, false if an error occurred
   * while interpreting the data list string (in which case the
   * 'getErrorMessageString()' method may be used to fetch the
   * error message).
   */
  public boolean setEntriesListStr(String dataListStr,char itemSepChar)
  {
    synchronized(entriesVec)
    {    //grab thread-synchronization lock for Vector object
      entriesVec.clear();                             //clear list
      addEntriesListStr(dataListStr,itemSepChar);     //add entries
      return true;
    }
  }

  /**
   * Clears any current entries; then interprets the given data list
   * string into host-address/port-number entries and adds them to the
   * end of the list.  If the given data list string contains no data
   * then this method just returns 'true'.
   * @param dataListStr a data list string in the form
   * "hostAddr:portNum,hostAddr:portNum,...", where ':' is the
   * separator character defined via the 'setSeparatorChar()' method
   * (default is ':').
   * @return true if successful, false if an error occurred
   * while interpreting the data list string (in which case the
   * 'getErrorMessageString()' method may be used to fetch the
   * error message).
   */
  public boolean setEntriesListStr(String dataListStr)
  {
    synchronized(entriesVec)
    {    //grab thread-synchronization lock for Vector object
      entriesVec.clear();                             //clear list
      addEntriesListStr(dataListStr);                 //add entries
      return true;
    }
  }

  /**
   * Returns the number of host-address/port-number entries in the list.
   * @return The number of entries in the list.
   */
  public int size()
  {
    return entriesVec.size();
  }

  /**
   * Removes the given host-address/port-number entry.
   * @param blkObj the host-address/port-number entry to remove.
   * @return true if the list contained the entry, false if not.
   */
  public boolean removeEntry(EntryBlock blkObj)
  {
    return entriesVec.remove(blkObj);
  }

  /**
   * Removes the given host-address/port-number entry.
   * @param addrStr the host address to use.
   * @param portNum the port number to use.
   * @return An entry block object containing the host address and port
   * number of the removed entry, or null if no matching entry was found.
   */
  public EntryBlock removeEntry(String addrStr,int portNum)
  {
    synchronized(entriesVec)
    {    //grab thread-synchronization lock for Vector object
                        //create entry-block for given hostAddr/portNum:
      final EntryBlock blkObj = new EntryBlock(addrStr,portNum);
      final int idx;    //find matching hostAddr/portNum in list:
      if((idx=entriesVec.indexOf(blkObj)) >= 0)
      {  //matching hostAddr/portNum entry found
        Object obj;     //remove and return entry:
        return ((obj=entriesVec.remove(idx)) instanceof EntryBlock) ?
                                                   (EntryBlock)obj : blkObj;
      }
      return null;
    }
  }

  /**
   * Removes all entries from the list.
   */
  public void clearAllEntries()
  {
    entriesVec.clear();
  }

  /**
   * Returns the host-address/port-number entry at the given index in the
   * list.
   * @param idx the index into the list to use.
   * @return The host-address/port-number entry at the given index in the
   * list, or null if no entries exists for the given index.
   */
  public EntryBlock getEntry(int idx)
  {
    synchronized(entriesVec)
    {    //grab thread-synchronization lock for Vector object
      if(idx < 0 || idx > entriesVec.size())
        return null;
      final Object obj;
      return ((obj=entriesVec.get(idx)) instanceof EntryBlock) ?
                                                     (EntryBlock)obj : null;
    }
  }

  /**
   * Fetches the first entry and deletes it from the list.
   * @return The first 'EntryBlock' object in the list, or null if none
   * were found.
   */
  public EntryBlock popFirstEntry()
  {
    synchronized(entriesVec)
    {    //grab thread-synchronization lock for Vector object
      final EntryBlock blkObj;
      if(entriesVec.size() > 0)
      {  //list not empty
        final Object obj;    //fetch and remove first entry
        blkObj = ((obj=entriesVec.remove(0)) instanceof EntryBlock) ?
                                                     (EntryBlock)obj : null;
      }
      else    //list is empty
        blkObj = null;
      return blkObj;
    }
  }

  /**
   * Fetches the first entry and deletes it from the list and then
   * adds a host-address/port-number entry to the end of the list.  If
   * an entry with the same host address and port number already exists
   * in the list then the list is left unchanged.  If the list is empty
   * then the given entry is added and null is returned.
   * @param blkObj the host-address/port-number data-block to add.
   * @return The first 'EntryBlock' object in the list, or null if none
   * were found.
   */
  public EntryBlock popFirstAndAddEntry(EntryBlock blkObj)
  {
    synchronized(entriesVec)
    {    //grab thread-synchronization lock for Vector object
      final EntryBlock poppedObj = popFirstEntry();
      addEntry(blkObj);
      return poppedObj;
    }
  }

  /**
   * Fetches the first entry and deletes it from the list and then
   * adds a host-address/port-number entry to the end of the list.  If
   * an entry with the same host address and port number already exists
   * in the list then the list is left unchanged.  If the list is empty
   * then the given entry is added and null is returned.
   * @param addrStr the host address to use.
   * @param portNum the port number to use.
   * @return The first 'EntryBlock' object in the list, or null if none
   * were found.
   */
  public EntryBlock popFirstAndAddEntry(String addrStr,int portNum)
  {
    return popFirstAndAddEntry(new EntryBlock(addrStr,portNum));
  }

  /**
   * Fetches a randomly-selected entry and deletes it from the list.
   * @return A randomly-selected 'EntryBlock' object in the list, or
   * null if none were found.
   */
  public EntryBlock popRandomEntry()
  {
    synchronized(entriesVec)
    {    //grab thread-synchronization lock for Vector object
      final EntryBlock blkObj;
      final int numEntries;
      if((numEntries=entriesVec.size()) > 0)
      {  //list not empty
        final Object obj;    //fetch and remove first entry
        blkObj = ((obj=entriesVec.remove(randomObj.nextInt(numEntries)))
                            instanceof EntryBlock) ? (EntryBlock)obj : null;
      }
      else    //list is empty
        blkObj = null;
      return blkObj;
    }
  }

  /**
   * Compares the given host-address/port-number to those held by the
   * first entry in the list.
   * @param hostAddr the host address to use.
   * @param portNum the port number to use.
   * @return true if the given host-address/port-number equals the
   * host address string and port number fields held by the first
   * entry in the list; false if not.
   */
  public boolean equalsFirstEntry(String hostAddr,int portNum)
  {
    final EntryBlock blkObj;
    return ((blkObj=getEntry(0)) != null) ?
                                    blkObj.equals(hostAddr,portNum) : false;
  }

  /**
   * Returns a data list string containing the host-name/port-number
   * entries in this list.  The returned string is in the form
   * "hostAddr:portNum,hostAddr:portNum,...", where ':' is the
   * separator character defined via the 'setSeparatorChar()' method
   * (default is ':') and ',' is the item-separator character defined
   * by the 'itemSepChar' parameter.
   * @param itemSepChar the item separator character.
   * @param randomFlag true to return the entries in random order;
   * false to return the entries in order as entered.
   * @return A data list string containing the host-name/port-number
   * entries in this list.
   */
  public String getEntriesListStr(char itemSepChar, boolean randomFlag)
  {
    final StringBuffer buff = new StringBuffer();
    if(randomFlag)
    {    //return entries in random order
      final Vector vec;      //copy entries into new Vector:
      synchronized(entriesVec)
      {  //grab thread-synchronization lock for Vector object
        vec = new Vector(entriesVec);
      }
      int vecSize;
      if((vecSize=vec.size()) > 0)
      {  //list is not empty
        Object obj;
        while(true)
        {     //remove one randomly-selected item from list:
          if((obj=vec.remove(randomObj.nextInt(vecSize))) instanceof
                                                                 EntryBlock)
          {   //'EntryBlock' object fetched OK
                        //add "hostAddr:portNum" entry to buffer:
            buff.append(((EntryBlock)obj).toString());
          }
          if(--vecSize <= 0)           //decrement Vector size
            break;           //if no more items then exit loop
          buff.append(itemSepChar);    //add item separator to list
        }
      }
    }
    else
    {    //return entries in normal order
      synchronized(entriesVec)
      {  //grab thread-synchronization lock for Vector object
        final Iterator iterObj = entriesVec.iterator();
        if(iterObj.hasNext())
        {     //list is not empty
          Object obj;
          while(true)
          {   //for each item in list
            if((obj=iterObj.next()) instanceof EntryBlock)
            { //'EntryBlock' object fetched OK
                        //add "hostAddr:portNum" entry to buffer:
              buff.append(((EntryBlock)obj).toString());
            }
            if(!iterObj.hasNext())     //if no more items then
              break;                   //exit loop
            buff.append(itemSepChar);       //add item separator to list
          }
        }
      }
    }
    return buff.toString();       //convert buffer to string and return
  }

  /**
   * Returns a data list string containing the host-name/port-number
   * entries in this list.  The returned string is in the form
   * "hostAddr:portNum,hostAddr:portNum,...", where ':' is the
   * separator character defined via the 'setSeparatorChar()' method
   * (default is ':') and ',' is the item-separator character defined
   * by the 'itemSepChar' parameter.
   * @param itemSepChar the item separator character.
   * @return A data list string containing the host-name/port-number
   * entries in this list.
   */
  public String getEntriesListStr(char itemSepChar)
  {
    return getEntriesListStr(itemSepChar,false);
  }

  /**
   * Returns a data list string containing the host-name/port-number
   * entries in this list.  The returned string is in the form
   * "hostAddr:portNum,hostAddr:portNum,...", where ':' is the
   * separator character defined via the 'setSeparatorChar()' method
   * (default is ':').
   * @param randomFlag true to return the entries in random order;
   * false to return the entries in order as entered.
   * @return A data list string containing the host-name/port-number
   * entries in this list.
   */
  public String getEntriesListStr(boolean randomFlag)
  {
    return getEntriesListStr(',',randomFlag);
  }

  /**
   * Returns a data list string containing the host-name/port-number
   * entries in this list.  The returned string is in the form
   * "hostAddr:portNum,hostAddr:portNum,...", where ':' is the
   * separator character defined via the 'setSeparatorChar()' method
   * (default is ':').
   * @return A data list string containing the host-name/port-number
   * entries in this list.
   */
  public String getEntriesListStr()
  {
    return getEntriesListStr(false);
  }

  /**
   * Removes and returns all entries in this list.  The returned string
   * is in the form "hostAddr:portNum,hostAddr:portNum,...", where ':'
   * is the separator character defined via the 'setSeparatorChar()'
   * method (default is ':') and ',' is the item-separator character
   * defined by the 'itemSepChar' parameter.
   * @param itemSepChar the item separator character.
   * @return A data list string containing the host-name/port-number
   * entries that were removed from this list.
   */
  public String popAllEntries(char itemSepChar)
  {
    synchronized(entriesVec)
    {    //grab thread-synchronization lock for Vector object
                                  //get string containing all entries:
      final String retStr = getEntriesListStr(itemSepChar);
      entriesVec.clear();         //clear all entries
      return retStr;              //return entries string
    }
  }

  /**
   * Removes and returns all entries in this list.  The returned string
   * is in the form "hostAddr:portNum,hostAddr:portNum,...", where ':'
   * is the separator character defined via the 'setSeparatorChar()'
   * method (default is ':').
   * @return A data list string containing the host-name/port-number
   * entries that were removed from this list.
   */
  public String popAllEntries()
  {
    synchronized(entriesVec)
    {    //grab thread-synchronization lock for Vector object
                                  //get string containing all entries:
      final String retStr = getEntriesListStr();
      entriesVec.clear();         //clear all entries
      return retStr;              //return entries string
    }
  }

  /**
   * Enters the call-back listener to be called when the 'fireListCommit()'
   * method is invoke.
   * @param listenerObj the call-back listener to use, or null to clear
   * any current listener.
   */
  public void setListCommitListenerObj(DataChangedListener listenerObj)
  {
    listCommitListenerObj = listenerObj;
  }

  /**
   * Indicates that the contents of the list should be "committed".  The
   * call-back listener setup by the 'setListCommitListenerObj()' method
   * is called.
   * @param sourceObj the source object to be passed along to the call-back
   * listener.
   */
  public void fireListCommit(Object sourceObj)
  {
    if(listCommitListenerObj != null)
      listCommitListenerObj.dataChanged(sourceObj);
  }

  /**
   * Sets the host-address/port-number separator character.  The default
   * value is ':'.
   * @param ch the separator character to use.
   */
  public static void setSeparatorChar(char ch)
  {
    sepCh = ch;
  }

  /**
   * Returns the host-address/port-number separator character.
   * @return The host-address/port-number separator character.
   */
  public static char getSeparatorChar()
  {
    return sepCh;
  }

  /**
   * Sets the 'keepDefaultServersFlag' flag.  This determines what
   * value is returned by the 'getKeepDefaultServersFlag()', but is
   * not otherwise used by this class.
   * @param flgVal the flag value to use.
   */
  public void setKeepDefaultServersFlag(boolean flgVal)
  {
    keepDefaultServersFlag = flgVal;
  }

  /**
   * Returns the 'keepDefaultServersFlag' flag.
   * @return The 'keepDefaultServersFlag' flag.
   */
  public boolean getKeepDefaultServersFlag()
  {
    return keepDefaultServersFlag;
  }

  /** Test program entry point. */
/*
  public static void main(String [] args)
  {
    final AddrPortListMgr mgrObj = new AddrPortListMgr();
    System.out.print("Enter initial:  ");
    String str = UtilFns.getUserConsoleString();
    if(!mgrObj.addEntriesListStr(str))
      System.err.println(mgrObj.getErrorMessageString());
    System.out.println("Initial contents:  \"" +
                                         mgrObj.getEntriesListStr() + "\"");
    EntryBlock blkObj;
    while(true)
    {
      System.out.print("Enter new item:  ");
      if((str=UtilFns.getUserConsoleString()) == null ||
            str.length() <= 0 || str.startsWith("Q") || str.startsWith("q"))
      {
        break;
      }
      System.out.println("Parsing:  \"" + str + "\"");
      if((blkObj=mgrObj.parseEntry(str)) != null)
      {
        blkObj = mgrObj.popFirstAndAddEntry(blkObj);
        System.out.println("Popped item:  \"" + blkObj + "\"");
      }
      else
        System.err.println(mgrObj.getErrorMessageString());
      System.out.println("Current contents:  \"" +
                                         mgrObj.getEntriesListStr() + "\"");
    }
  }
*/


  /**
   * Class EntryBlock holds a host-address string and a port number value.
   */
  public static class EntryBlock
  {
    /** Host address string. */
    public final String hostAddrStr;
    /** Port number value. */
    public final int portNumber;

    /**
     * Creates a data-block that holds a host-address string and a
     * port number value.
     * @param hostAddrStr the host address string to use.
     * @param portNumber the port number to use.
     */
    public EntryBlock(String hostAddrStr,int portNumber)
    {
      this.hostAddrStr = hostAddrStr;
      this.portNumber = portNumber;
    }

    /**
     * Compares the given object to this one.
     * @param obj the object to compare.
     * @return true if the given object is a 'EntryBlock' containing equal
     * host address string and port number fields.
     */
    public boolean equals(Object obj)
    {
      if(!(obj instanceof EntryBlock))
        return false;        //if object of wrong type then return false
      final EntryBlock blkObj = (EntryBlock)obj;
      if(hostAddrStr != null)
      {  //this object's host address string not null
        if(!hostAddrStr.equals(blkObj.hostAddrStr))
          return false;      //if host addr str not equal then return false
      }
      else
      {  //this object's host address string is null
        if(blkObj.hostAddrStr != null)
          return false;      //if obj's addr str not null then return false
      }
                             //return result of port number comparison:
      return (portNumber == blkObj.portNumber);
    }

    /**
     * Compares the given host-address/port-number to those held by this
     * object.
     * @param hostAddr the host address to use.
     * @param portNum the port number to use.
     * @return true if the given host-address/port-number equals the
     * host address string and port number fields held by this object.
     */
    public boolean equals(String hostAddr,int portNum)
    {
      if(hostAddrStr != null)
      {  //this object's host address string not null
        if(!hostAddrStr.equals(hostAddr))
          return false;      //if host addr str not equal then return false
      }
      else
      {  //this object's host address string is null
        if(hostAddr != null)
          return false;      //if obj's addr str not null then return false
      }
                             //return result of port number comparison:
      return (portNumber == portNum);
    }

    /**
     * Returns the hash code value for this object.  The hash code
     * for the string returned via 'toString()' is used.
     * @return the hash code value for this object.
     */
    public int hashCode()
    {
      return toString().hashCode();
    }

    /**
     * Returns a string representation of this data-block.  The string
     * is in the form "hostAddr:portNum", where ':' is the separator
     * character defined via the 'AddrPortListMgr.setSeparatorChar()'
     * method (default is ':').
     * @return A new string representation of this data-block.
     */
    public String toString()
    {
      return hostAddrStr + sepCh + portNumber;
    }
  }
}
