package com.isti.util;

import java.util.Date;

/**
 * An interface for objects that can be archived.  Archivable classes must
 * have a constructor with 'String' and 'Archivable.Marker' parameters,
 * like this:
 * <br>
 * public ArchivableImpl(String dataStr, Archivable.Marker mkrObj) ...
 * <br>
 * This constructor is used to create the class from a string of data.
 * The 'Archivable.Marker' is used to mark the constructor as being
 * available for de-archiving (and will usually be 'null').
 */

public interface Archivable
{
  public class Marker {}

  /**
   * Returns a 'Date' object representing the date to be used for archival
   * purposes.
   * @return A 'Date' object representing the date that the item should be
   * archived under.
   */
  public Date getArchiveDate();

  /**
   * Returns the archivable representation for this object.  When the
   * object is recreated from the string, it should be identical to the
   * original.
   * @return A String representing the archived form of the object.
   */
  public String toArchivedForm();
}
