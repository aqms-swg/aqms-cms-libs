//ArchivableData.java:  Defines an archive containing a data
//                      object and timestamp.
//
//   9/26/2003 -- [KF]  Initial version.
//   1/16/2004 -- [KF]  Added logging messages of errors and warnings.
//    8/6/2004 -- [KF]  Made global logging use console output if not set.
//   9/29/2006 -- [KF]  Moved logic to the 'ValueTimeBlock' base class.
//   6/29/2010 -- [ET]  Modified string-parameter contructors to throw
//                      exception in response to error processing string.
//
//=====================================================================
// Copyright (C) 2010 Instrumental Software Technologies, Inc.
//
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions
// are met:
// 1. Redistributions of source code, or portions of this source code,
//    must retain the above copyright notice, this list of conditions
//    and the following disclaimer.
// 2. Redistributions in binary form must reproduce the above copyright
//    notice, this list of conditions and the following disclaimer in
//    the documentation and/or other materials provided with the
//    distribution.
// 3. All advertising materials mentioning features or use of this
//    software must display the following acknowledgment:
//    "This product includes software developed by Instrumental
//    Software Technologies, Inc. (http://www.isti.com)"
// 4. If the software is provided with, or as part of a commercial
//    product, or is used in other commercial software products the
//    customer must be informed that "This product includes software
//    developed by Instrumental Software Technologies, Inc.
//    (http://www.isti.com)"
// 5. The names "Instrumental Software Technologies, Inc." and "ISTI"
//    must not be used to endorse or promote products derived from
//    this software without prior written permission. For written
//    permission, please contact "info@isti.com".
// 6. Products derived from this software may not be called "ISTI"
//    nor may "ISTI" appear in their names without prior written
//    permission of Instrumental Software Technologies, Inc.
// 7. Redistributions of any form whatsoever must retain the following
//    acknowledgment:
//    "This product includes software developed by Instrumental
//    Software Technologies, Inc. (http://www.isti.com/)."
// THIS SOFTWARE IS PROVIDED BY INSTRUMENTAL SOFTWARE
// TECHNOLOGIES, INC. "AS IS" AND ANY EXPRESSED OR IMPLIED
// WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES
// OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
// DISCLAIMED.  IN NO EVENT SHALL INSTRUMENTAL SOFTWARE TECHNOLOGIES,
// INC. OR ITS CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
// INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
// (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
// SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
// HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
// STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
// ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED
// OF THE POSSIBILITY OF SUCH DAMAGE.
//=====================================================================
//  A current version of the software can be found at
//                http://www.isti.com
//  Bug reports and comments should be directed to
//  Instrumental Software Technologies, Inc. at info@isti.com
//=====================================================================
//

package com.isti.util;

/**
 * Class ArchivableData defines an archive containing a data
 * object and timestamp.
 *
 * If the class for the data has a constructor with a String parameter then
 * the de-archived data should be identical to the original data,
 * otherwise the de-archived data will contain a string representation of the
 * original data.
 */
public class ArchivableData extends ValueTimeBlock implements Archivable
{
  /**
   * Constructor which initializes values.
   * @param data data object, such as Integer, Double, String, etc.
   * @param timestamp timestamp value for data which is
   * the number of seconds since January 1, 1970, 00:00:00 GMT.
   */
  public ArchivableData(Object data, long timestamp)
  {
    super(data,timestamp);
  }

  /**
   * Constructor which initializes values.
   * @param dataStr value-time format archivable representation for
   * this object.
   * @throws IllegalArgumentException if an error occurred.
   */
  public ArchivableData(String dataStr) throws IllegalArgumentException
  {
    this(getCheckValueTimeBlk(dataStr));
  }

  /**
   * Constructs a history entry.
   * This is part of the Archivable interface.
   * @param dataStr value-time format archivable representation for
   * this object.
   * @param mkrObj the marker object.
   * @throws IllegalArgumentException if an error occurred.
   */
  public ArchivableData(String dataStr, Archivable.Marker mkrObj)
                                             throws IllegalArgumentException
  {
    this(getCheckValueTimeBlk(dataStr));
  }

  /**
   * Constructor which initializes values.
   * @param vtb the value time block.
   */
  public ArchivableData(ValueTimeBlock vtb)
  {
    this(vtb.data,vtb.timestamp);
  }

  /**
   * Gets the value time block for the specified data string.
   * @param dataStr value-time format data string.
   * @return A new value time block.
   * @throws IllegalArgumentException if an error occurred.
   */
  protected static ValueTimeBlock getCheckValueTimeBlk(String dataStr)
                                             throws IllegalArgumentException
  {
    final ValueTimeBlock blkObj;
    if((blkObj=getValueTimeBlock(dataStr)) != null)
      return blkObj;
    throw new IllegalArgumentException(
                     "Error processsing data string into value-time block");
  }
}
