package com.isti.util;

import java.util.*;
import java.io.*;
import java.lang.reflect.Constructor;

/**
 * Class ArchiveManager defines an archive manager that organizes
 * items by date.  As a stand-alone class it implements a very simple
 * archival system (a single file); but it can be extended for more
 * complex archival needs.
 *  7/10/2003 -- [HS,ET]  Initial version.
 *  8/20/2003 -- [ET]  Fixed 'processArchivedItems()' method to respond
 *                     to terminate-processing flag from call-back method.
 *  8/22/2003 -- [ET]  Fixed bug where 'get/processArchivedItems()' would
 *                     always start with second item of first file; added
 *                     'getOldestArchiveFile()', 'getOldestItemInArchiveFile()'
 *                     and 'getOldestItemInEntireArchive()' and
 *                     'getArchiveAccessSyncObj()' methods.
 *   9/4/2003 -- [ET]  Modified to support 'maxCount' limits.
 *   9/8/2003 -- [ET]  Bug fix to 'maxCount' limit in 'processArchivedItems()'.
 *   9/9/2003 -- [KF]  Added 'getArchivedItemsVector' methods.
 *  9/15/2003 -- [KF]  Modified to close the archive output stream(s) before
 *                     deleting files.
 *  9/16/2003 -- [ET]  Removed 'archiveOutStmSyncObj', instead using
 *                     'archiveAccessSyncObj'; setup to have only public
 *                     methods contain 'synchronized' blocks.
 *  9/22/2003 -- [KF]  Add constructors throwing NullPointerException if
 *                     'archiveFileNameStr' is null,
 *                     Add 'getLogPrefixString', 'setLogPrefixString',
 *                     'getLogFile', and 'setLogFile' methods,
 *                     Add a return value to the 'archiveItem' method to
 *                     state if the item was added or not.
 *  9/24/2003 -- [ET]  Added 'checkArchiveItemOrder()' method (commented
 *                     out).
 *  9/25/2003 -- [KF]  Added logging messages of errors and warnings.
 *  9/30/2003 -- [KF]  Added 'setLeaveOutputStreamsOpenFlag' and
 *                     'getLeaveOutputStreamsOpenFlag' methods, and
 *                     added debug logging of stack trace for exceptions.
 *  11/6/2003 -- [KF]  Added logging to various 'set' methods.
 * 11/17/2003 -- [ET]  Changed logging level on various 'set' methods to
 *                     'debug2'.
 *  3/31/2004 -- [ET]  Fixed issue of rejecting added-items with archive
 *                     dates in the future by implementing a tolerance
 *                     value that can be modified by the new methods
 *                     'get/setTimeCheckOffsetMs()'; renamed method
 *                     'saveItemArchiveDate()' to 'checkItemArchiveDate()';
 *                     added explicit initializations to 'null' to
 *                     several member variables.
 *   8/6/2004 -- [KF]  Made global logging use console output if not set.
 * 12/21/2005 -- [ET]  Fixed methods 'getNewestItemInArchiveFile()' and
 *                     'getOldestItemInArchiveFile()' by making them close
 *                     input file when complete.
 *  9/24/2007 -- [ET]  Added 'get/setDontPurgeLastFileFlag()' methods and
 *                     implementation.
 *   7/9/2008 -- [ET]  Fixed 'getNewestItemInArchiveFile()' method so it
 *                     works when only one item is present in archive file.
 *  6/29/2010 -- [ET]  Slight change to error message in method
 *                     'convertStringToArchivable()'.
 */
public class ArchiveManager
{
  /** Constructor for 'Archivable' class used with this manager. */
  protected final Constructor archivedClassConstructor;

  /** Root directory of our archive. */
  protected final String archiveRootDirName;

  /** Name to use for archive file(s). */
  protected final String archiveFileNameStr;

  /** The OutputStream object we're archiving to. */
  protected OutputStream archiveOutStm = null;

  /** Flag set true after 'closeArchive()' called. */
  protected boolean archiveClosedFlag = false;

  /** A string that we delimit archived items by. */
  protected String archivedItemDelimiter = "\n";

  /** Flag set true if the 'archivedItemDelimiter' string is a newline. */
  protected boolean assumeNewlineFlag = true;

  /** true to purge items inside archives, false to only deal with files. */
  protected boolean purgeIntoArchiveFlag = true;

  /** true to not purge last file if none after it. */
  protected boolean dontPurgeLastFileFlag = false;

  /** true to leave the output streams open until the archive is closed. */
  protected boolean leaveOutputStreamsOpenFlag = true;

  /** Prefix that all archived-form items should begin with (or null). */
  protected String archivedFormPrefixStr = null;

  /** Thread synchronization object for archive access. */
  protected final Object archiveAccessSyncObj = new Object();

  /** Vector used if 'getArchiveFilesForDateRange()' not overridden. */
  protected Vector filesForDateRangeVec = null;

  /** Time offset used when checking item-date vs. current time. */
  protected int timeCheckOffsetMs = (int)(UtilFns.MS_PER_HOUR);

  /** The date of the last item archived. */
  protected Date lastItemArchiveDate = null;

  private LogFile logFileObj;
  private String logPrefixString = UtilFns.EMPTY_STRING;

//  protected long chkAcvItemOrderBadCount;   //for 'checkArchiveItemOrder()'
//  protected long chkAcvItemOrderTimeVal;    //for 'checkArchiveItemOrder()'
//  protected long chkAcvItemOrderItemCount;  //for 'checkArchiveItemOrder()'

  /**
   * Creates a new instance of ArchiveManager to archive instances of
   * the given class.  The given class must have a constructor with
   * 'String' and 'Archivable.Marker' parameters, like this:
   * <br>
   * public ArchivableImpl(String dataStr, Archivable.Marker mkrObj) ...
   * <br>
   * This constructor is used to create the class from a string of data.
   * The 'Archivable.Marker' is used to mark the constructor as being
   * available for de-archiving (and will usually be 'null').
   * @param classObj class object representing the class to be archived.
   * @param archiveRootDirName name of root directory in which to place
   * archive files (and possibly directories), or null to use the
   * current working directory.
   * @param archiveFileNameStr name to use for the archive file(s).
   * @throws NoSuchMethodException if a proper constructor does not exist
   * for the class.
   * @throws  NullPointerException
   *          If <code>archiveFileNameStr</code> is <code>null</code>
   */
  public ArchiveManager(Class classObj,String archiveRootDirName,
                                                  String archiveFileNameStr)
                                                throws NoSuchMethodException
  {
    if (archiveFileNameStr == null)
    {
      throw new NullPointerException();
    }
    this.archiveRootDirName = archiveRootDirName;
    this.archiveFileNameStr = archiveFileNameStr;
    archivedClassConstructor = classObj.getConstructor(
                     new Class[] { String.class, Archivable.Marker.class });
    if(archiveRootDirName != null)
    {    //archive root path was given; create any needed directories
      (new File(archiveRootDirName)).mkdirs();
    }
    logFileObj = LogFile.getGlobalLogObj(true); //default to the global log file
    setLogPrefixString("ArchiveManager");   //set the log prefix string
  }

  /**
   * Creates a new instance of ArchiveManager to archive instances of
   * the given class.  The given class must have a constructor with
   * 'String' and 'Archivable.Marker' parameters, like this:
   * <br>
   * public ArchivableImpl(String dataStr, Archivable.Marker mkrObj) ...
   * <br>
   * This constructor is used to create the class from a string of data.
   * The 'Archivable.Marker' is used to mark the constructor as being
   * available for de-archiving (and will usually be 'null').  The
   * archive files will be placed in the current working directory.
   * @param classObj class object representing the class to be archived.
   * @param archiveFileNameStr name to use for the archive file(s).
   * @throws NoSuchMethodException if a proper constructor does not exist
   * for the class.
   * @throws  NullPointerException
   *          If <code>archiveFileNameStr</code> is <code>null</code>
   */
  public ArchiveManager(Class classObj,String archiveFileNameStr)
                                                throws NoSuchMethodException
  {
    this(classObj,null,archiveFileNameStr);
  }

  /**
   * Configures whether or not the 'purgeArchive()' method removes items
   * from inside archive files.  If so then the oldest archive file
   * containing items on or after the purge-cutoff date will be rewritten
   * with items before the purge-cutoff date removed.  (This is the default
   * behavior.)  If not then the archive will be left unchanged (while the
   * older archive files will be deleted as usual).
   * @param flgVal true to configure purging of items inside archive files.
   */
  public void setPurgeIntoArchiveFlag(boolean flgVal)
  {
    synchronized(archiveAccessSyncObj)
    {    //only allow one thread access to the archive at a time
      purgeIntoArchiveFlag = flgVal;
    }
    if(getLogFile() != null)  //if log file exists
    {
      //log debug message
      getLogFile().debug2(getLogPrefixString() +
             ":  purgeIntoArchiveFlag was set to: " + purgeIntoArchiveFlag);
    }
  }

  /**
   * Returns the "purge items inside archives" flag.
   * @return true if items inside archives are purged, false if not.
   */
  public boolean getPurgeIntoArchiveFlag()
  {
    return purgeIntoArchiveFlag;
  }

  /**
   * Configures whether or not the 'purgeArchive()' method will remove
   * the newest archive file whose date is before the purge-cutoff date
   * when the next file in the archive sequence does not exist.  This
   * allows the archive to retain the value of an item that has not
   * changed its value in a relatively-long time.
   * @param flgVal true to configure the archive to not remove the "last"
   * file due to be purged (when not followed by another file); false to
   * purge the file (default).
   */
  public void setDontPurgeLastFileFlag(boolean flgVal)
  {
    synchronized(archiveAccessSyncObj)
    {    //only allow one thread access to the archive at a time
      dontPurgeLastFileFlag = flgVal;
    }
    if(getLogFile() != null)  //if log file exists
    {
      //log debug message
      getLogFile().debug2(getLogPrefixString() +
           ":  dontPurgeLastFileFlag was set to: " + dontPurgeLastFileFlag);
    }
  }

  /**
   * Returns the "purge last archive file" flag.
   * @return true if the archive is configured to not remove the "last"
   * file due to be purged (when not followed by another file); false if
   * configured to purge the file.
   */
  public boolean getDontPurgeLastFileFlag()
  {
    return dontPurgeLastFileFlag;
  }

  /**
   * Configures whether or not the output streams are left open until the
   * archive is closed.
   * @param flgVal true to leave the output streams open until the archive is
   * closed.
   */
  public void setLeaveOutputStreamsOpenFlag(boolean flgVal)
  {
    synchronized(archiveAccessSyncObj)
    {    //only allow one thread access to the archive at a time
      leaveOutputStreamsOpenFlag = flgVal;
    }
    if(getLogFile() != null)  //if log file exists
    {
      //log debug message
      getLogFile().debug2(getLogPrefixString() +
                              ":  leaveOutputStreamsOpenFlag was set to: " +
                                                leaveOutputStreamsOpenFlag);
    }
  }

  /**
   * Gets whether or not the output streams are left open until the
   * archive is closed.
   * @return true if leaving the output streams open until the archive is
   * closed.
   */
  public boolean getLeaveOutputStreamsOpenFlag()
  {
    return leaveOutputStreamsOpenFlag;
  }

  /**
   * Sets the delimiter string to be used in the archive file,
   * to seperate items.  This should be set to a string that will not
   * otherwise be seen in the archived form of any items.
   * @param str the delimiter string to use, or null for the default ("\n").
   */
  public void setArchivedItemDelimiter(String str)
  {
    synchronized(archiveAccessSyncObj)
    {    //only allow one thread access to the archive at a time
      archivedItemDelimiter = (str != null) ? str : "\n";
      assumeNewlineFlag = archivedItemDelimiter.equals("\n");
    }
    if(getLogFile() != null)  //if log file exists
    {
      //log debug message
      getLogFile().debug2(getLogPrefixString() +
           ":  archivedItemDelimiter was set to: " + archivedItemDelimiter);
    }
  }

  /**
   * Returns the delimiter string to be used in the archive file,
   * to seperate items.
   * @return The delimiter string in use.
   */
  public String getArchivedItemDelimiter()
  {
    return archivedItemDelimiter;
  }

  /**
   * Sets a prefix string that all items in their archived form should
   * begin with.  This allows leading extraneous data in the archived-form
   * string data of items to be ignored.  The default is null (no prefix
   * string).
   * @param str the prefix string to use, or null for none.
   */
  public void setArchivedFormPrefixStr(String str)
  {
    synchronized(archiveAccessSyncObj)
    {    //only allow one thread access to the archive at a time
                             //save string; convert empty string to null:
      archivedFormPrefixStr = (str != null && str.length() > 0) ? str : null;
    }
    if(getLogFile() != null)  //if log file exists
    {
      //log debug message
      getLogFile().debug2(getLogPrefixString() +
           ":  archivedFormPrefixStr was set to: " + archivedFormPrefixStr);
    }
  }

  /**
   * Returns the prefix string that all items in their archived form should
   * begin with.
   * @return The prefix string in use, or null if none.
   */
  public String getArchivedFormPrefixStr()
  {
    return archivedFormPrefixStr;
  }

  /**
   * Gets the log file.
   * @return the log file.
   *
   * @see setLogFile, getLogPrefixString
   */
  public LogFile getLogFile()
  {
    return logFileObj;
  }

  /**
   * Sets the log file.
   * @param logFileObj the log file object.
   *
   * @see getLogFile, setLogPrefixString
   */
  public void setLogFile(LogFile logFileObj)
  {
    //only allow one thread access to the archive at a time
    synchronized(archiveAccessSyncObj)
    {
      this.logFileObj = logFileObj;
    }
  }

  /**
   * Returns the prefix to use for log output.
   * @returns the prefix to use for log output.
   *
   * @see setLogPrefixString, getLogFile
   */
  public String getLogPrefixString()
  {
    return logPrefixString;
  }

  /**
   * Sets the prefix to use for log output.
   * @param logPrefixString the prefix to use for log output.
   *
   * @see getLogPrefixString, setLogFile
   */
  public final void setLogPrefixString(String logPrefixString)
  {
    //only allow one thread access to the archive at a time
    synchronized(archiveAccessSyncObj)
    {
      if (logPrefixString != null)
        this.logPrefixString = logPrefixString;
      else
        this.logPrefixString = UtilFns.EMPTY_STRING;
    }
    if(getLogFile() != null)  //if log file exists
    {
      //log debug message
      getLogFile().debug2(getLogPrefixString() +
                  ":  logPrefixString was set to: " + this.logPrefixString);
    }
  }

  /**
   * Sets the time offset used when checking the archive-date of added
   * items against the current system time.  This sets up a tolerance
   * value, in that the archive-date of added items can be at most
   * the specified number of milliseconds in the future.
   * @param offsetMsVal the time offset value, in milliseconds.
   */
  public void setTimeCheckOffsetMs(int offsetMsVal)
  {
    timeCheckOffsetMs = (offsetMsVal >= 0) ? offsetMsVal : 0;
  }

  /**
   * Returns the time offset used when checking the archive-date of added
   * items against the current system time.  This sets up a tolerance
   * value, in that the archive-date of added items can be at most
   * the specified number of milliseconds in the future.
   * @return Returns the time offset used when checking the archive-date
   * of added items against the current system time.
   */
  public int getTimeCheckOffsetMs()
  {
    return timeCheckOffsetMs;
  }

  /**
   * Inserts an item into the archive.
   * @param item the item to be archived.
   * @throws IOException if the archive file can not be written to.
   * @return true if the item was added, false otherwise.
   */
  public boolean archiveItem(Archivable item) throws IOException
  {
    return archiveItem(item, true);
  }

  /**
   * Inserts an item into the archive.
   * @param item the item to be archived.
   * @param checkDateFlag true to check the date.
   * @throws IOException if the archive file can not be written to.
   * @return true if the item was added, false otherwise.
   */
  protected boolean archiveItem(Archivable item, boolean checkDateFlag)
      throws IOException
  {
    boolean itemAdded = false;
    if(!archiveClosedFlag)
    {    //archive not "closed"
      synchronized(archiveAccessSyncObj)
      {  //only allow one thread access to the archive at a time
        IOException throwEx = null;
        try
        {
          //if the item date already saved or the item date saved now
          if (!checkDateFlag || checkItemArchiveDate(item))
          {
            //save the item archive date
            final OutputStream outStm = getOutputStreamForItem(item);
            writeItemToFile(item, outStm);
            outStm.flush();           //flush data out to the file
            itemAdded = true;
          }
        }
        catch (IOException ex)
        {
          throwEx = ex;
        }
        catch(Exception ex)  //some kind of exception error
        {
          if(getLogFile() != null)  //if log file exists
          {
            //log warning message
            getLogFile().warning(getLogPrefixString() +
                                                     ".archiveItem: " + ex);
            getLogFile().debug(UtilFns.getStackTraceString(ex));
          }
        }
        if (!leaveOutputStreamsOpenFlag)  //if not leaving the streams open
        {
          //make sure the archive output streams are closed
          closeArchiveOutStms();
        }
        if (throwEx != null)  //if exception should be thrown
          throw throwEx;      //throw the exception
      }
    }
    return itemAdded;
  }

  /**
   * Validates an item to be added the archive.
   * @param item the item to be archived.
   * @return true if the item date is valid, false otherwise.
   */
  protected boolean checkItemArchiveDate(Archivable item)
  {
    final Date archiveDate = item.getArchiveDate();
    //if the archive date is after the current time
    if (archiveDate.getTime() >
                             System.currentTimeMillis() + timeCheckOffsetMs)
    {
      if(getLogFile() != null)  //if log file exists
      {
        getLogFile().warning(getLogPrefixString() +
                             ":  Archive date (" + archiveDate +
                             ") is after the current time.");
      }
      return false;  //date was not saved
    }
    if (lastItemArchiveDate == null)  //if last item archive date is unknown
    {
      //get the last item in the archive
      final Archivable lastItem = getNewestItemInEntireArchive();
      if (lastItem != null)  //if the item exists
        //save the last item archive date
        lastItemArchiveDate = lastItem.getArchiveDate();
      else
        //set the time to the epoch time
        lastItemArchiveDate = new Date(0);
    }
    //if the archive date is greater than the last item date
    if (archiveDate.compareTo(lastItemArchiveDate) < 0)
    {
      if(getLogFile() != null)  //if log file exists
      {
        getLogFile().warning(getLogPrefixString() +
                             ":  Archive date (" + archiveDate +
                             ") is older than the last item archived (" +
                             lastItemArchiveDate + ")");
      }
      return false;  //date was not saved
    }
    lastItemArchiveDate = archiveDate;  //save the archive date
    return true;  //date was saved
  }

  /**
   * Does the actual writing of the item to the file.
   * @param item the item to write.
   * @param outStm the 'OutputStream' for the archive file to write to.
   * @throws IOException if an error occurs.
   */
  protected void writeItemToFile(Archivable item, OutputStream outStm)
                                                         throws IOException
  {
    outStm.write(item.toArchivedForm().getBytes());
    outStm.write(archivedItemDelimiter.getBytes());
  }

  /**
   * Processes all of the archived items between two date/time values
   * (inclusive).
   * @param startDate the start of the time range, or null to indicate
   * the "beginning of time".
   * @param endDate the end of the time range, or null to indicate
   * the "end of time".
   * @param maxCount the maximum number of objects to be returned,
   * or 0 for no limit.
   * @param callBackObj the call-back object to use to process each item.
   */
  public void processArchivedItems(Date startDate, Date endDate,
                                  ProcessCallBack callBackObj, int maxCount)
  {
    synchronized(archiveAccessSyncObj)
    {    //only allow one thread access to the archive at a time
      int itemCount = 0;
              //get enum of archive files covering the given date range:
      final Enumeration inputFiles = getArchiveFilesForDateRange(
                                             startDate, endDate).elements();
      String itemDataStr;
      Archivable itemObj;
      BufferedReader rdrObj = null;
      boolean firstFileFlag = true;
      boolean moreElementsFlag = inputFiles.hasMoreElements();
      InputStream inStmObj = null;
      File inputFile = null;

      outerLoop:             //label for 'break' statements

      while(!archiveClosedFlag && moreElementsFlag)
      {    //for each archive filename (while archive not closed)
        // a missing archive file isn't an error, it just means there are
        // no archived items for the period represented by the file
        inStmObj = null;
        try
        {
          inputFile = (File)inputFiles.nextElement();
          if (inputFile.exists())  //if input file exists
          {
            try
            {       //get next archive 'File' object and open it for input:
              inStmObj = new BufferedInputStream(
                                            new FileInputStream(inputFile));
            }
            catch(FileNotFoundException ex)
            {
              if(getLogFile() != null)  //if log file exists
              {
                getLogFile().warning(getLogPrefixString() +
                                           ":  Error opening file:  " + ex);
              }
            }
          }
        }
        catch (Exception ex)
        {
          if(getLogFile() != null)  //if log file exists
          {
            getLogFile().warning(getLogPrefixString() +
                                        ":  Error with input file:  " + ex);
            getLogFile().debug(UtilFns.getStackTraceString(ex));
          }
        }

        moreElementsFlag = inputFiles.hasMoreElements();
        if(inStmObj != null)
        {  //archive file stream setup OK
          if(assumeNewlineFlag)     //if assuming newlines then setup reader
            rdrObj = new BufferedReader(new InputStreamReader(inStmObj));
          while(!archiveClosedFlag &&
                (itemDataStr=(assumeNewlineFlag?getNextLineFromFile(rdrObj):
                                getNextItemDataFromFile(inStmObj))) != null)
          {     //for each item in the archive file
            if((itemObj=convertStringToArchivable(itemDataStr)) != null)
            {   //data string converted to item OK
              if(!moreElementsFlag)
              { //currently on the last archive file to be processed
                if(endDate != null && itemObj.getArchiveDate().after(endDate))
                  break outerLoop;     //if past end-time then exit loop
              }
              if(firstFileFlag)
              { //this is the first archive file to be processed
                if(startDate != null &&
                                 itemObj.getArchiveDate().before(startDate))
                {
                  continue;  //if before start-time then skip item
                }
              }                               //process item:
              if(!callBackObj.procArchivedItem(itemObj,itemDataStr))
                break outerLoop;  //if false returned then terminate proc
              if(maxCount > 0 && ++itemCount >= maxCount)
                break outerLoop;  //exit if count limit exists & exceeded
            }
          }

          close(inStmObj);  //close input stream

          inStmObj = null;
        }
        if(firstFileFlag)               //if flag set then
          firstFileFlag = false;        //clear "first" flag
      }
      if(inStmObj != null)
      {  //input file not yet closed
        close(inStmObj);  //close input stream
      }
    }
  }

  /**
   * Processes all of the archived items between two date/time values
   * (inclusive).
   * @param startDate the start of the time range, or null to indicate
   * the "beginning of time".
   * @param endDate the end of the time range, or null to indicate
   * the "end of time".
   * @param callBackObj the call-back object to use to process each item.
   */
  public void processArchivedItems(Date startDate, Date endDate,
                                                ProcessCallBack callBackObj)
  {
    processArchivedItems(startDate,endDate,callBackObj,0);
  }

  /**
   * Returns all of the archived items between two date/time values
   * (inclusive).
   * @param startDate the start of the time range, or null to indicate
   * the "beginning of time".
   * @param endDate the end of the time range, or null to indicate
   * the "end of time".
   * @param maxCount the maximum number of objects to be returned,
   * or 0 for no limit.
   * @return A vector of 'Archivable' objects with dates occuring
   * between the two times (inclusive).
   */
  public Vector getArchivedItemsVector(Date startDate, Date endDate,
                                                               int maxCount)
  {
         //process each item by adding it to the Vector:
    final Vector retVec = new Vector();
    processArchivedItems(startDate, endDate, (new ProcessCallBack()
        {
          public boolean procArchivedItem(Archivable itemObj,
                                                     String archivedFormStr)
          {
            retVec.add(itemObj);
            return true;
          }
        }), maxCount);
    return retVec;          //return vector of items
  }

  /**
   * Returns all of the archived items between two date/time values
   * (inclusive).
   * @param startDate the start of the time range, or null to indicate
   * the "beginning of time".
   * @param endDate the end of the time range, or null to indicate
   * the "end of time".
   * @return A vector of 'Archivable' objects with dates occuring
   * between the two times (inclusive).
   */
  public Vector getArchivedItemsVector(Date startDate, Date endDate)
  {
    return getArchivedItemsVector(startDate,endDate,0);
  }

  /**
   * Returns all of the archived items between two date/time values
   * (inclusive).
   * @param startDate the start of the time range, or null to indicate
   * the "beginning of time".
   * @param endDate the end of the time range, or null to indicate
   * the "end of time".
   * @param maxCount the maximum number of objects to be returned,
   * or 0 for no limit.
   * @return An enumeration of 'Archivable' objects with dates occuring
   * between the two times (inclusive).
   */
  public Enumeration getArchivedItems(Date startDate, Date endDate,
                                                               int maxCount)
  {
    return getArchivedItemsVector(startDate,endDate,maxCount).elements();
  }

  /**
   * Returns all of the archived items between two date/time values
   * (inclusive).
   * @param startDate the start of the time range, or null to indicate
   * the "beginning of time".
   * @param endDate the end of the time range, or null to indicate
   * the "end of time".
   * @return An enumeration of 'Archivable' objects with dates occuring
   * between the two times (inclusive).
   */
  public Enumeration getArchivedItems(Date startDate, Date endDate)
  {
    return getArchivedItems(startDate,endDate,0);
  }

  /**
   * Returns the data for next item from the archive file.
   * @param stmObj the input stream to read archive-file data from.
   * @return The string data for the next item, or null if
   * no more exist.
   */
  protected String getNextItemDataFromFile(InputStream stmObj)
  {
    final StringBuffer archivedFormBuff = new StringBuffer();
    int val,itemSizeVal;
    try
    {
      do
      {       //for each character parsed
        if((val=stmObj.read()) < 0)
          return null;                //if end-of-file then return null
        archivedFormBuff.append((char)val);      //add char to buffer
              //calculate size of data in  buffer without delimiter:
        itemSizeVal = archivedFormBuff.length() -
                                             archivedItemDelimiter.length();
      }                           //loop if delimiter not found:
      while(itemSizeVal < 0 || !archivedItemDelimiter.equals(
                                  archivedFormBuff.substring(itemSizeVal)));
    }
    catch(IOException ex)
    {
      if(getLogFile() != null)  //if log file exists
      {
        getLogFile().warning(getLogPrefixString() +
                             ":  I/O error getting next item data:  " + ex);
      }
      return null;
    }
    catch(Exception ex)
    {
      if(getLogFile() != null)  //if log file exists
      {
        getLogFile().warning(getLogPrefixString() +
                                 ":  Error getting next item data:  " + ex);
        getLogFile().debug(UtilFns.getStackTraceString(ex));
      }
      return null;
    }
    return archivedFormBuff.substring(0, itemSizeVal);
  }

  /**
   * Returns the next line of data from the given stream.
   * @param rdrObj the input stream to read data from.
   * @return The string data for the next line, or null if
   * no more are available or an error occurred.
   */
  protected String getNextLineFromFile(BufferedReader rdrObj)
  {
    try
    {
      return rdrObj.readLine();
    }
    catch(IOException ex)
    {
      if(getLogFile() != null)  //if log file exists
      {
        getLogFile().warning(getLogPrefixString() +
                                  ":  I/O error getting next line:  " + ex);
      }
      return null;
    }
    catch(Exception ex)
    {
      if(getLogFile() != null)  //if log file exists
      {
        getLogFile().warning(getLogPrefixString() +
                                      ":  Error getting next line:  " + ex);
        getLogFile().debug(UtilFns.getStackTraceString(ex));
      }
      return null;
    }
  }

  /**
   * Converts the given string to an 'Archivable' object.
   * @param str the string to convert.
   * @return An 'Archivable' object, or null if the string could not be
   * converted.
   */
  protected Archivable convertStringToArchivable(String str)
  {
                        //trim any whitespace in string:
    if(str != null && (str=str.trim()).length() > 0)
    {    //string contains non-whitespace data
      final int p;
      if(archivedFormPrefixStr != null)
      {  //prefix is expected; find start position of prefix in data string
        if((p=str.indexOf(archivedFormPrefixStr)) < 0)
          return null;       //if prefix not found then return null
      }
      else    //prefix not expected
        p = 0;          //use beginning of string
      try
      {       //create and return new instance of item:
        return (Archivable)(archivedClassConstructor.newInstance(
               new Object[] { str.substring(p), (Archivable.Marker)null }));
      }
      catch (Exception ex)
      {       //some kind of exception error; return null
        if(getLogFile() != null)  //if log file exists
        {
          getLogFile().warning(getLogPrefixString() +
                                    ":  Error converting string (\"" + str +
                                             "\") to archive item:  " + ex);
          getLogFile().debug(UtilFns.getStackTraceString(ex));
        }
      }
    }
    return null;
  }

  /**
   * Purges items in the archive.
   * @param startDate the start of the time range, or null to indicate
   * the "beginning of time".
   * @param endDate the end of the time range, or null to indicate
   * the "end of time".
   */
  public void purgeArchive(Date startDate, Date endDate)
  {
    synchronized(archiveAccessSyncObj)
    {
      final Enumeration inputFiles = getArchiveFilesForDateRange(
          startDate, endDate).elements();
      while(!archiveClosedFlag && inputFiles.hasMoreElements())
      {
        try
        {
          deleteFile((File)inputFiles.nextElement());
        }
        catch(Exception ex)
        {        //some kind of exception
          if(getLogFile() != null)  //if log file exists
          {
            getLogFile().warning(getLogPrefixString() +
                                        ":  Error deleting file:  " + ex);
          }
        }
      }
    }
  }

  /**
   * Purges items and archive files with an archive date older than the
   * given cutoff date.  The 'purgeIntoArchiveFlag' configures whether
   * or not items from inside archive files are removed.  If 'true' then
   * the oldest archive file containing items on or after the purge-cutoff
   * date will be rewritten with items before the purge-cutoff date removed.
   * If 'false' then the archive will be left unchanged (while the older
   * archive files will be deleted as usual).
   * @param cutoffDate the date of the oldest event to keep in the archive.
   * @return true if successful, false if an error occurred.
   */
  public boolean purgeArchive(Date cutoffDate)
  {
    synchronized(archiveAccessSyncObj)
    {    //only allow one thread access to the archive at a time
      boolean retFlag = true;
         //get archive file that would contain items at cutoff date:
      File cutoffFileObj =
               getArchiveFileForName(getArchiveFileNameForDate(cutoffDate));
      if(!cutoffFileObj.exists())      //if file does not exist then
        cutoffFileObj = null;          //clear handle to indicate

      File inFileObj;
      String itemDataStr;
      Archivable itemObj;
      BufferedReader rdrObj = null;
           //get enum of archive files at or before the cutoff date:
      final Enumeration e =
                   getArchiveFilesForDateRange(null, cutoffDate).elements();
      boolean moreElementsFlag = e.hasMoreElements();
      while(moreElementsFlag)
      {    //for each matching archive file
        inFileObj = (File)(e.nextElement());
        if(!(moreElementsFlag=e.hasMoreElements()) &&
                   cutoffFileObj != null && cutoffFileObj.equals(inFileObj))
        {  //last file in enum and file contains cutoff date
                //this is the "newest" file; we have to scan through it
                // and only keep the elements newer than the cutoff date:
          if(purgeIntoArchiveFlag)
          {     //flag set to allow purging inside of archive file
            try
            {     // copy the elements over; create temporary file for output
              final File tmpFileObj = getArchiveFileForName(
                               ("t" + System.currentTimeMillis() + ".tmp"));
                            //setup output stream to temporary file:
              final OutputStream outStmObj = new BufferedOutputStream(
                                          new FileOutputStream(tmpFileObj));
                            //setup input stream from archive file:
              final InputStream inStmObj = new BufferedInputStream(
                                            new FileInputStream(inFileObj));
              if(assumeNewlineFlag) //if assuming newlines then setup reader
                rdrObj = new BufferedReader(new InputStreamReader(inStmObj));
              boolean skippedFlag = false;
              boolean writtenFlag = false;
              while((itemDataStr=
                             (assumeNewlineFlag?getNextLineFromFile(rdrObj):
                                getNextItemDataFromFile(inStmObj))) != null)
              {   //for each item in archive file
                if((itemObj=convertStringToArchivable(itemDataStr)) != null)
                { //data string converted to item OK
                       //if not before cutoff date then write to temp file:
                  if(!itemObj.getArchiveDate().before(cutoffDate))
                  {
                    writtenFlag = true;         //indicate item written
                    writeItemToFile(itemObj, outStmObj);
                  }
                  else                          //if after cutoff date then
                  {
                    skippedFlag = true;         //indicate item skipped
                  }
                }
              }

              flush(outStmObj);  //flush data out to the file

              close(outStmObj);  //close output stream

              close(inStmObj);  //close input stream

              if(skippedFlag)
              { //at least one item skipped
                                        //get absolute path to archive file:
                final File fileObj = new File(inFileObj.getAbsolutePath());
                //if old archive file was deleted and temp archive has data
                if (deleteFile(inFileObj) && writtenFlag)
                  //rename the temp to the archive file name:
                  renameTo(tmpFileObj,fileObj);
                else
                  deleteFile(tmpFileObj);   //delete the temporary file
              }
              else   //no items were skipped; leave the archive as-is
                deleteFile(tmpFileObj);   //delete the temporary file
            }
            catch (Exception ex)
            {
              if(getLogFile() != null)  //if log file exists
              {
                getLogFile().warning(getLogPrefixString() +
                                ":  Error with purge into archive:  " + ex);
                getLogFile().debug(UtilFns.getStackTraceString(ex));
              }
              retFlag = false;      //clear return flag to indicate error
              break;                //exit loop (and method)
            }
          }
        }
        else
        {     //not last file in enum or file does not contain cutoff date
                   //if flag set and this is the last file to be purged
                   // and next file in sequence does not exist then
                   // don't purge this last file:
          if(dontPurgeLastFileFlag && !moreElementsFlag &&
                             !doesNextArchiveFileExist(inFileObj.getName()))
          {
            break;                //exit loop (and method)
          }
          try
          {
            deleteFile(inFileObj);
          }
          catch(Exception ex)
          {        //some kind of exception
            if(getLogFile() != null)  //if log file exists
            {
              getLogFile().warning(getLogPrefixString() +
                                          ":  Error deleting file:  " + ex);
              getLogFile().debug(UtilFns.getStackTraceString(ex));
            }
            retFlag = false;      //clear return flag to indicate error
            break;                //exit loop (and method)
          }
        }
      }
      return retFlag;
    }
  }

  /**
   * Deletes the file.
   * @param fileObj the file object.
   * @return  <code>true</code> if and only if the file is
   *          successfully deleted; <code>false</code> otherwise
   */
  protected boolean deleteFile(File fileObj)
  {
    closeArchiveOutStms();  //close the archive output streams
    if (!fileObj.delete())  //if file was not deleted
    {
      if(getLogFile() != null)  //if log file exists
      {
        getLogFile().warning(getLogPrefixString() +
                    ":  Unable to delete file (" + fileObj.getName() + ")");
      }
      return false;
    }
    return true;
  }

  /**
   * Renames the source file to the destination file.
   * @param fileObj the file object.
   * @param dest the new file name.
   * @return  <code>true</code> if and only if the renaming succeeded;
   *          <code>false</code> otherwise
   */
  protected boolean renameTo(File fileObj,File dest)
  {
    if (!fileObj.renameTo(dest))
    {
      if(getLogFile() != null)  //if log file exists
      {
        getLogFile().warning(getLogPrefixString() +
                         ":  Unable to rename file \"" + fileObj.getName() +
                                        "\" to \"" + dest.getName() + "\"");
      }
      return false;
    }
    return true;
  }

  /**
   * Flushes the specified output stream.
   * @param outStmObj the output stream to flush.
   */
  protected void flush(OutputStream outStmObj)
  {
    //flush output stream
    try
    {
      outStmObj.flush();
    }
    catch(IOException ex)
    {
      if(getLogFile() != null)  //if log file exists
      {
        getLogFile().warning(getLogPrefixString() +
                             ":  I/O error flushing output stream:  " + ex);
      }
    }
  }

  /**
   * Closes the specified input stream.
   * @param inStmObj the input stream to close.
   */
  protected void close(InputStream inStmObj)
  {
    //close input stream
    try
    {
      inStmObj.close();
    }
    catch(IOException ex)
    {
      if(getLogFile() != null)  //if log file exists
      {
        getLogFile().warning(getLogPrefixString() +
                               ":  I/O error closing input stream:  " + ex);
      }
    }
  }

  /**
   * Closes the specified output stream.
   * @param outStmObj the output stream to close.
   */
  protected void close(OutputStream outStmObj)
  {
    //close output stream
    try
    {
      outStmObj.close();
    }
    catch(IOException ex)
    {
      if(getLogFile() != null)  //if log file exists
      {
        getLogFile().warning(getLogPrefixString() +
                              ":  I/O error closing output stream:  " + ex);
      }
    }
  }

  /**
   * Close the archive output streams.
   */
  protected void closeArchiveOutStms()
  {
    if(archiveOutStm != null)
    {  //output stream is allocated
      close(archiveOutStm);  //close output stream
      archiveOutStm = null;               //release object
    }
  }

  /**
   * Releases resources allocated for this archive manager.
   */
  public void closeArchive()
  {
    synchronized(archiveAccessSyncObj)
    {    //only allow one thread access to the archive at a time
      archiveClosedFlag = true;        //indicate archive "closed"
      closeArchiveOutStms();           //close the archive output streams
    }
  }

  /**
   * Returns the "closed" status of this archive manager.
   * @return true if closed, false if not.
   */
  public boolean isArchiveClosed()
  {
    return archiveClosedFlag;
  }

  /**
   * Returns the newest archive file available.
   * @return A 'File' object for the newest archive file available,
   * or 'null' if none available.
   */
  public File getNewestArchiveFile()
  {
    synchronized(archiveAccessSyncObj)
    {    //only allow one thread access to the archive at a time
         //get Vector of all archive files available:
      final Vector vec = getArchiveFilesForDateRange(null,null);
      int i;
      if(vec != null && (i=vec.size()) > 0)
      {    //Vector with elements was returned
        Object obj;
        do
        {       //find last 'File' object in Vector:
          if((obj=vec.elementAt(--i)) instanceof File)
            return (File)obj;       //return 'File' object
        }
        while(i > 0);
      }
      return null;        //no 'File' object found
    }
  }

  /**
   * Returns the oldest archive file available.
   * @return A 'File' object for the oldest archive file available,
   * or 'null' if none available.
   */
  public File getOldestArchiveFile()
  {
    synchronized(archiveAccessSyncObj)
    {    //only allow one thread access to the archive at a time
         //get Vector of all archive files available:
      final Vector vec = getArchiveFilesForDateRange(null,null);
      if(vec != null)
      {    //Vector with elements was returned
        final int vecSize = vec.size();
        Object obj;
        for(int i=0; i<vecSize; ++i)
        {       //find first 'File' object in Vector:
          if((obj=vec.elementAt(i)) instanceof File)
            return (File)obj;       //return 'File' object
        }
      }
      return null;        //no 'File' object found
    }
  }

  /**
   * Returns the last 'Archivable' item in the given archive file.
   * @param fileObj the file to use.
   * @return An 'Archivable' object, or null if none could be found.
   */
  public Archivable getNewestItemInArchiveFile(File fileObj)
  {
    synchronized(archiveAccessSyncObj)
    {    //only allow one thread access to the archive at a time
      //if null handle or file does not exist
      if(fileObj == null || !fileObj.exists())
        return null;           //return null for no-item
      final int bufferSize = 25;
                     //circular buffer to hold last items in file:
      final String [] buff = new String[bufferSize];
      int pos = 0, itemCount = 0;
      String itemDataStr;
      Archivable itemObj = null;
      final InputStream inStmObj;
      try
      {         //open archive 'File' for input:
        inStmObj = new BufferedInputStream(new FileInputStream(fileObj));
      }
      catch(IOException ex)
      {         //error opening file
        if(getLogFile() != null)  //if log file exists
        {
          getLogFile().warning(getLogPrefixString() +
                                       ":  I/O error opening file:  " + ex);
        }
        return null;           //return null for no-item
      }
      BufferedReader rdrObj = null;
      if(assumeNewlineFlag)     //if assuming newlines then setup reader
        rdrObj = new BufferedReader(new InputStreamReader(inStmObj));
      while((itemDataStr=(assumeNewlineFlag?getNextLineFromFile(rdrObj):
                                  getNextItemDataFromFile(inStmObj))) != null)
      {    //for each item in file
        if(archivedFormPrefixStr == null ||
                              itemDataStr.indexOf(archivedFormPrefixStr) >= 0)
        {  //no prefix string or prefix string found OK
          buff[pos] = itemDataStr;       //save string in circular buffer
          if(++pos >= bufferSize)   //increment to next position
            pos = 0;                //if past end then wrap to beginning
          ++itemCount;                   //increment item count
        }
      }
      close(inStmObj);              //close input file
      if(itemCount > bufferSize)    //if more items than buffer size then
        itemCount = bufferSize;     //set to buffer size for next loop
           //check items in circular buffer, starting with the last item,
           // for one that successfully converts to an 'Archivable' object:
      while(--itemCount >= 0)
      {    //for each item in buffer
        if(--pos < 0)               //decrement to previous position
          pos = bufferSize - 1;     //if past beginning then wrap to end
        if((itemObj=convertStringToArchivable(buff[pos])) != null)
          break;          //if item data converts OK then exit loop
      }
      return itemObj;               //return item (or null if none)
    }
  }

  /**
   * Returns the last 'Archivable' item available in the entire archive.
   * @return An 'Archivable' object, or null if none could be found.
   */
  public Archivable getNewestItemInEntireArchive()
  {
    synchronized(archiveAccessSyncObj)
    {    //only allow one thread access to the archive at a time
      return getNewestItemInArchiveFile(getNewestArchiveFile());
    }
  }

  /**
   * Returns the first 'Archivable' item in the given archive file.
   * @param fileObj the file to use.
   * @return An 'Archivable' object, or null if none could be found.
   */
  public Archivable getOldestItemInArchiveFile(File fileObj)
  {
    synchronized(archiveAccessSyncObj)
    {    //only allow one thread access to the archive at a time
      //if null handle or file does not exist
      if(fileObj == null || !fileObj.exists())
        return null;           //return null for no-item
      String itemDataStr;
      Archivable itemObj;
      final InputStream inStmObj;
      try
      {         //open archive 'File' for input:
        inStmObj = new BufferedInputStream(new FileInputStream(fileObj));
      }
      catch(IOException ex)
      {         //error opening file
        if(getLogFile() != null)  //if log file exists
        {
          getLogFile().warning(getLogPrefixString() +
                                       ":  I/O error opening file:  " + ex);
        }
        return null;           //return null for no-item
      }
      BufferedReader rdrObj = null;
      if(assumeNewlineFlag)     //if assuming newlines then setup reader
        rdrObj = new BufferedReader(new InputStreamReader(inStmObj));
      while((itemDataStr=(assumeNewlineFlag?getNextLineFromFile(rdrObj):
                                  getNextItemDataFromFile(inStmObj))) != null)
      {    //for each item in file
        if((archivedFormPrefixStr == null ||
                           itemDataStr.indexOf(archivedFormPrefixStr) >= 0) &&
                     (itemObj=convertStringToArchivable(itemDataStr)) != null)
        {  //no prefix string or prefix string found OK and item converted OK
          close(inStmObj);          //close input file
          return itemObj;           //return item
        }
      }
      close(inStmObj);              //close input file
      return null;
    }
  }

  /**
   * Returns the first 'Archivable' item available in the entire archive.
   * @return An 'Archivable' object, or null if none could be found.
   */
  public Archivable getOldestItemInEntireArchive()
  {
    synchronized(archiveAccessSyncObj)
    {    //only allow one thread access to the archive at a time
      return getOldestItemInArchiveFile(getOldestArchiveFile());
    }
  }

  /**
   * Returns the Object used to thread-synchronize the archive.
   * @return The Object used to thread-synchronize the archive.
   */
  public Object getArchiveAccessSyncObj()
  {
    return archiveAccessSyncObj;
  }

  /**
   * Given 2 dates (inclusive), returns an enumeration of File objects that
   * can be used to later create a stream.  This should be overridden to
   * support more complex archive filesystems.  These files should be in
   * order from oldest to newest.
   * @param startDate the start of the time range, or null to indicate
   * the "beginning of time".
   * @param endDate the end of the time range, or null to indicate
   * the "end of time".
   * @return A Vector of 'File' objects.  The order of the 'File' objects as
   * they are read from the Enumeration should be chronological.
   */
  protected Vector getArchiveFilesForDateRange(Date startDate, Date endDate)
  {
    if(filesForDateRangeVec == null)
    {    //Vector not previously created
      filesForDateRangeVec = new Vector();       //create Vector
                                                 //add single 'File' object:
      filesForDateRangeVec.add(getArchiveFileForName(archiveFileNameStr));
    }
    return filesForDateRangeVec;       //return Vector with 'File' object
  }

  /**
   * Returns the name of the archive file that would contain items with
   * the given date.
   * @param dateObj the date object to use.
   * @return The file name string.
   */
  protected String getArchiveFileNameForDate(Date dateObj)
  {
    return archiveFileNameStr;
  }

  /**
   * Returns the file object for the given file name.
   * @param fNameStr the file name to use.
   * @return A new 'File' object for the given file name, placed in the
   * proper directory.
   */
  protected File getArchiveFileForName(String fNameStr)
  {
    return new File(archiveRootDirName,fNameStr);
  }

  /**
   * Returns an output stream for the file that the item should be
   * archived to.
   * @param item the item to be archived.
   * @return An 'OutputStream' to be used to write to the archive file.
   * @throws IOException if the archive file cannot be opened.
   */
  protected OutputStream getOutputStreamForItem(Archivable item)
                                                          throws IOException
  {
    if(archiveOutStm == null)
    {  //output stream not previously setup
                 //setup output stream to file, opened for append:
      archiveOutStm = new BufferedOutputStream(new FileOutputStream(
               getArchiveFileForName(archiveFileNameStr).getPath(),true));
    }
    return archiveOutStm;
  }

  /**
   * Determines if the next file in the archive sequence exists.
   * Subclasses should override this method (this version always
   * returns 'true').
   * @param fNameStr name of archive file.
   * @return true if the next file in the archive sequence exists (or
   * a date code could not be parsed from the given filename); false
   * otherwise.
   */
  protected boolean doesNextArchiveFileExist(String fNameStr)
  {
    return true;
  }

  /**
   * Checks all items in the archive to see if they are in time order.
   * @return The number of items out of order.
   */
/*
  public long checkArchiveItemOrder()
  {
    try
    {
      synchronized(archiveAccessSyncObj)
      {  //only allow one thread access to the archive at a time
        chkAcvItemOrderBadCount = chkAcvItemOrderTimeVal =
                                               chkAcvItemOrderItemCount = 0;
              //process all items in archive:
        processArchivedItems(null, null, new ProcessCallBack()
            {
              public boolean procArchivedItem(Archivable itemObj,
                                                     String archivedFormStr)
              {
                ++chkAcvItemOrderItemCount;      //increment item count
                final long itemTimeVal = itemObj.getArchiveDate().getTime();
                if(chkAcvItemOrderTimeVal > 0 &&
                                       itemTimeVal < chkAcvItemOrderTimeVal)
                {  //not first item and time less then previous item
                  ++chkAcvItemOrderBadCount;     //increment "bad" count
                  logFileObj.debug(
                               "ArchiveManager.checkArchiveItemOrder() \"" +
                                          archiveRootDirName + "\" item #" +
                              chkAcvItemOrderItemCount + " out of order (" +
                        chkAcvItemOrderBadCount + "):  " + archivedFormStr);
                }
                chkAcvItemOrderTimeVal = itemTimeVal;
                return true;
              }
            });
        if(chkAcvItemOrderBadCount > 0)
        {     //out-of-order items detected; log warning message
          logFileObj.warning("ArchiveManager.checkArchiveItemOrder() " +
                                        "detected \"" + archiveRootDirName +
                                           "\" items out of order, count=" +
                                 chkAcvItemOrderBadCount + ", totalItems=" +
                                                  chkAcvItemOrderItemCount);
        }
        else
        {
          logFileObj.debug3("ArchiveManager.checkArchiveItemOrder() " +
                               "verified order of \"" + archiveRootDirName +
               "\" items OK (totalItems=" + chkAcvItemOrderItemCount + ")");
        }
        return chkAcvItemOrderBadCount;
      }
    }
    catch(Exception ex)
    {
      logFileObj.warning("ArchiveManager.checkArchiveItemOrder() error:  " +
                                                                        ex);
      return 0;
    }
  }
*/


  /**
   * Interface ProcessCallBack defines the call-back object passed into
   * the 'processArchivedItems()' method.
   */
  public interface ProcessCallBack
  {
    /**
     * Processes the archived-item object.
     * @param itemObj the item to be processed.
     * @param archivedFormStr the archived-form data string for the item.
     * @return true if processing is to continue, false if processing
     * should be terminated.
     */
    public boolean procArchivedItem(Archivable itemObj,
                                                    String archivedFormStr);
  }
}
