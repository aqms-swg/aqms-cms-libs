//Averager.java:  Implements a "windowed" average -- 7/12/2000 -- [ET]
//
//
//=====================================================================
// Copyright (C) 2000 Instrumental Software Technologies, Inc.
//
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions
// are met:
// 1. Redistributions of source code, or portions of this source code,
//    must retain the above copyright notice, this list of conditions
//    and the following disclaimer.
// 2. Redistributions in binary form must reproduce the above copyright
//    notice, this list of conditions and the following disclaimer in
//    the documentation and/or other materials provided with the
//    distribution.
// 3. All advertising materials mentioning features or use of this
//    software must display the following acknowledgment:
//    "This product includes software developed by Instrumental
//    Software Technologies, Inc. (http://www.isti.com)"
// 4. If the software is provided with, or as part of a commercial
//    product, or is used in other commercial software products the
//    customer must be informed that "This product includes software
//    developed by Instrumental Software Technologies, Inc.
//    (http://www.isti.com)"
// 5. The names "Instrumental Software Technologies, Inc." and "ISTI"
//    must not be used to endorse or promote products derived from
//    this software without prior written permission. For written
//    permission, please contact "info@isti.com".
// 6. Products derived from this software may not be called "ISTI"
//    nor may "ISTI" appear in their names without prior written
//    permission of Instrumental Software Technologies, Inc.
// 7. Redistributions of any form whatsoever must retain the following
//    acknowledgment:
//    "This product includes software developed by Instrumental
//    Software Technologies, Inc. (http://www.isti.com/)."
// THIS SOFTWARE IS PROVIDED BY INSTRUMENTAL SOFTWARE
// TECHNOLOGIES, INC. "AS IS" AND ANY EXPRESSED OR IMPLIED
// WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES
// OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
// DISCLAIMED.  IN NO EVENT SHALL INSTRUMENTAL SOFTWARE TECHNOLOGIES,
// INC. OR ITS CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
// INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
// (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
// SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
// HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
// STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
// ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED
// OF THE POSSIBILITY OF SUCH DAMAGE.
//=====================================================================
//  A current version of the software can be found at
//                http://www.isti.com
//  Bug reports and comments should be directed to
//  Instrumental Software Technologies, Inc. at info@isti.com
//=====================================================================
//

package com.isti.util;

/**
 * Class Averager implements a "windowed" average, where the average of
 * the last 'n' values entered is calculated.
 */
public class Averager
{
  private final long [] valuesArray;        //array of values in "window"
  private final int windowSize;             //size of array "window"
  private long valuesTotal;                 //total of values in "window"
  private int valuesCount;                  //number of values in "window"
  private int valuesPos;                    //current position in "window"

    /**
     * Creates a "windowed" averaging object.
     * @param windowSize the number of values to track and use when
     * computing the average.  If the value is not greater than zero
     * then a runtime exception is thrown.
     */
  public Averager(int windowSize)
  {
    if(windowSize <= 0)                     //if bad "window" size then
      throw new RuntimeException(           //throw runtime exception
                        "Parameter 'windowSize' must be greater than zero");
    this.windowSize = windowSize;           //set "window" size
    valuesArray = new long[windowSize];     //allocate array for values
    clear();                                //set all values to zero
  }

    /**
     * Clears the averaging "window" back to its initial state.  All
     * values are set to zero.
     */
  public void clear()
  {
    for(int p=0; p<windowSize; ++p)         //clear values array
      valuesArray[p] = 0L;
    valuesTotal = 0L;                       //init total
    valuesCount = valuesPos = 0;            //init count and position
  }

    /**
     * Enters the new value into the averaging "window".
     * @param val the value to be entered.
     * @return The new calculated average.
     */
  public long enter(long val)
  {
    valuesTotal -= valuesArray[valuesPos];            //sub previous value
    valuesTotal += (valuesArray[valuesPos] = val);    //add & save new value
    if(++valuesPos >= windowSize)      //increment position in "window"
      valuesPos = 0;                   //if past end then wrap-around to beg
    if(valuesCount < windowSize)       //if "window" not yet full then
      ++valuesCount;                   //increment current count
    return getAverage();               //return new average
  }

    /**
     * Returns the current average of values in the "window".
     * @return  the current average of values in the "window".
     */
  public long getAverage()
  {
    return (valuesCount > 0) ? valuesTotal/valuesCount : 0L;
  }
}

