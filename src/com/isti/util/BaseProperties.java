//BaseProperties.java:  Base class for classes the manage a table of
//                      configuration property items.
//
//   7/12/2000 -- [ET]  Initial release version.
//  11/28/2000 -- [ET]  Small documentation change.
//   8/13/2001 -- [ET]  Moved 'load()' and associated methods from the
//                      'CfgProperties' class to support using URL paths
//                      to configuration files by applets.
//    2/6/2002 -- [ET]  Modified to read multiple tokens to end-of-line
//                      after each equals sign.
//    4/4/2001 -- [ET]  Added support for the 'set/getCmdLnLoadedFlag()'
//                      methods in 'CfgPropItem'.
//   4/17/2001 -- [ET]  Moved 'store()' and 'getDisplayString()' methods
//                      from 'CfgProperties' to here; added 'equalsStr'
//                      and 'newlineStr' parameters to 'store()'; added
//                      'setLoadedFlags()' method.
//   6/30/2002 -- [ET]  Added support for 'CfgPropItem' validator objects
//                      in 'load()'.
//  10/29/2002 -- [KF]  Modified to use Map interface with 'itemsTable';
//                      made 'itemsTable' private (use Map interface to
//                      access).
//   5/29/2003 -- [ET]  Added 'setAllToDefaults()' method.
//   5/30/2003 -- [ET]  Added support for 'ignoreItemFlag' for items in
//                      'store()'.
//  10/10/2003 -- [KF]  Added 'remove(CfgPropItem)' method.
//   11/5/2003 -- [ET]  Added 'load(String)' method.
//    1/8/2004 -- [KF]  Added default option to the 'load' methods.
//   1/21/2004 -- [ET]  Added version of 'getDisplayString()' with
//                      additional parameters.
//   2/13/2004 -- [ET,KF]  Modified 'load()' and 'store()' methods to implement
//                      "emptyStringDefault" flag setting on items.
//    8/5/2004 -- [KF]  Added 'SEPARATOR_STRING', 'SPECIAL_CHARS_STR' and support
//                      for 'PropertyEditorInformation.isValueStringQuoted'.
//  12/19/2005 -- [KF]  Changed to use the 'toArchivedForm()' method to support
//                      other locales.
//  10/30/2019 -- [KF]  Added 'getNoSaveItemFlag()' method.
//   3/16/2021 -- [KF]  Added 'load(InputStream,LoadSettingsType)',
//                     'getEmptyStringDefaultFlag' and
//                     'setEmptyStringDefaultFlag' methods.
//
//
//=====================================================================
// Copyright (C) 2021 Instrumental Software Technologies, Inc.
//
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions
// are met:
// 1. Redistributions of source code, or portions of this source code,
//    must retain the above copyright notice, this list of conditions
//    and the following disclaimer.
// 2. Redistributions in binary form must reproduce the above copyright
//    notice, this list of conditions and the following disclaimer in
//    the documentation and/or other materials provided with the
//    distribution.
// 3. All advertising materials mentioning features or use of this
//    software must display the following acknowledgment:
//    "This product includes software developed by Instrumental
//    Software Technologies, Inc. (http://www.isti.com)"
// 4. If the software is provided with, or as part of a commercial
//    product, or is used in other commercial software products the
//    customer must be informed that "This product includes software
//    developed by Instrumental Software Technologies, Inc.
//    (http://www.isti.com)"
// 5. The names "Instrumental Software Technologies, Inc." and "ISTI"
//    must not be used to endorse or promote products derived from
//    this software without prior written permission. For written
//    permission, please contact "info@isti.com".
// 6. Products derived from this software may not be called "ISTI"
//    nor may "ISTI" appear in their names without prior written
//    permission of Instrumental Software Technologies, Inc.
// 7. Redistributions of any form whatsoever must retain the following
//    acknowledgment:
//    "This product includes software developed by Instrumental
//    Software Technologies, Inc. (http://www.isti.com/)."
// THIS SOFTWARE IS PROVIDED BY INSTRUMENTAL SOFTWARE
// TECHNOLOGIES, INC. "AS IS" AND ANY EXPRESSED OR IMPLIED
// WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES
// OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
// DISCLAIMED.  IN NO EVENT SHALL INSTRUMENTAL SOFTWARE TECHNOLOGIES,
// INC. OR ITS CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
// INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
// (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
// SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
// HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
// STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
// ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED
// OF THE POSSIBILITY OF SUCH DAMAGE.
//=====================================================================
//  A current version of the software can be found at
//                http://www.isti.com
//  Bug reports and comments should be directed to
//  Instrumental Software Technologies, Inc. at info@isti.com
//=====================================================================
//

package com.isti.util;

import java.util.*;
import java.io.*;
import com.isti.util.propertyeditor.PropertyEditorInformation;

/**
 * Class BaseProperties is the base class for classes that manage a table
 * of configuration property items.  The 'CfgProperties' class extends this
 * class to add stream loading and saving.  The 'AppletProperties' class
 * adds loading of properties as parameters from a host HTML file.
 *
 * Each item has a name, a value, a default value, a loaded-flag (set true
 * if the item has been "loaded" from an input stream), and a required-flag
 * (set true if the item must be loaded from an input stream).  A vector or
 * array of 'CfgPropItem' objects may be used to define a set of property
 * names and default-value objects; or they be added individually.  The
 * default-value objects determine the data types for the items.
 */
public class BaseProperties implements Map
{
  /**
   * Load Settings Type
   */
  public enum LoadSettingsType
  {
    /** Load the settings values */
    LOAD_VALUES,
    /** Load the default settings values */
    LOAD_DEFAULT_VALUES,
    /** Reload the default settings values */
    RELOAD_DEFAULT_VALUES
  }

  /**
   * The separator string to use to separate values.
   */
  public static final String SEPARATOR_STRING = ", ";
  /**
   * String of special characters for 'insertQuoteChars()' method.
   */
  public static final String SPECIAL_CHARS_STR = "\"\r\n\t\\#=";
                        //FIFO hash table to store items in:
  private final Map itemsTable;
  protected String errorMessage = null;     //var to hold error messages
  protected boolean errMsgSetFlag = false;  //var to track error msg status
  private CfgFileTokenizer inTokens;        //tokenizer object
         //name of configuration file that was loaded:
  protected String loadedCfgFname = null;
  private boolean ignoreUnknownPropertyNames = false;
  private boolean emptyStringDefaultFlag = false;  

  /**
   * Creates a properties object that uses the specified map of items
   * or creates a new one if no map was specified (null).
   * @param itemsMap items map
   */
  public BaseProperties(Map itemsMap)
  {
    if (itemsMap == null)                //if no items map provided
      itemsTable = new FifoHashtable();  //create hashtable for items
    else if (itemsMap instanceof BaseProperties)
    {                                    //if items map is base properties
      itemsTable = ((BaseProperties)itemsMap).itemsTable;
    }                                    //then use the items table
    else
      itemsTable = itemsMap;             //otherwise use the given map
  }

    /** Creates an empty table of items. */
  public BaseProperties()
  {
    this((Map)null);
  }

    /**
     * Creates a table of items using the given Vector of 'CfgPropItem'
     * objects.  Any object in the vector that is not a 'CfgPropItem'
     * object is ignored.
     * @param vec vector of items
     */
  public BaseProperties(Vector vec)
  {
    this((Map)null);
    Object obj;
    Enumeration e = vec.elements();
    while(e.hasMoreElements())
    {    //step through vector entries; check each one
      if((obj=e.nextElement()) instanceof CfgPropItem)
        add((CfgPropItem)obj);         //if OK then add to table
    }
  }

    /**
     * Creates a table of items using the given array of 'CfgPropItem'
     * objects.
     * @param itemsArr array of items
     */
  public BaseProperties(CfgPropItem [] itemsArr)
  {
    this((Map)null);
    for(int p=0; p<itemsArr.length; ++p)
      add(itemsArr[p]);
  }

    /**
     * Adds the given property item to the table.  If an item with the
     * same name already exists in the table, it is replaced.
     * @return The given property item.
     * @param item the item to add
     */
  public final CfgPropItem add(CfgPropItem item)
  {
    if(item != null)         //make sure parameter not null
    {
      itemsTable.put(item.getName(),item);       //add item to table
      if (emptyStringDefaultFlag != item.getEmptyStringDefaultFlag())
      {
        item.setEmptyStringDefaultFlag(emptyStringDefaultFlag);
      }
    }
    return item;             //return item
  }

    /**
     * Creates a new property item and adds it to the table.  If an item
     * with the same name already exists in the table, it is replaced.
     * The new item's required-flag is set to false (see
     * 'CfgPropItem.setRequiredFlag()').
     * @param nameStr the name for the property item.
     * @param valueObj the default value object for the property item.
     * @return The newly created property item.
     */
  public CfgPropItem add(String nameStr,Object valueObj)
  {
    return add(new CfgPropItem(nameStr,valueObj));
  }

    /**
     * Creates a new property item and adds it to the table.  If an item
     * with the same name already exists in the table, it is replaced.
     * @param nameStr the name for the property item.
     * @param valueObj the default value object for the property item.
     * @param requiredFlag the value for the item's required-flag (see
     * 'CfgPropItem.setRequiredFlag()').
     * @return The newly created property item.
     */
  public CfgPropItem add(String nameStr,Object valueObj,
                                                       boolean requiredFlag)
  {
    return add(new CfgPropItem(nameStr,valueObj,requiredFlag));
  }

    /**
     * Removes the item in the table matching the given name.
     * @param name item name
     * @return The matching item, or null if none could be found.
     */
  public CfgPropItem remove(String name)
  {
    Object obj;

    if(!((obj=itemsTable.remove(name)) instanceof CfgPropItem))
      return null;           //if proper item not removed then return null
    return (CfgPropItem)obj;                //return item
  }

    /**
     * Returns the item in the table matching the given name, or null
     * if no matching item could be found.
     * @param name item name
     * @return the item
     */
  public CfgPropItem get(String name)
  {
    Object obj;

    if(!((obj=itemsTable.get(name)) instanceof CfgPropItem))
      return null;           //if no proper matching item then return null
    return (CfgPropItem)obj;           //return item
  }

    /**
     * Returns true if the table contains an item whose name matches the
     * given name.
     * @param name key name
     * @return true if the table contains an item whose name matches the
     * given name.
     */
  public boolean containsKey(String name)
  {
    return itemsTable.containsKey(name);
  }

    /**
     * Returns an enumeration of items in the table.  The items will be
     * enumerated in same order as they were added to the table.
     * @return enumeration of items
     */
  public Enumeration elements()
  {
    return new Vector(itemsTable.values()).elements();
  }

  /**
   * @return true if ignoring unknown property names.
   */
  public boolean isIgnoreUnknownPropertyNames()
  {
	return ignoreUnknownPropertyNames;
  }

    /**
     * Returns an enumeration of item names in the table.  The names will
     * be enumerated in same order as they were added to the table.
     * @return enumeration of keys
     */
  public Enumeration keys()
  {
    return new Vector(itemsTable.keySet()).elements();
  }

  /**
   * Removes the given property item from the table.
   * @return The given property item.
   * @param item the item to remove
   */
  public CfgPropItem remove(CfgPropItem item)
  {
    if(item != null)         //make sure parameter not null
      itemsTable.remove(item.getName());       //remove item from table
    return item;             //return item
  }

  /**
   * Set to ignore unknown property names.
   * @param b true to ignore unknown property names.
   */
  public void setIgnoreUnknownPropertyNames(boolean b)
  {
	ignoreUnknownPropertyNames = b;
  }

    /**
     * Stores the properties table to the given output stream in a manner
     * suitable to loading via the 'load()' method.  The items in the
     * table are stored in the same order that they were added.  The name
     * and value fields of each item are stored (but not the default-value
     * field).  The current date and time is stored as a comment line (if
     * 'showHeadersFlag' is true).
     * @param outStm an output stream.
     * @param header a descriptive string which, if not null and contains
     * data, is written as a leading comment line.
     * @param skipUnloadedFlag if true then only items with their
     * 'loadedFlag' set will be stored, if false then all items will
     * be stored.
     * @param showHeadersFlag if true then the headers are outputted.
     * @param showCmdLnOnlyFlag if true then items set as "command line
     * only" will also be stored.
     * @param equalsStr a String containing the characters to be placed
     * between each name and value; or if null then the characters " = "
     * will be used.
     * @param newlineStr a String containing the characters to be used to
     * separate items; or if null then the system default newline
     * character(s) will be used.
     * @exception IOException if an I/O error is detected.
     */
  public void store(OutputStream outStm,String header,
                           boolean skipUnloadedFlag,boolean showHeadersFlag,
               boolean showCmdLnOnlyFlag,String equalsStr,String newlineStr)
                                                          throws IOException
  {
    Object obj,valueObj;
    CfgPropItem item;
    String valStr;
    Object [] valObjArr;
    int i;
    StringBuffer buff;

                   //create buffered print-writer output object:
    PrintWriter out = new PrintWriter(new BufferedOutputStream(outStm));
    if(equalsStr == null || equalsStr.length() <= 0)
      equalsStr = " = ";     //if no equals string given then setup default
    if(newlineStr != null && newlineStr.length() <= 0)
      newlineStr = null;     //if empty newline string then make it null
    if(showHeadersFlag)
    {    //header flag is set
      if(header != null && header.length() > 0)  //if header has data then
        out.println("# " + header);              //store header as comment
      out.println("# " + new Date());  //put in current date/time as comment
      out.println();                   //put in a blank line
    }
    final Enumeration e = elements();
    while(e.hasMoreElements())
    {    //for each item in table
      if((obj=e.nextElement()) instanceof CfgPropItem)
      {       //object is valid property item
        item = (CfgPropItem)obj;       //convert to item type
        if((showCmdLnOnlyFlag || !item.getCmdLnOnlyFlag()) &&
            (!skipUnloadedFlag || item.getLoadedFlag()) &&
            !item.getNoSaveItemFlag() && !item.getIgnoreItemFlag())
        {     //"via command line only" flag is not set and not
              // skipping "unloaded" items or item was "loaded" from file
              // and ignore-item flag not set
                   //if empty-string-default flag set and item's value is
                   // equal to its default value then save an empty string:
          if(item.getEmptyStringDefaultFlag() &&
                                        item.equals(item.getDefaultValue()))
          {   //flag set and item value equal to default
            valStr = UtilFns.EMPTY_STRING;     //use empty string instead
          }
          else     //flag not set or item value not equal to default
          {
            valueObj = item.getValue();          //get item value object
            if(valueObj instanceof Object[])
                {   //item contains an array of values
              valObjArr = (Object [])valueObj;     //setup handle to array
              if(valObjArr.length > 0)
                  {      //array contain at least on element
                buff = new StringBuffer();
                i = 0;
                while(true)
                    {    //for each element in array
                  //convert element object to a string; if it's
                  // already a string then put double-quotes around it;
                  // quote ('\') any "special" chars:
                  buff.append(
                      insertQuoteChars(valObjArr[i],valObjArr[i].toString()));
                  if(++i >= valObjArr.length)
                    break;          //if no more elements then exit loop
                  buff.append(SEPARATOR_STRING); //add separator
                }
                valStr = buff.toString();     //enter string version buffer
              }
              else   //empty array
                valStr = "";   //set empty string
            }
            else
            {   //item contains a single value
              //convert item's value object to a string; if it's
              // already a string then put double-quotes around it;
              // quote ('\') any "special" chars:
              valStr = insertQuoteChars(valueObj,item.toArchivedForm());
            }
          }
                   //store name and value:
          if(newlineStr == null)       //if no newline then usual println
            out.println(item.getName() + equalsStr + valStr);
          else                         //if newline str then use it
            out.print(item.getName() + equalsStr + valStr + newlineStr);
        }
      }
    }
    if(out.checkError())          //flush stream and check error status
      throw new IOException();    //if stream error then throw exception
  }

    /**
     * Stores the properties table to the given output stream in a manner
     * suitable to loading via the 'load()' method.  The items in the
     * table are stored in the same order that they were added.  The name
     * and value fields of each item are stored (but not the default-value
     * field).  The current date and time is stored as a comment line (if
     * 'showHeadersFlag' is true).
     * @param outStm an output stream.
     * @param header a descriptive string which, if not null and contains
     * data, is written as a leading comment line.
     * @param skipUnloadedFlag if true then only items with their
     * 'loadedFlag' set will be stored, if false then all items will
     * be stored.
     * @param showHeadersFlag if true then the headers are outputted.
     * @param showCmdLnOnlyFlag if true then items set as "command line
     * only" will also be stored.
     * @param equalsStr a String containing the characters to be placed
     * between each name and value; or if null then the characters " = "
     * will be used.
     * @exception IOException if an I/O error is detected.
     */
  public void store(OutputStream outStm,String header,
                           boolean skipUnloadedFlag,boolean showHeadersFlag,
                                 boolean showCmdLnOnlyFlag,String equalsStr)
                                                          throws IOException
  {
    store(outStm,header,skipUnloadedFlag,showHeadersFlag,showCmdLnOnlyFlag,
                                                            equalsStr,null);
  }

    /**
     * Stores the properties table to the given output stream in a manner
     * suitable to loading via the 'load()' method.  The items in the
     * table are stored in the same order that they were added.  The name
     * and value fields of each item are stored (but not the default-value
     * field).  The current date and time is stored as a comment line (if
     * 'showHeadersFlag' is true).
     * @param outStm an output stream.
     * @param header a descriptive string which, if not null and contains
     * data, is written as a leading comment line.
     * @param skipUnloadedFlag if true then only items with their
     * 'loadedFlag' set will be stored, if false then all items will
     * be stored.
     * @param showHeadersFlag if true then the headers are outputted.
     * @param showCmdLnOnlyFlag if true then items set as "command line
     * only" will also be stored.
     * @exception IOException if an I/O error is detected.
     */
  public void store(OutputStream outStm,String header,
                           boolean skipUnloadedFlag,boolean showHeadersFlag,
                               boolean showCmdLnOnlyFlag) throws IOException
  {
    store(outStm,header,skipUnloadedFlag,showHeadersFlag,showCmdLnOnlyFlag,
                                                                 null,null);
  }

    /**
     * Stores the properties table to the given output stream in a manner
     * suitable to loading via the 'load()' method.  The items in the
     * table are stored in the same order that they were added.  The name
     * and value fields of each item are stored (but not the default-value
     * field).  The current date and time is stored as a comment line.
     * @param outStm an output stream.
     * @param header a descriptive string which, if not null and contains
     * data, is written as a leading comment line.
     * @param skipUnloadedFlag if true then only items with their
     * 'loadedFlag' set will be stored, if false then all items will
     * be stored.
     * @exception IOException if an I/O error is detected.
     */
  public void store(OutputStream outStm,String header,
                                boolean skipUnloadedFlag) throws IOException
  {
    store(outStm,header,skipUnloadedFlag,true,false,null,null);
  }

  /**
   * Reads a set of property items from the given input stream.  Each
   * item name read must match an existing property item name in
   * the current table, and the read item's value must be a valid
   * ASCII representation of the item's type.  The loading continues
   * even after an error has been detected; the text error message
   * corresponding to the first error detected is saved.
   * @param inStm an input stream.
   * @return true if successful, false if any errors are detected, in
   * which case a text error message is generated that may be fetched
   * via the 'getErrorMessage()' method.
   */
  public boolean load(InputStream inStm)
  {
    return load(inStm, LoadSettingsType.LOAD_VALUES);
  }

  /**
   * Reads a set of property items from the given input stream.  Each
   * item name read must match an existing property item name in
   * the current table, and the read item's value must be a valid
   * ASCII representation of the item's type.  The loading continues
   * even after an error has been detected; the text error message
   * corresponding to the first error detected is saved.
   * @param inStm an input stream.
   * @param defaultFlag true if property items are the defaults, false otherwise.
   * @return true if successful, false if any errors are detected, in
   * which case a text error message is generated that may be fetched
   * via the 'getErrorMessage()' method.
   */
  public boolean load(InputStream inStm, boolean defaultFlag)
  {
    return load(inStm, defaultFlag ? LoadSettingsType.LOAD_DEFAULT_VALUES
        : LoadSettingsType.LOAD_VALUES);
  }

    /**
     * Reads a set of property items from the given input stream.  Each
     * item name read must match an existing property item name in
     * the current table, and the read item's value must be a valid
     * ASCII representation of the item's type.  The loading continues
     * even after an error has been detected; the text error message
     * corresponding to the first error detected is saved.
     * @param inStm an input stream.
     * @param loadSettingsType the load settings type.
     * @return true if successful, false if any errors are detected, in
     * which case a text error message is generated that may be fetched
     * via the 'getErrorMessage()' method.
     */
  public boolean load(InputStream inStm, LoadSettingsType loadSettingsType)
  {
    int tokenType;
    String nameStr,valueStr;
    Enumeration e;
    Object obj;
    CfgPropItem itemObj;
    if (loadSettingsType != LoadSettingsType.RELOAD_DEFAULT_VALUES)
    {
      // clear each item's loaded-flag:
      e = elements();
      while (e.hasMoreElements())
      { // for each item in table
        if ((obj = e.nextElement()) instanceof CfgPropItem)
        { // object is valid property item
          ((CfgPropItem) obj).setLoadedFlag(false);
        }
      }
    }
                                            //create tokenizer object:
    inTokens = new CfgFileTokenizer(new InputStreamReader(inStm));
    try
    {
      while((tokenType=inTokens.nextToken()) != CfgFileTokenizer.TT_EOF)
      {  //parse tokens until end-of-file
        if(tokenType == CfgFileTokenizer.TT_WORD)
        {     //word token parsed OK; retrieve it:
          nameStr = inTokens.getNonNullTokenString();
          if(((itemObj=get(nameStr)) != null && !itemObj.getCmdLnOnlyFlag()) ||
        	  ignoreUnknownPropertyNames)
          {   //word token matches config prop name that's not cmd-ln only
            if((tokenType=inTokens.nextToken()) != CfgFileTokenizer.TT_EOF)
            {      //next token fetched OK
              if(tokenType == (int)'=')
              {    //next token is equals sign; read next token (to EOL)
                if((tokenType=inTokens.nextToken(true)) !=
                                                    CfgFileTokenizer.TT_EOF)
                {  //next token fetched OK
                  if(tokenType == CfgFileTokenizer.TT_WORD)
                  {     //word token parsed OK; retrieve it:
                    valueStr = inTokens.getNonNullTokenString();
                    if (itemObj == null)
                      continue;
                    if(!itemObj.getCmdLnLoadedFlag())
                    {   //item not already loaded from command line
                      if (!setValue(itemObj, loadSettingsType, valueStr))
                      { //error setting value; determine the cause
                        if((obj=itemObj.convertValueString(valueStr)) ==
                                                                       null)
                        {    //error converting value; build error message
                          setErrorMessage("Error loading \"" + nameStr +
                                    "\":  Unable to convert \"" + valueStr +
                                   "\" to type " + UtilFns.shortClassString(
                                                  itemObj.getValueClass()));
                        }
                        else if(!itemObj.validateValue(obj))
                        {    //value not valid; build error message
                          setErrorMessage("Error loading \"" + nameStr +
                                 "\":  Invalid value \"" + valueStr + "\"");
                        }
                        else
                        {    //other error; build error message
                          setErrorMessage("Error loading \"" + nameStr +
                                             "\":  Unable to set value \"" +
                                                           valueStr + "\"");
                        }
                      }
                    }
                  }
                  else  //not a word token; set error message
                    setIllegalCharMsg(tokenType);
                }
                else
                {  //unexpected end-of-file; set error message
                  setUnexpectedEOFMsg();
                  break;           //exit loop
                }
              }
              else //token was not equals sign; set error message
                setIllegalCharMsg(tokenType);
            }
            else
            {      //unexpected end-of-file; set error message
              setUnexpectedEOFMsg();
              break;           //exit loop
            }
          }
          else     //unmatched word token; build error message
            setErrorMessage("Unknown property name:  \"" + nameStr + "\"");
        }
        else  //not a word token; set error message
          setIllegalCharMsg(tokenType);
      }
    }
    catch(IOException ex)
    {         //I/O error detected
      setErrorMessage(ex.toString());       //copy error message
    }

    if(!errMsgSetFlag)
    {    //no previous error message set; check if required items loaded
      e = elements();
      while(e.hasMoreElements())
      {       //for each item in table
        if((obj=e.nextElement()) instanceof CfgPropItem)
        {     //object is valid property item
          itemObj = (CfgPropItem)obj;     //convert to item type
          if(itemObj.getRequiredFlag() && !itemObj.getLoadedFlag() &&
               !itemObj.getCmdLnLoadedFlag() && !itemObj.getCmdLnOnlyFlag())
          {   //item is required to be loaded but was not loaded
              // and is its "via command line only" flag is not set
            setErrorMessage("Required item not loaded:  \"" +   //build msg
                                                  itemObj.getName() + "\"");
            break;           //exit loop
          }
        }
      }
    }
    return !errMsgSetFlag;        //return true if OK, false if error
  }

    /**
     * Reads a set of property items from the given input string.  Each
     * item name read must match an existing property item name in
     * the current table, and the read item's value must be a valid
     * ASCII representation of the item's type.  The loading continues
     * even after an error has been detected; the text error message
     * corresponding to the first error detected is saved.
     * @param inStr the string containing the data to read.
     * @return true if successful, false if any errors are detected, in
     * which case a text error message is generated that may be fetched
     * via the 'getErrorMessage()' method.
     */
  public boolean load(String inStr)
  {
    return load(new ByteArrayInputStream(inStr.getBytes()));
  }

  /**
   * Reads a set of property items from the given input string.  Each
   * item name read must match an existing property item name in
   * the current table, and the read item's value must be a valid
   * ASCII representation of the item's type.  The loading continues
   * even after an error has been detected; the text error message
   * corresponding to the first error detected is saved.
   * @param inStr the string containing the data to read.
   * @param defaultFlag true if property items are the defaults, false otherwise.
   * @return true if successful, false if any errors are detected, in
   * which case a text error message is generated that may be fetched
   * via the 'getErrorMessage()' method.
   */
  public boolean load(String inStr, boolean defaultFlag)
  {
    return load(new ByteArrayInputStream(inStr.getBytes()), defaultFlag);
  }

    /**
     * Sets the 'loadedFlag' field for all items to the given flag value.
     * @param flgVal flag value
     */
  public void setLoadedFlags(boolean flgVal)
  {
    final Enumeration e = elements();
    Object obj;
    while(e.hasMoreElements())
    {    //for each item in table; if OK then set flag value
      if((obj=e.nextElement()) instanceof CfgPropItem)
        ((CfgPropItem)obj).setLoadedFlag(flgVal);
    }
  }

    /**
     * Returns the text message string corresponding to the last error
     * generated by the 'load()' method, or "No error" if no errors have
     * occurred.
     * @param str error message string
     */
  protected void setErrorMessage(String str)
  {
    if(!errMsgSetFlag)
    {    //no previous error message set
      errorMessage = str;              //set error message
      errMsgSetFlag = true;            //indicate not set
    }
  }

     //Builds "Illegal character" error message with given character value.
  protected void setIllegalCharMsg(int charVal)
  {
    if(charVal >= (int)' ' && charVal < 127)
    {         //character is printable; build message with character:
      setErrorMessage("Illegal character " + "'" + (char)charVal +
                                          "' on line " + inTokens.lineno());
    }
    else
    {         //not printable character; build alternate error message:
      setErrorMessage("Illegal or missing character(s) on line " +
                                                         inTokens.lineno());
    }
  }

    ///Builds "Unexpected end-of-file" error message.
  protected void setUnexpectedEOFMsg()
  {
    setErrorMessage("Unexpected end-of-file on line " + inTokens.lineno());
  }
  
  private boolean setValue(CfgPropItem itemObj,
      LoadSettingsType loadSettingsType, String valueStr)
  {
    final boolean defaultFlag = loadSettingsType != LoadSettingsType.LOAD_VALUES;
    // if not processing default value, item's
    // empty-string-default flag is set and value
    // is an empty string then enter default value:
    if (!defaultFlag && itemObj.getEmptyStringDefaultFlag()
        && valueStr.length() <= 0)
    {
      itemObj.setValue(itemObj.getDefaultValue());
      return true;
    }
    // if reload and item is not the default value
    if (loadSettingsType == LoadSettingsType.RELOAD_DEFAULT_VALUES
        && !itemObj.isDefaultValue())
    {
      return itemObj.setDefaultValueString(valueStr);
    }
    if (itemObj.setValueString(valueStr, defaultFlag))
    { // value set OK; indicate that it has been "loaded"
      itemObj.setLoadedFlag(true);
      return true;
    }
    return false;
   }

    /**
     * Returns the text message string corresponding to the first error
     * generated by the 'load()' method, or "No error" if no errors have
     * occurred.
     * @return the error message string
     */
  public String getErrorMessage()
  {
    errMsgSetFlag = false;             //clear error-message-set flag
    return (errorMessage != null) ? errorMessage : "No error";
  }

    /**
     * Returns the name of the configuration file that was loaded;
     * or null if no file was loaded.
     * @return the name of the configuration file that was loaded;
     * or null if no file was loaded.
     */
  public String getLoadedCfgFname()
  {
    return loadedCfgFname;
  }

    /**
     * Returns a string containing a display of the names and values for
     * all the property items in the table.
     * @param equalsStr a String containing the characters to be placed
     * between each name and value; or if null then the characters " = "
     * will be used.
     * @param newlineStr a String containing the characters to be used to
     * separate items; or if null then the system default newline
     * character(s) will be used.
     * @return A string containing a display of the names and values for
     * all the property items in the table.
     */
  public String getDisplayString(String equalsStr,String newlineStr)
  {
              //create byte array output stream to receive output:
    final ByteArrayOutputStream byteStm = new ByteArrayOutputStream();
    try
    {         //store items into byte array output stream:
      store(byteStm,"",false,false,false,equalsStr,newlineStr);
    }
    catch(IOException ex) {};
    final String retStr = byteStm.toString();    //string version of array
    int len = retStr.length();
    final int newlineStrLen;
    if(newlineStr != null && (newlineStrLen=newlineStr.length()) > 0)
    {    //newline string contains data
      if(retStr.endsWith(newlineStr))       //if trailing "newline" data
        len -= newlineStrLen;               //subtract to trim data
    }
    else
    {    //newline string does not contain data
              //trim any trailing whitespace characters (newline, etc):
      while(len > 1 && Character.isWhitespace(retStr.charAt(len-1)))
        --len;          //decrement to trim characters
    }
    return retStr.substring(0,len);    //return display data
  }

    /**
     * Returns a string containing a display of the names and values for
     * all the property items in the table.
     * @return A string containing a display of the names and values for
     * all the property items in the table.
     */
  public String getDisplayString()
  {
    return getDisplayString(null,null);
  }


  // Map interface - provides Map access to the items table.

  // Query Operations

  /**
   * Returns the number of key-value mappings in this map.  If the
   * map contains more than <tt>Integer.MAX_VALUE</tt> elements, returns
   * <tt>Integer.MAX_VALUE</tt>.
   *
   * @return the number of key-value mappings in this map.
   */
  public int size()
  {
    return itemsTable.size();
  }

  /**
   * Returns <tt>true</tt> if this map contains no key-value mappings.
   *
   * @return <tt>true</tt> if this map contains no key-value mappings.
   */
  public boolean isEmpty()
  {
    return itemsTable.isEmpty();
  }

  /**
   * Returns <tt>true</tt> if this map contains a mapping for the specified
   * key.
   *
   * @param key key whose presence in this map is to be tested.
   * @return <tt>true</tt> if this map contains a mapping for the specified
   * key.
   *
   * @throws ClassCastException if the key is of an inappropriate type for
   * 		  this map.
   * @throws NullPointerException if the key is <tt>null</tt> and this map
   *            does not not permit <tt>null</tt> keys.
   */
  public boolean containsKey(Object key)
  {
    return itemsTable.containsKey(key);
  }

  /**
   * Returns <tt>true</tt> if this map maps one or more keys to the
   * specified value.  More formally, returns <tt>true</tt> if and only if
   * this map contains at least one mapping to a value <tt>v</tt> such that
   * <tt>(value==null ? v==null : value.equals(v))</tt>.  This operation
   * will probably require time linear in the map size for most
   * implementations of the <tt>Map</tt> interface.
   *
   * @param value value whose presence in this map is to be tested.
   * @return <tt>true</tt> if this map maps one or more keys to the
   *         specified value.
   */
  public boolean containsValue(Object value)
  {
    return itemsTable.containsValue(value);
  }

  /**
   * Returns the value to which this map maps the specified key.  Returns
   * <tt>null</tt> if the map contains no mapping for this key.  A return
   * value of <tt>null</tt> does not <i>necessarily</i> indicate that the
   * map contains no mapping for the key; it's also possible that the map
   * explicitly maps the key to <tt>null</tt>.  The <tt>containsKey</tt>
   * operation may be used to distinguish these two cases.
   *
   * @param key key whose associated value is to be returned.
   * @return the value to which this map maps the specified key, or
   *	       <tt>null</tt> if the map contains no mapping for this key.
   *
   * @throws ClassCastException if the key is of an inappropriate type for
   * 		  this map.
   * @throws NullPointerException key is <tt>null</tt> and this map does not
   *		  not permit <tt>null</tt> keys.
   *
   * @see #containsKey(Object)
   */
  public Object get(Object key)
  {
    return itemsTable.get(key);
  }

  // Modification Operations

  /**
   * Associates the specified value with the specified key in this map
   * (optional operation).  If the map previously contained a mapping for
   * this key, the old value is replaced.
   *
   * @param key key with which the specified value is to be associated.
   * @param value value to be associated with the specified key.
   * @return previous value associated with specified key, or <tt>null</tt>
   *	       if there was no mapping for key.  A <tt>null</tt> return can
   *	       also indicate that the map previously associated <tt>null</tt>
   *	       with the specified key, if the implementation supports
   *	       <tt>null</tt> values.
   *
   * @throws UnsupportedOperationException if the <tt>put</tt> operation is
   *	          not supported by this map.
   * @throws ClassCastException if the class of the specified key or value
   * 	          prevents it from being stored in this map.
   * @throws IllegalArgumentException if some aspect of this key or value
   *	          prevents it from being stored in this map.
   * @throws NullPointerException this map does not permit <tt>null</tt>
   *            keys or values, and the specified key or value is
   *            <tt>null</tt>.
   */
  public Object put(Object key, Object value)
  {
    if (value instanceof CfgPropItem)
      return add((CfgPropItem)value);
    return add(new CfgPropItem(key.toString(),value));
  }

  /**
   * Removes the mapping for this key from this map if present (optional
   * operation).
   *
   * @param key key whose mapping is to be removed from the map.
   * @return previous value associated with specified key, or <tt>null</tt>
   *	       if there was no mapping for key.  A <tt>null</tt> return can
   *	       also indicate that the map previously associated <tt>null</tt>
   *	       with the specified key, if the implementation supports
   *	       <tt>null</tt> values.
   * @throws UnsupportedOperationException if the <tt>remove</tt> method is
   *         not supported by this map.
   */
  public Object remove(Object key)
  {
    return itemsTable.remove(key);
  }


  // Bulk Operations

  /**
   * Copies all of the mappings from the specified map to this map
   * (optional operation).  These mappings will replace any mappings that
   * this map had for any of the keys currently in the specified map.
   *
   * @param t Mappings to be stored in this map.
   *
   * @throws UnsupportedOperationException if the <tt>putAll</tt> method is
   * 		  not supported by this map.
   *
   * @throws ClassCastException if the class of a key or value in the
   * 	          specified map prevents it from being stored in this map.
   *
   * @throws IllegalArgumentException some aspect of a key or value in the
   *	          specified map prevents it from being stored in this map.
   *
   * @throws NullPointerException this map does not permit <tt>null</tt>
   *            keys or values, and the specified key or value is
   *            <tt>null</tt>.
   */
  public void putAll(Map t)
  {
    Map.Entry entry;

    Iterator it = t.entrySet().iterator();
    while(it.hasNext())
    {
      entry = (Map.Entry)it.next();
      put(entry.getKey(), entry.getValue());
    }
  }

  /**
   * Removes all mappings from this map (optional operation).
   *
   * @throws UnsupportedOperationException clear is not supported by this
   * 		  map.
   */
  public void clear()
  {
    itemsTable.clear();
  }

  /**
   * Sets value on all items to the default value.
   */
  public void setAllToDefaults()
  {
    final Enumeration e = elements();
    Object obj;
    CfgPropItem propObj;
    while(e.hasMoreElements())
    {    //for each item in table
      if((obj=e.nextElement()) instanceof CfgPropItem)
      {  //if item fetched OK then set value to default
        propObj = (CfgPropItem)obj;
        propObj.setValue(propObj.getDefaultValue());
      }
    }
  }


  // Views

  /**
   * Returns a set view of the keys contained in this map.  The set is
   * backed by the map, so changes to the map are reflected in the set, and
   * vice-versa.  If the map is modified while an iteration over the set is
   * in progress, the results of the iteration are undefined.  The set
   * supports element removal, which removes the corresponding mapping from
   * the map, via the <tt>Iterator.remove</tt>, <tt>Set.remove</tt>,
   * <tt>removeAll</tt> <tt>retainAll</tt>, and <tt>clear</tt> operations.
   * It does not support the add or <tt>addAll</tt> operations.
   *
   * @return a set view of the keys contained in this map.
   */
  public Set keySet()
  {
    return itemsTable.keySet();
  }

  /**
   * Returns a collection view of the values contained in this map.  The
   * collection is backed by the map, so changes to the map are reflected in
   * the collection, and vice-versa.  If the map is modified while an
   * iteration over the collection is in progress, the results of the
   * iteration are undefined.  The collection supports element removal,
   * which removes the corresponding mapping from the map, via the
   * <tt>Iterator.remove</tt>, <tt>Collection.remove</tt>,
   * <tt>removeAll</tt>, <tt>retainAll</tt> and <tt>clear</tt> operations.
   * It does not support the add or <tt>addAll</tt> operations.
   *
   * @return a collection view of the values contained in this map.
   */
  public Collection values()
  {
    return itemsTable.values();
  }

  /**
   * Returns a set view of the mappings contained in this map.  Each element
   * in the returned set is a <tt>Map.Entry</tt>.  The set is backed by the
   * map, so changes to the map are reflected in the set, and vice-versa.
   * If the map is modified while an iteration over the set is in progress,
   * the results of the iteration are undefined.  The set supports element
   * removal, which removes the corresponding mapping from the map, via the
   * <tt>Iterator.remove</tt>, <tt>Set.remove</tt>, <tt>removeAll</tt>,
   * <tt>retainAll</tt> and <tt>clear</tt> operations.  It does not support
   * the <tt>add</tt> or <tt>addAll</tt> operations.
   *
   * @return a set view of the mappings contained in this map.
   */
  public Set entrySet()
  {
    return itemsTable.entrySet();
  }

  /**
   * Returns the default empty-string-default flag for the property items.  A
   * value of 'true' will make an empty string read from the
   * configuration file be converted to the default value for
   * the item, and it will make an item value equal to the default
   * value be converted to the empty string when written to the
   * configuration file.
   * @return The current value of the empty-string-default flag for
   * this item.
   */
  public boolean getEmptyStringDefaultFlag()
  {
    return emptyStringDefaultFlag;
  }

  /**
   * Sets the default empty-string-default flag for the property items. A value
   * of 'true' will make an empty string read from the configuration file be
   * converted to the default value for the item, and it will make an item value
   * equal to the default value be converted to the empty string when written to
   * the configuration file.
   * 
   * @param flg the flag value to use.
   * @return A handle to this object.
   */
  public BaseProperties setEmptyStringDefaultFlag(boolean flg)
  {
    emptyStringDefaultFlag = flg;
    return this;
  }

  // Comparison and hashing

  /**
   * Compares the specified object with this map for equality.  Returns
   * <tt>true</tt> if the given object is also a map and the two Maps
   * represent the same mappings.  More formally, two maps <tt>t1</tt> and
   * <tt>t2</tt> represent the same mappings if
   * <tt>t1.entrySet().equals(t2.entrySet())</tt>.  This ensures that the
   * <tt>equals</tt> method works properly across different implementations
   * of the <tt>Map</tt> interface.
   *
   * @param o object to be compared for equality with this map.
   * @return <tt>true</tt> if the specified object is equal to this map.
   */
  public boolean equals(Object o)
  {
    return itemsTable.equals(o);
  }

  /**
   * Returns the hash code value for this map.  The hash code of a map
   * is defined to be the sum of the hashCodes of each entry in the map's
   * entrySet view.  This ensures that <tt>t1.equals(t2)</tt> implies
   * that <tt>t1.hashCode()==t2.hashCode()</tt> for any two maps
   * <tt>t1</tt> and <tt>t2</tt>, as required by the general
   * contract of Object.hashCode.
   *
   * @return the hash code value for this map.
   * @see Map.Entry#hashCode()
   * @see Object#hashCode()
   * @see Object#equals(Object)
   * @see #equals(Object)
   */
  public int hashCode()
  {
    return itemsTable.hashCode();
  }

  /**
   * Returns a string with a backslash quote character ('\')
   * inserted in front of each occurance of a "special" character
   * in the given source string if needed.
   * @param valueObj the value object.
   * @param stringValue the string value of the item.
   * @return a new string with its "special" characters quoted, or the
   * original string if no matching "special" characters were found.
   */
  protected static String insertQuoteChars(Object valueObj,String stringValue)
  {
    //if the value object contains property editor information
    if (valueObj instanceof PropertyEditorInformation)
    {
      //return the string value if it is already quoted
      if (((PropertyEditorInformation)valueObj).isValueStringQuoted())
      {
        return stringValue;
      }
    }

    //convert item's value object to a string; if it's
    // already a string then put double-quotes around it;
    // quote ('\') any "special" chars:
    final String valStr = (valueObj instanceof String) ? ("\"" +
        UtilFns.insertQuoteChars(
        stringValue,SPECIAL_CHARS_STR) + "\"") :
    UtilFns.insertQuoteChars(
    stringValue,SPECIAL_CHARS_STR);
    return valStr;
  }
}
