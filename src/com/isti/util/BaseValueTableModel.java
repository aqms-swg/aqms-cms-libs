//BaseValueTableModel.java:  Contains methods for handling values in a table.
//
// 11/15/2007 -- [KF]  Initial version.
//

package com.isti.util;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.HashSet;

public class BaseValueTableModel
    implements ValueTableModel
{
  private List rows; //list of 'List' columns
  private List columnNames; //list of 'String' column names
  private List readOnlyColumnNames; //list of read-only 'String' column names
  private Map columnMap; //column map
  private final ErrorMessageMgr errorMessageMgr; //the error message manager
  /** True if the model is editable, false otherwise */
  private boolean editableFlag = false;

  /**
   * Creates the base values table.
   */
  public BaseValueTableModel()
  {
    clearLists();
    errorMessageMgr = new ErrorMessageMgr(); //the error message manager
  }

  /**
   * Clears any current error message.
   */
  public synchronized void clearErrorMessageString()
  {
    errorMessageMgr.clearErrorMessageString();
  }

  /**
   * Clears the lists by setting them to empty immutable lists.
   */
  public final void clearLists()
  {
    rows = Collections.EMPTY_LIST;
    columnNames = Collections.EMPTY_LIST;
    readOnlyColumnNames = Collections.EMPTY_LIST;
    columnMap = Collections.EMPTY_MAP;
  }

  /**
   * Clears all rows and creates the lists if they have not already been created.
   */
  public void clearRows()
  {
    if (!columnsExist()) //if columns not already set up
    {
      createLists(); //create new lists
    }
    else
    {
      rows.clear();
      columnMap.clear();
    }
  }

  /**
   * Determines if the columns exist.
   * @return true if the columns exist, false otherwise.
   */
  public boolean columnsExist()
  {
    return columnNames.size() != 0;
  }

  /**
   * Creates new lists.
   */
  public void createLists()
  {
    rows = createRowsList();
    columnNames = createColumnNamesList();
    readOnlyColumnNames = Collections.EMPTY_LIST;
    columnMap = createColumnMap();
  }

  /**
   * Returns the <code>Class</code> for all <code>Object</code> instances
   * in the specified column.
   *
   * @param columnIndex the column index.
   *
   * @return The class.
   */
  public Class getColumnClass(int columnIndex)
  {
    /*
        final Object valueObj = getRawValueAt(0, columnIndex);
        if (valueObj != null)
        {
          return valueObj.getClass();
        }
     */
    return Object.class;
  }

  /**
   * Returns the number of columns in the model.
   *
   * @return The column count
   */
  public int getColumnCount()
  {
    return columnNames.size();
  }

  /**
   * Gets the column index for the specified column name.
   * @param columnName the column name.
   * @return the column index or -1 if none.
   */
  public int getColumnIndex(String columnName)
  {
    for (int columnIndex = 0; columnIndex < getColumnCount(); columnIndex++)
    {
      if (getColumnName(columnIndex).equals(columnName))
      {
        return columnIndex;
      }
    }
    return -1;
  }

  /**
   * Returns the name of a column in the model.
   *
   * @param columnIndex the column index.
   *
   * @return The column name.
   */
  public String getColumnName(int columnIndex)
  {
    return columnNames.get(columnIndex).toString();
  }

  /**
   * Returns the status of the error message.
   * @return true if an error message is set; false if not.
   */
  public boolean getErrorMessageFlag()
  {
    return errorMessageMgr.getErrorMessageFlag();
  }

  /**
   * Returns the current error message (if any).
   * @return The current error message, or null if no errors have occurred.
   */
  public String getErrorMessageString()
  {
    return errorMessageMgr.getErrorMessageString();
  }

  /**
   * Returns the number of rows in the model.
   *
   * @return The row count.
   */
  public int getRowCount()
  {
    return rows.size();
  }

  /**
   * Returns the value (<code>Object</code>) at a particular cell in the
   * table.
   *
   * @param rowIndex the row index.
   * @param columnIndex the column index.
   *
   * @return The value at the specified cell.
   */
  public Object getValueAt(int rowIndex, int columnIndex)
  {
    final Object value = getRawValueAt(rowIndex, columnIndex);
    return value;
  }

  /**
   * Returns the value (<code>Object</code>) at a particular cell in the
   * table.
   *
   * @param rowIndex the row index.
   * @param columnIndex the column index.
   *
   * @return The value at the specified cell.
   */
  public Object getRawValueAt(int rowIndex, int columnIndex)
  {
    return getModifiableRow(rowIndex).get(columnIndex);
  }

  /**
   * Import the values from the value table model for all rows.
   * @param vtm the value table model.
   */
  public void importValues(ValueTableModel vtm)
  {
    if (!columnsExist()) //if columns not already set up
    {
      createLists();
      //use the column names from the specified table model
      setColumnNames(vtm);
    }
    final int numRows = vtm.getRowCount();
    for (int rowIndex = 0; rowIndex < numRows; rowIndex++)
    {
      importValues(vtm, rowIndex);
    }
  }

  /**
   * Import the values from the value table model for the specifed row.
   * The lists should be created (via the 'createLists' method) first.
   * @param vtm the value table model.
   * @param rowIndex the row index.
   * @see createLists
   */
  protected void importValues(ValueTableModel vtm, int rowIndex)
  {
    final int fromColumnCount = vtm.getColumnCount();
    String columnName;
    int toColumnIndex;
    final List columnList = createFixedSizeList(getColumnCount());
    for (int fromColumnIndex = 0; fromColumnIndex < fromColumnCount;
         fromColumnIndex++)
    {
      columnName = vtm.getColumnName(fromColumnIndex);
      toColumnIndex = getColumnIndex(columnName);
      if (toColumnIndex >= 0)
      {
        columnList.set(toColumnIndex, vtm.getValueAt(rowIndex, fromColumnIndex));
      }
    }
    addFixedSizeRow(columnList);
  }

  /**
   * Returns <code>true</code> if the specified cell is editable, and
   * <code>false</code> if it is not.
   *
   * @param rowIndex the row index of the cell.
   * @param columnIndex the column index of the cell.
   *
   * @return true if the cell is editable, false otherwise.
   */
  public boolean isCellEditable(int rowIndex, int columnIndex)
  {
    //if model is not editable
    if (!isEditable())
    {
      return false; //cell is not editable
    }
    //if column is read-only
    if (readOnlyColumnNames.contains(getColumnName(columnIndex)))
    {
      return false; //cell is not editable
    }
    return true; //cell is editable
  }

  /**
   * Adds a read-only column.
   * @param columnName the column name.
   */
  protected void addReadOnlyColumn(String columnName)
  {
    if (readOnlyColumnNames.size() == 0)
    {
      readOnlyColumnNames = new VectorSet();
    }
    readOnlyColumnNames.add(columnName);
  }

  /**
   * Returns <code>true</code> if the model is editable by default, and
   * <code>false</code> if it is not.
   * @return true if the model is editable, false otherwise.
   */
  public final boolean isEditable()
  {
    return editableFlag;
  }

  /**
   * Determines whether the model is editable by default.
   * @param flag true if the model is editable, false otherwise.
   */
  public final void setEditable(boolean flag)
  {
    editableFlag = flag;
  }

  /**
   * Sets the value at a particular cell in the table.
   *
   * @param aValue the value (<code>null</code> permitted).
   * @param rowIndex the row index.
   * @param columnIndex the column index.
   */
  public void setValueAt(Object aValue, int rowIndex, int columnIndex)
  {
    getModifiableRow(rowIndex).set(columnIndex, aValue);
  }

  /**
   * Adds the row.
   * @param columnList the list of columns for the row.
   */
  protected void addRow(List columnList)
  {
    addFixedSizeRow(createFixedSizeList(columnList));
  }

  /**
   * Adds the row.
   * @param columnList the list of columns for the row.
   */
  protected void addFixedSizeRow(List columnList)
  {
    final Object columnMapKey = getColumnMapKey(columnList);
    final List columnMapValues = getColumnMapValues(columnMapKey);
    if (columnMapValues != null)
    {
      for (int i = 0; i < columnList.size(); i++)
      {
        columnMapValues.set(i, columnList.get(i));
      }
      return;
    }
    addColumnMapValues(columnMapKey, columnList);
    rows.add(columnList);
  }

  /**
   * Gets the column map key.
   * @param columnList the list of columns for the row.
   * @return the column map key or null if none.
   */
  protected Object getColumnMapKey(List columnList)
  {
    return columnList;
  }

  /**
   * Adds the column map values.
   * @param columnMapKey the column map key.
   * @param columnList the list of columns for the row.
   */
  protected void addColumnMapValues(Object columnMapKey, List columnList)
  {
    if (columnMapKey != null)
    {
      columnMap.put(columnMapKey, columnList);
    }
  }

  /**
   * Gets the column map values.
   * @param columnMapKey the column map key.
   * @return the list of columns for the row or null if none.
   */
  protected List getColumnMapValues(Object columnMapKey)
  {
    if (columnMapKey != null)
    {
      return (List) columnMap.get(columnMapKey);
    }
    return null;
  }

  /**
   * Appends to the error message string.
   * @param str the error message to append.
   */
  protected void appendErrorMessageString(String str)
  {
    //if there was already an error message
    if (getErrorMessageFlag())
    {
      str = getErrorMessageString() + "\n" + str;
    }
    setErrorMessageString(str);
  }

  /**
   * Create a new emtpy fixed size list.
   * @param size the size of the list.
   * @return the new list.
   */
  protected List createFixedSizeList(int size)
  {
    return Arrays.asList(new Object[size]);
  }

  /**
   * Create a new fixed size list.
   * @param c the collection whose elements are to be placed into this list.
   * @return the new list.
   */
  protected List createFixedSizeList(Collection c)
  {
    return Arrays.asList(c.toArray());
  }

  /**
   * Creates a new column map.
   * @return the new map.
   */
  protected Map createColumnMap()
  {
    return createMap();
  }

  /**
   * Create a new column names list.
   * @return the new list.
   */
  protected List createColumnNamesList()
  {
    return createList();
  }

  /**
   * Create a new list.
   * @return the new list.
   */
  protected List createList()
  {
    return new ArrayList();
  }

  /**
   * Creates a new map.
   * @return the new map.
   */
  protected Map createMap()
  {
    return new HashMap();
  }

  /**
   * Creates the new rows list.
   * @return the new list.
   */
  protected List createRowsList()
  {
    return createList();
  }

  /**
   * Gets an unmodifiable column names list.
   * @return an unmodifiable column names list.
   */
  protected List getColumnNames()
  {
    return Collections.unmodifiableList(columnNames);
  }

  /**
   * Gets the default column name for the specified column.
   * @param columnIndex the column index.
   * @return the default column name.
   */
  protected String getDefaultColumnName(int columnIndex)
  {
    final StringBuffer buffer = new StringBuffer();
    while (columnIndex >= 0)
    {
      buffer.insert(0, (char) ('A' + columnIndex % 26));
      columnIndex = columnIndex / 26 - 1;
    }
    return buffer.toString();
  }

  /**
   * Gets an unmodifiable row.
   * @param rowIndex the row index.
   * @return the unmodifiable list of columns for the row.
   */
  protected List getRow(int rowIndex)
  {
    return Collections.unmodifiableList(getModifiableRow(rowIndex));
  }

  /**
   * Determines if the value is null.
   * @param valueObj the value object.
   * @return true if the value is null, false otherwise.
   */
  protected boolean isNull(Object valueObj)
  {
    if (valueObj == null || valueObj.toString().length() == 0)
    {
      return true;
    }
    return false;
  }

  /**
   * Set the column names list to the default values.
   * The lists should be created (via the 'createLists' method) first.
   * @param numColumns the number of columns.
   * @see createLists
   */
  protected void setColumnNames(int numColumns)
  {
    for (int i = 0; i < numColumns; i++)
    {
      columnNames.add(getDefaultColumnName(i));
    }
  }

  /**
   * Sets the column names list.
   * The lists should be created (via the 'createLists' method) first.
   * @param l the column names list.
   * @return true if the column names list was set, false if error.
   * @see createLists
   */
  protected boolean setColumnNames(List l)
  {
    //ensure the header does not contain duplicate columns
    if (l.size() != new HashSet(l).size())
    {
      appendErrorMessageString(
          "Header contains duplicate columns: " + l);
      return false;
    }
    columnNames.addAll(l);
    return true;
  }

  /**
   * Sets the column names list.
   * The lists should be created (via the 'createLists' method) first.
   * @param vtm the value table model.
   * @see createLists
   */
  protected void setColumnNames(ValueTableModel vtm)
  {
    final int numColumns = vtm.getColumnCount();
    for (int columnIndex = 0; columnIndex < numColumns; columnIndex++)
    {
      columnNames.add(vtm.getColumnName(columnIndex));
    }
  }

  /**
   * Sets the error message.  Any previously set error message is
   * overwritten.
   * @param str the error message text to set.
   */
  protected void setErrorMessageString(String str)
  {
    errorMessageMgr.setErrorMessageString(str);
  }

  /**
   * Gets the row.
   * @param rowIndex the row index.
   * @return the list of columns for the row.
   */
  private List getModifiableRow(int rowIndex)
  {
    return (List) rows.get(rowIndex);
  }
}
