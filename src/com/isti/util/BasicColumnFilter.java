package com.isti.util;

/**
 * Class BasicColumnFilter defines a basic column filter.
 */
public class BasicColumnFilter implements IstiColumnFilter {
  /** Indicates which columns are visible or null for all visible */
  private final boolean[] columnsVisible;

  /**
   * Creates the basic column filter.
   * @param columnCount the column count.
   */
  public BasicColumnFilter(final int columnCount) {
    columnsVisible = new boolean[columnCount];
    for (int i = 0; i < columnCount; i++) {
      columnsVisible[i] = true;
    }
  }

  /**
   * Determines if the column is visible.
   * @param columnIndex the actual column index.
   * @return true if the column is visible, false otherwise.
   */
  public boolean isColumnVisible(int columnIndex) {
    return columnIndex > columnsVisible.length || columnsVisible[columnIndex];
  }

  /**
   * Sets if the column is visible or not.
   * @param columnIndex the actual column index.
   * @param b true for visible, false otherwise.
   */
  public void setColumnVisible(int columnIndex, boolean b) {
    columnsVisible[columnIndex] = b;
  }
}
