package com.isti.util;

/**
 * Basic Browser Launch.
 * Utility class to open a web page from a Swing application
 * in the user's default browser.
 * Supports: Mac OS X, GNU/Linux, Unix, Windows XP/Vista/7
 * This code was lifted from <a href=
 * "http://www.centerkey.com/java/browser/">www.centerkey.com/java/browser</a>
 */
public class BasicLaunchBrowser
    implements ILaunchBrowser
{
  /** The operating system name or null if none. */
  private static String osName = null;

  /** The web browsers for Unix/Linux. */
  public static final String[] WEB_BROWSERS =
      {
      "google-chrome", "firefox", "mozilla", "netscape"};
  private final String[] browsers;

  /** This flag is true to use the java.awt.Desktop, false otherwise. */
  private final boolean useDesktopFlag;

  /** Error message. */
  private String errorMessage = null;

  /** The browser command array or null if none. */
  private String cmdarray[] = null;

  /**
   * Creates a launch browser.
   */
  public BasicLaunchBrowser()
  {
    this(WEB_BROWSERS, isDesktopAvailable());
  }

  /**
   * Creates a launch browser.
   * @param browsers the web browsers for Unix/Linux.
   * @param useDesktopFlag true to use the java.awt.Desktop, false otherwise.
   * @see #WEB_BROWSERS
   * @see #isDesktopAvailable()
   */
  public BasicLaunchBrowser(String[] browsers, boolean useDesktopFlag)
  {
    this.browsers = browsers;
    this.useDesktopFlag = useDesktopFlag;
  }

  /**
   * Attempts to use the Desktop library from JDK 1.6+ to open a browser.
   * @param urlStr a string containing the URL.
   * @throws Exception if error.
   * @see #isDesktopAvailable()
   */
  public static void browse(String urlStr) throws Exception
  {
    //attempt to use
    Class d = Class.forName("java.awt.Desktop");
    d.getDeclaredMethod("browse", new Class[]
                        {java.net.URI.class}).invoke(
                            d.getDeclaredMethod("getDesktop", new Class[0]).
                            invoke(null, new Object[0]),
                            new Object[]
                            {java.net.URI.create(urlStr)});
    //above code mimicks:  java.awt.Desktop.getDesktop().browse()
  }

  /**
   * Returns message string for last error (or 'No error' if none).
   * @return message string for last error (or 'No error' if none).
   */
  public String getErrorMessage()
  {
    return (errorMessage != null) ? errorMessage : NO_ERROR;
  }

  /**
   * Determines if the desktop is available.
   * @return boolean true if the desktop is available, false otherwise.
   */
  public static boolean isDesktopAvailable()
  {
    return isDesktopAvailable(UtilFns.getJavaVersion());
  }

  /**
   * Determines if the desktop is available.
   * @param javaVersionString the Java version string.
   * @return boolean true if the desktop is available, false otherwise.
   * @see UtilFns.getJavaVersion()
   */
  public static boolean isDesktopAvailable(String javaVersionString)
  {
    if (!javaVersionString.equals(UtilFns.DEFAULT_JAVA_VERSION_STRING))
    {
      return UtilFns.compareVersionStrings(javaVersionString, "1.6") > 0;
    }
    return false;
  }

  /**
   * Displays the given URL address string in a browser window.
   * @param urlStr a string containing the URL.
   * @return true if successful; false if error (in which case an error message
   *         may be fetched via the 'getErrorMessage()' method).
   */
  public boolean showURL(String urlStr)
  {
    return showURL(urlStr, null);
  }

  /**
   * Displays the given URL address string in a browser window.
   * @param urlStr a string containing the URL.
   * @param titleStr a string used to select a target browser window or null if
   *          none.
   * @return true if successful; false if error (in which case an error message
   *         may be fetched via the 'getErrorMessage()' method).
   */
  public boolean showURL(String urlStr, String titleStr)
  {
    if (useDesktopFlag)
    {
      try
      { //attempt to use Desktop library from JDK 1.6+
        browse(urlStr);
        return true;
      }
      catch (Exception ignore)
      { //library not available or failed
      }
    }

    errorMessage = null;

    if (osName == null)
    {
      osName = System.getProperty("os.name");
    }

    try
    {
      if (cmdarray != null) //if browser has been found
      {
        errorMessage = launch(urlStr);
      }
      else if (osName.startsWith("Mac OS"))
      {
        Class.forName("com.apple.eio.FileManager").getDeclaredMethod(
            "openURL", new Class[]
            {String.class}).invoke(null,
                                   new Object[]
                                   {urlStr});
      }
      else if (osName.startsWith("Windows"))
      {
        cmdarray = new String[]
            {
            "rundll32", "url.dll,FileProtocolHandler", urlStr};
        errorMessage = launch();
      }
      else
      {
        //assume Unix or Linux
        String browser;
        for (int i = 0; i < browsers.length; i++)
        {
          browser = browsers[i];
          if (Runtime.getRuntime().exec(new String[]
                                        {"which", browser}).getInputStream().
              read() != -1)
          {
            cmdarray = new String[]
                {
                browser, urlStr};
            errorMessage = launch();
            break;
          }
        }
        if (cmdarray == null)
        {
          errorMessage = "Could not find a browser: " +
              UtilFns.toString(browsers);
        }
      }
    }
    catch (Exception ex)
    {
      errorMessage = "Error attempting to launch web browser\n" +
          ex.toString();
    }
    return errorMessage == null;
  }

  /**
   * Launch the browser.
   * @return null if successful; an error message if error.
   */
  protected String launch()
  {
    try
    {
      Runtime.getRuntime().exec(cmdarray);
    }
    catch (Exception ex)
    {
      return "Runtime.exec(" + UtilFns.toString(cmdarray) +
          ") exception:  " + ex;
    }
    return null;
  }

  /**
   * Launch the browser.
   * @param urlStr the URL string.
   * @return null if successful; an error message if error.
   */
  protected String launch(String urlStr)
  {
    cmdarray[cmdarray.length - 1] = urlStr;
    return launch();
  }

  public static void main(String[] args)
  {
    String javaVersionString = UtilFns.getJavaVersion();
    boolean useDesktopFlag = isDesktopAvailable(javaVersionString);
    System.out.println(
        (useDesktopFlag ? "" : "not ") +
        "using desktop (" + javaVersionString + ")");
    final BasicLaunchBrowser ilb = new BasicLaunchBrowser(WEB_BROWSERS,
        useDesktopFlag);
    String urlStr;
    if (args.length > 0)
    {
      for (int i = 0; i < args.length; i++)
      {
        urlStr = args[i];
        System.out.println("showing \"" + urlStr + "\" ...");
        if (!ilb.showURL(urlStr))
        {
          System.err.println("error showing URL: " + urlStr);
          break;
        }
      }
    }
    else
    {
      urlStr = "http://www.google.com";
      System.out.println("showing \"" + urlStr + "\" ...");
      if (!ilb.showURL(urlStr))
      {
        System.err.println("error showing URL: " + urlStr);
      }
    }
    String s = UtilFns.toString(ilb.cmdarray);
    if (s != UtilFns.DEFAULT_NULL_STR)
    {
      System.out.print(s + ": ");
    }
    System.out.println(ilb.getErrorMessage());
  }
}
