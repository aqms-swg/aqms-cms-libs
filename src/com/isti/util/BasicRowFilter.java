package com.isti.util;

/**
 * Class BasicRowFilter defines a basic row filter.
 */
public class BasicRowFilter implements IstiRowFilter {
  /** Indicates which rows are visible or null for all visible */
  private final boolean[] rowsVisible;

  /**
   * Creates the basic row filter.
   * @param rowCount the row count.
   */
  public BasicRowFilter(final int rowCount) {
    rowsVisible = new boolean[rowCount];
    for (int i = 0; i < rowCount; i++) {
      rowsVisible[i] = true;
    }
  }

  /**
   * Determines if the row is visible.
   * @param rowIndex the actual row index.
   * @return true if the row is visible, false otherwise.
   */
  public boolean isRowVisible(int rowIndex) {
    return rowIndex > rowsVisible.length || rowsVisible[rowIndex];
  }

  /**
   * Sets if the row is visible or not.
   * @param rowIndex the actual row index.
   * @param b true for visible, false otherwise.
   */
  public void setRowVisible(int rowIndex, boolean b) {
    rowsVisible[rowIndex] = b;
  }
}
