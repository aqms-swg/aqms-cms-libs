//BooleanCp:  'Boolean'-like wrapper class which holds a boolean value
//            and implements the Comparable interface.
//
//   7/12/2000 -- [ET]
//
//
//=====================================================================
// Copyright (C) 2000 Instrumental Software Technologies, Inc.
//
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions
// are met:
// 1. Redistributions of source code, or portions of this source code,
//    must retain the above copyright notice, this list of conditions
//    and the following disclaimer.
// 2. Redistributions in binary form must reproduce the above copyright
//    notice, this list of conditions and the following disclaimer in
//    the documentation and/or other materials provided with the
//    distribution.
// 3. All advertising materials mentioning features or use of this
//    software must display the following acknowledgment:
//    "This product includes software developed by Instrumental
//    Software Technologies, Inc. (http://www.isti.com)"
// 4. If the software is provided with, or as part of a commercial
//    product, or is used in other commercial software products the
//    customer must be informed that "This product includes software
//    developed by Instrumental Software Technologies, Inc.
//    (http://www.isti.com)"
// 5. The names "Instrumental Software Technologies, Inc." and "ISTI"
//    must not be used to endorse or promote products derived from
//    this software without prior written permission. For written
//    permission, please contact "info@isti.com".
// 6. Products derived from this software may not be called "ISTI"
//    nor may "ISTI" appear in their names without prior written
//    permission of Instrumental Software Technologies, Inc.
// 7. Redistributions of any form whatsoever must retain the following
//    acknowledgment:
//    "This product includes software developed by Instrumental
//    Software Technologies, Inc. (http://www.isti.com/)."
// THIS SOFTWARE IS PROVIDED BY INSTRUMENTAL SOFTWARE
// TECHNOLOGIES, INC. "AS IS" AND ANY EXPRESSED OR IMPLIED
// WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES
// OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
// DISCLAIMED.  IN NO EVENT SHALL INSTRUMENTAL SOFTWARE TECHNOLOGIES,
// INC. OR ITS CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
// INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
// (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
// SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
// HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
// STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
// ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED
// OF THE POSSIBILITY OF SUCH DAMAGE.
//=====================================================================
//  A current version of the software can be found at
//                http://www.isti.com
//  Bug reports and comments should be directed to
//  Instrumental Software Technologies, Inc. at info@isti.com
//=====================================================================
//

package com.isti.util;

import java.io.Serializable;

/**
 * Class BooleanCp is a 'Boolean'-like wrapper class which holds a
 * boolean value and implements the Comparable interface.
 */
public class BooleanCp extends Number implements Comparable,Serializable
{
  private Boolean booleanObj;

    /**
     * Allocates a BooleanCp object representing the value argument.
     * @param value the value of the BooleanCp.
     */
  public BooleanCp(boolean value)
  { booleanObj = Boolean.valueOf(value); }

    /**
     * Allocates a BooleanCp object representing the value true if the
     * string argument is not null and is equal, ignoring case, to the
     * string "true". Otherwise, allocate a Boolean object representing
     * the value false. Examples:
     *
     * new BooleanCp("True") produces a BooleanCp object that represents
     * true.
     * new BooleanCp("yes") produces a BooleanCp object that represents
     * false.
     * @param str the string to be converted to a BooleanCp.
     */
  public BooleanCp(String str)
  { booleanObj = Boolean.valueOf(str); }

    /**
     * Allocates a BooleanCp object which holds the given Boolean object.
     * @param bObj the value of the BooleanCp.
     */
  public BooleanCp(Boolean bObj)
  { booleanObj = bObj; }

    /**
     * Returns the handle of the Boolean object held by this object.
     * @return boolean object
     */
  public Boolean getBooleanObj()
  { return booleanObj; }

    /**
     * Returns the value of this BooleanCp object as a boolean primitive.
     * @return The primitive boolean value of this object.
     */
  public boolean booleanValue()
  { return booleanObj.booleanValue(); }

    /**
     * Returns the boolean value represented by the specified String.
     * A new BooleanCp object is constructed.  This BooleanCp represents
     * the value true if the string argument is not null and is equal,
     * ignoring case, to the string "true".
     *
     * Example: BooleanCp.valueOf("True") returns true.
     * Example: BooleanCp.valueOf("yes") returns false.
     * @param s a string.
     * @return The Boolean value represented by the string.
     */
  public static BooleanCp valueOf(String s)
  { return new BooleanCp(Boolean.valueOf(s)); }

    /**
     * Returns a String object representing this BooleanCp's value.
     * If this object represents the value true, a string equal to
     * "true" is returned. Otherwise, a string equal to "false" is
     * returned.
     * @return A string representation of this object.
     */
  public String toString()
  { return booleanObj.toString(); }

    /**
     * Returns a hash code for this BooleanCp object.
     * @return The integer 1231 if this object represents true;
     * returns the integer 1237 if this object represents false.
     */
  public int hashCode()
  { return booleanObj.hashCode(); }

    /**
     * Returns true if and only if the argument is not null and is a
     * BooleanCp or Boolean object that represents the same boolean
     * value as this object.
     * @param obj the object to compare with.
     * @return true if the Boolean objects represent the same value;
     * false otherwise.
     */
  public boolean equals(Object obj)
  {
    if(obj instanceof BooleanCp)  //if BooleanCp then use held Boolean obj
      return booleanObj.equals(((BooleanCp)obj).getBooleanObj());
    return booleanObj.equals(obj);     //otherwise then use param directly
  }

    /**
     * Returns the value of this object as an int.
     * @return integer value
     */
  public int intValue()
  { return booleanObj.booleanValue() ? 1 : 0; }

    /**
     * Returns the value of this object as a long.
     * @return long value
     */
  public long longValue()
  { return booleanObj.booleanValue() ? (long)1 : (long)0; }

    /**
     * Returns the value of this object as a float.
     * @return float value
     */
  public float floatValue()
  { return booleanObj.booleanValue() ? (float)1.0 : (float)0.0; }

    /**
     * Returns the value of this object as a double.
     * @return double value
     */
  public double doubleValue()
  { return booleanObj.booleanValue() ? (double)1.0 : (double)0.0; }

    /**
     * Returns the value of this object as a byte.
     * @return byte value
     */
  public byte byteValue()
  { return booleanObj.booleanValue() ? (byte)1 : (byte)0; }

    /**
     * Returns the value of this object as a short.
     * @return short value
     */
  public short shortValue()
  { return booleanObj.booleanValue() ? (short)1 : (short)0; }

    /**
     * Compares this object with the specified object 'obj'.  'false' is
     * considered to be "less than" 'true'.
     * @param obj an object to compare to.
     * @return A negative integer, zero, or a positive integer as this
     * object is less than, equal to, or greater than the specified object.
     * @exception ClassCastException if the specified object's type
     * prevents it from being compared to this object.
     */
  public int compareTo(Object obj)
  {
         //if 'obj' is a BooleanCp then cast it and get boolean value;
         // otherwise assume it's a Boolean and cast it to that:
    boolean objVal = (obj instanceof BooleanCp) ?
            ((BooleanCp)obj).booleanValue() : ((Boolean)obj).booleanValue();

    if(booleanObj.booleanValue() == objVal)
      return 0;         //if both the same then return 0 for equal
         //if this = 'true' then (true - false) = 1
         //if this = 'false' then (false - true) = -1
    return booleanObj.booleanValue() ? 1 : -1;
  }
}

