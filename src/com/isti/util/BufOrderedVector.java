//BufOrderedVector.java:  Extension of 'Vector' that supports specifying
//                        items to be returned last by the iterators.
//
//  6/18/2005 -- [ET]
//

package com.isti.util;

import java.util.Collection;
import java.util.List;
import java.util.ArrayList;
import java.util.Vector;
import java.util.Iterator;
import java.util.ListIterator;

/**
 * Class BufOrderedVector is an extension of 'Vector' that supports
 * specifying items to be returned last by the iterators.
 */
public class BufOrderedVector extends Vector
{
  protected List iteratorBufList = new ArrayList();
  protected List iterateLastList = null;
  protected final Object changeIterateLastListSyncObj = new Object();;

  /**
   * Constructs an empty vector with the specified initial capacity and
   * capacity increment.
   * @param   initialCapacity     the initial capacity of the vector.
   * @param   capacityIncrement   the amount by which the capacity is
   *                              increased when the vector overflows.
   * @exception IllegalArgumentException if the specified initial capacity
   *               is negative.
   */
  public BufOrderedVector(int initialCapacity, int capacityIncrement)
  {
    super(initialCapacity,capacityIncrement);
  }

  /**
   * Constructs an empty vector with the specified initial capacity and
   * with its capacity increment equal to zero.
   * @param   initialCapacity   the initial capacity of the vector.
   * @exception IllegalArgumentException if the specified initial capacity
   *               is negative
   */
  public BufOrderedVector(int initialCapacity)
  {
    super(initialCapacity);
  }

  /**
   * Constructs an empty vector so that its internal data array
   * has size <tt>10</tt> and its standard capacity increment is
   * zero.
   */
  public BufOrderedVector()
  {
  }

  /**
   * Constructs a vector containing the elements of the specified
   * collection, in the order they are returned by the collection's
   * iterator.
   * @param c the collection whose elements are to be placed into this
   *       vector.
   */
  public BufOrderedVector(Collection c)
  {
    super(c);
    syncIteratorBufList();        //synchronize buffered list for iterators
  }

  /**
   * Copies the components of this vector into the specified array. The
   * item at index <tt>k</tt> in this vector is copied into component
   * <tt>k</tt> of <tt>anArray</tt>. The array must be big enough to hold
   * all the objects in this vector, else an
   * <tt>IndexOutOfBoundsException</tt> is thrown.
   *
   * @param   anArray   the array into which the components get copied.
   */
  public synchronized void copyInto(Object anArray[])
  {
    super.copyInto(anArray);
    syncIteratorBufList();        //synchronize buffered list for iterators
  }

  /**
   * Sets the size of this vector. If the new size is greater than the
   * current size, new <code>null</code> items are added to the end of
   * the vector. If the new size is less than the current size, all
   * components at index <code>newSize</code> and greater are discarded.
   *
   * @param   newSize   the new size of this vector.
   * @throws  ArrayIndexOutOfBoundsException if new size is negative.
   */
  public synchronized void setSize(int newSize)
  {
    super.setSize(newSize);
    syncIteratorBufList();        //synchronize buffered list for iterators
  }

  /**
   * Sets the component at the specified <code>index</code> of this
   * vector to be the specified object. The previous component at that
   * position is discarded.<p>
   *
   * The index must be a value greater than or equal to <code>0</code>
   * and less than the current size of the vector. <p>
   *
   * This method is identical in functionality to the set method
   * (which is part of the List interface). Note that the set method reverses
   * the order of the parameters, to more closely match array usage.  Note
   * also that the set method returns the old value that was stored at the
   * specified position.
   *
   * @param      obj     what the component is to be set to.
   * @param      index   the specified index.
   * @exception  ArrayIndexOutOfBoundsException  if the index was invalid.
   * @see        #size()
   * @see        List
   * @see	   #set(int, java.lang.Object)
   */
  public synchronized void setElementAt(Object obj, int index)
  {
    super.setElementAt(obj,index);
    syncIteratorBufList();        //synchronize buffered list for iterators
  }

  /**
   * Deletes the component at the specified index. Each component in
   * this vector with an index greater or equal to the specified
   * <code>index</code> is shifted downward to have an index one
   * smaller than the value it had previously. The size of this vector
   * is decreased by <tt>1</tt>.<p>
   *
   * The index must be a value greater than or equal to <code>0</code>
   * and less than the current size of the vector. <p>
   *
   * This method is identical in functionality to the remove method
   * (which is part of the List interface).  Note that the remove method
   * returns the old value that was stored at the specified position.
   *
   * @param      index   the index of the object to remove.
   * @exception  ArrayIndexOutOfBoundsException  if the index was invalid.
   * @see        #size()
   * @see	   #remove(int)
   * @see	   List
   */
  public synchronized void removeElementAt(int index)
  {
    super.removeElementAt(index);
    syncIteratorBufList();        //synchronize buffered list for iterators
  }

  /**
   * Inserts the specified object as a component in this vector at the
   * specified <code>index</code>. Each component in this vector with
   * an index greater or equal to the specified <code>index</code> is
   * shifted upward to have an index one greater than the value it had
   * previously. <p>
   *
   * The index must be a value greater than or equal to <code>0</code>
   * and less than or equal to the current size of the vector. (If the
   * index is equal to the current size of the vector, the new element
   * is appended to the Vector.)<p>
   *
   * This method is identical in functionality to the add(Object, int) method
   * (which is part of the List interface). Note that the add method reverses
   * the order of the parameters, to more closely match array usage.
   *
   * @param      obj     the component to insert.
   * @param      index   where to insert the new component.
   * @exception  ArrayIndexOutOfBoundsException  if the index was invalid.
   * @see        #size()
   * @see	   #add(int, Object)
   * @see	   List
   */
  public synchronized void insertElementAt(Object obj, int index)
  {
    super.insertElementAt(obj,index);
    syncIteratorBufList();        //synchronize buffered list for iterators
  }

  /**
   * Adds the specified component to the end of this vector,
   * increasing its size by one. The capacity of this vector is
   * increased if its size becomes greater than its capacity. <p>
   *
   * This method is identical in functionality to the add(Object) method
   * (which is part of the List interface).
   *
   * @param   obj   the component to be added.
   * @see	   #add(Object)
   * @see	   List
   */
  public synchronized void addElement(Object obj)
  {
    super.addElement(obj);
    syncIteratorBufList();        //synchronize buffered list for iterators
  }

  /**
   * Removes the first (lowest-indexed) occurrence of the argument
   * from this vector. If the object is found in this vector, each
   * component in the vector with an index greater or equal to the
   * object's index is shifted downward to have an index one smaller
   * than the value it had previously.<p>
   *
   * This method is identical in functionality to the remove(Object)
   * method (which is part of the List interface).
   *
   * @param   obj   the component to be removed.
   * @return  <code>true</code> if the argument was a component of this
   *          vector; <code>false</code> otherwise.
   * @see	List#remove(Object)
   * @see	List
   */
  public synchronized boolean removeElement(Object obj)
  {
    final boolean retFlag = super.removeElement(obj);
    syncIteratorBufList();        //synchronize buffered list for iterators
    return retFlag;
  }

  /**
   * Removes all components from this vector and sets its size to zero.<p>
   *
   * This method is identical in functionality to the clear method
   * (which is part of the List interface).
   *
   * @see	#clear
   * @see	List
   */
  public synchronized void removeAllElements()
  {
    super.removeAllElements();
    syncIteratorBufList();        //synchronize buffered list for iterators
  }

  /**
   * Replaces the element at the specified position in this Vector with the
   * specified element.
   *
   * @param index index of element to replace.
   * @param element element to be stored at the specified position.
   * @return the element previously at the specified position.
   * @exception ArrayIndexOutOfBoundsException index out of range
   *		  (index &lt; 0 || index &gt;= size()).
   * @exception IllegalArgumentException fromIndex &gt; toIndex.
   */
  public synchronized Object set(int index, Object element)
  {
    final Object retObj = super.set(index,element);
    syncIteratorBufList();        //synchronize buffered list for iterators
    return retObj;
  }

  /**
   * Appends the specified element to the end of this Vector.
   *
   * @param o element to be appended to this Vector.
   * @return true (as per the general contract of Collection.add).
   */
  public synchronized boolean add(Object o)
  {
    final boolean retFlag = super.add(o);
    syncIteratorBufList();        //synchronize buffered list for iterators
    return retFlag;
  }

  /**
   * Removes the element at the specified position in this Vector.
   * shifts any subsequent elements to the left (subtracts one from their
   * indices).  Returns the element that was removed from the Vector.
   *
   * @exception ArrayIndexOutOfBoundsException index out of range (index
   * 		  &lt; 0 || index &gt;= size()).
   * @param index the index of the element to removed.
   */
  public synchronized Object remove(int index)
  {
    final Object retObj = super.remove(index);
    syncIteratorBufList();        //synchronize buffered list for iterators
    return retObj;
  }

  /**
   * Appends all of the elements in the specified Collection to the end of
   * this Vector, in the order that they are returned by the specified
   * Collection's Iterator.  The behavior of this operation is undefined if
   * the specified Collection is modified while the operation is in progress.
   * (This implies that the behavior of this call is undefined if the
   * specified Collection is this Vector, and this Vector is nonempty.)
   *
   * @param c elements to be inserted into this Vector.
   * @exception ArrayIndexOutOfBoundsException index out of range (index
   *		  &lt; 0 || index &gt; size()).
   */
  public synchronized boolean addAll(Collection c)
  {
    final boolean retFlag = super.addAll(c);
    syncIteratorBufList();        //synchronize buffered list for iterators
    return retFlag;
  }

  /**
   * Removes from this Vector all of its elements that are contained in the
   * specified Collection.
   *
   * @return true if this Vector changed as a result of the call.
   */
  public synchronized boolean removeAll(Collection c)
  {
    final boolean retFlag = super.removeAll(c);
    syncIteratorBufList();        //synchronize buffered list for iterators
    return retFlag;
  }

  /**
   * Retains only the elements in this Vector that are contained in the
   * specified Collection.  In other words, removes from this Vector all
   * of its elements that are not contained in the specified Collection.
   *
   * @return true if this Vector changed as a result of the call.
   */
  public synchronized boolean retainAll(Collection c)
  {
    final boolean retFlag = super.retainAll(c);
    syncIteratorBufList();        //synchronize buffered list for iterators
    return retFlag;
  }

  /**
   * Inserts all of the elements in in the specified Collection into this
   * Vector at the specified position.  Shifts the element currently at
   * that position (if any) and any subsequent elements to the right
   * (increases their indices).  The new elements will appear in the Vector
   * in the order that they are returned by the specified Collection's
   * iterator.
   *
   * @param index index at which to insert first element
   *		    from the specified collection.
   * @param c elements to be inserted into this Vector.
   * @exception ArrayIndexOutOfBoundsException index out of range (index
   *		  &lt; 0 || index &gt; size()).
   */
  public synchronized boolean addAll(int index, Collection c)
  {
    final boolean retFlag = super.addAll(index,c);
    syncIteratorBufList();        //synchronize buffered list for iterators
    return retFlag;
  }

  /**
   * Returns an iterator over the elements in this list in proper sequence.
   * This method references the buffered list.
   * @return an iterator over the elements in this list in proper sequence.
   */
  public synchronized Iterator iterator()
  {
    return (iteratorBufList != this) ? iteratorBufList.iterator() :
                                                           super.iterator();
  }

  /**
   * Returns a list iterator of the elements in this list (in proper
   * sequence).
   * This method references the buffered list.
   * @return a list iterator of the elements in this list (in proper
   * 	       sequence).
   */
  public synchronized ListIterator listIterator()
  {
    return (iteratorBufList != this) ? iteratorBufList.listIterator() :
                                                       super.listIterator();
  }

  /**
   * Returns a list iterator of the elements in this list (in proper
   * sequence), starting at the specified position in this list.  The
   * specified index indicates the first element that would be returned by
   * an initial call to the <tt>next</tt> method.  An initial call to
   * the <tt>previous</tt> method would return the element with the
   * specified index minus one.
   * This method references the buffered list.
   * @param index index of first element to be returned from the
   *		    list iterator (by a call to the <tt>next</tt> method).
   * @return a list iterator of the elements in this list (in proper
   * 	       sequence), starting at the specified position in this list.
   * @throws IndexOutOfBoundsException if the index is out of range (index
   *         &lt; 0 || index &gt; size()).
   */
  public synchronized ListIterator listIterator(int index)
  {
    return (iteratorBufList != this) ? iteratorBufList.listIterator(index) :
                                                  super.listIterator(index);
  }

  /**
   * Synchronizes the buffered list for iterators.  This method should
   * be called anytime that this BufOrderedVector is modified.
   */
  protected final void syncIteratorBufList()
  {
         //setup local handle to iterate-last list:
    final List localIterateLastList;
    synchronized(changeIterateLastListSyncObj)
    {    //only allow one thread at a time to change list object
      localIterateLastList = iterateLastList;    //setup local handle
    }
    int listCount;
    if(size() > 0 && localIterateLastList != null &&
                                (listCount=localIterateLastList.size()) > 0)
    {    //this Vector is not empty and iterate-last list is not empty
      iteratorBufList = new ArrayList(size());   //create buffered list
      final Iterator iterObj = super.iterator();
      Object obj;
      while(iterObj.hasNext())
      {  //for each item in this Vector
              //if item not in iterate-last list then add to buffered list:
        if(!localIterateLastList.contains(obj=iterObj.next()))
          iteratorBufList.add(obj);
      }
      do
      {  //for each item in iterate-last list
              //if item in this Vector then add to buffered list:
        if(contains(obj=localIterateLastList.get(--listCount)))
          iteratorBufList.add(obj);
      }
      while(listCount > 0);
    }
    else      //this Vector is empty or iterate-last list is empty
      iteratorBufList = this;     //make buffered list same is this Vector
  }

  /**
   * Enters the given item into the iterate-last list at the given
   * position.  If an item does not exist at the given position on
   * the iterate-last list then the given item is added to the end
   * of the list.  A 'null' item parameter may be given to remove
   * the current item at the given position.  The iterate-last list
   * specifies items that should be returned last when iterating over
   * this BufOrderedVector.  The first item in the iterate-last list will
   * be the last item returned by this BufOrderedVector's iterator.
   * @param idx index position for new item.
   * @param itemObj item to enter, or 'null' to remove item at
   * given index position.
   */
  public synchronized void setIterateLastListItem(int idx, Object itemObj)
  {
    final List listObj;      //setup local handle to list
    synchronized(changeIterateLastListSyncObj)
    {    //only allow one thread at a time to change list object
      if(iterateLastList == null)
      {  //list does not exist
        if(itemObj == null)
          return;            //if given item is null then just return
        iterateLastList = new ArrayList();       //create new list object
      }
      listObj = iterateLastList;       //set local handle to list
    }
    if(itemObj != null)
    {  //given item not null
      if(idx < listObj.size())            //if list large enough then
        listObj.set(idx,itemObj);         //replace item at index
      else                                //if list not large enough then
        listObj.add(itemObj);             //add item to end of list
    }
    else
    {  //given item is null
      if(idx < listObj.size())            //if list large enough then
        listObj.remove(idx);              //remove item at index
    }
    syncIteratorBufList();        //synchronize buffered list for iterators
  }

  /**
   * Returns the item at the specified index in the iterate-last list.
   * The iterate-last list specifies items that should be returned
   * last when iterating over this BufOrderedVector.  The first item
   * in the iterate-last list will be the last item returned by this
   * BufOrderedVector's iterator.
   * @param idx index position for the returned item.
   * @return The specified item, or null if none exists at the given
   * position.
   */
  public synchronized Object getIterateLastListItem(int idx)
  {
    final List listObj;      //setup local handle to list
    synchronized(changeIterateLastListSyncObj)
    {    //only allow one thread at a time to change list object
      if(iterateLastList == null)           //if not created then
        return null;                        //return null
      listObj = iterateLastList;       //set local handle to list
    }
    return (idx < listObj.size()) ? listObj.get(idx) : null;
  }

  /**
   * Clears the iterate-last list.  The iterate-last list specifies
   * items that should be returned last when iterating over this
   * BufOrderedVector.
   */
  public synchronized void clearIterateLastList()
  {
    synchronized(changeIterateLastListSyncObj)
    {    //only allow one thread at a time to change list object
      iterateLastList = null;
    }
    syncIteratorBufList();        //synchronize buffered list for iterators
  }
}
