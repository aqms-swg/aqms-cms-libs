//CallBackNoParam.java:  Interface for classes that implement a
//                       no-parameter call-back method.
//
//  6/26/2013 -- [ET]  Modified from 'CallBackStringFlag' interface.
//

package com.isti.util;

/**
 * CallBackNoParam is an interface for classes that implement a string
 * parameter call-back method with a return flag.
 */
public interface CallBackNoParam
{
  /**
   * Method called by the implementing class.
   */
  public void callBackMethod();
}
