//CallBackStringFlag.java:  Interface for classes that implement a string
//                          parameter call-back method with a return flag.
//
//  5/10/2006 -- [ET]  Modified from 'CallBackStringParam' interface.
//

package com.isti.util;

/**
 * CallBackStringFlag is an interface for classes that implement a string
 * parameter call-back method with a return flag.
 */
public interface CallBackStringFlag
{
  /**
   * Method called by the implementing class.
   * @param str string parameter to be passed.
   * @return flag value returned to invoker.
   */
  public boolean callBackFlagMethod(String str);
}
