package com.isti.util;

/**
 * Interface for classes that implement a string parameter call-back method.
 *
 *   7/9/2003 -- [ET]  Renamed from 'CallbackStringParam'; changed method
 *                     name to 'callBackMethod'; added more Javadoc.
 *
 */
public interface CallBackStringParam {

  /**
   * Method called by the implementing class.
   * @param str string parameter to be passed.
   */
  public void callBackMethod(String str);
}
