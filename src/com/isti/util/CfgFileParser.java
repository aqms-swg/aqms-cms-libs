//CfgFileParser:  Contains support functions for reading from
//                configuration files.
//
//   7/12/2000 -- [ET]  Initial release version.
//    2/8/2002 -- [ET]  Added support for passing the 'commaSepFlag'
//                      parameter to the 'CfgFileTokenizer' object.
//   3/15/2007 -- [ET]  Added 'resetInput()' method.
//
//
//=====================================================================
// Copyright (C) 2007 Instrumental Software Technologies, Inc.
//
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions
// are met:
// 1. Redistributions of source code, or portions of this source code,
//    must retain the above copyright notice, this list of conditions
//    and the following disclaimer.
// 2. Redistributions in binary form must reproduce the above copyright
//    notice, this list of conditions and the following disclaimer in
//    the documentation and/or other materials provided with the
//    distribution.
// 3. All advertising materials mentioning features or use of this
//    software must display the following acknowledgment:
//    "This product includes software developed by Instrumental
//    Software Technologies, Inc. (http://www.isti.com)"
// 4. If the software is provided with, or as part of a commercial
//    product, or is used in other commercial software products the
//    customer must be informed that "This product includes software
//    developed by Instrumental Software Technologies, Inc.
//    (http://www.isti.com)"
// 5. The names "Instrumental Software Technologies, Inc." and "ISTI"
//    must not be used to endorse or promote products derived from
//    this software without prior written permission. For written
//    permission, please contact "info@isti.com".
// 6. Products derived from this software may not be called "ISTI"
//    nor may "ISTI" appear in their names without prior written
//    permission of Instrumental Software Technologies, Inc.
// 7. Redistributions of any form whatsoever must retain the following
//    acknowledgment:
//    "This product includes software developed by Instrumental
//    Software Technologies, Inc. (http://www.isti.com/)."
// THIS SOFTWARE IS PROVIDED BY INSTRUMENTAL SOFTWARE
// TECHNOLOGIES, INC. "AS IS" AND ANY EXPRESSED OR IMPLIED
// WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES
// OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
// DISCLAIMED.  IN NO EVENT SHALL INSTRUMENTAL SOFTWARE TECHNOLOGIES,
// INC. OR ITS CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
// INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
// (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
// SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
// HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
// STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
// ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED
// OF THE POSSIBILITY OF SUCH DAMAGE.
//=====================================================================
//  A current version of the software can be found at
//                http://www.isti.com
//  Bug reports and comments should be directed to
//  Instrumental Software Technologies, Inc. at info@isti.com
//=====================================================================
//

package com.isti.util;

import java.io.Reader;
import java.io.IOException;

/**
 * Class CfgFileParser contains support functions for reading from
 * configuration files.
 */
public class CfgFileParser
{
         //define bracket characters:
  protected static final char OPEN_BRACKET_CHAR = '{';
  protected static final char CLOSE_BRACKET_CHAR = '}';
  protected final boolean wordSlashFlag;
  protected final boolean commaSepFlag;
  protected CfgFileTokenizer inTokens;      //tokenizer object
  protected String errorMessage = null;     //error message from parsing

  /**
   * Creates parser object which reads from the given stream.  The comma
   * character is parsed as part of a word token.
   * @param rdr reader
   * @param wordSlashFlag true to allow the forward-slash character to be
   * parsed as part of a word token (this disables '//' comments); false
   * to make the forward-slash character be a token separator.
   * @param commaSepFlag true for comma to be a token separator; false
   * for comma to be parsed as part of a word token.
   */
  public CfgFileParser(Reader rdr, boolean wordSlashFlag,
                                                       boolean commaSepFlag)
  {
    this.wordSlashFlag = wordSlashFlag;
    this.commaSepFlag = commaSepFlag;
    inTokens = new CfgFileTokenizer(rdr,wordSlashFlag,commaSepFlag);
  }

  /**
   * Creates parser object which reads from the given stream.  The comma
   * character is parsed as part of a word token.
   * @param rdr reader
   * @param wordSlashFlag true to allow the forward-slash character to be
   * parsed as part of a word token (this disables '//' comments); false
   * to make the forward-slash character be a token separator.
   */
  public CfgFileParser(Reader rdr, boolean wordSlashFlag)
  {
    this(rdr,wordSlashFlag,false);
  }

  /**
   * Creates parser object which reads from the given stream.  The
   * forward-slash character is treated as a separator and the comma
   * character is parsed as part of a word token.
   * @param rdr reader
   */
  public CfgFileParser(Reader rdr)
  {
    this(rdr,false,false);
  }

  /**
   * Resets the input stream for this parser.
   * @param rdr reader input stream object to use.
   */
  public void resetInput(Reader rdr)
  {
    inTokens = new CfgFileTokenizer(rdr,wordSlashFlag,commaSepFlag);
    errorMessage = null;          //clear any previous error message
  }

  /**
   * Parses "equals" sign ('=') followed by a string.
   * @return The string if successful; returns null and
   * builds error message if error.
   * @throws IOException
   */
  protected String parseEqualsString() throws IOException
  {
    int tType;

    if((tType=inTokens.nextToken()) != CfgFileTokenizer.TT_EOF)
    {    //not end-of-file
      if(tType != (int)'=')
      {         //equals sign not found
        setIllegalCharMsg(tType);           //build error message
        errorMessage += " ('=' expected)";  //append extra
        return null;
      }
      if((tType=inTokens.nextToken()) != CfgFileTokenizer.TT_EOF)
      {       //not end-of-file
        if(tType != CfgFileTokenizer.TT_WORD)
        {     //word-token not found
          setIllegalCharMsg(tType);      //build error message
          return null;
        }
        return inTokens.getNonNullTokenString();      //fetch & ret string
      }
    }
    setUnexpectedEOFMsg();        //build unexpected EOF message
    return null;                  //return error flag
  }

  /**
   * Parses "equals" sign ('=') followed by a number.
   * @return The number if successful; returns false and
   * builds error message if error.
   * @throws IOException
   */
  protected Number parseEqualsNumber() throws IOException
  {
    String str;
    Number num;

    if((str=parseEqualsString()) == null)                  //parse " = str"
      return null;           //if error then return null
    if((num=UtilFns.parseNumber(str)) == null)             //convert to #
    {         //error processing as numeric; build error message
      errorMessage = "Illegal numeric value (\"" + str +
                                         "\") on line " + inTokens.lineno();
      return null;
    }
    return num;         //return number
  }

  /**
   * Builds "Illegal character" error message with given character value.
   * @param charVal character value
   */
  protected void setIllegalCharMsg(int charVal)
  {
    if(charVal >= (int)' ' && charVal < 127)
    {         //character is printable; build message with character:
      errorMessage = "Illegal character " + "'" + (char)charVal +
                                           "' on line " + inTokens.lineno();
    }
    else
    {         //not printable character; build alternate error message:
      errorMessage = "Illegal or missing character(s) on line " +
                                                          inTokens.lineno();
    }
  }

  /** Builds "Unexpected end-of-file" error message. */
  protected void setUnexpectedEOFMsg()
  {
    errorMessage = "Unexpected end-of-file on line " + inTokens.lineno();
  }

  /** Builds "Open bracket not found" error message. */
  protected void setNoOpenBracketMsg()
  {
    errorMessage = "Open bracket ('" + OPEN_BRACKET_CHAR +
                                "') not found on line " + inTokens.lineno();
  }

  /** Builds "Close bracket not found" error message. */
  protected void setNoCloseBracketMsg()
  {
    errorMessage = "Close bracket ('" + CLOSE_BRACKET_CHAR +
                                "') not found on line " + inTokens.lineno();
  }

  /**
   * Builds "Duplicate entry for field" error message with given string.
   * @param fieldName field name
   */
  protected void setDupFieldMsg(String fieldName)
  {
    errorMessage = "Duplicate entry for \"" + fieldName +
                                    "\" field at line " + inTokens.lineno();
  }

  /**
   * Returns message string for last error (or 'No error' if none).
   * @return error message
   */
  public String getErrorMessage()
  {
    return (errorMessage != null) ? errorMessage : "No error";
  }
}

