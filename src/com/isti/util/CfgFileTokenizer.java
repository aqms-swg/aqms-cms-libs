//CfgFileTokenizer:  Extends StreamTokenizer, sets up for configuration file
//                   parsing.
//
//    1/4/2000 -- [ET]
//   4/18/2000 -- [ET]  Removed forward-slash and added comma as separator.
//   4/19/2000 -- [ET]  Made parser response to forward-slash configurable
//                      via the 'wordSlashFlag' on the constructor.
//   7/12/2000 -- [ET]  Initial release version.
//    2/6/2002 -- [ET]  Added 'commaSepFlag' parameter to constructor
//                      (with new default functionality of comma being
//                      parsed as part of a word token); added optional
//                      'eolFlag' parameter to 'nextToken()' method;
//                      changed from extension of 'StreamTokenizer' to
//                      encapsulation.
//  12/23/2002 -- [ET]  Modified 'nextToken()' to properly return the
//                      last token even when it is not followed by an
//                      end-of-line character (and 'eolFlag' == true).
//
//
//=====================================================================
// Copyright (C) 2000 Instrumental Software Technologies, Inc.
//
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions
// are met:
// 1. Redistributions of source code, or portions of this source code,
//    must retain the above copyright notice, this list of conditions
//    and the following disclaimer.
// 2. Redistributions in binary form must reproduce the above copyright
//    notice, this list of conditions and the following disclaimer in
//    the documentation and/or other materials provided with the
//    distribution.
// 3. All advertising materials mentioning features or use of this
//    software must display the following acknowledgment:
//    "This product includes software developed by Instrumental
//    Software Technologies, Inc. (http://www.isti.com)"
// 4. If the software is provided with, or as part of a commercial
//    product, or is used in other commercial software products the
//    customer must be informed that "This product includes software
//    developed by Instrumental Software Technologies, Inc.
//    (http://www.isti.com)"
// 5. The names "Instrumental Software Technologies, Inc." and "ISTI"
//    must not be used to endorse or promote products derived from
//    this software without prior written permission. For written
//    permission, please contact "info@isti.com".
// 6. Products derived from this software may not be called "ISTI"
//    nor may "ISTI" appear in their names without prior written
//    permission of Instrumental Software Technologies, Inc.
// 7. Redistributions of any form whatsoever must retain the following
//    acknowledgment:
//    "This product includes software developed by Instrumental
//    Software Technologies, Inc. (http://www.isti.com/)."
// THIS SOFTWARE IS PROVIDED BY INSTRUMENTAL SOFTWARE
// TECHNOLOGIES, INC. "AS IS" AND ANY EXPRESSED OR IMPLIED
// WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES
// OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
// DISCLAIMED.  IN NO EVENT SHALL INSTRUMENTAL SOFTWARE TECHNOLOGIES,
// INC. OR ITS CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
// INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
// (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
// SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
// HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
// STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
// ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED
// OF THE POSSIBILITY OF SUCH DAMAGE.
//=====================================================================
//  A current version of the software can be found at
//                http://www.isti.com
//  Bug reports and comments should be directed to
//  Instrumental Software Technologies, Inc. at info@isti.com
//=====================================================================
//

package com.isti.util;

import java.io.IOException;
import java.io.PrintStream;
import java.io.Reader;
import java.io.StreamTokenizer;

/**
 * Class CfgFileTokenizer extends StreamTokenizer and sets up for
 * configuration file parsing.
 */
public class CfgFileTokenizer
{
  private final StreamTokenizer sTokenizerObj;
         //define characters for tokenizer:
  private static final char COMMENT_CHAR = '#';  //define comment character
  private static final char QUOTE_CHAR1 = '\'';  //define quote character
  private static final char QUOTE_CHAR2 = '\"';  //define quote character

  private String tokenString = null;        //var to hold token string data

    /**
     * A constant indicating that the end of the stream has been read.
     */
  public static final int TT_EOF = StreamTokenizer.TT_EOF;

    /**
     * A constant indicating that the end of the line has been read.
     */
  public static final int TT_EOL = StreamTokenizer.TT_EOL;

    /**
     * A constant indicating that a number token has been read.
     */
  public static final int TT_NUMBER = StreamTokenizer.TT_NUMBER;

    /**
     * A constant indicating that a word token has been read.
     */
  public static final int TT_WORD = StreamTokenizer.TT_WORD;

    /**
     * Creates an object which reads from the given input stream.
     * @param rdr the input stream to read from.
     * @param wordSlashFlag true to allow the forward-slash character to be
     * parsed as part of a word token (this disables '//' comments); false
     * to make the forward-slash character be a token separator.
     * @param commaSepFlag true for comma to be a token separator; false
     * for comma to be parsed as part of a word token.
     */
  public CfgFileTokenizer(Reader rdr,boolean wordSlashFlag,
                                                       boolean commaSepFlag)
  {
    sTokenizerObj = new StreamTokenizer(rdr);    //create parent tokenizer
              //set offset value to 1 if comma is a separator, 0 if not:
    final int commaOffs = (commaSepFlag) ? 1 : 0;
    sTokenizerObj.resetSyntax();                 //clear defaults
    sTokenizerObj.whitespaceChars(0,(int)' ');   //define whitespace chars
    //( "=34, #=35, '=39, *=42, ,=44, /=47, <=60, ==61, >=62, {=123, }=125 )
                                            //word-chars = all except:
    sTokenizerObj.wordChars((int)' '+1,(int)'#'-1);        // " #
    sTokenizerObj.wordChars((int)'#'+1,(int)'\''-1);       // '
    sTokenizerObj.wordChars((int)'\''+1,(int)'*'-1);       // *
    sTokenizerObj.wordChars((int)'*'+1,(int)','-commaOffs);     // ,
    if(!wordSlashFlag)
    {    //forward-slash is a separator; enter it as such
      sTokenizerObj.wordChars((int)','+commaOffs,(int)'/'-1);   // /
      sTokenizerObj.wordChars((int)'/'+1,(int)'<'-1);      // <
    }
    else      //forward-slash can be part of a word; leave it out
      sTokenizerObj.wordChars((int)','+commaOffs,(int)'<'-1);   // <
    sTokenizerObj.wordChars((int)'>'+1,(int)'{'-1);        // = >
    sTokenizerObj.wordChars((int)'{'+1,(int)'{'+1);        // {
    sTokenizerObj.wordChars((int)'}'+1,(int)'}'+1);        // }
    sTokenizerObj.slashSlashComments(true);      //enable C++ style comments
    sTokenizerObj.slashStarComments(true);       //enable 'C'-style comments
    sTokenizerObj.commentChar((int)COMMENT_CHAR);     //define comment char
    sTokenizerObj.quoteChar((int)QUOTE_CHAR1);   //define quote character
    sTokenizerObj.quoteChar((int)QUOTE_CHAR2);   //define quote character
    sTokenizerObj.eolIsSignificant(true);        //make end-of-line be a token
  }

    /**
     * Creates an object which reads from the given input stream.  Commas
     * are parsed as part of a word token.
     * @param rdr the input stream to read from.
     * @param wordSlashFlag true to allow the forward-slash character to be
     * parsed as part of a word token (this disables '//' comments); false
     * to make the forward-slash character be a token separator.
     */
  public CfgFileTokenizer(Reader rdr,boolean wordSlashFlag)
  {
    this(rdr,wordSlashFlag,false);
  }

    /**
     * Creates an object which reads from the given input stream.  The
     * forward-slash character is parsed as a token separator (and '//'
     * comments are enabled) and commas are parsed as part of a word token.
     * @param rdr the input stream to read from.
     */
  public CfgFileTokenizer(Reader rdr)
  {
    this(rdr,false,false);
  }

    /**
     * Parses the next token from the input stream.
     * @param eolFlag if true then all tokens and words until the next
     * end-of-line are read in and returned as a single token string.
     * @return 'StreamTokenizer.TT_WORD' if a word or a quoted string
     * token was found (retrieve token with 'getTokenString()');
     * 'StreamTokenizer.TT_NUMBER' if a numeric value was parsed
     * (retrieve token with 'getTokenString()'); or 'StreamTokenizer.TT_EOF'
     * if the end of the input stream was reached.
     * @exception IOException if an I/O error occurs.
     */
  public int nextToken(boolean eolFlag) throws IOException
  {
    int tType;
    StringBuffer buff = null;

    if(!eolFlag)
    {    //not reading to end-of-line
      if((tType=sTokenizerObj.nextToken()) == StreamTokenizer.TT_EOL)
      {  //leading end-of-line found
        int maxCount = 30000;
                   //skip any further leading end-of-lines (within limits):
        while((tType=sTokenizerObj.nextToken()) == StreamTokenizer.TT_EOL &&
                                                            --maxCount > 0);
      }
    }
    else      //reading to end-of-line
      tType = sTokenizerObj.nextToken();    //read first token
    readLoop:
    while(true)
    {    //for each token
      if(tType == StreamTokenizer.TT_EOF)
      {  //end of file reached
        if(buff == null)          //if no data in buffer then
          return tType;           //return end-of-file code
        break;               //exit while loop and return tokens
      }
      switch(tType)
      {
        case StreamTokenizer.TT_WORD:            //word token
          tokenString = sTokenizerObj.sval;      //store it
          break;
        case StreamTokenizer.TT_NUMBER:          //numeric token; conv to str
          tokenString = Double.toString(sTokenizerObj.nval);
          tType = StreamTokenizer.TT_WORD;       //convert type to word
          break;
        case (int)QUOTE_CHAR1:                   //quoted string
        case (int)QUOTE_CHAR2:
          tokenString = sTokenizerObj.sval;      //save it
          tType = StreamTokenizer.TT_WORD;       //convert type to word
          break;
        case StreamTokenizer.TT_EOL:             //end-of-line
          break readLoop;                        //exit while loop
        default:
      }
      if(!eolFlag)           //if not reading to end-of-line then
        return tType;        //return token type value (and 'tokenString')
      if(buff == null)                 //if not yet allocated then
        buff = new StringBuffer();     //create buffer
      else                             //if already allocated then
        buff.append(' ');              //put in separator
      if(tType == StreamTokenizer.TT_WORD)  //if string token then
        buff.append(tokenString);           //add to buffer
      else                             //if character token then
        buff.append((char)tType);      //add to buffer
      tType = sTokenizerObj.nextToken();       //read next token
    }
              //if buffer was allocated then store string version:
    tokenString = (buff != null) ? buff.toString() : "";
    return StreamTokenizer.TT_WORD;    //indicate word string-token type
  }

    /**
     * Parses the next token from the input stream.
     * @return 'StreamTokenizer.TT_WORD' if a word or a quoted string
     * token was found (retrieve token with 'getTokenString()') or
     * 'StreamTokenizer.TT_EOF' if the end of the input stream was
     * reached.
     * @exception IOException if an I/O error occurs.
     */
  public int nextToken() throws IOException
  {
    return nextToken(false);
  }

    /**
     * Returns the token string fetched by 'nextToken()'.
     * @return the token string, or null if none available.
     */
  public String getTokenString()
  { return tokenString; }

    /**
     * Returns the token string fetched by 'nextToken()'.
     * @return the token string, or an empty string if none available.
     */
  public String getNonNullTokenString()
  { return (tokenString != null) ? tokenString : ""; }

    /**
     * Returns the token fetched by 'nextToken()' as a numeric object.
     * @return One of the Number-extended class objects 'Integer',
     * 'Double' or 'params.Boolean.Cp'; or null if the token could not
     * be converted.
     */
  public Number getNumberToken()
  {
    return UtilFns.parseNumber(tokenString);
  }

    /**
     * Returns the current line number.
     * @return the current line number.
     */
  public int lineno()
  {
    return sTokenizerObj.lineno();
  }

    /**
     * Sends display of tokens to the given print stream.
     * @param out output stream
     * @exception IOException if an I/O error occurs.
     */
  public void dumpTokens(PrintStream out) throws IOException
  {
    int tType;

    while((tType=sTokenizerObj.nextToken()) != StreamTokenizer.TT_EOF)
    {
      switch(tType)
      {
        case StreamTokenizer.TT_WORD:
          out.println("word = " + sTokenizerObj.sval);
          break;
        case StreamTokenizer.TT_NUMBER:
          out.println("number = " + sTokenizerObj.nval);
          break;
        case (int)QUOTE_CHAR1:
          out.println("string = '" + sTokenizerObj.sval + "'");
          break;
        case (int)QUOTE_CHAR2:
          out.println("string = \"" + sTokenizerObj.sval + "\"");
          break;
        case StreamTokenizer.TT_EOL:
          out.println("EOL");
          break;
        default:
          out.println("char = '" + (char)tType + "'");
      }
    }
  }
}
