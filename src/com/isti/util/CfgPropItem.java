//CfgPropItem.java:  Defines a configuration property item used with the
//                   'CfgProperties' class.
//
//   5/10/2000 -- [ET]  Changed the 'stringValue()' method so that it
//                      converts non-String value objects to Strings.
//   7/12/2000 -- [ET]  Initial release version.
//   12/7/2000 -- [ET]  Removed 'setName()' method; added command-
//                      line parameter name, description string, "enable
//                      via command line" flag, "show in help screen
//                      data" and "via command line only" flag parameters.
//  11/27/2001 -- [ET]  Added support for multiple values in an item (via
//                      'value' and 'defaultValue' being arrays of objects);
//                      fixed some comments.
//    4/3/2002 -- [ET]  Added 'duplicate()' and 'set/getCmdLnLoadedFlag()'
//                      methods.
//    7/7/2002 -- [ET]  Implemented validator object; added
//                      'getLastChangeTime()' and 'set/getAuxiliaryObj()'
//                      methods; changed 'private' variables to 'protected'.
//   8/28/2002 -- [ET]  Fixed bug where setting a value that was the same as
//                      the current value would result in an error flag being
//                      returned.
//  10/15/2002 -- [KF]  Added support for Color values.
//  10/21/2002 -- [ET]  Added 'compareValueString()' method; added support
//                      for group-select object; changed 'setAuxiliaryObj()'
//                      to return handle to 'this'.
//  10/27/2002 -- [KF]  Do not use validator object if the value is invalid.
//  10/27/2002 -- [KF]  Added 'getMapEntry()' to return Map.Entry interface.
//  10/31/2002 -- [KF]  Use 'colorToPropertyString' and 'parseColor' for
//                      converting Color property values.
//   11/6/2002 -- [ET]  Added 'synchronized' to most methods.
//   1/28/2003 -- [ET]  Minor changes to comments.
//    3/5/2003 -- [KF]  Added number formatter for floating point values.
//   3/20/2003 -- [KF]  Modified to use 'UtilFns.floatNumberToPropertyString'
//                      method for floating point values.
//   5/30/2003 -- [ET]  Added 'ignoreItemFlag'.
//  10/10/2003 -- [KF]  Added cached Number,boolean,String and Color values,
//                      changed 'addDataChangedListener' method to return a
//                      handle to this object.
//    1/8/2004 -- [KF]  Added new 'setValueString' method with default flag.
//   2/11/2004 -- [ET]  Added 'set/getEmptyStringDefaultFlag()' methods;
//                      fixed 'equals()' method to properly evaluate a
//                      given 'CfgPropItem' object; modified 'equals()'
//                      and 'hashCode()' to deal properly with array values.
//   4/13/2004 -- [ET]  Changed 'fireDataChanged()' method from "protected"
//                      to "public".
//    6/4/2004 -- [ET]  Added 'setDefaultAndValue()' method.
//    1/7/2005 -- [ET]  Added 'ItemGroupSelector' class.
//  12/19/2005 -- [KF]  Added the 'toArchivedForm()' method to support
//                      other locales.
//    1/5/2006 -- [KF]  Added support for "Archivable" data.
//    3/6/2006 -- [KF]  Added support for 'PropItemInterface'.
//    6/4/2008 -- [KF]  Added 'isSwitchChar()' method.
//  10/30/2019 -- [KF]  Added 'getNoSaveItemFlag()' method.
//   3/19/2020 -- [KF]  Added 'stringArrayValue' method'.
//   3/16/2021 -- [KF]  Added 'setDefaultValueString' method.
//   3/17/2021 -- [KF]  Added 'isBoolean' and 'reset' methods.
//
//
//=====================================================================
// Copyright (C) 2021 Instrumental Software Technologies, Inc.
//
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions
// are met:
// 1. Redistributions of source code, or portions of this source code,
//    must retain the above copyright notice, this list of conditions
//    and the following disclaimer.
// 2. Redistributions in binary form must reproduce the above copyright
//    notice, this list of conditions and the following disclaimer in
//    the documentation and/or other materials provided with the
//    distribution.
// 3. All advertising materials mentioning features or use of this
//    software must display the following acknowledgment:
//    "This product includes software developed by Instrumental
//    Software Technologies, Inc. (http://www.isti.com)"
// 4. If the software is provided with, or as part of a commercial
//    product, or is used in other commercial software products the
//    customer must be informed that "This product includes software
//    developed by Instrumental Software Technologies, Inc.
//    (http://www.isti.com)"
// 5. The names "Instrumental Software Technologies, Inc." and "ISTI"
//    must not be used to endorse or promote products derived from
//    this software without prior written permission. For written
//    permission, please contact "info@isti.com".
// 6. Products derived from this software may not be called "ISTI"
//    nor may "ISTI" appear in their names without prior written
//    permission of Instrumental Software Technologies, Inc.
// 7. Redistributions of any form whatsoever must retain the following
//    acknowledgment:
//    "This product includes software developed by Instrumental
//    Software Technologies, Inc. (http://www.isti.com/)."
// THIS SOFTWARE IS PROVIDED BY INSTRUMENTAL SOFTWARE
// TECHNOLOGIES, INC. "AS IS" AND ANY EXPRESSED OR IMPLIED
// WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES
// OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
// DISCLAIMED.  IN NO EVENT SHALL INSTRUMENTAL SOFTWARE TECHNOLOGIES,
// INC. OR ITS CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
// INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
// (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
// SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
// HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
// STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
// ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED
// OF THE POSSIBILITY OF SUCH DAMAGE.
//=====================================================================
//  A current version of the software can be found at
//                http://www.isti.com
//  Bug reports and comments should be directed to
//  Instrumental Software Technologies, Inc. at info@isti.com
//=====================================================================
//

package com.isti.util;

import java.util.Vector;
import java.util.Map;
import java.util.Enumeration;
import java.awt.Color;
import java.io.Serializable;

/**
 * Class CfgPropItem defines a configuration property item used with the
 * 'CfgProperties' (or 'AppletProperties') class.  The item has a name, a
 * value, a default value, a loaded-flag (set true if the item has been
 * "loaded"), a required-flag (set true if the item must be "loaded"), a
 * command-line parameter name, a description string, and several
 * additional flags.
 */
public class CfgPropItem implements Comparable,Serializable,PropItemInterface
{
  private static final String NO_VALUE = "";
  public static final char SWITCH1_CHAR = '-';   //first switch character
  public static final char SWITCH2_CHAR = '/';   //second switch character
  protected final String name;         //property name
  protected Object value;              //property value
  protected Number numberValue;        //property number value
  protected boolean booleanValue;      //property boolean value
  protected String stringValue;        //property string value
  protected Color colorValue;          //property color value
  protected Object defaultValue = NO_VALUE; //default property value
  protected boolean loadedFlag = false;     //true if loaded from file
  protected boolean requiredFlag;      //true if must be loaded
  protected boolean cmdLnLoadedFlag = false;  //true if loaded via cmd line
  protected final String cmdLnParamName; //command-line parameter name
  protected final String descriptionStr; //property item description string
  protected final boolean cmdLnEnbFlag;  //true to enable via command line
  protected boolean helpScreenFlag;      //true to show in help screen data
  protected final boolean cmdLnOnlyFlag; //true for via command line only
                                         //true for no save or help screen:
  protected boolean ignoreItemFlag = false;
                                         //true for no save or help screen:
  protected boolean noSaveItemFlag = false;
                        //true for "" equals default-value in config file:
  protected boolean emptyStringDefaultFlag = false;
                                         //validator object for item:
  protected CfgPropValidator validatorObj = null;
                                            //'DataChangedListener' objects:
  protected final Vector listenersVec = new Vector();
  protected long lastChangeTime = 0;        //time of last change to value
  protected Object groupSelObj = null;      //group selection object
  protected Object auxiliaryObj = null;     //auxiliary object for this item
  protected boolean startupConfigFlag = false;  //startup config flag


    /**
     * Creates a configuration property item.
     * @param name property name.
     * @param defaultValue default value for the property.
     * @param requiredFlag the value for the item's required-flag (see
     * 'setRequiredFlag()').
     * @param paramNameStr command-line parameter name for item.
     * @param descriptionStr description text for item.
     * @param cmdLnEnbFlag true to enable use of item as command-line
     * parameter.
     * @param helpScreenFlag true to include item in help screen data.
     * @param cmdLnOnlyFlag true for item via command line only.
     */
  public CfgPropItem(String name,Object defaultValue,boolean requiredFlag,
             String paramNameStr,String descriptionStr,boolean cmdLnEnbFlag,
                               boolean helpScreenFlag,boolean cmdLnOnlyFlag)
  {
    this.name = (name != null) ? name : ""; //load given parameter value
    setDefaultValue(defaultValue);          // (don't allow null handles)
    setValueObject(defaultValue);
    this.requiredFlag = requiredFlag;       //load required flag
    if(paramNameStr != null && paramNameStr.length() > 0 &&
                                isSwitchChar(paramNameStr.charAt(0)))
    {    //cmd-ln param name given and it starts with a switch character
        paramNameStr = paramNameStr.substring(1); //remove switch char
    }
    cmdLnParamName = paramNameStr;          //cmd-line parameter name
    this.descriptionStr = descriptionStr;   //description for item
    this.cmdLnEnbFlag = cmdLnEnbFlag;       //enable via command line flag
    this.helpScreenFlag = helpScreenFlag;   //show in help screen data flag
    this.cmdLnOnlyFlag = cmdLnOnlyFlag;     //via command line only flag
  }

    /**
     * Creates a configuration property item.
     * @param name property name.
     * @param defaultValue default value for the property.
     * @param requiredFlag the value for the item's required-flag (see
     * 'setRequiredFlag()').
     * @param cmdLnParamName command-line parameter name for item.
     * @param descriptionStr description text for item.
     */
  public CfgPropItem(String name,Object defaultValue,boolean requiredFlag,
                                String cmdLnParamName,String descriptionStr)
  {
    this(name,defaultValue,requiredFlag,cmdLnParamName,descriptionStr,
                                                           true,true,false);
  }

    /**
     * Creates a configuration property item.
     * @param name property name.
     * @param defaultValue default value for the property.
     * @param requiredFlag the value for the item's required-flag (see
     * 'setRequiredFlag()').
     */
  public CfgPropItem(String name,Object defaultValue,boolean requiredFlag)
  {
    this(name,defaultValue,requiredFlag,null,null,true,true,false);
  }

    /**
     * Creates a configuration property item.  Its required-flag is set
     * to false (see 'setRequiredFlag()').
     * @param name property name.
     * @param defaultValue default value for the property.
     * @param cmdLnParamName command-line parameter name for item.
     * @param descriptionStr description text for item.
     * @param cmdLnEnbFlag true to enable use of item as command-line
     * parameter.
     * @param helpScreenFlag true to include item in help screen data.
     */
  public CfgPropItem(String name,Object defaultValue,String cmdLnParamName,
          String descriptionStr,boolean cmdLnEnbFlag,boolean helpScreenFlag)
  {           //create item with required-flag==false
    this(name,defaultValue,false,cmdLnParamName,descriptionStr,
                                         cmdLnEnbFlag,helpScreenFlag,false);
  }

    /**
     * Creates a configuration property item.  Its required-flag is set
     * to false (see 'setRequiredFlag()').
     * @param name property name.
     * @param defaultValue default value for the property.
     * @param cmdLnParamName command-line parameter name for item.
     * @param descriptionStr description text for item.
     * @param cmdLnEnbFlag true to enable use of item as command-line
     * parameter.
     * @param helpScreenFlag true to include item in help screen data.
     * @param cmdLnOnlyFlag true for item via command line only.
     */
  public CfgPropItem(String name,Object defaultValue,String cmdLnParamName,
                                 String descriptionStr,boolean cmdLnEnbFlag,
                               boolean helpScreenFlag,boolean cmdLnOnlyFlag)
  {           //create item with required-flag==false
    this(name,defaultValue,false,cmdLnParamName,descriptionStr,
                                 cmdLnEnbFlag,helpScreenFlag,cmdLnOnlyFlag);
  }

    /**
     * Creates a configuration property item.  Its required-flag is set
     * to false (see 'setRequiredFlag()').
     * @param name property name.
     * @param defaultValue default value for the property.
     * @param cmdLnParamName command-line parameter name for item.
     * @param descriptionStr description text for item.
     */
  public CfgPropItem(String name,Object defaultValue,
                                String cmdLnParamName,String descriptionStr)
  {           //create item with required-flag==false
    this(name,defaultValue,false,cmdLnParamName,descriptionStr,true,true,
                                                                     false);
  }

    /**
     * Creates a configuration property item.  Its required-flag is set
     * to false (see 'setRequiredFlag()').
     * @param name property name.
     * @param defaultValue default value for the property.
     * @param cmdLnParamName command-line parameter name for item.
     */
  public CfgPropItem(String name,Object defaultValue,String cmdLnParamName)
  {           //create item with required-flag==false
    this(name,defaultValue,false,cmdLnParamName,null,true,true,false);
  }

    /**
     * Creates a configuration property item.  Its required-flag is set
     * to false (see 'setRequiredFlag()').
     * @param name property name.
     * @param defaultValue default value for the property.
     */
  public CfgPropItem(String name,Object defaultValue)
  {           //create item with required-flag==false and no cmd-ln name
    this(name,defaultValue,false,null,null,true,true,false);
  }

    /**
     * Returns the name of the property item.
     * @return String
     */
  public String getName()
  {
    return name;
  }

    /**
     * Returns the default value object for the property item.
     * @return Object
     */
  public synchronized Object getDefaultValue()
  {
    return defaultValue;
  }

    /**
     * Sets the default value object for the property item.
     * @param defaultValue the default value object.
     */
  public final synchronized void setDefaultValue(Object defaultValue)
  {
    //don't allow null handle
    if (defaultValue == null)
    {
      defaultValue = NO_VALUE;
    }
    this.defaultValue = defaultValue;
  }
  
  /**
   * Interprets the given string and enters it as the default value Object for
   * the property item.  The string is converted to an Object of the
   * same type as the default value object for the property item.
   * @param str the string value to interpret.
   * @return true if successful, false if the string could not be
   * converted to the proper type.
   */
  public synchronized boolean setDefaultValueString(String str)
  {
    final Object obj;   //call with usual 'defaultValue' object (not array):
    // (convert string to object of proper type)
    if((obj=setValueString(str,defaultValue)) == null)
      return false;          //if bad type then return error

    if(!obj.equals(defaultValue))  //if default value changed
    {
      //if validator present then validate new value:
      if(validatorObj != null && !validatorObj.validateValue(obj))
        return false;          //if validation failed then return error
    }
    setDefaultValue(obj);
    return true;             //return OK flag
  }

    /**
     * Sets the default value and the value object for the property item
     * to the given value.
     * @param valueObj the value object to use.
     */
  public synchronized void setDefaultAndValue(Object valueObj)
  {
    setDefaultValue(valueObj);
    setValueObject(valueObj);
  }

    /**
     * Returns the value object for the property item.
     * @return Object
     */
  public synchronized Object getValue()
  {
    return value;
  }
  
  /**
   * Reset to the default value.
   * @return true if the value was changed, false otherwise.
   */
  public synchronized boolean reset()
  {
    if (isDefaultValue())
    {
      return false;
    }
    setValueObject(defaultValue);
    fireDataChanged(); // indicate data changed
    return true;
  }

  /**
   * Sets the value object for the property item.
   * @param valueObj the value of the object.
   */
  protected final void setValueObject(Object valueObj)
  {
    value = valueObj;

    //get number value
    if (value instanceof Number)
      numberValue = (Number)value;
    else
      numberValue = Integer.valueOf(0);

    //get boolean value
    if (value instanceof Boolean)
      booleanValue = ((Boolean)value).booleanValue();
    else if (value instanceof BooleanCp)
      booleanValue = ((BooleanCp)value).booleanValue();
    else
      booleanValue = false;

    //get string value (only save Strings to conserve memory)
    if (value instanceof String)
      stringValue = (String)value;
    else
      stringValue = null;

    //get color value
    if (value instanceof Color)
      colorValue = (Color)value;
    else
      colorValue = null;
  }

    /**
     * Sets the value object for the property item.
     * @param valueObj the value of the object.
     * @return true if successful, false if given object's type does
     * not match the item's default-value object's type.
     */
  public synchronized boolean setValue(Object valueObj)
  {
    if (defaultValue instanceof Archivable && valueObj instanceof String)
    {
      valueObj = UtilFns.parseObject(defaultValue,(String)valueObj);
    }
    if(valueObj == null || valueObj.getClass() != defaultValue.getClass())
      return false;          //if new value not OK then return error
    if(valueObj.equals(value))
      return true;           //if value not changing then return OK
                        //if validator present then validate new value:
    if(validatorObj != null && !validatorObj.validateValue(valueObj))
      return false;          //if validation failed then return error
                        //new value is OK; enter it:
    setValueObject(valueObj);  //accept new object
    fireDataChanged();       //indicate data changed
    return true;             //return OK flag
  }

    /**
     * Sets the value object for the property item.
     * @param val the value of the object.
     * @return true if successful, false if given object's type does
     * not match the item's default-value object's type.
     */
  public boolean setValue(int val)
  { return setValue(Integer.valueOf(val)); }

    /**
     * Sets the value object for the property item.
     * @param val the value of the object.
     * @return true if successful, false if given object's type does
     * not match the item's default-value object's type.
     */
  public boolean setValue(long val)
  { return setValue(Long.valueOf(val)); }

    /**
     * Sets the value object for the property item.
     * @param val the value of the object.
     * @return true if successful, false if given object's type does
     * not match the item's default-value object's type.
     */
  public boolean setValue(float val)
  { return setValue(Float.valueOf(val)); }

    /**
     * Sets the value object for the property item.
     * @param val the value of the object.
     * @return true if successful, false if given object's type does
     * not match the item's default-value object's type.
     */
  public boolean setValue(double val)
  { return setValue(Double.valueOf(val)); }

    /**
     * Sets the value object for the property item.
     * @param val the value of the object.
     * @return true if successful, false if given object's type does
     * not match the item's default-value object's type.
     */
  public boolean setValue(byte val)
  { return setValue(Byte.valueOf(val)); }

    /**
     * Sets the value object for the property item.
     * @param val the value of the object.
     * @return true if successful, false if given object's type does
     * not match the item's default-value object's type.
     */
  public boolean setValue(short val)
  { return setValue(Short.valueOf(val)); }

    /**
     * Sets the value object for the property item.
     * @param flg the value of the object.
     * @return true if successful, false if given object's type does
     * not match the item's default-value object's type.
     */
  public boolean setValue(boolean flg)
  { return setValue(Boolean.valueOf(flg)); }

    /**
     * Interprets the given string and enters it as the value object for
     * the property item.  The string is converted to an object of the
     * same type as the given default value object for the property item.
     * @param str the string value to interpret.
     * @param defValObj the default value object to use for type checking
     * (this parameter allows for multiple values and types to be dealt
     * with).
     * @return an Object filled via the given string, or null if
     * the string could not be converted to the proper type.
     */
  public static Object setValueString(String str,Object defValObj)
  {
    return IstiNamedValue.setValueString(str,defValObj);
  }

  /**
   * Interprets the given string and enters it as the default value Object for
   * the property item.  The string is converted to an Object of the
   * same type as the default value object for the property item.
   * @param str the string value to interpret.
   * @param defaultFlag true to update the default value, false otherwise.
   * @return true if successful, false if the string could not be
   * converted to the proper type.
   */
  public synchronized boolean setValueString(String str, boolean defaultFlag)
  {
    final Object obj;   //call with usual 'defaultValue' object (not array):
                        // (convert string to object of proper type)
    if((obj=setValueString(str,defaultValue)) == null)
      return false;          //if bad type then return error

    if(!obj.equals(value))  //if value changed
    {
      //if validator present then validate new value:
      if(validatorObj != null && !validatorObj.validateValue(obj))
        return false;          //if validation failed then return error
      //new value is OK; enter it:
      setValueObject(obj);     //set value for parameter
      fireDataChanged();       //indicate data changed
    }
    if (defaultFlag)         //if updating the default value
      setDefaultValue(value);
    return true;             //return OK flag
  }

    /**
     * Interprets the given string and enters it as the value Object for
     * the property item.  The string is converted to an Object of the
     * same type as the default value object for the property item.
     * @param str the string value to interpret.
     * @return true if successful, false if the string could not be
     * converted to the proper type.
     */
  public synchronized boolean setValueString(String str)
  {
    return setValueString(str, false);
  }

    /**
     * Interprets the given string and compares it to the value Object for
     * this property item.  The string is converted to an Object of the
     * same type as the default value object for the property item.
     * @param str the string value to interpret.
     * @return true if the given string converts to a value equal to the
     * current value of this property item, false if not (or if the
     * given string could not be converted to the proper data type).
     */
  public synchronized boolean compareValueString(String str)
  {
    final Object obj;        //convert string to object:
    if((obj=setValueString(str,defaultValue)) == null)
      return false;          //return false if could not be converted
    try
    {         //compare to value, return result
      return value.equals(obj);
    }
    catch(Exception ex)
    {         //error comparing, return false
      return false;
    }
  }

    /**
     * Interprets the given string and returns it as a data value Object
     * that could be entered for this property item.  The string is
     * converted to an Object of the same type as the default value
     * object for the property item.  Note that this method does not
     * validate the given value.
     * @param str the string value to interpret.
     * @return the converted object, or null if the string could not
     * be converted to the proper type.
     */
  public synchronized Object convertValueString(String str)
  {
    return setValueString(str,defaultValue);
  }

    /**
     * Checks the class type of the given object against the class type of
     * default object for the property item.
     * @param obj the object to check.
     * @return true if the types match.
     */
  public synchronized boolean checkType(Object obj)
  {
    if(obj == null)
      return false;
    return (obj.getClass() == defaultValue.getClass());
  }

    /**
     * Returns a class object representing the data type used for the
     * property item's value and default-value.
     * @return the value-class object.
     */
  public synchronized Class getValueClass()
  {
    if(defaultValue == NO_VALUE)
      return null;
    return defaultValue.getClass();
  }

  /**
   * Determines if this value is a boolean value.
   * @return true if this value is a boolean value, false otherwise.
   */
  public boolean isBoolean()
  {
    final Class classObj = getValueClass();
    return classObj == Boolean.class || classObj == BooleanCp.class;
  }

    /**
     * Sets the 'loaded' flag for the property item.  A 'loaded' flag value
     * of 'true' indicates that the property item has been loaded from
     * a configuration file.
     * @param flg the 'loaded' flag value.
     * @return A handle to this object.
     */
  public synchronized CfgPropItem setLoadedFlag(boolean flg)
  {
    loadedFlag = flg;
    return this;
  }

    /**
     * Returns the 'loaded' flag for the property item.  A 'loaded' flag
     * value of 'true' indicates that the property item has been loaded
     * from a configuration file.
     * @return the 'loaded' flag value.
     */
  public synchronized boolean getLoadedFlag()
  {
    return loadedFlag;
  }

    /**
     * Sets the 'required' flag for the property item.  A 'required' flag
     * value of 'true' indicates that the property item must loaded when
     * the 'CfgProperties.load()' function is used.
     * @param flg the 'required' flag value.
     * @return A handle to this object.
     */
  public synchronized CfgPropItem setRequiredFlag(boolean flg)
  {
    requiredFlag = flg;
    return this;
  }

    /**
     * Returns the 'required' flag for the property item.  A 'required' flag
     * value of 'true' indicates that the property item must loaded when
     * the 'CfgProperties.load()' function is used.
     * @return the 'required' flag value.
     */
  public synchronized boolean getRequiredFlag()
  {
    return requiredFlag;
  }

    /**
     * Sets the loaded-from-command-line flag for the property item.  A
     * value of 'true' indicates that the property item has been loaded from
     * a the command line.
     * @param flg value for the loaded-from-command-line flag.
     * @return A handle to this object.
     */
  public synchronized CfgPropItem setCmdLnLoadedFlag(boolean flg)
  {
    cmdLnLoadedFlag = flg;
    return this;
  }

    /**
     * Returns the loaded-from-command-line flag for the property item.  A
     * value of 'true' indicates that the property item has been loaded from
     * a the command line.
     * @return value for the loaded-from-command-line flag.
     */
  public synchronized boolean getCmdLnLoadedFlag()
  {
    return cmdLnLoadedFlag;
  }

    /**
     * Sets the ignore flag for the property item.  A value of 'true'
     * will prevent the item from being included when storing or
     * displaying in the help screen.  The allows an obsolete item
     * to disappear while not generating an error if found in a
     * configuration file.  When this flag is set the help-screen
     * flag is cleared.
     * @param flg value for the ignore-item flag, true==ignore.
     * @return A handle to this object.
     */
  public synchronized CfgPropItem setIgnoreItemFlag(boolean flg)
  {
    if((ignoreItemFlag=flg))
      helpScreenFlag = false;
    return this;
  }

    /**
     * Returns the ignore flag for the property item.
     * @return value for the ignore-item flag, true==ignore.
     */
  public synchronized boolean getIgnoreItemFlag()
  {
    return ignoreItemFlag;
  }

  /**
   * Sets the no save flag for the property item.  A value of 'true'
   * will prevent the item from being included when storing.
   * @param flg value for the no save-item flag.
   * @return A handle to this object.
   */
  public synchronized CfgPropItem setNoSaveItemFlag(boolean flg)
  {
    noSaveItemFlag = flg;
    return this;
  }

  /**
   * Returns the no save flag for the property item.
   * @return value for the no save-item flag.
   */
  public synchronized boolean getNoSaveItemFlag()
  {
    return noSaveItemFlag;
  }

    /**
     * Sets the "show in help screen data" flag for the property item.  A
     * value of 'true' indicates that the property item will be shown.
     * @param flg value for the "show in help screen data" flag.
     * @return A handle to this object.
     */
  public synchronized CfgPropItem setHelpScreenFlag(boolean flg)
  {
    helpScreenFlag = flg;
    return this;
  }

    /**
     * Sets the empty-string-default flag for the property item.  A
     * value of 'true' will make an empty string read from the
     * configuration file be converted to the default value for
     * the item, and it will make an item value equal to the default
     * value be converted to the empty string when written to the
     * configuration file.
     * @param flg the flag value to use.
     * @return A handle to this object.
     */
  public synchronized CfgPropItem setEmptyStringDefaultFlag(boolean flg)
  {
    emptyStringDefaultFlag = flg;
    return this;
  }

    /**
     * Returns the empty-string-default flag for the property item.  A
     * value of 'true' will make an empty string read from the
     * configuration file be converted to the default value for
     * the item, and it will make an item value equal to the default
     * value be converted to the empty string when written to the
     * configuration file.
     * @return The current value of the empty-string-default flag for
     * this item.
     */
  public synchronized boolean getEmptyStringDefaultFlag()
  {
    return emptyStringDefaultFlag;
  }

  /**
   * Returns the archivable representation for this object.  When the
   * object is recreated from the string, it should be identical to the
   * original.
   * @return A String representing the archived form of the object.
   */
  public String toArchivedForm()
  {
    if (stringValue != null)  //use string value if known
      return stringValue;
    if (value instanceof Color)
      return UtilFns.colorToPropertyString((Color)value);
    if (value instanceof Archivable)
      return ((Archivable)value).toArchivedForm();
    return value.toString();
  }

    /**
     * Returns a String object representing the value of the property item.
     * @return the String object.
     */
  public synchronized String toString()
  {
    if(!(value instanceof Object[]))   //if not an array then
      return stringValue();            //return single value as string
    final Object [] valueArr = (Object [])value;
    final StringBuffer buff = new StringBuffer();
    if(valueArr.length > 0)
    {    //at least one element in array
      int i = 0;
      while(true)
      {  //for each element in array
        if(valueArr[i] != null)        //if element not null then
          buff.append(valueArr[i]);    //add to buffer
        if(++i >= valueArr.length)
          break;             //if no more then exit loop
        buff.append(" ");              //add separator
      }
    }
    return buff.toString();          //return string version of buffer
  }

    /**
     * Returns a hash code for the value object of the property item.
     * @return an integer hash code.
     */
  public synchronized int hashCode()
  {
    if(!(value instanceof Object[]))   //if not an array then
      return value.hashCode();         //return single value's hashcode
    final Object [] valueArr = (Object [])value;
    int hashVal = 0;
    for(int i=0; i<valueArr.length; ++i)
    {    //for each element in array
      if(valueArr[i] != null)               //if element not null then
        hashVal += valueArr[i].hashCode();  //add to hash-code
    }
    return hashVal;
  }

    /**
     * Returns true if the given object is a 'CfgPropItem' whose value is
     * equal to the value object of the property item or if the given
     * object itself is equal to the value object of the property item.
     * @param obj the object to compare.
     * @return true if the given object is a 'CfgPropItem' whose value is
     * equal to the value object of the property item or if the given
     * object itself is equal to the value object of the property item.
     */
  public synchronized boolean equals(Object obj)
  {
    if(obj instanceof CfgPropItem)          //if 'CfgPropItem' given then
      obj = ((CfgPropItem)obj).getValue();  //compare its value object
    if(!(value instanceof Object[]))   //if value not an array then
      return value.equals(obj);        //use value object's method
    if(!(obj instanceof Object[]))     //if given object not an array then
      return false;                    //return not equal
    final Object [] valueArr = (Object[])value;
    final int valueArrLen = valueArr.length;
    final Object [] objArr = (Object [])obj;
    if(objArr.length != valueArrLen)
      return false;     //if array lengths unequal then return not equal
    Object valObj;
    for(int i=0; i<valueArrLen; ++i)
    {    //for each element in array of values for property item
      if((valObj=valueArr[i]) != null)
      {  //element is not null
        if(!valObj.equals(objArr[i]))  //if different from item in given
          return false;                // array then return not equal
      }
      else
      {  //element is null
        if(objArr[i] != null)          //if given item is not null then
          return false;                //return not equal
      }
    }
    return true;
  }

    /**
     * Returns the value of the property object as an int, or zero if
     * the object cannot be converted.
     * @return the integer value.
     */
  public synchronized int intValue()
  {
    return numberValue.intValue();
  }

    /**
     * Returns the value of the property object as a long, or zero if
     * the object cannot be converted.
     * @return the long value.
     */
  public synchronized long longValue()
  {
    return numberValue.longValue();
  }

    /**
     * Returns the value of the property object as a float, or zero if
     * the object cannot be converted.
     * @return the float value.
     */
  public synchronized float floatValue()
  {
    return numberValue.floatValue();
  }

    /**
     * Returns the value of the property object as a double, or zero if
     * the object cannot be converted.
     * @return the double value.
     */
  public synchronized double doubleValue()
  {
    return numberValue.doubleValue();
  }

    /**
     * Returns the value of the property object as a byte, or zero if
     * the object cannot be converted.
     * @return the byte value.
     */
  public synchronized byte byteValue()
  {
    return numberValue.byteValue();
  }

    /**
     * Returns the value of the property object as a short, or zero if
     *  the object cannot be converted.
     * @return the short value.
     */
  public synchronized short shortValue()
  {
    return numberValue.shortValue();
  }

    /**
     * Returns the value of the property object as a boolean, or false if
     * the object cannot be converted.
     * @return the boolean value.
     */
  public synchronized boolean booleanValue()
  {
    return booleanValue;
  }

    /**
     * Returns a String object representing the value of the property item.
     * @return the String value.
     */
  public synchronized String stringValue()
  {
    if (stringValue != null)  //use string value if known
      return stringValue;
    if (value instanceof Color)
      return UtilFns.colorToPropertyString((Color)value);
    if (value instanceof Float || value instanceof Double)
      return UtilFns.floatNumberToPropertyString(value);
    return value.toString();
  }

  /**
   * Returns a String array object representing the value of the property item.
   * @return the String array value.
   */
  public synchronized String[] stringArrayValue()
  {
    return UtilFns.parseSeparatedSubstrings(stringValue(), ",", '\'', true);
  }

  /**
   * Creates and returns a clone of this object.
   * @return a clone of the this object.
   */
  public Object clone()
  {
    return duplicate();
  }

  /**
   * Returns the value of the property object as a Color, or null if
   * the object cannot be converted.
   * @return the Color value.
   */
  public synchronized Color colorValue()
  {
    return colorValue;
  }

    /**
     * Compares the value object of this property item with the given
     * object.
     * @param obj the value of the object.
     * @return A negative integer, zero, or a positive integer as this
     * object is less than, equal to, or greater than the specified object.
     * @return the integer comparison value.
     * @exception ClassCastException if the specified object's type
     * prevents it from being compared to this Object.
     */
  public synchronized int compareTo(Object obj) throws ClassCastException
  {
    return ((Comparable)value).compareTo(obj);
  }

    /**
     * Returns the command-line parameter name for item.
     * @return the name string.
     */
  public String getCmdLnParamName()
  {
    return cmdLnParamName;
  }

    /**
     * Returns the property item description string.
     * @return the description string.
     */
  public String getDescriptionStr()
  {
    return descriptionStr;
  }

    /**
     * Returns the "enable via command line" flag.
     * @return true if this item may be entered via a command-line
     * parameter.
     */
  public boolean getCmdLnEnbFlag()
  {
    return cmdLnEnbFlag;
  }

    /**
     * Returns the "show in help screen data" flag.
     * @return true if this item will be shown in the help screen.
     */
  public boolean getHelpScreenFlag()
  {
    return helpScreenFlag;
  }

    /**
     * Returns the "via command line only" flag.
     * @return true if this item may only be entered via a command-line
     * parameter.
     */
  public boolean getCmdLnOnlyFlag()
  {
    return cmdLnOnlyFlag;
  }

    /**
     * Returns a duplicate of this object.
     * @return handle to this object.
     */
  public synchronized CfgPropItem duplicate()
  {
    final CfgPropItem itemObj = new CfgPropItem(name,defaultValue,
                    requiredFlag,cmdLnParamName,descriptionStr,cmdLnEnbFlag,
                                              helpScreenFlag,cmdLnOnlyFlag);
    itemObj.setValueObject(value);
    itemObj.validatorObj = validatorObj;
    return itemObj;
  }

    /**
     * Sets the validator for this item.
     * @param validatorObj a 'CfgPropValidator' object.
     * @return A handle to this object.
     */
  public synchronized CfgPropItem setValidator(CfgPropValidator validatorObj)
  {
    //if current value is valid
    if (validateValue(validatorObj, value))
      this.validatorObj = validatorObj;
    return this;
  }

    /**
     * Sets the validator for this item.
     * @param minValueCompObj a "minimum" value object that implements
     * the 'Comparable' interface (or null for no minimum).
     * @param maxValueCompObj a "maximum" value object that implements
     * the 'Comparable' interface (or null for no maximum).
     * @return A handle to this object.
     */
  public CfgPropItem setValidator(Comparable minValueCompObj,
                                                 Comparable maxValueCompObj)
  {
    setValidator(new CfgPropValidator(minValueCompObj,maxValueCompObj));
    return this;
  }

    /**
     * Sets the validator for this item.
     * @param objArr an array of acceptable-value 'Object's.
     * @return A handle to this object.
     */
  public synchronized CfgPropItem setValidator(Object [] objArr)
  {
    setValidator(new CfgPropValidator(objArr));
    return this;
  }

    /**
     * Sets the validator for this item.
     * @param minValue minimum valid value.
     * @param maxValue maximum valid value.
     * @return A handle to this object.
     */
  public synchronized CfgPropItem setValidator(int minValue,int maxValue)
  {
    setValidator(new CfgPropValidator(Integer.valueOf(minValue),
                                                     Integer.valueOf(maxValue)));
    return this;
  }

    /**
     * Sets the validator for this item.
     * @param minValue minimum valid value.
     * @param maxValue maximum valid value.
     * @return A handle to this object.
     */
  public synchronized CfgPropItem setValidator(long minValue,long maxValue)
  {
    setValidator(new CfgPropValidator(Long.valueOf(minValue),
                                                        Long.valueOf(maxValue)));
    return this;
  }

    /**
     * Sets the validator for this item.
     * @param minValue minimum valid value.
     * @param maxValue maximum valid value.
     * @return A handle to this object.
     */
  public synchronized CfgPropItem setValidator(short minValue,short maxValue)
  {
    setValidator(new CfgPropValidator(Short.valueOf(minValue),
                                                       Short.valueOf(maxValue)));
    return this;
  }

    /**
     * Sets the validator for this item.
     * @param minValue minimum valid value.
     * @param maxValue maximum valid value.
     * @return A handle to this object.
     */
  public synchronized CfgPropItem setValidator(byte minValue,byte maxValue)
  {
    setValidator(new CfgPropValidator(Byte.valueOf(minValue),
                                                        Byte.valueOf(maxValue)));
    return this;
  }

    /**
     * Sets the validator for this item.
     * @param minValue minimum valid value.
     * @param maxValue maximum valid value.
     * @return A handle to this object.
     */
  public synchronized CfgPropItem setValidator(float minValue,float maxValue)
  {
    setValidator(new CfgPropValidator(Float.valueOf(minValue),
                                                       Float.valueOf(maxValue)));
    return this;
  }

    /**
     * Sets the validator for this item.
     * @param minValue minimum valid value.
     * @param maxValue maximum valid value.
     * @return A handle to this object.
     */
  public synchronized CfgPropItem setValidator(double minValue,double maxValue)
  {
    setValidator(new CfgPropValidator(Double.valueOf(minValue),
                                                      Double.valueOf(maxValue)));
    return this;
  }

    /**
     * Gets the validator object for this item.
     * @return handle to the validator object.
     */
  public synchronized CfgPropValidator getValidator()
  {
    return validatorObj;
  }

  /**
   * Checks that the given object is a valid data value using the specified
   * validator.
   * @param validatorObj a 'CfgPropValidator' object.
   * @param valueObj the value of the object.
   * @return true if the given object is valid (or if no validator has
   * been setup), false if not.
   */
  public static boolean validateValue(CfgPropValidator validatorObj,
                                      Object valueObj)
  {
    return PropItem.validateValue(validatorObj,valueObj);
  }

    /**
     * Checks that the given object is a valid data value using this item's
     * validator.
     * @param valueObj the value of the object.
     * @return true if the given object is valid (or if no validator has
     * been setup), false if not.
     */
  public synchronized boolean validateValue(Object valueObj)
  {
    return validateValue(validatorObj, valueObj);
  }

    /**
     * Registers the given 'DataChangedListener' object to be notified
     * when this object is changed.
     * @param listenerObj the 'DataChangedListener' object.
     * @return A handle to this object.
     */
  public synchronized CfgPropItem addDataChangedListener(
                                            DataChangedListener listenerObj)
  {
    if(listenerObj != null && listenersVec.indexOf(listenerObj) < 0)
      listenersVec.add(listenerObj);   //if OK and not in list then add
    return this;
  }

    /**
     * Unregisters the given 'DataChangedListener' object from the list of
     * listeners for this object.
     * @param listenerObj the 'DataChangedListener' object.
     */
  public synchronized void removeDataChangedListener(
                                            DataChangedListener listenerObj)
  {
    if(listenerObj != null)                 //if handle not null then
      listenersVec.remove(listenerObj);     //remove listener from list
  }

    /**
     * Called to indicate that this object has changed.  Each registered
     * listener's 'dataChanged()' method is called.
     */
  public void fireDataChanged()
  {
    lastChangeTime = System.currentTimeMillis(); //save time of last change
    Object obj;
    final Enumeration e = listenersVec.elements();
    while(e.hasMoreElements())
    {    //for each listener object; if OK then call data-changed method
      if((obj=e.nextElement()) instanceof DataChangedListener)
        ((DataChangedListener)obj).dataChanged(this);
    }
  }

    /**
     * Returns the time of last change to this item's value, in milliseconds
     * since 1/1/1970.  If the value has not been changed since the item was
     * constructed then zero will be returned.
     * @return the time value.
     */
  public long getLastChangeTime()
  {
    return lastChangeTime;
  }

    /**
     * Sets the group-select object for this item.  If this object
     * is of type 'String' or 'ItemGroupSelector' then it can
     * be used by the 'CfgPropertyInspector' class to place this
     * item onto a "settings" tab.
     * @param obj the group-select object to use.
     * @return A handle to this object.
     */
  public synchronized CfgPropItem setGroupSelObj(Object obj)
  {
    groupSelObj = obj;
    return this;
  }

    /**
     * Returns the group-select object for this item.  If this object
     * is of type 'String' or 'ItemGroupSelector' then it can
     * be used by the 'CfgPropertyInspector' class to place this
     * item onto a "settings" tab.
     * @return handle to the group-select object.
     */
  public synchronized Object getGroupSelObj()
  {
    return groupSelObj;
  }

    /**
     * Sets the "auxiliary" object for this item (usually a
     * 'CfgPropComponent' object).
     * @param obj the value of the object.
     * @return A handle to this object.
     */
  public synchronized CfgPropItem setAuxiliaryObj(Object obj)
  {
    auxiliaryObj = obj;
    return this;
  }

    /**
     * Returns the "auxiliary" object for this item (usually a
     * 'CfgPropComponent' object).
     * @return A handle to the "auxiliary" object.
     */
  public synchronized Object getAuxiliaryObj()
  {
    return auxiliaryObj;
  }

  /**
   * Sets the startup config flag.
   * @param b true if the item should be selected at startup.
   * @return A handle to this object.
   */
  public synchronized CfgPropItem setStartupConfigFlag(boolean b)
  {
    startupConfigFlag = b;
    return this;
  }

  /**
   * Determines if the current value is the same as the default value.
   * @return true if the current value is the same as the default value.
   */
  public boolean isDefaultValue()
  {
    if (value == defaultValue)
      return true;
    if (value == null)
      return defaultValue == NO_VALUE;
    return value.equals(defaultValue);
  }

  /**
   * Determines if the item should be selected at startup.
   * @return true if the item should be selected at startup.
   */
  public synchronized boolean isStartupConfigFlag()
  {
    return startupConfigFlag;
  }

  /**
   * Determines if the specified character is a switch character ('-' or '/').
   * @param str string
   * @return true if the specified character is a switch character,
   * false otherwise.
   */
  public static boolean isSwitchChar(char c)
  {
    return c == SWITCH1_CHAR || c == SWITCH2_CHAR;
  }

  // Map.Entry interface

  private transient Map.Entry mapEntryObj = null;

  /**
   * Map Entry interface
   * @return Map.Entry
   */
  public synchronized Map.Entry getMapEntry()
  {
    if (mapEntryObj == null)
      mapEntryObj = new MapEntry();
    return mapEntryObj;
  }

  class MapEntry implements Map.Entry
  {
    /**
     * Returns the key corresponding to this entry.
     *
     * @return the key corresponding to this entry.
     */
    public synchronized Object getKey()
    {
      return CfgPropItem.this.getName();
    }

    /**
     * Returns the value corresponding to this entry.  If the mapping
     * has been removed from the backing map (by the iterator's
     * <tt>remove</tt> operation), the results of this call are undefined.
     *
     * @return the value corresponding to this entry.
     */
    public synchronized Object getValue()
    {
      return CfgPropItem.this.getValue();
    }

    /**
     * Replaces the value corresponding to this entry with the specified
     * value (optional operation).  (Writes through to the map.)  The
     * behavior of this call is undefined if the mapping has already been
     * removed from the map (by the iterator's <tt>remove</tt> operation).
     *
     * @param value new value to be stored in this entry.
     * @return old value corresponding to the entry.
     *
     * @throws ClassCastException if the class of the specified value
     * 	      prevents it from being stored in the backing map.
     * @throws    IllegalArgumentException if some aspect of this value
     *	      prevents it from being stored in the backing map.
     * @throws NullPointerException the backing map does not permit
     *	      <tt>null</tt> values, and the specified value is
     *	      <tt>null</tt>.
     */
    public synchronized Object setValue(Object value)
    {
      final Object oldValue = CfgPropItem.this.getValue();
      if (value == null)
        throw new NullPointerException();
      else if(value.getClass() != defaultValue.getClass())
        throw new ClassCastException();
      else if (!CfgPropItem.this.setValue(value))
        throw new IllegalArgumentException();
      return oldValue;
    }

    /**
     * Compares the specified object with this entry for equality.
     * Returns <tt>true</tt> if the given object is also a map entry and
     * the two entries represent the same mapping.  More formally, two
     * entries <tt>e1</tt> and <tt>e2</tt> represent the same mapping
     * if<pre>
     *     (e1.getKey()==null ?
     *      e2.getKey()==null : e1.getKey().equals(e2.getKey()))  &&
     *     (e1.getValue()==null ?
     *      e2.getValue()==null : e1.getValue().equals(e2.getValue()))
     * </pre>
     * This ensures that the <tt>equals</tt> method works properly across
     * different implementations of the <tt>Map.Entry</tt> interface.
     *
     * @param o object to be compared for equality with this map entry.
     * @return <tt>true</tt> if the specified object is equal to this map
     *         entry.
     */
    public synchronized boolean equals(Object o)
    {
      if (!(o instanceof Map.Entry))
        return false;
      Map.Entry e = (Map.Entry)o;
      return ((getKey() == null) ? (e.getKey() == null) :
                                             getKey().equals(e.getKey())) &&
             ((getValue() == null) ? (e.getValue() == null) :
                                           getValue().equals(e.getValue()));
    }

    /**
     * Returns the hash code value for this map entry.  The hash code
     * of a map entry <tt>e</tt> is defined to be: <pre>
     *     (e.getKey()==null   ? 0 : e.getKey().hashCode()) ^
     *     (e.getValue()==null ? 0 : e.getValue().hashCode())
     * </pre>
     * This ensures that <tt>e1.equals(e2)</tt> implies that
     * <tt>e1.hashCode()==e2.hashCode()</tt> for any two Entries
     * <tt>e1</tt> and <tt>e2</tt>, as required by the general
     * contract of <tt>Object.hashCode</tt>.
     *
     * @return the hash code value for this map entry.
     * @see Object#hashCode()
     * @see Object#equals(Object)
     * @see #equals(Object)
     */
    public synchronized int hashCode()
    {
      return (getKey() == null) ? 0 : (getKey().hashCode() ^
                        ((getValue() == null) ? 0 : getValue().hashCode()));
    }
  }


  /**
   * Class ItemGroupSelector defines a run-time-configurable group
   * selector for 'CfgPropItem' objects.  The intended use is to
   * configure whether or not a group a items is displayed via the
   * 'CfgPropertyInspector' class.
   */
  public static class ItemGroupSelector
  {
    private Object selectorObj;
    private boolean enableFlag;

    /**
     * Creates a run-time-configurable group selector for 'CfgPropItem'
     * objects.
     * @param selectorObj the group-select object to use.
     * @param enableFlag true to enable this selector; false to disable
     * this selector.
     */
    public ItemGroupSelector(Object selectorObj, boolean enableFlag)
    {
      this.selectorObj = selectorObj;
      this.enableFlag = enableFlag;
    }

    /**
     * Creates a run-time-configurable group selector for 'CfgPropItem'
     * objects.  The selector is initialized as enabled.
     * @param selectorObj the group-select object to use.
     */
    public ItemGroupSelector(Object selectorObj)
    {
      this(selectorObj,true);
    }

    /**
     * Sets the group-select object used by this selector.
     * @param selObj the group-select object to use.
     */
    public void setSelectObj(Object selObj)
    {
      selectorObj = selObj;
    }

    /**
     * Returns the group-select object used by this selector.
     * @return The group-select object, or null if this selector has
     * been disabled.
     */
    public Object getSelectObj()
    {
      return enableFlag ? selectorObj : null;
    }

    /**
     * Returns the group-select string used by this selector.
     * @return The group-select string, or null if a group-select
     * string has not been assigned or if this selector has been
     * disabled.
     */
    public String getSelectStr()
    {
      return (enableFlag && selectorObj instanceof String) ?
                                                 (String)selectorObj : null;
    }

    /**
     * Sets this selector as enabled or disabled.
     * @param flgVal true to enable this selector; false to disable
     * this selector.
     */
    public void setEnableFlag(boolean flgVal)
    {
      enableFlag = flgVal;
    }

    /**
     * Returns the enabled/disabled status of this selector.
     * @return true if this selector is enabled; false if not.
     */
    public boolean getEnableFlag()
    {
      return enableFlag;
    }
  }
}
