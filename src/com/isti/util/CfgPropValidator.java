//CfgPropValidator.java:  Defines a configuration property validator
//                        object used with the 'CfgPropItem' class.
//
//    7/2/2002 -- [ET]
//    3/2/2006 -- [KF]  Added support for allowed characters.
//   5/11/2012 -- [KF]  Added support to use integer min/max values to specify
//                      the number of characters for string values.
//   1/19/2022 -- [KF]  Added 'isAllowAll' and 'setAllowAll' methods.
//
//
//=====================================================================
// Copyright (C) 2022 Instrumental Software Technologies, Inc.
//
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions
// are met:
// 1. Redistributions of source code, or portions of this source code,
//    must retain the above copyright notice, this list of conditions
//    and the following disclaimer.
// 2. Redistributions in binary form must reproduce the above copyright
//    notice, this list of conditions and the following disclaimer in
//    the documentation and/or other materials provided with the
//    distribution.
// 3. All advertising materials mentioning features or use of this
//    software must display the following acknowledgment:
//    "This product includes software developed by Instrumental
//    Software Technologies, Inc. (http://www.isti.com)"
// 4. If the software is provided with, or as part of a commercial
//    product, or is used in other commercial software products the
//    customer must be informed that "This product includes software
//    developed by Instrumental Software Technologies, Inc.
//    (http://www.isti.com)"
// 5. The names "Instrumental Software Technologies, Inc." and "ISTI"
//    must not be used to endorse or promote products derived from
//    this software without prior written permission. For written
//    permission, please contact "info@isti.com".
// 6. Products derived from this software may not be called "ISTI"
//    nor may "ISTI" appear in their names without prior written
//    permission of Instrumental Software Technologies, Inc.
// 7. Redistributions of any form whatsoever must retain the following
//    acknowledgment:
//    "This product includes software developed by Instrumental
//    Software Technologies, Inc. (http://www.isti.com/)."
// THIS SOFTWARE IS PROVIDED BY INSTRUMENTAL SOFTWARE
// TECHNOLOGIES, INC. "AS IS" AND ANY EXPRESSED OR IMPLIED
// WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES
// OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
// DISCLAIMED.  IN NO EVENT SHALL INSTRUMENTAL SOFTWARE TECHNOLOGIES,
// INC. OR ITS CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
// INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
// (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
// SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
// HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
// STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
// ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED
// OF THE POSSIBILITY OF SUCH DAMAGE.
//=====================================================================
//  A current version of the software can be found at
//                http://www.isti.com
//  Bug reports and comments should be directed to
//  Instrumental Software Technologies, Inc. at info@isti.com
//=====================================================================
//

package com.isti.util;

import java.util.Arrays;
import java.util.Vector;

/**
 * Class CfgPropValidator defines a configuration property validator
 * object used with the 'CfgPropItem' class.
 */
public class CfgPropValidator
{
  public final Comparable minValueCompObj;       //minimum value
  public final Comparable maxValueCompObj;       //maximum value
  public final Object [] validValuesArr;         //array of valid values
  public final Vector validValuesVec;            //Vector of valid values
  public final String allowedChars;              //allowed characters
  private volatile boolean allowAll = false;

    /**
     * Creates a validator object with minimum and maximum values.
     * @param minValueCompObj a "minimum" value object that implements
     * the 'Comparable' interface (or null for no minimum).
     * @param maxValueCompObj a "maximum" value object that implements
     * the 'Comparable' interface (or null for no maximum).
     */
  public CfgPropValidator(Comparable minValueCompObj,
                                                 Comparable maxValueCompObj)
  {
    this.minValueCompObj = minValueCompObj;
    this.maxValueCompObj = maxValueCompObj;
    validValuesArr = null;        //values array not used
    validValuesVec = null;        //values Vector not used
    allowedChars = null;          //allowed characters not used
  }

  
    /**
     * Creates a validator object with the given set of valid values.
     * @param objArr an array of acceptable-value 'Object's.
     */
  public CfgPropValidator(Object [] objArr)
  {
    validValuesArr = objArr;
    Vector vec;
    if(objArr != null)
    {    //given object array not null
      try
      {       //convert array to Vector of valid values:
        vec = new Vector(Arrays.asList(objArr));
      }
      catch(Exception ex)
      {       //if error then setup empty Vector
        vec = new Vector();
      }
    }
    else      //given object array is null
      vec = new Vector();    //setup empty Vector
    validValuesVec = vec;
    minValueCompObj = maxValueCompObj = null;
    allowedChars = null;          //allowed characters not used
  }

    /**
     * Creates a validator object with the given set of valid values.
     * @param allowedChars a String of characters allowed.
     */
  public CfgPropValidator(String allowedChars)
  {
    minValueCompObj = maxValueCompObj = null;
    validValuesArr = null;        //values array not used
    validValuesVec = null;        //values Vector not used
    this.allowedChars = allowedChars;
  }

    /**
     * Validates the given value object against this object.
     * @param valueObj the value object to be validated.
     * @return true if the object is considered valid, false if not.
     */
  public boolean validateValue(Object valueObj)
  {
    if (isAllowAll())
    {
      return true;
    }
    if(validValuesVec != null)
    {    //compare value object against Vector of valid values:
      return (validValuesVec.indexOf(valueObj) >= 0);
    }
    if(valueObj == null)          //if given value object is null then
      return false;               //return not-valid flag
    final String str = valueObj.toString();
    if (allowedChars != null)
    {
      char ch;
      for (int i = 0; i < str.length(); i++)
      {
        ch = str.charAt(i);
        if (allowedChars.indexOf(ch) < 0)
          return false;     //return not-valid flag
      }
    }
    try
    {
      if(minValueCompObj != null)
      {
        if(valueObj instanceof String && minValueCompObj instanceof Integer)
        {
          final int value = ((Integer)minValueCompObj).intValue();
          if(value != 0 && str.length() < value)
            return false;     //return not-valid flag
        }
        else if(minValueCompObj.compareTo(valueObj) > 0)
          return false;   //if min exists and > value then return not valid
      }
      if(maxValueCompObj != null)
      {
        if(valueObj instanceof String && maxValueCompObj instanceof Integer)
        {
          final int value = ((Integer)maxValueCompObj).intValue();
          if(value != 0 && str.length() > value)
            return false;     //return not-valid flag
        }
        else if(maxValueCompObj.compareTo(valueObj) < 0)
          return false;   //if max exists and < value then return not valid
      }
    }
    catch(ClassCastException ex)
    {         //exception from wrong value-object type
      return false;     //return not-valid flag
    }
    return true;                  //return true to indicate value is valid
  }

  /**
   * Returns true if this validator allows all values.
   * @return true if this validator allows all values.
   */
  public boolean isAllowAll()
  {
    return allowAll;
  }

    /**
     * Returns true if this validator contains a minimum value that forces
     * values to be greater than or equal to zero.
     * @return true if this validator contains a minimum value that forces
     * values to be greater than or equal to zero.
     */
  public boolean isMinimumNonNegative()
  {
    return (minValueCompObj instanceof Number &&
                            ((Number)minValueCompObj).doubleValue() >= 0.0);
  }

  /**
   * Set the allow all values flag.
   * @param b true if this validator allows all values.
   */
  public void setAllowAll(boolean b)
  {
    allowAll = b;
  }
}
