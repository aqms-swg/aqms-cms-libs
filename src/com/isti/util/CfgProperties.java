//CfgProperties.java:  Manages a table of configuration property items.
//
//   7/12/2000 -- [ET]  Initial release version.
//   12/7/2000 -- [ET]  Added 'getDisplayString()' method and optional
//                      'showHeadersFlag' parameter to 'store()';
//                      implemented command-line parameter support.
//   7/12/2001 -- [ET]  Fixed help screen so that the configuration file
//                      parameters names are included when setting up
//                      horizontal spacing; added support for showing
//                      version information parameters on help screen.
//   8/13/2001 -- [ET]  Moved 'load()' and associated methods to the
//                      'BaseProperties' class to support using URL paths
//                      to configuration files by applets; added ability
//                      to have configuration filenames be URL addresses.
//  11/27/2001 -- [ET]  Added support for multiple values in a property
//                      in 'processCmdLnParams()' and 'store()' methods.
//    4/3/2001 -- [ET]  Added support for the 'set/getCmdLnLoadedFlag()'
//                      methods in 'CfgPropItem'.
//   4/16/2001 -- [ET]  Moved 'store()' and 'getDisplayString()' methods
//                      to the 'BaseProperties' class.
//   7/15/2001 -- [ET]  Fixed so that items not shown on the help screen
//                      will not generate a newline.
//  10/29/2002 -- [KF]  Now uses Map interface to access 'itemsTable'.
//  11/04/2002 -- [KF]  Added 'addGroupDataChangedListener' and
//                     'removeGroupDataChangedListener' methods for groups.
//   5/16/2006 -- [KF]  Added 'getHelpScreenData()' methods for optionally
//                      showing the property name, pre-text, post-text, and
//                      the text to be displayed before the description.
//    6/4/2008 -- [KF]  Added 'isSwitchChar()', 'areCompressedFlagsEnabled()'
//                      and 'setCompressedFlagsEnabled()' methods.
//   2/14/2012 -- [KF]  Added 'setDescriptionSeparatorString()' method.
//   3/16/2021 -- [KF]  Added 'setEmptyStringDefaultFlag' method.
//   3/17/2021 -- [KF]  Added 'isBoolean' method.
//
//
//=====================================================================
// Copyright (C) 2021 Instrumental Software Technologies, Inc.
//
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions
// are met:
// 1. Redistributions of source code, or portions of this source code,
//    must retain the above copyright notice, this list of conditions
//    and the following disclaimer.
// 2. Redistributions in binary form must reproduce the above copyright
//    notice, this list of conditions and the following disclaimer in
//    the documentation and/or other materials provided with the
//    distribution.
// 3. All advertising materials mentioning features or use of this
//    software must display the following acknowledgment:
//    "This product includes software developed by Instrumental
//    Software Technologies, Inc. (http://www.isti.com)"
// 4. If the software is provided with, or as part of a commercial
//    product, or is used in other commercial software products the
//    customer must be informed that "This product includes software
//    developed by Instrumental Software Technologies, Inc.
//    (http://www.isti.com)"
// 5. The names "Instrumental Software Technologies, Inc." and "ISTI"
//    must not be used to endorse or promote products derived from
//    this software without prior written permission. For written
//    permission, please contact "info@isti.com".
// 6. Products derived from this software may not be called "ISTI"
//    nor may "ISTI" appear in their names without prior written
//    permission of Instrumental Software Technologies, Inc.
// 7. Redistributions of any form whatsoever must retain the following
//    acknowledgment:
//    "This product includes software developed by Instrumental
//    Software Technologies, Inc. (http://www.isti.com/)."
// THIS SOFTWARE IS PROVIDED BY INSTRUMENTAL SOFTWARE
// TECHNOLOGIES, INC. "AS IS" AND ANY EXPRESSED OR IMPLIED
// WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES
// OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
// DISCLAIMED.  IN NO EVENT SHALL INSTRUMENTAL SOFTWARE TECHNOLOGIES,
// INC. OR ITS CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
// INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
// (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
// SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
// HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
// STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
// ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED
// OF THE POSSIBILITY OF SUCH DAMAGE.
//=====================================================================
//  A current version of the software can be found at
//                http://www.isti.com
//  Bug reports and comments should be directed to
//  Instrumental Software Technologies, Inc. at info@isti.com
//=====================================================================
//

package com.isti.util;

import java.util.*;
import java.io.*;
import java.net.*;

/**
 * Class CfgProperties manages a table of configuration property items.
 * Each item has a name, a value, a default value, a loaded-flag (set true
 * if the item has been "loaded" from an input stream), a required-flag
 * (set true if the item must be loaded from an input stream), and
 * additional fields to support the item's use as a command-line parameter
 * specification.  A vector or array of 'CfgPropItem' objects may be used
 * to define a set of property names and default-value objects; or they
 * can be added individually.  The default-value objects determine the
 * data types for the items.  The table of items may be loaded from and
 * saved to an I/O stream, as a configuration file in which:
 *   Each item has the format name = value.
 *   String values should be enclosed in quotes ("").
 *   "Special" characters are "quoted" using a backslash (\), including
 *   \n \r \t \" \#.
 *   Comments may begin with # or //, or 'C'-style comments may be used.
 */
public class CfgProperties extends BaseProperties
{
    /** Dash text to be displayed before the command-line parameter name. */
  private static final String DASH_STRING = " -";
    /** Dashes text to be displayed before the parameter name. */
  private static final String DASHES_STRING = " --";
    /** Empty string. */
  private static final String EMPTY_TEXT = UtilFns.EMPTY_STRING;
                        //description used by 'getHelpScreenData()':
  private static final String CFG_PARAM_DESC_STR =
                                        "Load specified configuration file";
                        //description used by 'getHelpScreenData()':
  private static final String VINFO_PARAM_DESC_STR =
                                         "Show program version information";
         //config file name param strings filled by 'processCmdLnParams()':
  private String cfgFnParam1String = null;
  private String cfgFnParam2String = null;
         //version infoparam strings filled by 'processCmdLnParams()':
  private String vInfoParam1String = null;
  private String vInfoParam2String = null;
         //Vector of "extra" cmd-ln params filled by 'processCmdLnParams()':
  private Vector extraCmdLnParamsVec = null;     // (Vector of Strings)
  //compressed flags enabled
  private boolean compressedFlagsEnabled = false;
  // description separator text or null if none:
  private String descriptionSeparatorString = null;

    /** Creates an empty table of items. */
  public CfgProperties()
  {
    super();                 //call base class constructor
  }

    /**
     * Creates a table of items using the given Vector of 'CfgPropItem'
     * objects.  Any object in the vector that is not a 'CfgPropItem'
     * object is ignored.
     * @param vec vector
     */
  public CfgProperties(Vector vec)
  {
    super(vec);              //call base class constructor
  }

    /**
     * Creates a table of items using the given array of 'CfgPropItem'
     * objects.
     * @param itemsArr items array
     */
  public CfgProperties(CfgPropItem [] itemsArr)
  {
    super(itemsArr);         //call base class constructor
  }

    /**
     * Creates a properties object that uses the same table of items
     * as the given properties object.
     * @param basePropObj base property object
     */
  public CfgProperties(BaseProperties basePropObj)
  {
    super(basePropObj);      //call base class constructor
  }

    /**
     * Creates a new property item and adds it to the table.  If an item
     * with the same name already exists in the table, it is replaced.
     * @param nameStr the name for the property item.
     * @param valueObj the default value object for the property item.
     * @param requiredFlag the value for the item's required-flag (see
     * 'CfgPropItem.setRequiredFlag()').
     * @param cmdLnParamName command-line parameter name for item.
     * @param descriptionStr description text for item.
     * @param cmdLnEnbFlag true to enable use of item as command-line
     * parameter.
     * @param helpScreenFlag true to include item in help screen data.
     * @param cmdLnOnlyFlag true for item via command line only.
     * @return The newly created property item.
     */
  public CfgPropItem add(String nameStr,Object valueObj,
           boolean requiredFlag,String cmdLnParamName,String descriptionStr,
          boolean cmdLnEnbFlag,boolean helpScreenFlag,boolean cmdLnOnlyFlag)
  {
    return add(new CfgPropItem(nameStr,valueObj,requiredFlag,
                                 cmdLnParamName,descriptionStr,cmdLnEnbFlag,
                                             helpScreenFlag,cmdLnOnlyFlag));
  }

    /**
     * Creates a new property item and adds it to the table.  If an item
     * with the same name already exists in the table, it is replaced.
     * @param nameStr the name for the property item.
     * @param valueObj the default value object for the property item.
     * @param requiredFlag the value for the item's required-flag (see
     * 'CfgPropItem.setRequiredFlag()').
     * @param cmdLnParamName command-line parameter name for item.
     * @param descriptionStr description text for item.
     * @return The newly created property item.
     */
  public CfgPropItem add(String nameStr,Object valueObj,
           boolean requiredFlag,String cmdLnParamName,String descriptionStr)
  {
    return add(new CfgPropItem(nameStr,valueObj,requiredFlag,
                                            cmdLnParamName,descriptionStr));
  }

    /**
     * Creates a new property item and adds it to the table.  If an item
     * with the same name already exists in the table, it is replaced.
     * @param nameStr the name for the property item.
     * @param valueObj the default value object for the property item.
     * @param cmdLnParamName command-line parameter name for item.
     * @param descriptionStr description text for item.
     * @param cmdLnEnbFlag true to enable use of item as command-line
     * parameter.
     * @param helpScreenFlag true to include item in help screen data.
     * @return The newly created property item.
     */
  public CfgPropItem add(String nameStr,Object valueObj,
                                String cmdLnParamName,String descriptionStr,
                                boolean cmdLnEnbFlag,boolean helpScreenFlag)
  {
    return add(new CfgPropItem(nameStr,valueObj,
                cmdLnParamName,descriptionStr,cmdLnEnbFlag,helpScreenFlag));
  }

    /**
     * Creates a new property item and adds it to the table.  If an item
     * with the same name already exists in the table, it is replaced.
     * @param nameStr the name for the property item.
     * @param valueObj the default value object for the property item.
     * @param cmdLnParamName command-line parameter name for item.
     * @param descriptionStr description text for item.
     * @param cmdLnEnbFlag true to enable use of item as command-line
     * parameter.
     * @param helpScreenFlag true to include item in help screen data.
     * @param cmdLnOnlyFlag true for item via command line only.
     * @return The newly created property item.
     */
  public CfgPropItem add(String nameStr,Object valueObj,
                                String cmdLnParamName,String descriptionStr,
          boolean cmdLnEnbFlag,boolean helpScreenFlag,boolean cmdLnOnlyFlag)
  {
    return add(new CfgPropItem(nameStr,valueObj,
                                 cmdLnParamName,descriptionStr,cmdLnEnbFlag,
                                             helpScreenFlag,cmdLnOnlyFlag));
  }

    /**
     * Creates a new property item and adds it to the table.  If an item
     * with the same name already exists in the table, it is replaced.
     * @param nameStr the name for the property item.
     * @param valueObj the default value object for the property item.
     * @param cmdLnParamName command-line parameter name for item.
     * @param descriptionStr description text for item.
     * @return The newly created property item.
     */
  public CfgPropItem add(String nameStr,Object valueObj,
                                String cmdLnParamName,String descriptionStr)
  {
    return add(new CfgPropItem(nameStr,valueObj,
                                            cmdLnParamName,descriptionStr));
  }

    /**
     * Creates a new property item and adds it to the table.  If an item
     * with the same name already exists in the table, it is replaced.
     * @param nameStr the name for the property item.
     * @param valueObj the default value object for the property item.
     * @param cmdLnParamName command-line parameter name for item.
     * @return The newly created property item.
     */
  public CfgPropItem add(String nameStr,Object valueObj,
                                                      String cmdLnParamName)
  {
    return add(new CfgPropItem(nameStr,valueObj,cmdLnParamName));
  }

  /**
   * Processes the given command-line parameter strings using the table
   * of configuration property items.  All parameter names must be
   * preceded by 1 to 2 switch characters ('-' or '/').  Non-boolean
   * parameters must take one of the following forms:
   * 'name value', 'name=value' or 'name:value'.  Boolean parameters must
   * take one of the following forms:  'name', 'name-', 'name0', 'name1',
   * 'name=0', 'name=1', 'name=true', 'name=false', 'name:0', 'name:1',
   * 'name:true', 'name:false'; where no parameter or '1' translates to
   * 'true', and a trailing '-' or '0' translates to 'false'.  A
   * configuration file of property items may also be loaded (if given).
   * @param args the array of command-line parameter strings to process.
   * @param allowExtrasFlag if false then all unmatched parameter names
   * are flagged as errors; if true then all unmatched parameter names
   * are stored in a Vector that can be retrieved via the
   * 'getExtraCmdLnParamsVec()' method.
   * @param cfgFnParam1 a parameter name that may be used to specify a
   * configuration file name on the command line (or null for none).
   * @param cfgFnParam2 a second parameter name that may be used to specify
   * a configuration file name on the command line (or null for none).
   * @param defCfgFnStr a default configuration file name, used if no
   * other configuration file name is specified on the command line (or
   * null for none).  This configuration file is loaded before any other
   * command line parameters are processed.  If the file cannot be found,
   * no error is flagged.
   * @param vInfoParam1 a parameter name that may be used to display program
   * version information (or null for none).
   * @param vInfoParam2 a second parameter name that may be used to display
   * program version information (or null for none).
   * @return true if successful, false if any errors are detected, in
   * which case a text error message is generated that may be fetched
   * via the 'getErrorMessage()' method.
   */
  public boolean processCmdLnParams(String[] args,boolean allowExtrasFlag,
                   String cfgFnParam1,String cfgFnParam2,String defCfgFnStr,
                                      String vInfoParam1,String vInfoParam2)
  {
    boolean cfgLoadedFlag = false;     //true after config file loaded
         //flag set true if equals or colon char found for mandatory value:
    boolean valueCharFlag;
    boolean valueUsedFlag;             //set true if param value was used
    boolean paramMatchedFlag;          //set true if param name matched
    boolean badValueFlag;              //set true if invalid param value
    Enumeration e;
    Object obj;
    Object [] defObjArr,valObjArr;
    CfgPropItem itemObj;
    String paramStr,nextPStr,nameStr,valueStr;
    char ch;
    int p,pNum;

    cfgFnParam1String = cfgFnParam1;   //save config file parameters
    cfgFnParam2String = cfgFnParam2;   // for help-screen data
    loadedCfgFname = null;             //clear loaded cfg file name
    vInfoParam1String = vInfoParam1;   //save version info parameters
    vInfoParam2String = vInfoParam2;   // for help-screen data
              //clear any old Vector of "extra" command-line parameters:
    extraCmdLnParamsVec = null;
                        //setup given parameter strings to be non-null
                        // and without leading switch characters:
    cfgFnParam1 = (cfgFnParam1 != null) ?
                                        removeSwitchChars(cfgFnParam1) : EMPTY_TEXT;
    cfgFnParam2 = (cfgFnParam2 != null) ?
                                        removeSwitchChars(cfgFnParam2) : EMPTY_TEXT;

    if(defCfgFnStr != null && defCfgFnStr.length() > 0)
    {    //default configuration file name was given
              //check if any config-filename parameter on command line:
      for(pNum=0; pNum<args.length; ++pNum)
      {    //for each command-line parameter
        if((paramStr=args[pNum]) != null && paramStr.length() > 0 &&
                                      isSwitchChar(paramStr.charAt(0)))
        {     //non-empty parameter data and leading switching character
          paramStr = removeSwitchChars(paramStr);     //remove swith char(s)
              //if "equals" sign or colon found in parameter then trim:
          if((p=paramStr.indexOf('=')) > 0 || (p=paramStr.indexOf(':')) > 0)
            paramStr = paramStr.substring(0,p);  //take 1st part as name
          if(paramStr.equals(cfgFnParam1) || paramStr.equals(cfgFnParam2))
          {   //parameter matches config file parameter name
            cfgLoadedFlag = true;      //set flag
            break;                     //exit loop
          }
        }
      }
      if(!cfgLoadedFlag)
      {  //no config file name parameters found
        try
        {     //open configuration file for input:
          final InputStream inStm = UtilFns.isURLAddress(defCfgFnStr) ?
                new BufferedInputStream(new URL(defCfgFnStr).openStream()) :
                  new BufferedInputStream(new FileInputStream(defCfgFnStr));
          loadedCfgFname = defCfgFnStr;     //save name of file loaded
          boolean lFlag = load(inStm); //load default configuration file
          inStm.close();               //close configuration file
          if(!lFlag)                   //if load failed then
            return false;              //return error flag
        }
        catch(FileNotFoundException ex)
        {     //file not found exception; ignore and move on
        }
        catch(Exception ex)
        {     //other error opening file; set error message
          setErrorMessage("Error opening configuration file \"" +
                                                defCfgFnStr + "\":  " + ex);
          return false;           //return error flag
        }
      }
      else
        cfgLoadedFlag = false;    //reinitialize config file loaded flag
    }

    pNum = 0;                //reinitialize parameter index
    if(pNum < args.length && (paramStr=args[pNum]) != null)
    {    //first parameter found and is not null
      do
      {       //for each command-line parameter; get next parameter
        nextPStr = (++pNum < args.length) ? args[pNum] : null;
        if(paramStr.length() <= 0)
        {     //parameter string is empty
          paramStr = nextPStr;         //go to next cmd-line parameter
          continue;                    //skip to end of loop
        }
        paramMatchedFlag = valueUsedFlag = false;     //initialize flags
        nameStr = null;
        if(isSwitchChar(paramStr.charAt(0)))
        {     //parameter begins with switch character
          if((p=paramStr.indexOf('=')) > 0 || (p=paramStr.indexOf(':')) > 0)
          {   //"equals" sign or colon found in parameter
            nameStr = paramStr.substring(0,p);   //take 1st part as name
            valueStr = paramStr.substring(p+1);  //take 2nd part as value
            valueCharFlag = true;                //indicate '=' or ':' given
          }
          else
          {   //"equals" sign or colon not found in parameter
            nameStr = paramStr;        //take parameter as name
                        //take next param as (possible) value (not null):
            valueStr = (nextPStr != null) ? nextPStr : EMPTY_TEXT;
            valueCharFlag = false;     //indicate '=' or ':' not given
          }
          nameStr = removeSwitchChars(nameStr);  //remove switch char(s)

          if(nameStr.equals(cfgFnParam1) || nameStr.equals(cfgFnParam2))
          {   //parameter name matches param for config file name
            if(!cfgLoadedFlag)
            {      //config file not previously loaded
              final InputStream inStm;
              try
              {         //open configuration file for input:
                inStm = UtilFns.isURLAddress(valueStr) ?
                   new BufferedInputStream(new URL(valueStr).openStream()) :
                     new BufferedInputStream(new FileInputStream(valueStr));
              }
              catch(FileNotFoundException ex)
              {     //file not found exception
                setErrorMessage("Configuration file \"" +
                                                 valueStr + "\" not found");
                return false;               //return error flag
              }
              catch(Exception ex)
              {         //error opening file; set error message
                setErrorMessage("Error opening configuration file \"" +
                                                   valueStr + "\":  " + ex);
                return false;               //return error flag
              }
              loadedCfgFname = valueStr;    //save name of cfg file loaded
              boolean lFlag = load(inStm);  //load configuration file
              try { inStm.close(); }        //close configuration file
              catch(Exception ex) {}
              if(!lFlag)                    //if load failed then
                return false;               //return error flag
              paramMatchedFlag = true;      //indicate param name matched
              valueUsedFlag = true;         //indicate parameter value used
            }
            else
            {      //config file was previously loaded
              setErrorMessage(              //set error message
                          "Second configuration file parameter found:  \"" +
                                                           paramStr + "\"");
              return false;                 //return error flag
            }
          }

          badValueFlag = false;        //init invalid-value error flag
          e = elements();              //get enumeration of property items
          while(e.hasMoreElements())
          {   //for each property item
            if((obj=e.nextElement()) instanceof CfgPropItem)
            {      //valid property item fetched OK
              itemObj = (CfgPropItem)obj;        //setup handle to item
              if(itemObj.getCmdLnEnbFlag())
              {    //item-via-command-line-parameter flag is set
                if(!itemObj.isBoolean())
                {  //class of property item's value is not boolean
                  if(nameStr.equals(itemObj.getName()) ||
                                  nameStr.equals(itemObj.getCmdLnParamName()))
                  {     //parameter name matched
                    paramMatchedFlag = true;     //indicate name matched
                    if((obj=itemObj.getDefaultValue()) instanceof Object[])
                    {   //parameter contains multiple values
                      defObjArr = (Object [])obj;     //set handle to array
                      if(defObjArr.length > 0)
                      {      //at least one object in array
                                  //create array to hold value objects:
                        valObjArr = (Object [])(defObjArr.clone());
                        valueUsedFlag = true;
                        p = 0;
                        while(true)
                        {
                          if(valueStr != null && valueStr.length() > 0)
                          {  //value string contains data
                            if((valObjArr[p]=CfgPropItem.setValueString(
                                            valueStr,defObjArr[p])) == null)
                            {     //error entering parameter value
                              badValueFlag = true;    //set param error flag
                              break;                  //exit loop
                            }
                            if(++p >= defObjArr.length)
                              break;     //if no more then exit loop
                                  //get next command-line parameter:
                            nextPStr = (++pNum < args.length) ?
                                                          args[pNum] : null;
                            if(nextPStr != null && nextPStr.length() > 0 &&
                                       !isSwitchChar(nextPStr.charAt(0)))
                            {     //next param exists and is not a switch
                              valueStr = nextPStr;    //take param as value
                            }
                            else
                            {     //no non-switch parameter is next
                              valueStr = EMPTY_TEXT;          //set empty value
                              valueUsedFlag = false;  //ind param loaded
                            }
                          }
                          else
                          {  //no data in value string
                            valObjArr[p] = defObjArr[p];   //enter default
                            if(++p >= defObjArr.length)
                              break;     //if no more then exit loop
                          }
                        }
                        if(!badValueFlag)
                        {    //no errors so far; enter array of value
                          if(!itemObj.setValue(valObjArr))
                            badValueFlag = true;      //if error set flag
                        }
                      }
                      else   //no objects in array
                        badValueFlag = true;     //indicate error
                    }
                    else
                    {   //parameter contains single value
                      if(itemObj.setValueString(valueStr)) //set new value
                        valueUsedFlag = true;    //if OK then ind value used
                      else
                        badValueFlag = true;     //if error then set flag
                    }
                  }
                }
                else
                {  //class of property item's value is boolean
                  if(nameStr.equals(itemObj.getName()) ||
                                  nameStr.equals(itemObj.getCmdLnParamName()))
                  {     //parameter name directly matched
                    paramMatchedFlag = true;     //indicate name matched
                    if(valueCharFlag)
                    {   //"equals" sign or colon character was given
                      if(setBooleanValueStr(valueStr,itemObj))
                        valueUsedFlag = true;    //if OK then ind value used
                      else
                        badValueFlag = true;     //if error then set flag
                    }
                    else     //parameter name is by itself
                      itemObj.setValue(true);    //enter as 'true'
                  }
                  else
                  {     //parameter name not directly matched
                    if((p=nameStr.length()-1) > 0)
                    {   //length of parameter greater than 1
                      switch(ch=nameStr.charAt(p))
                      {      //check last character of parameter
                        case '-':      //parameter ends with '-'
                          ch = '0';         //convert to '0' for false
                        case '0':      //parameter ends with '0'
                        case '1':      //parameter ends with '1'
                                            //trim off trailing character:
                          nameStr = nameStr.substring(0,p);
                          if(nameStr.equals(itemObj.getName()) ||
                                nameStr.equals(itemObj.getCmdLnParamName()))
                          {  //parameter name matched
                            paramMatchedFlag = true;  //ind name matched
                                  //create String from param value char
                                  // and send as set-value parameter:
                            if(!setBooleanValueStr(
                                    (new String(new char [] {ch})),itemObj))
                            {     //error setting value
                              badValueFlag = true;    //set flag
                            }
                          }
                          break;
                      }
                    }
                  }
                }
                if(badValueFlag)
                {       //error parsing command-line value
                  if(valueStr != null && valueStr.length() > 0)
                  {     //value text was given; set error message
                    setErrorMessage("Invalid value given for " +
                             "parameter \"" + nameStr + "\":  " + valueStr);
                  }
                  else
                  {     //value text was not given; set error message
                    setErrorMessage("No value given for parameter \"" +
                                                           paramStr + "\"");
                  }
                  return false;             //return error flag
                }
                if(paramMatchedFlag)
                {  //parameter was matched; set flag for loaded from cmd ln
                  itemObj.setCmdLnLoadedFlag(true);
                  break;          //exit loop
                }
              }
            }
          }
        }
        else  //parameter does not begin with switch character
          valueCharFlag = false;       //indicate '=' or ':' not given

        //if command-line parameter name not matched, compressed flags are
        // enabled and the parameter string is more than one character
        if (!paramMatchedFlag && compressedFlagsEnabled &&
            paramStr.length() > 1)
        {
          final char firstChar = paramStr.charAt(0);
          if (nameStr != null &&
              isSwitchChar(paramStr.charAt(0)) &&
              firstChar != paramStr.charAt(1))
          {
            String s;
            final int numChars = nameStr.length();
            //check for each character
            for (int i = 0; i < numChars; i++)
            {
              s = nameStr.substring(i, i + 1);
              itemObj = getCfgPropItem(s, true);
              if (itemObj != null)
              {
                if(!itemObj.isBoolean())
                {
                  setErrorMessage("Invalid parameter \"" + firstChar + s +
                                  "\", only boolean flags may be specified " +
                                  "when using compressed flags");
                  return false; //return error flag
                }
                paramMatchedFlag = true;
                //set flag for loaded from cmd ln
                itemObj.setCmdLnLoadedFlag(true);
              }
              else
              {
                if (!processExtraCmdLnParam(allowExtrasFlag, firstChar + s))
                {
                  return false; //return error flag
                }
              }
            }
          }
        }
        if(!paramMatchedFlag)
        {     //command-line parameter name not matched
          if (!processExtraCmdLnParam(allowExtrasFlag, paramStr))
          {
            return false; //return error flag
          }
        }

              //if parameter value was used and not given (after equals or
              // colon) in same parameter then get another cmd-line param:
        if(valueUsedFlag && !valueCharFlag)
          paramStr = (++pNum < args.length) ? args[pNum] : null;
        else                           //if "next" cmd-line param was not
          paramStr = nextPStr;         // value then use it as next param
      }
      while(paramStr != null);
    }
    return true;             //return OK flag
  }

  /**
   * Processes the given command-line parameter strings using the table
   * of configuration property items.  All parameter names must be
   * preceded by 1 to 2 switch characters ('-' or '/').  Non-boolean
   * parameters must take one of the following forms:
   * 'name value', 'name=value' name:value'.  Boolean parameters must
   * take one of the following forms:  'name', 'name-', 'name0', 'name1',
   * 'name=0', 'name=1', 'name=true', 'name=false', 'name:0', 'name:1',
   * 'name:true', 'name:false'; where no parameter or '1' translates to
   * 'true', and a trailing '-' or '0' translates to 'false'.  A
   * configuration file of property items may also be loaded (if given).
   * @param args the array of command-line parameter strings to process.
   * @param allowExtrasFlag if false then all unmatched parameter names
   * are flagged as errors; if true then all unmatched parameter names
   * are stored in a Vector that can be retrieved via the
   * 'getExtraCmdLnParamsVec()' method.
   * @param cfgFnParam1 a parameter name that may be used to specify a
   * configuration file name on the command line (or null for none).
   * @param cfgFnParam2 a second parameter name that may be used to specify
   * a configuration file name on the command line (or null for none).
   * @param defCfgFnStr a default configuration file name, used if no
   * other configuration file name is specified on the command line (or
   * null for none).  This configuration file is loaded before any other
   * command line parameters are processed.  If the file cannot be found,
   * no error is flagged.
   * @return true if successful, false if any errors are detected, in
   * which case a text error message is generated that may be fetched
   * via the 'getErrorMessage()' method.
   */
  public boolean processCmdLnParams(String[] args,boolean allowExtrasFlag,
                   String cfgFnParam1,String cfgFnParam2,String defCfgFnStr)
  {
    return processCmdLnParams(args,allowExtrasFlag,cfgFnParam1,cfgFnParam2,
                                                     defCfgFnStr,null,null);
  }

  /**
   * Processes the given command-line parameter strings using the table
   * of configuration property items.  All parameter names must be
   * preceded by 1 to 2 switch characters ('-' or '/').  Non-boolean
   * parameters must take one of the following forms:
   * 'name value', 'name=value' name:value'.  Boolean parameters must
   * take one of the following forms:  'name', 'name-', 'name0', 'name1',
   * 'name=0', 'name=1', 'name=true', 'name=false', 'name:0', 'name:1',
   * 'name:true', 'name:false'; where no parameter or '1' translates to
   * 'true', and a trailing '-' or '0' translates to 'false'.  A
   * configuration file of property items may also be loaded (if given).
   * @param args the array of command-line parameter strings to process.
   * @param allowExtrasFlag if false then all unmatched parameter names
   * are flagged as errors; if true then all unmatched parameter names
   * are stored in a Vector that can be retrieved via the
   * 'getExtraCmdLnParamsVec()' method.
   * @param vInfoParam1 a parameter name that may be used to display program
   * version information (or null for none).
   * @param vInfoParam2 a second parameter name that may be used to display
   * program version information (or null for none).
   * @return true if successful, false if any errors are detected, in
   * which case a text error message is generated that may be fetched
   * via the 'getErrorMessage()' method.
   */
  public boolean processCmdLnParams(String[] args,boolean allowExtrasFlag,
                                      String vInfoParam1,String vInfoParam2)
  {
    return processCmdLnParams(args,allowExtrasFlag,null,null,null,
                                                   vInfoParam1,vInfoParam2);
  }

  /**
   * Processes the given command-line parameter strings using the table
   * of configuration property items.  All parameter names must be
   * preceded by 1 to 2 switch characters ('-' or '/').  Non-boolean
   * parameters must take one of the following forms:
   * 'name value', 'name=value' name:value'.  Boolean parameters must
   * take one of the following forms:  'name', 'name-', 'name0', 'name1',
   * 'name=0', 'name=1', 'name=true', 'name=false', 'name:0', 'name:1',
   * 'name:true', 'name:false'; where no parameter or '1' translates to
   * 'true', and a trailing '-' or '0' translates to 'false'.  A
   * configuration file of property items may also be loaded (if given).
   * @param args the array of command-line parameter strings to process.
   * @param allowExtrasFlag if false then all unmatched parameter names
   * are flagged as errors; if true then all unmatched parameter names
   * are stored in a Vector that can be retrieved via the
   * 'getExtraCmdLnParamsVec()' method.
   * @return true if successful, false if any errors are detected, in
   * which case a text error message is generated that may be fetched
   * via the 'getErrorMessage()' method.
   */
  public boolean processCmdLnParams(String[] args,boolean allowExtrasFlag)
  {
    return processCmdLnParams(args,allowExtrasFlag,null,null,null,
                                                                 null,null);
  }

    /**
     * Returns a copy of the given String with 1 to 2 leading switch
     * characters ('-' or '/') removed; or the original String if no
     * leading switch characters are found.
     * @param str string
     * @return string copy
     */
  public static String removeSwitchChars(String str)
  {
    if(str == null)
      return EMPTY_TEXT;
    final int len = str.length();
    int p = 0;
    if(p < len && isSwitchChar(str.charAt(p)))
    {    //string begins with a switch character; increment past character
      if(++p < len && isSwitchChar(str.charAt(p)))
      {  //2nd character is also a switch character
        ++p;            //increment past character
      }
      return str.substring(p);         //return string after switch char(s)
    }
    return str;         //no switch characters; return string
  }

  /**
   * Determines if compressed flags should be enabled. If enabled this allows
   * specifying multiple single character command line switches with a single
   * parameter, such as "-ab" rather than "-a -b").
   * @param b true if compressed flags should be enabled, false otherwise.
   * @return the table of configuration property items.
   */
  public CfgProperties setCompressedFlagsEnabled(boolean b)
  {
    compressedFlagsEnabled = b;
    return this;
  }

  /**
   * Set the description separator text.
   * @param s the the description separator text or null if none.
   * @return the table of configuration property items.
   */
  public CfgProperties setDescriptionSeparatorString(String s)
  {
    descriptionSeparatorString = s;
    return this;
  }

    /**
     * Interprets the given string as a boolean value and enters it as
     * the value object for the given property item.  A "0" is converted
     * to 'false' and a "1" is converted to 'true'.
     * @param str string
     * @param itemObj item object
     * @return true if successful, false if the string could not be
     * converted to a boolean value.
     */
  private boolean setBooleanValueStr(String str,CfgPropItem itemObj)
  {
    if(str == null || itemObj == null)      //if null handle(s) then
      return false;                         //return error flag
    if(str.equals("0"))
      str = "false";              //convert "0" to "false"
    else if(str.equals("1"))
      str = "true";               //convert "1" to "true"
    return itemObj.setValueString(str);     //convert string; return flag
  }

    /**
     * Returns Vector of "extra" command-line parameters Strings filled
     * in by the 'processCmdLnParams()' method; or null if no "extra"
     * command-line parameters were found.
     * @return vector of "extra" command-line parameters
     */
  public Vector getExtraCmdLnParamsVec()
  {
    return extraCmdLnParamsVec;
  }

    /**
     * Returns a String of data for use on a command-line parameters
     * "help" screen.  The configuration file parameter names given to
     * the 'processCndLnParams()' method are also included.
     * @return help screen text
     */
  public String getHelpScreenData()
  {
    return getHelpScreenData(true);
  }

    /**
     * Returns a String of data for use on a command-line parameters
     * "help" screen.  The configuration file parameter names given to
     * the 'processCndLnParams()' method are also included.
     * @param displayPropertyName true to always display the property name
     * (as well as the command-line parameter name), false to only display it
     * when there is no command-line parameter name.
     * @return help screen text
     */
  public String getHelpScreenData(boolean displayPropertyName)
  {
    return getHelpScreenData(displayPropertyName,null);
  }

    /**
     * Returns a String of data for use on a command-line parameters
     * "help" screen.  The configuration file parameter names given to
     * the 'processCndLnParams()' method are also included.
     * @param displayPropertyName true to always display the property name
     * (as well as the command-line parameter name), false to only display it
     * when there is no command-line parameter name.
     * @param preText text to be displayed before each command-line parameter
     * line or null for none.
     * @return help screen text
     */
  public String getHelpScreenData(
      boolean displayPropertyName,String preText)
  {
    return getHelpScreenData(displayPropertyName,preText,null);
  }

    /**
     * Returns a String of data for use on a command-line parameters
     * "help" screen.  The configuration file parameter names given to
     * the 'processCndLnParams()' method are also included.
     * @param displayPropertyName true to always display the property name
     * (as well as the command-line parameter name), false to only display it
     * when there is no command-line parameter name.
     * @param preText text to be displayed before each command-line parameter
     * line or null for none.
     * @param postText text to be displayed after each command-line parameter
     * line or null for none.
     * @return help screen text
     */
  public String getHelpScreenData(
      boolean displayPropertyName,String preText,String postText)
  {
    return getHelpScreenData(displayPropertyName,preText,postText,"  ");
  }

    /**
     * Returns a String of data for use on a command-line parameters
     * "help" screen.  The configuration file parameter names given to
     * the 'processCndLnParams()' method are also included.
     * @param displayPropertyName true to always display the property name
     * (as well as the command-line parameter name), false to only display it
     * when there is no command-line parameter name.
     * @param preText text to be displayed before each command-line parameter
     * line or null for none.
     * @param postText text to be displayed after each command-line parameter
     * line or null for none.
     * @param preDescText the text to be displayed before the description.
     * @return help screen text
     */
  public String getHelpScreenData(
      boolean displayPropertyName,String preText,String postText,String preDescText)
  {
    Enumeration e;
    Object obj;
    CfgPropItem itemObj;
    String cmdLnParamName,propertyName,descriptionStr;
    String cfg1Str,cfg2Str,vInfo1Str,vInfo2Str;

         //check configuration parameter names and setup so that 'cfg1Str'
         // is shorter than 'cfg2Str' or, if only one given, so that
         // 'cfg1str' contains the configuration parameter name:
    if(cfgFnParam2String == null || cfgFnParam2String.length() <= 0 ||
             (cfgFnParam1String != null && cfgFnParam1String.length() > 0 &&
                  cfgFnParam2String.length() > cfgFnParam1String.length()))
    {    //no second cfg param or it's longer then first one
      cfg1Str = cfgFnParam1String;          //keep current order
      cfg2Str = cfgFnParam2String;
    }
    else
    {    //second cfg param exists and it's not longer than first one
      cfg1Str = cfgFnParam2String;          //swap order
      cfg2Str = cfgFnParam1String;
    }

         //check version info parameter names and setup so that 'vInfo1Str'
         // is shorter than 'vInfo2Str' or, if only one given, so that
         // 'vInfo1str' contains the version info parameter name:
    if(vInfoParam2String == null || vInfoParam2String.length() <= 0 ||
             (vInfoParam1String != null && vInfoParam1String.length() > 0 &&
                  vInfoParam2String.length() > vInfoParam1String.length()))
    {    //no second version info param or it's longer then first one
      vInfo1Str = vInfoParam1String;        //keep current order
      vInfo2Str = vInfoParam2String;
    }
    else
    {    //second version info param exists & it's not longer than first one
      vInfo1Str = vInfoParam2String;        //swap order
      vInfo2Str = vInfoParam1String;
    }

         //find maximum lengths among propetry item names and
         // command-line parameter names:
                                                 //init length trackers:
    int maxPropNameLen = (cfg2Str != null) ? cfg2Str.length() : 0;
    int maxCmdNameLen = (cfg1Str != null) ? cfg1Str.length() : 0;
    if(vInfo2Str != null && vInfo2Str.length() > maxPropNameLen)
      maxPropNameLen = vInfo2Str.length();
    if(vInfo1Str != null && vInfo1Str.length() > maxCmdNameLen)
      maxCmdNameLen = vInfo1Str.length();
    e = elements();                         //enum of property items
    while(e.hasMoreElements())
    {    //for each property item
      if((obj=e.nextElement()) instanceof CfgPropItem &&
                             (itemObj=(CfgPropItem)obj).getCmdLnEnbFlag() &&
                                                itemObj.getHelpScreenFlag())
      {       //'CfgPropItem' fetched OK and cmd-ln and help screen enabled
        if((cmdLnParamName=itemObj.getCmdLnParamName()) != null &&
           cmdLnParamName.length() > maxCmdNameLen)
        {     //cmd-ln param name String fetched OK and is new maximum len
          maxCmdNameLen = cmdLnParamName.length();     //save new maximum length
        }
        if((displayPropertyName || cmdLnParamName == null || cmdLnParamName.length() <= 0) &&
           (propertyName=itemObj.getName()) != null && propertyName.length() > maxPropNameLen)
        {     //property name String fetched OK and is new maximum length
          maxPropNameLen = propertyName.length();    //save new maximum length
        }
      }
    }

    if (!displayPropertyName) //if not always displaying the property name
    {
      //use the larger of max property name or command-line parameter name
      if (maxPropNameLen >= maxCmdNameLen)
        maxCmdNameLen = ++maxPropNameLen;
      else
        maxPropNameLen = maxCmdNameLen;
    }

         //build help screen data:
    final StringBuffer buff = new StringBuffer();     //buffer for data

              //put in line for configuration parameter names:
    if(cfg1Str != null && cfg1Str.length() > 0)
    {    //at least one configuration parameter name exists
      if(cfg2Str == null || cfg2Str.length() <= 0)
      {       //only one configuration parameter name exists
        if(cfg1Str.length() > maxCmdNameLen &&
                                         cfg1Str.length() <= maxPropNameLen)
        {     //param name is wider than 1st column but not 2nd column
          cfg2Str = cfg1Str;      //put param at second column
          cfg1Str = EMPTY_TEXT;           //clear first column param slot
        }
        else  //param name width not between 1st and 2nd column widths
          cfg2Str = EMPTY_TEXT;           //make sure 2nd param is not null
      }
      if(cfg1Str.length() > 0)         //if 1st config param exists then
        cfg1Str = DASH_STRING + cfg1Str;      //add dash before name
      if(cfg2Str.length() > 0)         //if 2nd config param exists then
        cfg2Str = DASHES_STRING + cfg2Str;     //add dashes before name
      buff.append(      //add first config parameter name to data buffer:
                 UtilFns.fixStringLen(cfg1Str,maxCmdNameLen+3,false,false));
      buff.append(      //add 2nd config param name (or empty string):
                UtilFns.fixStringLen(cfg2Str,maxPropNameLen+3,false,false));
      buff.append(preDescText + CFG_PARAM_DESC_STR);
      if(size() > 0)                        //if more items to show then
        buff.append(UtilFns.newline);       //put in newline separator
    }
    
    int descriptionIndex = 0;
    if (descriptionSeparatorString != null)
    {
      descriptionIndex = maxCmdNameLen + maxPropNameLen + 6 + preDescText.length();
    }

    e = elements();                         //enum of property items
    while(e.hasMoreElements())
    {         //for each property item
      if((obj=e.nextElement()) instanceof CfgPropItem &&
                             (itemObj=(CfgPropItem)obj).getCmdLnEnbFlag() &&
                                                itemObj.getHelpScreenFlag())
      {       //'CfgPropItem' fetched OK and cmd-ln and help screen enabled
        if (preText != null)  //if pre-text add it
          buff.append(preText);
                        //get command-line parameter name for item:
        if((cmdLnParamName=itemObj.getCmdLnParamName()) != null && cmdLnParamName.length() > 0)
          cmdLnParamName = DASH_STRING + cmdLnParamName;       //if exists, put dash before param name
        else
          cmdLnParamName = EMPTY_TEXT;               //if no param then use empty string
        //if always displaying the property name or if command line parameter
        if (displayPropertyName || cmdLnParamName.length() > 0)
        {
          buff.append( //add command-line parameter name to data buffer:
              UtilFns.fixStringLen(cmdLnParamName,
              maxCmdNameLen+3, false, false));
        }
                        //get name for property item:
        if((propertyName=itemObj.getName()) != null && propertyName.length() > 0)
          propertyName = DASHES_STRING + propertyName;  //if exists, put dashes before prop name
        else
          propertyName = EMPTY_TEXT;  //if no name then use empty string
        //if always displaying the property name or if no command line parameter
        if (displayPropertyName || cmdLnParamName.length() <= 0)
        {
          buff.append( //add property name to data buffer:
              UtilFns.fixStringLen(propertyName, maxPropNameLen+3, false, false));
        }
        //add the pre-description text
        if((descriptionStr=itemObj.getDescriptionStr()) != null && descriptionStr.length() > 0)
        {
          buff.append(preDescText);
          if (descriptionSeparatorString != null)
          {
            String s;
            int beginIndex = 0;
            int endIndex;
            while ((endIndex = descriptionStr.indexOf(
                descriptionSeparatorString, beginIndex)) >= 0)
            {
              s = descriptionStr.substring(beginIndex, endIndex);
              buff.append(s);
              buff.append(UtilFns.newline);
              buff.append(UtilFns.fixStringLen(EMPTY_TEXT, descriptionIndex));
              beginIndex = endIndex + 1;
            }
            s = descriptionStr.substring(beginIndex);
            buff.append(s);
          }
          else
          {
            buff.append(descriptionStr);
          }
        }
        if (postText != null)  //if post-text add it
          buff.append(postText);
        buff.append(UtilFns.newline);       //put in newline separator
      }
    }

              //put in line for version information parameter names:
    if(vInfo1Str != null && vInfo1Str.length() > 0)
    {    //at least one configuration parameter name exists
      if(vInfo2Str == null || vInfo2Str.length() <= 0)
      {       //only one configuration parameter name exists
        if(vInfo1Str.length() > maxCmdNameLen &&
                                       vInfo1Str.length() <= maxPropNameLen)
        {     //param name is wider than 1st column but not 2nd column
          vInfo2Str = vInfo1Str;  //put param at second column
          vInfo1Str = EMPTY_TEXT;         //clear first column param slot
        }
        else  //param name width not between 1st and 2nd column widths
          vInfo2Str = EMPTY_TEXT;         //make sure 2nd param is not null
      }
      if(vInfo1Str.length() > 0)       //if 1st config param exists then
        vInfo1Str = DASH_STRING + vInfo1Str;  //add dash before name
      if(vInfo2Str.length() > 0)       //if 2nd config param exists then
        vInfo2Str = DASHES_STRING + vInfo2Str; //add dashes before name
      buff.append(      //add first config parameter name to data buffer:
               UtilFns.fixStringLen(vInfo1Str,maxCmdNameLen+3,false,false));
      buff.append(      //add 2nd config param name (or empty string):
              UtilFns.fixStringLen(vInfo2Str,maxPropNameLen+3,false,false));
      buff.append(preDescText + VINFO_PARAM_DESC_STR);
    }

    return buff.toString();       //return String version of buffer
  }

  /**
   * Determines if the specified character is a switch character ('-' or '/').
   * @param str string
   * @return true if the specified character is a switch character,
   * false otherwise.
   */
  public static boolean isSwitchChar(char c)
  {
    return CfgPropItem.isSwitchChar(c);
  }

  /**
   * group selection data changed listeners support
   */

  /**
   * Registers the given 'DataChangedListener' object with all items
   * that have the specified group selection object.
   * @param groupSelObj group selection object.
   * @param listenerObj the 'DataChangedListener' object.
   */
  public void addGroupDataChangedListener(Object groupSelObj,
                                     DataChangedListener listenerObj)
  {
    if (listenerObj == null || groupSelObj == null)
      return;
    //for each item
    final Iterator it = this.values().iterator();
    while (it.hasNext())
    {
      try
      {
        //if the item belongs to the specified group
        CfgPropItem item = ((CfgPropItem)it.next());
        if (item.getGroupSelObj() != null &&
            item.getGroupSelObj().equals(groupSelObj))
          //add the listener to the item
          item.addDataChangedListener(listenerObj);
      }
      catch (ClassCastException ex) {}
    }
  }

  /**
   * Determines if compressed flags are enabled.
   * @return true if compressed flags are enabled, false otherwise.
   */
  public boolean areCompressedFlagsEnabled()
  {
    return compressedFlagsEnabled;
  }

  /**
   * Unregisters the given 'DataChangedListener' object with all items
   * that have the specified group selection object.
   * @param groupSelObj group selection object.
   * @param listenerObj the 'DataChangedListener' object.
   */
  public void removeGroupDataChangedListener(Object groupSelObj,
                                        DataChangedListener listenerObj)
  {
    if (listenerObj == null || groupSelObj == null)
      return;
    //for each item
    final Iterator it = this.values().iterator();
    while (it.hasNext())
    {
      try
      {
        //if the item belongs to the specified group
        CfgPropItem item = ((CfgPropItem)it.next());
        if (item.getGroupSelObj() != null &&
            item.getGroupSelObj().equals(groupSelObj))
          //remove the listener to the item
          item.removeDataChangedListener(listenerObj);
      }
      catch (ClassCastException ex) {}
    }
  }

  /**
   * Get the configuration property item.
   * @param nameStr the name for the property item.
   * @param cmdLnParamFlag true to only return a command line property item.
   * @return the configuration property item or null if none.
   */
  protected CfgPropItem getCfgPropItem(String nameStr, boolean cmdLnParamFlag)
  {
    Object obj;
    CfgPropItem itemObj;
    final Enumeration e = elements(); //get enumeration of property items
    while (e.hasMoreElements())
    { //for each property item
      if ( (obj = e.nextElement()) instanceof CfgPropItem)
      { //valid property item fetched OK
        itemObj = (CfgPropItem) obj; //setup handle to item
        if (!cmdLnParamFlag || itemObj.getCmdLnEnbFlag())
        {
          if (nameStr.equals(itemObj.getName()) ||
              nameStr.equals(itemObj.getCmdLnParamName()))
          { //parameter name matched
            return itemObj;
          }
        }
      }
    }
    return null;
  }

  /**
   * Process the extra command line parameter.
   * @param allowExtrasFlag true if unmatched parameter names are allowed,
   * false otherwise.
   * @param paramStr the parameter string.
   * @return true if successful, false otherwise.
   */
  protected boolean processExtraCmdLnParam(
      boolean allowExtrasFlag, String paramStr)
  {
    if (!allowExtrasFlag)
    { //extra command-line parameters not allowed; set error message
      setErrorMessage("Invalid parameter \"" + paramStr + "\"");
      return false; //return error flag
    }
    //extra command-line parameters are allowed; add param to Vec
    if (extraCmdLnParamsVec == null) //if Vector not allocated
    {
      extraCmdLnParamsVec = new Vector(); // then allocate one now
    }
    extraCmdLnParamsVec.add(paramStr); //add param string to Vec
    return true;
  }

  @Override
  public CfgProperties setEmptyStringDefaultFlag(boolean flg)
  {
    super.setEmptyStringDefaultFlag(flg);
    return this;
  }
}
