//CircularBuffer.java:  Implements a circular buffer.
//
//  1/30/2006 -- [ET]  Initial version.
//   8/8/2006 -- [ET]  Added 'contains()' method.
//

package com.isti.util;

/**
 * Class CircularBuffer implements a circular buffer.  The buffer holds
 * a limit number of items such that when the buffer is full and an item
 * is added the oldest item in the buffer is removed.  Note that this
 * class is not thread synchronized.
 */
public class CircularBuffer
{
  protected Object [] bufferObjsArray;
  protected int allocBufferSize;
  protected int bufferPosition = 0;
  protected long totalItemsCounter = 0;

  /**
   * Creates a circular buffer.
   * @param bufferSize size of circular buffer.
   */
  public CircularBuffer(int bufferSize)
  {
    setBufferSize(bufferSize);
  }

  /**
   * Sets the buffer size of the circular buffer.  Any items currently
   * in the buffer are discarded.
   * @param bufferSize size of circular buffer.
   */
  public final void setBufferSize(int bufferSize)
  {
    if(bufferSize <= 0)
      throw new IllegalArgumentException("Buffer size not positive");
    bufferObjsArray = new Object[allocBufferSize=bufferSize];
  }

  /**
   * Adds the given item to the buffer.  If the buffer is full then
   * the oldest item in the buffer is removed.
   * @param obj item to be added.
   */
  public void add(Object obj)
  {
    bufferObjsArray[bufferPosition] = obj;       //enter item
    if(++bufferPosition >= allocBufferSize)      //increment position
      bufferPosition = 0;         //if past end then set to beginning
    ++totalItemsCounter;                         //increment counter
  }

  /**
   * Returns an array containing the items in the buffer in the order
   * in which they were entered.
   * @return A new array of objects.
   */
  public Object [] getBufferItems()
  {
    final Object [] retArr;
    if(totalItemsCounter <= allocBufferSize)
    {    //buffer not "past full"
      final int bufSize = (int)totalItemsCounter;
      retArr = new Object[bufSize];              //create return array
                             //copy items to return array:
      System.arraycopy(bufferObjsArray,0,retArr,0,bufSize);
    }
    else
    {    //buffer is "past full"
      retArr = new Object[allocBufferSize];      //create return array
      int pos = bufferPosition;
      int retIdx = 0;
      while(true)
      {       //for each item in array
        retArr[retIdx] = bufferObjsArray[pos];   //copy item
        if(++retIdx >= allocBufferSize)     //increment index
          break;             //if complete then exit loop
        if(++pos >= allocBufferSize)        //increment position
          pos = 0;           //if past end then set to beginning
      }
    }
    return retArr;
  }

  /**
   * Returns an array containing string items in the buffer in the order
   * in which they were entered.  The 'toString()' method is used to
   * convert each item to a string.
   * @return A new array of strings.
   */
  public String [] getStrBufferItems()
  {
    final String [] retArr;
    if(totalItemsCounter <= allocBufferSize)
    {    //buffer not "past full"
      final int bufSize = (int)totalItemsCounter;
      retArr = new String[bufSize];              //create return array
      for(int idx=0; idx<bufSize; ++idx)         //for each entered item
        retArr[idx] = bufferObjsArray[idx].toString();  //conv/copy item
    }
    else
    {    //buffer is "past full"
      retArr = new String[allocBufferSize];      //create return array
      int pos = bufferPosition;
      int retIdx = 0;
      while(true)
      {       //for each item in array
        retArr[retIdx] = bufferObjsArray[pos].toString();  //conv/copy item
        if(++retIdx >= allocBufferSize)     //increment index
          break;             //if complete then exit loop
        if(++pos >= allocBufferSize)        //increment position
          pos = 0;           //if past end then set to beginning
      }
    }
    return retArr;
  }

  /**
   * Returns a string containing items in the buffer in the order
   * in which they were entered.  The 'toString()' method is used to
   * convert each item to a string.
   * @param sepStr separator to be placed between each item.
   * @return A new string containing the items.
   */
  public String getBufferItemsAsString(String sepStr)
  {
    if(sepStr == null)       //if null parameter then
      sepStr = "";           //use empty string
    final String [] strArr = getStrBufferItems();     //get array of items
    final StringBuffer buff = new StringBuffer();     //create buffer
    if(strArr.length > 0)
    {    //items array not empty
      int idx = 0;
      while(true)
      {  //for each item in array
        buff.append(strArr[idx]);           //add to buffer
        if(++idx >= strArr.length)     //if all items processed then
          break;                       //exit loop
        buff.append(sepStr);                //add separator
      }
    }
    return buff.toString();       //return string version of buffer
  }

  /**
   * Returns a count of the total number of items that have ever been
   * entered into the buffer.
   * @return A count of the total number of items that have ever been
   * entered into the buffer.
   */
  public long getTotalItemsCount()
  {
    return totalItemsCounter;
  }

  /**
   * Determines whether or not an equivalent item is contained in the
   * buffer.
   * @param obj item to check for.
   * @return true if an equivalent item is contained in the buffer;
   * false if not.
   */
  public boolean contains(Object obj)
  {
              //determine current number of items in buffer:
    final int chkSize = (allocBufferSize < totalItemsCounter) ?
                                   allocBufferSize : (int)totalItemsCounter;
    if(obj != null)
    {    //given item not null
      for(int pos=0; pos<chkSize; ++pos)
      {  //for each item in buffer; if match then return true
        if(obj.equals(bufferObjsArray[pos]))
          return true;
      }
    }
    else
    {    //given item is null
      for(int pos=0; pos<chkSize; ++pos)
      {  //for each item in buffer; if match then return true
        if(bufferObjsArray[pos] == null)
          return true;
      }
    }
    return false;
  }
}
