//ClientLogViaCfgProps.java:  Creates a log file using 'CfgPropItem'
//                          objects.
//
//  5/31/2007 -- [ET]  Copied from 'LogFileViaCfgProps', with "clientLog..."
//                     configuration-property names retained.
// 10/27/2008 -- [ET]  Added 'setBaseWriteDirStr()', 'getBaseWriteDirStr()',
//                     'fromBaseDir()' methods and implementation.
//   9/1/2010 -- [ET]  Modified use of console-output file so a new one is
//                     created every 30 days (default) and files older than
//                     a year (default) are automatically deleted.
//

package com.isti.util;

import java.util.Iterator;
import java.io.File;
import java.io.PrintStream;

/**
 * Class ClientLogViaCfgProps creates a log file using 'CfgPropItem'
 * objects.  The usage will usually be to extend this class, call the
 * 'createLogFile()' method, and then make use of the 'logObj' variable.
 */
public class ClientLogViaCfgProps extends ErrorMessageMgr
{
  /** Log-file object. */
  protected LogFile logObj = null;
  /** Output stream for redirected console output. */
  protected PrintStream consoleRedirectStream = null;
  /** Base dir for write-access files (for 'fromBaseDir()' methods). */
  protected static String baseWriteDirStr = UtilFns.EMPTY_STRING;

  /** Configuration-properties object holding the log-file items. */
  public final CfgProperties logFileProps = new CfgProperties();

  /** Name of the console output redirect file. */
  public final CfgPropItem consoleRedirectFileNameProp =
                         logFileProps.add("consoleRedirectFileName","",null,
                                "Name of the console output redirect file");

  /** Name of the log file. */
  public final CfgPropItem clientLogFileNameProp =
                               logFileProps.add("clientLogFileName","",null,
                                                    "Name of the log file");

  /** Message level for log file output. */
  public final CfgPropItem clientLogFileLevelProp =
                logFileProps.add("clientLogFileLevel",LogFile.INFO_STR,null,
                                       "Message level for log file output");

  /** Message level for console output. */
  public final CfgPropItem clientConsoleLevelProp =
             logFileProps.add("clientConsoleLevel",LogFile.WARNING_STR,null,
                                        "Message level for console output");

  /** Maximum age for log files (days, 0=infinite). */
  public final CfgPropItem logFilesMaxAgeInDaysProp =
               logFileProps.add("logFilesMaxAgeInDays",Integer.valueOf(30),null,
                            "Maximum age for log files (days, 0=infinite)");

  /** Switch interval for console files, in days. */
  public final CfgPropItem consoleFilesSwitchIntvlDays = logFileProps.add(
                         "consoleFilesSwitchIntvlDays",Integer.valueOf(30),null,
                                "Switch interval for console files (days)");

  /** Console files maximum age in days (0=infinite). */
  public final CfgPropItem consoleFilesMaxAgeInDays = logFileProps.add(
                           "consoleFilesMaxAgeInDays",Integer.valueOf(366),null,
                                    "Maximum age for console files (days)");


  /**
   * Creates an object for creating a log file using 'CfgPropItem'
   * objects.
   */
  public ClientLogViaCfgProps()
  {
  }

  /**
   * Creates an object for creating a log file using 'CfgPropItem'
   * objects.
   * @param cfgPropsObj the configuration-properties object to use.
   * @param groupSelObj the configuration-group-selection object to use,
   * or null for none.
   */
  public ClientLogViaCfgProps(CfgProperties cfgPropsObj, Object groupSelObj)
  {
    addLogFilePropItems(cfgPropsObj,groupSelObj);
  }

  /**
   * Creates an object for creating a log file using 'CfgPropItem'
   * objects.
   * @param cfgPropsObj the configuration-properties object to use.
   */
  public ClientLogViaCfgProps(CfgProperties cfgPropsObj)
  {
    addLogFilePropItems(cfgPropsObj,null);
  }

  /**
   * Adds the log-file items to the given configuration-properties object.
   * @param cfgPropsObj the configuration-properties object to use.
   * @param groupSelObj the configuration-group-selection object to use,
   * or null for none.
   */
  public final void addLogFilePropItems(CfgProperties cfgPropsObj,
                                                         Object groupSelObj)
  {
    if(cfgPropsObj != null)
    {    //config-props object was given
      final Iterator iterObj = logFileProps.values().iterator();
      Object obj;
      while(iterObj.hasNext())
      {  //for each log-file item; set group object and add
        if((obj=iterObj.next()) instanceof CfgPropItem)
          cfgPropsObj.add(((CfgPropItem)obj).setGroupSelObj(groupSelObj));
      }
    }
    else      //config-props object not given
      setGroupSelObj(groupSelObj);     //just set group objects
  }

  /**
   * Adds the log-file items to the given configuration-properties object.
   * @param cfgPropsObj the configuration-properties object to use.
   */
  public final void addLogFilePropItems(CfgProperties cfgPropsObj)
  {
    addLogFilePropItems(cfgPropsObj,null);
  }

  /**
   * Sets the group-selection object for the log-file items.
   * @param groupSelObj the configuration-group-selection object to use,
   * or null for none.
   */
  public final void setGroupSelObj(Object groupSelObj)
  {
    final Iterator iterObj = logFileProps.values().iterator();
    Object obj;
    while(iterObj.hasNext())
    {    //for each log-file item; set group object
      if((obj=iterObj.next()) instanceof CfgPropItem)
        ((CfgPropItem)obj).setGroupSelObj(groupSelObj);
    }
  }

  /**
   * Creates a log file and sets it up using the configuration-property
   * item objects.  A redirected console output stream may also be setup.
   * @param abortOnErrorFlag if true then this method will return
   * immediately after the first error is flagged (without creating
   * the log file); if false then this method will always create the
   * log file.
   * @return 'true' if successful, false if an error occurred (in which
   * case an error message will be sent to the console and may be fetched
   * via the 'getErrorMessageString()' method).
   */
  public boolean createLogFile(boolean abortOnErrorFlag)
  {
    boolean retFlag = true;

         //interpret log-file message level string:
    final String logFileLevelStr = clientLogFileLevelProp.stringValue();
    final int logFileLevelVal;
    Integer integerObj;     //convert level string to value:
    Object obj;
    if((integerObj=LogFile.levelStringToValue(logFileLevelStr)) == null)
    {    //unable to match string; show error message
      final String errStr = "Invalid value for \"" +
                      clientLogFileLevelProp.getName() + "\" setting:  \"" +
                                                     logFileLevelStr + "\"";
      System.err.println(errStr);
      System.err.println("  Available values:  " +    //show avail values
                                         LogFile.getLevelNamesDisplayStr());
      enterErrorMessageString(errStr);
      if(abortOnErrorFlag)        //if flag then
        return false;             //return immediately after error
      retFlag = false;                      //setup return flag
              //attempt to use cfgProp default log-level string value:
      if((obj=clientLogFileLevelProp.getDefaultValue()) instanceof String &&
             ((integerObj=LogFile.levelStringToValue((String)obj)) != null))
      {       //default value fetched and converted OK; use it
        logFileLevelVal = integerObj.intValue();
      }
      else    //unable to use cfgProp default; use fallback default
        logFileLevelVal = LogFile.INFO;
    }
    else
      logFileLevelVal = integerObj.intValue();        //save value

    final int consoleLevelVal;
         //interpret console message level string:
    final String consoleLevelStr = clientConsoleLevelProp.stringValue();
         //convert level string to value:
    if((integerObj=LogFile.levelStringToValue(consoleLevelStr)) == null)
    {    //unable to match string; show error message
      final String errStr = "Invalid value for \"" +
                      clientConsoleLevelProp.getName() + "\" setting:  \"" +
                                                     consoleLevelStr + "\"";
      System.err.println(errStr);
      System.err.println("  Available values:  " +    //show avail values
                                         LogFile.getLevelNamesDisplayStr());
      enterErrorMessageString(errStr);
      if(abortOnErrorFlag)        //if flag then
        return false;             //return immediately after error
      retFlag = false;                      //setup return flag
              //attempt to use cfgProp default log-level string value:
      if((obj=clientConsoleLevelProp.getDefaultValue()) instanceof String &&
             ((integerObj=LogFile.levelStringToValue((String)obj)) != null))
      {       //default value fetched and converted OK; use it
        consoleLevelVal = integerObj.intValue();
      }
      else    //unable to use cfgProp default; use fallback default
        consoleLevelVal = LogFile.WARNING;
   }
    else
      consoleLevelVal = integerObj.intValue();        //save value

         //setup console output redirect for file (if filename given):
                                  //get console output file name:
    final String consoleFileName = (consoleRedirectFileNameProp != null) ?
              fromBaseDir(consoleRedirectFileNameProp.stringValue()) : null;
    if(consoleFileName != null && consoleFileName.length() > 0)
    {    //console output redirect filename was given
              //create any subdirectories needed for filename:
      FileUtils.createParentDirs(consoleFileName);
      try
      {       //setup console output file:
//        consoleRedirectStream = new PrintStream(new BufferedOutputStream(
//                             new TimestampOutputStream(new FileOutputStream(
//                                              consoleFileName,true))),true);
                   //use LogFile mechanism for console-output file so
                   // filenames have dates and old files are deleted
                   // (make sure all "console" output suppressed):
        final LogFile conLogObj = new LogFile(consoleFileName,
                               LogFile.ALL_MSGS,LogFile.NO_MSGS,false,true);
                        //enter switch-interval value from configuration:
        conLogObj.setLogFileSwitchIntervalDays(
                                    consoleFilesSwitchIntvlDays.intValue());
                        //enter maximum-age value from configuration:
        conLogObj.setMaxLogFileAge(consoleFilesMaxAgeInDays.intValue());
                        //get and enter output stream for LogFile:
        consoleRedirectStream =
                            new PrintStream(conLogObj.getLogOutputStream());
      }
      catch(Exception ex)
      {       //error opening file; show message
        final String errStr = "Error opening console redirect " +
             "output file (\"" + consoleFileName + "\") for output:  " + ex;
        System.err.println(errStr);
        enterErrorMessageString(errStr);
        consoleRedirectStream = null;       //make sure handle is null
        if(abortOnErrorFlag)      //if flag then
          return false;           //return immediately after error
        retFlag = false;                    //setup return flag
      }
      if(consoleRedirectStream != null)
      {       //output file opened OK
        consoleRedirectStream.println();    //start file with blank line
                                            //put in header message:
        consoleRedirectStream.println(
                                    "Console output redirect file opened " +
                                              UtilFns.timeMillisToString());
        if(!consoleRedirectStream.checkError())       //check for errors
        {     //no stream errors detected
          try
          {        //attempt to redirect console output streams to file:
            System.setOut(consoleRedirectStream);
            System.setErr(consoleRedirectStream);
          }
          catch(Exception ex)
          {        //error redirecting output streams; show message
            final String errStr = "Error redirecting console " +
                     "output to file (\"" + consoleFileName + "\"):  " + ex;
            System.err.println(errStr);
            enterErrorMessageString(errStr);
            consoleRedirectStream.close();       //close output stream
            consoleRedirectStream = null;        //make sure handle is null
            if(abortOnErrorFlag)       //if flag then
              return false;            //return immediately after error
            retFlag = false;                     //setup return flag
          }
        }
        else
        {     //error writing to file; show message
          final String errStr = "Error writing to redirected " +
                        "console output file (\"" + consoleFileName + "\")";
          System.err.println(errStr);
          enterErrorMessageString(errStr);
          consoleRedirectStream.close();         //close output stream
          consoleRedirectStream = null;          //clear handle
          if(abortOnErrorFlag)         //if flag then
            return false;              //return immediately after error
          retFlag = false;                       //setup return flag
        }
      }
    }
         //initialize global log file, use local TZ and add date to fnames:
    logObj = LogFile.initGlobalLogObj(
                           fromBaseDir(clientLogFileNameProp.stringValue()),
                                logFileLevelVal,consoleLevelVal,false,true);
    final int logFilesMaxAgeInDays = logFilesMaxAgeInDaysProp.intValue();
    if(logFilesMaxAgeInDays > 0)       //if given then enter max-age value
      logObj.setMaxLogFileAge(logFilesMaxAgeInDays);
    logObj.debug("  logFileLevel=" +   //show log and console output levels
          LogFile.getLevelString(logFileLevelVal) + " (" + logFileLevelVal +
              "), consoleLevel=" + LogFile.getLevelString(consoleLevelVal) +
            " (" + consoleLevelVal + "), maxAge = " + logFilesMaxAgeInDays +
                                                                   " days");
    return retFlag;
  }

  /**
   * Returns the log-file object created via the 'createLogFile()' method.
   * @return A 'LogFile' object, or null if none has been created.
   */
  public LogFile getLogFileObj()
  {
    return logObj;
  }

  /**
   * Closes the log-file object created via the 'createLogFile()' method.
   */
  public void closeLogFile()
  {
    if(logObj != null)
      logObj.close();
  }

  /**
   * Returns the console-redirect object created via the 'createLogFile()'
   * method.
   * @return A 'PrintStream' object for console output, or null if none
   * has been created.
   */
  public PrintStream getConsoleRedirectStreamObj()
  {
    return consoleRedirectStream;
  }

  /**
   * Closes the console-redirect object created via the 'createLogFile()'
   * method.
   */
  public void closeConsoleRedirectStream()
  {
    if(consoleRedirectStream != null)
      consoleRedirectStream.close();
  }

  /**
   * Sets the base-directory-path string for write-access files.
   * @param dirStr base-directory-path string for write-access files,
   * or an empty string or null for none.
   */
  public static void setBaseWriteDirStr(String dirStr)
  {
    baseWriteDirStr = (dirStr != null) ? dirStr : UtilFns.EMPTY_STRING;
  }

  /**
   * Returns the base-directory-path string for write-access files.
   * @return The base-directory-path string for write-access files,
   * or an empty string if none is setup.
   */
  public static String getBaseWriteDirStr()
  {
    return baseWriteDirStr;
  }

  /**
   * Converts the given pathname to a pathname relative to the current
   * write-access base directory.  If no write-access base directory is
   * setup or if the given pathname is absolute (or a URL) then the original
   * pathname string is returned.
   * @param pathStr the pathname string to use.
   * @param subOnEmptyFlag if true and the given pathname is null or
   * an empty string and a write-access base directory is setup then the
   * write-access base directory path is returned; if false and the given
   * pathname is null or an empty string then the original pathname
   * is returned.
   * @return The converted pathname string; or the original pathname
   * string if not converted.
   */
  public static String fromBaseDir(String pathStr, boolean subOnEmptyFlag)
  {
    if(baseWriteDirStr != null && baseWriteDirStr.trim().length() > 0 &&
                                             !UtilFns.isURLAddress(pathStr))
    {    //write-access base directory is setup and given path is not a URL
              //if given pathname is null or an empty string then
              // return write-access base dir or given pathname:
      if(pathStr == null || pathStr.trim().length() <= 0)
        return subOnEmptyFlag ? baseWriteDirStr : pathStr;
      final File pathFileObj = new File(pathStr);
      if(!pathFileObj.isAbsolute())
      {  //given path is not an absolute path
              //make given path a child of the write-access base dir
              // and return it as a pathname string:
        return (new File(baseWriteDirStr,pathStr)).getPath();
      }
    }
    return pathStr;          //return original string
  }

  /**
   * Converts the given pathname to a pathname relative to the current
   * write-access base directory.  If no write-access base directory is
   * setup or if the given pathname is absolute (or a URL) then the original
   * pathname string is returned.
   * @param pathStr the pathname string to use.
   * @return The converted pathname string; or the original pathname
   * string if not converted.
   */
  public static String fromBaseDir(String pathStr)
  {
    return fromBaseDir(pathStr,false);
  }
}
