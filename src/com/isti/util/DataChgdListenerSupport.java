//DataChgdListenerSupport.java:  Support class for managing a list of
//                               'DataChangedListener' objects.
//
//  3/24/2006 -- [ET]
//

package com.isti.util;

import java.util.ArrayList;

/**
 * Class DataChgdListenerSupport is a support class for managing a list of
 * 'DataChangedListener' objects.  The methods in this class are thread
 * safe.
 */
public class DataChgdListenerSupport
{
    /** Source object to be used when listener invoked. */
  protected final Object invokeSourceObj;
    /** List of 'DataChangedListener' objects. */
  protected ArrayList listenersList = null;

  /**
   * Creates a support object for managing a list of 'DataChangedListener'
   * objects.
   * @param sourceObj source object to be passed along when a listener
   * is invoked.
   */
  public DataChgdListenerSupport(Object sourceObj)
  {
    invokeSourceObj = sourceObj;
  }

  /**
   * Adds a listener object to the list of listeners to be invoked when
   * the 'fireListeners()' method is called.
   * @param listenerObj 'DataChangedListener' object to add.
   */
  public synchronized void addListener(DataChangedListener listenerObj)
  {
    if(listenersList == null)               //if not allocated then
      listenersList = new ArrayList();      //create list
    listenersList.add(listenerObj);         //add listener to list
  }

  /**
   * Removes a listener object from the list of listeners.
   * @param listenerObj 'DataChangedListener' object to remove.
   */
  public synchronized void removeListener(DataChangedListener listenerObj)
  {
    if(listenersList != null)
    {    //list has been allocated
      listenersList.remove(listenerObj);    //delete listener from list
      if(listenersList.size() <= 0)
        listenersList = null;          //if empty then deallocate list
    }
  }

  /**
   * Determines whether or not the given listener is contained in the list
   * of listeners.
   * @param listenerObj 'DataChangedListener' object to look for.
   * @return true if the given listener is contained in the list of
   * listeners; false if not.
   */
  public synchronized boolean containsListener(
                                            DataChangedListener listenerObj)
  {
    return (listenersList != null) ? listenersList.contains(listenerObj) :
                                                                      false;
  }

  /**
   * Invokes the listeners on the list.  The 'dataChanged()' method for
   * each listener is called.
   */
  public void fireListeners()
  {
    final ModIterator iterObj;
    synchronized(this)
    {    //grab thread-synchronization object for class instance
      if(listenersList == null || listenersList.size() <= 0)
        return;              //if no listeners then just return
              //create 'ModIterator' for list (iterates
              // over copy of list, so thread safe):
      iterObj = new ModIterator(listenersList);
    }
    Object obj;
    while(iterObj.hasNext())
    {    //for each listener in list; invoke method
      if((obj=iterObj.next()) instanceof DataChangedListener)
        ((DataChangedListener)obj).dataChanged(invokeSourceObj);
    }
  }
}
