//DelimiterSeparatedValues.java:  Contains methods for handling
//                                delimiter-separated values.
//                                The default is comma-separated values (CSV).
//
// 11/12/2007 -- [KF]  Initial version.
//

package com.isti.util;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.Writer;
import java.util.List;

public class DelimiterSeparatedValues
{
  /** The default comment text. */
  public final static String DEFAULT_COMMENT_TEXT = null;
  /** The standard comment text. */
  public final static String STANDARD_COMMENT_TEXT = "#";
  /** The default delimiter character. */
  public final static char DEFAULT_DELIMITER_CHAR = ',';
  /** The default characters that should be escaped. */
  public final static String DEFAULT_ESCAPED_CHARS = "\"";
  /** The default escape character. */
  public final static char DEFAULT_ESCAPE_CHAR = '"';
  /** The default line separator. */
  public final static String DEFAULT_LINE_SEPARATOR =
      System.getProperty("line.separator");
  /**
   * Use quotes value to specify that quotes should never be used.
   */
  public static final int USE_QUOTES_NEVER = 0;
  /**
   * Use quotes value to specify that quotes should be used as needed.
   */
  public static final int USE_QUOTES_AS_NEEDED = 1;
  /**
   * Use quotes value to specify that quotes should always be used.
   */
  public static final int USE_QUOTES_ALWAYS = 2;

  /** The comment text or null if none. */
  private final String commentText;
  /** The delimiter character. */
  private final char delimiterChar;
  /** The reserved characters that should be quoted. */
  private final String reservedChars;
  /** The characters that should be escaped. */
  private final String escapedChars;
  /** The escape character. */
  private final char escapeChar;

  /** The use quotes value. */
  private int useQuotesValue = USE_QUOTES_AS_NEEDED;
  /** The use quotes value array. */
  private int[] useQuotesValueArray = null;
  /** The line separator. */
  private String lineSeparator = DEFAULT_LINE_SEPARATOR;

  /**
   * Creates comma-separated values (CSV) without comment text.
   */
  public DelimiterSeparatedValues()
  {
    this(DEFAULT_COMMENT_TEXT, DEFAULT_DELIMITER_CHAR, DEFAULT_ESCAPED_CHARS,
         DEFAULT_ESCAPE_CHAR);
  }

  /**
   * Creates comma-separated values (CSV) with optional comment text.
   * @param commentText the text that starts comment lines or null if none.
   * @see STANDARD_COMMENT_TEXT
   */
  public DelimiterSeparatedValues(String commentText)
  {
    this(commentText, DEFAULT_DELIMITER_CHAR, DEFAULT_ESCAPED_CHARS,
         DEFAULT_ESCAPE_CHAR);
  }

  /**
   * Creates delimiter-separated values.
   * @param commentText the text that starts comment lines or null if none.
   * @param delimiterChar the delimiter character.
   * @param escapeChars the characters that should be escaped.
   *  This should always contain the escape character.
   * @param escapeChar the escape character.
   */
  public DelimiterSeparatedValues(
      String commentText, char delimiterChar, String escapeChars,
      char escapeChar)
  {
    this(commentText, delimiterChar, escapeChars, escapeChar,
         getReservedChars(delimiterChar));
  }

  /**
   * Creates delimiter-separated values.
   * @param commentText the text that starts comment lines or null if none.
   * @param delimiterChar the delimiter character.
   * @param escapedChars the characters that should be escaped.
   *  This should always contain the escape character.
   * @param escapeChar the escape character.
   * @param reservedChars the reserved characters that should be quoted.
   * @see getReservedChars
   */
  public DelimiterSeparatedValues(
      String commentText, char delimiterChar, String escapedChars,
      char escapeChar, String reservedChars)
  {
    this.commentText = commentText;
    this.delimiterChar = delimiterChar;
    this.escapedChars = escapedChars;
    this.escapeChar = escapeChar;
    this.reservedChars = reservedChars;
  }

  /**
   * Gets the line separator.
   * @return the line separator.
   */
  public String getLineSeparator()
  {
    return lineSeparator;
  }

  /**
   * Gets a quoted string where all quote characters are replaced
   * by double quote characters.
   * @param valueStr the value string or null if none.
   * @return a quoted string or null if none.
   */
  public String getQuotedText(String valueStr)
  {
    if (valueStr == null)
    {
      return null;
    }
    char currentChar;
    final StringBuffer valueBuffer = new StringBuffer(valueStr.length() + 2);
    valueBuffer.append('"');
    for (int i = 0; i < valueStr.length(); i++)
    {
      currentChar = valueStr.charAt(i);
      //if this character should be escaped
      if (escapedChars.indexOf(currentChar) >= 0)
      {
        valueBuffer.append(escapeChar); //add the escape character
      }
      valueBuffer.append(currentChar);
    }
    valueBuffer.append('"');
    //return string version of buffer
    return valueBuffer.toString();
  }

  /**
   * Gets the reserverd characters.
   * @param delimiterChar the delimiter character.
   * @return the reserverd characters.
   */
  public static String getReservedChars(char delimiterChar)
  {
    return "\n\r\"" + delimiterChar;
  }

  /**
   * Gets the use quotes value.
   * @return the use quotes value.
   * @see USE_QUOTES_NEVER, USE_QUOTES_AS_NEEDED, USE_QUOTES_ALWAYS
   */
  public int getUseQuotesValue()
  {
    return useQuotesValue;
  }

  /**
   * Gets the use quotes value for the specified column.
   * @param column the column.
   * @return the use quotes value.
   * @see USE_QUOTES_NEVER, USE_QUOTES_AS_NEEDED, USE_QUOTES_ALWAYS
   */
  public int getUseQuotesValue(int column)
  {
    if (useQuotesValueArray != null && column < useQuotesValueArray.length)
    {
      return useQuotesValueArray[column];
    }
    return getUseQuotesValue();
  }

  /**
   * Gets the value string for the value object.
   * @param valueObj the value object or null if none.
   * @return the value string or null if none.
   */
  public String getValueStr(Object valueObj)
  {
    if (valueObj == null)
    {
      return null;
    }
    return valueObj.toString();
  }

  /**
   * Read the next set of fields.
   * @param br the buffered reader.
   * @param valueBuffer the value buffer.
   * @param valueList the list of string values. This list is normally empty
   * when this method is called and is filled by this method.
   * @throws IOException if error.
   */
  public void readNext(
      BufferedReader br, List valueList, StringBuffer valueBuffer) throws
      IOException
  {
    String line;
    char currentChar = 0;
    boolean inQuotesFlag = false;

    valueBuffer.setLength(0); //clear the value buffer

    while ( (line = br.readLine()) != null) //while more lines exist
    {
      if (!inQuotesFlag) //if not in quotes
      {
        //if the line starts with the comment
        if (commentText != null && line.startsWith(commentText))
        {
          continue; //skip the line
        }
      }
      else //if in quotes
      {
        if (line.length() == 0) //if empty line
        {
          //add a line separator to the value
          valueBuffer.append(getLineSeparator());
          continue; //go to the next line
        }
      }

      int charIndex = 0;

      while (charIndex < line.length())
      {
        if (!inQuotesFlag) //if not in quotes
        {
          //check for quote
          if (charIndex < line.length())
          {
            currentChar = line.charAt(charIndex);
            if (currentChar == '"')
            {
              charIndex++; //skip quote character
              inQuotesFlag = true;
            }
          }
        }

        if (inQuotesFlag) //if in quotes
        {
          boolean escapeFoundFlag = false;
          boolean quoteFoundFlag = false;

          //look for ending quote
          for (; charIndex < line.length(); charIndex++)
          {
            currentChar = line.charAt(charIndex);
            if (escapeChar == '"') //if the escape char is a quote
            {
              if (currentChar == '"') //if quote character
              {
                if (quoteFoundFlag) //if quote was already found
                {
                  quoteFoundFlag = false; //clear the flag
                }
                else
                {
                  quoteFoundFlag = true; //quote was found
                  continue; //skip quote character
                }
              }
              else //if not quote character
              {
                if (quoteFoundFlag) //if quote was found
                {
                  break; //we are at ending quote
                }
              }
            }
            else //the escape char is not a quote
            {
              if (!escapeFoundFlag) //if previous char was not the escape char
              {
                if (currentChar == escapeChar) //if this is the escape char
                {
                  escapeFoundFlag = true;
                  continue; //skip the escape char
                }
                if (currentChar == '"') //if quote character
                {
                  quoteFoundFlag = true; //quote was found
                  charIndex++; //skip the quote character
                  break; //we are at ending quote
                }
              }
              else //previous char was the escape char
              {
                escapeFoundFlag = false;
              }
            }

            valueBuffer.append(currentChar); //add the character to the value
          }

          if (quoteFoundFlag) //if quote was found
          {
            inQuotesFlag = false; //no longer in quotes
          }
          else //quote was not found
          {
            //add a line separator to the value
            valueBuffer.append(getLineSeparator());
          }
        }

        if (!inQuotesFlag) //if not in quotes
        {
          boolean lastCharDelimiterFlag = false;

          for (; charIndex < line.length(); charIndex++)
          {
            currentChar = line.charAt(charIndex);
            if (currentChar == delimiterChar)
            {
              lastCharDelimiterFlag = true;
              charIndex++; //skip the delimiter
              break;
            }
            lastCharDelimiterFlag = false;
            valueBuffer.append(currentChar); //add the character to the value
          }
          addValue(valueList, valueBuffer);

          if (charIndex >= line.length()) //if no more characters
          {
            //if the last character was a delimiter
            if (lastCharDelimiterFlag)
            {
              addValue(valueList, valueBuffer); //add the empty value
            }
          }
        }
      }

      if (!inQuotesFlag) //done if not in quotes
      {
        break;
      }
    }
  }

  /**
   * Sets the line separator.
   * @param lineSeparator the line separator.
   */
  public void setLineSeparator(String lineSeparator)
  {
    this.lineSeparator = lineSeparator;
  }

  /**
   * Sets the use quotes value.
   * The use quotes value is normally USE_QUOTES_AS_NEEDED.
   * If quotes are never used (USE_QUOTES_NEVER), the values should not
   * contain any reserved characters, which are normally '\n', '\r', '"', and
   * the delimiter character (the default delimiter character is ','.)
   * @param v the use quotes value.
   * @see USE_QUOTES_NEVER, USE_QUOTES_AS_NEEDED, USE_QUOTES_ALWAYS
   */
  public void setUseQuotesValue(int v)
  {
    useQuotesValue = v;
  }

  /**
   * Sets the use quotes value array. This array is indexed by the column.
   * The use quotes value is normally USE_QUOTES_AS_NEEDED.
   * If quotes are never used (USE_QUOTES_NEVER), the values should not
   * contain any reserved characters, which are normally '\n', '\r', '"', and
   * the delimiter character (the default delimiter character is ','.)
   * @param v the use quotes value array or null if none.
   * @see USE_QUOTES_NEVER, USE_QUOTES_AS_NEEDED, USE_QUOTES_ALWAYS
   */
  public void setUseQuotesValue(int[] v)
  {
    useQuotesValueArray = v;
  }

  /**
   * Determines if the value should be quoted.
   * @param valueStr the value string or null if none.
   * @param column the column where 0 is the first column.
   * @return true if the value should be quoted, false otherwise.
   */
  public boolean shouldBeQuoted(String valueStr, int column)
  {
    if (valueStr == null)
    {
      return false;
    }
    if (column == 0 && commentText != null &&
        valueStr.startsWith(commentText))
    {
      return true;
    }
    char currentChar;
    for (int i = 0; i < valueStr.length(); i++)
    {
      currentChar = valueStr.charAt(i);
      if (reservedChars.indexOf(currentChar) >= 0)
      {
        return true;
      }
    }
    return false;
  }

  /**
   * Write the values.
   * @param w the writer.
   * @param valueList the list of values.
   * @throws IOException if error.
   */
  public void writeNext(Writer w, List valueList) throws IOException
  {
    if (valueList != null && valueList.size() != 0)
    {
      String valueStr;
      int columnUseQuotesValue;
      for (int column = 0; column < valueList.size(); column++)
      {
        if (column > 0)
        {
          w.write(delimiterChar);
        }
        valueStr = getValueStr(valueList.get(column));
        if (valueStr != null)
        {
          columnUseQuotesValue = getUseQuotesValue(column);
          if (columnUseQuotesValue == USE_QUOTES_ALWAYS ||
              (columnUseQuotesValue == USE_QUOTES_AS_NEEDED &&
               shouldBeQuoted(valueStr, column)))
          {
            valueStr = getQuotedText(valueStr);
          }
          w.write(valueStr);
        }
      }
      w.write(getLineSeparator()); //write the line separator
    }
  }

  /**
   * Add the value to the value list.
   * @param valueList the value list.
   * @param valueBuffer the value buffer.
   */
  private static void addValue(List valueList, StringBuffer valueBuffer)
  {
    valueList.add(valueBuffer.toString()); //add the value to the list
    valueBuffer.setLength(0); //clear the value buffer
  }

  private final static String TEST_INPUT =
      "# comment\n,\"aaa \n\n\"\"a\", bbb ,cc c,\"ddd\",eee\"e,\n" +
      "# comment\n,\"fff \n\n\"\"f\", ggg ,hh h,\"iii\",jjj\"j,\n";

  public static void main(String[] args)
  {
    boolean printValuesFlag = true;
    String inputFileName = null;
    String outputFileName = null;
    if (args.length > 0)
    {
      inputFileName = args[0];
      printValuesFlag = false;
      if (args.length > 1)
      {
        outputFileName = args[1];
      }
    }
    final java.io.Reader reader;
    if (inputFileName != null)
    {
      try
      {
        reader = new java.io.FileReader(inputFileName);
      }
      catch (Exception ex)
      {
        System.err.println("Could not open input file: " + ex);
        return;
      }
    }
    else
    {
      System.out.println(TEST_INPUT);
      reader = new java.io.StringReader(TEST_INPUT);
    }
    BufferedReader br = new BufferedReader(reader);
    DelimiterSeparatedValues input = new DelimiterSeparatedValues(
//        DEFAULT_COMMENT_TEXT, DEFAULT_DELIMITER_CHAR, DEFAULT_ESCAPED_CHARS,
//         DEFAULT_ESCAPE_CHAR);
        STANDARD_COMMENT_TEXT, DEFAULT_DELIMITER_CHAR, DEFAULT_ESCAPED_CHARS,
        DEFAULT_ESCAPE_CHAR);
    DelimiterSeparatedValues output = new DelimiterSeparatedValues(
//        DEFAULT_COMMENT_TEXT, DEFAULT_DELIMITER_CHAR, DEFAULT_ESCAPED_CHARS,
//         DEFAULT_ESCAPE_CHAR);
        DEFAULT_COMMENT_TEXT, DEFAULT_DELIMITER_CHAR, DEFAULT_ESCAPED_CHARS,
        '\\');
    Writer w = null;
    if (outputFileName != null)
    {
      try
      {
        w = new java.io.FileWriter(outputFileName);
      }
      catch (Exception ex)
      {
        System.err.println("Could not open output file: " + ex);
        return;
      }
    }

    try
    {
      StringBuffer valueBuffer = new StringBuffer();
      //list of string values
      final List valueList = new java.util.Vector();
      int count = 0;
      for (; ; )
      {
        valueList.clear();
        input.readNext(br, valueList, valueBuffer);
        if (valueList.size() == 0)
        {
          break;
        }
        ++count;
        for (int i = 0; i < valueList.size(); i++)
        {
          if (printValuesFlag)
          {
            System.out.println(
                count + "[" + i + "]: \"" + valueList.get(i) + "\"");
          }
        }
        if (w != null)
        {
          output.writeNext(w, valueList);
        }
      }
      System.out.println("Found " + count + " lines");
    }
    catch (Exception ex)
    {

    }
    if (w != null)
    {
      try
      {
        w.close();
      }
      catch (Exception ex)
      {
        System.err.println("Error closing output file: " + ex);
      }
    }
    if (br != null)
    {
      try
      {
        br.close();
      }
      catch (Exception ex)
      {
        System.err.println("Error closing input file: " + ex);
      }
    }
  }
}
