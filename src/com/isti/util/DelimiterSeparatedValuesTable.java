//DelimiterSeparatedValuesTable.java:  Contains methods for handling
//                                     delimiter-separated values in a table.
//
// 11/15/2007 -- [KF]  Initial version.
//

package com.isti.util;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.Writer;
import java.util.List;

public class DelimiterSeparatedValuesTable
    extends BaseValueTableModel implements ValueTableModel
{
  /**
   * Reads all of the rows.
   * This will attempt to load all of the rows into memory.
   * Any previous rows or error messages are cleared.
   * @param input the input DelimiterSeparatedValues.
   * @param br the BufferedReader.
   * @param headerFlag true if the first row is a header, false otherwise.
   * @return the number of rows read or 0 if error.
   * @throws IOException if I/O error.
   * @see getErrorMessages
   */
  public int readAll(DelimiterSeparatedValues input, BufferedReader br,
                     boolean headerFlag) throws IOException
  {
    int numColumns = 0;
    int count = 0;
    createLists(); //create new lists
    clearErrorMessageString(); //clear previous error message
    final StringBuffer valueBuffer = new StringBuffer();
    //list of string values
    final List valueList = createList();
    for (; ; )
    {
      valueList.clear();
      input.readNext(br, valueList, valueBuffer);
      if (valueList.size() == 0)
      {
        break;
      }
      if (count == 0) //if first row
      {
        numColumns = valueList.size();
        if (headerFlag) //if header is used
        {
          setColumnNames(valueList);
        }
        else //header is not used
        {
          setColumnNames(numColumns); //use the default column names
          addRow(valueList);
        }
      }
      else if (numColumns != valueList.size())
      {
        appendErrorMessageString(
            "Row " + count + " has the wrong number of columns (" +
            valueList.size() + " but should be " + numColumns + ")\n");
      }
      else
      {
        addRow(valueList);
      }
      ++count;
    }
    if (getErrorMessageFlag()) //if any errors
    {
      return 0; //indicate that there is an error
    }
    return count;
  }

  /**
   * Writes all the rows.
   * @param output the output DelimiterSeparatedValues.
   * @param w the writer.
   * @param headerFlag true if the first row is a header, false otherwise.
   * @throws IOException if I/O error.
   */
  public void writeAll(
      DelimiterSeparatedValues output, Writer w, boolean headerFlag) throws
      IOException
  {
    if (headerFlag)
    {
      output.writeNext(w, getColumnNames());
    }
    for (int rowIndex = 0; rowIndex < getRowCount(); rowIndex++)
    {
      output.writeNext(w, getRow(rowIndex));
    }
  }

  private final static String TEST_INPUT =
      "# comment\n,\"aaa \n\n\"\"a\", bbb ,cc c,\"ddd\",eee\"e,empty\n" +
      "# comment\n,\"fff \n\n\"\"f\", ggg ,hh h,\"iii\",jjj\"j,\n";

  public static void main(String[] args)
  {
    final DelimiterSeparatedValuesTable dsvt =
        new DelimiterSeparatedValuesTable();
    String inputFileName = null;
    String outputFileName = null;
    if (args.length > 0)
    {
      inputFileName = args[0];
      if (args.length > 1)
      {
        outputFileName = args[1];
      }
    }
    final java.io.Reader reader;
    if (inputFileName != null)
    {
      try
      {
        reader = new java.io.FileReader(inputFileName);
      }
      catch (Exception ex)
      {
        System.err.println("Could not open input file: " + ex);
        return;
      }
    }
    else
    {
      System.out.println(TEST_INPUT);
      reader = new java.io.StringReader(TEST_INPUT);
    }
    BufferedReader br = new BufferedReader(reader);
    DelimiterSeparatedValues input = new DelimiterSeparatedValues(
        DelimiterSeparatedValues.STANDARD_COMMENT_TEXT);
    DelimiterSeparatedValues output = new DelimiterSeparatedValues(
//      DelimiterSeparatedValues.DEFAULT_COMMENT_TEXT,
//      DelimiterSeparatedValues.DEFAULT_DELIMITER_CHAR,
//      DelimiterSeparatedValues.DEFAULT_ESCAPED_CHARS,
//      DelimiterSeparatedValues.DEFAULT_ESCAPE_CHAR);
        DelimiterSeparatedValues.DEFAULT_COMMENT_TEXT,
        DelimiterSeparatedValues.DEFAULT_DELIMITER_CHAR,
        DelimiterSeparatedValues.DEFAULT_ESCAPED_CHARS,
        '\\');
    Writer w = null;
    if (outputFileName != null)
    {
      try
      {
        w = new java.io.FileWriter(outputFileName);
      }
      catch (Exception ex)
      {
        System.err.println("Could not open output file: " + ex);
        return;
      }
    }

    try
    {
      String errorMessage;
      int count = dsvt.readAll(input, br, true);
      if (count == 0)
      {
        errorMessage = dsvt.getErrorMessageString();
        if (errorMessage.length() > 0)
        {
          dsvt.clearErrorMessageString();
          System.err.println("Error reading input:\n" + errorMessage);
        }
      }
      else
      {
        System.out.println("Found " + count + " lines");
        if (w != null)
        {
          dsvt.writeAll(output, w, true);
        }
      }
    }
    catch (Exception ex)
    {
      System.err.println(ex);
      ex.printStackTrace();
    }

    if (w != null)
    {
      try
      {
        w.close();
      }
      catch (Exception ex)
      {
        System.err.println("Error closing output file: " + ex);
      }
    }
    if (br != null)
    {
      try
      {
        br.close();
      }
      catch (Exception ex)
      {
        System.err.println("Error closing input file: " + ex);
      }
    }
  }
}
