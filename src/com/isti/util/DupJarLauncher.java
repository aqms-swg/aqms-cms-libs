//DupJarLauncher.java:  Launcher for a "secondary" program to be run out
//                      of a jar duplicated from an original jar.
//
//  2/16/2005 -- [ET]  Initial version.
// 10/27/2008 -- [ET]  Added optional 'progSingletonDirStr' parameter to
//                     'launch()' method; modified to use original program
//                     jar if copy fails.
//  3/18/2019 -- [KF]  Added optional 'javaCmd' parameter to 'launch()' method;
//                     added 'getJavaCmd()' method; modified to not copy the jar
//                     if the parent is not writable.
//

package com.isti.util;

import java.io.File;

/**
 * Class DupJarLauncher is a launcher for a "secondary" program to be run
 * out of a jar duplicated from an original jar.
 */
public class DupJarLauncher
{
  private static final String SH_CMD = "/bin/sh";
  private static final String JAVA_CMD_MAC = "./java";
  private static final String JAVA_CMD_DEFAULT = "java";
  private static final String JAVA_CMD_SH_MAC = SH_CMD + " java";
  private static final String JAVA_CMD_WIN = "java.bat";
  private static final String MSG_PROMPT = "DupJarLauncher:  ";

    //private constructor so that no object instances may be created
    // (static access only)
  private DupJarLauncher()
  {
  }

  /**
   * Get the Java command.
   * @return the Java command.
   */
  public static String getJavaCmd()
  {
    File file;
    String cmdStr = JAVA_CMD_DEFAULT;
    switch (UtilFns.getOsType())
    {
    case MAC:
      file = new File(JAVA_CMD_MAC);
      if (file.canExecute())
      {
        cmdStr = JAVA_CMD_MAC;
      }
      else if (file.canRead() && new File(SH_CMD).canExecute())
      {
        cmdStr = JAVA_CMD_SH_MAC;
      }
      file = null;
      break;
    case WINDOWS:
      if (new File(JAVA_CMD_WIN).canRead())
      {
        cmdStr = JAVA_CMD_WIN;
      }
      break;
    default:
      break;
    }
	  return cmdStr;
  }

  /**
   * Launches the specified program out of a jar duplicated from an
   * original jar.  If the duplicate jar does not exist or is older than
   * the original jar then the original jar is copied over.  The program
   * will be run via a system-exec of the following command:
   *   java jvmParamsStr -cp dupJarFNameStr progParamsStr
   * @param origJarFNameStr name or original jar file.
   * @param dupJarFNameStr name or duplicate jar file.
   * @param progClassNameStr name of program class containing 'main'
   * method to be run.
   * @param progSingletonNameStr name associated with program-singleton
   * file to be check to see if program is already running, or null for
   * none.
   * @param jvmParamsStr JVM parameters for 'java' command, or null for
   * none.
   * @param progParamsStr program parameters for 'java' command, or null for
   * none.
   * @param logObj log file to use, or null for none.
   * @param progSingletonDirStr directory for program-singleton file, or
   * null or empty string for default directory.
   * @param javaCmd the java command or null for the default.
   * @return null if successful; an error message if not.
   * @see #getJavaCmd()
   */
  public static String launch(String origJarFNameStr, String dupJarFNameStr,
                       String progClassNameStr, String progSingletonNameStr,
                  String jvmParamsStr, String progParamsStr, LogFile logObj,
                  String progSingletonDirStr, String javaCmd)
  {
	if (javaCmd == null)
	{
		javaCmd = JAVA_CMD_DEFAULT;
	}
    try
    {
      if(logObj == null)     //if no log file given then use dummy
        logObj = LogFile.getNullLogObj();
         //create program-singleton for checking if program aleady running:
      final ProgramSingleton progSingletonObj = (progSingletonDirStr != null
                               && progSingletonDirStr.trim().length() > 0) ?
            new ProgramSingleton(progSingletonDirStr,progSingletonNameStr) :
                                 new ProgramSingleton(progSingletonNameStr);
         //check if program is already running:
      if(progSingletonNameStr != null && progSingletonNameStr.length() > 0 &&
                                               progSingletonObj.isRunning())
      {  //program-single name given and active singleton file detected
        logObj.warning(MSG_PROMPT + "Error launching program class (\"" +
                        progClassNameStr + "\"):  Program already running");
        return "Program is already running";
      }
              //set launch jar name to be duplicate jar name:
      String launchJarFNameStr = dupJarFNameStr;
      try
      {
        final File origJarFileObj = new File(origJarFNameStr);
        final File dupJarFileObj = new File(dupJarFNameStr);
        if(dupJarFileObj.exists())
        {  //duplicate jar exists
          if(origJarFileObj.exists())
          {  //original jar exists; check last-modified times
            if(origJarFileObj.lastModified() > dupJarFileObj.lastModified())
            {  //original jar is newer; copy to duplicate
              logObj.debug(MSG_PROMPT + "Copying updated \"" +
                        origJarFileObj + "\" to \"" + dupJarFileObj + "\"");
              FileUtils.copyFile(origJarFileObj,dupJarFileObj);
            }
          }
          else
          {  //original jar does not exist; log debug message
            logObj.debug(MSG_PROMPT + "Original jar not found (\"" +
                                                   origJarFNameStr + "\")");
          }
        }
        else
        {  //duplicate jar does not exist
        	if(origJarFileObj.exists()) //original jar exists
        	{
        		File parent = dupJarFileObj.getAbsoluteFile().getParentFile();
        		// if parent exists and is not writable
        		if (parent != null && !parent.canWrite())
        		{
        			launchJarFNameStr = origJarFNameStr;   //change to original jar
        		}
        		else
        		{
        			// copy to duplicate
        			logObj.debug(MSG_PROMPT + "Copying \"" + origJarFileObj +
        					"\" to \"" + dupJarFileObj + "\"");
        			FileUtils.copyFile(origJarFileObj,dupJarFileObj);
        			if(!dupJarFileObj.exists())
        			{  //copied program jar not found
        				logObj.warning(MSG_PROMPT +
        						"Unable to find copied program jar file; using original jar");
        				launchJarFNameStr = origJarFNameStr;   //change to original jar
        			}
        		}
        	}
        	else
        	{  //original jar does not exist; log warning message
        		logObj.warning(MSG_PROMPT + "Error launching program class (\"" +
        				progClassNameStr + "\"):  Original jar not found (\"" +
        				origJarFNameStr + "\")");
        		return "Error launching program:  jar file (\"" +
        		origJarFNameStr + "\") not found";
        	}
        }
      }
      catch(Exception ex)
      {  //error copying program jar file
        logObj.warning(MSG_PROMPT +
              "Error copying program jar file; using original jar:  " + ex);
        launchJarFNameStr = origJarFNameStr;     //change to original jar
      }
         //create command string for launching program:
      final String cmdStr = javaCmd +
                      ((jvmParamsStr != null && jvmParamsStr.length() > 0) ?
                                (" "+jvmParamsStr) : UtilFns.EMPTY_STRING) +
                      " -cp " + launchJarFNameStr + " " + progClassNameStr +
                    ((progParamsStr != null && progParamsStr.length() > 0) ?
                                (" "+progParamsStr) : UtilFns.EMPTY_STRING);
      logObj.debug(MSG_PROMPT + "Executing command:  " + cmdStr);
      Runtime.getRuntime().exec(cmdStr);         //execute command
      return null;
    }
    catch(Exception ex)
    {    //some kind of exception error; log it
      logObj.warning(MSG_PROMPT + "Error launching program class (\"" +
                                          progClassNameStr + "\"):  " + ex);
      logObj.warning(UtilFns.getStackTraceString(ex));
      return "Error launching program:  " + ex;
    }
  }

  /**
   * Launches the specified program out of a jar duplicated from an
   * original jar.  If the duplicate jar does not exist or is older than
   * the original jar then the original jar is copied over.  The program
   * will be run via a system-exec of the following command:
   *   java jvmParamsStr -cp dupJarFNameStr progParamsStr
   * @param origJarFNameStr name or original jar file.
   * @param dupJarFNameStr name or duplicate jar file.
   * @param progClassNameStr name of program class containing 'main'
   * method to be run.
   * @param progSingletonNameStr name associated with program-singleton
   * file to be check to see if program is already running, or null for
   * none.
   * @param jvmParamsStr JVM parameters for 'java' command, or null for
   * none.
   * @param progParamsStr program parameters for 'java' command, or null for
   * none.
   * @param logObj log file to use, or null for none.
   * @param progSingletonDirStr directory for program-singleton file, or
   * null or empty string for default directory.
   * @return null if successful; an error message if not.
   */
  public static String launch(String origJarFNameStr, String dupJarFNameStr,
                       String progClassNameStr, String progSingletonNameStr,
                  String jvmParamsStr, String progParamsStr, LogFile logObj,
                                                 String progSingletonDirStr)
  {
	    return launch(origJarFNameStr,dupJarFNameStr,progClassNameStr,
	               progSingletonNameStr,jvmParamsStr,progParamsStr,logObj,
	               progSingletonDirStr,null);
  }

  /**
   * Launches the specified program out of a jar duplicated from an
   * original jar.  If the duplicate jar does not exist or is older than
   * the original jar then the original jar is copied over.  The program
   * will be run via a system-exec of the following command:
   *   java jvmParamsStr -cp dupJarFNameStr progParamsStr
   * @param origJarFNameStr name or original jar file.
   * @param dupJarFNameStr name or duplicate jar file.
   * @param progClassNameStr name of program class containing 'main'
   * method to be run.
   * @param progSingletonNameStr name associated with program-singleton
   * file to be check to see if program is already running, or null for
   * none.
   * @param jvmParamsStr JVM parameters for 'java' command, or null for
   * none.
   * @param progParamsStr program parameters for 'java' command, or null for
   * none.
   * @param logObj log file to use, or null for none.
   * @return null if successful; an error message if not.
   */
  public static String launch(String origJarFNameStr, String dupJarFNameStr,
                       String progClassNameStr, String progSingletonNameStr,
                  String jvmParamsStr, String progParamsStr, LogFile logObj)
  {
    return launch(origJarFNameStr,dupJarFNameStr,progClassNameStr,
               progSingletonNameStr,jvmParamsStr,progParamsStr,logObj,
               null,null);
  }
}
