//EndianUtilFns:  Contains various static endian utility functions
//
//  1/23/2003 -- [KF]
//

package com.isti.util;

public class EndianUtilFns
{
  //protected constructor so that no object instances may be created
  // (static access only)
  protected EndianUtilFns()
  {
  }

  /**
   * Gets a little-endian 16 bit short.
   * @param buf data buffer.
   * @return the 16 bit short.
   */
  public static short getLeShort(byte[] buf)
  {
    return (short) (((buf[1] & 0xFF) << 8) +
                    ((buf[0] & 0xFF) << 0));
  }

  /**
   * Gets a little-endian unsigned 16 bit short.
   * @param buf data buffer.
   * @return the 16 bit short.
   */
  public static int getLeUnsignedShort(byte[] buf)
  {
    return ((buf[1] & 0xFF) << 8) +
        ((buf[0] & 0xFF) << 0);
  }

  /**
   * Gets a little-endian 16 bit char.
   * @param buf data buffer.
   * @return the 16 bit char.
   * @exception EOFException If end of file is reached.
   * @exception IOException If other I/O error has occurred.
   */
  public static char getLeChar(byte[] buf)
  {
    return (char) (((buf[1] & 0xFF) << 8) +
                   ((buf[0] & 0xFF) << 0));
  }

  /**
   * Gets a little-endian 32 bit int.
   * @param buf data buffer.
   * @return the 32 bit integer.
   */
  public static int getLeInt(byte[] buf)
  {
    return ((buf[3] & 0xFF) << 24) +
        ((buf[2] & 0xFF) << 16) +
        ((buf[1] & 0xFF) << 8) +
        ((buf[0] & 0xFF) << 0);
  }

  /**
   * Gets a little-endian 64 bit long.
   * @param buf data buffer.
   * @return the 64 bit long.
   */
  public static long getLeLong(byte[] buf)
  {
    return ((buf[7] & 0xFFL) << 56) +
        ((buf[6] & 0xFFL) << 48) +
        ((buf[5] & 0xFFL) << 40) +
        ((buf[4] & 0xFFL) << 32) +
        ((buf[3] & 0xFFL) << 24) +
        ((buf[2] & 0xFFL) << 16) +
        ((buf[1] & 0xFFL) << 8) +
        ((buf[0] & 0xFFL) << 0);
  }

  /**
   * Gets a little-endian 32 bit float.
   * @param buf data buffer.
   * @return the 32 bit float.
   */
  public static float getLeFloat(byte[] buf)
  {
    return Float.intBitsToFloat(getLeInt(buf));
  }

  /**
   * Gets a little-endian 64 bit double.
   * @param buf data buffer.
   * @return the 64 bit double.
   */
  public static double getLeDouble(byte[] buf)
  {
    return Double.longBitsToDouble(getLeLong(buf));
  }
}