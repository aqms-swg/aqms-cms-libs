//ErrorMessageMgr.java:  Manages an error message string.
//
//   8/5/2003 -- [ET]  Initial version.
//  3/12/2004 -- [ET]  Added 'unfetchedMessageFlag' and related
//                     functionality.
//  8/31/2005 -- [ET]  Added "implements ErrorMsgMgrIntf".
//

package com.isti.util;

/**
 * Class ErrorMessageMgr manages an error message string.
 */
public class ErrorMessageMgr implements ErrorMsgMgrIntf
{
         //string for error message:
  private String errorMessageString = null;
         //flag set true when error message set but not yet fetched:
  private boolean unfetchedMessageFlag = false;

  /**
   * Returns the status of the error message.
   * @return true if an error message is set; false if not.
   */
  public synchronized boolean getErrorMessageFlag()
  {
    return (errorMessageString != null);
  }

  /**
   * Returns the current error message (if any).
   * @return The current error message, or null if no errors have occurred.
   */
  public synchronized String getErrorMessageString()
  {
    unfetchedMessageFlag = false;      //clear unfetched-message flag
    return errorMessageString;
  }

  /**
   * Clears any current error message.
   */
  public synchronized void clearErrorMessageString()
  {
    unfetchedMessageFlag = false;      //clear unfetched-message flag
    errorMessageString = null;         //clear error message
  }

  /**
   * Clears the current error message only if it has been fetched (via
   * the 'getErrorMessageString()' method).
   */
  public synchronized void clearFetchedErrorMessage()
  {
    if(!unfetchedMessageFlag)
      errorMessageString = null;         //clear error message
  }

  /**
   * Returns the "unfetched" status of the error message.
   * @return true if an error message is set and it has not been fetched
   * (via the 'getErrorMessageString()' method); false if not.
   */
  public synchronized boolean getUnfetchedMessageFlag()
  {
    return unfetchedMessageFlag;
  }

  /**
   * Sets the error message.  Any previously set error message is
   * overwritten.
   * @param str the error message text to set.
   */
  protected synchronized void setErrorMessageString(String str)
  {
         //set error message; set unfetched-message flag if given
         // error message if not null; otherwise clear flag:
    unfetchedMessageFlag = ((errorMessageString=str) != null);
  }

  /**
   * Enters the error message (if the error message is currently clear).
   * @param str the error message text to set.
   */
  protected synchronized void enterErrorMessageString(String str)
  {
    if(!unfetchedMessageFlag || errorMessageString == null)
    {    //message has been fetched or error message is null
              //set error message; set unfetched-message flag if given
              // error message if not null; otherwise clear flag:
      unfetchedMessageFlag = ((errorMessageString=str) != null);
    }
  }
}
