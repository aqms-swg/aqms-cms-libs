//ErrorMsgMgrIntf.java:  Interface for methods implemented in
//                       'ErrorMessageMgr' class.
//
//  8/31/2005 -- [ET]
//

package com.isti.util;

/**
 * Interface ErrorMsgMgrIntf specifies methods implemented in
 * 'ErrorMessageMgr' class.
 */
public interface ErrorMsgMgrIntf
{

  /**
   * Returns the status of the error message.
   * @return true if an error message is set; false if not.
   */
  public boolean getErrorMessageFlag();

  /**
   * Returns the current error message (if any).
   * @return The current error message, or null if no errors have occurred.
   */
  public String getErrorMessageString();

  /**
   * Clears any current error message.
   */
  public void clearErrorMessageString();

  /**
   * Clears the current error message only if it has been fetched (via
   * the 'getErrorMessageString()' method).
   */
  public void clearFetchedErrorMessage();

  /**
   * Returns the "unfetched" status of the error message.
   * @return true if an error message is set and it has not been fetched
   * (via the 'getErrorMessageString()' method); false if not.
   */
  public boolean getUnfetchedMessageFlag();
}
