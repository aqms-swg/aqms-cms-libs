package com.isti.util;

import java.util.Vector;
import java.util.Date;
import java.io.IOException;

/**
 * Class ExtendedArchiveManager extends an archive manager to support:
 * optional queuing of archive items.
 * optional purging of archive items.
 *  9/30/2003 -- [KF]  Initial version.
 * 10/03/2003 -- [KF]  Extend 'IstiThread' instead of 'Thread'.
 * 11/06/2003 -- [KF]  Log stack trace for IOException.
 * 01/02/2004 -- [KF]  Added polling option that may be used instead of thread
 *                     interrupt.
 *  3/31/2004 -- [ET]  Changed reference to 'saveItemArchiveDate()' to
 *                     'checkItemArchiveDate()'.
 *  7/12/2005 -- [ET]  Modified to make 'AbstractWorkerThread' class
 *                     extend 'IstiNotifyThread' to avoid use of thread
 *                     'interrupt()' calls that can be problematic under
 *                     Solaris OS; made worker-thread classes into static
 *                     member classes of main class.
 *  3/21/2007 -- [ET]  Added 'terminateWorkerThreads()' method.
 *  4/28/2010 -- [ET]  Added optional 'strictArchiveDateFlag' parameter
 *                     to constructor.
 */
public class ExtendedArchiveManager extends ArchiveManager
{
    /** The archive item queue vector. */
  protected final Vector archiveItemQueueVec = new Vector();
    /** Set 'true' if items must be in ascending date/time order. */
  protected final boolean strictArchiveDateFlag;
    //worker threads:
  protected static final Object archiveItemWTSyncObj = new Object();
  protected static ArchiveItemWorkerThread archiveItemWTObj = null;
  protected static final Object purgeWTSyncObj = new Object();
  protected static PurgeWorkerThread purgeWTObj = null;

  private boolean archiveItemQueueEnabled = false;
  private long archivePurgeAgeMilliSecs = 0;
  private long archivePollTime = 0;

  /**
   * Creates a new instance of ExtendedArchiveManager to archive instances of
   * the given class.  The given class must have a constructor with
   * 'String' and 'Archivable.Marker' parameters, like this:
   * <br>
   * public ArchivableImpl(String dataStr, Archivable.Marker mkrObj) ...
   * <br>
   * This constructor is used to create the class from a string of data.
   * The 'Archivable.Marker' is used to mark the constructor as being
   * available for de-archiving (and will usually be 'null').
   * @param classObj class object representing the class to be archived.
   * @param archiveRootDirName name of root directory in which to place
   * archive files (and possibly directories), or null to use the
   * current working directory.
   * @param archiveFileNameStr name to use for the archive file(s).
   * @param strictArchiveDateFlag true if items must be in ascending
   * date/time order; false if not.
   * @throws NoSuchMethodException if a proper constructor does not exist
   * for the class.
   * @throws  NullPointerException
   *          If <code>archiveFileNameStr</code> is <code>null</code>
   */
  public ExtendedArchiveManager(Class classObj, String archiveRootDirName,
                   String archiveFileNameStr, boolean strictArchiveDateFlag)
                                                throws NoSuchMethodException
  {
    super(classObj,archiveRootDirName,archiveFileNameStr);
    this.strictArchiveDateFlag = strictArchiveDateFlag;
    setLogPrefixString("ExtendedArchiveManager");  //set the log prefix string
  }

  /**
   * Creates a new instance of ExtendedArchiveManager to archive instances of
   * the given class.  The given class must have a constructor with
   * 'String' and 'Archivable.Marker' parameters, like this:
   * <br>
   * public ArchivableImpl(String dataStr, Archivable.Marker mkrObj) ...
   * <br>
   * This constructor is used to create the class from a string of data.
   * The 'Archivable.Marker' is used to mark the constructor as being
   * available for de-archiving (and will usually be 'null').
   * Archived items must be added in ascending date/time order.
   * @param classObj class object representing the class to be archived.
   * @param archiveRootDirName name of root directory in which to place
   * archive files (and possibly directories), or null to use the
   * current working directory.
   * @param archiveFileNameStr name to use for the archive file(s).
   * @throws NoSuchMethodException if a proper constructor does not exist
   * for the class.
   * @throws  NullPointerException
   *          If <code>archiveFileNameStr</code> is <code>null</code>
   */
  public ExtendedArchiveManager(Class classObj, String archiveRootDirName,
                     String archiveFileNameStr) throws NoSuchMethodException
  {
    this(classObj,archiveRootDirName,archiveFileNameStr,true);
  }

  /**
   * Creates a new instance of ExtendedArchiveManager to archive instances of
   * the given class.  The given class must have a constructor with
   * 'String' and 'Archivable.Marker' parameters, like this:
   * <br>
   * public ArchivableImpl(String dataStr, Archivable.Marker mkrObj) ...
   * <br>
   * This constructor is used to create the class from a string of data.
   * The 'Archivable.Marker' is used to mark the constructor as being
   * available for de-archiving (and will usually be 'null').  The
   * archive files will be placed in the current working directory.
   * Archived items must be added in ascending date/time order.
   * @param classObj class object representing the class to be archived.
   * @param archiveFileNameStr name to use for the archive file(s).
   * @throws NoSuchMethodException if a proper constructor does not exist
   * for the class.
   * @throws  NullPointerException
   *          If <code>archiveFileNameStr</code> is <code>null</code>
   */
  public ExtendedArchiveManager(Class classObj,String archiveFileNameStr)
                                                throws NoSuchMethodException
  {
    this(classObj,null,archiveFileNameStr,true);
  }

  /**
   * Inserts an item into the archive.
   * @param item the item to be archived.
   * @throws IOException if the archive file can not be written to.
   * @return true if the item was added, false otherwise.
   */
  public boolean archiveItem(Archivable item) throws IOException
  {
    if(!archiveItemQueueEnabled)
      return super.archiveItem(item,strictArchiveDateFlag);

    if(archiveClosedFlag)         //if archived closed then
      return false;               //abort method

    if(strictArchiveDateFlag)
    {  //items must be in ascending date/time order
      synchronized(archiveAccessSyncObj)
      {  //only allow one thread access to the archive at a time
        if(!checkItemArchiveDate(item))   //if item date not valid then
          return false;                   //abort without adding
      }
    }

    synchronized(archiveItemQueueVec)
    {  //grab thread-synchronization lock for queue Vector
         //add item to queue to be processed by worker thread:
      archiveItemQueueVec.add(item);
    }
    //signal the worker-thread to process message:
    signalWorkerThread(archiveItemWTObj);
    return true;
  }

  /**
   * Releases resources allocated for this archive manager.
   */
  public void closeArchive()
  {
    //remove this archive manager from the worker threads if it was added
    removeFromWorkerThread(archiveItemWTObj);
    removeFromWorkerThread(purgeWTObj);
    super.closeArchive();
  }

  /**
   * Gets the archive purge age in milliseconds.
   * The purge age is the number of milliseconds to keep objects
   * in the archive, or 0 for "infinity".
   * @return the archive purge age in milliseconds (0 if disabled.)
   *
   * @see setArchivePurgeAge
   */
  public long getArchivePurgeAge()
  {
    return archivePurgeAgeMilliSecs;
  }

  /**
   * Gets the archive poll time.
   * @return the archive poll time in milliseconds or 0
   * for interrupts (default).
   */
  public long getArchivePollTime()
  {
    return archivePollTime;
  }

  /**
   * Determines if the archive item queue is enabled.
   * @return true if the the archive item queue is enabled, false otherwise.
   *
   * @see setArchiveItemQueueEnabled
   */
  public boolean isArchiveItemQueueEnabled()
  {
    return archiveItemQueueEnabled;
  }

  /**
   * Enables/disables the archive item queue.
   * @param b true to enable, false to disable.
   *
   * NOTE: The archive poll time should be set prior to calling this method
   * via the 'setArchivePollTime' method.
   *
   * @see isArchiveItemQueueEnabled
   * @see setArchivePollTime
   */
  public void setArchiveItemQueueEnabled(boolean b)
  {
    if(archiveClosedFlag)         //if archived closed then
      return;                     //abort method

    //only allow one thread access to the archive at a time
    synchronized(archiveAccessSyncObj)
    {
      archiveItemQueueEnabled = b;
    }
    if (archiveItemQueueEnabled)  //if queue is enabled
    {
      synchronized (archiveItemWTSyncObj)
      {
        //if the archive item worker thread does not exist
        if (archiveItemWTObj == null)
        {
          //create it
          archiveItemWTObj = new ArchiveItemWorkerThread(archivePollTime);
        }
      }

      //add to the worker thread if not already done
      addToWorkerThread(archiveItemWTObj);
    }
    else
    {
      //remove this archive manager from the worker thread if it was added
      removeFromWorkerThread(archiveItemWTObj);
    }
  }

  /**
   * Sets the archive poll time.
   * @param archivePollTime the archive poll time in milliseconds or 0
   * for interrupts (default).
   */
  public void setArchivePollTime(long archivePollTime)
  {
    this.archivePollTime = archivePollTime;
  }

  /**
   * Sets the archive purge age in milliseconds.
   * The purge age is the number of milliseconds to keep objects
   * in the archive, or 0 for "infinity".
   * @param age the archive purge age in milliseconds (0 for disabled.)
   *
   * @see getArchivePurgeAge
   */
  public void setArchivePurgeAge(long age)
  {
    if(archiveClosedFlag)         //if archived closed then
      return;                     //abort method

    //only allow one thread access to the archive at a time
    synchronized(archiveAccessSyncObj)
    {
      archivePurgeAgeMilliSecs = age;
    }
    if (archivePurgeAgeMilliSecs > 0)  //if purge is enabled
    {
      synchronized (purgeWTSyncObj)
      {
        //if the archive item worker thread does not exist
        if (purgeWTObj == null)
        {
          //create it
          purgeWTObj = new PurgeWorkerThread();
        }
      }

      //add to the worker thread if not already done
      addToWorkerThread(purgeWTObj);
    }
    else
    {
      //remove this archive manager from the worker thread if it was added
      removeFromWorkerThread(purgeWTObj);
    }
  }

  /**
   * Terminates the worker threads started by this archive manager.  The
   * worker threads handle item adding and purging for the archive.  This
   * method can be used to terminate the worker threads for all created
   * archive-manager objects (instead of calling the 'closeArchive()'
   * method for each archive manager).
   */
  public static void terminateWorkerThreads()
  {
    synchronized(archiveItemWTSyncObj)
    {    //grab thread lock for 'archiveItemWTObj'
      if(archiveItemWTObj != null)     //if thread exists then
        archiveItemWTObj.terminate();  //terminate thread
    }
    synchronized(purgeWTSyncObj)
    {    //grab thread lock for 'purgeWTObj'
      if(purgeWTObj != null)           //if thread exists then
        purgeWTObj.terminate();        //terminate thread
    }
  }

  /**
   * Adds this archive manager to the worker thread
   * if it has not already been added.
   * @param wt the worker thread.
   */
  protected void addToWorkerThread(AbstractWorkerThread wt)
  {
    if (wt != null)
    {
      wt.addArchiveManager(this);
    }
  }

  /**
   * Inserts an item into the archive.
   * @param item the item to be archived.
   * @throws IOException if the archive file can not be written to.
   */
  protected void archiveQueuedItem(Archivable item) throws IOException
  {
    super.archiveItem(item, false);
  }

  /**
   * Signals the worker thread.
   * @param wt the worker thread.
   */
  protected void signalWorkerThread(AbstractWorkerThread wt)
  {
    if (archivePollTime == 0 && wt != null)
    {
      wt.notifyThread();
    }
  }

  /**
   * Purges items and archive files with an archive date older than the
   * purge age if enable.
   * @param currentTime the current time in milliseconds.
   * @return true if successful, false if an error occurred.
   *
   * The purge age must be set with a call to setArchivePurgeAge.
   *
   * @see setArchivePurgeAge, setArchivePurgeAge
   */
  protected boolean purgeArchive(long currentTime)
  {
    if (archivePurgeAgeMilliSecs > 0)  //if purge-from-archive is enabled
    {
      //purge old items from archive
      final Date cutoffDate = new Date(currentTime - archivePurgeAgeMilliSecs);
      return purgeArchive(cutoffDate);
    }
    return false;  //error, no purge age
  }

  /**
   * Removes this archive manager from the worker thread if it was added.
   * @param wt the worker thread.
   */
  protected void removeFromWorkerThread(AbstractWorkerThread wt)
  {
    if (wt != null)
    {
      wt.removeArchiveManager(this);
    }
  }


  /**
   * Class AbstractWorkerThread is a general worker thread for archive managers.
   */
  protected static abstract class AbstractWorkerThread
                                                    extends IstiNotifyThread
  {
    protected final static long DEF_WORKER_SLEEPTIME = 50000000000000L;
    protected final long sleepTime;
    protected Vector archiveManagerVector = new Vector();

    public AbstractWorkerThread(String name, long sleepTime)
    {
      super(name);
      if (sleepTime <= 0)
        this.sleepTime = DEF_WORKER_SLEEPTIME;
      else
        this.sleepTime = sleepTime;
      start();  //start the thread
    }

    /**
     * Adds an archive manager if it has not already been added.
     * @param am the archive manager.
     */
    public void addArchiveManager(ExtendedArchiveManager am)
    {
//      final int numArchiveManagers;
      synchronized (archiveManagerVector)
      {
//        numArchiveManagers = archiveManagerVector.size();
        if (!archiveManagerVector.contains(am))
          archiveManagerVector.add(am);
      }
    }

    /**
     * Gets the archive manager for the specified index.
     * @param index index of archive manager to return.
     * @return the archive manager or null if it does not exist.
     */
    public ExtendedArchiveManager getArchiveManager(int index)
    {
      try
      {
        synchronized (archiveManagerVector)
        {
          return (ExtendedArchiveManager)archiveManagerVector.get(index);
        }
      }
      catch (Exception ex)
      {
      }
      return null;
    }

    /**
     * Gets the number of archive managers handled by this class.
     * @return the number of archive managers handled by this class.
     */
    public int getNumArchiveManagers()
    {
      synchronized (archiveManagerVector)
      {
        return archiveManagerVector.size();
      }
    }

    /**
     * Processes an archive manager.
     * @param am the archive manager.
     */
    public abstract void processArchiveManager(ExtendedArchiveManager am);

    /**
     * Removes an archive manager.
     * @param am the archive manager.
     */
    public void removeArchiveManager(ExtendedArchiveManager am)
    {
      final int numArchiveManagers;
      synchronized (archiveManagerVector)
      {
        archiveManagerVector.remove(am);
        numArchiveManagers = archiveManagerVector.size();
      }

      if (numArchiveManagers <= 0)
        terminate();  //terminate the worker thread
    }

    /**
     * Runs the worker thread.
     */
    public void run()
    {
      while (!isTerminated())  //loop while thread not terminated
      {
        waitForNotify(sleepTime);   //wait until time to purge

        if (isTerminated())         //if terminated then
          return;                   //exit thread

        final int numArchiveManagers = getNumArchiveManagers();
        ExtendedArchiveManager am;
        for (int i = 0; i < numArchiveManagers; i++)
        {
          if (isTerminated())    //if terminated then
            return;                   //exit thread

          if ((am = getArchiveManager(i)) == null)
            break;

          if (isTerminated())         //if terminated then
            return;                   //exit thread

          processArchiveManager(am);  //do processing on the archive manager
        }
      }
    }
  }


  /**
   * Class ArchiveItemWorkerThread fetches items from the queue
   * and adds them to the archive.
   */
  protected static class ArchiveItemWorkerThread extends AbstractWorkerThread
  {
    public ArchiveItemWorkerThread(long sleepTime)
    {
      super(("ExtendedArchiveManager.ArchiveItemWorkerThread" +
                                               nextThreadNum()), sleepTime);
    }

    /**
     * Processes an archive manager.
     * @param am the archive manager.
     */
    public void processArchiveManager(ExtendedArchiveManager am)
    {
      while (!isTerminated())  //loop while thread not terminated
      {
        //if no archive manager or no objects in message-queue Vector
        if (am == null || am.archiveItemQueueVec.size() <= 0)
        {
          return;           //done
        }

        try
        {
          //fetch first item from queue and add it to the archive
          am.archiveQueuedItem(
              (Archivable)(am.archiveItemQueueVec.firstElement()));

          //grab thread-synchronization lock for message-queue Vector
          synchronized(am.archiveItemQueueVec)
          {
            //remove first item from message-queue Vector:
            am.archiveItemQueueVec.remove(0);
          }
        }
        catch(IOException ex)  //I/O error archiving item
        {
          if(am.getLogFile() != null)
          {
            am.getLogFile().warning(am.getLogPrefixString() +
                                    ":  I/O error archiving item:  " + ex);
            am.getLogFile().debug(UtilFns.getStackTraceString(ex));
          }
          return;           //done
        }
        catch(Exception ex)  //error archiving item
        {
          if(am.getLogFile() != null)
          {
            am.getLogFile().warning(am.getLogPrefixString() +
                                    ":  Error archiving item:  " + ex);
            am.getLogFile().debug(UtilFns.getStackTraceString(ex));
          }
          return;           //done
        }
      }
    }

    /**
     * Terminates this thread if the thread is not terminated and alive.
     */
    public void terminate()
    {
      synchronized (ExtendedArchiveManager.archiveItemWTSyncObj)
      {
        ExtendedArchiveManager.archiveItemWTObj = null;
      }
      super.terminate();
    }
  }


  /**
   * Class PurgeWorkerThread purges items from the archive.
   */
  protected static class PurgeWorkerThread extends AbstractWorkerThread
  {
    protected final static long DEF_PURGE_SLEEPTIME = UtilFns.MS_PER_MINUTE;

    public PurgeWorkerThread()
    {
      super("ExtendedArchiveManager.PurgeWorkerThread-" + nextThreadNum(),
            DEF_PURGE_SLEEPTIME);
    }

    /**
     * Processes an archive manager.
     * @param am the archive manager.
     */
    public void processArchiveManager(ExtendedArchiveManager am)
    {
      if (am.getArchivePurgeAge() > 0)  //if purge is enabled
      {
        //purge archive again if not already done
        final long currentTime = System.currentTimeMillis();
        am.purgeArchive(currentTime);
      }
    }

    /**
     * Terminates this thread if the thread is not terminated and alive.
     */
    public void terminate()
    {
      synchronized (ExtendedArchiveManager.purgeWTSyncObj)
      {
        ExtendedArchiveManager.purgeWTObj = null;
      }
      super.terminate();
    }
  }
}
