//ExtendedComparable.java:  Extends the Comparable interface to allow multiple
// ways sorting.
//
//  12/7/2005 -- [KF]  Initial version.
//

package com.isti.util;

/**
 * This interface extends the Comparable interface to allow multiple
 * ways sorting.<p>
 */
public interface ExtendedComparable extends Comparable
{
  /**
   * The value for the no sort type.
   */
  public static final int NO_SORT_TYPE = 0;

  /**
   * Compares this object with the specified object and index for order.
   * Returns a negative integer, zero, or a positive integer as this object is
   * less than, equal to, or greater than the specified object.<p>
   * @param   o the Object to be compared.
   * @param   sortType the sort type.
   * @return  a negative integer, zero, or a positive integer as this object
   *		is less than, equal to, or greater than the specified object.
   *
   * @throws ClassCastException if the specified object's type prevents it
   *         from being compared to this Object.
   */
    public int compareTo(Object o,int sortType);
}
