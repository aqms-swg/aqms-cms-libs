//FifoHashtable.java:  Version of HashTable class which can return its
//                     elements in the same order as they were entered.
//
//   7/12/2000 -- [ET]  Initial version.
//   7/31/2000 -- [ET]  Added Vector-like methods and 'swapEntries()'
//                      method.
//    8/5/2000 -- [ET]  Fixed up double-spaced lines.
//   9/12/2000 -- [ET]  Added 'putSort()' method.
//    4/2/2001 -- [ET]  Added optional comparator parameter to 'putSort()'
//                      method.
//   4/24/2001 -- [ET]  Added 'toQuotedStrings()' & 'quotedStringToTable()'
//                      methods.
//   8/23/2001 -- [ET]  Added 'putAll()' methods; added 'synchronized'
//                      keyword to several methods that didn't have it.
//   1/30/2002 -- [ET]  Changed 'putSort()' to allow 'compObj' parameter
//                      to be null.
//   9/17/2002 -- [ET]  Added 'putSortByValue()' methods.
//   9/26/2002 -- [ET]  Fixed 'putSortByValue()' methods; improved ability
//                      to deal with duplicate values in table; added
//                      'removeValue()' method.
//  10/28/2002 -- [KF]  Added support for Map interface.
//   1/28/2003 -- [ET]  Minor documentation changes.
//   2/26/2003 -- [ET]  Fixed bug in 'removeElementAt()' that would put the
//                      hashmap out of sync anytime the method was used.
//   8/15/2003 -- [ET]  Added 'removeAllKeys()' and 'removeAllValues()'
//                      methods.
//  10/11/2003 -- [ET]  Modified the iterators created by the 'keySet()',
//                      'values()' and 'entrySet()' methods so that they
//                      do not support 'remove()'.
//  10/14/2003 -- [ET]  Removed version of 'putAll()' with "FifoHashtable"
//                      parameter; added 'putSortAll()' and
//                      'putSortByValueAll()' methods; changed the
//                      'MapEntry' member class declaration to
//                      "public static".
//   7/22/2004 -- [ET]  Added return value to 'removeElementAt()' method.
//   10/6/2004 -- [ET]  Modified to deal properly with 'null' values in
//                      table; fixed 'putSort()' action when table
//                      previously contained key; disabled non-working
//                      'setValue()' method in 'MapEntry'; changed
//                      'removeElementAt(int)' calls to 'remove(int)';
//                      changed 'elementAt(int)' calls to 'get(int)';
//                      changed 'insertElementAt()' calls to 'add()';
//                      changed 'setElementAt()' calls to 'set()';
//                      changed 'Enumeration' usage to 'Iterator'.
//  12/10/2004 -- [ET]  Added optional 'dontQuoteKeysFlag' parameter to
//                      'toQuotedStrings()' method.
//   1/18/2005 -- [ET]  Added optional 'dontQuoteValuesFlag' parameter to
//                      'toQuotedStrings()' method.
//  12/12/2005 -- [ET]  Added support for 'ExtendedComparable'.
//   8/11/2006 -- [ET]  Added optional 'sortType' parameter to 'putSortAll()'
//                      and 'putSortByValueAll()' methods.
//   2/14/2008 -- [ET]  Added 'getSubsetTable()' and 'getStringValue()'
//                      methods.
//

package com.isti.util;

import java.util.*;

/**
 * Class FifoHashtable is a version of the HashTable class which can
 * return its elements in the same order as they were entered.  All
 * methods are synchronized.
 */
public class FifoHashtable implements Cloneable,Map
{
                   //internal HashMap object:
  private final HashMap hashMapObj;
                   //vector of HashMap keys in FIFO order:
  private final Vector keysVec;
                   //vector of HashMap values in FIFO order:
  private final Vector valuesVec;
                   //vector of map entry values in FIFO order:
  private final Vector mapVec;

    /**
     * Constructs a new, empty hashtable with the specified initial
     * capacity and the specified load factor.
     * @param initialCapacity the initial capacity of the hashtable.
     * @param loadFactor the load factor of the hashtable.
     * @exception IllegalArgumentException if the initial capacity is
     * less than zero, or if the load factor is nonpositive.
     */
  public FifoHashtable(int initialCapacity,float loadFactor)
  {
    this(new HashMap(initialCapacity,loadFactor));
  }

    /**
     * Constructs a new, empty hashtable with the specified initial
     * capacity and the default load factor, which is 0.75.
     * @param initialCapacity the initial capacity of the hashtable.
     * @exception IllegalArgumentException if the initial capacity is
     * less than zero.
     */
  public FifoHashtable(int initialCapacity)
  {
    this(new HashMap(initialCapacity));
  }

    /**
     * Constructs a new, empty hashtable with a default capacity and load
     * factor, which is 0.75.
     */
  public FifoHashtable()
  {
    this(new HashMap());
  }

  /**
   * Gets the compare result
   * @param compObj the Comparable Object.
   * @param obj the other Object.
   * @param sortType the sort type.
   * @return the compare results.
   */
  private static int getCompareResult(Object compObj, Object obj, int sortType)
  {
    final int compareResult;
    if (sortType != 0 && compObj instanceof ExtendedComparable)
      compareResult = ( (ExtendedComparable) compObj).compareTo(obj, sortType);
    else
      compareResult = ( (Comparable) compObj).compareTo(obj);
    return compareResult;
  }

  /**
   * Constructs with the specified hashtable.
   * @param hashMapObj hash map object
   */
  protected FifoHashtable(HashMap hashMapObj)
  {
    this.hashMapObj = hashMapObj;
    keysVec = new Vector();            //create FIFO vector for keys
    valuesVec = new Vector();          //create FIFO vector for values
    mapVec = new Vector();             //create FIFO vector for map entries
  }

    //internal constructor for cloning:
  private FifoHashtable(HashMap hMap,Vector kVec,Vector vVec,
                                                                Vector mVec)
  {
    hashMapObj = hMap;
    keysVec = kVec;
    valuesVec = vVec;
    mapVec = mVec;
  }

    /**
     * Associates the specified value with the specified key in this
     * hashtable.  If the hashtable previously contained a mapping for
     * this key, the old value is replaced.
     * @param key key with which the specified value is to be associated.
     * @param value value to be associated with the specified key.
     * @return previous value associated with specified key, or <tt>null</tt>
     *	       if there was no mapping for key.  A <tt>null</tt> return can
     *	       also indicate that the HashMap previously associated
     *	       <tt>null</tt> with the specified key.
     */
  public synchronized Object put(Object key,Object value)
  {
                   //put key/value pair into HashMap; save previous value:
    final Object oldObj = hashMapObj.put(key,value);
    final int p;
    if((p=keysVec.indexOf(key)) >= 0)
    {    //previous value was associated with key
      keysVec.remove(p);               //remove key from FIFO vector
      if(p < valuesVec.size())         //if position OK then
        valuesVec.remove(p);           //remove old value from FIFO vector
      if(p < mapVec.size())            //if position OK then
        mapVec.remove(p);              //remove old map entry from FIFO vector
    }
    keysVec.add(key);                  //add key to FIFO vector
    valuesVec.add(value);              //add new value to FIFO vector
    mapVec.add(new MapEntry(key,value)); //add new map entry to FIFO vector
    return oldObj;           //return old value (or null)
  }

    /**
     * Copies all of the mappings from the specified map to this one.
     * These mappings replace any mappings that this map had for any of the
     * keys currently in the specified Map.
     * @param mapObj mappings to be stored in this map.
     */
  public synchronized void putAll(Map mapObj)
  {
    final Iterator i = mapObj.entrySet().iterator();
    Map.Entry e;
    while (i.hasNext())
    {
      e = (Map.Entry)i.next();
      put(e.getKey(),e.getValue());
    }
  }

    /**
     * Adds a key/value pair to the hashtable, placing it in sorted
     * order according to the key.  The key should implement the Comparable
     * interface.  Associates the specified value with the specified key in
     * this hashtable.  If the table previously contained a mapping for
     * this key, the old value is replaced.
     * @param key key with which the specified value is to be associated.
     * @param value value to be associated with the specified key.
     * @param sortDirFlag true for ascending sort order, false for
     * descending.
     * @return previous value associated with specified key, or <tt>null</tt>
     *	       if there was no mapping for key.  A <tt>null</tt> return can
     *	       also indicate that the HashMap previously associated
     *	       <tt>null</tt> with the specified key.
     */
  public synchronized Object putSort(Object key,Object value,boolean sortDirFlag)
  {
    return putSort(key,value,sortDirFlag,ExtendedComparable.NO_SORT_TYPE);
  }

    /**
     * Adds a key/value pair to the hashtable, placing it in sorted
     * order according to the key.  The key should implement the Comparable
     * interface.  Associates the specified value with the specified key in
     * this hashtable.  If the table previously contained a mapping for
     * this key, the old value is replaced.
     * @param key key with which the specified value is to be associated.
     * @param value value to be associated with the specified key.
     * @param sortDirFlag true for ascending sort order, false for
     * descending.
     * @param sortType the sort type.
     * @return previous value associated with specified key, or <tt>null</tt>
     *	       if there was no mapping for key.  A <tt>null</tt> return can
     *	       also indicate that the HashMap previously associated
     *	       <tt>null</tt> with the specified key.
     */
  public synchronized Object putSort(Object key,Object value,
                        boolean sortDirFlag,int sortType)
  {
                   //put key/value pair into HashMap:
    final Object oldObj = hashMapObj.put(key,value);
    int p;
    if((p=keysVec.indexOf(key)) < 0)
    {    //previous value was not associated with key
      int tableSize = keysVec.size();       //get size of keys FIFO vector
      if(valuesVec.size() < tableSize)      //if values vector is smaller
        tableSize = valuesVec.size();       // then adjust table size value
      p = 0;
      Object obj;
      int compareResult;
      if(key instanceof Comparable)
      {       //key implements the Comparable interface
        while(p < tableSize)
        {     //loop while index is less than # of elements
          try
          {
            if((obj=keysVec.get(p)) != null)
            {      //element at position in keys FIFO vector fetched OK
              compareResult = getCompareResult(key, obj, sortType);
              if(sortDirFlag)
              {    //ascending-order sort
                if(compareResult < 0)
                  break;     //if less than key-obj at position then stop
              }
              else
              {    //descending-order sort
                if(compareResult > 0)
                  break;     //if greater than key-obj at position then stop
              }
            }
          }
          catch(Exception ex) {}       //if exception error then move on
          ++p;          //increment index to next position in vector
        }
      }
      else    //key does not implement the Comparable interface
        p = tableSize;       //setup to add to end of FIFO vectors
         //insert key/value into FIFO vectors at sort position
      keysVec.add(p,key);
      valuesVec.add(p,value);
      mapVec.add(p,new MapEntry(key,value));
    }
    else      //previous value was associated with key
    {              //replace Vector entries in same positions:
      if(p < valuesVec.size())
        valuesVec.set(p,value);
      if(p < mapVec.size())
        mapVec.set(p,new MapEntry(key,value));
    }
    return oldObj;           //return old value (or null)
  }

    /**
     * Adds a key/value pair to the hashtable, placing it in sorted
     * order according to the key.  The key should implement the Comparable
     * interface.  Associates the specified value with the specified key in
     * this hashtable.  If the table previously contained a mapping for
     * this key, the old value is replaced.
     * @param key key with which the specified value is to be associated.
     * @param value value to be associated with the specified key.
     * @param sortDirFlag true for ascending sort order, false for
     * descending.
     * @param compObj the comparator to use for sorting the 'key' objects,
     * or null to use the "natural" sort order.
     * @return previous value associated with specified key, or <tt>null</tt>
     *	       if there was no mapping for key.  A <tt>null</tt> return can
     *	       also indicate that the HashMap previously associated
     *	       <tt>null</tt> with the specified key.
     */
  public synchronized Object putSort(Object key,Object value,
                                     boolean sortDirFlag,Comparator compObj)
  {
    if(compObj == null)                          //if no comparator then
      return putSort(key,value,sortDirFlag);     //use other version
                   //put key/value pair into HashMap:
    final Object oldObj = hashMapObj.put(key,value);
    int p;
    if((p=keysVec.indexOf(key)) < 0)
    {    //previous value was not associated with key
      int tableSize = keysVec.size();       //get size of keys FIFO vector
      if(valuesVec.size() < tableSize)      //if values vector is smaller
        tableSize = valuesVec.size();       // then adjust table size value
      p = 0;
      Object obj;
      if(key instanceof Comparable)
      {       //key implements the Comparable interface
        while(p < tableSize)
        {     //loop while index is less than # of elements
          try
          {
            if((obj=keysVec.get(p)) != null)
            {      //element at position in keys FIFO vector fetched OK
              if(sortDirFlag)
              {    //ascending-order sort
                if(compObj.compare(key,obj) < 0)
                  break;     //if less than key-obj at position then stop
              }
              else
              {    //descending-order sort
                if(compObj.compare(key,obj) > 0)
                  break;     //if greater than key-obj at position then stop
              }
            }
          }
          catch(Exception ex) {}       //if exception error then move on
          ++p;          //increment index to next position in vector
        }
      }
      else    //key does not implement the Comparable interface
        p = tableSize;       //setup to add to end of FIFO vectors
         //insert key/value into FIFO vectors at sort position
      keysVec.add(p,key);
      valuesVec.add(p,value);
      mapVec.add(p,new MapEntry(key,value));
    }
    else      //previous value was associated with key
    {              //replace Vector entries in same positions:
      if(p < valuesVec.size())
        valuesVec.set(p,value);
      if(p < mapVec.size())
        mapVec.set(p,new MapEntry(key,value));
    }
    return oldObj;           //return old value (or null)
  }

    /**
     * Copies all of the mappings from the specified map to this one,
     * placing them in sorted order according to the keys.  The keys
     * should implement the Comparable interface.  These mappings
     * replace any mappings that this map had for any of the keys
     * currently in the specified Map.
     * @param mapObj mappings to be stored in this map.
     * @param sortDirFlag true for ascending sort order, false for
     * descending.
     */
  public synchronized void putSortAll(Map mapObj,boolean sortDirFlag)
  {
    putSortAll(mapObj,sortDirFlag,ExtendedComparable.NO_SORT_TYPE);
  }

    /**
     * Copies all of the mappings from the specified map to this one,
     * placing them in sorted order according to the keys.  The keys
     * should implement the Comparable interface.  These mappings
     * replace any mappings that this map had for any of the keys
     * currently in the specified Map.
     * @param mapObj mappings to be stored in this map.
     * @param sortDirFlag true for ascending sort order, false for
     * descending.
     * @param sortType the sort type.
     */
  public synchronized void putSortAll(Map mapObj,
                                           boolean sortDirFlag,int sortType)
  {
    final Iterator i = mapObj.entrySet().iterator();
    Map.Entry e;
    while (i.hasNext())
    {
      e = (Map.Entry)i.next();
      putSort(e.getKey(),e.getValue(),sortDirFlag,sortType);
    }
  }

    /**
     * Copies all of the mappings from the specified map to this one,
     * placing them in sorted order according to the keys.  The keys
     * should implement the Comparable interface.  These mappings
     * replace any mappings that this map had for any of the keys
     * currently in the specified Map.
     * @param mapObj mappings to be stored in this map.
     * @param sortDirFlag true for ascending sort order, false for
     * descending.
     * @param compObj the comparator to use for sorting the 'key' objects,
     * or null to use the "natural" sort order.
     */
  public synchronized void putSortAll(Map mapObj,boolean sortDirFlag,
                                                         Comparator compObj)
  {
    final Iterator i = mapObj.entrySet().iterator();
    Map.Entry e;
    while (i.hasNext())
    {
      e = (Map.Entry)i.next();
      putSort(e.getKey(),e.getValue(),sortDirFlag,compObj);
    }
  }

    /**
     * Adds a key/value pair to the hashtable, placing it in sorted
     * order according to the value.  The value should implement the
     * Comparable interface.  Associates the specified value with the
     * specified key in this hashtable.  If the table previously
     * contained a mapping for this key, the old value is deleted and
     * the new value is placed in proper sort order.
     * @param key key with which the specified value is to be associated.
     * @param value value to be associated with the specified key.
     * @param sortDirFlag true for ascending sort order, false for
     * descending.
     * @return previous value associated with specified key, or <tt>null</tt>
     *	       if there was no mapping for key.  A <tt>null</tt> return can
     *	       also indicate that the HashMap previously associated
     *	       <tt>null</tt> with the specified key.
     */
  public synchronized Object putSortByValue(Object key,Object value,boolean sortDirFlag)
  {
    return putSortByValue(key,value,sortDirFlag,ExtendedComparable.NO_SORT_TYPE);
  }

    /**
     * Adds a key/value pair to the hashtable, placing it in sorted
     * order according to the value.  The value should implement the
     * Comparable interface.  Associates the specified value with the
     * specified key in this hashtable.  If the table previously
     * contained a mapping for this key, the old value is deleted and
     * the new value is placed in proper sort order.
     * @param key key with which the specified value is to be associated.
     * @param value value to be associated with the specified key.
     * @param sortDirFlag true for ascending sort order, false for
     * descending.
     * @param sortType the sort type.
     * @return previous value associated with specified key, or <tt>null</tt>
     *	       if there was no mapping for key.  A <tt>null</tt> return can
     *	       also indicate that the HashMap previously associated
     *	       <tt>null</tt> with the specified key.
     */
  public synchronized Object putSortByValue(Object key,Object value,
                               boolean sortDirFlag,int sortType)
  {
                   //put key/value pair into HashMap:
    final Object oldObj = hashMapObj.put(key,value);
    int p;
    if((p=keysVec.indexOf(key)) >= 0)
    {    //previous value was associated with key
      keysVec.remove(p);               //remove key from FIFO vector
      if(p < valuesVec.size())         //if position OK then
        valuesVec.remove(p);           //remove old value from FIFO vector
      if(p < mapVec.size())            //if position OK then
        mapVec.remove(p);              //remove old map entry from FIFO vector
    }
    int tableSize = keysVec.size();       //get size of keys FIFO vector
    if(valuesVec.size() < tableSize)      //if values vector is smaller
      tableSize = valuesVec.size();       // then adjust table size value
    p = 0;
    Object obj;
    int compareResult;
    if(value instanceof Comparable)
    {       //value implements the Comparable interface
      while(p < tableSize)
      {     //loop while index is less than # of elements
        try
        {
          if((obj=valuesVec.get(p)) != null)
          {      //element at position in values FIFO vector fetched OK
            compareResult = getCompareResult(value, obj, sortType);
            if(sortDirFlag)
            {    //ascending-order sort
              if(compareResult < 0)
                break;     //if less than value-obj at position then stop
            }
            else
            {    //descending-order sort
              if(compareResult > 0)
                break;     //if greater than value-obj at position then stop
            }
          }
        }
        catch(Exception ex) {}       //if exception error then move on
        ++p;          //increment index to next position in vector
      }
    }
    else    //key does not implement the Comparable interface
      p = tableSize;       //setup to add to end of FIFO vectors
       //insert key/value into FIFO vectors at sort position
    keysVec.add(p,key);
    valuesVec.add(p,value);
    mapVec.add(p,new MapEntry(key,value));
    return oldObj;           //return old value (or null)
  }

    /**
     * Adds a key/value pair to the hashtable, placing it in sorted
     * order according to the value.  The value should implement the
     * Comparable interface.  Associates the specified value with the
     * specified key in this hashtable.  If the table previously
     * contained a mapping for this key, the old value is deleted and
     * the new value is placed in proper sort order.
     * @param key key with which the specified value is to be associated.
     * @param value value to be associated with the specified key.
     * @param sortDirFlag true for ascending sort order, false for
     * descending.
     * @param compObj the comparator to use for sorting the 'value' objects,
     * or null to use the "natural" sort order.
     * @return previous value associated with specified key, or <tt>null</tt>
     *	       if there was no mapping for key.  A <tt>null</tt> return can
     *	       also indicate that the HashMap previously associated
     *	       <tt>null</tt> with the specified key.
     */
  public synchronized Object putSortByValue(Object key,Object value,
                                     boolean sortDirFlag,Comparator compObj)
  {
    if(compObj == null)                               //if no comparator then
      return putSortByValue(key,value,sortDirFlag);   //use other version
                   //put key/value pair into HashMap:
    final Object oldObj = hashMapObj.put(key,value);
    int p;
    if((p=keysVec.indexOf(key)) >= 0)
    {    //previous value was associated with key
      keysVec.remove(p);               //remove key from FIFO vector
      if(p < valuesVec.size())         //if position OK then
        valuesVec.remove(p);           //remove old value from FIFO vector
      if(p < mapVec.size())            //if position OK then
        mapVec.remove(p);              //remove old map entry from FIFO vector
    }
    int tableSize = keysVec.size();       //get size of keys FIFO vector
    if(valuesVec.size() < tableSize)      //if values vector is smaller
      tableSize = valuesVec.size();       // then adjust table size value
    p = 0;
    Object obj;
    if(value instanceof Comparable)
    {       //value implements the Comparable interface
      while(p < tableSize)
      {     //loop while index is less than # of elements
        try
        {
          if((obj=valuesVec.get(p)) != null)
          {      //element at position in values FIFO vector fetched OK
            if(sortDirFlag)
            {    //ascending-order sort
              if(compObj.compare(value,obj) < 0)
                break;     //if less than value-obj at position then stop
            }
            else
            {    //descending-order sort
              if(compObj.compare(value,obj) > 0)
                break;     //if greater than value-obj at position then stop
            }
          }
        }
        catch(Exception ex) {}       //if exception error then move on
        ++p;          //increment index to next position in vector
      }
    }
    else    //key does not implement the Comparable interface
      p = tableSize;       //setup to add to end of FIFO vectors
       //insert key/value into FIFO vectors at sort position
    keysVec.add(p,key);
    valuesVec.add(p,value);
    mapVec.add(p,new MapEntry(key,value));
    return oldObj;           //return old value (or null)
  }

    /**
     * Copies all of the mappings from the specified map to this one,
     * placing them in sorted order according to the values.  The values
     * should implement the Comparable interface.  These mappings
     * replace any mappings that this map had for any of the keys
     * currently in the specified Map.
     * @param mapObj mappings to be stored in this map.
     * @param sortDirFlag true for ascending sort order, false for
     * descending.
     */
  public synchronized void putSortByValueAll(Map mapObj,boolean sortDirFlag)
  {
    putSortByValueAll(mapObj,sortDirFlag,ExtendedComparable.NO_SORT_TYPE);
  }

    /**
     * Copies all of the mappings from the specified map to this one,
     * placing them in sorted order according to the values.  The values
     * should implement the Comparable interface.  These mappings
     * replace any mappings that this map had for any of the keys
     * currently in the specified Map.
     * @param mapObj mappings to be stored in this map.
     * @param sortDirFlag true for ascending sort order, false for
     * descending.
     * @param sortType the sort type.
     */
  public synchronized void putSortByValueAll(Map mapObj,
                                           boolean sortDirFlag,int sortType)
  {
    final Iterator i = mapObj.entrySet().iterator();
    Map.Entry e;
    while (i.hasNext())
    {
      e = (Map.Entry)i.next();
      putSortByValue(e.getKey(),e.getValue(),sortDirFlag,sortType);
    }
  }

    /**
     * Copies all of the mappings from the specified map to this one,
     * placing them in sorted order according to the values.  The values
     * should implement the Comparable interface.  These mappings
     * replace any mappings that this map had for any of the keys
     * currently in the specified Map.
     * @param mapObj mappings to be stored in this map.
     * @param sortDirFlag true for ascending sort order, false for
     * descending.
     * @param compObj the comparator to use for sorting the 'value' objects,
     * or null to use the "natural" sort order.
     */
  public synchronized void putSortByValueAll(Map mapObj,boolean sortDirFlag,
                                                         Comparator compObj)
  {
    final Iterator i = mapObj.entrySet().iterator();
    Map.Entry e;
    while (i.hasNext())
    {
      e = (Map.Entry)i.next();
      putSortByValue(e.getKey(),e.getValue(),sortDirFlag,compObj);
    }
  }

    /**
     * Returns the value to which this hashtable maps the specified key.
     * Returns <tt>null</tt> if the hashtable contains no mapping for this
     * key.  A return value of <tt>null</tt> does not <i>necessarily</i>
     * indicate that the hashtable contains no mapping for the key; it's
     * also possible that the hashtable explicitly maps the key to
     * <tt>null</tt>.  The <tt>containsKey</tt> operation may be used
     * to distinguish these two cases.
     * @param key key whose associated value is to be returned.
     * @return The value to which this hashtable maps the specified key.
     */
  public synchronized Object get(Object key)
  {
    return hashMapObj.get(key);
  }

    /**
     * Returns the string value to which this hashtable maps the specified
     * key.  Returns the given default-string value if the hashtable
     * contains no mapping for this key.
     * @param key key whose associated value is to be returned.
     * @param defaultStr default-string value to be returned if the given
     * key is not matched.
     * @return The string value to which this hashtable maps the specified
     * key, or the default-string value if no match.
     */
  public synchronized String getStringValue(Object key, String defaultStr)
  {
    final Object obj;
    return ((obj=hashMapObj.get(key)) instanceof String) ?
                                                   (String)obj : defaultStr;
  }

    /**
     * Returns the string value to which this hashtable maps the specified
     * key.  Returns 'null' if the hashtable contains no mapping for this
     * key.
     * @param key key whose associated value is to be returned.
     * @return The string value to which this hashtable maps the specified
     * key, or 'null' if no match.
     */
  public synchronized String getStringValue(Object key)
  {
    return getStringValue(key,null);
  }

    /**
     * Removes the mapping for this key from this hashtable if present.
     * @param key key whose mapping is to be removed from the hashtable.
     * @return previous value associated with specified key, or <tt>null</tt>
     *	       if there was no mapping for key.  A <tt>null</tt> return can
     *	       also indicate that the hashtable previously associated
     *         <tt>null</tt> with the specified key.
     */
  public synchronized Object remove(Object key)
  {
                   //remove key/value pair from HashMap:
    final Object oldObj = hashMapObj.remove(key);
    final int p;
    if((p=keysVec.indexOf(key)) >= 0)
    {    //previous value was associated with key
      keysVec.remove(p);          //remove key from FIFO vector
      if(p < valuesVec.size())    //if position OK then
        valuesVec.remove(p);      //remove old value from FIFO vector
      if(p < mapVec.size())       //if position OK then
        mapVec.remove(p);         //remove old map entry from FIFO vector
    }
    return oldObj;           //return old value (or null)
  }

    /**
     * Removes the value and its key from this hashtable if present.
     * @param obj value to be removed from the hashtable.
     * @return previous key associated with specified value, or <tt>null</tt>
     *	       if the value was not found.  A <tt>null</tt> return can
     *	       also indicate that the hashtable previously associated
     *         <tt>null</tt> with the specified value.
     */
  public synchronized Object removeValue(Object obj)
  {
    final Object oldKeyObj;
    final int p;

    if((p=valuesVec.indexOf(obj)) < 0)
      return null;      //if value not found then return null
    if(p < keysVec.size())
    {    //position in vector OK; remove key from hashtable
      hashMapObj.remove(oldKeyObj=keysVec.get(p));
      keysVec.remove(p);               //remove key from FIFO vector
    }
    else
      oldKeyObj = null;
    valuesVec.remove(p);     //remove value from FIFO vector
    mapVec.remove(p);        //remove map entry from FIFO vector
    return oldKeyObj;        //return old key object
  }

    /**
     * Removes from this table all of the elements that have keys
     * matching items that are contained in the given Collection.
     * @param collObj the Collection of key-items to be removed.
     * @return true if this table changed as a result of the call.
     */
  public synchronized boolean removeAllKeys(Collection collObj)
  {
    boolean retFlag = false;
    final Iterator iterObj = collObj.iterator();
    int idx;
    while(iterObj.hasNext())
    {    //for each element in given Collection; find index of key
      if((idx=keysVec.indexOf(iterObj.next())) >= 0)
      {  //index found
        removeElementAt(idx);     //remove entry from table
        retFlag = true;           //indicate removal
      }
    }
    return retFlag;
  }

    /**
     * Removes from this table all of the elements that have values
     * matching items that are contained in the given Collection.
     * @param collObj the Collection of value-items to be removed.
     * @return true if this table changed as a result of the call.
     */
  public synchronized boolean removeAllValues(Collection collObj)
  {
    boolean retFlag = false;
    final Iterator iterObj = collObj.iterator();
    int idx;
    while(iterObj.hasNext())
    {    //for each element in given Collection; find index of value
      if((idx=valuesVec.indexOf(iterObj.next())) >= 0)
      {  //index found
        removeElementAt(idx);     //remove entry from table
        retFlag = true;           //indicate removal
      }
    }
    return retFlag;
  }

    /**
     * Tests if the specified object is a key in this hashtable.
     * @param   key   possible key.
     * @return  <code>true</code> if and only if the specified object
     *          is a key in this hashtable, as determined by the
     *          <tt>equals</tt> method; <code>false</code> otherwise.
     */
  public synchronized boolean containsKey(Object key)
  {
    return hashMapObj.containsKey(key);
  }

    /**
     * Returns true if this Hashtable maps one or more keys to this value.
     * This operation is more expensive than the 'containsKey()' method.
     * Note that this method is identical in functionality to 'contains()'
     * (which predates the Map interface).
     * @param value value whose presence in this map is to be tested.
     * @return true if this Hashtable maps one or more keys to this value.
     */
  public synchronized boolean containsValue(Object value)
  {
    return hashMapObj.containsValue(value);
  }

    /**
     * Tests if some key maps into the specified value in this hashtable.
     * This operation is more expensive than the 'containsKey()' method.
     * Note that this method is identical in functionality to
     * 'containsValue()' (which is part of the Map interface in
     * the collections framework).
     * @param value value whose presence in this map is to be tested.
     * @return     <code>true</code> if and only if some key maps to the
     *             <code>value</code> argument in this hashtable as
     *             determined by the <tt>equals</tt> method;
     *             <code>false</code> otherwise.
     */
  public synchronized boolean contains(Object value)
  {
    return hashMapObj.containsValue(value);
  }

    /**
     * Determines <tt>true</tt> if this hashtable contains no key-value
     * mappings.
     * @return <tt>true</tt> if this hashtable contains no key-value
     * mappings.
     */
  public synchronized boolean isEmpty()
  {
    return hashMapObj.isEmpty();
  }

    /**
     * Gets the number of key-value mappings in this hashtable.
     * @return the number of key-value mappings in this hashtable.
     */
  public synchronized int size()
  {
    return hashMapObj.size();
  }

    /**
     * Clears this hashtable so that it contains no keys.
     */
  public synchronized void clear()
  {
    hashMapObj.clear();
    keysVec.clear();
    valuesVec.clear();
    mapVec.clear();
  }

    /**
     * Compares the specified Object with this Map for equality,
     * as per the definition in the Map interface.
     * @param o object
     * @return true if the specified Object is equal to this Map.
     */
  public synchronized boolean equals(Object o)
  {
    return hashMapObj.equals(o);
  }

    /**
     * Gets the hash code value for this Map as per the definition in the
     * Map interface.
     * @return the hash code value for this Map as per the definition in the
     * Map interface.
     */
  public synchronized int hashCode()
  {
    return hashMapObj.hashCode();
  }

    /**
     * Returns a new Vector containing the keys in the hashtable.  The
     * keys will enumerated in same order as they were added to the table.
     * The data in the the returned Vector will not be affected by later
     * modifications to the hashtable, and the hashtable will not be
     * affected by modifications to the returned Vector.
     * @return A new Vector containing the keys in the hashtable.
     */
  public synchronized Vector getKeysVector()
  {
    return (Vector)(keysVec.clone());
  }

    /**
     * Returns a new Vector containing a sub-list of the keys in the
     * hashtable.  The keys will enumerated in same order as they were
     * added to the table.  The data in the the returned Vector will not
     * be affected by later modifications to the hashtable, and the
     * hashtable will not be affected by modifications to the returned
     * Vector.
     * @param fromIndex low endpoint (inclusive) of the sub-list.
     * @param toIndex high endpoint (exclusive) of the sub-list.
     * @return A new Vector containing a sub-list of the keys in the
     * hashtable.
     * @throws IndexOutOfBoundsException if the endpoint index value is
     * out of range (fromIndex < 0 || toIndex > size).
     * @throws IllegalArgumentException if the endpoint indices are out
     * of order (fromIndex > toIndex).
     */
  public synchronized Vector getKeysVector(int fromIndex,int toIndex)
  {
    return new Vector(keysVec.subList(fromIndex,toIndex));
  }

    /**
     * Returns a new Vector containing the values in the hashtable.  The
     * values will enumerated in same order as they were added to the table.
     * The data in the the returned Vector will not be affected by later
     * modifications to the hashtable, and the hashtable will not be
     * affected by modifications to the returned Vector.
     * @return A new Vector containing the values in the hashtable.
     */
  public synchronized Vector getValuesVector()
  {
    return (Vector)(valuesVec.clone());
  }

    /**
     * Returns a new Vector containing a sub-list of the values in the
     * hashtable.  The values will enumerated in same order as they were
     * added to the table.  The data in the the returned Vector will not
     * be affected by later modifications to the hashtable, and the
     * hashtable will not be affected by modifications to the returned
     * Vector.
     * @param fromIndex low endpoint (inclusive) of the sub-list.
     * @param toIndex high endpoint (exclusive) of the sub-list.
     * @return A new Vector containing a sub-list of the values in the
     * hashtable.
     * @throws IndexOutOfBoundsException if the endpoint index value is
     * out of range (fromIndex < 0 || toIndex > size).
     * @throws IllegalArgumentException if the endpoint indices are out
     * of order (fromIndex > toIndex).
     */
  public synchronized Vector getValuesVector(int fromIndex,int toIndex)
  {
    return new Vector(valuesVec.subList(fromIndex,toIndex));
  }

    /**
     * Returns a new HashMap object containing the keys and values in the
     * hashtable.  The data in the the returned HashMap will not be
     * affected by later modifications to the hashtable, and the hashtable
     * will not be affected by modifications to the returned HashMap.
     * @return a new HashMap object containing the keys and values
     */
  public synchronized HashMap getHashMap()
  {
    return (HashMap)(hashMapObj.clone());
  }

    /**
     * Returns an enumeration of the keys in the table.  The keys will
     * be enumerated in same order as they were added to the table.
     * The data referenced by the the returned enumeration will not be
     * affected by later modifications to the hashtable.
     * @return an enumeration of the keys in the table
     */
  public synchronized Enumeration keys()
  {
    return ((Vector)(keysVec.clone())).elements();
  }

    /**
     * Returns an enumeration of the values in the hashtable.  The values
     * will be enumerated in same order as they were added to the table.
     * The data referenced by the the returned enumeration will not be
     * affected by later modifications to the hashtable.
     * @return an enumeration of the values in the hashtable
     */
  public synchronized Enumeration elements()
  {
    return ((Vector)(valuesVec.clone())).elements();
  }

    /**
     * Searches for the first occurrence of the given argument, testing for
     * equality using the equals method.
     * @param obj the object to find.
     * @return The index of the first occurrence of the argument in this
     * FifoHashtable; returns -1 if the object is not found.
     */
  public synchronized int indexOf(Object obj)
  {
    return valuesVec.indexOf(obj);
  }

    /**
     * Searches for the first occurrence of the given argument, beginning
     * the search at 'idx', testing for equality using the equals method.
     * @param obj the object to find.
     * @param idx the starting search position.
     * @return The index of the first occurrence of the argument at
     * position 'idx' or later in this FifoHashtable; returns -1 if
     * the object is not found.
     */
  public synchronized int indexOf(Object obj,int idx)
  {
    return valuesVec.indexOf(obj,idx);
  }

    /**
     * Searches for the last occurrence of the given argument, testing for
     * equality using the equals method.
     * @param obj the object to find.
     * @return The index of the last occurrence of the argument in this
     * FifoHashtable; returns -1 if the object is not found.
     */
  public synchronized int lastIndexOf(Object obj)
  {
    return valuesVec.lastIndexOf(obj);
  }

    /**
     * Searches for the last occurrence of the given argument, beginning
     * the search at 'idx', testing for equality using the equals method.
     * @param obj the object to find.
     * @param idx the starting search position.
     * @return The index of the last occurrence of the argument at
     * position 'idx' or earlier in this FifoHashtable; returns -1 if
     * the object is not found.
     */
  public synchronized int lastIndexOf(Object obj,int idx)
  {
    return valuesVec.lastIndexOf(obj,idx);
  }

    /**
     * Searches for the first occurrence of the given key, testing for
     * equality using the equals method.
     * @param obj the key to find.
     * @return The index of the first occurrence of the key in this
     * FifoHashtable; returns -1 if the object is not found.
     */
  public synchronized int indexOfKey(Object obj)
  {
    return keysVec.indexOf(obj);
  }

    /**
     * Searches for the first occurrence of the given key, beginning
     * the search at 'idx', testing for equality using the equals method.
     * @param obj the key to find.
     * @param idx the starting search position.
     * @return The index of the first occurrence of the key at
     * position 'idx' or later in this FifoHashtable; returns -1 if
     * the object is not found.
     */
  public synchronized int indexOfKey(Object obj,int idx)
  {
    return keysVec.indexOf(obj,idx);
  }

    /**
     * Searches for the last occurrence of the given key, testing for
     * equality using the equals method.
     * @param obj the key to find.
     * @return The index of the last occurrence of the key in this
     * FifoHashtable; returns -1 if the object is not found.
     */
  public synchronized int lastIndexOfKey(Object obj)
  {
    return keysVec.lastIndexOf(obj);
  }

    /**
     * Searches for the last occurrence of the given key, beginning
     * the search at 'idx', testing for equality using the equals method.
     * @param obj the key to find.
     * @param idx the starting search position.
     * @return The index of the last occurrence of the key at
     * position 'idx' or earlier in this FifoHashtable; returns -1 if
     * the object is not found.
     */
  public synchronized int lastIndexOfKey(Object obj,int idx)
  {
    return keysVec.lastIndexOf(obj,idx);
  }

    /**
     * Returns the object at the specified index in the FifoHashtable.
     * @param idx the index value to use.
     * @exception ArrayIndexOutOfBoundsException if the index is negative
     * or not less than the current size of the FifoHashtable.
     * @return the object at the specified index in the FifoHashtable.
     */
  public synchronized Object elementAt(int idx)
  {
    return valuesVec.get(idx);
  }

    /**
     * Returns the key at the specified index in the FifoHashtable.
     * @param idx the index value to use.
     * @exception ArrayIndexOutOfBoundsException if the index is negative
     * or not less than the current size of the FifoHashtable.
     * @return the key at the specified index in the FifoHashtable.
     */
  public synchronized Object keyAt(int idx)
  {
    return keysVec.get(idx);
  }

    /**
     * Sets the value object at the specified index of this FifoHashtable
     * to be the specified object.  The key object remains unchanged and
     * the previous value object is discarded.
     * @param obj the new value object to be set.
     * @param idx the specified index; must be greater than or equal to
     * zero and less than the size of this FifoHashtable.
     * @exception ArrayIndexOutOfBoundsException if the index is negative
     * or not less than the current size of the FifoHashtable.
     */
  public synchronized void setElementAt(Object obj,int idx)
  {
         //get key for position; will throw exception if index out of range:
    Object keyObj = keyAt(idx);
    valuesVec.set(idx,obj);   //put new object into values vector
    mapVec.set(idx,new MapEntry(keyObj,obj));
    hashMapObj.put(keyObj,obj);        //put key,new-value into hash map
  }

    /**
     * Removes the entry at the specified index of this FifoHashtable.
     * Each entry in this FifoHashtable with an index greater than the
     * specified index is shifted downward to have an index one smaller
     * than it had previously. The size of this FifoHashtable is decreased
     * by 1.
     * @param idx the specified index; must be greater than or equal to
     * zero and less than the size of this FifoHashtable.
     * @exception ArrayIndexOutOfBoundsException if the index is negative
     * or not less than the current size of the FifoHashtable.
     * @return The entry object that was removed.
     */
  public synchronized Object removeElementAt(int idx)
  {
         //get key obj for pos; will throw exception if idx out of range:
    final Object keyObj = keyAt(idx);
    keysVec.remove(idx);          //remove key object at index
    valuesVec.remove(idx);        //remove value object at index
    mapVec.remove(idx);           //remove map entry object at index
    return hashMapObj.remove(keyObj);  //remove key,value from hash map
  }

    /**
     * Swaps the table entries for the given key objects.  The 'key,value'
     * associations are not changed, but rather the position of the first
     * 'key,value' entry is swapped with the second.
     * @param key1Obj first key value
     * @param key2Obj second key value
     * @return true if successful, false if either of the key objects
     * could not be found in the table.
     */
  public synchronized boolean swapEntries(Object key1Obj,Object key2Obj)
  {
    int pos1,pos2;

    try
    {
      if((pos1=keysVec.indexOf(key1Obj)) < 0 ||
                                        (pos2=keysVec.indexOf(key2Obj)) < 0)
      {  //positions of key objects not found
        return false;        //return error
      }
      keysVec.set(pos1,key2Obj);                 //swap key objects
      keysVec.set(pos2,key1Obj);
      final Object obj = valuesVec.get(pos1);    //swap value objects
      valuesVec.set(pos1,valuesVec.get(pos2));
      valuesVec.set(pos2,obj);
      final Object mapEntry = mapVec.get(pos1);
      mapVec.set(pos1,mapVec.get(pos2));
      mapVec.set(pos2,mapEntry);
      return true;             //return OK flag
    }
    catch(ArrayIndexOutOfBoundsException ex)
    {    //invalid index error
      return false;          //return error
    }
  }

    /**
     * Creates and returns a "subset" of this table.  The returned "subset"
     * table will contain entries whose keys match the values in the given
     * list, in the order specified in the given list.
     * @param keysListObj list of key objects for the "subset" table.
     * @return A new "subset" FifoHashtable object.
     */
  public synchronized FifoHashtable getSubsetTable(List keysListObj)
  {
    final FifoHashtable retTableObj = new FifoHashtable();
    final Iterator iterObj = keysListObj.iterator();
    int idx;
    while(iterObj.hasNext())
    {  //for each key in given list; if matched then copy entry
      if((idx=indexOfKey(iterObj.next())) >= 0)
        retTableObj.put(keyAt(idx),elementAt(idx));
    }
    return retTableObj;
  }

    /**
     * Returns a String of substrings gathered from this hash table.
     * Each table entry is converted to a pair of strings with the
     * format "key"="value" and will appear in the same order as entered
     * into the table.
     * @param sepStr the string that separates the pairs of substrings.
     * @param quoteCharsFlag if true then special characters, such as
     * double-quote and control characters, will be "quoted" (or "escaped")
     * in the substrings using the backslash character (\r \n \t \\ \").
     * @param dontQuoteKeysFlag if true then the keys will not be
     * surrounded by double-quotes ("); if false then the keys will
     * be surrounded by double-quotes (").
     * @param dontQuoteValuesFlag if true then the values will not be
     * surrounded by double-quotes ("); if false then the values will
     * be surrounded by double-quotes (").
     * @return A string of substrings, or an empty string if no substrings
     * could be generated (will not return null).
     */
  public synchronized String toQuotedStrings(String sepStr,
                           boolean quoteCharsFlag,boolean dontQuoteKeysFlag,
                                                boolean dontQuoteValuesFlag)
  {
    final String SPCHARS = "\"\r\n\t\\";
    final String KQC = dontQuoteKeysFlag ? UtilFns.EMPTY_STRING : "\"";
    final String VQC = dontQuoteValuesFlag ? UtilFns.EMPTY_STRING : "\"";
    final StringBuffer retBuff = new StringBuffer();  //create buffer
    final Iterator iterObj = keysVec.iterator();      //get table keys
    Object obj;
    if(iterObj.hasNext())
    {  //at least one object in iteration
      if(quoteCharsFlag)
      {     //"quote" special chars with '\'
        while(true)
        {   //for each key; add string to return buffer
          if((obj=iterObj.next()) != null)
          { //valid object fetched OK; add double-quoted string to buffer
                                       //"quote" special chars with '\':
            retBuff.append(KQC + UtilFns.insertQuoteChars(
                                       obj.toString(),SPCHARS) + KQC + "=");
            if((obj=hashMapObj.get(obj)) != null)
            {    //value for key fetched OK; add double-quoted string
                                       //"quote" special chars with '\':
              retBuff.append(VQC + UtilFns.insertQuoteChars(
                                             obj.toString(),SPCHARS) + VQC);
            }
            else      //value for key not found
              retBuff.append("(null)");   //add indicator to buffer
          }
          if(!iterObj.hasNext())          //if no more elements then
            break;                        //exit loop
          retBuff.append(sepStr);         //put in separator
        }
      }
      else
      {     //don't "quote" special chars with '\'
        while(true)
        {   //for each element; add string to return buffer
          if((obj=iterObj.next()) != null)
          { //valid object fetched OK; add double-quoted string to buffer
            retBuff.append(KQC + obj.toString() + KQC + "=");
            if((obj=hashMapObj.get(obj)) != null)
            {    //value for key fetched OK; add double-quoted string
              retBuff.append(VQC + obj.toString() + VQC);
            }
            else      //value for key not found
              retBuff.append("(null)");   //add indicator to buffer
          }
          if(!iterObj.hasNext())          //if no more elements then
            break;                        //exit loop
          retBuff.append(sepStr);         //put in separator
        }
      }
    }
    return retBuff.toString();         //return string version of bufffer
  }

    /**
     * Returns a String of substrings gathered from this hash table.
     * Each table entry is converted to a pair of strings with the
     * format "key"="value" and will appear in the same order as entered
     * into the table.
     * @param sepChar the character that separates the pairs of substrings.
     * @param quoteCharsFlag if true then special characters, such as
     * double-quote and control characters, will be "quoted" (or "escaped")
     * in the substrings using the backslash character (\r \n \t \\ \").
     * @param dontQuoteKeysFlag if true then the keys will not be
     * surrounded by double-quotes ("); if false then the keys will
     * be surrounded by double-quotes (").
     * @param dontQuoteValuesFlag if true then the values will not be
     * surrounded by double-quotes ("); if false then the values will
     * be surrounded by double-quotes (").
     * @return A string of substrings, or an empty string if no substrings
     * could be generated (will not return null).
     */
  public String toQuotedStrings(char sepChar,
                           boolean quoteCharsFlag,boolean dontQuoteKeysFlag,
                                                boolean dontQuoteValuesFlag)
  {
                             //convert character to String object:
    return toQuotedStrings(new String(new char[] {sepChar}),
                      quoteCharsFlag,dontQuoteKeysFlag,dontQuoteValuesFlag);
  }

    /**
     * Returns a String of substrings gathered from this hash table.
     * Each table entry is converted to a pair of strings with the
     * format "key"="value" and will appear in the same order as entered
     * into the table.
     * @param sepStr the string that separates the pairs of substrings.
     * @param quoteCharsFlag if true then special characters, such as
     * double-quote and control characters, will be "quoted" (or "escaped")
     * in the substrings using the backslash character (\r \n \t \\ \").
     * @param dontQuoteKeysFlag if true then the keys will not be
     * surrounded by double-quotes ("); if false then the keys will
     * be surrounded by double-quotes (").
     * @return A string of substrings, or an empty string if no substrings
     * could be generated (will not return null).
     */
  public String toQuotedStrings(String sepStr,
                           boolean quoteCharsFlag,boolean dontQuoteKeysFlag)
  {
    return toQuotedStrings(sepStr,quoteCharsFlag,dontQuoteKeysFlag,false);
  }
    /**
     * Returns a String of substrings gathered from this hash table.
     * Each table entry is converted to a pair of strings with the
     * format "key"="value" and will appear in the same order as entered
     * into the table.
     * @param sepChar the character that separates the pairs of substrings.
     * @param quoteCharsFlag if true then special characters, such as
     * double-quote and control characters, will be "quoted" (or "escaped")
     * in the substrings using the backslash character (\r \n \t \\ \").
     * @param dontQuoteKeysFlag if true then the keys will not be
     * surrounded by double-quotes ("); if false then the keys will
     * be surrounded by double-quotes (").
     * @return A string of substrings, or an empty string if no substrings
     * could be generated (will not return null).
     */
  public String toQuotedStrings(char sepChar,
                           boolean quoteCharsFlag,boolean dontQuoteKeysFlag)
  {
    return toQuotedStrings(sepChar,quoteCharsFlag,dontQuoteKeysFlag,false);
  }

    /**
     * Returns a String of substrings gathered from this hash table.
     * Each table entry is converted to a pair of strings with the
     * format "key"="value" and will appear in the same order as entered
     * into the table.
     * @param sepChar the character that separates the pairs of substrings.
     * @param quoteCharsFlag if true then special characters, such as
     * double-quote and control characters, will be "quoted" (or "escaped")
     * in the substrings using the backslash character (\r \n \t \\ \").
     * @return A string of substrings, or an empty string if no substrings
     * could be generated (will not return null).
     */
  public String toQuotedStrings(char sepChar,boolean quoteCharsFlag)
  {
                             //convert character to String object:
    return toQuotedStrings(sepChar,quoteCharsFlag,false,false);
  }

    /**
     * Returns a String of substrings gathered from this hash table.
     * Each table entry is converted to a pair of strings with the
     * format "key"="value" and will appear in the same order as entered
     * into the table.
     * @param sepStr the string that separates the pairs of substrings.
     * @param quoteCharsFlag if true then special characters, such as
     * double-quote and control characters, will be "quoted" (or "escaped")
     * in the substrings using the backslash character (\r \n \t \\ \").
     * @return A string of substrings, or an empty string if no substrings
     * could be generated (will not return null).
     */
  public String toQuotedStrings(String sepStr,boolean quoteCharsFlag)
  {
    return toQuotedStrings(sepStr,quoteCharsFlag,false,false);
  }

    /**
     * Returns a String of substrings gathered from this hash table.
     * Each table entry is converted to a pair of strings with the
     * format "key"="value", separated by commas, and appearing in
     * the same order as entered into the table.  Special characters,
     * such as double-quote and control characters, will be "quoted"
     * ("escaped") in the substrings using the backslash character
     * (\r \n \t \\ \").
     * @return A string of substrings, or an empty string if no substrings
     * could be generated (will not return null).
     */
  public String toQuotedStrings()
  {
    return toQuotedStrings(",",true,false,false);
  }

    /**
     * Returns a table of substring pairs parsed from the given string.
     * Each substring pair should be in the format "key="value", and
     * each pair separated by the given separator character.  Special
     * characters, such as double-quote and control characters, may be
     * "quoted" ("escaped") in the substrings using the backslash
     * character (i.e. \").
     * @param inTable a FifoHashtable object to be loaded with items, or
     * null to have a new FifoHashtable object created and loaded.
     * @param str the source string.
     * @param sepChar the character that separates the substrings.
     * @param strictFlag if true then the string format is strictly
     * interpreted--no extraneous spaces are allowed and every substring
     * item must be surrounded by double-quotes.
     * @return A FifoHashtable of string pairs, or null if the proper string
     * format was not found.
     */
  public static FifoHashtable quotedStringsToTable(
           FifoHashtable inTable,String str,char sepChar,boolean strictFlag)
  {
         //table to be returned; given table or new one if null:
    final FifoHashtable retTable = (inTable != null) ? inTable :
                                                        new FifoHashtable();
    final int strLen = str.length();
    int sPos=0,ePos,subLen,q;
    String subStr;
    do
    {    //for each substring in source string; find next separator char
      if((ePos=UtilFns.findCharPos(str,sepChar,sPos)) < sPos)
        ePos = strLen;  //if separator char not found then use end of string
      subStr = str.substring(sPos,ePos);
      if(strictFlag)
      {       //string is to be strictly interpreted
        if((subLen=subStr.length()) <= 0)
          break;        //if empty substring then exit loop
                             //find equals sign in substring:
        if(subLen < 2 || (q=UtilFns.findCharPos(subStr,'=')) < 2 ||
                                                              q >= subLen-1)
        {     //equals sign not found
          return null;       //return error
        }
                             //check for quote characters (""):
        if(subStr.charAt(0) != '\"' || subStr.charAt(q-1) != '\"' ||
              subStr.charAt(q+1) != '\"' || subStr.charAt(subLen-1) != '\"')
        {     //both sets of leading/trailing quotes not found
          return null;       //return error
        }
      }
      else    //string not strictly interpreted
      {
        subStr = subStr.trim();        //trim leading and trailing spaces
        if((subLen=subStr.length()) <= 0)
          break;        //if empty substring then exit loop
                             //find equals sign in substring:
        if(subLen < 2 || (q=UtilFns.findCharPos(subStr,'=')) < 2 ||
                                                              q >= subLen-1)
        {     //equals sign not found
          return null;       //return error
        }
      }
         //remove quote characters from key/value string and add to table:
      retTable.put(UtilFns.removeQuoteChars(subStr.substring(0,q),true),
               UtilFns.removeQuoteChars(subStr.substring(q+1,subLen),true));
    }              //move start position past separator character
    while((sPos=ePos+1) < strLen);     //loop while more data in string
    return retTable;         //return table of substring pairs
  }

    /**
     * Returns a table of substring pairs parsed from the given string.
     * Each substring pair should be in the format "key="value", and
     * each pair separated by the given separator character.  Special
     * characters, such as double-quote and control characters, may be
     * "quoted" ("escaped") in the substrings using the backslash
     * character (i.e. \").
     * The string format is strictly interpreted in that no extraneous
     * spaces are allowed and every substring item must be surrounded by
     * double-quotes.
     * @param inTable a FifoHashtable object to be loaded with items, or
     * null to have a new FifoHashtable object created and loaded.
     * @param str the source string.
     * @param sepChar the character that separates the substrings.
     * @return A FifoHashtable of string pairs, or null if the proper string
     * format was not found.
     */
  public static FifoHashtable quotedStringsToTable(FifoHashtable inTable,
                                                    String str,char sepChar)
  {
    return quotedStringsToTable(inTable,str,sepChar,true);
  }

    /**
     * Returns a table of substring pairs parsed from the given string.
     * Each substring pair should be in the format "key="value", and
     * each pair separated by a comma.  Special characters, such as
     * double-quote and control characters, may be "quoted" ("escaped")
     * in the substrings using the backslash character (i.e. \").
     * The string format is strictly interpreted in that no extraneous
     * spaces are allowed and every substring item must be surrounded by
     * double-quotes.
     * @param inTable a FifoHashtable object to be loaded with items, or
     * null to have a new FifoHashtable object created and loaded.
     * @param str the source string.
     * @return A FifoHashtable of string pairs, or null if the proper string
     * format was not found.
     */
  public static FifoHashtable quotedStringsToTable(FifoHashtable inTable,
                                                                 String str)
  {
    return quotedStringsToTable(inTable,str,',',true);
  }


    /**
     * Returns a table of substring pairs parsed from the given string.
     * Each substring pair should be in the format "key="value", and
     * each pair separated by the given separator character.  Special
     * characters, such as double-quote and control characters, may be
     * "quoted" ("escaped") in the substrings using the backslash
     * character (i.e. \").
     * @param str the source string.
     * @param sepChar the character that separates the substrings.
     * @param strictFlag if true then the string format is strictly
     * interpreted--no extraneous spaces are allowed and every substring
     * item must be surrounded by double-quotes.
     * @return A FifoHashtable of string pairs, or null if the proper string
     * format was not found.
     */
  public static FifoHashtable quotedStringsToTable(String str,char sepChar,
                                                         boolean strictFlag)
  {
    return quotedStringsToTable(null,str,sepChar,strictFlag);
  }

    /**
     * Returns a table of substring pairs parsed from the given string.
     * Each substring pair should be in the format "key="value", and
     * each pair separated by the given separator character.  Special
     * characters, such as double-quote and control characters, may be
     * "quoted" ("escaped") in the substrings using the backslash
     * character (i.e. \").
     * The string format is strictly interpreted in that no extraneous
     * spaces are allowed and every substring item must be surrounded by
     * double-quotes.
     * @param str the source string.
     * @param sepChar the character that separates the substrings.
     * @return A new FifoHashtable of string pairs, or null if the proper string
     * format was not found.
     */
  public static FifoHashtable quotedStringsToTable(String str,char sepChar)
  {
    return quotedStringsToTable(null,str,sepChar,true);
  }

    /**
     * Returns a table of substring pairs parsed from the given string.
     * Each substring pair should be in the format "key="value", and
     * each pair separated by a comma.  Special characters, such as
     * double-quote and control characters, may be "quoted" ("escaped")
     * in the substrings using the backslash character (i.e. \").
     * The string format is strictly interpreted in that no extraneous
     * spaces are allowed and every substring item must be surrounded by
     * double-quotes.
     * @param str the source string.
     * @return A new FifoHashtable of string pairs, or null if the proper
     * string format was not found.
     */
  public static FifoHashtable quotedStringsToTable(String str)
  {
    return quotedStringsToTable(null,str,',',true);
  }

    /**
     * Creates a shallow copy of this hashtable.  All the structure of the
     * hashtable itself is copied, but the keys and values are not cloned.
     * This is a relatively expensive operation.
     * @return a clone of the hashtable.
     */
  public synchronized Object clone()
  {
    return new FifoHashtable((HashMap)(hashMapObj.clone()),
                     (Vector)(keysVec.clone()),(Vector)(valuesVec.clone()),
                     (Vector)(mapVec.clone()));
  }

    /**
     * Returns a string representation of the hashtable in the form of a
     * set of entries, enclosed in braces and separated by the ASCII
     * characters "<tt>,&nbsp;</tt>" (comma and space).  Each entry is
     * rendered as the key, an equals sign <tt>=</tt>, and the associated
     * element, where the <tt>toString</tt> method is used to convert the
     * key and element to strings.  The entries are presented in same order
     * as they were entered.
     * @return  a string representation of this hashtable.
     */
  public synchronized String toString()
  {
    final String nullString="(null)";
    Object obj;

    StringBuffer buff = new StringBuffer(); //create string buffer
    final Iterator iterObj = keysVec.iterator();    //get keys in FIFO order
    buff.append("{");                       //start with opening bracket
    if(iterObj.hasNext())
    {    //more than zero elements available
      while(true)
      {
        if((obj=iterObj.next()) != null)    //fetch key
          buff.append(obj.toString());      //if key not null then add it
        else                                //if null object then
          buff.append(nullString);          //indicate null object
        buff.append("=");
        if((obj=hashMapObj.get(obj)) != null)    //get matching value
          buff.append(obj.toString());      //if not null then add it
        else                                //if null object then
          buff.append(nullString);          //indicate null object
        if(!iterObj.hasNext())
          break;                  //if no more elements then exit loop
        buff.append(", ");        //add separator before next element
      }
    }
    buff.append("}");             //end with closing bracket
    return buff.toString();       //return string version of buffer
  }

  // Views

  private transient Set keySet = null;
  private transient Set entrySet = null;
  private transient Collection values = null;

  /**
   * Returns a read-only set view of the keys contained in this map.
   * The set does not support any add or remove operations.  The
   * 'keySet().iterator()' method may be used to iterate over the keys
   * in this table (as long as the table is not modified between
   * iterations).
   * @return A set view of the keys contained in this map.
   */
  public synchronized Set keySet()
  {
    if (keySet == null)
    {    //key-set object not yet created; create it now

      keySet = new AbstractSet()
          {
            public Iterator iterator()
            {
              return new Iterator()    //return modified iterator that
                  {                    // does not support 'remove()'
                    private final Iterator iterObj = keysVec.iterator();

                    public boolean hasNext()
                    {
                      return iterObj.hasNext();
                    }

                    public Object next()
                    {
                      return iterObj.next();
                    }

                    public void remove()
                    {
                      throw new UnsupportedOperationException();
                    }
                  };
            }

            public int size()
            {
              return keysVec.size();
            }

            public boolean contains(Object o)
            {
              return keysVec.contains(o);
            }
          };
    }
    return keySet;
  }

  /**
   * Returns a read-only collection view of the values contained in this map.
   * The collection does not support any add or remove operations.  The
   * 'values().iterator()' method may be used to iterate over the values
   * in this table (as long as the table is not modified between
   * iterations).
   * @return A collection view of the values contained in this map.
   */
  public synchronized Collection values()
  {
    if (values == null)
    {    //values-collection object not yet created; create it now

      values = new AbstractCollection()
          {
            public Iterator iterator()
            {
              return new Iterator()    //return modified iterator that
                  {                    // does not support 'remove()'
                    private final Iterator iterObj = valuesVec.iterator();

                    public boolean hasNext()
                    {
                      return iterObj.hasNext();
                    }

                    public Object next()
                    {
                      return iterObj.next();
                    }

                    public void remove()
                    {
                      throw new UnsupportedOperationException();
                    }
                  };
            }

            public int size()
            {
              return valuesVec.size();
            }

            public boolean contains(Object o)
            {
              return valuesVec.contains(o);
            }
          };
    }
    return values;
  }

  /**
   * Returns a read-only set view of the mappings contained in this map.
   * The set does not support any add or remove operations.  The
   * 'entrySet().iterator()' method may be used to iterate over the
   * entries in this table (as long as the table is not modified
   * between iterations).
   * @return A set view of the mappings contained in this map.
   * @see Map.Entry
   */
  public synchronized Set entrySet()
  {
    if (entrySet == null)
    {    //entry-set object not yet created; create it now

      entrySet = new AbstractSet()
          {
            public Iterator iterator()
            {
              return new Iterator()    //return modified iterator that
                  {                    // does not support 'remove()'
                    private final Iterator iterObj = mapVec.iterator();

                    public boolean hasNext()
                    {
                      return iterObj.hasNext();
                    }

                    public Object next()
                    {
                      return iterObj.next();
                    }

                    public void remove()
                    {
                      throw new UnsupportedOperationException();
                    }
                  };
            }

            public boolean contains(Object o)
            {
              return FifoHashtable.this.mapVec.contains(o);
            }

            public int size()
            {
              return FifoHashtable.this.mapVec.size();
            }
          };
    }
    return entrySet;
  }

  // Map.Entry interface

  public static class MapEntry implements Map.Entry
  {
    protected final Object key;
    protected final Object value;

    public MapEntry(Object key,Object value)
    {
      this.key = key;
      this.value = value;
    }

    /**
     * Returns the key corresponding to this entry.
     * @return the key corresponding to this entry.
     */
    public Object getKey()
    {
      return key;
    }

    /**
     * Returns the value corresponding to this entry.  If the mapping
     * has been removed from the backing map (by the iterator's
     * <tt>remove</tt> operation), the results of this call are undefined.
     * @return the value corresponding to this entry.
     */
    public Object getValue()
    {
      return value;
    }

    /**
     * This method is not supported.
     * @param value value object.
     * @return Nothing.
     * @throws UnsupportedOperationException Always throws this exception.
     */
    public Object setValue(Object value)
    {
      throw new UnsupportedOperationException();
    }

    /**
     * Compares the specified object with this entry for equality.
     * Returns <tt>true</tt> if the given object is also a map entry and
     * the two entries represent the same mapping.  More formally, two
     * entries <tt>e1</tt> and <tt>e2</tt> represent the same mapping
     * if<pre>
     *     (e1.getKey()==null ?
     *      e2.getKey()==null : e1.getKey().equals(e2.getKey()))  &&
     *     (e1.getValue()==null ?
     *      e2.getValue()==null : e1.getValue().equals(e2.getValue()))
     * </pre>
     * This ensures that the <tt>equals</tt> method works properly across
     * different implementations of the <tt>Map.Entry</tt> interface.
     *
     * @param o object to be compared for equality with this map entry.
     * @return <tt>true</tt> if the specified object is equal to this map
     *         entry.
     */
    public boolean equals(Object o)
    {
      if (!(o instanceof Map.Entry))
        return false;
      Map.Entry e = (Map.Entry)o;
      return ((getKey()==null) ? e.getKey()==null :
              getKey().equals(e.getKey())) && ((getValue()==null) ?
              e.getValue()==null : getValue().equals(e.getValue()));
    }

    /**
     * Returns the hash code value for this map entry.  The hash code
     * of a map entry <tt>e</tt> is defined to be: <pre>
     *     (e.getKey()==null   ? 0 : e.getKey().hashCode()) ^
     *     (e.getValue()==null ? 0 : e.getValue().hashCode())
     * </pre>
     * This ensures that <tt>e1.equals(e2)</tt> implies that
     * <tt>e1.hashCode()==e2.hashCode()</tt> for any two Entries
     * <tt>e1</tt> and <tt>e2</tt>, as required by the general
     * contract of <tt>Object.hashCode</tt>.
     *
     * @return the hash code value for this map entry.
     * @see Object#hashCode()
     * @see Object#equals(Object)
     * @see #equals(Object)
     */
    public int hashCode()
    {
      return ((getKey()==null) ? 0 : getKey().hashCode()) ^
             ((getValue()==null) ? 0 : getValue().hashCode());
    }
  }
}
