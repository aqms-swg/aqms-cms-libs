//FileDataMonitor.java:  Reads in the data from a file and then monitors
//                       the file for changes.
//
//   6/7/2005 -- [ET]  Initial version.
//  11/9/2006 -- [ET]  Modified to always close URL connection to file.
//  1/17/2008 -- [ET]  Added optional 'allowMissingFlag' parameter to
//                     constructor.
//  1/22/2009 -- [ET]  Added optional 'forceFileReadFlag' parameter to
//                     'checkReadFileData()' method.
//  3/16/2011 -- [ET]  Added indicator to timeout-error message of whether
//                     or not last-modified time for file was successfully
//                     fetched before timeout occurred; added recheck of
//                     file last-modified time after read of data.
//

package com.isti.util;

import java.io.File;
import java.io.InputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.net.URL;
import java.net.URLConnection;

/**
 * Class FileDataMonitor reads in the data from a file and then monitors
 * the file for changes.  The last-modified date on the file is used to
 * detect when its data has changed.  The file may be specified as a
 * local pathname or as a remote URL.  The file is accessed via a separate
 * thread (using a timeout) to prevent the possibility of having the
 * calling thread held up because of an indefinite I/O block.
 */
public class FileDataMonitor extends ErrorMessageMgr
{
    /** Empty array used to indicate error. */
  public static final byte [] ERROR_ARRAY = new byte[0];
  public static final byte [] NO_FILE_ARRAY = new byte[0];
  private final String fileNameString;
  private final boolean allowMissingFlag;
  private URL fileAccessUrlObj = null;
  private long fileLastModifiedTimeMs = 0;
  private long prevFileLastModTimeMs = 0;

  /**
   * Creates a file-data-monitor object.
   * @param fNameStr name of file to be read and monitored.
   * @param allowMissingFlag true to allow the file to be missing (and
   * not generate an error); false to generate an error when the file
   * is missing (the default).
   */
  public FileDataMonitor(String fNameStr, boolean allowMissingFlag)
  {
    fileNameString = fNameStr;
    this.allowMissingFlag = allowMissingFlag;
  }

  /**
   * Creates a file-data-monitor object.
   * @param fNameStr name of file to be read and monitored.
   */
  public FileDataMonitor(String fNameStr)
  {
    this(fNameStr,false);
  }

  /**
   * Checks if the file has been modified and returns its contents if
   * so.  The first time this method is called the file data will be
   * returned.
   * @param timeOutMs timeout value for file access, or 0 for infinite.
   * @param maxReadBytesLimit maximum number of bytes to read from file,
   * or 0 for no limit.
   * @param forceFileReadFlag true to force reading of file data; false
   * to only read file data if file-modified time changed.
   * @return A byte array containing the file data, 'null' if the file
   * has not been modified, 'NO_FILE_ARRAY' if the file was not found
   * (and the 'allowMissingFlag' constructor parameter was set to 'true),
   * or 'ERROR_ARRAY' if an error occurred (in which case the
   * 'getErrorMessageString()' method may be used to fetch the
   * error message).
   */
  public byte [] checkReadFileData(int timeOutMs, long maxReadBytesLimit,
                                                  boolean forceFileReadFlag)
  {
    clearErrorMessageString();         //clear any previous error message
    fileAccessUrlObj = null;           //clear any previous URL
              //create and start file-data-access thread:
    final DataAccessThread dataAccessThreadObj = new DataAccessThread(
                     fileNameString,allowMissingFlag,fileLastModifiedTimeMs,
                                       maxReadBytesLimit,forceFileReadFlag);
    final int threadSleepDelay = 25;
    while(true)
    {    //loop while waiting for access to be completed
      try
      {       //delay between checks:
        Thread.sleep(threadSleepDelay);
      }
      catch(InterruptedException ex)
      {       //delay interrupted; exit thread
        return null;
      }
      if(dataAccessThreadObj.accessCompletedFlag)
        break;          //if access completed then exit loop
      if(timeOutMs > 0 && (timeOutMs-=threadSleepDelay) <= 0)
      {  //timeout was given and has been reached
              //set message depending on whether or not last-modified
              // time for file was fetched OK:
        final String str = dataAccessThreadObj.lastModTimeFetchedFlag ?
                                                      "read from" : "check";
        setErrorMessageString("Timeout while attempting to " + str +
                                        " file \"" + fileNameString + "\"");
        return ERROR_ARRAY;
      }
    }
              //enter file-access URL from thread object:
    fileAccessUrlObj = dataAccessThreadObj.fileAccessUrlObj;
              //save previous last-modified time for file:
    prevFileLastModTimeMs = fileLastModifiedTimeMs;
              //enter last-modified time from thread object:
    fileLastModifiedTimeMs = dataAccessThreadObj.fileLastModifiedTimeMs;
              //if error then enter error message from thead object:
    if(dataAccessThreadObj.returnedByteArray == ERROR_ARRAY)
      setErrorMessageString(dataAccessThreadObj.getErrorMessageString());
    return dataAccessThreadObj.returnedByteArray;
  }

  /**
   * Checks if the file has been modified and returns its contents if
   * so.  The first time this method is called the file data will be
   * returned.
   * @param timeOutMs timeout value for file access, or 0 for infinite.
   * @param maxReadBytesLimit maximum number of bytes to read from file,
   * or 0 for no limit.
   * @return A byte array containing the file data, 'null' if the file
   * has not been modified, 'NO_FILE_ARRAY' if the file was not found
   * (and the 'allowMissingFlag' constructor parameter was set to 'true),
   * or 'ERROR_ARRAY' if an error occurred (in which case the
   * 'getErrorMessageString()' method may be used to fetch the
   * error message).
   */
  public byte [] checkReadFileData(int timeOutMs, long maxReadBytesLimit)
  {
    return checkReadFileData(timeOutMs,maxReadBytesLimit,false);
  }

  /**
   * Returns the name of the file being monitored.
   * @return The file name string.
   */
  public String getFileNameString()
  {
    return fileNameString;
  }

  /**
   * Returns the URL used to access the file in the last call to the
   * 'checkReadFileData()' method.
   * @return The URL used to access the file in the last call to the
   * 'checkReadFileData()' method, or null if none available.
   */
  public URL getFileAccessUrlObj()
  {
    return fileAccessUrlObj;
  }

  /**
   * Returns the last-modified time for the file in the last call to the
   * 'checkReadFileData()' method.
   * @return The last-modified time for the file in the last call to the
   * 'checkReadFileData()' method, or 0 if not available.
   */
  public long getFileLastModifiedTimeMs()
  {
    return fileLastModifiedTimeMs;
  }

  /**
   * Returns the previous last-modified time for the file in the last call
   * to the 'checkReadFileData()' method.
   * @return The previous last-modified time for the file in the last call
   * to the 'checkReadFileData()' method, or 0 if not available.
   */
  public long getPrevFileLastModTimeMs()
  {
    return prevFileLastModTimeMs;
  }


  /**
   * Class DataAccessThread defines a thread for accessing the file and
   * its data.
   */
  private static class DataAccessThread extends ErrorMessageMgr
                                                         implements Runnable
  {
    private final String fileNameString;
    private final boolean allowMissingFlag;
    private final long maxReadBytesLimit;
    private final boolean forceFileReadFlag;
    private URL fileAccessUrlObj = null;
    private URLConnection urlConnectionObj = null;
    private long fileLastModifiedTimeMs;
    private boolean lastModTimeFetchedFlag = false;
    private boolean accessCompletedFlag = false;
    private byte [] returnedByteArray = null;

    /**
     * Creates a data-access-thread object and starts the access thread.
     * @param fileNameString name of file to be accessed.
     * @param allowMissingFlag true to allow the file to be missing (and
     * not generate an error); false to generate an error when the file
     * is missing.
     * @param fileLastModifiedTimeMs file-modified time to check against.
     * @param maxReadBytesLimit maximum number of bytes to read from file,
     * or 0 for no limit.
     * @param forceFileReadFlag true to force reading of file data; false
     * to only read file data if file-modified time changed.
     */
    public DataAccessThread(String fileNameString, boolean allowMissingFlag,
                        long fileLastModifiedTimeMs, long maxReadBytesLimit,
                                                  boolean forceFileReadFlag)
    {
      this.fileNameString = fileNameString;
      this.allowMissingFlag = allowMissingFlag;
      this.fileLastModifiedTimeMs = fileLastModifiedTimeMs;
      this.maxReadBytesLimit = maxReadBytesLimit;
      this.forceFileReadFlag = forceFileReadFlag;
      (new Thread(this,
            ("FileDataMonitorAccess-"+System.currentTimeMillis()))).start();
    }

    /**
     * Executing method for thread.
     */
    public void run()
    {
      returnedByteArray = doCheckReadFileData();
      accessCompletedFlag = true;
    }

    /**
     * Checks if the file has been modified and returns its contents if
     * so.
     * @return A byte array containing the file data, 'null' if the file
     * has not been modified, or 'ERROR_ARRAY' if an error occurred (in
     * which case the 'getErrorMessageString()' method may be used to
     * fetch the error message).
     */
    public byte [] doCheckReadFileData()
    {
      byte [] retByteArr = null;
      InputStream inStmObj = null;
      try
      {            //open connection to URL:
        if((retByteArr=openFileConnection()) != null)
          return retByteArr;      //if error then return indicator
              //fetch last-modified time for monitored file:
        final long lastModTime = fetchLastModTime();
        if(lastModTime > 0)
        {   //last-modified time fetched OK
          lastModTimeFetchedFlag = true;         //indicate fetched OK
          if(lastModTime != fileLastModifiedTimeMs || forceFileReadFlag)
          {      //last-modified time changed or force-read flag set
            fileLastModifiedTimeMs = lastModTime;     //update tracking var
            try
            {       //fetch input stream for URL connection:
              if((inStmObj=urlConnectionObj.getInputStream()) == null)
              {     //null handle returned
                setErrorMessageString("Unable to fetch input stream " +
                                     "for file \"" + fileNameString + "\"");
                return ERROR_ARRAY;         //return error indicator
              }
            }
            catch(Exception ex)
            {       //error fetching input stream
              setErrorMessageString("Error fetching input stream " +
                             "for file \"" + fileNameString + "\":  " + ex);
              return ERROR_ARRAY;           //return error indicator
            }
            final ByteArrayOutputStream outStmObj =
                                              new ByteArrayOutputStream();
            try
            {           //mark time before start of read:
              final long sTimeVal = System.currentTimeMillis();
                        //read data from file:
              FileUtils.transferStream(inStmObj,outStmObj,maxReadBytesLimit);
                        //need to close and reopen connection to file
                        // to get updated last-modified time:
              inStmObj.close();            //close input stream
                             //if read took less than a second then
                             // delay a second before rechecking file:
              if(System.currentTimeMillis() - sTimeVal < 1000)
                Thread.sleep(1000);
              openFileConnection();        //reopen input stream
              inStmObj = urlConnectionObj.getInputStream();
                        //if last-modified time for file is same then
                        // return read data; otherwise null for no change
                        // (because file is being updated):
              if(fetchLastModTime() == lastModTime)
                retByteArr = outStmObj.toByteArray();
              else
                retByteArr = null;
              inStmObj.close();            //close input stream
            }
            catch(Exception ex)
            {       //error reading file data
              setErrorMessageString("Error reading from file \"" +
                                           fileNameString + "\":  " + ex);
              inStmObj.close();        //close input stream
              return ERROR_ARRAY;      //return error indicator
            }
            return retByteArr;         //return read data (or null)
          }
                   //last-modified time not changed
                   //setup to return no-change indicator
        }
        else
        {     //unable to fetch last-modified time
          setErrorMessageString("Unable to fetch last-modified time " +
                                    "for file \""  + fileNameString + "\"");
          retByteArr = ERROR_ARRAY;    //setup to return error indicator
        }
      }
      catch(Exception ex)
      {    //some kind of exception error; log it
        setErrorMessageString("Error accessing file \""  +
                                               fileNameString + "\":  " + ex);
        retByteArr = ERROR_ARRAY;      //setup to return error indicator
      }
      try     //close URL connection to file:
      {            //fetch input stream for URL connection:
        if(urlConnectionObj != null &&
                       (inStmObj=urlConnectionObj.getInputStream()) != null)
        {     //URL connection and input stream for URL connection OK
          inStmObj.close();       //close input stream (and URL connection)
        }
      }
      catch(Exception ex)
      {       //error fetching or closing input stream; ignore
      }
      return retByteArr;
    }

    /**
     * Opens the URL connection to the file.
     * @return null if successful, ERROR_ARRAY if error, or NO_FILE_ARRAY
     * if file could not be opened.
     */
    private byte [] openFileConnection()
    {
      try
      {            //open connection to URL:
        if((fileAccessUrlObj=FileUtils.fileMultiOpenInputURL(
                                                   fileNameString)) != null)
        {  //URL created for file and file found OK
          urlConnectionObj = fileAccessUrlObj.openConnection();
        }
        else
        {  //unable to find file
          if(allowMissingFlag)         //if missing file allowed then
            return NO_FILE_ARRAY;      //just return no data
          setErrorMessageString("Unable to find file \"" +
                                                     fileNameString + "\"");
          return ERROR_ARRAY;          //return error indicator
        }
      }
      catch(IOException ex)
      {       //some kind of exception error
        setErrorMessageString("Error accessing file \"" +
                                             fileNameString + "\":  " + ex);
        return ERROR_ARRAY;            //return error indicator
      }
      return null;
    }

    /**
     * Fetches the last-modified time value for the monitored file.
     * @return last-modified time value (ms since 1/1/1970), or 0 if error.
     */
    private long fetchLastModTime()
    {
      long lastModTime;
      try
      {  //fetch last-modified time for file:
        lastModTime = urlConnectionObj.getLastModified();
      }
      catch(Exception ex)
      {  //some kind of exception error
        lastModTime = 0;          //indicate time not fetched
      }
      if(lastModTime <= 0)
      {  //unable to fetch last-modified time value
        try
        {   //access URL as a file and fetch its last-mod time value:
          lastModTime =
                      (new File(fileAccessUrlObj.getPath())).lastModified();
        }
        catch(Exception ex)
        {      //unable to fetch last-modified time via file
          lastModTime = 0;        //indicate time not fetched
        }
      }
      return lastModTime;
    }
  }
}
