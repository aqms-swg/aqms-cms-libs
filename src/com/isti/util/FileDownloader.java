//FileDownloader.java:  Downloads a file with optional dialogs.
//
//  6/11/2004 -- [KF]  Initial version.
//  6/24/2004 -- [KF]  Use terminate flag in base class,
//                     deprecated 'terminate' method.
//   8/6/2004 -- [KF]  Made global logging use null output if not set.
//  8/11/2004 -- [ET]  Class renamed from "DownloadFile" to "FileDownloader";
//                     added 'requestDialogFocus()' method.
//  9/10/2004 -- [ET]  Added 'isDownloadDone()' method; improved error
//                     message for 'UnknownHostException'; modified to
//                     remove zero-length file created when error occurs
//                     during download attempt; fixed problem where
//                     thread terminate ignored if "fileUrl.openStream()"
//                     threw exception.
//  9/13/2004 -- [ET]  Modified to retain the last-modified date/time
//                     values for unzipped files.
//   5/5/2006 -- [ET]  Added optional 'unzipDownloadedFileFlag' parameter
//                     to constructor; modified to extend 'IstiNotifyThread'
//                     instead of 'IstiThread'; added 'getOutputFileObj()'
//                     and 'setCallBackObj()' methods.
//  5/10/2006 -- [ET]  Modified to not log stack trace if download failure
//                     is via file-not-found or unknown-host exception.
//  1/29/2009 -- [KF]  Added 'setBackupFile()', 'setCheckLastModified()' and
//                     'setDeleteFileAfterUnzip()' methods.
//   2/3/2009 -- [KF]  Added 'checkConnection()', 'checkHttpURLConnection()'
//                     and 'getBackupFile()' methods.
//  7/20/2010 -- [KF]  Added 'isCloseWhenDone' and 'setCloseWhenDone' methods.
//  3/25/2013 -- [KF]  Added 'createURL', 'createOutputFileObject',
//                     'getFileNameStr', 'setFileNameStr', 'getDialogObj'
//                     and 'getProgressBarObj' methods.
//  3/25/2019 -- [KF]  Added support for downloading multiple files.
//

package com.isti.util;

import java.io.Closeable;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.URI;
import java.net.URL;
import java.net.URLConnection;
import java.net.UnknownHostException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.zip.ZipFile;

/**
 * Class FileDownloader downloads a file with optional dialogs.
 */
public class FileDownloader extends IstiNotifyThread
{
  /** The path-separator */
  public static final String PATH_SEPARATOR = ";";

  /**
   * Create an immutable URL list.
   * @param fileNames the file names optionally using the path-separator.
   * @return the immutable URL list or an empty list if no file names.
   * @throws Exception if the file names cannot be parsed.
   * @throws NullPointerException if the file names are null.
   * @see #PATH_SEPARATOR
   */
  public static List<URL> createUrlList(String ... fileNames) throws Exception
  {
    int beginIndex, endIndex;
    String str;
    URI uri;
    URL url;
    URI lastUri = null;
    List<URL> urlList = Collections.emptyList();
    for (int index = 0; index < fileNames.length; index++)
    {
      beginIndex = 0;
      str = fileNames[index];
      do
      {
        // if no path separator
        if ((endIndex = str.indexOf(PATH_SEPARATOR, beginIndex)) < 0)
        {
          // use end of string
          endIndex = str.length();
        }
        uri = new URI(str.substring(beginIndex, endIndex));
        // if last URI exists and this URI is relative
        if (lastUri != null && !uri.isAbsolute())
        {
          // resolve the last URI against this URI
          uri = lastUri.resolve(uri);
        }
        // create the URL
        url = uri.toURL();
        // save this URI
        lastUri = uri;
        beginIndex = endIndex + 1;
        // if this is the first URL
        if (urlList.isEmpty())
        {
          // if there are no more URLs
          if (beginIndex >= str.length() && index + 1 >= fileNames.length)
          {
            return Collections.singletonList(url);
          }
          urlList = new ArrayList<URL>();
        }
        urlList.add(url);
      } while (beginIndex < str.length());
    }
    if (!urlList.isEmpty())
    {
      urlList = Collections.unmodifiableList(urlList);
    }
    return urlList;
  }

  /**
   * Get the file name string.
   * @param urlList the immutable URL list.
   * @return the file name string
   * @throws Exception if the URL list is null or empty.
   */
  protected static final String getFileNameStr(List<URL> urlList) throws Exception
  {
    if (urlList.size() == 1)
    {
      return urlList.get(0).toString();
    }
    else
    {
      String s = urlList.get(0).toString();
      StringBuilder sb = new StringBuilder(s);
      for (int index = 1; index < urlList.size(); index++)
      {
        s = urlList.get(index).toString();
        sb.append(PATH_SEPARATOR);
        sb.append(s);
      }
      return sb.toString();
    }
  }

  /**
   * Create an immutable URL list only the specified file name.
   * @param fileNameStr the file name string.
   * @return the immutable URL list.
   * @throws Exception if the file names cannot be parsed.
   */
  private static List<URL> singletonList(String fileNameStr) throws Exception
  {
    final URL url = new URL(fileNameStr);
    return Collections.singletonList(url);
  }

  private List<URL> _urlList;
  private final File _outputDir;
  private final IstiDialogInterface dialogObj;
  private final ProgressIndicatorInterface progressBarObj;
  private final boolean unzipDownloadedFileFlag;
  private CallBackStringParam callBackObj = null;
  private boolean backupFileFlag = false;
  private boolean checkLastModifiedFlag = false;
  private boolean deleteFileAfterUnzipFlag = true;
  private boolean checkConnectionFlag = true;

  private int retryDelayTimeMS = 1000;  //default to 1 second between attempts
  private int maxRetryCount = 10;       //default to up to 10 retries

  private long downloadLength = 0;          //download length
  private long maxProgressVal = 0;          //maximum progress value
  private List _outputFileList = Collections.emptyList();
  private boolean downloadDoneFlag = false; //true after attempt finished
  private boolean closeWhenDoneFlag = true; // true to close when done
  private boolean outputFirstDirFlag = false; // true to output to the first directory
  //sync obj for finished operations:
  private final Object threadFinishedSyncObj = new Object();

  /**
   * Creates a file download thread.
   * It downloads the specified file to the output directory.
   * @param urlList the immutable URL list.
   * @param outputDir the output directory.
   * @param callBackObj if not null then a call-back object whose method
   * is called (after the download is complete) with a string parameter
   * containing null if successful or an error message if an error occurred.
   * @param dialogObj the dialog object or null for none.
   * @param progressBarObj the progress bar object or null for none.
   * @param unzipDownloadedFileFlag true to unzip file after download (if
   * it is a ZIP file); false to not unzip file.
   * @throws Exception if the URL list is null or empty.
   */
  public FileDownloader(List<URL> urlList, File outputDir,
      CallBackStringParam callBackObj, IstiDialogInterface dialogObj,
      ProgressIndicatorInterface progressBarObj,
      boolean unzipDownloadedFileFlag) throws Exception
  {
    super("FileDownloader-" + nextThreadNum());
    LogFile.getGlobalLogObj(false).debug3(
        "FileDownloader:  " + getFileNameStr(urlList));
    // save parameters
    this._urlList = urlList;
    this._outputDir = outputDir;
    this.callBackObj = callBackObj;
    this.dialogObj = dialogObj;
    this.progressBarObj = progressBarObj;
    this.unzipDownloadedFileFlag = unzipDownloadedFileFlag;
  }

  /**
   * Creates a file download thread.
   * It downloads the specified file to the output directory and if the file
   * is a ZIP file it unzips the file.
   * @param fileNameStr the installer file name string.
   * @param outputDir the output directory.
   * @param callBackObj if not null then a call-back object whose method
   * is called (after the download is complete) with a string parameter
   * containing null if successful or an error message if an error occurred.
   * @param dialogObj the dialog object or null for none.
   * @param progressBarObj the progress bar object or null for none.
   * @throws Exception if the file name string cannot be parsed.
   */
  public FileDownloader(String fileNameStr, File outputDir,
      CallBackStringParam callBackObj, IstiDialogInterface dialogObj,
      ProgressIndicatorInterface progressBarObj) throws Exception
  {
    this(fileNameStr,outputDir,callBackObj,dialogObj,progressBarObj,true);
  }

  /**
   * Creates a file download thread.
   * It downloads the specified file to the output directory.
   * @param fileNameStr the installer file name string.
   * @param outputDir the output directory.
   * @param callBackObj if not null then a call-back object whose method
   * is called (after the download is complete) with a string parameter
   * containing null if successful or an error message if an error occurred.
   * @param dialogObj the dialog object or null for none.
   * @param progressBarObj the progress bar object or null for none.
   * @param unzipDownloadedFileFlag true to unzip file after download (if
   * it is a ZIP file); false to not unzip file.
   * @throws Exception if the file name string cannot be parsed.
   */
  public FileDownloader(String fileNameStr, File outputDir,
      CallBackStringParam callBackObj, IstiDialogInterface dialogObj,
      ProgressIndicatorInterface progressBarObj,
      boolean unzipDownloadedFileFlag) throws Exception
  {
    this(singletonList(fileNameStr), outputDir, callBackObj, dialogObj,
        progressBarObj, unzipDownloadedFileFlag);
  }

  /**
   * Add the output file.
   * @param outputFile the output file.
   */
  protected void addOutputFileObj(File outputFile)
  {
	  synchronized (this)
	  {
		  if (_outputFileList == Collections.emptyList())
		  {
			  _outputFileList = new ArrayList();
		  }
		  _outputFileList.add(outputFile);
	  }
  }

  /**
   * Backup the output file.
   * @param outputFile the output file.
   */
  protected void backupOutputFile(File outputFile)
  {
    outputFile.renameTo(getBackupFile(outputFile));
  }

  /**
   * Checks the connection.
   * @param urlCon the URL connection.
   * @return the error message or null if none.
   * @exception IOException if an I/O error has occurred.
   */
  protected String checkConnection(final URLConnection urlCon)
      throws IOException
  {
    String errorMessage = null;
    if (urlCon instanceof HttpURLConnection)
    {
      errorMessage = checkHttpURLConnection((HttpURLConnection)urlCon);
    }
    return errorMessage;
  }

  /**
   * Checks the HTTP connection.
   * @param htpUrlCon the HTTP URL connection.
   * @return the error message or null if none.
   * @exception IOException if an I/O error has occurred.
   */
  protected String checkHttpURLConnection(final HttpURLConnection htpUrlCon)
      throws IOException
  {
    String errorMessage = null;
    final int responseCode = htpUrlCon.getResponseCode();
    if (responseCode != HttpURLConnection.HTTP_OK)
    {
      errorMessage = responseCode + " " + htpUrlCon.getResponseMessage();
    }
    return errorMessage;
  }

  /**
   * Checks the last modified time.
   * @param urlCon the URL connection.
   * @param outputFile the output file.
   * @return the last modified time or -1 if the download should be terminated.
   */
  protected long checkLastModified(
		  final URLConnection urlCon, final File outputFile)
  {
    final long lastModified = urlCon.getLastModified();
    // if checking last modified and last modified is equal
    if (checkLastModifiedFlag && outputFile.lastModified() == lastModified)
    {
    	return -1;
    }
    return lastModified;
  }

  /**
   * Create the output file object.
   * @param outputDir the output directory.
   * @param fileUrl the file URL.
   * @return the output file object.
   */
  protected File createOutputFileObject(File outputDir, URL fileUrl)
  {
    final File inputFile = new File(fileUrl.getFile());
    //use 'File' object to get trailing filename from URL:
    final String inputFileNameStr = inputFile.getName();
    return new File(outputDir, inputFileNameStr);
  }

  /**
   * Performs thread-finished operations.
   * @param errMsgStr error message to be entered into the call-back
   * object, or null for none.
   */
  protected void doThreadFinished(String errMsgStr)
  {
    LogFile.getGlobalLogObj(false).debug3(
                  "FileDownloader:  Performing thread-finished operations");
    final CallBackStringParam callBackObj = this.callBackObj;
    if(callBackObj != null)                    //if call-back given then
      callBackObj.callBackMethod(errMsgStr);   //enter result string
  }

  /**
   * Gets the backup file for the specified output file.
   * @param outputFile the output file.
   * @return the backup file.
   */
  protected File getBackupFile(File outputFile)
  {
    return FileUtils.createUnusedFileNameObj(
       outputFile.getName(), outputFile.getParentFile(), true, true);
  }

  /**
   * Get the dialog object.
   * @return the dialog object or null for none.
   */
  public IstiDialogInterface getDialogObj()
  {
    return dialogObj;
  }

  /**
   * Get the file name string.
   * @return the file name string
   */
  public String getFileNameStr()
  {
    try
    {
      return getFileNameStr(getUrlList());
    }
    catch (Exception ex)
    {
      // should never happen
      return UtilFns.EMPTY_STRING;
    }
  }

  /**
   * Get the output directory for the specified URL.
   * @param url the URL.
   * @return the output directory.
   */
  protected File getOutputDir(URL url)
  {
    if (outputFirstDirFlag)
    {
      final String[] names = this._outputDir.list();
      if (names != null)
      {
        for (String name : names)
        {
          final File file = new File(_outputDir, name);
          if (file.isDirectory())
          {
            return file;
          }
        }
      }
    }
    return _outputDir;
  }

  /**
   * Returns the file object for the last file downloaded (or attempted).
   * @return The file object for the last file downloaded, or null if none.
   */
  public File getOutputFileObj()
  {
	  return getOutputFileObj(-1);
  }

  /**
   * Returns the file object for the file downloaded (or attempted).
   * @param index the file index or -1 for the last.
   * @return The file object for the file downloaded, or null if none.
   */
  public File getOutputFileObj(int index)
  {
	File outputFile = null;
	synchronized (this)
	{
		Object obj;
		final int size = _outputFileList.size();
		if (index < 0)
		{
			index = size - 1;
		}
		if (index >= 0 && index < size &&
				(obj = _outputFileList.get(index)) instanceof File)
		{
			outputFile = (File) obj;
		}
	}
    return outputFile;
  }

  /**
   * Get the progress bar object.
   * @return the the progress bar object or null for none.
   */
  public ProgressIndicatorInterface getProgressBarObj()
  {
    return progressBarObj;
  }

  /**
   * Get the immutable URL list.
   * @return the immutable URL list.
   */
  public List<URL> getUrlList()
  {
    synchronized (this)
    {
      return this._urlList;
    }
  }

  /**
   * Initialize the progress bar.
   */
  protected void initProgressBar()
  {
    maxProgressVal = downloadLength;
    if (maxProgressVal > Integer.MAX_VALUE) //if max is too large then
      maxProgressVal = Integer.MAX_VALUE;   //set max to largest value
    progressBarObj.setMaximum((int)maxProgressVal);
  }

  /**
   * Determines if dialog is closed when the download is done.
   * @return true if dialog is closed when the download is done, false
   *         otherwise.
   */
  public boolean isCloseWhenDone() {
    return closeWhenDoneFlag;
  }

  /**
   * Returns the completion status of the download attempt.
   * @return true if the download attempt has finish; false if not.
   */
  public boolean isDownloadDone()
  {
    return downloadDoneFlag;
  }

  /**
   * Determine if output is going to the first directory.
   * @return true if output is going to the first directory, false otherwise.
   */
  public boolean isOutputFirstDirFlag()
  {
    return outputFirstDirFlag;
  }

  /**
   * Perfoms a 'requestFocus()' on the currently-displayed dialog (if any).
   */
  public void requestDialogFocus()
  {
    try
    {
      if(dialogObj != null)
        dialogObj.requestFocus();
    }
    catch(Exception ex)
    {    //some kind of exception error; log it
      LogFile.getGlobalLogObj(false).debug(
                             "FileDownloader.requestDialogFocus():  " + ex);
      LogFile.getGlobalLogObj(false).debug(UtilFns.getStackTraceString(ex));
    }
  }

  /**
   * Runs the thread.
   */
  public void run()
  {
	  if(isTerminated())
	  {    //thread terminated
		  downloadDoneFlag = true;    //indicate download attempt finished
		  LogFile.getGlobalLogObj(false).debug(
				  "FileDownloader:  Terminate flag set at startup; aborting");
		  return;
	  }
	  String errMsgStr = null;
	  try
	  {
		  final List<URL> urlList = getUrlList();
		  for (URL url : urlList)
		  {
			  LogFile.getGlobalLogObj(false).debug3(
					  "FileDownloader:  Started download for \"" +
							  url.toString() + "\"");
			  errMsgStr = run(getOutputDir(url), url);
			  if (errMsgStr != null || isTerminated())
			  {
				  break;
			  }
		  }
	  }
	  catch (Exception ex)
	  {
		  errMsgStr = "Run error: " + ex.toString();
	  }
	  setDownloadDone(errMsgStr);
  }

  /**
   * Download the specified file.
   * @param outputDir the output directory.
   * @param fileUrl the file URL.
   * @return the error message or null if none.
   */
  protected String run(final File outputDir, final URL fileUrl)
  {
    String errMsgStr = null;
    File outputFile = null;           //output file
    Object zipFileObj = null;         //ZIP file object
    InputStream in = null;            //input stream
    OutputStream out = null;          //output stream
    long lastModified = 0;            //last modified time
    try
    {
      //if the directory does not exist and can't be created or
      // is not a directory
      if (!outputDir.exists() ? !outputDir.mkdirs() :
                                                   !outputDir.isDirectory())
      {
        errMsgStr = "Unable to create output directory: " + outputDir;
        return errMsgStr;
      }

      if (dialogObj != null)
      {
        dialogObj.show();       //display dialog popup
      }

      final URLConnection fileUrlCon = fileUrl.openConnection();
      if (checkConnectionFlag)  //if the connection should be checked
      {
        if ( (errMsgStr = checkConnection(fileUrlCon)) != null)
        {
          return errMsgStr;
        }
      }

      if(!isTerminated())
      {    //thread not terminated; fetch file length
        downloadLength = fileUrlCon.getContentLength();

        //create File object to filename:
        outputFile = createOutputFileObject(outputDir, fileUrl);
        lastModified = checkLastModified(fileUrlCon, outputFile);
        if (lastModified < 0)
        {
        	return errMsgStr;
        }

        if (backupFileFlag && outputFile.exists())
        {
            backupOutputFile(outputFile);
        }

        if (progressBarObj != null)
        {
          initProgressBar();
        }

        try
        {
          out = new FileOutputStream(outputFile);
        }
        catch (Exception ex)
        {
          errMsgStr = "Unable to open output directory: " + outputDir;
          return errMsgStr;
        }

        LogFile.getGlobalLogObj(false).debug3(
                             "FileDownloader:  Opening input stream to \"" +
                            		 fileUrl + "\"");
        int retryCount = 0;
        while(!isTerminated())
        {       //loop while retrying open and transfer of data
          try
          {
            in = fileUrl.openStream();
            if(isTerminated())  //if thread is terminated
            {
              break;
            }
            LogFile.getGlobalLogObj(false).debug3(
                "FileDownloader:  Opening output stream to \"" +
                outputFile + "\"");
            //transfer the ZIP file:
            LogFile.getGlobalLogObj(false).debug3(
                "FileDownloader:  Starting transfer of data");
            transferStream(in, out);
            LogFile.getGlobalLogObj(false).debug3(
                "FileDownloader:  Finished transfer of data");
            break;
          }
          catch(Exception ex)
          {
            if(++retryCount >= maxRetryCount)    //if too many attempts then
            {
              errMsgStr = "Unable to transfer file: " + fileUrl;
              return errMsgStr;
            }
            FileUtils.close(out);  //close the output stream
            out = null;                   //indicate closed
            waitForNotify(retryDelayTimeMS);     //delay between attempts
            LogFile.getGlobalLogObj(false).debug(
                "FileDownloader:  Retrying transfer from \"" +
                fileUrl + "\" after error:  " + ex);
          }
        }
        FileUtils.close(in);  //close the input stream
        in = null;
        FileUtils.close(out);  //close the output stream
        out = null;

        if(!isTerminated())  //if thread is not terminated
        {
          if (unzipDownloadedFileFlag)
          {
        	  zipFileObj = unzipZipFile(outputFile, outputDir);
          }
        }
        else
        {     //terminate flag set
          LogFile.getGlobalLogObj(false).debug3(
              "FileDownloader:  Terminate flag set; aborting");
        }
      }
    }
    catch (Throwable ex)
    {
      //this block catches a "Throwable" instead of an "Exception"
      // because 'ZipFile' can throw an 'InternalError'
      LogFile.getGlobalLogObj(false).warning("FileDownloader:  " + ex);
      if (errMsgStr == null)
      {  //call-back string not previously entered
        if(ex instanceof FileNotFoundException)
          errMsgStr = "Unable to find file:  " + fileUrl;
        else if(ex instanceof UnknownHostException)
        {     //unknown host; setup to show "friendlier" error message
          errMsgStr = "Unable to find file (\"" +
                                                fileUrl + "\"):  " + ex;
        }
        else
        {     //not file-not-found exception; show stack trace
          LogFile.getGlobalLogObj(false).warning(
                                           UtilFns.getStackTraceString(ex));
          errMsgStr = "Error loading data:  " + ex;
        }
      }
    }
    finally
    {
    	try                //cleanup
    	{
            FileUtils.close(in);  //close the input stream
            in = null;
            FileUtils.close(out);  //close the output stream
            out = null;
    		if (zipFileObj != null)  //if the ZIP file object exists
    		{
    			try
    			{
    				if (zipFileObj instanceof Closeable)
    				{
    					//close the ZIP file
    					((Closeable) zipFileObj).close();
    				}
    				zipFileObj = null;
    			}
    			catch (Exception ex)
    			{
    				LogFile.getGlobalLogObj(false).warning(
    						"FileDownloader ZIP close: " + ex.toString());
    				LogFile.getGlobalLogObj(false).warning(
    						UtilFns.getStackTraceString(ex));
    			}
    			if (outputFile != null)  //if ZIP file was created
    			{
    				if (deleteFileAfterUnzipFlag)
    				{
    					outputFile.delete(); //delete the ZIP file
    					outputFile = null;
    				}
    			}
    		}
    		if (outputFile != null && outputFile.exists())
    		{
    			if ( (isTerminated() || errMsgStr != null) &&
    					 outputFile.length() == 0)
    			{ //terminated or error occurred, and zero-length file created
    				outputFile.delete(); //remove created file
					outputFile = null;
    			}
    			else
    			{
    		        if (lastModified >= 0)
    		        {
    		        	outputFile.setLastModified(lastModified);
    		        }
        			addOutputFileObj(outputFile);
    			}
    		}
    	}
    	catch (Exception ex)
    	{
    		LogFile.getGlobalLogObj(false).warning("FileDownloader cleanup: " +
    				ex.toString());
    		LogFile.getGlobalLogObj(false).warning(
    				UtilFns.getStackTraceString(ex));
    	}
    }
    return errMsgStr;
  }

  /**
   * Determines if the output file should be backed up if it already exists.
   * The default is false.
   * @param b true if the output file should be backed up if it already exists.
   */
  public void setBackupFile(boolean b)
  {
    backupFileFlag = b;
  }

  /**
   * Sets the call-back object to use used.
   * @param callBackObj if not null then a call-back object whose method
   * is called (after the download is complete) with a string parameter
   * containing null if successful or an error message if an error occurred.
   */
  public void setCallBackObj(CallBackStringParam callBackObj)
  {
    this.callBackObj = callBackObj;
  }

  /**
   * Determines if the connection should be checked.
   * The default is true.
   * @param b true if the connection should be checked.
   */
  public void setCheckConnection(boolean b)
  {
    checkConnectionFlag = b;
  }

  /**
   * Determines if the last modified time should be checked.
   * The default is false.
   * @param b true if the last modified time should be checked.
   */
  public void setCheckLastModified(boolean b)
  {
    checkLastModifiedFlag = b;
  }

  /**
   * Sets if the dialog is closed when the download is done.
   * @param b true if dialog is closed when the download is done, false
   *          otherwise.
   */
  public void setCloseWhenDone(boolean b)
  {
    closeWhenDoneFlag = b;
  }

  /**
   * Determines if the file should be deleted after unzip.
   * The default is true.
   * @param b true if the file should be deleted after unzip.
   */
  public void setDeleteFileAfterUnzip(boolean b)
  {
    deleteFileAfterUnzipFlag = b;
  }

  /**
   * Sets the download done.
   * @param errMsgStr error message to be entered into the call-back
   * object, or null for none.
   */
  protected void setDownloadDone(String errMsgStr)
  {
    downloadDoneFlag = true;      //indicate download attempt finished
    // if dialog exists and close when done
    if(dialogObj != null && closeWhenDoneFlag)
      dialogObj.close();          //close dialog popup
    threadFinished(errMsgStr);    //perform thread-finished operations
  }

  /**
   * Set the file name string.
   * @param fileNameStr the file name string.
   * @throws Exception if the file name string cannot be parsed.
   */
  public void setFileNameStr(String fileNameStr) throws Exception
  {
    synchronized (this)
    {
      this._urlList = singletonList(fileNameStr);
    }
  }

  /**
   * Set if output is going to the first directory.
   * @param outputFirstDirFlag true if output is going to the first directory, false otherwise.
   */
  public void setOutputFirstDirFlag(boolean outputFirstDirFlag)
  {
    this.outputFirstDirFlag = outputFirstDirFlag;
  }

  /**
   * Terminates this thread if the thread is not terminated and alive.
   * @deprecated Should use the <code>terminateThread</code> method instead.
   * @see terminateThread
   */
  public void terminate()
  {
    super.terminate();
  }

  /**
   * Terminates this download thread.
   * @param msgStr message string given to call-back object.
   */
  public void terminateThread(String msgStr)
  {
    synchronized(threadFinishedSyncObj)
    {
      //only allow one thread to deal with terminate/finished at a time
      if(!isTerminated())  //if thread is not terminated
      {
        //thread not already terminated
        LogFile.getGlobalLogObj(false).debug2(
                           "FileDownloader:  Download terminate requested");
        //do thread-finished operations:
        doThreadFinished(msgStr);
        super.terminate();  //terminate the thread
      }
    }
  }

  /**
   * Performs thread-finished operations (if terminate flag not set).
   * @param errMsgStr error message to be entered into the call-back
   * object, or null for none.
   */
  protected void threadFinished(String errMsgStr)
  {
    synchronized(threadFinishedSyncObj)
    {
      //only allow one thread to deal with terminate/finished at a time
      if(!isTerminated())  //if thread is not terminated
      {
        doThreadFinished(errMsgStr);      //do thread-finished operations
      }
      else
      {
        LogFile.getGlobalLogObj(false).debug2(
            "FileDownloader.threadFinished():  Terminate flag already set");
      }
    }
  }

  /**
   * Transfers data from the input stream to the output stream.
   * @param inputStream the input stream.
   * @param outputStream the output stream.
   *
   * @exception  IOException  if an I/O error occurs.
   */
  protected void transferStream(
      InputStream inputStream, OutputStream outputStream)
      throws IOException
  {
    long progressValue = 0;

    int b;
    final byte[] buffer = new byte[FileUtils.STREAM_TRANSFER_BUFFER_SIZE];
    while(!isTerminated())  //while thread is not terminated
    {
      //exit if no more data
      if ((b = inputStream.read(buffer, 0,
                               FileUtils.STREAM_TRANSFER_BUFFER_SIZE)) <= 0)
      {
        break;
      }

      if(progressBarObj != null && downloadLength > 0)
      {  //progress bar given and length is greater than zero
        progressValue += b;
        final int dialogVal =
                     (int)(progressValue * maxProgressVal / downloadLength);
        progressBarObj.setValue(dialogVal);
      }
      outputStream.write(buffer, 0, b);
    }
  }

  /**
   * Unzip the file.
   * @param file the ZIP file.
   * @param outputDir the output directory.
   * @return the ZIP file object or null if none.
   * @exception IOException if an I/O error has occurred.
   */
  protected Object unzipZipFile(File file, File outputDir) throws IOException
  {
    ZipFile zf = null;
    if (ZipFileUtils.hasZipExtension(file.getName()))
    {
      //extract the files
      zf = ZipFileUtils.openZipFile(file);
      ZipFileUtils.unzipZipFile(zf, outputDir, true);
    }
    return zf;
  }
}
