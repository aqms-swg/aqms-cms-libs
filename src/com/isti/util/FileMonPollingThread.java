//FileMonPollingThread.java:  Defines a thread for monitoring a file via
//                            a 'FileDataMonitor' object.
//
//  3/16/2007 -- [ET]  Initial version.
//  1/22/2009 -- [ET]  Added 'fireForceFileRead()' method; added
//                     optional 'forceFileReadFlag' parameter to
//                     'checkReadFileData()' method.
//  3/15/2011 -- [ET]  Added optional constructor parameter 'indRecoveryFlag'
//                     and functionality.
//

package com.isti.util;

/**
 * Class FileMonPollingThread defines a thread for monitoring a file via
 * a 'FileDataMonitor' object.
 */
public class FileMonPollingThread extends IstiNotifyThread
{
    /** Empty array used to indicate error. */
  public static final byte [] ERROR_ARRAY = FileDataMonitor.ERROR_ARRAY;
  protected final FileDataMonitor fileDataMonitorObj;
  protected final int pollingIntervalMs;
  protected final int fileAccessTimeOutMs;
  protected final long maxReadBytesLimit;
  protected final boolean indRecoveryFlag;
  protected final FileDataCallBack fileDataCallBackObj;
  protected boolean forceFileReadFlag = false;

  /**
   * Creates a thread for monitoring a file via a 'FileDataMonitor' object.
   * After being created, this thread should be started via its 'start()'
   * method.
   * @param fNameStr name of input file to be read and monitored.
   * @param pollingIntervalMs number of milliseconds to wait between
   * polls of the input file.
   * @param fileAccessTimeOutMs timeout value for file access, or 0 for
   * infinite.
   * @param maxReadBytesLimit maximum number of bytes to read from file,
   * or 0 for no limit.
   * @param indRecoveryFlag if true then the call-back method will be
   * invoked with (true,null) if the previous file-access attempt failed
   * and the current attempt succeeded and no change in the file
   * last-modified time was detected.
   * @param fileDataCallBackObj call-back object to be invoked when changes
   * in the file data are detected.
   */
  public FileMonPollingThread(String fNameStr, int pollingIntervalMs,
                            int fileAccessTimeOutMs, long maxReadBytesLimit,
              boolean indRecoveryFlag, FileDataCallBack fileDataCallBackObj)
  {
    super("FileMonPollingThread-" + fNameStr);
    if(fNameStr == null || fileDataCallBackObj == null)
      throw new NullPointerException("Null parameter(s)");
    if(pollingIntervalMs <= 0)
      throw new IllegalArgumentException("pollingIntervalMs <= 0");
    fileDataMonitorObj = new FileDataMonitor(fNameStr);
    this.pollingIntervalMs = pollingIntervalMs;
    this.fileAccessTimeOutMs = fileAccessTimeOutMs;
    this.maxReadBytesLimit = maxReadBytesLimit;
    this.indRecoveryFlag = indRecoveryFlag;
    this.fileDataCallBackObj = fileDataCallBackObj;
  }

  /**
   * Creates a thread for monitoring a file via a 'FileDataMonitor' object.
   * After being created, this thread should be started via its 'start()'
   * method.
   * @param fNameStr name of input file to be read and monitored.
   * @param pollingIntervalMs number of milliseconds to wait between
   * polls of the input file.
   * @param fileAccessTimeOutMs timeout value for file access, or 0 for
   * infinite.
   * @param maxReadBytesLimit maximum number of bytes to read from file,
   * or 0 for no limit.
   * @param fileDataCallBackObj call-back object to be invoked when changes
   * in the file data are detected.
   */
  public FileMonPollingThread(String fNameStr, int pollingIntervalMs,
                            int fileAccessTimeOutMs, long maxReadBytesLimit,
                                       FileDataCallBack fileDataCallBackObj)
  {
    this(fNameStr,pollingIntervalMs,fileAccessTimeOutMs,maxReadBytesLimit,
                                                 false,fileDataCallBackObj);
  }

  /**
   * Executing method for file-monitoring thread.
   */
  public void run()
  {
    byte [] byteArr;
    boolean lastAttemptFlag = true;    //false if last attempt failed
    while(!isTerminated())
    {    //loop while thread not terminated
                   //sleep for interval time:
      if(!waitForNotify(pollingIntervalMs) && isTerminated())
        break;     //if thread terminated then exit loop (and method)
      try
      {
        if(forceFileReadFlag)
        {  //force-read flag set via 'fireForceReadFlag()' method
          forceFileReadFlag = false;             //clear flag
          byteArr = checkReadFileData(true);     //read file
        }
        else       //force-read flag not set; check/read file
          byteArr = checkReadFileData(false);
        if(byteArr != null)
        {  //change in file data detected
          if(byteArr != ERROR_ARRAY)
          {   //error value not returned; invoke call-back with file data
            fileDataCallBackObj.callBackMethod(true,byteArr);
            lastAttemptFlag = true;         //indicate access succeeded
          }
          else
          {   //error value returned; invoke call-back with error message
            fileDataCallBackObj.callBackMethod(false,
                     fileDataMonitorObj.getErrorMessageString().getBytes());
            lastAttemptFlag = false;        //indicate access failed
          }
        }
        else if(indRecoveryFlag && !lastAttemptFlag)
        {  //no change in file data detected, last attempt failed
           // and flag set for indicating recovery
          fileDataCallBackObj.callBackMethod(true,null);
          lastAttemptFlag = true;           //indicate access succeeded
        }
      }
      catch(Exception ex)
      {  //some kind of exception error; show message
        System.err.println("Error in FileMonPollingThread:  " + ex);
        ex.printStackTrace();
      }
    }
  }

  /**
   * Fires a forced read of the file by the monitoring thread, as if
   * the file's last-modified time had changed.
   */
  public void fireForceFileRead()
  {
    forceFileReadFlag = true;     //set indicator flag
    notifyThread();               //wake file-monitoring thread now
  }

  /**
   * Checks if the file has been modified and returns its contents if
   * so.  The first time this method is called the file data will be
   * returned.  This method may be used to do the initial read of file
   * data before using the monitoring thread to track future changes
   * to the data.
   * @param forceFileReadFlag true to force reading of file data; false
   * to only read file data if file-modified time changed.
   * @return A byte array containing the file data, 'null' if the file
   * has not been modified, or 'ERROR_ARRAY' if an error occurred (in
   * which case the 'getErrorMessageString()' method may be used to
   * fetch the error message).
   */
  public byte [] checkReadFileData(boolean forceFileReadFlag)
  {
    return fileDataMonitorObj.checkReadFileData(fileAccessTimeOutMs,
                                       maxReadBytesLimit,forceFileReadFlag);
  }

  /**
   * Checks if the file has been modified and returns its contents if
   * so.  The first time this method is called the file data will be
   * returned.  This method may be used to do the initial read of file
   * data before using the monitoring thread to track future changes
   * to the data.
   * @return A byte array containing the file data, 'null' if the file
   * has not been modified, or 'ERROR_ARRAY' if an error occurred (in
   * which case the 'getErrorMessageString()' method may be used to
   * fetch the error message).
   */
  public byte [] checkReadFileData()
  {
    return fileDataMonitorObj.checkReadFileData(fileAccessTimeOutMs,
                                                   maxReadBytesLimit,false);
  }

  /**
   * Returns the 'FileDataMonitor' object used by this object.
   * @return The 'FileDataMonitor' object used by this object.
   */
  public FileDataMonitor getFileDataMonitorObj()
  {
    return fileDataMonitorObj;
  }

  /**
   * Returns the status of the error message.
   * @return true if an error message is set; false if not.
   */
  public boolean getErrorMessageFlag()
  {
    return fileDataMonitorObj.getErrorMessageFlag();
  }

  /**
   * Returns the current error message (if any).
   * @return The current error message, or null if no errors have occurred.
   */
  public String getErrorMessageString()
  {
    return fileDataMonitorObj.getErrorMessageString();
  }


  /**
   * Interface FileDataCallBack defines a call-back method to be invoked
   * when new file data is available.
   */
  public interface FileDataCallBack
  {
    /**
     * Method invoked when new file data is available.
     * @param flagVal true for success; false for error (in which case
     * the 'byteArr' parameter will contain an error message).
     * @param byteArr file data (if 'flagVal'==true), or the data from
     * an error-message string (if 'flagVal'==false).
     */
    public void callBackMethod(boolean flagVal, byte [] byteArr);
  }

}
