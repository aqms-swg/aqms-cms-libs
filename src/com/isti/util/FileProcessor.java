//FileProcessor.java:  Helper class for processing a text input file into
//                     a text output file.
//
//  5/6/2005 -- [ET]
//

package com.isti.util;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.PrintWriter;
import java.io.FileNotFoundException;
import java.io.IOException;

/**
 * Class FileProcessor helper class for processing a text input file into
 * a text output file.
 */
public class FileProcessor
{
    /** Reader object for input file. */
  public BufferedReader inputReaderObj = null;
    /** Writer object for output file. */
  public PrintWriter outputWriterObj = null;

  /**
   * Creates a file processor object and opens input and output files.
   * @param inFName name of input text file to use.
   * @param outFName name of output text file to use.
   * @throws FileNotFoundException if the input file is not found.
   * @throws IOException if an I/O error occurs.
   */
  public FileProcessor(String inFName, String outFName)
                                   throws FileNotFoundException, IOException
  {
    inputReaderObj = new BufferedReader(new FileReader(inFName));
    outputWriterObj = new PrintWriter(new BufferedWriter(
                                                 new FileWriter(outFName)));
  }

  /**
   * Reads and returns the next line of data from the input file.  When
   * the end of the input file is reached, the input and output files are
   * closed and 'null' is returned.
   * @return A string containing a line of data from the input file, or
   * null if the end of the input file has been reached.
   * @throws IOException if an I/O error occurs.
   */
  public String readNextLine() throws IOException
  {
    String retStr;
    if((retStr=inputReaderObj.readLine()) == null)
    {    //end of file reached
      inputReaderObj.close();     //close input file
      outputWriterObj.close();    //close output file
    }
    return retStr;
  }

  /**
   * Writes the given line of data to the output file.  A line-end is
   * appended to the data written to the file.
   * @param str a string containing the line of data to be written.
   * @throws IOException if an I/O error occurs.
   */
  public void writeNextLine(String str) throws IOException
  {
    outputWriterObj.println(str);
  }

  /**
   * Closes the input file.  This method does not usually need to be
   * called because the 'readNextLine()' method closes the input and
   * output file after it reaches the end of the input file.
   */
  public void closeInputFile()
  {
    try
    {
      inputReaderObj.close();
    }
    catch(IOException ex)
    {
    }
  }

  /**
   * Closes the output file.  This method does not usually need to be
   * called because the 'readNextLine()' method closes the input and
   * output file after it reaches the end of the input file.
   */
  public void closeOutputFile()
  {
    outputWriterObj.close();
  }
}
