// FileToByteArray.java:  Utility to read an input file and convert its
//                        data into a byte array in a ".java" file.
//
//  10/19/2000 -- [ET]
//

package com.isti.util;

import java.io.*;

/**
 * Class FileToByteArray is a utility to read an input file and convert its
 * data into a byte array in a ".java" file.
 */
public class FileToByteArray
{
  private static final String EXT_STR = ".java"; //extension for output file

  public static final void main(String[] args)
  {
    if(args.length <= 0)
    {
      System.out.println("Usage:  FileToByteArray infile");
      return;
    }
    final BufferedInputStream in;
    try
    {              //open input file:
      in = new BufferedInputStream(new FileInputStream(args[0]));
    }
    catch(Exception ex)
    {
      System.out.println("Unable to open input file \"" + args[0] +
                                                              "\":  " + ex);
      return;
    }
    int p;
    if((p=args[0].lastIndexOf('.')) < 0)    //find last period
      p = args[0].length();       //if no periods then use string length
    final StringBuffer baseNameBuff =
                                   new StringBuffer(args[0].substring(0,p));
    p = 0;
    while(p < baseNameBuff.length())
    {
      baseNameBuff.setCharAt(p,        //make character uppercase
                             Character.toUpperCase(baseNameBuff.charAt(p)));
      if((p=baseNameBuff.toString().indexOf('_',p+1)) > 0 &&
                                              p + 1 < baseNameBuff.length())
      {     //replace "_x" with uppercase "X"
        baseNameBuff.deleteCharAt(p);
      }
      else
        break;
    }
    final String baseName = baseNameBuff.toString();
                   //put new extension on output file:
    final String outName = baseName + EXT_STR;
    if(outName.equals(args[0]))
    {
      System.out.println("Output file name same as input file name (\"" +
                                                  outName + "\"; aborting");
      try { in.close(); }         //close input file
      catch(IOException ex) {}
      return;
    }
    final PrintWriter out;
    try
    {              //open output file:
      out = new PrintWriter(new BufferedWriter(new FileWriter(outName)));
    }
    catch(Exception ex)
    {
      System.out.println("Unable to open output file \"" + outName +
                                                              "\":  " + ex);
      try { in.close(); }         //close input file
      catch(IOException ex2) {}
      return;
    }
              //output header and declarations:
    out.println("// " + outName + ":  A byte array representation of \"" +
                                                            args[0] + "\"");
    out.println("//      created by the FileToByteArray.java utility.");
    out.println("//");
    out.println();
    out.println("/**");
    out.println(" * Class " + baseName +
                  " is a byte array representation of \"" + args[0] + "\"");
    out.println(" * created by the FileToByteArray.java utility.");
    out.println(" */");
    out.println("public class " + baseName);
    out.println("{");
    out.println("  public static final byte [] dataArray = new byte []");
    out.print("  {");

    int val,lastVal = 0,count = 0;
    boolean firstOnLineFlag = true;
    try
    {
      while((val=in.read()) >= 0)
      {
        ++count;
        if(firstOnLineFlag)
        {
          firstOnLineFlag = false;
          if(count > 1)
            out.print(",");
          out.println();
          out.print("    ");
        }
        else
        {
          if(lastVal < 10)
            out.print(",   ");
          else if(lastVal < 100)
            out.print(",  ");
          else
            out.print(", ");
          if(count % 6 == 0)
            firstOnLineFlag = true;
        }
        out.print("(byte)" + val);
        lastVal = val;
      }
    }
    catch(Exception ex)
    {
      System.out.println("Error reading data from input file \"" +
                                                            args[0] + "\"");
    }

              //output ending syntax for declarations:
    out.println();
    out.println("  };          //byte count = " + count);
    out.println("}");

    try { in.close(); }           //close input file
    catch(IOException ex) {}
    out.close();                  //close output file
    System.out.println("\"" + args[0] + "\" ==> \"" + outName +
                                               "\", byte count = " + count);
  }
}

