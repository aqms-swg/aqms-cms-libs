//FileUtils.java:  Contains various static utility methods for handling
//                 files.

package com.isti.util;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Enumeration;
import java.util.Properties;
import java.util.jar.JarFile;
import java.io.*;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLConnection;
import java.net.URLClassLoader;
import com.isti.util.topleveldummy.TopLevelDummyUtil;

/**
 * Class FileUtils contains various static utility methods for handling
 * files.
 */
public class FileUtils
{
  protected static final boolean DEBUG_FLAG = false;  //true for debug msgs
                             //dummy object for class loading:
  protected static FileUtils fileUtilsObj = new FileUtils();
                             //stream transfer buffer size:
  protected static int STREAM_TRANSFER_BUFFER_SIZE = 2048;

  /** Number of milliseconds in an hour. */
  public static final long MS_PER_HOUR = 60L * 60L * 1000L;
  /** Number of milliseconds in a day. */
  public static final long MS_PER_DAY = MS_PER_HOUR * 24L;
         //filename tracking string used by 'createUnusedFileNameObj()':
  protected static String lastCreateUnusedFileNameStr = "";
         //thread sync object used by 'createUnusedFileNameObj()':
  protected static final Object lastCreateUnusedFNSyncObj = new Object();

    //private constructor so that no object instances may be created
    // (static access only)
  private FileUtils()
  {
  }

    /**
     * Attempts to open the given name as a local file, as a URL, as a
     * resource and as an entry in a 'jar' file (whichever works first).
     * @param nameStr file name string.
     * @return The (unbuffered) InputStream object opened for the name, or
     * null if the name could not be opened.
     */
  public static InputStream fileMultiOpenInputStream(String nameStr)
  {
    return fileMultiOpenInputStream(nameStr, (Properties)null);
  }
  
    /**
     * Attempts to open the given name as a local file, as a URL, as a
     * resource and as an entry in a 'jar' file (whichever works first).
     * @param nameStr file name string.
     * @param urlRequestProps the request properties to use for URL connections
     *                     or null if none.
     * @return The (unbuffered) InputStream object opened for the name, or
     * null if the name could not be opened.
     */
  public static InputStream fileMultiOpenInputStream(
      String nameStr, Properties urlRequestProps)
  {
    if(nameStr.indexOf('\\') >= 0)
    {    //given filename contains Windows-style separators (backslashes)
              //convert backslashes to forward-slashes and attempt open:
      final InputStream stmObj;
      if((stmObj=doFileMultiOpenInputStream(
          nameStr.replace('\\','/'), urlRequestProps)) != null)
      {  //file opened successfully
        return stmObj;       //return stream object
      }
    }
    return doFileMultiOpenInputStream(nameStr, urlRequestProps);
  }

    /**
     * Attempts to open the given name as a local file, as a URL, as a
     * resource and as an entry in a 'jar' file (whichever works first).
     * @param nameStr file name string.
     * @return The (unbuffered) InputStream object opened for the name, or
     * null if the name could not be opened.
     */
  protected static InputStream doFileMultiOpenInputStream(String nameStr)
  {
    return doFileMultiOpenInputStream(nameStr, (Properties)null);
  }
  
    /**
     * Attempts to open the given name as a local file, as a URL, as a
     * resource and as an entry in a 'jar' file (whichever works first).
     * @param nameStr file name string.
     * @param urlRequestProps the request properties to use for URL connections
     *                     or null if none.
     * @return The (unbuffered) InputStream object opened for the name, or
     * null if the name could not be opened.
     */
  @SuppressWarnings("resource")
  protected static InputStream doFileMultiOpenInputStream(
      String nameStr, Properties urlRequestProps)
  {
    if(DEBUG_FLAG)
      System.out.println("fileMultiOpenInputStream(\"" + nameStr + "\"):");

    try
    {    //attempt to open as local file; return reader if successful:
      return new FileInputStream(nameStr);
    }
    catch(Exception ex) {}
    if(DEBUG_FLAG)
    {
      System.out.println(
                       "fileMultiOpenInputStream:  FileInputStream failed");
    }

    InputStream stmObj;
    URL urlObj = null;
    try
    {    //attempt to open name as a URL specifier to a file:
      urlObj = new URL(nameStr);
      URLConnection connObj = openConnection(urlObj);
      if (urlRequestProps != null)
      {
        Object keyObj;
        String key;
        String value;
        Enumeration keysEnum = urlRequestProps.keys();
        while (keysEnum.hasMoreElements())
        {
          keyObj = keysEnum.nextElement();
          if (keyObj != null)
          {
            key = keyObj.toString();
            value = urlRequestProps.getProperty(key);
            connObj.setRequestProperty(key, value);
          }
        }
      }
      if((stmObj=connObj.getInputStream()) != null)
        return stmObj;       //if opened OK then return stream handle
    }
    catch(Exception ex) {}
    if(DEBUG_FLAG)
    {
      System.out.println(
                        "fileMultiOpenInputStream:  url.openStream failed");
    }

    try
    {    //attempt to open as a resource directly to a stream:
      if((stmObj=ClassLoader.getSystemResourceAsStream(nameStr)) != null)
        return stmObj;       //if opened OK then return stream handle
    }
    catch(Exception ex) {}
    if(DEBUG_FLAG)
    {
      System.out.println("fileMultiOpenInputStream:  " +
                            "ClassLoader.getSystemResourceAsStream failed");
    }

    urlObj = null;
    try
    {    //attempt to open as a local file specified with a URL:
      if((urlObj=ClassLoader.getSystemResource(nameStr)) != null)
      {  //system resource for name fetched OK
        final File fileObj;       //if file exists then open it:
        if((fileObj=new File(urlObj.getFile())).exists())
          return new FileInputStream(fileObj);
              //otherwise attempt to open as a URL input stream:
        return openConnection(urlObj).getInputStream();
      }
    }
    catch(Exception ex) {}
    if(DEBUG_FLAG)
    {
      System.out.println("fileMultiOpenInputStream:  " +
                     "ClassLoader.getSystemResource.getFile/Stream failed");
    }

    try
    {    //attempt to open as a resource directly to a stream using
         // a the top-level object:
      if((stmObj=TopLevelDummyUtil.getTopLevelDummyClass().
                                      getResourceAsStream(nameStr)) != null)
      {
        return stmObj;
      }
      if(DEBUG_FLAG)
      {
        System.out.println("fileMultiOpenInputStream:  " +
                       "topLevelDummyObj.getSystemResourceAsStream failed");
      }
         //attempt to open as a resource directly to a stream using
         // a local object:
      if((stmObj=fileUtilsObj.getClass().getResourceAsStream(nameStr)) != null)
        return stmObj;
      if(DEBUG_FLAG)
      {
        System.out.println("fileMultiOpenInputStream:  " +
                           "fileUtilsObj.getSystemResourceAsStream failed");
      }
    }
    catch(Exception ex)
    {
      if(DEBUG_FLAG)
      {
        System.out.println("fileMultiOpenInputStream:  " +
                    "getClass.getSystemResourceAsStream exception:  " + ex);
      }
    }
    catch(Throwable ex) //catch "Throwable" in case 'NoClassDefFoundError'
    {                   // thrown because 'TopLevelDummy' class missing
      ex.printStackTrace();       //send stack trace to console
    }

    JarFile jarFileObj = null;
    try
    {    //attempt to open as entry in jar file ("jarfile!jarfileentry"):
      final int xPos;
      if((xPos=nameStr.indexOf('!')) > 0)
      {  //exclamation point found in name
        int p;          //look for colon before exclamation point
        if((p=nameStr.indexOf(':')) >= 0 && p < xPos)
          ++p;     //if found before '!' then increment to next position
        else
          p = 0;   //if not found then set at first position
                             //try to make jar file object:
        jarFileObj = new JarFile(nameStr.substring(p,xPos));
                             //try to return stream for jar file entry:
        return jarFileObj.getInputStream(jarFileObj.getJarEntry(
                                                nameStr.substring(xPos+1)));
      }
    }
    catch(Exception ex) {}
    if(DEBUG_FLAG)
    {
      System.out.println(
                      "fileMultiOpenInputStream:  JarFile/JarEntry failed");
    }

    if(DEBUG_FLAG)
    {
      System.out.println("fileMultiOpenInputStream(\"" + nameStr +
                                               "\"):  All attempts failed");
    }
    try
    {
      if (jarFileObj != null)
      {
    	jarFileObj.close();
      }
    }
    catch (Exception ex) {}
    return null;
  }

    /**
     * Attempts to open the given name as a local file, as a URL, as a
     * resource and as an entry in a 'jar' file (whichever works first).
     * @param nameStr file name string.
     * @return The buffered InputStream object opened for the name, or
     * null if the name could not be opened.
     */
  public static BufferedInputStream fileMultiOpenStream(String nameStr)
  {
    final InputStream stmObj;
    return ((stmObj=fileMultiOpenInputStream(nameStr)) != null) ?
                                     new BufferedInputStream(stmObj) : null;
  }

    /**
     * Attempts to open the given name as a local file, as a URL, as a
     * resource and as an entry in a 'jar' file (whichever works first).
     * @param nameStr file name string.
     * @return The BufferedReader object opened for the name, or
     * null if the name could not be opened.
     */
  public static BufferedReader fileMultiOpen(String nameStr)
  {
    return fileMultiOpen(nameStr, (Properties)null);
  }

    /**
     * Attempts to open the given name as a local file, as a URL, as a
     * resource and as an entry in a 'jar' file (whichever works first).
     * @param nameStr file name string.
     * @param urlRequestProps the request properties to use for URL connections
     *                        or null if none.
     * @return The BufferedReader object opened for the name, or
     * null if the name could not be opened.
     */
  public static BufferedReader fileMultiOpen(
      String nameStr, Properties urlRequestProps)
  {
    final InputStream stmObj = fileMultiOpenInputStream(nameStr, urlRequestProps);
    return getBufferedReader(stmObj);
  }

    /**
     * Attempts to open the given name via either the application method.
     * @param nameStr file name string.
     * @return The (unbuffered) InputStream object opened for the name, or
     * null if the name could not be opened.
     */
  public static InputStream appResourceOpenInputStream(String nameStr)
  {
    return fileMultiOpenInputStream(nameStr);
  }

    /**
     * Attempts to open the given name via either the application method.
     * @param nameStr file name string.
     * @return The buffered InputStream object opened for the name, or
     * null if the name could not be opened.
     */
  public static BufferedInputStream appResourceOpenStream(String nameStr)
  {
    return fileMultiOpenStream(nameStr);
  }

    /**
     * Attempts to open the given name via either the application method.
     * @param nameStr file name string.
     * @return The BufferedReader object opened for the name, or
     * null if the name could not be opened.
     */
  public static BufferedReader appResourceOpen(String nameStr)
  {
    final InputStream stmObj = fileMultiOpenInputStream(nameStr);
    return getBufferedReader(stmObj);
  }

    /**
     * Attempts to open the given name as a local file, as a URL, as a
     * resource and as an entry in a 'jar' file (whichever works first).
     * @param nameStr file name string.
     * @return The URL object opened for the name, or
     * null if the name could not be opened.
     */
  public static URL fileMultiOpenInputURL(String nameStr)
  {
    if(nameStr.indexOf('\\') >= 0)
    {    //given filename contains Windows-style separators (backslashes)
              //convert backslashes to forward-slashes and attempt open:
      final URL urlObj;
      if((urlObj=doFileMultiOpenInputURL(
                                        nameStr.replace('\\','/'))) != null)
      {  //file opened successfully
        return urlObj;       //return URL object
      }
    }
    return doFileMultiOpenInputURL(nameStr);
  }

    /**
     * Attempts to open the given name as a local file, as a URL, as a
     * resource and as an entry in a 'jar' file (whichever works first).
     * @param nameStr file name string.
     * @return The URL object opened for the name, or
     * null if the name could not be opened.
     */
  protected static URL doFileMultiOpenInputURL(String nameStr)
  {
    if(DEBUG_FLAG)
      System.out.println("fileMultiOpenInputURL(\"" + nameStr + "\"):");

    try
    {    //attempt to open as a local file:
      final File fileObj = new File(nameStr);
      if(fileObj.isFile())             //if a valid file then
        return fileObj.toURI().toURL();  //return URL version of name
    }
    catch(Exception ex) {}
    if(DEBUG_FLAG)
    {
      System.out.println("fileMultiOpenInputURL:  File.toUrl failed");
    }

    InputStream stmObj = null;
    URL urlObj = null;
    try
    {    //attempt to open name as a URL specifier to a file:
      urlObj = new URL(nameStr);
      if((stmObj=urlObj.openStream()) != null)
      {  //stream connection opened OK with name as URL
        return urlObj;       //return URL object
      }
    }
    catch(Exception ex) {}
    finally
    {
      close(stmObj);      //close input stream
    }
    if(DEBUG_FLAG)
    {
      System.out.println(
                        "fileMultiOpenInputStream:  url.openStream failed");
    }

    try
    {    //attempt to open as a resource directly to a stream:
      if((urlObj=ClassLoader.getSystemResource(nameStr)) != null)
        return urlObj;
    }
    catch(Exception ex) {}
    if(DEBUG_FLAG)
    {
      System.out.println("fileMultiOpenInputURL:  " +
                                    "ClassLoader.getSystemResource failed");
    }

    try
    {    //attempt to open as a resource directly to a URL using
         // a the top-level object:
      if((urlObj=TopLevelDummyUtil.getTopLevelDummyClass().
                                              getResource(nameStr)) != null)
      {
        return urlObj;
      }
      if(DEBUG_FLAG)
      {
        System.out.println("fileMultiOpenInputURL:  " +
                               "topLevelDummyObj.getSystemResource failed");
      }
         //attempt to open as a resource directly to a stream using
         // a local object:
      if((urlObj=fileUtilsObj.getClass().getResource(nameStr)) != null)
        return urlObj;
      if(DEBUG_FLAG)
      {
        System.out.println("fileMultiOpenInputURL:  " +
                                   "fileUtilsObj.getSystemResource failed");
      }
    }
    catch(Exception ex)
    {
      if(DEBUG_FLAG)
      {
        System.out.println("fileMultiOpenInputURL:  " +
                            "getClass.getSystemResource exception:  " + ex);
      }
    }
    catch(Throwable ex) //catch "Throwable" in case 'NoClassDefFoundError'
    {                   // thrown because 'TopLevelDummy' class missing
      ex.printStackTrace();       //send stack trace to console
    }

    if(DEBUG_FLAG)
    {
      System.out.println("fileMultiOpenInputURL(\"" + nameStr +
                                               "\"):  All attempts failed");
    }
    return null;
  }

  /**
   * Get the working directory path.
   * @return the working directory path or an empty string if error.
   */
  public static String getWorkingDirPath()
  {
	try
	{
	  String path = new File(".").getAbsolutePath();
	  if(path.endsWith("."))
	  {  //path ends with "."
		//remove trailing "."
		path =        
			path.substring(0,path.length()-1);
	  }
	  return path;
	}
	catch(Exception ex)
	{
	  if(DEBUG_FLAG)
	  {
		System.err.println("Error getting working directory path:  " + ex);
	  }
	}
	return UtilFns.EMPTY_STRING;
  }
  
  /**
   * Get a unique file for the specified file.
   * @param fileObj the file.
   * @param prefix the prefix text or null if none.
   * @return the unique file.
   */
  public static File getUniqueFile(File fileObj, String prefix)
  {
	// calculate hash-code value based on the path
	final long hashCode = UtilFns.toUnsignedLong((
		getWorkingDirPath()).hashCode());
	String child = Long.toString(hashCode);
	if (prefix != null)
	{
	  child = prefix + child;
	}
	String parent = fileObj.getParent();
	String name = fileObj.getName();
	if (fileObj.isAbsolute())
	{
	  fileObj = new File(parent, child);
	}
	else
	{
	  // add the user home
	  String home = System.getProperty("user.home");
	  fileObj = new File(home, child);
	  if (parent != null)
	  {
		fileObj = new File(fileObj, parent);
	  }
	}
	if (name != null)
	{
	  fileObj = new File(fileObj, name); 
	}
	return fileObj;
  }

  /**
   * Writes a data string to a file.
   * @param fileNameStr the name of the output file.
   * @param dataStr the data to be written.
   * @param appendFlag true to append to current contents of file, false
   * to overwrite.
   * @throws IOException if an error occurred while opening or writing to
   * the output file.
   */
  public static void writeStringToFile(String fileNameStr,String dataStr,
                                      boolean appendFlag) throws IOException
  {
                                  //open file for output:
    final FileWriter wtrObj = new FileWriter(fileNameStr,appendFlag);
	try
	{
	  wtrObj.write(dataStr);        //write data string to file
	}
    finally
    {
      close(wtrObj);      //close output file
    }
  }

  /**
   * Writes a data string to a file.  Any previous contents in the file are
   * overritten.
   * @param fileNameStr the name of the output file.
   * @param dataStr the data to be written.
   * @throws IOException if an error occurred while opening or writing to
   * the output file.
   */
  public static void writeStringToFile(String fileNameStr,String dataStr)
                                                          throws IOException
  {
    writeStringToFile(fileNameStr,dataStr,false);
  }

  /**
   * Writes a data string to a file.
   * @param fileObj the output file object to use.
   * @param dataStr the data to be written.
   * @param appendFlag true to append to current contents of file, false
   * to overwrite.
   * @throws IOException if an error occurred while opening or writing to
   * the output file.
   */
  public static void writeStringToFile(File fileObj,String dataStr,
                                      boolean appendFlag) throws IOException
  {
    writeStringToFile(fileObj.getPath(),dataStr,appendFlag);
  }

  /**
   * Writes a data string to a file.  Any previous contents in the file are
   * overritten.
   * @param fileObj the output file object to use.
   * @param dataStr the data to be written.
   * @throws IOException if an error occurred while opening or writing to
   * the output file.
   */
  public static void writeStringToFile(File fileObj,String dataStr)
                                                          throws IOException
  {
    writeStringToFile(fileObj.getPath(),dataStr,false);
  }

  /**
   * Writes a data string to a file.
   * @param fileNameStr the name of the output file.
   * @param dataStr the data to be written.
   * @param appendFlag true to append to current contents of file, false
   * to overwrite.
   * @param charsetNameStr name of charset to use for conversion from data
   * bytes to characters, or null for default charset.
   * @throws IOException if an error occurred while opening or writing to
   * the output file.
   */
  public static void writeStringToFile(String fileNameStr,String dataStr,
               boolean appendFlag, String charsetNameStr) throws IOException
  {
              //convert string to data bytes (via charset if given):
    final byte [] dataArr = (charsetNameStr != null) ?
                     dataStr.getBytes(charsetNameStr) :  dataStr.getBytes();
    FileOutputStream outStmObj = null;
    try
    {
      //open file for output
      outStmObj = new FileOutputStream(fileNameStr,appendFlag);
      outStmObj.write(dataArr);               //write data bytes to file
    }
    finally
    {
      close(outStmObj);                      //close output file
    }
  }

  /**
   * Writes a data string to a file.  Any previous contents in the file are
   * overritten.
   * @param fileNameStr the name of the output file.
   * @param dataStr the data to be written.
   * @param charsetNameStr name of charset to use for conversion from data
   * bytes to characters, or null for default charset.
   * @throws IOException if an error occurred while opening or writing to
   * the output file.
   */
  public static void writeStringToFile(String fileNameStr,String dataStr,
                                   String charsetNameStr) throws IOException
  {
    writeStringToFile(fileNameStr,dataStr,false,charsetNameStr);
  }

  /**
   * Writes a data string to a file.
   * @param fileObj the output file object to use.
   * @param dataStr the data to be written.
   * @param appendFlag true to append to current contents of file, false
   * to overwrite.
   * @param charsetNameStr name of charset to use for conversion from data
   * bytes to characters, or null for default charset.
   * @throws IOException if an error occurred while opening or writing to
   * the output file.
   */
  public static void writeStringToFile(File fileObj,String dataStr,
               boolean appendFlag, String charsetNameStr) throws IOException
  {
    writeStringToFile(fileObj.getPath(),dataStr,appendFlag,charsetNameStr);
  }

  /**
   * Writes a data string to a file.  Any previous contents in the file are
   * overritten.
   * @param fileObj the output file object to use.
   * @param dataStr the data to be written.
   * @param charsetNameStr name of charset to use for conversion from data
   * bytes to characters, or null for default charset.
   * @throws IOException if an error occurred while opening or writing to
   * the output file.
   */
  public static void writeStringToFile(File fileObj,String dataStr,
                                   String charsetNameStr) throws IOException
  {
    writeStringToFile(fileObj.getPath(),dataStr,false,charsetNameStr);
  }

  /**
   * Writes a data buffer to a file.  Any previous contents in the file are
   * overritten.
   * @param fileNameStr the name of the output file.
   * @param btArr the data to be written.
   * @throws IOException if an error occurred while opening or writing to
   * the output file.
   */
  public static void writeBufferToFile(String fileNameStr, byte [] btArr)
                                                          throws IOException
  {
	ByteArrayInputStream inStmObj = null;
    final FileOutputStream outStm = new FileOutputStream(fileNameStr);
    try
    {
      //open file for output:
      inStmObj = new ByteArrayInputStream(btArr);
      transferStream(inStmObj,outStm);        //write data to file
    }
    finally
    {
      close(outStm);                         //close output file
      close(inStmObj);
    }
  }

  /**
   * Reads data from an input stream into a string.  The stream is expected
   * to provide characters organized into one or more lines.
   * @param rdrObj the input stream to use.
   * @return A String containing the contents of the file.
   * @throws IOException if an error occurred while reading from the
   * input stream.
   */
  public static String readStreamToString(BufferedReader rdrObj)
                                                          throws IOException
  {
                             //create string-writer to collect output:
    final StringWriter strWtrObj = new StringWriter();
                             //wrap print-writer around string-writer:
    final PrintWriter prtWtrObj = new PrintWriter(strWtrObj);
    String str;
    while((str=rdrObj.readLine()) != null)  //for each line of data in file
      prtWtrObj.println(str);               //send line to string-writer
    prtWtrObj.flush();                 //flush output to string-writer
    return strWtrObj.toString();       //return string version of data
  }

  /**
   * Reads data from an input stream into a string.  The stream is expected
   * to provide characters organized into one or more lines.
   * @param stmObj the input stream to use.
   * @return A String containing the contents of the file.
   * @throws IOException if an error occurred while reading from the
   * input stream.
   */
  public static String readStreamToString(InputStream stmObj)
                                                          throws IOException
  {
    return readStreamToString(getBufferedReader(stmObj));
  }

  /**
   * Reads data from an input stream into a string.
   * @param inStmObj input stream to use.
   * @param charsetNameStr name of charset to use for conversion from data
   * bytes to characters, or null for default charset.
   * @return A String containing the contents of the file.
   * @throws IOException if an error occurred while reading from the
   * input stream.
   */
  public static String readStreamToString(InputStream inStmObj,
                                   String charsetNameStr) throws IOException
  {
    if(!(inStmObj instanceof BufferedInputStream))    //if not buffered then
      inStmObj = new BufferedInputStream(inStmObj);   //buffer stream
    final int numAvail = inStmObj.available();   //determine size
    final byte [] buff = new byte[numAvail];     //create byte buffer
    inStmObj.read(buff,0,numAvail);              //read data into buffer
    return (charsetNameStr != null) ?            //convert bytes to string
                         new String(buff,charsetNameStr) : new String(buff);
  }

  /**
   * Reads data from a file into a string.  The file is expected to contain
   * characters organized into one or more lines.
   * @param fileNameStr the name of the input file.
   * @return A String containing the contents of the file.
   * @throws FileNotFoundException if the specified file is not found.
   * @throws IOException if an error occurred while opening or reading from
   * the input file.
   */
  public static String readFileToString(String fileNameStr)
                                   throws FileNotFoundException, IOException
  {
	final String retStr;
    final BufferedReader rdrObj =      //create file input reader
                            new BufferedReader(new FileReader(fileNameStr));
    try
    {
      retStr = readStreamToString(rdrObj);
    }
    finally
    {
      close(rdrObj);                    //close input file
    }
    return retStr;
  }

  /**
   * Reads data from a file into a string.  The file is expected to contain
   * characters organized into one or more lines.
   * @param fileObj the input file object.
   * @return A String containing the contents of the file.
   * @throws FileNotFoundException if the specified file is not found.
   * @throws IOException if an error occurred while opening or reading from
   * the input file.
   */
  public static String readFileToString(File fileObj)
                                   throws FileNotFoundException, IOException
  {
    return readFileToString(fileObj.getPath());
  }

  /**
   * Reads data from a file into a string.
   * @param fileNameStr the name of the input file.
   * @param charsetNameStr name of charset to use for conversion from data
   * bytes to characters, or null for default charset.
   * @return A String containing the contents of the file.
   * @throws FileNotFoundException if the specified file is not found.
   * @throws IOException if an error occurred while opening or reading from
   * the input file.
   */
  public static String readFileToString(String fileNameStr,
            String charsetNameStr) throws FileNotFoundException, IOException
  {
	final String retStr;
                                       //open stream to input file:
    final FileInputStream inStmObj = new FileInputStream(fileNameStr);
    try
    {
      //read stream into string:
      retStr = readStreamToString(inStmObj,charsetNameStr);
    }
    finally
    {
      close(inStmObj);                  //close input file
    }
    return retStr;
  }

  /**
   * Reads data from a file into a string.
   * @param fileObj the input file object.
   * @param charsetNameStr name of charset to use for conversion from data
   * bytes to characters, or null for default charset.
   * @return A String containing the contents of the file.
   * @throws FileNotFoundException if the specified file is not found.
   * @throws IOException if an error occurred while opening or reading from
   * the input file.
   */
  public static String readFileToString(File fileObj,
            String charsetNameStr) throws FileNotFoundException, IOException
  {
    return readFileToString(fileObj.getPath(),charsetNameStr);
  }

  /**
   * Reads data from a file into a string, where the given name may specify
   * a local file, a URL, a resource or an entry in a 'jar' file (whichever
   * works first).  The file is expected to contain  characters organized
   * into one or more lines.
   * @param fileNameStr the name of the input file.
   * @return A String containing the contents of the file.
   * @throws FileNotFoundException if the specified file is not found.
   * @throws IOException if an error occurred while opening or reading from
   * the input file.
   */
  public static String readMultiOpenFileToString(String fileNameStr)
                                   throws FileNotFoundException, IOException
  {
    final BufferedReader rdrObj;       //attempt to open filename:
    if((rdrObj=fileMultiOpen(fileNameStr)) == null)
    {    //unable to open filename; throw exception
      throw new FileNotFoundException("Unable to open \"" +
                                         fileNameStr + "\" as file or URL");
    }
    final String retStr;
    try
    {
      retStr = readStreamToString(rdrObj);
    }
    finally
    {
      close(rdrObj);                    //close input file
    }
    return retStr;
  }

  /**
   * Reads data from an input stream into a byte array.  The stream
   * is expected to provide characters organized into one or more lines.
   * @param stmObj the input stream to use.
   * @return A byte array containing the contents of the file.
   * @throws IOException if an error occurred while reading from the
   * input stream.
   */
  public static byte [] readStreamToBuffer(InputStream stmObj)
                                                          throws IOException
  {
              //create byte-array output stream and transfer into it:
    final ByteArrayOutputStream outStmObj = new ByteArrayOutputStream();
    transferStream(stmObj,outStmObj);
    outStmObj.flush();                 //make sure data is flushed through
    return outStmObj.toByteArray();    //return byte array containing data
  }

  /**
   * Reads data from a file into a buffer.
   * @param fileNameStr the name of the input file.
   * @return A byte array containing the contents of the file.
   * @throws FileNotFoundException if the specified file is not found.
   * @throws IOException if an error occurred while opening or reading from
   * the input file.
   */
  public static byte [] readFileToBuffer(String fileNameStr)
                                   throws FileNotFoundException, IOException
  {
    final BufferedInputStream inStm =       //create file input reader
                  new BufferedInputStream(new FileInputStream(fileNameStr));
    final byte [] byteArr;
    try
    {
      byteArr = readStreamToBuffer(inStm);
    }
    finally
    {
      close(inStm);                //close input file
    }
    return byteArr;
  }

  /**
   * Reads data from a file into a buffer.
   * @param fileObj the 'File' object for the input file.
   * @return A byte array containing the contents of the file.
   * @throws FileNotFoundException if the specified file is not found.
   * @throws IOException if an error occurred while opening or reading from
   * the input file.
   */
  public static byte [] readFileToBuffer(File fileObj)
                                   throws FileNotFoundException, IOException
  {
    return readFileToBuffer(fileObj.getPath());
  }

  /**
   * Reads data from a file into a byte array, where the given name may
   * specify a local file, a URL, a resource or an entry in a 'jar' file
   * (whichever works first).
   * @param fileNameStr the name of the input file.
   * @return A byte array containing the contents of the file.
   * @throws FileNotFoundException if the specified file is not found.
   * @throws IOException if an error occurred while opening or reading from
   * the input file.
   */
  public static byte [] readMultiOpenFileToBuffer(String fileNameStr)
                                   throws FileNotFoundException, IOException
  {
    final BufferedInputStream inStm;       //attempt to open filename:
    if((inStm=fileMultiOpenStream(fileNameStr)) == null)
    {    //unable to open filename; throw exception
      throw new FileNotFoundException("Unable to open \"" +
                                         fileNameStr + "\" as file or URL");
    }
    final byte [] byteArr;
    try
    {
      byteArr = readStreamToBuffer(inStm);
    }
    finally
    {
      close(inStm);                //close input file
    }
    return byteArr;
  }

  /**
   * Creates any needed parent directories needed for the given file
   * object.  The given object should denote a file, not a directory.
   * @param fileObj the file object to use.
   * @return true if successful or if the needed parent directories already
   * existed, false if error.
   */
  public static boolean createParentDirs(File fileObj)
  {
    try
    {
      final File parentFileObj;
      if(!fileObj.exists() && (parentFileObj=fileObj.getParentFile()) !=
                                       null && !parentFileObj.isDirectory())
      {  //file doesn't exist, has a path and the path doesn't exist
        return parentFileObj.mkdirs();      //make all needed directories
      }
      return true;
    }
    catch(Exception ex)
    {    //some kind of exception error occurred
      return false;
    }
  }

  /**
   * Creates any needed parent directories needed for the given file/path
   * name.  The given name should denote a file, not a directory.
   * @param nameStr file name string.
   * @return true if successful or if the needed parent directories already
   * existed, false if error.
   */
  public static boolean createParentDirs(String nameStr)
  {
    try
    {
      final File fileObj = new File(nameStr);     //create file object
      final File parentFileObj;
      if(!fileObj.exists() && (parentFileObj=fileObj.getParentFile()) !=
                                       null && !parentFileObj.isDirectory())
      {  //file doesn't exist, has a path and the path doesn't exist
        return parentFileObj.mkdirs();      //make all needed directories
      }
      return true;
    }
    catch(Exception ex)
    {    //some kind of exception error occurred
      return false;
    }
  }

  /**
   * Returns an array of abstract pathnames denoting the files in the
   * specified directory.
   * @param directoryObj directory object.
   * <p> If the file does is not a directory, then this
   * method returns <code>null</code>.  Otherwise an array of
   * <code>File</code> objects is returned, one for each file or directory in
   * the directory.  Pathnames denoting the directory itself and the
   * directory's parent directory are not included in the result.  Each
   * resulting abstract pathname is constructed from this abstract pathname
   * using the <code>{@link #File(java.io.File, java.lang.String)
   * File(File,&nbsp;String)}</code> constructor.  Therefore if this pathname
   * is absolute then each resulting pathname is absolute; if this pathname
   * is relative then each resulting pathname will be relative to the same
   * directory.
   *
   * <p> There is no guarantee that the name strings in the resulting array
   * will appear in any specific order; they are not, in particular,
   * guaranteed to appear in alphabetical order.
   *
   * @return  An array of abstract pathnames denoting the files and
   *          directories in the directory denoted by this abstract
   *          pathname.  The array will be empty if the directory is
   *          empty.  Returns <code>null</code> if this abstract pathname
   *          does not denote a directory, or if an I/O error occurs.
   */
  public static File[] listFiles(File directoryObj)
  {
    return directoryObj.listFiles();
  }

  /**
   * Returns an array of abstract pathnames denoting the files in the
   * specified directory.
   * @param directoryObj directory object
   * @param filter The filename filter to use, or null for none.
   *
   * @see com.isti.util.gui.IstiFileFilter
   *
   * <p> If the file does is not a directory, then this
   * method returns <code>null</code>.  Otherwise an array of
   * <code>File</code> objects is returned, one for each file or directory in
   * the directory.  Pathnames denoting the directory itself and the
   * directory's parent directory are not included in the result.  Each
   * resulting abstract pathname is constructed from this abstract pathname
   * using the <code>{@link #File(java.io.File, java.lang.String)
   * File(File,&nbsp;String)}</code> constructor.  Therefore if this pathname
   * is absolute then each resulting pathname is absolute; if this pathname
   * is relative then each resulting pathname will be relative to the same
   * directory.
   *
   * <p> There is no guarantee that the name strings in the resulting array
   * will appear in any specific order; they are not, in particular,
   * guaranteed to appear in alphabetical order.
   *
   * @return  An array of abstract pathnames denoting the files and
   *          directories in the directory denoted by this abstract
   *          pathname.  The array will be empty if the directory is
   *          empty.  Returns <code>null</code> if this abstract pathname
   *          does not denote a directory, or if an I/O error occurs.
   */
  public static File[] listFiles(File directoryObj, FileFilter filter)
  {
    return (filter != null) ? directoryObj.listFiles(filter) :
                                                   directoryObj.listFiles();
  }

  /**
   * Returns an array of abstract pathnames denoting the requested files in
   * the specified directory and all subdirectories.  Entries for the
   * directories themselves are not included in the result.  Each
   * resulting abstract pathname is constructed from the given abstract
   * pathname using the 'File' constructor.  Therefore if the given pathname
   * is absolute then each resulting pathname is absolute; if the given
   * pathname is relative then each resulting pathname will be relative
   * to the same directory. There is no guarantee that the name strings
   * in the resulting array will appear in any specific order; they are
   * not, in particular, guaranteed to appear in alphabetical order.  Note
   * that the given filename filter must accept all directory entries
   * (i.e., an 'IstiFileFilter' with 'setAcceptDirectories(true)').
   * @param dirFileObj directory object to use.
   * @param filterObj filename filter to use, or null for none.
   * @return A new array of 'File' objects denoting the requested files
   * in the specified directory and all subdirectories, or null if the
   * given 'File' object does not denote a directory.
   */
  public static File [] listAllFiles(File dirFileObj, FileFilter filterObj)
  {
    final File [] dirFileArr = dirFileObj.listFiles(filterObj);
    if(dirFileArr != null && dirFileArr.length > 0)
    {    //entries were found in given directory
      ArrayList fileListObj = null;
      File [] foundFilesArr;
      File fileObj;
      for(int dfIdx=0; dfIdx<dirFileArr.length; ++dfIdx)
      {  //for each entry found in given directory
        if(dirFileArr[dfIdx].isDirectory())
        {     //entry is a directory
          if(fileListObj == null)
          {   //file list not yet setup
            fileListObj = new ArrayList();  //create file list
            for(int i=0; i<dirFileArr.length; ++i)
            { //for each entry found in given directory
                   //if entry not a directory then add to list:
              if(!(fileObj=dirFileArr[i]).isDirectory())
                fileListObj.add(fileObj);
            }
          }             //recursively find entries in directory:
          if((foundFilesArr=listAllFiles(dirFileArr[dfIdx],filterObj)) !=
                                           null && foundFilesArr.length > 0)
          {   //directory contains entries; add them to list
            fileListObj.addAll(Arrays.asList(foundFilesArr));
          }
        }
      }
         //if file list was created then convert to array and return:
      if(fileListObj != null)
        return (File [])(fileListObj.toArray(new File[fileListObj.size()]));
    }
         //no file list was created; return files array for given directory:
    return dirFileArr;
  }

  /**
   * Gets a buffered reader for the specified input stream.
   * @param stmObj the input stream or null if none.
   * @return the buffered reader or null if no input stream.
   */
  public static BufferedReader getBufferedReader(InputStream stmObj)
  {
    return (stmObj != null) ?
        new BufferedReader(new InputStreamReader(stmObj)) : null;
  }

  /**
   * Gets a buffered reader for the specified input stream.
   * @param stmObj the input stream or null if none.
   * @param charsetName the name of a supported charset or null for default.
   * @return the buffered reader or null if no input stream.
   */
  public static BufferedReader getBufferedReader(
      InputStream stmObj, String charsetName)
  {
    if (stmObj != null && charsetName != null)
    {
      try
      {
        return new BufferedReader(new InputStreamReader(stmObj, charsetName));
      }
      catch (Exception ex) {}
    }
    return getBufferedReader(stmObj);
  }

  /**
   * @return the age of the file in milliseconds.
   * @param f file
   * @param currentTime current time
   */
  public static long getFileAge(File f, long currentTime)
  {
    long fileTime = f.lastModified();
    long age = currentTime - fileTime;
    return age;
  }
  
  /**
   * Deletes the directory and any subdirectories.
   * @param dir the directory to delete.
   * @return true if the directory was deleted, false otherwise.
   */
  public static boolean deleteDirectory(File dir)
  {
    final String[] names = dir.list();
    if (names != null && names.length != 0)
    {
      int i = 0;
      do
      {
        final File file = new File(dir, names[i]);
        if (file.isDirectory())
        {
          deleteDirectory(file);
        }
        file.delete();  //delete file
      }
      while(++i < names.length);
    }
    return dir.delete();  //delete directory
  }

  /**
   * Deletes the specified file or if a directory is specified deletes the
   * directory and any subdirectories.
   * @param file the 'File' object to delete.
   */
  public static void deleteFiles(File file)
  {
    if (file.isDirectory())
      deleteDirectory(file);
    else
      file.delete();
  }

  /**
   * Deletes files in the specified list that are older than the given age.
   * @param fileList the array of 'File' objects to delete.
   * @param maxAgeMs the maximum number of milliseconds to keep files.
   * @see listFiles
   */
  public static void deleteFiles(File[] fileList, long maxAgeMs)
  {
    final long currentTime = System.currentTimeMillis();
    for(int i=0; i<fileList.length; i++)
    {    //for each file in array
      long age = getFileAge(fileList[i], currentTime);
      if(age > maxAgeMs)
      {  //file is older than maximum-allowed age
        if(DEBUG_FLAG)
        {
          System.out.println("deleting file: " + fileList[i].getName() +
                                                          " (" + age + ")");
        }
        fileList[i].delete();     //delete file
      }
    }
  }

  /**
   * Deletes files in the specified list that are older than the given age.
   * @param fileList the array of 'File' objects to delete.
   * @param maxAgeDays the maximum number of days to keep files.
   * @see listFiles
   */
  public static void deleteFiles(File[] fileList, int maxAgeDays)
  {
    deleteFiles(fileList,(long)maxAgeDays*MS_PER_DAY);
  }

  /**
   * Deletes files older than the given age from the specified directory.
   * @param directoryObj the directory to use.
   * @param maxAgeMs the maximum number of milliseconds to keep files, or
   * 0 to delete no files.
   * @param filterStr the file filter string to use (i.e.:  "*.txt",
   * "temp.*", "temp.txt", "*bak*"), or null for none.
   * @see IstiFileFilter#createFileFilter
   */
  public static void deleteOldFiles(File directoryObj, long maxAgeMs,
                                                           String filterStr)
  {
    if(maxAgeMs > 0)
    {    //maximum age was provided
      if(directoryObj.isDirectory())
      {  //given directory object is OK
              //create file filter (if filter-string given) and use it to
              // generate list of files in the directory:
        final File [] fileList = listFiles(directoryObj,
              ((filterStr!=null)?IstiFileFilter.createFileFilter(filterStr):
                                                                     null));
        deleteFiles(fileList, maxAgeMs);         //delete older files
      }
    }
  }

  /**
   * Deletes files older than the given age from the specified directory.
   * @param directoryObj the directory to use.
   * @param maxAgeMs the maximum number of milliseconds to keep files, or
   * 0 to delete no files.
   */
  public static void deleteOldFiles(File directoryObj, long maxAgeMs)
  {
    deleteOldFiles(directoryObj,maxAgeMs,null);
  }

  /**
   * Deletes files older than the given age from the specified directory.
   * @param directoryObj the directory to use.
   * @param maxAgeDays the maximum number of days to keep files, or 0
   * to delete no files.
   * @param filterStr the file filter string to use (i.e.:  "*.txt",
   * "temp.*", "temp.txt", "*bak*"), or null for none.
   * @see IstiFileFilter#createFileFilter
   */
  public static void deleteOldFiles(File directoryObj, int maxAgeDays,
                                                           String filterStr)
  {
    deleteOldFiles(directoryObj,(long)maxAgeDays*MS_PER_DAY,filterStr);
  }

  /**
   * Deletes files older than the given age from the specified directory.
   * @param directoryObj the directory to use.
   * @param maxAgeDays the maximum number of days to keep files, or 0
   * to delete no files.
   */
  public static void deleteOldFiles(File directoryObj, int maxAgeDays)
  {
    deleteOldFiles(directoryObj,(long)maxAgeDays*MS_PER_DAY,null);
  }

  /**
   * Returns the index of the extension portion of the file name string.
   * @return the index of the extension portion of the file name string
   * or -1 if none (includes the ".".)
   * @param fileNameStr the name of the file.
   *
   * @see FileFilter#accept
   */
  public static int getFileExtensionIndex(String fileNameStr)
  {
    final int periodIndex = fileNameStr.lastIndexOf('.');
    if (periodIndex >= 0) // if period was found
    {
      // get the index of the name without the path
      int nameIndex = fileNameStr.lastIndexOf(File.separatorChar);
      // name was not found and separator is not slash
      if (nameIndex < 0 && File.separatorChar != '/')
      {
        // try to find slash
        nameIndex = fileNameStr.lastIndexOf('/');
      }
      // if name was found
      if (nameIndex >= 0)
      {
        // if period is not in the name
        if (periodIndex < nameIndex)
        {
          return -1;
        }
      }
    }
    return periodIndex;
  }

  /**
   * Returns the extension portion of the file name string
   * or null if none (does not include the ".".)
   * @param fileNameStr the name of the file.
   * @return The extension portion of the file name string
   * or null if none (does not include the ".".)
   */
  public static String getFileExtension(String fileNameStr)
  {
    //find the file extension
    final int extIndex = getFileExtensionIndex(fileNameStr);
    if(extIndex > 0 && extIndex < fileNameStr.length()-1)
    {
      return fileNameStr.substring(extIndex+1);
    }
    return null;
  }

  /**
   * Returns the file name string without the extension portion.
   * @param fileNameStr the name of the file.
   * @return The file name string without the extension portion.
   */
  public static String getFileNameWithoutExtension(String fileNameStr)
  {
    final int extIdx = getFileExtensionIndex(fileNameStr);
    if(extIdx >= 0)
      fileNameStr = fileNameStr.substring(0, extIdx);
    return fileNameStr;
  }

  /**
   * Removes leading "dot" directory from given filename.  If the given
   * filename starts with "./" or ".\" then the first two characters
   * are removed.
   * @param fileNameStr filename to process.
   * @return The filename string with the leading "dot" directory
   * removed, or the given string if the leading "dot" directory
   * was not found.
   */
  public static String removeLeadingDotDir(String fileNameStr)
  {
    return (fileNameStr != null && fileNameStr.length() > 1 &&
                                             fileNameStr.charAt(0) == '.' &&
          (fileNameStr.charAt(1) == '/' || fileNameStr.charAt(1) == '\\')) ?
                                     fileNameStr.substring(2) : fileNameStr;
  }

  /**
   * Returns the last-modified time value for the file denoted by the
   * given name.  The file may be a local file, a URL, a resource or
   * an entry in a 'jar' file (whichever works first).
   * @param nameStr file name string.
   * @return The last-modified time value for the file denoted by the
   * given name, or -1 if the time value could not be determined.
   */
  public static long getLastModifiedTime(String nameStr)
  {
    try
    {
      long timeVal;          //attempt as a local file:
      final File fileObj = new File(nameStr);
      if(fileObj.exists() && (timeVal=fileObj.lastModified()) > 0)
        return timeVal;
      final URL urlObj;      //attempt as a URL
      final URLConnection urlConnObj;
      if((urlObj=FileUtils.fileMultiOpenInputURL(nameStr)) != null &&
                             (urlConnObj=openConnection(urlObj)) != null &&
                                 (timeVal=urlConnObj.getLastModified()) > 0)
      {
        return timeVal;
      }
    }
    catch(Exception ex)
    {    //some kind of exception error
    }
    return -1;          //return error indicator value
  }

  /**
   * Returns the file-length value for the file denoted by the
   * given name.  The file may be a local file, a URL, a resource or
   * an entry in a 'jar' file (whichever works first).
   * @param nameStr file name string.
   * @return The file-length value for the file denoted by the
   * given name, or -1 if the length could not be determined.
   */
  public static long getFileLength(String nameStr)
  {
    try
    {
      long lengthVal;        //attempt as a local file:
      final File fileObj = new File(nameStr);
      if(fileObj.exists() && (lengthVal=fileObj.length()) >= 0)
        return lengthVal;
      final URL urlObj;      //attempt as a URL
      final URLConnection urlConnObj;
      if((urlObj=FileUtils.fileMultiOpenInputURL(nameStr)) != null &&
                             (urlConnObj=openConnection(urlObj)) != null &&
                             (lengthVal=urlConnObj.getContentLength()) >= 0)
      {
        return lengthVal;
      }
    }
    catch(Exception ex)
    {    //some kind of exception error
    }
    return -1;          //return error indicator value
  }

  /**
   * Tests whether the specified URL exists.
   * @param urlObj the URL.
   * @return true if the URL exists, false otherwise.
   * @exception  IOException  if an I/O exception occurs.
   */
  public static boolean urlExists(URL urlObj) throws java.io.IOException
  {
    final InputStream stmObj = urlObj.openStream();
    stmObj.close();
    return true;
  }

  /**
   * Attempts to get a URL from the given name as a URL,
   * as a local file, and as a system resource (whichever works first).
   * @param nameStr file name string.
   * @return the URL or null if a URL could not be determined.
   */
  public static URL getURL(String nameStr)
  {
    if(nameStr.indexOf('\\') >= 0)
    {    //given filename contains Windows-style separators (backslashes)
              //convert backslashes to forward-slashes and attempt:
      final URL urlObj;
      if((urlObj=doGetURL(nameStr.replace('\\','/'))) != null)
      {  //file opened successfully
        return urlObj;       //return URL object
      }
    }
    return doGetURL(nameStr);
  }

  /**
   * Attempts to get a URL from the given name as a URL,
   * as a local file, and as a system resource (whichever works first).
   * @param nameStr file name string.
   * @return the URL or null if a URL could not be determined.
   */
  protected static URL doGetURL(String nameStr)
  {
    URL urlObj = null;

    try
    {    //attempt to open name as a URL:
      urlObj = new URL(nameStr);
      if(urlExists(urlObj))      //if URL exists
      {
        return urlObj;  //return the URL
      }
    }
    catch(Exception ex) {}

    try
    {    //attempt to open name as local file:
      urlObj = new File(nameStr).toURI().toURL();
      if(urlExists(urlObj))      //if URL exists
      {
        return urlObj;  //return the URL
      }
    }
    catch(Exception ex) {}

    try
    {    //attempt to open name as a system resource:
      urlObj = ClassLoader.getSystemResource(nameStr);
      if(urlExists(urlObj))      //if URL exists
      {
        return urlObj;  //return the URL
      }
    }
    catch(Exception ex) {}

    return null;  //URL could not be determined
  }

  /**
   * Determines if the given name denotes an existing local file or a
   * URL or 'jar'-file resource that may be accessed.
   * @param nameStr file name string.
   * @return true if the given name denotes an existing file; false if
   * not.
   */
  public static boolean existsMultiOpen(String nameStr)
  {
    if(!(new File(nameStr)).exists())
    {    //name does not denote an existing local file
      final InputStream stmObj;   //attempt to open stream to name
      if((stmObj=fileMultiOpenInputStream(nameStr)) == null)
        return false;        //if unable to open stream then return false
      close(stmObj);        //close opened stream
    }
    return true;             //indicate filename exists
  }

  /**
   * Returns an indicator of whether or not the given character is
   * a directory separator character.
   * @param ch the character to test.
   * @return true if the given character is one of the following:
   * '/', '\', ':'.
   */
  public static boolean isDirSepChar(char ch)
  {
    return (ch == '/' || ch == '\\' || ch == ':');
  }

  /**
   * Returns an indicator of whether or not the given string ends with
   * a directory separator character.
   * @param str the string to test.
   * @return true if the given string ends with one of the following:
   * '/', '\', ':'.
   */
  public static boolean endsWithDirSepChar(String str)
  {
    final int len;
    return (str != null && (len=str.length()) > 0 &&
                                           isDirSepChar(str.charAt(len-1)));
  }

  /**
   * Combines the given directory and file names.  If the directory name
   * does not end with a directory-separator character ('/', '\' or ':')
   * then a forward-slash ('/') separator character is used.
   * @param dirStr the directory name to use.
   * @param fileStr the file name to use.
   * @param forwardSlashFlag true to replace all backslash characters ('\')
   * in the path with forward-slash characters ('/').
   * @return The combined path name.
   */
  public static String buildPathName(String dirStr,String fileStr,
                                                   boolean forwardSlashFlag)
  {
    final String pathStr = dirStr +    //build path; insert sep if needed
       (FileUtils.endsWithDirSepChar(dirStr) ? UtilFns.EMPTY_STRING : "/") +
                                                                    fileStr;
              //return path; if flag then change all '\' to '/':
    return forwardSlashFlag ? pathStr.replace('\\','/') : pathStr;
  }

  /**
   * Combines the given directory and file names.  If the directory name
   * does not end with a directory-separator character ('/', '\' or ':')
   * then a forward-slash ('/') separator character is used.  All
   * backslash characters ('\') in the path are replaced with
   * forward-slash characters ('/').
   * @param dirStr the directory name to use.
   * @param fileStr the file name to use.
   * @return The combined path name.
   */
  public static String buildPathName(String dirStr,String fileStr)
  {
    return buildPathName(dirStr,fileStr,true);
  }

  /**
   * Add a string to the end of a file name (before the extension if present.)
   * @param fileNameStr the name of the file.
   * @param s string to add.
   * @return the new file name string.
   */
  public static String addStrToFileName(String fileNameStr, String s)
  {
    //find the file extension
    final int extIndex = getFileExtensionIndex(fileNameStr);
    if(extIndex >= 0)  //if file extension exists
    {
      //add text before file extension
      fileNameStr = fileNameStr.substring(0, extIndex) +
                       s +
                       fileNameStr.substring(extIndex, fileNameStr.length());
    }
    else
    {
      //add date text to end
      fileNameStr += s;
    }
    return fileNameStr;
  }

  /**
   * Creates a backup file name for the specified file name.
   * @param fileNameStr the name of the file.
   * @return new file name string
   */
  public static String createBackupFileName(String fileNameStr)
  {
    return createBackupFileName(fileNameStr, null);
  }

  /**
   * Creates a backup file name for the specified file name.
   * @param fileNameStr the name of the file.
   * @param backupStr backup string to be added (bak) or null for none.
   * @return new file name string
   */
  public static String createBackupFileName(String fileNameStr, String backupStr)
  {
    if(backupStr != null)  //if backup string was provided
    {
      //add backup string
      fileNameStr = addStrToFileName(fileNameStr, backupStr);
    }

    //get just the last name in the pathname's name sequence
    String lastNameStr = new File(fileNameStr).getName();
    //remove the extension and put in lower case
    lastNameStr = getFileNameWithoutExtension(lastNameStr).toLowerCase();

    String filterString = addStrToFileName(fileNameStr, "*");
    IstiFileFilter filter = new IstiFileFilter(filterString);
    File[] fileList = listFiles(
        new File(fileNameStr).getParentFile(), filter);

    int lastNameStrLength = lastNameStr.length();
    int maxBn = 0;
    for (int i = 0; i < fileList.length; i++)  //for each backup found
    {
      String fn = fileList[i].getName();  //get the file name
      fn = getFileNameWithoutExtension(fn);  //remove the extension
      int bnIdx = fn.toLowerCase().indexOf(lastNameStr);  //find last name
      if(bnIdx >= 0)  //if last name was found
      {
        bnIdx += lastNameStrLength;  //add the length of last name
        //get the backup number
        int bn = Integer.valueOf(fn.substring(bnIdx)).intValue();
        if(bn > maxBn)  //if it is greater than the max
          maxBn = bn;  //save it
      }
    }
    //return the backup file name with the next number
    return addStrToFileName(fileNameStr, Integer.toString(++maxBn));
  }

  /**
   * Creates a new 'File' object that references an unused file name.
   * The given template string is used to generate the file name, with
   * a numeric value added to make it unique (if necessary).
   * @param tmplStr the template to use when generating the file name,
   * or null for none.
   * @param parentDirStr the name of the parent directory to use, or
   * null to use the current user directory.
   * @param useTimeFlag if true then the current time value (in ms since
   * 1/1/1970) is used in the file name, if false then an incrementing
   * value is used, starting at 1 (if the template string name itself
   * is already in use).
   * @param fileExtFlag true to insert numeric value before the filename
   * extension (if present); false to always append numeric value to end.
   * @return A new 'File' object, or null if an unused file name object
   * could not be created.
   */
  public static File createUnusedFileNameObj(String tmplStr,
                String parentDirStr,boolean useTimeFlag,boolean fileExtFlag)
  {
    final int maxAttempts = 1000;      //max # of attempts allowed
    int cnt = 0;                       //# of attempts counter
    if(tmplStr == null)                //if template filename not given then
      tmplStr = UtilFns.EMPTY_STRING;  //use empty string
    final int tmplStrLen = tmplStr.length();
              //if non-empty template string then setup separator to use:
    final String sepStr = (tmplStrLen > 0) ? "_" : UtilFns.EMPTY_STRING;
    String fNameStr;
    File retFileObj;
    String retFileStr;
    boolean sameFNameFlag;
    if(useTimeFlag)
    {    //using time (in ms) to generate filename
      while(true)
      {  //for each attempt; create a new filename using time value
        fNameStr = fileExtFlag ?
                           addStrToFileName(tmplStr,(sepStr + Long.toString(
                                             System.currentTimeMillis()))) :
                                          (tmplStr + sepStr + Long.toString(
                                               System.currentTimeMillis()));
              //create File object using filename and parent directory name:
        retFileObj = new File(parentDirStr,fNameStr);
              //get absolute pathname string for file:
        retFileStr = retFileObj.getAbsolutePath();
        synchronized(lastCreateUnusedFNSyncObj)
        {     //only allow one thread access at a time
                   //set flag if filename is same as previous:
          sameFNameFlag = retFileStr.equals(lastCreateUnusedFileNameStr);
        }
        if(!sameFNameFlag && !retFileObj.exists())
          break;             //if name not same and not exist then exit loop
        if(++cnt >= maxAttempts)       //increment attempts count
          return null;                 //if too many attempts then abort
        try { Thread.sleep(1); }       //delay for 1 millisecond
        catch(InterruptedException ex) {}
      }
    }
    else
    {    //not using time to generate filename
      if(tmplStrLen <= 0)    //if empty template string then
        ++cnt;               //don't try using template string directly
      while(true)
      {  //for each attempt
              //if first attempt then use template string directly;
              // if not first attempt then insert counter value
        fNameStr = (cnt == 0) ? tmplStr : (fileExtFlag ?
                addStrToFileName(tmplStr,(sepStr + Integer.toString(cnt))) :
                                (tmplStr + sepStr + Integer.toString(cnt)));
              //create File object using filename and parent directory name:
        retFileObj = new File(parentDirStr,fNameStr);
              //get absolute pathname string for file:
        retFileStr = retFileObj.getAbsolutePath();
        synchronized(lastCreateUnusedFNSyncObj)
        {     //only allow one thread access at a time
                   //set flag if filename is same as previous:
          sameFNameFlag = retFileStr.equals(lastCreateUnusedFileNameStr);
        }
        if(!sameFNameFlag && !retFileObj.exists())
          break;             //if name not same and not exist then exit loop
        if(++cnt >= maxAttempts)       //increment attempts count
          return null;                 //if too many attempts then abort
      }
    }
    synchronized(lastCreateUnusedFNSyncObj)
    {     //only allow one thread access at a time
               //save filename for next iteration:
      lastCreateUnusedFileNameStr = retFileStr;
    }
    return retFileObj;       //return File object created
  }

  /**
   * Creates a new 'File' object that references an unused file name.
   * The given template string is used to generate the file name, with
   * a numeric value inserted before the extension (if present) to make
   * it unique (if necessary).
   * @param tmplStr the template to use when generating the file name,
   * or null for none.
   * @param parentDirStr the name of the parent directory to use, or
   * null to use the current user directory.
   * @param useTimeFlag if true then the current time value (in ms since
   * 1/1/1970) is used in the file name, if false then an incrementing
   * value is used, starting at 1 (if the template string name itself
   * is already in use).
   * @return A new 'File' object, or null if an unused file name object
   * could not be created.
   */
  public static File createUnusedFileNameObj(String tmplStr,
                                    String parentDirStr,boolean useTimeFlag)
  {
    return createUnusedFileNameObj(tmplStr,parentDirStr,useTimeFlag,true);
  }

  /**
   * Creates a new 'File' object that references an unused file name.
   * The given template string is used to generate the file name, with
   * a numeric value added to make it unique (if necessary).
   * @param tmplStr the template to use when generating the file name,
   * or null for none.
   * @param parentDirFileObj the File object for the parent directory to
   * use, or null to use the current user directory.
   * @param useTimeFlag if true then the current time value (in ms since
   * 1/1/1970) is used in the file name, if false then an incrementing
   * value is used, starting at 1 (if the template string name itself
   * is already in use).
   * @param fileExtFlag true to insert numeric value before the filename
   * extension (if present); false to always append numeric value to end.
   * @return A new 'File' object, or null if an unused file name object
   * could not be created.
   */
  public static File createUnusedFileNameObj(String tmplStr,
              File parentDirFileObj,boolean useTimeFlag,boolean fileExtFlag)
  {
    return createUnusedFileNameObj(tmplStr,
           ((parentDirFileObj != null) ? parentDirFileObj.getPath() : null),
                                                   useTimeFlag,fileExtFlag);
  }

  /**
   * Creates a new 'File' object that references an unused file name.
   * The given template string is used to generate the file name, with
   * a numeric value inserted before the extension (if present) to make
   * it unique (if necessary).
   * @param tmplStr the template to use when generating the file name,
   * or null for none.
   * @param parentDirFileObj the File object for the parent directory to
   * use, or null to use the current user directory.
   * @param useTimeFlag if true then the current time value (in ms since
   * 1/1/1970) is used in the file name, if false then an incrementing
   * value is used, starting at 1 (if the template string name itself
   * is already in use).
   * @return A new 'File' object, or null if an unused file name object
   * could not be created.
   */
  public static File createUnusedFileNameObj(String tmplStr,
                                  File parentDirFileObj,boolean useTimeFlag)
  {
    return createUnusedFileNameObj(tmplStr,parentDirFileObj,useTimeFlag,true);
  }

  /**
   * Transfers data from the input stream to the output stream.
   * @param inputStreamObj the input stream.
   * @param outputStreamObj the output stream.
   * @exception  IOException  if an I/O error occurs.
   */
  public static void transferStream(InputStream inputStreamObj,
                            OutputStream outputStreamObj) throws IOException
  {
    int b;
    final byte [] buffer = new byte[STREAM_TRANSFER_BUFFER_SIZE];
    while((b=inputStreamObj.read(buffer,0,STREAM_TRANSFER_BUFFER_SIZE)) > 0)
    {
      outputStreamObj.write(buffer, 0, b);
    }
  }

  /**
   * Transfers data from the input stream to the output stream.
   * @param inputStreamObj the input stream.
   * @param outputStreamObj the output stream.
   * @param maxBytesLimit the maximum number of bytes to be transferred,
   * or 0 for no limit.
   * @exception  IOException  if an I/O error occurs.
   */
  public static void transferStream(InputStream inputStreamObj,
        OutputStream outputStreamObj, long maxBytesLimit) throws IOException
  {
    if(maxBytesLimit <= 0)
    {    //max-bytes limit not given; use other version of method
      transferStream(inputStreamObj,outputStreamObj);
      return;
    }
    int b;
    long byteTotal = 0;
    final byte [] buffer = new byte[STREAM_TRANSFER_BUFFER_SIZE];
    while(true)
    {    //loop while transferring buffers of data
      if(byteTotal + STREAM_TRANSFER_BUFFER_SIZE >= maxBytesLimit)
        break;          //if next transfer can reach limit then exit loop
      if((b=inputStreamObj.read(buffer,0,STREAM_TRANSFER_BUFFER_SIZE)) <= 0)
        return;         //if end-of-file reached then exit method
      outputStreamObj.write(buffer,0,b);    //write to output stream
      byteTotal += b;                       //add to byte total
    }
              //do last transfer; up to byte limit:
    if((b=inputStreamObj.read(buffer,0,(int)(maxBytesLimit-byteTotal))) > 0)
      outputStreamObj.write(buffer,0,b);
  }

  /**
   * Transfers data from the input stream to the output stream.
   * @param inputStreamObj the input stream.
   * @param outputStreamObj the output stream.
   * @param callBack callBack method or null to execute on the current thread.
   * @exception  IOException  if an I/O error occurs.
   */
  public static void transferStream(InputStream inputStreamObj,
                  OutputStream outputStreamObj, CallBackCompletion callBack)
                                                          throws IOException
  {
    if(callBack == null)  //if no callback method was provided
    {
      //transfer the data on the current thread
      transferStream(inputStreamObj, outputStreamObj);
    }
    else
    {
      //create a stream transfer thread and start it
      (new StreamTransferThread(
                          inputStreamObj,outputStreamObj,callBack)).start();
    }
  }

  /**
   * Transfers data from the input stream to the output stream.
   * @param inputStream the input stream.
   * @param outputStream the output stream.
   * @param callBack callBack method or null to execute on the current thread.
   * @param showPopup true to show a popup or false for no popup.
   */
/*
  public static void transferStream(
      InputStream inputStream, OutputStream outputStream,
      CallBackCompletion callBack, boolean showPopup)
  {
    final IstiDialogPopup popupObj;

    if(showPopup)
      popupObj = new IstiDialogPopup(
          null, "Downloading ShakeMap data...", "Downloading",
          IstiDialogPopup.PLAIN_MESSAGE, "Close", false);
    else
      popupObj = null;

    try
    {
      if(popupObj != null)  //if popup object exists
      {
        popupObj.show();     //show popup
      }

      transferStream(inputStream, outputStream, callBack);

      if(popupObj != null)  //if popup object exists
      {
        popupObj.close();
      }
    }
    catch (Exception ex)
    {
      if(popupObj != null)  //if popup object exists
      {
        final String msgStr = ex.toString();
        popupObj.setMessageStr(msgStr);
        popupObj.getOptionPaneObj().setMessageType(
            IstiDialogPopup.ERROR_MESSAGE);
        popupObj.show();     //show popup
      }
    }
  }
*/

  /**
   * Copies one file to another.  The last-modified-time value for
   * the destination file is not set to that of the source file.
   * @param sourceFileNameStr name of source file.
   * @param destFileNameStr name of destination file.
   * @throws FileNotFoundException if the source file is not found.
   * @throws IOException if an I/O error occurs.
   */
  public static void copyFile(String sourceFileNameStr,
           String destFileNameStr) throws FileNotFoundException, IOException
  {
    if(destFileNameStr.equals(sourceFileNameStr))
      throw new IOException("Destination same as source");
    BufferedInputStream inStm = null;
    BufferedOutputStream outStm = null;
    try
    {
      inStm = new BufferedInputStream(new FileInputStream(sourceFileNameStr));
      outStm = new BufferedOutputStream(new FileOutputStream(destFileNameStr));
      transferStream(inStm,outStm);
    }
    finally
    {
      close(outStm);
      close(inStm);
    }
  }

  /**
   * Copies one file to another.  The last-modified-time value for
   * the destination file is not set to that of the source file.
   * @param sourceFileObj source file object.
   * @param destFileObj destination file object.
   * @throws FileNotFoundException if the source file is not found.
   * @throws IOException if an I/O error occurs.
   */
  public static void copyFile(File sourceFileObj, File destFileObj)
                                   throws FileNotFoundException, IOException
  {
    copyFile(sourceFileObj.getPath(),destFileObj.getPath());
  }

  /**
   * Copies one file to another.
   * @param sourceFileNameStr name of source file.
   * @param destFileNameStr name of destination file.
   * @param copyLastModFlag true to set the last-modified-time value
   * for the destination file to that of the source file.
   * @throws FileNotFoundException if the source file is not found.
   * @throws IOException if an I/O error occurs.
   */
  public static void copyFile(String sourceFileNameStr,
                            String destFileNameStr, boolean copyLastModFlag)
                                   throws FileNotFoundException, IOException
  {
    copyFile(sourceFileNameStr,destFileNameStr);
    if(copyLastModFlag)
    {  //flag set; copy last-modified-time value
      copyLastModifiedTime(new File(sourceFileNameStr),
                                                 new File(destFileNameStr));
    }
  }

  /**
   * Copies one file to another.
   * @param sourceFileObj source file object.
   * @param destFileObj destination file object.
   * @param copyLastModFlag true to set the last-modified-time value
   * for the destination file to that of the source file.
   * @throws FileNotFoundException if the source file is not found.
   * @throws IOException if an I/O error occurs.
   */
  public static void copyFile(File sourceFileObj, File destFileObj,
                                                    boolean copyLastModFlag)
                                   throws FileNotFoundException, IOException
  {
    copyFile(sourceFileObj.getPath(),destFileObj.getPath());
    if(copyLastModFlag)
    {  //flag set; copy last-modified-time value
      copyLastModifiedTime(sourceFileObj,destFileObj);
    }
  }

  /**
   * Moves the specified file to a new location (even if the new location
   * is on a different filesystem).  If the file is moved to a different
   * filesystem then the last-modified-time value is copied from the
   * original file.  The 'destDirFileObj' object may specify the
   * destination directory or the destination path and filename.  The
   * destination directory must already exist.
   * @param sourceFileObj source file object.
   * @param destDirFileObj destination directory or file object.
   * @throws FileNotFoundException if the source file is not found.
   * @throws IOException if an I/O error occurs.
   */
  public static void moveFile(File sourceFileObj, File destDirFileObj)
                                   throws FileNotFoundException, IOException
  {
         //if specified destination is a directory then add filename:
    if(destDirFileObj.isDirectory())
      destDirFileObj = new File(destDirFileObj,sourceFileObj.getName());
         //attempt to move file via operating-system 'rename':
    if(!sourceFileObj.renameTo(destDirFileObj))
    {  //move via 'rename' failed; copy file to destination
      copyFile(sourceFileObj,destDirFileObj,true);
      if(destDirFileObj.exists())      //if copy succeeded then
        sourceFileObj.delete();        //delete original file
    }
  }

  /**
   * Moves the specified file to a new location (even if the new location
   * is on a different filesystem).  If the file is moved to a different
   * filesystem then the last-modified-time value is copied from the
   * original file.  The 'destDirFileObj' object may specify the
   * destination directory or the destination path and filename.  The
   * destination directory must already exist.
   * @param sourceFileNameStr name of source file.
   * @param destDirNameStr name of destination directory or file.
   * @throws FileNotFoundException if the source file is not found.
   * @throws IOException if an I/O error occurs.
   */
  public static void moveFile(String sourceFileNameStr,
                                                      String destDirNameStr)
                                   throws FileNotFoundException, IOException
  {
    moveFile(new File(sourceFileNameStr), new File(destDirNameStr));
  }

  /**
   * Copies the last-modified-time value from one file to another.
   * @param sourceFileObj source file object.
   * @param destFileObj destination file object.
   * @throws  SecurityException
   *          If a security manager exists and its <code>{@link
   *          java.lang.SecurityManager#checkWrite(java.lang.String)}</code>
   *          method denies write access to the named file.
   */
  public static void copyLastModifiedTime(File sourceFileObj,
                                                           File destFileObj)
  {
    final long timeVal;      //if time value OK then copy it over:
    if((timeVal=sourceFileObj.lastModified()) > 0)
      destFileObj.setLastModified(timeVal);
  }

  /**
   * Reads given number of lines from tail end of given file.
   * @param sourceFileNameStr name of source file.
   * @param numberOfLines number of lines to read.
   * @param outputStreamObj output stream to send read data into.
   * @throws FileNotFoundException if the source file is not found.
   * @throws IOException if an I/O error occurs.
   */
  public static void readFileTail(String sourceFileNameStr,
                           long numberOfLines, OutputStream outputStreamObj)
                                   throws FileNotFoundException, IOException
  {
    if(numberOfLines <= 0)        //if no lines then
      return;                     //just return
    final RandomAccessFile rndAccFileObj =       //open file for reading
                                new RandomAccessFile(sourceFileNameStr,"r");
    try
    {
      final long srcFileLen = rndAccFileObj.length();   //get file length
      final int buffLen = 10240;                 //read buffer size
      final byte [] buff = new byte[buffLen];      //read buffer
      long numBuffs = 0;                 //buffers-read count
      long srcFilePos = srcFileLen;      //read position in file
      int buffPos = buffLen;             //scan position in buffer
      int buffUseLen = buffLen;          //length of read buffer in use
      long lineCount = 0;                //line counter
      boolean firstBuffFlag = true;      //cleared after first buffer read
      outerLoop:
    	while(srcFilePos > 0)         //loop until beginning of file reached
    	{    //for each buffer of data read in
    	  if((srcFilePos-=buffLen) >= 0)   //subtract buffer len from file pos
    	  {  //at least one buffer-size amount of data left to process
    		rndAccFileObj.seek(srcFilePos);     //set read position in file
    		rndAccFileObj.readFully(buff);      //read in buffer of data
    		++numBuffs;                    //increment #-of-buffers-read count
    	  }
    	  else
    	  {  //less than one buffer-size about of data left to process
    		if((buffUseLen+=srcFilePos) > 0)    //calculate remaining # of bytes
    		{     //more than zero bytes remaining
    		  rndAccFileObj.seek(0);       //set read pos to beginning of file
    		  rndAccFileObj.readFully(buff,0,buffUseLen);      //read last data
    		  ++numBuffs;                  //increment #-of-buffers-read count
    		}
    		srcFilePos = 0;                //indicate beginning of file reached
    	  }
    	  if(buffUseLen > 0)
    	  {  //read buffer contains data
    		buffPos = buffUseLen;     //set read position to end of buffer
    		if(firstBuffFlag)
    		{     //this is first buffer of data processed
    		  firstBuffFlag = false;            //reset flag
    		  //if file ends with newline character then
    		  // skip past it to remove it from line count:
    		  if(buff[buffPos-1] == '\n')
    			--buffPos;
    		}
    		while(buffPos > 0)
    		{     //for each character in buffer
    		  if(buff[--buffPos] == '\n')       //decrement position in buffer
    		  {   //character is newline
    			if(++lineCount >= numberOfLines)     //increment line count
    			{      //number-of-lines value reached
    			  ++buffPos;               //set position to next after newline
    			  break outerLoop;         //exit outer loop
    			}
    		  }
    		}
    	  }
    	  else         //no data in read buffer
    		break;     //exit outer loop
    	}
      //if buffer contains data from requested lines then output it:
      if(buffPos < buffUseLen)
    	outputStreamObj.write(buff,buffPos,buffUseLen-buffPos);
      while(--numBuffs > 0)
      {    //for each remaining buffer-sized amount of requested data processed
    	rndAccFileObj.readFully(buff);   //read data from file
    	outputStreamObj.write(buff);     //output data
      }
    }
    finally
    {
      close(rndAccFileObj);             //close input file
    }
  }

  /**
   * Check the connection.
   * 
   * @param linkStr the link string.
   * @return the URL or null if error or link does not exist.
   */
  public static URL checkConnection(String linkStr)
  {
    URL urlObj = null;
    try
    {
      urlObj = checkConnection(new URL(linkStr));
    }
    catch (Exception ex)
    {
      urlObj = null;
    }
    return urlObj;
  }

  /**
   * Check the connection.
   * 
   * @param urlObj the URL.
   * @return the URL or null if error or link does not exist.
   */
  public static URL checkConnection(URL urlObj)
  {
    InputStream is = null;
    try
    {
      final URLConnection conn = openConnection(urlObj, true);
      if (conn instanceof HttpURLConnection)
      {
        ((HttpURLConnection) conn).disconnect();
      }
    }
    catch (Exception ex)
    {
      urlObj = null;
    }
    finally
    {
      UtilFns.closeQuietly(is);
    }
    return urlObj;
  }

  /**
   * Open the URL connection.
   * @param urlObj the URL object.
   * @return the URL connection.
   * @throws IOException if an I/O error occurs.
   */
  public static URLConnection openConnection(URL urlObj)
    throws IOException
  {
    return openConnection(urlObj, true);
  }

  /**
   * Open the URL connection.
   * @param urlObj the URL object.
   * @param checkResponse true to check the response code, otherwise file.
   * @return the URL connection.
   * @throws IOException if an I/O error occurs.
   */
  public static URLConnection openConnection(URL urlObj, boolean checkResponse)
	  throws IOException
  {
    URLConnection conn = urlObj.openConnection();
    if (conn instanceof HttpURLConnection)
    {
      HttpURLConnection httpConn = (HttpURLConnection) conn;
      int responseCode = httpConn.getResponseCode();
      switch (responseCode)
      {
      case HttpURLConnection.HTTP_MULT_CHOICE:
      case HttpURLConnection.HTTP_MOVED_PERM:
      case HttpURLConnection.HTTP_MOVED_TEMP:
      case HttpURLConnection.HTTP_SEE_OTHER:
      case HttpURLConnection.HTTP_USE_PROXY:
        // get redirect URL from "Location" header field
        String location = httpConn.getHeaderField("Location");
        if (location != null)
        {
          urlObj = new URL(location);
          // get the cookie if available for login
          String cookies = httpConn.getHeaderField("Set-Cookie");
          httpConn.disconnect();
          httpConn = null;
          // open the new connection again
          conn = urlObj.openConnection();
          if (cookies != null)
          {
            conn.setRequestProperty("Cookie", cookies);
          }
          if (checkResponse && conn instanceof HttpURLConnection)
          {
            httpConn = (HttpURLConnection) conn;
            responseCode = httpConn.getResponseCode();
          }
        }
        break;
      }
      if (checkResponse && responseCode != HttpURLConnection.HTTP_OK)
      {
        if (httpConn != null)
        {
          httpConn.disconnect();
          httpConn = null;
        }
        throw new HttpConnectionException(
            "Invalid Response Code (" + responseCode + ")", responseCode,
            urlObj);
      }
    }
    return conn;
  }
  
  /**
   * Close the Closeable object.
   * @param closeable the Closeable object or null if none.
   */
  public static void close(Closeable closeable)
  {
    try
    {
      if (closeable != null)
      {
    	closeable.close();
      }
    }
    catch (Exception ex)
    {
    }
  }

  /**
   * Closes the given input stream.
   * @param inputStreamObj the input stream.
   * @return true if the stream was closed successfully false otherwise.
   */
  public static boolean closeStream(InputStream inputStreamObj)
  {
    try
    {
      inputStreamObj.close();     //close the stream
      return true;      //stream was closed successfully
    }
    catch (Exception ex)
    {
      return false;     //error closing the stream
    }
  }

  /**
   * Closes the given output stream.
   * @param outputStreamObj the output stream.
   * @return true if the stream was closed successfully false otherwise.
   */
  public static boolean closeStream(OutputStream outputStreamObj)
  {
    try
    {
      outputStreamObj.close();    //close the stream
      return true;      //stream was closed successfully
    }
    catch (Exception ex)
    {
      return false;     //error closing the stream
    }
  }
}

/**
 * Stream transfer thread.
 */
class StreamTransferThread extends IstiThread
{
  private final InputStream inputStreamObj;      //input stream
  private final OutputStream outputStreamObj;    //output stream
  private final CallBackCompletion callBack;     //callback

  /**
   * Creates a file copy thread.
   * @param inputStreamObj the input stream.
   * @param outputStreamObj the output stream.
   * @param callBack callback method or null if callback not needed.
   */
  StreamTransferThread(InputStream inputStreamObj,
                  OutputStream outputStreamObj, CallBackCompletion callBack)
  {
    super("StreamTransferThread-" + nextThreadNum());
    //save the parameters
    this.inputStreamObj = inputStreamObj;
    this.outputStreamObj = outputStreamObj;
    this.callBack = callBack;
  }

  /**
   * Runs the connect thread.
   */
  public void run()
  {
    Object retVal = null;
    String errorMsg = null;

    try
    {
      FileUtils.transferStream(inputStreamObj, outputStreamObj);  //copy data
    }
    catch (Exception ex)
    {
      errorMsg = ex.toString();
    }

    if(callBack != null)  //if callback was provided
    {
      //callback and clear
      callBack.jobCompleted(retVal, errorMsg);
    }
  }
}
