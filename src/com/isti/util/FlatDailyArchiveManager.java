package com.isti.util;

import java.io.*;
import java.util.*;
import java.text.DateFormat;
import java.text.SimpleDateFormat;

/**
 * Class FlatDailyArchiveManager is an extension of ArchiveManager that
 * uses a single file for each day, all stored in a single directory.
 * Old archive files are purged (deleted) via the 'purgeArchive()' method,
 * but old items within archive files are not purged (unless the behavior
 * is changed via the 'setPurgeIntoArchiveFlag()' method).
 *  7/21/2003 -- [ET]  Changed default date-format time zone to GMT;
 *                     added 'setDateFormatTimeZone()'.
 *  9/16/2003 -- [ET]  Removed 'archiveOutStmSyncObj', instead using
 *                     'archiveAccessSyncObj'.
 *  9/17/2003 -- [KF]  Extend ExtendedArchiveManager.
 *  9/19/2003 -- [KF]  Fix problem with NullPointerException with
 *                     the 'getArchiveFilesForDateRange' method.
 *  5/18/2004 -- [KF]  Create a new date formatter for each instance.
 *  9/24/2007 -- [ET]  Added 'getDateForArchiveFileName()' and
 *                     'doesNextArchiveFileExist()' methods.
 *  4/28/2010 -- [ET]  Added optional 'strictArchiveDateFlag' parameter
 *                     to constructor.
 */
public class FlatDailyArchiveManager extends ExtendedArchiveManager
{
    /** Default date-formatter pattern string ("yyyyMMdd"). */
  public static final String DEF_DATE_FORMAT_STR = "yyyyMMdd";
    /** Default datecode separator string ("_"). */
  public static final String DEF_FNAME_DATE_SEPSTR = "_";
                        //date formatter for parsing and formatting dates:
  private DateFormat dateFormatObj = null;
                        //flag used to track 'dateFormatObj':
  private boolean dateFormatChangedFlag = false;
                        //datecode separator string:
  private String fNameDateSepStr = DEF_FNAME_DATE_SEPSTR;
                        //filename str for current 'archiveOutStm' object:
  private String archiveStmFileNameStr = null;
                        //filename datecode parser:
  private SimpleDateFormat fileNameDateParserObj;

  /**
   * Creates a new FlatDailyArchiveManager.  The 'purgeIntoArchiveFlag'
   * is set 'false' (see 'setPurgeIntoArchiveFlag()' for details).
   * The given class must have a constructor with 'String' and
   * 'Archivable.Marker' parameters, like this:
   * <br>
   * public ArchivableImpl(String dataStr, Archivable.Marker mkrObj) ...
   * <br>
   * This constructor is used to create the class from a string of data.
   * The 'Archivable.Marker' is used to mark the constructor as being
   * available for de-archiving (and will usually be 'null').
   * @param classObj class object representing the class to be archived.
   * @param archiveRootDirName name of root directory in which to place
   * archive files (and possibly directories), or null to use the
   * current working directory.
   * @param baseFileNameStr the "base" file name to used to generate
   * archive file names.  A date code will be inserted before the
   * extension part of the file name.
   * @param strictArchiveDateFlag true if items must be in ascending
   * date/time order; false if not.
   * @throws NoSuchMethodException if a proper constructor does not exist
   * for the class.
   * @throws  NullPointerException
   *          If <code>baseFileNameStr</code> is <code>null</code>
   */
  public FlatDailyArchiveManager(Class classObj, String archiveRootDirName,
                      String baseFileNameStr, boolean strictArchiveDateFlag)
                                                throws NoSuchMethodException
  {
    super(classObj,archiveRootDirName,baseFileNameStr,strictArchiveDateFlag);
    setPurgeIntoArchiveFlag(false);         //clear purge-into flag
    createDefaultDateFormatter();  //create default date formatter
  }

  /**
   * Creates a new FlatDailyArchiveManager.  The 'purgeIntoArchiveFlag'
   * is set 'false' (see 'setPurgeIntoArchiveFlag()' for details).
   * The given class must have a constructor with 'String' and
   * 'Archivable.Marker' parameters, like this:
   * <br>
   * public ArchivableImpl(String dataStr, Archivable.Marker mkrObj) ...
   * <br>
   * This constructor is used to create the class from a string of data.
   * The 'Archivable.Marker' is used to mark the constructor as being
   * available for de-archiving (and will usually be 'null').
   * Archived items must be added in ascending date/time order.
   * @param classObj class object representing the class to be archived.
   * @param archiveRootDirName name of root directory in which to place
   * archive files (and possibly directories), or null to use the
   * current working directory.
   * @param baseFileNameStr the "base" file name to used to generate
   * archive file names.  A date code will be inserted before the
   * extension part of the file name.
   * @throws NoSuchMethodException if a proper constructor does not exist
   * for the class.
   * @throws  NullPointerException
   *          If <code>baseFileNameStr</code> is <code>null</code>
   */
  public FlatDailyArchiveManager(Class classObj, String archiveRootDirName,
                        String baseFileNameStr) throws NoSuchMethodException
  {
    this(classObj,archiveRootDirName,baseFileNameStr,true);
  }

  /**
   * Creates a new FlatDailyArchiveManager.  The 'purgeIntoArchiveFlag'
   * is set 'false' (see 'setPurgeIntoArchiveFlag()' for details).
   * The given class must have a constructor with 'String' and
   * 'Archivable.Marker' parameters, like this:
   * <br>
   * public ArchivableImpl(String dataStr, Archivable.Marker mkrObj) ...
   * <br>
   * This constructor is used to create the class from a string of data.
   * The 'Archivable.Marker' is used to mark the constructor as being
   * available for de-archiving (and will usually be 'null').  The
   * archive files will be placed in the current working directory.
   * Archived items must be added in ascending date/time order.
   * @param classObj class object representing the class to be archived.
   * @param baseFileNameStr the "base" file name to used to generate
   * archive file names.  A date code will be inserted before the
   * extension part of the file name.
   * @throws NoSuchMethodException if a proper constructor does not exist
   * for the class.
   * @throws  NullPointerException
   *          If <code>baseFileNameStr</code> is <code>null</code>
   */
  public FlatDailyArchiveManager(Class classObj, String baseFileNameStr)
                                                throws NoSuchMethodException
  {
    this(classObj,null,baseFileNameStr,true);
  }

  /**
   * Sets the 'DateFormat' object used to generate date codes inserted into
   * archive file names.  The date codes must be in year-month-day order
   * such that a lexicographic sort will result in proper ordering by time.
   * If this method is not called then the default 'DateFormat' object is
   * used ("yyyyMMdd").
   * @param dFormatObj the 'DateFormat' object to use, or null to
   * set the default 'DateFormat' object.
   */
  public void setDateFormatObj(DateFormat dFormatObj)
  {
    synchronized(archiveAccessSyncObj)
    {    //only allow one thread access to the archive at a time
      if (dFormatObj != null)
        dateFormatObj = dFormatObj;
      else
        createDefaultDateFormatter();  //create default date formatter

      dateFormatChangedFlag = true;         //indicate formatter changed
    }
  }

  /**
   * Returns the 'DateFormat' object used to generate date codes inserted
   * into archive file names.
   * @return A 'DateFormat' object.
   */
  public DateFormat getDateFormatObj()
  {
    return dateFormatObj;
  }

  /**
   * Sets the time zone for the 'DateFormat' object used to generate date
   * codes inserted into archive file names.  The default time zone is
   * GMT.
   * @param timeZoneObj the time zone object to use.
   */
  public void setDateFormatTimeZone(TimeZone timeZoneObj)
  {
    dateFormatObj.setTimeZone(timeZoneObj);
  }

  /**
   * Sets the separator string put in front of date codes inserted into
   * archive file names.  If this method is not called then the default
   * separator string is used ("_").
   * @param sepStr the separator string to use, or null to set the
   * default separator string.
   */
  public void setFNameDateSepStr(String sepStr)
  {
    synchronized(archiveAccessSyncObj)
    {    //only allow one thread access to the archive at a time
      fNameDateSepStr = (sepStr != null) ? sepStr : DEF_FNAME_DATE_SEPSTR;
    }
  }

  /**
   * Returns the separator string put in front of date codes inserted into
   * archive file names.
   * @return The separator string.
   */
  public String getFNameDateSepStr()
  {
    return fNameDateSepStr;
  }

  /**
   * Creates the default date formatter.
   */
  protected void createDefaultDateFormatter()
  {
    //create default date-formatter (YYYYMMDD), using GMT time zone.
    dateFormatObj = UtilFns.createDateFormatObj(
        DEF_DATE_FORMAT_STR, UtilFns.GMT_TIME_ZONE_OBJ);
  }

  /**
   * Returns the name of the archive file that would contain items with
   * the given date.
   * @param dateObj the date object to use.
   * @return The file name string.
   */
  protected String getArchiveFileNameForDate(Date dateObj)
  {
    return FileUtils.addStrToFileName(archiveFileNameStr,
                         (fNameDateSepStr + dateFormatObj.format(dateObj)));
  }

  /**
   * Returns an output stream for the file that the item should be
   * archived to, with a name based on the archive date of item.
   * @param item the item to be archived.
   * @return An 'OutputStream' to be used to write to the archive file.
   * @throws IOException if the archive file cannot be opened.
   */
  protected OutputStream getOutputStreamForItem(Archivable item)
                                                          throws IOException
  {
    final String fileNameStr =         //create archive filename for item
                           getArchiveFileNameForDate(item.getArchiveDate());
    if(archiveOutStm != null)
    {  //output stream for archive currently allocated
      if(archiveStmFileNameStr != null &&
                                archiveStmFileNameStr.equals(fileNameStr))
      {     //file name string for stream exists and matches file name
        return archiveOutStm;   //return previously-setup stream
      }
      try { archiveOutStm.close(); }      //close previously-setup stream
      catch(Exception ex) {}
      archiveOutStm = null;               //release object
      archiveStmFileNameStr = null;       //clear file name string
    }
                 //create file with generated file name:
    final File fileObj = getArchiveFileForName(fileNameStr);
                 //setup output stream to file, opened for append:
    archiveOutStm = new BufferedOutputStream(
                            new FileOutputStream(fileObj.getPath(),true));
    archiveStmFileNameStr = fileNameStr;  //save file name string
    return archiveOutStm;       //return output stream for archive file
  }

  /**
   * Returns all of the archive file names that can potentially contain
   * items with an archive date between startDate and endDate (inclusive).
   * These files should be in order from oldest to newest.
   * @param startDate the start of the time range, or null to indicate
   * the "beginning of time".
   * @param endDate the end of the time range, or null to indicate
   * the "end of time".
   * @return A new 'Vector' of File objects.
   */
  protected Vector getArchiveFilesForDateRange(Date startDate, Date endDate)
  {
    final Vector retVec = new Vector();     //Vector of File objs to return
         //get list of files in the archive-root directory
         // that match the "name_*.ext" filter:
    final File [] fileArr =
              (new File((archiveRootDirName!=null)?archiveRootDirName:".")).
                                  listFiles(IstiFileFilter.createFileFilter(
                                                 FileUtils.addStrToFileName(
                                archiveFileNameStr,(fNameDateSepStr+"*"))));
    if (fileArr != null && fileArr.length > 0)  //if file array is not empty
    {
      //perform lexicographic sort to put files in order by time:
      Arrays.sort(fileArr);
      //setup start-time value in milliseconds (if available):
      final long startDateMsVal = (startDate != null) ?
                                  startDate.getTime() : 0;
      //setup end-time value in milliseconds (if available):
      final long endDateMsVal = (endDate != null) ? endDate.getTime() : 0;
      //verify which file names have valid date codes between
      // the start/end dates and add verified files to the Vector:
      File fileObj;
      Date dateObj;
      long dateMsVal;
      for(int i=0; i<fileArr.length; ++i)
      {    //for each file in array
        fileObj = fileArr[i];
                        //parse date code from file:
        dateObj = getDateForArchiveFileName(fileObj.getName());
                        //get epoch ms version of date code:
        dateMsVal = (dateObj != null) ? dateObj.getTime() : 0;
//      System.out.println("FDAM:  dateObj=\"" + dateObj + "\", startDate=\"" +
//                             startDate + "\", endDate=\"" + endDate + "\"");
        if(dateObj != null && (startDate == null ||
                           dateMsVal+UtilFns.MS_PER_DAY > startDateMsVal) &&
                             (endDate == null || dateMsVal <= endDateMsVal))
        {  //date code parsed and with start/end dates (if given)
          retVec.add(fileObj);      //add file to Vector
//        System.out.println("FDAM:  Added \"" + fileObj + "\"");
        }
      }
    }
    return retVec;           //return Vector of 'File' objects
  }

  /**
   * Returns a 'DateFormat' parser that may be used to parse a date value
   * from an archive file name.  Usages of this date formatter should
   * be thread-synchronized to the formatter object.
   * @return A 'DateFormat' parser object.
   */
  public DateFormat getFileNameDateParserObj()
  {
    if(fileNameDateParserObj == null || dateFormatChangedFlag)
    {    //parser not yet setup or 'dateFormatObj' changed
      dateFormatChangedFlag = false;
         //create date-parser to extract date codes from file names:
      final Object obj;
      final String qt = "'"; //quote character for parse-pattern string
         //attempt to convert clone of date-formatter to date-parser:
      if((obj=dateFormatObj.clone()) instanceof SimpleDateFormat)
      {  //date-formatter object type is SimpleDateFormat
        fileNameDateParserObj = (SimpleDateFormat)obj;   //set handle to obj
              //create date parser with pattern:  'name_'yyyyMMdd'.ext'
        fileNameDateParserObj.applyPattern(FileUtils.addStrToFileName(
                                                 (qt+archiveFileNameStr+qt),
                (fNameDateSepStr+qt+fileNameDateParserObj.toPattern()+qt)));
      }
      else
      {  //date-formatter object type is not SimpleDateFormat
              //create default date formatter:
        fileNameDateParserObj = new SimpleDateFormat(
                      FileUtils.addStrToFileName((qt+archiveFileNameStr+qt),
                              (fNameDateSepStr+qt+DEF_DATE_FORMAT_STR+qt)));
      }
      fileNameDateParserObj.setLenient(false);   //strict pattern parsing
    }
    return fileNameDateParserObj;
  }

  /**
   * Parses the given filename for a date code.
   * @param fNameStr name of archive file.
   * @return A new 'Date' object containing the date code, or null if
   * the date code could not be parsed.
   */
  protected Date getDateForArchiveFileName(String fNameStr)
  {
    Date dateObj;
    try
    {                   //get date-format parser object:
      final DateFormat fnDateParserObj = getFileNameDateParserObj();
      synchronized(fnDateParserObj)
      {  //thread-synchronize access to date-formatter
                        //parse date code from filename:
        dateObj = fnDateParserObj.parse(fNameStr);
      }
    }
    catch(Exception ex)
    {            //some kind of exception error
      dateObj = null;      //indicate did not parse
    }
    return dateObj;
  }

  /**
   * Determines if the next file in the archive sequence exists.  For
   * this "daily" archiver it is the archive file for the day following
   * date specified in the given archive filename.
   * @param fNameStr name of archive file.
   * @return true if the next file in the archive sequence exists (or
   * a date code could not be parsed from the given filename); false
   * otherwise.
   */
  protected boolean doesNextArchiveFileExist(String fNameStr)
  {
                   //get date code for given filename, increase by one
                   // day and test if archive file for that date exists:
    final Date dateObj;
    return ((dateObj=getDateForArchiveFileName(fNameStr)) != null) &&
                                        (new File(getArchiveFileNameForDate(
                 new Date(dateObj.getTime()+UtilFns.MS_PER_DAY)))).exists();
  }
}
