//HeartbeatFile.java:  Defines a utility that generates a heartbeat
//                 file that can be used to determine if the program is running.
//
//  8/31/2005 -- [KF]  Initial version.
//   9/8/2005 -- [KF]  Changed to ensure the heartbeat file directory exists.
// 12/11/2009 -- [ET]  Fixed 'setLogObj()' response to 'null' parameter;
//                     increased debug level on periodic log outputs.
//

package com.isti.util;

import java.io.File;
import java.util.Iterator;
import java.util.List;
import java.util.Vector;

import com.isti.util.queue.NotifyThread;


/**
 * Class HeartbeatFile defines a utility that generates a heartbeat
//                 file that can be used to determine if the program is running.
 */
public class HeartbeatFile
{
  //the minimum interval for updates
  private final int minIntervalMSecs = 1;
  //the heartbeat thread sync object
  private final Object heartbeatThreadSyncObj = new Object();
  //the heartbeat thread
  private HeartbeatThread heartbeatThreadObj = null;
  //the mandatory thread sync list
  private final Object threadListSyncObj = new Object();
  //the mandatory thread list
  private List threadListObj = null;

  private String heartbeatFilename;  //the filename for the heartbeat file
  private int intervalMSecs;  //the interval for the heartbeat file
  private LogFile logObj;  //the LogFile object to use for output

  /**
   * Creates a thread heartbeat object.
   *
   * The "addThread", "setFilename", "setIntervalMSecs" and "setLogObj" methods
   * should be called to setup the heartbeat parameters before starting the
   * heartbeat thread via the "start" method.
   */
  public HeartbeatFile()
  {
    heartbeatFilename = null;
    intervalMSecs = 0;
    setLogObj(null);
  }

  /**
   * Adds a mandatory thread for the heartbeat.
   * @param threadObj the Thread object.
   * @return true if the thread was added, false otherwise.
   */
  public boolean addThread(NotifyThread threadObj)
  {
    return getThreadList().add(threadObj);
  }

  /**
   * Adds a mandatory thread for the heartbeat.
   * @param threadObj the Thread object.
   * @return true if the thread was added, false otherwise.
   */
  public boolean addThread(Thread threadObj)
  {
    return getThreadList().add(threadObj);
  }

  /**
   * Determines if the thread name.
   * @param obj the thread object.
   * @return the thread name.
   */
  protected String getThreadName(Object obj)
  {
    if (obj instanceof Thread)
    {
      return ((Thread)obj).getName();
    }
    return obj.toString();
  }

  /**
   * Creates the thread list.
   * @return the thread list.
   */
  protected List createThreadList()
  {
    return new Vector();
  }

  /**
   * Gets the thread list.
   * @return the thread list.
   */
  protected final List getThreadList()
  {
    synchronized (threadListSyncObj)
    {
      if (threadListObj == null)
        threadListObj = createThreadList();
      return threadListObj;
    }
  }

  /**
   * Gets the thread list iterator.
   * @return the thread list iterator.
   */
  protected Iterator getThreadListIterator()
  {
    return getThreadList().iterator();
  }

  /**
   * Determines if the thread is alive.
   * @param obj the thread object.
   * @return true if the thread is alive, false otherwise.
   */
  protected boolean isThreadAlive(Object obj)
  {
    if (obj instanceof Thread)
    {
      return ((Thread)obj).isAlive();
    }
    if (obj instanceof NotifyThread)
    {
      return ((NotifyThread)obj).isRunning();
    }
    logObj.warning(
          "Heartbeat thread \"" + obj.toString() +
          "\" is an unsupported class:  " + obj.getClass().getName());
    return false;
  }

  /**
   * Sets the filename for the heartbeat file.
   * @param heartbeatFilename the filename for the heartbeat file.
   */
  public void setFilename(String heartbeatFilename)
  {
    this.heartbeatFilename = heartbeatFilename;
  }

  /**
   * Sets the interval for the heartbeat file.
   * @param intervalMSecs the interval for the heartbeat file.
   */
  public void setIntervalMSecs(int intervalMSecs)
  {
    //don't allow interval to be less than minimum:
    this.intervalMSecs =
        (intervalMSecs >= minIntervalMSecs) ? intervalMSecs : minIntervalMSecs;
  }

  /**
   * Sets the logfile object.
   * @param logObj the LogFile object to use for output or null for none.
   */
  public void setLogObj(LogFile logObj)
  {
    this.logObj = (logObj != null) ? logObj : LogFile.getNullLogObj();
  }

  /**
   * Starts the heartbeat thread.
   *
   * The "addThread", "setFilename", "setIntervalMSecs" and "setLogObj" methods
   * should be called to setup the heartbeat parameters before starting the
   * heartbeat thread.
   * @return true if there was an error starting the heartbeat thread,
   * false otherwise.
   */
  public boolean start()
  {
    try
    {
      synchronized (heartbeatThreadSyncObj)
      {
        if (intervalMSecs >= minIntervalMSecs && heartbeatThreadObj == null)
        {
          //make sure the heartbeat file directory exists
          final File heartbeatFile = new File(heartbeatFilename);
          final File heartbeatParentFile = heartbeatFile.getParentFile();
          if (heartbeatParentFile != null && !heartbeatParentFile.exists())
          {
            if (!heartbeatParentFile.mkdirs())  //if error creating the directory
            {
              logObj.warning(
                  "Heartbeat file directory \"" +
                  heartbeatParentFile.getAbsolutePath() +
                  "\" could not be created.");
              return false;
            }
          }
          heartbeatThreadObj = new HeartbeatThread();
          heartbeatThreadObj.start();
        }
      }
    }
    catch (Exception ex)
    {
      logObj.warning(
          "Heartbeat thread could not be started:  " + ex);
      return false;
    }
    return true;
  }

  /**
   * Terminates the heartbeat thread.
   */
  public void terminate()
  {
    synchronized (heartbeatThreadSyncObj)
    {
      if (heartbeatThreadObj != null)
      {
        heartbeatThreadObj.terminate();
        heartbeatThreadObj = null;
      }
    }
  }

  /**
   * Updates the heartbeat file if all mandatory threads are alive.
   * @return true if the heartbeat file was updated, false otherwise.
   */
  private boolean updateHeartbeat()
  {
    String threadName;
    Object obj;
    boolean updateFlag = true;  //default to updating the heartbeat file
    if (getThreadList() == null || getThreadList().size() <= 0)
    {
      logObj.debug2("Heartbeat thread list is empty");
    }
    else
    {
      final Iterator it = getThreadListIterator();
      while (it.hasNext())
      {
        obj = it.next();
        threadName = getThreadName(obj);
        if (!isThreadAlive(obj))  //if the thread is not alive
        {
          updateFlag = false; //do not update the heartbeat file
          logObj.warning(
              "Heartbeat thread \"" + threadName + "\" is not alive");
        }
        else
        {
          logObj.debug3(
              "Heartbeat thread \"" + threadName + "\" is alive");
        }
      }
    }

    if (!updateFlag)  //exit if any threads are not well
      return false;

    try
    {
      final String heartbeatText = Long.toString(System.currentTimeMillis());
      FileUtils.writeStringToFile(
      heartbeatFilename, heartbeatText, false);
      logObj.debug3(
      "Heartbeat file (" + heartbeatFilename +
      ") updated:  " + heartbeatText);
    }
    catch (Exception ex)
    {
      logObj.warning(
          "Heartbeat file (" + heartbeatFilename +
          ") could not be updated:  " + ex);
    }
    return true;
  }

  /**
   * Class HeartbeatThread defines a separate thread used to build the
   * heartbeat file.
   */
  private class HeartbeatThread extends IstiThread
  {
    /**
     * Creates a heartbeat thread.
     * @param threadName the name for this thread.
     */
    public HeartbeatThread(String threadName)
    {
      super(threadName);
      setDaemon(true);       //make this a "daemon" (background) thread
    }

    /**
     * Creates a heartbeat thread.  A default name is given to the thread.
     */
    public HeartbeatThread()
    {
      this("HeartbeatThread-" + nextThreadNum());
    }

    /**
     * Executing method for heartbeat thread.
     */
    public void run()
    {
      while(!isTerminated())
      {       //loop until terminate flag set
        try
        {
          updateHeartbeat();  //update the heartbeat
          try
          {
            sleep(intervalMSecs);  //delay between heartbeats
          }
          catch(InterruptedException ex)  //sleep delay interrupted
          {
            if(isTerminated())           //if terminate flag set then
              break;                     //exit loop
          }
        }
        catch(Exception ex)
        {
          logObj.warning(getName() + "  :" + ex);
        }
      }
      logObj.debug(getName() + " terminated OK");
    }
  }
}
