package com.isti.util;

public interface HtmlConstants {
  /** Anchor element. */
  public static final String ANCHOR_ELEMENT = "A";

  /** Bold element. */
  public static final String BOLD_ELEMENT = "B";

  /** Color attribute. */
  public static final String COLOR_ATTRIBUTE = "COLOR";

  /** Font element. */
  public static final String FONT_ELEMENT = "FONT";

  /** Horizontal Rule element. */
  public static final String HORIZONTAL_RULE_ELEMENT = "HR";

  /** HREF attribute. */
  public static final String HREF_ATTRIBUTE = "HREF";

  /** HTML element. */
  public static final String HTML_ELEMENT = "HTML";

  /** Line break element. */
  public static final String LINE_BREAK_ELEMENT = "BR";

  /** Paragraph element. */
  public static final String PARAGRAPH_ELEMENT = "P";

  /** Word Break element. */
  public static final String WORD_BREAK_ELEMENT = "WBR";
}
