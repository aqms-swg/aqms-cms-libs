//HtmlUtilFns.java:  Contains various static HTML utility functions
//
//  1/14/2008 -- [KF]  Initial version.
//  3/13/2012 -- [KF]  Added 'createHref' and 'createTag' methods.
//
//=====================================================================
// Copyright (C) 2012 Instrumental Software Technologies, Inc.
//
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions
// are met:
// 1. Redistributions of source code, or portions of this source code,
//    must retain the above copyright notice, this list of conditions
//    and the following disclaimer.
// 2. Redistributions in binary form must reproduce the above copyright
//    notice, this list of conditions and the following disclaimer in
//    the documentation and/or other materials provided with the
//    distribution.
// 3. All advertising materials mentioning features or use of this
//    software must display the following acknowledgment:
//    "This product includes software developed by Instrumental
//    Software Technologies, Inc. (http://www.isti.com)"
// 4. If the software is provided with, or as part of a commercial
//    product, or is used in other commercial software products the
//    customer must be informed that "This product includes software
//    developed by Instrumental Software Technologies, Inc.
//    (http://www.isti.com)"
// 5. The names "Instrumental Software Technologies, Inc." and "ISTI"
//    must not be used to endorse or promote products derived from
//    this software without prior written permission. For written
//    permission, please contact "info@isti.com".
// 6. Products derived from this software may not be called "ISTI"
//    nor may "ISTI" appear in their names without prior written
//    permission of Instrumental Software Technologies, Inc.
// 7. Redistributions of any form whatsoever must retain the following
//    acknowledgment:
//    "This product includes software developed by Instrumental
//    Software Technologies, Inc. (http://www.isti.com/)."
// THIS SOFTWARE IS PROVIDED BY INSTRUMENTAL SOFTWARE
// TECHNOLOGIES, INC. "AS IS" AND ANY EXPRESSED OR IMPLIED
// WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES
// OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
// DISCLAIMED.  IN NO EVENT SHALL INSTRUMENTAL SOFTWARE TECHNOLOGIES,
// INC. OR ITS CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
// INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
// (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
// SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
// HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
// STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
// ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED
// OF THE POSSIBILITY OF SUCH DAMAGE.
//=====================================================================
//  A current version of the software can be found at
//                http://www.isti.com
//  Bug reports and comments should be directed to
//  Instrumental Software Technologies, Inc. at info@isti.com
//=====================================================================
//

package com.isti.util;

import java.awt.Color;
import java.util.Arrays;
import java.util.List;

/**
 * Class HtmlUtilFns contains various static HTML utility methods.
 */
public class HtmlUtilFns implements HtmlConstants
{
  /** HTML start tag, any HTML strings should start with this. */
  public static final String HTML_START_TAG =
      getTagText(HTML_ELEMENT, true, null);

  /** The close tag optional element list. */
  private static final String[] closeTagOptionalElements =
  { HORIZONTAL_RULE_ELEMENT, LINE_BREAK_ELEMENT, PARAGRAPH_ELEMENT, WORD_BREAK_ELEMENT };
  
  /** The close tag optional element list. */
  private static final List closeTagOptionalElementList =
    Arrays.asList(closeTagOptionalElements);

  //private constructor so that no object instances may be created
  // (static access only)
  private HtmlUtilFns()
  {
  }

  /**
   * Create the HREF text.
   * @param url the URL.
   * @param s the text or null if none.
   * @return the HREF text.
   */
  public static String createHref(String url, String s)
  {
    return createTag(ANCHOR_ELEMENT, s, getNameValueText(HREF_ATTRIBUTE, url));
  }

  /**
   * Create the tag text.
   * @param element the element (such as BOLD_ELEMENT) or null if none.
   * @return the tag text or null if none.
   */
  public static String createTag(String element)
  {
    return createTag(element, (String) null);
  }

  /**
   * Create the tag text.
   * @param element the element (such as BOLD_ELEMENT) or null if none.
   * @param s the text or null if none.
   * @return the tag text or null if none.
   */
  public static String createTag(String element, String s)
  {
    return createTag(element, s, (String[]) null);
  }

  /**
   * Create the tag text.
   * @param element the element (such as BOLD_ELEMENT) or null if none.
   * @param s the text or null if none.
   * @param attributeText the attribute text or null if none.
   * @return the tag text or null if none.
   */
  public static String createTag(String element, String s,
      String attributeText)
  {
    return createTag(element, s, getStringArray(attributeText));
  }

  /**
   * Create the tag text.
   * @param element the element (such as BOLD_ELEMENT) or null if none.
   * @param s the text or null if none.
   * @param attributeTextArray the attribute text array or null if none.
   * @return the tag text or null if none.
   */
  public static String createTag(String element, String s,
      String[] attributeTextArray)
  {
    String attributeText;
    boolean closingTagFlag = true;
    final StringBuffer sb = new StringBuffer();
    sb.append('<');
    sb.append(element);
    if (attributeTextArray != null)
    {
      for (int i = 0; i < attributeTextArray.length; i++)
      {
        attributeText = attributeTextArray[i];
        sb.append(" " + attributeText);
      }
    }
    sb.append('>');
    
    if (s != null)
    {
      sb.append(s);
    }
    else if (isClosingTagOptional(element))
    {
      closingTagFlag = false;
    }

    if (closingTagFlag)
    {
      sb.append("</");
      sb.append(element);
      sb.append('>');
    }
    return sb.toString();
  }

  /**
   * Gets the font color text.
   * @param color the color or null if none.
   * @return the font color text or null if none.
   */
  public static String getColorAttributeText(Color color)
  {
    if (color != null)
    {
      return COLOR_ATTRIBUTE + "=" +
          Integer.toHexString(color.getRGB() & 0xFFFFFF);
    }
    return null;
  }
  
  /**
   * Gets the tag text.
   * @param element the element (such as BOLD_ELEMENT) or null if none.
   * @param startFlag true for start, false for end.
   * @param attributeText the attribute text or null if none.
   * @return the tag text or null if none.
   */
  private static String getTagText(
      String element, boolean startFlag, String attributeText)
  {
    if (element == null)
    {
      return element;
    }
    final StringBuffer sb = new StringBuffer();
    sb.append('<');
    if (!startFlag)
    {
      sb.append('/');
    }
    sb.append(element);
    if (attributeText != null)
    {
      sb.append(" " + attributeText);
    }
    sb.append('>');
    return sb.toString();
  }

  /**
   * Gets the font text for the specified color.
   * @param s the text or null if none.
   * @param color the color or null if none.
   * @return the font text.
   */
  public static String getFontText(String s, Color color)
  {
    return getText(s, FONT_ELEMENT, getColorAttributeText(color));
  }

  /**
   * Gets the html text by adding the HTML elements if needed.
   * @param s the text or null if none.
   * @return the html text or null if none.
   */
  public static String getHtmlText(String s)
  {
    if (s != null && !startsWithHtml(s))
    {
      return getText(s, HTML_ELEMENT);
    }
    return s;
  }
  
  /**
   * Get the name value text.
   * @param name the name.
   * @param value the value.
   * @return the name value text.
   */
  public static String getNameValueText(String name, String value)
  {
    return name + "=" + getQuotedText(value);
  }

  /**
   * Get the quoted text.
   * @param s the text.
   * @return the quoted text.
   */
  public static String getQuotedText(String s)
  {
    return StringConstants.QUOTE_STRING + s + StringConstants.QUOTE_STRING;
  }

  /**
   * Gets the string array with the specified string.
   * @param s the string or null if none.
   * @return the string array or null if none.
   */
  public static String[] getStringArray(String s)
  {
    if (s == null)
      return null;
    return new String[] { s };
  }

  /**
   * Gets the text with the specified element.
   * @param s the text or null if none.
   * @param element the element (such as BOLD_ELEMENT) or null if none.
   * @return the text with the specified element text.
   */
  public static String getText(String s, String element)
  {
    return getText(s, element, null);
  }

  /**
   * Gets the text with the specified element.
   * @param s the text or null if none.
   * @param element the element (such as BOLD_ELEMENT) or null if none.
   * @param attributeText the attribute text or null if none.
   * @return the text with the specified element text.
   */
  public static String getText(String s, String element, String attributeText)
  {
    if (s != null && element != null)
    {
      return getTagText(element, true, attributeText) + s +
          getTagText(element, false, null);
    }
    return s;
  }
  
  /**
   * Determines if the closing tag is optional for the specified element.
   * @param element the element (such as BOLD_ELEMENT) or null if none.
   * @return true if the closing tag is optional, false otherwise.
   */
  public static boolean isClosingTagOptional(String element)
  {
    return closeTagOptionalElementList.contains(element);
  }

  /**
   * Determines if the text starts with the HTML start tag.
   * @param s the text.
   * @return true if the text starts with the HTML start tag, false
   *         otherwise.
   */
  public static boolean startsWithHtml(String s)
  {
    return s.toUpperCase().startsWith(HTML_START_TAG);
  }
}
