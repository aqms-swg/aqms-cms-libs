package com.isti.util;

import java.io.IOException;
import java.net.URL;

/**
 * Thrown to indicate that a HTTP connection could not be opened.
 */
public class HttpConnectionException extends IOException
{
  private static final long serialVersionUID = -1;
  private final int responseCode;
  private final URL urlObj;

  /**
   * Constructs a new {@code HttpConnectionException} with detail message
   * responseCode and the URL.
   * 
   * @param detail       the detail message.
   * @param responseCode the HTTP response code from server.
   * @param urlObj       the URL.
   */
  public HttpConnectionException(String detail, int responseCode, URL urlObj)
  {
    super(detail);
    this.responseCode = responseCode;
    this.urlObj = urlObj;
  }

  /**
   * Get the URL.
   * 
   * @return the URL.
   */
  public URL getURL()
  {
    return urlObj;
  }

  /**
   * Returns the HTTP response code
   *
   * @return The HTTP response code.
   */
  public int responseCode()
  {
    return responseCode;
  }
}
