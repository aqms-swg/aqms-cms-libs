package com.isti.util;

/**
 * The ISTI column filter.
 */
public interface IstiColumnFilter {
  /**
   * Determines if the column is visible.
   * @param columnIndex the actual column index.
   * @return true if the column is visible, false otherwise.
   */
  public boolean isColumnVisible(int columnIndex);
}
