//IstiDataInputStream:  Extends DataInputStream.
//
// NOTE: DataInputStream can't be directly extended since it has final methods.
//
//   1/23/2003 -- [KF]
//

package com.isti.util;

import java.io.*;

public class IstiDataInputStream extends FilterInputStream implements DataInput
{
  //buffer
  private final byte[] buf;

  //if true default is little-endian otherwise it is big-endian.
  private boolean littleEndian;

  //Data Input Stream
  private final DataInputStream dis;

  //Buffered reader for readLine method
  private BufferedReader br;

  /**
   * Create an IstiDataInputStream that reads from the specified InputStream.
   *
   * This is the same as an DataInputStream with the following changes:
   * supports reading little-endian as well as big-endian binary data,
   * uses a BufferedReader for the readLine method,
   * and supports reading arrays.
   *
   * The default is initially big-endian.
   *
   * @param in  the underlying <code>InputStream</code> from which to read
   * @exception StreamCorruptedException The version or magic number are
   * incorrect.
   * @exception IOException An exception occurred in the underlying stream.
   */
  public IstiDataInputStream(InputStream in)
      throws IOException, StreamCorruptedException
  {
    this(in, false);  //default is initially big-endian
  }

  /**
   * Create an IstiDataInputStream that reads from the specified InputStream.
   *
   * This is the same as an DataInputStream except it supports reading
   * little-endian as well as big-endian binary data.
   *
   * @param in  the underlying <code>InputStream</code> from which to read
   * @exception StreamCorruptedException The version or magic number are
   * incorrect.
   * @param little if true default is little-endian otherwise it is big-endian.
   * @exception IOException An exception occurred in the underlying stream.
   */
  public IstiDataInputStream(InputStream in, boolean little)
      throws IOException, StreamCorruptedException
  {
    super(in);
    dis = new DataInputStream(in);
    buf = new byte[8];
    br = null;
    littleEndian = little;  //save default
  }

  /**
   * Determines if the default is big-endian.
   * @return true if the default is big-endian.
   */
  public boolean isBigEndian()
  {
    return !littleEndian;
  }

  /**
   * Determines if the default is little-endian.
   * @return true if the default is little-endian.
   */
  public boolean isLittleEndian()
  {
    return littleEndian;
  }

  /**
   * Sets the default to big-endian or little-endian.
   * @param big if true default is big-endian otherwise it is little-endian.
   */
  public void setBigEndian(boolean big)
  {
    littleEndian = !big;
  }

  /**
   * Sets the default to little-endian or big-endian.
   * @param little if true default is little-endian otherwise it is big-endian.
   */
  public void setLittleEndian(boolean little)
  {
    littleEndian = little;
  }

  // FilterInputStream

  /**
   * Reads the next byte of data from this input stream. The value
   * byte is returned as an <code>int</code> in the range
   * <code>0</code> to <code>255</code>. If no byte is available
   * because the end of the stream has been reached, the value
   * <code>-1</code> is returned. This method blocks until input data
   * is available, the end of the stream is detected, or an exception
   * is thrown.
   *
   * @return     the next byte of data, or <code>-1</code> if the end of the
   *             stream is reached.
   * @exception  IOException  if an I/O error occurs.
   * @see        java.io.FilterInputStream#in
   */
  public int read() throws IOException
  {
    return dis.read();
  }

  /**
   * Reads up to <code>byte.length</code> bytes of data from this
   * input stream into an array of bytes. This method blocks until some
   * input is available.
   *
   * @param      b   the buffer into which the data is read.
   * @return     the total number of bytes read into the buffer, or
   *             <code>-1</code> if there is no more data because the end of
   *             the stream has been reached.
   * @exception  IOException  if an I/O error occurs.
   * @see        java.io.FilterInputStream#read(byte[], int, int)
   */
  public int read(byte b[]) throws IOException
  {
    return dis.read(b);
  }

  /**
   * Reads up to <code>len</code> bytes of data from this input stream
   * into an array of bytes. This method blocks until some input is
   * available.
   *
   * @param      b     the buffer into which the data is read.
   * @param      off   the start offset of the data.
   * @param      len   the maximum number of bytes read.
   * @return     the total number of bytes read into the buffer, or
   *             <code>-1</code> if there is no more data because the end of
   *             the stream has been reached.
   * @exception  IOException  if an I/O error occurs.
   * @see        java.io.FilterInputStream#in
   */
  public int read(byte b[], int off, int len) throws IOException
  {
    return dis.read(b, off, len);
  }

  /**
   * Skips over and discards <code>n</code> bytes of data from the
   * input stream. The <code>skip</code> method may, for a variety of
   * reasons, end up skipping over some smaller number of bytes,
   * possibly <code>0</code>. The actual number of bytes skipped is
   * returned.
   *
   * @param      n   the number of bytes to be skipped.
   * @return     the actual number of bytes skipped.
   * @exception  IOException  if an I/O error occurs.
   */
  public long skip(long n) throws IOException
  {
    return dis.skip(n);
  }

  /**
   * Returns the number of bytes that can be read from this input
   * stream without blocking.
   *
   * @return     the number of bytes that can be read from the input stream
   *             without blocking.
   * @exception  IOException  if an I/O error occurs.
   * @see        java.io.FilterInputStream#in
   */
  public int available() throws IOException
  {
    return dis.available();
  }

  /**
   * Closes this input stream and releases any system resources
   * associated with the stream.
   *
   * @exception  IOException  if an I/O error occurs.
   * @see        java.io.FilterInputStream#in
   */
  public void close() throws IOException
  {
    dis.close();
  }

  /**
   * Marks the current position in this input stream. A subsequent
   * call to the <code>reset</code> method repositions this stream at
   * the last marked position so that subsequent reads re-read the same bytes.
   * <p>
   * The <code>readlimit</code> argument tells this input stream to
   * allow that many bytes to be read before the mark position gets
   * invalidated.
   *
   * @param   readlimit   the maximum limit of bytes that can be read before
   *                      the mark position becomes invalid.
   * @see     java.io.FilterInputStream#in
   * @see     java.io.FilterInputStream#reset()
   */
  public synchronized void mark(int readlimit)
  {
    dis.mark(readlimit);
  }

  /**
   * Repositions this stream to the position at the time the
   * <code>mark</code> method was last called on this input stream.
   * <p>
   * Stream marks are intended to be used in
   * situations where you need to read ahead a little to see what's in
   * the stream. Often this is most easily done by invoking some
   * general parser. If the stream is of the type handled by the
   * parse, it just chugs along happily. If the stream is not of
   * that type, the parser should toss an exception when it fails.
   * If this happens within readlimit bytes, it allows the outer
   * code to reset the stream and try another parser.
   *
   * @exception  IOException  if the stream has not been marked or if the
   *               mark has been invalidated.
   * @see        java.io.FilterInputStream#in
   * @see        java.io.FilterInputStream#mark(int)
   */
  public synchronized void reset() throws IOException
  {
    dis.reset();
  }

  /**
   * Tests if this input stream supports the <code>mark</code>
   * and <code>reset</code> methods.
   *
   * @return  <code>true</code> if this stream type supports the
   *          <code>mark</code> and <code>reset</code> method;
   *          <code>false</code> otherwise.
   * @see     java.io.FilterInputStream#in
   * @see     java.io.InputStream#mark(int)
   * @see     java.io.InputStream#reset()
   */
  public boolean markSupported()
  {
    return dis.markSupported();
  }

  // DataInput interface

  /**
   * Reads some bytes from an input
   * stream and stores them into the buffer
   * array <code>b</code>. The number of bytes
   * read is equal
   * to the length of <code>b</code>.
   * <p>
   * This method blocks until one of the
   * following conditions occurs:<p>
   * <ul>
   * <li><code>b.length</code>
   * bytes of input data are available, in which
   * case a normal return is made.
   *
   * <li>End of
   * file is detected, in which case an <code>EOFException</code>
   * is thrown.
   *
   * <li>An I/O error occurs, in
   * which case an <code>IOException</code> other
   * than <code>EOFException</code> is thrown.
   * </ul>
   * <p>
   * If <code>b</code> is <code>null</code>,
   * a <code>NullPointerException</code> is thrown.
   * If <code>b.length</code> is zero, then
   * no bytes are read. Otherwise, the first
   * byte read is stored into element <code>b[0]</code>,
   * the next one into <code>b[1]</code>, and
   * so on.
   * If an exception is thrown from
   * this method, then it may be that some but
   * not all bytes of <code>b</code> have been
   * updated with data from the input stream.
   *
   * @param     b   the buffer into which the data is read.
   * @exception  EOFException  if this stream reaches the end before reading
   *               all the bytes.
   * @exception  IOException   if an I/O error occurs.
   */
  public void readFully(byte b[]) throws IOException
  {
    dis.readFully(b);
  }

  /**
   *
   * Reads <code>len</code>
   * bytes from
   * an input stream.
   * <p>
   * This method
   * blocks until one of the following conditions
   * occurs:<p>
   * <ul>
   * <li><code>len</code> bytes
   * of input data are available, in which case
   * a normal return is made.
   *
   * <li>End of file
   * is detected, in which case an <code>EOFException</code>
   * is thrown.
   *
   * <li>An I/O error occurs, in
   * which case an <code>IOException</code> other
   * than <code>EOFException</code> is thrown.
   * </ul>
   * <p>
   * If <code>b</code> is <code>null</code>,
   * a <code>NullPointerException</code> is thrown.
   * If <code>off</code> is negative, or <code>len</code>
   * is negative, or <code>off+len</code> is
   * greater than the length of the array <code>b</code>,
   * then an <code>IndexOutOfBoundsException</code>
   * is thrown.
   * If <code>len</code> is zero,
   * then no bytes are read. Otherwise, the first
   * byte read is stored into element <code>b[off]</code>,
   * the next one into <code>b[off+1]</code>,
   * and so on. The number of bytes read is,
   * at most, equal to <code>len</code>.
   *
   * @param     b   the buffer into which the data is read.
   * @param off  an int specifying the offset into the data.
   * @param len  an int specifying the number of bytes to read.
   * @exception  EOFException  if this stream reaches the end before reading
   *               all the bytes.
   * @exception  IOException   if an I/O error occurs.
   */
  public void readFully(byte b[], int off, int len) throws IOException
  {
    dis.readFully(b, off, len);
  }

  /**
   * Makes an attempt to skip over
   * <code>n</code> bytes
   * of data from the input
   * stream, discarding the skipped bytes. However,
   * it may skip
   * over some smaller number of
   * bytes, possibly zero. This may result from
   * any of a
   * number of conditions; reaching
   * end of file before <code>n</code> bytes
   * have been skipped is
   * only one possibility.
   * This method never throws an <code>EOFException</code>.
   * The actual
   * number of bytes skipped is returned.
   *
   * @param      n   the number of bytes to be skipped.
   * @return     the number of bytes skipped, which is always <code>n</code>.
   * @exception  EOFException  if this stream reaches the end before skipping
   *               all the bytes.
   * @exception  IOException   if an I/O error occurs.
   */
  public int skipBytes(int n) throws IOException
  {
    return dis.skipBytes(n);
  }

  /**
   * Reads one input byte and returns
   * <code>true</code> if that byte is nonzero,
   * <code>false</code> if that byte is zero.
   * This method is suitable for reading
   * the byte written by the <code>writeBoolean</code>
   * method of interface <code>DataOutput</code>.
   *
   * @return     the <code>boolean</code> value read.
   * @exception  EOFException  if this stream reaches the end before reading
   *               all the bytes.
   * @exception  IOException   if an I/O error occurs.
   */
  public boolean readBoolean() throws IOException
  {
    return dis.readBoolean();
  }

  /**
   * Reads and returns one input byte.
   * The byte is treated as a signed value in
   * the range <code>-128</code> through <code>127</code>,
   * inclusive.
   * This method is suitable for
   * reading the byte written by the <code>writeByte</code>
   * method of interface <code>DataOutput</code>.
   *
   * @return     the 8-bit value read.
   * @exception  EOFException  if this stream reaches the end before reading
   *               all the bytes.
   * @exception  IOException   if an I/O error occurs.
   */
  public byte readByte() throws IOException
  {
    return dis.readByte();
  }

  /**
   * Reads one input byte, zero-extends
   * it to type <code>int</code>, and returns
   * the result, which is therefore in the range
   * <code>0</code>
   * through <code>255</code>.
   * This method is suitable for reading
   * the byte written by the <code>writeByte</code>
   * method of interface <code>DataOutput</code>
   * if the argument to <code>writeByte</code>
   * was intended to be a value in the range
   * <code>0</code> through <code>255</code>.
   *
   * @return     the unsigned 8-bit value read.
   * @exception  EOFException  if this stream reaches the end before reading
   *               all the bytes.
   * @exception  IOException   if an I/O error occurs.
   */
  public int readUnsignedByte() throws IOException
  {
    return dis.readUnsignedByte();
  }

  /**
   * Reads a 16 bit short.
   *
   * @return the 16 bit short read.
   * @exception EOFException If end of file is reached.
   * @exception IOException If other I/O error has occurred.
   */
  public short readShort()  throws IOException
  {
    if (littleEndian)
    {
      dis.readFully(buf, 0, 2);
      return EndianUtilFns.getLeShort(buf);
    }
    else
    {
      return dis.readShort();
    }
  }

  /**
   * Reads an unsigned 16 bit short.
   *
   * @return the 16 bit short read.
   * @exception EOFException If end of file is reached.
   * @exception IOException If other I/O error has occurred.
   */
  public int readUnsignedShort() throws IOException
  {
    if (littleEndian)
    {
      dis.readFully(buf, 0, 2);
      return EndianUtilFns.getLeUnsignedShort(buf);
    }
    else
    {
      return dis.readUnsignedShort();
    }
  }

  /**
   * Reads a 16 bit char.
   *
   * @return the 16 bit char read.
   * @exception EOFException If end of file is reached.
   * @exception IOException If other I/O error has occurred.
   */
  public char readChar()  throws IOException
  {
    if (littleEndian)
    {
      dis.readFully(buf, 0, 2);
      return EndianUtilFns.getLeChar(buf);
    }
    else
    {
      return dis.readChar();
    }
  }

  /**
   * Reads a 32 bit int.
   *
   * @return the 32 bit integer read.
   * @exception EOFException If end of file is reached.
   * @exception IOException If other I/O error has occurred.
   */
  public int readInt()  throws IOException
  {
    if (littleEndian)
    {
      dis.readFully(buf, 0, 4);
      return EndianUtilFns.getLeInt(buf);
    }
    else
    {
      return dis.readInt();
    }
  }

  /**
   * Reads a 64 bit long.
   *
   * @return the read 64 bit long.
   * @exception EOFException If end of file is reached.
   * @exception IOException If other I/O error has occurred.
   */
  public long readLong()  throws IOException
  {
    if (littleEndian)
    {
      dis.readFully(buf, 0, 8);
      return EndianUtilFns.getLeLong(buf);
    }
    else
    {
      return dis.readLong();
    }
  }

  /**
   * Reads a 32 bit float.
   *
   * @return the 32 bit float read.
   * @exception EOFException If end of file is reached.
   * @exception IOException If other I/O error has occurred.
   */
  public float readFloat() throws IOException
  {
    return Float.intBitsToFloat(readInt());
  }

  /**
   * Reads a 64 bit double.
   *
   * @return the 64 bit double read.
   * @exception EOFException If end of file is reached.
   * @exception IOException If other I/O error has occurred.
   */
  public double readDouble() throws IOException
  {
    return Double.longBitsToDouble(readLong());
  }

  /**
   * Read a line of text.  A line is considered to be terminated by any one
   * of a line feed ('\n'), a carriage return ('\r'), or a carriage return
   * followed immediately by a linefeed.
   *
   * @return     A String containing the contents of the line, not including
   *             any line-termination characters, or null if the end of the
   *             stream has been reached
   *
   * @exception  IOException  If an I/O error occurs
   */
  public String readLine() throws IOException
  {
    if (br == null)
    {
      br = new BufferedReader(new InputStreamReader(in));
    }
    return br.readLine();
  }

  /**
   * Reads in a string that has been encoded using a modified UTF-8 format.
   * The general contract of <code>readUTF</code>
   * is that it reads a representation of a Unicode
   * character string encoded in Java modified
   * UTF-8 format; this string of characters
   * is then returned as a <code>String</code>.
   * <p>
   * First, two bytes are read and used to
   * construct an unsigned 16-bit integer in
   * exactly the manner of the <code>readUnsignedShort</code>
   * method . This integer value is called the
   * <i>UTF length</i> and specifies the number
   * of additional bytes to be read. These bytes
   * are then converted to characters by considering
   * them in groups. The length of each group
   * is computed from the value of the first
   * byte of the group. The byte following a
   * group, if any, is the first byte of the
   * next group.
   * <p>
   * If the first byte of a group
   * matches the bit pattern <code>0xxxxxxx</code>
   * (where <code>x</code> means "may be <code>0</code>
   * or <code>1</code>"), then the group consists
   * of just that byte. The byte is zero-extended
   * to form a character.
   * <p>
   * If the first byte
   * of a group matches the bit pattern <code>110xxxxx</code>,
   * then the group consists of that byte <code>a</code>
   * and a second byte <code>b</code>. If there
   * is no byte <code>b</code> (because byte
   * <code>a</code> was the last of the bytes
   * to be read), or if byte <code>b</code> does
   * not match the bit pattern <code>10xxxxxx</code>,
   * then a <code>UTFDataFormatException</code>
   * is thrown. Otherwise, the group is converted
   * to the character:<p>
   * <pre><code>(char)(((a&amp; 0x1F) &lt;&lt; 6) | (b &amp; 0x3F))
   * </code></pre>
   * If the first byte of a group
   * matches the bit pattern <code>1110xxxx</code>,
   * then the group consists of that byte <code>a</code>
   * and two more bytes <code>b</code> and <code>c</code>.
   * If there is no byte <code>c</code> (because
   * byte <code>a</code> was one of the last
   * two of the bytes to be read), or either
   * byte <code>b</code> or byte <code>c</code>
   * does not match the bit pattern <code>10xxxxxx</code>,
   * then a <code>UTFDataFormatException</code>
   * is thrown. Otherwise, the group is converted
   * to the character:<p>
   * <pre><code>
   * (char)(((a &amp; 0x0F) &lt;&lt; 12) | ((b &amp; 0x3F) &lt;&lt; 6) | (c &amp; 0x3F))
   * </code></pre>
   * If the first byte of a group matches the
   * pattern <code>1111xxxx</code> or the pattern
   * <code>10xxxxxx</code>, then a <code>UTFDataFormatException</code>
   * is thrown.
   * <p>
   * If end of file is encountered
   * at any time during this entire process,
   * then an <code>EOFException</code> is thrown.
   * <p>
   * After every group has been converted to
   * a character by this process, the characters
   * are gathered, in the same order in which
   * their corresponding groups were read from
   * the input stream, to form a <code>String</code>,
   * which is returned.
   * <p>
   * The <code>writeUTF</code>
   * method of interface <code>DataOutput</code>
   * may be used to write data that is suitable
   * for reading by this method.
   * @return     a Unicode string.
   * @exception  EOFException            if this stream reaches the end
   *               before reading all the bytes.
   * @exception  IOException             if an I/O error occurs.
   * @exception  UTFDataFormatException  if the bytes do not represent a
   *               valid UTF-8 encoding of a string.
   */
  public String readUTF() throws IOException
  {
    return dis.readUTF();
  }

  /**
   * Reads from the
   * stream <code>in</code> a representation
   * of a Unicode  character string encoded in
   * Java modified UTF-8 format; this string
   * of characters  is then returned as a <code>String</code>.
   * The details of the modified UTF-8 representation
   * are  exactly the same as for the <code>readUTF</code>
   * method of <code>DataInput</code>.
   *
   * @param      in   a data input stream.
   * @return     a Unicode string.
   * @exception  EOFException            if the input stream reaches the end
   *               before all the bytes.
   * @exception  IOException             if an I/O error occurs.
   * @exception  UTFDataFormatException  if the bytes do not represent a
   *               valid UTF-8 encoding of a Unicode string.
   * @see        java.io.DataInputStream#readUnsignedShort()
   */
  public final static String readUTF(DataInput in) throws IOException
  {
    return DataInputStream.readUTF(in);
  }

  //Array methods

  /**
   * Reads an array of bytes.
   * @param length array length
   * @return the array.
   * @exception EOFException If end of file is reached.
   * @exception IOException If other I/O error has occurred.
   */
  public byte[] readByteArray(int length) throws IOException
  {
    byte[] array = new byte[length];
    for (int i = 0; i < length; i++)
    {
      array[i] = readByte();
    }
    return array;
  }

  /**
   * Reads an array of unsigned bytes.
   * @param length array length
   * @return the array.
   * @exception EOFException If end of file is reached.
   * @exception IOException If other I/O error has occurred.
   */
  public int[] readUnsignedByteArray(int length) throws IOException
  {
    int[] array = new int[length];
    for (int i = 0; i < length; i++)
    {
      array[i] = readUnsignedByte();
    }
    return array;
  }

  /**
   * Reads an array of shorts.
   * @param length array length
   * @return the array.
   * @exception EOFException If end of file is reached.
   * @exception IOException If other I/O error has occurred.
   */
  public short[] readShortArray(int length)  throws IOException
  {
    short[] array = new short[length];
    for (int i = 0; i < length; i++)
    {
      array[i] = readShort();
    }
    return array;
  }

  /**
   * Reads an array of unsigned shorts.
   * @param length array length
   * @return the array.
   * @exception EOFException If end of file is reached.
   * @exception IOException If other I/O error has occurred.
   */
  public int[] readUnsignedShortArray(int length) throws IOException
  {
    int[] array = new int[length];
    for (int i = 0; i < length; i++)
    {
      array[i] = readUnsignedShort();
    }
    return array;
  }

  /**
   * Reads an array of chars.
   * @param length array length
   * @return the array.
   * @exception EOFException If end of file is reached.
   * @exception IOException If other I/O error has occurred.
   */
  public char[] readCharArray(int length)  throws IOException
  {
    char[] array = new char[length];
    for (int i = 0; i < length; i++)
    {
      array[i] = readChar();
    }
    return array;
  }

  /**
   * Reads an array of ints.
   * @param length array length
   * @return the array.
   * @exception EOFException If end of file is reached.
   * @exception IOException If other I/O error has occurred.
   */
  public int[] readIntArray(int length)  throws IOException
  {
    int[] array = new int[length];
    for (int i = 0; i < length; i++)
    {
      array[i] = readInt();
    }
    return array;
  }

  /**
   * Reads an array of longs.
   * @param length array length
   * @return the array.
   * @exception EOFException If end of file is reached.
   * @exception IOException If other I/O error has occurred.
   */
  public long[] readLongArray(int length) throws IOException
  {
    long[] array = new long[length];
    for (int i = 0; i < length; i++)
    {
      array[i] = readLong();
    }
    return array;
  }

  /**
   * Reads an array of floats.
   * @param length array length
   * @return the array.
   * @exception EOFException If end of file is reached.
   * @exception IOException If other I/O error has occurred.
   */
  public float[] readFloatArray(int length) throws IOException
  {
    float[] array = new float[length];
    for (int i = 0; i < length; i++)
    {
      array[i] = readFloat();
    }
    return array;
  }

  /**
   * Reads an array of doubles.
   * @param length array length
   * @return the array.
   * @exception EOFException If end of file is reached.
   * @exception IOException If other I/O error has occurred.
   */
  public double[] readDoubleArray(int length) throws IOException
  {
    double[] array = new double[length];
    for (int i = 0; i < length; i++)
    {
      array[i] = readDouble();
    }
    return array;
  }
}