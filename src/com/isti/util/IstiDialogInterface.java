//IstiDialogInterface.java:  Defines the methods used to operate an
//                           'IstiDialogPopup' object.
//
//  10/3/2003 -- [ET]  Initial version.
//   4/5/2004 -- [ET]  Added 'setVisibleViaInvokeLater()' method.
//  6/11/2004 -- [KF]  Extend 'WindowConstants'.
//

package com.isti.util;

import javax.swing.WindowConstants;

/**
 * Interface IstiDialogInterface defines the methods used to operate
 * a 'IstiDialogPopup' object.  Other objects may use this interface to
 * optionally operate a dialog popup without having to reference the
 * 'IstiDialogPopup' class.
 */
public interface IstiDialogInterface extends WindowConstants
{

  /**
   * Displays the popup dialog window.  If the dialog is modal then this
   * method blocks until the dialog is dismissed by the user or the
   * 'close()' method is called.
   * @return The Object chosen by the user (UNINITIALIZED_VALUE if the
   * user has not yet made a choice, or null if the user closed the window
   * without making a choice) or the value entered via the 'close()' method.
   */
  public Object show();

  /**
   * Displays the popup dialog window.  The method blocks until the
   * dialog is dismissed by the user or the 'close()' method is called
   * (even if the window is non-modal).
   * @return The Object chosen by the user (UNINITIALIZED_VALUE if the
   * user has not yet made a choice, or null if the user closed the window
   * without making a choice) or the value entered via the 'close()' method.
   */
  public Object showAndWait();

  /**
   * Sets the visibility of the dialog window.  If the dialog is
   * non-modal then the call is redirected to the dialog object's
   * 'show()' or  'hide()' method (depending on the state of the
   * given flag parameter).
   * @param flgVal true for visible, false for hidden.
   */
  public void setVisible(boolean flgVal);

  /**
   * Sets the visibility of the dialog window, using the event-dispatch
   * thread via a call to 'SwingUtilities.invokeLater()'.  This allows
   * a modal dialog to be shown without blocking the invoking thread.
   * @param flgVal true for visible, false for hidden.
   */
  public void setVisibleViaInvokeLater(final boolean flgVal);

  /**
   * Determines whether the popup dialog should be visible when its
   * parent is visible.
   * @return <code>true</code> if the popup dialog is visible;
   * <code>false</code> otherwise.
   */
  public boolean isVisible();

  /**
   * Returns true if the popup dialog window is showing.
   * @return true if the popup dialog window is showing, false if not.
   */
  public boolean isShowing();

  /**
   * Waits for the popup dialog window to be visible, up to the given
   * maximum time value.
   * @param maxWaitTimeMs the maximum time to wait, in milliseconds.
   * @return true if the dialog is visible, false if the maximum time was
   * reached without the dialog being visible.
   */
  public boolean waitForDialogVisible(int maxWaitTimeMs);

  /**
   * Closes the popup dialog window.
   * @param obj the value to be returned via the 'show()' or 'showAndWait()'
   * method.
   */
  public void close(Object obj);

  /**
   * Closes the popup dialog window.  (Same as 'hide()').
   */
  public void close();

  /**
   * Waits (if necessary) for the popup dialog window to be visible and
   * then closes it.
   * @param obj the value to be returned via the 'show()' or 'showAndWait()'
   * method.
   * @param maxWaitTimeMs the maximum time to wait for the popup dialog
   * window to be visible, in milliseconds.
   */
  public void closeWithWait(Object obj,int maxWaitTimeMs);

  /**
   * Waits (if necessary) for the popup dialog window to be visible and
   * then closes it.
   * @param maxWaitTimeMs the maximum time to wait for the popup dialog
   * window to be visible, in milliseconds.
   */
  public void closeWithWait(int maxWaitTimeMs);

  /**
   * Hides the popup dialog window.  (Same as 'close()').
   */
  public void hide();

  /**
   * Disposes the popup dialog window.
   */
  public void dispose();

  /**
   * Repaints the popup dialog window.
   */
  public void repaint();

  /**
   * Validates the popup dialog window.
   */
  public void validate();

  /**
   * Invalidates the popup dialog window.
   */
  public void invalidate();

  /**
   * Causes this dialog to be sized to fit the preferred size and layouts
   * of its subcomponents. If the dialog and/or its owner are not yet
   * displayable, both are made displayable before calculating the
   * preferred size. The Window will be validated after the preferredSize
   * is calculated.
   */
  public void pack();

  /**
   * Sets the size of the dialog.
   * @param width the width to use.
   * @param height the height to use.
   */
  public void setSize(int width,int height);

  /**
   * Sets whether the dialog is resizable by the user.
   * @param resizableFlag true if the user can resize this dialog; false
   * otherwise.
   */
  public void setResizable(boolean resizableFlag);

  /**
   * Moves the dialog to a new location. The top-left corner of
   * the new location is specified by the <code>x</code> and <code>y</code>
   * parameters in the coordinate space of this component's parent.
   * @param x The <i>x</i>-coordinate of the new location's
   * top-left corner in the parent's coordinate space.
   * @param y The <i>y</i>-coordinate of the new location's
   * top-left corner in the parent's coordinate space.
   */
  public void setLocation(int x, int y);

  /**
   * Requests that the dialog get the input focus, and that the
   * dialog's top-level ancestor become the focused Window.
   */
  public void requestFocus();

  /**
   * Specifies whether this dialog should be modal.
   * @param modalFlag true for modal, false for modeless (allows other
   * windows to run).
   */
  public void setModal(boolean modalFlag);

  /**
   * Returns the modal status of the dialog.
   * @return true if modal, false if modeless (allows other windows
   * to run).
   */
  public boolean isModal();

  /**
   * Sets the operation which will happen by default when the user
   * initiates a "close" on this dialog.  The possible choices are:
   * DO_NOTHING_ON_CLOSE - do not do anything - require the program to
   * handle the operation in the windowClosing method of a registered
   * WindowListener object; HIDE_ON_CLOSE - automatically hide the
   * dialog after invoking any registered WindowListener objects;
   * DISPOSE_ON_CLOSE - automatically hide and dispose the dialog
   * after invoking any registered WindowListener objects.   The
   * value is set to HIDE_ON_CLOSE by default.
   * @param selVal one of the following constants:  DISPOSE_ON_CLOSE,
   * DO_NOTHING_ON_CLOSE, or HIDE_ON_CLOSE.
   */
  public void setDefaultCloseOperation(int selVal);

  /**
   * Sets the message object displayed by this dialog.
   * @param obj the message obj.
   */
  public void setMessageObj(Object obj);

  /**
   * Returns the message object displayed by this dialog.
   * @return The message obj.
   */
  public Object getMessageObj();

  /**
   * Sets the message object displayed by this dialog to the given string.
   * @param str the message string.
   */
  public void setMessageStr(String str);

  /**
   * Returns the message string displayed by this dialog.
   * @return The message String object, or null if a String object
   * could not be retrieved.
   */
  public String getMessageStr();

  /**
   * Sets the title displayed by this dialog.
   * @param str the title string to display.
   */
  public void setTitleStr(String str);

  /**
   * Returns the title displayed by this dialog.
   * @return The title string.
   */
  public String getTitleStr();

  /**
   * Returns the value the user has input.
   * @return The Object the user specified.
   */
  public Object getInputValue();

  /**
   * Returns the value the user has selected. UNINITIALIZED_VALUE implies
   * the user has not yet made a choice, null means the user closed the
   * window with out choosing anything. Otherwise the returned value will
   * be one of the options defined in this object.
   * @return The Object chosen by the user, UNINITIALIZED_VALUE if the
   * user has not yet made a choice, or null if the user closed the
   * window without making a choice.
   */
  public Object getValue();

  /**
   * Sets a user-defined message string that may be fetched via the
   * 'getMessageString()' method.
   * @param str the message text.
   */
  public void setUserMessageString(String str);

  /**
   * Returns the user-defined message string set via the
   * 'setMessageString()' method.
   * @return The message text, or null if none has been set.
   */
  public String getUserMessageString();

  /**
   * Requests that the current "initial" component have focus.
   */
  public void setInitialFocus();
}
