//IstiEncryptionUtils.java:  A collection of static utility fields and
//                       methods for encryption.
//
// For more information on encryption and security in general look at:
// http://java.sun.com/developer/onlineTraining/Security/Fundamentals/index.html
//
// CA files may be found at:
// http://www.isti.com/libs/zips/CA.zip
//
//   9/1/2004 -- [KF]  Initial version.
//  9/16/2004 -- [KF]  Added methods with data array parameter.
//   4/4/2007 -- [KF]  Changed to work with OpenSSL generated private keys.
// 10/11/2017 -- [KF]  Modified for Java 1.9 compatibility.
//

package com.isti.util;

import java.io.*;
import java.security.*;
import java.security.cert.*;
import java.security.spec.*;
import java.util.*;
import java.math.BigInteger;
import java.nio.charset.Charset;

/**
 * Class IstiEncryptionUtils contains a collection of static utility fields and
 * methods for encryption.
 *
 * @see com.isti.util.test.IstiEncryptionUtilsTest
 */
public class IstiEncryptionUtils
{
  /**
   * The default character encoding.
   * @see String#getBytes
   */
  public final static Charset DEFAULT_CHARACTER_ENCODING = UtilFns.UTF_8;

  /**
   * The default key size.
   */
  public final static int DEFAULT_KEYSIZE = 2048;

  /**
   * The DSA algorithm.
   * @see generateKeyPairGenerator, generateSignatureText, isValidSignatureText
   */
  public final static String DSA_ALGORITHM = "DSA";

  /**
   * The RSA algorithm.
   * @see generateKeyPairGenerator
   *
   * If this is used with the 'generateSignatureText' or 'isValidSignatureText'
   * methods it is automatically replaced with 'SHA1_WITH_RSA'.
   * @see generateSignatureText, isValidSignatureText
   */
  public final static String RSA_ALGORITHM = "RSA";

  /**
   * The SHA algorithm.
   * @see generateMessageDigest, encrypt
   */
  public final static String SHA_ALGORITHM = "SHA";

  /**
   * The SHA1 with RSA algorithm.
   * @see generateSignatureText, isValidSignatureText
   */
  public final static String SHA1_WITH_RSA = "SHA1withRSA";

  /** Private key flag. */
  private final static boolean PRIVATE_KEY_FLAG = false;

  /** Public key flag. */
  private final static boolean PUBLIC_KEY_FLAG = true;

  //private constructor so that no object instances may be created
  // (static access only)
  private IstiEncryptionUtils()
  {
  }

  /**
   * Decodes the specified buffer.
   * @param buffer the buffer.
   * @return the decoded bytes.
   * @throws IOException if error.
   */
  public static byte[] decodeBuffer(byte[] source) throws IOException
  {
	  return Base64.decode(source);
  }

  /**
   * Decodes the specified input stream.
   * @param inStream the input stream.
   * @return the decoded bytes.
   * @throws IOException if error.
   */
  public static byte[] decodeBuffer(InputStream inStream) throws IOException
  {
	byte[] source = FileUtils.readStreamToBuffer(inStream);
	return decodeBuffer(source);
  }

  /**
   * Decodes the specified buffer.
   * @param buffer the buffer.
   * @return the decoded bytes.
   * @throws IOException if error.
   */
  public static byte[] decodeBuffer(String buffer) throws IOException
  {
    return decodeBuffer(buffer.getBytes(DEFAULT_CHARACTER_ENCODING));
  }

  /**
   * Decodes the bytes for the key.
   * @param inStream the input stream.
   * @param publicKeyFlag true for public key, false for private key.
   * @return the decoded bytes.
   */
  private static byte[] decodeKeyBytes(InputStream inStream, boolean publicKeyFlag)
  {
    byte[] bytes = null;
    try
    {
      //read the stream into the buffer
      bytes = FileUtils.readStreamToBuffer(inStream);
      final BufferedReader rdrObj =
          FileUtils.getBufferedReader(new ByteArrayInputStream(bytes));
      String str = rdrObj.readLine();
      if (str != null && str.startsWith(getBeginCommentText(publicKeyFlag)))
      {
        final StringWriter strWtrObj = new StringWriter();
        //wrap print-writer around string-writer:
        final PrintWriter prtWtrObj = new PrintWriter(strWtrObj);
        final String endCommentText = getEndCommentText(publicKeyFlag);
        while ( (str = rdrObj.readLine()) != null) //for each line of data in file
        {
          if (str.equals(endCommentText))
          {
            final String buffer = strWtrObj.toString(); //return string version of data
            bytes = decodeBuffer(buffer);
            break;
          }
          prtWtrObj.println(str); //send line to string-writer
        }
        prtWtrObj.flush(); //flush output to string-writer
      }
    }
    catch (Exception ex)
    {

      //some kind of exception error; log it
      LogFile.getGlobalLogObj(true).warning(
          generateKeyErrorMessage(publicKeyFlag) + ex);
      LogFile.getGlobalLogObj(true).warning(UtilFns.getStackTraceString(ex));
    }
    return bytes;
  }

  /**
   * Encodes the specified bytes.
   * @param byteArray the array of bytes.
   * @return the encoded buffer.
   */
  public static String encode(byte[] byteArray)
  {
	try
	{
	  byte[] encoded = Base64.encodeBytesToBytes(byteArray, 0, byteArray.length,
		  Base64.DO_BREAK_LINES);
	  return new String(encoded, DEFAULT_CHARACTER_ENCODING);
	}
	catch (Exception ex)
	{
      LogFile.getGlobalLogObj(true).warning("Error with encode:  " + ex);
	}
	return UtilFns.EMPTY_STRING;
  }

  /**
   * Encrypts the specified text.
   * @param original the text to encrypt.
   * @param md the message digest.
   * @return the text encrypted or an empty string if an error occurred.
   */
  public static String encrypt(String original, MessageDigest md)
  {
    if (md != null)  //if message digest was provided
    {
      try
      {
        md.reset();
        md.update(original.getBytes(DEFAULT_CHARACTER_ENCODING));
        final byte raw[] = md.digest();
        final String hash = encode(raw);
        return hash;
      }
      catch(Exception ex)
      {
        //some kind of exception error; log it
        LogFile.getGlobalLogObj(true).warning("Error with encrypt:  " + ex);
        LogFile.getGlobalLogObj(true).warning(UtilFns.getStackTraceString(ex));
      }
    }
    return UtilFns.EMPTY_STRING;
  }

  /**
   * Encrypts the specified text.
   * @param original the text to encrypt.
   * @param algorithm the name of the algorithm requested.
   * @return the text encrypted or an empty string if an error occurred.
   */
  public static String encrypt(String original, String algorithm)
  {
    return encrypt(original, generateMessageDigest(algorithm));
  }

  /**
   * Gets the key error message.
   * @param publicKeyFlag true for public key, false for private key.
   * @return the key error message.
   */
  private static String generateKeyErrorMessage(boolean publicKeyFlag)
  {
    return "Could not generate " + (publicKeyFlag?"Public":"Private") + "Key: ";
  }

  /**
   * Gets the begin comment text.
   * @param publicKeyFlag true for public key, false for private key.
   * @return the comment text.
   */
  private static String getBeginCommentText(boolean publicKeyFlag)
  {
    return getCommentText(publicKeyFlag, true);
  }

  /**
   * Gets the end comment text.
   * @param publicKeyFlag true for public key, false for private key.
   * @return the comment text.
   */
  private static String getEndCommentText(boolean publicKeyFlag)
  {
    return getCommentText(publicKeyFlag, false);
  }

  /**
   * Gets the comment text.
   * @param beginFlag true for begin, false for end.
   * @param publicKeyFlag true for public key, false for private key.
   * @return the comment text.
   */
  private static String getCommentText(boolean publicKeyFlag, boolean beginFlag)
  {
    //-----BEGIN PRIVATE KEY-----
    StringBuffer commentText = new StringBuffer("-----");
    if (beginFlag)
      commentText.append("BEGIN ");
    else
      commentText.append("END ");
    if (publicKeyFlag)
      commentText.append("PUBLIC");
    else
      commentText.append("PRIVATE");
    commentText.append(" KEY-----");
    return commentText.toString();
  }

  /**
   * Generates a key and initializes it with
   * the data read from the input stream <code>inStream</code>.
   * @param inStream an input stream with the public key.
   * @param publicKeyFlag true for public key, false for private key.
   * @return the key or null if error.
   */
  private static Key generateKey(
      InputStream inStream, boolean publicKeyFlag)
  {
    return generateKey(inStream, publicKeyFlag, publicKeyFlag);
  }

  /**
   * Generates a key and initializes it with
   * the data read from the input stream <code>inStream</code>.
   * @param inStream an input stream with the public key.
   * @param publicKeyFlag true for public key, false for private key.
   * @param isX509EncodedFlag true if X509 encoded, false otherwise.
   * @return the key or null if error.
   */
  private static Key generateKey(
      InputStream inStream, boolean publicKeyFlag, boolean isX509EncodedFlag)
  {
    try
    {
      String algorithm = RSA_ALGORITHM;
      final KeySpec keySpec;

      if (isX509EncodedFlag)  //if X509 encoded
      {
        keySpec = new X509EncodedKeySpec(decodeBuffer(inStream));
      }
      else
      {
        keySpec = new PKCS8EncodedKeySpec(decodeKeyBytes(inStream, publicKeyFlag));
      }
      return generateKey(algorithm, publicKeyFlag, keySpec);
    }
    catch (Exception ex)
    {

      //some kind of exception error; log it
      LogFile.getGlobalLogObj(true).warning(
          generateKeyErrorMessage(publicKeyFlag) + ex);
      LogFile.getGlobalLogObj(true).warning(UtilFns.getStackTraceString(ex));
    }
    return null;
  }

  /**
   * Generates a key.
   * @param algorithm the name of the requested key algorithm.
   * @param publicKeyFlag true for public key, false for private key.
   * @param keySpec the 'KeySpec'.
   * @return the key or null if error.
   */
  private static Key generateKey(
      String algorithm, boolean publicKeyFlag, KeySpec keySpec)
  {
    try
    {
      final KeyFactory kf = KeyFactory.getInstance(algorithm);
      if (publicKeyFlag)
      {
        return kf.generatePublic(keySpec);
      }
      else
      {
        return kf.generatePrivate(keySpec);
      }
    }
    catch (Exception ex)
    {

      //some kind of exception error; log it
      LogFile.getGlobalLogObj(true).warning(
          generateKeyErrorMessage(publicKeyFlag) + ex);
      LogFile.getGlobalLogObj(true).warning(UtilFns.getStackTraceString(ex));
    }
    return null;
  }

  /**
   * Generates a key pair.
   * @param kpg the key pair generator.
   * <p>If this KeyPairGenerator has not been initialized explicitly,
   * provider-specific defaults will be used for the size and other
   * (algorithm-specific) values of the generated keys.
   *
   * <p>This will generate a new key pair every time it is called.
   *
   * <p>This method is functionally equivalent to
   * {@link #genKeyPair() genKeyPair}.
   *
   * @return the generated key pair
   */
  public static KeyPair generateKeyPair(KeyPairGenerator kpg)
  {
    final KeyPair kp = kpg.generateKeyPair();
    return kp;
  }

  /**
   * Generates a KeyPairGenerator object that implements the specified digest
   * algorithm. If the default provider package
   * provides an implementation of the requested digest algorithm,
   * an instance of KeyPairGenerator containing that implementation is
   * returned.
   * If the algorithm is not available in the default
   * package, other packages are searched.
   *
   * @param algorithm the standard string name of the algorithm.
   * See Appendix A in the <a href=
   * "../../../guide/security/CryptoSpec.html#AppA">
   * Java Cryptography Architecture API Specification &amp; Reference </a>
   * for information about standard algorithm names.
   *
   * @return the new KeyPairGenerator object implementing the specified
   * algorithm or null if the algorithm is not available in the environment.
   */
  public static KeyPairGenerator generateKeyPairGenerator(String algorithm)
  {
    try
    {
      final KeyPairGenerator kpg = KeyPairGenerator.getInstance(algorithm);
      kpg.initialize(DEFAULT_KEYSIZE);
      return kpg;
    }
    catch(Exception ex)
    {
      //some kind of exception error; log it
      LogFile.getGlobalLogObj(true).warning(
          "Could not generate key pair (" + algorithm + ") :" + ex);
      LogFile.getGlobalLogObj(true).warning(UtilFns.getStackTraceString(ex));
    }
    return null;
  }

  /**
   * Generates a MessageDigest object that implements the specified digest
   * algorithm. If the default provider package
   * provides an implementation of the requested digest algorithm,
   * an instance of MessageDigest containing that implementation is returned.
   * If the algorithm is not available in the default
   * package, other packages are searched.
   *
   * @param algorithm the name of the algorithm requested.
   * See Appendix A in the <a href=
   * "../../../guide/security/CryptoSpec.html#AppA">
   * Java Cryptography Architecture API Specification &amp; Reference </a>
   * for information about standard algorithm names.
   *
   * @return a Message Digest object implementing the specified algorithm or
   * null if the algorithm is not available in the caller's environment.
   */
  public static MessageDigest generateMessageDigest(String algorithm)
  {
    try
    {
      final MessageDigest md = MessageDigest.getInstance(algorithm);
      return md;
    }
    catch(NoSuchAlgorithmException ex)
    {
      //some kind of exception error; log it
      LogFile.getGlobalLogObj(true).warning(
          "Could not generate MessageDigest (" + algorithm + ") :" + ex);
      LogFile.getGlobalLogObj(true).warning(UtilFns.getStackTraceString(ex));
    }
    return null;
  }

  /**
   * Generates a private key and initializes it with the specified data.
   * @param data the private key data.
   * @return the private key or null if error.
   * @see #generatePrivateKey(InputStream)
   */
  public static PrivateKey generatePrivateKey(byte[] data)
  {
    try
    {
      final InputStream inStream = new ByteArrayInputStream(data);
      return generatePrivateKey(inStream);
    }
    catch(Exception ex)
    {
      //some kind of exception error; log it
      LogFile.getGlobalLogObj(true).warning(
          "Could not generate PrivateKey:  " + ex);
      LogFile.getGlobalLogObj(true).warning(UtilFns.getStackTraceString(ex));
    }
    return null;
  }

  /**
   * Generates a private key and initializes it with
   * the data read from the file.
   * @param privFile a file with the private key.
   * @return the private key or null if error.
   * @see #generatePrivateKey(InputStream)
   */
  public static PrivateKey generatePrivateKey(File privFile)
  {
    try
    {
      final InputStream inStream = new FileInputStream(privFile);
      return generatePrivateKey(inStream);
    }
    catch(Exception ex)
    {
      //some kind of exception error; log it
      LogFile.getGlobalLogObj(true).warning(
          "Could not generate PrivateKey:  " + ex);
      LogFile.getGlobalLogObj(true).warning(UtilFns.getStackTraceString(ex));
    }
    return null;
  }

  /**
   * Generates a private key and initializes it with
   * the data read from the input stream <code>inStream</code>.
   *
   * <p>The private key provided in the data must be in PKCS8 format either
   * binary DER-encoded or in printable (Base64) encoding.
   * If the private key is provided in Base64 encoding, it must be bounded at
   * the beginning by -----BEGIN PRIVATE KEY-----, and must be bounded at
   * the end by -----END PRIVATE KEY-----.
   *
   * @param inStream an input stream with the private key.
   * @return the private key or null if error.
   */
  public static PrivateKey generatePrivateKey(InputStream inStream)
  {
    return (PrivateKey)generateKey(inStream, PRIVATE_KEY_FLAG);
  }

  /**
   * Generates a private key and initializes it with
   * the data read from the file.
   * @param privFile a file with the private key.
   * @return the private key or null if error.
   */
  public static PrivateKey generatePrivateKey(String privFile)
  {
    final InputStream inStream =
        FileUtils.fileMultiOpenInputStream(privFile);
    if (inStream == null)  //if file could not be open
    {
      LogFile.getGlobalLogObj(true).warning(
          "Could not open private key file: " + privFile);
      return null;
    }
    return generatePrivateKey(inStream);
  }

  /**
   * Generates a public key and initializes it with the specified data.
   * @param data the private key data.
   * @return the public key or null if error.
   */
  public static PublicKey generatePublicKey(byte[] data)
  {
    try
    {
      final InputStream inStream = new ByteArrayInputStream(data);
      return generatePublicKey(inStream);
    }
    catch(Exception ex)
    {
      //some kind of exception error; log it
      LogFile.getGlobalLogObj(true).warning(
          "Could not generate PublicKey:  " + ex);
      LogFile.getGlobalLogObj(true).warning(UtilFns.getStackTraceString(ex));
    }
    return null;
  }

  /**
   * Generates a public key and initializes it with
   * the data read from the file.
   * @param pubFile a file with the public key.
   * @return the public key or null if error.
   */
  public static PublicKey generatePublicKey(File pubFile)
  {
    try
    {
      final InputStream inStream = new FileInputStream(pubFile);
      return generatePublicKey(inStream);
    }
    catch(Exception ex)
    {
      //some kind of exception error; log it
      LogFile.getGlobalLogObj(true).warning(
          "Could not generate PublicKey:  " + ex);
      LogFile.getGlobalLogObj(true).warning(UtilFns.getStackTraceString(ex));
    }
    return null;
  }

  /**
   * Generates a public key and initializes it with
   * the data read from the input stream <code>inStream</code>.
   * @param inStream an input stream with the public key.
   * @return the public key or null if error.
   */
  public static PublicKey generatePublicKey(InputStream inStream)
  {
    return (PublicKey)generateKey(inStream, PUBLIC_KEY_FLAG);
  }

  /**
   * Generates a public key and initializes it with
   * the data read from the file.
   * @param pubFile a file with the public key.
   * @return the public key or null if error.
   */
  public static PublicKey generatePublicKey(String pubFile)
  {
    final InputStream inStream =
        FileUtils.fileMultiOpenInputStream(pubFile);
    if (inStream == null)  //if file could not be open
    {
      LogFile.getGlobalLogObj(true).warning(
          "Could not open public key file: " + pubFile);
      return null;
    }
    return generatePublicKey(inStream);
  }

  /**
   * Generates a Signature object that implements the specified digest
   * algorithm. If the default provider package
   * provides an implementation of the requested digest algorithm,
   * an instance of Signature containing that implementation is returned.
   * If the algorithm is not available in the default
   * package, other packages are searched.
   *
   * @param algorithm the standard name of the algorithm requested.
   * See Appendix A in the <a href=
   * "../../../guide/security/CryptoSpec.html#AppA">
   * Java Cryptography Architecture API Specification &amp; Reference </a>
   * for information about standard algorithm names.
   *
   * @return the new Signature object.
   *
   * @exception NoSuchAlgorithmException if the algorithm is
   * not available in the environment.
   */
  public static Signature generateSignature(String algorithm)
  {
    try
    {
      final Signature sig = Signature.getInstance(algorithm);
      return sig;
    }
    catch(Exception ex)
    {
      //some kind of exception error; log it
      LogFile.getGlobalLogObj(true).warning(
          "Could not generate Signature (" + algorithm + ") :" + ex);
      LogFile.getGlobalLogObj(true).warning(UtilFns.getStackTraceString(ex));
    }
    return null;
  }

  /**
   * Generates the signature text for the specified message text and key pair.
   * @param msgText the message text.
   * @param kp the key pair.
   * @return the signature text.
   */
  public static String generateSignatureText(String msgText, KeyPair kp)
  {
    return generateSignatureText(msgText, kp.getPrivate());
  }

  /**
   * Generates the signature text for the specified message text and private key.
   * @param msgText the message text.
   * @param priv the private key.
   * @return the signature text.
   */
  public static String generateSignatureText(String msgText, PrivateKey priv)
  {
    String algorithm = priv.getAlgorithm();

    if (RSA_ALGORITHM.equals(algorithm))  //if RSA is specified
    {
      algorithm = SHA1_WITH_RSA;  //use SHA1withRSA
    }

    try
    {
      //acquire a signature
      final Signature sig = Signature.getInstance(algorithm);
      //initialize the signature with the private key
      sig.initSign(priv);
      //provide signature with message to sign
      sig.update(msgText.getBytes(DEFAULT_CHARACTER_ENCODING));
      //sign it and get digest
      final byte raw[] = sig.sign();
      final String sigText = encode(raw);
      return sigText;
    }
    catch(Exception ex)
    {
      //some kind of exception error; log it
      LogFile.getGlobalLogObj(true).warning(
          "Could not generate Signature (" + algorithm + ") :" + ex);
      LogFile.getGlobalLogObj(true).warning(UtilFns.getStackTraceString(ex));
    }
    return UtilFns.EMPTY_STRING;
  }

  /**
   * Generates a X.509 certificate object and initializes it with
   * the specified data.
   *
   * <p>The given data must contain a single certificate.
   *
   * <p>The X.509 certificate provided in the data must be
   * DER-encoded and may be supplied in binary or printable (Base64) encoding.
   * If the certificate is provided in Base64 encoding, it must be bounded at
   * the beginning by -----BEGIN CERTIFICATE-----, and must be bounded at
   * the end by -----END CERTIFICATE-----.
   *
   * <p>Note that if the given input stream does not support
   * {@link java.io.InputStream#mark(int) mark} and
   * {@link java.io.InputStream#reset() reset}, this method will
   * consume the entire input stream.
   *
   * @param data the certificate data.
   *
   * @return a certificate object initialized with the data.
   *
   * @exception CertificateException on parsing errors.
   */
  public static X509Certificate generateX509Certificate(byte[] data)
  {
    try
    {
      final InputStream inStream = new ByteArrayInputStream(data);
      return generateX509Certificate(inStream);
    }
    catch(Exception ex)
    {
      //some kind of exception error; log it
      LogFile.getGlobalLogObj(true).warning(
          "Could not generate X509Certificate:  " + ex);
      LogFile.getGlobalLogObj(true).warning(UtilFns.getStackTraceString(ex));
    }
    return null;
  }

  /**
   * Generates a X.509 certificate object and initializes it with
   * the data read from the file.
   *
   * <p>The given file must contain a single certificate.
   *
   * <p>The X.509 certificate provided in the file must be
   * DER-encoded and may be supplied in binary or printable (Base64) encoding.
   * If the certificate is provided in Base64 encoding, it must be bounded at
   * the beginning by -----BEGIN CERTIFICATE-----, and must be bounded at
   * the end by -----END CERTIFICATE-----.
   *
   * <p>Note that if the given input stream does not support
   * {@link java.io.InputStream#mark(int) mark} and
   * {@link java.io.InputStream#reset() reset}, this method will
   * consume the entire input stream.
   *
   * @param certificateFile the certificate file.
   *
   * @return a certificate object initialized with the data
   * from the file.
   *
   * @exception CertificateException on parsing errors.
   */
  public static X509Certificate generateX509Certificate(File certificateFile)
  {
    try
    {
      final InputStream inStream = new FileInputStream(certificateFile);
      return generateX509Certificate(inStream);
    }
    catch(Exception ex)
    {
      //some kind of exception error; log it
      LogFile.getGlobalLogObj(true).warning(
          "Could not generate X509Certificate:  " + ex);
      LogFile.getGlobalLogObj(true).warning(UtilFns.getStackTraceString(ex));
    }
    return null;
  }

  /**
   * Generates a X.509 certificate object and initializes it with
   * the data read from the input stream <code>inStream</code>.
   *
   * <p>The given input stream <code>inStream</code> must contain a single
   * certificate.
   *
   * <p>The X.509 certificate provided in <code>inStream</code> must be
   * DER-encoded and may be supplied in binary or printable (Base64) encoding.
   * If the certificate is provided in Base64 encoding, it must be bounded at
   * the beginning by -----BEGIN CERTIFICATE-----, and must be bounded at
   * the end by -----END CERTIFICATE-----.
   *
   * <p>Note that if the given input stream does not support
   * {@link java.io.InputStream#mark(int) mark} and
   * {@link java.io.InputStream#reset() reset}, this method will
   * consume the entire input stream.
   *
   * @param inStream an input stream with the certificate data.
   *
   * @return a certificate object initialized with the data
   * from the input stream.
   *
   * @exception CertificateException on parsing errors.
   */
  public static X509Certificate generateX509Certificate(InputStream inStream)
  {
    try
    {
      CertificateFactory cf = CertificateFactory.getInstance("X.509");
      X509Certificate cert = (X509Certificate)cf.generateCertificate(inStream);
      inStream.close();
      return cert;
    }
    catch(Exception ex)
    {
      //some kind of exception error; log it
      LogFile.getGlobalLogObj(true).warning(
          "Could not generate X509Certificate:  " + ex);
      LogFile.getGlobalLogObj(true).warning(UtilFns.getStackTraceString(ex));
    }
    return null;
  }

  /**
   * Generates a X.509 certificate object and initializes it with
   * the data read from the file.
   *
   * <p>The given file must contain a single certificate.
   *
   * <p>The X.509 certificate provided in the file must be
   * DER-encoded and may be supplied in binary or printable (Base64) encoding.
   * If the certificate is provided in Base64 encoding, it must be bounded at
   * the beginning by -----BEGIN CERTIFICATE-----, and must be bounded at
   * the end by -----END CERTIFICATE-----.
   *
   * <p>Note that if the given input stream does not support
   * {@link java.io.InputStream#mark(int) mark} and
   * {@link java.io.InputStream#reset() reset}, this method will
   * consume the entire input stream.
   *
   * @param certificateFile the certificate file.
   *
   * @return a certificate object initialized with the data
   * from the input stream.
   *
   * @exception CertificateException on parsing errors.
   */
  public static X509Certificate generateX509Certificate(String certificateFile)
  {
    try
    {
      final InputStream inStream =
          FileUtils.fileMultiOpenInputStream(certificateFile);
      if (inStream == null)  //if file could not be open
      {
        LogFile.getGlobalLogObj(true).warning(
            "Could not open certificate file: " + certificateFile);
        return null;
      }
      return generateX509Certificate(inStream);
    }
    catch(Exception ex)
    {
      //some kind of exception error; log it
      LogFile.getGlobalLogObj(true).warning(
          "Could not generate X509Certificate:  " + ex);
      LogFile.getGlobalLogObj(true).warning(UtilFns.getStackTraceString(ex));
    }
    return null;
  }

  /**
   * Generates a X.509 certificate revocation list (CRL) object and initializes
   * it with the specified data.
   *
   * @param data the CRL data.
   *
   * @return a CRL object initialized with the data.
   *
   * @exception CRLException on parsing errors.
   */
  public static X509CRL generateX509CRL(byte[] data)
  {
    try
    {
      final InputStream inStream = new ByteArrayInputStream(data);
      return generateX509CRL(inStream);
    }
    catch(Exception ex)
    {
      //some kind of exception error; log it
      LogFile.getGlobalLogObj(true).warning(
          "Could not generate X509CRL:  " + ex);
      LogFile.getGlobalLogObj(true).warning(UtilFns.getStackTraceString(ex));
    }
    return null;
  }

  /**
   * Generates a X.509 certificate revocation list (CRL) object and initializes
   * it with the data read from the file.
   *
   * <p>In order to take advantage of the specialized CRL format
   * supported by this certificate factory,
   * the returned CRL object can be typecast to the corresponding
   * CRL class. For example, if this certificate
   * factory implements X.509 CRLs, the returned CRL object
   * can be typecast to the <code>X509CRL</code> class.
   *
   * @param crlFile the CRL file.
   *
   * @return a CRL object initialized with the data from the file.
   *
   * @exception CRLException on parsing errors.
   */
  public static X509CRL generateX509CRL(File crlFile)
  {
    try
    {
      final InputStream inStream = new FileInputStream(crlFile);
      return generateX509CRL(inStream);
    }
    catch(Exception ex)
    {
      //some kind of exception error; log it
      LogFile.getGlobalLogObj(true).warning(
          "Could not generate X509CRL:  " + ex);
      LogFile.getGlobalLogObj(true).warning(UtilFns.getStackTraceString(ex));
    }
    return null;
  }

  /**
   * Generates a X.509 certificate revocation list (CRL) object and initializes
   * it with the data read from the input stream <code>inStream</code>.
   *
   * <p>Note that if the given input stream does not support
   * {@link java.io.InputStream#mark(int) mark} and
   * {@link java.io.InputStream#reset() reset}, this method will
   * consume the entire input stream.
   *
   * @param inStream an input stream with the CRL data.
   *
   * @return a CRL object initialized with the data
   * from the input stream.
   *
   * @exception CRLException on parsing errors.
   */
  public static X509CRL generateX509CRL(InputStream inStream)
  {
    try
    {
      CertificateFactory cf = CertificateFactory.getInstance("X.509");
      X509CRL crl = (X509CRL)cf.generateCRL(inStream);
      inStream.close();
      return crl;
    }
    catch(Exception ex)
    {
      //some kind of exception error; log it
      LogFile.getGlobalLogObj(true).warning(
          "Could not generate X509CRL:  " + ex);
      LogFile.getGlobalLogObj(true).warning(UtilFns.getStackTraceString(ex));
    }
    return null;
  }

  /**
   * Generates a X.509 certificate revocation list (CRL) object and initializes
   * it with the data read from the file.
   *
   * @param crlFile the CRL file.
   *
   * @return a CRL object initialized with the data from the file.
   *
   * @exception CRLException on parsing errors.
   */
  public static X509CRL generateX509CRL(String crlFile)
  {
    final InputStream inStream =
        FileUtils.fileMultiOpenInputStream(crlFile);
    if (inStream == null)  //if file could not be open
    {
      LogFile.getGlobalLogObj(true).warning(
          "Could not open CRL file: " + crlFile);
      return null;
    }
    return generateX509CRL(inStream);
  }

  /**
   * Gets the CRL entry, if any, with the given certificate serialNumber.
   * @param crl the certificate revocation list
   * @param serialNumber the serial number of the certificate for which a CRL
   * entry is to be looked up
   * @return the entry with the given serial number, or null if no such entry
   * exists in this CRL.
   * @see X509CRLEntry
   */
  public static X509CRLEntry getRevokedCertificate(
      X509CRL crl, BigInteger serialNumber)
  {
    try
    {
      final X509CRLEntry revokedCert = crl.getRevokedCertificate(serialNumber);
      return revokedCert;
    }
    catch(Exception ex)
    {
      //some kind of exception error; log it
      LogFile.getGlobalLogObj(true).warning(
          "Could not generate X509CRL:  " + ex);
      LogFile.getGlobalLogObj(true).warning(UtilFns.getStackTraceString(ex));
    }
    return null;
  }

  /**
   * Gets the CRL entry, if any, with the given certificate serialNumber.
   * @param crl the certificate revocation list
   * @param cert the certificate for which a CRL entry is to be looked up
   * @return the entry with the given serial number, or null if no such entry
   * exists in this CRL.
   * @see X509CRLEntry
   */
  public static X509CRLEntry getRevokedCertificate(
      X509CRL crl, X509Certificate cert)
  {
    return getRevokedCertificate(crl, cert.getSerialNumber());
  }

  /**
   * Validates the signature text.
   * @param msgText the message text.
   * @param sigText the signature text.
   * @param kp tke key pair.
   * @return true if the signature text is valid, false otherwise.
   */
  public static boolean isValidSignatureText(
      String msgText, String sigText, KeyPair kp)
  {
    return isValidSignatureText(msgText, sigText, kp.getPublic());
  }

  /**
   * Validates the signature text.
   * @param msgText the message text.
   * @param sigText the signature text.
   * @param pub tke public key.
   * @return true if the signature text is valid, false otherwise.
   */
  public static boolean isValidSignatureText(
      String msgText, String sigText, PublicKey pub)
  {
    String algorithm = pub.getAlgorithm();

    if (RSA_ALGORITHM.equals(algorithm))  //if RSA is specified
    {
      algorithm = SHA1_WITH_RSA;  //use SHA1withRSA
    }

    try
    {
      //acquire a signature
      final Signature sig = Signature.getInstance(algorithm);
      sig.initVerify(pub);
      return isValidSignatureText(msgText, sigText, sig);
    }
    catch(InvalidKeyException ex)
    {
      LogFile.getGlobalLogObj(false).debug("Key is invalid: " + ex);
    }
    catch(Exception ex)
    {
      //some kind of exception error; log it
      LogFile.getGlobalLogObj(true).warning(
          "isValidSignatureText error:  " + ex);
      LogFile.getGlobalLogObj(true).warning(UtilFns.getStackTraceString(ex));
    }
    return false;
  }

  /**
   * Validates the signature text.
   * @param msgText the message text.
   * @param sigText the signature text.
   * @param sig tke signature.
   * @return true if the signature text is valid, false otherwise.
   */
  public static boolean isValidSignatureText(
      String msgText, String sigText, Signature sig)
  {
    try
    {
      final byte[] raw = decodeBuffer(sigText);
      sig.update(msgText.getBytes(DEFAULT_CHARACTER_ENCODING));
      return sig.verify(raw);
    }
    catch(Exception ex)
    {
      //some kind of exception error; log it
      LogFile.getGlobalLogObj(true).warning(
          "isValidSignatureText error:  " + ex);
      LogFile.getGlobalLogObj(true).warning(UtilFns.getStackTraceString(ex));
    }
    return false;
  }

  /**
   * Validates the signature text.
   * @param msgText the message text.
   * @param sigText the signature text.
   * @param cert tke certificate.
   * @return true if the signature text is valid, false otherwise.
   */
  public static boolean isValidSignatureText(
      String msgText, String sigText, X509Certificate cert)
  {
    try
    {
      return isValidSignatureText(msgText, sigText, cert.getPublicKey());
    }
    catch (Exception ex)
    {
      //some kind of exception error; log it
      LogFile.getGlobalLogObj(true).warning(
          "isValidSignatureText error:  " + ex);
      LogFile.getGlobalLogObj(true).warning(UtilFns.getStackTraceString(ex));
    }
    return false;
  }

  /**
   * Checks that the X.509 certificate is currently valid. It is if
   * the current date and time are within the validity period given in the
   * certificate.
   * <p>
   * The validity period consists of two date/time values:
   * the first and last dates (and times) on which the certificate
   * is valid. It is defined in
   * ASN.1 as:
   * <pre>
   * validity             Validity<p>
   * Validity ::= SEQUENCE {
   *     notBefore      CertificateValidityDate,
   *     notAfter       CertificateValidityDate }<p>
   * CertificateValidityDate ::= CHOICE {
   *     utcTime        UTCTime,
   *     generalTime    GeneralizedTime }
   * </pre>
   *
   * @param cert tke certificate.
   * @returns true if it is valid, false otherwise.
   */
  public static boolean isValidX509Certificate(X509Certificate cert)
  {
    try
    {
      cert.checkValidity();  //check if the certificate is valid
      return true;
    }
    catch(CertificateException ex)
    {
      LogFile.getGlobalLogObj(false).debug("Certificate is not valid: " + ex);
    }
    catch (Exception ex)
    {
      LogFile.getGlobalLogObj(true).warning(
          "isValidX509Certificate error: " + ex);
      LogFile.getGlobalLogObj(true).warning(UtilFns.getStackTraceString(ex));
    }
    return false;
  }

  /**
   * Checks that the X.509 CRL is currently valid. It is if
   * the current date and time are within the validity period given in the
   * certificate.
   *
   * @param crl the certificate revocation list
   * @returns true if it is valid, false otherwise.
   */
  public static boolean isValidX509CRL(X509CRL crl)
  {
    try
    {
      final Date currentDate = new Date();
      final Date nextUpdate = crl.getNextUpdate();
      final Date thisUpdate = crl.getThisUpdate();

      if (thisUpdate != null && thisUpdate.after(currentDate))
      {
        LogFile.getGlobalLogObj(false).debug(
            "CRL update is after current time");
        return false;
      }

      if (nextUpdate != null && nextUpdate.before(currentDate))
      {
        LogFile.getGlobalLogObj(false).debug(
            "CRL next update is before current time");
        return false;
      }

      return true;
    }
    catch (Exception ex)
    {
      LogFile.getGlobalLogObj(true).warning("isValidX509CRL error: " + ex);
      LogFile.getGlobalLogObj(true).warning(UtilFns.getStackTraceString(ex));
    }
    return false;
  }

  /**
   * Verifies that the X.509 certificate is valid and was signed
   * using the private key that corresponds to the given public key.
   * @param cert tke certificate.
   * @param key the PublicKey used to carry out the verification.
   * @return true if certificate is from the public key, false otherwise.
   */
  public static boolean verifyX509Certificate(
      X509Certificate cert, PublicKey key)
  {
    try
    {
      cert.verify(key);
      return true;
    }
    catch (GeneralSecurityException ex)
    {
      LogFile.getGlobalLogObj(false).debug("Certificate is not valid: " + ex);
    }
    catch (Exception ex)
    {
      LogFile.getGlobalLogObj(true).warning(
          "verifyX509Certificate error: " + ex);
      LogFile.getGlobalLogObj(true).warning(UtilFns.getStackTraceString(ex));
    }
    return false;
  }

  /**
   * Verifies that the X.509 certificate revocation list (CRL) is valid and
   * was signed using the private key that corresponds to the given public key.
   * @param crl the certificate revocation list
   * @param key the PublicKey used to carry out the verification.
   * @return true if CRL is from the public key, false otherwise.
   */
  public static boolean verifyX509CRL(X509CRL crl, PublicKey key)
  {
    try
    {
      crl.verify(key);
      return true;
    }
    catch (GeneralSecurityException ex)
    {
      LogFile.getGlobalLogObj(false).debug("CRL is not valid: " + ex);
    }
    catch (Exception ex)
    {
      LogFile.getGlobalLogObj(true).warning("verifyX509CRL error: " + ex);
      LogFile.getGlobalLogObj(true).warning(UtilFns.getStackTraceString(ex));
    }
    return false;
  }

  /**
   * Verifies the X.509 information.
   * @param certificateFile the certificate file.
   * @param certificateOfAuthorityFile the certificate of authority file.
   * @param crlFile the CRL file.
   * @return true if the X.509 information is valid, false otherwise.
   */
  public static boolean verifyX509Information(
      String certificateFile, String certificateOfAuthorityFile, String crlFile)
  {
    final X509Certificate certificate =
        generateX509Certificate(certificateFile);
    final X509Certificate certificateOfAuthority =
        generateX509Certificate(certificateOfAuthorityFile);
    final X509CRL crl = generateX509CRL(crlFile);
    return verifyX509Information(certificate, certificateOfAuthority, crl);
  }

  /**
   * Verifies the X.509 information.
   * @param certificate the certificate.
   * @param certificateOfAuthority the certificate of authority.
   * @param crl the CRL.
   * @return true if the X.509 information is valid, false otherwise.
   */
  public static boolean verifyX509Information(
      X509Certificate certificate, X509Certificate certificateOfAuthority,
      X509CRL crl)
  {
    if (certificate == null || !isValidX509Certificate(certificate))
    {
      LogFile.getGlobalLogObj(false).debug("Certificate is not valid");
      return false;
    }

    if (certificateOfAuthority == null ||
        !isValidX509Certificate(certificateOfAuthority))
    {
      LogFile.getGlobalLogObj(false).debug(
          "Certificate of authority is not valid");
      return false;
    }

    if (crl == null || !isValidX509CRL(crl))
    {
      LogFile.getGlobalLogObj(false).debug("CRL is not valid");
      return false;
    }

    if (!verifyX509Certificate(
                               certificate, certificateOfAuthority.getPublicKey()))
    {
      LogFile.getGlobalLogObj(false).debug(
          "Certificate is not from certificate of authority");
      return false;
    }

    if (!verifyX509CRL(crl, certificateOfAuthority.getPublicKey()))
    {
      LogFile.getGlobalLogObj(false).debug(
          "CRL is not from certificate of authority");
      return false;
    }

    if (getRevokedCertificate(crl, certificate) != null)
    {
      LogFile.getGlobalLogObj(false).debug("Certificate is on the CRL");
      return false;
    }

    return true;
  }
}
