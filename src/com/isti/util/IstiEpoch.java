/*--- formatted by Jindent 2.1, (www.c-lab.de/~jindent) ---*/

//
// =====================================================================
// Copyright (C) 2010 Instrumental Software Technologies, Inc.
//
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions
// are met:
// 1. Redistributions of source code, or portions of this source code,
// must retain the above copyright notice, this list of conditions
// and the following disclaimer.
// 2. Redistributions in binary form must reproduce the above copyright
// notice, this list of conditions and the following disclaimer in
// the documentation and/or other materials provided with the
// distribution.
// 3. All advertising materials mentioning features or use of this
// software must display the following acknowledgment:
// "This product includes software developed by Instrumental
// Software Technologies, Inc. (http://www.isti.com)"
// 4. If the software is provided with, or as part of a commercial
// product, or is used in other commercial software products the
// customer must be informed that "This product includes software
// developed by Instrumental Software Technologies, Inc.
// (http://www.isti.com)"
// 5. The names "Instrumental Software Technologies, Inc." and "ISTI"
// must not be used to endorse or promote products derived from
// this software without prior written permission. For written
// permission, please contact "info@isti.com".
// 6. Products derived from this software may not be called "ISTI"
// nor may "ISTI" appear in their names without prior written
// permission of Instrumental Software Technologies, Inc.
// 7. Redistributions of any form whatsoever must retain the following
// acknowledgment:
// "This product includes software developed by Instrumental
// Software Technologies, Inc. (http://www.isti.com/)."
// THIS SOFTWARE IS PROVIDED BY INSTRUMENTAL SOFTWARE
// TECHNOLOGIES, INC. "AS IS" AND ANY EXPRESSED OR IMPLIED
// WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES
// OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
// DISCLAIMED.  IN NO EVENT SHALL INSTRUMENTAL SOFTWARE TECHNOLOGIES,
// INC. OR ITS CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
// INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
// (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
// SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
// HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
// STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
// ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED
// OF THE POSSIBILITY OF SUCH DAMAGE.
// =====================================================================
// A current version of the software can be found at
// http://www.isti.com
// Bug reports and comments should be directed to
// Instrumental Software Technologies, Inc. at info@isti.com
// =====================================================================
//
package com.isti.util;

import java.text.DecimalFormat;
import java.util.Calendar;
import java.util.Comparator;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.StringTokenizer;
import java.util.TimeZone;

/**
 * IstiEpoch is a class that is used to help convert from IstiEpochs (either
 * longs or doubles) into GregorianCalendar
 * @author Instrumental Software Technologies, Inc. (<a
 *         href="http://www.isti.com">ISTI</a>)
 * @version 1.0.232
 */
public class IstiEpoch implements Comparable, Comparator {
  final static public String epochFormat = "Epoch";

  final static public String defaultFormat = "Year:Day:Hr:Min";

  final static public String defaultBottomFormat = "Min:Sec";

  private final static String[] formatType = new String[] { "None", "AllTime",
      "Year:Day:Hr:Min:Sec", "Year:Day:Hr:Min", "Year:Day:Hr", "Year:Day",
      "Day:Hr:Min:Sec.Msec", "Day:Hr:Min:Sec", "Day:Hr:Min", "Day:Hr:", "Day",
      "Hr:Min:Sec.MSec", "Hr:Min:Sec", "Hr:Min", "Min:Sec.Msec", "Min:Sec",
      "Min", "Sec.Msec", "Msec", "Epoch" };

  final static public int None = 0;

  final static public int AllTime = 1;

  final static public int YearDayHrMinSec = 2;

  final static public int YearDayHrMin = 3;

  final static public int YearDayHr = 4;

  final static public int YearDay = 5;

  final static public int DayHrMinSecMsec = 6;

  final static public int DayHrMinSec = 7;

  final static public int DayHrMin = 8;

  final static public int DayHr = 9;

  final static public int Day = 10;

  final static public int HrMinSecMSec = 11;

  final static public int HrMinSec = 12;

  final static public int HrMin = 13;

  final static public int MinSecMsec = 14;

  final static public int MinSec = 15;

  final static public int Min = 16;

  final static public int SecMsec = 17;

  final static public int Msec = 18;

  final static public int Epoch = 19;

  final static String[] monthNames = new String[] { "Jan", "Feb", "Mar", "Apr",
      "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec" };

  // the time converter or null if none
  private IstiEpochConverter _converter = null;

  /**
   * The number of milliseconds since January 1, 1970, 00:00:00 GMT.
   */
  private long _millisecondsSince1970 = 0;

  private String message = null;

  /**
   * This constructor sets the time to 0 milliseconds.
   * @see #settoCurrentTime()
   */
  public IstiEpoch() {
  }

  /**
   * Creates an IstiEpoch from an existing calendar.
   * @param calendar the calendar
   */
  public IstiEpoch(Calendar calendar) {
    if (calendar != null) {
      setTime(calendar);
    }
  }

  /**
   * Creates an IstiEpoch from a Date
   * @param date the date
   */
  public IstiEpoch(Date date) {
    if (date != null) {
      setTime(date);
    }
  }

  /**
   * Creates an IstiEpoch from the number of seconds from the Epoch time
   * @param seconds the seconds from the Epoch time
   */
  public IstiEpoch(double seconds) {
    setTime(seconds);
  }

  /**
   * Creates an IstiEpoch from another Epoch time
   * @param epoch the other Epoch time or null if none
   */
  public IstiEpoch(IstiEpoch epoch) {
    if (epoch != null) {
      setTime(epoch);
    }
  }

  /**
   * Creates an IstiEpoch from the number of milliseconds from the Epoch time
   * @param milliseconds milliseconds since 1970.
   */
  public IstiEpoch(long milliseconds) {
    setTimeInMillis(milliseconds);
  }

  /**
   * Creates an IstiEpoch from the number of seconds and milliseconds from the
   * Epoch time
   * @param seconds seconds since 1970.
   * @param milliseconds milliseconds since 1970.
   */
  public IstiEpoch(long seconds, long milliseconds) {
    setTime(seconds, milliseconds);
  }

  /**
   * Creates an IstiEpoch from a string in RESP output format YYYY,DDD,HH:MM:SS
   * from the Epoch time
   * @param respTime the RESP time
   */
  public IstiEpoch(String respTime) {
    setTime(respTime);
  }

  /**
   * This function returns the number of seconds between this and another
   * IstiEpoch
   * @param epoch the other epoch time
   */
  public double absDiff(IstiEpoch epoch) {
    double thisTime = getEpoch();
    double newTime = epoch.getEpoch();
    if (thisTime > newTime) {
      return (thisTime - newTime);
    }
    return (newTime - thisTime);
  }

  /**
   * This function adds the number of seconds to the time
   * @param seconds the number of seconds
   */
  public void add(double seconds) {
    double curTime = getEpoch();
    curTime += seconds;
    setTime(curTime);
  }

  /**
   * Get the Btime formatted string
   * @return the Btime formatted string
   */
  public String btimeFormattedString() {
    // String format must be YYYY,DDD,HH:MM:SS.FFFF.
    String year = getYear();
    String jday = getJDay();
    String hr = getHr();
    String mn = getMn();
    String sec = getSecond();
    String retStr = year;
    retStr = retStr.concat(",");
    retStr = retStr.concat(jday);
    retStr = retStr.concat(",");
    retStr = retStr.concat(hr);
    retStr = retStr.concat(":");
    retStr = retStr.concat(mn);
    retStr = retStr.concat(":");
    retStr = retStr.concat(sec);
    retStr = retStr.concat(".");
    retStr = retStr.concat(getTenthsMillisecond());
    return retStr;
  }

  /**
   * format like this..... "yyyyMMdd.HHmmss"
   * @return a string representation of the time
   */
  public String CanadaFormattedString() {
    String year = getYear();
    int mon = getIntMonth();
    // mon runs from 0 - 11, need to add one
    mon++;
    int dom = getIntDOM();
    String mday = Integer.toString(dom);
    DecimalFormat df = new DecimalFormat("00");
    mday = df.format(dom);
    String hr = getHr();
    String mn = getMn();
    String sec = getSecond();
    String smon = Integer.toString(mon);
    smon = df.format(mon);
    String retStr = year;
    retStr = retStr.concat(smon);
    retStr = retStr.concat(mday);
    retStr = retStr.concat(".");
    retStr = retStr.concat(hr);
    retStr = retStr.concat(mn);
    retStr = retStr.concat(sec);
    return retStr;
  }

  /**
   * Clear the time by setting the Epoch to 0.
   */
  public void clear() {
    setTimeInMillis(0);
  }

  /**
   * Compares its two arguments for order.
   * @param time1 the first time.
   * @param time2 the second time.
   * @return a negative integer, zero, or a positive integer as the first
   *         argument is less than, equal to, or greater than the second
   */
  public int compare(IstiEpoch time1, IstiEpoch time2) {
    return time1.compareTo(time2);
  }

  /**
   * Compares its two arguments for order.
   * @param o1 the first Object.
   * @param o2 the second Object.
   * @return a negative integer, zero, or a positive integer as the first
   *         argument is less than, equal to, or greater than the second
   * @throws ClassCastException - if an argument is not an IstiEpoch.
   */
  public int compare(Object o1, Object o2) throws ClassCastException {
    return compare((IstiEpoch) o1, (IstiEpoch) o2);
  }

  /**
   * Compares this IstiEpoch to another Object. If the Object is an IstiEpoch,
   * this function behaves like compareTo(IstiEpoch). Otherwise, it throws a
   * ClassCastException (as IstiEpochs are comparable only to other IstiEpochs).
   * @param anotherTime the time to be compared.
   * @return the value 0 if the argument is an IstiEpoch equal to this
   *         IstiEpoch; a value less than 0 if the argument is an IstiEpoch
   *         after this IstiEpoch; and a value greater than 0 if the argument is
   *         an IstiEpoch before this IstiEpoch.
   */
  public int compareTo(IstiEpoch anotherTime) {
    long rtime = anotherTime.getTimeInMillis();
    long thistime = getTimeInMillis();
    if (thistime == rtime) {
      return 0;
    }
    if (thistime > rtime) {
      return 1;
    }
    return -1;
  }

  /**
   * Compares this IstiEpoch to another Object. If the Object is an IstiEpoch,
   * this function behaves like compareTo(IstiEpoch). Otherwise, it throws a
   * ClassCastException (as IstiEpochs are comparable only to other IstiEpochs).
   * @param o Object
   * @return the value 0 if the argument is an IstiEpoch equal to this
   *         IstiEpoch; a value less than 0 if the argument is an IstiEpoch
   *         after this IstiEpoch; and a value greater than 0 if the argument is
   *         an IstiEpoch before this IstiEpoch
   * @throws ClassCastException - if the argument is not an IstiEpoch.
   */
  public int compareTo(Object o) throws ClassCastException {
    return compareTo((IstiEpoch) o);
  }

  /**
   * This function returns the number of seconds between this and another
   * IstiEpoch
   * @param epoch another epoch time
   */
  public double diff(IstiEpoch epoch) {
    double thisTime = getEpoch();
    double newTime = epoch.getEpoch();
    return (thisTime - newTime);
  }

  public boolean equals(Object o) {
    return o instanceof IstiEpoch && isEqualTo((IstiEpoch) o);
  }

  public String fileFormattedString() {
    String year = getYear();
    String jday = getJDay();
    String hr = getHr();
    String mn = getMn();
    String sec = getSecond();
    String retStr = year;
    retStr = retStr.concat(".");
    retStr = retStr.concat(jday);
    retStr = retStr.concat(".");
    retStr = retStr.concat(hr);
    retStr = retStr.concat(".");
    retStr = retStr.concat(mn);
    retStr = retStr.concat(".");
    retStr = retStr.concat(sec);
    return retStr;
  }

  /**
   * format like this..... 20070410T22:53:00.000Z
   * @return String
   */
  public String FissuresFormattedString() {
    // String format must be YYYY,DDD,HH:MM:SS.FFFF.
    // 19951231T235959
    String year = getYear();
    int mon = getIntMonth();
    // mon runs from 0 - 11, need to add one
    mon++;
    int dom = getIntDOM();
    String mday = Integer.toString(dom);
    DecimalFormat df = new DecimalFormat("00");
    mday = df.format(dom);
    String hr = getHr();
    String mn = getMn();
    String sec = getSecond();
    String retStr = year;
    String smon = Integer.toString(mon);
    smon = df.format(mon);
    retStr = retStr.concat(smon);
    retStr = retStr.concat(mday);
    retStr = retStr.concat("T");
    retStr = retStr.concat(hr);
    retStr = retStr.concat(":");
    retStr = retStr.concat(mn);
    retStr = retStr.concat(":");
    retStr = retStr.concat(sec);
    retStr = retStr.concat(".");
    retStr = retStr.concat(getTenthsMillisecond());
    retStr = retStr.concat("Z");
    return retStr;
  }

  /**
   * Get a string with at least the specified number of places by padded with
   * zero's.
   * @param item the text
   * @param place the number of places
   */
  public String formatInt(String item, int places) {
    String retString = new String(item);
    if (item.length() >= places) {
      return retString;
    }

    int needed = places - item.length();
    retString = "";
    for (int i = 0; i < needed; i++) {
      retString = retString.concat("0");
    }
    retString = retString.concat(item);
    return retString;
  }

  public String formattedString() {
    String year = getYear();
    String jday = getJDay();
    String hr = getHr();
    String mn = getMn();
    String sec = getSecond();
    String retStr = year;
    retStr = retStr.concat(":");
    retStr = retStr.concat(jday);
    retStr = retStr.concat(":");
    retStr = retStr.concat(hr);
    retStr = retStr.concat(":");
    retStr = retStr.concat(mn);
    retStr = retStr.concat(":");
    retStr = retStr.concat(sec);
    return retStr;
  }

  public String formattedString(int type) {
    String year = getYear();
    String jday = getJDay();
    String hr = getHr();
    String mn = getMn();
    String sec = getSecond();
    String msec = getMSecond();
    String retStr = "";
    switch (type) {
    case AllTime:
      retStr = year;
      retStr = retStr.concat(":");
      retStr = retStr.concat(jday);
      retStr = retStr.concat(":");
      retStr = retStr.concat(hr);
      retStr = retStr.concat(":");
      retStr = retStr.concat(mn);
      retStr = retStr.concat(":");
      retStr = retStr.concat(sec);
      retStr = retStr.concat(".");
      retStr = retStr.concat(msec);
      break;

    case YearDayHrMinSec:
      retStr = year;
      retStr = retStr.concat(":");
      retStr = retStr.concat(jday);
      retStr = retStr.concat(":");
      retStr = retStr.concat(hr);
      retStr = retStr.concat(":");
      retStr = retStr.concat(mn);
      retStr = retStr.concat(":");
      retStr = retStr.concat(sec);
      break;

    case YearDay:
      retStr = year;
      retStr = retStr.concat(":");
      retStr = retStr.concat(jday);
      break;

    case YearDayHr:
      retStr = year;
      retStr = retStr.concat(":");
      retStr = retStr.concat(jday);
      retStr = retStr.concat(":");
      retStr = retStr.concat(hr);
      break;

    case YearDayHrMin:
      retStr = year;
      retStr = retStr.concat(":");
      retStr = retStr.concat(jday);
      retStr = retStr.concat(":");
      retStr = retStr.concat(hr);
      retStr = retStr.concat(":");
      retStr = retStr.concat(mn);
      break;

    case HrMin:
      retStr = hr;
      retStr = retStr.concat(":");
      retStr = retStr.concat(mn);
      break;

    case HrMinSec:
      retStr = hr;
      retStr = retStr.concat(":");
      retStr = retStr.concat(mn);
      retStr = retStr.concat(":");
      retStr = retStr.concat(sec);
      break;

    case HrMinSecMSec:
      retStr = hr;
      retStr = retStr.concat(":");
      retStr = retStr.concat(mn);
      retStr = retStr.concat(":");
      retStr = retStr.concat(sec);
      retStr = retStr.concat(".");
      retStr = retStr.concat(msec);
      break;

    case MinSec:
      retStr = mn;
      retStr = retStr.concat(":");
      retStr = retStr.concat(sec);
      break;

    case Min:
      retStr = mn;
      break;

    case MinSecMsec:
      retStr = mn;
      retStr = retStr.concat(":");
      retStr = retStr.concat(sec);
      retStr = retStr.concat(".");
      retStr = retStr.concat(msec);
      break;

    case SecMsec:
      retStr = sec;
      retStr = retStr.concat(".");
      retStr = retStr.concat(msec);
      break;

    case Msec:
      retStr = msec;
      break;

    case Epoch:
      retStr = Long.toString(getIntEpochSeconds());
      break;

    case DayHrMinSecMsec:
      retStr = jday;
      retStr = retStr.concat(":");
      retStr = retStr.concat(hr);
      retStr = retStr.concat(":");
      retStr = retStr.concat(mn);
      retStr = retStr.concat(":");
      retStr = retStr.concat(sec);
      retStr = retStr.concat(".");
      retStr = retStr.concat(msec);
      break;

    case DayHrMinSec:
      retStr = jday;
      retStr = retStr.concat(":");
      retStr = retStr.concat(hr);
      retStr = retStr.concat(":");
      retStr = retStr.concat(mn);
      retStr = retStr.concat(":");
      retStr = retStr.concat(sec);
      break;

    case DayHrMin:
      retStr = jday;
      retStr = retStr.concat(":");
      retStr = retStr.concat(hr);
      retStr = retStr.concat(":");
      retStr = retStr.concat(mn);
      break;

    case DayHr:
      retStr = jday;
      retStr = retStr.concat(":");
      retStr = retStr.concat(hr);
      break;

    case Day:
      retStr = jday;
      break;

    case None:
      retStr = "";
      break;

    default:
      retStr = "";
    }

    return retStr;
  }

  public String formattedString(String name) {
    for (int i = 0; i < formatType.length; i++) {
      if (name.compareTo((formatType[i])) == 0) {
        return formattedString(i);
      }
    }
    return "";
  }

  /**
   * Get the value for the given calendar field.
   * @param calVal the calendar field.
   * @return the value
   */
  public int get(int calVal) {
    return getConverter().get(this, calVal);
  }

  /**
   * Get the time converter.
   * @return the time converter.
   */
  private final IstiEpochConverter getConverter() {
    if (_converter == null) {
      _converter = new IstiEpochConverter();
    }
    return _converter;
  }

  /**
   * Get the number of seconds from the epoch time
   * @return the number seconds from the Epoch time
   */
  public double getEpoch() {
    return ((double) getTimeInMillis()) / 1000.0;
  }

  /**
   * Get the GregorianCalendar
   * @return the GregorianCalendar
   */
  public GregorianCalendar getGregorianCalendar() {
    return getConverter().getGregorianCalendar(this);
  }

  /**
   * Get the hour of day
   * @return the hour of day
   */
  public String getHr() {
    String hrStr = Integer.toString(getIntInt(Calendar.HOUR_OF_DAY));
    return (formatInt(hrStr, 2));
  }

  /**
   * Get the day of month
   * @return the day of month
   */
  public int getIntDOM() {
    return get(Calendar.DAY_OF_MONTH);
  }

  /**
   * Get the day of year
   * @return the day of year
   */
  public int getIntDOY() {
    return get(Calendar.DAY_OF_YEAR);
  }

  /**
   * Get the number of seconds from the epoch time
   * @return the number of seconds from the epoch time
   * @deprecated This only supports up to the year 2038, use
   *             {@link #getIntEpochSeconds()}.
   */
  public int getIntEpoch() {
    final long longEpoch = getIntEpochSeconds();
    final int intEpoch = (int) longEpoch;
    if (intEpoch < 0 && longEpoch > 0) {
      return Integer.MAX_VALUE;
    }
    return intEpoch;
  }

  /**
   * Get the number of milliseconds from the epoch time
   * @return the number of milliseconds from the epoch time
   */
  public long getIntEpochMSecs() {
    return getTimeInMillis();
  }

  /**
   * Get the number of seconds from the epoch time
   * @return the number of milliseconds from the epoch time
   */
  public long getIntEpochSeconds() {
    return getTimeInMillis() / 1000L;
  }

  /**
   * Get the hour of day.
   * @return the hour of day
   */
  public int getIntHr() {
    return get(Calendar.HOUR_OF_DAY);
  }

  /**
   * Get the value for the given calendar field.
   * @param calVal the calendar field.
   * @return the value
   */
  public int getIntInt(int calVal) {
    return get(calVal);
  }

  /**
   * Get the minute within the hour.
   * @return the minute within the hour
   */
  public int getIntMn() {
    return get(Calendar.MINUTE);
  }

  /**
   * Get the month.
   * @return the month.
   */
  public int getIntMonth() {
    return get(Calendar.MONTH);
  }

  /**
   * Get the second within the minute.
   * @return the second within the minute
   */
  public int getIntSec() {
    return get(Calendar.SECOND);
  }

  /**
   * Get the year
   * @return the year
   */
  public int getIntYear() {
    return get(Calendar.YEAR);
  }

  /**
   * Get the text for the day of the year.
   * @return the text for the day of the year
   */
  public String getJDay() {
    String retStr = Integer.toString(getIntDOY());
    return (formatInt(retStr, 3));
  }

  /**
   * Get the number of seconds from the epoch time
   * @return the number of seconds from the epoch time
   */
  public long getLongEpoch() {
    return getIntEpochSeconds();
  }

  /**
   * Get the day of month
   * @return the day of month
   */
  public int getmDay() {
    return get(Calendar.DAY_OF_MONTH);
  }

  /**
   * Get the month
   * @return the month
   */
  public String getMDay() {
    String retStr = Integer.toString(getIntMonth());
    return (formatInt(retStr, 2));
  }

  /**
   * Get the message that some methods leave
   * @return the message or null if none
   */
  public String getMessage() {
    return message;
  }

  /**
   * Gets the number of milliseconds since the second.
   * @return the number of milliseconds since the second.
   */
  private int getMilliSinceSec() {
    return (int) (getTimeInMillis() % 1000);
  }

  /**
   * Get the text for the minute within the hour
   * @return the text for the minute within the hour
   */
  public String getMn() {
    String minStr = Integer.toString(getIntInt(Calendar.MINUTE));
    return (formatInt(minStr, 2));
  }

  /**
   * Get the text for the month
   * @return the text for the month
   */
  public String getMonStr() {
    int day = getIntMonth();
    return (monthNames[day]);
  }

  /**
   * Get the text for milliseconds
   * @param place the number of places
   * @return the text for milliseconds
   */
  public String getMsecond(int places) {
    String retStr = null;
    final int milliSinceSec = getMilliSinceSec();
    if (milliSinceSec == 0) {
      // even second.....
      return "0000";
    }

    retStr = Long.toString(milliSinceSec);
    String rv = formatInt(retStr, places);
    // need to trim ending zeros...
    while (retStr.endsWith("0")) {
      retStr = retStr.substring(0, retStr.length() - 1);
    }
    return rv;
  }

  /**
   * Get the text for milliseconds with 3 places
   * @return the text for milliseconds with 3 places
   */
  public String getMSecond() {
    return getMsecond(3);
  }

  /**
   * Get the text for second within the minute
   * @return the text for second within the minute
   */
  public String getSecond() {
    String retStr = Integer.toString(getIntInt(Calendar.SECOND));
    return (formatInt(retStr, 2));
  }

  /**
   * Get the text for tenths milliseconds
   * @return the text for tenths milliseconds
   */
  public String getTenthsMillisecond() {
    String retStr = null;
    int sec = getMilliSinceSec();
    if ((sec % 1000) == 0) {
      // even second.....
      return "0000";
    }

    retStr = Integer.toString(sec);
    String rv = formatInt(retStr, 3);
    rv = new String(rv + "0");
    return rv;
  }

  /**
   * Get the time
   * @return the time
   */
  public Date getTime() {
    return getConverter().getTime(this);
  }

  /**
   * Gets the current time as a long.
   * @return the current time as UTC milliseconds from the epoch, January 1,
   *         1970, 00:00:00.
   */
  public long getTimeInMillis() {
    return _millisecondsSince1970;
  }

  /**
   * Get the time zone
   * @return the time zone
   */
  public TimeZone getTimeZone() {
    return UtilFns.GMT_TIME_ZONE_OBJ;
  }

  /**
   * Get the text for the year
   * @return the text for the year
   */
  public String getYear() {
    int year = getIntYear();
    return Integer.toString(year);
  }

  /**
   * Returns a hash code value for this object. The result is the exclusive OR
   * of the two halves of the primitive long value returned by the getTime()
   * method. That is, the hash code is the value of the expression:
   * (int)(getTimeInMillis()^(getTimeInMillis() >>> 32))
   * @return a hash code value for this object.
   */
  public int hashCode() {
    return (int) (getTimeInMillis() ^ (getTimeInMillis() >>> 32));
  }

  public String hrminsecmsString() {
    String hr = getHr();
    String mn = getMn();
    String sec = getSecond();
    String msec = getMSecond();
    String retStr = hr;
    retStr = retStr.concat(":");
    retStr = retStr.concat(mn);
    retStr = retStr.concat(":");
    retStr = retStr.concat(sec);
    retStr = retStr.concat(":");
    retStr = retStr.concat(msec);
    return retStr;
  }

  public String hrminsecString() {
    String hr = getHr();
    String mn = getMn();
    String sec = getSecond();
    String retStr = hr;
    retStr = retStr.concat(":");
    retStr = retStr.concat(mn);
    retStr = retStr.concat(":");
    retStr = retStr.concat(sec);
    return retStr;
  }

  /**
   * Determines if this IstiEpoch is in the specified time range.
   * @return true is this IstiEpoch lies between the 2 argument times inclusive
   *         of the end times, false otherwise
   */
  public boolean inTimeRange(IstiEpoch start, IstiEpoch end) {
    long stime = start.getTimeInMillis();
    long etime = end.getTimeInMillis();
    long thistime = getTimeInMillis();
    if (thistime >= stime && thistime <= etime) {
      return true;
    }
    return false;
  }

  /**
   * Determines if this IstiEpoch if after given time
   * @return true if this IstiEpoch if after given time, false otherwise.
   */
  public boolean isAfter(double rtime) {
    double thistime = getEpoch();
    if (thistime > rtime) {
      return true;
    }
    return false;
  }

  /**
   * Determines if this IstiEpoch if after given time
   * @return true if this IstiEpoch if after given time, false otherwise
   */
  public boolean isAfter(IstiEpoch refTime) {
    long rtime = refTime.getTimeInMillis();
    long thistime = getTimeInMillis();
    if (thistime > rtime) {
      return true;
    }
    return false;
  }

  /**
   * Determines if this IstiEpoch if after or equal to the given time
   * @return true if this IstiEpoch if after or equal to the given time, false
   *         otherwise
   */
  public boolean isAfterEqual(IstiEpoch refTime) {
    long rtime = refTime.getTimeInMillis();
    long thistime = getTimeInMillis();
    if (thistime >= rtime) {
      return true;
    }
    return false;
  }

  /**
   * Determines if this IstiEpoch is after and not equal to the given time
   * @return true if this IstiEpoch is after and not equal to the given time,
   *         false otherwise
   */
  public boolean isAfterNotEqual(IstiEpoch refTime) {
    long rtime = refTime.getTimeInMillis();
    long thistime = getTimeInMillis();
    if (thistime > rtime) {
      return true;
    }
    return false;
  }

  /**
   * Determines if this IstiEpoch is before the given time
   * @return true if this IstiEpoch if before given time, false otherwise
   */
  public boolean isBefore(double rtime) {
    double thistime = getEpoch();
    if (thistime < rtime) {
      return true;
    }
    return false;
  }

  /**
   * Determines if this IstiEpoch is before the given time
   * @return true if this IstiEpoch if before given time, false otherwise
   */
  public boolean isBefore(IstiEpoch refTime) {
    long rtime = refTime.getTimeInMillis();
    long thistime = getTimeInMillis();
    if (thistime < rtime) {
      return true;
    }
    return false;
  }

  /**
   * Determines if this IstiEpoch is before or equal to the given time
   * @return true if this IstiEpoch if before or equal to the given time, false
   *         otherwise
   */
  public boolean isBeforeEqual(IstiEpoch refTime) {
    long rtime = refTime.getTimeInMillis();
    long thistime = getTimeInMillis();
    if (thistime <= rtime) {
      return true;
    }
    return false;
  }

  /**
   * Determines if this IstiEpoch is before and not equal to the given time
   * @return true if this IstiEpoch if before and not equal to the given time,
   *         false otherwise
   */
  public boolean isBeforeNotEqual(IstiEpoch refTime) {
    long rtime = refTime.getTimeInMillis();
    long thistime = getTimeInMillis();
    if (thistime < rtime) {
      return true;
    }
    return false;
  }

  /**
   * Determines if this IstiEpoch is equal to the given time
   * @return true if this IstiEpoch is equal to the given time, false otherwise
   */
  public boolean isEqualTo(IstiEpoch refTime) {
    long rtime = refTime.getTimeInMillis();
    long thistime = getTimeInMillis();
    if (thistime == rtime) {
      return true;
    }
    return false;
  }

  /**
   * Determines if this IstiEpoch is at an even minute
   * @return true if this IstiEpoch is at an even minute, false otherwise
   */
  public boolean isEvenMinute() {
    int sec = getIntInt(Calendar.SECOND);
    if (sec == 0) {
      return true;
    }
    return false;
  }

  public String ISO8601FormattedString() {
    // String format must be YYYY,DDD,HH:MM:SS.FFFF.
    // 19951231T235959
    String year = getYear();
    int mon = getIntMonth();
    mon++;
    int dom = getIntDOM();
    String mday = Integer.toString(dom);
    DecimalFormat df = new DecimalFormat("00");
    mday = df.format(dom);
    String hr = getHr();
    String mn = getMn();
    String sec = getSecond();
    String retStr = year;
    String smon = Integer.toString(mon);
    smon = df.format(mon);
    retStr = retStr.concat(smon);
    retStr = retStr.concat(mday);
    retStr = retStr.concat("T");
    retStr = retStr.concat(hr);
    retStr = retStr.concat(":");
    retStr = retStr.concat(mn);
    retStr = retStr.concat(":");
    retStr = retStr.concat(sec);
    retStr = retStr.concat(".");
    retStr = retStr.concat(getTenthsMillisecond());
    retStr = retStr.concat("Z");
    return retStr;
  }

  public String labelformattedString() {
    String retStr = yearmondayString();
    return retStr;
  }

  public String longformattedString() {
    String year = getYear();
    String jday = getJDay();
    String hr = getHr();
    String mn = getMn();
    String sec = getSecond();
    String retStr = year;
    retStr = retStr.concat(":");
    retStr = retStr.concat(jday);
    retStr = retStr.concat(":");
    retStr = retStr.concat(hr);
    retStr = retStr.concat(":");
    retStr = retStr.concat(mn);
    retStr = retStr.concat(":");
    retStr = retStr.concat(sec);
    retStr = retStr.concat(".");
    retStr = retStr.concat(getMSecond());
    return retStr;
  }

  public String minsecString() {
    String mn = getMn();
    String sec = getSecond();
    String msec = getMSecond();
    String retStr = mn;
    retStr = retStr.concat(":");
    retStr = retStr.concat(sec);
    if (msec == "") {
      return retStr;
    }
    retStr = retStr.concat(".");
    retStr = retStr.concat(msec);
    return retStr;
  }

  /**
   * This function subtracts the number of seconds from the time
   * @param seconds the number of seconds
   */
  public void minus(double seconds) {
    double curTime = getEpoch();
    curTime -= seconds;
    setTime(curTime);
  }

  public String secString() {
    String sec = getSecond();
    String msec = getMSecond();
    String retStr = sec;
    if (msec == "") {
      return retStr;

    }

    retStr = retStr.concat(".");
    retStr = retStr.concat(msec);
    return retStr;
  }

  /**
   * Set everything but the seconds.
   * @param year the year
   * @param month the month
   * @param day the day of year
   * @param hr the hour of day
   * @param min the minute within the hour
   */
  public void set(int year, int month, int day, int hr, int min) {
    setYear(year);
    setMonth(month);
    setmDay(day);
    setHr(hr);
    setMn(min);
  }

  /**
   * trims the time to the start of the current day (leave time alone)
   */
  public void setDateToTODAY() {
    int hr = getIntHr();
    int min = getIntMn();
    int sec = getIntSec();
    int msec = getMilliSinceSec();
    settoCurrentTime();
    setHr(hr);
    setMn(min);
    setSec(sec);
    setMSecond(msec);
  }

  /**
   * Set the hour of day
   * @param hour the hour of day
   */
  public void setHr(int hour) {
    getConverter().set(this, Calendar.HOUR_OF_DAY, hour);
  }

  /**
   * set time passing in seconds since 1970
   * @param seconds seconds since 1970
   */
  public void setIntTime(double seconds) {
    setTime(seconds);
  }

  /**
   * set time passing in seconds since 1970
   * @param seconds seconds since 1970
   */
  public void setIntTime(int seconds) {
    setIntTime((long) seconds);
  }

  /**
   * set time passing in seconds since 1970
   * @param seconds seconds since 1970
   */
  public void setIntTime(long seconds) {
    // convert seconds to ms
    setTimeInMillis(seconds * 1000L);
  }

  /**
   * Set the day of year
   * @param doy the day of year
   */
  public void setJDay(int doy) {
    getConverter().set(this, Calendar.DAY_OF_YEAR, doy);
  }

  /**
   * Set the day of month
   * @param day the day of month
   */
  public void setmDay(int day) {
    getConverter().set(this, Calendar.DAY_OF_MONTH, day);
  }

  /**
   * Set the minute within the hour
   * @param min the minute within the hour
   */
  public void setMn(int min) {
    getConverter().set(this, Calendar.MINUTE, min);
  }

  /**
   * Set the month
   * @param month the month
   * @see
   */
  public void setMonth(int month) {
    getConverter().set(this, Calendar.MONTH, month);
  }

  /**
   * Set the millisecond within the second
   * @param millisecond the millisecond within the second
   */
  public void setMSecond(int millisecond) {
    getConverter().set(this, Calendar.MILLISECOND, millisecond);
  }

  /**
   * Set the second within the minute
   * @param second the second within the minute
   */
  public void setSec(int second) {
    setSecond(second);
  }

  /**
   * Set the second within the minute
   * @param second the second within the minute
   */
  public void setSecond(int second) {
    getConverter().set(this, Calendar.SECOND, second);
  }

  /**
   * Set time passing in an existing calendar.
   * @param calendar the calendar.
   */
  public final void setTime(Calendar calendar) {
    setTime(calendar.getTime());
  }

  /**
   * Sets the time.
   * @param date the date.
   */
  public final void setTime(Date date) {
    // milliseconds since 1970
    setTimeInMillis(date.getTime());
  }

  /**
   * set time passing in seconds since 1970
   * @param dsecs the seconds since 1970
   */
  public final void setTime(double dsecs) {
    // convert seconds to ms
    setTimeInMillis((long) (dsecs * 1000.));
  }

  /**
   * Set the time
   * @param year the year
   * @param jday the day of year
   * @param hr the hour of day
   * @param min the minute within the hour
   * @param sec the second within the minute
   */
  public void setTime(int year, int jday, int hr, int min, int sec) {
    setYear(year);
    setJDay(jday);
    setHr(hr);
    setMn(min);
    setSec(sec);
  }

  /**
   * Set time passing in another Epoch time.
   * @param epoch the epoch time.
   */
  public final void setTime(IstiEpoch epoch) {
    setTimeInMillis(epoch.getTimeInMillis());
  }

  /**
   * set time passing in milliseconds since 1970.
   * @param msecs milliseconds since 1970.
   */
  public final void setTime(long msecs) {
    // milliseconds since 1970
    setTimeInMillis(msecs);
  }

  /**
   * set time passing in seconds and milliseconds since 1970
   * @param seconds seconds since 1970.
   * @param milliseconds milliseconds since 1970.
   */
  public final void setTime(long seconds, long milliseconds) {
    setTimeInMillis(seconds * 1000L + milliseconds);
  }

  /**
   * set time from RESP format string time. YYYY,DDD,HH:MM:SS
   * @param respTime the RESP format string time
   */
  public final void setTime(String respTime) {
    StringTokenizer st = new StringTokenizer(respTime, ",");
    int count = 0;
    String val;
    while (st.hasMoreTokens()) {
      val = st.nextToken().trim();
      if (count == 0) {
        setYear(Integer.parseInt(val));
      }
      if (count == 1) {
        setJDay(Integer.parseInt(val));
      }
      if (count == 2) {
        // now we have hh:mm:ss
        StringTokenizer stn = new StringTokenizer(val, ":");
        int ct = 0;
        while (stn.hasMoreTokens()) {
          val = stn.nextToken().trim();
          if (ct == 0) {
            // hour.
            setHr(Integer.parseInt(val));
          }
          if (ct == 1) {
            // min.
            setMn(Integer.parseInt(val));
          }
          if (ct == 2) {
            // sec.
            double doubSecs = Double.parseDouble(val);
            setSecond((int) doubSecs);
          }
          ct++;
        }
      }
      count++;
    }
  }

  /**
   * accepts the time in the format YYYY-MM-DDTHH:MM:SS.mmmZ
   * @param ftime String
   */
  public void setTimeAltFissuresTime(String ftime) {
    // reset the time
    setIntTime(0);
    if (ftime.length() >= 4) {
      int year = Integer.parseInt(ftime.substring(0, 4));
      setYear(year);
    }
    if (ftime.length() >= 6) {
      int month = Integer.parseInt(ftime.substring(4, 6));
      month -= 1;
      setMonth(month);
    }
    if (ftime.length() >= 8) {
      int day = Integer.parseInt(ftime.substring(6, 8));
      setmDay(day);
    }
    if (ftime.length() >= 11) {
      int hour = Integer.parseInt(ftime.substring(9, 11));
      setHr(hour);
    }
    if (ftime.length() >= 14) {
      int min = Integer.parseInt(ftime.substring(12, 14));
      setMn(min);
    }

    if (ftime.length() >= 17) {
      int sec = Integer.parseInt(ftime.substring(15, 17));
      setSec(sec);
    }
    if (ftime.length() >= 20) {
      int msec = Integer.parseInt(ftime.substring(18, 20));
      setMSecond(msec);
    }
  }

  /**
   * accepts the time in the format YYYYMMDD.HHMM
   * @param ftime String
   */
  public void setTimeCanadaTime(String ftime) {
    // reset the time
    setIntTime(0);
    if (ftime.length() >= 4) {
      int year = Integer.parseInt(ftime.substring(0, 4));
      setYear(year);
    }
    if (ftime.length() >= 6) {
      int month = Integer.parseInt(ftime.substring(4, 6));
      month -= 1;
      setMonth(month);
    }
    if (ftime.length() >= 8) {
      int day = Integer.parseInt(ftime.substring(6, 8));
      setmDay(day);
    }

    if (ftime.length() >= 11) {
      int hour = Integer.parseInt(ftime.substring(9, 11));
      setHr(hour);
    }
    if (ftime.length() >= 13) {
      int min = Integer.parseInt(ftime.substring(11, 13));
      setMn(min);
    }
  }

  /**
   * accepts the time in the format YYYY-MM-DDTHH:MM:SS.mmmZ
   * @param ftime String
   */
  public void setTimeFissuresTime(String ftime) {
    // reset the time
    StringBuffer sb = new StringBuffer();
    sb.append("IstiEpoch, parsing >" + ftime + "<");
    try {
      setIntTime(0);

      int s = 0;
      int e = 4;
      String ts = "";

      if (ftime.length() >= 4) {

        ts = ftime.substring(s, e);
        sb.append("From " + s + " - " + e + " = " + ts);
        int year = Integer.parseInt(ts);
        setYear(year);
      }
      if (ftime.length() >= 7) {
        s = 5;
        e = 7;
        ts = ftime.substring(s, e);
        sb.append("From " + s + " - " + e + " = " + ts);
        int month = Integer.parseInt(ts);
        month -= 1;
        setMonth(month);
      }
      if (ftime.length() >= 10) {
        s = 8;
        e = 10;
        ts = ftime.substring(s, e);
        sb.append("From " + s + " - " + e + " = " + ts);
        int day = Integer.parseInt(ts);
        setmDay(day);
      }
      if (ftime.length() >= 13) {
        s = 11;
        e = 13;
        ts = ftime.substring(s, e);
        sb.append("From " + s + " - " + e + " = " + ts);

        int hour = Integer.parseInt(ts);
        setHr(hour);
      }
      if (ftime.length() >= 16) {
        s = 14;
        e = 16;
        ts = ftime.substring(s, e);
        sb.append("From " + s + " - " + e + " = " + ts);

        int min = Integer.parseInt(ts);
        setMn(min);
      }
      if (ftime.length() >= 19) {
        s = 17;
        e = 19;
        ts = ftime.substring(s, e);
        sb.append("From " + s + " - " + e + " = " + ts);

        int sec = Integer.parseInt(ts);
        setSec(sec);
      }
    } catch (NumberFormatException ex) {
      sb.append(UtilFns.getStackTraceString(ex));
      message = sb.toString();
      throw ex;
    }
    message = sb.toString();
  }

  /**
   * Sets the current time from the given long value.
   * @param millis the new time in UTC milliseconds from the epoch, January 1,
   *          1970, 00:00:00.
   */
  public final void setTimeInMillis(long millis) {
    // milliseconds since 1970
    _millisecondsSince1970 = millis;
  }

  /**
   * trims the time to the start of the current day
   */
  public void setTimetoDay() {
    int year = getIntYear();
    int mon = getIntMonth();
    int day = getmDay();
    setTimeInMillis(0);
    setYear(year);
    setMonth(mon);
    setmDay(day);
  }

  /**
   * trims the time to the start of the desired day
   * @param epoch the epoch time
   */
  public void setTimetoDay(IstiEpoch epoch) {
    int year = epoch.getIntYear();
    int mon = epoch.getIntMonth();
    int day = epoch.getmDay();
    setTimeInMillis(0);
    setYear(year);
    setMonth(mon);
    setmDay(day);
  }

  /**
   * Set to the current time.
   * @return this epoch time
   */
  public IstiEpoch settoCurrentTime() {
    setTimeInMillis(System.currentTimeMillis());
    return this;
  }

  /**
   * Set the year
   * @param year the year
   */
  public void setYear(int year) {
    getConverter().set(this, Calendar.YEAR, year);
  }

  /**
   * Get a string representation of the epoch
   * @return a string representation of the epoch
   */
  public String toString() {
    String tstr = longformattedString();
    String extra = Double.toString(getEpoch());
    tstr.concat(" - " + extra);
    return tstr;
  }

  public String toWaveviewerRequestString() {
    double tDoub = getEpoch();
    DecimalFormat dForm = new DecimalFormat();
    dForm.setMinimumIntegerDigits(5);
    dForm.setMaximumIntegerDigits(10);
    dForm.setMaximumFractionDigits(4);
    dForm.setMinimumFractionDigits(3);
    dForm.setGroupingUsed(false);
    String tmp = dForm.format(tDoub);
    return tmp;
  }

  public String yeardayString() {
    String year = getYear();
    String jday = getJDay();
    String retStr = year;
    retStr = retStr.concat(":");
    retStr = retStr.concat(jday);
    return retStr;
  }

  public String yearmondayhrminString() {
    String year = getYear();
    String mon = getMonStr();
    String mday = getMDay();
    String hr = getHr();
    String mn = getMn();
    String retStr = year;
    retStr = retStr.concat(" ");
    retStr = retStr.concat(mon);
    retStr = retStr.concat(" ");
    retStr = retStr.concat(mday);
    retStr = retStr.concat(" ");
    retStr = retStr.concat(hr);
    retStr = retStr.concat(":");
    retStr = retStr.concat(mn);
    return retStr;
  }

  public String yearmondayString() {
    String year = getYear();
    String mon = getMonStr();
    String mday = getMDay();
    String retStr = year;
    retStr = retStr.concat(" ");
    retStr = retStr.concat(mon);
    retStr = retStr.concat(" ");
    retStr = retStr.concat(mday);
    return retStr;
  }

  static public String[] getFormattedStrings() {
    return formatType;
  }

  static public String getFormatType(int type) {
    return formatType[type];
  }

  public static void main(String[] args) {
    System.out.println();
    IstiEpoch ie = new IstiEpoch();
    ie.setTime(10);
    System.out.println("IstiEpoch: " + ie);
    ie.setTime(1000);
    System.out.println("IstiEpoch: " + ie);
    ie.setTime(100000);
    System.out.println("IstiEpoch: " + ie);

    IstiEpoch time0 = new IstiEpoch("2003,111,01:10:01");
    // YYYY-MM-DDTHH:MM:SS.mmmZ
    time0.setTimeFissuresTime("1965-01-27T12:09:10.033Z");
    IstiEpoch time1 = new IstiEpoch();
    IstiEpoch time2 = new IstiEpoch();

    System.out.println();
    System.out.println("time0 is " + time0);
    System.out.println("time1 is " + time1);
    System.out.println("time2 is " + time2);

    Date date0 = time0.getTime();
    Date date1 = time1.getTime();
    Date date2 = time2.getTime();
    int epochComp;
    int dateComp;

    System.out.println();
    System.out.println("time1 equals time2: " + time1.equals(time2));
    System.out.println("time1 compare time1 to time2: "
        + time1.compare(time1, time2));
    System.out.println("time1 compareTo time2: "
        + (epochComp = time1.compareTo(time2)));
    dateComp = date1.compareTo(date2);
    if (epochComp != dateComp) {
      System.out.println("Epoch compare " + epochComp
          + " is not equal to date compare " + dateComp);
    }

    System.out.println();
    System.out.println("time0 equals time2: " + time0.equals(time2));
    System.out.println("time0 compare time0 to time2: "
        + time0.compare(time0, time2));
    System.out.println("time0 compareTo time2: "
        + (epochComp = time0.compareTo(time2)));
    dateComp = date0.compareTo(date2);
    if (epochComp != dateComp) {
      System.out.println("Epoch compare " + epochComp
          + " is not equal to date compare " + dateComp);
    }

    System.out.println();
    System.out.println("time1 equals time0: " + time1.equals(time0));
    System.out.println("time1 compare time1 to time0: "
        + time1.compare(time1, time0));
    System.out.println("time1 compareTo time0: "
        + (epochComp = time1.compareTo(time0)));
    dateComp = date1.compareTo(date0);
    if (epochComp != dateComp) {
      System.out.println("Epoch compare " + epochComp
          + " is not equal to date compare " + dateComp);
    }
  }

  /**
   * Get a string representation for the time
   * @param seconds the seconds from the Epoch time
   * @return a string representation for the time
   */
  public static String toString(double seconds) {
    IstiEpoch epoch = new IstiEpoch(seconds);
    String tstr = epoch.longformattedString();
    return tstr;
  }
}
