/*--- formatted by Jindent 2.1, (www.c-lab.de/~jindent) ---*/

//
// =====================================================================
// Copyright (C) 2000 Instrumental Software Technologies, Inc.
//
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions
// are met:
// 1. Redistributions of source code, or portions of this source code,
// must retain the above copyright notice, this list of conditions
// and the following disclaimer.
// 2. Redistributions in binary form must reproduce the above copyright
// notice, this list of conditions and the following disclaimer in
// the documentation and/or other materials provided with the
// distribution.
// 3. All advertising materials mentioning features or use of this
// software must display the following acknowledgment:
// "This product includes software developed by Instrumental
// Software Technologies, Inc. (http://www.isti.com)"
// 4. If the software is provided with, or as part of a commercial
// product, or is used in other commercial software products the
// customer must be informed that "This product includes software
// developed by Instrumental Software Technologies, Inc.
// (http://www.isti.com)"
// 5. The names "Instrumental Software Technologies, Inc." and "ISTI"
// must not be used to endorse or promote products derived from
// this software without prior written permission. For written
// permission, please contact "info@isti.com".
// 6. Products derived from this software may not be called "ISTI"
// nor may "ISTI" appear in their names without prior written
// permission of Instrumental Software Technologies, Inc.
// 7. Redistributions of any form whatsoever must retain the following
// acknowledgment:
// "This product includes software developed by Instrumental
// Software Technologies, Inc. (http://www.isti.com/)."
// THIS SOFTWARE IS PROVIDED BY INSTRUMENTAL SOFTWARE
// TECHNOLOGIES, INC. "AS IS" AND ANY EXPRESSED OR IMPLIED
// WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES
// OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
// DISCLAIMED.  IN NO EVENT SHALL INSTRUMENTAL SOFTWARE TECHNOLOGIES,
// INC. OR ITS CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
// INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
// (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
// SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
// HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
// STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
// ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED
// OF THE POSSIBILITY OF SUCH DAMAGE.
// =====================================================================
// A current version of the software can be found at
// http://www.isti.com
// Bug reports and comments should be directed to
// Instrumental Software Technologies, Inc. at info@isti.com
// =====================================================================
//
package com.isti.util;

import java.util.Comparator;


/**
 * IstiEpoch is a class that is used to help convert from IstiEpochs (either
 * longs or doubles) into GregorianCalendar
 *
 * @author Instrumental Software Technologies, Inc. (<a href="http://www.isti.com">ISTI</a>)
 * @version 1.0.232
 */
public class IstiEpochComparator implements Comparator
{

	IstiEpoch       first = null;
	IstiEpoch       second = null;

	/*
	 * public IstiEpochComparator () {
	 *
	 * }
	 */

	/**
	 * Method declaration
	 *
	 *
	 * @param o1
	 * @param o2
	 *
	 * @return
	 *
	 * @see
	 */
	public int compare(Object o1, Object o2)
	{

            if (o1 == null) {
                System.err.println("o1 is null");
            }
            if (o2 == null) {
                System.err.println("o2 is null");
            }

		first = (IstiEpoch) o1;
		second = (IstiEpoch) o2;

		if (first.equals(second))
		{
			return 0;

		}

		if (first.isBefore(second))
		{
			return -1;

		}

		if (first.isAfter(second))
		{
			return 1;

		}

		return 0;

	}


}

