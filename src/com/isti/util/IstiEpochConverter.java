package com.isti.util;

/**
 * <p>Title: </p>
 * <p>Description: </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: </p>
 * @author unascribed
 * @version 1.0
 */

import java.util.GregorianCalendar;
import java.util.Date;

public class IstiEpochConverter {


  private final GregorianCalendar gregC;
  private final Date mainDate;

  public IstiEpochConverter() {

    gregC = new GregorianCalendar(UtilFns.GMT_TIME_ZONE_OBJ);
    mainDate = new Date();

  }
/*
  static private IstiEpochConverter instance = null;

  static public IstiEpochConverter getInstance() {


    if(instance == null)
      instance = new IstiEpochConverter();

    return instance;

  }
*/
  public GregorianCalendar getGregorianCalendar()
  {
    return gregC;
  }

  public GregorianCalendar getGregorianCalendar(IstiEpoch epoch)
  {
    setTime(epoch);
    return gregC;
  }


  public Date getTime() {
    return mainDate;
  }

  public Date getTime(IstiEpoch epoch) {
    setTime(epoch);
    return mainDate;
  }

  public void setTime(IstiEpoch epoch) {

    mainDate.setTime(epoch.getTimeInMillis());
    gregC.setTime(mainDate);

  }

  public void set(IstiEpoch time, int calVal, int val) {
    setTime(time);
    gregC.set(calVal, val);
    time.setTime(gregC.getTime());
  }

  public int get(int calVal) {

    return gregC.get(calVal);

  }

  public int get(IstiEpoch time, int calVal) {

    setTime(time);
    return gregC.get(calVal);

  }

}
