//IstiEventListener.java:  Defines an event listener.
//
//   6/18/2009 -- [KF]  Initial version.
//

package com.isti.util;

import java.util.EventListener;
import java.util.EventObject;

/**
 * Interface IstiEventListener defines an event listener.
 */
public interface IstiEventListener extends EventListener {
  /**
   * This notification tells listeners an event has occurred.
   * @param e the event object.
   */
  public void notifyEvent(EventObject e);
}
