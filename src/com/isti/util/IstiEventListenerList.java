//IstiEventListenerList.java:  Defines an event listener list.
//
//   6/18/2009 -- [KF]  Initial version.
//

package com.isti.util;

import java.util.ArrayList;
import java.util.EventObject;

/**
 * Class IstiEventListenerList defines an event listener list.
 */
public class IstiEventListenerList {
  /** The event listener list. */
  private final ArrayList eventListenerList = new ArrayList();

  /**
   * Adds the event listener.
   * @param l the event listener.
   */
  public void add(IstiEventListener l) {
    if (!eventListenerList.contains(l)) { //if not already in the list
      eventListenerList.add(l);
    }
  }

  /**
   * Removes all of the event listeners.
   */
  public void clear() {
    eventListenerList.clear();
  }

  /**
   * Get the event listener for the specified index.
   * @param index the index.
   * @return the event listener.
   */
  public IstiEventListener getEventListener(int index) {
    return ((IstiEventListener) eventListenerList.get(index));
  }

  /**
   * This notification tells listeners an event has occurred.
   * @param e the event object.
   */
  public void notifyEvent(EventObject e) {
    for (int index = 0; index < size(); index++) {
      getEventListener(index).notifyEvent(e);
    }
  }

  /**
   * Removes the event listener.
   * @param l the event listener.
   */
  public void remove(IstiEventListener l) {
    eventListenerList.remove(l);
  }

  /**
   * Gets the number of event listeners.
   * @return the number of event listeners.
   */
  public int size() {
    return eventListenerList.size();
  }
}
