//IstiFileFilter.java:  A convenience implementation of FileFilter that
//                      filters out all files except for those type files
//                      that it knows about.
//
//   6/4/2002 -- [KF]  Initial version.
//  7/19/2002 -- [ET]  Class moved to isti.util.gui and name changed.
//  4/22/2003 -- [KF]  Move class to isti.util since it may be used for non-gui,
//                     use Vector instead of Hashtable for filters,
//                     add 'isAcceptingDirectories' and 'setAcceptDirctories'
//                     methods for selection of always accepting directories,
//                     use classes for handling various types of filters,
//                     add support for wildcard filter.
//  3/22/2005 -- [KF]  Changed 'getDescription' to ensure iterator has elements.
// 10/30/2006 -- [ET]  Fixed spelling of 'setAcceptDirectories()' method.
//

package com.isti.util;

import java.io.File;
import java.util.Vector;
import java.util.Iterator;
import java.util.StringTokenizer;

/**
 * A convenience implementation of FileFilter that filters out
 * all files except for those type files that it knows about.
 *
 * Case is ignored.
 *
 * Example - create a new filter that filters out all files
 * but gif and jpg image files:
 *
 *     JFileChooser chooser = new JFileChooser();
 *     IstiFileFilter filter = new IstiFileFilter(
 *                   new String[]{"*.gif", "*.jpg"}, "JPEG & GIF Images")
 *     chooser.addChoosableFileFilter(filter);
 *     chooser.showOpenDialog(this);
 */
public class IstiFileFilter extends javax.swing.filechooser.FileFilter
    implements java.io.FileFilter
{
  protected static final boolean DEBUG_FLAG = false;  //true for debug msgs
  private final Vector filterList = new Vector();
  private String description = null;
  private String fullDescription = null;
  private boolean acceptDirectoriesFlag = true;
  private boolean useFiltersInDescription = true;

  protected final static String WC_STRING = "*";  //wildcard string

  /**
   * Creates a file filter. If no filters are added, then all
   * files are accepted.
   *
   * @see #addFilter
   * @see java.io.FileFilter
   *
   * By default all directories are accepted to allow directory traversal
   * with file choosers.
   * @see isAcceptingDirectories
   * @see setAcceptDirectories
   */
  public IstiFileFilter()
  {
  }

  /**
   * Creates a file filter that accepts the given file type.
   * Example: new IstiFileFilter("*.jpg");
   *
   * @param filter filter file string
   *
   * @see #addFilter
   * @see java.io.FileFilter
   * @see createFileFilter
   *
   * By default directories are not accepted for use with FileUtils.listFiles.
   * @see FileUtils.listFiles
   * @see isAcceptingDirectories
   * @see setAcceptDirectories
   *
   * If a simple "java.io.FileFilter" is needed the 'createFileFilter' method
   * may be used.
   * @see createFileFilter
   */
  public IstiFileFilter(String filter)
  {
    this();
    if(filter!=null) addFilter(filter);
    setAcceptDirectories(false);
  }

  /**
   * Creates a file filter that accepts the given file type.
   * Example: new IstiFileFilter("*.jpg", "JPEG Image Images");
   *
   * @param filter filter file string.
   * @param description file description, or null for none.
   *
   * @see #addFilter
   * @see java.io.FileFilter
   * @see createFileFilter
   *
   * By default all directories are accepted to allow directory traversal
   * with file choosers.
   * @see isAcceptingDirectories
   * @see setAcceptDirectories
   */
  public IstiFileFilter(String filter, String description)
  {
    this();
    if(filter != null)
      addFilter(filter);
    if(description != null)
      setDescription(description);
  }

  /**
   * Creates a file filter from the given string array and description.
   * Example: new IstiFileFilter(String {"*.gif", "*.jpg"}, "Gif and JPG Images");
   *
   * @param filters array of filter file string.
   * @param description file description, or null for none.
   *
   * @see #addFilter
   */
  public IstiFileFilter(String[] filters, String description)
  {
    this();
    for (int i = 0; i < filters.length; i++)
    {
      // add filters one by one
      addFilter(filters[i]);
    }
    if(description != null)
      setDescription(description);
  }

  /**
   * Determines if directories should always be accepted.
   * @return true if always accepting directories.
   */
  public boolean isAcceptingDirectories()
  {
    return acceptDirectoriesFlag;
  }

  /**
   * Select if directories should always be accepted.
   * @param b true if always accepting directories
   */
  public void setAcceptDirectories(boolean b)
  {
    acceptDirectoriesFlag = b;
  }

  /**
   * Determines if this file should be shown in the directory pane.
   * @param f file
   * @return true if this file should be shown in the directory pane,
   * false if it shouldn't.
   *
   * @see #getFilter
   * @see FileFilter#accepts
   */
  public boolean accept(File f)
  {
    if (f != null)  //if file specified
    {
      //if accepting directories and this is a directory
      if (acceptDirectoriesFlag && f.isDirectory())
      {
        return true;
      }
      if (f != null)
      {
        // go through the filters
        for (Iterator filters = filterList.iterator();
             filters.hasNext() ; )
        {
          java.io.FileFilter filter = (java.io.FileFilter)filters.next();
          if (filter.accept(f))  //if accepted by this filter
            return true;
        }
      }
    }
    return false;
  }

  /**
   * Gets the extension portion of the string.
   * @param s file name string
   * @return the extension portion of the string.
   *
   * @see FileFilter#accept
   */
  public static String getExtension(String s)
  {
    int i = s.lastIndexOf('.');
    if (i > 0 && i < s.length()-1)
    {
      return s.substring(i+1).toLowerCase();
    }
    return null;
  }

  /**
   * Gets the prefix portion of the string.
   * @param s file name string
   * @return the prefix portion of the string.
   *
   * @see FileFilter#accept
   */
  public static String getPrefix(String s)
  {
    int i = s.indexOf('.');
    if (i > 0 && i < s.length()-1)
    {
      return s.substring(0, i).toLowerCase();
    }
    return null;
  }

  /**
   * Adds a filetype filter to filter against.
   * @param filter filter file string
   *
   * For example: the following code will create a filter that filters
   * out all files except those that end in "jpg" and "tif":
   *
   *   IstiFileFilter filter = new IstiFileFilter();
   *   filter.addFilter("*.jpg");
   *   filter.addFilter("*.tif");
   */
  public void addFilter(String filter)
  {
    filterList.add(createFileFilter(filter));
    fullDescription = null;
  }


  /**
   * Returns the human readable description of this filter. For
   * example: "JPEG and GIF Image Files (*.jpg, *.gif)"
   * @return the human readable description of this filter
   *
   * @see setDescription
   * @see setFilterListInDescription
   * @see isFilterListInDescription
   * @see FileFilter#getDescription
   */
  public String getDescription()
  {
    if(fullDescription == null)
    {
      if(description == null || isFilterListInDescription())
      {
        fullDescription = description==null ? "(" : description + " (";
        // build the description from the filter list
        // go through the filters
        for (Iterator filters = filterList.iterator();
             filters.hasNext() ; )
        {
          java.io.FileFilter filter = (java.io.FileFilter)filters.next();
          fullDescription += filter.toString();
          while (filters.hasNext())
          {
            filter = (java.io.FileFilter)filters.next();
            fullDescription += ", " + filter.toString();
          }
        }
        fullDescription += ")";
      }
      else
      {
        fullDescription = description;
      }
    }
    return fullDescription;
  }

  /**
   * Sets the human readable description of this filter.
   * @param description file description.
   *
   * For example: filter.setDescription("Gif and JPG Images");
   *
   * @see setDescription
   * @see setFilterListInDescription
   * @see isFilterListInDescription
   */
  public void setDescription(String description)
  {
    this.description = description;
    fullDescription = null;
  }

  /**
   * Determines whether the filter list (*.jpg, *.gif, etc) should
   * show up in the human readable description.
   * @param b true if the filter list should show up.
   *
   * Only relevent if a description was provided in the constructor
   * or using setDescription();
   *
   * @see getDescription
   * @see setDescription
   * @see isFilterListInDescription
   */
  public void setFilterListInDescription(boolean b)
  {
    useFiltersInDescription = b;
    fullDescription = null;
  }

  /**
   * Determines whether the filter list (*.jpg, *.gif, etc) should
   * show up in the human readable description.
   * @return true if the filter list (*.jpg, *.gif, etc) should
   * show up in the human readable description, false otherwise.
   *
   * Only relevent if a description was provided in the constructor
   * or using setDescription();
   *
   * @see getDescription
   * @see setDescription
   * @see setFilterListInDescription
   */
  public boolean isFilterListInDescription()
  {
    return useFiltersInDescription;
  }

  /**
   * Determines if the filter has wildcards.
   * @param filter filter file string
   * @return true if the filter has wildcards.
   */
  protected static boolean hasWildcards(String filter)
  {
    final int wc1 = filter.indexOf(WC_STRING);
    if (wc1 >= 0)
    {
      return true;
    }
    return false;
  }

  /**
   * Determines if the filter has multiple wildcards.
   * @param filter filter file string
   * @return true if the filter has multiple wildcards.
   */
  protected static boolean hasMultipleWildcards(String filter)
  {
    final int wc1 = filter.indexOf(WC_STRING);
    if (wc1 >= 0)
    {
      final int wc2 = filter.lastIndexOf(WC_STRING);
      if (wc2 >= 0 && wc2 != wc1)
        return true;
    }
    return false;
  }

  /**
   * Create the file filter for the specified filter string.
   *
   * @param filter filter file string ("*.txt", "temp.*", "temp.txt", "*bak*")
   *
   * @return the file filter.
   */
  public static java.io.FileFilter createFileFilter(String filter)
  {
    final java.io.FileFilter fileFilter;

    filter = filter.toLowerCase();  //convert to lower case

    if (hasWildcards(filter))  //if filter has any wildcards
    {
      if (hasMultipleWildcards(filter))  //multiple wildcard filter
      {
        fileFilter = new WildcardFilter(filter);
      }
      else if (filter.endsWith(".*"))  // prefix filter
      {
        fileFilter = new PrefixFileFilter(filter);
      }
      else if (filter.startsWith("*."))  // extension filter
      {
        fileFilter = new ExtensionFileFilter(filter);
      }
      else
      {
        fileFilter = new WildcardFilter(filter);
      }
    }
    else  // specific file filter
    {
      fileFilter = new SpecificFileFilter(filter);
    }
    return fileFilter;
  }
}


/**
 * Abstract file filter.
 */
abstract class AbstractFileFilter implements java.io.FileFilter
{
  protected final String filter;  //filter file string

  /**
   * Constructs an abstruct file filter.
   * @param filter filter file string
   */
  public AbstractFileFilter(String filter)
  {
    this.filter = filter;
  }

  /**
   * Tests whether or not the specified abstract pathname should be
   * included in a pathname list.
   *
   * @param  pathname  The abstract pathname to be tested
   * @return  <code>true</code> if and only if <code>pathname</code>
   *          should be included
   */
  public boolean accept(File pathname)
  {
    boolean accepted = false;

    if (pathname != null)
    {
      String filename = pathname.getName().toLowerCase();
      accepted = accept(filename);
    }
    return accepted;
  }

  /**
   * Tests whether or not the specified abstract pathname should be
   * included in a pathname list.
   *
   * @param  filename  The filename to be tested
   * @return  <code>true</code> if and only if <code>pathname</code>
   *          should be included
   */
  abstract public boolean accept(String filename);

  /**
   * Gets a string representation of the object.
   * @return a string representation of the object.
   */
  public String toString()
  {
    return filter;
  }
}

/**
 * Extension file filter.
 */
class ExtensionFileFilter extends AbstractFileFilter
{
  private final String fileExtension;  //file extension

  /**
   * Constructs a extension file filter.
   * @param filter filter file string
   */
  public ExtensionFileFilter(String filter)
  {
    super(filter);
    this.fileExtension = IstiFileFilter.getExtension(filter);
  }

  /**
   * Tests whether or not the specified abstract pathname should be
   * included in a pathname list.
   *
   * @param  filename  The filename to be tested
   * @return  <code>true</code> if and only if <code>pathname</code>
   *          should be included
   */
  public boolean accept(String filename)
  {
    String extension = IstiFileFilter.getExtension(filename);
    if (extension != null && extension.equals(fileExtension))
    {
      return true;
    }
    return false;
  }
}

/**
 * Prefix file filter.
 */
class PrefixFileFilter extends AbstractFileFilter
{
  private final String filePrefix;  //file prefix

  /**
   * Constructs a prefix file filter.
   * @param filter filter file string
   */
  public PrefixFileFilter(String filter)
  {
    super(filter);
    this.filePrefix = IstiFileFilter.getPrefix(filter);
  }

  /**
   * Tests whether or not the specified abstract pathname should be
   * included in a pathname list.
   *
   * @param  filename  The filename to be tested
   * @return  <code>true</code> if and only if <code>pathname</code>
   *          should be included
   */
  public boolean accept(String filename)
  {
    String prefix = IstiFileFilter.getPrefix(filename);
    if (prefix != null && prefix.equals(filePrefix))
    {
      return true;
    }
    return false;
  }
}

/**
 * Single file filter.
 */
class SpecificFileFilter extends AbstractFileFilter
{
  private String fileNameStr;  //file name string

  /**
   * Constructs a single file filter.
   * @param filter filter file string
   */
  public SpecificFileFilter(String filter)
  {
    super(filter);
    fileNameStr = new File(filter).getName();
  }

  /**
   * Tests whether or not the specified abstract pathname should be
   * included in a pathname list.
   *
   * @param  filename  The filename to be tested
   * @return  <code>true</code> if and only if <code>pathname</code>
   *          should be included
   */
  public boolean accept(String filename)
  {
    if (fileNameStr.equals(filename))
    {
      return true;
    }
    return false;
  }
}

/**
 * Wildcard file filter.
 */
class WildcardFilter extends AbstractFileFilter
{
  private String fileNameStr;  //file name string

  /**
   * Constructs a wildcard file filter.
   * @param filter filter file string
   */
  public WildcardFilter(String filter)
  {
    super(filter);
    fileNameStr = new File(filter).getName();
  }

  /**
   * Tests whether or not the specified abstract pathname should be
   * included in a pathname list.
   *
   * @param  filename  The filename to be tested
   * @return  <code>true</code> if and only if <code>pathname</code>
   *          should be included
   */
  public boolean accept(String filename)
  {
    int idx = 0;
    boolean wildCard = false;
    final StringTokenizer tokens =
        new StringTokenizer(fileNameStr, IstiFileFilter.WC_STRING, true);

    while (tokens.hasMoreTokens())  //while there are more tokens
    {
      //get the next token
      final String token = tokens.nextToken().toLowerCase();

      if (token.equals(IstiFileFilter.WC_STRING))  //if token is wildcard
      {
        wildCard = true;  //set the wildcard flag
      }
      else
      {
        final int nextIdx = filename.indexOf(token, idx);  //find the token
        if (wildCard)  //if last token was wildcard
        {
          wildCard = false;  //clear the wildcard flag
          if (nextIdx < 0)  //if the token wasn't found
            break;  //we are done
          idx = nextIdx + token.length();  //set idx past this token
        }
        else if (nextIdx == idx)  //token at current index
          idx += token.length();  //set idx past this token
        else
          break;  //we are done
      }

      //if all tokens found and end with wildcard or end of string
      if (!tokens.hasMoreTokens() && (wildCard || idx == filename.length()))
      {
        return true;
      }
      if (IstiFileFilter.DEBUG_FLAG)
      {
        System.out.println("skipping: " + filename);
      }
    }
    return false;
  }
}
