//IstiLocale.java:  Extends a Locale to add additional functionality.
//
//    1/6/2006 -- [KF]  Initial version.
//

package com.isti.util;

import java.util.Locale;
import java.util.Date;
import java.util.Vector;
import java.util.Arrays;
import java.text.DecimalFormat;

/**
 * Class IstiLocale extends a Locale to add additional functionality.
 */
public class IstiLocale implements Archivable
{
  //static data

  /**
   * The display name for the system default locale.
   */
  public static String DEFAULT_DISPLAY_NAME = "Default";

  //private data

  /** The archive date. */
  private Date _archiveDate = null;
  /** The 'Locale' object. */
  private final Locale _localeObj;
  /** The locale display name. */
  private final String _displayName;


  /**
   * Allocates a new <code>IstiLocale</code> object.
   * @param istiLocaleObj <code>IstiLocale</code> object.
   */
  public IstiLocale(IstiLocale istiLocaleObj)
  {
    _archiveDate = istiLocaleObj._archiveDate;  //save the archive date
    _displayName = istiLocaleObj._displayName;  //save the display name
    _localeObj = istiLocaleObj._localeObj;  //save the locale object
  }

  /**
   * Allocates a new <code>IstiLocale</code> object.
   * @param localeObj <code>Locale</code> object.
   */
  public IstiLocale(Locale localeObj)
  {
    _localeObj = localeObj; //save the locale object
    if (localeObj == null)  //if no locale
      _displayName = DEFAULT_DISPLAY_NAME;  //use the default
    else
      _displayName = getDisplayName(localeObj, UtilFns.EMPTY_STRING);
  }

  /**
   * Allocates a new <code>IstiLocale</code> object.
   * @param dataStr the data string.
   * @param mkrObj the marker object.
   */
  public IstiLocale(String dataStr, Archivable.Marker mkrObj)
  {
    if (dataStr == null || DEFAULT_DISPLAY_NAME.equals(dataStr))
    {
      _displayName = DEFAULT_DISPLAY_NAME;
      _localeObj = null;
    }
    else
    {
      _localeObj = getLocaleFromDataString(dataStr);
      _displayName = getDisplayName(_localeObj, UtilFns.EMPTY_STRING);
    }
  }

  /**
   * Allocates a new <code>IstiLocale</code> object.
   * @param displayName the locale display name.
   */
  public IstiLocale(String displayName)
  {
    if (displayName == null || DEFAULT_DISPLAY_NAME.equals(displayName))
    {
      _displayName = DEFAULT_DISPLAY_NAME;
      _localeObj = null;
    }
    else
    {
      _displayName = displayName;
      _localeObj = getLocaleFromDisplayName(displayName);
    }
  }

  /**
   * Creates and returns a copy of this object.
   * @return a copy of this object.
   */
  public Object clone()
  {
    return new IstiLocale(this);
  }

  /**
   * Indicates whether some other object is "equal to" this one.
   * @param   obj   the reference object with which to compare.
   * @return  <code>true</code> if this object is the same as the obj
   *          argument; <code>false</code> otherwise.
   */
  public boolean equals(IstiLocale obj)
  {
    return getDisplayName().equals(obj.getDisplayName());
  }

  /**
   * Indicates whether some other object is "equal to" this one.
   * @param   obj   the reference object with which to compare.
   * @return  <code>true</code> if this object is the same as the obj
   *          argument; <code>false</code> otherwise.
   */
  public boolean equals(Locale obj)
  {
    return getLocale().equals(obj);
  }

  /**
   * Indicates whether some other object is "equal to" this one.
   * @param   obj   the reference object with which to compare.
   * @return  <code>true</code> if this object is the same as the obj
   *          argument; <code>false</code> otherwise.
   */
  public boolean equals(Object obj)
  {
    if (obj instanceof IstiLocale)
      return equals((IstiLocale)obj);
    if (obj instanceof Locale)
      return equals((Locale)obj);
    if (obj instanceof String)
      return getDisplayName().equals(obj);
    return false;
  }

  /**
   * Returns a 'Date' object representing the date to be used for archival
   * purposes.
   * @return A 'Date' object representing the date that the item should be
   * archived under.
   */
  public Date getArchiveDate()
  {
    if (_archiveDate == null)
      return new Date();
    return _archiveDate;
  }

  /**
   * The set of Locales for which number formats are installed.
   * @return the set of Locales for which number formats are installed.
   */
  public static Locale[] getAvailableNumberLocales()
  {
    return DecimalFormat.getAvailableLocales();
  }

  /**
   * Gets the display name.
   * @return the display name.
   */
  public String getDisplayName()
  {
    return _displayName;
  }

  /**
   * Gets the display name for the specified locale.
   * @param localeObj the locale object.
   * @return the the display name or null if not available.
   */
  public static String getDisplayName(Locale localeObj)
  {
    return getDisplayName(localeObj, null);
  }

  /**
   * Gets the display name for the specified locale.
   * @param localeObj the locale object.
   * @param defaultDisplayName the default display name.
   * @return the the display name or the default display name if not available.
   */
  public static String getDisplayName(
      Locale localeObj, String defaultDisplayName)
  {
    final String displayCountry = localeObj.getDisplayCountry();
    //if the locale has a country
    if (displayCountry != null && displayCountry.length() > 0)
    {
      return localeObj.getDisplayName();
    }
    return defaultDisplayName;
  }

  /**
   * Gets the 'Locale' object.
   * @return the 'Locale' object.
   */
  public Locale getLocale()
  {
    if (_localeObj == null)
      return UtilFns.getDefaultLocale();
    return _localeObj;
  }

  /**
   * Gets the locale for the specified data string.
   * @param dataStr the data string.
   * @return the 'Locale' or null if not found.
   */
  public static Locale getLocaleFromDataString(String dataStr)
  {
    if (dataStr == null || dataStr.length() <= 0 |
        dataStr.equals(DEFAULT_DISPLAY_NAME))
      return UtilFns.getSystemLocale();
    final Locale[] locales = getAvailableNumberLocales();
    final int numLocales = locales.length;
    String localeDataStr;
    for (int i = 0; i < numLocales; i++)
    {
      localeDataStr = locales[i].toString();
      if (localeDataStr == null)  //skip if no display string
        continue;
      if (dataStr.equals(localeDataStr))
        return locales[i];
    }
    return null;
  }

  /**
   * Gets the locale for the specified display name.
   * @param displayName the locale display name.
   * @return the 'Locale' or null if not found.
   */
  public static Locale getLocaleFromDisplayName(String displayName)
  {
    if (displayName == null || displayName.length() <= 0 |
        displayName.equals(DEFAULT_DISPLAY_NAME))
      return UtilFns.getSystemLocale();
    final Locale[] locales = getAvailableNumberLocales();
    final int numLocales = locales.length;
    String localeDisplayName;
    for (int i = 0; i < numLocales; i++)
    {
      localeDisplayName = getDisplayName(locales[i]);
      if (localeDisplayName == null)  //skip if no display name
        continue;
      if (displayName.equals(localeDisplayName))
        return locales[i];
    }
    return null;
  }

  /**
   * Gets the configuration property validator.
   * @return the 'CfgPropValidator'.
   */
  public static CfgPropValidator getCfgPropValidator()
  {
    //get the locale display names and sort them
    // and add DEFAULT_LOCALE_DISPLAY_NAME to the top
    final Locale[] locales = getAvailableNumberLocales();
    final int numLocales = locales.length;
    final Vector localeDisplayNamesVector = new VectorSet();
    localeDisplayNamesVector.add(DEFAULT_DISPLAY_NAME);
    String localeDisplayName;
    for (int i = 0; i < numLocales; i++)
    {
      localeDisplayName = getDisplayName(locales[i]);
      if (localeDisplayName != null)  //if display name is available
        localeDisplayNamesVector.add(localeDisplayName);
    }
    final String[] displayNames = (String[])localeDisplayNamesVector.toArray(
        new String[localeDisplayNamesVector.size()]);
    Arrays.sort(displayNames,1,displayNames.length);

    return new CfgPropValidator(displayNames);
  }

  /**
   * Returns a hash code value for the object. This method is
   * supported for the benefit of hashtables such as those provided by
   * <code>java.util.Hashtable</code>.
   * @return  a hash code value for this object.
   */
  public int hashCode()
  {
    return getLocale().hashCode();
  }

  /**
   * Sets the 'Date' object representing the date to be used for archival
   * purposes.
   * @param dateObj the archive date.
   */
  public void setArchiveDate(Date dateObj)
  {
    _archiveDate = dateObj;
  }

  /**
   * Returns the archivable representation for this object.  When the
   * object is recreated from the string, it should be identical to the
   * original.
   * @return A String representing the archived form of the object.
   */
  public String toArchivedForm()
  {
    if (DEFAULT_DISPLAY_NAME.equals(getDisplayName()))
      return DEFAULT_DISPLAY_NAME;
    return getLocale().toString();
  }

  /**
   * Returns a string representation of the object.
   * @return  a string representation of the object.
   */
  public String toString()
  {
    return getDisplayName();
  }
}
