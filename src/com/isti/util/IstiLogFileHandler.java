//IstiLogFileHandler.java:  Java logging Handler that publishes its messages
//                          into an ISTI LogFile object.
//
//  5/21/2013 -- [ET]
//   2/9/2018 -- [KF]  Added exception information if available.
//

package com.isti.util;

import java.util.Collections;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Map.Entry;
import java.util.logging.Handler;
import java.util.logging.Level;
import java.util.logging.LogRecord;

import com.isti.util.LogFile;

/**
 * Class IstiLogFileHandler is a Java logging Handler that publishes its
 * messages into an ISTI LogFile object.  Logging levels are converted.
 */
public class IstiLogFileHandler extends Handler
{
    /** Target ISTI LogFile object. */
  protected final LogFile istiLogObj;
    /** Translation table, Java Logger levels to ISTI LogFile levels. */
  protected static final Map<Level, Integer> levelXlatTable;
  static
  {
	final Map map = new HashMap<Level, Integer>(9);
    try       //fill translation table:
    {
    	map.put(Level.OFF, Integer.valueOf(LogFile.NO_MSGS));
    	map.put(Level.SEVERE, Integer.valueOf(LogFile.ERROR));
    	map.put(Level.WARNING, Integer.valueOf(LogFile.WARNING));
    	map.put(Level.INFO, Integer.valueOf(LogFile.INFO));
    	map.put(Level.CONFIG, Integer.valueOf(LogFile.INFO));
    	map.put(Level.FINE, Integer.valueOf(LogFile.DEBUG));
    	map.put(Level.FINER, Integer.valueOf(LogFile.DEBUG2));
    	map.put(Level.FINEST, Integer.valueOf(LogFile.DEBUG3));
    	map.put(Level.ALL, Integer.valueOf(LogFile.ALL_MSGS));
    }
    catch(Throwable ex)
    {  //unexpected error; report to console but otherwise move on
      System.err.println(
                     "IstiLogFileHandler error filling xlat table:  " + ex);
    }
    levelXlatTable = Collections.unmodifiableMap(map);
  }
  
  /**
   * Creates a Java logging Handler that publishes its messages into an
   * ISTI LogFile object.
   * @param lObj target ISTI LogFile object.
   */
  public IstiLogFileHandler(LogFile lObj)
  {
    istiLogObj = lObj;
    final Level levelObj;    //set handler log level to LogFile level
    if((levelObj=levelFromIstiLevel(lObj.getLogFileLevel())) != null)
      setLevel(levelObj);
  }
  
  /**
   * Publishes the given message into the ISTI LogFile object.
   * @param recObj log record to be published, or null for no action.
   */
  public void publish(LogRecord recObj)
  {
    if(recObj == null || !isLoggable(recObj))
      return;
    String str = recObj.getMessage();
    Throwable t = recObj.getThrown();
    if (str == null && t == null)
    	return;
    if (str != null)
    	str = str.trim();
    int level = levelToIstiLevel(recObj.getLevel());
    istiLogObj.println(level, str, t);
  }
  
  /**
   * Flush any buffered output.  Performs no action in this class.
   */
  public void flush()
  {
  }
  
  /**
   * Close the Handler.  Performs no action in this class.
   */
  public void close()
  {
  }

  /**
   * Converts the given Java-logging 'Level' object to the corresponding
   * ISTI LogFile level value.
   * @param levelObj Java-logging 'Level' object to be converted.
   * @return An ISTI LogFile level value.
   */
  public static int levelToIstiLevel(Level levelObj)
  {
	int level = LogFile.INFO;
	Integer levelInt = levelXlatTable.get(levelObj);
	if (levelInt != null)
	{
		level = levelInt.intValue();
	}
	return level;
  }

  /**
   * Converts the given ISTI LogFile level value to the corresponding
   * Java-logging 'Level' object.
   * @param levelVal the ISTI LogFile level value to be converted.
   * @return A Java-logging 'Level' object, or null if the given value
   * could not be matched.
   */
  public static Level levelFromIstiLevel(int levelVal)
  {
    try
    {
      final Iterator<Entry<Level, Integer>> iterObj =
    		  levelXlatTable.entrySet().iterator();
      Map.Entry<Level, Integer> entryObj;
      while(iterObj.hasNext())
      {  //for each entry in table; if value matches then return key
        entryObj = iterObj.next();
        if(entryObj.getValue().intValue() == levelVal)
          return entryObj.getKey();
      }
    }
    catch(Exception ex)
    {  //some kind of exception error; just return null for no match
    }
    return null;
  }
}
