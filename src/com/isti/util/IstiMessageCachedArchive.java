//IstiMessageCachedArchive.java:  Implements an ArchiveManager with a
//                                front-end cache.
//   9/9/2003 -- [ET]  Initial version.
//  9/17/2003 -- [ET]  Added log message shown when breaks in message-number
//                     sequence detected during preload of cache.
// 10/03/2003 -- [KF]  Extend 'IstiThread' instead of 'Thread'.
// 10/24/2003 -- [ET]  Changed 'ArchivableMsgObjEntry' from a class into an
//                     interface and created 'BasicArchivableMsgObjEntry'
//                     in its place.
//  11/4/2003 -- [ET]  Added 'getLastMsgNumber()', 'getLastCacheMessageObj()',
//                     'getLastArchiveMessageObj()' and 'getLastMessageObj()',
//                     methods.
// 11/21/2003 -- [ET]  Added 'waitForAddMessageQueueEmpty()' method.
//  7/14/2004 -- [ET]  Added optional 'useLookupKeyFlag' parameter to
//                     constructor; modified 'cacheContains()' to operate
//                     with 'useLookupKeyFlag'==false.
//  7/22/2004 -- [ET]  Added 'getCacheSize()' method; added return value to
//                     'removeOldObjects()' method; added optional params
//                     'cacheMaxObjectCount' and 'cacheMaxTotalDataSize'
//                     to constructor and added associated "set" methods.
//  7/12/2005 -- [ET]  Modified to make 'AddMessageWorkerThread' class
//                     extend 'IstiNotifyThread' to avoid use of thread
//                     'interrupt()' calls that can be problematic under
//                     Solaris OS.
//   7/9/2008 -- [ET]  Fixed issue with 'getLastMessageObj()' where it
//                     would always fetch the message from the archive
//                     (instead of the cache).
//  4/29/2010 -- [ET]  Added optional 'cacheOnlyFlag' parameter to methods
//                     'getMessages()' and 'getNewerMessages()'.
//

package com.isti.util;

import java.util.Vector;
import java.util.Iterator;
import java.util.Date;
import java.io.IOException;
import com.isti.util.IstiTimeObjectCache.VectorWithCount;

/**
 * Class IstiMessageCachedArchive implements an ArchiveManager with a
 * front-end cache.  The 'removeOldObjects()' method should be called
 * on a periodic basis to remove old objects from the cache and archive.
 */
public class IstiMessageCachedArchive
{
  protected final IstiMessageObjectCache messageCacheObj;
  protected final ArchiveManager archiveMgrObj;
  protected long cacheRemoveAgeMilliSecs;
  protected long archiveRemoveAgeMilliSecs;
  protected long maxRequestAgeMilliSecs;
  protected final LogFile logObj;
  protected final Object dataAccessSyncObj;
  protected final Vector addMessageQueueVec = new Vector();
  protected final AddMessageWorkerThread addMessageWorkerThreadObj =
                                               new AddMessageWorkerThread();
  protected boolean closingCacheArchiveFlag = false;

  /**
   * Constructs a cached-archive object.  If 'useLookupKeyFlag'==true
   * then the cache is setup to use the "dataStr" parameter (when adding)
   * as a table-lookup key (which forces cache entries to have unique
   * "dataStr" values).
   * @param archiveMgrObj an ArchiveManager object configured to
   * archive a subclass of 'ArchivableMsgObjEntry'.
   * @param logObj log file object to use or null for no logging.
   * @param tolerance number of milliseconds for time tolerance
   * that is used with time checks
   * @param cacheRemoveAgeSecs the number of seconds to keep objects
   * in the cache.
   * @param cacheMaxObjectCount the maximum-object count for the cache,
   * or 0 for no limit.
   * @param cacheMaxTotalDataSize the maximum-total-data size for the
   * cache, or 0 for no limit.
   * @param archiveRemoveAgeSecs the number of seconds to keep objects
   * in the archive, or 0 for "infinity".
   * @param maxRequestAgeSecs maximum age (in seconds) for messages
   * returned via the 'get...Messages()' methods, or 0 for no limit.
   * @param useLookupKeyFlag true to use the "dataStr" parameter (when
   * adding) as a table-lookup key (which forces cache entries to have
   * unique "dataStr" values).
   */
  public IstiMessageCachedArchive(ArchiveManager archiveMgrObj,
                     LogFile logObj, long tolerance, int cacheRemoveAgeSecs,
                        int cacheMaxObjectCount, long cacheMaxTotalDataSize,
                            int archiveRemoveAgeSecs, int maxRequestAgeSecs,
                                                   boolean useLookupKeyFlag)
  {
    if(archiveMgrObj == null)
      throw new NullPointerException("ArchiveManager parameter is null");
    if(cacheRemoveAgeSecs == 0)
    {
      throw new RuntimeException(
                               "Cache-remove-age parameter cannot be zero");
    }
    this.archiveMgrObj = archiveMgrObj;
    this.logObj = logObj;
    cacheRemoveAgeMilliSecs = (long)cacheRemoveAgeSecs * 1000L;
    archiveRemoveAgeMilliSecs = (long)archiveRemoveAgeSecs * 1000L;
    maxRequestAgeMilliSecs = (long)maxRequestAgeSecs * 1000L;
                   //create message-object cache:
    messageCacheObj = new IstiMessageObjectCache(logObj,tolerance,
                             cacheRemoveAgeMilliSecs,null,useLookupKeyFlag);
                   //setup maximum count and size values:
    messageCacheObj.setMaximumObjectCount(cacheMaxObjectCount);
    messageCacheObj.setMaximumTotalDataSize(cacheMaxTotalDataSize);
                   //get the thread-sync object from the archive manager:
    dataAccessSyncObj = archiveMgrObj.getArchiveAccessSyncObj();
                   //start the add-message worker thread:
    addMessageWorkerThreadObj.start();
  }

  /**
   * Constructs a cached-archive object.  If 'useLookupKeyFlag'==true
   * then the cache is setup to use the "dataStr" parameter (when adding)
   * as a table-lookup key (which forces cache entries to have unique
   * "dataStr" values).
   * @param archiveMgrObj an ArchiveManager object configured to
   * archive a subclass of 'ArchivableMsgObjEntry'.
   * @param logObj log file object to use or null for no logging.
   * @param tolerance number of milliseconds for time tolerance
   * that is used with time checks
   * @param cacheRemoveAgeSecs the number of seconds to keep objects
   * in the cache.
   * @param archiveRemoveAgeSecs the number of seconds to keep objects
   * in the archive, or 0 for "infinity".
   * @param maxRequestAgeSecs maximum age (in seconds) for messages
   * returned via the 'get...Messages()' methods, or 0 for no limit.
   * @param useLookupKeyFlag true to use the "dataStr" parameter (when
   * adding) as a table-lookup key (which forces cache entries to have
   * unique "dataStr" values).
   */
  public IstiMessageCachedArchive(ArchiveManager archiveMgrObj,
                     LogFile logObj, long tolerance, int cacheRemoveAgeSecs,
                            int archiveRemoveAgeSecs, int maxRequestAgeSecs,
                                                   boolean useLookupKeyFlag)
  {
    this(archiveMgrObj,logObj,tolerance,cacheRemoveAgeSecs,0,0,
                   archiveRemoveAgeSecs,maxRequestAgeSecs,useLookupKeyFlag);
  }

  /**
   * Constructs a cached-archive object.  The cache is setup to use
   * the "dataStr" parameter (when adding) as a table-lookup key
   * (which forces cache entries to have unique "dataStr" values).
   * @param archiveMgrObj an ArchiveManager object configured to
   * archive a subclass of 'ArchivableMsgObjEntry'.
   * @param logObj log file object to use or null for no logging.
   * @param tolerance number of milliseconds for time tolerance
   * that is used with time checks
   * @param cacheRemoveAgeSecs the number of seconds to keep objects
   * in the cache.
   * @param cacheMaxObjectCount the maximum-object count for the cache,
   * or 0 for no limit.
   * @param cacheMaxTotalDataSize the maximum-total-data size for the
   * cache, or 0 for no limit.
   * @param archiveRemoveAgeSecs the number of seconds to keep objects
   * in the archive, or 0 for "infinity".
   * @param maxRequestAgeSecs maximum age (in seconds) for messages
   * returned via the 'get...Messages()' methods, or 0 for no limit.
   */
  public IstiMessageCachedArchive(ArchiveManager archiveMgrObj,
                     LogFile logObj, long tolerance, int cacheRemoveAgeSecs,
                        int cacheMaxObjectCount, long cacheMaxTotalDataSize,
                            int archiveRemoveAgeSecs, int maxRequestAgeSecs)
  {
    this(archiveMgrObj,logObj,tolerance,cacheRemoveAgeSecs,
                                  cacheMaxObjectCount,cacheMaxTotalDataSize,
                               archiveRemoveAgeSecs,maxRequestAgeSecs,true);
  }

  /**
   * Constructs a cached-archive object.  The cache is setup to use
   * the "dataStr" parameter (when adding) as a table-lookup key
   * (which forces cache entries to have unique "dataStr" values).
   * @param archiveMgrObj an ArchiveManager object configured to
   * archive a subclass of 'ArchivableMsgObjEntry'.
   * @param logObj log file object to use or null for no logging.
   * @param tolerance number of milliseconds for time tolerance
   * that is used with time checks
   * @param cacheRemoveAgeSecs the number of seconds to keep objects
   * in the cache.
   * @param archiveRemoveAgeSecs the number of seconds to keep objects
   * in the archive, or 0 for "infinity".
   * @param maxRequestAgeSecs maximum age (in seconds) for messages
   * returned via the 'get...Messages()' methods, or 0 for no limit.
   */
  public IstiMessageCachedArchive(ArchiveManager archiveMgrObj,
                     LogFile logObj, long tolerance, int cacheRemoveAgeSecs,
                            int archiveRemoveAgeSecs, int maxRequestAgeSecs)
  {
    this(archiveMgrObj,logObj,tolerance,cacheRemoveAgeSecs,0,0,
                               archiveRemoveAgeSecs,maxRequestAgeSecs,true);
  }

  /**
   * Adds the given message object to the cache and to the archive.
   * If the cache was constructed to use the "dataStr" parameter (when
   * adding) as a table-lookup key and a message with a matching key is
   * already in the cache then the given message object is not added.
   * The message object is queued and then added via a worker thread.
   * @param archivableMsgObj an 'ArchivableMsgObjEntry' object containing
   * the message data to be added.
   */
  public void addArchivableMsgObj(ArchivableMsgObjEntry archivableMsgObj)
  {
    synchronized(addMessageQueueVec)
    {    //grab thread-synchronization lock for message-queue Vector
              //add message obj to queue to be processed by worker thread:
      addMessageQueueVec.add(archivableMsgObj);
              //notify worker-thread sleep to get it to process message:
      addMessageWorkerThreadObj.notifyThread();
    }
  }

  /**
   * Performs the work of adding the given message object to the cache
   * and to the archive.  If the cache was constructed to use the
   * "dataStr" parameter (when adding) as a table-lookup key and a
   * message with a matching key is already in the cache then the given
   * message object is not added.
   * @param archivableMsgObj an 'ArchivableMsgObjEntry' object containing
   * the message data to be added.
   * @throws IOException if the archive file can not be written to.
   */
  protected void doAddArchivableMsgObj(
                  ArchivableMsgObjEntry archivableMsgObj) throws IOException
  {
    synchronized(dataAccessSyncObj)
    {    //grab the thread-synchronization object
      if(!messageCacheObj.containsObjectKey(archivableMsgObj))
      {    //matching object-key not already in cache
        final long msgTime = archivableMsgObj.getTimeGenerated();
        final long curTime = System.currentTimeMillis();
        if(msgTime > curTime - cacheRemoveAgeMilliSecs)
        {     //message is not too old for cache; add to cache
          messageCacheObj.addMsgObjEntry(archivableMsgObj);
        }
        else if(logObj != null)
        {
          logObj.debug2(getLogPrefixString() +
                           ":  Entry not added to cache; too old (msgNum=" +
                             archivableMsgObj.getMsgNum() + ", msgDate=\"" +
                                                 UtilFns.timeMillisToString(
                 archivableMsgObj.getArchiveDate().getTime(),true) + "\")");
          logObj.debug3(getLogPrefixString() + ":  dataStr = \"" +
                                       archivableMsgObj.getKeyStr() + "\"");
        }
        if(archiveRemoveAgeMilliSecs <= 0 ||
                              msgTime > curTime - archiveRemoveAgeMilliSecs)
        {     //message is not too old for archive; add to archive
          archiveMgrObj.archiveItem(archivableMsgObj);
        }
        else if(logObj != null)
        {
          logObj.debug2(getLogPrefixString() +
                         ":  Entry not added to archive; too old (msgNum=" +
                             archivableMsgObj.getMsgNum() + ", msgDate=\"" +
                                                 UtilFns.timeMillisToString(
                 archivableMsgObj.getArchiveDate().getTime(),true) + "\")");
          logObj.debug3(getLogPrefixString() + ":  dataStr = \"" +
                                       archivableMsgObj.getKeyStr() + "\"");
        }
      }
      else if(logObj != null)
      {
        logObj.debug2(getLogPrefixString() + ":  Entry not added because " +
                       "matching msg-object-key already in cache (msgNum=" +
                             archivableMsgObj.getMsgNum() + ", msgDate=\"" +
                                                 UtilFns.timeMillisToString(
                 archivableMsgObj.getArchiveDate().getTime(),true) + "\")");
        logObj.debug3(getLogPrefixString() + ":  dataStr = \"" +
                                       archivableMsgObj.getKeyStr() + "\"");
      }
    }
  }

  /**
   * Retrieves a copy of messages in the specified time range (inclusive).
   * If a message-count limit ('maxCount') is given and the number of
   * messages returned is less than the number requested then the
   * 'count' value in the returned 'VectorWithCount' object will be
   * greater than the 'maxCount' value.
   * @param beginTime begin time in milliseconds or 0 for start of cache.
   * @param beginMsgNum begin message number.
   * @param endTime end time in milliseconds or 0 for end of cache.
   * @param endMsgNum end message number.
   * @param maxCount the maximum number of messages to be returned,
   * or 0 for no limit.
   * @param cacheOnlyFlag true if messages are to be fetched only from
   * the cache; false if messages may be fetched from the archive or
   * the cache.
   * @return A new 'VectorWithCount' of 'ArchivableMsgObjEntry' objects
   * that also holds the requested number of items in its 'count' field.
   */
  public VectorWithCount getMessages(long beginTime,long beginMsgNum,
                                   final long endTime, final long endMsgNum,
                                        int maxCount, boolean cacheOnlyFlag)
  {
    if(logObj != null)
    {
      logObj.debug3(getLogPrefixString() + ".getMessages(), beginTime=" +
                 beginTime + ", beginMsgNum=" + beginMsgNum + ", endTime=" +
           endTime + ", endMsgNum=" + endMsgNum + ", maxCount=" + maxCount);
    }
    synchronized(dataAccessSyncObj)
    {    //grab the thread-synchronization object
      if(maxRequestAgeMilliSecs > 0)
      {  //max request age limit was specified; calc time value for limit
        final long maxRequestTimeVal = System.currentTimeMillis() -
                                                     maxRequestAgeMilliSecs;
        if(beginTime < maxRequestTimeVal)
        {     //requested time is before limit
          if(logObj != null)
          {
            logObj.debug(getLogPrefixString() +
                     ".getMessages():  Requested begin time (" + beginTime +
                  ") adjusted to " + maxRequestAgeMilliSecs + "ms limit (" +
                                                   maxRequestTimeVal + ")");
          }
          beginTime = maxRequestTimeVal;         //use time value for limit
          beginMsgNum = 0;                       //clear any given msg num
        }
      }

      long archiveFetchEndTime;             //end time for archive fetch
      final boolean beginTimeInCacheFlag;   //true to skip archive search
      Object obj;
      final ArchivableMsgObjEntry firstCacheMsgObj;
      if((obj=messageCacheObj.getFirstMessage()) instanceof
                                                      ArchivableMsgObjEntry)
      {  //first message in cache fetched OK
        firstCacheMsgObj = (ArchivableMsgObjEntry)obj;     //set handle
                        //get time of first message in cache:
        archiveFetchEndTime = firstCacheMsgObj.getTimeGenerated();
        if(endTime > 0 && archiveFetchEndTime > endTime)
        {     //start of cache is after given end time
          archiveFetchEndTime = endTime;    //use given end time
        }
              //if begin message number given then indicate begin time
              // is within cache time range if >= first message time;
              // if begin message number not given then only indicate
              // if time is greater (to make sure none missed):
        if(beginMsgNum > 0)
        {     //begin message number was given
          beginTimeInCacheFlag =
                         (beginTime >= firstCacheMsgObj.getTimeGenerated());
        }
        else
        {     //begin message number not given
          beginTimeInCacheFlag =
                          (beginTime > firstCacheMsgObj.getTimeGenerated());
        }
      }
      else
      {  //unable to fetch first message in cache
        firstCacheMsgObj = null;            //indicate empty cache
        archiveFetchEndTime = endTime;      //use given end time
              //indicate begin time not within cache time range:
        beginTimeInCacheFlag = false;
      }

      final VectorWithCount archiveVec;
      final int archiveVecSize;
      if(!beginTimeInCacheFlag && !cacheOnlyFlag)
      {  //begin time not within cache time range & archive fetched allowed
        archiveVec = new VectorWithCount();
                                  //setup begin date for archive processing:
        final Date archiveBeginDateObj = (beginTime > 0) ?
                                                 new Date(beginTime) : null;
                                  //setup end date for archive processing:
        final Date archiveEndDateObj = (archiveFetchEndTime > 0) ?
                                       new Date(archiveFetchEndTime) : null;
                   //create final versions of 'begin' values that can
                   // be accessed from within anonymous class below:
        final long reqBeginTime = beginTime;
        final long reqBeginMsgNum = beginMsgNum;
                   //process items from archive; loading into Vector:
        archiveMgrObj.processArchivedItems(archiveBeginDateObj,
                    archiveEndDateObj, (new ArchiveManager.ProcessCallBack()
            {
                       //process each message-item object from archive:
              public boolean procArchivedItem(Archivable itemObj,
                                                     String archivedFormStr)
              {
                if(closingCacheArchiveFlag)      //if 'closing' flag then
                  return false;                  //terminate processing
                if(itemObj instanceof ArchivableMsgObjEntry)
                {  //item is instance is of type 'ArchivableMsgObjEntry'
                  final ArchivableMsgObjEntry msgObj =
                                             (ArchivableMsgObjEntry)itemObj;
                  if(reqBeginMsgNum > 0 &&
                                     msgObj.getMsgNum() == reqBeginMsgNum &&
                                  msgObj.getTimeGenerated() == reqBeginTime)
                  {     //begin msgNum given and matches current item
                             //discard current message & any previous ones:
                    archiveVec.clear();
                    return true;                 //keep processing
                  }

                  if(endMsgNum > 0 && msgObj.getMsgNum() == endMsgNum &&
                                       msgObj.getTimeGenerated() == endTime)
                  {     //end msgNum given and matches current item
                                       //discard current message and
                    return false;      //process no more from archive
                  }
                }
                        //check if fetched item matches first item in
                        // cache; if so then stop fetching from archive:
                if(firstCacheMsgObj != null)
                {      //first item from cache is available
                  if(archivedFormStr != null &&
                                               archivedFormStr.length() > 0)
                  {    //archived-form string contains data
                    if(archivedFormStr.equals(
                                         firstCacheMsgObj.toArchivedForm()))
                    {       //item from archive matches first cache entry
                      return false;      //process no more from archive
                    }
                  }         //no archived-form string given; compare items
                  else if(itemObj.equals(firstCacheMsgObj))
                    return false;        //if equal to first cache entry then
                }                        // process no more from archive
                archiveVec.add(itemObj);             //add item to vector
                return true;                         //keep processing
              }
            }), ((maxCount > 0) ? (maxCount + 2) : 0));
                 //if max-count given then pass in maxCount+1 to allow
                 // determination of whether or not more items remain

        archiveVecSize = archiveVec.size();
        if(logObj != null)
        {
          logObj.debug3(getLogPrefixString() + ".getMessages():  " +
                             "Matching # from archive = " + archiveVecSize);
        }
        if(maxCount > 0 && archiveVecSize >= maxCount)
        {   //max-count limit given and # of items at or beyond limit
          if(logObj != null)
          {
            logObj.debug3(getLogPrefixString() + ".getMessages():  " +
                        "Item-count limit reached via fetch from archive " +
                                     "(numFetched=" + archiveVecSize + ")");
          }
          int p = archiveVecSize;
          while(p > maxCount)        //remove any items beyond lmit
            archiveVec.removeElementAt(--p);
                 //enter number-of-items-requested value into return object
                 // (make sure it's larger than 'maxCount'):
          archiveVec.setCount(archiveVecSize + maxCount);
          return archiveVec;         //return Vector of items
        }
      }
      else
      {  //no fetch from archive
        archiveVec = null;
        archiveVecSize = 0;
      }

      if(firstCacheMsgObj != null && (endTime <= 0 ||
                            endTime >= firstCacheMsgObj.getTimeGenerated()))
      {  //cache not empty and requested end time is within cache time range
              //calc max-count value for cache (less items from archive):
        final int cacheMaxCount = (maxCount > 0) ?
                                            (maxCount - archiveVecSize) : 0;
              //fetch matching items from cache:
        final VectorWithCount cacheVec = messageCacheObj.getMessages(
                     beginTime,beginMsgNum,endTime,endMsgNum,cacheMaxCount);
        final int cacheVecSize;
        if((cacheVecSize=cacheVec.size()) > 0)
        {     //one or more matching items fetched from cache
          if(logObj != null)
          {
            logObj.debug3(getLogPrefixString() + ".getMessages():  " +
                                 "Matching # from cache = " + cacheVecSize);
          }
          if(archiveVec == null || archiveVecSize <= 0)
            return cacheVec; //if no items from archive then return cache vec
          archiveVec.addAll(cacheVec); //append cache items to archive items
                                       //add cache 'count' value to archive:
          archiveVec.setCount(archiveVecSize + cacheVec.getCount());
          return archiveVec;      //return combined Vector of items
        }
      }
         //no items fetched from cache
      if(archiveVec != null)
      {  //vector of items was fetched from archive
        archiveVec.setCount(archiveVecSize);     //enter "requested" count
        return archiveVec;                       //return Vector of items
      }
         //no items fetched; return empty vec:
      return new VectorWithCount();
    }
  }

  /**
   * Retrieves a copy of messages in the specified time range (inclusive).
   * If a message-count limit ('maxCount') is given and the number of
   * messages returned is less than the number requested then the
   * 'count' value in the returned 'VectorWithCount' object will be
   * greater than the 'maxCount' value.
   * @param beginTime begin time in milliseconds or 0 for start of cache.
   * @param beginMsgNum begin message number.
   * @param endTime end time in milliseconds or 0 for end of cache.
   * @param endMsgNum end message number.
   * @param maxCount the maximum number of messages to be returned,
   * or 0 for no limit.
   * @return A new 'VectorWithCount' of 'ArchivableMsgObjEntry' objects
   * that also holds the requested number of items in its 'count' field.
   */
  public VectorWithCount getMessages(long beginTime,long beginMsgNum,
                     final long endTime, final long endMsgNum, int maxCount)
  {
    return getMessages(beginTime,beginMsgNum,endTime,endMsgNum,maxCount,
                                                                     false);
  }

  /**
   * Retrieves a copy of messages in the specified time range (inclusive).
   * @param beginTime begin time in milliseconds or 0 for start of cache.
   * @param beginMsgNum begin message number.
   * @param endTime end time in milliseconds or 0 for end of cache.
   * @param endMsgNum end message number.
   * @return A new 'VectorWithCount' of 'ArchivableMsgObjEntry' objects
   * that also holds the requested number of items in its 'count' field.
   */
  public VectorWithCount getMessages(long beginTime, long beginMsgNum,
                                               long endTime, long endMsgNum)
  {
    return getMessages(beginTime,beginMsgNum,endTime,endMsgNum,0,false);
  }

  /**
   * Returns a list of messages newer or equal to the specified time value
   * or later than the specified message number.  If a non-zero message
   * number is given and the given time value matches then messages with
   * message numbers greater than the given message number are returned;
   * otherwise messages newer or equal to the specified time value are
   * returned (within a tolerance value).
   * @param time time in milliseconds.
   * @param msgNum message number.
   * @param maxCount the maximum number of messages to be returned,
   * or 0 for no limit.
   * @param cacheOnlyFlag true if messages are to be fetched only from
   * the cache; false if messages may be fetched from the archive or
   * the cache.
   * @return A new 'VectorWithCount' of 'ArchivableMsgObjEntry' objects
   * that also holds the requested number of items in its 'count' field.
   */
  public VectorWithCount getNewerMessages(long time, long msgNum,
                                        int maxCount, boolean cacheOnlyFlag)
  {
    return getMessages(time,msgNum,0,0,maxCount,cacheOnlyFlag);
  }

  /**
   * Returns a list of messages newer or equal to the specified time value
   * or later than the specified message number.  If a non-zero message
   * number is given and the given time value matches then messages with
   * message numbers greater than the given message number are returned;
   * otherwise messages newer or equal to the specified time value are
   * returned (within a tolerance value).
   * @param time time in milliseconds.
   * @param msgNum message number.
   * @param maxCount the maximum number of messages to be returned,
   * or 0 for no limit.
   * @return A new 'VectorWithCount' of 'ArchivableMsgObjEntry' objects
   * that also holds the requested number of items in its 'count' field.
   */
  public VectorWithCount getNewerMessages(long time, long msgNum,
                                                               int maxCount)
  {
    return getMessages(time,msgNum,0,0,maxCount,false);
  }

  /**
   * Returns a list of messages newer or equal to the specified time value
   * or later than the specified message number.  If a non-zero message
   * number is given and the given time value matches then messages with
   * message numbers greater than the given message number are returned;
   * otherwise messages newer or equal to the specified time value are
   * returned (within a tolerance value).
   * @param time time in milliseconds.
   * @param msgNum message number.
   * @return A new 'VectorWithCount' of 'ArchivableMsgObjEntry' objects
   * that also holds the requested number of items in its 'count' field.
   */
  public VectorWithCount getNewerMessages(long time, long msgNum)
  {
    return getMessages(time,msgNum,0,0,0,false);
  }

  /**
   * Returns a list of messages older or equal to the specified time value
   * or sooner than the specified message number.  If a non-zero message
   * number is given and the given time value matches then messages with
   * message numbers less than the given message number are returned;
   * otherwise messages older or equal to the specified time value are
   * returned (within a tolerance value).
   * @param time time in milliseconds.
   * @param msgNum message number.
   * @param maxCount the maximum number of messages to be returned,
   * or 0 for no limit.
   * @return A new 'VectorWithCount' of 'ArchivableMsgObjEntry' objects
   * that also holds the requested number of items in its 'count' field.
   */
  public VectorWithCount getOlderMessages(long time, long msgNum,
                                                               int maxCount)
  {
    return getMessages(0,0,time,msgNum,maxCount,false);
  }

  /**
   * Returns a list of messages older or equal to the specified time value
   * or sooner than the specified message number.  If a non-zero message
   * number is given and the given time value matches then messages with
   * message numbers less than the given message number are returned;
   * otherwise messages older or equal to the specified time value are
   * returned (within a tolerance value).
   * @param time time in milliseconds.
   * @param msgNum message number.
   * @return A new 'VectorWithCount' of 'ArchivableMsgObjEntry' objects
   * that also holds the requested number of items in its 'count' field.
   */
  public VectorWithCount getOlderMessages(long time, long msgNum)
  {
    return getMessages(0,0,time,msgNum,0,false);
  }

  /**
   * Performs the initial loading of the cache from existing items in
   * the archive.
   */
  public void preloadCacheFromArchive()
  {
    synchronized(dataAccessSyncObj)
    {    //grab the thread-synchronization object
              //process items from archive starting with the
              // max-cache age and load them into the cache:
      archiveMgrObj.processArchivedItems(
               new Date(System.currentTimeMillis()-cacheRemoveAgeMilliSecs),
                                  null, new ArchiveManager.ProcessCallBack()
          {
                   //process each message-item object from archive:
            public boolean procArchivedItem(Archivable itemObj,
                                                     String archivedFormStr)
            {                       //if object type OK then add to cache:
              if(itemObj instanceof ArchivableMsgObjEntry)
              {
                messageCacheObj.addMsgObjEntry(
                                            (ArchivableMsgObjEntry)itemObj);
              }
              return true;                    //keep processing
            }
          });
      if(logObj != null)
      {  //log object is avail; check if any breaks in msg-num seq detected
        final long timeVal;
        if((timeVal=messageCacheObj.getLastMsgNumSeqBreakTime()) > 0)
        {  //at least one break in msgNum seq detected; log time of last one
          logObj.debug(getLogPrefixString() + ":  Time of last detected " +
                                  "break in message # sequence in cache (" +
               timeVal + ") = " + UtilFns.timeMillisToString(timeVal,true));
        }
      }
    }
  }

  /**
   * Removes old objects from the cache and archive.  This method should
   * be called on a periodic basis.
   * @return true if any objects were removed from the cache; false if no
   * objects were removed from the cache.
   */
  public boolean removeOldObjects()
  {
    synchronized(dataAccessSyncObj)
    {    //grab the thread-synchronization object
                   //remove old objects from cache:
      final boolean retFlag = messageCacheObj.removeOldObjects();
      if(archiveRemoveAgeMilliSecs > 0)
      {  //non-zero remove-from-archive age value
        archiveMgrObj.purgeArchive(         //remove old items from archive
            new Date(System.currentTimeMillis()-archiveRemoveAgeMilliSecs));
      }
      return retFlag;
    }
  }

  /**
   * Determines whether or not a message object "equal" to the given
   * message object is contained in the cache.  Equality is determined
   * using the 'dataStr' member of the objects.  The message-queue is
   * also checked to see if any waiting messages are "equal".
   * @param archivableMsgObj an 'ArchivableMsgObjEntry' object to use.
   * @return true if a message object "equal" to the given  message
   * object is contained in the cache.
   */
  public boolean cacheContains(ArchivableMsgObjEntry archivableMsgObj)
  {
    synchronized(addMessageQueueVec)
    {    //grab thread-synchronization lock for message-queue Vector
              //check if matching message is in message-queue:
      if(addMessageQueueVec.size() > 0)
      {  //message-queue Vector contains items
        final Iterator iterObj = addMessageQueueVec.iterator();
        while(iterObj.hasNext())
        {     //for each item in queue
          if(archivableMsgObj.equals(iterObj.next()))
            return true;     //if match then return flag
        }
      }
         //check cache for match; return result (if using "dataStr"
         // as a lookup key then search via hash-table key):
      return messageCacheObj.getUseLookupKeyFlag() ?
                       messageCacheObj.containsObjectKey(archivableMsgObj) :
                            messageCacheObj.containsEntry(archivableMsgObj);
    }
  }

  /**
   * Returns the status of whether or not the cache is empty.
   * @return true if the cache is empty, false if not.
   */
  public boolean isCacheEmpty()
  {
    return messageCacheObj.isEmpty();
  }

  /**
   * Returns the number of entries in the cache.
   * @return The number of entries in the cache.
   */
  public int getCacheSize()
  {
    return messageCacheObj.size();
  }

  /**
   * Returns the newest message object entered into the cache.
   * @return The newest 'ArchivableMsgObjEntry' object entered into
   * the cache, or null if none have been entered.
   */
  public ArchivableMsgObjEntry getLastCacheMessageObj()
  {
    synchronized(dataAccessSyncObj)
    {    //grab the thread-synchronization object
      final IstiMessageObjectCache.MessageObjectEntry entryObj;
      return ((entryObj=messageCacheObj.getLastMessage()) instanceof
            ArchivableMsgObjEntry) ? (ArchivableMsgObjEntry)entryObj : null;
    }
  }

  /**
   * Returns the newest message object entered into the archive.
   * @return The newest 'ArchivableMsgObjEntry' object entered into
   * the archive, or null if none have been entered.
   */
  public ArchivableMsgObjEntry getLastArchiveMessageObj()
  {
    synchronized(dataAccessSyncObj)
    {    //grab the thread-synchronization object
      final Archivable archivableObj;
      return ((archivableObj=archiveMgrObj.getNewestItemInEntireArchive())
                                         instanceof ArchivableMsgObjEntry) ?
                                (ArchivableMsgObjEntry)archivableObj : null;
    }
  }

  /**
   * Returns the newest message object entered into the cache or archive.
   * @return The newest 'ArchivableMsgObjEntry' object entered into the
   * cache or archive, or null if none have been entered.
   */
  public ArchivableMsgObjEntry getLastMessageObj()
  {
    synchronized(dataAccessSyncObj)
    {    //grab the thread-synchronization object
      return (messageCacheObj.isEmpty()) ? getLastArchiveMessageObj() :
                                                   getLastCacheMessageObj();
    }
  }

  /**
   * Returns the message number for the newest message entered into
   * the cache.
   * @return The message number for the newest message entered into
   * the cache, or 0 if none have been entered.
   */
  public long getLastCacheMsgNum()
  {
    synchronized(dataAccessSyncObj)
    {    //grab the thread-synchronization object
      return messageCacheObj.getLastMsgNum();
    }
  }

  /**
   * Returns the message number for the newest message entered into
   * the archive.
   * @return The message number for the newest message entered into
   * the archive, or 0 if none have been entered.
   */
  public long getLastArchiveMsgNum()
  {
    synchronized(dataAccessSyncObj)
    {    //grab the thread-synchronization object
      final Archivable archivableObj;
      if((archivableObj=archiveMgrObj.getNewestItemInEntireArchive())
                                           instanceof ArchivableMsgObjEntry)
      {  //newest item in archive fetched OK; return its message number
        return ((ArchivableMsgObjEntry)archivableObj).getMsgNum();
      }
      return 0;
    }
  }

  /**
   * Returns the message number for the newest message entered into
   * the cache or archive.
   * @return The message number for the newest message entered into
   * the cache or archive, or 0 if none have been entered.
   */
  public long getLastMsgNumber()
  {
    synchronized(dataAccessSyncObj)
    {    //grab the thread-synchronization object
      return (messageCacheObj.isEmpty()) ? getLastArchiveMsgNum() :
                                                       getLastCacheMsgNum();
    }
  }

  /**
   * Modifies the remove-from-cache age used by the 'removeOldObjects()'
   * and 'preloadCacheFromArchive()' methods.
   * @param ageInSecs the age to use, in seconds.
   */
  public void setCacheRemoveAgeSecs(int ageInSecs)
  {
    synchronized(dataAccessSyncObj)
    {    //grab the thread-synchronization object
      cacheRemoveAgeMilliSecs = (long)ageInSecs * 1000L;
                             //update remove age for cache:
      messageCacheObj.setDefaultRemoveAge(cacheRemoveAgeMilliSecs);
    }
  }

  /**
   * Modifies the maximum-object count for the cache.  When the
   * 'removeOldObjects()' method is called the cache size will
   * be trimmed to this value as needed.
   * @param val the maximum-object count for the cache, or 0 for
   * no maximum-object count limit (the default).
   */
  public void setCacheMaxObjectCount(int val)
  {
    synchronized(dataAccessSyncObj)
    {    //grab the thread-synchronization object
                                   //update max-count value for cache:
      messageCacheObj.setMaximumObjectCount(val);
    }
  }

  /**
   * Modifies the maximum-total-data size for the cache.  To calculate the
   * total-data size the 'setDataSize()' method needs to have been called
   * on each cache object.  When the 'removeOldObjects()' method is
   * called the cache size will be trimmed until the total-data size is
   * under this value as needed.
   * @param val the maximum-total-data size for the cache, or 0 for
   * no maximum-total-data size limit (the default).
   */
  public void setCacheMaxTotalDataSize(long val)
  {
    synchronized(dataAccessSyncObj)
    {    //grab the thread-synchronization object
                             //update max-total-data-size value for cache:
      messageCacheObj.setMaximumTotalDataSize(val);
    }
  }

  /**
   * Modifies the remove-from-archive age used by the 'removeOldObjects()'
   * method.
   * @param ageInSecs the age to use, in seconds.
   */
  public void setArchiveRemoveAgeSecs(int ageInSecs)
  {
    synchronized(dataAccessSyncObj)
    {    //grab the thread-synchronization object
      archiveRemoveAgeMilliSecs = (long)ageInSecs * 1000L;
    }
  }

  /**
   * Modifies the max-request age used by the 'get...Messages()'
   * methods.
   * @param ageInSecs the age to use, in seconds.
   */
  public void setMaxRequestAgeSecs(int ageInSecs)
  {
    synchronized(dataAccessSyncObj)
    {    //grab the thread-synchronization object
      maxRequestAgeMilliSecs = (long)ageInSecs * 1000L;
    }
  }

  /**
   * Returns the is-empty status of the add-message queue.
   * @return true if the add-message queue is empty, false if messages
   * are waiting to be added to the cache and archive.
   */
  public boolean isAddMessageQueueEmpty()
  {
    return addMessageQueueVec.isEmpty();
  }

  /**
   * Waits for the add-message queue to be empty, up to the specified
   * amount of time.
   * @param maxWaitMs the maximum number of milliseconds to wait for
   * the queue to be empty, or 0 to wait indefinitely.
   * @return true if the add-message queue is empty, false if messages
   * are waiting to be added and the max-wait-time was reached
   */
  public boolean waitForAddMessageQueueEmpty(long maxWaitMs)
  {
    if(addMessageQueueVec.isEmpty())        //if queue empty then
      return true;                          //return flag
                   //calcuate timeout-time for max-wait:
    final long timeoutTime = (maxWaitMs > 0) ?
                                 System.currentTimeMillis() + maxWaitMs : 0;
                   //setup delay between checks, min maxWait/10:
    final long delayValMs = (maxWaitMs > 0 && maxWaitMs < 1000) ?
                                                        maxWaitMs/10 : 1000;
    do
    {    //loop while waiting for queue to be empty:
      try
      {            //delay between checks:
        Thread.sleep(delayValMs);
      }
      catch(InterruptedException ex)
      {            //delay interrupted
        break;     //exit loop and return
      }
      if(addMessageQueueVec.isEmpty())      //if queue empty then
        return true;                        //return flag
    }    //loop and no maxWait value or if timeout not reached
    while(maxWaitMs <= 0 || System.currentTimeMillis() <= timeoutTime);
    return addMessageQueueVec.isEmpty();    //return queue-status flag value
  }

  /**
   * Returns the Object used to thread-synchronize the cached and archived
   * data.
   * @return The Object used to thread-synchronize the cached and archived
   * data.
   */
  public Object getDataAccessSyncObj()
  {
    return dataAccessSyncObj;
  }

  /**
   * Returns the message cache.  Access to the cache should be thread-
   * synchronized to the 'dataAccessSyncObj' object.
   * @return The IstiMessageObjectCache object used by this object.
   */
  public IstiMessageObjectCache getMessageCacheObj()
  {
    return messageCacheObj;
  }

  /**
   * Closes the archive and cache.
   */
  public void close()
  {
    closingCacheArchiveFlag = true;         //terminate 'getMessages()'
    addMessageWorkerThreadObj.terminate();  //terminate add-msg worker thread
    synchronized(dataAccessSyncObj)
    {    //grab the thread-synchronization object
      archiveMgrObj.closeArchive();
      if(logObj != null)
        logObj.debug(getLogPrefixString() + ":  Archive manager closed");
      messageCacheObj.close();
    }
  }

  /**
   * Returns the prefix to use for log output.
   * @returns the prefix to use for log output.
   */
  protected String getLogPrefixString()
  {
    return "IstiMessageCachedArchive";
  }


  /**
   * Class AddMessageWorkerThread fetches message objects from the queue
   * and adds them to the cache and archive.
   */
  public class AddMessageWorkerThread extends IstiNotifyThread
  {
    public AddMessageWorkerThread()
    {
      super("msgCachedArchiveAddThread-" + nextThreadNum());
    }

    /**
     * Runs the add-message worker thread.
     */
    public void run()
    {
      mainThreadLoop:        //loop label for inner 'break' statement
      while(true)
      {  //loop while thread not terminated
        waitForNotify();          //wait until thread is notified
        if(isTerminated())
          break;        //if terminated then exit loop (and thread)
        if(addMessageQueueVec.size() > 0)
        {     //message-queue Vector contains items
          while(true)
          {   //loop while processing message objects from queue
            try    //fetch first message object from message-queue
            {      // Vector and add to cache and archive:
              doAddArchivableMsgObj(
                  (ArchivableMsgObjEntry)(addMessageQueueVec.elementAt(0)));
            }
            catch(Exception ex)
            {      //error adding message
              if(logObj != null)
              {
                logObj.warning(getLogPrefixString() +
                     ":  Error adding message to cache or archive:  " + ex);
                logObj.warning(UtilFns.getStackTraceString(ex));
              }
            }
            synchronized(addMessageQueueVec)
            { //grab thread-synchronization lock for message-queue Vector
                   //remove first item from message-queue Vector:
              addMessageQueueVec.remove(0);
              if(addMessageQueueVec.size() <= 0)
              {    //no more objects in message-queue Vector
                                  //clear any lingering notify status:
                clearThreadWaitNotifyFlag();
                break;            //exit inner loop
              }
            }
            if(isTerminated())         //if terminated then
              break mainThreadLoop;    //exit loop (and thread)
          }
        }
      }
    }
  }


  /**
   * Interface ArchivableMsgObjEntry combines the MessageObjectEntry and
   * Archivable interfaces.
   */
  public interface ArchivableMsgObjEntry
               extends IstiMessageObjectCache.MessageObjectEntry, Archivable
  {
  }

  /**
   * Abstract class BasicArchivableMsgObjEntry is an extension of the message-
   * object-entry class that also implements the 'Archivable' interface.
   * This class should be extended to include an Archivable-interface-
   * compatible constructor and a 'toArchivedForm()' method.  The 'equals()'
   * method should also be implemented.
   */
  public static abstract class BasicArchivableMsgObjEntry
                      extends IstiMessageObjectCache.BasicMessageObjectEntry
                                            implements ArchivableMsgObjEntry
  {
    /**
     * Creates a message object entry.
     * @param timeGenerated the time the message was generated.
     * @param msgObj message object.
     * @param msgStr message string (used as a key into the cache-table).
     * @param msgNum message number.
     */
    public BasicArchivableMsgObjEntry(long timeGenerated, Object msgObj,
                                                 String msgStr, long msgNum)
    {
      super(timeGenerated,msgObj,msgStr,msgNum);
    }

    /**
     * No-argument constructor for subclasses.
     */
    protected BasicArchivableMsgObjEntry()
    {
    }

    /**
     * Returns a 'Date' object representing the date to be used for archival
     * purposes.
     * @return A 'Date' object representing the date that the item should be
     * archived under.
     */
    public Date getArchiveDate()
    {
      return new Date(timeGenerated);
    }
  }
}
