//IstiMessageObjectCache.java:  Message object cache.
//
//   5/5/2003 -- [KF]
//   6/5/2003 -- [ET]  Modified to make the message numbers specified in
//                     'getNetMessages()' and 'getOlderMessage()' be
//                     considered non-inclusive; improved various
//                     Javadoc method descriptions.
//   6/6/2003 -- [ET]  Changed 'log.debug()' outputs to 'log.debug3()'.
//  6/11/2003 -- [ET]  Added 'close()' method; modified to keep message-
//                     number file open between updates to it.
//  8/15/2003 -- [ET]  Modified to use a 'FifoHashtable'.
//  8/20/2003 -- [ET]  Added 'useDataStrKeyFlag' parameter to constructors;
//                     added 'addMsgObjEntry()' method.
//   9/4/2003 -- [ET]  Modified to support 'maxCount' limits.
//   9/8/2003 -- [ET]  Renamed 'dataStr' to 'keyStr'; added
//                     'getFirst/LastMessage()' methods.
//   9/9/2003 -- [ET]  Fixed 'get' methods for 'endTime'==0.
//  9/17/2003 -- [ET]  Added work-arounds for dealing with breaks in
//                     message-number sequence.
// 10/24/2003 -- [ET]  Changed 'MessageObjectEntry' from a class into an
//                     interface and created 'BasicMessageObjectEntry'
//                     in its place.
// 10/28/2003 -- [ET]  Fixed reference to 'TimeObjectEntry'.
//
//

package com.isti.util;

import java.io.File;
import java.io.FileDescriptor;
import java.io.IOException;
import java.io.RandomAccessFile;

/**
 * Class IstiMessageObjectCache implements a message object cache.
 * The 'removeOldObjects()' method should be called on a periodic
 * basis to remove old objects from the cache.
 */
public class IstiMessageObjectCache extends IstiTimeObjectCache
{
  protected long lastMsgNumTracker = 0;
  protected final String msgNumFileName;
  protected RandomAccessFile msgNumRAFileObj = null;
  protected FileDescriptor msgNumFDescObj = null;
  protected final Object msgNumFileSyncObj = new Object();
  protected long lastMsgNumSeqBreakTime = 0;
  protected long lastMsgNumSeqBreakMsgNumLogged = 0;

  /**
   * Constructs a time object cache.
   * @param logObj log file object to use or null for no logging.
   * @param tolerance number of milliseconds for time tolerance
   * that is used with time checks
   */
  public IstiMessageObjectCache(LogFile logObj, long tolerance)
  {
    this(logObj, tolerance, 0);
  }

  /**
   * Constructs a message object cache with a default remove age.  The
   * cache is setup to use the "keyStr" parameter (when adding) as a
   * table-lookup key (which forces cache entries to have unique
   * "keyStr" values).
   * @param logObj log file object to use or null for no logging.
   * @param tolerance number of milliseconds for time tolerance
   * that is used with time checks
   * @param removeAge number of milliseconds to keep objects in the cache
   * or 0 for no default.
   */
  public IstiMessageObjectCache(LogFile logObj, long tolerance,
                                                             long removeAge)
  {
    this(logObj, tolerance, removeAge, null, true);
  }

  /**
   * Constructs a message object cache with a default remove age.
   * @param logObj log file object to use or null for no logging.
   * @param tolerance number of milliseconds for time tolerance
   * that is used with time checks
   * @param removeAge number of milliseconds to keep objects in the cache
   * or 0 for no default.
   * @param useLookupKeyFlag true to use the "keyStr" parameter (when
   * adding) as a table-lookup key (which forces cache entries to have
   * unique "keyStr" values).
   */
  public IstiMessageObjectCache(LogFile logObj, long tolerance,
                                   long removeAge, boolean useLookupKeyFlag)
  {
    this(logObj, tolerance, removeAge, null, useLookupKeyFlag);
  }

  /**
   * Constructs a message object cache.  The cache is setup to use the
   * "keyStr" parameter (when adding) as a table-lookup key (which
   * forces cache entries to have unique "keyStr" values).
   * @param logObj log file object to use or null for no logging.
   * @param tolerance number of milliseconds for time tolerance
   * that is used with time checks.
   * @param removeAge number of milliseconds to keep messages in the cache.
   * @param msgNumFileName message number file name or null for none.
   */
  public IstiMessageObjectCache(LogFile logObj, long tolerance,
                                      long removeAge, String msgNumFileName)
  {
    this(logObj, tolerance, removeAge, msgNumFileName, true);
  }

  /**
   * Constructs a message object cache.
   * @param logObj log file object to use or null for no logging.
   * @param tolerance number of milliseconds for time tolerance
   * that is used with time checks.
   * @param removeAge number of milliseconds to keep messages in the cache.
   * @param msgNumFileName message number file name or null for none.
   * @param useLookupKeyFlag true to use the "keyStr" parameter (when
   * adding) as a table-lookup key (which forces cache entries to have
   * unique "keyStr" values).
   */
  public IstiMessageObjectCache(LogFile logObj, long tolerance,
            long removeAge, String msgNumFileName, boolean useLookupKeyFlag)
  {
    super(logObj, tolerance, removeAge, useLookupKeyFlag);
    this.msgNumFileName = msgNumFileName;
    if(msgNumFileName != null)
    {    //message-number filename was specified
      if(new File(msgNumFileName).exists())
        readMsgNumFile();    //if msg-num file exists then read last msg #
      else if(logObj != null)
      {
        logObj.info(getLogPrefixString() +
                    " creating last-message number file " + msgNumFileName);
      }
      try
      {       //open as random-access file, contents written when changed:
        msgNumRAFileObj = new RandomAccessFile(msgNumFileName,"rw");
              //get FileDescriptor to use to flush output to file:
        msgNumFDescObj = msgNumRAFileObj.getFD();
      }
      catch(Exception ex)
      {       //error opening file
        msgNumRAFileObj = null;        //indicate not open
        if(logObj != null)
        {
          logObj.warning(getLogPrefixString() +
                                 " error opening message-number file \"" +
                                             msgNumFileName + "\":  " + ex);
        }
      }
    }
  }

  /**
   * Adds the message-object entry.
   * @param msgObj the message-object entry to add.
   */
  public void addMsgObjEntry(MessageObjectEntry msgObj)
  {
    synchronized(objectCache)
    {    //grab thread synchronization lock for cache
      final long msgNum = msgObj.getMsgNum();
      if(lastMsgNumTracker > 0)
      {  //this is not the first item added
        if(msgNum != lastMsgNumTracker + 1)
        {     //break in message number sequence detected
                        //save time of current message:
          lastMsgNumSeqBreakTime = msgObj.getTimeGenerated();
          if(logObj != null && msgNum != lastMsgNumSeqBreakMsgNumLogged)
          {   //not same msgNum is last time; log message
            logObj.debug(getLogPrefixString() +
                          ":  Message # sequence break detected in cache " +
                                   "data, prevMsgNum=" + lastMsgNumTracker +
                         ", currentMsgNum=" + msgNum + ", currentTimeGen=" +
                                                    lastMsgNumSeqBreakTime);
                   //save last sequence-break message number logged:
            lastMsgNumSeqBreakMsgNumLogged = msgNum;
          }
        }
        else  //msgNum sequence OK; clear any logged-msgNum value
          lastMsgNumSeqBreakMsgNumLogged = 0;
      }
      lastMsgNumTracker = msgNum;      //save last message number used
      if(msgNumRAFileObj != null)      //if msgNum file open then
        writeMsgNumFile();             //save last message number to file
      add(msgObj);
    }
  }

  /**
   * Adds the message.
   * @param timeGenerated the time the message was generated.
   * @param msgObj message object.
   * @param msgStr message string.
   * @param msgNum message number.
   */
  public void addMessage(long timeGenerated, Object msgObj,
                                                 String msgStr, long msgNum)
  {
    addMsgObjEntry(
        new BasicMessageObjectEntry(timeGenerated, msgObj, msgStr, msgNum));
  }

  /**
   * Returns a list of messages newer or equal to the specified time value
   * or later than the specified message number.  If a non-zero message
   * number is given and the given time value matches then messages with
   * message numbers greater than the given message number are returned;
   * otherwise messages newer or equal to the specified time value are
   * returned (within a tolerance value).
   * @param time time in milliseconds.
   * @param msgNum message number.
   * @param maxCount the maximum number of messages to be returned,
   * or 0 for no limit.
   * @return A new 'VectorWithCount' of 'MessageObjectEntry' objects that
   * also holds the requested number of items in its 'count' field.
   */
  public VectorWithCount getNewerMessages(long time, long msgNum, int maxCount)
  {
    return getMessages(time, msgNum, 0, 0, maxCount);
  }

  /**
   * Returns a list of messages newer or equal to the specified time value
   * or later than the specified message number.  If a non-zero message
   * number is given and the given time value matches then messages with
   * message numbers greater than the given message number are returned;
   * otherwise messages newer or equal to the specified time value are
   * returned (within a tolerance value).
   * @param time time in milliseconds.
   * @param msgNum message number.
   * @return A new 'VectorWithCount' of 'MessageObjectEntry' objects that
   * also holds the requested number of items in its 'count' field.
   */
  public VectorWithCount getNewerMessages(long time, long msgNum)
  {
    return getMessages(time, msgNum, 0, 0, 0);
  }

  /**
   * Retrieves a copy of messages in the specified time range.  Any
   * specified begin or end message number is only used if the given
   * begin or end time matches.  Unlike the time values, the begin and
   * end message numbers are considered non-inclusive.
   * @param beginTime begin time in milliseconds or 0 for start of cache.
   * @param beginMsgNum begin message number (objects with message
   * numbers after this one are to be returned).
   * @param endTime end time in milliseconds or 0 for end of cache
   * @param endMsgNum end message number (objects with message numbers
   * before this one are to be returned).
   * @param maxCount the maximum number of messages to be returned,
   * or 0 for no limit.
   * @return A new 'VectorWithCount' of 'MessageObjectEntry' objects that
   * also holds the requested number of items in its 'count' field.
   */
  public VectorWithCount getMessages(long beginTime, long beginMsgNum,
                                 long endTime, long endMsgNum, int maxCount)
  {
    synchronized(objectCache)
    {
      return subListVector(beginTime, beginMsgNum, endTime, endMsgNum,
                                                                  maxCount);
    }
  }

  /**
   * Retrieves a copy of messages in the specified time range.  Any
   * specified begin or end message number is only used if the given
   * begin or end time matches.  Unlike the time values, the begin and
   * end message numbers are considered non-inclusive.
   * @param beginTime begin time in milliseconds or 0 for start of cache.
   * @param beginMsgNum begin message number (objects with message
   * numbers after this one are to be returned).
   * @param endTime end time in milliseconds or 0 for end of cache
   * @param endMsgNum end message number (objects with message numbers
   * before this one are to be returned).
   * @return A new 'VectorWithCount' of 'MessageObjectEntry' objects that
   * also holds the requested number of items in its 'count' field.
   */
  public VectorWithCount getMessages(long beginTime, long beginMsgNum,
                                               long endTime, long endMsgNum)
  {
    synchronized(objectCache)
    {
      return subListVector(beginTime, beginMsgNum, endTime, endMsgNum, 0);
    }
  }

  /**
   * Returns a list of messages older or equal to the specified time value
   * or sooner than the specified message number.  If a non-zero message
   * number is given and the given time value matches then messages with
   * message numbers less than the given message number are returned;
   * otherwise messages older or equal to the specified time value are
   * returned (within a tolerance value).
   * @param time time in milliseconds.
   * @param msgNum message number.
   * @param maxCount the maximum number of messages to be returned,
   * or 0 for no limit.
   * @return A new 'VectorWithCount' of 'MessageObjectEntry' objects that
   * also holds the requested number of items in its 'count' field.
   */
  public VectorWithCount getOlderMessages(long time, long msgNum, int maxCount)
  {
    return getMessages(0, 0, time, msgNum, maxCount);
  }

  /**
   * Returns a list of messages older or equal to the specified time value
   * or sooner than the specified message number.  If a non-zero message
   * number is given and the given time value matches then messages with
   * message numbers less than the given message number are returned;
   * otherwise messages older or equal to the specified time value are
   * returned (within a tolerance value).
   * @param time time in milliseconds.
   * @param msgNum message number.
   * @return A new 'VectorWithCount' of 'MessageObjectEntry' objects that
   * also holds the requested number of items in its 'count' field.
   */
  public VectorWithCount getOlderMessages(long time, long msgNum)
  {
    return getMessages(0, 0, time, msgNum, 0);
  }

  /**
   * Returns the first message object in the cache.
   * @return The first ('TimeObjectEntry') message object in the cache,
   * or null if the cache is empty.
   */
  public MessageObjectEntry getFirstMessage()
  {
    synchronized(objectCache)
    {
      return (objectCache.size() > 0) ? getMessage(0) : null;
    }
  }

  /**
   * Returns the last message object in the cache.
   * @return The last ('TimeObjectEntry') message object in the cache,
   * or null if the cache is empty.
   */
  public MessageObjectEntry getLastMessage()
  {
    synchronized(objectCache)
    {
      final int cacheSize;
      return ((cacheSize=objectCache.size()) > 0) ?
                                              getMessage(cacheSize-1) : null;
    }
  }

  /**
   * Return the last message number.
   * @return The last message number, or 0 if none have been entered.
   */
  public long getLastMsgNum()
  {
    return lastMsgNumTracker;
  }

  /**
   * Returns the prefix to use for log output.
   * @returns the prefix to use for log output.
   */
  protected String getLogPrefixString()
  {
    return "IstiMessageObjectCache";
  }

  /**
   * Returns the message entry at the given index.
   * @param index index of the entry.
   * @return The message entry object.
   * @throws ArrayIndexOutOfBoundsException if the index is negative
   * or not less than the current size of the cache.
   */
  protected MessageObjectEntry getMessage(int index)
  {
    return (MessageObjectEntry)objectCache.elementAt(index);
  }

  /**
   * Gets the message time for the specified message number.
   * @param msgNum the message number to use.
   * @return The message time or 0 if not found.
   */
  protected long getMsgTime(long msgNum)
  {
    for (int i = objectCache.size() - 1; i >= 0; i--)
    {
      final MessageObjectEntry objectEntry = getMessage(i);
      final long objectMsgNum = objectEntry.getMsgNum();

      if (objectMsgNum == msgNum)
        return objectEntry.getTimeGenerated();

      if (objectMsgNum < msgNum)
        break;
    }
    return 0;
  }

  /**
   * Retrieves a sub list of objects.  Any specified begin or end message
   * number is only used if the given begin or end time matches.  Unlike
   * the time values, the begin and end message numbers are considered
   * non-inclusive.    The thread-sychronization lock for 'objectCache'
   * should be held before this method is used.
   * @param beginTime minimum time (in milliseconds, inclusive) for
   * returned objects, or 0 for start of cache.
   * @param beginMsgNum begin message number (objects with message
   * numbers after this one are to be returned).
   * @param endTime maximum time (in milliseconds, inclusive) for
   * returned objects, or 0 for end of cache.
   * @param endMsgNum end message number (objects with message numbers
   * before this one are to be returned).
   * @param maxCount the maximum number of message objects to be returned,
   * or 0 for no limit.
   * @return A new 'VectorWithCount' of 'MessageObjectEntry' objects that
   * also holds the requested number of items in its 'count' field.
   */
  protected VectorWithCount subListVector(long beginTime, long beginMsgNum,
                                 long endTime, long endMsgNum, int maxCount)
  {
    try
    {
      final int cacheSize;
      if((cacheSize=objectCache.size()) > 0)
      {  //cache not empty; get first message object in cache
        final MessageObjectEntry firstCacheObj = getMessage(0);
        boolean useMsgNum = false;  //default to not using message numbers
        if(firstCacheObj.getTimeGenerated() > lastMsgNumSeqBreakTime &&
                                      ((beginMsgNum > 0 && beginTime > 0) ||
                                            (endMsgNum > 0 && endTime > 0)))
        {     //cache contains no breaks in msgNum sequence and
              // both message number and time provided for begin or end
          useMsgNum = true;    //may use message number
          if(beginTime > 0)
          {     //begin time was specified
            if(beginMsgNum <= 0 || getMsgTime(beginMsgNum) != beginTime)
            {      //begin msgNum not given or begin time doesn't
                   // match time in message object for msgNum
              useMsgNum = false;       //don't use message numbers
            }
          }
          if(endTime > 0)
          {     //end time was specified
            if(endMsgNum <= 0 || getMsgTime(endMsgNum) != endTime)
            {      //end msgNum not given or end time doesn't
                   // match time in message object for msgNum
              useMsgNum = false;       //don't use message numbers
            }
          }
        }
        if(!useMsgNum)
        {     //not using message numbers; fetch via time values
          final VectorWithCount vec = getObjects(beginTime,endTime,maxCount);
          final int vecSize;
          if((vecSize=vec.size()) > 0)
          {   //return vector contains items
                   //if begin message-number and time were given and can
                   // be matched to item at or near beginning of vector
                   // then remove it and all items before it:
            if(beginMsgNum > 0 && beginTime > 0)
            {
              int i = 0;
              Object obj;
              MessageObjectEntry entryObj;
              long timeVal;
              do
              {    //for each item in to vector to be searched
                if((obj=vec.elementAt(i)) instanceof MessageObjectEntry)
                {  //fetch item is of type 'MessageObjectEntry'
                  entryObj = (MessageObjectEntry)obj;      //set handle
                  if((timeVal=entryObj.getTimeGenerated()) <= beginTime)
                  {     //item time value is at or before begin time
                    if(entryObj.getMsgNum() == beginMsgNum &&
                                                       timeVal == beginTime)
                    {   //item msgNum and time match begin msgNum and time
                      if(vec.getCount() > i)     //if requested-count large
                        vec.addToCount(-i-1);    // enough then decrease it
                      do     //remove matching item and any before it
                        vec.removeElementAt(i);
                      while(--i >= 0);
                      break;      //exit loop
                    }
                  }
                }
              }
              while(++i < vecSize);
            }
                   //if end message-number and time were given and can
                   // be matched to item at or near end of vector
                   // then remove it and all items after it:
            if(endMsgNum > 0 && endTime > 0)
            {
              int i = vecSize - 1;
              Object obj;
              MessageObjectEntry entryObj;
              long timeVal;
              do
              {    //for each item in to vector to be searched
                if((obj=vec.elementAt(i)) instanceof MessageObjectEntry)
                {  //fetch item is of type 'MessageObjectEntry'
                  entryObj = (MessageObjectEntry)obj;      //set handle
                  if((timeVal=entryObj.getTimeGenerated()) >= endTime)
                  {     //item time value is at or after end time
                    if(entryObj.getMsgNum() == endMsgNum &&
                                                         timeVal == endTime)
                    {   //item msgNum and time match end msgNum and time
                      int cnt = vecSize - i;     //calc # to be removed
                      if(vec.getCount() >= cnt)  //if requested-count large
                        vec.addToCount(-cnt);    // enough then decrease it
                      do     //remove matching item and any after it
                        vec.removeElementAt(i);
                      while(--cnt > 0);
                      break;      //exit loop
                    }
                  }
                }
              }
              while(--i >= 0);
            }
          }
          return vec;
        }

                                       //get begin msgNum for cache:
        final long cacheBeginMsgNum = firstCacheObj.getMsgNum();
                                       //get end msgNum for cache:
        final long cacheEndMsgNum = getMessage(cacheSize-1).getMsgNum();
                                       //calc msgNum difference for cache:
        final long cacheDiffMsgNum = cacheEndMsgNum - cacheBeginMsgNum;

              //find index for first item to be returned:
        int firstIndex = -1;
        if(beginMsgNum >= cacheBeginMsgNum)
        {     //given begin msgNum is >= cache begin msgNum
          int i;
          if(cacheDiffMsgNum > 0)
          {   //cache-msgNum-diff value OK
                   //calculate index for item based on msgNum values:
            if((i=(int)((double)cacheSize/cacheDiffMsgNum*
                               (beginMsgNum-cacheBeginMsgNum))) < cacheSize)
            {      //calculated index value not too large
              if(getMessage(i).getMsgNum() > beginMsgNum)
              {    //msgNum is after begin msgNum
                        //scan backward until at or before begin msgNum:
                while(--i >= 0 &&
                              getMessage(i).getMsgNum() > beginMsgNum);
                firstIndex = i + 1;         //save index to item
              }
              else
                ++i;       //increment before scanning forward below
            }
            else   //calculated index value too large
              i = 0;
          }
          else     //total-cache-msgNum too small
            i = 0;
          if(firstIndex < 0)
          {   //index value not yet found; scan forward to find item
            while(i < cacheSize)
            {   //loop until after begin msgNum or end of cache
              if(getMessage(i).getMsgNum() > beginMsgNum)
              {      //item msgNum greater than begin msgNum
                firstIndex = i;     //save index to item
                break;              //exit loop
              }
              ++i;
            }
          }
        }
        else  //given begin msgNum < cache begin msgNum
          firstIndex = 0;         //use first item in cache

              //find index for last item to be returned:
        int lastIndex = -1;
        if(endMsgNum > 0 && endMsgNum <= cacheEndMsgNum)
        {     //end msgNum was given and is <= cache end msgNum
          if(endMsgNum >= cacheBeginMsgNum)
          {   //given end msgNum is >= cache begin msgNum
            int i;
            if(cacheDiffMsgNum > 0)
            {   //cache-msgNum-diff value OK
                     //calculate index for item based on msgNum values:
              if((i=(int)((double)cacheSize/cacheDiffMsgNum*
                                 (endMsgNum-cacheBeginMsgNum))) < cacheSize)
              {      //calculated index value not too large
                if(getMessage(i).getMsgNum() < endMsgNum)
                {    //msgNum is before end msgNum
                          //scan forward until at or after end msgNum:
                  while(++i < cacheSize &&
                                getMessage(i).getMsgNum() < endMsgNum);
                  lastIndex = i - 1;         //save index to item
                }
                else
                  --i;       //decrement before scanning backward below
              }
              else   //calculated index value too large
                i = cacheSize - 1;
            }
            else     //total-cache-msgNum too small
              i = cacheSize - 1;
            if(lastIndex < 0)
            {   //index value not yet found; scan backward to find item
              while(i >= 0)
              {   //loop until before end msgNum or beginning of cache
                if(getMessage(i).getMsgNum() < endMsgNum)
                {      //item msgNum less than end msgNum
                  lastIndex = i;     //save index to item
                  break;              //exit loop
                }
                --i;
              }
            }
          }
        }
        else  //given end msgNum is after cache end msgNum
          lastIndex = cacheSize - 1;        //use last item in cache

        if (firstIndex >= 0 && lastIndex >= firstIndex)
        {     //both index values were found and are OK
                   //calculate number of items requested:
          final int reqCount = lastIndex - firstIndex + 1;
                   //if max-count limit given and total is greater than
                   // max-count limit then adjust end index:
          if(maxCount > 0 && lastIndex-firstIndex+1 > maxCount)
            lastIndex = firstIndex + maxCount - 1;
                   //return copy of sub-list from cache:
          return new VectorWithCount(            //(with requested count)
              objectCache.getValuesVector(firstIndex,lastIndex+1),reqCount);
        }
      }
    }
    catch (Exception ex)
    {
      if (logObj != null)  //if logging
      {
        logObj.warning(getLogPrefixString() +
                                        " error accessing cache:  " + ex);
      }
    }
    return new VectorWithCount();      //return empty list
  }

  /**
   * Returns the time for the last detected break in the message number
   * sequence for messages added to the cache.
   * @return The time for the last detected break in the message number
   * sequence, or 0 if no breaks were detected.
   */
  public long getLastMsgNumSeqBreakTime()
  {
    return lastMsgNumSeqBreakTime;
  }

  /**
   * Reads the last message number from the message-number file.
   */
  protected void readMsgNumFile()
  {
    if (msgNumFileName == null)  //if message number filename was not specified
      return;  //exit

    synchronized (msgNumFileSyncObj)
    {    //get thread-sync lock for message-number file
      try
      {
        final String msgNumStr =       //read msg-number string from file
                          FileUtils.readFileToString(msgNumFileName).trim();
              //convert to integer; if no data then interpret as zero:
        lastMsgNumTracker = (msgNumStr.length() > 0) ?
                                              Long.parseLong(msgNumStr) : 0;
        if(logObj != null)
        {
          logObj.info(getLogPrefixString() +
                           " last message number was " + lastMsgNumTracker);
        }
      }
      catch (Exception ex)
      {
        if(logObj != null)
        {
          logObj.warning(getLogPrefixString() +
                             " error reading last message number:  " + ex);
        }
      }
    }
  }

  /**
   * Writes the last message number to the message-number file.
   */
  protected void writeMsgNumFile()
  {
    synchronized (msgNumFileSyncObj)
    {    //get thread-sync lock for message-number file
      if(msgNumRAFileObj != null)
      {    //message-number file is open
        if(logObj != null)
        {
          logObj.debug3(getLogPrefixString() +
                            " last message number is " + lastMsgNumTracker);
        }
        try
        {
          msgNumRAFileObj.setLength(0);     //clear current data
                                            //write number to file:
          msgNumRAFileObj.writeBytes(Long.toString(lastMsgNumTracker));
          msgNumFDescObj.sync();            //flush data out to file
        }
        catch (Exception ex)
        {
          if(logObj != null)
          {
            logObj.warning(getLogPrefixString() +
                               " error writing last message number:  " + ex);
          }
        }
      }
    }
  }

  /**
   * Deallocates resources associated with the cache.
   */
  public void close()
  {
    super.close();
    synchronized(msgNumFileSyncObj)
    {    //get thread-sync lock for message-number file
      if(msgNumRAFileObj != null)
      {  //message-number file is open
        try { msgNumRAFileObj.close(); }
        catch(IOException ex) {}
        msgNumRAFileObj = null;
      }
    }
    if(logObj != null)
      logObj.debug(getLogPrefixString() + ":  Cache closed");
  }


  /**
   * Interface for message object entry.
   */
  public interface MessageObjectEntry
                                 extends IstiTimeObjectCache.TimeObjectEntry
  {
    /**
     * Returns the message number.
     * @returns the message number.
     */
    public long getMsgNum();
  }


  /**
   * Message object entry class.
   */
  public static class BasicMessageObjectEntry extends BasicTimeObjectEntry
                                               implements MessageObjectEntry
  {
    protected long msgNum;

    /**
     * Creates a message object entry.
     * @param timeGenerated the time the message was generated.
     * @param msgObj message object.
     * @param msgStr message string.
     * @param msgNum message number.
     */
    public BasicMessageObjectEntry(
        long timeGenerated, Object msgObj, String msgStr, long msgNum)
    {
      super(timeGenerated, msgObj, msgStr);
      this.msgNum = msgNum;
    }

    /**
     * No-argument constructor for subclasses.
     */
    protected BasicMessageObjectEntry()
    {
    }

    /**
     * Returns the message number.
     * @returns the message number.
     */
    public long getMsgNum()
    {
      return msgNum;
    }
  }
}
