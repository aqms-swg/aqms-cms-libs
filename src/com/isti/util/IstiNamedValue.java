//IstiNamedValue.java:  Defines a named value object.
//
//    3/6/2006 -- [KF]  Initial version.
//    7/8/2006 -- [KF]  Added 'IstiNamedValue(String)' constructor.
//

package com.isti.util;

import java.awt.Color;

/**
 * Class IstiNamedValue defines a named value object.
 */
public class IstiNamedValue implements IstiNamedValueInterface
{
  private final String name; //value name
  private Object defaultValue; //default value
  private Object value; //value

  private String valueString = null; //the value string

  /**
   * Creates a value.
   * @param str the string representation for this object.
   */
  public IstiNamedValue(String str)
  {
    final int sepChIndex = str.indexOf(sepCh);
    if (sepChIndex < 0)  //if no separator character
    {
      name = str;
      valueString = null;
    }
    else
    {
      name = str.substring(0, sepChIndex);
      valueString = str.substring(sepChIndex+1);;
    }
    value = defaultValue = valueString;
  }

  /**
   * Creates a value.
   * @param valueName the value name.
   * @param defaultValueObj the default value object.
   */
  public IstiNamedValue(String valueName, Object defaultValueObj)
  {
    this(valueName, defaultValueObj, null);
  }

  /**
   * Creates a value.
   * @param valueName the value name.
   * @param defaultValueObj the default value object.
   * @param valueObj the value object or null for the default.
   */
  public IstiNamedValue(String valueName, Object defaultValueObj,
                        Object valueObj)
  {
    name = valueName;
    defaultValue = defaultValueObj;
    value = defaultValue;
    if (valueObj != null)
      setValue(valueObj);
  }

  /**
   * Creates and returns a clone of this object.
   * @return a clone of the this object.
   */
  public Object clone()
  {
    return new IstiNamedValue(name,defaultValue,value);
  }

  /**
   * Returns the default value object for the value.
   * @return the default value object for the value.
   */
  public synchronized Object getDefaultValue()
  {
    return defaultValue;
  }

  /**
   * Returns the name of the value.
   * @return the name of the value.
   */
  public String getName()
  {
    return name;
  }

  /**
   * Returns the value object for the value.
   * @return the value object for the value.
   */
  public synchronized Object getValue()
  {
    return value;
  }

  /**
   * Determines if the current value is the same as the default value.
   * @return true if the current value is the same as the default value.
   */
  public boolean isDefaultValue()
  {
    if (value == defaultValue)
      return true;
    if (value == null || defaultValue == null)
      return false;
    return value.equals(defaultValue);
  }

  /**
   * Sets the value object for the value.
   * @param valueObj the value object.
   * @return true if successful, false if given object's type does
   * not match the item's default-value object's type.
   */
  public synchronized boolean setValue(Object valueObj)
  {
    valueObj = checkValue(valueObj);  //check the value object
    if (valueObj == null)
      return false; //if new value not OK then return error
    if (valueObj.equals(value))
      return true; //if value not changing then return OK
    //if validator present then validate new value:
    //new value is OK; enter it:
    value = valueObj; //accept new object
    valueString = null;  //clear the value string
    return true; //return OK flag
  }

  /**
   * Sets the value for the value.
   * The string is converted to an object of the
   * same type as the given default value object for named value.
   * @param str the string value to interpret.
   * @return true if successful, false if the string could not be
   * converted to the proper type.
   */
  public boolean setValueString(String str)
  {
    final Object valueObj = setValueString(str, defaultValue);
    if (valueObj != null)
      setValue(valueObj);
    return valueObj != null;
  }

  /**
   * Interprets the given string and enters it as the value object for
   * the property item.  The string is converted to an object of the
   * same type as the given default value object for the property item.
   * @param str the string value to interpret.
   * @param defValObj the default value object to use for type checking
   * (this parameter allows for multiple values and types to be dealt
   * with).
   * @return an Object filled via the given string, or null if
   * the string could not be converted to the proper type.
   */
  public static Object setValueString(String str,Object defValObj)
  {
    final Object obj;
    boolean boolTypeFlag = false;

    if(str == null || defValObj == null)
      return null;           //if no data then return error
    try
    {    //determine data type and convert string accordingly
      if(defValObj instanceof Integer)
        obj = UtilFns.parseInteger(str);
      else if(defValObj instanceof Long)
        obj = UtilFns.parseLong(str);
      else if(defValObj instanceof Float)
        obj = Float.valueOf(str);
      else if(defValObj instanceof Double)
        obj = Double.valueOf(str);
      else if(defValObj instanceof Byte)
        obj = Byte.valueOf(str);
      else if(defValObj instanceof Short)
        obj = Short.valueOf(str);
      else if(defValObj instanceof String)
        obj = str;      //if string type then just copy it
      else if(defValObj instanceof Boolean)
      {
        obj = Boolean.valueOf(str);
        boolTypeFlag = true;           //indicate boolean type
      }
      else if(defValObj instanceof BooleanCp)
      {
        obj = new BooleanCp(str);
        boolTypeFlag = true;           //indicate boolean type
      }
      else if (defValObj instanceof Color)
      {
        obj = UtilFns.parseColor(str);
      }
      else if (defValObj instanceof Archivable)
      {
        obj = UtilFns.parseArchivable((Archivable)defValObj, str);
      }
      else
      {
        obj = UtilFns.parseObject(defValObj,str);
      }
         //if boolean type then check for proper keywords:
      if(boolTypeFlag && !str.equalsIgnoreCase("true") &&
                                             !str.equalsIgnoreCase("false"))
        return null;         //if bad then return error
    }
    catch(NumberFormatException ex)
    { return null; }    //if error converting then return
    return obj;                   //return new value object
  }

  /**
   * Returns a String object representing the value.
   * @return the String value.
   */
  public synchronized String stringValue()
  {
    if (valueString == null)
      valueString = determineStringValue(value);
    return valueString;
  }

  /**
   * Returns a String object representing this object.
   * @return the String object.
   */
  public synchronized String toString()
  {
    String nameString = UtilFns.EMPTY_STRING;
    if (name != null)
      nameString = name + sepCh;
    return nameString + stringValue();
  }

  /**
   * Checks the value object.
   * @param valueObj the value object.
   * @return the value object or null if invalid value.
   */
  protected synchronized Object checkValue(Object valueObj)
  {
    if (defaultValue instanceof Archivable && valueObj instanceof String)
    {
      valueObj = UtilFns.parseObject(defaultValue, (String) valueObj);
    }
    if (valueObj == null || valueObj.getClass() != defaultValue.getClass())
      return null; //if new value not OK then return error
    return valueObj;
  }

  /**
   * Returns a String object representing the value.
   * @param value the value.
   * @return the String value.
   */
  protected static String determineStringValue(Object value)
  {
    if (value == null)
      return UtilFns.EMPTY_STRING;
    if (value instanceof Color)
      return UtilFns.colorToPropertyString( (Color) value);
    if (value instanceof Float || value instanceof Double)
      return UtilFns.floatNumberToPropertyString(value);
    if (! (value instanceof Object[])) //if not an array then
      return value.toString();
    final Object[] valueArr = (Object[]) value;
    final StringBuffer buff = new StringBuffer();
    if (valueArr.length > 0)
    { //at least one element in array
      int i = 0;
      while (true)
      { //for each element in array
        if (valueArr[i] != null) //if element not null then
          buff.append(valueArr[i]); //add to buffer
        if (++i >= valueArr.length)
          break; //if no more then exit loop
        buff.append(" "); //add separator
      }
    }
    return buff.toString();
  }
}
