//IstiNamedValueInterface.java:  Defines a named value object interface.
//
//    3/6/2006 -- [KF]  Initial version.
//

package com.isti.util;

/**
 * IstiNamedValueInterface defines a named value object interface.
 */
public interface IstiNamedValueInterface
{
  /** The separator character */
  public static final char sepCh = '=';

  /**
   * Creates and returns a clone of this object.
   * @return a clone of the this object.
   */
  public Object clone();

  /**
   * Returns the default value object for the value.
   * @return the default value object for the value.
   */
  public Object getDefaultValue();

  /**
   * Returns the name of the value.
   * @return the name of the value.
   */
  public String getName();

  /**
   * Returns the value object for the value.
   * @return the value object for the value.
   */
  public Object getValue();

  /**
   * Determines if the current value is the same as the default value.
   * @return true if the current value is the same as the default value.
   */
  public boolean isDefaultValue();

  /**
   * Sets the value object for the value.
   * @param valueObj the value object.
   * @return true if successful, false if given object's type does
   * not match the item's default-value object's type.
   */
  public boolean setValue(Object valueObj);

  /**
   * Sets the value for the value.
   * The string is converted to an object of the
   * same type as the given default value object for named value.
   * @param str the string value to interpret.
   * @return true if successful, false if the string could not be
   * converted to the proper type.
   */
  public boolean setValueString(String str);

  /**
   * Returns a String object representing the value.
   * @return the String value.
   */
  public String stringValue();
}
