//IstiNotifyThread.java:  Extension of 'Thread' that adds wait/notify
//                        and terminate functionality.
//
//   4/6/2004 -- [ET]  Created by building upon 'IstiThread' class.
// 10/19/2004 -- [ET]  Modified to extend 'IstiThread'; modified
//                     'terminate()' method to check if notify interrupt
//                     was successful and call parent 'terminate()'
//                     if not; deprecated 'sleep()' method.
//

package com.isti.util;

/**
 * Class IstiThread extends 'Thread' to add wait/notify and terminate
 * functionality.  Subclasses should use the 'waitForNotify()' method
 * to perform thread-sleep delays.
 */
public class IstiNotifyThread extends IstiThread
{
  /**
   * Allocates a new <code>IstiNotifyThread</code> object. This constructor
   * has the same effect as <code>IstiNotifyThread(null, target, name)</code>.
   *
   * @param   target   the object whose <code>run</code> method is called.
   * @param   name     the name of the new thread.
   */
  public IstiNotifyThread(Runnable target, String name)
  {
    this(null, target, name);
  }

  /**
   * Allocates a new <code>IstiNotifyThread</code> object. This constructor
   * has the same effect as <code>IstiNotifyThread(null, null, name)</code>.
   *
   * @param   name   the name of the new thread.
   */
  public IstiNotifyThread(String name)
  {
    this(null, null, name);
  }

  /**
   * Allocates a new <code>IstiNotifyThread</code> object so that it has
   * <code>target</code> as its run object, has the specified
   * <code>name</code> as its name, and belongs to the thread group
   * referred to by <code>group</code>.
   * <p>
   * If <code>group</code> is <code>null</code>, the group is
   * set to be the same ThreadGroup as
   * the thread that is creating the new thread.
   *
   * <p>If there is a security manager, its <code>checkAccess</code>
   * method is called with the ThreadGroup as its argument.
   * This may result in a SecurityException.
   * <p>
   * If the <code>target</code> argument is not <code>null</code>, the
   * <code>run</code> method of the <code>target</code> is called when
   * this thread is started. If the target argument is
   * <code>null</code>, this thread's <code>run</code> method is called
   * when this thread is started.
   * <p>
   * The priority of the newly created thread is set equal to the
   * priority of the thread creating it, that is, the currently running
   * thread. The method <code>setPriority</code> may be used to
   * change the priority to a new value.
   * <p>
   * The newly created thread is initially marked as being a daemon
   * thread if and only if the thread creating it is currently marked
   * as a daemon thread. The method <code>setDaemon </code> may be used
   * to change whether or not a thread is a daemon.
   *
   * @param      group     the thread group.
   * @param      target   the object whose <code>run</code> method is called.
   * @param      name     the name of the new thread.
   * @exception  SecurityException  if the current thread cannot create a
   *               thread in the specified thread group.
   */
  public IstiNotifyThread(ThreadGroup group, Runnable target, String name)
  {
    super(group, target, name);
  }

  /**
   * Allocates a new <code>IstiNotifyThread</code> object. This constructor has
   * the same effect as <code>IstiNotifyThread(group, null, name)</code>
   *
   * @param      group   the thread group.
   * @param      name    the name of the new thread.
   * @exception  SecurityException  if the current thread cannot create a
   *               thread in the specified thread group.
   */
  public IstiNotifyThread(ThreadGroup group, String name)
  {
    this(group, null, name);
  }

  /**
   * Performs a thread-notify and terminates this thread if the thread
   * is not terminated and alive.
   */
  public void terminate()
  {
    if(!terminateFlag && isAlive())
    {    //thread is not terminated and is alive
      terminateFlag = true;            //set terminate flag
      notifyThread();                  //wake up 'waitforNotify()' method
      waitForTerminate();              //wait for thread to terminate
      if(isAlive())               //if thread is still alive then
        super.terminate();        //use parent class terminate method
    }
  }

  /**
   * This method is deprecated because it will not be interrupted by
   * the 'notifyThread()' method.  The 'waitForNotify()' method should
   * be used instead for thread-sleep delays.
   * Causes the currently executing thread to sleep (temporarily cease
   * execution) for the specified number of milliseconds. The thread
   * does not lose ownership of any monitors.
   *
   * @param      millis   the length of time to sleep in milliseconds.
   * @exception  InterruptedException if another thread has interrupted
   *             the current thread.  The <i>interrupted status</i> of the
   *             current thread is cleared when this exception is thrown.
   * @see        java.lang.Object#notify()
   * @deprecated Use 'waitForNotify()' instead.
   */
  public static void sleep(long millis) throws InterruptedException
  {
    IstiThread.sleep(millis);
  }
}
