package com.isti.util;

/**
 * The ISTI row filter.
 */
public interface IstiRowFilter {
  /**
   * Determines if the row is visible.
   * @param rowIndex the actual row index.
   * @return true if the row is visible, false otherwise.
   */
  public boolean isRowVisible(int rowIndex);
}
