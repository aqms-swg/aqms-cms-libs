//IstiSound:  Sound utility class.
//
// 12/12/2002 -- [KF]  Initial version.
//   1/1/2004 -- [ET]  Added 'close()', 'reopen()' and 'isOpen()' methods;
//                     replaced use of 'Calendar' object with long-integer
//                     ('Calendar' object is rather "heavy").
//

package com.isti.util;

import java.io.File;
import java.io.InputStream;
import java.io.IOException;
import java.net.URL;
import javax.sound.sampled.*;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

/**
 * Class IstiSound is a sound utility class that manages a sound 'Clip'
 * object.
 * The ActionListener interface is implemented to support the use of a
 * javax.swing.Timer to play the sound.
 */
public class IstiSound implements LineListener, ActionListener
{
  protected final Clip clipObj;             //sound clip
  protected long stopPlayTimeVal = 0;       //time to play song until reached

  /**
   * Creates a sound from the specified audio input stream.
   * @param audiosource audio input stream
   * @throws IOException if an I/O exception occurs
   * @throws LineUnavailableException if a matching line is not available due
   *  to resource restrictions
   */
  public IstiSound(AudioInputStream audiosource)
                                 throws IOException,LineUnavailableException
  {
    final DataLine.Info info =
        new DataLine.Info(Clip.class, audiosource.getFormat());
    clipObj = (Clip)AudioSystem.getLine(info);
    clipObj.open(audiosource);
    clipObj.addLineListener(this);
    stopPlayTimeVal = 0;
  }

  /**
   * Creates a sound from the specified sound file.
   * @param file the sound file
   * @throws UnsupportedAudioFileException if the File does not point to a
   *  valid sound file data recognized by the system
   * @throws IOException if an I/O exception occurs
   * @throws LineUnavailableException if a matching line is not available due
   *  to resource restrictions
   * @see play
   */
  public IstiSound(File file) throws UnsupportedAudioFileException,
                                        IOException,LineUnavailableException
  {
    this(AudioSystem.getAudioInputStream(file));
  }

  /**
   * Creates a sound from the specified sound file.
   * @param url URL for the sound file
   * @throws UnsupportedAudioFileException if the File does not point to a
   *  valid sound file data recognized by the system
   * @throws IOException if an I/O exception occurs
   * @throws LineUnavailableException if a matching line is not available due
   *  to resource restrictions
   * @see play
   */
  public IstiSound(URL url) throws UnsupportedAudioFileException,
                                        IOException,LineUnavailableException
  {
    this(AudioSystem.getAudioInputStream(url));
  }

  /**
   * Creates a sound from the specified input stream.
   * @param stmObj the input stream to use.
   * @throws UnsupportedAudioFileException if the File does not point to a
   *  valid sound file data recognized by the system
   * @throws IOException if an I/O exception occurs
   * @throws LineUnavailableException if a matching line is not available due
   *  to resource restrictions
   * @see play
   */
  public IstiSound(InputStream stmObj) throws UnsupportedAudioFileException,
                                        IOException,LineUnavailableException
  {
    this(AudioSystem.getAudioInputStream(stmObj));
  }

  /**
   * Reopens to use the sound from the specified audio input stream.
   * The format of new sound must be the same as the previous sound.
   * @param audiosource audio input stream
   * @throws IOException if an I/O exception occurs
   * @throws LineUnavailableException if a matching line is not available
   * due to resource restrictions
   */
  public void reopen(AudioInputStream audiosource)
                                 throws IOException,LineUnavailableException
  {
    stopPlayTimeVal = 0;
    if(clipObj.isOpen())
      clipObj.close();
    clipObj.open(audiosource);
  }

  /**
   * Reopens to use the sound from the specified sound file.
   * The format of new sound must be the same as the previous sound.
   * @param file the sound file
   * @throws UnsupportedAudioFileException if the File does not point to a
   *  valid sound file data recognized by the system
   * @throws IOException if an I/O exception occurs
   * @throws LineUnavailableException if a matching line is not available due
   *  to resource restrictions
   * @see play
   */
  public void reopen(File file) throws UnsupportedAudioFileException,
                                        IOException,LineUnavailableException
  {
    reopen(AudioSystem.getAudioInputStream(file));
  }

  /**
   * Reopens to use the sound from the specified sound file.
   * The format of new sound must be the same as the previous sound.
   * @param url URL for the sound file
   * @throws UnsupportedAudioFileException if the File does not point to a
   *  valid sound file data recognized by the system
   * @throws IOException if an I/O exception occurs
   * @throws LineUnavailableException if a matching line is not available due
   *  to resource restrictions
   * @see play
   */
  public void reopen(URL url) throws UnsupportedAudioFileException,
                                        IOException,LineUnavailableException
  {
    reopen(AudioSystem.getAudioInputStream(url));
  }

  /**
   * Reopens to use the sound from the specified input stream.
   * The format of new sound must be the same as the previous sound.
   * @param stmObj the input stream to use.
   * @throws UnsupportedAudioFileException if the File does not point to a
   *  valid sound file data recognized by the system
   * @throws IOException if an I/O exception occurs
   * @throws LineUnavailableException if a matching line is not available due
   *  to resource restrictions
   * @see play
   */
  public void reopen(InputStream stmObj)
                                       throws UnsupportedAudioFileException,
                                        IOException,LineUnavailableException
  {
    reopen(AudioSystem.getAudioInputStream(stmObj));
  }

  /**
   * Checks if the specified file is a supported sound file.
   * @param file the sound file
   * @return true if the file is supported
   */
  public static boolean isSoundFile(File file)
  {
    try
    {
      AudioSystem.getAudioFileFormat(file);
    }
    catch (Exception ex)
    {
      return false;
    }
    return true;
  }

  /**
   * Checks if the specified file is a supported sound file.
   * @param url URL for the sound file
   * @return true if the file is supported
   */
  public static boolean isSoundFile(URL url)
  {
    try
    {
      AudioSystem.getAudioFileFormat(url);
    }
    catch (Exception ex)
    {
      return false;
    }
    return true;
  }

  /**
   * Checks if a sound is active
   * @return true if a sound is active
   */
  public synchronized boolean isActive()
  {
    return clipObj.isActive();
  }

  /**
   * Plays a sound continuously.
   */
  public synchronized void loop()
  {
    clipObj.loop(Clip.LOOP_CONTINUOUSLY);
  }

  /**
   * Plays a sound the specified number of times.
   * @param count number of times to play the sound
   */
  public synchronized void loop(int count)
  {
    clipObj.loop(count);
  }

  /**
   * Plays a sound from start to finish.
   * This is the same as calling reset and then start.
   * @param waitFlag if true then this method blocks until the playing
   * of the sound is complete.
   * @see reset, start
   */
  public synchronized void play(boolean waitFlag)
  {
    reset();            //reset sound to the beginning.
    start();            //play the sound
    if(waitFlag)
    {    //flag set; wait for complete
      while(isActive())
      {  //loop while waiting for sound complete
        try { Thread.sleep(10); }      //delay to give CPU a break
        catch(InterruptedException ex) {}
      }
    }
  }

  /**
   * Plays a sound from start to finish.
   * This is the same as calling reset and then start.
   * @see reset, start
   */
  public synchronized void play()
  {
    play(false);
  }

  /**
   * Plays a sound for the specified duration.
   * @param secondsVal play duration in seconds.
   */
  public synchronized void play(int secondsVal)
  {
         //check if already playing sound:
    final boolean firstTimeFlag = (stopPlayTimeVal == 0);

         //update the end time:
    stopPlayTimeVal = System.currentTimeMillis() + (secondsVal * 1000L);

    if (firstTimeFlag)       //if playing the sound for the first time
      play(false);           // then start playing now
  }

  /**
   * Supports the LineListener interface.
   * Invoked when a line's status changes.
   * @param lineEvent line event
   */
  public synchronized void update(LineEvent lineEvent)
  {
    final LineEvent.Type type = lineEvent.getType();

    if (type.equals(LineEvent.Type.STOP))
    {    //playing of clip just finished
      if (System.currentTimeMillis() < stopPlayTimeVal)
        play(false);         //if more time left then play clip again
      else                        //if no more time then
        stopPlayTimeVal = 0;      //cancel duration play
    }
//    else if (type.equals(LineEvent.Type.CLOSE)) {}
//    else if (type.equals(LineEvent.Type.OPEN)) {}
//    else if (type.equals(LineEvent.Type.START)) {}
  }

  /**
   * Supports the ActionListener interface.
   * Invoked when an action occurs.
   * @param e action event.
   */
  public synchronized void actionPerformed(ActionEvent e)
  {
    play(false);             //play the sound
  }

  /**
   * Resets a sound to the beginning.
   */
  public synchronized void reset()
  {
    clipObj.setFramePosition(0);       //go to beginning
  }

  /**
   * Starts or resumes playing a sound from the current position.
   */
  public synchronized void start()
  {
    clipObj.start();                   //start playing the clip
  }

  /**
   * Stops playing a sound.
   */
  public synchronized void stop()
  {
    stopPlayTimeVal = 0;          //cancel duration play
    clipObj.stop();               //stop playing the clip
  }

  /**
   * Release resources associated with this sound object.  The sound
   * cannot be played after this method is called.
   */
  public synchronized void close()
  {
    stopPlayTimeVal = 0;          //cancel any duration play
    if(clipObj.isActive())        //if playing then
      clipObj.stop();             //stop playing
    clipObj.close();              //release clip resources
  }

  /**
   * Queries whether or not this sound object is open.
   * @return true if open, false if not.
   */
  public synchronized boolean isOpen()
  {
    return clipObj.isOpen();
  }
}
