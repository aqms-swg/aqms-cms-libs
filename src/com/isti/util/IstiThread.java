//IstiThread.java:  Extends a thread to add additional functionality and
//                  to ensure the thread is given a name.
//
//                  See also the 'IstiNotifyThread' class that works better
//                  under the Solaris OS as it does not cause problems with I/O.
//
//  10/03/2003 -- [KF]  Initial version.
//  10/06/2003 -- [ET]  Added explict "= 0" initial value to
//                      'threadInitNumber' declaration.
//  10/12/2004 -- [KF]  Added 'wasStarted()' method.
//  10/18/2004 -- [KF]  Added 'notifyThread()', 'clearWakeupFlag()',
//                      'waitForTerminate()' and 'waitForNotify()'
//                      methods.
//  10/19/2004 -- [ET]  Fixed some of the 'waitForNotify()' return
//                      values and modified so as to not check the
//                      variable 'threadWaitNotifyFlag' twice;
//                      renamed 'clearNotifyIndicatorFlag()' method
//                      to 'clearThreadWaitNotifyFlag()'.
//   2/20/2007 -- [KF]  Changed so that if the thread is created on the event
//                      dispatch thread the priority of the newly created
//                      thread is set to the normal priority.
//   2/23/2007 -- [KF]  Added support to specify the maximum number of
//                      milliseconds to wait for the thread to terminate.
//   6/17/2016 -- [KF]  Modified for thread safety.
//

package com.isti.util;

import javax.swing.SwingUtilities;


/**
 * Class IstiThread extends a thread to add additional functionality and
 * to ensure the thread is given a name.
 */
public class IstiThread extends Thread
{
  /** The default terminate wait time in milliseconds. */
  public static int DEFAULT_TERMINATE_WAIT_TIME = 100;
  /** Flag set true if the thread should be terminated. */
  protected volatile boolean terminateFlag = false;
  /** For automatic numbering anonymous threads. */
  private static int threadInitNumber = 0;
  /** Thread synchronization object used for 'wait' and 'notify'. */
  private final Object threadWaitSyncObject = new Object();
  /** Flag set true after 'notifyThread()' method called. */
  private boolean threadWaitNotifyFlag = false;

  /** Start sync object. */
  private final Object startSyncObj = new Object();
  /** Started flag: true if the thread was already started, false otherwise. */
  private boolean startedFlag = false;

  /**
   * Returns (and increments) the next thread number for automatic numbering
   * anonymous threads.
   * @return the next thread number.
   */
  public static synchronized int nextThreadNum()
  {
    return threadInitNumber++;
  }

  /**
   * Allocates a new <code>IstiThread</code> object. This constructor has
   * the same effect as <code>IstiThread(null, target, name)</code>.
   *
   * @param   target   the object whose <code>run</code> method is called.
   * @param   name     the name of the new thread.
   */
  public IstiThread(Runnable target, String name)
  {
    this(null, target, name);
  }

  /**
   * Allocates a new <code>IstiThread</code> object. This constructor has
   * the same effect as <code>IstiThread(null, null, name)</code>.
   *
   * @param   name   the name of the new thread.
   */
  public IstiThread(String name)
  {
    this(null, null, name);
  }

  /**
   * Allocates a new <code>IstiThread</code> object so that it has
   * <code>target</code> as its run object, has the specified
   * <code>name</code> as its name, and belongs to the thread group
   * referred to by <code>group</code>.
   * <p>
   * If <code>group</code> is <code>null</code>, the group is
   * set to be the same ThreadGroup as
   * the thread that is creating the new thread.
   *
   * <p>If there is a security manager, its <code>checkAccess</code>
   * method is called with the ThreadGroup as its argument.
   * This may result in a SecurityException.
   * <p>
   * If the <code>target</code> argument is not <code>null</code>, the
   * <code>run</code> method of the <code>target</code> is called when
   * this thread is started. If the target argument is
   * <code>null</code>, this thread's <code>run</code> method is called
   * when this thread is started.
   * <p>
   * The priority of the newly created thread is normally set equal to the
   * priority of the thread creating it, that is, the currently running
   * thread. If the thread is created on the event dispatch thread the
   * priority of the newly created thread is set to the normal priority.
   * The method <code>setPriority</code> may be used to
   * change the priority to a new value.
   * <p>
   * The newly created thread is initially marked as being a daemon
   * thread if and only if the thread creating it is currently marked
   * as a daemon thread. The method <code>setDaemon </code> may be used
   * to change whether or not a thread is a daemon.
   *
   * @param      group     the thread group.
   * @param      target   the object whose <code>run</code> method is called.
   * @param      name     the name of the new thread.
   * @exception  SecurityException  if the current thread cannot create a
   *               thread in the specified thread group.
   */
  public IstiThread(ThreadGroup group, Runnable target, String name)
  {
    super(group, target, name);
    try
    {
      if (SwingUtilities.isEventDispatchThread())
      {
        if (Thread.currentThread().getPriority() > Thread.NORM_PRIORITY)
        {
          setPriority(Thread.NORM_PRIORITY);
        }
      }
    }
    catch (Throwable ex)
    { //error checking or setting priority; ignore and move on
    }
  }

  /**
   * Allocates a new <code>IstiThread</code> object. This constructor has
   * the same effect as <code>IstiThread(group, null, name)</code>
   *
   * @param      group   the thread group.
   * @param      name    the name of the new thread.
   * @exception  SecurityException  if the current thread cannot create a
   *               thread in the specified thread group.
   */
  public IstiThread(ThreadGroup group, String name)
  {
    this(group, null, name);
  }

  /**
   * Starts this thread if the thread is not terminated and not already alive.
   * @see wasStarted
   */
  public void start()
  {
    synchronized(startSyncObj)
    {
      if (startedFlag)  //exit if the thread was already started
        return;
      startedFlag = true;
    }
    //if the thread is not terminated and not already alive then start thread
    if (!terminateFlag && !isAlive())
      super.start();
  }

  /**
   * Terminates this thread if the thread is not terminated and alive.
   */
  public void terminate()
  {
    terminate(DEFAULT_TERMINATE_WAIT_TIME);
  }

  /**
   * Terminates this thread if the thread is not terminated and alive.
   * @param waitTimeMs the maximum number of milliseconds to wait for
   * the thread to terminate, or 0 to wait indefinitely.
   */
  public void terminate(long waitTimeMs)
  {
    //if the thread is not terminated and alive
    if (!terminateFlag && isAlive())
    {
      terminateFlag = true;            //set terminate flag
      try { interrupt(); }             //interrupt sleep
      catch (SecurityException ex) {}
      waitForTerminate(waitTimeMs);    //wait for thread to terminate
    }
  }

  /**
   * Tests if this thread is terminated.
   *
   * @return  <code>true</code> if this thread is terminated;
   *          <code>false</code> otherwise.
   */
  public boolean isTerminated()
  {
    return terminateFlag;
  }

  /**
   * Performs a thread wait.
   * If a notify has occurred since the last call to 'waitForNotify()'
   * or 'clearThreadWaitNotifyFlag()' then this method will return
   * immediately.
   */
  public void waitForNotify()
  {
    waitForNotify(0);
  }

  /**
   * Performs a thread wait, up to the given timeout value.
   * If a notify has occurred since the last call to 'waitForNotify()'
   * or 'clearThreadWaitNotifyFlag()' then this method will return
   * immediately.
   * @param waitTimeMs the maximum number of milliseconds to wait for
   * the thread-notify, or 0 to wait indefinitely.
   * @return true if the wait-timeout value was reached; false if a
   * thread-notify, thread-interrupt or thread-terminate occurred.
   */
  public boolean waitForNotify(long waitTimeMs)
  {
    try
    {
      synchronized(threadWaitSyncObject)
      {  //grab thread synchronization lock object
        if(!isTerminated())
        {     //thread has not been terminated
          if(!threadWaitNotifyFlag)
          {   //'notifyThread()' method not called
                   //wait until specified time, notify or interrupt:
            threadWaitSyncObject.wait(waitTimeMs);
            if(!threadWaitNotifyFlag)  //if notify was not called then
              return true;             //indicate thread-wait finished
          }
          threadWaitNotifyFlag = false;
        }
      }
    }
    catch(InterruptedException ex)
    {    //thread was interrupted
    }
    return false;       //indicate thread-wait did not finish
  }

  /**
   * Performs a thread-notify.  This will "wake up" the 'waitForNotify()'
   * method.
   */
  public void notifyThread()
  {
    synchronized(threadWaitSyncObject)
    {    //grab thread synchronization lock object
      threadWaitNotifyFlag = true;          //indicate notify called
      threadWaitSyncObject.notifyAll();     //notify on object
    }
  }

  /**
   * Clears the thread-wait notify flag.  This will clear any previous
   * "notifies" set via the 'notifyThread()' method.
   */
  public void clearThreadWaitNotifyFlag()
  {
    synchronized(threadWaitSyncObject)
    {
      threadWaitNotifyFlag = false;
    }
  }

  /**
   * Determines if the thread was already started.
   * @return true if the thread was already started, false otherwise.
   */
  public boolean wasStarted()
  {
    synchronized(startSyncObj)
    {
      return startedFlag;
    }
  }

  /**
   * Waits (up to 100 milliseconds) for the thread to terminate.
   */
  public void waitForTerminate()
  {
    waitForTerminate(DEFAULT_TERMINATE_WAIT_TIME);
  }

  /**
   * Waits (up to 'waitTimeMs' milliseconds) for the thread to terminate.
   * @param waitTimeMs the maximum number of milliseconds to wait for
   * the thread to terminate, or 0 to wait indefinitely.
   */
  public void waitForTerminate(long waitTimeMs)
  {
    try { join(waitTimeMs); }  //wait for thread to terminate
    catch(InterruptedException ex) {}
  }
}
