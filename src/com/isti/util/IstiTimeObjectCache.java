//IstiTimeObjectCache.java:  Time object cache.
//
//   5/5/2003 -- [KF]  Initial version.
//   6/5/2003 -- [ET]  Modified 'getObjects()' to only put in tolerance
//                     value on non-zero time values.
//   6/6/2003 -- [ET]  Changed 'log.debug()' outputs to 'log.debug3()'.
//  6/11/2003 -- [ET]  Added 'close()' method.
//  8/22/2003 -- [ET]  Modified to use a 'FifoHashtable' and to support
//                     the 'containsKey()' and 'containsObjectKey()'
//                     methods; added protected no-argument constructor
//                     to 'BasicTimeObjectEntry' class; changed
//                     'log.debug3()' outputs to 'log.debug5()'; added
//                     'setDefaultRemoveAge()' method.
//   9/4/2003 -- [ET]  Modified to support 'maxCount' limits.
//   9/8/2003 -- [ET]  Renamed 'dataStr' to 'keyStr'; added
//                     'getFirst/LastObject()' methods.
//   9/9/2003 -- [ET]  Fixed 'get' methods for 'endTime'==0; added 'size()'
//                     and 'isEmpty() methods.
//  9/17/2003 -- [ET]  Added log warning if item added with time value less
//                     than that of previous item.
//  7/14/2004 -- [ET]  Added 'containsEntry()' and 'getUseLookupKeyFlag()'
//                     methods.
//  7/22/2004 -- [ET]  Added return value to 'removeOldObjects()' method;
//                     added support for object-count and total-data-size
//                     limits.
//  12/8/2004 -- [ET]  Added 'getMessageCount()' and 'getCacheInformation()'
//                     methods.
//  1/31/2005 -- [ET]  Fixed 'totalCacheDataSize' tracking in method
//                     'removeOldObjects()'.
//  4/28/2010 -- [ET]  Lowered log level of "Time value of added item
//                     is less than time value of previous item" message
//                     from 'warning' to 'debug2'.
//

package com.isti.util;

import java.util.List;
import java.util.Collection;
import java.util.Vector;
import java.util.Iterator;

/**
 * Class IstiTimeObjectCache implements the time object cache.  The
 * 'removeOldObjects()' method should be called on a periodic basis
 * to remove old objects from the cache.
 */
public class IstiTimeObjectCache
{
  protected final LogFile logObj;
  protected final boolean useLookupKeyFlag;
  protected long tolerance = 0;
  protected long defaultRemoveAge = 0;
  protected int maximumObjectCount = 0;
  protected long maximumTotalDataSize = 0;
  protected long totalCacheDataSize = 0;
  protected final FifoHashtable objectCache = new FifoHashtable();
  protected long lastMsgTimeTracker = 0;

  /**
   * Constructs a time object cache with a default remove age.
   * @param logObj log file object to use or null for no logging.
   * @param tolerance number of milliseconds for time tolerance
   * that is used with time checks
   * @param removeAge number of milliseconds to keep objects in the cache
   * or 0 for no default.
   * @param useLookupKeyFlag true to use the "keyStr" parameter (when
   * adding) as a table-lookup key (which forces cache entries to have
   * unique "keyStr" values).
   */
  public IstiTimeObjectCache(LogFile logObj, long tolerance,
                                   long removeAge, boolean useLookupKeyFlag)
  {
    this.logObj = logObj;
    this.tolerance = tolerance;
    this.defaultRemoveAge = removeAge;
    this.useLookupKeyFlag = useLookupKeyFlag;
  }

  /**
   * Constructs a time object cache with a default remove age.  The
   * cache is setup to use the "keyStr" parameter (when adding) as
   * a table-lookup key (which forces cache entries to have unique
   * "keyStr" values).
   * @param logObj log file object to use or null for no logging.
   * @param tolerance number of milliseconds for time tolerance
   * that is used with time checks
   * @param removeAge number of milliseconds to keep objects in the cache
   * or 0 for no default.
   */
  public IstiTimeObjectCache(LogFile logObj, long tolerance, long removeAge)
  {
    this.logObj = logObj;
    this.tolerance = tolerance;
    this.defaultRemoveAge = removeAge;
    this.useLookupKeyFlag = true;
  }

  /**
   * Constructs a time object cache.
   * @param logObj log file object to use or null for no logging.
   * @param tolerance number of milliseconds for time tolerance
   * that is used with time checks
   * @param useLookupKeyFlag true to use the "keyStr" parameter (when
   * adding) as a table-lookup key (which forces cache entries to have
   * unique "keyStr" values).
   */
  public IstiTimeObjectCache(LogFile logObj, long tolerance,
                                                   boolean useLookupKeyFlag)
  {
    this(logObj, tolerance, 0, useLookupKeyFlag);
  }

  /**
   * Constructs a time object cache.  The cache is setup to use the
   * "keyStr" parameter (when adding) as a table-lookup key (which
   * forces cache entries to have unique "keyStr" values).
   * @param logObj log file object to use or null for no logging.
   * @param tolerance number of milliseconds for time tolerance
   * that is used with time checks
   */
  public IstiTimeObjectCache(LogFile logObj, long tolerance)
  {
    this(logObj, tolerance, 0, true);
  }

  /**
   * Adds the time entry.
   * @param timeGenerated the time the message was generated
   * @param dataObj data object.
   * @param keyStr message string.
   */
  public void add(long timeGenerated, Object dataObj, String keyStr)
  {
    add(new BasicTimeObjectEntry(timeGenerated, dataObj, keyStr));
  }

  /**
   * Retrieves a copy of all of the objects.
   * @return A new vector of objects (TimeObjectEntry).
   */
  public Vector getAllObjects()
  {
    return objectCache.getValuesVector();
  }

  /**
   * Retrieves a copy of the first 'n' objects.
   * @param numObjects the maximum number of objects to return.
   * @return A new vector of objects (TimeObjectEntry).
   */
  public Vector getFirstObjects(int numObjects)
  {
    if(numObjects > objectCache.size())     //if less avail than requested
      numObjects = objectCache.size();      // then reduce request
    return objectCache.getValuesVector(0,numObjects);
  }

  /**
   * Returns the first object in the cache.
   * @return The first ('TimeObjectEntry') object in the cache, or null
   * if the cache is empty.
   */
  public TimeObjectEntry getFirstObject()
  {
    synchronized(objectCache)
    {
      return (objectCache.size() > 0) ? getObject(0) : null;
    }
  }

  /**
   * Returns the last object in the cache.
   * @return The last ('TimeObjectEntry') object in the cache, or null
   * if the cache is empty.
   */
  public TimeObjectEntry getLastObject()
  {
    synchronized(objectCache)
    {
      final int cacheSize;
      return ((cacheSize=objectCache.size()) > 0) ?
                                              getObject(cacheSize-1) : null;
    }
  }

  /**
   * Returns the log file.
   * @return the log file object.
   */
  public LogFile getLogFile()
  {
    return logObj;
  }

  /**
   * Returns the 'useLookupKeyFlag' flag value.
   * @return true if the "keyStr" parameter is to be used (when adding)
   * as a table-lookup key (which forces cache entries to have unique
   * "keyStr" values); false if not.
   */
  public boolean getUseLookupKeyFlag()
  {
    return useLookupKeyFlag;
  }

  /**
   * Retrieves a copy of objects that are newer or equal to the specified time.
   * @param time time in milliseconds
   * included, false otherwise
   * @param maxCount the maximum number of objects to be returned,
   * or 0 for no limit.
   * @return A new 'VectorWithCount' of 'TimeObjectEntry' objects that
   * also holds the requested number of items in its 'count' field.
   */
  public VectorWithCount getNewerObjects(long time, int maxCount)
  {
    return getObjects(time, 0, maxCount);
  }

  /**
   * Retrieves a copy of objects that are newer or equal to the specified time.
   * @param time time in milliseconds
   * included, false otherwise
   * @return A new 'VectorWithCount' of 'TimeObjectEntry' objects that
   * also holds the requested number of items in its 'count' field.
   */
  public VectorWithCount getNewerObjects(long time)
  {
    return getObjects(time, 0, 0);
  }

  /**
   * Retrieves a copy of objects in the specified time range (inclusive).
   * @param beginTime begin time in milliseconds or 0 for start of cache
   * @param endTime end time in milliseconds or 0 for end of cache.
   * @param maxCount the maximum number of objects to be returned,
   * or 0 for no limit.
   * @return A new 'VectorWithCount' of 'TimeObjectEntry' objects that
   * also holds the requested number of items in its 'count' field.
   */
  public VectorWithCount getObjects(long beginTime, long endTime, int maxCount)
  {
    synchronized(objectCache)
    {
      if(beginTime > 0)                //if begin-time given then
        beginTime -= tolerance;        //subtract tolerance value
      if(endTime > 0)                  //if end-time given then
        endTime += tolerance;          //add tolerance value

      return subListVector(beginTime, endTime, maxCount);
    }
  }

  /**
   * Retrieves a copy of objects in the specified time range (inclusive).
   * @param beginTime begin time in milliseconds or 0 for start of cache
   * @param endTime end time in milliseconds or 0 for end of cache.
   * @return A new 'VectorWithCount' of 'TimeObjectEntry' objects that
   * also holds the requested number of items in its 'count' field.
   */
  public VectorWithCount getObjects(long beginTime, long endTime)
  {
    synchronized(objectCache)
    {
      if(beginTime > 0)                //if begin-time given then
        beginTime -= tolerance;        //subtract tolerance value
      if(endTime > 0)                  //if end-time given then
        endTime += tolerance;          //add tolerance value

      return subListVector(beginTime, endTime, 0);
    }
  }

  /**
   * Retrieves a copy of objects that are older or equal to the specified time.
   * @param time time in milliseconds.
   * @param maxCount the maximum number of objects to be returned,
   * or 0 for no limit.
   * @return A new 'VectorWithCount' of 'TimeObjectEntry' objects that
   * also holds the requested number of items in its 'count' field.
   */
  public VectorWithCount getOlderObjects(long time, int maxCount)
  {
    return getObjects(0, time, maxCount);
  }

  /**
   * Retrieves a copy of objects that are older or equal to the specified time.
   * @param time time in milliseconds.
   * @return A new 'VectorWithCount' of 'TimeObjectEntry' objects that
   * also holds the requested number of items in its 'count' field.
   */
  public VectorWithCount getOlderObjects(long time)
  {
    return getObjects(0, time, 0);
  }

  /**
   * Removes old objects using the specified remove age.
   * @param removeAge number of milliseconds to keep objects in the cache.
   * @return true if any objects were removed; false if no objects were
   * removed.
   */
  public boolean removeOldObjects(long removeAge)
  {
    boolean retFlag = false;
    synchronized(objectCache)
    {
      if(removeAge > 0)
      {  //remove age was given
                        //get current time in milliseconds:
        final long currentTime = System.currentTimeMillis();
        final long removeTime = currentTime - removeAge;
              //find objects before and including the remove time:
        final List listObj = subListVector(0, removeTime, 0);
        if(listObj.size() > 0)
        {  //sub-list contains items; remove sub-list
          final Iterator iterObj = listObj.iterator();
          Object obj;
          while(iterObj.hasNext())
          {   //for each object in list; remove object from cache
            objectCache.removeValue(obj=iterObj.next());
                        //subtract entry's data size from total for cache:
            if(obj instanceof TimeObjectEntry)
              totalCacheDataSize -= ((TimeObjectEntry)obj).getDataSize();
          }
          retFlag = true;
        }
      }
      if(maximumObjectCount > 0)
      {  //maximum-object count limit exists
        final int cacheSize;
        Object obj;
        if((cacheSize=objectCache.size()) > maximumObjectCount)
        {     //more objects in cache than limit
          final int numRemVal = cacheSize - maximumObjectCount;
              //remove oldest objects until under limit:
          for(int c=0; c<numRemVal; ++c)
          {   //for each oldest entry to be removed
            obj = objectCache.removeElementAt(0);
                        //subtract entry's data size from total for cache:
            if(obj instanceof TimeObjectEntry)
              totalCacheDataSize -= ((TimeObjectEntry)obj).getDataSize();
          }
          retFlag = true;
        }
      }
      if(maximumTotalDataSize > 0)       //if max-data-size limit exists
        checkRemoveViaTotalDataSize();   // then check and limit data size
      else if(objectCache.size() <= 0)   //if cache empty then
        totalCacheDataSize = 0;          //clear total-data size
//      cacheSize = objectCache.size();            //get new cache size
    }
//    if (logObj != null)  //if logging
//      logObj.debug5(getLogPrefixString() +
//                   " remove: size is " + cacheSize);
    return retFlag;
  }

  /**
   * Removes old objects from the cache with the default remove age.
   * @return true if any objects were removed; false if no objects were
   * removed.
   */
  public boolean removeOldObjects()
  {
    synchronized(objectCache)
    {
      return removeOldObjects(defaultRemoveAge); //remove the old objects
    }
  }

  /**
   * Checks if the total-data size for the cache is above the
   * 'maximumTotalDataSize' limit and, if so, removes entries
   * until the total-data size is under the limit.
   * The thread-synchronization object for the cache should
   * be held by the caller of this method.
   */
  protected void checkRemoveViaTotalDataSize()
  {
    Object obj;
    int cacheSize = objectCache.size();
    while(--cacheSize > 0 && totalCacheDataSize > maximumTotalDataSize)
    {    //while cache not empty and total-data size is above limit
      obj = objectCache.removeElementAt(0);      //remove oldest entry
//      if(logObj != null)
//      {     //log object is available
//        logObj.debug("IstiTimeObjectCache:  Total-data size (" +
//            totalCacheDataSize + ") over maximum (" + maximumTotalDataSize +
//                      "), removed oldest entry (" + obj + "), newNumEnts=" +
//                                                                 cacheSize);
//      }
                  //subtract entry's data size from total for cache:
      if(obj instanceof TimeObjectEntry)
        totalCacheDataSize -= ((TimeObjectEntry)obj).getDataSize();
    }
    if(cacheSize <= 0)                 //if cache empty then
      totalCacheDataSize = 0;          //clear total-data size
  }

  /**
   * Clears all entries in the cache.
   */
  public void clearCache()
  {
    synchronized(objectCache)
    {
      objectCache.clear();
      totalCacheDataSize = 0;
    }
  }

  /**
   * Sets the remove-age value used by the no-parameter version of
   * 'removeOldObjects()'.
   * @param ageVal the remove-age values to use, in milliseconds.
   */
  public void setDefaultRemoveAge(long ageVal)
  {
    synchronized(objectCache)
    {
      defaultRemoveAge = ageVal;
    }
  }

  /**
   * Sets the maximum-object count for the cache.  When the
   * 'removeOldObjects()' method is called the cache size will
   * be trimmed to this value as needed.
   * @param val the maximum-object count for the cache, or 0 for
   * no maximum-object count limit (the default).
   */
  public void setMaximumObjectCount(int val)
  {
    synchronized(objectCache)
    {
      maximumObjectCount = val;
    }
  }

  /**
   * Returns the maximum-object count for the cache.  When the
   * 'removeOldObjects()' method is called the cache size will
   * be trimmed to this value as needed.
   * @return The maximum-object count for the cache, or 0 for
   * no maximum-object count limit (the default).
   */
  public int getMaximumObjectCount()
  {
    return maximumObjectCount;
  }

  /**
   * Sets the maximum-total-data size for the cache.  To calculate the
   * total-data size the 'setDataSize()' method needs to have been called
   * on each cache object.  When the 'removeOldObjects()' method is
   * called the cache size will be trimmed until the total-data size is
   * under this value as needed.
   * @param val the maximum-total-data size for the cache, or 0 for
   * no maximum-total-data size limit (the default).
   */
  public void setMaximumTotalDataSize(long val)
  {
    synchronized(objectCache)
    {
      maximumTotalDataSize = val;
    }
  }

  /**
   * Returns the maximum-total-data size for the cache.  To calculate the
   * total-data size the 'setDataSize()' method needs to have been called
   * on each cache object.  When the 'removeOldObjects()' method is
   * called the cache size will be trimmed until the total-data size is
   * under this value as needed.
   * @return The maximum-total-data size for the cache, or 0 for
   * no maximum-total-data size limit (the default).
   */
  public long getMaximumTotalDataSize()
  {
    return maximumTotalDataSize;
  }

  /**
   * Returns the total-data size for the cache.  To calculate the
   * total-data size the 'setDataSize()' method needs to have been
   * called on each cache object.
   * @return The total-data size for the cache.
   */
  public long getTotalCacheDataSize()
  {
    synchronized(objectCache)
    {
      return totalCacheDataSize;
    }
  }

  /**
   * Returns the number of objects in the cache.
   * @return The number of objects in the cache.
   */
  public int getMessageCount()
  {
    return objectCache.size();
  }

  /**
   * Returns information about the cache.
   * @return A new 'CacheInformation' object.
   */
  public CacheInformation getCacheInformation()
  {
    synchronized(objectCache)
    {
      return new CacheInformation(getFirstObject(),getLastObject(),
                                     objectCache.size(),totalCacheDataSize);
    }
  }

  /**
   * Adds the time object entry.
   * @param entryObj time-entry object.
   */
  public void add(TimeObjectEntry entryObj)
  {
    synchronized(objectCache)
    {
      if(maximumObjectCount > 0 && objectCache.size() >= maximumObjectCount)
      {  //maximum-object count limit exists and cache is at or over limit
                        //remove oldest entry from cache:
        final Object obj = objectCache.removeElementAt(0);
                        //subtract entry's data size from total for cache:
        if(obj instanceof TimeObjectEntry)
          totalCacheDataSize -= ((TimeObjectEntry)obj).getDataSize();
//        if(logObj != null)
//        {     //log object is available
//          logObj.debug("IstiTimeObjectCache:  At max count (" +
//                                                        maximumObjectCount +
//                         "), adding new and removing oldest entry (old=\"" +
//                                     obj + "\", new=\"" + entryObj + "\")");
//        }
      }
                                  //if flag then use 'keyStr' as table key,
                                  // otherwise use new (unique) object:
      objectCache.put((useLookupKeyFlag ? entryObj.getKeyStr() :
                                                   new Object()), entryObj);
                        //add new entry's data size to total for cache:
      totalCacheDataSize += entryObj.getDataSize();
      if(maximumTotalDataSize > 0)       //if max-data-size limit exists
        checkRemoveViaTotalDataSize();   // then check and limit data size
      if(logObj != null)
      {  //log object is available; check time vs. previous item time
        final long timeVal;
        if((timeVal=entryObj.getTimeGenerated()) < lastMsgTimeTracker)
        {     //time value before that of previous item; log warning
          logObj.debug2(getLogPrefixString() +
                                 ":  Time value of added item (" + timeVal +
                            ") is less than time value of previous item (" +
                                                  lastMsgTimeTracker + ")");
        }
        lastMsgTimeTracker = timeVal;       //save last item time value
      }
    }
//    if (logObj != null)  //if logging
//      logObj.debug5(getLogPrefixString() +
//                   " add: size is " + cacheSize);
  }

  /**
   * Returns true if the cache contains an entry with a 'keyStr' equal
   * to the given object.  For this method to work the cache must have
   * been constructed setup to use the "keyStr" parameter (when adding)
   * as a table-lookup key.
   * @param keyObj the key object to use.
   * @return true if the cache contains a matching object; false if not.
   */
  public boolean containsKey(Object keyObj)
  {
    return objectCache.containsKey(keyObj);
  }

  /**
   * Returns true if the cache contains an entry with a 'keyStr' equal
   * to the 'keyStr' for the given entry-object.  For this method to work
   * the cache must have been constructed setup to use the "keyStr"
   * parameter (when adding) as a table-lookup key.
   * @param entryObj the entry-object to use.
   * @return true if the cache contains a matching object; false if not.
   */
  public boolean containsObjectKey(TimeObjectEntry entryObj)
  {
    return objectCache.containsKey(entryObj.getKeyStr());
  }

  /**
   * Returns true if the cache contains an entry equal to the given
   * entry-object.
   * @param entryObj the entry-object to use.
   * @return true if the cache contains a matching object; false if not.
   */
  public boolean containsEntry(TimeObjectEntry entryObj)
  {
    return objectCache.containsValue(entryObj);
  }

  /**
   * Returns the number of entries in the cache.
   * @return The number of entries in the cache.
   */
  public int size()
  {
    return objectCache.size();
  }

  /**
   * Returns the status of whether or not the cache is empty.
   * @return true if the cache is empty, false if not.
   */
  public boolean isEmpty()
  {
    return objectCache.isEmpty();
  }

  /**
   * Gets the prefix to use for log output.
   * @return the prefix to use for log output.
   */
  protected String getLogPrefixString()
  {
    return "IstiTimeObjectCache";
  }

  /**
   * Returns the entry at the given index.
   * @param index index of the entry.
   * @return The entry object.
   * @throws ArrayIndexOutOfBoundsException if the index is negative
   * or not less than the current size of the cache.
   */
  protected TimeObjectEntry getObject(int index)
  {
    return (TimeObjectEntry)(objectCache.elementAt(index));
  }

  /**
   * Retrieves a sub list of objects.  The thread-sychronization lock
   * for 'objectCache' should be held before this method is used.
   * @param beginTime minimum time (in milliseconds, inclusive) for
   * returned objects, or 0 for start of cache.
   * @param endTime maximum time (in milliseconds, inclusive) for
   * returned objects, or 0 for end of cache.
   * @param maxCount the maximum number of objects to be returned,
   * or 0 for no limit.
   * @return A new 'VectorWithCount' of 'TimeObjectEntry' objects that
   * also holds the requested number of items in its 'count' field.
   */
  protected VectorWithCount subListVector(long beginTime, long endTime,
                                                               int maxCount)
  {
    try
    {
      final int cacheSize;
      if((cacheSize=objectCache.size()) > 0)
      {  //cache not empty; get begin time for cache
        final long cacheBeginTime = getObject(0).getTimeGenerated();
                                       //get end time for cache:
        final long cacheEndTime = getObject(cacheSize-1).getTimeGenerated();
                                       //calc total elaped time for cache:
        final long cacheTotalTime = cacheEndTime - cacheBeginTime;

              //find index for first item to be returned:
        int firstIndex = -1;
        if(beginTime > cacheBeginTime)
        {     //given begin time is after cache begin time
          int i;
          if(cacheTotalTime > 0)
          {   //total-cache-time value OK
                   //estimate index for item based on time values:
            if((i=(int)((double)cacheSize/cacheTotalTime*
                                   (beginTime-cacheBeginTime))) < cacheSize)
            {      //estimated index value not too large
              if(getObject(i).getTimeGenerated() >= beginTime)
              {    //at or after desired position
                        //scan backward until time is before begin time:
                while(--i >= 0 &&
                              getObject(i).getTimeGenerated() >= beginTime);
                firstIndex = i + 1;         //save index to item
              }
              else
                ++i;       //increment before scanning forward below
            }
            else   //estimated index value too large
              i = 0;
          }
          else     //total-cache-time too small
            i = 0;
          if(firstIndex < 0)
          {   //index value not yet found; scan forward to find item
            while(i < cacheSize)
            {   //loop while item times are before begin time & not at end
              if(getObject(i).getTimeGenerated() >= beginTime)
              {      //item time equal or later than begin time
                firstIndex = i;     //save index to item
                break;              //exit loop
              }
              ++i;
            }
          }
        }
        else  //given begin time <= cache begin time
          firstIndex = 0;         //use first item in cache

              //find index for last item to be returned:
        int lastIndex = -1;
        if(endTime > 0 && endTime < cacheEndTime)
        {     //end time was given and is before cache end time
          if(endTime >= cacheBeginTime)
          {   //given end time is >= cache begin time
            int i;
            if(cacheTotalTime > 0)
            {      //total-cache-time value OK
                        //estimate index for item based on time values:
              if((i=(int)((double)cacheSize/cacheTotalTime*
                                     (endTime-cacheBeginTime))) < cacheSize)
              {    //estimated index value not too large
                if(getObject(i).getTimeGenerated() <= endTime)
                {  //at or before desired position
                        //scan forward until time is after end time:
                  while(++i < cacheSize &&
                                getObject(i).getTimeGenerated() <= endTime);
                  lastIndex = i - 1;        //save index to item
                }
                else
                  --i;       //decrement before scanning backward below
              }
              else      //estimated index value too large
                i = cacheSize - 1;
            }
            else        //total-cache-time too small
              i = cacheSize - 1;
            if(lastIndex < 0)
            {      //index value not yet found; scan backward to find item
              while(i >= 0)
              {    //loop while item times are after end time & not at beg
                if(getObject(i).getTimeGenerated() <= endTime)
                {       //item time equal or earlier than end time
                  lastIndex = i;       //save index to item
                  break;               //exit loop
                }
                --i;
              }
            }
          }
        }
        else  //given end time is >= cache end time
          lastIndex = cacheSize - 1;   //use last item in cache

        if (firstIndex >= 0 && lastIndex >= firstIndex)
        {     //both index values were found and are OK
                   //calculate number of items requested:
          final int reqCount = lastIndex - firstIndex + 1;
                   //if max-count limit given and total is greater than
                   // max-count limit then adjust end index:
          if(maxCount > 0 && reqCount > maxCount)
            lastIndex = firstIndex + maxCount - 1;
                   //return copy of sub-list from cache:
          return new VectorWithCount(            //(with requested count)
              objectCache.getValuesVector(firstIndex,lastIndex+1),reqCount);
        }
      }
    }
    catch (Exception ex)
    {
      if (logObj != null)  //if logging
      {
        logObj.warning(getLogPrefixString() +
                                        " error accessing cache:  " + ex);
      }
    }
    return new VectorWithCount();      //return empty list
  }

  /**
   * Deallocates resources associated with the cache.
   */
  public void close()
  {
    objectCache.clear();          //clear cache data
  }


  /**
   * Time object entry
   */
  public interface TimeObjectEntry
  {
    /**
     * Returns the time the object was generated.
     * @return the time the object was generated.
     */
    public long getTimeGenerated();

    /**
     * Returns the time object.
     * @return the time object.
     */
    public Object getDataObj();

    /**
     * Returns the data string.
     * @return the data string.
     */
    public String getKeyStr();

    /**
     * Returns the data size for the entry object.
     * @return The data size for the entry object.
     */
    public int getDataSize();
  }


  /**
   * Basic time object entry.
   */
  public static class BasicTimeObjectEntry implements TimeObjectEntry
  {
    protected long timeGenerated;
    protected Object dataObj;
    protected String keyStr;
    protected int dataSize;

    /**
     * Creates a message object cache entry.
     * @param timeGenerated the time the message was generated.
     * @param dataObj data object.
     * @param keyStr key string (to be used as a lookup key into the cache).
     */
    public BasicTimeObjectEntry(long timeGenerated, Object dataObj,
                                                             String keyStr)
    {
      this.timeGenerated = timeGenerated;
      this.dataObj = dataObj;
      this.keyStr = keyStr;
    }

    /**
     * No-argument constructor for subclasses.
     */
    protected BasicTimeObjectEntry()
    {
    }

    /**
     * Returns the time the object was generated.
     * @return the time the object was generated.
     */
    public long getTimeGenerated()
    {
      return timeGenerated;
    }

    /**
     * Returns the data object.
     * @return the data object.
     */
    public Object getDataObj()
    {
      return dataObj;
    }

    /**
     * Returns the key string.
     * @return the key string.
     */
    public String getKeyStr()
    {
      return keyStr;
    }

    /**
     * Sets the data size for the entry object.  If a data size is
     * entered for each cache object then a total-data size limit
     * can be enforced by the cache.  The object's data size
     * should be setup before the object is added to the cache.
     * @param val the data size value to use.
     */
    public void setDataSize(int val)
    {
      dataSize = val;
    }

    /**
     * Returns the data size for the entry object.
     * @return The data size for the entry object.
     */
    public int getDataSize()
    {
      return dataSize;
    }
  }


  /**
   * Subclass of Vector that also holds a 'count' value.
   */
  public static class VectorWithCount extends Vector
  {
    protected int count = 0;

    /**
     * Constructs an empty VectorWithCount.
     */
    public VectorWithCount()
    {
    }

    /**
     * Constructs a VectorWithCount containing the elements of the
     * specified collection, in the order they are returned by the
     * collection's iterator.
     * @param cObj the collection whose elements are to be placed
     * into this VectorWithCount.
     */
    public VectorWithCount(Collection cObj)
    {
      super(cObj);
    }

    /**
     * Constructs a VectorWithCount containing the elements of the
     * specified collection, in the order they are returned by the
     * collection's iterator.
     * @param cObj the collection whose elements are to be placed
     * into this VectorWithCount.
     * @param count the count value to use.
     */
    public VectorWithCount(Collection cObj,int count)
    {
      super(cObj);
      this.count = count;
    }

    /**
     * Sets the count value.
     * @param count the count value to use.
     */
    public void setCount(int count)
    {
      this.count = count;
    }

    /**
     * Adds to the count value.
     * @param val the value to add.
     */
    public void addToCount(int val)
    {
      count += val;
    }

    /**
     * Returns the count value.
     * @return The count value, or 0 if none set.
     */
    public int getCount()
    {
      return count;
    }
  }


  /**
   * Class CacheInformation holds information about the cache.
   */
  public static class CacheInformation
  {
         /** First message object in cache. */
    public final TimeObjectEntry firstMsgObj;
         /** Last message object in cache. */
    public final TimeObjectEntry lastMsgObj;
         /** Number of message objects in cache. */
    public final int messageCount;
         /** Total data size for objects in cache. */
    public final long totalDataSize;

    /**
     * Creates an object that holds information about the cache.
     * @param firstMsgObj first message object in cache.
     * @param lastMsgObj last message object in cache.
     * @param messageCount number of message objects in cache.
     * @param totalDataSize total data size for objects in cache.
     */
    public CacheInformation(TimeObjectEntry firstMsgObj,
           TimeObjectEntry lastMsgObj, int messageCount, long totalDataSize)
    {
      this.firstMsgObj = firstMsgObj;
      this.lastMsgObj = lastMsgObj;
      this.messageCount = messageCount;
      this.totalDataSize = totalDataSize;
    }
  }
}
