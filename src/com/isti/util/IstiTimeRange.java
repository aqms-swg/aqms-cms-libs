package com.isti.util;

/**
 * <p>Title: </p>
 * <p>Description: </p>
 * <p>Copyright: Copyright (c) 1999</p>
 * <p>Company: ISTI</p>
 * @author unascribed
 * @version 1.0
 */

public class IstiTimeRange {

  private IstiEpoch start = null;
  private IstiEpoch end = null;

  public IstiTimeRange() {
    start = new IstiEpoch();
    end = new IstiEpoch();
  }

  public IstiTimeRange(IstiEpoch startTime, IstiEpoch endTime) {
    start = new IstiEpoch(startTime);
    end = new IstiEpoch(endTime);
  }

  /**
   * return number of milliseconds between start and end
   * @return
   */
  public long getDuration() {

    long msecs = 0;

    if (start.isBeforeEqual(end)) {

      msecs = end.getIntEpochMSecs();
      msecs -= start.getIntEpochMSecs();

    }  else {

      msecs = start.getIntEpochMSecs();
      msecs -= end.getIntEpochMSecs();

    }

    return msecs;

  }

  public static void main(String[] args) {
    new IstiTimeRange();
  }
  public IstiEpoch getEnd() {
    return end;
  }
  public IstiEpoch getStart() {
    return start;
  }
  public void setStart(IstiEpoch start) {
    this.start = start;
  }
  public void setEnd(IstiEpoch end) {
    this.end = end;
  }
}