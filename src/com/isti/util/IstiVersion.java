//IstiVersion.java: Describes a particular version of something: a product, API, etc.
//
//   8/5/2000 -- [KF]  Initial version.
// 10/15/2003 -- [KF]  Check for beta last with comparisons,
//                     skip all versions <= 0 at the end of the version array.
//  3/21/2019 -- [KF]  Modified to use UtilFns methods.
//  6/26/2019 -- [KF]  Modified to handle null values.
//

package com.isti.util;

/**
 * Class IstiVersion describes a particular version of something: a product, API, etc.
 */
public class IstiVersion implements Comparable
{
  private final static int MAJOR = 0;
  private final static int MINOR = 1;
  private final static int PATCH = 2;
  private final static int BUILD = 3;

  /**
   * Beta string.
   */
  public final static String BETA_STRING = "beta";

  /**
   * Divider string for version numbers.
   */
  public final static String DIVIDER = ".";

  /**
   * Return a string representation of the version.
   * @param version the version.
   * @param beta true if beta, false otherwise.
   * @return a string representation of the version.
   */
  public static String toString(int[] version, boolean beta)
  {
    final StringBuilder versionStr = new StringBuilder();
    if (version != null && version.length > 0)
    {
      versionStr.append(version[0]);
      for (int i = 1; i < version.length; i++)
      {
        versionStr.append(DIVIDER);
        versionStr.append(version[i]);
      }
      if (beta)
      {
        versionStr.append(BETA_STRING);
      }
    }
    return versionStr.toString();
  }

  private final boolean beta;  //true if beta
  private final int [] version = UtilFns.createVersionArray(); //version array
  private volatile String versionString;  //version string

  //constructors

  /**
   * Creates a version.
   * The string is searched for a valid version string (such as "1.49.01")
   * starting at the end of the string.
   * Beta versions should contain "beta" (such as "1.47Beta").
   * Any other non-numeric characters leading or trailing a version number are ignored.
   *
   * @param complete the complete version string.
   */
  public IstiVersion(String complete)
  {
	  beta = isBeta(complete);
	  UtilFns.parseVersionNumbers(complete, version);
  }

  /**
   * Creates a version.
   * @param major the major version.
   * @param minor the minor version.
   * @param patch the patch version.
   * @param build the build version.
   * @param beta true if beta, false otherwise.
   */
  public IstiVersion(int major, int minor, int patch, int build, boolean beta)
  {
    this.beta = beta;
    version[MAJOR] = major;
    version[MINOR] = minor;
    version[PATCH] = patch;
    version[BUILD] = build;
  }

  /**
   * Creates a version that isn't a beta version.
   * @param args an array of versions.
   */
  public IstiVersion(int ... args)
  {
    this(args, false);
  }

  /**
   * Creates a version.
   * @param args an array of versions.
   * @param beta true if beta, false otherwise.
   */
  public IstiVersion(int[] args, boolean beta)
  {
    this.beta = beta;
    if (args != null)
    {
      int versionsLen = Math.min(args.length, version.length);
      for (int i = 0; i < versionsLen; i++)
      {
        version[i] = args[i];
      }
    }
  }

  //protected methods

  /**
   * Determines if the complete version string contains beta.
   * @param complete the complete version string.
   * @return true if beta.
   */
  protected static boolean isBeta(String complete)
  {
    return complete != null && complete.toLowerCase().indexOf(BETA_STRING) >= 0;
  }

  // public methods

  /**
   * Compares this IstiVersion with the specified IstiVersion.
   * If the specified Object is a IstiVersion, this function behaves like
   * <code>compareTo(IstiVersion)</code>.  Otherwise, it throws a
   * <code>ClassCastException</code> (as Versions are comparable
   * only to other Versions).
   *
   * @param   o the <code>Object</code> to be compared.
   * @return  the value <code>0</code> if the versions are equal;
   *            a value less than <code>0</code> if this version
   *            is less than the specified version;
   *            and a value greater than <code>0</code> if this version
   *            is greater than the specified version.
   * @exception <code>ClassCastException</code> if the argument is not a
   *		  <code>IstiVersion</code>.
   */
  public int compareTo(java.lang.Object o)
  {
    return compareTo((IstiVersion)o);
  }

  /**
   * Compares this IstiVersion with the specified IstiVersion.
   * @param   o the <code>Object</code> to be compared.
   * @return  the value <code>0</code> if the versions are equal;
   *            a value less than <code>0</code> if this version
   *            is less than the specified version;
   *            and a value greater than <code>0</code> if this version
   *            is greater than the specified version.
   */
  public int compareTo(IstiVersion o)
  {
	  int result = UtilFns.compareVersionNumbers(version, o.version);
	  if (result == 0 && isBeta() != o.isBeta())
	  {
		  if (o.isBeta())  //if the specified version is beta
		  {
			  result = 1;  //this version is greater than the specified version
		  }
		  else // this version is beta
		  {
			  result = -1;  //this version is less than the specified version
		  }
	  }
	  return result;
  }

  /**
   * Compares this IstiVersion to the specified object.
   * The result is <code>true</code> if and only if the argument is not
   * <code>null</code> and is a <code>IstiVersion</code> object that represents
   * the same version as this object.
   * @param   o the <code>Object</code> to be compared.
   * @return  <code>true</code> if the <code>IstiVersion </code>are equal;
   *          <code>false</code> otherwise.
   */
  public boolean equals(java.lang.Object o)
  {
    return o instanceof IstiVersion && equals((IstiVersion)o);
  }

  /**
   * Compares this IstiVersion to the specified object.
   * The result is <code>true</code> if and only if the argument is not
   * <code>null</code> and is a <code>IstiVersion</code> object that represents
   * the same version as this object.
   * @param   o the <code>Object</code> to be compared.
   * @return  <code>true</code> if the <code>IstiVersion </code>are equal;
   *          <code>false</code> otherwise.
   */
  public boolean equals(IstiVersion o)
  {
    return o != null && compareTo(o) == 0;
  }

  /**
   * Gets the build.
   * @return the buld.
   */
  public int getBuild()
  {
    return version[BUILD];
  }

  /**
   * Get the major version.
   * @return the major version.
   */
  int getMajor()
  {
    return version[MAJOR];
  }

  /**
   * Gets the minor version.
   * @return the minor version.
   */
  public int getMinor()
  {
    return version[MINOR];
  }

  /**
   * Gets the patch version.
   * @return the patch version.
   */
  public int getPatch()
  {
    return version[PATCH];
  }

  /**
   * Determines if this is a beta version.
   * @return true if beta, false otherwise.
   */
  public boolean isBeta()
  {
    return beta;
  }

  /**
   * Returns a string representation of this version.
   * @return a string representation of this version.
   */
  public String toString()
  {
	String s = versionString;
	if (s == null)
	{
		s = toString(version, beta);
	    versionString = s;
	}
    return s;
  }
}