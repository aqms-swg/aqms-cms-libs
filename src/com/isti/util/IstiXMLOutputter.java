//IstiXMLOutputter.java:  Extends the 'XMLOutputter' class to add additional
//                        functionality.
//
//   8/23/2005 -- [KF]  Initial version.
//

package com.isti.util;

import org.jdom.output.XMLOutputter;

/**
 * Class IstiXMLOutputter extends the 'XMLOutputter' class to add additional
 * functionality.
 */
public class IstiXMLOutputter extends XMLOutputter
{
  /**
   * Default encoding.
   * This may be used for the XMLOutputter.setEncoding() method.
   */
  public final static String DEFAULT_ENCODING_FORMAT =
      IstiXmlUtils.UTF_8_ENCODING_FORMAT;

  /** The encoding format */
  private String encodingFormat = null;

  /**
   * This will create an <code>XMLOutputter</code> with no additional
   * whitespace (indent or newlines) added; the whitespace from the
   * element text content is fully preserved.
   */
  public IstiXMLOutputter()
  {
    this(DEFAULT_ENCODING_FORMAT);
  }

  /**
   * This will create an <code>XMLOutputter</code> with no additional
   * whitespace (indent or newlines) added; the whitespace from the
   * element text content is fully preserved.
   * @param encoding set encoding format.  Use XML-style names like
   *                 "UTF-8" or "ISO-8859-1" or "US-ASCII"
   * (see {@link #setEncoding}).
   */
  public IstiXMLOutputter(String encoding)
  {
    super();
    setEncoding(encoding);
  }

  /**
   * Gets the encoded string using the current encoding.
   * @param s the string to encode.
   * @return the encoded string or the original string if an error occurs.
   * (see {@link #setEncoding}).
   */
  public String getEncodedString(String s)
  {
    return IstiXmlUtils.getEncodedString(s,encodingFormat);
  }

  /**
   * Sets the output encoding.  The name should be an accepted XML
   * encoding.
   *
   * @param encoding the encoding format.  Use XML-style names like
   *                 "UTF-8" or "ISO-8859-1" or "US-ASCII"
   * (see {@link #makeWriter}).
   */
  public void setEncoding(String encoding)
  {
    super.setEncoding(encoding);
    encodingFormat = encoding;
  }
}
