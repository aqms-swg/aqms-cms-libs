//IstiXmlLoader.java:  Implements a XML file loader.
//
//   8/23/2002 -- [KF]
//

package com.isti.util;

import java.util.*;
import org.jdom.Element;

/**
 * Class IstiMenuLoader implements a XML file loader.
 */
public abstract class IstiXmlLoader extends IstiXmlUtils
{
  /**
   * Constructs a XML file loader.
   */
  public IstiXmlLoader()
  {
    super();
  }

  /**
   * Gets the next child for the specified element.
   * @param parentElement the parent element to process.
   * @param childIndex index of child element to return.
   * @param parentObject the parent object for the element.
   *
   * @return     an object for the child element or null if no more children.
   */
  protected Object processChildElement(Element parentElement, int childIndex,
                                       Object parentObject)
  {
    // if no parent element
    if (parentElement == null)
      return null;                 // we are done, exit method

    // if index is greater than the number of children
    if (childIndex >= parentElement.getChildren().size())
      return null;  // no more children

    Object element = parentElement.getChildren().get(childIndex);
    if (element instanceof Element)
    {
      final Object object = processElement((Element)element, parentObject,
          parentElement);
      processChildren((Element)element, object);
      return object;
    }
    return null;  // no more children
  }

  /**
   * Gets the next child with the specifed attribute value for the specified
   * element.
   * @param parentElement the parent element to process.
   * @param attribute the attribute of the element to find.
   * @param value the attribute value of the element to find.
   * @param parentObject the parent object for the element.
   *
   * @return     an object for the child element or null if no more children.
   */
  protected Object processChildElement(Element parentElement, String attribute,
                                       String value, Object parentObject)
  {
    // if no parent element
    if (parentElement == null)
      return null;                 // we are done, exit method

    // for each child
    for (int childIndex = 0; childIndex < parentElement.getChildren().size();
         childIndex++)
    {
      Object childObject = parentElement.getChildren().get(childIndex);
      if (childObject instanceof Element)
      {
        final Element element = (Element)childObject;

        // if it is a match
        final String attributeValue = element.getAttributeValue(attribute);
        if (attributeValue.equals(value))
        {
          // process the element
          final Object object = processElement(element, parentObject,
              parentElement);

          // process the children
          processChildren(element, object);
          return object;
        }
      }
    }
    return null;  // no more children
  }

  /**
   * Processes the children for the specified element.
   * @param parentElement the parent element to process
   * @param parentObject the parent object for the element
   */
  protected void processChildren(Element parentElement, Object parentObject)
  {
    // if no parent element
    if (parentElement == null)
      return;                      // we are done, exit method

    // get the children
    final List childrenList = parentElement.getChildren();

    // if no children found
    if (childrenList == null || childrenList.size() <= 0)
      return;                      // we are done, exit method

    final Iterator iterObj = childrenList.iterator();

    while (iterObj.hasNext())        // for each element in the list
    {
      final Object obj = iterObj.next();

      if (!(obj instanceof Element)) // if element not fetched OK
      {
        return;                      // abort method
      }

      // process this element
      final Element element = (Element)obj;
      final Object currentObject = processElement(element, parentObject,
          parentElement);

      // get the children of this element
      processChildren(element, currentObject);
    }
  }

  /**
   * Processes the element for this document.
   * NOTE: Implement this function to process elements.
   * @param element the element to process
   * @param parentObject the parent object for the element
   * @param parentElement the parent element for the element.
   *
   * @return     an object for the element or null if no object is needed.
   */
  protected abstract Object processElement(Element element, Object parentObject,
      Element parentElement);
}