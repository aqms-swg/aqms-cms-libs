//IstiXmlUtils.java:  Implements utilities for XML.
//
//   9/4/2002 -- [KF]  Initial version.
//  9/11/2002 -- [ET]  Added 'elementToString()' method.
//  9/16/2002 -- [ET]  Added 'stringToElement()' method.
//  9/25/2002 -- [ET]  Improved 'loadFile()' method (can now find files in
//                     jars and as URLs) and added 'loadStream()' methods.
// 10/24/2002 -- [ET]  Added 'saveToStream()' and 'saveToFile()' methods.
//  4/14/2003 -- [ET]  Modified 'loadStream()' and 'stringToElement()'
//                     to handle "IOException" being throwable by
//                     'SAXBuilder.build()'.
//  7/31/2003 -- [ET]  Modified 'saveToFile()' method to create any needed
//                     directories above the given file.
//  8/19/2003 -- [ET]  Added reference to xerces 'XML11DTDDVFactoryImpl'.
// 11/19/2003 -- [ET]  Modified calls to 'getRootElement()' to use
//                     'detachRootElement()' instead so that the returned
//                     element has no parent.
// 11/24/2003 -- [ET]  Modified 'loadFile()' and 'loadStream()' to allow
//                     'rootElementName' parameter value of 'null'.
// 11/29/2003 -- [ET]  Added optional 'normalizeFlag' parameter to the
//                     'elementToString()' method; added 'getChildDataStr()'
//                     method; modified 'saveToFile()' to make sure that
//                     output stream is always closed and added version
//                     with only filename parameter; changed default
//                     'setTextTrim' for file outputter to 'true'; added
//                     'set...' methods to allow configuration of XML
//                     outputters; added 'setRootElement()' method.
// 12/23/2003 -- [ET]  Changed default 'setTextTrim' for file outputter
//                     back to 'false'.
//  1/25/2004 -- [ET]  Modified 'loadFile()' to close input file.
//  3/12/2004 -- [ET]  Modified to extend 'ErrorMessageMgr' and to clear
//                     any fetched error message at beginning of "load"
//                     and "save" methods; added 'getLoadFileOpenedFlag()'
//                     method.
//  2/10/2005 -- [ET]  Added 'jaxp' dummy Xerces references.
//  8/23/2005 -- [KF]  Added 'getEncodedString', 'setDocumentStylesheet' and
//                    'setElementNamespace' methods.
//  12/6/2005 -- [ET]  Added synchronization for 'xmlBuilderObj' in
//                     'loadStream()' method; added method
//                     'setLoadExternalDTDFeature()'; added dummy
//                     reference to 'javax.xml.transform.Source'
//                     to help web services support under Java 1.3.
//  9/13/2006 -- [ET]  Added 'getAnyNSChild()' method.
// 11/27/2006 -- [KF]  Added 'useDocumentFlag' and 'createRootElement()',
//                     'setFileLineSeparator()', 'setFileOmitDeclaration()'
//                     and 'setFileOmitEncoding()' methods.
//  8/30/2007 -- [ET]  Added '...EscapedCodes()' methods; added methods
//                     'elementToFixedString()' and 'getChildDataFixedStr()';
//                     added optional 'normalizeFlag' parameter to
//                     'getChildDataStr()' method and changed default
//                     behavior to be to not normalize.
//   7/8/2010 -- [ET]  Modified methods 'ctrlCharsToEscapedCodes()' and
//                     'elementToFixedString()' to make them also convert
//                     characters greater than 127; removed 'maxEscVal'
//                     limit from 'ctrlCharsFromEscapedCodes()' method;
//                     modified 'stringToElement()' method to make it
//                     interpret string via 'UTF-8' encoding; fixed
//                     'convertFromEscapedCodes()' reaction to 'maxEscVal'
//                     parameter equal to zero.
//  2/01/2012 -- [KF]  Added support to set URL connection request properties.
//  3/13/2012 -- [KF]  Added 'createCdata()' method.
//

package com.isti.util;

import java.util.List;
import java.util.Iterator;
import java.util.Properties;
import java.io.*;

import org.jdom.CDATA;
import org.jdom.Document;
import org.jdom.Element;
import org.jdom.JDOMException;
import org.jdom.input.SAXBuilder;
import org.jdom.output.XMLOutputter;
import org.jdom.Namespace;
import org.jdom.ProcessingInstruction;

/**
 * Class IstiXmlUtils implements utilities for XML.  The JDOM library
 * 'jdom.jar' is required, and under Java 1.3 the Xerces library
 * ('xerces.jar' from Apache) is also needed.
 */
public class IstiXmlUtils extends ErrorMessageMgr
{
  /** Use document flag. */
  public static boolean useDocumentFlag = false;

    /** Flag set true if file opened successfully by 'loadFile()' method. */
  protected boolean loadFileOpenedFlag = false;

  // dummy so that this class that is needed for JDOM is known
  //  (This is needed for Java 1.3 to get the 'xerces.jar' library
  //   and 'SAXParser' class included in the JBuilder-generated jar;
  //   the 'xerces.jar' library is included inside Java 1.4.):
  protected static final org.apache.xerces.parsers.SAXParser
                                                      dummySAXParser = null;
  // dummy references to get these Xerces objects
  //   included in JBuilder-generated jars:
  protected static final org.apache.xerces.parsers.XML11Configuration
                                                       dummyXml11Obj = null;
  protected static final org.apache.xerces.impl.dv.dtd.DTDDVFactoryImpl
                                                   dummyDTDDVFactObj = null;
  protected static final org.apache.xerces.impl.dv.dtd.XML11DTDDVFactoryImpl
                                                  dummyxDTDDVFactObj = null;
  protected static final org.apache.xerces.jaxp.DocumentBuilderFactoryImpl
                                                      dummydBFactObj = null;
  protected static final org.apache.xerces.jaxp.DocumentBuilderImpl
                                                       dummydBldrObj = null;
  protected static final org.apache.xerces.jaxp.JAXPConstants
                                                       dummyjConsObj = null;
  protected static final org.apache.xerces.jaxp.SAXParserFactoryImpl
                                                       dummysPFacObj = null;
  protected static final org.apache.xerces.jaxp.SAXParserImpl
                                                       dummysParsObj = null;
  protected static final javax.xml.transform.Source dummysXtsrcObj = null;

    /** Root element in file. */
  protected Element rootElement = null;

    /** XML stylesheet text. */
  public final static String XML_STYLESHEET = "xml-stylesheet";
    /** XML stylesheet processing text. */
  public final static String XML_STYLESHEET_PROCESSING_INSTRUCTION =
      "type=\"text/xsl\" href=\"";
    /** XML file extension. */
  public static final String XML_FILE_EXTENSION = ".xml";
    /** Static JDOM XML Document builder object (without validation). */
  public static final SAXBuilder xmlBuilderObj = new SAXBuilder(false);
    /** Static JDOM XML text output formatter object (for string output). */
  public static final XMLOutputter xmlStrOutputterObj = new XMLOutputter();
    /** Static JDOM XML text output formatter object (for file output). */
  public static final XMLOutputter xmlFileOutputterObj = new XMLOutputter();
    /** Static text output formatter object (for "fixed" string output). */
  public static final XMLOutputter xmlFixedStrOutputterObj =
                                                         new XMLOutputter();
    /** UTF-8 encoding.  May be used for the setEncoding() method. */
  public final static String UTF_8_ENCODING_FORMAT = "UTF-8";
    /** ISO-8859-1 encoding.  May be used for the setEncoding() method. */
  public static final String ISO_8859_1_ENCODING_FORMAT = "ISO-8859-1";

  static
  {      //set "file" formatter to use newlines:
    xmlFileOutputterObj.setNewlines(true);
         //set "fixed-string" formatter to trim whitespace between elements:
    xmlFixedStrOutputterObj.setTrimAllWhite(true);
  }

  /**
   * Sets the Document stylesheet.
   * @param documentObj the 'Document' object.
   * @param uri <code>String</code> URI of the stylesheet.
   */
  public static void setDocumentStylesheet(Document documentObj,String uri)
  {
    //add stylesheet
    final ProcessingInstruction stylesheet = new ProcessingInstruction(
      XML_STYLESHEET, XML_STYLESHEET_PROCESSING_INSTRUCTION+uri+"\"");
    documentObj.getContent().add(0, stylesheet);
  }

  /**
   * Sets whether the XML declaration will be omitted while outputting to files.
   * The default value is 'false'.
   * @param flgVal <code>true</code> to omit the XML declaration.
   */
  public static void setFileOmitDeclaration(boolean flgVal)
  {
    xmlFileOutputterObj.setOmitDeclaration(flgVal);
  }

  /**
   * Sets whether the document encoding will be omitted while outputting to files.
   * The default value is 'false'.
   * @param flgVal <code>true</code> to omit the document encoding.
   */
  public static void setFileOmitEncoding(boolean flgVal)
  {
    xmlFileOutputterObj.setOmitEncoding(flgVal);
  }

  /**
   * Sets whether newlines should be added while outputting to files.
   * The default value is 'true'.
   * @param flgVal <code>true</code> indicates new lines should be
   * added for beautification.
   */
  public static void setFileOutNewlines(boolean flgVal)
  {
    xmlFileOutputterObj.setNewlines(flgVal);
  }

  /**
   * Sets the indent <code>String</code> to use while outputting to files.
   * This is usually a <code>String</code> of empty spaces. If you pass
   * null, or the empty string (""), then no indentation will
   * happen.  Default: none (null)
   * @param indentStr <code>String</code> to use for indentation.
   */
  public static void setFileOutIndent(String indentStr)
  {
    xmlFileOutputterObj.setIndent(indentStr);
  }

  /**
   * Sets whether the text has leading/trailing whitespace trimmed while
   * outputting to files.  Default: false
   * @param flgVal true to trim the leading/trailing whitespace,
   * false to use text verbatim.
   */
  public static void setFileOutTextTrim(boolean flgVal)
  {
    xmlFileOutputterObj.setTextTrim(flgVal);
  }

  /**
   * Sets the element namespace.
   * @param elementObj the 'Element' object.
   * @param prefix <code>String</code> prefix to map to
   *               <code>Namespace</code>.
   * @param uri <code>String</code> URI of new <code>Namespace</code>.
   */
  public static void setElementNamespace(Element elementObj,String prefix,String uri)
  {
    setElementNamespace(elementObj,Namespace.getNamespace(prefix, uri));
  }

  /**
   * Sets the element namespace.
   * @param elementObj the 'Element' object.
   * @param namespaceObj the 'Namespace' object.
   */
  public static void setElementNamespace(Element elementObj,Namespace namespaceObj)
  {
    elementObj.setNamespace(namespaceObj);
  }

  /**
   * This will set the newline separator (<code>lineSeparator</code>).
   * The default is <code>\r\n</code>. Note that if the "newlines"
   * property is false, this value is irrelevant.
   * @param lineSeparator <code>String</code> line separator to use or
   * null for the system default.
   * @see setFileOutNewlines
   */
  public static void setFileLineSeparator(String lineSeparator)
  {
    if (lineSeparator == null)
    {
      lineSeparator = System.getProperty("line.separator");
    }
    if (lineSeparator != null)
    {
      xmlFileOutputterObj.setLineSeparator(lineSeparator);
    }
  }

  /**
   * Sets whether empty elements are expanded from
   * <code>&lt;tagName/&gt;</code> to
   * <code>&lt;tagName&gt;&lt;/tagName&gt;</code>
   * while outputting to files.  Default: false
   * @param flgVal true to expand elements.
   */
  public static void setFileOutExpandEmptyElements(boolean flgVal)
  {
    xmlFileOutputterObj.setExpandEmptyElements(flgVal);
  }

  /**
   * Sets whether newlines should be added while outputting to strings.
   * The default value is 'true'.
   * @param flgVal <code>true</code> indicates new lines should be
   * added for beautification.
   */
  public static void setStrOutNewlines(boolean flgVal)
  {
    xmlStrOutputterObj.setNewlines(flgVal);
  }

  /**
   * Sets the indent <code>String</code> to use while outputting to strings.
   * This is usually a <code>String</code> of empty spaces. If you pass
   * null, or the empty string (""), then no indentation will
   * happen.  Default: none (null)
   * @param indentStr <code>String</code> to use for indentation.
   */
  public static void setStrOutIndent(String indentStr)
  {
    xmlStrOutputterObj.setIndent(indentStr);
  }

  /**
   * Sets whether the text has leading/trailing whitespace trimmed while
   * outputting to strings.  Default: false
   * @param flgVal true to trim the leading/trailing whitespace,
   * false to use text verbatim.
   */
  public static void setStrOutTextTrim(boolean flgVal)
  {
    xmlStrOutputterObj.setTextTrim(flgVal);
  }

  /**
   * Sets whether empty elements are expanded from
   * <code>&lt;tagName/&gt;</code> to
   * <code>&lt;tagName&gt;&lt;/tagName&gt;</code>
   * while outputting to strings.  Default: false
   * @param flgVal true to expand elements.
   */
  public static void setStrOutExpandEmptyElements(boolean flgVal)
  {
    xmlStrOutputterObj.setExpandEmptyElements(flgVal);
  }

  /**
   * Sets the state of the load-external-DTD feature on the SAX parser.
   * This determines if the parser will attempt to load exteral DTD files
   * specified at the top of the XML file.  The feature may not work with
   * parser libraries other than Xerces.  (If Internet access is not
   * available when the parser attempts to load an external DTD then
   * an exception will be thrown.  Setting this feature to 'false'
   * prevents the parser from attempting to load the external DTD file.)
   * @param flgVal true to enable to loading of external DTD files; false
   * to disable to loading of external DTD files.
   */
  public static void setLoadExternalDTDFeature(boolean flgVal)
  {
    synchronized(xmlBuilderObj)
    {
      xmlBuilderObj.setFeature(
           "http://apache.org/xml/features/nonvalidating/load-external-dtd",
                                                                    flgVal);
    }
  }

  /**
   * Adds the ".xml" file extension to a file name.
   * @param fileName the file name to use.
   * @return file name with XML file extension added if needed.
   */
  public static String addXmlFileExtension(String fileName)
  {
    if (!fileName.toLowerCase().endsWith(XML_FILE_EXTENSION))
    {
      fileName += XML_FILE_EXTENSION;
    }
    return fileName;
  }

  /**
   * Constructs a XML utilities object.
   */
  public IstiXmlUtils()
  {
  }

  /**
   * Creates a new instance of the class represented by the class or
   * interface with the given string name.  The class is instantiatied as if
   * by a <code>new</code> expression with an empty argument list.  The class
   * is initialized if it has not already been initialized.
   *
   * @param      className   the fully qualified name of the desired class.
   *
   * @return a newly allocated instance of the class represented by this
   *             object.
   */
  protected Object getComponentForClass(String className)
  {
    try
    {
      Class classForName = Class.forName(className);
      if (classForName != null)
      {
        try
        {
          return classForName.getDeclaredConstructor().newInstance();
        }
        catch (InstantiationException e)
        {
          enterErrorMessageString(
              "Cannot instantiate class  " + className +": " + e);
        }
        catch (IllegalAccessException e)
        {
          enterErrorMessageString(
              "Cannot instantiate class " + className + ": " + e);
        }
      }
    }
    catch (ClassNotFoundException e)
    {
      enterErrorMessageString("Cannot get class for " + className + ": " + e);
    }
    catch (Exception e)
    {
      enterErrorMessageString("Error getting class for " + className + ": " + e);
    }
    return null;
  }

  /**
   * Finds the element with the specified attribute value.
   * @param attribute the attribute of the element to find.
   * @param value the attribute value of the element to find.
   * @return the element with the specified attribute value.
   */
  protected Element findElement(String attribute, String value)
  {
    return findElement(attribute, value, rootElement);
  }

  /**
   * Finds the element with the specified attribute value.
   * @param attribute the attribute of the element to find.
   * @param value the attribute value of the element to find.
   * @param element the element to start with.
   * @return the element with the specified attribute value.
   */
  protected Element findElement(String attribute, String value, Element element)
  {
    // if any of the parameters are empty
    if (attribute == null || value == null || element == null)
      return null;                      // we are done, exit method

    // check this element
    final String attributeValue = element.getAttributeValue(attribute);
    if (attributeValue != null && attributeValue.equals(value))
      return element;

    // get the children
    final List childrenList = element.getChildren();

    // if no children found
    if (childrenList == null || childrenList.size() <= 0)
      return null;                      // we are done, exit method

    final Iterator iterObj = childrenList.iterator();

    while (iterObj.hasNext())        // for each element in the list
    {
      final Object obj = iterObj.next();

      if (!(obj instanceof Element)) // if element not fetched OK
      {
        return null;                // abort method
      }

      // process this child element
      final Element childElement = findElement(attribute, value, (Element)obj);

      // if a match was found
      if (childElement != null)
        return childElement;  // return the match
    }
    return null;  // no match found
  }

  /**
   * Create the CDATA (Unparsed) Character Data. A CDATA section cannot contain the
   * string "]]>". Everything inside a CDATA section is ignored by the XML
   * parser.
   * @param text content of CDATA.
   * @return the CDATA (Unparsed) Character Data.
   */
  public static CDATA createCdata(String text)
  {
    return new CDATA(text);
  }
  
  /**
   * Gets the root element that was loaded.
   * @return the root element that was loaded, or null if none.
   */
  public Element getRootElement()
  {
    return rootElement;
  }

  /**
   * Gets the root element for the specified document.
   * @param docObj the Document.
   * @return the root element.
   */
  private static Element getRootElement(Document docObj)
  {
    if (useDocumentFlag)
      return docObj.getRootElement();
    else
      return docObj.detachRootElement();
  }

  /**
   * Sets the root element.
   * @param elemObj the element object to use.
   * @see createRootElement
   */
  public void setRootElement(Element elemObj)
  {
    rootElement = elemObj;
  }

  /**
   * Loads an XML file.  Attempts to open the given name as a local file,
   * as a URL, as a resource and as an entry in a 'jar' file (whichever
   * works first).
   * @param fileName the name of the XML file to load.
   * @param rootElementName the expected name of the root element in the
   * XML file, or null to accept any name.
   * @return true if the file was loaded, false if an error occurred (in
   * which case an error message may be fetched via the 'getErrorMessage()'
   * method).
   */
  public boolean loadFile(String fileName, String rootElementName)
  {
    return loadFile(fileName, rootElementName, (Properties)null);
  }
  
  /**
   * Loads an XML file.  Attempts to open the given name as a local file,
   * as a URL, as a resource and as an entry in a 'jar' file (whichever
   * works first).
   * @param fileName the name of the XML file to load.
   * @param rootElementName the expected name of the root element in the
   * XML file, or null to accept any name.
   * @param urlRequestProps the request properties to use for URL connections
   *                     or null if none.
   * @return true if the file was loaded, false if an error occurred (in
   * which case an error message may be fetched via the 'getErrorMessage()'
   * method).
   */
  public boolean loadFile(String fileName, String rootElementName,
      Properties urlRequestProps)
  {
    clearFetchedErrorMessage();        //clear prev error msg (if fetched)
    loadFileOpenedFlag = false;        //clear file-opened-OK flag
    final BufferedReader rdrObj;
    if((rdrObj=FileUtils.fileMultiOpen(fileName, urlRequestProps)) == null)
    {    //unable to open file; set error message
      setErrorMessageString(
                       "Unable to open XML data file \"" + fileName + "\"");
      return false;
    }
    loadFileOpenedFlag = true;         //set file-opened-OK flag
    final boolean retFlag = loadStream(rdrObj,rootElementName,fileName);
    try { rdrObj.close(); }            //close file input stream
    catch(IOException ex) {}
    return retFlag;                    //return result flag
  }

  /**
   * Returns the flag indicator of whether or not the XML file was
   * successfully opened in the last call to 'loadFile()'.
   * @return true if the file was successfully opened, false if not.
   */
  public boolean getLoadFileOpenedFlag()
  {
    return loadFileOpenedFlag;
  }

  /**
   * Loads an XML data from an input stream.
   * @param rdrObj input stream to use.
   * @param rootElementName the expected name of the root element in the
   * XML file, or null to accept any name.
   * @param fileName filename string to use for error messages, or null
   * for no filename.
   * @return true if the file was loaded, false if an error occurred (in
   * which case an error message may be fetched via the 'getErrorMessage()'
   * method).
   */
  public boolean loadStream(Reader rdrObj, String rootElementName,
                                                            String fileName)
  {
    clearFetchedErrorMessage();        //clear prev error msg (if fetched)
    final Element element;
    synchronized(xmlBuilderObj)
    {
      try          //load file into JDOM Element objects:
      {
        //build document from file:
        final Document documentObj = xmlBuilderObj.build(rdrObj);

        //get root element from document:
        element = getRootElement(documentObj);
      }
      catch(Exception ex)
      {  //error processing file; show message
        Throwable tObj;      //if JDOMException then use root cause if avail:
        final Throwable throwObj = (ex instanceof JDOMException &&
                 (tObj=((JDOMException)ex).getCause()) != null) ? tObj : ex;
        if(fileName != null)
        {     //error loading; set error message
          setErrorMessageString("Error loading XML data from file \"" +
                                             fileName + "\":  " + throwObj);
        }
        else
          setErrorMessageString("Error loading XML data:  " + throwObj);
        return false;             //abort method
      }
    }

         //confirm root element name (if given):
    if (rootElementName != null && (element == null ||
                      !element.getName().equalsIgnoreCase(rootElementName)))
    {    //expected root element not found; setup name for error message
      if(fileName != null)
      {  //error loading; set error message
        setErrorMessageString("Root element \"" + rootElementName +
                               "\" not found in file \"" + fileName + "\"");
      }
      else
      {
        setErrorMessageString("Root element \"" + rootElementName +
                                                "\" not found in XML data");
      }
      return false;                //abort method
    }

    // save the root element
    rootElement = element;
    return true;
  }

  /**
   * Loads an XML data from an input stream.
   * @param rdrObj input stream to use.
   * @param rootElementName the expected name of the root element in the
   * XML file, or null to accept any name.
   * @return true if the file was loaded, false if an error occurred (in
   * which case an error message may be fetched via the 'getErrorMessage()'
   * method).
   */
  public boolean loadStream(Reader rdrObj, String rootElementName)
  {
    return loadStream(rdrObj,rootElementName,null);
  }

  /**
   * Loads an XML data from an input stream.
   * @param stmObj input stream to use.
   * @param rootElementName the expected name of the root element in the
   * XML file, or null to accept any name.
   * @param fileName filename string to use for error messages, or null
   * for no filename.
   * @return true if the file was loaded, false if an error occurred (in
   * which case an error message may be fetched via the 'getErrorMessage()'
   * method).
   */
  public boolean loadStream(InputStream stmObj, String rootElementName,
                                                            String fileName)
  {
    return loadStream(new InputStreamReader(stmObj),rootElementName,
                                                                  fileName);
  }

  /**
   * Loads an XML data from an input stream.
   * @param stmObj input stream to use.
   * @param rootElementName the expected name of the root element in the
   * XML file, or null to accept any name.
   * @return true if the file was loaded, false if an error occurred (in
   * which case an error message may be fetched via the 'getErrorMessage()'
   * method).
   */
  public boolean loadStream(InputStream stmObj, String rootElementName)
  {
    return loadStream(new InputStreamReader(stmObj),rootElementName,null);
  }

  /**
   * Saves an element to a file.  Any needed directories above the file
   * are created.
   * @param fileName the name of the file to use.
   * @param elemObj element object to be saved.
   * @return true if the file was saved, false if an error occurred (in
   * which case an error message may be fetched via the 'getErrorMessage()'
   * method).
   */
  public boolean saveToFile(String fileName,Element elemObj)
  {
    clearFetchedErrorMessage();        //clear prev error msg (if fetched)
              //create any needed directories above file:
    FileUtils.createParentDirs(fileName);
    final BufferedWriter wtrObj;
    try
    {                   //create output stream for file:
      wtrObj = new BufferedWriter(new FileWriter(fileName));
    }
    catch(Exception ex)
    {         //error occurred; set error message
      setErrorMessageString(ex.toString());
      return false;     //return error flag
    }
    try
    {
      saveToStream(wtrObj,elemObj);    //save element data to stream
      wtrObj.close();                  //close output stream
      return true;                     //return OK flag
    }
    catch(Exception ex)
    {         //error occurred; set error message
      setErrorMessageString(ex.toString());
      try { wtrObj.close(); }          //close output stream
      catch(IOException ex2) {}
      return false;                    //return error flag
    }
  }

  /**
   * Saves the "root" element loaded via 'loadFile()' to a file.  Any
   * needed directories above the file are created.
   * @param fileName the name of the file to use.
   * @return true if the file was saved, false if an error occurred (in
   * which case an error message may be fetched via the 'getErrorMessage()'
   * method).
   */
  public boolean saveToFile(String fileName)
  {
    return saveToFile(fileName,rootElement);
  }

  /**
   * Returns true if an error was detected.  The error message may be
   * fetched via the 'getErrorMessage()' method.
   * @return true if an error was detected.
   */
  public boolean getErrorFlag()
  {
    return getErrorMessageFlag();
  }

  /**
   * Returns message string for last error (or 'No error' if none).
   * @return the error message.
   */
  public String getErrorMessage()
  {
    final String str;
    return ((str=getErrorMessageString()) != null) ? str : "No error";
  }

  /**
   * Clears the error message string.
   */
  public void clearErrorMessage()
  {
    clearErrorMessageString();
  }

  /**
   * Creates the root element with the specified name.
   * @param name <code>String</code> name of element.
   * @return the root Element.
   * @see setRootElement
   */
  public Element createRootElement(String name)
  {
    final Element elemObj = new RootElement(name);
    setRootElement(elemObj);
    return elemObj;
  }

  /**
   * Enters an error message (if none previously entered).
   * @param str the error message.
   */
  protected void setErrorMessage(String str)
  {
    enterErrorMessageString(str);
  }

  /**
   * Converts a JDOM 'Element' object to an XML-format string.
   * @param xmlMsgObj the JDOM 'Element' object to use.
   * @param normalizeFlag if true then the returned string is normalized
   * by removing CR/LF characters (all surrounding whitespace is
   * removed and internal whitespace is changed to a single space).
   * @return The string version of the element (and its children).
   * @throws IOException if an error occurs while converting.
   */
  public static String elementToString(Element xmlMsgObj,
                                   boolean normalizeFlag) throws IOException
  {
    final StringWriter wtrObj = new StringWriter();
         //convert 'Element' object & send output to 'StringWriter' stream:
                                  //('output()' method flushes data)
    synchronized(xmlStrOutputterObj)
    {
      xmlStrOutputterObj.output(xmlMsgObj,wtrObj);
    }
         //convert stream to String and return:
    return normalizeFlag ?        //if flag then normalize (remove CR/LFs)
       org.jdom.Text.normalizeString(wtrObj.toString()) : wtrObj.toString();
  }

  /**
   * Converts a JDOM 'Element' object to an XML-format string.
   * @param xmlMsgObj the JDOM 'Element' object to use.
   * @return The string version of the element (and its children).
   * @throws IOException if an error occurs while converting.
   */
  public static String elementToString(Element xmlMsgObj) throws IOException
  {
    return elementToString(xmlMsgObj,false);
  }

  /**
   * Converts a JDOM 'Element' object to an XML-format string.  Whitespace
   * between elements is removed and control characters and characters
   * greater than 127 within elements are encoded to "&##;" strings.
   * This allows linefeeds to be encoded and then decoded later.
   * @param xmlMsgObj the JDOM 'Element' object to use.
   * @return The string version of the element (and its children).
   * @throws IOException if an error occurs while converting.
   */
  public static String elementToFixedString(Element xmlMsgObj)
                                                          throws IOException
  {
    final StringWriter wtrObj = new StringWriter();
         //convert 'Element' object & send output to 'StringWriter' stream:
                                  //('output()' method flushes data)
    synchronized(xmlFixedStrOutputterObj)
    {
      xmlFixedStrOutputterObj.output(xmlMsgObj,wtrObj);
    }
         //convert control characters to "&##;" escape strings:
    final StringBuffer buff = wtrObj.getBuffer();
    IstiXmlUtils.ctrlCharsToEscapedCodes(buff);
    return buff.toString();       //return string version of buffer
  }

  /**
   * Converts an XML-format string to a JDOM 'Element' object.
   * @param xmlStr the XML-format string to use.
   * @return a JDOM 'Element' object.
   * @throws JDOMException if an error occurs while parsing.
   */
  public static Element stringToElement(String xmlStr) throws JDOMException
  {
    synchronized(xmlBuilderObj)
    {
      try
      {
        try
        {     //interpret given string via UTF-8 encoding:
          final ByteArrayInputStream inStmObj = new ByteArrayInputStream(
                                    xmlStr.getBytes(UTF_8_ENCODING_FORMAT));
          return getRootElement(xmlBuilderObj.build(inStmObj));
        }
        catch(UnsupportedEncodingException ex)
        {     //interpret via UTF-8 encoding failed; use default below
        }
        return getRootElement(xmlBuilderObj.build(new StringReader(xmlStr)));
      }
      catch(IOException ex)
      {  //exception reading data; wrap new JDOMException around it
        throw new JDOMException("I/O error while reading XML string data",
                                                                        ex);
      }
    }
  }

  /**
   * Saves an element to a stream.
   * @param wtrObj output stream to use.
   * @param elemObj element object to be saved.
   * @throws IOException if an error occurs.
   */
  public static void saveToStream(Writer wtrObj,Element elemObj)
                                                          throws IOException
  {
    synchronized(xmlFileOutputterObj)
    {
      final Document docObj;
      if (useDocumentFlag && (docObj = elemObj.getDocument()) != null)
        xmlFileOutputterObj.output(docObj, wtrObj);
      else
        xmlFileOutputterObj.output(elemObj, wtrObj);
    }
  }

  /**
   * Saves an element to a stream.
   * @param stmObj output stream to use.
   * @param elemObj element object to be saved.
   * @throws IOException if an error occurs.
   */
  public static void saveToStream(OutputStream stmObj,Element elemObj)
                                                          throws IOException
  {
    synchronized (xmlFileOutputterObj)
    {
      final Document docObj;
      if (useDocumentFlag && (docObj = elemObj.getDocument()) != null)
        xmlFileOutputterObj.output(docObj, stmObj);
      else
        xmlFileOutputterObj.output(elemObj, stmObj);
    }
  }

  /**
   * Converts all the child elements of the given element to strings
   * and returns the concatenation of all the strings.  Any textual
   * content directly held under this element is also included.
   * @param parentElementObj the element object to use.
   * @param normalizeFlag if true then the returned string is normalized
   * by removing CR/LF characters (all surrounding whitespace is
   * removed and internal whitespace is changed to a single space).
   * @param fixStrFlag true to "fix" string data (whitespace between
   * elements removed and control characters within elements encoded
   * to "&##;" strings); false to normalize string data.
   * @return The string version of all the child data and elements.
   */
  protected static String getChildDataStr(Element parentElementObj,
                                  boolean normalizeFlag, boolean fixStrFlag)
  {
    final StringWriter wtrObj = new StringWriter();
    if(parentElementObj != null)
    {    //element not null
      final String str;
      if((str=parentElementObj.getText()) != null && str.length() > 0)
      {  //text content exists
        wtrObj.write(str);        //write text to stream
        wtrObj.flush();           //flush buffers
      }
      final List childrenList = parentElementObj.getChildren();
      if(childrenList.size() > 0)
      {  //element contains children
                        //if flag then use "fixed-string" outputter:
        final XMLOutputter outputterObj = fixStrFlag ?
                               xmlFixedStrOutputterObj : xmlStrOutputterObj;
        synchronized(outputterObj)
        {     //grab thread lock for outputter object
          final Iterator iterObj = childrenList.iterator();
          Object obj;
          while(iterObj.hasNext())
          {   //for each child element under parent
            if((obj=iterObj.next()) instanceof Element)
            {      //child Element object fetched OK
                        //output string version of element to stream:
                                  //('output()' method flushes data)
              try { outputterObj.output((Element)obj,wtrObj); }
              catch(IOException ex) {}      //ignore any IO exceptions
            }
          }
        }
      }
    }
         //convert stream to String (if flag then "fix" string):
    final String retStr = fixStrFlag ?
             ctrlCharsToEscapedCodes(wtrObj.toString()) : wtrObj.toString();
         //return generated string (if flag then normalize string):
    return normalizeFlag ? org.jdom.Text.normalizeString(retStr) : retStr;
  }

  /**
   * Converts all the child elements of the given element to strings
   * and returns the concatenation of all the strings.  Any textual
   * content directly held under this element is also included.
   * @param parentElementObj the element object to use.
   * @param normalizeFlag if true then the returned string is normalized
   * by removing CR/LF characters (all surrounding whitespace is
   * removed and internal whitespace is changed to a single space).
   * @return The string version of all the child data and elements.
   */
  public static String getChildDataStr(Element parentElementObj,
                                                      boolean normalizeFlag)
  {
    return getChildDataStr(parentElementObj,normalizeFlag,false);
  }

  /**
   * Converts all the child elements of the given element to strings
   * and returns the concatenation of all the strings.  Any textual
   * content directly held under this element is also included.  The
   * returned text is not normalized.
   * @param parentElementObj the element object to use.
   * @return The string version of all the child data and elements.
   */
  public static String getChildDataStr(Element parentElementObj)
  {
    return getChildDataStr(parentElementObj,false,false);
  }

  /**
   * Converts all the child elements of the given element to strings
   * and returns the concatenation of all the strings.  Any textual
   * content directly held under this element is also included.  Whitespace
   * between elements is removed and control characters within elements are
   * encoded to "&##;" strings.  This allows linefeeds to be encoded and
   * then decoded later.
   * @param parentElementObj the element object to use.
   * @return The string version of all the child data and elements.
   */
  public static String getChildDataFixedStr(Element parentElementObj)
  {
    return getChildDataStr(parentElementObj,false,true);
  }

  /**
   * Returns the first child element within the given element with the
   * given local name (regardless of namespace).
   * @param parentElem element containing the child element.
   * @param childNameStr name of child element to match, or null to
   * match the first child element found.
   * @return The first matching child element, or null if none found.
   */
  public static Element getAnyNSChild(Element parentElem,
                                                        String childNameStr)
  {
    final List listObj;
    if((listObj=parentElem.getChildren()) != null)
    {
      final Iterator iterObj = listObj.iterator();
      Object obj;
      Element elemObj;
      while(iterObj.hasNext())
      {
        if((obj=iterObj.next()) instanceof Element)
        {
          elemObj = (Element)obj;
          if(childNameStr == null || childNameStr.equals(elemObj.getName()))
            return elemObj;
        }
      }
    }
    return null;
  }

  /**
   * Gets the encoded string.
   * @param s the string to encode.
   * @param encoding set encoding format.  Use XML-style names like
   *                 "UTF-8" or "ISO-8859-1" or "US-ASCII"
   * @return the encoded string or the original string if an error occurs.
   */
  public static String getEncodedString(String s,String encoding)
  {
    try { s = new String(s.getBytes(),encoding); }
    catch (Exception ex) {}
    return s;
  }

  /**
   * Converts control characters and characters greater than 127 in the
   * given data to "&##;" escape strings.
   * @param buff data buffer to be read and modified.
   * @return true if any control characters where converted; false
   * if not.
   */
  public static boolean ctrlCharsToEscapedCodes(StringBuffer buff)
  {
    boolean modFlag = false;
    int encLen, idx = 0, buffLen = buff.length();
    String encStr;
    char ch;
    while(idx < buffLen)
    {  //for each character in buffer
      if((ch=buff.charAt(idx)) < ' ' || ch > (char)0x007F)
      {  //control character or character greater than 127 found
                        //build encoded string for character:
        encStr = "&#" + Integer.toString((int)ch) + ';';
        buff.replace(idx,idx+1,encStr);     //replace char with encoded str
        encLen = encStr.length();           //length of inserted string
        buffLen += encLen - 1;              //adjust buffer length
        idx += encLen;                      //adjust index
        modFlag = true;                     //indicate control chars found
      }
      else  //not a control character
        ++idx;          //increment to next character in buffer
    }
    return modFlag;
  }

  /**
   * Converts control characters and characters greater than 127 in the
   * given data to "&##;" escape strings.
   * @param srcStr source string for data.
   * @return The converted string, or the original string if no
   * control characters were converted.
   */
  public static String ctrlCharsToEscapedCodes(String srcStr)
  {
    final StringBuffer buff = new StringBuffer(srcStr);
    return ctrlCharsToEscapedCodes(buff) ? buff.toString() : srcStr;
  }

  /**
   * Converts "&##;" escape strings in the given data to characters.
   * @param srcStr source string for data.
   * @param maxEscVal maximum value for "&##;" codes, or 0 for no maximum.
   * @return The converted string, or the original string if no
   * characters were converted.
   */
  public static String convertFromEscapedCodes(String srcStr, int maxEscVal)
  {
    int ampPos;
    if((ampPos=srcStr.indexOf('&')) < 0)
      return srcStr;         //if no escape strings then return given string
    final StringBuffer buff = new StringBuffer(srcStr);
    int codeVal, p = ampPos, buffLen = buff.length();
    char ch;
    outerLoop:          //label to allow 'break' from outer loopr
    while(true)
    {  //loop while processing escape strings
      if(++p >= buffLen)          //increment scan position
        break outerLoop;          //if at end then exit outer loop
      if((ch=buff.charAt(p)) == '#')
      {  //found '#' character after ampersand
        while(true)
        {  //loop while processing numeric chars after "&#"
          if(++p >= buffLen)      //increment scan position
            break outerLoop;      //if at end then exit outer loop
          if(!Character.isDigit(ch=buff.charAt(p)))
          {  //character is not numeric
            if(ch == ';')
            {  //character is terminating semicolon
              if(p > ampPos+2)
              {  //numeric chars found after "&#"
                try
                {
                  codeVal = Integer.parseInt(buff.substring(ampPos+2,p));
                  if(codeVal > 0 && (maxEscVal <= 0 || codeVal <= maxEscVal))
                  {  //code value positive and <= max (if max given)
                             //replace ampersand with decoded character:
                    buff.setCharAt(ampPos,(char)codeVal);
                             //remove rest of escape string:
                    buff.delete(ampPos+1,p+1);
                    buffLen -= (p - ampPos);     //adjust buffer length
                    p = ampPos;                  //adjust parse position
                  }
                }
                catch(Exception ex)
                {  //some kind of exception error; move on to next chars
                }
              }
            }
            break;
          }
        }
      }
      else if(ch == '&')     //if another ampersand then decrement pos
        --p;                 // to reprocess ampersand via outer loop
      while(true)
      {  //loop while seaching for next ampersand
        if(++p >= buffLen)        //increment scan position
          break outerLoop;        //if at end then exit outer loop
        if(buff.charAt(p) == '&')
        {  //ampersand found
          ampPos = p;             //save position of ampersand
          break;                  //loop and process next escape string
        }
      }
    }
    return buff.toString();       //return string version of buffer
  }

  /**
   * Converts "&##;" escape strings in the given data to control
   * characters.
   * @param srcStr source string for data.
   * @return The converted string, or the original string if no
   * characters were converted.
   */
  public static String ctrlCharsFromEscapedCodes(String srcStr)
  {
    return convertFromEscapedCodes(srcStr,0);
  }


  /**
   * Root element.
   */
  public static class RootElement extends Element
  {
    /**
     * Creates the root element and ensures there is a document.
     * @param name <code>String</code> name of element.
     */
    public RootElement(String name)
    {
      super(name);
      if (useDocumentFlag)
      {
        //make sure there is a document
        Document docObj;
        if ( (docObj = getDocument()) == null)
        {
          docObj = new Document(this);
          setDocument(docObj);
        }
      }
    }
  }
}
