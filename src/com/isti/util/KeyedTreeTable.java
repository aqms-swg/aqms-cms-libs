//KeyedTreeTable.java:  Defines a "keyed" tree-table where any number
//                      of 'value' objects may be associated with a
//                      'key' object.
//
//   6/1/2004 -- [ET]  Initial version.
//  7/30/2007 -- [ET]  Fixed memory leak by removing 'dupsCheckListObj'.
//

package com.isti.util;

import java.util.List;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;

/**
 * Class KeyedTreeTable defines a "keyed" tree-table where any number
 * of 'value' objects may be associated with a 'key' object.  Removal
 * of "old" entries is also supported.  The methods in this class are
 * thread safe.
 */
public class KeyedTreeTable
{
    /** Flat list of timed-value blocks for all 'value' objects in table. */
  protected final ArrayList valueBlksListObj = new ArrayList();
    /** Entries table where each entry-value is a 'List' object. */
  protected final HashMap keyEntriesMapObj = new HashMap();
    /** Flag set true if duplicate entries are to be rejected. */
  protected final boolean rejectDuplicatesFlag;

  /**
   * Creates a "keyed" tree-table where any number of 'value' objects
   * may be associated with a 'key' object.
   * @param rejectDuplicatesFlag true to reject duplicate 'value' objects.
   */
  public KeyedTreeTable(boolean rejectDuplicatesFlag)
  {
    this.rejectDuplicatesFlag = rejectDuplicatesFlag;
  }

  /**
   * Adds a 'value' object to the table.
   * @param keyObj the 'key' object to be associated with the 'value'
   * object.
   * @param valueObj the 'value' object to use.
   * @return true if the 'value' object was added; false if the 'value'
   * object matched an existing 'value' object in the table and the
   * table was constructed with 'rejectDuplicatesFlag'==true.
   */
  public synchronized boolean addEntry(Object keyObj, Object valueObj)
  {
              //create timed-value block for value:
    final TimedValueBlock blkObj = new TimedValueBlock(valueObj);
    Object obj;
    final List listObj;
    if((obj=keyEntriesMapObj.get(keyObj)) instanceof List)
    {  //matching key found in table
      listObj = (List)obj;        //set handle to list for key
      if(rejectDuplicatesFlag)
      {  //checking for and rejecting duplicate values
        final Iterator iterObj = listObj.iterator();
        while(iterObj.hasNext())
        {  //for each 'TimedValueBlock' object in list
          if((obj=iterObj.next()) instanceof TimedValueBlock)
          {  //'TimedValueBlock' object fetched OK
            if(valueObj.equals(((TimedValueBlock)obj).valueObj))
              return false;       //if duplicate then return false
          }
        }
      }
    }
    else
    {  //no matching key in table
      listObj = new ArrayList();                 //create new list object
      keyEntriesMapObj.put(keyObj,listObj);      //enter list into table
    }
    listObj.add(blkObj);               //add timed-value block to list
    blkObj.listObj = listObj;          //enter list handle into block
    valueBlksListObj.add(blkObj);           //add block to "flat" list
    return true;
  }

  /**
   * Returns an iterator for the values associated with the given
   * key object.  Later changes to the table will not affect the
   * list referenced via the returned iterator.
   * @param keyObj the key object to use.
   * @return A new iterator, or null if no entry matches the given
   * key object.
   */
  public synchronized Iterator getValues(Object keyObj)
  {
    final Object obj;
    if((obj=keyEntriesMapObj.get(keyObj)) instanceof List &&
                                             ((List)obj).size() > 0)
    {    //List object exists for table and List is not empty
              //use ModIterator so that a copy of the list is use:
      return new ModIterator((List)obj)
          {                            //modify 'next()' method to extract
            public Object next()       // 'value' object from block
            {
              final Object obj;
              return ((obj=super.next()) instanceof TimedValueBlock) ?
                                      ((TimedValueBlock)obj).valueObj : obj;
            }
          };
    }
    return null;
  }

  /**
   * Removs the given 'value' object from the table.
   * @param keyObj the 'key' object associated with the 'value' object.
   * @param valueObj the 'value' object to remove.
   * @return true if the 'value' object was removed; false if a matching
   * 'value' could not be found.
   */
  public synchronized boolean removeEntry(Object keyObj, Object valueObj)
  {
    Object obj;
    final List listObj;
    final int p;
              //get List for 'key' object; get matching
              // 'TimedValueBlock' in List for given 'value':
    if((obj=keyEntriesMapObj.get(keyObj)) instanceof List &&
                                             (p=(listObj=(List)obj).indexOf(
                                     new TimedValueBlock(valueObj))) >= 0 &&
                            (obj=listObj.get(p)) instanceof TimedValueBlock)
    {    //matching 'TimedValueBlock' object fetched OK; remove it
      return removeTimedValueBlock((TimedValueBlock)obj);
    }
    return false;
  }

  /**
   * Removes all entries associated with the given 'key' object.
   * @param keyObj the 'key' object to remove.
   * @return true if any entries were removed; false if not.
   */
  public synchronized boolean removeEntriesForKey(Object keyObj)
  {
    boolean retFlag = false;
    Object obj;
    if((obj=keyEntriesMapObj.get(keyObj)) instanceof List)
    {    //List for given key fetched OK
      final Iterator iterObj = new ModIterator((List)obj);
      while(iterObj.hasNext())
      {  //for each timed-value block in List; remove from List
        if((obj=iterObj.next()) instanceof TimedValueBlock &&
                                removeTimedValueBlock((TimedValueBlock)obj))
        {     //timed-value block was removed
          retFlag = true;
        }
      }
    }
    return retFlag;
  }

  /**
   * Removes "old" entries from the table.
   * @param minTimeMsVal the minimum time value (in ms since 1/1/1970 GMT)
   * such that entries with time values below this value are removed.
   * @param maxRemoveCount the maximum number of entries to be removed,
   * or 0 for no limit.
   * @param returnRemovedFlag true to return an iterator that can be used
   * to fetch a list of removed entries; false for none.
   * @return An iterator that can be used to fetch a list of removed
   * values if entries were removed and 'returnRemovedFlag'==true;
   * null otherwise.
   */
  public Iterator removeOldEntries(long minTimeMsVal, int maxRemoveCount,
                                                  boolean returnRemovedFlag)
  {
    final Iterator iterObj;       //get iterator for "flat" list
    synchronized(this)            // of timed-value blocks:
    {    //only allow one thread at a time to use table
      if(valueBlksListObj.size() <= 0)
        return null;         //if table empty then just return
      iterObj = new ModIterator(valueBlksListObj);
    }
    Object obj;
    TimedValueBlock blkObj;
    ArrayList removedListObj = null;
    int cnt = 0;
    while(iterObj.hasNext())
    {    //for each entry to be checked
      if((obj=iterObj.next()) instanceof TimedValueBlock)
      {  //'TimedValueBlock' object fetched OK
        blkObj = (TimedValueBlock)obj;
        if(blkObj.timestamp >= minTimeMsVal)
          break;        //if beyond minimum timestamp then exit loop
        removeTimedValueBlock(blkObj);      //remove timed-value block
        if(returnRemovedFlag)
        {     //flag set; add removed value to list
          if(removedListObj == null)             //if list not created then
            removedListObj = new ArrayList();    //create list
          removedListObj.add(blkObj.valueObj);   //add value to list
        }
        if(maxRemoveCount > 0 && ++cnt >= maxRemoveCount)
          break;        //if limit and reached then exit loop
      }
    }
              //if "removed" list created then return iterator; else null:
    return (removedListObj != null) ? removedListObj.iterator() : null;
  }

  /**
   * Removes "old" entries from the table.
   * @param minTimeMsVal the minimum time value (in ms since 1/1/1970 GMT)
   * such that entries with time values below this value are removed.
   * @param maxRemoveCount the maximum number of entries to be removed,
   * or 0 for no limit.
   */
  public void removeOldEntries(long minTimeMsVal, int maxRemoveCount)
  {
    removeOldEntries(minTimeMsVal,maxRemoveCount,false);
  }

  /**
   * Returns a display string containing all the current values in the
   * table.  The values are shown in a "flat" list in the order that
   * they were entered and with their associated timestamp values.
   * @return A new display-list string.
   */
  public String getValuesDisplayStr()
  {
    final StringBuffer buff = new StringBuffer();
    final Iterator iterObj;       //get iterator for "flat" list
    synchronized(this)            // of timed-value blocks:
    {    //only allow one thread at a time to use table
      iterObj = new ModIterator(valueBlksListObj);
    }
    Object obj;
    TimedValueBlock blkObj;
    if(iterObj.hasNext())
    {    //list not empty
      while(true)
      {  //for each timed-value block in list
        if((obj=iterObj.next()) instanceof TimedValueBlock)
        {     //'TimedValueBlock' object fetched OK
          blkObj = (TimedValueBlock)obj;
          buff.append("\"" + blkObj.valueObj + "\", time=" +
                                                          blkObj.timestamp);
          if(!iterObj.hasNext())
            break;           //if no more entries then exit loop
          buff.append(", ");           //add separator
        }
      }
    }
    return buff.toString();
  }

  /**
   * Returns a display string containing all the current entries in the
   * table.  Each key is shown, followed by its associated values.
   * @param sepStr the separator string to use between key-entries.
   * @return A new display-list string.
   */
  public synchronized String getTableDisplayStr(String sepStr)
  {
    final StringBuffer buff = new StringBuffer();
    final Iterator keySetIterObj = keyEntriesMapObj.keySet().iterator();
    if(keySetIterObj.hasNext())
    {    //table is not empty
      Object keyObj;
      Iterator iterObj;
      while(true)
      {  //for each entry in table
        keyObj = keySetIterObj.next();      //fetch and show 'key' object
        buff.append("key=\"" + keyObj + "\", values:  ");
        if((iterObj=getValues(keyObj)) != null)
        {     //List object for key fetched OK
          if(iterObj.hasNext())
          {   //List is not empty
            while(true)
            {      //for each value in list; show value
              buff.append("\"" + iterObj.next() + "\"");
              if(!iterObj.hasNext())
                break;       //if no more values then exit loop
              buff.append(", ");       //add separator
            }
          }
        }
        else
          buff.append("???");
        if(!keySetIterObj.hasNext())
          break;             //if no more entries then exit loop
        buff.append(sepStr);           //add given separator string
      }
    }
    return buff.toString();
  }

  /**
   * Returns a display string containing all the current entries in the
   * table.  Each key is shown, followed by its associated values, with
   * each key-entry on a separate line.
   * @return A new display-list string.
   */
  public String getTableDisplayStr()
  {
    return getTableDisplayStr(UtilFns.newline);
  }

  /**
   * Removes the given timed-value block from the table.
   * @param blkObj the timed-value block block to remove.
   * @return true if the block was removed; false if a matching
   * 'block could not be found.
   */
  protected synchronized boolean removeTimedValueBlock(
                                                     TimedValueBlock blkObj)
  {
    final boolean retFlag;
    if(blkObj.listObj != null)
    {    //List object for block OK, remove product from block list
      retFlag = blkObj.listObj.remove(blkObj);
              //if no more blocks in list then remove entry from table:
      if(blkObj.listObj.size() <= 0)
        keyEntriesMapObj.values().remove(blkObj.listObj);
         //remove from "flat" list of timed-value blocks:
      valueBlksListObj.remove(blkObj);
    }
    else
      retFlag = false;
    return retFlag;
  }


  /**
   * Class TimedValueBlock defines a block that holds a 'value' object,
   * a timestamp, and a reference to a 'List' object.
   */
  protected static class TimedValueBlock
  {
    public final Object valueObj;
    public final long timestamp;
    public List listObj = null;

    /**
     * Creates a timed-value block.
     * @param valueObj the value object to use.
     */
    public TimedValueBlock(Object valueObj)
    {
      this.valueObj = valueObj;
      timestamp = System.currentTimeMillis();
    }

    /**
     * Returns the string representation for the 'value' object held
     * by this block.
     * @return The string representation for the 'value' object held
     * by this block.
     */
    public String toString()
    {
      return (valueObj != null) ? valueObj.toString() : "(null)";
    }

    /**
     * Tests if the given object is equal to the 'value' object held
     * by this block.
     * @param obj the given object.
     * @return true if equal; false if not.
     */
    public boolean equals(Object obj)
    {
      return (valueObj != null) ?
                          valueObj.equals((obj instanceof TimedValueBlock) ?
                     ((TimedValueBlock)obj).valueObj : obj) : (obj == null);
    }

    /**
     * Returns the hash code for the 'value' object held by this block.
     * @return The hash code for the 'value' object held by this block.
     */
    public int hashCode()
    {
      return (valueObj != null) ? valueObj.hashCode() : 0;
    }
  }
}
