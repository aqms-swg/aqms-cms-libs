package com.isti.util;
import java.io.*;

/**
 * <p>Title: LineProcOutputStream</p>
 * <p>Description: Abstract class that can be dropped in, in place of any
 * OutputStream class (stdout for example), to handle character-stream data
 * as lines.  A user-supplied call-back method is handed one line of data
 * at a time.</p>
 * @author Hal Schechner
 *
 *   7/9/2003 -- [ET]  Modified 'flush()' to only execute "call-back"
 *                     method if buffer contains data; various minor
 *                     modifications.
 *
 */
public abstract class LineProcOutputStream extends OutputStream
                                            implements CallBackStringParam {

  /** Where we keep the current line. */
  protected final StringBuffer lineBuffer = new StringBuffer();

  /** String defining what newline char(s) looks like. */
  protected final String EOL = "\n";

  /** Length of newline char(s). */
  protected final int EOL_LEN = EOL.length();

  /***************************************/

  /**
   * Enters the given character into the output stream.  The characters
   * are buffered until a newline ('\n') character is encountered; then
   * the entire line of characters (including the newline) is sent to
   * the call-back method.
   * @param ch the character to enter.
   */
  public void write(int ch) {
    lineBuffer.append((char)ch);
    final int endPos = lineBuffer.length() - EOL_LEN;
    if(endPos >= 0 && EOL.equals(lineBuffer.substring(endPos))) {
      // send the line to the call-back method
      callBackMethod(lineBuffer.toString());
      // empty the buffer
      lineBuffer.setLength(0);
    }
  }

  /**
   * Send buffer content to call-back method (if not empty).
   */
  public void flush() {
    if(lineBuffer.length() > 0) {
      callBackMethod(lineBuffer.toString());
      lineBuffer.setLength(0);
    }
  }
}
