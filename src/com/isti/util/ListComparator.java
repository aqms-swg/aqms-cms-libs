//ListComparator.java:  Implements a comparator that determines
//                      order using a given list of objects.
//
//  1/20/2004 -- [ET]
//

package com.isti.util;

import java.util.Comparator;
import java.util.List;
import java.util.Arrays;

/**
 * Class ListComparator implements a comparator that determines
 * order using a given list of objects.
 */
public class ListComparator implements Comparator
{
  protected final List comparatorListObj;
  protected final int comparatorListSize;

  /**
   * Creates a comparator that determines order using the given list
   * of objects.
   * @param listObj the list of objects to use.
   */
  public ListComparator(List listObj)
  {
    comparatorListObj = listObj;
    comparatorListSize = comparatorListObj.size();
  }

  /**
   * Creates a comparator that determines order using the given array
   * of objects.
   * @param objArr the array of objects to use.
   */
  public ListComparator(Object [] objArr)
  {
    this(Arrays.asList(objArr));
  }

  /**
   * Compares its two arguments for order via their position in the
   * list.  Returns a negative integer, zero, or a positive integer
   * as the first argument is less than, equal to, or greater than
   * the second.  If an object is not found in the list then it is
   * considered to be "after" all objects that are in the list.
   * @param obj1 the first object to be compared.
   * @param obj2 the second object to be compared.
   * @return a negative integer, zero, or a positive integer as the
   * 	       first argument is less than, equal to, or greater than the
   *	       second.
   * @throws ClassCastException if the arguments' types prevent them
   *         from being compared by this Comparator.
   */
  public int compare(Object obj1, Object obj2)
  {
    int idx1;           //get index of object in list:
    if((idx1=comparatorListObj.indexOf(obj1)) < 0)
      idx1 = comparatorListSize;       //if not in list then use last+1
    int idx2;           //get index of object in list:
    if((idx2=comparatorListObj.indexOf(obj2)) < 0)
      idx2 = comparatorListSize;       //if not in list then use last+1
    return idx1 - idx2;      //return difference of index values
  }

  /**
   * Indicates whether some other object is &quot;equal to&quot; this
   * Comparator.
   * @param   obj   the reference object with which to compare.
   * @return  <code>true</code> only if the specified object is also
   *		a comparator and it imposes the same ordering as this
   *		comparator.
   */
  public boolean equals(Object obj)
  {
    return (obj instanceof ListComparator &&
         comparatorListObj.equals(((ListComparator)obj).comparatorListObj));
  }
}
