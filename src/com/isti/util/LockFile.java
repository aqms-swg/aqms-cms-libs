package com.isti.util;

import java.io.Closeable;
import java.io.File;
import java.io.RandomAccessFile;
import java.nio.channels.FileLock;

/**
 * @author kevin
 */
public class LockFile implements Closeable
{
  /**
   * Create an exclusive lock on the specified file.
   * 
   * @param file the file.
   * @return the lock file if able to acquire an exclusive lock, otherwise null.
   */
  public static LockFile createLock(final File file)
  {
    return createLock(file, false);
  }

  /**
   * Create an exclusive lock on the specified file.
   * 
   * @param file   the file.
   * @param shared {@code true} to request a shared lock, {@code false} to
   *               request an exclusive lock
   * @return the lock file if able to acquire an exclusive lock, otherwise null.
   */
  public static LockFile createLock(final File file, boolean shared)
  {
    // Try to get the lock
    RandomAccessFile randomAccessFile = null;
    FileLock lock = null;
    try
    {
      randomAccessFile = new RandomAccessFile(file, "rw");
      lock = randomAccessFile.getChannel().tryLock(0L, Long.MAX_VALUE, shared);
      // lockFile =
      if (lock != null && lock.isValid())
      {
        return new LockFile(file, randomAccessFile, lock);
      }
    }
    catch (Exception ex)
    {
      LogFile.getGlobalLogObj().warning(
          "unable to acquire lock (" + file + "): " + ex.getLocalizedMessage());
    }
    UtilFns.closeQuietly(lock);
    UtilFns.closeQuietly(randomAccessFile);
    return null;
  }

  private final File file;
  private final FileLock lock;
  private final RandomAccessFile randomAccessFile;

  /**
   * Create the lock file.
   * 
   * @param file             the file.
   * @param randomAccessFile the random access file.
   * @param lock             the file lock.
   */
  private LockFile(File file, RandomAccessFile randomAccessFile, FileLock lock)
  {
    this.file = file;
    this.randomAccessFile = randomAccessFile;
    this.lock = lock;
  }

  @Override
  public void close()
  {
    try
    {
      lock.release();
    }
    catch (Exception ex)
    {
      LogFile.getGlobalLogObj().warning(
          "unable to release lock (" + file + "): " + ex.getLocalizedMessage());
    }
    UtilFns.closeQuietly(randomAccessFile);
    try
    {
      file.delete();
    }
    catch (Exception ex)
    {
      LogFile.getGlobalLogObj().logMessage(LogFile.WARNING,
          "could not delete lock file:" + file, ex);
    }
  }
}
