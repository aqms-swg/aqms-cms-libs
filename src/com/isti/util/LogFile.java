//LogFile.java:  Provides log file output services.
//
//   7/12/2000 -- [ET]  Initial release version.
//   8/21/2001 -- [ET]  Changed output of timestamps from AM/PM to 24-hour
//                      clock format; added 'gmtFlag' parameters to allow
//                      timestamps in GMT.
//   8/15/2002 -- [ET]  Added 'DEBUG2' through 'DEBUG5' message levels.
//   8/16/2002 -- [ET]  Added definitions of String versions for message
//                      levels and 'levelStringToValue()' method.
//   10/2/2002 -- [ET]  Removed "Log file opened" message sent to console.
//   1/19/2003 -- [ET]  Added thread synchronization.
//    2/3/2003 -- [ET]  Added ability to created any parent directories
//                      needed for specified log output file/path name.
//   4/14/2003 -- [KF]  Added the option to use the date in the file name
//                      ('useDateInFnameFlag'.)
//    6/9/2003 -- [KF]  Added 'getMaxLogFileAge' and 'setMaxLogFileAge'
//                      methods to optionally delete old log files.
//    6/9/2003 -- [ET}  Modifed the 'logFileFilterStr' variable used by
//                      'deleteOldLogFiles()' so as to only delete old
//                      log files that match the log file name given in
//                      the constructor; added 'FNAME_DATE_SEPSTR';
//                      added 'NO_LEVEL' and version of 'println()'
//                      method without a log-level parameter.
//   6/10/2003 -- [HS]  Added "Log file continued in ___" and "Log file
//                      continued from ___" to the end/start of date-based
//                      log files.
//   6/10/2003 -- [ET]  Added 'setLogFileSwitchIntervalDays()' method and
//                      implementation.
//   6/11/2003 -- [ET]  Fixed null-pointer exception in constructor when
//                      null-string filename given.
//   6/19/2003 -- [KF]  Added 'getDateFormat()' method.
//   6/27/2003 -- [ET]  Added 'getLevelStringNull()', 'getLevelNamesVector()',
//                      'getLevelNamesArray()' & 'getLevelNamesDisplayStr()'
//                      methods; modified 'levelStringToValue()' to trim
//                      whitespace on string parameter before processing.
//    7/9/2003 -- [ET]  Added 'getLogOutputStream()' method.
//   7/16/2003 -- [ET]  Moved 'MS_PER_DAY' to "UtilFns".
//    8/6/2004 -- [KF]  Added 'getGlobalLogObj(boolean consoleFlag)' method.
//   8/11/2004 -- [ET]  Modified to only create static "console" and "null"
//                      log file objects when they are needed.
//   8/12/2004 -- [KF]  Added 'initGlobalLogObj(LogFile logObj)',
//                      'getConsoleLogObj()' and 'getNullLogObj()' methods;
//                      changed console log file object to log all messages.
//    2/8/2006 -- [ET]  Added message-tracking implementation and methods.
//   12/7/2006 -- [ET]  Moved date-formatter patterns to public strings.
//   7/13/2009 -- [KF]  Added 'IstiConsoleLogger' class.
//   7/13/2009 -- [ET]  Added "logWriter != null" check to 'logMessage()'
//                      method.
//   7/14/2009 -- [KF]  Removed 'NO_LEVEL' from 'IstiLogger' interface.
//  11/15/2011 -- [ET]  Added optional 'writeHdrFdrFlag' parameter to
//                      constructor; added 'setWriteDatePrefixFlag()'
//                      method.
//  4/06/2012 -- [KF]  Added 'getMinLevel()' method.
//  4/21/2017 -- [KF]  Added 'logStandardErr' and 'logStandardOut' methods.
//
//=====================================================================
// Copyright (C) 2017 Instrumental Software Technologies, Inc.
//
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions
// are met:
// 1. Redistributions of source code, or portions of this source code,
//    must retain the above copyright notice, this list of conditions
//    and the following disclaimer.
// 2. Redistributions in binary form must reproduce the above copyright
//    notice, this list of conditions and the following disclaimer in
//    the documentation and/or other materials provided with the
//    distribution.
// 3. All advertising materials mentioning features or use of this
//    software must display the following acknowledgment:
//    "This product includes software developed by Instrumental
//    Software Technologies, Inc. (http://www.isti.com)"
// 4. If the software is provided with, or as part of a commercial
//    product, or is used in other commercial software products the
//    customer must be informed that "This product includes software
//    developed by Instrumental Software Technologies, Inc.
//    (http://www.isti.com)"
// 5. The names "Instrumental Software Technologies, Inc." and "ISTI"
//    must not be used to endorse or promote products derived from
//    this software without prior written permission. For written
//    permission, please contact "info@isti.com".
// 6. Products derived from this software may not be called "ISTI"
//    nor may "ISTI" appear in their names without prior written
//    permission of Instrumental Software Technologies, Inc.
// 7. Redistributions of any form whatsoever must retain the following
//    acknowledgment:
//    "This product includes software developed by Instrumental
//    Software Technologies, Inc. (http://www.isti.com/)."
// THIS SOFTWARE IS PROVIDED BY INSTRUMENTAL SOFTWARE
// TECHNOLOGIES, INC. "AS IS" AND ANY EXPRESSED OR IMPLIED
// WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES
// OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
// DISCLAIMED.  IN NO EVENT SHALL INSTRUMENTAL SOFTWARE TECHNOLOGIES,
// INC. OR ITS CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
// INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
// (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
// SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
// HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
// STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
// ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED
// OF THE POSSIBILITY OF SUCH DAMAGE.
//=====================================================================
//  A current version of the software can be found at
//                http://www.isti.com
//  Bug reports and comments should be directed to
//  Instrumental Software Technologies, Inc. at info@isti.com
//=====================================================================
//

package com.isti.util;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintStream;
import java.io.PrintWriter;
import java.text.DateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.Vector;

import com.isti.util.logging.IstiConsoleLogger;
import com.isti.util.logging.IstiLoggerOutputStream;

/**
 * Class LogFile provides log file output services.  The 'initLogObj()'
 * and 'getLogObj()' functions support the use of a single "global" log
 * file object.  Other "local" log file objects may also be created and
 * used.  Each log message is received with one of the the following
 * message levels:
 *   ERROR   -  An error which causes the program to abort.
 *   WARNING -  An unexpected condition, but the program continues.
 *   INFO    -  An informational message.
 *   DEBUG   -  A debug message, for developers only.
 *   DEBUG2-5 - Debug messages of increasing detail, for developers only.
 * The console and log file outputs are configurable such
 * that only messages at or beyond a set message level are retained.  The
 * message level is shown on each output message, and the date & time are
 * shown on each message sent to the log file.  Options are available for
 * adding a date code to the filename and creating new log files as the
 * date code changes, as well as automatic deletion of old log files.
 */
public class LogFile extends IstiConsoleLogger
{
  /**
   * A message level value used to specify that no log-level indicator
   * is to be output with the message (998).  May be used with the
   * 'println(level,msg)' method.
   */
  public static final int NO_LEVEL = 998;

         //min/max level values:
  private static final int MIN_LEVEL_VAL = ALL_MSGS;
  private static final int MAX_LEVEL_VAL = NO_MSGS;

  /**
   * Default for 'gmtFlag'.
   */
  public static final boolean DEFAULT_GMT_FLAG = false;
  /**
   * Default for 'useDateInFnameFlag'.
   */
  public static final boolean DEFAULT_USE_DATE_IN_FNAME_FLAG = false;
  /**
   * Separator used between file name and date code ("_").
   */
  public static final String FNAME_DATE_SEPSTR = "_";
  /**
   * Pattern string for "file" date formatter ("yyyyMMdd").
   */
  public static final String FILE_DATE_PATTERNSTR = "yyyyMMdd";
  /**
   * Pattern string for "short" date formatter ("MMM dd yyyy HH:mm:ss").
   */
  public static final String SHORT_DATE_PATTERNSTR = "MMM dd yyyy HH:mm:ss";
  /**
   * Pattern string for "long" date formatter ("MMMM dd, yyyy HH:mm:ss z").
   */
  public static final String LONG_DATE_PATTERNSTR =
                                                 "MMMM dd, yyyy HH:mm:ss z";

  private String logFileName = null;      //log file name in use
  private PrintWriter logWriter = null;   //log file stream handle
  private final String baseFileName;      //file name given to constructor
  private int logFileLevel = NO_MSGS;     //min level for log file output
              //"file" date formatter:
  private final DateFormat fileDateFormatter =
                          UtilFns.createDateFormatObj(FILE_DATE_PATTERNSTR);
              //"short"-style date formatter:
  private final DateFormat shortDateFormatter =
                         UtilFns.createDateFormatObj(SHORT_DATE_PATTERNSTR);
              //"long"-style date formatter:
  private final DateFormat longDateFormatter =
                          UtilFns.createDateFormatObj(LONG_DATE_PATTERNSTR);
  private static final Object staticLogsSyncObj = new Object();
  private static LogFile globalLogObj = null;    //global log file object
  private static LogFile consoleLogObj = null;   //console log file object
  private static LogFile nullLogObj = null;      //"null" log file object
  private final boolean useDateInFnameFlag;  //true to use date in file name
  private final boolean writeHdrFdrFlag;     //true for headers/footers
  private final File directoryObj;           //directory object
  private String logFileFilterStr = "*.log"; //log file filter (default)
  private int maxLogFileAge = 0;  //maximum log file age or 0 to disable
  private long logFileStartTime = 0;         //start time for log file
              //minimum interval between new log files; default = 1 day
  private long logFileSwitchIntervalMS = UtilFns.MS_PER_DAY;
  private boolean writeDatePrefixFlag = true;    //true for date prefix
              //Vector of log-level name strings:
  private static Vector levelNamesVector = null;
              //array of log-level name strings:
  private static String [] levelNamesArray = null;
              //circular buffer for message tracking:
  private CircularBuffer msgTrackingBufferObj = null;
              //level value for message tracking:
  private int msgTrackingBuffLevel = NO_MSGS;
              //list of tracked-message listeners:
  private ArrayList trackedMsgListenersList = null;

  /**
   * Creates a new LogFile object.
   * @param fName log file name to use (if a path name then any needed
   * parent directories will be created), or null for none.
   * @param logFileLevel minimum level of messages to send to the
   * log file, such as NO_MSGS, INFO, etc.
   * @param consoleLevel minimum level of messages to send to the
   * console, such as NO_MSGS, INFO, etc.
   * @param gmtFlag true to use GMT for timestamps, false to use the
   * local timezone.
   * @param useDateInFnameFlag true to use the date in the file name (and
   * create a new log file everytime the date changes).
   * @param writeHdrFdrFlag true to write header and footer messages
   * to log file; false for no header and footer messages.
   */
  public LogFile(String fName,int logFileLevel, int consoleLevel,
                                boolean gmtFlag, boolean useDateInFnameFlag,
                                                    boolean writeHdrFdrFlag)
  {
    super(consoleLevel);
    this.baseFileName = fName;         //save given file name
    this.useDateInFnameFlag = useDateInFnameFlag;  //save use date flag
    this.writeHdrFdrFlag = writeHdrFdrFlag;

    if(logFileLevel >= NO_MSGS || baseFileName == null ||
                                                  baseFileName.length() < 1)
    {    //no log file output or file name not given
      directoryObj = null;        //indicate no directory
      return;
    }
         //get directory for file name:
    directoryObj = new File(baseFileName).getParentFile();

    if(gmtFlag)  //GMT flag set
    {
      fileDateFormatter.setTimeZone(UtilFns.GMT_TIME_ZONE_OBJ);
      shortDateFormatter.setTimeZone(UtilFns.GMT_TIME_ZONE_OBJ);
      longDateFormatter.setTimeZone(UtilFns.GMT_TIME_ZONE_OBJ);
    }

    //setup filter string for deleting old log files such that it will
    // match the given filename with any date string inserted:
    logFileFilterStr = FileUtils.addStrToFileName(baseFileName,
                                                   (FNAME_DATE_SEPSTR+"*"));

    //determine the file name and create it
    determineLogFileName(new Date());

    this.logFileLevel = logFileLevel; //save log file output level
  }

  /**
   * Creates a new LogFile object.
   * @param fName log file name to use (if a path name then any needed
   * parent directories will be created), or null for none.
   * @param logFileLevel minimum level of messages to send to the
   * log file, such as NO_MSGS, INFO, etc.
   * @param consoleLevel minimum level of messages to send to the
   * console, such as NO_MSGS, INFO, etc.
   * @param gmtFlag true to use GMT for timestamps, false to use the
   * local timezone.
   * @param useDateInFnameFlag true to use the date in the file name (and
   * create a new log file everytime the date changes).
   */
  public LogFile(String fName,int logFileLevel, int consoleLevel,
                                boolean gmtFlag, boolean useDateInFnameFlag)
  {
    this(fName,logFileLevel,consoleLevel,gmtFlag,useDateInFnameFlag,true);
  }

  /**
   * Creates a new LogFile object.
   * @param fName log file name to use (if a path name then any needed
   * parent directories will be created).
   * @param logFileLevel minimum level of messages to send to the
   * log file, such as NO_MSGS, INFO, etc.
   * @param consoleLevel minimum level of messages to send to the
   * console, such as NO_MSGS, INFO, etc.
   * @param gmtFlag true to use GMT for timestamps, false to use the
   * local timezone.
   */
  public LogFile(String fName, int logFileLevel, int consoleLevel,
                                                            boolean gmtFlag)
  {
    this(fName,logFileLevel,consoleLevel,gmtFlag,
                                       DEFAULT_USE_DATE_IN_FNAME_FLAG,true);
  }

  /**
   * Creates a new LogFile object, using the local timezone for
   * timestamps.
   * @param fName log file name to use (if a path name then any needed
   * parent directories will be created).
   * @param logFileLevel minimum level of messages to send to the
   * log file, such as NO_MSGS, INFO, etc.
   * @param consoleLevel minimum level of messages to send to the
   * console, such as NO_MSGS, INFO, etc.
   */
  public LogFile(String fName, int logFileLevel, int consoleLevel)
  {
    this(fName,logFileLevel,consoleLevel,DEFAULT_GMT_FLAG,
                                       DEFAULT_USE_DATE_IN_FNAME_FLAG,true);
  }

  /**
   * Gets the maximum log file age.
   * @return the maximum age (in days) or 0 if disabled.
   */
  public int getMaxLogFileAge()
  {
    return maxLogFileAge;
  }

  /**
   * Sets the maximum log file age, in days.  If 'useDateInFnameFlag' is
   * enabled then anytime a new log file name is put into use, any
   * matching log files older than the given number of days are deleted.
   * If 'useDateInFnameFlag' is not enabled then old log files will not
   * be deleted.  Note also that when this method is called, any matching
   * log files older than the given age are deleted immediatedly (if
   * 'useDateInFnameFlag' is enabled.)
   * @param maxAge maximum age (in days) or 0 to disable.
   */
  public synchronized void setMaxLogFileAge(int maxAge)
  {
    maxLogFileAge = maxAge;       //set max-age-in-days value
    if(useDateInFnameFlag)        //if use-date enabled then
      deleteOldLogFiles();        //delete old log files now
  }

  /**
   * Sets the minimum number of days between log file switches via
   * date changes.  This setting only has an effect if the log file
   * was constructed with the 'useDateInFnameFlag' parameter set to
   * 'true'.  The default value is 1 day.
   * @param numDays if > 0 then the minimum number of days between
   * log file switches; otherwise the log file switching feature is
   * disabled.
   */
  public synchronized void setLogFileSwitchIntervalDays(int numDays)
  {
    logFileSwitchIntervalMS = numDays * UtilFns.MS_PER_DAY;
  }

  /**
   * Sets whether or not a date-code prefix is inserted before each
   * entry in the log file.
   * @param flgVal true to have a date-code prefix inserted before
   * each entry in the log file; false for not.
   */
  public synchronized void setWriteDatePrefixFlag(boolean flgVal)
  {
    writeDatePrefixFlag = flgVal;
  }

  /**
   * Sets up message tracking.  The 'getTrackedMessages()' method will
   * return the last 'msgCount' number of messages with a message level
   * of 'msgLevel' or greater.
   * @param msgLevel minimum message level to be tracked, or NO_MSGS
   * for none.
   * @param msgCount number of messages to be tracked.
   */
  public synchronized void setupMessageTracking(int msgLevel, int msgCount)
  {
    if(msgLevel <= ERROR && msgCount > 0)
    {    //message level in range and positive # of msgs to be tracked
      msgTrackingBuffLevel = msgLevel;      //set message level
                        //create circular buffer for storing messages:
      msgTrackingBufferObj = new CircularBuffer(msgCount);
    }
    else
    {    //message level not in range or not positive # of msgs
      msgTrackingBufferObj = null;          //clear handle to buffer
      msgTrackingBuffLevel = NO_MSGS;       //indicate not tracking
    }
  }

  /**
   * Returns an array of strings containing the tracked messages.  See
   * the 'setupMessageTracking()' method for details.
   * @param maxAgeMs maximum age (in milliseconds) of messages returned,
   * or 0 for all messages.
   * @return An array of strings containing the tracked messages, or null
   * if message tracking is not set up.
   */
  public synchronized String [] getTrackedMessages(int maxAgeMs)
  {
    if(msgTrackingBufferObj != null)
    {    //message tracking is setup
      try
      {
        if(maxAgeMs <= 0)         //if no max age then return all messages
          return msgTrackingBufferObj.getStrBufferItems();
        final Date minDateObj =   //convert mag age to minimum date
                              new Date(System.currentTimeMillis()-maxAgeMs);
                                  //get items from message-tracking buffer:
        final Object [] objsArr = msgTrackingBufferObj.getBufferItems();
        final int numItems;
        if((numItems=objsArr.length) <= 0)  //if no messages then
          return new String[0];             //return empty array
        List listObj = Arrays.asList(objsArr);   //convert array to list
              //scan backwards through list converting dated-message
              // objects to string objects; if a message is found to
              // be older than the maximum age then it and all previous
              // messages are discarded:
        Object obj;
        TrackedDatedMessage datedMsgObj;
        int i = numItems - 1;
        do
        {     //for each dated-message object in list
          if((obj=listObj.get(i)) instanceof TrackedDatedMessage)
          {   //fetched item is dated-message object
            datedMsgObj = (TrackedDatedMessage)obj;
            if(datedMsgObj.dateObj != null &&
                              minDateObj.compareTo(datedMsgObj.dateObj) > 0)
            { //message is older than maximum age
                   //remove message and all previous messages from list:
              listObj = listObj.subList(i+1,numItems);
              break;              //exit list
            }
                   //change dated-message object to message string:
            listObj.set(i,datedMsgObj.messageStr);
          }
          else if(!(obj instanceof String))      //if fetched item not a
            listObj.set(i,obj.toString());       // string then convert
        }
        while(--i >= 0);     //loop until after first item processed
                   //convert list to array of strings:
        return (String [])(listObj.toArray(new String[listObj.size()]));
      }
      catch(Exception ex)
      {       //some kind of exception error; just return null
      }
    }
    return null;
  }

  /**
   * Returns an array of strings containing the tracked messages.  See
   * the 'setupMessageTracking()' method for details.
   * @return An array of strings containing the tracked messages, or null
   * if message tracking is not set up.
   */
  public synchronized String [] getTrackedMessages()
  {
    return getTrackedMessages(0);
  }

  /**
   * Returns a string containing the tracked messages.  See
   * the 'setupMessageTracking()' method for details.
   * @param sepStr separator to be placed between each item.
   * @param maxAgeMs maximum age (in milliseconds) of messages returned,
   * or 0 for all messages.
   * @return A string containing the tracked messages, or null if message
   * tracking is not set up.
   */
  public synchronized String getTrackedMsgsAsString(String sepStr,
                                                               int maxAgeMs)
  {
    final String [] strArr;            //get array of items:
    if((strArr=getTrackedMessages(maxAgeMs)) == null)
      return null;
    if(sepStr == null)       //if null parameter then
      sepStr = "";           //use empty string
    final StringBuffer buff = new StringBuffer();     //create buffer
    if(strArr.length > 0)
    {    //items array not empty
      int idx = 0;
      while(true)
      {  //for each item in array
        buff.append(strArr[idx]);           //add to buffer
        if(++idx >= strArr.length)     //if all items processed then
          break;                       //exit loop
        buff.append(sepStr);                //add separator
      }
    }
    return buff.toString();       //return string version of buffer
  }

  /**
   * Returns a string containing the tracked messages.  See
   * the 'setupMessageTracking()' method for details.
   * @param sepStr separator to be placed between each item.
   * @return A string containing the tracked messages, or null if message
   * tracking is not set up.
   */
  public synchronized String getTrackedMsgsAsString(String sepStr)
  {
    return getTrackedMsgsAsString(sepStr,0);
  }

  /**
   * Adds the given tracked-message listener.  The listener's
   * 'dataChanged()' method is called whenever a new log message
   * is tracked.
   * @param listenerObj the 'DataChangedListener' object to be added.
   */
  public synchronized void addTrackedMsgListener(
                                            DataChangedListener listenerObj)
  {
    if(trackedMsgListenersList == null)               //if list not created
      trackedMsgListenersList = new ArrayList();      // then create it now
    trackedMsgListenersList.add(listenerObj);         //add to list
  }

  /**
   * Removes the given tracked-message listener.
   * @param listenerObj the 'DataChangedListener' object to be removed.
   */
  public synchronized void removeTrackedMsgListener(
                                            DataChangedListener listenerObj)
  {
    if(trackedMsgListenersList != null)
    {    //list exists
      trackedMsgListenersList.remove(listenerObj);    //remove from list
      if(trackedMsgListenersList.size() <= 0)    //if list now empty then
        trackedMsgListenersList = null;          //clear handle
    }
  }

  /**
   * Calls the 'dataChanged()' method on all tracked-message listeners.
   */
  private void fireTrackedMsgListeners()
  {
    if(trackedMsgListenersList != null)
    {    //tracked-message-listeners list exists; call method on each one
      final Iterator iterObj = trackedMsgListenersList.iterator();
      while(iterObj.hasNext())              //(use this object as 'source')
        ((DataChangedListener)(iterObj.next())).dataChanged(this);
    }
  }

  /**
   * Determines the log file name and creates the log file if changed.
   * @param dateObj date object.
   */
  private void determineLogFileName(Date dateObj)
  {
    final long currentTimeVal = System.currentTimeMillis();     //get time
    if(logFileStartTime <= 0 || (useDateInFnameFlag &&
                                              logFileSwitchIntervalMS > 0 &&
              currentTimeVal - logFileStartTime >= logFileSwitchIntervalMS))
    {    //first time through or logfile-switching enabled
         // and enough time elapsed to allow switch
      if(useDateInFnameFlag)
      {  //logfile-switching enabled; get calendar object with current time
        final Calendar calObj =        //use same timezone as timestamps
                        Calendar.getInstance(getDateFormat().getTimeZone());
        calObj.setTime(new Date(currentTimeVal));
              //set time to start of current day so next log file
              // will start at 00:00 time:
        calObj.set(Calendar.HOUR_OF_DAY,0);
        calObj.set(Calendar.MINUTE,0);
        calObj.set(Calendar.SECOND,0);
        calObj.set(Calendar.MILLISECOND,0);
        logFileStartTime = calObj.getTime().getTime();
      }
      else    //logfile-switching not enabled
        logFileStartTime = currentTimeVal;  //set time to indicate started
      final String newLogFileName;     //new log file name
      final String oldLogFileName;     //old log file name
      if (useDateInFnameFlag)  //if using the date
      {
        //get the date text, prepend separator
        final String dateText = FNAME_DATE_SEPSTR +
                                          fileDateFormatter.format(dateObj);
        //add date text
        newLogFileName = FileUtils.addStrToFileName(baseFileName, dateText);
      }
      else
      {
        //use the file name
        newLogFileName = baseFileName;
      }

      if (!newLogFileName.equals(logFileName))  //if the log file name has changed
      {
        boolean writeContinuedFrom = false; //don't do the "Continued From"
                                            // thing unless we should
        if(writeHdrFdrFlag && logWriter != null)
        {   //writing of header and footer messages enabled and
            // this is not the first logfile, we're continuing
          writeContinuedFrom = true;
          logWriter.println("Log file continues in \"" + newLogFileName +
                                                                        "\"");
        }
        closeLogFile();  //close the old log file
        oldLogFileName = logFileName;         //save the old log file name
        logFileName = newLogFileName;         //save the new log file name
        createLogFile(dateObj);               //create the new log file
        if(writeContinuedFrom && logWriter != null)
        {  //not the first file; show where we came from
          logWriter.println("  continued from \"" + oldLogFileName + "\"");
        }
        deleteOldLogFiles();  //delete old log files
      }
    }
  }

  /**
   * Delete old log files.
   */
  private void deleteOldLogFiles()
  {
    //if directory was found and maximum age was specified
    if (directoryObj != null && maxLogFileAge > 0)
    {
      //delete old log files
      FileUtils.deleteOldFiles(directoryObj, maxLogFileAge, logFileFilterStr);
    }
  }

  /**
   * Close the log file.
   */
  private void closeLogFile()
  {
    if (logWriter != null)    //if log writer exists
    {
      logWriter.close();      //close file
      logWriter = null;       //set stream pointer to null
    }
  }

  /**
   * Creates the log file.
   * @param dateObj date object
   */
  private void createLogFile(Date dateObj)
  {
    try       //open to append to output file, with autoflush
    {              //create any parent directories needed for output file:
      FileUtils.createParentDirs(logFileName);
      logWriter = new PrintWriter(new BufferedWriter(
                                    new FileWriter(logFileName,true)),true);
      try     //write log file "header"
      {
        if(writeHdrFdrFlag)
        {  //writing of header and footer messages enabled
          logWriter.println();         //put in separator
          logWriter.println(           //put in date/time
                    "Log file opened " + longDateFormatter.format(dateObj));
        }
        if(!logWriter.checkError())  //no stream errors detected
        {
//          if(consoleLevel <= INFO)     //if info level then show message
//            System.out.println("Log file \"" + logFileName + "\" opened");
        }
        else  //stream error detected
        {
          System.err.println("Error writing to log file \"" +
                                                        logFileName + "\"");
          logFileName = null;       //clear file name
        }
      }
      catch(Exception ex)         //error writing to file
      {
        System.err.println("Exception writing to log file \"" +
                                                logFileName + "\":  " + ex);
        logFileName = null;       //clear file name
      }
    }
    catch(IOException ex)
    {
      System.err.println("Exception opening log file \"" + ex.getMessage() +
                         "\":  " + ex);
      logFileName = null;       //clear file name
    }

    if (logFileName == null)  //if log file name is empty
    {
      closeLogFile();  //close the log file
    }
  }

  /**
   * Sends the given string to the log file.  (And possibly the
   * console--see the 'consoleLevel' parameter on the constructor.)
   * @param level the message level to be used, or NO_LEVEL for none.
   * @param str the string to be outputted.
   * @param t Throwable associated with a log message or null if none.
   * @return true if successful, false if an I/O error was detected.
   */
  protected synchronized boolean logMessage(int level, String str, Throwable t)
  {
    //send to console
    super.logMessage(level, str, t);
    str = getMessage(str, t);
    boolean logOutputFlag, retFlag, msgTrkFlag = false;
    if(level < logFileLevel)
    {    //given level lower than set level
      if(level < msgTrackingBuffLevel)
        return true;    //if not tracking msg then just return OK flag
      logOutputFlag = false;      //don't send output to log file
      retFlag = true;             //setup to return OK flag
    }
    else if(logWriter == null)
    {    //log file not open
      if(level < msgTrackingBuffLevel)
        return false;   //if not tracking msg then just return error flag
      logOutputFlag = false;      //don't send output to log file
      retFlag = false;            //setup to return error flag
    }
    else
    {    //message should be written to log file
      logOutputFlag = true;       //send output to log file
      retFlag = true;             //setup to return OK flag
    }
    try       //write to log file
    {              //create output string preceded by date/time and level:
      final Date dateObj = new Date();
                   //if flag then insert date prefix:
      final String outStr = writeDatePrefixFlag ?
                      (getDateFormat().format(dateObj) + ":  " + str) : str;
         //if tracking messages and given level >= tracking level then
         // add message to tracking buffer:
      if(level >= msgTrackingBuffLevel && msgTrackingBufferObj != null)
      {
        msgTrackingBufferObj.add(new TrackedDatedMessage(dateObj,outStr));
        msgTrkFlag = true;        //indicate message tracked
      }
      if(logOutputFlag)
      {  //message should be sent to log file
        println(dateObj,outStr);       //send out message string
        if(logWriter.checkError())
        {     //stream error detected
          logWriter.close();           //close file
          logWriter = null;            //set stream pointer to null
          retFlag = false;             //setup to return error flag
          System.err.println("Error writing to log file \"" + logFileName +
                                                                      "\"");
        }
      }
    }
    catch(Exception ex)           //error writing to file
    {
      if(logWriter != null)
      {  //log file stream handle is open
        logWriter.close();        //close file
        logWriter = null;         //set stream pointer to null
      }
      retFlag = false;            //setup to return error flag
    }
    if(msgTrkFlag)                //if message was tracked then
      fireTrackedMsgListeners();  //fire tracked-message listeners
    return retFlag;               //return status flag
  }

  /**
   * Gets the message for the level.
   * @param level the message level.
   * @return the message.
   */
  protected String getMessage(int level)
  {
    if (level < NO_LEVEL)
    {
      return super.getMessage(level);
    }
    return UtilFns.EMPTY_STRING;
  }

  /**
   * Sends the given string to the log file.  (And possibly the
   * console--see the 'consoleLevel' parameter on the constructor.)
   * A log-level indicator is not included in the output.
   * @param str the string to be outputted.
   * @return true if successful, false if an I/O error was detected.
   */
  public boolean println(String str)
  {
    return println(NO_LEVEL,str);
  }

  /**
   * Sends the given string to the log file.
   * @param dateObj date object.
   * @param str the string to be outputted.
   */
  private void println(Date dateObj, String str)
  {
    //determine the file name and create if changed
    determineLogFileName(dateObj);

    logWriter.println(str);
  }

  /**
   * Closes the log file.
   */
  public synchronized void close()
  {
    if(logWriter != null)
    {    //log file is open; put in message & date/time
      if(writeHdrFdrFlag)
      {  //writing of header and footer messages enabled
        final Date dateObj = new Date();
        println(dateObj, "Log file closed " +
                                         longDateFormatter.format(dateObj));
      }
      logWriter.close();          //close file
      logWriter = null;           //indicate file not open
    }
  }

  /**
   * Returns an 'OutputStream' object whose output is directed to
   * this log file.
   * @return A new 'LogOutputStream' object associated with this log file.
   */
  public LogOutputStream getLogOutputStream()
  {
    return new LogOutputStream(this);
  }

  /**
   * Returns the current log file name in use.
   * @return The log file name, or null if an error occurred.
   */
  public String getFileName()
  {
    return logFileName;
  }

  /**
   * Determines if the log file is open.
   * @return true if the log file is open.
   */
  public boolean isOpen()
  {
    return logWriter != null;
  }

  /**
   * Gets the current level for log file output messages.  (See the
   * 'logFileLevel' parameter on the constructor.)
   * @return the current level for log file output messages
   */
  public int getLogFileLevel()
  {
    return logFileLevel;
  }

  /**
   * Get the minimum level for all output messages.
   * @return the minimum level for all output messages.
   * @see #isLoggable(int)
   */
  public int getMinLevel()
  {
    int level = super.getMinLevel();
    if (logFileLevel < level)
      level = logFileLevel;
    return level;
  }
  
  /**
   * Set the logger for standard error.
   * 
   * @return standard error.
   */
  public PrintStream logStandardErr()
  {
	return IstiLoggerOutputStream.logStandardErr(this, WARNING);
  }

  /**
   * Set the logger for standard error.
   * @param level the level.
   * @return standard error.
   */
  public PrintStream logStandardErr(int level)
  {
	return IstiLoggerOutputStream.logStandardErr(this, level);
  }

  /**
   * Set the logger for standard output.
   * 
   * @return standard output.
   */
  public PrintStream logStandardOut()
  {
	return IstiLoggerOutputStream.logStandardOut(this, INFO);
  }

  /**
   * Set the logger for standard output.
   * @param level the level.
   * @return standard output.
   */
  public PrintStream logStandardOut(int level)
  {
	return IstiLoggerOutputStream.logStandardOut(this, level);
  }

  /**
   * Sets the current level for console output messages.  (See the
   * 'logFileLevel' parameter on the constructor.)
   * @param level the log file level to be used.
   */
  public synchronized void setLogFileLevel(int level)
  {
    logFileLevel = level;
  }

  /**
   * Returns a Vector containing all the valid log-level name strings.
   * @return A Vector of String objects.
   */
  public static Vector getLevelNamesVector()
  {
    if(levelNamesVector == null)
    {    //level-names vector not yet created
      levelNamesVector = new Vector();
      String str;
      for(int l=MAX_LEVEL_VAL; l>=MIN_LEVEL_VAL; --l)
      {  //for each possible log-level value (don't show "NO_LEVEL")
        if(l != NO_LEVEL && (str=getLevelStringNull(l)) != null)
          levelNamesVector.add(str);   //if matches a name then add to vec
      }
    }
    return levelNamesVector;
  }

  /**
   * Returns an array containing all the valid log-level name strings.
   * @return An array of String objects.
   */
  public static String [] getLevelNamesArray()
  {
    if(levelNamesArray == null)
    {    //level-names array not yet created
              //get level-names vector on convert to a string array:
      final Vector vec = getLevelNamesVector();
      levelNamesArray = (String [])(vec.toArray(new String[vec.size()]));
    }
    return levelNamesArray;
  }

  /**
   * Returns an array containing all the valid log-level name strings.
   * @return An array of String objects.
   */
  public static String getLevelNamesDisplayStr()
  {                          //convert to display-list of strings:
    return UtilFns.enumToQuotedStrings(getLevelNamesVector().elements());
  }

  /**
   * Initializes the "global" log file object.  This function would
   * be used by a program's "main" module to establish a "global"
   * log file.  Other modules can then get a handle to the "global"
   * log file via the 'getGlobalLogObj()' function.
   * @param logObj the log file object.
   * @return A handle to the "global" log file object.
   */
  public static LogFile initGlobalLogObj(LogFile logObj)
  {
    synchronized(staticLogsSyncObj)
    {                        //save handle
      return globalLogObj = logObj;
    }
  }

  /**
   * Initializes the "global" log file object.  This function would
   * be used by a program's "main" module to establish a "global"
   * log file.  Other modules can then get a handle to the "global"
   * log file via the 'getGlobalLogObj()' function.
   * @param fName log file name to use (if a path name then any needed
   * parent directories will be created).
   * @param consoleLevel minimum level of messages to send to the
   * console, such as NO_MSGS, INFO, etc.
   * @param logFileLevel minimum level of messages to send to the
   * log file, such as NO_MSGS, INFO, etc.
   * @param gmtFlag true to use GMT for timestamps, false to use the
   * local timezone.
   * @param useDateInFnameFlag true to use the date in the file name (and
   * create a new log file everytime the date changes).
   * @return A handle to the "global" log file object.
   */
  public static LogFile initGlobalLogObj(String fName,int logFileLevel,
                int consoleLevel,boolean gmtFlag,boolean useDateInFnameFlag)
  {
    return initGlobalLogObj(new LogFile(fName,logFileLevel,consoleLevel,
                                        gmtFlag,useDateInFnameFlag));
  }

  /**
   * Initializes the "global" log file object.  This function would
   * be used by a program's "main" module to establish a "global"
   * log file.  Other modules can then get a handle to the "global"
   * log file via the 'getGlobalLogObj()' function.
   * @param fName log file name to use (if a path name then any needed
   * parent directories will be created).
   * @param consoleLevel minimum level of messages to send to the
   * console, such as NO_MSGS, INFO, etc.
   * @param logFileLevel minimum level of messages to send to the
   * log file, such as NO_MSGS, INFO, etc.
   * @param gmtFlag true to use GMT for timestamps, false to use the
   * local timezone.
   * @return A handle to the "global" log file object.
   */
  public static LogFile initGlobalLogObj(String fName,int logFileLevel,
                                           int consoleLevel,boolean gmtFlag)
  {
    return initGlobalLogObj(fName,logFileLevel,consoleLevel,gmtFlag,false);
  }

  /**
   * Initializes the "global" log file object, using the local timezone
   * for timestamps.  This function would be used by a program's "main"
   * module to establish a "global" log file.  Other modules can then
   * get a handle to the "global" log file via the 'getGlobalLogObj()'
   * function.
   * @param fName log file name to use (if a path name then any needed
   * parent directories will be created).
   * @param consoleLevel minimum level of messages to send to the
   * console, such as NO_MSGS, INFO, etc.
   * @param logFileLevel minimum level of messages to send to the
   * log file, such as NO_MSGS, INFO, etc.
   * @return A handle to the "global" log file object.
   */
  public static LogFile initGlobalLogObj(String fName,int logFileLevel,
                                                           int consoleLevel)
  {
    return initGlobalLogObj(fName,logFileLevel,consoleLevel,false);
  }

  /**
   * Gets a handle to the "global" log file object.  A program's
   * "main" module would establish a "global" log file via the
   * 'initGlobalLogObj()' function.  Other modules can then get
   * a handle to the "global" log file via this function.  If
   * 'initGlobalLogObj()' was not called previously then a "global"
   * log file named "default.log" will be created and used.
   * @return A handle to the "global" log file object.
   */
  public static LogFile getGlobalLogObj()
  {
    synchronized(staticLogsSyncObj)
    {
      if(globalLogObj != null)         //if initialzed then
        return globalLogObj;           //return log file object
                                       //if not then create and return one:
      return initGlobalLogObj("default.log",INFO,INFO);
    }
  }

  /**
   * Gets a handle to the "global" log file object.  A program's
   * "main" module would establish a "global" log file via the
   * 'initGlobalLogObj()' function.  Other modules can then get
   * a handle to the "global" log file via this function.
   * If 'initGlobalLogObj()' was not called previously then either a null log
   * file that does nothing or a console log file is returned.
   * @param consoleFlag true to return a console log file if
   * 'initGlobalLogObj()' was not called previously, otherwise a null log file
   * is returned.
   * NOTE: Console output should be limited to messages that will be displayed
   * infrequently.
   * @return A handle to the "global" log file object.
   */
  public static LogFile getGlobalLogObj(boolean consoleFlag)
  {
    synchronized(staticLogsSyncObj)
    {
      if(globalLogObj != null)         //if initialzed then
        return globalLogObj;           //return log file object
      if(consoleFlag)                  //if console then
      {  //flag set to use "console" log file object
        return getConsoleLogObj();     //return "console" log file object
      }
      return getNullLogObj();          //return "null" log file object
    }
  }

  /**
   * Gets a handle to the console log file object.
   * @return A handle to the console log file object.
   */
  public static LogFile getConsoleLogObj()
  {
    synchronized(staticLogsSyncObj)
    {
      if(consoleLogObj == null)      //if not created then create one now
        consoleLogObj = new LogFile(null,NO_MSGS,ALL_MSGS);
      return consoleLogObj;
    }
  }

  /**
   * Gets a handle to the null log file object.
   * @return A handle to the null log file object.
   */
  public static LogFile getNullLogObj()
  {
    synchronized(staticLogsSyncObj)
    {
      if(nullLogObj == null)      //if not created then create one now
        nullLogObj = new LogFile(null,NO_MSGS,NO_MSGS);
      return nullLogObj;
    }
  }

  /**
   * Gets the date formatter for log file entries.
   * @return the date formatter for log file entries.
   */
  public DateFormat getDateFormat()
  {
    return shortDateFormatter;
  }


  /**
   * Class TrackedDatedMessage holds a message string and a Date object.
   */
  private static class TrackedDatedMessage
  {
    public final Date dateObj;
    public final String messageStr;

    /**
     * Creates a tracked-dated-message object.
     * @param dateObj Date object to use.
     * @param messageStr message string to use.
     */
    public TrackedDatedMessage(Date dateObj, String messageStr)
    {
      this.dateObj = dateObj;
      this.messageStr = messageStr;
    }

    /**
     * Returns the message string.
     * @return The message string.
     */
    public String toString()
    {
      return messageStr;
    }
  }
}
