package com.isti.util;
import java.io.*;

/**
 * Class LogOutputStream defines an output stream that sends data to a
 * LogFile object.
 *   7/1/2003 -- [HS]  Initial version.
 *   7/9/2003 -- [ET]  Fixed extra-newline added to each line of output;
 *                     added 'closeLogFile()' and 'setCloseLogFileFlag()'
 *                     methods; added overridden 'close()' method.
 */
public class LogOutputStream extends LineProcOutputStream {

  /** The LogFile object to use. */
  protected final LogFile outFile;

  /** The logging level to use. */
  protected int logLevel = LogFile.NO_LEVEL;

  /** Flag set 'true' to close LogFile when this output stream is closed. */
  protected boolean closeLogFileFlag = true;

  /**
   * Creates an output stream that sends data to a LogFile object.
   * @param logFileObj the LogFile object to use.
   */
  public LogOutputStream(LogFile logFileObj) {
    outFile = logFileObj;
  }

  /**
   * Implements the CallbackStringParam interface.
   * @param s string parameter to use.
   */
  public void callBackMethod(String s) {
    outFile.println(logLevel, UtilFns.stripTrailingNewline(s));
  }

  /**
   * Returns a reference to the logfile in use.
   * @return the LogFile instance that this instance logs to.
   */
  public LogFile getLogfile() {
    return outFile;
  }

  /**
   * Set the logging level used for outputs to the log file.
   * @param newLevel the new logging level.
   */
  public void setLoggingLevel(int newLevel) {
    logLevel = newLevel;
  }

  /**
   * Configures whether or not the 'LogFile' object held by this output
   * stream will be closed when the stream is closed.  The default
   * behavior is for the 'LogFile' object to be closed.
   * @param flgVal true to close the 'LogFile' object when this output
   * stream is closed; false to not.
   */
  public void setCloseLogFileFlag(boolean flgVal)
  {
    closeLogFileFlag = flgVal;
  }

  /**
   * Closes the LogFile used by this object.
   */
  public void closeLogFile()
  {
    outFile.close();
  }

  /**
   * Overidden version that also flushes any buffered data and closes the
   * LogFile object (if 'closeLogFileFlag' is 'true').
   */
  public void close() throws IOException
  {
    flush();                      //flush any buffered data
    super.close();
    if(closeLogFileFlag)          //if flag set then
      closeLogFile();             //close LogFile object
  }


//  public static void main(String [] args) {
//    PrintStream oldStdout = System.out;
//    LogFile lf = new LogFile("LogOutStmTest.log",
//                                         LogFile.ALL_MSGS, LogFile.NO_MSGS);
//    LogOutputStream los = new LogOutputStream(lf);
//    System.setOut(new PrintStream(los));
//    System.out.println("This is a test");
//    System.out.println("so is this");
//    System.out.print("This is a multi ");
//    System.out.print("function call ");
//    System.out.println("test");
//    System.out.println("Test Completed");
//    System.setOut(oldStdout);
//    los.closeLogFile();
//  }
}
