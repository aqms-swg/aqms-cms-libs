//MeasurementUnits.java:  Contains various methods and
//                constants for measurement units.
//
//   1/6/2006 -- [KF]  Initial version.
//

package com.isti.util;

import java.util.Locale;

/**
 * Class MeasurementUnits contains various methods and
//                constants for  measurement units.
 */
public class MeasurementUnits implements MeasurementUnitsInformation
{
  //static data

  /**
   * The display name for the default measurement units.
   */
  public static String DEFAULT_DISPLAY_NAME = "Default";
  /**
   * The display name for the metric measurement units.
   */
  public static String METRIC_DISPLAY_NAME = "KM";
  /**
   * The display name for the U.S. measurement units.
   */
  public static String US_DISPLAY_NAME = "Miles";

  /**
   * Display names.
   */
  private final static String[] displayNames = {
      DEFAULT_DISPLAY_NAME,
      METRIC_DISPLAY_NAME,
      US_DISPLAY_NAME
  };

  //private data

  // current measurement units
  private int iMeasurementUnits = DEFAULT_MEASUREMENT_UNITS;

  /**
   * Converts the value to the current units.
   * @param d the value in meters.
   * @param mu the measurement units.
   * @return the converted value.
   */
  public static int getIntegerValue(double d, int mu)
  {
    return (int)(getValue(d, mu) + 0.5);
  }

  /**
   * Gets the measurement units.
   * @return the measurement units.
   */
  public int getMeasurementUnits()
  {
    return getMeasurementUnits(iMeasurementUnits);
  }

  /**
   * Gets the measurement units or the measurement units for the default locale.
   * @param mu the measurement units.
   * @return the measurement units.
   */
  public static int getMeasurementUnits(int mu)
  {
    return getMeasurementUnits(mu, UtilFns.getDefaultLocale());
  }

  /**
   * Gets the measurement units.
   * @param mu the measurement units.
   * @param localeObj the locale object.
   * @return the measurement units.
   */
  public static int getMeasurementUnits(int mu, Locale localeObj)
  {
    if (mu == DEFAULT_MEASUREMENT_UNITS)
    {
      if (Locale.US.equals(localeObj))
        return MILES;
      return KILOM;
    }
    return mu;
  }

  /**
   * Gets the measurement units string for the specified specified units.
   * @param pluralFlag true to use the plural text.
   * @param mu the measurement units.
   * @return the measurement units string.
   */
  public static String getMeasurementUnitsString(boolean pluralFlag, int mu)
  {
    String unitText = "meter";
    String pUnitText = null;

    switch (mu = getMeasurementUnits(mu))
    {
      case KILOM:
        unitText = "km";
        pUnitText = unitText;
        break;

      case MILES:
        unitText = "mile";
        break;

      case FEET:
        unitText = "foot";
        pUnitText = "feet";
        break;
    }

    if (!pluralFlag)  //if not plural
      return unitText;

    if (pUnitText == null)  //if not special plural just add "s"
        pUnitText = unitText+"s";
    return pUnitText;
  }

  /**
   * Gets the measurement units string for the specified specified units.
   * @param d the value in meters.
   * @param mu the measurement units.
   * @return the measurement units string.
   */
  public static String getMeasurementUnitsString(double d, int mu)
  {
    return getMeasurementUnitsString(getIntegerValue(d, mu), mu);
  }

  /**
   * Gets the measurement units string for the specified specified units.
   * @param dVal the value in meters.
   * @param mu the measurement units.
   * @return the measurement units string.
   */
  public static String getMeasurementUnitsString(int dVal, int mu)
  {
    //if value is not 1 then use plural:
    return getMeasurementUnitsString(dVal!=1, mu);
  }

  /**
   * Gets the configuration property validator.
   * @return the 'CfgPropValidator'.
   */
  public static CfgPropValidator getCfgPropValidator()
  {
    return new CfgPropValidator(displayNames);
  }

  /**
   * Converts the value to the current units.
   * @param d the value in meters.
   * @return the converted value.
   */
  public double getValue(double d)
  {
    return getValue(d, iMeasurementUnits);
  }

  /**
   * Converts the value to the specified units.
   * @param d the value in meters.
   * @param mu the measurement units.
   * @return the converted value.
   */
  public static double getValue(double d, int mu)
  {
    // convert distance units
    switch (mu = getMeasurementUnits(mu))
    {
      case KILOM:
        d *= METER_TO_KM;
        break;

      case MILES:
        d *= METER_TO_MILE;
        break;

      case FEET:
        d *= METER_TO_FEET;
        break;
    }
    return d;
  }

  /**
   * Parses the data string for measurement units.
   * @param dataStr the measurement units data string.
   * @return the measurement units.
   */
  public static int parseMeasurementUnits(String dataStr)
  {
    if (METRIC_DISPLAY_NAME.equalsIgnoreCase(dataStr))
      return KILOM;
    if (US_DISPLAY_NAME.equalsIgnoreCase(dataStr))
      return MILES;
    return DEFAULT_MEASUREMENT_UNITS;
  }

  /**
   * Gets the value string in the current units.
   * @param d the value in meters.
   * @return the value string.
   */
  public String getValueString(double d)
  {
    return getValueString(d, iMeasurementUnits);
  }

  /**
   * Gets the value string in the specified units.
   * @param d the value in meters.
   * @param mu the measurement units.
   * @return the value string.
   */
  public static String getValueString(double d, int mu)
  {
    return getValueString(getIntegerValue(d, mu), mu);
  }

  /**
   * Gets the value string in the specified units.
   * @param dVal the value in meters.
   * @param mu the measurement units.
   * @return the value string.
   */
  public static String getValueString(int dVal, int mu)
  {
    return dVal + " " + getMeasurementUnitsString(dVal, mu);
  }

  /**
   * Determines if the current units is metric.
   * @return true if the units is metric, false otherwise.
   */
  public boolean isMetric()
  {
    return isMetric(iMeasurementUnits);
  }

  /**
   * Determines if the specified units is metric.
   * @param mu the measurement units.
   * @return true if the units is metric, false otherwise.
   */
  public static boolean isMetric(int mu)
  {
    switch (mu = getMeasurementUnits(mu))
    {
      case MILES:
      case FEET:
        return false;
    }
    return true;
  }

  /**
   * Sets the measurement units.
   * @param mu the measurement units.
   */
  public void setMeasurementUnits(int mu)
  {
    iMeasurementUnits = mu;
  }

  /**
   * Sets the measurement units.
   * @param dataStr the measurement units data string.
   */
  public void setMeasurementUnits(String dataStr)
  {
    setMeasurementUnits(parseMeasurementUnits(dataStr));
  }
}
