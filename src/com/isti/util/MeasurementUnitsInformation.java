//MeasurementUnitsInformation.java: Defines constants and methods for
//                                 getting/setting the measurement units.
//
//   1/4/2006 -- [KF]
//

package com.isti.util;

/**
 * Interface MeasurementUnitsInformation constants and methods for
 * getting/setting the measurement units.
 */
public interface MeasurementUnitsInformation
{
  // measurement units values

  /** Kilometer distance units. */
  public static final int KILOM = 0;
  /** Meter distance units. */
  public static final int METER = 1;
  /** Miles distance units. */
  public static final int MILES = 2;
  /** Feet distance units. */
  public static final int FEET  = 3;

  /** The default measurement units. */
  public static final int DEFAULT_MEASUREMENT_UNITS = -1;

  // units conversion values

    /** Feet to meters multiplier. */
  public static final double FEET_TO_METER = 0.3048;
    /** Meters to feet multiplier. */
  public static final double METER_TO_FEET = 1.0 / FEET_TO_METER;
    /** Miles to meters multiplier. */
  public static final double MILE_TO_METER = 5280 * FEET_TO_METER;
    /** Meters to miles multiplier. */
  public static final double METER_TO_MILE = 1.0 / MILE_TO_METER;
    /** Miles to kilometers multiplier. */
  public static final double MILE_TO_KM = MILE_TO_METER / 1000.0;
    /** Kilometers to miles multiplier. */
  public static final double KM_TO_MILE = 1000.0 / MILE_TO_METER;
  /** Meters to kilometers multiplier. */
  public static final double METER_TO_KM = 1.0e-3;

  /**
   * Gets the measurement units.
   * @return the measurement units.
   */
  public int getMeasurementUnits();

  /**
   * Sets the measurement units.
   * @param mu the measurement units.
   */
  public void setMeasurementUnits(int mu);
}
