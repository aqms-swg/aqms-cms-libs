//MemoryInfoLogger.java:  Defines a debugging utility that generates
//                        periodic log messages showing information
//                        about memory usage in a program.
//
//  3/21/2003 -- [ET]
//  5/21/2003 -- [ET]  Added 'setDaemon()' to thread constructor.
//  6/10/2003 -- [ET]  Added extra log-file parameters.
// 10/03/2003 -- [KF]  Extend 'IstiThread' instead of 'Thread'.
//

package com.isti.util;

import java.io.PrintStream;

/**
 * Class MemoryInfoLogger defines a debugging utility that generates
 * periodic log messages showing information about memory usage in a
 * program.  The messages are in the form "Memory info:  total=#,
 * free=#, diff=#", with each value in bytes.  A new thread is created
 * to generate the log messages.
 */
public class MemoryInfoLogger
{
  protected final LogFile logObj;
  protected final PrintStream altOutputStream;
  protected int intervalSeconds = 60;
  protected StringBuffer stringBufferObj = new StringBuffer();
  protected LoggerRunningThread loggerRunningThreadObj = null;
  protected int logLevelVal = LogFile.DEBUG;
  protected boolean closeOnTerminateFlag = false;

  /**
   * Creates a memory information logger object and starts a new
   * log-message-generation thread.
   * @param logObj the LogFile object to use for output.
   * @param intervalSeconds the number of seconds between log outputs.
   */
  public MemoryInfoLogger(LogFile logObj,int intervalSeconds)
  {
    if(logObj == null)
      throw new NullPointerException();
    this.logObj = logObj;
    altOutputStream = null;            //don't use alternate output stream
    startLoggerThread(intervalSeconds);
  }

  /**
   * Creates a memory information logger object and starts a new
   * log-message-generation thread.
   * @param logFileName the name of the log file to be created and used
   * for output.
   * @param intervalSeconds the number of seconds between log outputs.
   */
  public MemoryInfoLogger(String logFileName,int intervalSeconds)
  {
    if(logFileName == null)
      throw new NullPointerException();
              //setup to close log file when logger terminates:
    closeOnTerminateFlag = true;
    logObj = new LogFile(logFileName,LogFile.ALL_MSGS,LogFile.NO_MSGS);
    altOutputStream = null;            //don't use alternate output stream
    startLoggerThread(intervalSeconds);
  }

  /**
   * Creates a memory information logger object and starts a new
   * log-message-generation thread.
   * @param logFileName the name of the log file to be created and used
   * for output.
   * @param intervalSeconds the number of seconds between log outputs.
   * @param useDateInFnameFlag true to use the date in the file name (and
   * create a new log file every time the date changes).
   */
  public MemoryInfoLogger(String logFileName,int intervalSeconds,
                                                 boolean useDateInFnameFlag)
  {
    if(logFileName == null)
      throw new NullPointerException();
              //setup to close log file when logger terminates:
    closeOnTerminateFlag = true;
    logObj = new LogFile(logFileName,LogFile.ALL_MSGS,LogFile.NO_MSGS,
                               LogFile.DEFAULT_GMT_FLAG,useDateInFnameFlag);
    altOutputStream = null;            //don't use alternate output stream
    startLoggerThread(intervalSeconds);
  }

  /**
   * Creates a memory information logger object and starts a new
   * log-message-generation thread.  The date is used in the file name
   * and a new log file is created every time the date changes).
   * @param logFileName the name of the log file to be created and used
   * for output.
   * @param intervalSeconds the number of seconds between log outputs.
   * @param maxLogFileAgeDays the maximum log file age, in days.
   */
  public MemoryInfoLogger(String logFileName,int intervalSeconds,
                                                      int maxLogFileAgeDays)
  {
    if(logFileName == null)
      throw new NullPointerException();
              //setup to close log file when logger terminates:
    closeOnTerminateFlag = true;
    logObj = new LogFile(logFileName,LogFile.ALL_MSGS,LogFile.NO_MSGS,
                                             LogFile.DEFAULT_GMT_FLAG,true);
    logObj.setMaxLogFileAge(maxLogFileAgeDays);
    altOutputStream = null;            //don't use alternate output stream
    startLoggerThread(intervalSeconds);
  }

  /**
   * Creates a memory information logger object and starts a new
   * log-message-generation thread.  The date is used in the file name
   * and a new log file is created every time the date changes).
   * @param logFileName the name of the log file to be created and used
   * for output.
   * @param intervalSeconds the number of seconds between log outputs.
   * @param maxLogFileAgeDays the maximum log file age, in days.
   * @param logFileSwitchIntervalDays the minimum number of days between
   * log file switches via date changes.
   */
  public MemoryInfoLogger(String logFileName,int intervalSeconds,
                        int maxLogFileAgeDays,int logFileSwitchIntervalDays)
  {
    if(logFileName == null)
      throw new NullPointerException();
              //setup to close log file when logger terminates:
    closeOnTerminateFlag = true;
    logObj = new LogFile(logFileName,LogFile.ALL_MSGS,LogFile.NO_MSGS,
                                             LogFile.DEFAULT_GMT_FLAG,true);
    logObj.setMaxLogFileAge(maxLogFileAgeDays);
    logObj.setLogFileSwitchIntervalDays(logFileSwitchIntervalDays);
    altOutputStream = null;            //don't use alternate output stream
    startLoggerThread(intervalSeconds);
  }

  /**
   * Creates a memory information logger object and starts a new
   * message-generation thread.
   * @param stmObj the stream object to use for output.
   * @param intervalSeconds the number of seconds between log outputs.
   */
  public MemoryInfoLogger(PrintStream stmObj,int intervalSeconds)
  {
    if(stmObj == null)
      throw new NullPointerException();
    altOutputStream = stmObj;
    logObj = null;                     //don't use log file output
    startLoggerThread(intervalSeconds);
  }

  /**
   * Starts the memory-information-logging thread.
   * @param intervalSeconds the number of seconds between log outputs.
   */
  protected synchronized void startLoggerThread(int intervalSeconds)
  {
                             //don't allow interval to be less than 1:
    this.intervalSeconds = (intervalSeconds > 0) ? intervalSeconds : 1;
    (loggerRunningThreadObj =          //start logger thread
                       new LoggerRunningThread("MemoryInfoLogger")).start();
  }

  /**
   * Sets the flag to enable console output of log messages.
   * @param flgVal true to enable console output, false to disable.
   */
  public synchronized void setConsoleOutputFlag(boolean flgVal)
  {
    if(logObj != null)
      logObj.setConsoleLevel(flgVal ? LogFile.ALL_MSGS : LogFile.NO_MSGS);
  }

  /**
   * Sets the log-level value to be used.  The default value is
   * 'LogFile.DEBUG'.
   * @param levelVal the log-level value to be used.
   */
  public synchronized void setLogLevelVal(int levelVal)
  {
    logLevelVal = levelVal;
  }

  /**
   * Sets whether or not the log file is closed when the logging thread
   * terminates.
   * @param flgVal true to close the log file on terminate, false to not.
   */
  public synchronized void setCloseLogOnTerminateFlag(boolean flgVal)
  {
    closeOnTerminateFlag = flgVal;
  }

  /**
   * Terminates the logging thread created by this MemoryInfoLogger.
   */
  public synchronized void terminate()
  {
    if(loggerRunningThreadObj != null)
    {    //thread holder object is OK
      loggerRunningThreadObj.terminate();
      loggerRunningThreadObj = null;        //indicate thread stopped
    }
  }

  /**
   * Sends the given message to the output log file or stream.
   * @param msgStr the message string to send.
   */
  protected void logOutputStr(String msgStr)
  {
    if(logObj != null)
      logObj.println(logLevelVal,msgStr);
    else
      altOutputStream.println(msgStr);
  }

  /**
   * Entry point for test program.  Runs for five seconds, sending results
   * to "MemoryInfoLoggerTest.log" and console.
   * @param args string array of command-line arguments.
   */
//  public static void main(String[] args)
//  {
//    Thread.currentThread().setName("MemoryInfoLoggerTestMain");
//    final MemoryInfoLogger memoryInfoLoggerObj =
//                         new MemoryInfoLogger("MemoryInfoLoggerTest.log",1);
//    memoryInfoLoggerObj.setConsoleOutputFlag(true);
//    try { Thread.sleep(5000); }
//    catch(InterruptedException ex) {}
//    memoryInfoLoggerObj.terminate();
//  }

  /**
   * Class LoggerRunningThread defines a separate thread used to build and
   * output the log file entries.
   */
  protected class LoggerRunningThread extends IstiThread
  {
    /**
     * Creates a logger thread.
     * @param threadName the name for this thread.
     */
    public LoggerRunningThread(String threadName)
    {
      super(threadName);
      setDaemon(true);       //make this a "daemon" (background) thread
    }

    /**
     * Creates a logger thread.  A default name is given to the thread.
     */
    public LoggerRunningThread()
    {
      this("MemoryInfoLoggerThread-" + nextThreadNum());
    }

    /**
     * Executing method for logger thread.
     */
    public void run()
    {
      while(!isTerminated())
      {       //loop until terminate flag set
        try
        {     //delay between log outputs
          sleep((long)intervalSeconds*1000);
        }
        catch(InterruptedException ex)
        {     //sleep delay interrupted
          if(isTerminated())           //if terminate flag set then
            break;                     //exit loop
        }
        logOutputStr("Memory info:  " + UtilFns.getMemoryInfoStr());
      }
      logOutputStr("MemoryInfoLogger terminated OK");
      if(closeOnTerminateFlag && logObj != null)
        logObj.close();      //if flag and logger in use then close log file
    }
  }
}
