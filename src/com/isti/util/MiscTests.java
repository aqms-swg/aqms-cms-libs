//MiscTests.java:  Holder for miscellaneous 'isti.util' tests.
//
//  8/30/2004 -- [ET]
//

package com.isti.util;

/**
 * Class MiscTests is a holder for miscellaneous 'isti.util' tests.
 */
public class MiscTests
{
  public static void main(String [] args)
  {
    final long [] timeValsArr = new long []
      { 0, UtilFns.MS_PER_SECOND, UtilFns.MS_PER_MINUTE,
        UtilFns.MS_PER_HOUR, 5*UtilFns.MS_PER_DAY, 1*UtilFns.MS_PER_DAY+
                      23*UtilFns.MS_PER_HOUR+34*UtilFns.MS_PER_MINUTE+
                                              56*UtilFns.MS_PER_SECOND };

    int i;
    System.out.println("durationMillisToString(val,true,true):");
    for(i=0; i<timeValsArr.length; ++i)
    {
      System.out.println(timeValsArr[i] + " => " +
                  UtilFns.durationMillisToString(timeValsArr[i],true,true));
    }
    System.out.println();

    System.out.println("durationMillisToString(val,true,false):");
    for(i=0; i<timeValsArr.length; ++i)
    {
      System.out.println(timeValsArr[i] + " => " +
                 UtilFns.durationMillisToString(timeValsArr[i],true,false));
    }
    System.out.println();

    System.out.println("durationMillisToString(val,false,false):");
    for(i=0; i<timeValsArr.length; ++i)
    {
      System.out.println(timeValsArr[i] + " => " +
                 UtilFns.durationMillisToString(timeValsArr[i],false,false));
    }
  }
}
