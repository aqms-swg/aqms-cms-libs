//ModBoolean:  'Boolean'-like wrapper class which holds a modifiable
//             boolean value and implements the Comparable interface.
//
//  11/3/2000 -- [ET]  Modified from 'BooleanCp' classs.
//

package com.isti.util;

/**
 * Class ModBoolean is a 'Boolean'-like wrapper class which holds a
 * modifiable boolean value and implements the Comparable interface.
 */
public class ModBoolean extends Number implements Comparable
{
  protected boolean booleanVal;

  /**
   * Allocates a ModBoolean object representing the value argument.
   * @param value the value of the ModBoolean.
   */
  public ModBoolean(boolean value)
  {
    booleanVal = value;
  }

  /**
   * Allocates a ModBoolean object representing the value true if the
   * string argument is not null and is equal, ignoring case, to the
   * string "true". Otherwise, allocate a Boolean object representing
   * the value false. Examples:
   * new ModBoolean("True") produces a ModBoolean object that represents
   * true.
   * new ModBoolean("yes") produces a ModBoolean object that represents
   * false.
   * @param str the string to be converted to a ModBoolean.
   */
  public ModBoolean(String str)
  {
    booleanVal = Boolean.valueOf(str).booleanValue();
  }

  /**
   * Allocates a ModBoolean object which holds the given Boolean object.
   * @param bObj the value of the ModBoolean.
   */
  public ModBoolean(Boolean bObj)
  {
    booleanVal = bObj.booleanValue();
  }

  /**
   * Sets the value of this ModBoolean object.
   * @param val boolean value to be set.
   */
  public void setValue(boolean val)
  {
    booleanVal = val;
  }

  /**
   * Returns the value of this ModBoolean object as a boolean primitive.
   * @return The primitive boolean value of this object.
   */
  public boolean getValue()
  {
    return booleanVal;
  }

  /**
   * Returns the value of this ModBoolean object as a boolean primitive.
   * @return The primitive boolean value of this object.
   */
  public boolean booleanValue()
  {
    return booleanVal;
  }

  /**
   * Returns the boolean value represented by the specified String.
   * A new ModBoolean object is constructed.  This ModBoolean represents
   * the value true if the string argument is not null and is equal,
   * ignoring case, to the string "true".
   * Example: ModBoolean.valueOf("True") returns true.
   * Example: ModBoolean.valueOf("yes") returns false.
   * @param s a string.
   * @return The Boolean value represented by the string.
   */
  public static ModBoolean valueOf(String s)
  {
    return new ModBoolean(Boolean.valueOf(s));
  }

  /**
   * Returns a String object representing this ModBoolean's value.
   * If this object represents the value true, a string equal to
   * "true" is returned. Otherwise, a string equal to "false" is
   * returned.
   * @return A string representation of this object.
   */
  public String toString()
  {
    return Boolean.toString(booleanVal);
  }

  /**
   * Returns a hash code for this ModBoolean object.
   * @return The integer 1231 if this object represents true;
   * returns the integer 1237 if this object represents false.
   */
  public int hashCode()
  {
    return booleanVal ? 1231 : 1237;
  }

  /**
   * Returns true if and only if the argument is not null and is a
   * ModBoolean, BooleanCp or Boolean object that represents the same
   * boolean value as this object.
   * @param obj the object to compare with.
   * @return true if the Boolean objects represent the same value;
   * false otherwise.
   */
  public boolean equals(Object obj)
  {
    if(obj instanceof ModBoolean) //if ModBoolean then use held boolean value
      return booleanVal == ((ModBoolean)obj).booleanVal;
    if(obj instanceof BooleanCp)  //if BooleanCp then use held boolean value
      return booleanVal == ((BooleanCp)obj).booleanValue();
    if(obj instanceof Boolean)    //if Boolean then use held boolean value
      return booleanVal == ((Boolean)obj).booleanValue();
    return false;
  }

  /**
   * Returns the value of this object as an int.
   * @return integer value
   */
  public int intValue()
  {
    return booleanVal ? 1 : 0;
  }

  /**
   * Returns the value of this object as a long.
   * @return long value
   */
  public long longValue()
  {
    return booleanVal ? (long)1 : (long)0;
  }

  /**
   * Returns the value of this object as a float.
   * @return float value
   */
  public float floatValue()
  {
    return booleanVal ? (float)1.0 : (float)0.0;
  }

  /**
   * Returns the value of this object as a double.
   * @return double value
   */
  public double doubleValue()
  {
    return booleanVal ? 1.0 : 0.0;
  }

  /**
   * Returns the value of this object as a byte.
   * @return byte value
   */
  public byte byteValue()
  {
    return booleanVal ? (byte)1 : (byte)0;
  }

  /**
   * Returns the value of this object as a short.
   * @return short value
   */
  public short shortValue()
  {
    return booleanVal ? (short)1 : (short)0;
  }

  /**
   * Compares this object with the specified object 'obj'.  'false' is
   * considered to be "less than" 'true'.
   * @param obj an object to compare to.
   * @return A negative integer, zero, or a positive integer as this
   * object is less than, equal to, or greater than the specified object.
   * @exception ClassCastException if the specified object's type
   * prevents it from being compared to this object.
   */
  public int compareTo(Object obj)
  {
         //if 'obj' is a ModBoolean then cast it and get boolean value;
         // otherwise assume it's a Boolean and cast it to that:
    boolean objVal = (obj instanceof ModBoolean) ?
           ((ModBoolean)obj).booleanValue() : ((Boolean)obj).booleanValue();

    if(booleanVal == objVal)
      return 0;         //if both the same then return 0 for equal
         //if this = 'true' then (true - false) = 1
         //if this = 'false' then (false - true) = -1
    return booleanVal ? 1 : -1;
  }
}
