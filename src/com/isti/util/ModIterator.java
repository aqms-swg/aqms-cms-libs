//ModIterator.java:  Modified version of an iterator.
//
//  11/5/2003 -- [ET]  Initial version.
//  7/30/2004 -- [ET]  Modified to also allow a 'Collection' object to
//                     be given.
//   5/9/2006 -- [KF]  Added support for the "Enumeration" interface.
//

package com.isti.util;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Enumeration;
import java.util.List;
import java.util.Iterator;
import java.util.NoSuchElementException;

/**
 * Class ModIterator is a modified version of an iterator that uses less
 * resources when only one item is available and that can (optionally)
 * use a copy of the given list for iteration, allowing the list to be
 * structurally modified while being iterated through.  A 'Collection'
 * may also be given.
 */
public class ModIterator implements Iterator, Enumeration
{
  protected final Iterator sourceIterObj;
  protected final Object singleItemObj;
  protected boolean itemsRemainingFlag;

  /**
   * Creates a modified version of an iterator.
   * @param listObj the 'List' (or 'Collection') object to be iterated
   * though, or null for none.
   * @param useCopyFlag if true then a copy of the given list is used for
   * iteration, allowing the list to be structurally modified while being
   * iterated through.
   */
  public ModIterator(Collection listObj, boolean useCopyFlag)
  {
    final int sourceListSize;
    if(listObj != null && (sourceListSize=listObj.size()) > 0)
    {    //given list contains at least one item
      if(sourceListSize > 1)
      {  //given list contains more than one item
              //if copy flag is set then make a copy of the list and use
              // its iterator; otherwise just use the iterator directly:
        sourceIterObj = useCopyFlag ?
                 ((new ArrayList(listObj)).iterator()) : listObj.iterator();
        singleItemObj = null;          //handle to single item not used
      }
      else
      {  //given list contains one item
        sourceIterObj = null;               //indicate no iterator
                   //fetch the item from the given list (using 'get()')
                   // or collection (using 'next()'):
        singleItemObj = (listObj instanceof List) ? ((List)listObj).get(0) :
                                                  listObj.iterator().next();
      }
      itemsRemainingFlag = true;       //indicate items are remaining
    }
    else
    {    //given list contains no items
      sourceIterObj = null;            //indicate no iterator
      singleItemObj = null;            //handle to single item not used
      itemsRemainingFlag = false;      //indicate no items are remaining
    }
  }

  /**
   * Creates a modified version of an iterator.  A copy of the given list
   * is used for iteration, allowing the list to be structurally modified
   * while being iterated through.
   * @param listObj the 'List' (or 'Collection') object to be iterated
   * though, or null for none.
   */
  public ModIterator(Collection listObj)
  {
    this(listObj,true);
  }

  /**
   * Returns <tt>true</tt> if the enumeration has more elements. (In other
   * words, returns <tt>true</tt> if <tt>nextElement</tt> would return an
   * element rather than throwing an exception.)
   * @return <tt>true</tt> if the enumeration has more elements.
   */
  public boolean hasMoreElements()
  {
    return hasNext();
  }

  /**
   * Returns <tt>true</tt> if the iteration has more elements. (In other
   * words, returns <tt>true</tt> if <tt>next</tt> would return an element
   * rather than throwing an exception.)
   * @return <tt>true</tt> if the iterator has more elements.
   */
  public boolean hasNext()
  {
    if(sourceIterObj != null)               //if iterator in use then
      return sourceIterObj.hasNext();       //use iterator
    return itemsRemainingFlag;         //if no iterator then use flag
  }

  /**
   * Returns the next element in the iteration.
   * @return the next element in the iteration.
   * @exception NoSuchElementException if the iteration has no more
   * elements.
   */
  public Object next()
  {
    if(sourceIterObj != null)               //if iterator in use then
      return sourceIterObj.next();          //use iterator
    if(itemsRemainingFlag)
    {    //an item is remaining
      itemsRemainingFlag = false;      //clear flag
      return singleItemObj;            //return item
    }
    throw new NoSuchElementException();     //no more items, throw exception
  }

  /**
   * Returns the next element in the enumeration.
   * @return the next element in the enumeration.
   * @exception NoSuchElementException if the enumeration has no more
   * elements.
   */
  public Object nextElement()
  {
    return next();
  }

  /**
   * This operation is not supported.
   * @exception UnsupportedOperationException always throws this exception.
   */
  public void remove()
  {
    throw new UnsupportedOperationException();
  }
}
