//MsgViaFileProcessor.java:  Defines a message-via-file processor.
//
// 11/19/2003 -- [ET]  Initial version.
//  10/5/2006 -- [ET]  Modified to deal with 'processMsg()' returning 'null'
//                     by deleting input file (rather than leaving file
//                     in "process" directory to be processed again and
//                     again).
// 12/20/2006 -- [ET]  Added descriptions to 'ViaFileProperties' items.
//   7/9/2010 -- [ET]  Added "get/setProcCharsetNameStr()" methods.
//

package com.isti.util;

import java.io.File;
import java.io.IOException;

/**
 * Class MsgViaFileProcessor.java defines a message-via-file processor.
 * An input directory is polled for new files; anytime one is found it
 * is moved to a processing directory, read, processed by a call-back
 * object, and then moved to a storage directory or deleted.
 */
public class MsgViaFileProcessor
{
  protected static final String DEF_PROCDIR_NAME = "msgProcess";
  protected static final int MAX_STORAGE_AGE_CHECK_SECS =
                                     10 * (int)(UtilFns.SECONDS_PER_MINUTE);
  protected final File inputDirFileObj;
  protected final File storageDirFileObj;
  protected final File processDirFileObj;
  protected final int inputPollDelayMS;
  protected final int storageAgeSecsVal;
  protected final ProcMsgCallBack procMsgCallBackObj;
  protected MsgFileProcessingThread msgFileProcessingThreadObj = null;
  protected String procCharsetNameStr = null;         //charset to use

  /**
   * Creates a message-via-file processor.
   * @param inputDirFile the input directory to poll for message files,
   * or null to use the current directory.
   * @param storageDirFile the storage directory to use, or null for none.
   * @param processDirFile the process directory to use for message files,
   * or null to use the default directory "msgProcess".
   * @param pollDelayMS the delay, in milliseconds, between polls of the
   * input directory to check for new files.
   * @param storageAgeSecs the maximum time, in seconds, to keep copies
   * of the message files; or 0 to keep no copies.
   * @param procMsgCallBack the call-back object to use to process the
   * message data read from the input files.
   * @throws IOException if the given input, process or storage directory
   * is not found and cannot be created.
   * @throws IllegalArgumentException if the given input and storage
   * directories are the same.
   */
  public MsgViaFileProcessor(File inputDirFile,File storageDirFile,
                     File processDirFile,int pollDelayMS,int storageAgeSecs,
                         ProcMsgCallBack procMsgCallBack) throws IOException
  {
    if(inputDirFile != null)
    {    //input directory given; if not found then create it:
      if(!inputDirFile.isDirectory() && !inputDirFile.mkdirs())
      {    //error creating directory; throw exception
        throw new IOException("Unable to create input directory \"" +
                                    inputDirFile.getAbsolutePath() + "\"");
      }
      inputDirFileObj = inputDirFile;       //save input directory object
    }
    else      //input directory not given
      inputDirFileObj = new File(".");      //use local directory
    if(storageDirFile != null)
    {    //storage directory given; if not found then create it:
      if(!storageDirFile.isDirectory() && !storageDirFile.mkdirs())
      {    //error creating directory; throw exception
        throw new IOException("Unable to create storage directory \"" +
                                    storageDirFile.getAbsolutePath() + "\"");
      }
      storageDirFileObj = storageDirFile;       //save storage directory object
    }
    else      //storage directory not given
      storageDirFileObj = null;             //indicate no storage directory
         //if process directory file object not given then use default:
    processDirFileObj = (processDirFile != null) ? processDirFile :
                                                 new File(DEF_PROCDIR_NAME);
         //if process directory not found then create it:
    if(!processDirFileObj.isDirectory() && !processDirFileObj.mkdirs())
    {    //error creating directory; throw exception
      throw new IOException("Unable to create process directory \"" +
                                processDirFileObj.getAbsolutePath() + "\"");
    }
    if(inputDirFileObj.equals(processDirFileObj))
    {    //directories are equal; throw exception
      throw new IllegalArgumentException(
                              "Input and process directories are the same");
    }
    if(storageDirFileObj != null &&
                                  inputDirFileObj.equals(storageDirFileObj))
    {    //directories are equal; throw exception
      throw new IllegalArgumentException(
                              "Input and storage directories are the same");
    }
    if(storageDirFileObj != null &&
                                processDirFileObj.equals(storageDirFileObj))
    {    //directories are equal; throw exception
      throw new IllegalArgumentException(
                            "Process and storage directories are the same");
    }
    if(!inputDirFileObj.canRead())
    {    //unable to read from input directory; throw exception
      throw new IOException("Unable to read from input directory \"" +
                                    inputDirFile.getAbsolutePath() + "\"");
    }
    if(storageDirFileObj != null && !storageDirFileObj.canWrite())
    {    //unable to write to storage directory; throw exception
      throw new IOException("Unable to write to storage directory \"" +
                                   storageDirFile.getAbsolutePath() + "\"");
    }
    if(procMsgCallBack == null)
      throw new NullPointerException("Null 'procMsgCallBack' parameter");
    procMsgCallBackObj = procMsgCallBack;        //save call-back object
    inputPollDelayMS = pollDelayMS;              //save poll delay value
              //save max storage age (if no storage dir then set to zero):
    storageAgeSecsVal = (storageDirFileObj != null) ? storageAgeSecs : 0;
  }

  /**
   * Creates a message-via-file processor.
   * @param inputDirName the input directory to poll for message files,
   * or null to use the current directory.
   * @param storageDirName the storage directory to use, or null for none.
   * @param processDirName the process directory to use for message files,
   * or null to use the default directory "msgProcess".
   * @param pollDelayMS the delay, in milliseconds, between polls of the
   * input directory to check for new files.
   * @param storageAgeSecs the maximum time, in seconds, to keep copies
   * of the message files; or 0 to keep no copies.
   * @param procMsgCallBack the call-back object to use to process the
   * message data read from the input files.
   * @throws IOException if the given input, process or storage directory
   * is not found and cannot be created.
   * @throws IllegalArgumentException if the given input and storage
   * directories are the same.
   */
  public MsgViaFileProcessor(String inputDirName,String storageDirName,
                      String processDirName,int pollDelayMS,int storageAgeSecs,
                         ProcMsgCallBack procMsgCallBack) throws IOException
  {
    this(((inputDirName != null && inputDirName.trim().length() > 0) ?
                                             new File(inputDirName) : null),
           ((storageDirName != null && storageDirName.trim().length() > 0) ?
                                           new File(storageDirName) : null),
           ((processDirName != null && processDirName.trim().length() > 0) ?
                                           new File(processDirName) : null),
                                pollDelayMS,storageAgeSecs,procMsgCallBack);
  }

  /**
   * Creates a message-via-file processor using to given set of
   * configuration-property objects.
   * @param viaFileProps the set of configuration-property objects to use.
   * @param procMsgCallBack the call-back object to use to process the
   * message data read from the input files.
   * @throws IOException if the given input, process or storage directory
   * is not found and cannot be created.
   * @throws IllegalArgumentException if the given input and storage
   * directories are the same.
   */
  public MsgViaFileProcessor(ViaFileProperties viaFileProps,
                         ProcMsgCallBack procMsgCallBack) throws IOException
  {
    this(viaFileProps.inputDirNameProp.stringValue(),
                              viaFileProps.storageDirNameProp.stringValue(),
                              viaFileProps.processDirNameProp.stringValue(),
                               viaFileProps.inputPollDelayMSProp.intValue(),
                                 viaFileProps.storageAgeSecsProp.intValue(),
                                                           procMsgCallBack);
  }

  /**
   * Starts the messages-via-file processsing thread.
   */
  public void startProcessing()
  {
    if(msgFileProcessingThreadObj == null)
    {    //processing thread not created; create one now
      msgFileProcessingThreadObj = new MsgFileProcessingThread();
      msgFileProcessingThreadObj.start();   //start processing thread
    }
  }

  /**
   * Stops the messages-via-file processsing thread.
   */
  public void stopProcessing()
  {
    if(msgFileProcessingThreadObj != null)
    {    //processsing thread was created
      msgFileProcessingThreadObj.terminate();    //terminate thread
      msgFileProcessingThreadObj = null;         //discard thread object
    }
  }

  /**
   * Sets the charset to use when reading input data bytes into strings.
   * @param nameStr name of charset to use, or null for default.
   */
  public void setProcCharsetNameStr(String nameStr)
  {
    procCharsetNameStr = nameStr;
  }

  /**
   * Returns the charset used when reading input data bytes into strings.
   * @return name of charset in use, or null if default.
   */
  public String getProcCharsetNameStr()
  {
    return procCharsetNameStr;
  }

  /**
   * Class MsgFileProcessingThread defines the messages-via-file
   * processsing thread.
   */
  protected class MsgFileProcessingThread extends IstiThread
  {

    /**
     * Creates a messages-via-file processsing thread.
     */
    public MsgFileProcessingThread()
    {
      super("MsgFileProcessingThread");
    }

    /**
     * Executing method for the messages-via-file processsing thread.
     */
    public void run()
    {
      try
      {            //set flag if using "storage" directory:
        final boolean doStorageFlag = (storageAgeSecsVal > 0 &&
                                                 storageDirFileObj != null);
                   //calculate interval between checks of files in the
                   // "storage" directory for deletion; start with
                   // age value divided by 10:
        final int storageAgeDivTen = storageAgeSecsVal / 10;
                   //if age value divided by 10 is greater than the
                   // maximum-allowed check value then use maximum:
        final long storageAgeCheckIntervalMs =
                (long)((storageAgeDivTen <= MAX_STORAGE_AGE_CHECK_SECS) ?
                            storageAgeDivTen : MAX_STORAGE_AGE_CHECK_SECS) *
                                                      UtilFns.MS_PER_SECOND;
        File [] filesArrObj;
        int idx;
        File currFileObj,storeFileObj;
        String dataMsgStr, resultStr = "";
        long timeVal;
        long nextStorageAgeCheckTime =   //setup next storage-age check time
                     System.currentTimeMillis() + storageAgeCheckIntervalMs;
        outerLoop:                //label for 'break' statement
        while(!isTerminated())
        {     //loop until thread is terminated
          UtilFns.sleep(inputPollDelayMS);  //delay between check for files
          if(isTerminated())      //if thread terminated then
            break;                //exit loop (and thread)
              //move files from "input" to "process" directory:
          if((filesArrObj=inputDirFileObj.listFiles()) != null)
          {   //array of File objects from "input" directory fetched OK
            for(idx=0; idx<filesArrObj.length; ++idx)
            { //for each File object in array
              if(isTerminated())       //if thread terminated then
                break outerLoop;       //exit loops (and thread)
              currFileObj = filesArrObj[idx];    //get file object
              if(currFileObj.length() > 0)
              {    //file length is greater than zero
                try     //create path for message file in process
                {       // directory; make sure filename is not in use:
                  if((storeFileObj=FileUtils.createUnusedFileNameObj(
                    currFileObj.getName(),processDirFileObj,false)) != null)
                  {  //process pathname created OK; move file to process dir
                    currFileObj.renameTo(storeFileObj);
                  }
                }
                catch(Exception ex)
                {  //some kind of exception error; ignore it
                }
              }
            }
          }
              //process files in "process" directory:
          if((filesArrObj=processDirFileObj.listFiles()) != null)
          {   //array of File objects from "process" directory fetched OK
            for(idx=0; idx<filesArrObj.length; ++idx)
            { //for each File object in array
              if(isTerminated())      //if thread terminated then
                break outerLoop;      //exit loops (and thread)
              currFileObj = filesArrObj[idx];    //get file object
              try
              {    //read file data into a string (if possible):
                dataMsgStr = currFileObj.canRead() ?
                                     FileUtils.readFileToString(currFileObj,
                                                 procCharsetNameStr) : null;
              }
              catch(Exception ex)
              {    //some kind of exception error
                dataMsgStr = null;      //indicate no file data
              }
                   //process message-file data via call-back obj:
              if(dataMsgStr != null &&
                                   (resultStr=procMsgCallBackObj.processMsg(
                                 currFileObj.getName(),dataMsgStr)) != null)
              {    //file data was read in and processed OK
                if(doStorageFlag)
                {  //using "storage" directory
                             //trim whitespace from result string; if it
                             // contains data then add prefix and suffix:
                  if((resultStr=resultStr.trim()).length() > 0)
                    resultStr = "M" + resultStr + "_";
                }
              }
              else if(doStorageFlag)   //error processing file data; if using
                resultStr = "Error_";  // "storage" dir then set error prefix
                   //move file from "process" to "storage" dir; or delete:
              if(resultStr != null)
              {    //process call-back did not return 'null'
                if(doStorageFlag)
                {     //using "storage" directory
                  try      //create path for message file in storage
                  {        // directory; use processing-result string
                           // and make sure filename is not in use:
                    if((storeFileObj=FileUtils.createUnusedFileNameObj(
                                            resultStr+currFileObj.getName(),
                                          storageDirFileObj,false)) != null)
                    { //storage pathname created OK
                                //move file to storage directory:
                      if(currFileObj.renameTo(storeFileObj) &&
                                                      !currFileObj.exists())
                      {    //move successful and file is gone
                        currFileObj = null;      //indicate moved OK
                      }
                    }
                  }
                  catch(Exception ex)
                  {   //some kind of exception error; delete file below
                  }
                }
              }
              if(currFileObj != null)
              {     //message file was not moved to storage directory
                try
                {   //attempt to delete file:
                  currFileObj.delete();
                }
                catch(Exception ex)
                {
                }
                if(currFileObj.exists())
                {   //message file still exists; throw exception
                  throw new RuntimeException(
                                    "Unable to delete message file \"" +
                       currFileObj.getName() + "\", aborting processing");
                }
              }
            }
          }
          if(doStorageFlag && (timeVal=System.currentTimeMillis()) >=
                                                    nextStorageAgeCheckTime)
          {   //using "storage" directory and check time reached
            try
            {
              FileUtils.deleteOldFiles(storageDirFileObj,
                             (long)storageAgeSecsVal*UtilFns.MS_PER_SECOND);
            }
            catch(Exception ex)
            {      //some kind of exception error; ignore it
            }      // (storage-file deletion is non-essential)
                                  //setup next storage-age check time:
            nextStorageAgeCheckTime = timeVal + storageAgeCheckIntervalMs;
          }
        }
      }
      catch(Exception ex)
      {  //some kind of exception error; send error message and abort
        procMsgCallBackObj.processMsg(null,
                                  "Error processing message files:  " + ex);
      }
    }
  }


  /**
   * Interface ProcMsgCallBack defines a method for processing a message
   * and returning a completion/success result.
   */
  public interface ProcMsgCallBack
  {
    /**
     * Processes a message string and returns a completion/success result.
     * @param fileNameStr the name of source-file for the message, or
     * null if an error occurred.
     * @param msgStr the message string to process, or an error message
     * string if the 'fileNameStr' parameter is null.
     * @return A non-null ID (or empty) string generated while processing
     * the message (typically a message number), or null if the message
     * was not successfully processed.
     */
    public String processMsg(String fileNameStr,String msgStr);
  }


  /**
   * Class ViaFileProperties manages a set of configuration property items
   * used with the 'MsgViaFileProcessor' class.
   */
  public static class ViaFileProperties
  {
         /** Default input directory name ("msgInput"). */
    public static final String DEF_INPUTDIR_NAME = "msgInput";
         /** Default storage directory name ("msgSave"). */
    public static final String DEF_STOREDIR_NAME = "msgSave";
         /** Number of seconds in a day; integer value. */
    public static final int SECS_PER_DAY = (int)(UtilFns.SECONDS_PER_DAY);
         /** Configuration properties object that holds property items. */
    public final CfgProperties propertiesObj;
         /** Input directory pathname to poll for message files. */
    public final CfgPropItem inputDirNameProp;
         /** Storage directory pathname. */
    public final CfgPropItem storageDirNameProp;
         /** Process directory pathname. */
    public final CfgPropItem processDirNameProp;
         /** Delay between polls of the input directory (ms). */
    public final CfgPropItem inputPollDelayMSProp;
         /** Max time to keep copies of the msg files (0 = no copies). */
    public final CfgPropItem storageAgeSecsProp;


    /**
     * Constructs a properties manager object.
     * @param propsObj the configuration-properties object to add item to,
     * or null to create a new configuration-properties object.
     * @param groupSelObj the configuration-group-selection object to use,
     * or null for none.
     */
    public ViaFileProperties(CfgProperties propsObj,Object groupSelObj)
    {
           //use given config-props object, or create new one:
      propertiesObj = (propsObj != null) ? propsObj : new CfgProperties();

           //Input directory to poll for message files.
      inputDirNameProp = propertiesObj.add("inputDirName",
                         DEF_INPUTDIR_NAME,null,"Input directory pathname").
                                                setGroupSelObj(groupSelObj);

           //Storage directory pathname.
      storageDirNameProp = propertiesObj.add("storageDirName",
                       DEF_STOREDIR_NAME,null,"Storage directory pathname").
                                                setGroupSelObj(groupSelObj);

           //Process directory pathname.
      processDirNameProp = propertiesObj.add("processDirName",
                        DEF_PROCDIR_NAME,null,"Process directory pathname").
                                                setGroupSelObj(groupSelObj);

           //Delay between polls of the input directory (ms).
      inputPollDelayMSProp = propertiesObj.add("inputPollDelayMS",
                         Integer.valueOf(1000),null,"Delay between polls (ms)").
               setValidator(1,100*SECS_PER_DAY).setGroupSelObj(groupSelObj);

           //Max time to keep copies of the msg files (0 = no copies).
      storageAgeSecsProp = propertiesObj.add("storageAgeSecs",
           Integer.valueOf(SECS_PER_DAY),null,"Max time to keep copies (secs)").
             setValidator(0,36500*SECS_PER_DAY).setGroupSelObj(groupSelObj);
    }

    /**
     * Constructs a connection-properties manager object, using a newly-
     * created configuration-properties object.
     */
    public ViaFileProperties()
    {
      this(null,null);
    }

    /**
     * Returns the 'CfgProperties' object holding the configuration-property
     * items.
     * @return The 'CfgProperties' object holding the configuration-property
     * items.
     */
    public CfgProperties getPropertiesObj()
    {
      return propertiesObj;
    }
  }
}
