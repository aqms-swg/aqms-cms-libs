//NullOutputStream.java:  Implements an output stream that discards data.
//
//  2/17/2009 -- [ET]
//

package com.isti.util;

import java.io.OutputStream;
import java.io.IOException;

/**
 * Class NullOutputStream implements an output stream that discards data.
 */
public class NullOutputStream extends OutputStream
{
  /**
   * Writes the specified byte to this output stream.
   * @param      b   the <code>byte</code>.
   * @exception  IOException  if an I/O error occurs.
   */
  public void write(int b) throws IOException
  {
  }

  /**
   * Writes <code>len</code> bytes from the specified byte array
   * starting at offset <code>off</code> to this output stream.
   * @param      b     the data.
   * @param      off   the start offset in the data.
   * @param      len   the number of bytes to write.
   * @exception  IOException  if an I/O error occurs.
   */
  public void write(byte b[], int off, int len) throws IOException
  {
  }
}
