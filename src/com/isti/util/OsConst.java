package com.isti.util;

/** Operating System Constants */
public interface OsConst extends StringConstants {
	/** Operating System Type */
	enum OsType {
		/** Windows */
		WINDOWS(OS_WINDOWS_TEXT),
		/** Mac (Mac OS X, macOS) */
		MAC(OS_MAC_TEXT),
		/** Unix (does not include Mac) */
		UNIX("linux", "-ux", "aix", "bsd", "irix", "solaris", "sunos", "unix"),
		/** Other */
		OTHER;

		/**
		 * Get the OS type.
		 * 
		 * @param osName the OS name or null if none.
		 * @return the OS type, not null.
		 */
		static OsType getOsType(String osName) {
			if (osName != null && !UNKNOWN_STRING.equals(osName)) {
				osName = osName.toLowerCase();
				final OsType[] osTypes = OsType.values();
				for (OsType osType : osTypes) {
					for (String text : osType.textArray) {
						if (osName.indexOf(text) != -1) {
							return osType;
						}
					}
				}
			}
			return OTHER;
		}

		private final String[] textArray;

		OsType(String... text) {
			textArray = text;
		}
	}

	/** Mac OS Text (lower case) */
	static final String OS_MAC_TEXT = "mac";
	/** Windows OS Text (lower case) */
	static final String OS_WINDOWS_TEXT = "windows";
}
