package com.isti.util;

import java.net.PasswordAuthentication;

/**
 * Password Authenticator
 */
public interface PasswordAuthenticator {
  /**
   * Get the password authentication.
   * @return the password authentication or null if none.
   */
  public PasswordAuthentication getPasswordAuthentication();
}
