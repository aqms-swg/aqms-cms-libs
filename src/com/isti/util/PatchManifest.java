//PatchManifest.java:  Utility for patching the manifest portion of a
//                     Windows executable file so it will request UAC
//                     administrator privileges when run.
//
// 10/10/2011 -- [ET]
//

package com.isti.util;

import java.io.InputStream;
import java.io.OutputStream;
import java.io.BufferedInputStream;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.FileNotFoundException;

public class PatchManifest
{
// Executable created by InstallAnywhere 2011 contains:
// 	<security>
//		<requestedPrivileges>
//			<requestedExecutionLevel
//				level="highestAvailable"
//				uiAccess="false"/>
//		</requestedPrivileges>
//	</security>
// Need to make it 'level="requireAdministrator"'
//  while not changing the byte count.
//
// Usage:  java -jar PatchManifest.jar installer.exe [patchedInstaller.exe]
//

  protected static int STREAM_TRANSFER_BUFFER_SIZE = 2048;

  public static void main(String [] args)
  {
    try
    {
      final byte [] SEARCH_ARR = "level=\"highestAvailable\"\n\t\t\t\t".
                                                       getBytes("US-ASCII");
      final byte [] REPLACE_ARR = "level=\"requireAdministrator\"\n".
                                                       getBytes("US-ASCII");

      final byte [] btArr = readFileToBuffer(args[0]);
      final int btArrLen = btArr.length;
      int p, q, scanPos = 0;
      byte firstSearchBt = SEARCH_ARR[0];
      searchLoop:
      while(true)
      {
        if(scanPos >= btArrLen)
        {
          System.out.println("PatchManifest (\"" + args[0] +
                                     "\"):  Unable to find search pattern");
          System.exit(1);
          return;
        }
        if(btArr[scanPos] == firstSearchBt)
        {
          p = 1;
          while(scanPos+p < btArrLen)
          {  //loop while matching search bytes
            if(p >= SEARCH_ARR.length)
            {
              for(q=0; q<REPLACE_ARR.length; ++q)
                btArr[scanPos+q] = REPLACE_ARR[q];
              System.out.println("PatchManifest (\"" + args[0] +
                                                    "\"):  Replacing " + p +
                                           " bytes at position " + scanPos);
              break searchLoop;
            }
            if(btArr[scanPos+p] != SEARCH_ARR[p])
              break;
            ++p;
          }
        }
        ++scanPos;
      }
      final String outFileStr = (args.length > 1) ? args[1] : args[0];
      writeBufferToFile(outFileStr,btArr);
      System.exit(0);
    }
    catch(Exception ex)
    {
      ex.printStackTrace();
      System.exit(1);
    }
  }


  /**
   * Reads data from a file into a buffer.
   * @param fileNameStr the name of the input file.
   * @return A byte array containing the contents of the file.
   * @throws FileNotFoundException if the specified file is not found.
   * @throws IOException if an error occurred while opening or reading from
   * the input file.
   */
  public static byte [] readFileToBuffer(String fileNameStr)
                                   throws FileNotFoundException, IOException
  {
    final BufferedInputStream inStm =       //create file input reader
                  new BufferedInputStream(new FileInputStream(fileNameStr));
    final byte [] byteArr = readStreamToBuffer(inStm);
    inStm.close();                //close input file
    return byteArr;
  }

  /**
   * Reads data from an input stream into a byte array.  The stream
   * is expected to provide characters organized into one or more lines.
   * @param stmObj the input stream to use.
   * @return A byte array containing the contents of the file.
   * @throws IOException if an error occurred while reading from the
   * input stream.
   */
  public static byte [] readStreamToBuffer(InputStream stmObj)
                                                          throws IOException
  {
              //create byte-array output stream and transfer into it:
    final ByteArrayOutputStream outStmObj = new ByteArrayOutputStream();
    transferStream(stmObj,outStmObj);
    outStmObj.flush();                 //make sure data is flushed through
    return outStmObj.toByteArray();    //return byte array containing data
  }


  /**
   * Transfers data from the input stream to the output stream.
   * @param inputStreamObj the input stream.
   * @param outputStreamObj the output stream.
   * @exception  IOException  if an I/O error occurs.
   */
  public static void transferStream(InputStream inputStreamObj,
                            OutputStream outputStreamObj) throws IOException
  {
    int b;
    final byte [] buffer = new byte[STREAM_TRANSFER_BUFFER_SIZE];
    while((b=inputStreamObj.read(buffer,0,STREAM_TRANSFER_BUFFER_SIZE)) > 0)
    {
      outputStreamObj.write(buffer, 0, b);
    }
  }

  /**
   * Writes a data buffer to a file.  Any previous contents in the file are
   * overritten.
   * @param fileNameStr the name of the output file.
   * @param btArr the data to be written.
   * @throws IOException if an error occurred while opening or writing to
   * the output file.
   */
  public static void writeBufferToFile(String fileNameStr, byte [] btArr)
                                                          throws IOException
  {
                                            //open file for output:
    final FileOutputStream outStm = new FileOutputStream(fileNameStr);
    final ByteArrayInputStream inStmObj = new ByteArrayInputStream(btArr);

    transferStream(inStmObj,outStm);        //write data to file
    outStm.close();                         //close output file
  }
}
