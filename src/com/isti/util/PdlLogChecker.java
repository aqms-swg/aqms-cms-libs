package com.isti.util;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileFilter;
import java.io.FileReader;
import java.text.DateFormat;
import java.util.Date;

public class PdlLogChecker implements FileFilter, TimeConstants
{
  public static void main(String[] args)
  {
    new PdlLogChecker(args);
  }

  private long allMaxTimeDiff = 0;
  private int count = 0;
  private final DateFormat dateFormat;
  private final LogFile logFile;

  public PdlLogChecker(String[] args)
  {
    logFile = LogFile.getConsoleLogObj();
    logFile.setConsoleLevel(LogFile.INFO);
    dateFormat = logFile.getDateFormat();
    File file;
    if (args.length > 0)
    {
      for (String arg : args)
      {
        file = new File(arg);
        checkFile(file);
      }
    }
    else
    {
      file = new File(".");
      checkFile(file);
    }
    if (count == 0)
    {
      logFile.warning("No log files found");
      return;
    }
    logFile.info(count + " files found");
    if (count > 1)
    {
      logTimeDiff(allMaxTimeDiff, null);
    }
  }

  @Override
  public boolean accept(File pathname)
  {
    final String name = pathname.getName();
    return name.startsWith("QWPdlFeeder_") && name.endsWith(".log");
  }

  private void checkFile(File file)
  {
    if (file.isDirectory())
    {
      final File[] files = file.listFiles(this);
      for (int i = 0; i < files.length; i++)
      {
        checkFile(files[i]);
      }
      return;
    }
    count++;
    logFile.info(file.toString());
    String line;
    int index;
    Date date;
    long time;
    long lastTime = 0;
    long timeDiff;
    long maxTimeDiff = 0;
    try (BufferedReader br = new BufferedReader(new FileReader(file)))
    {
      while ((line = br.readLine()) != null)
      {
        if (!line.contains("onIndexerEvent:"))
        {
          continue;
        }
        index = line.lastIndexOf(':');
        logFile.debug(line);
        date = dateFormat.parse(line.substring(0, index));
        time = date.getTime();
        if (lastTime != 0)
        {
          timeDiff = time - lastTime;
          if (timeDiff > maxTimeDiff)
          {
            maxTimeDiff = timeDiff;
            if (maxTimeDiff > allMaxTimeDiff)
            {
              allMaxTimeDiff = maxTimeDiff;
            }
          }
        }
        lastTime = time;
      }
      logTimeDiff(maxTimeDiff, file);
    }
    catch (Exception ex)
    {
      logFile.error("Error checking file", ex);
    }
  }

  private String getTimeDiff(long timeDiff)
  {
    int days = 0;
    int hours = 0;
    int minutes = 0;
    int seconds = 0;
    while (timeDiff >= MS_PER_DAY)
    {
      timeDiff -= MS_PER_DAY;
      days++;
    }
    while (timeDiff >= MS_PER_HOUR)
    {
      timeDiff -= MS_PER_HOUR;
      hours++;
    }
    while (timeDiff >= MS_PER_MINUTE)
    {
      timeDiff -= MS_PER_MINUTE;
      minutes++;
    }
    while (timeDiff >= MS_PER_SECOND)
    {
      timeDiff -= MS_PER_SECOND;
      seconds++;
    }
    StringBuilder sb = new StringBuilder();
    if (days > 0)
    {
      if (sb.length() > 0)
      {
        sb.append(", ");
      }
      if (days == 1)
      {
        sb.append("1 day");
      }
      else
      {
        sb.append(days + " days");
      }
    }

    if (hours > 0)
    {
      if (sb.length() > 0)
      {
        sb.append(", ");
      }
      if (hours == 1)
      {
        sb.append("1 hour");
      }
      else
      {
        sb.append(hours + " hours");
      }
    }

    if (minutes > 0)
    {
      if (sb.length() > 0)
      {
        sb.append(", ");
      }
      if (minutes == 1)
      {
        sb.append("1 minute");
      }
      else
      {
        sb.append(minutes + " minutes");
      }
    }
    if (seconds > 0)
    {
      if (sb.length() > 0)
      {
        sb.append(", ");
      }
      if (seconds == 1)
      {
        sb.append("1 second");
      }
      else
      {
        sb.append(seconds + " seconds");
      }
    }
    if (timeDiff > 0)
    {
      if (sb.length() > 0)
      {
        sb.append(", ");
      }
      if (timeDiff == 1)
      {
        sb.append("1 millsecond");
      }
      else
      {
        sb.append(timeDiff + " millseconds");
      }
    }
    return sb.toString();
  }

  private void logTimeDiff(long maxTimeDiff, File file)
  {
    if (file != null)
    {
      logFile.info("max time diff " + maxTimeDiff + " ["
          + getTimeDiff(maxTimeDiff) + "] (" + file + ")");
    }
    else
    {
      logFile.info(
          "max time diff " + maxTimeDiff + " [" + getTimeDiff(maxTimeDiff));
    }
  }
}
