//ProcessUtilFns.java:  Contains various static utility methods
//                      for working with processes.
//
//  6/23/2004 -- [KF]  Initial version.
// 12/01/2006 -- [KF]  Added 'exec(java.lang.String, java.io.PrintStream,
//                     java.io.PrintStream)' method.
//  2/25/2009 -- [ET]  Added version of 'exec()' method with array of
//                     command strings.
//

package com.isti.util;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.IOException;
import java.io.OutputStream;
import java.io.PrintStream;

/**
 * Class ProcessUtilFns contains various static utility methods
 * for working with processes.
 */
public class ProcessUtilFns
{
  //protected constructor so that no object instances may be created
  // (static access only)
  protected ProcessUtilFns()
  {
  }

  /**
   * Executes the specified string command in a separate process.
   * <p>
   * Warning: It is possible that the program will deadlock if the process
   * generates enough output to overflow the system.
   * @see exec(java.lang.String, java.io.PrintStream, java.io.PrintStream)
   * <p>
   * The <code>command</code> argument is parsed into tokens and then
   * executed as a command in a separate process. The token parsing is
   * done by a {@link java.util.StringTokenizer} created by the call:
   * <blockquote><pre>
   * new StringTokenizer(command)
   * </pre></blockquote>
   * with no further modifications of the character categories.
   * This method has exactly the same effect as
   * <code>exec(command, null)</code>.
   *
   * @param      command   a specified system command.
   * @return     a <code>Process</code> object for managing the process.
   * @exception  SecurityException  if a security manager exists and its
   *             <code>checkExec</code> method doesn't allow creation of a process.
   * @exception  IOException if an I/O error occurs
   * @see        java.lang.Runtime#exec(java.lang.String, java.lang.String[])
   * @see     java.lang.SecurityManager#checkExec(java.lang.String)
   */
  public static Process exec(String command) throws IOException
  {
    return Runtime.getRuntime().exec(command);
  }

  /**
   * Executes the specified string command in a separate process.
   * <p>
   * The <code>command</code> argument is parsed into tokens and then
   * executed as a command in a separate process. The token parsing is
   * done by a {@link java.util.StringTokenizer} created by the call:
   * <blockquote><pre>
   * new StringTokenizer(command)
   * </pre></blockquote>
   * with no further modifications of the character categories.
   * This method has exactly the same effect as
   * <code>exec(command, null)</code>.
   *
   * @param      command   a specified system command.
   * @param      stderrPrintStream "standard" error output print stream or
   *             null if the "standard" error output is to be ignored.
   * @param      stdoutPrintStream "standard" output print stream or
   *             null if the "standard" output is to be ignored.
   * @return     a <code>Process</code> object for managing the process.
   * @exception  SecurityException  if a security manager exists and its
   *             <code>checkExec</code> method doesn't allow creation of a process.
   * @exception  IOException if an I/O error occurs
   * @see        java.lang.Runtime#exec(java.lang.String, java.lang.String[])
   * @see     java.lang.SecurityManager#checkExec(java.lang.String)
   */
  public static Process exec(String command, PrintStream stderrPrintStream,
                           PrintStream stdoutPrintStream) throws IOException
  {
    final Process process = Runtime.getRuntime().exec(command);
    final ProcessOutputHandler stderrProcessOutputHandler =
        new ProcessOutputHandler(process.getErrorStream(), stderrPrintStream);
    final ProcessOutputHandler stdoutProcessOutputHandler =
        new ProcessOutputHandler(process.getInputStream(), stdoutPrintStream);
    return new ProcessWithThreads(process,
        new Thread[]{stderrProcessOutputHandler,stdoutProcessOutputHandler});
  }

  /**
   * Executes the specified command and arguments in a separate process.
   * The command specified by the tokens in <code>commandArr</code> is
   * executed as a command in a separate process.
   * @param      commandArr   array containing the command to call and
   *                          its arguments.
   * @param      stderrPrintStream "standard" error output print stream or
   *             null if the "standard" error output is to be ignored.
   * @param      stdoutPrintStream "standard" output print stream or
   *             null if the "standard" output is to be ignored.
   * @return     a <code>Process</code> object for managing the process.
   * @exception  SecurityException  if a security manager exists and its
   *             <code>checkExec</code> method doesn't allow creation of a process.
   * @exception  IOException if an I/O error occurs
   * @see        java.lang.Runtime#exec(java.lang.String, java.lang.String[])
   * @see     java.lang.SecurityManager#checkExec(java.lang.String)
   */
  public static Process exec(String [] commandArr,
                                              PrintStream stderrPrintStream,
                                              PrintStream stdoutPrintStream)
                                                          throws IOException
  {
    final Process process = Runtime.getRuntime().exec(commandArr);
    final ProcessOutputHandler stderrProcessOutputHandler =
        new ProcessOutputHandler(process.getErrorStream(), stderrPrintStream);
    final ProcessOutputHandler stdoutProcessOutputHandler =
        new ProcessOutputHandler(process.getInputStream(), stdoutPrintStream);
    return new ProcessWithThreads(process,
        new Thread[]{stderrProcessOutputHandler,stdoutProcessOutputHandler});
  }

  /**
   * Returns the exit value for the specified process.
   * @param p the process.
   *
   * @return  the exit value of the process represented by this
   *          <code>Process</code> object or
   *          null if the process has not yet terminated.
   *          By convention, <code>0</code> indicates normal termination.
   */
  public static Integer exitValue(Process p)
  {
    try { return Integer.valueOf(p.exitValue()); }
    catch (IllegalThreadStateException ex) {}
    return null;  //the process has not yet terminated
  }

  /**
   * causes the current thread to wait, if necessary, until the
   * specified process has  terminated. This method returns
   * immediately if the process has already terminated. If the
   * process has not yet terminated, the calling thread will be
   * blocked until the process exits.
   * @param p the process.
   *
   * @return     the exit value of the process.
   *             By convention, <code>0</code> indicates normal termination.
   * @exception  InterruptedException  if the current thread is
   *             {@link Thread#interrupt() interrupted} by another thread
   *             while it is waiting, then the wait is ended and an
   *             <code>InterruptedException</code> is thrown.
   */
  public static Integer waitFor(Process p) throws InterruptedException
  {
    return Integer.valueOf(p.waitFor());
  }

  /**
   * causes the current thread to wait, if necessary, until the
   * specified process has  terminated. This method returns
   * immediately if the process has already terminated. If the
   * process has not yet terminated, the calling thread will be
   * blocked until the process exits or the wait time has been reached.
   * If the wait time is reached the process will be terminated.
   * @param p the process.
   * @param millis Waits at most millis milliseconds for this thread to exit.
   * A timeout of 0 means to wait forever.
   *
   * @return     the exit value of the process or null if wait time was reached.
   *             By convention, <code>0</code> indicates normal termination.
   * @exception  InterruptedException  if the current thread is
   *             {@link Thread#interrupt() interrupted} by another thread
   *             while it is waiting, then the wait is ended and an
   *             <code>InterruptedException</code> is thrown.
   */
  public static Integer waitFor(Process p, long millis)
      throws InterruptedException
  {
    if (millis == 0)  //if no wait time
      return waitFor(p);  //wait forever

    long elapsedTime;
    long timeRemaining;
    long sleepTime = 1000;  //default to 1 second between retries
    final long startTime = System.currentTimeMillis();

    for (;;)
    {
      //return the exit value
      try { return Integer.valueOf(p.exitValue()); }
      //if the process has not been terminated
      catch (IllegalThreadStateException ex)
      {
        //if the wait time has been reached
        if ((elapsedTime = System.currentTimeMillis() - startTime) >= millis)
        {
          p.destroy();  //terminate the process
          return null;  //the wait time has been reached
        }
        //make sure the sleep time isn't larger than the maximum
        if (sleepTime > (timeRemaining = millis - elapsedTime))
          sleepTime = timeRemaining;
        Thread.sleep(sleepTime);  //wait before checking again
      }
    }
  }

  /**
   * Process output handler to make sure output does not overflow.
   */
  private static class ProcessOutputHandler extends IstiThread
  {
    private final BufferedReader br;
    private final PrintStream ps;

    /**
     * Create the process output handler.
     * @param is InputStream the input stream.
     * @param ps PrintStream the print stream or null for none.
     */
    public ProcessOutputHandler(InputStream is, PrintStream ps)
    {
      super("ProcessOutputHandler"+nextThreadNum());
      this.br = new BufferedReader(new InputStreamReader(is));
      this.ps = ps;
    }

    /**
     * The logic for the thread to be executed when the thread is started.
     */
    public void run()
    {
      try
      {
        String line;
        while ( (line = br.readLine()) != null)
        {
          if (ps != null)
            ps.println(line);
        }
      }
      catch (Exception ex) {}
    }
  }

  /**
   * Process with threads.
   */
  private static class ProcessWithThreads extends Process
  {
    //the process
    private final Process process;
    //the threads
    private final Thread[] threads;

    /**
     * Creates a process with handlers.
     * @param process the process.
     * @param threads the threads.
     */
    public ProcessWithThreads(Process process, Thread[] threads)
    {
      this.process = process;
      this.threads = threads;
      for (int i = 0; i < threads.length; i++)
      {
        threads[i].start();  //make sure the thread has started
      }
    }

    /**
     * Gets the output stream of the subprocess.
     * Output to the stream is piped into the standard input stream of
     * the process represented by this <code>Process</code> object.
     * <p>
     * Implementation note: It is a good idea for the output stream to
     * be buffered.
     *
     * @return  the output stream connected to the normal input of the
     *          subprocess.
     */
    public OutputStream getOutputStream()
    {
      return process.getOutputStream();
    }

    /**
     * Gets the input stream of the subprocess.
     * The stream obtains data piped from the standard output stream
     * of the process represented by this <code>Process</code> object.
     * <p>
     * Implementation note: It is a good idea for the input stream to
     * be buffered.
     *
     * @return  the input stream connected to the normal output of the
     *          subprocess.
     */
    public InputStream getInputStream()
    {
      return process.getInputStream();
    }

    /**
     * Gets the error stream of the subprocess.
     * The stream obtains data piped from the error output stream of the
     * process represented by this <code>Process</code> object.
     * <p>
     * Implementation note: It is a good idea for the input stream to be
     * buffered.
     *
     * @return  the input stream connected to the error stream of the
     *          subprocess.
     */
    public InputStream getErrorStream()
    {
      return process.getErrorStream();
    }

    /**
     * causes the current thread to wait, if necessary, until the
     * process represented by this <code>Process</code> object has
     * terminated. This method returns
     * immediately if the subprocess has already terminated. If the
     * subprocess has not yet terminated, the calling thread will be
     * blocked until the subprocess exits.
     *
     * @return     the exit value of the process. By convention,
     *             <code>0</code> indicates normal termination.
     * @exception  InterruptedException  if the current thread is
     *             {@link Thread#interrupt() interrupted} by another thread
     *             while it is waiting, then the wait is ended and an
     *             <code>InterruptedException</code> is thrown.
     */
    public int waitFor() throws InterruptedException
    {
      try
      {
        return process.waitFor();
      }
      catch (InterruptedException ex)
      {
        terminateHandlers();
        throw ex;
      }
    }

    /**
     * Returns the exit value for the subprocess.
     *
     * @return  the exit value of the subprocess represented by this
     *          <code>Process</code> object. by convention, the value
     *          <code>0</code> indicates normal termination.
     * @exception  IllegalThreadStateException  if the subprocess represented
     *             by this <code>Process</code> object has not yet terminated.
     */
    public int exitValue()
    {
      return process.exitValue();
    }

    /**
     * Kills the subprocess. The subprocess represented by this
     * <code>Process</code> object is forcibly terminated.
     */
    public void destroy()
    {
      process.destroy();
      terminateHandlers();
    }

    /**
     * Terminate the handlers.
     */
    private void terminateHandlers()
    {
      for (int i = 0; i < threads.length; i++)
      {
        try { threads[i].interrupt(); } catch (Exception ex) {}
      }
    }
  }
}
