//ProgramInformationInterface.java: Defines methods for program information.
//
//  1/19/2006 -- [KF]  Initial version.
//  1/27/2006 -- [KF]  Removed 'getConfigGroupName()' method.
//

package com.isti.util;

import javax.swing.JFrame;
import com.isti.util.gui.ViewHTMLPanelHandler;
import com.isti.util.gui.IstiDialogUtil;

/**
 * Interface ProgramInformationInterface defines methods for program information.
 */
public interface ProgramInformationInterface
{
  /**
   * The default settings config group name string.
   */
  public static final String defaultSettingsConfigGroupName = "Settings";


  /**
   * Gets the 'IstiDialogUtil' object.
   * @return the 'IstiDialogUtil' object or null if none.
   */
  public IstiDialogUtil getIstiDialogUtil();

  /**
   * Get the configuration properties for the program.
   * @return the configuration properties for the program.
   */
  public CfgProperties getProgramCfgProperties();

  /**
   * Returns the frame object for the program.
   * @return The frame object for the program.
   */
  public JFrame getProgramFrameObj();

  /**
   * Returns the main log file object for the program.
   * @return The main log file object for the program.
   */
  public LogFile getProgramLogFileObj();

  /**
   * Get the name string for the program.
   * @return the name string for the program.
   */
  public String getProgramName();

  /**
   * Get the revision string for the program.
   * @return the revision string for the program.
   */
  public String getProgramRevision();

  /**
   * Get the version string for program.
   * @return the version string for program.
   */
  public String getProgramVersion();

  /**
   * Gets the settings config group name string.
   * @return the settings config group name string.
   * @see defaultSettingsConfigGroupName.
   */
  public String getSettingsConfigGroupName();

  /**
   * Gets the settings manual filename.
   * @return the settings manual filename or null if none.
   */
  public String getSettingsManualFilename();

  /**
   * Gets the settings manual title string.
   * @return the settings manual title string or null if none.
   */
  public String getSettingsManualTitleString();

  /**
   * Gets the 'ViewHTMLPanelHandler' object.
   * @return the 'ViewHTMLPanelHandler' object or null if none.
   */
  public ViewHTMLPanelHandler getViewHTMLPanelHandler();

  /**
   * Determinies if saving the configuration file should be allowed.
   * @return true if saving the configuration file should be allowed,
   * false otherwise.
   * @see saveConfiguration
   */
  public boolean isSaveConfigAllowed();

  /**
   * Commits various settings to their configuration items and (possibly)
   * saves the configuration to the default configuration filename.  A
   * separate thread and a slight delay are used so that multiple near-
   * simultaneous calls result in a single write to the config file.
   * @param forceFlag if true then the the configuration is always
   * saved to file; if false then the configuration is only saved to
   * file if a change in the committed-program settings is detected.
   * @param popupFlag if true and an error occurs while saving then
   * a popup message window is shown to display the error.
   * @see isSaveConfigAllowed
   */
  public void saveConfiguration(boolean forceFlag,boolean popupFlag);
}
