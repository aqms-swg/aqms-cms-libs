//ProgramSingleton.java:  Manages a "program singleton" file used to
//                        determine if another instance of the given
//                        program is already running.
//
//  1/29/2004 -- [ET]
//  6/28/2004 -- [KF]  Added 'isRunning' and 'waitForClose' methods.
//  9/14/2004 -- [KF]  Added constructors with parent and file parameter.
//  1/22/2008 -- [ET]  Modified to create any needed parent directories
//                     for the program-singleton file.
//

package com.isti.util;

import java.io.*;

/**
 * Class ProgramSingleton manages a "program singleton" file used to
 * determine if another instance of the given program is already
 * running.  The name of the "program singleton" file will be the
 * program name given to the constructor plus the extension ".psf".
 * While the file-manager thread created by this class is running,
 * the file will be written to once every 30 seconds.  The standard
 * usage of this class is to call the 'initialize()' method in the
 * main method of the program (aborting if the method returns false)
 * and call the 'close()' method from the shutdown-hook for the program
 * (setup via 'Runtime.getRuntime().addShutdownHook()').
 */
public class ProgramSingleton
{
  /** The singleton file extension. */
  public static final String SINGLETON_FILE_EXT = ".psf";
  protected final File singletonFileObj;
  protected final int UPDATE_INTERVAL_MS = 30000;
  protected FileManagerThread fileManagerThreadObj = null;
  protected RandomAccessFile singletonFileAccessObj = null;

  /**
   * Creates a "program singleton" file manager.
   * @param programNameStr the program name to be used.
   */
  public ProgramSingleton(String programNameStr)
  {
    this(new File(programNameStr + SINGLETON_FILE_EXT));
  }

  /**
   * Creates a "program singleton" file manager.
   * @param parent the parent pathname string.
   * @param programNameStr the program name to be used.
   */
  public ProgramSingleton(String parent, String programNameStr)
  {
    this(new File(parent, programNameStr + SINGLETON_FILE_EXT));
  }

  /**
   * Creates a "program singleton" file manager.
   * @param parent the parent file.
   * @param programNameStr the program name to be used.
   */
  public ProgramSingleton(File parent, String programNameStr)
  {
    this(new File(parent, programNameStr + SINGLETON_FILE_EXT));
  }

  /**
   * Creates a "program singleton" file manager.
   * @param singletonFileObj the singleton file object.
   */
  public ProgramSingleton(File singletonFileObj)
  {
    this.singletonFileObj = singletonFileObj;
  }

  /**
   * Determines if another instance of the program is running or not.
   * @return true if another instance is running, false otherwise.
   */
  public boolean isRunning()
  {
    synchronized(singletonFileObj)
    {    //only allow one thread to access file at a time
      if(singletonFileObj.exists())
      {  //program-singleton file exists
        try
        {     //attempt to read numeric time value from file:
          final BufferedReader rdrObj = new BufferedReader(new FileReader(
                                                         singletonFileObj));
          final String str = rdrObj.readLine();
          try { rdrObj.close(); }
          catch(Exception ex) {};
          final long fileTimeVal;
          if(str != null)
          {   //line of data read from file OK
            try
            {
              fileTimeVal = Long.parseLong(str.trim());
              final long curTimeVal = System.currentTimeMillis();
              if(curTimeVal > fileTimeVal &&
                            curTimeVal - fileTimeVal < UPDATE_INTERVAL_MS*2)
              {  //file time not newer than current and within two intervals
//                System.out.println("DEBUG:  Current time value found");
                return true;     //indicate another instance is running
              }
            }
            catch(NumberFormatException ex)
            {      //unable to parse numeric value; move on
            }
          }
        }
        catch(FileNotFoundException ex)
        {     //file not found; move on and attempt to create new file below
        }
        catch(IOException ex)
        {     //IO error; assume that other instance is using file
//          System.out.println("DEBUG:  IO error opening file:  " + ex);
          return true;     //indicate another instance is running
        }
        catch(Exception ex)
        {     //unexpected error; display and move on
          System.err.println("ProgramSingleton:  " +
                            "Error reading program-singleton file:  " + ex);
          ex.printStackTrace();
        }
      }
    }
    return false;
  }

  /**
   * Checks to see if another instance of the program is running and,
   * if not, initializes the "program singleton" file manager thread.
   * @return true if successful, false if another instance of the program
   * is already running.
   */
  public boolean initialize()
  {
    if (isRunning())  //if an instance is already running
      return false;     //indicate another instance is running

    if(fileManagerThreadObj != null)        //if any thread running then
      fileManagerThreadObj.terminate();     //terminate running thread
    fileManagerThreadObj = new FileManagerThread();
    fileManagerThreadObj.start();      //start file-manager thread
    return true;                            //return OK flag
  }

  /**
   * Terminates the "program singleton" file manager thread and
   * closes and deletes the "program singleton" file.
   */
  public void close()
  {
    if(fileManagerThreadObj != null)
    {    //thread is running
      fileManagerThreadObj.terminate();     //terminate thread
      fileManagerThreadObj = null;          //release thread object
    }
    synchronized(singletonFileObj)
    {    //only allow one thread to access file at a time
      try
      {
        if(singletonFileAccessObj != null)
        {     //file output stream is open; close it
//          System.out.println("DEBUG:  Closing program-singleton access");
          try { singletonFileAccessObj.close(); }
          catch(IOException ex) {}
          singletonFileAccessObj = null;    //indicate stream closed
          singletonFileObj.delete();        //delete program-singleton file
//          System.out.println("DEBUG:  Program-singleton file deleted");
        }
      }
      catch(Exception ex)
      {     //ignore exceptions and exit
      }
    }
  }

  /**
   * Returns a string representation of the object.
   * @return a string representation of the object.
   */
  public String toString()
  {
	  return singletonFileObj.toString();
  }

  /**
   * Wait for the program close.
   * @param millis Waits at most millis milliseconds for this program to close.
   * A timeout of 0 means to wait forever.
   * @return true if the program closed, false otherwise.
   */
  public boolean waitForClose(int millis)
  {
    return waitForClose(millis, 1000);
  }

  /**
   * Wait for the program close.
   * @param millis Waits at most millis milliseconds for this program to close.
   * A timeout of 0 means to wait forever.
   * @param retry the number of millis to wait between retries.
   * @return true if the program closed, false otherwise.
   */
  public boolean waitForClose(int millis, int retry)
  {
    long elapsedTime;
    long timeRemaining;
    long sleepTime = retry;  //default time between retries
    final long startTime = System.currentTimeMillis();

    while (isRunning())  //while the program is running
    {
      if (millis > 0)  //if wait time was specified
      {
        //if the wait time has been reached
        if ((elapsedTime = System.currentTimeMillis() - startTime) >= millis)
        {
          return false;  //the wait time has been reached
        }
        //make sure the sleep time isn't larger than the maximum
        if (sleepTime > (timeRemaining = millis - elapsedTime))
          sleepTime = timeRemaining;
      }

      try
      {
        Thread.sleep(sleepTime);  //wait before checking again
      }
      catch (InterruptedException ex)
      {
        return false;  //the thread was interrupted before the close
      }
    }
    return true;  //program is closed
  }

    //test program
//  public static void main(String [] args)
//  {
//    final ProgramSingleton programSingletonObj =
//                               new ProgramSingleton("ProgramSingletonTest");
//    Runtime.getRuntime().addShutdownHook(new Thread()
//        {
//          public void run()
//          {
//            System.out.println("DEBUG:  Running shutdown hook");
//            programSingletonObj.close();
//          }
//        });
//    if(programSingletonObj.initialize())
//    {
//      System.out.println("Program started OK; enter 'Q' to quit");
//      while(!("Q".equalsIgnoreCase(UtilFns.getUserConsoleString())));
//    }
//    else
//      System.out.println("Instance of program already running; aborting");
//  }

  /**
   * Class FileManagerThread defines the "program singleton" file managing
   * thread.
   */
  protected class FileManagerThread extends Thread
  {
    private boolean terminateFlag = false;

    /**
     * Creates the "program singleton" file managing thread.
     */
    public FileManagerThread()
    {
      super("ProgramSingleton");       //set thread name
      setDaemon(true);                 //mark as "daemon" thread
    }

    /**
     * Executing method for the "program singleton" file managing thread.
     */
    public void run()
    {
      synchronized(singletonFileObj)
      {  //only allow one thread to access file at a time
        try
        {
          final File parentFileObj;
          if(!singletonFileObj.exists() &&
                 (parentFileObj=singletonFileObj.getParentFile()) != null &&
                                               !parentFileObj.isDirectory())
          {  //file doesn't exist, has a path and the path doesn't exist
            parentFileObj.mkdirs();      //make all needed directories
          }
                   //open file for random-access write output:
          singletonFileAccessObj =
                                new RandomAccessFile(singletonFileObj,"rw");
          while(!terminateFlag)
          {
            try
            {
              singletonFileAccessObj.seek(0);         //rewind file
              singletonFileAccessObj.setLength(0);    //clear file
                        //write numeric-string version of current time:
              singletonFileAccessObj.writeBytes(
                                 Long.toString(System.currentTimeMillis()));
            }
            catch(IOException ex)
            {      //ignore any IO exceptions
            }
                   //wait until interval complete or thread terminated:
            singletonFileObj.wait(UPDATE_INTERVAL_MS);
          }
        }
        catch(Exception ex)
        {     //unexpected error; display and move on
          System.err.println("ProgramSingleton:  " +
                            "Error writing program-singleton file:  " + ex);
          ex.printStackTrace();
        }
//        System.out.println("DEBUG:  Exiting file-manager thread");
      }
    }

    /**
     * Terminates this thread if the thread is not terminated and alive.
     */
    public void terminate()
    {
      if (!terminateFlag && isAlive())
      {  //thread is not terminated and is alive
//        System.out.println("DEBUG:  Terminating file-manager thread");
        terminateFlag = true;          //set terminate flag
        try
        {
          synchronized(singletonFileObj)
          {   //grab thread-synchronization lock for program-singleton file
            singletonFileObj.notifyAll();        //interrupt wait
          }
          join(100);                   //wait for thread to terminate
        }
        catch(Exception ex)
        {
          System.err.println("ProgramSingleton:  " +
                           "Error terminating file-manager thread:  " + ex);
          ex.printStackTrace();
        }
      }
    }
  }
}
