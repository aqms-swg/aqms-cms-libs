//ProgressIndicatorInterface.java:  Defines the methods used to operate a
//                                  'JProgressBar' object.
//  10/3/2003 -- [ET]
//

package com.isti.util;

/**
 * Interface ProgressIndicatorInterface defines the methods used to operate
 * a 'JProgressBar' object.  Other objects may use this interface to
 * optionally operate a progress bar without having to reference the
 * 'JProgressBar' class.
 */
public interface ProgressIndicatorInterface
{
  /**
   * Sets the minimum value for the progress indicator.
   */
  public void setMinimum(int val);

  /**
   * Sets the maximum value for the progress indicator.
   */
  public void setMaximum(int val);

  /**
   * Returns the minimum value for the progress indicator.
   */
  public int getMinimum();

  /**
   * Returns the maximum value for the progress indicator.
   */
  public int getMaximum();

  /**
   * Sets the value for the progress indicator.
   * @param the value to set.
   */
  public void setValue(int val);

  /**
   * Returns the value for the progress indicator.
   * @return the current value.
   */
  public int getValue();

  /**
   * Sets the orientation for the progress indicator.
   * @param newOrientation a value indicating the orientation (usually
   * JProgressBar.VERTICAL or JProgressBar.HORIZONTAL).
   */
  public void setOrientation(int newOrientation);

  /**
   * Returns the orientation for the progress indicator.
   * @return A value indicating the orientation (usually
   * JProgressBar.VERTICAL or JProgressBar.HORIZONTAL).
   */
  public int getOrientation();
}
