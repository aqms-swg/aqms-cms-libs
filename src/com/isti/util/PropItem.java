//PropItem.java:  Defines a property item object.
//
//    3/7/2006 -- [KF]  Initial version.
//

package com.isti.util;

/**
 * Class PropItem defines a property item object.
 */
public class PropItem extends IstiNamedValue implements PropItemInterface
{
  private final String description; //value description string
  protected Object auxiliaryObj = null;     //auxiliary object for this item
                                         //validator object for item:
  protected Object groupSelObj = null;      //group selection object
  protected CfgPropValidator validatorObj = null;

  /**
   * Creates a value.
   * @param valueName the value name.
   * @param defaultValueObj the default value object.
   */
  public PropItem(String valueName, Object defaultValueObj)
  {
    this(valueName, defaultValueObj, null, null);
  }

  /**
   * Creates a value.
   * @param valueName the value name.
   * @param defaultValueObj the default value object.
   * @param descriptionStr the description text or null for the default.
   */
  public PropItem(String valueName, Object defaultValueObj,
                        String descriptionStr)
  {
    this(valueName, defaultValueObj, null, descriptionStr);
  }

  /**
   * Creates a value.
   * @param valueName the value name.
   * @param defaultValueObj the default value object.
   * @param valueObj the value object or null for the default.
   * @param descriptionStr the description text or null for the default.
   */
  public PropItem(String valueName, Object defaultValueObj,
                        Object valueObj, String descriptionStr)
  {
    super(valueName,defaultValueObj,valueObj);
    if (descriptionStr == null)
      description = UtilFns.getPrettyString(getName());
    else
      description = descriptionStr;
  }

  /**
   * Creates and returns a clone of this object.
   * @return a clone of the this object.
   */
  public Object clone()
  {
    return new PropItem(getName(),getDefaultValue(),getValue(),description);
  }

  /**
   * Returns the "auxiliary" object for this value.
   * @return A handle to the "auxiliary" object.
   */
  public synchronized Object getAuxiliaryObj()
  {
    return auxiliaryObj;
  }

  /**
   * Returns the description string.
   * @return the description string.
   */
  public String getDescriptionStr()
  {
    return description;
  }

  /**
   * Returns the group-select object for this item.
   * @return handle to the group-select object.
   */
  public Object getGroupSelObj()
  {
    return groupSelObj;
  }

  /**
   * Gets the validator object for this item.
   * @return handle to the validator object.
   */
  public synchronized CfgPropValidator getValidator()
  {
    return validatorObj;
  }

    /**
     * Sets the "auxiliary" object for this item.
     * @param obj the value of the object.
     * @return A handle to this object.
     */
  public synchronized PropItem setAuxiliaryObj(Object obj)
  {
    auxiliaryObj = obj;
    return this;
  }

  /**
   * Sets the group-select object for this item.
   * @param obj the group-select object to use.
   * @return A handle to this object.
   */
  public synchronized PropItem setGroupSelObj(Object obj)
  {
    groupSelObj = obj;
    return this;
  }

  /**
   * Sets the validator for this item.
   * @param validatorObj a 'CfgPropValidator' object.
   * @return A handle to this object.
   */
  public synchronized PropItem setValidator(CfgPropValidator validatorObj)
  {
    //if current value is valid
    if (validateValue(validatorObj, getValue()))
      this.validatorObj = validatorObj;
    return this;
  }

  /**
   * Checks that the given object is a valid data value using the specified
   * validator.
   * @param validatorObj a 'CfgPropValidator' object.
   * @param valueObj the value of the object.
   * @return true if the given object is valid (or if no validator has
   * been setup), false if not.
   */
  public static boolean validateValue(CfgPropValidator validatorObj,
                                      Object valueObj)
  {
    if(validatorObj == null)           //if no validator then
      return true;                     //return OK flag
    if(valueObj instanceof Object[])
        {    //given object is an array of objects; validate each element
      Object [] objArr = (Object [])valueObj;
      for(int i=0; i<objArr.length; ++i)
                {  //for each object element in array
        if(!validatorObj.validateValue(objArr[i]))
          return false;      //if any does not validate then return false
      }
      return true;           //if all validate then return OK flag
    }
    //not an array of objects; validate and return result
    return validatorObj.validateValue(valueObj);
  }

  /**
   * Checks the value object.
   * @param valueObj the value object.
   * @return the value object or null if invalid value.
   */
  protected synchronized Object checkValue(Object valueObj)
  {
    valueObj = super.checkValue(valueObj);
    //if value specified then validate new value:
    if(valueObj != null && !validateValue(validatorObj,valueObj))
      return null;          //if validation failed then return error
    return valueObj;
  }
}
