//PropItemInterface.java:  Defines a property item object interface.
//
//    3/7/2006 -- [KF]  Initial version.
//

package com.isti.util;

/**
 * IstiNamedValueInterface defines a property item object interface.
 */
public interface PropItemInterface extends IstiNamedValueInterface
{
  /**
   * Returns the "auxiliary" object for this value.
   * @return A handle to the "auxiliary" object.
   */
  public Object getAuxiliaryObj();

  /**
   * Returns the description string.
   * @return the description string.
   */
  public String getDescriptionStr();

  /**
   * Returns the group-select object for this item.
   * @return handle to the group-select object.
   */
  public Object getGroupSelObj();

  /**
   * Gets the validator object for this item.
   * @return handle to the validator object.
   */
  public CfgPropValidator getValidator();
}
