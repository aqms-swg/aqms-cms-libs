package com.isti.util;

import com.isti.util.ProgramSingleton;
import com.isti.util.UtilFns;
import com.isti.util.LogFile;

/**
 * <p>Title: </p>
 *
 * <p>Description: </p>
 * This program is a generic program restarter. if the program it launches shuts
 * down with any return code other than 0, it will restart that program ad infinitum.
 * There is a 10 second pause to avoid banging away at a bad command.
 * <p>Copyright: Copyright (c) 2005</p>
 *
 * <p>Company: </p>
 *
 * @author not attributable
 * @version 1.0
 */
public class Restarter {

    LogFile logObj = null;

    public Restarter(String clientNameStr) {

        logObj = LogFile.initGlobalLogObj("ISTI_Restarter.log",LogFile.INFO, LogFile.INFO,false, true);

        logObj.info("client is >" + clientNameStr + "<");

        CLIENT_PROGRAM_SINGLETON_NAME = clientNameStr;

        //create the program singleton object
        clientProgramSingletonObj =
                new ProgramSingleton(CLIENT_PROGRAM_SINGLETON_NAME);
    }


       //create client "program singleton" object:
       private final ProgramSingleton clientProgramSingletonObj;


       /** Name for the "program singleton" object. */
        public static String CLIENT_PROGRAM_SINGLETON_NAME = "TMP_NAME";

       String cmdStr = new String("/usr/bin/java -jar /Users/sidhellman/jbproject/Test/Test.jar");

       public String runClient(String command) {

           cmdStr = command;
           return runClient();

       }


       public String runClient()
       {
           String clientNameStr = CLIENT_PROGRAM_SINGLETON_NAME;

           //if the client is already running
           if (clientProgramSingletonObj.isRunning())
           {
               return clientNameStr + " is already running";
           }

           boolean needToStart = true;

           while (true) {
               try
               {
                   if (needToStart) {
                       logObj.info("Starting " + clientNameStr + " process ('" +
                                   cmdStr + "')");

                       //            final Process p = Runtime.getRuntime().exec(cmdStr, null, parentFile);
                       final Process p = Runtime.getRuntime().exec(cmdStr);

                       Runtime.getRuntime().addShutdownHook(new Thread() {
                           public void run() {
                               logObj.info("shutting down");
                               //                           p.destroy();
                           }
                       });

                       int retVal = 0;
                       //see if the process terminated
                       boolean keepChecking = true;
                       while (keepChecking) {
                           try {
                             try {
                                 Thread.sleep(3000);
                             } catch (InterruptedException ex1) {
                             }

                               retVal = p.exitValue();
                               if (retVal == 0) {
                                   // if we exited ok, just let it be
                                   logObj.info("Clean exit of " + clientNameStr + ", retVal was " +
                                                      retVal);
                                   System.exit(0);
                               }

                               // wait 10 seconds before attempting to restart
                               try {
                                   Thread.sleep(10000);
                               } catch (InterruptedException ex1) {
                               }

                               logObj.info("need to restart. " + clientNameStr + ", retVal was " +
                                                  retVal);
                               keepChecking = false;
                               needToStart = true;

                           } catch (IllegalThreadStateException ex) {
                               //process is still running, we are done
                               if (needToStart) {
                                   logObj.info(clientNameStr +
                                           " process started successfully");
                                   needToStart = false;
                               }
                               //          return null;
                           }
                       }
                   }
               }
               catch(Exception ex)  //exception while trying to run the client
               {
                   //show stack trace
                   logObj.info(UtilFns.getStackTraceString(ex));
                   return clientNameStr + "could not be launched" ;
               }
               //    return clientNameStr + " process terminated";
           }
       }

       public static void main(String[] args) {
           Restarter restarter = new Restarter(args[0]);
           restarter.runClient(args[1]);
       }
   }
