//SimplePropsParser.java:  Simple parser for a string of name=value
//                         property items.
//
//  8/13/2004 -- [ET]  Initial version.
//  10/6/2004 -- [ET]  Added null and empty-string parameter check to
//                     'getValue()' method.
//

package com.isti.util;

/**
 * Class SimplePropsParser is a simple parser for a string of name=value
 * property items.  Any of the items may be surrounded by quotes and
 * whitespace is ignore.  Using this class consumes less resources than
 * converting the string to a 'Properties' or a 'CfgProperties' object.
 */
public class SimplePropsParser
{
  protected final String propertiesString;
  protected final char separatorChar;
  protected final int propsStrLen;
  protected int propsStrPos = 0;

  /**
   * Creates a simple parser for a string of name=value property items.
   * @param propsStr the string of property items to parse.
   * @param sepCh the separator character for the property items.
   */
  public SimplePropsParser(String propsStr, char sepCh)
  {
    if(propsStr == null)
      throw new NullPointerException("Null parameter");
    propertiesString = propsStr;
    separatorChar = sepCh;
    propsStrLen = propertiesString.length();
  }

  /**
   * Creates a simple parser for a string of name=value property items.
   * A comma is used as the separator character.
   * @param propsStr the string of property items to parse.
   */
  public SimplePropsParser(String propsStr)
  {
    this(propsStr,',');
  }

  /**
   * Returns the value associated with the given name string.  Note that
   * this method will be more efficient if property items are requested
   * in same order as they appear in the property-items string.
   * @param nameStr the name string to find.
   * @return The associated value string, or null if none found.
   */
  public String getValue(String nameStr)
  {
    if(nameStr == null || nameStr.length() <= 0)
      return null;           //if no name string then return null
    final int nameStrLen = nameStr.length();
    int p, cPos, sPos = propsStrPos;
    boolean quoteFlag,eqFoundFlag;
    char ch;
    while(sPos < propsStrLen)
    {    //for each time the name string is found
      if((p=propertiesString.indexOf(nameStr,sPos)) < 0)
      {  //name string not found after current position
              //if current position is zero or name string not
              // found in entire string then return null:
        if(sPos <= 0 || (p=propertiesString.indexOf(nameStr)) < 0)
          return null;
      }
      sPos = p + nameStrLen;      //setup new start pos for next iteration
              //make sure character before name is some kind of separator:
      if(p <= 0 || (ch=propertiesString.charAt(p-1)) == '\"' || ch <= ' ' ||
                                                        ch == separatorChar)
      {  //character before name is beginning of string or a separator
              //find equals character and value string:
        p = sPos;                 //start past name string
        quoteFlag = false;
        while(true)
        {     //for each character before the equals character
          if(p >= propsStrLen)    //if at end of string then
            return null;          //return null for "not found"
          if((ch=propertiesString.charAt(p)) == '=')
          {     //equals character found
            eqFoundFlag = true;   //indicate found
            break;                //exit inner loop
          }
          if(ch != ' ' && ch != '\t')
          {   //character is not space or tab character
            if(ch == '\"')
            {      //double-quote character found
              if(quoteFlag)
              {    //not the first double-quote character found
                eqFoundFlag = false;   //indicate equals character not found
                break;                 //exit inner loop
              }
              else      //this is the first double-quote character found
                quoteFlag = true;      //indicate double-quote char found
            }
            else
            {      //not double-quote character; unexpected character found
              eqFoundFlag = false;     //indicate equals character not found
              break;                   //exit inner loop
            }
          }
          ++p;               //increment to next character
        }
        if(eqFoundFlag)
        {     //equals character was found
          if(++p >= propsStrLen)            //increment past equals char
          {   //end of string reached
            propsStrPos = 0;                //reset current position
            return UtilFns.EMPTY_STRING;    //return an empty string
          }
                   //find separator character (ignore quotes):
          if((cPos=UtilFns.findCharPos(propertiesString,separatorChar,p)) < 0)
            cPos = propsStrLen;   //if not found then use end-of-string pos
                        //setup new current position after separator:
          if((propsStrPos=cPos+1) >= propsStrLen)
            propsStrPos = 0;      //if past end of string then reset pos
              //remove quote (escape) chars and double-quotes and return:
          return UtilFns.removeQuoteChars(
                            propertiesString.substring(p,cPos).trim(),true);
        }
      }
    }
    return null;
  }

  /** Test program. */
/*
  public static void main(String [] args)
  {
    final String propsStr = "\"ClientName\"=\"CISN Display\"," +
     "\"ClientVersion\"=\"0.363Beta\"," +
     "\"DistribName\"=\"QWClient_default\"," +
     "\"StartupTime\"=\"Aug 12 2004 18:54:20 EDT\"," +
     "\"ClientIP\"=\"192.168.0.66\",\"ClientHost\"=\"cp1800\"," +
     "\"CommVersion\"=\"1.0\",\"OpenORBVersion\"=\"1.4.0_23Feb2004_mod\"," +
     "\"ClientOSName\"=\"Windows 2000\",\"JavaVersion\"=\"1.3.1\"";
//    final String propsStr = "\"ClientName\" = \"CISN Display\", " +
//     "\"ClientVersion\" = \" 0.363Beta \"," +
//     "\"DistribName\"=\"QWClient_default\"," +
//     "\"StartupTime\" =\"Aug 12 2004 18:54:20 EDT\"," +
//     "\"ClientIP\"=\"192.168.0.66\", \"ClientHost\"=\"cp1800\"," +
//     "\"CommVersion\"=\"1.0\",\"OpenORBVersion\" = \"1.4.0_23Feb2004_mod\"," +
//     "\"ClientOSName\"=\"Windows 2000\",\"JavaVersion\"=\"1.3.1\"";

    final SimplePropsParser parserObj = new SimplePropsParser(propsStr);
    final String [] namesArr = new String [] {
               "ClientName", "ClientVersion","DistribName", "StartupTime",
               "ClientIP", "ClientHost", "CommVersion", "OpenORBVersion",
               "ClientOSName", "JavaVersion" };
//    final String [] namesArr = new String [] {
//            "ClientName", "ClientVersion", "ClientIP", "ClientHost",
//            "DistribName", "StartupTime", "JavaVersion", "CommVersion",
//            "OpenORBVersion", "ClientOSName" };

    for(int i=0; i<namesArr.length; ++i)
    {
      System.out.println(namesArr[i] + ":  \"" +
                                    parserObj.getValue(namesArr[i]) + "\"");
    }
  }
*/
}
