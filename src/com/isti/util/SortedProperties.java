//SortedProperties.java:  Extends Properties to add sorting.
//
//    5/9/2006 -- [KF]  Initial version.
//

package com.isti.util;

import java.util.Enumeration;
import java.util.Properties;
import java.util.Set;
import java.util.TreeSet;

/**
 * Class SortedProperties extends Properties to add sorting.
 */
public class SortedProperties extends Properties
{
  /**
   * Creates an empty property list with no default values.
   */
  public SortedProperties()
  {
    this(null);
  }

  /**
   * Creates an empty property list with the specified defaults.
   * @param   defaults   the defaults.
   */
  public SortedProperties(Properties defaults)
  {
    super(defaults);
  }

  /**
   * Returns an enumeration of the keys in this property list.
   * @return  an enumeration of the keys in this property list.
   */
  public synchronized Enumeration keys()
  {
    return new ModIterator(keySet(),false);
  }

  /**
   * Returns a Set view of the keys contained in this property list.
   * @return a set view of the keys contained in this property list.
   */
  public Set keySet()
  {
    return new TreeSet(super.keySet());
  }
}
