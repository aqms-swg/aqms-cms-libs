//StringConstants:  Defines simple string constants.
//
//=====================================================================
// Copyright (C) 2020 Instrumental Software Technologies, Inc.
//
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions
// are met:
// 1. Redistributions of source code, or portions of this source code,
//    must retain the above copyright notice, this list of conditions
//    and the following disclaimer.
// 2. Redistributions in binary form must reproduce the above copyright
//    notice, this list of conditions and the following disclaimer in
//    the documentation and/or other materials provided with the
//    distribution.
// 3. All advertising materials mentioning features or use of this
//    software must display the following acknowledgment:
//    "This product includes software developed by Instrumental
//    Software Technologies, Inc. (http://www.isti.com)"
// 4. If the software is provided with, or as part of a commercial
//    product, or is used in other commercial software products the
//    customer must be informed that "This product includes software
//    developed by Instrumental Software Technologies, Inc.
//    (http://www.isti.com)"
// 5. The names "Instrumental Software Technologies, Inc." and "ISTI"
//    must not be used to endorse or promote products derived from
//    this software without prior written permission. For written
//    permission, please contact "info@isti.com".
// 6. Products derived from this software may not be called "ISTI"
//    nor may "ISTI" appear in their names without prior written
//    permission of Instrumental Software Technologies, Inc.
// 7. Redistributions of any form whatsoever must retain the following
//    acknowledgment:
//    "This product includes software developed by Instrumental
//    Software Technologies, Inc. (http://www.isti.com/)."
// THIS SOFTWARE IS PROVIDED BY INSTRUMENTAL SOFTWARE
// TECHNOLOGIES, INC. "AS IS" AND ANY EXPRESSED OR IMPLIED
// WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES
// OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
// DISCLAIMED.  IN NO EVENT SHALL INSTRUMENTAL SOFTWARE TECHNOLOGIES,
// INC. OR ITS CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
// INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
// (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
// SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
// HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
// STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
// ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED
// OF THE POSSIBILITY OF SUCH DAMAGE.
//=====================================================================
//  A current version of the software can be found at
//                http://www.isti.com
//  Bug reports and comments should be directed to
//  Instrumental Software Technologies, Inc. at info@isti.com
//=====================================================================
//

package com.isti.util;

/**
 * Interface StringConstants defines simple string constants.<br>
 * All of these constants are simple as they have no external dependencies.
 */
public interface StringConstants {
  /** Hex value prefix */
  public static final String HEX_PREFIX = "0x";

  /** Static string containing the numeric characters '0' through '9'. */
  public static final String INTEGER_CHARS = "0123456789";

  /**
   * Static string containing the upper-case HEX characters 'A' through 'F'.
   */
  public static final String HEX_ALPHA_CAP_CHARS = "ABCDEF";

  /**
   * Static string containing the lower-case HEX characters 'a' through 'f'.
   */
  public static final String HEX_ALPHA_LOW_CHARS = "abcdef";

  /**
   * Static string containing the HEX characters.
   */
  public static final String HEX_CHARS = INTEGER_CHARS + HEX_ALPHA_CAP_CHARS
      + HEX_ALPHA_LOW_CHARS;

  /**
   * Static string containing the numeric characters '0' through '9' and the
   * sign characters '-' and '+'.
   */
  public static final String SIGNED_INT_CHARS = INTEGER_CHARS + "-+";

  /**
   * Static string containing the numeric characters '0' through '9' and the
   * decimal point character '.'.
   */
  public static final String FLOAT_CHARS = INTEGER_CHARS + ".";

  /**
   * Static string containing the numeric characters '0' through '9', the
   * decimal point character '.' and the sign characters '-' and '+'.
   */
  public static final String SIGNED_FLOAT_CHARS = FLOAT_CHARS + "-+";

  /**
   * Static string containing the numeric characters '0' through '9', the
   * decimal point character '.' and the exponent characters 'e' and 'E'.
   */
  public static final String EFLOAT_CHARS = FLOAT_CHARS + "eE";

  /**
   * Static string containing the numeric characters '0' through '9', the
   * decimal point character '.', the exponent characters 'e' and 'E', and the
   * sign characters '-' and '+'.
   */
  public static final String SIGNED_EFLOAT_CHARS = SIGNED_FLOAT_CHARS + "eE";

  /**
   * Static string containing the upper-case characters 'A' through 'Z'.
   */
  public static final String ALPHA_CAP_CHARS = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";

  /**
   * Static string containing the lower-case characters 'a' through 'z'.
   */
  public static final String ALPHA_LOW_CHARS = "abcdefghijklmnopqrstuvwxyz";

  /**
   * Static string containing the characters 'A' through 'Z' and 'a' through
   * 'z'.
   */
  public static final String ALPHA_CHARS = ALPHA_CAP_CHARS + ALPHA_LOW_CHARS;

  /**
   * Static string containing the characters 'A' through 'Z', 'a' through 'z'
   * and '0' though '9'.
   */
  public static final String ALPHANUMERIC_CHARS = ALPHA_CHARS + INTEGER_CHARS;

  /**
   * Static string containing the characters needed to specify a time of day
   * ('0' through '9', ':' and '.').
   */
  public static final String TOD_CHARS = ":." + INTEGER_CHARS;

  /**
   * Static string containing the asterisk wildcard character.
   */
  public static final String WILDCARD_ALL_CHAR = "*";

  /**
   * Static string containing the question-mark wildcard character.
   */
  public static final String WILDCARD_SINGLE_CHAR = "?";

  /**
   * Static string containing asterisk and question-mark wildcard characters.
   */
  public static final String WILDCARD_CHARS = WILDCARD_ALL_CHAR
      + WILDCARD_SINGLE_CHAR;

  /** Boolean true text. */
  public static final String BOOLEAN_TRUE_TEXT = Boolean.TRUE.toString();

  /** Boolean false text. */
  public static final String BOOLEAN_FALSE_TEXT = Boolean.FALSE.toString();

  /** Empty string. */
  public static final String EMPTY_STRING = "";

  /** Empty string. */
  public static final String[] EMPTY_STRINGS = new String[0];

  /** String containing a single space character. */
  public static final String SPACE_STRING = " ";

  /** String containing a single double-quote ("). */
  public static final String QUOTE_STRING = "\"";

  /** String containing all alphanumeric characters 0-9, A-Z and a-z. */
  public static final String ALPHANUM_CHARS_STRING = ALPHANUMERIC_CHARS;

  /** String of invalid file name characters. */
  public static final String INVALID_FILENAME_CHARS = "%\"*<>?|/\\:";

  /** Static string containing white space characters. */
  public static final String WHITE_SPACE_CHARS = " \t\n\r\f";

  /** DateFormat string, 24-hour clock ("MMM dd yyyy HH:mm:ss z"). */
  public static final String DATE_FORMAT24_STR = "MMM dd yyyy HH:mm:ss z";

  /** DateFormat string, AM/PM clock ("MMM dd yyyy hh:mm:ss a z"). */
  public static final String DATE_FORMAT_AMPM_STR = "MMM dd yyyy hh:mm:ss a z";

  /** Unknown String */
  public static final String UNKNOWN_STRING = "(Unknown)";
  
  /** The default Java version string. */
  public static final String DEFAULT_JAVA_VERSION_STRING = UNKNOWN_STRING;

  /** The default separator string. */
  public static final String DEFAULT_SEP_STR = ", ";

  /** The default null string. */
  public static final String DEFAULT_NULL_STR = "null";

  /** DateFormat string for parsing and formatting RFC-822 date/time values. */
  public static final String DATE_FORMAT_RFC_822_STR = "EEE, dd MMM yyyy HH:mm:ss z";
}
