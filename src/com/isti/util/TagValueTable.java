//TagValueTable.java:  Defines a table where each entry contains a
//                     tag string and a mutable integer value.
//
//  1/27/2005 -- [ET]  Initial version.
//   2/8/2005 -- [ET]  Added 'clone()' method.
// 10/20/2006 -- [ET]  Fixed 'remove()' method.
//  7/15/2010 -- [ET]  Added "get/setValue()" methods to 'MutableLong'
//                     class; added 'createMutableLong()' method.
//   9/1/2010 -- [ET]  Added optional 'sepStr' parameter to method
//                     'getEntriesListStr()'.
//

package com.isti.util;

/**
 * Class TagValueTable defines a table where each entry contains a
 * a tag string and a mutable integer value.
 */
public class TagValueTable
{
         //table of entries with key = String, value = MutableLong:
  protected final FifoHashtable hashTableObj;

  /**
   * Creates a tag-value table.
   */
  public TagValueTable()
  {
    hashTableObj = new FifoHashtable();
  }

  /**
   * Protected constructor for 'entriesListStrToTable()' method.
   * @param hashTableObj table object to use.
   */
  protected TagValueTable(FifoHashtable hashTableObj)
  {
    this.hashTableObj = hashTableObj;
  }

  /**
   * Sets the value for the given tag string.  If the tag string
   * has a previous value then it will be overwritten.
   * @param tagStr the tag string to use.
   * @param value the value to use.
   */
  public void put(String tagStr, long value)
  {
    synchronized(hashTableObj)
    {    //only allow one thread to access table at a time
      final Object obj;      //if entry exists then update value:
      if((obj=hashTableObj.get(tagStr)) instanceof MutableLong)
        ((MutableLong)obj).setValue(value);
      else                   //if no match then create new entry:
        hashTableObj.put(tagStr,createMutableLong(value));
    }
  }

  /**
   * Returns the value for the given tag string.
   * @param tagStr the tag string to use.
   * @return The value for the tag string, or 0 if no value is set for
   * the tag string.
   */
  public long get(String tagStr)
  {
    final Object obj;
    return ((obj=hashTableObj.get(tagStr)) instanceof MutableLong) ?
                                         ((MutableLong)obj).getValue() : 0L;
  }

  /**
   * Returns the value for the given tag string.
   * @param tagStr the tag string to use.
   * @return A 'Long' object containing the value for the tag string, or
   * null if no value is set for the tag string.
   */
  public Long getLong(String tagStr)
  {
    final Object obj;
    return ((obj=hashTableObj.get(tagStr)) instanceof MutableLong) ?
                           (Long.valueOf(((MutableLong)obj).getValue())) : null;
  }

  /**
   * Returns the value for the given tag string.
   * @param tagStr the tag string to use.
   * @return A 'MutableLong' object containing the value for the tag
   * string, or null if no value is set for the tag string.
   */
  public MutableLong getMutableLong(String tagStr)
  {
    final Object obj;
    return ((obj=hashTableObj.get(tagStr)) instanceof MutableLong) ?
                                                    (MutableLong)obj : null;
  }

  /**
   * Removes the table entry for the given tag string.
   * @param tagStr the tag string for the entry to be removed.
   * @return A 'MutableLong' object containing the value for the tag
   * string, or null if no value is set for the tag string.
   */
  public MutableLong remove(String tagStr)
  {
    final Object obj;
    return ((obj=hashTableObj.remove(tagStr)) instanceof MutableLong) ?
                                                    (MutableLong)obj : null;
  }

  /**
   * Creates a new 'MutableLong' object.  This method may be overridden
   * by a subclass.
   * @param value value for new 'MutableLong' object.
   * @return A new 'MutableLong' object.
   */
  public MutableLong createMutableLong(long value)
  {
    return new MutableLong(value);
  }

  /**
   * Returns the number of entries in the table.
   * @return The number of entries in the table.
   */
  public int getNumEntries()
  {
    return hashTableObj.size();
  }

  /**
   * Creates a shallow copy of the table.  All the structure of the
   * table itself is copied, but the keys and values are not cloned.
   * @return A new 'TagValueTable' object cloned from the table.
   */
  public Object clone()
  {
    return new TagValueTable(hashTableObj);
  }

  /**
   * Returns a data list string of the entries contained in the table.
   * If the table contains no entries then an empty string is returned.
   * @param sepStr separator string to use between entries in the
   * returned string.
   * @return A list string in the form:  "tag"=value,...
   */
  public String getEntriesListStr(String sepStr)
  {
         //return list string (quote/escape special characters, surround
         // tags with double-quotes, don't surround values with quotes):
    return hashTableObj.toQuotedStrings(sepStr,true,false,true);
  }

  /**
   * Returns a data list string of the entries contained in the table.
   * A comma is used to separate entries in the returned string.
   * If the table contains no entries then an empty string is returned.
   * @return A list string in the form:  "tag"=value,...
   */
  public String getEntriesListStr()
  {
    return getEntriesListStr(",");
  }

  /**
   * Creates a tag-value table from the given list string.
   * @param listStr a list string in the form:  "tag"=value,...
   * @return A new 'TagValueTable' object, or null if the string could
   * not be successfully parsed.
   */
  public static TagValueTable entriesListStrToTable(String listStr)
  {
    final FifoHashtable tableObj;      //convert list string to table:
    if((tableObj=FifoHashtable.quotedStringsToTable(listStr,',',false)) ==
                                                                       null)
    {
      return null;
    }
         //convert table values from strings to 'MutableLong' objects:
    Object obj;
    MutableLong mutLongObj;
    final int tSize = tableObj.size();
    for(int i=0; i<tSize; ++i)
    {    //for each entry in table
      if((obj=tableObj.elementAt(i)) instanceof String &&
                  (mutLongObj=MutableLong.stringToObj((String)obj)) != null)
      {       //string value converted to 'MutableLong' object OK
        tableObj.setElementAt(mutLongObj,i);     //set val to 'MutableLong'
      }
      else    //unable to convert value to 'MutableLong' object
        return null;
    }
         //create tag-value from hash-table and return it:
    return new TagValueTable(tableObj);
  }

//  /**
//   * Test program.
//   * @param args string array of command-line arguments.
//   */
//  public static void main(String [] args)
//  {
//    final TagValueTable tableObj = new TagValueTable();
//    tableObj.put("tag1",1);
//    tableObj.put("tag2",2);
//    tableObj.put("tag3",9);
//    tableObj.put("tag4",4);
//    tableObj.put("tag5",5);
//    tableObj.put("tag3",3);
//    final String str = tableObj.getEntriesListStr();
//    System.out.println("Table1:  " + str);
//    final TagValueTable t2Obj = TagValueTable.entriesListStrToTable(str);
//    System.out.println("Table2:  " + ((t2Obj != null) ?
//                                     t2Obj.getEntriesListStr() : "<null>"));
//    System.out.println("t2Obj.get(\"tag3\")=" + t2Obj.get("tag3"));
//  }


  /**
   * Class MutableLong defines a long integer value that may be modified.
   */
  public static class MutableLong
  {
    private long value;

    /**
     * Creates a long integer value that may be modified.
     * @param value initial value.
     */
    public MutableLong(long value)
    {
      this.value = value;
    }

    /**
     * Sets the current value.
     * @param value to use.
     */
    public void setValue(long value)
    {
      this.value = value;
    }

    /**
     * Returns the current value.
     * @return the current value.
     */
    public long getValue()
    {
      return value;
    }

    /**
     * Returns a numeric string representation of the value.
     * @return A numeric string representation of the value.
     */
    public String toString()
    {
      return Long.toString(value);
    }

    /**
     * Creates a 'MutableLong' object from the given numeric string.
     * @param str the numeric string to use.
     * @return A new 'MutableLong' object, or null if the string could
     * not be successfully parsed.
     */
    public static MutableLong stringToObj(String str)
    {
      try
      {                 //skip any leading '+' and trim whitespace:
        final int p = (str.startsWith("+")) ? 1 : 0;
        return new MutableLong(Long.parseLong(str.substring(p).trim()));
      }
      catch(NumberFormatException ex)
      {       //parsing error
        return null;
      }
      catch(NullPointerException ex)
      {       //null parameter given
        return null;
      }
    }
  }
}
