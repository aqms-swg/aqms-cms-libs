//TagValueTrkTable.java:  Defines a table where each entry contains a
//                        a tag string and a mutable integer value.
//                        Optional historical tracking of entered values
//                        is also available.
//
//  7/16/2010 -- [ET]
//

package com.isti.util;

/**
 * Class TagValueTrkTable defines a table where each entry contains a
 * a tag string and a mutable integer value.  Optional historical tracking
 * of entered values is also available.
 */
public class TagValueTrkTable extends TagValueTable
{
    /** Default number of values to track (100). */
  public static final int DEF_TRKBUFF_SIZE = 100;
  private final int trackingBufferSize;

  /**
   * Creates a tag-value table, with optional historical tracking of
   * values.
   * @param trackValsFlag true to track values, false to not track values
   * (operate like TagValueTable).
   * @param trkBuffSize number of values to track.
   */
  public TagValueTrkTable(boolean trackValsFlag, int trkBuffSize)
  {
    trackingBufferSize = trackValsFlag ? trkBuffSize : 0;
  }

  /**
   * Creates a tag-value table, with optional historical tracking of
   * values.  The default number of values are tracked.
   * @param trackValsFlag true to track values, false to not track values
   * (operate like TagValueTable).
   */
  public TagValueTrkTable(boolean trackValsFlag)
  {
    this(trackValsFlag,DEF_TRKBUFF_SIZE);
  }

  /**
   * Creates a tag-value table, with historical tracking of values.  The
   * default number of values are tracked.
   */
  public TagValueTrkTable()
  {
    this(true,DEF_TRKBUFF_SIZE);
  }

  /**
   * Creates a new 'TrackingMutableLong' object.
   * @param value value for new 'TrackingMutableLong' object.
   * @return A new 'TrackingMutableLong' object.
   */
  public MutableLong createMutableLong(long value)
  {
    return new TrackingMutableLong(value);
  }

  /**
   * Determines if the given value was entered for the given tag string
   * via a previous call to the 'put()' method.
   * @param tagStr tag string to use.
   * @param value value to use.
   * @return true if the value was found in the tracking buffer; false
   * if the value was not found in the tracking buffer or the tracking
   * buffer is not setup.
   */
  public boolean isValueInTrkBuffer(String tagStr, long value)
  {
    final Object obj;
    return ((obj=hashTableObj.get(tagStr)) instanceof TrackingMutableLong) ?
               ((TrackingMutableLong)obj).isValueInTrkBuffer(value) : false;
  }

  /**
   * Determines if the given value was entered for the given tag string
   * via a previous call to the 'put()' method.
   * @param tagStr tag string to use.
   * @param valueObj value object to use.
   * @return true if the value was found in the tracking buffer; false
   * if the value was not found in the tracking buffer or the tracking
   * buffer is not setup.
   */
  public boolean isValueInTrkBuffer(String tagStr, Long valueObj)
  {
    final Object obj;
    return ((obj=hashTableObj.get(tagStr)) instanceof TrackingMutableLong) ?
            ((TrackingMutableLong)obj).isValueInTrkBuffer(valueObj) : false;
  }


  /**
   * Class TrackingMutableLong extends MutableLong to add historical
   * tracking of values.
   */
  public class TrackingMutableLong extends MutableLong
  {
    private final CircularBuffer circularBufferObj;   //tracking buffer

    /**
     * Creates a long integer value that may be modified.
     * @param value initial value.
     */
    public TrackingMutableLong(long value)
    {
      super(value);
      circularBufferObj = (trackingBufferSize > 0) ?
                              new CircularBuffer(trackingBufferSize) : null;
      circularBufferObj.add(Long.valueOf(value));    //enter into circular buffer
    }

    /**
     * Sets the current value.
     * @param value value to use.
     */
    public void setValue(long value)
    {
      super.setValue(value);                     //set new value
      if(circularBufferObj != null)              //if buffer setup then
        circularBufferObj.add(Long.valueOf(value));  //enter into circular buffer
    }

    /**
     * Determines if the given value was entered via a previous call to
     * the 'setValue()' method.
     * @param value value to use.
     * @return true if the value was found in the tracking buffer; false
     * if the value was not found in the tracking buffer or the tracking
     * buffer is not setup.
     */
    public boolean isValueInTrkBuffer(long value)
    {
      return (circularBufferObj != null) ?
                        circularBufferObj.contains(Long.valueOf(value)) : false;
    }

    /**
     * Determines if the given value was entered via a previous call to
     * the 'setValue()' method.
     * @param valueObj value object to use.
     * @return true if the value was found in the tracking buffer; false
     * if the value was not found in the tracking buffer or the tracking
     * buffer is not setup.
     */
    public boolean isValueInTrkBuffer(Long valueObj)
    {
      return (circularBufferObj != null) ?
                               circularBufferObj.contains(valueObj) : false;
    }
  }
}
