//ThreadLogger.java:  Defines a debugging utility that generates periodic
//                    log messages showing information about the current
//                    threads running in a program.
//
//  1/22/2003 -- [ET]  Initial version.
//  3/21/2003 -- [ET]  Added 'setLogLevelVal()' method; took out empty
//                     'main()' method.
//  5/21/2003 -- [ET]  Added 'setDaemon()' to thread constructor.
//   6/9/2003 -- [ET]  Changed default log-level value from 'INFO' to
//                     'NO_LEVEL'.
//  6/10/2003 -- [ET]  Added extra log-file parameters; added
//                     'setCloseOnTerminateFlag()' method.
// 10/03/2003 -- [KF]  Extend 'IstiThread' instead of 'Thread'.
//  6/24/2010 -- [ET]  Modified to use 'logObj.warning()' instead of
//                     'logObj.error()'; added 'closeOnTerminateFlag'
//                     check.
//

package com.isti.util;

/**
 * Class ThreadLogger defines a debugging utility that generates periodic
 * log messages showing information about the current threads running in
 * a program.  A new thread is created to generate the log messages.  The
 * default behavior is that log messages are only generated when a change
 * in the threads is detected.  This behavior may be changed via the
 * 'setOutputOnChangeFlag()' method.
 */
public class ThreadLogger
{
  protected final LogFile logObj;
  protected int intervalMSecs = 1000;
  protected StringBuffer stringBufferObj = new StringBuffer();
  protected boolean outputOnChangeFlag = true;
  protected ThreadGroup rootThreadGroupObj = null;
  protected int totalThreadCount = 0;
  protected String errorMessageString = null;
  protected LoggerRunningThread loggerRunningThreadObj = null;
  protected final static String newlineStr = UtilFns.newline;
  protected int logLevelVal = LogFile.NO_LEVEL;
  protected boolean closeOnTerminateFlag = false;

  /**
   * Creates a thread logger object and starts a new log-message-generation
   * thread.
   * @param logObj the LogFile object to use for output.
   * @param intervalMSecs the number of milliseconds between log outputs.
   */
  public ThreadLogger(LogFile logObj,int intervalMSecs)
  {
    if(logObj == null)
      throw new NullPointerException();
    this.logObj = logObj;
    startLoggerThread(intervalMSecs);
  }

  /**
   * Creates a thread logger object and starts a new log-message-generation
   * thread.
   * @param logFileName the name of the log file to be created and used
   * for output.
   * @param intervalMSecs the number of milliseconds between log outputs.
   */
  public ThreadLogger(String logFileName,int intervalMSecs)
  {
    if(logFileName == null)
      throw new NullPointerException();
              //setup to close log file when logger terminates:
    closeOnTerminateFlag = true;
    logObj = new LogFile(logFileName,LogFile.ALL_MSGS,LogFile.NO_MSGS);
    startLoggerThread(intervalMSecs);
  }

  /**
   * Creates a thread logger object and starts a new log-message-generation
   * thread.
   * @param logFileName the name of the log file to be created and used
   * for output.
   * @param intervalMSecs the number of milliseconds between log outputs.
   * @param useDateInFnameFlag true to use the date in the file name (and
   * create a new log file every time the date changes).
   */
  public ThreadLogger(String logFileName,int intervalMSecs,
                                                 boolean useDateInFnameFlag)
  {
    if(logFileName == null)
      throw new NullPointerException();
              //setup to close log file when logger terminates:
    closeOnTerminateFlag = true;
    logObj = new LogFile(logFileName,LogFile.ALL_MSGS,LogFile.NO_MSGS,
                               LogFile.DEFAULT_GMT_FLAG,useDateInFnameFlag);
    startLoggerThread(intervalMSecs);
  }

  /**
   * Creates a thread logger object and starts a new log-message-generation
   * thread.  The date is used in the file name and a new log file is
   * created every time the date changes).
   * @param logFileName the name of the log file to be created and used
   * for output.
   * @param intervalMSecs the number of milliseconds between log outputs.
   * @param maxLogFileAgeDays the maximum log file age, in days.
   */
  public ThreadLogger(String logFileName,int intervalMSecs,
                                                      int maxLogFileAgeDays)
  {
    if(logFileName == null)
      throw new NullPointerException();
              //setup to close log file when logger terminates:
    closeOnTerminateFlag = true;
    logObj = new LogFile(logFileName,LogFile.ALL_MSGS,LogFile.NO_MSGS,
                                             LogFile.DEFAULT_GMT_FLAG,true);
    logObj.setMaxLogFileAge(maxLogFileAgeDays);
    startLoggerThread(intervalMSecs);
  }

  /**
   * Creates a thread logger object and starts a new log-message-generation
   * thread.  The date is used in the file name and a new log file is
   * created every time the date changes).
   * @param logFileName the name of the log file to be created and used
   * for output.
   * @param intervalMSecs the number of milliseconds between log outputs.
   * @param maxLogFileAgeDays the maximum log file age, in days.
   * @param logFileSwitchIntervalDays the minimum number of days between
   * log file switches via date changes.
   */
  public ThreadLogger(String logFileName,int intervalMSecs,
                        int maxLogFileAgeDays,int logFileSwitchIntervalDays)
  {
    if(logFileName == null)
      throw new NullPointerException();
              //setup to close log file when logger terminates:
    closeOnTerminateFlag = true;
    logObj = new LogFile(logFileName,LogFile.ALL_MSGS,LogFile.NO_MSGS,
                                             LogFile.DEFAULT_GMT_FLAG,true);
    logObj.setMaxLogFileAge(maxLogFileAgeDays);
    logObj.setLogFileSwitchIntervalDays(logFileSwitchIntervalDays);
    startLoggerThread(intervalMSecs);
  }

  /**
   * Creates a thread logger object with no log output file.  In this
   * configuration, no log-message-generation thread is created, and
   * thread information may be requested on demand via the
   * 'getThreadsInfoStr()' methods.
   */
  public ThreadLogger()
  {
    this.logObj = new LogFile(null,LogFile.NO_MSGS,LogFile.NO_MSGS);
    if(!setupRootThreadGroupObj())     //setup 'rootThreadGroupObj' handle
      System.err.println(errorMessageString);    //if error then show msg
  }

  /**
   * Starts the thread-logging thread.
   * @param intervalMSecs the number of milliseconds between log outputs.
   */
  protected synchronized void startLoggerThread(int intervalMSecs)
  {
                             //don't allow interval to be less than 1:
    this.intervalMSecs = (intervalMSecs > 0) ? intervalMSecs : 1;
    if(!setupRootThreadGroupObj())     //setup 'rootThreadGroupObj' handle
    {    //error occurred
      logObj.warning(errorMessageString);   //log error message
      return;                          //exit without starting new thread
    }
    (loggerRunningThreadObj =          //start logger thread
                           new LoggerRunningThread("ThreadLogger")).start();
  }

  /**
   * Sets up the 'rootThreadGroupObj' handle.
   * @return true if successful, false if an error occurs (in which case
   * 'errorMessageString' will be set to the error message text).
   */
  protected synchronized boolean setupRootThreadGroupObj()
  {
    try
    {         //fetch root thread group object:
      if((rootThreadGroupObj=getRootThreadGroup()) == null)
      {       //method returned null
        errorMessageString = "Unable to fetch root thread group";
        return false;
      }
    }
    catch(Exception ex)
    {         //method threw exception
      errorMessageString = "Error fetching root thread group:  " + ex;
      return false;
    }
    return true;
  }

  /**
   * Sets the flag to enable console output of log messages.
   * @param flgVal true to enable console output, false to disable.
   */
  public synchronized void setConsoleOutputFlag(boolean flgVal)
  {
    logObj.setConsoleLevel(flgVal ? LogFile.ALL_MSGS : LogFile.NO_MSGS);
  }

  /**
   * Sets the flag that determines if threads information is logged
   * only when a change is detected or all the time regardless.
   * @param flgVal true to log only when a change is detected (the default),
   * false to send a log message at each interval regardless.
   */
  public synchronized void setOutputOnChangeFlag(boolean flgVal)
  {
    outputOnChangeFlag = flgVal;
  }

  /**
   * Sets the log-level value to be used.  The default value is
   * 'LogFile.NO_LEVEL'.
   * @param levelVal the log-level value to be used.
   */
  public synchronized void setLogLevelVal(int levelVal)
  {
    logLevelVal = levelVal;
  }

  /**
   * Sets whether or not the log file is closed when the logging thread
   * terminates.
   * @param flgVal true to close the log file on terminate, false to not.
   */
  public synchronized void setCloseLogOnTerminateFlag(boolean flgVal)
  {
    closeOnTerminateFlag = flgVal;
  }

  /**
   * Terminates the logging thread created by this ThreadLogger.
   */
  public synchronized void terminate()
  {
    if(loggerRunningThreadObj != null)
    {    //thread holder object is OK
      loggerRunningThreadObj.terminate();
      loggerRunningThreadObj = null;        //indicate thread stopped
    }
  }

  /**
   * Returns an information string for all of the current threads.
   * @param outChangeFlag if false then the information string will always
   * be returned; if true then the information string will only be
   * returned when a change in the threads is detected, otherwise
   * 'null' will be returned.  If an error occurs while fetching the
   * thread information then 'null' will be returned.  The error status
   * may be determined via the 'getErrorMessageString()' method.
   * @return An information string, or 'null'.
   */
  public synchronized String getThreadsInfoStr(boolean outChangeFlag)
  {
    final String lastDataStr = outChangeFlag ?
                                            stringBufferObj.toString() : "";
    final int len;
    if((len=stringBufferObj.length()) > 0)       //if string buffer has data
      stringBufferObj.delete(0,len);             // then clear it
    totalThreadCount = 0;    //clear thread count; will be set by 'fetch'
    if(!fetchThreadsInfo(rootThreadGroupObj,"")) //fetch threads information
      return null;           //if error then return null
                   //add line showing total number of threads
    stringBufferObj.append("Total number of threads = " +
                                             totalThreadCount + newlineStr);
    final String newDataStr = stringBufferObj.toString();
              //if flag not set or info changed then return string:
    if(!outChangeFlag || !newDataStr.equals(lastDataStr))
      return stringBufferObj.toString();
    return null;        //if flag set and info not changed then return null
  }

  /**
   * Returns an information string for all of the current threads.
   * If an error occurs while fetching the thread information then
   * 'null' will be returned.  The error status may be determined
   * via the 'getErrorMessageString()' method.
   * @return An information string, or 'null'.
   */
  public String getThreadsInfoStr()
  {
    return getThreadsInfoStr(false);
  }

  /**
   * Logs information for all of the current threads.
   * @return true if successful, false if error (in which case a error
   * message is logged).
   */
  public synchronized boolean logThreadsInfo()
  {
    final String infoStr;         //fetch threads information string:
    if((infoStr=getThreadsInfoStr(outputOnChangeFlag)) != null)
    {    //info string returned
      logObj.println(logLevelVal,infoStr);  //log information string
      return true;                //return OK flag
    }
    return (errorMessageString == null);    //return true if no error
  }

  /**
   * Enters information for all of the threads in the given ThreadGroup
   * and all of its subgroups into the StringBuffer object.
   * @param threadGroupObj the ThreadGroup to use.
   * @param indentStr the indentation string to place in front of each
   * line of log output.
   * @return true if successful, false if error (in which case a error
   * message is logged).
   */
  protected boolean fetchThreadsInfo(ThreadGroup threadGroupObj,
                                                           String indentStr)
  {
    try
    {
      int i,numGroups;
      final ThreadGroup[] groupArr;
      synchronized(threadGroupObj)
      {                                          //show group info:
        stringBufferObj.append(indentStr +
                        getThreadGroupInfoStr(threadGroupObj) + newlineStr);
        indentStr += "  ";                       //increase indent
                        //get maximum possible # of threads for array size:
        int numThreads = threadGroupObj.activeCount();
        if(totalThreadCount <= 0)           //if not previously set then
          totalThreadCount = numThreads;    //set total thread count
        final Thread [] threadArr = new Thread[numThreads];   //create array
                        //load array of Thread objects (in current group
                        // only, not subgroups) and save actual count:
        numThreads = threadGroupObj.enumerate(threadArr,false);
        for(i=0; i<numThreads; ++i)
        {     //show info for each thread
          stringBufferObj.append(indentStr +
                               getThreadInfoStr(threadArr[i]) + newlineStr);
        }
                        //get maximum possible # of groups for array size:
        numGroups = threadGroupObj.activeGroupCount();
        groupArr = new ThreadGroup[numGroups];        //create array
                        //load array of ThreadGroup objects (only direct
                        // decendants) and save actual count:
        numGroups = threadGroupObj.enumerate(groupArr,false);
      }
      for(i=0; i<numGroups; ++i)
      {  //recusively log info for each group
        if(!fetchThreadsInfo(groupArr[i],indentStr))
          return false;           //if error then return false
      }
      return true;                //return OK flag
    }
    catch(Exception ex)
    {         //some kind of exception error occurred; log it
      logObj.warning(errorMessageString=
                                "Error logging thread information:  " + ex);
      return false;               //return error flag
    }
  }

  /**
   * Returns the current error message (if any).
   * @return The current error message, or null if no errors have occurred.
   */
  public synchronized String getErrorMessageString()
  {
    return errorMessageString;
  }

  /**
   * Clears any current error message.
   */
  public synchronized void clearErrorMessageString()
  {
    errorMessageString = null;
  }

  /**
   * Returns the root ThreadGroup object.
   * @return The root ThreadGroup object.
   */
  public static ThreadGroup getRootThreadGroup()
  {
    ThreadGroup threadGroupObj = Thread.currentThread().getThreadGroup();
    ThreadGroup gObj;   //get thread group parent until top-most found:
    while((gObj=threadGroupObj.getParent()) != null)
      threadGroupObj = gObj;
    return threadGroupObj;
  }

  /**
   * Returns an informational string about the given thread.
   * @param threadObj the thread to use.
   * @return An informational string.
   */
  public static String getThreadInfoStr(Thread threadObj)
  {
    return "Thread[\"" + threadObj.getName() + "\", priority=" +
              threadObj.getPriority() + ", daemon=" + threadObj.isDaemon() +
                         ", interrupted=" + threadObj.isInterrupted() + "]";

//    return "Thread[\"" + threadObj.getName() + "\", priority=" +
//                                    threadObj.getPriority() + ", group=\"" +
//                       threadObj.getThreadGroup().getName() + "\", alive=" +
//                  threadObj.isAlive() + ", daemon=" + threadObj.isDaemon() +
//                         ", interrupted=" + threadObj.isInterrupted() + "]";
  }

  /**
   * Returns an informational string about the given thread group.
   * @param threadGroupObj the thread group to use.
   * @return An informational string.
   */
  public static String getThreadGroupInfoStr(ThreadGroup threadGroupObj)
  {
    return "ThreadGroup[\"" + threadGroupObj.getName() + "\", numthreads=" +
                           threadGroupObj.activeCount() + ", maxpriority=" +
                             threadGroupObj.getMaxPriority() + ", daemon=" +
                                            threadGroupObj.isDaemon() + "]";

//    return "ThreadGroup[\"" + threadGroupObj.getName() + "\", numthreads=" +
//                           threadGroupObj.activeCount() + ", maxpriority=" +
//                             threadGroupObj.getMaxPriority() + ", daemon=" +
//                                threadGroupObj.isDaemon() + ", destroyed=" +
//                                         threadGroupObj.isDestroyed() + "]";
  }

  /**
   * Entry point for test program.  Runs for five seconds, sending results
   * to "ThreadLoggerTest.log" and console.
   * @param args string array of command-line arguments.
   */
//  public static void main(String[] args)
//  {
//    Thread.currentThread().setName("ThreadLoggerTestMain");
//
//standard logging of threads info method:
//    final ThreadLogger threadLoggerObj =
//                              new ThreadLogger("ThreadLoggerTest.log",1000);
//    threadLoggerObj.setOutputOnChangeFlag(false);
//    threadLoggerObj.setConsoleOutputFlag(true);
//    try { Thread.sleep(5000); }
//    catch(InterruptedException ex) {}
//    threadLoggerObj.terminate();
//
//threads info on-demand method:
//    final ThreadLogger threadLoggerObj = new ThreadLogger();
//    String infoStr;
//    for(int c=0; c<5; ++c)
//    {
//      try { Thread.sleep(1000); }
//      catch(InterruptedException ex) {}
//      if((infoStr=threadLoggerObj.getThreadsInfoStr()) != null)
//        System.out.println(infoStr);
//      else
//        System.err.println(threadLoggerObj.getErrorMessageString());
//    }
//  }

  /**
   * Class LoggerRunningThread defines a separate thread used to build and
   * output the log file entries.
   */
  protected class LoggerRunningThread extends IstiThread
  {
    /**
     * Creates a logger thread.
     * @param threadName the name for this thread.
     */
    public LoggerRunningThread(String threadName)
    {
      super(threadName);
      setDaemon(true);       //make this a "daemon" (background) thread
    }

    /**
     * Creates a logger thread.  A default name is given to the thread.
     */
    public LoggerRunningThread()
    {
      this("ThreadLoggerThread-" + nextThreadNum());
    }

    /**
     * Executing method for logger thread.
     */
    public void run()
    {
      while(!isTerminated())
      {       //loop until terminate flag set
        try
        {     //delay between log outputs
          sleep(intervalMSecs);
        }
        catch(InterruptedException ex)
        {     //sleep delay interrupted
          if(isTerminated())           //if terminate flag set then
            break;                     //exit loop
        }
        if(!logThreadsInfo())
        {     //error logging threads info (error message already logged)
          logObj.warning("Aborting ThreadLogger");    //log abort message
          if(closeOnTerminateFlag)
            logObj.close();                 //if flag then close log file
          return;                                     //abort thread
        }
      }
      logObj.println(logLevelVal,"ThreadLogger terminated OK");
      if(closeOnTerminateFlag)
        logObj.close();           //if flag then close log file
    }
  }
}
