//TimeConstants:  Defines simple time constants.
//
//=====================================================================
// Copyright (C) 2012 Instrumental Software Technologies, Inc.
//
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions
// are met:
// 1. Redistributions of source code, or portions of this source code,
//    must retain the above copyright notice, this list of conditions
//    and the following disclaimer.
// 2. Redistributions in binary form must reproduce the above copyright
//    notice, this list of conditions and the following disclaimer in
//    the documentation and/or other materials provided with the
//    distribution.
// 3. All advertising materials mentioning features or use of this
//    software must display the following acknowledgment:
//    "This product includes software developed by Instrumental
//    Software Technologies, Inc. (http://www.isti.com)"
// 4. If the software is provided with, or as part of a commercial
//    product, or is used in other commercial software products the
//    customer must be informed that "This product includes software
//    developed by Instrumental Software Technologies, Inc.
//    (http://www.isti.com)"
// 5. The names "Instrumental Software Technologies, Inc." and "ISTI"
//    must not be used to endorse or promote products derived from
//    this software without prior written permission. For written
//    permission, please contact "info@isti.com".
// 6. Products derived from this software may not be called "ISTI"
//    nor may "ISTI" appear in their names without prior written
//    permission of Instrumental Software Technologies, Inc.
// 7. Redistributions of any form whatsoever must retain the following
//    acknowledgment:
//    "This product includes software developed by Instrumental
//    Software Technologies, Inc. (http://www.isti.com/)."
// THIS SOFTWARE IS PROVIDED BY INSTRUMENTAL SOFTWARE
// TECHNOLOGIES, INC. "AS IS" AND ANY EXPRESSED OR IMPLIED
// WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES
// OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
// DISCLAIMED.  IN NO EVENT SHALL INSTRUMENTAL SOFTWARE TECHNOLOGIES,
// INC. OR ITS CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
// INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
// (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
// SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
// HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
// STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
// ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED
// OF THE POSSIBILITY OF SUCH DAMAGE.
//=====================================================================
//  A current version of the software can be found at
//                http://www.isti.com
//  Bug reports and comments should be directed to
//  Instrumental Software Technologies, Inc. at info@isti.com
//=====================================================================
//

package com.isti.util;

/**
 * Interface TimeConstants defines simple time constants.<br>
 * All of these constants are simple as they have no external dependencies.
 */
public interface TimeConstants {
  /** Number of milliseconds in a second (1000). */
  public static final long MS_PER_SECOND = 1000L;

  /** Number of seconds in a minute (60). */
  public static final long SECONDS_PER_MINUTE = 60L;

  /** Number of milliseconds in an minute (60 * 1000). */
  public static final long MS_PER_MINUTE = SECONDS_PER_MINUTE * MS_PER_SECOND;

  /** Number of minutes in an hour (60). */
  public static final long MINUTES_PER_HOUR = 60L;

  /** Number of milliseconds in an hour (60 * 60 * 1000). */
  public static final long MS_PER_HOUR = MS_PER_MINUTE * MINUTES_PER_HOUR;

  /** Number of hours in a day (24). */
  public static final long HOURS_PER_DAY = 24L;

  /** Number of minutes in a day (24 * 60). */
  public static final long MINUTES_PER_DAY = HOURS_PER_DAY * MINUTES_PER_HOUR;

  /** Number of seconds in a day (24 * 60 * 60). */
  public static final long SECONDS_PER_DAY = HOURS_PER_DAY * MINUTES_PER_HOUR
      * SECONDS_PER_MINUTE;

  /** Number of milliseconds in an hour (24 * 60 * 60 * 1000). */
  public static final long MS_PER_DAY = MS_PER_HOUR * HOURS_PER_DAY;
}
