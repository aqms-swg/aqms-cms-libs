//TimeEventQueue.java:  Defines a queue of time-event values.
//
//   6/7/2006 -- [ET]
//

package com.isti.util;

import java.util.Vector;

/**
 * Class TimeEventQueue defines a queue of time-event values.
 */
public class TimeEventQueue
{
  protected final Vector queueVectorObj = new Vector();

  /**
   * Creates a queue of time-event values.
   */
  public TimeEventQueue()
  {
  }

  /**
   * Adds a time-event value to the end of the queue.
   * @param timeVal time-event value.
   */
  public void addEvent(long timeVal)
  {
    queueVectorObj.add(Long.valueOf(timeVal));
  }

  /**
   * Determines if the given current-time value is at or after the next
   * time-event value in the queue (and removes it if so).  Multiple
   * time-event values may be removed from the queue if the current-time
   * value is at or after them.
   * @param currentTimeVal current-time value to use.
   * @return true if the given current-time value is at or after the next
   * time-event value in the queue; false if not.
   */
  public boolean checkPullNextEvent(long currentTimeVal)
  {
    boolean retFlag = false;
    synchronized(queueVectorObj)
    {    //make sure Vector unchanged between accesses
      Object obj;
      while(queueVectorObj.size() > 0 &&
                              (obj=queueVectorObj.get(0)) instanceof Long &&
                                  currentTimeVal >= ((Long)obj).longValue())
      {  //while Vector not empty and given time is >= next value
        queueVectorObj.remove(0);           //remove time value
        retFlag = true;                     //indicate time event reached
      }
      return retFlag;
    }
  }

  /**
   * Determines if the current system time is at or after the next
   * time-event value in the queue (and removes it if so).  Multiple
   * time-event values may be removed from the queue if the current system
   * time value is at or after them.
   * @return true if the current system time is at or after the next
   * time-event value in the queue; false if not.
   */
  public boolean checkPullNextEvent()
  {
    return checkPullNextEvent(System.currentTimeMillis());
  }

  /**
   * Returns the next time-event value from the queue.  The queue
   * is left unchanged.
   * @return The next time-event value from the queue, or 0 if none
   * available.
   */
  public long peekNextEvent()
  {
    synchronized(queueVectorObj)
    {    //make sure Vector unchanged between accesses
      final Object obj;
      return (queueVectorObj.size() > 0 &&
                              (obj=queueVectorObj.get(0)) instanceof Long) ?
                                                ((Long)obj).longValue() : 0;
    }
  }

  /**
   * Removes and returns the next time-event value from the queue.
   * @return The next time-event value from the queue, or 0 if none
   * available.
   */
  public long pullNextEvent()
  {
    synchronized(queueVectorObj)
    {    //make sure Vector unchanged between accesses
      final Object obj;
      return (queueVectorObj.size() > 0 &&
                           (obj=queueVectorObj.remove(0)) instanceof Long) ?
                                                ((Long)obj).longValue() : 0;
    }
  }

  /**
   * Returns the current size of the time-event queue.
   * @return The current size of the time-event queue.
   */
  public int getEventQueueSize()
  {
    return queueVectorObj.size();
  }
}
