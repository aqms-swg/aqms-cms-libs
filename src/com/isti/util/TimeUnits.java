package com.isti.util;

/**
 * Time Units (millisecond, second, minute, hour, day)
 */
public class TimeUnits implements TimeConstants {
  /** The text. */
  private final String text;

  /** The number of milliseconds per unit. */
  private final long msPerUnit;

  /** Millisecond */
  public static final TimeUnits MILLISECOND = new TimeUnits("millisecond", 1);

  /** Second */
  public static final TimeUnits SECOND = new TimeUnits("second", MS_PER_SECOND);

  /** Minute */
  public static final TimeUnits MINUTE = new TimeUnits("minute", MS_PER_MINUTE);

  /** Hour */
  public static final TimeUnits HOUR = new TimeUnits("hour", MS_PER_HOUR);

  /** Day */
  public static final TimeUnits DAY = new TimeUnits("day", MS_PER_DAY);

  /** Time units array */
  public static final TimeUnits[] TIME_UNITS = { MILLISECOND, SECOND, MINUTE,
      HOUR, DAY };

  /**
   * Find the time units for the specified units text.
   * @param unitsText the units text.
   * @throws IllegalArgumentException if the time unites could not be found.
   */
  public TimeUnits(String unitsText) {
    TimeUnits timeUnits = parseTimeUnits(unitsText, null);
    if (timeUnits == null) {
      throw new IllegalArgumentException("invalid units text: " + unitsText);
    }
    this.text = timeUnits.text;
    this.msPerUnit = timeUnits.msPerUnit;
  }

  /**
   * Create the time units.
   * @param text the units text (singular.)
   * @param msPerUnit the number of milliseconds per unit.
   */
  protected TimeUnits(String text, long msPerUnit) {
    this.text = text.toLowerCase();
    this.msPerUnit = msPerUnit;
  }

  /**
   * Get the number of milliseconds per unit.
   * @return the number of milliseconds per unit.
   */
  public long getMsPerUnit() {
    return msPerUnit;
  }

  /**
   * Get the text for the value and time units.
   * @param value the number of time units.
   * @return the text for the value and time units.
   */
  public String getTextWithValue(long value) {
    return getTextWithValue(value, this);
  }

  /**
   * Get the text for the value and time units.
   * @param valueText the text for the number of time units.
   * @return the text for the value and time units.
   */
  public String getTextWithValue(String valueText) {
    long value = 0L;
    try {
      value = Long.parseLong(valueText);
    } catch (NumberFormatException ex) {
    }
    return getTextWithValue(value);
  }

  /**
   * The singular text for the time units.
   * @return the text for the time units.
   */
  public String toString() {
    return toString(false);
  }

  /**
   * The text for the time units.
   * @param pluralFlag true for plural, false for singular.
   * @return the text for the time units.
   */
  public String toString(boolean pluralFlag) {
    if (pluralFlag) {
      return text + 's';
    }
    return text;
  }

  /**
   * The text for the time units. If the value is 1 the singular text is used.
   * @param value the number of time units.
   * @return the text for the time units.
   */
  public String toString(long value) {
    return toString(value != 1);
  }

  /**
   * Get the number of milliseconds represented by the text and may include text
   * for the time units.
   * @param s the text.
   * @param timeUnits the time units to use if not specified in the text.
   * @return the number of milliseconds or 0 if the value could not be
   *         determined.
   */
  public static long getMilliseconds(String s, TimeUnits timeUnits) {
    long value = 0L;
    if (s != null && s.length() > 0) {
      String valueText = s;
      String unitsText = null;
      int index = s.lastIndexOf(' ');
      if (index >= 0) {
        valueText = s.substring(0, index).trim();
        unitsText = s.substring(index + 1);
        timeUnits = parseTimeUnits(unitsText, timeUnits);
      }
      try {
        value = Long.parseLong(valueText);
      } catch (NumberFormatException ex) {
      }
      if (timeUnits != null) {
        value *= timeUnits.getMsPerUnit();
      }
    }
    return value;
  }

  /**
   * Gets the text value with time units.
   * @param s the text which may include text for the time units.
   * @param defaultValue the time units to add if not already specified.
   * @param minimumValue the minimum value.
   * @return the text value with time units or null if the value could not be
   *         determined or is not greater than or equal to the minimum value.
   */
  public static String getTextValueWithTimeUnits(String s,
      TimeUnits defaultValue, long minimumValue) {
    if (s != null && s.length() > 0) {
      TimeUnits timeUnits = null;
      String valueText = s;
      String unitsText = null;
      int index = s.lastIndexOf(' ');
      if (index >= 0) {
        valueText = s.substring(0, index).trim();
        unitsText = s.substring(index + 1);
        timeUnits = parseTimeUnits(unitsText, timeUnits);
      }
      try {
        long value = Long.parseLong(valueText);
        if (value >= minimumValue) {
          if (timeUnits == null) {
            timeUnits = defaultValue;
          }
          return getTextWithValue(value, timeUnits);
        }
      } catch (NumberFormatException ex) {
      }
    }
    return null;
  }

  /**
   * Get the text for the value and time units text.
   * @param value the number of time units.
   * @param unitsText the time units text.
   * @return the text.
   */
  protected static String getTextWithValue(long value, String unitsText) {
    return getTextWithValue(Long.toString(value), unitsText);
  }

  /**
   * Get the text for the value and time units text.
   * @param value the number of time units.
   * @param unitsText the time units text.
   * @return the text.
   */
  protected static String getTextWithValue(long value, TimeUnits timeUnits) {
    return getTextWithValue(value, timeUnits.toString(value));
  }

  /**
   * Get the text for the value and time units text.
   * @param valueText the text for the number of time units.
   * @param unitsText the time units text.
   * @return the text.
   */
  protected static String getTextWithValue(String valueText, String unitsText) {
    return valueText + " " + unitsText;
  }

  /**
   * Parse the units text.
   * @param unitsText the units text.
   * @param defaultValue the default time units.
   * @return the time units from the specified units text or the default value.
   */
  protected static TimeUnits parseTimeUnits(String unitsText,
      TimeUnits defaultValue) {
    if (unitsText != null) {
      unitsText = unitsText.toLowerCase();
      for (int i = 0; i < TIME_UNITS.length; i++) {
        if (unitsText.startsWith(TIME_UNITS[i].text)) {
          return TIME_UNITS[i];
        }
      }
    }
    return defaultValue;
  }

  /**
   * Gets the time units for the specified text.
   * @param s the text or null if none.
   * @return the time units or null if none.
   */
  public static TimeUnits valueOf(String s) {
    return valueOf(s, null);
  }

  /**
   * Gets the time units for the specified text. If the text contains any space
   * the text after the space is used.
   * @param s the text or null if none.
   * @param defaultValue the default value if no time units found.
   * @return the time units or the default value if none.
   */
  public static TimeUnits valueOf(String s, TimeUnits defaultValue) {
    if (s != null) {
      int index = s.lastIndexOf(' ');
      if (index >= 0) {
        s = s.substring(index + 1);
      }
    }
    return parseTimeUnits(s, defaultValue);
  }
}
