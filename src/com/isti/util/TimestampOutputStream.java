//TimestampOutputStream.java:  Output stream filter that adds a timestamp
//                             of the current time in front of each line.
//
//  1/15/2003 -- [ET]
//

package com.isti.util;

import java.io.FilterOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.TimeZone;


/**
 * Class TimestampOutputStream is an output stream filter that adds a
 * timestamp of the current time in front of each line.
 */
public class TimestampOutputStream extends FilterOutputStream
{
  protected static final char NEWLINE_CHAR = '\n';
  protected boolean newlineFlag = true;     //true after newline char
                                  //formatter for output date/time:
  protected final DateFormat dateFormatObj =
                                           DateFormat.getDateTimeInstance();

    /**
     * Creates a timestamp output stream.
     * @param streamObj the output stream to use.
     * @param dateFormatStr the date/time format to use, or null to use
     * the default "MMM dd yyyy HH:mm:ss" format.
     */
  public TimestampOutputStream(OutputStream streamObj,String dateFormatStr)
  {
    super(streamObj);
    if(dateFormatStr != null && dateFormatStr.trim().length() > 0)
      setDateFormatPattern(dateFormatStr);  //enter given format pattern
    else                                    //enter default format pattern:
      setDateFormatPattern("MMM dd yyyy HH:mm:ss");
  }

    /**
     * Creates a timestamp output stream.  Uses the default
     * "MMM dd yyyy HH:mm:ss" format.
     * @param streamObj the output stream to use.
     */
  public TimestampOutputStream(OutputStream streamObj)
  {
    this(streamObj,null);
  }

    /**
     * Sets the pattern string used to generate the timestamps.  See the
     * Java help reference for 'SimpleDateFormat' for details on the
     * characters used in the pattern string.
     * @param patternStr the pattern string to apply.
     */
  public void setDateFormatPattern(String patternStr)
  {
    try       //trap any exceptions that might occur
    {              //if object is 'SimpleDateFormat' type and pattern
                   // string is not null then apply it:
      if(dateFormatObj instanceof SimpleDateFormat && patternStr != null)
        ((SimpleDateFormat)dateFormatObj).applyPattern(patternStr);
    }
    catch(Exception ex) {}        //ignore any exceptions
  }

    /**
     * Sets the time zone used for the timestamps.
     * @param timeZoneObj the time zone to use.
     */
  public void setDateFormatTimeZone(TimeZone timeZoneObj)
  {
    dateFormatObj.setTimeZone(timeZoneObj);
  }

    /**
     * Writes a character.
     * @param ch character
     * @throws IOException
     */
  public void write(int ch) throws IOException
  {
    if(newlineFlag)               //if last char was newline then
      writeTimestamp();           //send out timestamp
    out.write(ch);                //send character
    newlineFlag = (ch == NEWLINE_CHAR);     //if newline then set flag
  }

    /**
     * Writes the given data array.
     * @param data data array
     * @throws IOException
     */
  public void write(byte[] data) throws IOException
  {
    write(data,0,data.length);
  }

    /**
     * Writes the given section of the given array.
     * @param data data array
     * @param start start index
     * @param len length
     * @throws IOException
     */
  public void write(byte[] data,int start,int len) throws IOException
  {
    int p = start, sPos = start;
    while(p < len)
    {    //for each line of character
      if(newlineFlag)
      {  //last char was newline
        writeTimestamp();              //send out timestamp
        newlineFlag = false;           //reset newline flag
      }
      while(p < len)
      {  //for each character in line
        if(data[p++] == NEWLINE_CHAR)       //check & increment
        {     //newline character found
          out.write(data,sPos,p-sPos);      //write line of data
          sPos = p;                         //set new start point
          newlineFlag = true;               //set newline flag
          break;                            //exit inner loop
        }
      }
    }
    if(p > sPos)                       //if data remaining then
      out.write(data,sPos,p-sPos);     //write data
  }

    /**
     * Writes a timestamp and ":  " separator to the output stream.
     * @throws IOException if an I/O error occurs.
     */
  public void writeTimestamp() throws IOException
  {
    out.write((dateFormatObj.format(new Date()) + ":  ").getBytes());
  }
}
