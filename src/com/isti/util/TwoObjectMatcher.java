//TwoObjectMatcher.java:  Manages a set of object pairs and provides
//                        a fast comparison match vs. a given pair.
//
//   1/5/2004 -- [ET]  Initial version.
//  7/16/2008 -- [ET]  Added 'remove()', 'clear()' and 'isEmpty()' methods.
//

package com.isti.util;

import java.util.HashMap;
import java.util.HashSet;

/**
 * Class TwoObjectMatcher manages a set of object pairs and provides
 * a fast comparison match vs. a given pair.
 */
public class TwoObjectMatcher
{
  private final HashMap hashMapObj = new HashMap();

  /**
   * Adds the given pair of objects to this matcher.
   * @param obj1 the first object to use.
   * @param obj2 the second object to use.
   */
  public void add(Object obj1, Object obj2)
  {
    final Object obj;
    if((obj=hashMapObj.get(obj1)) instanceof HashSet)
      ((HashSet)obj).add(obj2);   //add 'obj2' to hash-set for 'obj1'
    else
    {    //no hash-set for 'obj1'; create one
      final HashSet setObj = new HashSet();
      setObj.add(obj2);                //add 'obj2' to hash-set
      hashMapObj.put(obj1,setObj);     //add hash-set for 'obj1' to table
    }
  }

  /**
   * Removes the given pair of objects from this matcher.
   * @param obj1 the first object of the pair to be removed.
   * @param obj2 the second object of the pair to be removed.
   * @return true if the given pair of objects was held in this matcher
   * and was removed; false if the given pair of objects was not held
   * in this matcher.
   */
  public boolean remove(Object obj1, Object obj2)
  {
    final Object obj;
    if((obj=hashMapObj.get(obj1)) instanceof HashSet)
    {  //hash-set for 'obj1' exists
      final HashSet hSetObj = (HashSet)obj;
                        //remove 'obj2' from hash-set; save result flag:
      final boolean retFlag = hSetObj.remove(obj2);
      if(hSetObj.isEmpty())            //if 'obj1' hash-set now empty then
        hashMapObj.remove(obj1);       //remove 'obj1' from main hash-map
      return retFlag;                  //return result flag
    }
    return false;       //no match for 'obj1'; return indicator flag
  }

  /**
   * Removes all entries from this matcher.
   */
  public void clear()
  {
    hashMapObj.clear();
  }

  /**
   * Checks if the given pair objects is held by this matcher.
   * @param obj1 the first object to use.
   * @param obj2 the second object to use.
   * @return true if the given pair objects is held by this matcher,
   * false if not.
   */
  public boolean contains(Object obj1, Object obj2)
  {
    final Object obj;
    return (((obj=hashMapObj.get(obj1)) instanceof HashSet) &&
                                             ((HashSet)obj).contains(obj2));
  }

  /**
   * Determines if this matcher is empty.
   * @return true if this matcher is empty; false if this matcher
   * contains any objects.
   */
  public boolean isEmpty()
  {
    return hashMapObj.isEmpty();
  }
}
