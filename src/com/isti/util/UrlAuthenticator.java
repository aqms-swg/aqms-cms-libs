package com.isti.util;

import java.net.Authenticator;
import java.net.PasswordAuthentication;

/**
 * URL Authenticator
 */
public class UrlAuthenticator extends Authenticator implements
    PasswordAuthenticator {
  /** The password authenticator */
  private final PasswordAuthenticator passwordAuthenticator;

  /**
   * Create the URL authenticator.
   * @param passwordAuthenticator the password authenticator.
   */
  public UrlAuthenticator(PasswordAuthenticator passwordAuthenticator) {
    this.passwordAuthenticator = passwordAuthenticator;
  }

  /**
   * Called when when a password-protected URL is accessed.
   * @return the password authentication or null if none.
   */
  public PasswordAuthentication getPasswordAuthentication() {
    return passwordAuthenticator.getPasswordAuthentication();
  }

  /**
   * Set the default authenticator.
   * @param authenticator the authenticator.
   */
  public static void setDefaultAuthenticator(Authenticator authenticator) {
    Authenticator.setDefault(authenticator);
  }
}
