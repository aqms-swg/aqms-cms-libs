//UtilFns.java:  Contains various static utility functions
//
//=====================================================================
// Copyright (C) 2022 Instrumental Software Technologies, Inc.
//
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions
// are met:
// 1. Redistributions of source code, or portions of this source code,
//    must retain the above copyright notice, this list of conditions
//    and the following disclaimer.
// 2. Redistributions in binary form must reproduce the above copyright
//    notice, this list of conditions and the following disclaimer in
//    the documentation and/or other materials provided with the
//    distribution.
// 3. All advertising materials mentioning features or use of this
//    software must display the following acknowledgment:
//    "This product includes software developed by Instrumental
//    Software Technologies, Inc. (http://www.isti.com)"
// 4. If the software is provided with, or as part of a commercial
//    product, or is used in other commercial software products the
//    customer must be informed that "This product includes software
//    developed by Instrumental Software Technologies, Inc.
//    (http://www.isti.com)"
// 5. The names "Instrumental Software Technologies, Inc." and "ISTI"
//    must not be used to endorse or promote products derived from
//    this software without prior written permission. For written
//    permission, please contact "info@isti.com".
// 6. Products derived from this software may not be called "ISTI"
//    nor may "ISTI" appear in their names without prior written
//    permission of Instrumental Software Technologies, Inc.
// 7. Redistributions of any form whatsoever must retain the following
//    acknowledgment:
//    "This product includes software developed by Instrumental
//    Software Technologies, Inc. (http://www.isti.com/)."
// THIS SOFTWARE IS PROVIDED BY INSTRUMENTAL SOFTWARE
// TECHNOLOGIES, INC. "AS IS" AND ANY EXPRESSED OR IMPLIED
// WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES
// OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
// DISCLAIMED.  IN NO EVENT SHALL INSTRUMENTAL SOFTWARE TECHNOLOGIES,
// INC. OR ITS CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
// INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
// (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
// SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
// HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
// STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
// ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED
// OF THE POSSIBILITY OF SUCH DAMAGE.
//=====================================================================
//  A current version of the software can be found at
//                http://www.isti.com
//  Bug reports and comments should be directed to
//  Instrumental Software Technologies, Inc. at info@isti.com
//=====================================================================
//

package com.isti.util;

import java.awt.Color;
import java.awt.Toolkit;
import java.awt.datatransfer.Clipboard;
import java.awt.datatransfer.DataFlavor;
import java.awt.datatransfer.StringSelection;
import java.awt.datatransfer.Transferable;
import java.io.BufferedReader;
import java.io.ByteArrayOutputStream;
import java.io.Closeable;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.io.Reader;
import java.io.StringWriter;
import java.lang.reflect.Constructor;
import java.lang.reflect.Method;
import java.net.Inet4Address;
import java.net.InetAddress;
import java.net.MalformedURLException;
import java.net.URL;
import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;
import java.text.DateFormat;
import java.text.DateFormatSymbols;
import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.text.NumberFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Collection;
import java.util.Date;
import java.util.Enumeration;
import java.util.Hashtable;
import java.util.Iterator;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Properties;
import java.util.TimeZone;
import java.util.Timer;
import java.util.Vector;

/**
 * Class UtilFns contains various static utility methods.
 */
public class UtilFns implements OsConst, StringConstants, TimeConstants
{
  /**
   * Public IP Hosts
   * @see #getPublicIP()
   */
  public static String[] PUBLIC_IP_HOSTS =
    {
        "http://checkip.amazonaws.com", "http://myexternalip.com/raw",
        "https://ipv4.icanhazip.com"
    }; 
  
    /** TimeZone object setup for "GMT". */
  public static final TimeZone GMT_TIME_ZONE_OBJ =
                                                TimeZone.getTimeZone("GMT");
  /** UTF-8  */
  public static final Charset UTF_8 = StandardCharsets.UTF_8;

  /** ISO Latin Alphabet No. 1, a.k.a. ISO-LATIN-1 */
  public static final Charset ISO_8859_1 = StandardCharsets.ISO_8859_1;

  // color property string length:
  public static final int colorPropertyStringLength = 8;

                                            //'DataChangedListener' objects:
  private static final Vector listenersVec = new Vector();
  /**
   * The system locale.
   */
  private static final Locale systemLocaleObj = Locale.getDefault();
  /**
   * The default locale sync object.
   */
  private static final Object defaultLocaleSyncObj = new Object();
  /**
   * The default locale.
   */
  private static Locale defaultLocaleObj = systemLocaleObj;
  /**
   * The decimal separator character.
   */
  private static char decimalSeparator =
      new DecimalFormatSymbols(defaultLocaleObj).getDecimalSeparator();

         //date formatter objects, set up below in static initializer:
  private static final DateFormat dateFormatter =     //date formatter:
                                     createDateFormatObj(DATE_FORMAT24_STR);
  private static final DateFormat dateFormatterGMT =  //GMT date formatter:
                   createDateFormatObj(DATE_FORMAT24_STR,GMT_TIME_ZONE_OBJ);
  private static final DateFormat dateFormatterAmPm =      //AM/PM version
                                  createDateFormatObj(DATE_FORMAT_AMPM_STR);
  private static final DateFormat dateFormatterGMTAmPm =   //AM/PM GMT ver
                createDateFormatObj(DATE_FORMAT_AMPM_STR,GMT_TIME_ZONE_OBJ);
  /** RFC-822 DateFormat object. */
  private static final DateFormat rfc_822DateFormatObj =
      createDateFormatObj(DATE_FORMAT_RFC_822_STR,GMT_TIME_ZONE_OBJ);
         //runtime object for system:
  private static final Runtime systemRuntimeObj = Runtime.getRuntime();
         //number formatter for floating point values:
  private static final NumberFormat floatNumberFormatObj =
      createFloatNumberFormat("#0.0#");
         //number formatter for variable floating point values:
  private static final NumberFormat variableNumberFormatObj =
      createFloatNumberFormat("#0.0#");
  private static final int variableNumberFormatMaximumIntegerDigits =
      variableNumberFormatObj.getMaximumIntegerDigits();

         //time zone IDs:
  private static String [] timeZoneIDs = TimeZone.getAvailableIDs();
  static
  {
    Arrays.sort(timeZoneIDs);  //sort the time zone IDs
  }
         //Time zone validator object:
  public final static CfgPropValidator timeZoneValidator =
                                          new CfgPropValidator(timeZoneIDs);

  static
  {
    floatNumberFormatObj.setMaximumFractionDigits(8);
  }

         /**
          * A static string containing the proper system-dependent value
          * for the "newline" character sequence.  This string may be
          * embedded into other strings that are then displayed or written
          * to files.  Its value is set by a static initializer.
          */
  public static final String newline;
  static      //static initialization block; executes only once
  {                //create output stream which writes to byte array:
    ByteArrayOutputStream btOutStream = new ByteArrayOutputStream();
    PrintWriter out = new PrintWriter(btOutStream);
    out.println();           //enter newline character
    out.flush();             //flush data out to byte array
    newline = btOutStream.toString();  //save string version of data
    out.close();             //close stream
  }
         //string used to generate substrings of space characters:
  private static String blanksString = "                              " +
                       "                                                  ";
         //variable to track current length of 'blanksString':
  private static int blanksStringLen = blanksString.length();
         //thread synchronization object for use with blanks string:
  private static final Object blanksStringSyncObj = new Object();
         //handle for root thread group object:
  private static ThreadGroup rootThreadGroupObj = null;

  /**
   * Array of Color Hash Table RGB Values.
   */
  private static Integer COLOR_HASH_VALUES[] =
  {
    Integer.valueOf(Color.white.getRGB()),
    Integer.valueOf(Color.lightGray.getRGB()),
    Integer.valueOf(Color.gray.getRGB()),
    Integer.valueOf(Color.darkGray.getRGB()),
    Integer.valueOf(Color.black.getRGB()),
    Integer.valueOf(Color.red.getRGB()),
    Integer.valueOf(Color.pink.getRGB()),
    Integer.valueOf(Color.orange.getRGB()),
    Integer.valueOf(Color.yellow.getRGB()),
    Integer.valueOf(Color.green.getRGB()),
    Integer.valueOf(Color.magenta.getRGB()),
    Integer.valueOf(Color.cyan.getRGB()),
    Integer.valueOf(Color.blue.getRGB())
  };

  /**
  * Array of Color Hash Table Strings.
  */
  private static String COLOR_HASH_STRINGS[] =
  {
    "white".toLowerCase(),
    "lightGray".toLowerCase(),
    "gray".toLowerCase(),
    "darkGray".toLowerCase(),
    "black".toLowerCase(),
    "red".toLowerCase(),
    "pink".toLowerCase(),
    "orange".toLowerCase(),
    "yellow".toLowerCase(),
    "green".toLowerCase(),
    "magenta".toLowerCase(),
    "cyan".toLowerCase(),
    "blue".toLowerCase()
  };

  /**
  * Array of Color Strings.
  */
  private static String COLOR_DISPLAY_STRINGS[] =
  {
    getPrettyString("white"),
    getPrettyString("lightGray"),
    getPrettyString("gray"),
    getPrettyString("darkGray"),
    getPrettyString("black"),
    getPrettyString("red"),
    getPrettyString("pink"),
    getPrettyString("orange"),
    getPrettyString("yellow"),
    getPrettyString("green"),
    getPrettyString("magenta"),
    getPrettyString("cyan"),
    getPrettyString("blue")
  };

         //Hashtables used by string/colorRGB conversion methods:
  private static Hashtable strToClrHashtable = null;
  private static Hashtable clrToStrHashtable = null;

    /** List of IP addresses for 'getLocalHostName()' to try to ignore. */
  public static final String [] FAKE_IP_ADDRS =
                         {"192.168.","172.16.","10.","255.","127.0.0.","0.",
                "fe8","fec","fed","fee","fef","FE8","FEC","FED","FEE","FEF",
                                            "2001:","2002:","0:0:0:0:0:0:"};

    /** String of special characters for 'insertQuoteChars()' method. */
  public static final String DEF_SPECIAL_CHARS_STR = "\"\r\n\t\\";

  private static volatile String osName;
  private static volatile OsType osType;

    //private constructor so that no object instances may be created
    // (static access only)
  private UtilFns()
  {
  }

  /**
   * Add all the elements in new list to the old list.
   * @param oldList the old list.
   * @param newList the new list.
   * @param maxSize the maximum size for the old list.
   * @param index index at which to insert the first element.
   * @return <tt>true</tt> if all of the elements were added, false otherwise.
   */
  public static boolean addAll(
	  List oldList, List newList, int maxSize, int index)
  {
	final int oldListSize = oldList.size();
	if (oldListSize >= maxSize)
	  return false;
	boolean allAdded = true;
	final int newListSize = newList.size();
	maxSize -= oldListSize;
	if (newListSize > maxSize)
	{
	  newList = newList.subList(newListSize - maxSize, newListSize);
	  allAdded = false;
	}
	oldList.addAll(index, newList);
	return allAdded;
  }

  /**
   * Checks the current Java version and prints an error message to standard error if any.
   * @param javaVersionString the current Java version string.
   * @param minJavaVersionString the minimum Java version string.
   * @return the java version or null if error.
   * @see #getJavaVersion()
   */
  public static String checkJavaVersion(final String minJavaVersionString)
  {
	  final String javaVersionString = getJavaVersion();
	  return checkJavaVersion(javaVersionString, minJavaVersionString, System.err);
  }

  /**
   * Checks the specified Java version.
   * @param javaVersionString the current Java version string.
   * @param minJavaVersionString the minimum Java version string.
   * @return true if the current Java version is not less than the minimum,
   * false otherwise.
   * @see #getJavaVersion()
   */
  public static boolean checkJavaVersion(
      String javaVersionString, String minJavaVersionString)
  {
	  if (javaVersionString != null && javaVersionString.length() != 0 &&
			  !DEFAULT_JAVA_VERSION_STRING.equals(javaVersionString) &&
			  compareVersionStrings(
					  javaVersionString,minJavaVersionString) < 0)
	  {
		  return false;
	  }
	  return true;
  }

  /**
   * Checks the specified Java version.
   * @param javaVersionString the current Java version string.
   * @param minJavaVersionString the minimum Java version string.
   * @return true if the current Java version is not less than the minimum,
   * false otherwise.
   * @param error the object to append the error message to.
   * @return the java version or null if error.
   * @see #getJavaVersion()
   */
  public static String checkJavaVersion(
      final String javaVersionString, final String minJavaVersionString,
      final Appendable error)
  {
	  if(!checkJavaVersion(javaVersionString, minJavaVersionString))
	  {  //version string format OK and version is too low; save error message
		  try
		  {
			  error.append("This program requires a newer version of Java (Java \"");
			  error.append(javaVersionString);
			  error.append("\" in use, Java \"");
			  error.append(minJavaVersionString);
			  error.append("\" or later required)");
		  }
		  catch (Exception ex) {}
		  return null;
	  }
	  return javaVersionString;
  }

  /**
   * Close quietly ignoring any exceptions.
   * @param closeable the object to be closed or null if none.
   */
  public static void closeQuietly(Closeable closeable)
  {
	if (closeable != null)
	{
	  try { closeable.close(); } catch (Exception ex) {}
	}
  }

  /**
   * Close the resource quietly ignoring any errors.
   * 
   * @param closeable
   *            the resource or null if none.
   */
  public static void closeQuietly(AutoCloseable closeable) {
      try {
          if (closeable != null) {
              closeable.close();
          }
      } catch (Exception ex) {
      }
  }

  /**
   * Get the character set.
   * 
   * @param charsetName
   *            the character set name.
   * @param defaultCharset
   *            the default character set or null for the default character
   *            set of this Java virtual machine.
   * @return the character set for the specified character set name or the
   *         default character set if none.
   */
  public static Charset getCharset(String charsetName,
          Charset defaultCharset)
  {
      Charset charset = defaultCharset;
      try
      {
          charset = Charset.forName(charsetName);
      }
      catch (Exception ex)
      {
          if (charset == null)
          {
              charset = Charset.defaultCharset();
          }
      }
      return charset;
  }

  /**
   * Gets the Java version.
   * @return the Java version or "(Unknown)" if unavailable.
   */
  public static String getJavaVersion()
  {
    //get Java version in use:
    String s = null;
    try
    {
    	s = System.getProperty("java.version");
    }
    catch (Exception ex)
    {
    }
    if (s == null)
    {
      s = UNKNOWN_STRING; //indicate unable to fetch
    }
    return s;
  }

  /**
   * GEt the OS name.
   * @return the OS name or "(Unknown)" if unavailable.
   */
  public static String getOsName()
  {
	  String s = osName;
	  if (s == null)
	  {
		  try
		  {
			  s = System.getProperty("os.name");
		  }
		  catch (Exception ex)
		  {
		  }
		  if (s == null)
		  {
			  s = UNKNOWN_STRING; //indicate unable to fetch
		  }
		  osName = s;
	  }
	  return s;
  }

  /**
   * Get the operating system type.
   * @return the operating system type.
   */
  public static OsType getOsType()
  {
	  OsType t = osType;
	  if (t == null)
	  {
		  t = OsType.getOsType(getOsName());
		  osType = t;
	  }
	  return t;
  }

  /**
   * Determines if the operating system type is Mac.
   * @return true if Mac, false otherwise.
   */
  public static boolean isMac()
  {
	  return getOsType() == OsType.MAC;
  }

  /**
   * Determines if the operating system type is Unix.
   * @return true if Unix, false otherwise.
   */
  public static boolean isUnix()
  {
	  return getOsType() == OsType.UNIX;
  }

  /**
   * Determines if the operating system type is Windows.
   * @return true if Windows, false otherwise.
   */
  public static boolean isWindows()
  {
	  return getOsType() == OsType.WINDOWS;
  }

    /**
     * Returns the current time in seconds since midnight,
     * January 1, 1970 UTC.
     * @return The current time in seconds since midnight,
     * January 1, 1970 UTC.
     */
  public static long getSystemTimeSec()
  {
    return System.currentTimeMillis()/MS_PER_SECOND;
  }

    /**
     * Converts a 32-bit integer value to a 64-bit long as if the value
     * is unsigned.  This is useful for converting epoch time values back
     * to 'long' after they have been cast to 'int' (extending the useful
     * range into 2/2106).
     * @param intTime a time value in seconds.
     * @return The 'long' integer version of the time value.
     */
  public static long intTimeToLong(int intTime)
  {
    if(intTime >= 0)
      return (long)intTime;
    return (long)intTime + 0x100000000L;
  }

  /**
   * Converts the given Date into a RFC-822 date/time string.
   * @param date the time value to be formatted into a time string.
   * @return the RFC-822 date/time string.
   */
  public static final String dateToRFC_822String(Date date)
  {
    synchronized(rfc_822DateFormatObj)
    {
      return rfc_822DateFormatObj.format(date);
    }
  }

  /**
   * Converts the given integer time value to a RFC-822 date/time string.
   * @param millis a time in milliseconds since midnight,
   * January 1, 1970 UTC.
   * @return the RFC-822 date/time string.
   */
  public static final String timeMillisToRFC_822String(long millis)
  {
    return dateToRFC_822String(new Date(millis));
  }

  /**
   * Converts the given integer time value to a date/time string.
   * @param millis a time in milliseconds since midnight,
   * January 1, 1970 UTC.
   * @param gmtFlag true for GMT time zone, false for local time zone.
   * @param amPmFlag true for AM/PM clock, false for 24-hour clock.
   * @return A string representation of the given time, using some
   * variation of following format:  "Mmm dd yyyy hh:mm:ss ZZZ".
   */
  public static String timeMillisToString(long millis,boolean gmtFlag,
                                                           boolean amPmFlag)
  {
                        //get date-format object for flags:
    final DateFormat formatterObj = getLocalDateFormatObj(gmtFlag,amPmFlag);
    synchronized(formatterObj)
    {    //only allow one thread at a time to use format object
      return formatterObj.format(new Date(millis));
    }
  }

  /**
   * Converts the given integer time value to a date/time string using
   * the 24-hour clock format.
   * @param millis a time in milliseconds since midnight,
   * January 1, 1970 UTC.
   * @param gmtFlag true for GMT time zone, false for local time zone.
   * @return A string representation of the given time, using some
   * variation of following format:  "Mmm dd yyyy hh:mm:ss ZZZ".
   */
  public static String timeMillisToString(long millis,boolean gmtFlag)
  {
    return timeMillisToString(millis,gmtFlag,false);
  }

  /**
   * Converts the given integer time value to a date/time string using
   * the 24-hour clock format and the local time zone.
   * @param millis a time in milliseconds since midnight,
   * January 1, 1970 UTC.
   * @return A string representation of the given time, using some
   * variation of following format:  "Mmm dd yyyy hh:mm:ss ZZZ".
   */
  public static String timeMillisToString(long millis)
  {
    return timeMillisToString(millis,false,false);
  }

  /**
   * Converts the current time value to a date/time string using
   * the 24-hour clock format and the local time zone.
   * @return A string representation of the given time, using some
   * variation of following format:  "Mmm dd yyyy hh:mm:ss ZZZ".
   */
  public static String timeMillisToString()
  {
    return timeMillisToString(System.currentTimeMillis(),false,false);
  }

  /**
   * Returns a duration-time string for the given time value.
   * @param durationMs the number of milliseconds.
   * @param showDaysFlag if true and the number of hours is not less
   * than 24 then the number of days is displayed; if false then the
   * total number of hours is always displayed.
   * @param showSecsFlag if true then the number of seconds is included.
   * @param verboseFlag if true then show verbose text.
   * @return A duration-time string in form:  "# days H:MM:SS" (with the
   * days and seconds display controlled by the flag parameters).
   */
  public static String durationMillisToString(
      long durationMs, boolean showDaysFlag, boolean showSecsFlag,
      boolean verboseFlag)
  {
    final int days;
    if(showDaysFlag)
    {    //showing days; calculate number
      if((days=(int)(durationMs/UtilFns.MS_PER_DAY)) > 0)
        durationMs -= (long)days * UtilFns.MS_PER_DAY;
    }
    else      //not showing days
      days = 0;              //setup to not show days
    final int hrs;      //calc # of hours and remove from total:
    if((hrs=(int)(durationMs/UtilFns.MS_PER_HOUR)) > 0)
      durationMs -= (long)hrs * UtilFns.MS_PER_HOUR;
    int min;            //calc # of minutes and remove from total:
    if((min=(int)(durationMs/UtilFns.MS_PER_MINUTE)) > 0)
      durationMs -= (long)min * UtilFns.MS_PER_MINUTE;
    int sec;            //calc # of seconds and remove from total:
    if((sec=(int)(durationMs/UtilFns.MS_PER_SECOND)) > 0)
      durationMs -= (long)sec * UtilFns.MS_PER_SECOND;
    if((int)durationMs >= 500)
      ++sec;            //if 1/2-second+ remaining then increment # of secs
    if(!showSecsFlag && sec >= 30)
      ++min;            //if not showing secs then round-up if necessary

    if (verboseFlag)  //if verbose duration-time string
      return ((days > 0) ? (days + " day" + ((days!=1)?"s":"") + " ") : "") +
          ((hrs > 0) ? (hrs + " hour" + ((hrs!=1)?"s":"") + " ") : "") +
          ((!showSecsFlag || min > 0) ? (min + " minute" + ((min!=1)?"s":"") + " ") : "") +
          ((showSecsFlag) ?
           (sec + " second" + ((sec!=1)?"s":"") + " ") : "");
              //build and return duration-time string (show days if
              // non-zero, add leading '0' to minutes or seconds if
              // less than 10, show seconds if flag set):
    return ((days > 0) ? (days + " day" + ((days>1)?"s":"") + " ") : "") +
               hrs + ':' + (((min < 10) ? "0" : "") + min) + (showSecsFlag ?
                                (':' + ((sec < 10) ? "0" : "") + sec) : "");
  }

  /**
   * Returns a duration-time string for the given time value.
   * @param durationMs the number of milliseconds.
   * @param showDaysFlag if true and the number of hours is not less
   * than 24 then the number of days is displayed; if false then the
   * total number of hours is always displayed.
   * @param showSecsFlag if true then the number of seconds is included.
   * @return A duration-time string in form:  "# days H:MM:SS" (with the
   * days and seconds display controlled by the flag parameters).
   */
  public static String durationMillisToString(long durationMs,
                                 boolean showDaysFlag, boolean showSecsFlag)
  {
    return durationMillisToString(durationMs,showDaysFlag,showSecsFlag,false);
  }

  /**
   * Returns a duration-time string for the given time value.  The
   * number of seconds is included in the display.
   * @param durationMs the number of milliseconds.
   * @param showDaysFlag if true and the number of hours is not less
   * than 24 then the number of days is displayed; if false then the
   * total number of hours is always displayed.
   * @return A duration-time string in form:  "# days H:MM:SS" (with the
   * days and seconds display controlled by the flag parameters).
   */
  public static String durationMillisToString(long durationMs,
                                                       boolean showDaysFlag)
  {
    return durationMillisToString(durationMs,showDaysFlag,true);
  }

  /**
   * Returns a duration-time string for the given time value.  The
   * number of seconds is included in the display.  If the number of
   * hours is not less than 24 then the number of days is displayed.
   * @param durationMs the number of milliseconds.
   * @return A duration-time string in form:  "# days H:MM:SS" (with the
   * days and seconds display controlled by the flag parameters).
   */
  public static String durationMillisToString(long durationMs)
  {
    return durationMillisToString(durationMs,true,true);
  }

  /**
   * Parses the given time value string.
   * @param dateStr a date/time string in the form
   * "MMM dd yyyy hh:mm:ss [a] z".
   * @param gmtFlag true for GMT time zone, false for local time zone.
   * @param amPmFlag true for AM/PM clock, false for 24-hour clock.
   * @return A Date, or null if the input could not be parsed.
   */
  public static Date parseStringToDate(String dateStr,boolean gmtFlag,
                                                           boolean amPmFlag)
  {
                        //get date-format object for flags:
    final DateFormat formatterObj = getLocalDateFormatObj(gmtFlag,amPmFlag);
    try
    {
      synchronized(formatterObj)
      {  //only allow one thread at a time to use format object
        return formatterObj.parse(dateStr);
      }
    }
    catch(Exception ex)
    {    //some kind of exception error; return null indicator
      return null;
    }
  }

  /**
   * Parses the given time value string using
   * the 24-hour clock format.
   * @param s the time value string.
   * @param gmtFlag true for GMT time zone, false for local time zone.
   * @return A Date, or null if the input could not be parsed.
   */
  public static Date parseStringToDate(String s,boolean gmtFlag)
  {
    return parseStringToDate(s,gmtFlag,false);
  }

  /**
   * Parses the given time value string using
   * the 24-hour clock format and the local time zone.
   * @param s the time value string.
   * @return A Date, or null if the input could not be parsed.
   */
  public static Date parseStringToDate(String s)
  {
    return parseStringToDate(s,false,false);
  }
  
  /**
   * Returns the local 'DateFormat' object selected by the given flags.
   * @param gmtFlag true for GMT time zone, false for local time zone.
   * @param amPmFlag true for AM/PM clock, false for 24-hour clock.
   * @return A 'DateFormat' object.
   */
  private static DateFormat getLocalDateFormatObj(
                                           boolean gmtFlag,boolean amPmFlag)
  {
    if(amPmFlag)
      return gmtFlag ? dateFormatterGMTAmPm : dateFormatterAmPm;
    return gmtFlag ? dateFormatterGMT : dateFormatter;
  }

    /**
     * Sets the pattern string for the given 'DateFormat' object.  If the
     * given 'DateFormat' object is not of type 'SimpleDateFormat' then
     * it is left unchanged.  See the Java help reference for
     * 'SimpleDateFormat' for details on the characters used in
     * the pattern string.
     * @param formatObj the 'DateFormat' to be changed.
     * @param patternStr the pattern string to apply.
     * @return true if the pattern was applied successfully; false if an
     * error occurred.
     */
  public static boolean setDateFormatPattern(DateFormat formatObj,
                                                          String patternStr)
  {
    try       //trap any exceptions that might occur
    {              //if object is 'SimpleDateFormat' type and pattern
                   // string is not null then apply it:
      if(formatObj instanceof SimpleDateFormat && patternStr != null)
        ((SimpleDateFormat)formatObj).applyPattern(patternStr);
      return true;
    }
    catch(Exception ex) {}        //ignore any exceptions
    return false;
  }

    /**
     * Registers the given 'DataChangedListener' object to be notified
     * when the data such as the default locale is changed.
     * @param listenerObj the 'DataChangedListener' object.
     */
  public static void addDataChangedListener(
      DataChangedListener listenerObj)
  {
    if(listenerObj != null)
    {
      synchronized(listenersVec)
      {
        if(!listenersVec.contains(listenerObj))  //if already on list then
          listenersVec.add(listenerObj);         //add listener
      }
    }
  }

    /**
     * Unregisters the given 'DataChangedListener' object from the list of
     * listeners for data.
     * @param listenerObj the 'DataChangedListener' object.
     */
  public static void removeDataChangedListener(
      DataChangedListener listenerObj)
  {
    if(listenerObj != null)                 //if handle not null then
    {
      synchronized(listenersVec)
      {
        listenersVec.remove(listenerObj); //remove listener from list
      }
    }
  }

    /**
     * Called to indicate that this object has changed.  Each registered
     * listener's 'dataChanged()' method is called.
     * @param sourceObj source object for the change.
     */
  public static void fireDataChanged(Object sourceObj)
  {
    final Vector localListenersVec;
    synchronized(listenersVec)
    {
      localListenersVec = (Vector)listenersVec.clone();
    }

    Object obj;
    final Enumeration e = localListenersVec.elements();
    while(e.hasMoreElements())
    {    //for each listener object; if OK then call data-changed method
      if((obj=e.nextElement()) instanceof DataChangedListener)
        ((DataChangedListener)obj).dataChanged(sourceObj);
    }
  }

  /**
   * Gets the default locale.
   * @see addDataChangedListener, removeDataChangedListener
   * @return the default locale.
   */
  public static Locale getDefaultLocale()
  {
    return defaultLocaleObj;
  }

  /**
   * Gets the system locale.
   * @return the system locale.
   */
  public static Locale getSystemLocale()
  {
    return systemLocaleObj;
  }

  /**
   * Gets the decimal separator character.
   * @return the decimal separator character.
   */
  public static char getDecimalSeparator()
  {
    return decimalSeparator;
  }

  /**
   * Sets the date format symbols for the date format object.
   * @param formatObj the date format.
   * @param newSymbols the date format symbols.
   */
  public static void setDateFormatSymbols(
      DateFormat formatObj,DateFormatSymbols newSymbols)
  {
    if(formatObj instanceof SimpleDateFormat)  //should be simple date format
    {
      //set the new date format symbols
      ((SimpleDateFormat)formatObj).setDateFormatSymbols(newSymbols);
    }
  }

  /**
   * Sets the decimal format symbols for the number format object.
   * @param formatObj the number format.
   * @param newSymbols the decimal format symbols.
   */
  public static void setDecimalFormatSymbols(
      NumberFormat formatObj,DecimalFormatSymbols newSymbols)
  {
    if(formatObj instanceof DecimalFormat)  //should be decimal format
    {
      //set the new date format symbols
      ((DecimalFormat)formatObj).setDecimalFormatSymbols(newSymbols);
    }
  }

  /**
   * Sets the default locale.
   * @see addDataChangedListener, removeDataChangedListener
   * @param localeObj the default locale.
   */
  public static void setDefaultLocale(Locale localeObj)
  {
    if (localeObj == null)  //if no locale
      localeObj = systemLocaleObj;  //use the system locale

    synchronized (defaultLocaleSyncObj)
    {
      if (defaultLocaleObj.equals(localeObj))  //exit if no change
        return;

      defaultLocaleObj = localeObj; //save the new default locale
    }
    decimalSeparator =
            new DecimalFormatSymbols(localeObj).getDecimalSeparator();

    //update the float number formatters
    final DecimalFormatSymbols newSymbols = new DecimalFormatSymbols(localeObj);
    setDecimalFormatSymbols(floatNumberFormatObj,newSymbols);
    setDecimalFormatSymbols(variableNumberFormatObj,newSymbols);

    //update the date formatters
    final DateFormatSymbols newFormatSymbols = new DateFormatSymbols(localeObj);
    setDateFormatSymbols(dateFormatter, newFormatSymbols);
    setDateFormatSymbols(dateFormatterGMT, newFormatSymbols);
    setDateFormatSymbols(dateFormatterAmPm, newFormatSymbols);
    setDateFormatSymbols(dateFormatterGMTAmPm, newFormatSymbols);

    fireDataChanged(localeObj);  //notify change of the default locale
  }

    /**
     * Creates a 'DateFormat' object and applies the given pattern string to
     * it.  The recommended method of using "DateFormat.getDateTimeInstance()"
     * is followed.  See the Java help reference for 'SimpleDateFormat' for
     * details on the characters used in the pattern string.
     * @param patternStr the pattern string to apply.
     * @param timeZoneObj timezone object to apply, or null or none.
     * @return A new 'SimpleDateFormat' object with the given pattern
     * string applied to it.
     */
  public static DateFormat createDateFormatObj(String patternStr,
                                                       TimeZone timeZoneObj)
  {
    return createDateFormatObj(patternStr,timeZoneObj,getDefaultLocale());
  }

    /**
     * Creates a 'DateFormat' object and applies the given pattern string to
     * it.  The recommended method of using "DateFormat.getDateTimeInstance()"
     * is followed.  See the Java help reference for 'SimpleDateFormat' for
     * details on the characters used in the pattern string.
     * @param patternStr the pattern string to apply.
     * @param timeZoneObj timezone object to apply, or null or none.
     * @param localeObj the locale object.
     * @return A new 'SimpleDateFormat' object with the given pattern
     * string applied to it.
     */
  public static DateFormat createDateFormatObj(
      String patternStr,TimeZone timeZoneObj,Locale localeObj)
  {
              //create instance (the "right" way):
    DateFormat formatObj = DateFormat.getDateTimeInstance(
        DateFormat.DEFAULT,DateFormat.DEFAULT,localeObj);
              //attempt to set pattern; if fails create instance directly:
    if(!setDateFormatPattern(formatObj,patternStr))
      formatObj = new SimpleDateFormat(patternStr,localeObj);
    if(timeZoneObj != null)                 //if timezone obj given then
      formatObj.setTimeZone(timeZoneObj);   //set timezone object
    return formatObj;        //return new instance
  }

    /**
     * Creates a 'DateFormat' object and applies the given pattern string to
     * it.  The recommended method of using "DateFormat.getDateTimeInstance()"
     * is followed.  See the Java help reference for 'SimpleDateFormat' for
     * details on the characters used in the pattern string.
     * @param patternStr the pattern string to apply.
     * @return A new 'SimpleDateFormat' object with the given pattern
     * string applied to it.
     */
  public static DateFormat createDateFormatObj(String patternStr)
  {
    return createDateFormatObj(patternStr,(TimeZone)null);
  }
    /**
     * Creates a 'DateFormat' object and applies the given pattern string to
     * it.  The recommended method of using "DateFormat.getDateTimeInstance()"
     * is followed.  See the Java help reference for 'SimpleDateFormat' for
     * details on the characters used in the pattern string.
     * @param patternStr the pattern string to apply.
     * @param localeObj the locale object.
     * @return A new 'SimpleDateFormat' object with the given pattern
     * string applied to it.
     */
  public static DateFormat createDateFormatObj(
      String patternStr,Locale localeObj)
  {
    return createDateFormatObj(patternStr,(TimeZone)null,localeObj);
  }

  /**
   * Creates a float number formatter.
   * @return  A float number formatter.
   */
  public static NumberFormat createFloatNumberFormat()
  {
    final NumberFormat floatFormatterObj =
        NumberFormat.getNumberInstance(getDefaultLocale());
    return floatFormatterObj;
  }

  /**
   * Creates a float number formatter.
   * @param localeObj the locale object.
   * @return  A float number formatter.
   */
  public static NumberFormat createFloatNumberFormat(Locale localeObj)
  {
    final NumberFormat floatFormatterObj =
        NumberFormat.getNumberInstance(localeObj);
    return floatFormatterObj;
  }

  /**
   * Creates a float number formatter.
   * @param numDigits the minimum and maximum number of fraction digits
   * to be shown.
   * @return A float number formatter.
   */
  public static NumberFormat createFloatNumberFormat(int numDigits)
  {
    return createFloatNumberFormat(numDigits, numDigits);
  }

  /**
   * Creates a float number formatter.
   * @param numDigits the minimum and maximum number of fraction digits
   * to be shown.
   * @param localeObj the locale object.
   * @return  A float number formatter.
   */
  public static NumberFormat createFloatNumberFormat(
      int numDigits,Locale localeObj)
  {
    return createFloatNumberFormat(numDigits, numDigits, localeObj);
  }

  /**
   * Creates a float number formatter.
   * @param minDigits the minimum number of fraction digits to be shown.
   * @param maxDigits the maximum number of fraction digits to be shown.
   * @return  A float number formatter.
   */
  public static NumberFormat createFloatNumberFormat(int minDigits,
                                                              int maxDigits)
  {
    return createFloatNumberFormat(minDigits,maxDigits,getDefaultLocale());
  }

  /**
   * Creates a float number formatter.
   * @param minDigits the minimum number of fraction digits to be shown.
   * @param maxDigits the maximum number of fraction digits to be shown.
   * @param localeObj the locale object.
   * @return  A float number formatter.
   */
  public static NumberFormat createFloatNumberFormat(
      int minDigits,int maxDigits,Locale localeObj)
  {
    final NumberFormat floatFormatterObj = createFloatNumberFormat(localeObj);
    floatFormatterObj.setMinimumFractionDigits(minDigits);
    floatFormatterObj.setMaximumFractionDigits(maxDigits);
    return floatFormatterObj;
  }

  /**
   * Creates a float number formatter.
   * @param patternStr the pattern string to apply.
   * @return  A float number formatter.
   */
  public static NumberFormat createFloatNumberFormat(String patternStr)
  {
    return createFloatNumberFormat(patternStr,getDefaultLocale());
  }

  /**
   * Creates a float number formatter.
   * @param patternStr the pattern string to apply.
   * @param localeObj the locale object.
   * @return  A float number formatter.
   */
  public static NumberFormat createFloatNumberFormat(
      String patternStr,Locale localeObj)
  {
    final NumberFormat floatFormatterObj =
        new DecimalFormat(patternStr,new DecimalFormatSymbols(localeObj));
    return floatFormatterObj;
  }

  /**
   * Creates a new "java.util.Timer" object whose associated thread has the
   * specified name, and may be specified to run as a daemon.  This provides
   * access to the Java 1.5 'Timer' constructor with (String,boolean)
   * parameters while still retaining Java 1.4 compatibility.  If the
   * Java 1.5 'Timer' constructor is not available then an "unnamed-thread"
   * 'Timer' object will be returned.
   * @param nameStr name for associated thread.
   * @param daemonFlag true if associated thread should run as a daemon.
   * @return A new "java.util.Timer" object.
   */
  public static Timer createNamedTimerObj(String nameStr, boolean daemonFlag)
  {
    try
    {              //find constructor with (String,boolean) parameters:
      final Constructor timerConstructorObj =
                                     Timer.class.getConstructor(new Class []
                                           { String.class, boolean.class });
                   //invoke constructor with given (String,boolean) params:
      return (Timer)(timerConstructorObj.newInstance(new Object []
                 { nameStr, (daemonFlag ? Boolean.TRUE : Boolean.FALSE) }));
    }
    catch(Exception ex)
    {  //unable to construct Timer object with (String,boolean) parameters
    }
    return new Timer(daemonFlag);           //return "standard" Timer object
  }

  /**
   * Gets all the available time zone IDs supported.
   * @return an array of time zone IDs.
   */
  public static String[] getTimeZoneIDs()
  {
    return timeZoneIDs;
  }

  	/**
	 * Parses the given time string. The time string format is HH:MM:SS [AM|PM]
	 * [TZ] where HH is hour 0-23 or 1-12 with AM/PM, MM is minutes, SS is
	 * seconds and TZ is the time zone (default local time zone.)
	 * 
	 * @param timeStr
	 *            the time string.
	 * @param date
	 *            the date or null if none.
	 * @return the calendar or null if the input could not be parsed.
	 */
  public static Calendar parseTime(final String timeStr, final Date date)
  {
	  int index = 1;
	  String s;
	  String pattern = "HH:mm:ss";
	  String [] sa = timeStr.split(" ", 3);
	  if (index < sa.length)
	  {
		  s = sa[index];
		  // check for AM / PM
		  if (s.equalsIgnoreCase("AM") || s.equalsIgnoreCase("PM"))
		  {
			  index++;
			  pattern = "hh:mm:ss a";
		  }
	  }
	  final DateFormat df = createDateFormatObj(pattern);
	  // check for time zone
	  if (index < sa.length)
	  {
		  s = sa[index];
		  TimeZone tz = parseTimeZoneID(s);
		  df.setTimeZone(tz);
	  }
	  try
	  {
		  final Calendar cal = df.getCalendar();
		  if (date != null)
		  {
			  cal.setTime(date);
			  int year = cal.get(Calendar.YEAR);
			  int day = cal.get(Calendar.DAY_OF_YEAR);
			  date.setTime(df.parse(timeStr).getTime());
			  cal.set(Calendar.YEAR, year);
			  cal.set(Calendar.DAY_OF_YEAR, day);
		  }
		  else
		  {
			  df.parse(timeStr);
		  }
		  return cal;
	  }
	  catch (Exception ex) {}
	  return null;
  }

  /**
   * Parses the time zone ID property.
   * @param id time zone ID.
   * @return time zone for the ID.
   */
  public static TimeZone parseTimeZoneID(String id)
  {
    //convert the ID to a time zone
    return TimeZone.getTimeZone(id);
  }

  /**
   * Delays for the given number of milliseconds.
   * @param millisecs the number of milliseconds to delay.
   * @return true if the delay was completed, false if the delay was
   * interrupted via an 'InterruptedException'.
   */
  public static boolean sleep(long millisecs)
  {
    try
    {
      Thread.sleep(millisecs);
      return true;
    }
    catch(InterruptedException ex)
    {
      return false;
    }
  }

    /**
     * Converts the given Class object to "short" String name
     * (with "java.lang." removed).
     * @param classObj a Class object.
     * @return A "short" string representation of the given class or
     * an empty string if the given class could not be converted.
     */
  public static String shortClassString(Class classObj)
  {
    int p;
    String str;

    if(classObj == null || (str=classObj.getName()) == null)
      return EMPTY_STRING;  //if any null handles then return empty string
    if((p=str.lastIndexOf((int)'.')) >= 0)  //find last period
      str = str.substring(p+1);             //truncate after period
    return str;                             //return String
  }
  
  /**
   * Split the text in the string into the collection.
   * @param str the string.
   * @param collection the collection.
   * @param sepChar the separator character.
   * @param trim true to trim extra separator characters, false otherwise.
   */
  public static void split(String str, Collection collection, char sepChar, boolean trim)
  {
	  String s;
	  int beginIndex = 0, endIndex = 0;
	  while (beginIndex < str.length())
	  {
		  if (trim && str.charAt(beginIndex) == sepChar)
		  {
			  beginIndex++;
			  continue;
		  }
		  if ((endIndex = str.indexOf(sepChar, beginIndex)) < 0)
		  {
			  endIndex = str.length();
		  }
		  s = str.substring(beginIndex, endIndex);
		  beginIndex = endIndex + 1;
		  collection.add(s);
	  }
  }
  
  /**
   * Returns a string whose value is this string, with all leading and trailing
   * {@link Character#isWhitespace(char) white space} removed.
   * @param s the string
   * @return a string whose value is this string, with all leading and trailing
   *         white space removed
   */
  public static String strip(String s)
  {
    int left = indexOfNonWhitespace(s);
    int right = lastIndexOfNonWhitespace(s);
    if (left == -1 || right == -1)
    {
      s = EMPTY_STRING;
    }
    else if (left != 0 || right != s.length() - 1)
    {
      s = s.substring(left, right + 1);
    }
    return s;
  }

    /**
     * Returns a new string created by stripping the bracket characters
     * from around the given string.  For example, "[word]" would return
     * "word".  If the brackets are not found then the original string
     * is returned.
     * @param str the string to use.
     * @return A new converted string, or the original string if brackets
     * were not found.
     */
  public static String stripBrackets(String str)
  {
    int len;

    if(str == null || (len=str.length()) < 2 || str.charAt(0) != '[' ||
                                                   str.charAt(len-1) != ']')
      return str;
    return str.substring(1,len-1);
  }

    /**
     * Removes all occurrences of the given character from the given string.
     * @param str the given string.
     * @param ch the character to be removed.
     * @return A new string containing the contents of the given string
     * minus all occurrences of the given character; or the original string
     * if no occurrences were found.
     */
  public static String stripChar(String str,char ch)
  {
    final StringBuffer buff = new StringBuffer(str);
    int pos = 0, c = 0;
    while((pos=buff.toString().indexOf(ch,pos)) >= 0)
    {    //for each occurrence of character in string
      buff.deleteCharAt(pos);     //delete character
      ++c;                        //increment count
    }
         //if character(s) deleted then return new string; otherwise orig:
    return (c > 0) ? buff.toString() : str;
  }

  /**
   * Returns a string whose value is this string, with all leading
   * {@link Character#isWhitespace(char) white space} removed.
   * @param s the string
   * @return  a string whose value is this string, with all leading white
   *          space removed
   */
  public static String stripLeading(String s)
  {
    int left = indexOfNonWhitespace(s);
    if (left == -1)
    {
      s = EMPTY_STRING;
    }
    else if (left != 0)
    {
      s = s.substring(left);
    }
    return s;
  }

  /**
   * Returns a string whose value is this string, with all trailing
   * {@link Character#isWhitespace(char) white space} removed.
   * @param s the string
   * @return  a string whose value is this string, with all leading white
   *          space removed
   */
  public static String stripTrailing(String s)
  {
    int right = lastIndexOfNonWhitespace(s);
    if (right == -1)
    {
      s = EMPTY_STRING;
    }
    else if (right != s.length() - 1)
    {
      s = s.substring(0, right + 1);
    }
    return s;
  }

  /**
   * Removes a given "strip" string from the end of a "data" string.
   * @param dataStr the data string to use.
   * @param stripStr the "strip" string to be removed.
   * @return The "data" string with the trailing "strip" string removed,
   * or the original "data" string (if the trailing "strip" string was
   * not found).
   */
  public static String stripTrailingString(String dataStr,String stripStr)
  {
    if(dataStr != null && stripStr != null && dataStr.endsWith(stripStr))
    {    //strings OK and ends with given string; strip and return
      return dataStr.substring(0,dataStr.length()-stripStr.length());
    }
    return dataStr;          //return original string
  }

  /**
   * Removes a newline from the end of a "data" string.
   * @param dataStr the data string to use.
   * @return The "data" string with the trailing newline removed,
   * or the original "data" string (if the trailing newline was
   * not found).
   */
  public static String stripTrailingNewline(String dataStr)
  {
    return stripTrailingString(dataStr,newline);
  }

    /**
     * Converts the given string to a 'Number' object.
     * @param str the string to convert.
     * @return One of the Number-extended class objects 'Integer',
     * 'Double' or 'BooleanCp'; or null if the string could not
     * be converted.
     */
  public static Number parseNumber(String str)
  {
    final int strLen;
    if(str == null || (strLen=str.length()) < 1)
      return null;      //if token string null or empty then return null
    try
    {
      if(str.charAt(0) >= 'A')
      {  //starts with alphabetic character; check for 'true'/'false' text
        if(str.equalsIgnoreCase(BOOLEAN_TRUE_TEXT))   //if 'true' then
          return new BooleanCp(true);         //return 'true' BooleanCp
        if(str.equalsIgnoreCase(BOOLEAN_FALSE_TEXT))  //if 'false' then
          return new BooleanCp(false);        //return 'false' BooleanCp
      }
      char ch;
      for(int p=0; p<strLen; ++p)
      {  //for each character in string
              //if decimal point or scientific-notation character
              // then attempt to parse as double value:
        if((ch=str.charAt(p)) == '.' || ch == 'E' || ch == 'e')
          return Double.valueOf(str);
      }
              //if alternate decimal separator in use and found
              // then process as double using separator:
      if(decimalSeparator != '.' && str.indexOf(decimalSeparator) >= 0)
        return Double.valueOf(str.replace(decimalSeparator,'.'));
      if(str.charAt(0) < 'A')               //if non-alphabetic then
        return Integer.valueOf(str);            //process as Integer
    }
    catch(NumberFormatException ex) {}      //error parsing number
    return null;        //if reached here then could not be converted
  }

    /**
     * Converts the given string to a 'Number' object.
     * @param str the string to convert.
     * @param valueClass the specific number class for the value.
     * @return One of the Number-extended class objects 'Integer',
     * 'Double' or 'BooleanCp'; or null if the string could not
     * be converted.
     */
  public static Number parseNumber(String str, Class valueClass)
  {
    try
    {
      if (BooleanCp.class.equals(valueClass))
        return new BooleanCp(str);

      //if decimal separator is not '.' replace '.' with the decimal separator
      if (decimalSeparator != '.')
        str = str.replace('.',decimalSeparator);

      final Number numberObj = floatNumberFormatObj.parse(str);
      //if number class does not match the value class
      if (!(numberObj.getClass().equals(valueClass)))
      {
        if (Integer.class.equals(valueClass))
          return Integer.valueOf(numberObj.intValue());
        if (Long.class.equals(valueClass))
          return Long.valueOf(numberObj.longValue());
        if (Float.class.equals(valueClass))
          return Float.valueOf(numberObj.floatValue());
        if (Double.class.equals(valueClass))
          return Double.valueOf(numberObj.doubleValue());
        if (Byte.class.equals(valueClass))
          return Byte.valueOf(numberObj.byteValue());
        if (Short.class.equals(valueClass))
          return Short.valueOf(numberObj.shortValue());
      }
      return numberObj;
    }
    catch(Exception ex) {}      //error parsing number
    return null;        //if reached here then could not be converted
  }

    /**
     * Inputs a line of data from the user console (System.in).  This
     * method blocks until a line a data is entered.
     * @return The entered string, or null if an I/O error occurred.
     */
  public static String getUserConsoleString()
  {
    try
    {
      return (new BufferedReader(new InputStreamReader(
                                                    System.in))).readLine();
    }
    catch(IOException ex)
    {
      return null;
    }
  }

  /**
   * Gets the display name index for the specifed color value.
   * @param value the RGB value of the color.
   * @return the index of the color or -1 if not found.
   * @see #getColorDisplayNames()
   */
  public static int getColorDisplayNameIndex(int value)
  {
    for (int i = 0; i < COLOR_HASH_VALUES.length; i++)
    {
      if (COLOR_HASH_VALUES[i].intValue() == value)
        return i;
    }
    return -1;
  }

  /**
   * Gets the value for the specified display name index.
   * @param index the display name index.
   * @return the RGB value of the color.
   * @see #getColorDisplayNames()
   */
  public static int getColorDisplayNameValue(int index)
  {
    return COLOR_HASH_VALUES[index].intValue();
  }

  /**
   * Gets the display name for the specifed color value.
   * @param value the RGB value of the color.
   * @return color name string or hex string if the color value was not found.
   * @see #getColorDisplayNames()
   */
  public static String getColorDisplayName(int value)
  {
    final String displayName;
    final int index = getColorDisplayNameIndex(value);
    if (index >= 0)
      displayName = COLOR_DISPLAY_STRINGS[index];
    else
      displayName = colorRGBToString(value,true);
    return displayName;
  }

  /**
   * Gets a list of string color display names.
   * @return an array of display names.
   */
  public static String[] getColorDisplayNames()
  {
    return COLOR_DISPLAY_STRINGS;
  }

    /**
     * Converts the given string to the integer RGB representation of the
     * corresponding color.  (As matching the static fields in
     * 'java.awt.Color'.)  A case-insensitive comparison is used.
     * @param str the string to be converted.
     * @return An Integer object containing the integer RGB representation
     * of the Color object corresponding to the given string, or null if
     * the string could not be converted.
     */
  public static Integer stringToColorRGB(String str)
  {
    return stringToColorRGB(str,false);
  }

    /**
     * Converts the given color name string to the integer RGB
     * representation of the corresponding color.  (As matching the static
     * fields in 'java.awt.Color'.)  A case-insensitive comparison is used.
     * If 'tryRGBFlag' is true and no color name match is found then the
     * method will attempt to interpret the string as a numeric RGB value
     * or values.  As a single value, if it is a decimal value it is
     * interpreted as an opaque sRGB color, otherwise it is interpreted
     * as a sRGB color with an alpha component.
     * @param str the string to be converted.
     * @param tryRGBFlag if true then and no color name match is found
     * then the method will attempt to interpret the string as a numeric
     * RGB value or values.
     * @return An Integer object containing the integer RGB representation
     * of the Color object corresponding to the given string, or null if
     * the string could not be converted.
     */
  public static Integer stringToColorRGB(String str,boolean tryRGBFlag)
  {
    //create hash table if not yet created
    if(strToClrHashtable == null)
    {
      strToClrHashtable = new Hashtable();
      //put in an entry for each static color field in 'java.awt.Color':
      for (int i = 0; i < COLOR_HASH_STRINGS.length; i++)
        strToClrHashtable.put(COLOR_HASH_STRINGS[i],COLOR_HASH_VALUES[i]);
    }

    final Object obj = strToClrHashtable.get(str.toLowerCase());

    if(obj instanceof Integer)
      return (Integer)obj;   //if name match then return Integer object
    if(tryRGBFlag)
    {    //try to interpret as numeric RGB value(s)
      Color colorObj;        //handle for Color object
      int p;
      if((p=str.indexOf(',')) >= 0)
      {       //at least one comma found
        int [] intArr = new int[3];      //create array of 3 integers
        final int len = str.length();    //get string length
        int c = 0, stPos = 0;
        while(true)
        {     //attempt to convert 3 comma-separated integers
          try
          {        //convert an integer:
            intArr[c] = Integer.parseInt(str.substring(stPos,p));
          }
          catch(Exception ex)
          {             //error converting integer
            return null;          //return null for error
          }
          if(++c >= 3)            //increment counter
            break;           //if 3 complete then exit loop
          if((stPos=p+1) >= len)       //put start position after comma
            return null;          //if no more data then return error
          if((p=str.indexOf(',',stPos)) < 0)     //find next comma
            p = len;         //if no comma then use end-of-string position
        }
        if(p < len)          //if more data remaining in string then
          return null;       //return null for error
        try
        {               //create color object using parsed integers:
          colorObj = new Color(intArr[0],intArr[1],intArr[2]);
        }
        catch(Exception ex)
        {          //error creating color
          return null;       //return null for error
        }
      }
      else
      {       //no commas found
        try
        {          //attempt to convert single integer
          boolean hasalpha = false;
          int value;
          try
          {
            //attempt to convert decimal
            value = Integer.parseInt(str);
          }
          catch (NumberFormatException ex)
          {
            //not decimal, attempt to decode
            value = Long.decode(str).intValue();
            hasalpha = true;
          }

          colorObj = new Color(value, hasalpha);
        }
        catch(Exception ex)
        {          //error converting integer or creating color
          return null;       //return null for error
        }
      }
      try
      {       //convert color object to integer; return Integer object
        return Integer.valueOf(colorObj.getRGB());
      }
      catch(Exception ex)
      {            //error creating Integer object from color object value
        return null;         //return null for error
      }
    }
    return null;   //if no matching Integer object found then return null
  }

  /**
   * Converts the given boolean value to a string.
   * @param b the boolean value.
   * @return the string representing the boolean value.
   */
  public static String booleanToString(boolean b)
  {
    return b ? BOOLEAN_TRUE_TEXT : BOOLEAN_FALSE_TEXT;
  }

  /**
   * Converts the given boolean value to a Boolean.
   * @param b the boolean value.
   * @return the Boolean.
   */
  public static Boolean booleanValueOf(boolean b)
  {
    return b ? Boolean.TRUE : Boolean.FALSE;
  }

  /**
   * Converts the given string to a Boolean.
   * @param s the string representing the boolean value.
   * @return the Boolean.
   */
  public static Boolean booleanValueOf(String s)
  {
    return booleanValueOf(stringToBoolean(s));
  }

  /**
   * Converts the given string to a boolean value.
   * @param s the string representing the boolean value.
   * @return the boolean value.
   */
  public static boolean stringToBoolean(String s)
  {
    return stringToBoolean(s,false);
  }

  /**
   * Converts the given string to a boolean value.
   * @param s the string representing the boolean value.
   * @param defaultValue the default value if the string is null.
   * @return the boolean value.
   */
  public static boolean stringToBoolean(String s,boolean defaultValue)
  {
    if (s == null)
      return defaultValue;
    return s.equalsIgnoreCase(BOOLEAN_TRUE_TEXT);
  }

    /**
     * Converts the given integer color RGB value to a string name
     * corresponding to the color.  (As matching the static fields in
     * 'java.awt.Color'.)
     * @param colorRGBVal the integer color RGB value to use.
     * @return A string color name or null if the color RGB value could
     * not be converted.
     */
  public static String colorRGBToString(int colorRGBVal)
  {
    //create hash table if not yet created
    if(clrToStrHashtable == null)
    {    //hash table not yet created; do it now
      clrToStrHashtable = new Hashtable();
         //put in an entry for each static color field in 'java.awt.Color':
      for (int i = 0; i < COLOR_HASH_VALUES.length; i++)
        clrToStrHashtable.put(COLOR_HASH_VALUES[i],COLOR_HASH_STRINGS[i]);
    }

    final Object obj = clrToStrHashtable.get(Integer.valueOf(colorRGBVal));
    if(obj == null || !(obj instanceof String))
      return null;      //if no matching String found then return null
    return (String)obj;           //return string
  }

  /**
   * Converts the given integer color RGB value to a string name
   * corresponding to the color.  (As matching the static fields in
   * 'java.awt.Color'.)
   * @param colorRGBVal RGB value of color.
   * @param useHexFlag true if hex string should be used if no match.
   * @return A string color name if the color RGB value could
   * be converted. Otherwise a hex string is returned if the useHexFlag is true
   * or null if it is not.
   */
  public static String colorRGBToString(int colorRGBVal,boolean useHexFlag)
  {
    String colorStr = colorRGBToString(colorRGBVal);
    if (colorStr == null && useHexFlag)
    {    //no match to name color and use-hex flag is set
                   //get hex-string for color value, 8 characters long
      colorStr = toHexString(colorRGBVal,colorPropertyStringLength).
                                                              toUpperCase();
      if(colorStr.startsWith("FF"))              //if alpha==255 then
        colorStr = colorStr.substring(2);        //strip leading alpha value
      colorStr = HEX_PREFIX + colorStr;          //add leading hex indicator
    }
    return colorStr;
  }

  /**
   * Creates a string representation of the integer argument as an
   * unsigned integer in base-16.
   *
   * @param   i   an integer.
   * @param   numDigits the number of hex digits to pad if necessary.
   * @return  the string representation of the unsigned integer value
   *          represented by the argument in hexadecimal (base&nbsp;16).
   */
  public static String toHexString(int i, int numDigits)
  {
    String hexString = Integer.toHexString(i);

    for (int len = hexString.length(); len < numDigits; len++)
      hexString = "0" + hexString;
    return hexString;
  }

  /**
   * Converts the given integer color value to a property string name
   * corresponding to the color.
   * @param colorObj the color.
   * @return A property string representing the color.
   */
  public static String colorToPropertyString(Color colorObj)
  {
    return toHexString(colorObj.getRGB(),colorPropertyStringLength).toUpperCase();
  }

  /**
   * Converts the given float number value to a property string.
   * @param value the number value.
   * @return A property string representing the number.
   */
  public static String floatNumberToPropertyString(Object value)
  {
    return floatNumberFormatObj.format(value);
  }

  /**
   * Converts the given float number value to a string
   * where the value displayed will be at least one fractional digit and
   * will always be non-zero as long as the value is greater than zero.
   * @param value the number value.
   * @return A string representing the number.
   */
  public static String floatNumberToString(double value)
  {
    int digits = 1;  //default to one fractional digit

    //if the value is greater than zero but less than 0.1
    if (value > 0 && value < 0.1)
    {
      double x = value;
      while ((x *= 10) < 1.0)  //while the value rounded is less than 1
      {
        digits++;  //increase the number of fractional digits
      }
    }
    return floatNumberToString(value, digits);
  }

  /**
   * Converts the given float number value to a string.
   * @param value the number value.
   * @param digits the maximum number of fraction digits to be shown.
   * @return A string representing the number.
   */
  public static String floatNumberToString(double value, int digits)
  {
    return floatNumberToString(value, digits, 0, null);
  }

  /**
   * Converts the given float number value to a string.
   * @param value the number value.
   * @param digits the maximum number of fraction digits to be shown.
   * @param columns the number of the columns for the string or 0 for no limit.
   * @param maxValueObj the maximum value object or null if not needed.
   * @return A string representing the number.
   */
  public static String floatNumberToString(
      double value, int digits, int columns, Comparable maxValueObj)
  {
    String retVal;
    synchronized (variableNumberFormatObj)
    {
      if (columns > 0)  //if columns specified
      {
        //format the string to the number of integer digits needed
        variableNumberFormatObj.setMaximumIntegerDigits(columns);
      }
      else
      {
        //restore the default
        variableNumberFormatObj.setMaximumIntegerDigits(
            variableNumberFormatMaximumIntegerDigits);
      }

      //format the string to the number of digits needed
      variableNumberFormatObj.setMaximumFractionDigits(digits);
      retVal = variableNumberFormatObj.format(value);

      //find decimal separator
      final int decimalSeparatorIndex = retVal.indexOf(decimalSeparator);

      //if columns specified and string length is greater
      if (columns > 0 && retVal.length() > columns)
      {
        //change the number of digits to use what is left
        digits = columns - 1 - decimalSeparatorIndex;
        //format the string to the number of digits needed
        variableNumberFormatObj.setMaximumFractionDigits(digits);
        retVal = variableNumberFormatObj.format(value);
      }

      //if the max value was specified
      if (maxValueObj != null)
      {
    	  try
    	  {
    		  //get the new value
    		  final Number newValue = variableNumberFormatObj.parse(retVal);
    		  //if the max value is less than the new value (from rounding)
    		  if (maxValueObj.compareTo(newValue) < 0)
    		  {
    			  //subtract 1 from the last fraction digit
    			  final int numFracDigits =
    					  retVal.length() - 1 - decimalSeparatorIndex;
    			  //if decimal separator was found and number of fractional digits found
    			  if (decimalSeparatorIndex >= 0 && numFracDigits > 0)
    			  {
    				  final double subValue = Math.pow(10, -numFracDigits);
    				  retVal = variableNumberFormatObj.format(value - subValue);
    			  }
    		  }
    	  }
    	  catch (Exception ex) {}
      }
    }

    return retVal;
  }

  /**
   * Converts the given data string to an object.
   * @param valueClass the value class.
   * @param dataStr the data string.
   * @return the object.
   */
  public static Object parseObject(Class valueClass, String dataStr)
  {
    // find a valueOf method and make a new instance of the object
    try
    {
      Method m = valueClass.getMethod("valueOf", String.class);
      return m.invoke(null, dataStr);
    }
    catch (Exception ex) {}
    //Find a constructor with a String parameter and
    //make a new instance of the object.
    try
    {
      final Class[] parameterTypes = {String.class};
      final java.lang.reflect.Constructor c =
          valueClass.getConstructor(parameterTypes);
      final Object[] initargs = {dataStr};
      return c.newInstance(initargs);
    }
    catch (Exception ex) {}
    return null; //if none of the above then return error
  }

  /**
   * Converts the given data string to an object.
   * @param defaultValueObj the default value object.
   * @param dataStr the data string.
   * @return the object.
   */
  public static Object parseObject(Object defaultValueObj, String dataStr)
  {
    try
    {
      return parseObject(defaultValueObj.getClass(), dataStr);
    }
    catch (Exception ex) {}
    return null; //if none of the above then return error
  }

  /**
   * Converts the given data string to an 'Archivable' object.
   * @param valueClass the 'Archivable' value class.
   * @param dataStr the data string.
   * @return the 'Archivable' object.
   */
  public static Archivable parseArchivable(Class valueClass, String dataStr)
  {
    //Find the archivable constructor and
    //make a new instance of the object.
    try
    {
      final Class[] parameterTypes = {String.class, Archivable.Marker.class};
      final java.lang.reflect.Constructor c =
          valueClass.getConstructor(parameterTypes);
      final Object[] initargs = {dataStr, null};
      final Object obj = c.newInstance(initargs);
      if (obj instanceof Archivable)
        return (Archivable)obj;
    }
    catch (Exception ex) {}
    return null; //if none of the above then return error
  }

  /**
   * Converts the given data string to an 'Archivable' object.
   * @param defaultValueObj the 'Archivable' default value object.
   * @param dataStr the data string.
   * @return the 'Archivable' object.
   */
  public static Archivable parseArchivable(Archivable defaultValueObj, String dataStr)
  {
    try
    {
      return parseArchivable(defaultValueObj.getClass(), dataStr);
    }
    catch (Exception ex) {}
    return null; //if none of the above then return error
  }

  /**
   * Converts the given property string name to a color value.
   * @param colorString the property string for the color.
   * @return the color.
   * @exception NumberFormatException if the specified string
   * cannot be interpreted as a color property string.
   */
  public static Color parseColor(String colorString)
      throws NumberFormatException
  {
    final int value = (int)Long.parseLong(colorString, 16);

    // If the length of the colorString is less than 7 unless the colorString
    // is "0" since "0" is a common number for clear, and should be
    // transparent.
    if (colorString.length() < 7 && !colorString.equals("0"))
    {
      // Just a RGB value, use regular JDK1.1 constructor
      return new Color(value);
    }
    return new Color(value, true);
  }

  /**
   * Parse the text for an integer.
   * @param s the text.
   * @return the integer.
   */
  public static Integer parseInteger(String s)
  {
	int prefix = 10;
	if (s.startsWith(HEX_PREFIX))
	{
	  prefix = 16;
	  s = s.substring(HEX_PREFIX.length());
	}
	return Integer.valueOf(s, prefix);
  }

  /**
   * Parse the text for a long integer.
   * @param s the text.
   * @return the long integer.
   */
  public static Long parseLong(String s)
  {
	int prefix = 10;
	if (s.startsWith(HEX_PREFIX))
	{
	  prefix = 16;
	  s = s.substring(HEX_PREFIX.length());
	}
	return Long.valueOf(s, prefix);
  }

  /**
   * Returns a fully opaque version of the given color (with alpha = 255).
   * @param colorObj the Color object to use.
   * @return A new Color object.
   */
  public static Color createOpaqueColor(Color colorObj)
  {
    return new Color(colorObj.getRed(),colorObj.getGreen(),
                                                        colorObj.getBlue());
  }

    /**
     * Returns a string with a backslash quote character ('\')
     * inserted in front of each occurance of a "special" character
     * in the given source string.  If they are included in the string
     * of "special" characters, the ASCII characters tab, newline, and
     * carriage return are written as \t, \n, and \r, respectively.
     * @param dataStr the data source string.
     * @param specialChars a string of "special" characters, each of
     * which should be quoted in the source string.
     * @return a new string with its "special" characters quoted, or the
     * original string if no matching "special" characters were found.
     */
  public static String insertQuoteChars(String dataStr,String specialChars)
  {
    int p;
    boolean changedFlag=false;

    if(dataStr == null)           //if null source string then
      return null;                //return null
                   //create string buffer from data source string:
    StringBuffer buff = new StringBuffer(dataStr);
    if(specialChars != null && specialChars.length() > 0)
    {    //special-characters string contains data
      p = 0;
      while(p < buff.length())
      {       //check each character in buffer
        if(specialChars.indexOf(buff.charAt(p)) >= 0)
        {     //"special" character found
          buff.insert(p,'\\');        //insert backslash before character
          ++p;                        //increment past backslash
          switch(buff.charAt(p))
          {   //put in ASCII symbols for \r \n \t
            case '\r':
              buff.setCharAt(p,'r');
              break;
            case '\n':
              buff.setCharAt(p,'n');
              break;
            case '\t':
              buff.setCharAt(p,'t');
              break;
            default:
          }
          changedFlag = true;         //indicate string was changed
        }
        ++p;       //increment to next character
      }
    }
         //return string version of buffer, or original string if unchanged:
    return changedFlag ? buff.toString() : dataStr;
  }

    /**
     * Removes the backslash quote characters ('\') put in by
     * 'insertQuoteChars()'.  The sequences \t, \n, and \r are
     * converted to the ASCII characters tab, newline, and carriage
     * return, respectively.
     * @param dataStr the data source string.
     * @param removeDblFlag true to remove any surrounding pair of
     * double-quote characters ("") from string; false to leave them alone.
     * @return a new string with the quote characters removed, or the
     * original string if no quote characters were found.
     */
  public static String removeQuoteChars(String dataStr,
                                                      boolean removeDblFlag)
  {
    int p;

    if(dataStr == null)           //if null source string then
      return null;                //return null
    if((p=dataStr.indexOf('\\')) >= 0)
    {    //data string contains at least one backslash
                   //create string buffer from data source string:
      StringBuffer buff = new StringBuffer(dataStr);
      do
      {       //loop while more backslashes found
        if(p >= buff.length() - 1)     //if no more chars after '\' then
          break;                       //leave it alone and exit loop
        buff.deleteCharAt(p);          //delete backslash character
        switch(buff.charAt(p))
        {   //put in ASCII codes for \r \n \t (if found)
          case 'r':
            buff.setCharAt(p,'\r');
            break;
          case 'n':
            buff.setCharAt(p,'\n');
            break;
          case 't':
            buff.setCharAt(p,'\t');
            break;
          default:
        }
      }            //loop if another backslash character found
      while((p=buff.toString().indexOf('\\',++p)) > 0);

      if(removeDblFlag && (p=buff.length()) > 1 && buff.charAt(0) == '\"' &&
                                                   buff.charAt(p-1) == '\"')
      {  //flag set and surrounding double-quote characters found
        return buff.substring(1,p-1);       //return string without quotes
      }
      return buff.toString();          //return string version of buffer
    }
         //no backslash characters found in string
    if(removeDblFlag && (p=dataStr.length()) > 1 &&
                   dataStr.charAt(0) == '\"' && dataStr.charAt(p-1) == '\"')
    {  //flag set and surrounding double-quote characters found
      return dataStr.substring(1,p-1);      //return string without quotes
    }
    return dataStr;                    //return original string
  }

    /**
     * Removes the backslash quote characters ('\') put in by
     * 'insertQuoteChars()'.  The sequences \t, \n, and \r are
     * converted to the ASCII characters tab, newline, and carriage
     * return, respectively.
     * @param dataStr the data source string.
     * @return a new string with the quote characters removed, or the
     * original string if no quote characters were found.
     */
  public static String removeQuoteChars(String dataStr)
  {
    return removeQuoteChars(dataStr,false);
  }

    /**
     * Searches the given string for the position of the given character.
     * Any occurrence of the given character that is preceded by a
     * backslash character will be ignored.  Areas of the string
     * surrounded by double-quotes ("") are ignored, but any double-quote
     * characters preceded by a backslash (\") are not considered to be
     * opening or closing quote characters.
     * @param str the string to search.
     * @param ch the character to search for.
     * @param startPos the position in the string to start searching from.
     * @return The position at which the character was found, or -1 if
     * the character was not found.
     */
  public static int findCharPos(String str,char ch,int startPos)
  {
    int foundPos;
    if(startPos >= str.length())
      return -1;        //if start pos beyond end of string then return -1
    while(true)
    {
      if((foundPos=str.indexOf(ch,startPos)) < 0)     //search for character
        return -1;      //if not found then return "not found" value
      if(foundPos == 0)      //if search char found at first position then
        return foundPos;     //return its position
      if(str.charAt(foundPos-1) != '\\')
      {       //found search char is not preceded by a backslash
        --startPos;     //dec start pos because it gets "incremented" below
        do    //loop while finding opening double-quote character
        {                         //search for double-quote character:
          if((startPos=str.indexOf('\"',startPos+1)) < 0)
            return foundPos; //if no opening quote then return found pos
        }     //loop while found double-quote char preceded by a bashslash
        while(startPos > 0 && str.charAt(startPos-1) == '\\');

        if(startPos > foundPos)   //if opening quote after found pos then
          return foundPos;        //return found position of search char

        do    //loop while finding closing double-quote character
        {                         //search for double-quote character:
          if((startPos=str.indexOf('\"',startPos+1)) < 0)
            return -1;  //if no closing quote then return "not found" value
        }     //loop while found double-quote char preceded by a bashslash
        while(str.charAt(startPos-1) == '\\');
        ++startPos;     //move new start pos past closing double-quote char
      }
      else    //found search char is preceded by a backslash
        startPos = foundPos + 1;       //restart search at next position
    }
  }

    /**
     * Searches the entire given string for the position of the given
     * character.  Any occurrence of the given character that is preceded
     * by a backslash character will be ignored.  Areas of the string
     * surrounded by double-quotes ("") are ignored, but any double-quote
     * characters preceded by a backslash (\") are not considered to be
     * opening or closing quote characters.
     * @param str the string to search.
     * @param ch the character to search for.
     * @return The position at which the character was found, or -1 if
     * the character was not found.
     */
  public static int findCharPos(String str,char ch)
  {
    return findCharPos(str,ch,0);
  }

    /**
     * Searches the given string for the first occurrence of any character
     * in the given search string.  Any occurrence of a search character
     * that is preceded by a backslash character will be ignored.  Areas
     * of the string surrounded by double-quotes ("") are ignored, but
     * any double-quote characters preceded by a backslash (\") are not
     * considered to be opening or closing quote characters.
     * @param str the string to search.
     * @param searchCharsStr the string of search characters.
     * @param startPos the position in the string to start searching from.
     * @return The first position at which a search character was found,
     * or -1 if none were found.
     */
  public static int findCharPos(String str,String searchCharsStr,
                                                               int startPos)
  {
    if(searchCharsStr == null)         //if no search string then
      return -1;                       //return not-found value
    final int strLen = str.length();
    int p, minPos = strLen;            //initialize min-position value
    final int searchCharsStrLen = searchCharsStr.length();
    for(int i=0; i<searchCharsStrLen; ++i)
    {    //for each character in search string; check for match in string
      if((p=findCharPos(str,searchCharsStr.charAt(i),startPos)) >= 0 &&
                                                                 p < minPos)
      {  //match found and position is less than current minimum position
        minPos = p;     //save position
      }
    }
         //if any matches found then return position of first one:
    return (minPos < strLen) ? minPos : -1;
  }

    /**
     * Searches the given string for the first occurrence of any character
     * in the given search string.  Any occurrence of a search character
     * that is preceded by a backslash character will be ignored.  Areas
     * of the string surrounded by double-quotes ("") are ignored, but
     * any double-quote characters preceded by a backslash (\") are not
     * considered to be opening or closing quote characters.
     * @param str the string to search.
     * @param searchCharsStr the string of search characters.
     * @return The first position at which a search character was found,
     * or -1 if none were found.
     */
  public static int findCharPos(String str,String searchCharsStr)
  {
    return findCharPos(str,searchCharsStr,0);
  }

  /**
   * Searches the given string for the first occurrence of a non-whitespace
   * character.
   * @param str the string to search.
   * @return The first position at which a whitespace character was found, or
   *         '-1' if none was found.
   */
  public static int indexOfNonWhitespace(String str)
  {
    return indexOfNonWhitespace(str, 0);
  }

  /**
   * Searches the given string for the first occurrence of a non-whitespace
   * character. If the given start position is less than zero or greater than
   * the stringLength-1 then '-1' is returned.
   * @param str      the string to search.
   * @param startPos the position at which to start the search.
   * @return The first position at which a whitespace character was found, or
   *         '-1' if none was found.
   */
  public static int indexOfNonWhitespace(String str,int startPos)
  {
    if(startPos >= 0)
    {    //given start position is OK
      final int len = str.length();
      for(int p=startPos; p<len; ++p)
      {  //for each character at and after 'startPos'
        if(!Character.isWhitespace(str.charAt(p)))
          return p;     //if non-whitespace then return position
      }
    }
    return -1;          //no non-whitespace found; return -1
  }

  /**
   * Searches the given string for the last occurrence of a non-whitespace
   * character.
   * @param str the string to search.
   * @return The last position at which a whitespace character was found, or
   *         '-1' if none was found.
   */
  public static int lastIndexOfNonWhitespace(String str)
  {
    final int len = str.length();
    for(int p=len - 1; p >= 0; --p)
    {  //for each character at and after 'startPos'
      if(!Character.isWhitespace(str.charAt(p)))
        return p;     //if whitespace then return position
    }
    return -1;          //no non-whitespace found; return -1
  }

    /**
     * Searches the given string for the first occurrence of a whitespace
     * character.  If the given start position is less than zero or
     * greater than the stringLength-1 then '-1' is returned.
     * @param str the string to search.
     * @param startPos the position at which to start the search.
     * @return The first position at which a whitespace character was
     * found, or '-1' if none was found.
     */
  public static int indexOfWhitespace(String str,int startPos)
  {
    if(startPos >= 0)
    {    //given start position is OK
      final int len = str.length();
      for(int p=startPos; p<len; ++p)
      {  //for each character at and after 'startPos'
        if(Character.isWhitespace(str.charAt(p)))
          return p;     //if whitespace then return position
      }
    }
    return -1;          //no whitespace found; return -1
  }

    /**
     * Searches the given string for the first occurrence of a whitespace
     * character.
     * @param str the string to search.
     * @return The first position at which a whitespace character was
     * found, or '-1' if none was found.
     */
  public static int indexOfWhitespace(String str)
  {
    return indexOfWhitespace(str,0);
  }

  /**
   * Converts the iterator to an enumeration.
   * @param it the iterator.
   * @return an enumeration.
   */
  public static Enumeration iteratorToEnum(final Iterator it)
  {
    return new Enumeration()
    {
      /**
       * Returns <tt>true</tt> if the enumeration has more elements.
       * @return <tt>true</tt> if the enumeration has more elements.
       */
      public boolean hasMoreElements() { return it.hasNext(); }

      /**
       * Returns the next element in the enumeration.
       * @return the next element in the enumeration.
       * @exception NoSuchElementException iteration has no more elements.
       */
      public Object nextElement() { return it.next(); }
    };
  }

  /**
   * Converts the enumeration to an iterator.
   * @param e the enumeration.
   * @return an iterator.
   */
  public static Iterator enumToIterator(final Enumeration e)
  {
    return new Iterator()
    {
      /**
       * Returns <tt>true</tt> if the iteration has more elements.
       * @return <tt>true</tt> if the iterator has more elements.
       */
      public boolean hasNext() { return e.hasMoreElements(); }

      /**
       * Returns the next element in the iteration.
       * @return the next element in the iteration.
       * @exception NoSuchElementException iteration has no more elements.
       */
      public Object next() { return e.nextElement(); }

      /**
       *
       * Removes from the underlying collection the last element returned by the
       * iterator (optional operation).
       * @exception UnsupportedOperationException the <tt>remove</tt>
       *		  operation is not supported by this Iterator.
       */
      public void remove() { throw new UnsupportedOperationException(); }
    };
  }

    /**
     * Returns a string representation of the data elements referenced
     * by the given enumeration.  The returned string will have a comma
     * and space between each element.
     * @param e the enumeration of data objects to use.
     * @return The list string.
     */
  public static String enumToListString(Enumeration e)
  {
    Object obj;
    StringBuffer buff = new StringBuffer(); //create string buffer
    if(e.hasMoreElements())
    {    //more than zero elements available
      while(true)
      {
        if((obj=e.nextElement()) != null)   //fetch element
          buff.append(obj.toString());      //if not null then add it
        else                                //if null object then
          buff.append("(null)");            //indicate null object
        if(!e.hasMoreElements())
          break;                  //if no more elements then exit loop
        buff.append(", ");        //add separator before next element
      }
    }
    return buff.toString();       //return string version of buffer
  }

    /**
     * Returns a string representation of the data elements referenced
     * by the given enumeration.  The returned string will have leading
     * and trailing brackets ("{}") and a comma and space between each
     * element.
     * @param e the enumeration of data objects to use.
     * @return The string data.
     */
  public static String enumToString(Enumeration e)
  {
    Object obj;
    StringBuffer buff = new StringBuffer(); //create string buffer
    buff.append("{");                       //start with opening bracket
    if(e.hasMoreElements())
    {    //more than zero elements available
      while(true)
      {
        if((obj=e.nextElement()) != null)   //fetch element
          buff.append(obj.toString());      //if not null then add it
        else                                //if null object then
          buff.append("(null)");            //indicate null object
        if(!e.hasMoreElements())
          break;                  //if no more elements then exit loop
        buff.append(", ");        //add separator before next element
      }
    }
    buff.append("}");             //end with closing bracket
    return buff.toString();       //return string version of buffer
  }

    /**
     * Returns a String of substrings gathered from the given enumeration.
     * Each substring is generated via each enumerated object's
     * 'toString()' method, will be surrounded by double-quotes ("")
     * and will be separated by the given separator string.
     * @param enumObj the given enumeration.
     * @param sepStr the string that separates the substrings.
     * @param quoteCharsFlag if true then special characters, such as
     * double-quote and control characters, will be "quoted" in the
     * substrings using the backslash character (\r \n \t \\ \").
     * @return A string of substrings, or an empty string if no substrings
     * could be generated (will not return null).
     */
  public static String enumToQuotedStrings(Enumeration enumObj,String sepStr,
                                                     boolean quoteCharsFlag)
  {
    final StringBuffer retBuff = new StringBuffer();  //create buffer
    if(enumObj != null)
    {    //enumeration object not null
      Object obj;
      if(enumObj.hasMoreElements())
      {  //at least one object in enumeration
        if(quoteCharsFlag)
        {     //"quote" special chars with '\'
          while(true)
          {   //for each element; add string to return buffer
            if((obj=enumObj.nextElement()) != null)
            { //valid object fetched OK; add double-quoted string to buffer
              retBuff.append(QUOTE_STRING + //"quote" special chars with '\'
                    insertQuoteChars(obj.toString(),DEF_SPECIAL_CHARS_STR) +
                                                              QUOTE_STRING);
            }
            if(!enumObj.hasMoreElements())     //if no more elements then
              break;                        //exit loop
            retBuff.append(sepStr);         //put in separator
          }
        }
        else
        {     //don't "quote" special chars with '\'
          while(true)
          {   //for each element; add string to return buffer
            if((obj=enumObj.nextElement()) != null)
            { //valid object fetched OK; add double-quoted string to buffer
              retBuff.append(QUOTE_STRING + obj.toString() + QUOTE_STRING);
            }
            if(!enumObj.hasMoreElements())     //if no more elements then
              break;                        //exit loop
            retBuff.append(sepStr);         //put in separator
          }
        }
      }
    }
    return retBuff.toString();         //return string version of bufffer
  }

    /**
     * Returns a String of substrings gathered from the given enumeration.
     * Each substring is generated via each enumerated object's
     * 'toString()' method, will be surrounded by double-quotes ("")
     * and will be separated by the given separator character.
     * @param enumObj the given enumeration.
     * @param sepChar the character that separates the substrings.
     * @param quoteCharsFlag if true then special characters, such as
     * double-quote and control characters, will be "quoted" in the
     * substrings using the backslash character (\r \n \t \\ \").
     * @return A string of substrings, or an empty string if no substrings
     * could be generated (will not return null).
     */
  public static String enumToQuotedStrings(Enumeration enumObj,char sepChar,
                                                     boolean quoteCharsFlag)
  {
                             //convert character to String object:
    return enumToQuotedStrings(enumObj,new String(new char[] {sepChar}),
                                                            quoteCharsFlag);
  }

    /**
     * Returns a String of substrings gathered from the given enumeration.
     * Each substring is generated via each enumerated object's
     * 'toString()' method, will be surrounded by double-quotes ("")
     * and will be separated by a comma.  Special characters, such as
     * double-quote and control characters, will be "quoted" in the
     * substrings using the backslash character (\r \n \t \\ \").
     * @param enumObj the given enumeration.
     * @return A string of substrings, or an empty string if no substrings
     * could be generated (will not return null).
     */
  public static String enumToQuotedStrings(Enumeration enumObj)
  {
    return enumToQuotedStrings(enumObj,',',true);
  }
  
  /**
   * Determines if two objects are equal.
   * 
   * @param o1 the first object or null if none.
   * @param o2 the second object or null if none.
   * @return true if equal, false otherwise.
   */
  public static boolean equals(Object o1, Object o2)
  {
    return (o1 == o2) || (o1 != null && o1.equals(o2));
  }

  /**
   * Determines if two maps are equal.
   * 
   * @param first  the first map.
   * @param second the second map.
   * @return true if equal, false otherwise.
   */
  public static boolean equals(Map<Object, Object> first,
      Map<Object, Object> second)
  {
    if (first.size() != second.size())
    {
      return false;
    }
    return first.entrySet().stream()
        .allMatch(e -> e.getValue().equals(second.get(e.getKey())));
  }

  /**
   * Returns a String of substrings gathered from the given list.
   * Each substring is generated via each entry's 'toString()' method
   * and will be separated by the given separator string.
   * @param listObj source list of entries.
   * @param sepStr separator string value.
   * @return A string of substrings, or an empty string if no substrings
   * could be generated (will not return null).
   */
  public static String listObjToListStr(List listObj, String sepStr)
  {
    if(listObj == null)           //if null parameter then
      return EMPTY_STRING;        //return empty string
    final StringBuffer buffObj = new StringBuffer();
    final Iterator iterObj = listObj.iterator();
    Object obj;
    if(iterObj.hasNext())
    {  //list not empty
      while(true)
      {  //for each entry in list
        if((obj=iterObj.next()) != null)    //if not null then
          buffObj.append(obj.toString());   //add entry to string
        if(!iterObj.hasNext())    //if no more entries then
          break;                  //exit loop
        buffObj.append(sepStr);             //add separator to string
      }
    }
    return buffObj.toString();    //convert buffer to string and return
  }

  /**
   * Returns a Vector of substrings from the given string.  Each
   * substring is surrounded by double-quotes ("") and separated by
   * the given separator character.  Special characters, such as
   * double-quote and control characters, may be "quoted" in the
   * substrings using the backslash character (i.e. \").
   * @param str the source string.
   * @param sepChar the character that separates the substrings.
   * @param strictFlag if true then the string format is strictly
   * interpreted--no extraneous spaces are allowed and every substring
   * must be surrounded by double-quotes.
   * @return A Vector of strings, or null if the proper string
   * format was not found.
   */
  public static Vector quotedStringsToVector(String str,char sepChar,
                                                         boolean strictFlag)
  {
    if(str == null)
      return null;
    final Vector retVec = new Vector();     //Vector object to be returned
    final int strLen;
    if((strLen=str.length()) <= 0)
      return retVec;
    int sPos=0,ePos,p;
    String subStr;
    do
    {    //for each substring in source string; find next separator char
      if((ePos=findCharPos(str,sepChar,sPos)) < sPos)
        ePos = strLen;  //if separator char not found then use end of string
      subStr = str.substring(sPos,ePos);
      if(strictFlag)
      {       //string is to be strictly interpreted
        if((p=subStr.length()) <= 0)
          break;        //if empty substring then exit loop
                             //check for quote characters (""):
        if(p < 2 || subStr.charAt(0) != '\"' || subStr.charAt(p-1) != '\"')
        {     //leading and trailing double-quote chars not found
          return null;            //return error
        }
      }
      else    //string not strictly interpreted
      {
        subStr = subStr.trim();        //trim leading and trailing spaces
        if(subStr.length() <= 0)
          break;        //if empty substring then exit loop
      }
      subStr = removeQuoteChars(subStr,true);    //remove quote characters
      retVec.add(subStr);              //add substring to return Vector
    }              //move start position past separator character
    while((sPos=ePos+1) < strLen);     //loop while more data in string
    return retVec;           //return Vector of substrings
  }

  /**
   * Returns a Vector of substrings from the given string.  Each
   * substring is surrounded by double-quotes ("") and separated by
   * the given separator character.  Special characters, such as
   * double-quote and control characters, may be "quoted" in the
   * substrings using the backslash character (i.e. \").  The string
   * format is strictly interpreted--no extraneous spaces are allowed
   * and every substring must be surrounded by double-quotes.
   * @param str the source string.
   * @param sepChar the character that separates the substrings.
   * @return A Vector of strings, or null if the proper string
   * format was not found.
   */
  public static Vector quotedStringsToVector(String str,char sepChar)
  {
    return quotedStringsToVector(str,sepChar,true);
  }

  /**
   * Returns a Vector of substrings from the given string.  Each
   * substring is surrounded by double-quotes ("") and separated by
   * commas (,).  Special characters, such as double-quote and control
   * characters, may be "quoted" in the substrings using the backslash
   * character (i.e. \").  The string format is strictly interpreted--
   * no extraneous spaces are allowed and every substring must be
   * surrounded by double-quotes.
   * @param str the source string.
   * @return A Vector of strings, or null if the proper string
   * format was not found.
   */
  public static Vector quotedStringsToVector(String str)
  {
    return quotedStringsToVector(str,',',true);
  }

  /**
   * Returns a Vector of substrings from the given string.  Each
   * substring is surrounded by double-quotes ("") and separated by
   * one of the the given separator characters.  Special characters,
   * such as  double-quote and control characters, may be "quoted"
   * in the substrings using the backslash character (i.e. \").
   * @param str the source string.
   * @param sepCharsStr a string of separator characters that may be
   * used to separate the substrings, or null for " ,;".
   * @param strictFlag if true then the string format is strictly
   * interpreted--no extraneous spaces are allowed and every substring
   * must be surrounded by double-quotes.
   * @return A Vector of strings, or null if the proper string
   * format was not found.
   */
  public static Vector quotedStringsToVector(String str,String sepCharsStr,
                                                         boolean strictFlag)
  {
    if(str == null)
      return null;
    if(sepCharsStr == null || sepCharsStr.length() <= 0)
      sepCharsStr = " ,;";        //if no separator data then put in default
    final Vector retVec = new Vector();     //Vector object to be returned
    final int strLen;
    if((strLen=str.length()) <= 0)
      return retVec;
    int sPos=0,ePos,p;
    String subStr;
    do
    {    //for each substring in source string
      if(!strictFlag)   //if not strict then scan though any leading spaces
        while(Character.isWhitespace(str.charAt(sPos)) && ++sPos < strLen-1);
      if((ePos=findCharPos(str,sepCharsStr,sPos)) < sPos)
        ePos = strLen;  //if separator char not found then use end of string
      subStr = str.substring(sPos,ePos);
      if(strictFlag)
      {       //string is to be strictly interpreted
        if((p=subStr.length()) <= 0)
          break;        //if empty substring then exit loop
                             //check for quote characters (""):
        if(p < 2 || subStr.charAt(0) != '\"' || subStr.charAt(p-1) != '\"')
        {     //leading and trailing double-quote chars not found
          return null;            //return error
        }
      }
      else    //string not strictly interpreted
      {                 //scan though any spaces after separator:
        while(ePos < strLen-1 && Character.isWhitespace(str.charAt(ePos+1)))
          ++ePos;
        subStr = subStr.trim();        //trim leading and trailing spaces
        if(subStr.length() <= 0)
          break;        //if empty substring then exit loop
      }
      subStr = removeQuoteChars(subStr,true);    //remove quote characters
      retVec.add(subStr);              //add substring to return Vector
    }              //move start position past separator character
    while((sPos=ePos+1) < strLen);     //loop while more data in string
    return retVec;           //return Vector of substrings
  }

  /**
   * Returns a Vector of substrings from the given list string.  The
   * substrings are expected to be separated by a ',' separator
   * character and may be surrounded by quotes (which are removed,
   * and special characters, such as double-quote and control
   * characters, may be "quoted" in the substrings using the backslash
   * character [i.e. \"]).
   * @param str the source string.
   * @return A Vector of strings, or an empty Vector if no data was
   * found in the given list string.
   */
  public static Vector listStringToVector(String str)
  {
    return listStringToVector(str, ',', true);
  }

  /**
   * Returns a Vector of substrings from the given list string.  The
   * substrings are expected to be separated by the given separator
   * character and may be surrounded by quotes (which are removed,
   * and special characters, such as double-quote and control
   * characters, may be "quoted" in the substrings using the backslash
   * character [i.e. \"]).
   * @param str the source string.
   * @param sepChar the character that separates the substrings.
   * @param trimFlag if true then all leading and trailing spaces in
   * substrings is trimmed.
   * @return A Vector of strings, or an empty Vector if no data was
   * found in the given list string.
   */
  public static Vector listStringToVector(String str,char sepChar,
                                                           boolean trimFlag)
  {
    final Vector retVec = new Vector();     //Vector object to be returned
    if(str == null)
      return retVec;
    final int strLen;
    if((strLen=str.length()) <= 0)
      return retVec;
    int sPos=0,ePos;
    String subStr;
    do
    {    //for each substring in source string; find next separator char
      if((ePos=findCharPos(str,sepChar,sPos)) < sPos)
        ePos = strLen;  //if separator char not found then use end of string
      subStr = str.substring(sPos,ePos);
      if(trimFlag)                     //if flag then
        subStr = subStr.trim();        //trim leading and trailing spaces
//      if(subStr.length() <= 0)
//        break;        //if empty substring then exit loop
      if(subStr.length() > 0 && subStr.charAt(0) == '\"')
        subStr = removeQuoteChars(subStr,true);  //remove quote characters
      retVec.add(subStr);              //add substring to return Vector
    }              //move start position past separator character
    while((sPos=ePos+1) < strLen);     //loop while more data in string
    return retVec;           //return Vector of substrings
  }

  /**
   * Load the properties from the specified file the specified character
   * encoding.
   * <p>
   * Unlike {@code java.util.Properties#load(Reader)} escape sequences are not
   * supported, the backslash character is not interpreted as a special
   * character and values cannot span multiple lines.
   * 
   * @param props       the properties.
   * @param inputStream the input stream.
   * @param cs          A charset or null for the default ISO 8859-1.
   * @return the properties.
   * @throws IOException If an I/O error occurs
   * 
   * @see #load(Properties, InputStream)
   */
  public static Properties load(Properties props, File file,
      Charset cs) throws IOException {
    if (cs == null) {
      cs = ISO_8859_1;
    }
    try (BufferedReader br = new BufferedReader(
        new InputStreamReader(new FileInputStream(file), cs))) {
      load(props, br);
    }
    return props;
  }

  /**
   * Load the properties from the specified input stream with the specified
   * character encoding.
   * <p>
   * Unlike {@code java.util.Properties#load(Reader)} escape sequences are not
   * supported, the backslash character is not interpreted as a special
   * character and values cannot span multiple lines.
   * 
   * @param props       the properties.
   * @param inputStream the input stream.
   * @param cs          A charset or null for the default ISO 8859-1.
   * @return the properties.
   * @throws IOException If an I/O error occurs
   * 
   * @see #load(Properties, InputStream)
   */
  public static Properties load(Properties props, InputStream inputStream,
      Charset cs) throws IOException {
    if (cs == null) {
      cs = ISO_8859_1;
    }
    try (BufferedReader br = new BufferedReader(
        new InputStreamReader(inputStream, cs))) {
      load(props, br);
    }
    return props;
  }

  /**
   * Load the properties from the specified reader with the specified character
   * encoding.
   * <p>
   * Unlike {@code java.util.Properties#load(Reader)} escape sequences are not
   * supported, the backslash character is not interpreted as a special
   * character and values cannot span multiple lines.
   * 
   * @param props       the properties.
   * @param inputStream the input stream.
   * @return the properties.
   * @throws IOException If an I/O error occurs
   * 
   * @see #load(Properties, InputStream)
   */
  public static Properties load(Properties props, Reader reader)
      throws IOException {
    String s;
    char ch;
    int index, spaceIndex;
    BufferedReader br;
    if (reader instanceof BufferedReader) {
      br = (BufferedReader) reader;
    } else {
      br = new BufferedReader(reader);
    }
    while ((s = br.readLine()) != null) {
      s = stripLeading(s);
      if (s.isEmpty()) {
        continue; // ignore empty line
      }
      ch = s.charAt(0);
      if (ch == '#' || ch == '!') {
        continue; // ignore comment
      }
      index = s.indexOf('=');
      spaceIndex = s.indexOf(' ');
      if (spaceIndex != -1) // if space found
      {
        if (index == -1) // if no equals
        {
          index = spaceIndex; // use the space index
        } else if (spaceIndex < index) // space is before equals
        {
          // look for non-whitespace character before equals
          for (int i = spaceIndex + 1; i < index; i++) {
            if (!Character.isWhitespace(s.charAt(i))) {
              index = i - 1; // set index to space before
              break;
            }
          }
        }
      }
      if (index == -1) // if no value
      {
        props.setProperty(strip(s), EMPTY_STRING);
      } else {
        props.setProperty(strip(s.substring(0, index)),
            stripLeading(s.substring(index + 1)).replace("\\\\", "\\"));
      }
    }
    return props;
  }

  /**
   * Moves the specified element to the specified position in the specified list
   * if it already present, otherwise it adds it at the position.
   * Each component in the list with an index greater or equal to the
   * specified <code>index</code> is shifted upward to have an index
   * one greater than the value it had previously. <p>
   *
   * If the index is out of range the element is moved to the end of the list.
   *
   * @param listObj the list object.
   * @param index index at which the specified element is to be inserted.
   * @param element element to be inserted.
   * @return true if the list changed as a result of the call.
   *
   * @throws UnsupportedOperationException if the <tt>add</tt> method is not
   * 		  supported by the list.
   * @throws ClassCastException if the class of the specified element
   * 		  prevents it from being added to the list.
   * @throws IllegalArgumentException if some aspect of this element
   *            prevents it from being added to the list.
   */
  public static boolean moveListElement(List listObj, int index,
                                                             Object element)
  {
    final int currentIndex = listObj.indexOf(element);
    if (currentIndex != index)  //if the element is not already at the position
    {
      if (currentIndex >= 0)  //if element is already in the vector
        listObj.remove(currentIndex);  //remove it from the current position
      try
      {
        listObj.add(index, element);
      }
      catch (Exception ex)
      {
        listObj.add(element);
      }
      return true;
    }
    return false;
  }

  /**
   * Creates a string of "key" = "value" items from the given table/map.
   * @param tableObj the table/map object to use.
   * @param sepStr separator to put between items.
   * @return A string of "key" = "value" items from the given table/map.
   */
  public static String getTableContentsString(Map tableObj, String sepStr)
  {
    final StringBuffer buff = new StringBuffer();
    try
    {
      final Iterator iterObj = tableObj.keySet().iterator();
      if(iterObj.hasNext())
      {  //table contains data
        Object obj;
        while(true)
        {     //for each "key" = "value" item in table
          buff.append(QUOTE_STRING);             //leading quote
          buff.append(obj=iterObj.next());       //key
          buff.append("\" = \"");                //quote, =, quote
          buff.append(tableObj.get(obj));        //value
          buff.append(QUOTE_STRING);             //trailing quote
          if(!iterObj.hasNext())
            break;           //if not more items then exit loop
          buff.append(sepStr);                   //separator
        }
      }
    }
    catch(Exception ex)
    {         //if error then return message string
      return "getTableContentsString() error:  " + ex;
    }
    return buff.toString();       //return string version of buffer
  }

  /**
   * Creates a string of "key" = "value" items from the given table/map,
   * with ", " separating the items.
   * @param tableObj the table/map object to use.
   * @return A string of "key" = "value" items from the given table/map.
   */
  public static String getTableContentsString(Map tableObj)
  {
    return getTableContentsString(tableObj,", ");
  }

  /**
   * Removes items from the given Vector that are empty string or null.
   * @param vec the given Vector.
   * @return The given Vector.
   */
  public static Vector removeEmptyStrings(Vector vec)
  {
    if(vec == null)          //if null handle then
      return null;           //return null
    final Enumeration e = vec.elements();
    Object obj;
    while(e.hasMoreElements())
    {    //for each item in Vector
      if(((obj=e.nextElement()) instanceof String &&
                                ((String)obj).length() <= 0) || obj == null)
      {  //item is empty string or null
        vec.remove(obj);     //remove item
      }
    }
    return vec;              //return Vector
  }

  /**
   * Returns the given string "fixed" to the desired length, (possibly)
   * truncating or adding spaces as necessary.
   * @param inString the given string.
   * @param outLen the desired length of the output string.
   * @param rightJustFlag true for right-justified (spaces inserted
   * before the given string data); false for left-justified (spaces
   * added after the given string data).
   * @param truncateFlag if true and the given string is longer than the
   * desired length then it is truncated to generate the returned string;
   * if false then the string data is never truncated.
   * @return A String object with the given length.
   */
  public static String fixStringLen(String inString,int outLen,
                                 boolean rightJustFlag,boolean truncateFlag)
  {
    if(outLen <= 0)          //if non-positive length then
      return EMPTY_STRING;   //return empty string
    if(inString == null)     //if null string handle given then
      inString = EMPTY_STRING;  //change to empty string
    final int len = inString.length();      //get length of given string
    final int bLen;
    if((bLen=(outLen-len)) <= 0)
    {    //length of given string is greater than or equal to desired length
              //if flag then truncate; else return original string:
      return truncateFlag ? inString.substring(0,outLen) : inString;
    }
         //length of given string is smaller than desired length
              //pad given string data with spaces and return new string:
    return rightJustFlag ? (getSpacerString(bLen) + inString) :
                                         (inString + getSpacerString(bLen));
  }

  /**
   * Returns the given string "fixed" to the desired length,
   * truncating or adding spaces as necessary.
   * @param inString the given string.
   * @param outLen the desired length of the output string.
   * @param rightJustFlag true for right-justified (spaces inserted
   * before the given string data); false for left-justified (spaces
   * added after the given string data).
   * @return A String object with the given length.
   */
  public static String fixStringLen(String inString,int outLen,
                                                      boolean rightJustFlag)
  {
    return fixStringLen(inString,outLen,rightJustFlag,true);
  }

  /**
   * Returns the given string "fixed" to the desired length,
   * truncating or adding spaces as necessary.  If spaces are added,
   * the returned string is left-justified (spaces added after the
   * given string data).
   * @param inString the given string.
   * @param outLen the desired length of the output string.
   * @return A String object with the given length.
   */
  public static String fixStringLen(String inString,int outLen)
  {
    return fixStringLen(inString,outLen,false,true);
  }

  /**
   * Returns a string containing the given number of space characters.
   * This method is thread safe.
   * @param numSpaces the desired number of space characters.
   * @return A string containing the desired number of space characters.
   */
  public static String getSpacerString(int numSpaces)
  {
    synchronized(blanksStringSyncObj)
    {    //only allow one thread access at a time
      if(numSpaces > blanksStringLen)
      {  //required # of spaces is larger than current 'blanksString'
            //create array of 'numSpaces' blanks & set new 'blanksStringLen'
        final char [] charArr = new char [blanksStringLen=numSpaces];
        Arrays.fill(charArr,' ');           //fill char array with spaces
        blanksString = new String(charArr); //create string of spaces
      }
              //return substring of blanks string at desired length:
      return blanksString.substring(0,numSpaces);
    }
  }

  /**
   * Returns a "centered" version of the given string.  Leading spaces
   * are added to "center" the string into the given field length.
   * @param inString the given string.
   * @param fieldLen the field length for centering.
   * @return A "centered" version of the given string, or the given
   * string itself if it is not shorter than the field length.
   */
  public static String createCenteredString(String inString, int fieldLen)
  {
    final int sLen = inString.length();
    final int rmdr;
              //calc remainder of space in field; if greater than zero then
              // return string with "centering" spaces inserted on left:
    if((rmdr=fieldLen-sLen) > 0)
      return UtilFns.fixStringLen(inString,sLen+rmdr/2,true,false);
    return inString;         //given string no shorter than field; return it
  }

  /**
   * Performs a "soft" word-wrap of the given data.  If a line has to be
   * shortened more than 20% of the given line-width value then it is
   * allowed to go beyond the given line-width value.
   * @param srcStr data string to use.
   * @param lineWidth target line-width value.
   * @param indentStr indentation to be put in front of generated lines
   * (after the first one), or null for none.
   * @param newLineStr string to use for generate line-ends.
   * @param sepCharsStr string of characters to be interpreted as word
   * separators, or null to use any character <= the space character.
   * @return A new string containing the word-wrapped data.
   */
  public static String softWordWrapString(String srcStr, int lineWidth,
                    String indentStr, String newLineStr, String sepCharsStr)
  {
    if(lineWidth <= 0)
      lineWidth = 40;
    if(indentStr == null)
      indentStr = "";
    final boolean spaceSepFlag =       //set flag if <= ' ' for separator
                         (sepCharsStr == null || sepCharsStr.length() <= 0);
    srcStr = trimEndStr(srcStr);
    final int srcStrLen = srcStr.length();
    int p, chkEPos, sPos = 0, ePos;
    char ch;
    final int minBreakPos = lineWidth / 5;
    final StringBuffer buff = new StringBuffer();
    outerLoop:          //label for 'break' statement
    do
    {  //for each line-wrap
      ePos = sPos + lineWidth;    //setup max-line-width position
              //check for line-ends in line data:
      chkEPos = (ePos < srcStrLen) ? ePos : srcStrLen;
      for(p=sPos; p<chkEPos; ++p)
      {  //for each character in line to be checked
        if(srcStr.charAt(p) == '\n')
        {     //line-end character found
              //add data to buffer, up to end of line:
          buff.append(trimEndStr(srcStr.substring(sPos,p)));
          buff.append(newLineStr);
          if((sPos=p+1) >= srcStrLen)  //reset start position
            break outerLoop;      //if end of data then exit method
          buff.append(indentStr);      //indent before next line
          ePos = sPos + lineWidth;     //setup max-line-width position
          chkEPos = (ePos < srcStrLen) ? ePos : srcStrLen;
        }
      }
      if(ePos >= srcStrLen)
      {  //rest of source string fits within line-width
        buff.append(trimEndStr(srcStr.substring(sPos)));
        break;
      }
      ++ePos;
      while(true)            //scan backwards for separators
      {  //for each character scanned backwards
        ch = srcStr.charAt(--ePos);    //check if char is separator
        if(spaceSepFlag ? (ch <= ' ') : (sepCharsStr.indexOf(ch) >= 0))
        {  //separator found; do line break
          buff.append(trimEndStr(srcStr.substring(sPos,ePos+1)));
          buff.append(newLineStr);
          buff.append(indentStr);
          break;
        }
        if(ePos <= minBreakPos)
        {  //minimum backwards position reached
          ePos = sPos + lineWidth;
          while(true)        //scan forwards for separators
          {  //for each character scanned forwards
            if(++ePos >= srcStrLen)
            {  //end of source string reached
              buff.append(trimEndStr(srcStr.substring(sPos)));
              break;
            }
            ch = srcStr.charAt(ePos);  //check if char is separator
            if(spaceSepFlag ? (ch <= ' ') : (sepCharsStr.indexOf(ch) >= 0))
            {  //separator found; do line break
              buff.append(trimEndStr(srcStr.substring(sPos,ePos+1)));
              buff.append(newLineStr);
              buff.append(indentStr);
              break;
            }
          }
          break;
        }
      }
    }
    while((sPos=ePos+1) < srcStrLen);
    return buff.toString();
  }

  /**
   * Performs a "soft" word-wrap of the given data.  If a line has to be
   * shortened more than 20% of the given line-width value then it is
   * allowed to go beyond the given line-width value.
   * @param srcStr data string to use.
   * @param lineWidth target line-width value.
   * @param indentStr indentation to be put in front of generated lines
   * (after the first one), or null for none.
   * @param newLineStr string to use for generate line-ends.
   * @return A new string containing the word-wrapped data.
   */
  public static String softWordWrapString(String srcStr, int lineWidth,
                                        String indentStr, String newLineStr)
  {
    return softWordWrapString(srcStr,lineWidth,indentStr,newLineStr,null);
  }

  /**
   * Performs a "soft" word-wrap of the given data.  If a line has to be
   * shortened more than 20% of the given line-width value then it is
   * allowed to go beyond the given line-width value.
   * @param srcStr data string to use.
   * @param lineWidth target line-width value.
   * @param indentStr indentation to be put in front of generated lines
   * (after the first one), or null for none.
   * @return A new string containing the word-wrapped data.
   */
  public static String softWordWrapString(String srcStr, int lineWidth,
                                                           String indentStr)
  {
    return softWordWrapString(srcStr,lineWidth,indentStr,newline,null);
  }

  /**
   * Trims whitespace at the end of the given string.
   * @param str string to use.
   * @return The given string with its trailing whitespace trimmed,
   * or the string itself if no trailing whitespace was found.
   */
  public static String trimEndStr(String str)
  {
    int p = str.length();
    if(p > 0 && Character.isWhitespace(str.charAt(--p)))
    {    //whitespace character found at end; scan back for more
      while(--p >= 0 && Character.isWhitespace(str.charAt(p)));
      return str.substring(0,p+1);     //trim trailing whitespace and return
    }
    return str;
  }

  /**
   * Trims leading and trailing whitespace from each line inside
   * of the given string (while retaining the line separations).
   * This can be useful when processing multi-line text that has
   * been read from an XML element.  If 'centerFieldLen' is greater
   * than zero then leading spaces are added to "center" the lines
   * into the given field length.
   * @param inString the given string.
   * @param newLineStr the newline separator to be used between lines
   * in the returned string.
   * @param centerFieldLen the field length for centering, or 0
   * for no centering.
   * @return The trimmed multi-line string.
   */
  public static String trimMultiLineString(String inString,
                                      String newLineStr, int centerFieldLen)
  {
    final StringBuffer buff = new StringBuffer();
    if(inString != null)
    {    //given string contains data
      inString = inString.trim();      //trim leading/trailing whitespace
      final int inStrLen = inString.length();
      String str;
      int ePos, sPos = 0;
      while(true)
      {  //for each "line" of data in string; find next newline
        if((ePos=inString.indexOf('\n',sPos)) < 0)
          ePos = inStrLen;   //if none found then use end-of-string position
        str = inString.substring(sPos,ePos).trim();
        if((str.length()) > 0)
        {     //line sub-string contains data
          if(centerFieldLen > 0)  //if >0 then insert spaces for centering
            str = createCenteredString(str,centerFieldLen);
          buff.append(str);            //add string to buffer
        }
        if((sPos=ePos+1) >= inStrLen)
          break;             //if no more data then exit loop
        buff.append(newLineStr);       //add newline separator
      }
    }
    return buff.toString();       //return string version of buffer
  }

  /**
   * Trims leading and trailing whitespace from each line inside
   * of the given string (while retaining the line separations).
   * This can be useful when processing multi-line text that has
   * been read from an XML element.
   * @param inString the given string.
   * @param newLineStr the newline separator to be used between lines
   * in the returned string.
   * @return The trimmed multi-line string.
   */
  public static String trimMultiLineString(String inString,
                                                          String newLineStr)
  {
    return trimMultiLineString(inString,newLineStr,0);
  }

  /**
   * Trims leading and trailing whitespace from each line inside
   * of the given string (while retaining the line separations).
   * This can be useful when processing multi-line text that has
   * been read from an XML element.  If 'centerFieldLen' is greater
   * than zero then leading spaces are added to "center" the lines
   * into the given field length.  The lines in the returned string
   * will be separated via the newline ('\n') character.
   * @param inString the given string.
   * @param centerFieldLen the field length for centering, or 0
   * for no centering.
   * @return The trimmed multi-line string.
   */
  public static String trimMultiLineString(String inString,
                                                         int centerFieldLen)
  {
    return trimMultiLineString(inString,"\n",centerFieldLen);
  }

  /**
   * Trims leading and trailing whitespace from each line inside
   * of the given string (while retaining the line separations).
   * This can be useful when processing multi-line text that has
   * been read from an XML element.  The lines in the returned
   * string will be separated via the newline ('\n') character.
   * @param inString the given string.
   * @return The trimmed multi-line string.
   */
  public static String trimMultiLineString(String inString)
  {
    return trimMultiLineString(inString,0);
  }

  /**
   * Returns a new version of the given string that has its first
   * character capitialized (if possible).
   * @param str the string to use.
   * @return A new String object if the capitialization changed the
   * given string, otherwise the original string.
   */
  public static String capFirstLetter(String str)
  {
    final char ch;
    if(str == null || str.length() <= 0 ||
        !Character.isLetter(ch=str.charAt(0)) || !Character.isLowerCase(ch))
    {    //no data or not starting with lower-case letter
      return str;            //just return original string
    }
              //convert first character to upper-case and return new string:
    return Character.toUpperCase(ch) + str.substring(1);
  }

  /**
   * Returns a pretty version of the specified string:
   * Replaces periods with spaces.
   * Capatilizes the first letter of the first word.
   * Removes control characters.
   * Removes white space from beginning and end of string.
   * Ensures only a single space between words.
   * Ensures that there is a space before every capital letter
   * if the previous or next letter is lower case.
   * @param str the string.
   * @param allUpperFlag true to have all words start with upper-case,
   * false to have only first word start with upper-case.
   * @return pretty string.
   */
  public static String getPrettyString(String str,boolean allUpperFlag)
  {
    //exit if no string
    if(str == null)
      return null;
    //replace periods with spaces
    str = str.replace('.', ' ');
    //replace underscores with spaces
    str = str.replace('_', ' ');
    //remove white space
    str = str.trim();
    //exit if no data
    if(str.length() <= 0)
      return str;            //return the empty string

    final char[] chars = str.toCharArray();
    String pretty = "";

    int i = 0;
    boolean addSpaceFlag = false;
    char ch = chars[i++];

    //capatilizes the first letter of the first word
    if(Character.isLowerCase(ch))
      ch = Character.toUpperCase(ch);

    //add the character to the pretty string
    pretty += ch;

    if (str.length() <= 1)  //done if only 1 character
      return pretty;

    for (char saveCh = ch, nextCh = chars[i++]; i <= chars.length; i++)
    {
      final char prevCh = saveCh;
      ch = saveCh = nextCh;

      nextCh = (i >= chars.length) ? 0 : chars[i];

      if(Character.isWhitespace(ch))       //if character is white space
      {
        //set flag so that we add a space when whitespace ends
        addSpaceFlag = true;
      }
      else if(!Character.isISOControl(ch)) //character is not control
      {
        if(addSpaceFlag) //if we need to add space
        {
          addSpaceFlag = false; //clear the add space flag
          pretty += ' ';        //add the space to the pretty string
          if(allUpperFlag)
          {   //flag set; capitalize the first letter of every word
            if(Character.isLowerCase(ch))
              ch = Character.toUpperCase(ch);
          }
          else
          {   //flag set; lower-case the first letter of every word
            if(Character.isUpperCase(ch) && Character.isLowerCase(nextCh))
              ch = Character.toLowerCase(ch);
          }
        }
        else
        {
          //add a space before every capital letter
          //if the previous or next letter is lower case
          if(Character.isUpperCase(ch) && (Character.isLowerCase(prevCh) ||
                                             Character.isLowerCase(nextCh)))
          {
            pretty += ' ';
            //if not all upper then lower-case the first letter of every word
            if(!allUpperFlag && Character.isLowerCase(nextCh))
              ch = Character.toLowerCase(ch);    //lower-case first letter
          }
        }
        //add the character to the pretty string
        pretty += ch;
      }
    }
    return pretty;
  }

  /**
   * Returns a pretty version of the specified string:
   * Capatilizes the first letter of every word.
   * Removes control characters.
   * Removes white space from beginning and end of string.
   * Ensures only a single space between words.
   * Ensures that there is a space before every capital letter
   * if the previous or next letter is lower case.
   * @param str the string.
   * @return pretty string.
   */
  public static String getPrettyString(String str)
  {
    return getPrettyString(str,true);
  }

  /**
   * Returns a string containing the given single character.  This method
   * can be useful because "Character.toString()" is only available
   * on Java 1.4 and later.
   * @param ch the character to use.
   * @return A new String object containing the given single character.
   */
  public static String charToString(char ch)
  {
    return String.valueOf(new char [] { ch } );
  }

  /**
   * Returns an Internet-address object for the local host.  If multiple
   * IPs are available, then an Internet-address object with a "valid"
   * Internet IP address will be returned in favor of one corresponding
   * to an "invalid" Internet IP address (such as "192.168...").
   * @param preferIP4Flag true to give preference to "valid" IPv4
   * addresses over "valid" IPv6 addresses.
   * @param addrInfoBuff string buffer to be appended with informational
   * text on the host addresses that were found.
   * @return A new 'InetAddress' object, or null if one could not be
   * created.
   */
  public static InetAddress getLocalHostAddrObj(boolean preferIP4Flag,
                                                  StringBuffer addrInfoBuff)
  {
    InetAddress localHostAddr = null;
    try
    {         //get name/address of local machine:
      localHostAddr = InetAddress.getLocalHost();
      final InetAddress [] addrArr =   //get all addresses for host
                      InetAddress.getAllByName(localHostAddr.getHostName());
              //check through (possibly) multiple host addresses until one
              // is found that does not match any address prefixes in the
              // 'FAKE_IP_ADDRS' list (as these are "internal" addresses):
      String str;
      final String sepStr = "; ";
      if(preferIP4Flag)
      {  //flag set for IPv4 preference; first look for IPv4 address only
        for(int pos=0; pos<addrArr.length; ++pos)
        {  //for each local host address
          if(addrArr[pos] != null && addrArr[pos] instanceof Inet4Address &&
                                     !(addrArr[pos].isLinkLocalAddress()) &&
                                     !(addrArr[pos].isSiteLocalAddress()) &&
                              (str=addrArr[pos].getHostAddress()) != null &&
                                                      !isFakeIPAddress(str))
          {  //string version of IP address fetched OK and not "fake"
            if(addrInfoBuff != null)
            {  //info buffer was given; enter text for addresses
              addrInfoBuff.append(addrArr[pos]); //put selected addr first
              for(int i=0; i<pos; ++i)           //put in rest of addresses
                addrInfoBuff.append(sepStr + addrArr[i]);
              for(int i=pos+1; i<addrArr.length; ++i)
                addrInfoBuff.append(sepStr + addrArr[i]);
            }
            return addrArr[pos];       //return address object
          }
        }
      }
      for(int pos=0; pos<addrArr.length; ++pos)
      {  //for each local host address
        if(addrArr[pos] != null && !(addrArr[pos].isLinkLocalAddress()) &&
                                     !(addrArr[pos].isSiteLocalAddress()) &&
                              (str=addrArr[pos].getHostAddress()) != null &&
                                                      !isFakeIPAddress(str))
        {  //string version of IP address fetched OK and not "fake"
          if(addrInfoBuff != null)
          {  //info buffer was given; enter text for addresses
            addrInfoBuff.append(addrArr[pos]);   //put selected addr first
            for(int i=0; i<pos; ++i)             //put in rest of addresses
              addrInfoBuff.append(sepStr + addrArr[i]);
            for(int i=pos+1; i<addrArr.length; ++i)
              addrInfoBuff.append(sepStr + addrArr[i]);
          }
          return addrArr[pos];         //return address object
        }
      }
      if(preferIP4Flag)
      {  //flag set for IPv4 pref; take first IPv4 address (even if fake)
        for(int pos=0; pos<addrArr.length; ++pos)
        {  //for each local host address
          if(addrArr[pos] != null && addrArr[pos] instanceof Inet4Address &&
                                        !(addrArr[pos].isLoopbackAddress()))
          {  //IPv4 address found and is not "127.x.x.x" address
            if(addrInfoBuff != null)
            {  //info buffer was given; enter text for addresses
              addrInfoBuff.append(addrArr[pos]); //put selected addr first
              for(int i=0; i<pos; ++i)           //put in rest of addresses
                addrInfoBuff.append(sepStr + addrArr[i]);
              for(int i=pos+1; i<addrArr.length; ++i)
                addrInfoBuff.append(sepStr + addrArr[i]);
            }
            return addrArr[pos];       //return address object
          }
        }
      }
      if(addrInfoBuff != null && addrArr.length > 0 && addrArr[0] != null)
      {  //info buffer was given and addresses exist; enter text
        addrInfoBuff.append(addrArr[0]);         //put in first address
        for(int i=1; i<addrArr.length; ++i)      //put in rest of addresses
          addrInfoBuff.append(sepStr + addrArr[i]);
      }
    }
    catch(Exception ex) {}   //if error then return default address object
    return localHostAddr;         //return local-host address object
  }

  /**
   * Returns an Internet-address object for the local host.  If multiple
   * IPs are available, then an Internet-address object with a "valid"
   * Internet IP address will be returned in favor of one corresponding
   * to an "invalid" Internet IP address (such as "192.168...").
   * @param preferIP4Flag true to give preference to "valid" IPv4
   * addresses over "valid" IPv6 addresses.
   * @return A new 'InetAddress' object, or null if one could not be
   * created.
   */
  public static InetAddress getLocalHostAddrObj(boolean preferIP4Flag)
  {
    return getLocalHostAddrObj(preferIP4Flag,null);
  }

  /**
   * Returns an Internet-address object for the local host.  If multiple
   * IPs are available, then an Internet-address object with a "valid"
   * Internet IP address will be returned in favor of one corresponding
   * to an "invalid" Internet IP address (such as "192.168...").
   * @return A new 'InetAddress' object, or null if one could not be
   * created.
   */
  public static InetAddress getLocalHostAddrObj()
  {
    return getLocalHostAddrObj(false,null);
  }

  /**
   * Returns the local host IP.  If multiple IPs are available, then
   * an IP corresponding to a "valid" Internet IP address will be
   * returned in favor of one corresponding to an "invalid" Internet
   * IP address (such as "192.168...").  If no host IP can be found
   * then an empty string is returned.
   * @return A string containing the local host IP, or an empty string.
   */
  public static String getLocalHostIP()
  {
    return getLocalHostIP(getLocalHostAddrObj());
  }

  /**
   * Returns the host IP address for the given address object.  If no
   * host IP can be found then an empty string is returned.
   * @param addrObj Internet-address object.
   * @return A string containing the local host IP, or an empty string.
   * @see getLocalHostAddrObj
   */
  public static String getLocalHostIP(InetAddress addrObj)
  {
    String localHostIPString = UtilFns.EMPTY_STRING;
    if (addrObj != null)  //if address exists
    {
      try
      {       //determine local IP address:
        localHostIPString = addrObj.getHostAddress();
      }
      catch(Exception ex)
      {  //some kind of exception error; log it
        toGlobalLogFileWarn("Error determining local IP address:  " + ex);
        toGlobalLogFileWarn(UtilFns.getStackTraceString(ex));
      }
    }
    if(localHostIPString == null)      //make sure value is not null
      localHostIPString = UtilFns.EMPTY_STRING;
    return localHostIPString;
  }

  /**
   * Returns the local host name.
   * @return A string containing the local host name, or an empty
   * string if the host name could not be determined.
   */
  public static String getLocalHostName()
  {
    return getLocalHostName(getLocalHostAddrObj());
  }

  /**
   * Returns the local host name.
   * @param addrObj the Internet-address object.
   * @return A string containing the local host name, or an empty
   * string if the host name could not be determined.
   * @see getLocalHostAddrObj
   */
  public static String getLocalHostName(InetAddress addrObj)
  {
    String localHostNameString = UtilFns.EMPTY_STRING;
    if (addrObj != null)  //if address exists
    {
      try
      {       //determine local host name:
        localHostNameString = addrObj.getHostName();
      }
      catch(Exception ex)
      {  //some kind of exception error; log it
        toGlobalLogFileWarn("Error determining local host name:  " + ex);
        toGlobalLogFileWarn(UtilFns.getStackTraceString(ex));
      }
    }
    if(localHostNameString == null)    //make sure value is not null
      localHostNameString = UtilFns.EMPTY_STRING;
    return localHostNameString;
  }

  /**
   * Get the public IP address.
   * 
   * @param publicIpHosts the public IP hosts or null / empty for the defaults.
   * @return the public IP address or null if none.
   * @see #PUBLIC_IP_HOSTS
   */
  public static InetAddress getPublicIP(String ... publicIpHosts)
  {
    if (publicIpHosts == null || publicIpHosts.length == 0)
    {
      publicIpHosts = PUBLIC_IP_HOSTS;
    }
    for (String publicIPHost : publicIpHosts)
    {
      try (BufferedReader in = new BufferedReader(new InputStreamReader(
          new URL(publicIPHost).openStream())))
      {
        return InetAddress.getByName(in.readLine());
      }
      catch (Exception ex) {
      }
    }
    return null;
  }

    /**
     * Creates a new URL containing the given reference (anchor) string.
     * If the URL already contains a reference then it is replaced.
     * @param urlObj the URL object to use.
     * @param refStr the reference (anchor) string to use, or null to
     * specify no reference.
     * @return A new URL object containing the given reference, or the
     * original URL object if it already contained the given reference.
     */
  public static URL setURLReference(URL urlObj,String refStr)
  {
    String urlStr = urlObj.toString();      //get string version of URL
    String curRefStr;
    if((curRefStr=urlObj.getRef()) != null)
    {    //current URL contains a reference
      if(curRefStr.equals(refStr))     //if same as requested reference then
        return urlObj;                 //return original URL object
      curRefStr = "#" + curRefStr;
      if(urlStr.endsWith(curRefStr))
        urlStr = urlStr.substring(0,urlStr.length()-curRefStr.length());
    }
    else
    {    //current URL does not contain a reference
      if(refStr == null)               //if no reference requested then
        return urlObj;                 //return original URL object
    }
    if(refStr != null)            //if reference string given then
      urlStr += "#" + refStr;     //add to URL string
    try
    {         //create and return new URL:
      return new URL(urlStr);
    }
    catch(MalformedURLException ex)
    {         //some kind of exception error
      return urlObj;              //return original URL object
    }
  }

    /**
     * Returns true if the contents of the given string qualify as a valid
     * URL address; otherwise returns false.  If the string begins with 1
     * to 10 letters (or digits) followed by "://" then it is considered
     * a valid URL address.
     * @param str the string to check.
     * @return true if the contents of the given string qualify as a valid
     * URL address; otherwise false.
     */
  public static boolean isURLAddress(String str)
  {
    if(str == null || str.length() <= 0)
      return false;          //if no string data then return false
    int p;         //find position of "://" in string:
    if((p=str.indexOf("://",1)) <= 0 || p > 10)
      return false;          //if not found then return false
    while(--p >= 0)          //scan backward through leading characters
    {    //for each character before colon
      if(!Character.isLetterOrDigit(str.charAt(p)))
        return false;        //if not a letter or digit then return false
    }
    return true;        //return true for valid URL address
  }

    /**
     * Returns true if the contents of the given string qualify as a valid
     * IP address (in the from #.#.#.#); otherwise returns false.
     * @param str the string to check.
     * @return true if the contents of the given string qualify as a valid
     * IP address; otherwise false.
     */
  public static boolean isIPAddress(String str)
  {
    final int len;
    if(str == null || (len=str.length()) <= 0)
      return false;          //if no string data then return false
    int val, ePos, sPos = 0, count = 0;
    do   //for each integer value in IP address
    {         //find next '.' separator
      if((ePos=str.indexOf('.',sPos)) < 0)
        ePos = len;     //if no '.' then use end of string position
      try
      {       //parse to integer value
        val = Integer.parseInt(str.substring(sPos,ePos));
      }
      catch(NumberFormatException ex)
      {       //error parsing value
        return false;
      }
      if(val < 0 || val > 255)    //if value out of range then
        return false;             //return invalid flag
      ++count;          //increment count of number of values read
    }
    while((sPos=ePos+1) < len);   //loop if more data to process
    return (count == 4) ? true : false;     //return OK if 4 values read
  }

    /**
     * Returns true if the given string contains a "fake" IP address
     * (one that cannot be used as a "real" Internet address).  The
     * address is considered "fake" if it begins with one of the
     * following:  "192.168.", "172.16.", "10.","255.", "127.0.0.",
     * "0.0.0.0", "fe8", "fec", "fed", "fee", "fef", "2001:", "2002:"
     * or "0:0:0:0:0:0:".
     * @param str the string to check.
     * @return true if the given string contains a "fake" IP address;
     * otherwise false.
     */
  public static boolean isFakeIPAddress(String str)
  {
    if(str != null)
    {    //given string not null
      for(int i=0; i<FAKE_IP_ADDRS.length; ++i)
      {  //for each entry in the "fake" list of prefixes
              //if beg of IP addr matches "fake" prefix then return true:
        if(str.startsWith(FAKE_IP_ADDRS[i]))
          return true;
      }
    }
    return false;
  }

    /**
     * Replace the first occurrence of a given substring with a new substring.
     * @param dataStr the data string to be used.
     * @param oldStr the "old" substring to be replaced.
     * @param newStr the "new" substring to be entered.
     * @return A new String containing the "new" substring, or the original
     * String if no occurrences of the "old" substring were found.
     */
  public static String replaceSubstring(String dataStr,String oldStr,
                                                              String newStr)
  {
    final int sPos;
    if((sPos=dataStr.indexOf(oldStr)) < 0)
      return dataStr;
    return dataStr.substring(0,sPos) + newStr +
                                    dataStr.substring(sPos+oldStr.length());
  }

    /**
     * Replace all of the occurrences of a given substring with a new substring.
     * @param dataStr the data string to be used.
     * @param oldStr the "old" substring to be replaced.
     * @param newStr the "new" substring to be entered.
     * @return A new String containing the "new" substring, or the original
     * String if no occurrences of the "old" substring were found.
     */
  public static String replaceAllSubstrings(String dataStr,String oldStr,
                                                              String newStr)
  {
    final int oldStrLen = oldStr.length();
    final StringBuffer sb = new StringBuffer(dataStr);
    int fromIndex = Integer.MAX_VALUE;
    int i;

    while ((i=dataStr.lastIndexOf(oldStr,fromIndex)) >= 0)
    {
      fromIndex = i-1;
      sb.replace(i,i+oldStrLen, newStr);
    }
    return sb.toString();
  }

  /**
   * Returns a new string with all surrounding whitespace removed and
   * internal whitespace normalized to a single space.  If
   * only whitespace exists, an empty string is returned.
   * @param str the string to be normalized.
   * @return A new, normalized-version of the string if whitespace was
   * found and removed; the original string if no whitespace was found;
   * or an empty string if no non-whitespace data was found.
   */
  public static String normalizeString(String str)
  {
    if(str == null)
      return EMPTY_STRING;        //if null then return empty string
         //create string buffer with given data, trimmed:
    final StringBuffer buff = new StringBuffer(str.trim());
    int len;
    if((len=buff.length()) <= 0)
      return EMPTY_STRING;        //if was all whitespace then return ""
         //set "changed" flag if 'trim' changed the length:
    boolean changedFlag = (len != str.length());
    boolean wSpaceFlag = false;
    char ch;
    for(int i=1; i<len; ++i)
    {    //for each character (after the first)
      if(Character.isWhitespace(ch=buff.charAt(i)))
      {  //character is a whitespace character
        if(wSpaceFlag)
        {     //previous character was whitespace
          buff.deleteCharAt(i);        //delete current character
          --len;                       //reduce length of data
          changedFlag = true;          //indicate data changed
        }
        else
        {     //previous character was not whitespace
          if(ch != ' ')
          {   //current character is not a space
            buff.setCharAt(i,' ');     //change it to a space
            changedFlag = true;        //indicate data changed
          }
        }
        wSpaceFlag = true;        //indicate "whitespace" character found
      }
      else    //character is not a whitespace character
        wSpaceFlag = false;       //clear "whitespace" flag
    }
    if(!changedFlag)         //if data not changed then
      return str;            //return original string
    return buff.toString();       //return string version of buffer
  }

  /**
   * Returns a new string with all characters not matching the given
   * keep-characters string removed.
   * @param str string to be processed.
   * @param keepCharsStr string of "keep" characters to be matched.
   * @return A new version of the string if characters were removed,
   * or the original string if no characters were removed.
   */
  public static String removeSpecifiedChars(String str, String keepCharsStr)
  {
    if(str == null)
      return EMPTY_STRING;        //if null then return empty string
    if(keepCharsStr == null)
      return str;
    final StringBuffer buff = new StringBuffer();
    final int strLen = str.length();
    char ch;
    for(int i=0; i<strLen; ++i)
    {  //for each character in given string
      if(keepCharsStr.indexOf(ch=str.charAt(i)) >= 0)
        buff.append(ch);     //if char in 'keep' string then add to buffer
    }
    final String retStr = buff.toString();  //get string version of buffer
              //if no chars removed then return original string;
              // otherwise return modified string:
    return str.equals(retStr) ? str : retStr;
  }

  /**
   * Returns a new string with all non-alphnumeric characters removed.
   * @param str string to be processed.
   * @return A new version of the string if characters were removed,
   * or the original string if no characters were removed.
   */
  public static String removeNonAlphanumChars(String str)
  {
    return removeSpecifiedChars(str,ALPHANUM_CHARS_STRING);
  }

  /**
   * Parses a string of substrings using the given separator.
   * @param dataStr the data string to parse.
   * @param sepStr the separator string (if null or an empty string then
   * "." is used).
   * @return A list containing the substrings found, or an empty list
   * if the given string contained no data.
   */
  public static ArrayList parseSeparatedSubstrsToList(String dataStr,
                                                              String sepStr)
  {
    final ArrayList listObj = new ArrayList();
    final int dataStrLen;
    if(dataStr != null && (dataStrLen=dataStr.length()) > 0)
    {    //string contains data
      if(sepStr == null || sepStr.length() <= 0)
        sepStr = ".";        //if no separator then use default
      final int sepStrLen = sepStr.length();
      int ePos, sPos = 0;
      do
      {       //for each substring found
        if((ePos=dataStr.indexOf(sepStr,sPos)) < 0)   //find next separator
          ePos = dataStrLen;      //if none found then use end of string pos
        listObj.add(dataStr.substring(sPos,ePos));        //save substring
      }       //move to next position after separator; loop if data remaining
      while((sPos=ePos+sepStrLen) < dataStrLen);
    }
    return listObj;
  }

  /**
   * Parses a string of substrings using the given separator.  A quote
   * character may be specified to allow a single substring to contain
   * separator characters, such as:  "one.two three"
   * @param dataStr the data string to parse.
   * @param sepStr the separator string (if null or an empty string then
   * "." is used).
   * @param quoteChar a quote character (i.e., '"') to be used at the
   * beginning and end of a substring, or a null ('\0') character for none.
   * @param trimFlag if true then leading and trailing whitespace will be
   * removed from the parsed substrings.
   * @param removeQuotesFlag if true then quote characters surrounding
   * substrings will be removed.
   * @return An list containing the substrings found, or an empty list
   * if the given string contained no data.
   */
  public static ArrayList parseSeparatedSubstrsToList(String dataStr,
                            String sepStr, char quoteChar, boolean trimFlag,
                                                   boolean removeQuotesFlag)
  {
    final ArrayList listObj = new ArrayList();
    final int dataStrLen;
    if(dataStr != null && (dataStrLen=dataStr.length()) > 0)
    {    //string contains data
      if(sepStr == null || sepStr.length() <= 0)
        sepStr = ".";        //if no separator then use default
      final int sepStrLen = sepStr.length();
                   //set flag true if separator is all whitespace:
      final boolean sepSpaceFlag = (sepStr.trim().length() <= 0);
      int p, q, ePos, sPos = 0;
      String str, tStr;
      outerLoop:
      do
      {       //for each substring
        if(sepSpaceFlag && Character.isWhitespace(dataStr.charAt(sPos)))
        {  //separator is all whitespace and extra whitespace found
          do
          {        //skip past extra whitespace character:
            if(++sPos >= dataStrLen)        //if at end of string then
              break outerLoop;              //exit both loops
          }        //loop if additional whitespace character found:
          while(Character.isWhitespace(dataStr.charAt(sPos)));
        }
        if((ePos=dataStr.indexOf(sepStr,sPos)) < 0)   //find next separator
          ePos = dataStrLen;      //if none found then use end of string pos
        if(quoteChar > '\0')
        {  //quote character was given; check if separator between quotes
          if((p=dataStr.indexOf(quoteChar,sPos)) >= 0 && p < ePos &&
              p+1 < dataStrLen && (q=dataStr.indexOf(quoteChar,p+1)) > ePos)
          {  //quote character found before next separator and
             // matching quote character found after separator
            if((ePos=dataStr.indexOf(sepStr,q)) < 0)  //find next separator
              ePos = dataStrLen;  //if none found then use end of string pos
          }
          str = dataStr.substring(sPos,ePos);    //extract substring
          tStr = str.trim();      //trim whitespace and check for quotes
          if(removeQuotesFlag && (q=tStr.length()) >= 2 &&
               tStr.charAt(0) == quoteChar && tStr.charAt(q-1) == quoteChar)
          {  //flag set and leading & trailing quote characters found
            str = tStr.substring(1,q-1);    //remove quote characters
            if(trimFlag)          //if flag set then
              str = str.trim();   //trim any whitespace around substring
          }
          else if(trimFlag)       //quote chars not found; if flag set then
            str = tStr;           //use trimmed string
        }
        else
        {  //quote character not given
          str = dataStr.substring(sPos,ePos);    //extract substring
          if(trimFlag)            //if flag set then
            str = str.trim();     //trim any whitespace around substring
        }
        listObj.add(str);         //save substring
      }       //move to next position after separator; loop if data remaining
      while((sPos=ePos+sepStrLen) < dataStrLen);
    }
    return listObj;
  }

  /**
   * Parses a string of substrings using the given separator.  A quote
   * character may be specified to allow a single substring to contain
   * separator characters, such as:  "one.two three"  The surrounding
   * quote characters will be removed.
   * @param dataStr the data string to parse.
   * @param sepStr the separator string (if null or an empty string then
   * "." is used).
   * @param quoteChar a quote character (i.e., '"') to be used at the
   * beginning and end of a substring, or a null ('\0') character for none.
   * @param trimFlag if true then leading and trailing whitespace will be
   * removed from the parsed substrings.
   * @return An list containing the substrings found, or an empty list
   * if the given string contained no data.
   */
  public static ArrayList parseSeparatedSubstrsToList(String dataStr,
                            String sepStr, char quoteChar, boolean trimFlag)
  {
    return parseSeparatedSubstrsToList(
                                    dataStr,sepStr,quoteChar,trimFlag,true);
  }

  /**
   * Parses a string of substrings using the given separator.
   * @param dataStr the data string to parse.
   * @param sepStr the separator string (if null or an empty string then
   * "." is used).
   * @return An array containing the substrings found, or an empty array
   * if the given string contained no data.
   */
  public static String [] parseSeparatedSubstrings(String dataStr,
                                                              String sepStr)
  {
                         //parse substrings into list object:
    final ArrayList listObj = parseSeparatedSubstrsToList(dataStr,sepStr);
                         //convert list to array of strings and return it:
    return (String [])(listObj.toArray(new String[listObj.size()]));
  }

  /**
   * Parses a string of substrings using the given separator.  A quote
   * character may be specified to allow a single substring to contain
   * separator characters, such as:  "one.two three"  The surrounding
   * quote characters will be removed.
   * @param dataStr the data string to parse.
   * @param sepStr the separator string (if null or an empty string then
   * "." is used).
   * @param quoteChar a quote character (i.e., '"') to be used at the
   * beginning and end of a substring, or a null ('\0') character for none.
   * @param trimFlag if true then leading and trailing whitespace will be
   * removed from the parsed substrings.
   * @param removeQuotesFlag if true then quote characters surrounding
   * substrings will be removed.
   * @return An array containing the substrings found, or an empty array
   * if the given string contained no data.
   */
  public static String [] parseSeparatedSubstrings(String dataStr,
                            String sepStr, char quoteChar, boolean trimFlag,
                                                   boolean removeQuotesFlag)
  {
                         //parse substrings into list object:
    final ArrayList listObj = parseSeparatedSubstrsToList(
                        dataStr,sepStr,quoteChar,trimFlag,removeQuotesFlag);
                         //convert list to array of strings and return it:
    return (String [])(listObj.toArray(new String[listObj.size()]));
  }

  /**
   * Parses a string of substrings using the given separator.  A quote
   * character may be specified to allow a single substring to contain
   * separator characters, such as:  "one.two three"  The surrounding
   * quote characters will be removed.
   * @param dataStr the data string to parse.
   * @param sepStr the separator string (if null or an empty string then
   * "." is used).
   * @param quoteChar a quote character (i.e., '"') to be used at the
   * beginning and end of a substring, or a null ('\0') character for none.
   * @param trimFlag if true then leading and trailing whitespace will be
   * removed from the parsed substrings.
   * @return An array containing the substrings found, or an empty array
   * if the given string contained no data.
   */
  public static String [] parseSeparatedSubstrings(String dataStr,
                            String sepStr, char quoteChar, boolean trimFlag)
  {
                         //parse substrings into list object:
    final ArrayList listObj = parseSeparatedSubstrsToList(
                                    dataStr,sepStr,quoteChar,trimFlag,true);
                         //convert list to array of strings and return it:
    return (String [])(listObj.toArray(new String[listObj.size()]));
  }

  /**
   * Parses the leading digits of string as an integer while ignoring
   * any trailing non-digit characters.  Any leading whitespace is
   * trimmed before the string is parsed.
   * @param numStr the string to parse.
   * @return The parsed integer value.
   * @throws NumberFormatException if no valid numeric data is found.
   */
  public static int parseLeadingInt(String numStr)
                                                throws NumberFormatException
  {
    final int len;      //trim and check if any data:
    if(numStr == null || (len=(numStr=numStr.trim()).length()) <= 0)
      throw new NumberFormatException("String contains no data");
    int p = 0;     //scan until non-digit or end of string found:
    while(Character.isDigit(numStr.charAt(p)) && ++p < len);
    if(p == 0)     //if no digits found then throw exception
      throw new NumberFormatException("String contains no numeric data");
                             //parse numeric portion of string:
    return Integer.parseInt(numStr.substring(0,p));
  }

  /**
   * Factory method for making a <code>NumberFormatException</code> given the
   * specified input which caused the error.
   *
   * @param cs
   *            the input causing the error
   */
  public static NumberFormatException
          createNumberFormatExceptionForInput(CharSequence cs) {
      return new NumberFormatException(
              "For input string: \"" + cs.toString() + "\"");
  }

  /**
   * Create the version array.
   * @return the version array.
   */
  static int[] createVersionArray()
  {
	  return new int[4];
  }

  /**
   * Parses the character sequence argument as a signed decimal integer. The
   * characters in the character sequence must all be decimal digits, except
   * that the first character may be an ASCII minus sign {@code '-'} (
   * <code>'&#92;u002D'</code>) to indicate a negative value. The resulting
   * integer value is returned, exactly as if the argument and the radix 10
   * were given as arguments to the
   * {@link #parseInt(java.lang.CharSequence, int, int, int, boolean)} method.
   *
   * @param cs
   *            a {@code CharSequence} containing the {@code int}
   *            representation to be parsed
   * @return the integer value represented by the argument in decimal.
   * @exception NumberFormatException
   *                if the character sequence does not contain a parsable
   *                integer.
   */
  public static int parseInt(CharSequence cs) {
      return parseInt(cs, 0, cs.length(), 10, true);
  }

  /**
   * Parses the character sequence argument as a signed decimal integer. The
   * characters in the character sequence must all be decimal digits, except
   * that the first character may be an ASCII minus sign {@code '-'} (
   * <code>'&#92;u002D'</code>) to indicate a negative value. The resulting
   * integer value is returned, exactly as if the argument and the radix 10
   * were given as arguments to the
   * {@link #parseInt(java.lang.CharSequence, int, int, int, boolean)} method.
   *
   * @param cs
   *            the {@code CharSequence} containing the integer representation
   *            to be parsed
   * @param start
   *            the start index.
   * @param end
   *            the end index.
   * @return the integer value represented by the argument in decimal.
   * @exception NumberFormatException
   *                if the character sequence does not contain a parsable
   *                integer.
   */
  public static int parseInt(CharSequence cs, int start, int end) {
      return parseInt(cs, start, end, 10, true);
  }

  /**
   * Parses the character sequence argument as a signed integer in the radix
   * specified by the second argument. The characters in the character
   * sequence must all be digits of the specified radix (as determined by
   * whether {@link java.lang.Character#digit(char, int)} returns a
   * nonnegative value), except that the first character may be an ASCII minus
   * sign {@code '-'} (<code>'&#92;u002D'</code>) to indicate a negative value
   * or an ASCII plus sign {@code '+'} (<code>'&#92;u002B'</code>) to indicate
   * a positive value. The resulting integer value is returned.
   * <p>
   * An exception of type {@code NumberFormatException} is thrown if any of
   * the following situations occurs:
   * <ul>
   * <li>The first argument is {@code null} or is a character sequence of
   * length zero.
   * <li>The radix is either smaller than
   * {@link java.lang.Character#MIN_RADIX} or larger than
   * {@link java.lang.Character#MAX_RADIX}.
   * <li>Any character of the character sequence is not a digit of the
   * specified radix, except that the first character may be a minus sign
   * {@code '-'} (<code>'&#92;u002D'</code>) provided that the character
   * sequence is longer than length 1.
   * <li>The value represented by the character sequence is not a value of
   * type {@code int}.
   * </ul>
   * <p>
   * Examples: <blockquote>
   * 
   * <pre>
   * parseInt("0", 10) returns 0
   * parseInt("473", 10) returns 473
   * parseInt("-0", 10) returns 0
   * parseInt("+0", 10) returns 0
   * parseInt("-FF", 16) returns -255
   * parseInt("1100110", 2) returns 102
   * parseInt("2147483647", 10) returns 2147483647
   * parseInt("-2147483648", 10) returns -2147483648
   * parseInt("2147483648", 10) throws a NumberFormatException
   * parseInt("99", 8) throws a NumberFormatException
   * parseInt("Kona", 10) throws a NumberFormatException
   * parseInt("Kona", 27) returns 411787
   * </pre>
   * 
   * </blockquote>
   *
   * @param cs
   *            the {@code CharSequence} containing the integer representation
   *            to be parsed
   * @param start
   *            the start index.
   * @param end
   *            the end index.
   * @param radix
   *            the radix to be used while parsing {@code s}.
   * @param lenientFlag
   *            true for lenient parsing, false otherwise.
   * @return the integer represented by the character sequence argument in the
   *         specified radix.
   * @exception NumberFormatException
   *                if the {@code CharSequence} does not contain a parsable
   *                {@code int}.
   */
  public static int parseInt(CharSequence cs, int start, int end, int radix,
          boolean lenientFlag) throws NumberFormatException {
      if (cs == null) {
          throw new NumberFormatException("null");
      }
      if (radix < Character.MIN_RADIX) {
          throw new NumberFormatException(
                  "radix " + radix + " less than Character.MIN_RADIX");
      }
      if (radix > Character.MAX_RADIX) {
          throw new NumberFormatException(
                  "radix " + radix + " greater than Character.MAX_RADIX");
      }
      if (start < 0 || start > cs.length()) {
          throw new NumberFormatException("invalid start index: " + start);
      }
      if (end < start || end > cs.length()) {
          throw new NumberFormatException("invalid end index: " + end);
      }
      if (lenientFlag) {
          // check for comma
          for (int i = start; i < end; i++) {
              if (cs.charAt(i) == ',') {
                  end = i; // end at comma
                  break;
              }
          }
      }

      int result = 0;
      boolean negative = false;
      int i = 0, len = end - start;
      int limit = -Integer.MAX_VALUE;
      int multmin;
      int digit;

      if (lenientFlag) {
          // skip over leading space
          while ((i < len) && (cs.charAt(start + i) <= ' ')) {
              i++;
          }
          // skip trailing space
          while ((i < len) && (cs.charAt(start + len - 1) <= ' ')) {
              len--;
          }
          // skip decimal point and fractional number
          for (int j = i; j < len; j++) {
              if (cs.charAt(start + j) == '.') {
                  len = j;
                  break;
              }
          }
      }
      if (len > i) {
          char firstChar = cs.charAt(start + i);
          if (firstChar < '0') { // Possible leading sign
              switch (firstChar) {
              case '-':
                  negative = true;
                  limit = Integer.MIN_VALUE;
                  break;
              case '+':
                  break;
              default:
                  throw createNumberFormatExceptionForInput(
                          cs.subSequence(start, end));
              }

              if (++i >= len) // Cannot have lone sign
                  throw createNumberFormatExceptionForInput(
                          cs.subSequence(start, end));
          }
          multmin = limit / radix;
          while (i < len) {
              // Accumulating negatively avoids surprises near MAX_VALUE
              digit = Character.digit(cs.charAt(start + i++), radix);
              if (digit < 0) {
                  throw createNumberFormatExceptionForInput(
                          cs.subSequence(start, end));
              }
              if (result < multmin) {
                  throw createNumberFormatExceptionForInput(
                          cs.subSequence(start, end));
              }
              result *= radix;
              if (result < limit + digit) {
                  throw createNumberFormatExceptionForInput(
                          cs.subSequence(start, end));
              }
              result -= digit;
          }
      } else {
          throw createNumberFormatExceptionForInput(
                  cs.subSequence(start, end));
      }
      return negative ? result : -result;
  }

  /**
   * Parses a version string into integer values.  The version number values
   * are expected to be separated by periods ('.') or '_'.  Any other
   * non-numeric characters leading or trailing a version number are ignored.
   * @param verStr the version string to be parsed.
   * @return An array of integers, with each entry containing
   * a version number or 0 if no number value was found.
   */
  public static int [] parseVersionNumbers(String verStr)
  {
	  final int[] version = createVersionArray();
	  parseVersionNumbers(verStr, version);
	  return version;
  }

  /**
   * Parses a version string into integer values.  The version number values
   * are expected to be separated by periods ('.') or '_'.  Any other
   * non-numeric characters leading or trailing a version number are ignored.
   * @param verStr the version string to be parsed.
   */
  static void parseVersionNumbers(final String verStr, final int[] version)
  {
	  if (verStr == null || verStr.length() == 0)
		  return;
	  try
	  {
		  char c;
		  int endIndex;
		  int beginIndex = 0;
		  // skip leading non-numeric characters
		  while (!Character.isDigit(verStr.charAt(beginIndex)))
		  {
			  if (++beginIndex >= verStr.length())
				  return;
		  }
		  endIndex = beginIndex + 1;
		  for (int i = 0; i < version.length; i++)
		  {
			  // loop while digit
			  for (;;)
			  {
				  // exit if no more characters
				  if (endIndex >= verStr.length())
				  {
					  version[i] = parseInt(verStr, beginIndex, endIndex);
					  return;
				  }
				  c = verStr.charAt(endIndex);
				  if (!Character.isDigit(c)) // exit loop if not digit
					  break;
				  endIndex++;
			  }
			  if (c == '.' || c == '_') // if separator
			  {
				  version[i] = parseInt(verStr, beginIndex, endIndex);
				  beginIndex = endIndex + 1; // begin next after separator
				  endIndex = beginIndex;
				  // exit if no more characters
				  if (endIndex >= verStr.length())
					  return;
			  }
			  else // exit if trailing non-numeric character
			  {
				  version[i] = parseInt(verStr, beginIndex, endIndex);
				  return;
			  }
		  }
	  }
	  catch (Exception ex) {}
  }

  /**
   * Compares version strings.  If numeric version strings given then a
   * numeric comparison is performed (so, for example, "10" is greater
   * than "9").
   * @param ver1Str first version string.
   * @param ver2Str second version string.
   * @param def1Val default value to be returned if first version string
   * contains no data (null or empty) and second version string contains
   * data.
   * @param bothNullVal value to be returned if both version strings
   * contain no data (null or empty).
   * @return An integer that is less than, equal to or greater than zero
   * as the first string is less than, equal to or greater than the second
   * string (or one of the given values).
   */
  public static int compVersionStrs(String ver1Str, String ver2Str,
                                               int def1Val, int bothNullVal)
  {
    if(ver1Str == null || (ver1Str=ver1Str.trim()).length() <= 0)
    {  //first version string is null or empty
      if(bothNullVal == def1Val)       //if return values same then
        return def1Val;                // return it
              //if both strings null/empty then return both-null value:
      return (ver2Str == null || (ver2Str=ver2Str.trim()).length() <= 0) ?
                                                      bothNullVal : def1Val;
    }
    if(ver2Str == null || (ver2Str=ver2Str.trim()).length() <= 0)
      return -1;
    try       //if numeric version strings then do numeric comparison
    {         // (so, for example, "10" is greater than "9"):
      if(Character.isDigit(ver1Str.charAt(0)))
      {  //looks like numeric strings in use
        return Float.compare(     //return comparison if valid float values:
                       Float.parseFloat(ver1Str),Float.parseFloat(ver2Str));
      }
    }
    catch(Exception ex)
    {  //unable to convert as float values; use comparison below
    }
    return ver1Str.compareTo(ver2Str);
  }

  /**
   * Compares version strings.  If numeric version strings given then a
   * numeric comparison is performed (so, for example, "10" is greater
   * than "9").
   * @param ver1Str first version string.
   * @param ver2Str second version string.
   * @return An integer that is less than, equal to or greater than zero
   * as the first string is less than, equal to or greater than the second
   * string.
   */
  public static int compVersionStrs(String ver1Str, String ver2Str)
  {
    return compVersionStrs(ver1Str,ver2Str,-1,0);
  }

  /**
   * Compares two version number values.
   * @param version1 the first version.
   * @param version2 the second version.
   * @return The value 1, 0, or -1 as the first version string is
   * greater than, equal to or less than the second.
   * @see #parseVersionNumbers(String)
   */
  public static int compareVersionNumbers(int[] version1, int[] version2)
  {
	  int v1, v2;
	  final int len = Math.min(version1.length, version2.length);
	  for (int i = 0; i < len; i++)
	  {
		  v1 = version1[i];
		  v2 = version2[i];
		  if (v1 != v2)  //if the version numbers are different
		  {
			  if (v1 < v2)  ///if this version is less than the specified version
				  return -1;  //this version is less than the specified version
			  else
				  return 1;  //this version is greater than the specified version
		  }
	  }
	  return 0;  //the versions are the same
  }

  /**
   * Compares two version strings containing version number values
   * separated by periods ('.') or '_' ('.').  Any non-numeric
   * characters trailing a version number are ignored.
   * @param ver1Str the first version string.
   * @param ver2Str the second version string.
   * @return The value 1, 0, or -1 as the first version string is
   * greater than, equal to or less than the second.
   * @see #parseVersionNumbers(String)
   */
  public static int compareVersionStrings(String ver1Str,String ver2Str)
  {
	  final int[] version1 = parseVersionNumbers(ver1Str);
	  final int[] version2 = parseVersionNumbers(ver2Str);
	  return compareVersionNumbers(version1, version2);
  }

  /**
   * Fetches stack trace data from a throwable and returns it in a string.
   * @param throwObj the throwable object to use.
   * @return A string containing the stack trace data.
   */
  public static String getStackTraceString(Throwable throwObj)
  {
    String retStr = EMPTY_STRING;
                             //create string-writer to receive data:
    final StringWriter strWtrObj = new StringWriter();
    try
    {                        //wrap print-writer around string-writer:
      final PrintWriter prtWtrObj = new PrintWriter(strWtrObj);
                             //get stack trace data:
      throwObj.printStackTrace(new PrintWriter(prtWtrObj));
      prtWtrObj.flush();     //flush data out of print writer
              //convert stream to string; strip trailing newline:
      retStr = stripTrailingNewline(strWtrObj.toString());
    }
    catch(Exception ex) {}   //ignore any exceptions
    return retStr;           //return stack trace data
  }

  /**
   * Returns a string of memory information.
   * @return A string of memory information in the form
   * "total=#, free=#, diff=#", with each value in bytes.
   */
  public static String getMemoryInfoStr()
  {
    try
    {
      final long totalVal = systemRuntimeObj.totalMemory();
      final long freeVal = systemRuntimeObj.freeMemory();
      return "total=" + totalVal + ", free=" + freeVal +
                                             ", diff=" + (totalVal-freeVal);
    }
    catch(Exception ex)
    {
      return "Error in 'getMemoryInfoStr()':  " + ex;
    }
  }

  /**
   * Returns the root ThreadGroup object.
   * @return The root ThreadGroup object, or null if an error occurred.
   */
  public static ThreadGroup getRootThreadGroupObj()
  {
    if(rootThreadGroupObj == null)
    {    //root thread group handle not yet setup
      try
      {
        ThreadGroup threadGroupObj = Thread.currentThread().getThreadGroup();
        ThreadGroup gObj;    //get thread group parent until top-most found:
        while((gObj=threadGroupObj.getParent()) != null)
          threadGroupObj = gObj;
        rootThreadGroupObj = threadGroupObj;
      }
      catch(Exception ex)
      {       //some kind of exception error; leave handle at null
      }
    }
    return rootThreadGroupObj;
  }

  /**
   * Returns a count of the total number of threads in the JVM.
   * @return A count of the total number of threads in the JVM, or 0
   * if an error occurred.
   */
  public static int getTotalThreadCount()
  {
    try
    {         //get root thread group object and thread count:
      final ThreadGroup threadGroupObj;
      return ((threadGroupObj=getRootThreadGroupObj()) != null) ?
                                           threadGroupObj.activeCount() : 0;
    }
    catch(Exception ex)
    {    //some kind of exception error
      return 0;         //return zero count
    }
  }

  /**
   * Emits an audio beep.
   */
  public static void beep()
  {
    try
    {       //output audio beep:
//      java.awt.Toolkit.getDefaultToolkit().beep();
                 //invoke method via reflection to avoid direct
                 // reference to 'java.awt.Toolkit' class:
      final Class tkClassObj = Class.forName("java.awt.Toolkit");
      final Method gdtkMethObj =
    	  tkClassObj.getMethod("getDefaultToolkit", (Class[]) null);
      final Object tkObj = gdtkMethObj.invoke((Object) null, (Object[]) null);
      final Method beepMethObj = tkClassObj.getMethod("beep", (Class[]) null);
      beepMethObj.invoke(tkObj, (Object[]) null);
    }
    catch(Exception ex) {}
  }

  /**
   * Determines if the character is valid for a filename.
   * @param ch the character to check.
   * @return true if valid, false otherwise.
   */
  public  static boolean isValidFileNameChar(char ch)
  {
    if (INVALID_FILENAME_CHARS.indexOf(ch) >= 0)  //if invalid char
      return false;  //return not valid
    if (ch == File.separatorChar)  //if separator char
      return false;  //return not valid
    if (Character.isISOControl(ch))  //if control char
      return false;  //return not valid
    if (ch == ' ')   //if space char
      return true;   //return valid
    if (Character.isWhitespace(ch))  //if whitespace other than space char
      return false;  //return not valid
    return true;   //return valid
  }

  /**
   * Converts the given string to a string that is safe to use as a file name.
   * @param str the string to be converted.
   * @return The given string if it is safe to use as a file name; otherwise
   * returns a converted version that is safe.
   */
  public static String stringToFileName(String str)
  {
    final StringBuffer fileName = new StringBuffer();
    final int strLen = str.length();
    boolean changedFlag = false;
    char ch;
    for(int i = 0; i < strLen; i++)
    {  //for each character in given string
      if(isValidFileNameChar(ch=str.charAt(i)))  //if char valid then
        fileName.append(ch);                     //append to buffer
      else
      {  //char not valid; append hex representation to buffer
        fileName.append('%' + toHexString(ch, 4));
        changedFlag = true;            //indicate data changed
      }
    }
                   //convert buffer to string and trim whitespace:
    final String retStr = fileName.toString().trim();
                   //if no invalid chars and 'trim()' did not change length
                   // then return given string; otherwise return new string:
    return (!changedFlag && retStr.length() == strLen) ? str : retStr;
  }

  /**
   * Converts a file-name string that was encoded via 'stringToFileName()'
   * back to its original form.
   * @param fNameStr file-name string to use.
   * @return The given string if no conversion occured; otherwise returns
   * the converted version.
   */
  public static String fileNameToString(String fNameStr)
  {
    final int fNameStrLen = fNameStr.length();
    int pos;
    if((pos=fNameStr.indexOf('%')) < 0 || pos > fNameStrLen-5)
      return fNameStr;  //if no hex representations then return given string
              //create buffer, start with data before '%':
    final StringBuffer buff = new StringBuffer(fNameStr.substring(0,pos));
    int val,nPos;
    while(true)
    {  //for each '%' character found
      try
      {       //attempt to interpret as "%####" hex representation:
        val = Integer.parseInt(fNameStr.substring(pos+1,pos+5),16);
        buff.append((char)val);        //add decoded char to buffer
        pos += 5;                      //move past hex representation
      }
      catch(Exception ex)
      {  //unable to interpret as "%####" hex representation
        buff.append(fNameStr.substring(pos,pos+1));   //add '%' to buffer
        ++pos;                                        //move past '%'
      }
      if((nPos=fNameStr.indexOf('%',pos)) < 0 || nPos > fNameStrLen-5)
      {  //no more hex representations found
        if(pos < fNameStrLen)                    //if chars left then
          buff.append(fNameStr.substring(pos));  //add remaining to buffer
        break;                                   //exit loop
      }
      buff.append(fNameStr.substring(pos,nPos)); //add intermediate chars
      pos = nPos;                                //save '%' position; loop
    }
    final String retStr = buff.toString();       //convert buffer to string
                   //if no conversion then return given string;
                   // otherwise return converted string:
    return retStr.equals(fNameStr) ? fNameStr : retStr;
  }

  /**
   * Converts an array of strings to a single string containing all the
   * strings in the array.
   * @param strArr source array of strings.
   * @param sepStr separator string to use between source strings, or null
   * or none.
   * @return A single string containing all the strings in the given array.
   */
  public static String stringArrayToString(String [] strArr, String sepStr)
  {
    final StringWriter wtrObj = new StringWriter();
    if(sepStr == null)            //if no separator then
      sepStr = EMPTY_STRING;      //use empty string
    if(strArr.length > 0)
    {  //given array not empty
      int i = 0;
      while(true)
      {  //for each element in given array
        wtrObj.write(strArr[i]);       //add element to string
        if(++i >= strArr.length)       //if no more elements then
          break;                       //exit loop
        wtrObj.write(sepStr);          //add separator to string
      }
    }
    return wtrObj.toString();
  }

  /**
   * Converts an array of strings to a single string containing all the
   * strings in the array (each separated by a space).
   * @param strArr source array of strings.
   * @return A single string containing all the strings in the given array.
   */
  public static String stringArrayToString(String [] strArr)
  {
    return stringArrayToString(strArr," ");
  }

  /**
   * Sends the given message to the "global" log file object.  This
   * method references 'LogFile' via reflection to avoid a compile-time
   * class dependency on that object.  The equivalent call is:
   *   LogFile.getGlobalLogObj(true).warning(msgStr)
   * @param msgStr message string to send out.
   */
  public static void toGlobalLogFileWarn(String msgStr)
  {
    try
    {    //create 'LogFile' object via reflection:
                   //get class object for 'LogFile':
      final Class logFileClassObj = Class.forName("com.isti.util.LogFile");
                   //get method object for 'getGlobalLogObj(boolean)':
      final Method getGlobalLogObjMethodObj =
                                logFileClassObj.getMethod("getGlobalLogObj",
                                              new Class [] {boolean.class});
                   //get method object for 'warning(String)':
      final Method warningMethodObj = logFileClassObj.getMethod("warning",
                                              new Class [] {String.class});
                   //invoke 'LogFile.getGlobalLogObj(true)':
      final Object logFileInstanceObj = getGlobalLogObjMethodObj.invoke(
                                        null, new Object [] {Boolean.TRUE});
                   //invoke 'warning(msgStr)' method on LogFile object:
      warningMethodObj.invoke(logFileInstanceObj, new Object [] {msgStr});
    }
    catch(Throwable ex)
    {  //some kind of exception error; fall back to console output
      System.err.println(msgStr);
    }
  }

  /**
   * Get a fixed-size list backed by the specified array.  (Changes to
   * the returned list "write through" to the array.)
   * @param a the array by which the list will be backed or null if none.
   * @return a list view of the specified array or null if none.
   */
  public static List toList(Object[] a)
  {
    if (a == null)
    {
      return null;
    }
    return Arrays.asList(a);
  }

  /**
   * Get the string representation of the <code>Object</code> argument
   * using the default separator string.
   * @param   obj   an <code>Object</code>.
   * @return  if the argument is <code>null</code>, then a string equal to
   *          <code>"null"</code>; otherwise, the value of
   *          <code>obj.toString()</code> is returned.
   * @see #toString(Object, String, String, Collection)
   */
  public static String toString(Object obj)
  {
    return toString(obj, DEFAULT_NULL_STR, DEFAULT_SEP_STR);
  }
  
  /**
   * Get the string representation of the <code>Object</code> argument
   * without a separator string.
   * @param   obj   an <code>Object</code>.
   * @return  if the argument is <code>null</code>, then nullStr; otherwise,
   *          the value of <code>obj.toString()</code> is returned.
   * @param nullStr the null string
   */
  public static String toString(Object obj, String nullStr)
  {
    if (obj == null)          //if null parameter then
    {
      return nullStr;         //return null string
    }
    return obj.toString();
  }

  /**
   * Get the string representation of the <code>Object</code> argument
   * using the specified separator string.
   * @param   obj   an <code>Object</code>.
   * @return  if the argument is <code>null</code>, then nullStr; otherwise,
   *          the value of <code>obj.toString()</code> is returned.
   * @param nullStr the null string
   * @param sepStr separator string value.
   * @see #toString(Object, String, String, Collection)
   */
  public static String toString(Object obj, String nullStr, String sepStr)
  {
    return toString(obj, nullStr, sepStr, new ArrayList());
  }

  /**
   * Get the string representation of the <code>Object</code> argument
   * using the specified separator string.
   * @param   obj   an <code>Object</code>.
   * @return  if the argument is <code>null</code>, then nullStr; otherwise,
   *          the value of <code>obj.toString()</code> is returned.
   * @param nullStr the null string
   * @param sepStr separator string value.
   * @param all a collection of all values for deep contents or null if none.
   * @see #DEFAULT_NULL_TEXT
   * @see #DEFAULT_SEP_STR
   */
  public static String toString(Object obj, String nullStr, String sepStr,
      Collection all)
  {
    if (obj == null)          //if null parameter then
    {
      return nullStr;         //return null string
    }

    if (all != null && all.contains(obj))
    {
      return "[...]";
    }

    final Collection colObj;

    if (obj instanceof Object[])  //if the object is an array
    {
      //create a collection of the array
      colObj = Arrays.asList((Object[]) obj);
    }
    else if (obj instanceof Collection)  //if the object is a Collection
    {
      colObj = (Collection) obj;
    }
    else
    {
      return obj.toString();
    }

    if (all != null)
    {
      all.add(obj);
    }

    String s;
    final StringBuffer buffObj = new StringBuffer();
    buffObj.append('[');
    final Iterator iterObj = colObj.iterator();
    if(iterObj.hasNext())  //if the collection is not empty
    {
      while(true)
      {
        if (all != null)  // if deep contents use recursion
        {
          s = toString(iterObj.next(), nullStr, sepStr, all);
        }
        else if (obj != null)
        {
          s = obj.toString();
        }
        else
        {
          s = nullStr;
        }
        buffObj.append(s);
        if(!iterObj.hasNext())                  //if no more entries then
          break;                                //exit loop
        buffObj.append(sepStr);                 //add separator to string
      }
    }
    buffObj.append("]");

    if (all != null)
    {
      all.remove(obj);
    }

    return buffObj.toString();
  }

  /**
   * Check if a string ends with a specified suffix (optionally case insensitive).
   *
   * @param str the string to check or null.
   * @param suffix the suffix to find or null.
   * @param ignoreCase indicates whether the compare should ignore case
   *  (case insensitive) or not.
   * @return <code>true</code> if the string ends with the suffix or
   *  both <code>null</code>
   * @see java.lang.String#endsWith(String)
   */
  public static boolean endsWith(String str, String suffix, boolean ignoreCase)
  {
	  if (str == null || suffix == null)
	  {
		  return (str == null && suffix == null);
      }
      if (suffix.length() > str.length())
      {
          return false;
      }
      int strOffset = str.length() - suffix.length();
      return str.regionMatches(ignoreCase, strOffset, suffix, 0, suffix.length());
  }

  /**
   * Case insensitive check if a string ends with a specified prefix.
   * <p><code>null</code>s are handled without exceptions. Two <code>null</code>
   * references are considered to be equal. The comparison is case insensitive.</p>
   *
   * @param str the string to check or null.
   * @param suffix the suffix to find or null.
   * @return <code>true</code> if the string ends with the suffix, case insensitive, or
   *  both <code>null</code>
   * @see java.lang.String#endsWith(String)
   */
  public static boolean endsWithIgnoreCase(String str, String suffix)
  {
      return endsWith(str, suffix, true);
  }

  /**
   * Checks if a string is blank (trimmed to "" or null).
   *
   * @param str the string to check or null.
   * @return <code>true</code> if the string is blank (trimmed to "" or null).
   */
  public static boolean isBlank(String str)
  {
      return str == null || str.trim().length() == 0;
  }

  /**
   * Checks if a string is empty ("" or null).
   *
   * @param str the string to check or null.
   * @return <code>true</code> if the string is empty ("" or null).
   */
  public static boolean isEmpty(String str)
  {
      return str == null || str.length() == 0;
  }

  /**
   * Check if a string starts with a specified prefix (optionally case insensitive).
   *
   * @param str the string to check or null.
   * @param prefix the prefix to find or null.
   * @param ignoreCase indicates whether the compare should ignore case
   *  (case insensitive) or not.
   * @return <code>true</code> if the string starts with the prefix or
   *  both <code>null</code>
   * @see java.lang.String#startsWith(String)
   */
  public static boolean startsWith(String str, String prefix, boolean ignoreCase)
  {
	  if (str == null || prefix == null)
	  {
		  return (str == null && prefix == null);
	  }
      if (prefix.length() > str.length())
      {
    	  return false;
      }
      return str.regionMatches(ignoreCase, 0, prefix, 0, prefix.length());
  }

  /**
   * Case insensitive check if a string starts with a specified prefix.
   * <p><code>null</code>s are handled without exceptions. Two <code>null</code>
   * references are considered to be equal. The comparison is case insensitive.</p>
   *
   * @param str the string to check or null.
   * @param prefix the prefix to find or null.
   * @return <code>true</code> if the string starts with the prefix, case insensitive, or
   *  both <code>null</code>
   * @see java.lang.String#startsWith(String)
   */
  public static boolean startsWithIgnoreCase(String str, String prefix)
  {
      return startsWith(str, prefix, true);
  }

  /**
   * Converts the argument to a {@code long} by an unsigned
   * conversion.  In an unsigned conversion to a {@code long}, the
   * high-order 32 bits of the {@code long} are zero and the
   * low-order 32 bits are equal to the bits of the integer
   * argument.
   *
   * Consequently, zero and positive {@code int} values are mapped
   * to a numerically equal {@code long} value and negative {@code
   * int} values are mapped to a {@code long} value equal to the
   * input plus 2<sup>32</sup>.
   *
   * @param  x the value to convert to an unsigned {@code long}
   * @return the argument converted to {@code long} by an unsigned
   *         conversion
   */
  public static long toUnsignedLong(int x)
  {
      return ((long) x) & 0xffffffffL;
  }
 
  /**
   * Create the URL.
   * 
   * @param spec the {@code String} to parse as a URL.
   * @return the URL or null if error.
   */
  public static URL createURL(String spec)
  {
    try
    {
      return new URL(spec);
    }
    catch (MalformedURLException e)
    {
    }
    return null;
  }
  
  /**
   * Get the clip board contents.
   * @return the clip board contents or null if none.
   */
  public static String getClipboard()
  {
    String data = null;
    try
    {
      final Clipboard clipboard = Toolkit.getDefaultToolkit()
          .getSystemClipboard();
      final Transferable contents = clipboard.getContents(UtilFns.class);
      if (contents != null)
      {
        data = contents.getTransferData(DataFlavor.stringFlavor).toString();
      }
    }
    catch (Exception ex)
    {
    }
    return data;
  }

  /**
   * Set the clip board contents.
   * @param data the string for the contents.
   */
  public static void setClipboard(String data)
  {
    try
    {
      final Clipboard clipboard = Toolkit.getDefaultToolkit()
          .getSystemClipboard();
      final StringSelection selection = new StringSelection(data);
      clipboard.setContents(selection, selection);
    }
    catch (Exception ex)
    {
    }
  }

  /**
   * Creates a new instance of the class.
   * @param clazz the class.
   * @return the new instance or null if error.
   */
  public static Object newInstance(Class clazz)
  {
     try
     {
        return clazz.getDeclaredConstructor().newInstance();
     }
     catch (Exception ex)
     {
     }
     return null;
  }
}
