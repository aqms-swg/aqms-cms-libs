package com.isti.util;

/**
 * A <code>ValueTableModel</code> is a two dimensional data structure that
 * can store arbitrary <code>Object</code> instances. Individual
 * objects can be accessed by specifying the row index and column index for
 * the object. Each column in the model has a name associated with it.
 */
public interface ValueTableModel
{
  /**
   * Returns the <code>Class</code> for all <code>Object</code> instances
   * in the specified column.
   *
   * @param columnIndex the column index.
   *
   * @return The class.
   */
  public Class getColumnClass(int columnIndex);

  /**
   * Returns the number of columns in the model.
   *
   * @return The column count
   */
  public int getColumnCount();

  /**
   * Gets the column index for the specified column name.
   * @param columnName the column name.
   * @return the column index or -1 if none.
   */
  public int getColumnIndex(String columnName);

  /**
   * Returns the name of a column in the model.
   *
   * @param columnIndex the column index.
   *
   * @return The column name.
   */
  public String getColumnName(int columnIndex);

  /**
   * Returns the number of rows in the model.
   *
   * @return The row count.
   */
  public int getRowCount();

  /**
   * Returns the value (<code>Object</code>) at a particular cell in the
   * table.
   *
   * @param rowIndex the row index.
   * @param columnIndex the column index.
   *
   * @return The value at the specified cell.
   */
  public Object getValueAt(int rowIndex, int columnIndex);

  /**
   * Import the values from the value table model for all rows.
   * @param vtm the value table model.
   */
  public void importValues(ValueTableModel vtm);

  /**
   * Returns <code>true</code> if the specified cell is editable, and
   * <code>false</code> if it is not.
   *
   * @param rowIndex the row index of the cell.
   * @param columnIndex the column index of the cell.
   *
   * @return true if the cell is editable, false otherwise.
   */
  public boolean isCellEditable(int rowIndex, int columnIndex);

  /**
   * Sets the value at a particular cell in the table.
   *
   * @param aValue the value (<code>null</code> permitted).
   * @param rowIndex the row index.
   * @param columnIndex the column index.
   */
  public void setValueAt(Object aValue, int rowIndex, int columnIndex);
}
