//ValueTimeBlock.java:  Defines a data block containing a parameter value
//                      object and timestamp.
//
//   10/5/2000 -- [ET]  Initial release version.
//   1/15/2002 -- [ET]  Removed 'final' from variable definitions.
//   10/2/2003 -- [KF]  Added 'dataEquals', 'equals' and 'timestampEquals'
//                      methods.
//   9/29/2006 -- [KF]  Added logic previously in the 'ArchivableData' class.
//
//=====================================================================
// Copyright (C) 2006 Instrumental Software Technologies, Inc.
//
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions
// are met:
// 1. Redistributions of source code, or portions of this source code,
//    must retain the above copyright notice, this list of conditions
//    and the following disclaimer.
// 2. Redistributions in binary form must reproduce the above copyright
//    notice, this list of conditions and the following disclaimer in
//    the documentation and/or other materials provided with the
//    distribution.
// 3. All advertising materials mentioning features or use of this
//    software must display the following acknowledgment:
//    "This product includes software developed by Instrumental
//    Software Technologies, Inc. (http://www.isti.com)"
// 4. If the software is provided with, or as part of a commercial
//    product, or is used in other commercial software products the
//    customer must be informed that "This product includes software
//    developed by Instrumental Software Technologies, Inc.
//    (http://www.isti.com)"
// 5. The names "Instrumental Software Technologies, Inc." and "ISTI"
//    must not be used to endorse or promote products derived from
//    this software without prior written permission. For written
//    permission, please contact "info@isti.com".
// 6. Products derived from this software may not be called "ISTI"
//    nor may "ISTI" appear in their names without prior written
//    permission of Instrumental Software Technologies, Inc.
// 7. Redistributions of any form whatsoever must retain the following
//    acknowledgment:
//    "This product includes software developed by Instrumental
//    Software Technologies, Inc. (http://www.isti.com/)."
// THIS SOFTWARE IS PROVIDED BY INSTRUMENTAL SOFTWARE
// TECHNOLOGIES, INC. "AS IS" AND ANY EXPRESSED OR IMPLIED
// WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES
// OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
// DISCLAIMED.  IN NO EVENT SHALL INSTRUMENTAL SOFTWARE TECHNOLOGIES,
// INC. OR ITS CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
// INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
// (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
// SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
// HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
// STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
// ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED
// OF THE POSSIBILITY OF SUCH DAMAGE.
//=====================================================================
//  A current version of the software can be found at
//                http://www.isti.com
//  Bug reports and comments should be directed to
//  Instrumental Software Technologies, Inc. at info@isti.com
//=====================================================================
//

package com.isti.util;

import java.util.Date;

/**
 * Class ValueTimeBlock defines a data block containing a parameter value
 * object and a timestamp value.
 *
 * If the class for the data has a constructor with a String parameter then
 * the de-archived data should be identical to the original data,
 * otherwise the de-archived data will contain a string representation of the
 * original data.
 */
public class ValueTimeBlock extends ValueTimeUtilFns implements Archivable
{
  /** The data object (Double, String, etc.) */
  public Object data;
  /** The timestamp value (seconds since January 1, 1970, 00:00:00 GMT.) */
  public long timestamp;

    /**
     * Constructor which initializes values.
     * @param data data object, such as Integer, Double, String, etc.
     * @param timestamp timestamp value for data
     * (seconds since January 1, 1970, 00:00:00 GMT.)
     * @see getCurrentTimestamp
     */
  public ValueTimeBlock(Object data,long timestamp)
  {
         //set values from parameters:
    this.data = data;
    this.timestamp = timestamp;
  }

    /**
     * Constructor which initializes values.
     * @param dataStr the archivable representation for this object.
     */
  public ValueTimeBlock(String dataStr)
  {
    this(getValueTimeBlock(dataStr));
  }

    /**
     * Constructor which initializes values.
     * This is part of the Archivable interface.
     * @param dataStr the archivable representation for this object.
     * @param mkrObj the marker object.
     */
  public ValueTimeBlock(String dataStr, Archivable.Marker mkrObj)
  {
    this(getValueTimeBlock(dataStr));
  }

    /**
     * Constructor which initializes values.
     * @param vtb the value time block.
     */
  public ValueTimeBlock(ValueTimeBlock vtb)
  {
    this(vtb.data, vtb.timestamp);
  }

  /**
   * Indicates whether some other data is equal to this object's data.
   * @param   data  the reference data with which to compare.
   * @return  <code>true</code> if this object's data is the same as the data
   *          argument; <code>false</code> otherwise.
   */
  public boolean dataEquals(Object data)
  {
    if (this.data == null)
      return data == null;
    else
      return this.data.equals(data);
  }

  /**
   * Indicates whether some other object is equal to this one.
   * @param   obj   the reference object with which to compare.
   * @return  <code>true</code> if this object is the same as the obj
   *          argument; <code>false</code> otherwise.
   */
  public boolean equals(Object obj)
  {
    if (!(obj instanceof ValueTimeBlock))
      return false;
    return equals((ValueTimeBlock)obj);
  }

  /**
   * Indicates whether some other object is equal to this one.
   * @param   obj   the reference object with which to compare.
   * @return  <code>true</code> if this object is the same as the obj
   *          argument; <code>false</code> otherwise.
   */
  public boolean equals(ValueTimeBlock obj)
  {
    if (obj == null)
      return false;
    return timestampEquals(obj.timestamp) && dataEquals(obj.data);
  }

  /**
   * Returns a 'Date' object representing the date to be used for archival
   * purposes.
   * This is part of the Archivable interface.
   * @return A 'Date' object representing the date that the item should be
   * archived under.
   */
  public Date getArchiveDate()
  {
    return new Date(getArchiveTime());
  }

  /**
   * Returns the number of milliseconds since January 1, 1970, 00:00:00 GMT
   * representing the date to be used for archival purposes.
   * @return  the number of milliseconds since January 1, 1970, 00:00:00 GMT.
   */
  public long getArchiveTime()
  {
    //convert seconds to ms
    return getTime(timestamp);
  }

  /**
   * Returns a string representation of the timestamp.
   * @return a string representation of the timestamp.
   */
  public String getTimestampText()
  {
    return getTimestampText(timestamp);
  }

  /**
   * Returns a hash code value for the object. This method is
   * supported for the benefit of hashtables such as those provided by
   * <code>java.util.Hashtable</code>.
   * @return a hash code value for this object.
   */
  public int hashCode()
  {
    return toString().hashCode();
  }

  /**
   * Indicates whether some other timestamp is equal to this object's timestamp.
   * @param   timestamp   the reference timestamp with which to compare.
   * @return  <code>true</code> if this object's timestamp is the same as the
   *          timestamp argument; <code>false</code> otherwise.
   */
  public boolean timestampEquals(long timestamp)
  {
    return this.timestamp == timestamp;
  }

  /**
   * Returns the archivable representation for this object.  When the
   * object is recreated from the string, it should be identical to the
   * original.
   * This is part of the Archivable interface.
   * @return A String representing the archived form of the object.
   */
  public String toArchivedForm()
  {
    return toString();
  }

  /**
   * Returns a string representation of the object.
   * @return  a string representation of the object.
   */
  public String toString()
  {
    final String dataString;
    final String className;
    if (data != null)
    {
      dataString = data.toString();
      className = data.getClass().getName();
    }
    else
    {
      dataString = "(null)";
      className = UtilFns.EMPTY_STRING;
    }
    final String dataStr =
        getTimestampText() + DATA_SEPARATOR +
        dataString + DATA_SEPARATOR + className;
    return dataStr;
  }
}
