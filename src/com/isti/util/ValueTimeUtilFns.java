//ValueTimeUtilFns.java:  Contains various static utility methods
//                        for working with time values.
//
//  9/29/2006 -- [KF]  Initial version.
//  6/29/2010 -- [ET]  Modified 'getComponentForClass()' method to make
//                     it return null if exception caught; modified
//                     'getValueTimeBlock()' method to make it return
//                     null if 'getComponentForClass()' returns null
//                     or if parse of time-text fails; lowered log-output
//                     levels and improved messages.
//
//=====================================================================
// Copyright (C) 2010 Instrumental Software Technologies, Inc.
//
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions
// are met:
// 1. Redistributions of source code, or portions of this source code,
//    must retain the above copyright notice, this list of conditions
//    and the following disclaimer.
// 2. Redistributions in binary form must reproduce the above copyright
//    notice, this list of conditions and the following disclaimer in
//    the documentation and/or other materials provided with the
//    distribution.
// 3. All advertising materials mentioning features or use of this
//    software must display the following acknowledgment:
//    "This product includes software developed by Instrumental
//    Software Technologies, Inc. (http://www.isti.com)"
// 4. If the software is provided with, or as part of a commercial
//    product, or is used in other commercial software products the
//    customer must be informed that "This product includes software
//    developed by Instrumental Software Technologies, Inc.
//    (http://www.isti.com)"
// 5. The names "Instrumental Software Technologies, Inc." and "ISTI"
//    must not be used to endorse or promote products derived from
//    this software without prior written permission. For written
//    permission, please contact "info@isti.com".
// 6. Products derived from this software may not be called "ISTI"
//    nor may "ISTI" appear in their names without prior written
//    permission of Instrumental Software Technologies, Inc.
// 7. Redistributions of any form whatsoever must retain the following
//    acknowledgment:
//    "This product includes software developed by Instrumental
//    Software Technologies, Inc. (http://www.isti.com/)."
// THIS SOFTWARE IS PROVIDED BY INSTRUMENTAL SOFTWARE
// TECHNOLOGIES, INC. "AS IS" AND ANY EXPRESSED OR IMPLIED
// WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES
// OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
// DISCLAIMED.  IN NO EVENT SHALL INSTRUMENTAL SOFTWARE TECHNOLOGIES,
// INC. OR ITS CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
// INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
// (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
// SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
// HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
// STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
// ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED
// OF THE POSSIBILITY OF SUCH DAMAGE.
//=====================================================================
//  A current version of the software can be found at
//                http://www.isti.com
//  Bug reports and comments should be directed to
//  Instrumental Software Technologies, Inc. at info@isti.com
//=====================================================================
//

package com.isti.util;

import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;
import java.util.Date;

public class ValueTimeUtilFns
{
  /** The data separator */
  public final static String DATA_SEPARATOR = ";";
  /** The constructor parameter types. */
  protected final static Class[] constructorParameterTypes = { String.class };
  /** The GMT flag (true for GMT time zone, false for local time zone.) */
  protected final static boolean gmtFlag = true;

  //protected constructor so that no object instances may be created
  // (static access only)
  protected ValueTimeUtilFns()
  {
  }

  /**
   * Get the current time value.
   * @return the number of milliseconds since January 1, 1970, 00:00:00 GMT.
   */
  public final static long getCurrentTime()
  {
    return System.currentTimeMillis();
  }

  /**
   * Get the current time stamp.
   * @return the number of seconds since January 1, 1970, 00:00:00 GMT.
   */
  public final static long getCurrentTimestamp()
  {
    return getTimestamp(getCurrentTime());
  }

  /**
   * Gets the time for the specified timestamp.
   * @param timestamp the timestamp
   * (seconds since January 1, 1970, 00:00:00 GMT.)
   * @return the milliseconds since January 1, 1970, 00:00:00 GMT.
   */
  public final static long getTime(long timestamp)
  {
    //convert seconds to ms
    return timestamp * UtilFns.MS_PER_SECOND;
  }

  /**
   * Get the time stamp for the specified time value.
   * @param millis the milliseconds since January 1, 1970, 00:00:00 GMT.
   * @return the number of seconds since January 1, 1970, 00:00:00 GMT.
   */
  public final static long getTimestamp(long millis)
  {
    return millis / UtilFns.MS_PER_SECOND;
  }

  /**
   * Returns a string representation of the specified timestamp.
   * @param timestamp the timestamp
   * (seconds since January 1, 1970, 00:00:00 GMT.)
   * @return a string representation of the specified timestamp.
   */
  public final static String getTimestampText(long timestamp)
  {
    final long millis = getTime(timestamp);
    return UtilFns.timeMillisToString(millis, gmtFlag);
  }

  /**
   * Returns a string representation of the specified timestamp.
   * @param timestamp the timestamp
   * (seconds since January 1, 1970, 00:00:00 GMT.)
   * @param gmtFlag true for GMT time zone, false for local time zone.
   * @return a string representation of the specified timestamp.
   */
  public final static String getTimestampText(long timestamp, boolean gmtFlag)
  {
    final long millis = getTime(timestamp);
    return UtilFns.timeMillisToString(millis, gmtFlag);
  }

  /**
   * Returns a string representation of the specified time value.
   * @param millis the milliseconds since January 1, 1970, 00:00:00 GMT.
   * @return a string representation of the specified timestamp.
   */
  public final static String getTimeText(long millis)
  {
    return UtilFns.timeMillisToString(millis, gmtFlag);
  }

  /**
   * Returns a string representation of the specified time value.
   * @param millis the milliseconds since January 1, 1970, 00:00:00 GMT.
   * @param gmtFlag true for GMT time zone, false for local time zone.
   * @return a string representation of the specified timestamp.
   */
  public final static String getTimeText(long millis, boolean gmtFlag)
  {
    return UtilFns.timeMillisToString(millis, gmtFlag);
  }

  /**
   * Gets the constructor for the class represented by the class or
   * interface with the given string name.
   * @param      className   the fully qualified name of the desired class.
   * @return the constructor or null if not found.
   */
  protected final static Constructor getConstructorForClass(String className)
  {
    try
    {
      Class classForName = Class.forName(className);
      if(classForName != null)
      {
        try
        {
          //get the constructor
          return classForName.getConstructor(constructorParameterTypes);
        }
        catch(Exception e)
        {
          LogFile.getGlobalLogObj(true).debug("ValueTimeUtilFns." +
                     "getConstructorForClass:  Cannot instantiate class '" +
                                                    className + "':  " + e);
        }
      }
    }
    catch(ClassNotFoundException e)
    {
      LogFile.getGlobalLogObj(true).debug(
                              "ValueTimeUtilFns:  Cannot find class for '" +
                                                    className + "':  " + e);
    }
    catch(Exception e)
    {
      LogFile.getGlobalLogObj(true).warning(
                    "Error getting class for \"" + className + "\":  " + e);
      LogFile.getGlobalLogObj(true).warning(UtilFns.getStackTraceString(e));
    }
    return null;  //no constructor found
  }

  /**
   * Creates a new instance of the class represented by the class or
   * interface with the given string name.  The class is instantiatied as if
   * by a <code>new</code> expression with an empty argument list.  The class
   * is initialized if it has not already been initialized.
   *
   * @param      className   the fully qualified name of the desired class.
   * @param      dataTxt     the text representation of the data.
   *
   * @return a newly allocated instance of the class represented by this object
   * or null if the class could not be instantiated.
   */
  protected final static Object getComponentForClass(String className,
                                                             String dataTxt)
  {
    if(className.length() <= 0)  //if no class name
    {
      LogFile.getGlobalLogObj(true).debug("ValueTimeUtilFns." +
                                    "getComponentForClass:  No class name");
      return null;
    }
    try
    {
      //get the constructor
      final Constructor dc = getConstructorForClass(className);
      if(dc != null)  //if the constructor was found create a new instance
        return dc.newInstance(new Object[] {dataTxt});
    }
    catch(InvocationTargetException e)
    {
      LogFile.getGlobalLogObj(true).debug("ValueTimeUtilFns." +
                 "getComponentForClass:  Cannot create object for class '" +
                                         className + "':  " + e.getCause());
    }
    catch(Exception e)
    {
      LogFile.getGlobalLogObj(true).debug("ValueTimeUtilFns." +
                      "getComponentForClass:  Cannot instantiate class '" +
                                                   className + "':  " + e);
    }
    return null;
  }

  /**
   * Gets the value time block for the specified data string.
   * @param dataStr the data string.
   * @return A new value time block, or null if an error occurred.
   */
  protected final static ValueTimeBlock getValueTimeBlock(String dataStr)
  {
    try
    {
      final int timeIndex = 0;
      final int dataIndex = dataStr.indexOf(DATA_SEPARATOR);
      final int classIndex = dataStr.lastIndexOf(DATA_SEPARATOR);

      final String timeText = dataStr.substring(timeIndex,dataIndex);
      final String dataTxt = dataStr.substring(dataIndex+1,classIndex);
      final String className = dataStr.substring(classIndex+1);
      final Object compObj;
      if((compObj=getComponentForClass(className,dataTxt)) != null)
      {  //created value object OK
        final Date dateObj;
        if((dateObj=UtilFns.parseStringToDate(timeText,gmtFlag)) != null)
        {  //parsed time text OK
          return new ValueTimeBlock(compObj,
                               (dateObj.getTime() / UtilFns.MS_PER_SECOND));
        }
        else
        {  //unable to parse time text
          LogFile.getGlobalLogObj(true).debug(
                                   "ValueTimeUtilFns.getValueTimeBlock:  " +
                             "Unable to parse time text in data string \"" +
                                                            dataStr + "\"");
        }
      }
    }
    catch(Exception ex)
    {
      LogFile.getGlobalLogObj(true).warning("ValueTimeUtilFns." +
                         "getValueTimeBlock:  Error parsing data string \"" +
                                                     dataStr + "\":  " + ex);
      LogFile.getGlobalLogObj(true).warning(UtilFns.getStackTraceString(ex));
    }
    return null;
  }
}
