//VectorSet.java:  Extends the "Vector" class to support the "Set" interface.
//
//   10/2/2003 -- [KF]  Initial version.
//

package com.isti.util;

import java.util.Collection;
import java.util.Iterator;
import java.util.Set;
import java.util.Vector;

/**
 * Class VectorSet extends the "Vector" class to support the "Set" interface.
 */
public class VectorSet extends Vector implements Set
{
  /**
   * Constructs an empty vector so that its internal data array
   * has size <tt>10</tt> and its standard capacity increment is
   * zero.
   */
  public VectorSet() {
    this(10);
  }

  /**
   * Constructs a vector containing the elements of the specified
   * collection, in the order they are returned by the collection's
   * iterator.
   *
   * @param c the collection whose elements are to be placed into this
   *       vector.
   */
  public VectorSet(Collection c) {
    elementData = new Object[(c.size()*110)/100]; // 10% for growth
    addAll(c);
  }

  /**
   * Constructs an empty vector with the specified initial capacity and
   * with its capacity increment equal to zero.
   *
   * @param   initialCapacity   the initial capacity of the vector.
   * @exception IllegalArgumentException if the specified initial capacity
   *               is negative
   */
  public VectorSet(int initialCapacity) {
    this(initialCapacity, 0);
  }

  /**
   * Constructs an empty vector with the specified initial capacity and
   * capacity increment.
   *
   * @param   initialCapacity     the initial capacity of the vector.
   * @param   capacityIncrement   the amount by which the capacity is
   *                              increased when the vector overflows.
   * @exception IllegalArgumentException if the specified initial capacity
   *               is negative
   */
  public VectorSet(int initialCapacity, int capacityIncrement) {
    super();
    if (initialCapacity < 0)
      throw new IllegalArgumentException("Illegal Capacity: "+
          initialCapacity);
    this.elementData = new Object[initialCapacity];
    this.capacityIncrement = capacityIncrement;
  }

  /**
   * Constructs a vector containing the elements of the specified
   * set, in the order they are returned by the set's
   * iterator.
   *
   * @param s the set whose elements are to be placed into this
   *       vector.
   */
  public VectorSet(Set s) {
    elementCount = s.size();
    elementData = new Object[(elementCount*110)/100]; // 10% for growth
    s.toArray(elementData);
  }

  /**
   * Inserts the specified element at the specified position in this Vector
   * if it is not already present.
   * Shifts the element currently at that position (if any) and any
   * subsequent elements to the right (adds one to their indices).
   *
   * @param index index at which the specified element is to be inserted.
   * @param element element to be inserted.
   * @exception ArrayIndexOutOfBoundsException index is out of range
   *		  (index &lt; 0 || index &gt; size()).
   */
  public void add(int index, Object element) {
    if (!contains(element))  //if element is not already in the vector
      super.insertElementAt(element, index);
  }

  /**
   * Appends the specified element to the end of this Vector
   * if it is not already present.
   * More formally, adds the specified element,
   * <code>o</code>, to this set if this set contains no element
   * <code>e</code> such that <code>(o==null ? e==null :
   * o.equals(e))</code>.  If this set already contains the specified
   * element, the call leaves this set unchanged and returns <tt>false</tt>.
   * In combination with the restriction on constructors, this ensures that
   * sets never contain duplicate elements.<p>
   *
   * @param o element to be added to this set.
   * @return <tt>true</tt> if this set did not already contain the specified
   *         element.
   */
  public synchronized boolean add(Object o) {
    if (!contains(o))  //if object is not already in the vector
      return super.add(o);
    return false;   //return this set already contains the specified element
  }

  /**
   * Adds all of the elements in the specified collection to this set if
   * they're not already present.  If the specified
   * collection is also a set, the <tt>addAll</tt> operation effectively
   * modifies this set so that its value is the <i>union</i> of the two
   * sets.  The behavior of this operation is unspecified if the specified
   * collection is modified while the operation is in progress.
   *
   * @param c collection whose elements are to be added to this set.
   * @return <tt>true</tt> if this set changed as a result of the call.
   *
   * @see #add(Object)
   */
  public synchronized boolean addAll(Collection c) {
    Object element;
    boolean modified = false;
    Iterator e = c.iterator();
    while (e.hasNext()) {
      element = e.next();
      if (!contains(element)) {  //if element is not already in the vector
        super.addElement(element);
        modified = true;
      }
    }
    return modified;
  }

  /**
   * Inserts all of the elements in in the specified Collection into this
   * Vector at the specified position if they're not already present.
   * Shifts the element currently at that position (if any) and any
   * subsequent elements to the right (increases their indices).
   * The new elements will appear in the Vector in the order that they
   * are returned by the specified Collection's iterator.
   *
   * @param index index at which to insert first element
   *		    from the specified collection.
   * @param c elements to be inserted into this Vector.
   * @exception ArrayIndexOutOfBoundsException index out of range (index
   *		  &lt; 0 || index &gt; size()).
   * @return <tt>true</tt> if this set changed as a result of the call.
   */
  public synchronized boolean addAll(int index, Collection c) {
    Object element;
    boolean modified = false;
    Iterator e = c.iterator();
    while (e.hasNext()) {
      element = e.next();
      if (!contains(element)) {  //if element is not already in the vector
        super.insertElementAt(element, index++);
        modified = true;
      }
    }
    return modified;
  }

  /**
   * Adds the specified component to the end of this vector
   * if it is not already present.
   *
   * This method is identical in functionality to the add(Object) method
   * (which is part of the List interface).
   *
   * @param   obj   the component to be added.
   * @see	   #add(Object)
   * @see	   List
   */
  public synchronized void addElement(Object obj) {
    if (!contains(obj))  //if object is not already in the vector
      super.addElement(obj);
  }

  /**
   * Inserts the specified object as a component in this vector at the
   * specified <code>index</code> if it is not already present. Each
   * component in this vector with an index greater or equal to the
   * specified <code>index</code> is shifted upward to have an index
   * one greater than the value it had previously. <p>
   *
   * The index must be a value greater than or equal to <code>0</code>
   * and less than or equal to the current size of the vector. (If the
   * index is equal to the current size of the vector, the new element
   * is appended to the Vector.)<p>
   *
   * This method is identical in functionality to the add(Object, int) method
   * (which is part of the List interface). Note that the add method reverses
   * the order of the parameters, to more closely match array usage.
   *
   * @param      obj     the component to insert.
   * @param      index   where to insert the new component.
   * @exception  ArrayIndexOutOfBoundsException  if the index was invalid.
   * @see        #size()
   * @see	   #add(int, Object)
   * @see	   List
   */
  public synchronized void insertElementAt(Object obj, int index) {
    if (!contains(obj))  //if object is not already in the vector
      super.insertElementAt(obj, index);
  }

  /**
   * Replaces the element at the specified position in this Vector with the
   * specified element if it is not already present.
   *
   * @param index index of element to replace.
   * @param element element to be stored at the specified position.
   * @return the element previously at the specified position.
   * @exception ArrayIndexOutOfBoundsException index out of range
   *		  (index &lt; 0 || index &gt;= size()).
   * @exception IllegalArgumentException fromIndex &gt; toIndex.
   */
  public synchronized Object set(int index, Object element) {
    if (!contains(element))  //if element is not already in the vector
      return super.set(index, element);
    return get(index);
  }

  /**
   * Sets the component at the specified <code>index</code> of this
   * vector to be the specified object if it is not already present.
   * The previous component at that position is discarded.<p>
   *
   * The index must be a value greater than or equal to <code>0</code>
   * and less than the current size of the vector. <p>
   *
   * This method is identical in functionality to the set method
   * (which is part of the List interface). Note that the set method reverses
   * the order of the parameters, to more closely match array usage.  Note
   * also that the set method returns the old value that was stored at the
   * specified position.
   *
   * @param      obj     what the component is to be set to.
   * @param      index   the specified index.
   * @exception  ArrayIndexOutOfBoundsException  if the index was invalid.
   * @see        #size()
   * @see        List
   * @see	   #set(int, java.lang.Object)
   */
  public synchronized void setElementAt(Object obj, int index) {
    if (!contains(obj))  //if object is not already in the vector
      super.setElementAt(obj, index);
  }
}