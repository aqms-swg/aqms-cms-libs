package com.isti.util;

public interface Version
{
    /** Version string for program. */
  public static final String PROGRAM_VERSION = "2.4.262";
}
