//WriterOutputStream.java:  Converts a 'Writer' to an 'OutputStream'.

package com.isti.util;

import java.io.OutputStream;
import java.io.IOException;
import java.io.Writer;


/**
 * Class WriterOutputStream converts a 'Writer' to an 'OutputStream'.
 */
public class WriterOutputStream extends OutputStream
{
  protected final Writer writerObj;

    /**
     * Creates a 'Writer' to 'OutputStream' converter.
     * @param writerObj the source 'Writer' object.
     */
  public WriterOutputStream(Writer writerObj)
  {
    this.writerObj = writerObj;
  }

    /**
     * Writes a character.
     * @param ch character
     * @throws IOException
     */
  public void write(int ch) throws IOException
  {
    writerObj.write(ch);
  }

    /**
     * Writes the given data array.
     * @param data data array
     * @throws IOException
     */
  public void write(byte[] data) throws IOException
  {
    write(data,0,data.length);
  }

    /**
     * Writes the given section of the given array.
     * @param data data array
     * @param start start index
     * @param len length
     * @throws IOException
     */
  public void write(byte[] data,int start,int len) throws IOException
  {
    writerObj.write(new String(data,start,len));
  }

    /**
     * Flushes the stream.
     * @throws IOException
     */
  public void flush() throws IOException
  {
    writerObj.flush();
  }

    /**
     * Closes the stream.
     * @throws IOException
     */
  public void close() throws IOException
  {
    writerObj.close();
  }
}
