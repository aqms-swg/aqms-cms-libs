//XmlConfigLoader.java:  Defines a loader for reading configuration-property
//                       settings from an XML-format input file.
//
//  12/5/2003 -- [ET]  Initial version.
//  3/12/2004 -- [ET]  Modified 'loadConfiguration()' to allow configuration
//                     file to be missing; modified to clear any fetched
//                     error message at beginning of 'loadConfiguration()'
//                     method.
//  5/20/2004 -- [ET]  Added optional 'defaultsFlag' parameter to the
//                     'loadConfiguration()' method and modified so that
//                     if the configuration file is not found then any
//                     previously-setup root element is left unchanged.
//  9/16/2004 -- [ET]  Modified 'loadConfiguration()' so that if "display-
//                     version" or "display-help" parameter given without
//                     valid config file present then an error message is
//                     not generated.
//  2/21/2006 -- [ET]  Added optional 'cfgParamSpecStr' parameter to
//                     'loadConfiguration()' method; fixed implementation
//                     of strings set via 'setHelpParamNames()' method.
//  2/28/2008 -- [KF]  Added 'getVersionString()' and 'setVersionString()'
//                     methods.
//

package com.isti.util;

import java.util.List;
import java.util.Vector;
import org.jdom.Element;

/**
 * Class XmlConfigLoader defines a loader for reading configuration-property
 * settings from an XML-format input file.
 */
public class XmlConfigLoader extends IstiXmlUtils
{
         //default names for display-version-info parameters:
  protected static final String DEF_VER1_STR = "version";
  protected static final String DEF_VER2_STR = "v";
         //default names for display-help-info parameters:
  protected static final String DEF_HELP1_STR = "help";
  protected static final String DEF_HELP2_STR = "h";
         //names for display-version-info parameters:
  protected static String version1String = DEF_VER1_STR;
  protected static String version2String = DEF_VER2_STR;
         //names for display-help-info parameters:
  protected static String help1String = DEF_HELP1_STR;
  protected static String help2String = DEF_HELP2_STR;
  private String versionString = null;

  /**
   * Loads the XML-format configuration file and loads the set of
   * "name = value" items specified by the "Settings" element in
   * the configuration file.  Attempts to open the given name as a
   * local file, as a URL, as a resource and as an entry in a 'jar'
   * file (whichever works first).  Command-line parameters may also
   * be processed via this method.  If the configuration file is not
   * found and 'mustFlag'==false then an empty root element containing
   * a "Settings" element is created.  If the display-version-info or
   * display-help-info command-line parameter is detected then the
   * program is terminated (via 'System.exit(0)') after the information
   * is sent to the console.
   * @param fileName the name of the XML file to load.
   * @param rootElementName the expected name of the root element in the
   * XML file, or null to accept any name.
   * @param settingsElementName the name of the "Settings" element whose
   * text contains the "name = value" items.
   * @param mustFlag if true and a configuration file with a "Settings"
   * element is not found then an error will be generated and the method
   * will return 'false'.
   * @param cfgPropObj the 'CfgProperties' object to be loaded from the
   * configuration file.
   * @param programArgs string array of command-line parameters to be
   * processed, or null for none.
   * @param defaultsFlag true to load the items as defaults into the
   * configuration-properties object; false to load the items normally.
   * @param cfgParamSpecStr name of command-line parameter for specifying
   * the configuration file, or null for none.
   * @return 'true' if successful, false if an error occurred (in which
   * case the 'getErrorMessage()' method may be used to fetch a
   * description of the error).
   */
  public boolean loadConfiguration(String fileName, String rootElementName,
                               String settingsElementName, boolean mustFlag,
                            CfgProperties cfgPropObj, String [] programArgs,
                               boolean defaultsFlag, String cfgParamSpecStr)
  {
    clearFetchedErrorMessage();        //clear prev error msg (if fetched)
    if(settingsElementName == null || cfgPropObj == null)
    {    //null parameter; set error message
      setErrorMessageString("Null parameter");
      return false;
    }
    boolean retFlag = true;

       //check for leading 'cfgParamSpecStr' parameter:
    final String arg0Str;
    final int specLen;
    if(cfgParamSpecStr != null &&
                            (specLen=cfgParamSpecStr.trim().length()) > 0 &&
                            programArgs != null && programArgs.length > 0 &&
                                           (arg0Str=programArgs[0]) != null)
    {    //config-file-specifier parameter-name given and
         // at least one command-line parameter available
      final int arg0Len = arg0Str.length();      //length of first parameter
      int p = 0;      //skip past up to 2 leading dash characters:
      while(p < arg0Len && arg0Str.charAt(p) == '-' && ++p < 2);
      if(arg0Len >= p+specLen)
      {  //parameter is long enough for match
            //if equals char found then trim param; else use full length:
        final int sepPos = (arg0Len > p+specLen+1 &&
                  arg0Str.charAt(p+specLen) == '=') ? p+specLen : arg0Len;
        if(arg0Str.substring(p,sepPos).equals(cfgParamSpecStr))
        {   //parameter spec matched
          final int nameSlotVal;
          if(sepPos >= arg0Len)
          { //equals char was not found; use next parameter for filename
            if(programArgs.length > 1 && programArgs[1] != null)
            {    //valid second parameter is available
              fileName = programArgs[1];    //fetch filename parameter
              nameSlotVal = 1;       //indicate second parameter used
            }
            else
              nameSlotVal = 0;       //indicate only first parameter used
          }
          else
          {   //equals char was found; use rest of string for filename
            fileName = arg0Str.substring(sepPos+1);
            nameSlotVal = 0;         //indicate only first parameter used
          }
          final int len;     //check if name surrounded by quotes:
          if((len=fileName.length()) > 1 && ((fileName.charAt(0) == '\"' &&
                                          fileName.charAt(len-1) == '\"') ||
                                              (fileName.charAt(0) == '\'' &&
                                           fileName.charAt(len-1) == '\'')))
          {      //name is surrounded by quotes; remove them
            fileName = fileName.substring(1,len-1);
          }
          if(programArgs.length > nameSlotVal+1)
          {      //more command-line parameters are available
                 //"shift down" the remaining command-line parameters:
            int newPos = 0;
            final String [] newArgsArr =  //new string array for params
                          new String[programArgs.length-nameSlotVal-1];
                      //for each remaining param, copy to new array:
            for(p=nameSlotVal+1; p<programArgs.length; ++p)
              newArgsArr[newPos++] = programArgs[p];
            programArgs = newArgsArr;     //enter new array of params
          }
          else   //no more command-line parameters are available
            programArgs = new String[0];  //setup empty array
        }
      }
    }

    if(!loadFile(fileName,rootElementName))      //load XML file data
    {    //unable to load date from file
              //if "must" flag is set or if the config file was successfully
              // loaded (meaning file syntax error) then return error:
      if(mustFlag || getLoadFileOpenedFlag())
        retFlag = false;               //setup to return error flag
      else
        clearErrorMessageString();     //clear generated error message
              //if no current root element then create empty root element:
      if(getRootElement() == null)
        setRootElement(new Element(rootElementName));
    }
    Element rootElemObj;                   //check root element
    if((rootElemObj=getRootElement()) == null)
    {    //root element not found
      if(mustFlag)
      {  //"must" flag is set; build error message
        enterErrorMessageString("Unable to load root element from file \"" +
                                                           fileName + "\"");
        retFlag = false;               //setup to return error flag
      }
                             //create empty root element:
      setRootElement(rootElemObj = new Element(rootElementName));
    }
         //get list of "Settings" elements:
    final List settingsElemsList =
                               rootElemObj.getChildren(settingsElementName);
    if(settingsElemsList.size() > 0)
    {    //at least one "Settings" element found
      if(settingsElemsList.size() > 1)
      {  //more than one "Settings" element found
        enterErrorMessageString("More than one \"" + settingsElementName +
                               "\" element found in configuration file \"" +
                                                           fileName + "\"");
        retFlag = false;               //setup to return error flag
      }
    }
    else
    {    //no "Settings" elements found
      if(mustFlag)
      {  //"must" flag is set; build error message
        enterErrorMessageString("No \"" + settingsElementName +
                              "\" elements found in configuration file \"" +
                                                           fileName + "\"");
        retFlag = false;               //setup to return error flag
      }
              //create and add "Settings" element:
      settingsElemsList.add(new Element(settingsElementName));
    }
         //fetch "Settings" element object:
    Object obj;
    Element elementObj;
    String elemNameStr = null;
    if(!((obj=settingsElemsList.get(0)) instanceof Element) ||
                                                !settingsElementName.equals(
                       (elemNameStr=((elementObj=(Element)obj).getName()))))
    {    //unable to fetch Element or name is not "Settings"; set err msg
      enterErrorMessageString("Error fetching \"" + settingsElementName +
                        "\" element from configuration file \"" + fileName +
                               "\" (found name = \"" + elemNameStr + "\")");
      retFlag = false;                 //setup to return error flag
      elementObj = new Element(settingsElementName);  //setup dummy element
    }
         //process text inside "Settings" element:
              //fetch text inside element, load "name = value"
              // items into 'CfgProperties' object:
    if(!cfgPropObj.load(elementObj.getText(),defaultsFlag))
    {    //error processing "name = value" items; fetch and show error msg
      enterErrorMessageString("Error processing \"" + settingsElementName +
                        "\" element from configuration file \"" + fileName +
                                    "\":  " + cfgPropObj.getErrorMessage());
      retFlag = false;                 //setup to return error flag
    }

    if(programArgs != null && programArgs.length > 0)
    {
         //process command-line parameters:
      final Vector vec;
      final String str;
      final boolean cmdLnProcFlag = cfgPropObj.processCmdLnParams(
                            programArgs,true,version1String,version2String);
      if((vec=cfgPropObj.getExtraCmdLnParamsVec()) != null &&
             !vec.isEmpty() && (obj=vec.firstElement()) instanceof String &&
                                                 ((String)obj).length() > 0)
      {  //extra cmd-ln params were gathered and first one is valid String
                        //remove any leading switch characters:
        str = CfgProperties.removeSwitchChars((String)obj);
        if(str.equalsIgnoreCase(help1String) ||
                   str.equalsIgnoreCase(help2String) || str.startsWith("?"))
        {     //parameter matches one for help screen request; show data
          System.out.println("Command-line parameters:");
          System.out.println(cfgPropObj.getHelpScreenData());
          System.exit(0);         //exit program
        }
        else if(str.equalsIgnoreCase(version2String) ||
                                       str.equalsIgnoreCase(version1String))
        {     //show version information
          if (versionString != null)
          {
            System.out.println(versionString);
          }
          System.exit(0);    //exit program
        }
        else
        {     //unexpected parameter; set error message:
          setErrorMessageString("Invalid command-line parameter \"" +
                                                      ((String)obj) + "\"");
          retFlag = false;                  //setup to return error flag
        }
      }
      if(!cmdLnProcFlag)
      {  //error processing cmd-line params; set error message
        setErrorMessageString(cfgPropObj.getErrorMessage());
        retFlag = false;
      }
    }
    return retFlag;
  }

  /**
   * Loads the XML-format configuration file and loads the set of
   * "name = value" items specified by the "Settings" element in
   * the configuration file.  Attempts to open the given name as a
   * local file, as a URL, as a resource and as an entry in a 'jar'
   * file (whichever works first).  Command-line parameters may also
   * be processed via this method.  If the configuration file is not
   * found and 'mustFlag'==false then an empty root element containing
   * a "Settings" element is created.  If the display-version-info or
   * display-help-info command-line parameter is detected then the
   * program is terminated (via 'System.exit(0)') after the information
   * is sent to the console.
   * @param fileName the name of the XML file to load.
   * @param rootElementName the expected name of the root element in the
   * XML file, or null to accept any name.
   * @param settingsElementName the name of the "Settings" element whose
   * text contains the "name = value" items.
   * @param mustFlag if true and a configuration file with a "Settings"
   * element is not found then an error will be generated and the method
   * will return 'false'.
   * @param cfgPropObj the 'CfgProperties' object to be loaded from the
   * configuration file.
   * @param programArgs string array of command-line parameters to be
   * processed, or null for none.
   * @param cfgParamSpecStr name of command-line parameter for specifying
   * the configuration file, or null for none.
   * @return 'true' if successful, false if an error occurred (in which
   * case the 'getErrorMessage()' method may be used to fetch a
   * description of the error).
   */
  public boolean loadConfiguration(String fileName,String rootElementName,
                                String settingsElementName,boolean mustFlag,
                             CfgProperties cfgPropObj,String [] programArgs,
                                                     String cfgParamSpecStr)
  {
    return loadConfiguration(fileName,rootElementName,settingsElementName,
                     mustFlag,cfgPropObj,programArgs,false,cfgParamSpecStr);
  }

  /**
   * Loads the XML-format configuration file and loads the set of
   * "name = value" items specified by the "Settings" element in
   * the configuration file.  Attempts to open the given name as a
   * local file, as a URL, as a resource and as an entry in a 'jar'
   * file (whichever works first).  Command-line parameters may also
   * be processed via this method.  If the configuration file is not
   * found and 'mustFlag'==false then an empty root element containing
   * a "Settings" element is created.  If the display-version-info or
   * display-help-info command-line parameter is detected then the
   * program is terminated (via 'System.exit(0)') after the information
   * is sent to the console.
   * @param fileName the name of the XML file to load.
   * @param rootElementName the expected name of the root element in the
   * XML file, or null to accept any name.
   * @param settingsElementName the name of the "Settings" element whose
   * text contains the "name = value" items.
   * @param mustFlag if true and a configuration file with a "Settings"
   * element is not found then an error will be generated and the method
   * will return 'false'.
   * @param cfgPropObj the 'CfgProperties' object to be loaded from the
   * configuration file.
   * @param programArgs string array of command-line parameters to be
   * processed, or null for none.
   * @return 'true' if successful, false if an error occurred (in which
   * case the 'getErrorMessage()' method may be used to fetch a
   * description of the error).
   */
  public boolean loadConfiguration(String fileName,String rootElementName,
                                String settingsElementName,boolean mustFlag,
                             CfgProperties cfgPropObj,String [] programArgs)
  {
    return loadConfiguration(fileName,rootElementName,settingsElementName,
                                mustFlag,cfgPropObj,programArgs,false,null);
  }

  /**
   * Loads the XML-format configuration file and loads the set of
   * "name = value" items specified by the "Settings" element in
   * the configuration file.  Attempts to open the given name as a
   * local file, as a URL, as a resource and as an entry in a 'jar'
   * file (whichever works first).  If the configuration file is not
   * found and 'mustFlag'==false then an empty root element containing
   * a "Settings" element is created.  If the display-version-info or
   * display-help-info command-line parameter is detected then the
   * program is terminated (via 'System.exit(0)') after the information
   * is sent to the console.
   * @param fileName the name of the XML file to load.
   * @param rootElementName the expected name of the root element in the
   * XML file, or null to accept any name.
   * @param settingsElementName the name of the "Settings" element whose
   * text contains the "name = value" items.
   * @param mustFlag if true and a configuration file with a "Settings"
   * element is not found then an error will be generated and the method
   * will return 'false'.
   * @param cfgPropObj the 'CfgProperties' object to be loaded from the
   * configuration file.
   * @param defaultsFlag true to load the items as defaults into the
   * configuration-properties object; false to load the items normally.
   * @return 'true' if successful, false if an error occurred (in which
   * case the 'getErrorMessage()' method may be used to fetch a
   * description of the error).
   */
  public boolean loadConfiguration(String fileName,String rootElementName,
                                String settingsElementName,boolean mustFlag,
                              CfgProperties cfgPropObj,boolean defaultsFlag)
  {
    return loadConfiguration(fileName,rootElementName,settingsElementName,
                                mustFlag,cfgPropObj,null,defaultsFlag,null);
  }

  /**
   * Gets the version string to be displayed.
   * @return the version string or null if none.
   */
  public String getVersionString()
  {
    return versionString;
  }

  /**
   * Sets the version string to be displayed.
   * @param s the version string or null if none.
   */
  public void setVersionString(String s)
  {
    versionString = s;
  }

  /**
   * Sets the names for the display-version-info command-line parameters.
   * If this method is not used then the parameters will default to
   * "version" and "v".
   * @param ver1Str first parameter name to use.
   * @param ver2Str second parameter name to use.
   */
  public static void setVersionParamNames(String ver1Str,String ver2Str)
  {
    version1String = ver1Str;
    version2String = ver2Str;
  }

  /**
   * Sets the names for the display-help-info command-line parameters.
   * If this method is not used then the parameters will default to
   * "help" and "h".
   * @param help1Str first parameter name to use.
   * @param help2Str second parameter name to use.
   */
  public static void setHelpParamNames(String help1Str,String help2Str)
  {
    help1String = help1Str;
    help2String = help2Str;
  }
}
