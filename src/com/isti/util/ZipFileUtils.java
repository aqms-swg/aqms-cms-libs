//ZipFileUtils.java:  Contains various static utility methods
//                    for working with ZIP files.
//
//  6/20/2003 -- [KF]  Initial version.
//  8/21/2003 -- [ET]  Took out unneeded references to 'TopLevelDummy'.
//  10/3/2003 -- [ET]  Modified 'unzipZipFile()' method to deal with the
//                     ability of 'ZipFile' to throw an "InternalError"
//                     (via enumerator's 'nextElement()' method) and
//                     modified it to throw 'ZipException' in response
//                     to any error (no more return flag).
//  10/6/2003 -- [ET]  Removed unneeded 'fileUtilsObj' variable.
//  4/28/2004 -- [KF]  Added 'FILE_EXTENSIONS' and 'hasZipExtension' method.
//   6/2/2004 -- [KF]  Added support for directories.
//   6/9/2004 -- [KF]  Use 'mkdirs' instead of 'mkdir', added optional filename
//                     filter and 'junkPathsFlag' parameters, return the number
//                     of files unzipped.
//  9/13/2004 -- [ET]  Added optional 'keepDateFlag' parameter to
//                     'unzipZipFile()' methods.
//   5/2/2006 -- [ET]  Added 'getSingleDirInZip()' method.
//   5/9/2006 -- [ET]  Modified 'getSingleDirInZip()' method to only check
//                     topmost directory of each entry.
//  11/5/2007 -- [SH]  Added a zipAndDeleteDir() method to zip up a directory
//                     that we want to remove, but want to store first.
//  6/14/2011 -- [ET]  Added 'extractFileFromZip()' method.
//

package com.isti.util;

import java.util.Enumeration;
import java.util.zip.ZipFile;
import java.util.zip.ZipEntry;
import java.util.zip.ZipException;
import java.util.zip.ZipOutputStream;
import java.io.*;

/**
 * Class ZipFileUtils contains various static utility methods
 * for working with ZIP files.
 */
public class ZipFileUtils
{
  /**
   * Zip file extensions (in lower case.)
   */
  public static String[] FILE_EXTENSIONS =
  {
    "zip",
    "jar",
  };

  //private constructor so that no object instances may be created
  // (static access only)
  private ZipFileUtils()
  {
  }

  /**
   * Determines if the specified filename ends with a ZIP file extension.
   * @param name the name of the zip file
   * @return true if the specified filename ends with a ZIP file extension.
   */
  public static boolean hasZipExtension(String name)
  {
    name = name.toLowerCase().trim();  //convert to lower case and trim whitespace
    for (int i = 0; i < FILE_EXTENSIONS.length; i++)
    {
      if (name.endsWith(FILE_EXTENSIONS[i]))
        return true;
    }
    return false;
  }

  /**
   * Opens a zip file for reading.
   *
   * <p>First, if there is a security
   * manager, its <code>checkRead</code> method
   * is called with the <code>name</code> argument
   * as its argument to ensure the read is allowed.
   *
   * @param name the name of the zip file
   * @return The zip file.
   * @exception ZipException if a ZIP format error has occurred
   * @exception IOException if an I/O error has occurred
   * @exception  SecurityException  if a security manager exists and its
   *             <code>checkRead</code> method doesn't allow read access to the file.
   * @see SecurityManager#checkRead(java.lang.String)
   */
  public static ZipFile openZipFile(String name) throws IOException
  {
    return new ZipFile(name);
  }

  /**
   * Opens a new <code>ZipFile</code> to read from the specified
   * <code>File</code> object in the specified mode.  The mode argument
   * must be either <tt>OPEN_READ</tt> or <tt>OPEN_READ | OPEN_DELETE</tt>.
   *
   * <p>First, if there is a security manager, its <code>checkRead</code>
   * method is called with the <code>name</code> argument as its argument to
   * ensure the read is allowed.
   *
   * @param file the ZIP file to be opened for reading
   * @param mode the mode in which the file is to be opened
   * @return The zip file.
   * @exception ZipException if a ZIP format error has occurred.
   * @exception IOException if an I/O error has occurred.
   * @exception  SecurityException  if a security manager exists and its
   *             <code>checkRead</code> method doesn't allow read access to the file.
   * @exception IllegalArgumentException
   *            If the <tt>mode</tt> argument is invalid.
   * @see SecurityManager#checkRead(java.lang.String)
   */
  public static ZipFile openZipFile(File file, int mode) throws IOException
  {
    return new ZipFile(file, mode);
  }

  /**
   * Opens a ZIP file for reading given the specified File object.
   * @param file the ZIP file to be opened for reading
   * @return The zip file.
   * @exception ZipException if a ZIP error has occurred.
   * @exception IOException if an I/O error has occurred.
   */
  public static ZipFile openZipFile(File file)
                                            throws ZipException, IOException
  {
    return new ZipFile(file);
  }

  /**
   * Unzips all entries from the zip file to the specified directory.
   * @param zipFileObj the zip file.
   * @param outputDir the output directory.
   * @return The number of files unzipped.
   * @exception ZipException if a ZIP error has occurred or if the
   * output directory could not be created.
   */
  public static int unzipZipFile(ZipFile zipFileObj, File outputDir)
                                                         throws ZipException
  {
    return unzipZipFile(zipFileObj, outputDir, UtilFns.EMPTY_STRING);
  }

  /**
   * Unzips all entries from the zip file to the specified directory.
   * @param zipFileObj the zip file.
   * @param outputDir the output directory.
   * @param keepDateFlag true to retain the last-modified date/time
   * values for the unzipped files; false to use the current time.
   * @return The number of files unzipped.
   * @exception ZipException if a ZIP error has occurred or if the
   * output directory could not be created.
   */
  public static int unzipZipFile(ZipFile zipFileObj, File outputDir,
                                   boolean keepDateFlag) throws ZipException
  {
    return unzipZipFile(zipFileObj, outputDir, keepDateFlag,
                                                      UtilFns.EMPTY_STRING);
  }

  /**
   * Unzips all entries from the zip file to the specified directory.
   * @param zipFileObj the zip file.
   * @param outputDir the output directory.
   * @param outputFilePrefix prefix to add to output filenames.
   * @return The number of files unzipped.
   * @exception ZipException if a ZIP error has occurred or if the
   * output directory could not be created.
   */
  public static int unzipZipFile(ZipFile zipFileObj, File outputDir,
                                String outputFilePrefix) throws ZipException
  {
    return unzipZipFile(zipFileObj, outputDir, false, outputFilePrefix,
                                                               null, false);
  }

  /**
   * Unzips all entries from the zip file to the specified directory.
   * @param zipFileObj the zip file.
   * @param outputDir the output directory.
   * @param keepDateFlag true to retain the last-modified date/time
   * values for the unzipped files; false to use the current time.
   * @param outputFilePrefix prefix to add to output filenames.
   * @return The number of files unzipped.
   * @exception ZipException if a ZIP error has occurred or if the
   * output directory could not be created.
   */
  public static int unzipZipFile(ZipFile zipFileObj, File outputDir,
          boolean keepDateFlag, String outputFilePrefix) throws ZipException
  {
    return unzipZipFile(zipFileObj, outputDir, keepDateFlag,
                                             outputFilePrefix, null, false);
  }

  /**
   * Unzips entries from the zip file to the specified directory.
   * @param zipFileObj the zip file.
   * @param outputDir the output directory.
   * @param outputFilePrefix prefix to add to output filenames.
   * @param filter The filename filter to use, or null for none.
   * @param junkPathsFlag tue to junk paths (do not make directories),
   * false otherwise.
   * @exception ZipException if a ZIP error has occurred or if the
   * output directory could not be created.
   * @see IstiFileFilter.
   * @return The number of files unzipped.
   */
  public static int unzipZipFile(ZipFile zipFileObj, File outputDir,
                                 String outputFilePrefix, FileFilter filter,
                                  boolean junkPathsFlag) throws ZipException
  {
    return unzipZipFile(zipFileObj, outputDir, false, outputFilePrefix,
                                                     filter, junkPathsFlag);
  }

  /**
   * Unzips entries from the zip file to the specified directory.
   * @param zipFileObj the zip file.
   * @param outputDir the output directory.
   * @param keepDateFlag true to retain the last-modified date/time
   * values for the unzipped files; false to use the current time.
   * @param outputFilePrefix prefix to add to output filenames.
   * @param filter The filename filter to use, or null for none.
   * @param junkPathsFlag tue to junk paths (do not make directories),
   * false otherwise.
   * @exception ZipException if a ZIP error has occurred or if the
   * output directory could not be created.
   * @see IstiFileFilter.
   * @return The number of files unzipped.
   */
  public static int unzipZipFile(ZipFile zipFileObj, File outputDir,
                              boolean keepDateFlag, String outputFilePrefix,
                                   FileFilter filter, boolean junkPathsFlag)
                                                         throws ZipException
  {
    //if the directory does not exist and can't be created or
    // is not a directory
    if (!outputDir.exists() ? !outputDir.mkdirs() : !outputDir.isDirectory())
    {
      throw new ZipException("ZipFileUtils.unzipZipFile:  Unable to " +
                 "create output directory \"" + outputDir.getName() + "\"");
    }

    int fileCount = 0;
    InputStream in = null;
    OutputStream out = null;
    long entryTimeVal;
    File ouputFile;
    final Enumeration entries = zipFileObj.entries();
    while (entries.hasMoreElements())  //while there are more entries
    {
      entryTimeVal = 0;           //initialize time value
      ouputFile = null;           //initialize handle to output file
      try
      {
        //get the next entry
        final ZipEntry ze = (ZipEntry)entries.nextElement();
        final String outputFileName = ze.getName();
        if(keepDateFlag)                    //if retaining time value then
          entryTimeVal = ze.getTime();      //fetch time value for entry

        //output file
        ouputFile = new File(outputDir, outputFilePrefix + outputFileName);

        //if file filter was specified and file doesn't match
        if (filter != null && !filter.accept(ouputFile))
          continue;  //skip it

        final File parentFile = ouputFile.getParentFile();
        if (parentFile != null)  //if there is a parent file
        {
          if (junkPathsFlag)  //if junking paths
          {
            //only use the output file name, not the path
            ouputFile =
                new File(outputDir, outputFilePrefix + ouputFile.getName());
          }
          //make sure the directory exists
          else if (!parentFile.isDirectory() && !parentFile.mkdirs())
          {
            if (parentFile.exists())
              throw new ZipException(
                  "ZipFileUtils.unzipZipFile: file exists with the same name " +
                  "as the directory \"" + parentFile.getName() + "\"");
            else
              throw new ZipException(
                  "ZipFileUtils.unzipZipFile: could not create directory \"" +
                  parentFile.getName() + "\"");
          }
        }

        if (ze.isDirectory())  //if the entry is a directory
        {
          //make sure the directory exists
          if (!ouputFile.isDirectory() && !ouputFile.mkdirs())
          {
            if (ouputFile.exists())
              throw new ZipException(
                  "ZipFileUtils.unzipZipFile: file exists with the same name " +
                  "as the directory \"" + ouputFile.getName() + "\"");
            else
              throw new ZipException(
                  "ZipFileUtils.unzipZipFile: could not create directory \"" +
                  ouputFile.getName() + "\"");
          }
        }
        else
        {
          //create the input and ouput streams
          in = zipFileObj.getInputStream(ze);
          out = new FileOutputStream(ouputFile);

          //transfer the zip file
          FileUtils.transferStream(in, out);
          fileCount++;  //increment the file count
        }
      }
      catch (Throwable throwableObj)
      {       //this block catches a "Throwable" instead of an "Exception"
              // because 'ZipFile' can throw an 'InternalError'
        throw new ZipException("ZipFileUtils.unzipZipFile:  " +
                                                              throwableObj);
      }
      finally
      {       //always close I/O streams
        FileUtils.close(in);   //close the input stream
        FileUtils.close(out);  //close the output stream
      }
         //if entry time setup and file handle OK then set last-mod time:
      if(entryTimeVal > 0 && ouputFile != null)
        ouputFile.setLastModified(entryTimeVal);
    }
    return fileCount;
  }

  /**
   * Determines if the given archive contains entries residing in a
   * single directory and returns the directory name if so.
   * @param zipFileObj archive to use.
   * @return The name of the directory if the archive contains a single
   * directory, an empty string if no directories were found, or null
   * if multiple directories were found.
   */
  public static String getSingleDirInZip(ZipFile zipFileObj)
  {
    String retDirStr;
    final Enumeration enumObj;
    if(zipFileObj != null && (enumObj=zipFileObj.entries()) != null &&
                                                  enumObj.hasMoreElements())
    {    //zip file OK and contains entries
      retDirStr = null;
      Object obj;
      ZipEntry entryObj;
      File dirFileObj;
      String str;
      do
      {  //for each entry in zip file
        if((obj=enumObj.nextElement()) instanceof ZipEntry)
        {     //zip file entry fetched OK
          entryObj = (ZipEntry)obj;
          if(!entryObj.isDirectory())
          {   //entry is not a directory
                   //get topmost parent directory for zip-file entry:
            if((dirFileObj=(new File(
                              entryObj.getName())).getParentFile()) != null)
            {      //parent directory found
              do             //save directory name; check for parent:
                str = dirFileObj.getPath();
              while((dirFileObj=dirFileObj.getParentFile()) != null);
            }
            else
              str = UtilFns.EMPTY_STRING;   //if no dir then empty string
            if(retDirStr == null)      //if no previous dirs found then
              retDirStr = str;         //use current directory
            else if(!str.equals(retDirStr))
            {      //current directory different from previous directory
              retDirStr = null;        //indicate multiple directories
              break;                   //exit loop
            }
          }
        }
      }
      while(enumObj.hasMoreElements());
    }
    else      //no entries in zip file
      retDirStr = UtilFns.EMPTY_STRING;
    return retDirStr;
  }

  /**
   * from java2s.com online example - takes a directory, and zip's it up, and then deletes the dir from the disk
   * @param dirname String directory to zip up
   * @param zipFile String the file to put the dir in. adds ".zip" if it is missing. (if file exists, we delete it first)
   * @return String the name of the zip file. return null on error.
   * @throws FileNotFoundException
   * @throws IOException
   */
  public static String zipAndDeleteDir(String dirname, String zipFile) throws
      FileNotFoundException, IOException
  {

    if (!(hasZipExtension(zipFile))) {
      zipFile = zipFile.concat(".zip");
    }

    File zFile = new File(zipFile);
    if (zFile.exists()) {
      if (!(zFile.delete())) {
        // tried to delete the zip file and failed
        return null;
      }
    }

    // Check that the directory is a directory, and get its contents
    final File d = new File(dirname);
    if (!d.isDirectory())
      throw new IllegalArgumentException("Not a directory:  "
                                         + dirname);
    String[] entries = d.list();
    byte[] buffer = new byte[4096]; // Create a buffer for copying
    int bytesRead;

    FileOutputStream zipOut = new FileOutputStream(zipFile);
    ZipOutputStream out = new ZipOutputStream(zipOut);

    for (int i = 0; i < entries.length; i++) {
      File f = new File(d, entries[i]);
      if (f.isDirectory())
        continue;//Ignore directory
      FileInputStream in = new FileInputStream(f); // Stream to read file
      ZipEntry entry = new ZipEntry(f.getPath()); // Make a ZipEntry
      out.putNextEntry(entry); // Store entry
      while ((bytesRead = in.read(buffer)) != -1)
        out.write(buffer, 0, bytesRead);
      in.close();
    }
    out.close();

    if (!(FileUtils.deleteDirectory(d))) {
      // problem deleting the directory,
      return null;
    }

    return zipOut.toString();
  }

  /**
   * Extracts a file from the given '.zip' (or '.jar') archive to the
   * given location.  The directory path of the file in the zip archive
   * is disregarded.
   * @param zipFileObj File object for source '.zip' (or '.jar') archive.
   * @param destDirFileObj File object specifying destination directory.
   * @param fileNameStr name of file to be extracted.
   * @return true if successful; false if file could not be extracted.
   * @throws IOException if an I/O error occurs.
   */
  public static boolean extractFileFromZip(File zipFileObj,
                              File destDirFileObj, final String fileNameStr)
                                                          throws IOException
  {
         //create zip-file object for given file object:
    final ZipFile zFileObj = ZipFileUtils.openZipFile(zipFileObj);
         //unzip given filename from source zip file
         // (keep timestamps, junk any paths in jar file):
    int uzCount = ZipFileUtils.unzipZipFile(zFileObj,
                                   destDirFileObj,true,UtilFns.EMPTY_STRING,
      new FileFilter()
        {       //create filter for given filename:
          public boolean accept(File fileObj)
          {
            return fileNameStr.equalsIgnoreCase(fileObj.getName());
          }
        },true);
    if(uzCount > 0 && !(new File(destDirFileObj,fileNameStr)).exists())
      uzCount = 0;      //if unable to locate file then clear count
    return (uzCount > 0);         //return true if successful
  }
}
