//BilinearInterp.java:  Calculates results via bilinear interpolation.
//
//  12/29/2000 -- [ET]
//
//
//=====================================================================
// Copyright (C) 2000 Instrumental Software Technologies, Inc.
//
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions
// are met:
// 1. Redistributions of source code, or portions of this source code,
//    must retain the above copyright notice, this list of conditions
//    and the following disclaimer.
// 2. Redistributions in binary form must reproduce the above copyright
//    notice, this list of conditions and the following disclaimer in
//    the documentation and/or other materials provided with the
//    distribution.
// 3. All advertising materials mentioning features or use of this
//    software must display the following acknowledgment:
//    "This product includes software developed by Instrumental
//    Software Technologies, Inc. (http://www.isti.com)"
// 4. If the software is provided with, or as part of a commercial
//    product, or is used in other commercial software products the
//    customer must be informed that "This product includes software
//    developed by Instrumental Software Technologies, Inc.
//    (http://www.isti.com)"
// 5. The names "Instrumental Software Technologies, Inc." and "ISTI"
//    must not be used to endorse or promote products derived from
//    this software without prior written permission. For written
//    permission, please contact "info@isti.com".
// 6. Products derived from this software may not be called "ISTI"
//    nor may "ISTI" appear in their names without prior written
//    permission of Instrumental Software Technologies, Inc.
// 7. Redistributions of any form whatsoever must retain the following
//    acknowledgment:
//    "This product includes software developed by Instrumental
//    Software Technologies, Inc. (http://www.isti.com/)."
// THIS SOFTWARE IS PROVIDED BY INSTRUMENTAL SOFTWARE
// TECHNOLOGIES, INC. "AS IS" AND ANY EXPRESSED OR IMPLIED
// WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES
// OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
// DISCLAIMED.  IN NO EVENT SHALL INSTRUMENTAL SOFTWARE TECHNOLOGIES,
// INC. OR ITS CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
// INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
// (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
// SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
// HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
// STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
// ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED
// OF THE POSSIBILITY OF SUCH DAMAGE.
//=====================================================================
//  A current version of the software can be found at
//                http://www.isti.com
//  Bug reports and comments should be directed to
//  Instrumental Software Technologies, Inc. at info@isti.com
//=====================================================================
//

package com.isti.util.bilinearinterp;

import java.io.*;
import java.util.*;
import com.isti.util.UtilFns;

/**
 * Class BilinearInterp calculate results via bilinear interpolation.
 * The class is instantiated with two-dimensional data from an input
 * stream.  The formulas used are described as "bilinear interpolation"
 * in the book "Numerical Recipies in C".
 */
public class BilinearInterp
{
                   //characters that can be valid in a numeric string:
  private static final String NUMERIC_CHARS = "-+.0123456789eE";
  private boolean errorFlag = false;        //true if error in input data
  private String errorMessage = null;       //last error message
  private int dataRowSize = 0;              //size of data "row"
  private int dataColSize = 0;              //size of data "column"
  private double [] x1ValsArr = null;       //array of 'x1' values
  private double [] x2ValsArr = null;       //array of 'x2' values
  private double [][] yValsArr = null;      //"grid" array of 'y' values
              //variables for tracking min/max on both 'x' values:
  private double minX1Value = 0.0, maxX1Value = 0.0;
  private double minX2Value = 0.0, maxX2Value = 0.0;

    /**
     * Constructs an object for calculating the results of bilinear
     * interpolations using a given data set.  If an error was detected
     * in the given data set, the 'getErrorFlag()' method will return
     * 'true' and the 'getErrorMessage()' method will report the error
     * message text.
     * @param readerObj an input stream Reader object references a data
     * set made up of lines in the following format:
     *         x1 x2 y
     * where 'y' is the result value for the given (x1,x2) point, and
     * the set of x1,x2 values must define a "grid" pattern.
     */
  public BilinearInterp(Reader readerObj)
  {
    if(readerObj == null)
    {         //null parameter
      setErrorMessage("Null parameter");    //enter error message
      return;                               //abort constructor
    }
                             //create buffered version of reader object:
    final BufferedReader rdr = new BufferedReader(readerObj);
                             //create Vector for 'EntryBlock' objects
    final Vector entriesVec = new Vector();
    int lineNum = 0;         //current line number in stream
              //init variables for tracking min/max on both 'x' values:
    minX1Value = minX2Value = Double.MAX_VALUE;
    maxX1Value = maxX2Value = -Double.MAX_VALUE;
    double x1Val,x2Val,yVal;
    int len,sPos,ePos;
    String lineStr;
    try
    {
      while((lineStr=rdr.readLine()) != null)
      {  //for each line of data from input stream
        ++lineNum;
//        if((lineNum-1) % 100 == 0)
//          System.out.print("\rlineNum = " + lineNum);
        lineStr = lineStr.trim();      //trim any leading/trailing spaces
        if((len=lineStr.length()) > 0 && !lineStr.startsWith("#") &&
                                                  !lineStr.startsWith("//"))
        {     //line contains data and doesn't begin as comment
          if((ePos=lineStr.indexOf(' ')) <= 0)   //find 1st space
          {        //no space character found; enter error message
            setErrorMessage("No space-character separators found on line " +
                                                                   lineNum);
            return;                         //abort constructor
          }
          try
          {        //attempt to parse string to double:
            x1Val = Double.parseDouble(lineStr.substring(0,ePos));
          }
          catch(NumberFormatException ex)
          {        //error parsing string; enter error message
            setErrorMessage("Number format error parsing \"" +
                       lineStr.substring(0,ePos) + "\" on line " + lineNum);

            return;                         //abort constructor
          }
          sPos = ePos + 1;        //start at next character after space
          if((ePos=lineStr.indexOf(' ',sPos)) <= 0)   //find 2nd space
          {        //no space character found; enter error message
            setErrorMessage("Second space-character separator " +
                                            "not found on line " + lineNum);
            return;                         //abort constructor
          }
          try
          {        //attempt to parse string to double:
            x2Val = Double.parseDouble(lineStr.substring(sPos,ePos));
          }
          catch(NumberFormatException ex)
          {        //error parsing string; enter error message
            setErrorMessage("Number format error parsing \"" +
                    lineStr.substring(sPos,ePos) + "\" on line " + lineNum);

            return;                         //abort constructor
          }
          sPos = ePos + 1;        //start at next character after space
          if(sPos >= len)
          {        //no more data on line to parse; enter error message
            setErrorMessage("Third data value not found on line " +
                                                                   lineNum);
            return;                         //abort constructor
          }
          try
          {        //attempt to parse string to double:
            yVal = Double.parseDouble(lineStr.substring(sPos));
          }
          catch(NumberFormatException ex)
          {        //error parsing string; enter error message
            setErrorMessage("Number format error parsing \"" +
                         lineStr.substring(sPos) + "\" on line " + lineNum);

            return;                         //abort constructor
          }
          if(x1Val < minX1Value)       //track min/max values for 'x1'
            minX1Value = x1Val;
          if(x1Val > maxX1Value)
            maxX1Value = x1Val;
          if(x2Val < minX2Value)       //track min/max values for 'x2'
            minX2Value = x2Val;
          if(x2Val > maxX2Value)
            maxX2Value = x2Val;
                        //create 'EntryBlock' object and add to Vector:
          entriesVec.add(new EntryBlock(x1Val,x2Val,yVal));
        }
      }
    }
    catch(IOException ex)
    {              //I/O error while reading; enter error message
      setErrorMessage("I/O error reading input data:  " + ex);
      return;                               //abort constructor
    }
    final int entriesVecLen;
    if((entriesVecLen=entriesVec.size()) < 4)
    {    //no entries found; enter error message
      setErrorMessage("Less than 4 entries found");
      return;                               //abort constructor
    }
         //figure out if it's 'x1' or 'x2' that changes less often:
    EntryBlock eBlock;
    try
    {         //get (x1,x2) from first entry:
      eBlock = (EntryBlock)(entriesVec.elementAt(0));
      x1Val = eBlock.x1Val;
      x2Val = eBlock.x2Val;
              //get handle to second entry:
      eBlock = (EntryBlock)(entriesVec.elementAt(1));
    }
    catch(Exception ex)
    {
      setErrorMessage("Error interpreting data entries");
      return;                               //abort constructor
    }
    final boolean x2Flag;    //set true if 'x2' values change less often
    if(x1Val < eBlock.x1Val && x2Val == eBlock.x2Val)
      x2Flag = true;
    else if(x2Val < eBlock.x2Val && x1Val == eBlock.x1Val)
      x2Flag = false;
    else
    {
      setErrorMessage("Data entries do not start in proper grid format");
      return;                               //abort constructor
    }
         //figure out the size of a "row" in the grid:
    dataRowSize = 0;
    Enumeration e;
    Object obj;
    if(x2Flag)
    {    //the 'x2' column changes less often
      x1Val = -Double.MAX_VALUE;       //setup "previous" 'x1; value
      e = entriesVec.elements();       //get enum of data entries
      while(e.hasMoreElements())
      {       //for each data entry
        if((obj=e.nextElement()) instanceof EntryBlock)
        {     //valid 'EntryBlock' object fetched OK
          eBlock = (EntryBlock)obj;    //set handle to object
          if(eBlock.x2Val != x2Val)
          {   //'x2' value not equal to one in previous data entry
            if(dataRowSize < 2)
            {      //"row" size too short; enter error message
              setErrorMessage("'Row' size in grid not large enough");
              return;                               //abort constructor
            }
            break;           //exit loop
          }
          if(eBlock.x1Val <= x1Val)
          {      //'x1' value not increasing; enter error message
            setErrorMessage("'x1' value (" +
                                         eBlock.x1Val + ") not increasing");
            return;                               //abort constructor
          }
          x1Val = eBlock.x1Val;        //save 'x1' value for next time
          ++dataRowSize;               //increment "row" size
        }
      }
    }
    else
    {    //the 'x1' column changes less often
      x2Val = -Double.MAX_VALUE;       //setup "previous" 'x2; value
      e = entriesVec.elements();       //get enum of data entries
      while(e.hasMoreElements())
      {       //for each data entry
        if((obj=e.nextElement()) instanceof EntryBlock)
        {     //valid 'EntryBlock' object fetched OK
          eBlock = (EntryBlock)obj;    //set handle to object
          if(eBlock.x1Val != x1Val)
          {   //'x1' value not equal to one in previous data entry
            if(dataRowSize < 2)
            {      //"row" size too small; enter error message
              setErrorMessage("'Row' size in grid not large enough");
              return;                               //abort constructor
            }
            break;           //exit loop
          }
          if(eBlock.x2Val <= x2Val)
          {      //'x2' value not increasing; enter error message
            setErrorMessage("'x2' value (" +
                                         eBlock.x2Val + ") not increasing");
            return;                               //abort constructor
          }
          x2Val = eBlock.x2Val;        //save 'x2' value for next time
          ++dataRowSize;               //increment "row" size
        }
      }
    }
    if(dataRowSize <= 0 || entriesVecLen % dataRowSize != 0)
    {    //bad "row" size or not proper row/column grid; enter error msg
      setErrorMessage("Data entries not in 'even' grid format");
      return;                               //abort constructor
    }
    dataColSize = entriesVecLen / dataRowSize;   //calc "column" size
         //fill arrays with data values:
    if(x2Flag)
    {    //the 'x2' column changes less often
      x1ValsArr = new double[dataRowSize];  //setup 'x1' array as "column"
      x2ValsArr = new double[dataColSize];  //setup 'x2' array as "row"
                                            //setup 'y' values grid array:
      yValsArr = new double [dataRowSize][dataColSize];
      int j, k = 0;     //'j' increments as we go along a row; 'k', column
      e = entriesVec.elements();       //get enum of data entries
      x2Val = -Double.MAX_VALUE;       //setup "previous" 'x2' value
      while(k < dataColSize)
      {       //for each "row" in data "grid"
        x1Val = -Double.MAX_VALUE;     //setup "previous" 'x1' value
        j = 0;
        while(j < dataRowSize)
        {     //for each "column" in data "row"
          if(!e.hasMoreElements())
          {        //no more input data; enter error message
            setErrorMessage("Reached unexpected end of input data");
            return;                     //abort constructor
          }
          if((obj=e.nextElement()) instanceof EntryBlock)
          {        //valid 'EntryBlock' object fetched OK
            eBlock = (EntryBlock)obj;       //set handle to object
            if(j == 0)
            {      //on first "column" in "row"
              if(eBlock.x2Val <= x2Val)
              {    //'x2' value not increasing; enter error message
                setErrorMessage("'x2' value (" + eBlock.x2Val +
                                                        ") not increasing");
                return;                       //abort constructor
              }
                             //enter 'x2' value and save for next time:
              x2ValsArr[k] = x2Val = eBlock.x2Val;
            }
            else if(eBlock.x2Val != x2Val)
            {      //not first "column" and 'x2' value not same
              setErrorMessage("'x2' value (" + eBlock.x2Val +
                                                ") changes within \"row\"");
              return;                       //abort constructor
            }
            if(eBlock.x1Val <= x1Val)
            {      //'x1' value not increasing; enter error message
              setErrorMessage("'x1' value (" + eBlock.x1Val +
                                                        ") not increasing");
              return;                       //abort constructor
            }
                             //enter 'x1' value and save for next time:
            x1ValsArr[j] = x1Val = eBlock.x1Val;
                             //enter 'y' value for (x1,x2) entry
            yValsArr[j][k] = eBlock.yVal;
          }
          ++j;               //increment "column" count
        }
        ++k;                 //increment "row" count
      }

    }
    else
    {    //the 'x1' column changes less often
    }

  }

    //Enters given error message.
  private void setErrorMessage(String str)
  {
    errorMessage = str;           //enter error message
    errorFlag = true;             //set error flag
  }

    /**
     * Returns true if an error was detected in the data set used to
     * construct this object.
     * @return true if an error was detected in the data set used to
     * construct this object.
     */
  public boolean getErrorFlag()
  {
    return errorFlag;
  }

    /**
     * Returns the text message associated with the first error detected
     * in the data set used to construct this object.
     * @return  the text message associated with the first error detected
     * in the data set used to construct this object.
     */
  public String getErrorMessage()
  {
    return (errorMessage != null) ? errorMessage : "No error";
  }

    /**
     * Calculates, via bilinear interpolation, a resultant value given the
     * two data values and the data set held by this object.
     * @param x1Val the given 'x1' data value.
     * @param x2Val the given 'x2' data value.
     * @return A 'ResultBlock' object holding the resultant value or
     * with error flag(s) set to true.
     */
  public ResultBlock interpolateValues(double x1Val,double x2Val)
  {
    int idx;

    if(getErrorFlag())       //if constructor error then return message
      return new ResultBlock(x1Val,x2Val,getErrorMessage());
              //check if arrays and sizes are OK:
    if(x1ValsArr == null || x2ValsArr == null || yValsArr == null)
      return new ResultBlock(x1Val,x2Val,"Null array handle(s)");
    if(dataRowSize == 0 || dataColSize == 0)
      return new ResultBlock(x1Val,x2Val,"Data row or column size is zero");
              //check if given values are in range:
    if(x1Val < minX1Value || x1Val > maxX1Value ||
                                   x2Val < minX2Value || x2Val > maxX2Value)
    {    //one or more given values out of range
      return new ResultBlock(x1Val,x2Val);       //"out of range" error
    }
              //search array for given 'x1':
    idx = Arrays.binarySearch(x1ValsArr,x1Val);
    if(idx == x1ValsArr.length - 1)    //if matched last array element then
      --idx;                           //decrement by one element
              //set 'j' to index of entry below given 'x1':
    final int j = (idx >= 0) ? idx : (-(idx + 1) - 1);
              //search array for given 'x2':
    idx = Arrays.binarySearch(x2ValsArr,x2Val);
    if(idx == x2ValsArr.length - 1)    //if matched last array element then
      --idx;                           //decrement by one element
              //set 'k' to index of entry below given 'x2':
    final int k = (idx >= 0) ? idx : (-(idx + 1) - 1);
              //check if index values are OK:
    if(j < 0 || j >= x1ValsArr.length-1 || k < 0 || k >= x2ValsArr.length-1)
      return new ResultBlock(x1Val,x2Val);       //"out of range" error

    final double y1,y2,y3,y4;     //values of 4 surrounding data points
    final double t,u;             //position scale values, 0 to 1
    try
    {
      y1 = yValsArr[j][k];        //enter values for 4 surrounding points
      y2 = yValsArr[j+1][k];
      y3 = yValsArr[j+1][k+1];
      y4 = yValsArr[j][k+1];
              //calculate position scale value for 'x1':
      t = (x1Val - x1ValsArr[j]) / (x1ValsArr[j+1] - x1ValsArr[j]);
              //calculate position scale value for 'x2':
      u = (x2Val - x2ValsArr[k]) / (x2ValsArr[k+1] - x2ValsArr[k]);
    }
    catch(Exception ex)
    {    //error occurred (probably bad index or divide by zero)
      return new ResultBlock(x1Val,x2Val,"Internal computation error");
    }
              //calculate interpolated result:
    final double result = ((1-t) * (1-u) * y1) + (t * (1-u) * y2) +
                                            (t * u * y3) + ((1-t) * u * y4);
    return new ResultBlock(result,x1Val,x2Val,x1ValsArr[j],x2ValsArr[k],y1,
            x1ValsArr[j+1],x2ValsArr[k],y2,x1ValsArr[j+1],x2ValsArr[k+1],y3,
                                            x1ValsArr[j],x2ValsArr[k+1],y4);
  }

    /**
     * Calculates, via bilinear interpolation, a set of resultant values
     * given one or more sets of data value pairs, using the data set held
     * by this object.  This method uses a very "friendly" parse, in that
     * any characters that cannot be part of a valid numeric value are
     * simply ignored.
     * @param valuesStr a string containing pairs of numeric strings
     * representing sets of (x1,x2) values.
     * @return An array of 'ResultBlock' objects holding the resultant
     * values or with error flag(s) set to true.  If a parsing error occurs
     * anywhere in the given 'valuesStr' string then the returned array
     * will contain a single 'ResultBlock' object holding the error message.
     */
  public ResultBlock[] interpolateMultiString(String valuesStr)
  {
    final Vector resultsVec = new Vector(); //Vector of 'ResultBlock' objs
    int vecSize = 0;                        //size of Vector
    String errMsgStr = null;                //error message (or null)
    final int strLen;
    if(valuesStr != null &&
                         (strLen=(valuesStr=valuesStr.trim()).length()) > 0)
    {    //given string contains data
      int sPos=0,ePos;            //position index variables
      double x1Val,x2Val;         //parsed values variables
      while(sPos < strLen)
      {  //for each pair of numeric strings found
              //parse first numeric string of pair:
                             //skip any leading non-numeric characters:
        while(NUMERIC_CHARS.indexOf(valuesStr.charAt(sPos)) < 0 &&
                                                           ++sPos < strLen);
        if(sPos >= strLen)        //if end of string reached then
          break;                  //exit parsing loop
        ePos = sPos;         //scan through numeric characters:
        while(NUMERIC_CHARS.indexOf(valuesStr.charAt(ePos)) >= 0 &&
                                                           ++ePos < strLen);

//        System.out.println("sPos=" + sPos + ", ePos=" + ePos +
//                             ", 1st_str=" + valuesStr.substring(sPos,ePos));

        try
        {          //attempt to parse numeric string value:
          x1Val = Double.parseDouble(valuesStr.substring(sPos,ePos));
        }
        catch(NumberFormatException ex)
        {          //error parsing value; enter error message
          errMsgStr = "Error parsing value (\"" +
                                     valuesStr.substring(sPos,ePos) + "\")";
          break;             //exit parsing loop
        }
        if(ePos >= strLen)
        {     //reached end of string data; enter error message
          errMsgStr = "Second value of pair not found (after \"" +
                                     valuesStr.substring(sPos,ePos) + "\")";
          break;             //exit parsing loop
        }
              //parse second numeric string of pair:
        sPos = ePos;         //skip any leading non-numeric characters:
        while(NUMERIC_CHARS.indexOf(valuesStr.charAt(sPos)) < 0 &&
                                                           ++sPos < strLen);
        ePos = sPos;         //scan through numeric characters:
        while(NUMERIC_CHARS.indexOf(valuesStr.charAt(ePos)) >= 0 &&
                                                           ++ePos < strLen);
        try
        {          //attempt to parse numeric string value:
          x2Val = Double.parseDouble(valuesStr.substring(sPos,ePos));
        }
        catch(NumberFormatException ex)
        {          //error parsing value; enter error message
          errMsgStr = "Error parsing value (\"" +
                                     valuesStr.substring(sPos,ePos) + "\")";
          break;             //exit parsing loop
        }
        sPos = ePos;         //setup start position for next pair
              //perform interpolation; add result block to Vector:
        resultsVec.add(interpolateValues(x1Val,x2Val));
      }
              //if no error yet and no values parsed then enter error msg:
      if((vecSize=resultsVec.size()) <= 0 && errMsgStr == null)
        errMsgStr = "Unable to find any data values to parse";
    }
    else
    {    //no data in given string; setup return error message
      errMsgStr = "No data values given";
    }
              //if parsing error occurred then return array with single
              // 'ResultBlock' object containing error message:
    if(errMsgStr != null)
      return new ResultBlock[] {new ResultBlock(errMsgStr)};
              //convert Vector to 'ResultBlock' array and return:
    return (ResultBlock[])(resultsVec.toArray(new ResultBlock[vecSize]));
  }

    /**
     * Returns the data grid "row" size.
     * @return the data grid "row" size.
     */
  public int getDataRowSize()
  {
    return dataRowSize;
  }

    /**
     * Returns the data grid "column" size.
     * @return the data grid "column" size.
     */
  public int getDataColSize()
  {
    return dataColSize;
  }

    /**
     * Returns the minimum 'x1' values in the data set.
     * @return the minimum 'x1' values in the data set.
     */
  public double getMinX1Value()
  {
    return minX1Value;
  }

  /**
   * Returns the maximum 'x1' values in the data set.
   * @return the maximum 'x1' values in the data set.
   */
  public double getMaxX1Value()
  {
    return maxX1Value;
  }

  /**
   * Returns the minimum 'x2' values in the data set.
   * @return the minimum 'x2' values in the data set.
   */
  public double getMinX2Value()
  {
    return minX2Value;
  }

  /**
   * Returns the maximum 'x2' values in the data set.
   * @return the maximum 'x2' values in the data set.
   */
  public double getMaxX2Value()
  {
    return maxX2Value;
  }

  /**
   * Test program for class.
   * @param args arguments
   */
  public static void main(String[] args)
  {
    final String fName = (args.length > 0) ? args[0] : "fine_depth_25.xy";
    final Reader rdr;
    try
    {
      rdr = new FileReader(fName);
    }
    catch(Exception ex)
    {
      System.err.println("Error opening input file \"" + fName + "\"");
      return;
    }
    System.out.println("Loading data set file...");
    final BilinearInterp bInterpObj = new BilinearInterp(rdr);
    try { rdr.close(); }
    catch(IOException ex) {}
    if(bInterpObj.getErrorFlag())
    {
      System.err.println("BilinearInterp error:  " +
                                              bInterpObj.getErrorMessage());
      return;
    }

    System.out.println("rowSize=" + bInterpObj.getDataRowSize() +
                                ", colSize=" + bInterpObj.getDataColSize());
    System.out.println("minX1=" + bInterpObj.getMinX1Value() + ", maxX1=" +
                                                bInterpObj.getMaxX1Value());
    System.out.println("minX2=" + bInterpObj.getMinX2Value() + ", maxX2=" +
                                                bInterpObj.getMaxX2Value());
    String str;
    ResultBlock [] rBlockArr;
    int idx;
    while(true)
    {
      System.out.println();
      System.out.print("Enter pairs of x1,x2 values (<Enter> to exit): ");
      if((str=UtilFns.getUserConsoleString()) == null || str.length() <= 0)
        break;
      rBlockArr = bInterpObj.interpolateMultiString(str);
      for(idx=0; idx<rBlockArr.length; ++idx)
        System.out.println(rBlockArr[idx].getDisplayString());
    }
  }


  /**
   * Class EntryBlock holds the three double values for an entry.
   */
  private class EntryBlock
  {
    public final double x1Val;         //first 'x' value
    public final double x2Val;         //second 'x' value
    public final double yVal;          //'y' value

    /**
     * Constructs a data block.
     */
    public EntryBlock(double x1Val,double x2Val,double yVal)
    {
      this.x1Val = x1Val;
      this.x2Val = x2Val;
      this.yVal = yVal;
    }
  }


    /**
     * Class ResultBlock holds result of a bilinear interpolation
     * operation.
     */
  public class ResultBlock
  {
    /** The result value of the bilinear interpolation operation. */
    public final double result;

    /**
     * Flag set true if the given values were outside the range of
     * the data set.
     */
    public final boolean outOfRangeFlag;

    /**
     * Flag set true if an error prevents a result from being returned.
     * See the error message in 'errorMessage'.
     */
    public final boolean errorFlag;

    /** String holding an error message, or null if no error. */
    public final String errorMessage;

    /** The 'x1' and 'x2' values used to generate the result. */
    public final double x1Val,x2Val;

    /** 'x1', 'x2' and 'y' values for first of four surrounding points. */
    public final double p1x1Val,p1x2Val,p1yVal;

    /** 'x1', 'x2' and 'y' values for second of four surrounding points. */
    public final double p2x1Val,p2x2Val,p2yVal;

    /** 'x1', 'x2' and 'y' values for third of four surrounding points. */
    public final double p3x1Val,p3x2Val,p3yVal;

    /** 'x1', 'x2' and 'y' values for fourth of four surrounding points. */
    public final double p4x1Val,p4x2Val,p4yVal;


    /**
     * Constructs a result block holding the given result value, the 'x1'
     * and 'x2' values used to generate the result, and values for the
     * four points in the data grid surrounding the given point.
     */
    public ResultBlock(double val,double x1Val,double x2Val,
                                double p1x1Val,double p1x2Val,double p1yVal,
                                double p2x1Val,double p2x2Val,double p2yVal,
                                double p3x1Val,double p3x2Val,double p3yVal,
                                double p4x1Val,double p4x2Val,double p4yVal)
    {
      result = val;
      this.x1Val = x1Val;
      this.x2Val = x2Val;
      this.p1x1Val = p1x1Val;
      this.p1x2Val = p1x2Val;
      this.p1yVal = p1yVal;
      this.p2x1Val = p2x1Val;
      this.p2x2Val = p2x2Val;
      this.p2yVal = p2yVal;
      this.p3x1Val = p3x1Val;
      this.p3x2Val = p3x2Val;
      this.p3yVal = p3yVal;
      this.p4x1Val = p4x1Val;
      this.p4x2Val = p4x2Val;
      this.p4yVal = p4yVal;
      errorFlag = outOfRangeFlag = false;
      errorMessage = null;
    }

    /**
     * Constructs a result block indicating that the given values were
     * out of range of the data set.
     */
    public ResultBlock(double x1Val,double x2Val)
    {
      result = 0.0;
      this.x1Val = x1Val;
      this.x2Val = x2Val;
      p1x1Val = p1x2Val = p1yVal = p2x1Val = p2x2Val = p2yVal = p3x1Val =
                        p3x2Val = p3yVal = p4x1Val = p4x2Val = p4yVal = 0.0;
      errorFlag = outOfRangeFlag = true;
      errorMessage = "Given value(s) out of range of the data set";
    }

    /**
     * Constructs a result block indicating an error described by the
     * given error message text.
     */
    public ResultBlock(double x1Val,double x2Val,String errMsgStr)
    {
      result = 0.0;
      this.x1Val = x1Val;
      this.x2Val = x2Val;
      p1x1Val = p1x2Val = p1yVal = p2x1Val = p2x2Val = p2yVal = p3x1Val =
                        p3x2Val = p3yVal = p4x1Val = p4x2Val = p4yVal = 0.0;
      outOfRangeFlag = false;
      errorFlag = true;
      errorMessage = errMsgStr;
    }

    /**
     * Constructs a result block indicating an error described by the
     * given error message text.
     */
    public ResultBlock(String errMsgStr)
    {
      this(0.0,0.0,errMsgStr);
    }

    /**
     * Returns a display string of the contents of this block.
     */
    public String getDisplayString()
    {
      if(errorFlag)
      {       //error; show just 'x1', 'x2' and error message
        return "x1Val=" + x1Val + ", x2Val=" + x2Val + "; Error:  " +
                                                               errorMessage;
      }
      return "result=" + result + ", x1Val=" + x1Val + ", x2Val=" + x2Val +
                 UtilFns.newline + "    p1x1Val=" + p1x1Val + ", p1x2Val=" +
         p1x2Val + ", p1yVal=" + p1yVal + UtilFns.newline + "    p2x1Val=" +
                   p2x1Val + ", p2x2Val=" + p2x2Val + ", p2yVal=" + p2yVal +
                 UtilFns.newline + "    p3x1Val=" + p3x1Val + ", p3x2Val=" +
         p3x2Val + ", p3yVal=" + p3yVal + UtilFns.newline + "    p4x1Val=" +
                    p4x1Val + ", p4x2Val=" + p4x2Val + ", p4yVal=" + p4yVal;
    }
  }
}

