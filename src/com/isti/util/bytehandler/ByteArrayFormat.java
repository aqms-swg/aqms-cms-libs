package com.isti.util.bytehandler;

import java.util.*;

/**
 * <p>Title: </p>
 *
 * <p>Description: </p>
 *
 * <p>Copyright: Copyright (c) 2005</p>
 *
 * <p>Company: </p>
 *
 * @author not attributable
 * @version 1.0
 */
public class ByteArrayFormat
    extends Vector {
  public ByteArrayFormat() {
    super();
  }

  public ByteArrayFormat(String format) throws ClassCastException {
    super();
    this.setFormat(format);
  }

  public ByteArrayFormat(int initialCapacity) {
    super(initialCapacity);
  }

  public ByteArrayFormat(int initialCapacity, int capacityIncrement) {
    super(initialCapacity, capacityIncrement);
  }

  public ByteArrayFormat(Collection c) {
    super(c);
  }

  /**
   * parse the string into a real format.
   * @param format String
   * @throws ClassCastException
   */
  public void setFormat(String format) throws ClassCastException {

    String s;
//    char c;
//    char len;

    FormatElement fe;

    for (int i = 0; i < format.length(); i++) {

      s = format.substring(i, i + 1);
      if (s.equals("s")) {
        // this is a string, get the lengt
        StringBuffer sb = new StringBuffer("s");
        for (int ii = i+1; ii < format.length(); ii++) {
          String len = format.substring(ii, ii+1);
          if (Character.isDigit(len.charAt(0))) {
            sb.append(len);
          } else {
            break;
          }
          i++;
        }
        //        stringLen = Integer.parseInt(sb.toString());
        s = sb.toString();
      }

      fe = new FormatElement(s);
      add(fe);

    }
  }

  public String toString() {
    StringBuffer sb = new StringBuffer();
    Iterator it = this.iterator();
    while (it.hasNext()) {
      sb.append(((FormatElement)(it.next())).toString());
    }
    return sb.toString();
  }

  public int getStringLen(int position) {

    return ((FormatElement)(get(position))).getStringLen();

  }

  public char charAt(int position) {

    return ((FormatElement)(get(position))).getType();

  }

  public static void main(String[] args) {
    new ByteArrayFormat();
  }
}
