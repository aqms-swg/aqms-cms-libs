package com.isti.util.bytehandler;

import java.io.*;
import java.math.*;
import java.util.*;

import com.isti.util.*;



/**
 * <p>Title: </p>
 *
 * <p>Description: </p>
 *
 * <p>Copyright: Copyright (c) 1999</p>
 *
 * <p>Company: ISTI</p>
 *
 * @author not attributable
 * @version 1.0
 *
   * This holds all the objects that need to go in and out of the ByteArray.
   Format definitions.
   <ul>
   <li>   x - pad byte  	no value (1 byte)
   <li>   c - char 	string of length 1 (1 byte)
   <li>   b - signed  char integer (1 byte)
   <li>   h - short 	integer (2 bytes)
   <li>   i - int 	integer (4 bytes)
   <li>   l - long 	integer (8 bytes)
   <li>   f - float 	float (4 bytes)
   <li>   d - double 	float (8 bytes)
   <li>   s - char[] 	string
   </ul>
   *
   * all bytes are assumed to be network bytte order.
   */


public class ByteHandler {

  DataOutputStream dos = null;
  DataInputStream dis = null;
  DataInputStream dis_forreader = null;
  ByteArrayOutputStream baos = null;
  ByteArrayInputStream bais = null;

  OutputStreamWriter osw = null;
  InputStreamReader isr = null;

  //  ByteArrayOutputStream baos = new ByteArrayOutputStream();
//     DataOutputStream dos = new DataOutputStream(baos);

//  String format;
  ByteArrayFormat byteformat;


  HashMap objects = null;




  public ByteHandler() {
    super();
    objects = new HashMap();

    byteformat = new ByteArrayFormat();

//    baos = new ByteArrayOutputStream();
//    dos = new DataOutputStream(baos);
//
//    try {
//      osw = new OutputStreamWriter(dos, "US-ASCII");
//    }
//    catch (UnsupportedEncodingException ex) {
//      ex.printStackTrace();
//      System.exit(-1);
//    }


  }


  public boolean parseByteAr(byte[] ba) throws Exception {


    bais = new ByteArrayInputStream(ba);
    dis = new DataInputStream(bais);

    ByteArrayInputStream tbais = new ByteArrayInputStream(ba);
    this.dis_forreader = new DataInputStream(tbais);

    isr = new InputStreamReader(dis_forreader,"US-ASCII");

    for (int i = 0; i< byteformat.size(); i++) {
      if (!parseValue(i)) {
        return false;
      }
    }

    return true;

  }


  public boolean parseValue(int position) throws Exception {


    char type = byteformat.charAt(position);

    Object ob = null;

    Class ctype = getClassType(type);

    if (pad.class.equals(ctype)) {
      return true;
    }

    //    System.out.println("position " + position + ": class type " + ctype.getName());

    char[] ca = null;
    byte[] ba = null;

    if (ctype.getName().equals(Integer.class.getName())) {
      ob = Integer.valueOf(dis.readInt());
      // tmp is an instance of the type it is supposed to be
      objects.put(Integer.valueOf(position), ob);

      ca = new char[4];
      isr.read(ca,0,4);


    }
    if (ctype.getName().equals(Short.class.getName())) {
      ob = Short.valueOf(dis.readShort());
      // tmp is an instance of the type it is supposed to be
      objects.put(Integer.valueOf(position), ob);

      int size = 2;
      ca = new char[size];
      isr.read(ca,0,size);

    }
    if (ctype.getName().equals(Long.class.getName())) {
      ob = Long.valueOf(dis.readLong());
      // tmp is an instance of the type it is supposed to be
      objects.put(Integer.valueOf(position), ob);

      int size = 8;
      ca = new char[size];
      isr.read(ca,0,size);

    }
    if (ctype.getName().equals(Byte.class.getName())) {
      ob = Byte.valueOf(dis.readByte());
      // tmp is an instance of the type it is supposed to be
      objects.put(Integer.valueOf(position), ob);

      int size = 1;
      ca = new char[size];
      isr.read(ca,0,size);

    }
    if (ctype.getName().equals(Double.class.getName())) {
      ob = Double.valueOf(dis.readDouble());
      // tmp is an instance of the type it is supposed to be
      objects.put(Integer.valueOf(position), ob);

      int size = 8;
      ca = new char[size];
      isr.read(ca,0,size);

    }
    if (ctype.getName().equals(Float.class.getName())) {
      ob = Float.valueOf(dis.readFloat());
      // tmp is an instance of the type it is supposed to be
      objects.put(Integer.valueOf(position), ob);

      int size = 4;
      ca = new char[size];
      isr.read(ca,0,size);


    }
    if (ctype.getName().equals(Character.class.getName())) {

      int count = 1;

      ca = new char[count];

      isr.read(ca,0,count);

      // just move ahead.
      dis.readByte();

      Character ch = Character.valueOf(ca[0]);

      //      objects.put(Integer.valueOf(position), s);

      // tmp is an instance of the type it is supposed to be
      objects.put(Integer.valueOf(position), ch);
    }
    if (ctype.getName().equals(Boolean.class.getName())) {
      ob = Boolean.valueOf(dis.readBoolean());
      // tmp is an instance of the type it is supposed to be
      objects.put(Integer.valueOf(position), ob);

      int size = 1;
      ca = new char[size];
      isr.read(ca,0,size);

    }
    if (ctype.getName().equals(String.class.getName())) {

      int len = byteformat.getStringLen(position);
      //      System.out.println("len is " + len);

      ob = Byte.valueOf(dis.readByte());

      int size = 1;
      ca = new char[size];
      isr.read(ca,0,size);


      int count = ((Byte)ob).intValue();

      ca = new char[count];
      ba = new byte[count];

      isr.read(ca,0,count);

      dis.read(ba,0,count);

      String s = new String(ca);

      objects.put(Integer.valueOf(position), s);

      int needToRead = (len) - count;

      if (needToRead > 0) {

        //        System.out.println("read " + count +", size was " + len + ", need to read is " + needToRead);

        ca = new char[needToRead];
        ba = new byte[needToRead];

        isr.read(ca,0,needToRead);

        dis.read(ba,0,needToRead);
      }
    }

    return true;
  }

  /**
   * put the tmp object into the vector at the desired location.
   * returns true if it is allowed.
   * @param tmp Object
   * @param position int
   * @return boolean
   */
  public boolean setValue(int position, Object tmp) throws ClassCastException {

    char type = byteformat.charAt(position);

    Class ctype = getClassType(type);

    if (pad.class.equals(ctype)) {
      if (tmp != null) {
        LogFile.getGlobalLogObj().warning("trying to set the value of the padded field.");
      }
      return true;
    }

    if (ctype.isInstance(tmp)) {
      if (type == 's') {
        int len = byteformat.getStringLen(position);
        int slen =((String)(tmp)).length() ;
        if (len < slen) {
          String ts = ((String)(tmp)).substring(0,len);
          tmp = ts;
        }
      }
      // tmp is an instance of the type it is supposed to be
      objects.put(Integer.valueOf(position), tmp);
    } else {
      // try to cast if to the desire type.

      throw new ClassCastException("object is not of type " + ctype.getName() + ". object was " + tmp);
    }

    return true;

  }

  public boolean initVector() throws ClassCastException {

    for (int i = 0; i < byteformat.size(); i++ ) {
      initValue(i);
    }

    return true;

  }

  /**
   * put the tmp object into the vector at the desired location.
   * returns true if it is allowed.
   * @param tmp Object
   * @param position int
   * @return boolean
   */
  public boolean initValue(int position) throws ClassCastException {

    char type = byteformat.charAt(position);

    Class ctype = getClassType(type);

    Object obj = getNewClassObject(type);

    if (ctype.isInstance(obj)) {
      // tmp is an instance of the type it is supposed to be
      objects.put(Integer.valueOf(position), obj);
    } else {
      throw new ClassCastException("object is not of type " + ctype.getName() + ". object was " + obj);
    }

    return true;

  }



  public Object getValue(int position) throws ClassCastException {

    Object obj = objects.get(Integer.valueOf(position));

    Class ctype = getClassType(byteformat.charAt(position));


    if (ctype.isInstance(obj)) {

      return obj;

    }

    throw new ClassCastException("object in position " + position + " is not of type " + ctype.getName());

  }

  private char getType(int position) {
    //    System.err.println(">" + byteformat + "<");
    char type = byteformat.charAt(position);
    return type;
  }

  /**
 * return the class type for type defined by the char "type"
 * @param type char
 * @return Class
 */
public Class getClassType(char type) throws ClassCastException {

  switch (type) {
    case 'i':
      return Integer.class;
    case 'c':
      return Character.class;
    case 'b':
      return Byte.class;
    case 'h':
      return Short.class;
    case 'l':
      return Long.class;
    case 'f':
      return Float.class;
    case 'd':
      return Double.class;
    case 's':
      return String.class;
    case 'x':
      // just padding.
      return new pad().getClass();
    default:
      throw new ClassCastException("type " + type + " is not a valid classtype");
  }
}


  /**
   * return the class type for type defined by the char "type"
   * @param type char
   * @return Class
   */
  private Object getNewClassObject(char type) throws ClassCastException {

    switch (type) {
      case 'i':
        return Integer.valueOf(0);
      case 'c':
        return Character.valueOf(' ');
      case 'b':
        byte b = 0;
        return Byte.valueOf(b);
      case 'h':
        short s = 0;
        return Short.valueOf(s);
      case 'l':
        return Long.valueOf(0);
      case 'f':
        return Float.valueOf(0f);
      case 'd':
        return Double.valueOf(0.0);
      case 's':
        return new String("");
      case 'x':
        // just padding.
        return new pad().getClass();
      default:
        throw new ClassCastException("type " + type + " is not a valid classtype");
    }
  }

  /**
   * returns the number of bytes that we will end up having in the byte array
   */
  public int getSize() throws ClassCastException {

    int total = 0;

//    System.out.println("number of object here is " + objects.size());
    for (int i= 0; i < objects.size(); i++) {
      total += getObjectSize(i);
    }

    return total;
  }

  private int getObjectSize(int position) throws ClassCastException {
    Object obj = objects.get(Integer.valueOf(position));
    char type = this.getType(position);

    //if string, return size of string
    if (type == 's') {
      String s = (String)obj;
      return s.length();
    }

    return getObjectSize(type);

  }

  private int getObjectSize(char type) throws ClassCastException {
    switch (type) {
    case 'i':
      return 4;
    case 'c':
      return 1;
    case 'b':
      return 1;
    case 'h':
      return 2;
    case 'l':
      return 8;
    case 'f':
      return 4;
    case 'd':
      return 8;
    case 'x':
      // just padding.
      return 1;
    default:
      throw new ClassCastException("type " + type + " size is unknown.");
    }

  }

  /*
   * default padding class.
   */
  class pad {
    pad() {

    }
  }

  /**
   * changing the format forces a clearing of the vector of items.
   * @param format String
   */
  public void setFormat(String sformat) throws ClassCastException {
    this.byteformat = new ByteArrayFormat(sformat);
    this.initVector();
  }



  public String toString() {


    StringBuffer sb = new StringBuffer();

    //  String ts = null;
    //
    //  byte[] tb = null;
    Object ob = null;
    //
    try {
      for (int i = 0; i < objects.size(); i++) {

        if (i != 0)
          sb.append("|");

        ob = getValue(i);
        if (ob instanceof Integer) {
          sb.append(((Integer)ob).intValue());
        }
        if (ob instanceof Long) {
          sb.append(((Long)ob).longValue());
        }
        if (ob instanceof Short) {
          sb.append(((Short)ob).shortValue());
        }
        if (ob instanceof Double) {
          sb.append(((Double)ob).doubleValue());
        }
        if (ob instanceof Float) {
          sb.append(((Float)ob).floatValue());
        }
        if (ob instanceof Character) {
          sb.append(((Character)ob).charValue());
        }
        if (ob instanceof Boolean) {
          sb.append(((Boolean)ob).booleanValue());
        }
        if (ob instanceof Byte) {
          sb.append(((Byte)ob).byteValue());
        }
        if (ob instanceof String) {
          sb.append("(");
          sb.append(((String)ob).length());
          sb.append(")");
          sb.append(((String)ob));
        }

      }
    }
    catch (ClassCastException ex) {
      ex.printStackTrace();
    }


    //    printBytes(nb);


    return sb.toString();
  }

  public byte[] toByteArray() throws Exception {

    baos = new ByteArrayOutputStream();
    dos = new DataOutputStream(baos);

    try {
      osw = new OutputStreamWriter(dos, "US-ASCII");
    }
    catch (UnsupportedEncodingException ex) {
      ex.printStackTrace();
      System.exit(-1);
    }

    Object ob = null;

    //    System.out.println("numb of objects is " + objects.size());

    try {
      for (int i = 0; i < objects.size(); i++) {
        ob = getValue(i);
        //        System.out.println("object type is " + ob.getClass().getName());
        if (ob instanceof Integer) {
          dos.writeInt(((Integer)ob).intValue());
          dos.flush();
          //          System.out.println("this is test code to convert from an int to a byte array and back to an int");
          //          byte[] b = int2byte(((Integer)ob).intValue());
          //          int tint = byte2int(b);
        }
        if (ob instanceof Long) {
          dos.writeLong(((Long)ob).longValue());
          dos.flush();
        }
        if (ob instanceof Short) {
          dos.writeShort(((Short)ob).shortValue());
          dos.flush();
        }
        if (ob instanceof Double) {
          dos.writeDouble(((Double)ob).doubleValue());
          dos.flush();
        }
        if (ob instanceof Float) {
          dos.writeFloat(((Float)ob).floatValue());
          dos.flush();
        }
        if (ob instanceof Character) {
          //          dos.writeChar(((Character)ob).charValue());
          Character ch = (Character)ob;
          String s = new String(ch.toString());
          osw.write(s,0,1);
          osw.flush();
        }
        if (ob instanceof Boolean) {
          dos.writeBoolean(((Boolean)ob).booleanValue());
          dos.flush();
        }
        if (ob instanceof Byte) {
          dos.writeByte(((Byte)ob).byteValue());
          dos.flush();
        }
        if (ob instanceof String) {
          dos.writeByte(((String)ob).length());
          dos.flush();
          osw.write((String)ob,0,((String)ob).length());
          osw.flush();
          //          dos.writeChars(((String)ob));
        }


      }
    }
    catch (ClassCastException ex) {
    }

    byte nb[] = baos.toByteArray();

    //    printBytes(nb);

    //    System.out.println("byte array size is " + nb.length);


    return nb;

  }

  public static short byte2short(byte[] bytes) throws ClassCastException {

     short sumInt = 0;
     int shiftCount = bytes.length-1;

     if (bytes.length > 2) {
       throw new ClassCastException("byte array has " + bytes.length + " bytes, too long for a short");
     }

     for (int i = bytes.length -1; i >= 0; i--, shiftCount--) {
       int tmpInt = bytes[i] & 0xff;
       tmpInt <<= (8*shiftCount);
       sumInt += tmpInt;
     }
     return sumInt;
   }

  public int byte2int(byte[] bytes) {

    int sumInt = 0;
    int shiftCount = bytes.length-1;


    for (int i = 0; i < bytes.length; i++, shiftCount--) {
      int tmpInt = bytes[i] & 0xff;
      tmpInt <<= (8*shiftCount);
      sumInt += tmpInt;
    }
    return sumInt;
  }

  public long byte2long(byte[] bytes) {

    long sumInt = 0;
    int shiftCount = bytes.length-1;


    for (int i = 0; i < bytes.length; i++, shiftCount--) {
      long tmpInt = bytes[i] & 0xff;
      tmpInt <<= (8*shiftCount);
      sumInt += tmpInt;
    }
    return sumInt;
  }

  /**
   * should always return a 2 element byte array
   * @param myint short
   * @return byte[]
   */
  public byte[] short2byte(short myshort) {

    int tint = myshort;

    return int2byte(tint);

  }

  /**
   * should always return a 4 element byte array
   * @param myint int
   * @return byte[]
   */
  public byte[] int2byte(int myint) {

   byte[] tmpByte = null;

   BigInteger bi = new BigInteger(Integer.toString(myint));

   tmpByte = bi.toByteArray();

   if (tmpByte.length != 4) {

     byte[] newByte = new byte[4];

     for (int i = 0; i < 4; i++) {
       newByte[i] = 0;
     }

     int diff = newByte.length - tmpByte.length;

     for (int i = 0; i < tmpByte.length; i++) {
       byte tb = tmpByte[i];
       int pos = diff + i;
       newByte[pos] = tb;
     }

     tmpByte = newByte;

   }

   return tmpByte;

 }
 /**
    * should always return a 4 element byte array
    * @param myint int
    * @return byte[]
    */
   public byte[] long2byte(long mylong) {

    byte[] tmpByte = null;

    BigInteger bi = new BigInteger(Long.toString(mylong));

    tmpByte = bi.toByteArray();

    if (tmpByte.length != 8) {

      byte[] newByte = new byte[8];

      for (int i = 0; i < 8; i++) {
        newByte[i] = 0;
      }

      int diff = newByte.length - tmpByte.length;

      for (int i = 0; i < tmpByte.length; i++) {
        byte tb = tmpByte[i];
        int pos = diff + i;
        newByte[pos] = tb;
      }

      tmpByte = newByte;

    }

    return tmpByte;

  }

  public byte[] double2byte(double mylong) {

    long l = Double.doubleToLongBits(mylong);
    byte b[] = new byte[8];
    int shift=64-8;
    for (int i=0; i<8; i++,shift-=8)
    { b[i] = (byte) (l >>> shift); }


    try {
      dos.writeDouble(mylong);
    }
    catch (IOException ex) {
      ex.printStackTrace();
    }

    byte nb[] = baos.toByteArray();

    printBytes(b);
    printBytes(nb);

    return nb;

  }


  private void printBytes(byte[] ar) {
    for (int i = 0; i < ar.length; i++) {
      System.out.println(ar[i] & 0xff);
    }
  }

  public void printBytes() throws Exception {
    try {
      byte[] ar = this.toByteArray();
      for (int i = 0; i < ar.length; i++) {
        System.out.println(ar[i] & 0xff);
      }
    }
    catch (IOException ex) {
      ex.printStackTrace();
      return;
    }
  }

  public void compareBytes(ByteHandler bh2) throws Exception{
    byte[] ar = null;
    byte[] ar2 = null;
    try {
      ar = this.toByteArray();
      ar2 = bh2.toByteArray();
    }
    catch (IOException ex) {
      return;
    }

    byte[] maxar = null;

    int s1 = ar.length;
    int s2 = ar2.length;
    int min = 0;
    int max = 0;
    if (s1 > s2) {
      min = s2;
      max = s1;
      maxar = ar;
    }
    else  {
      min = s1;
      max = s2;
      maxar = ar2;
    }
    int i = 0;

    System.out.println("\n\n\n");

    for (; i < min; i++) {
      System.out.print(ar[i] & 0xff);
      System.out.println("\t" + (ar2[i] & 0xff));
    }

    for (; i < max; i++) {
      if (max == s1)
        System.out.println(maxar[i]  & 0xff);
      else
        System.out.println("\t" + (maxar[i]  & 0xff));
    }

  }



/*
     <li>   x - pad byte  	no value (1 byte)
   <li>   c - char 	string of length 1 (1 byte)
   <li>   b - signed  char integer (1 byte)
   <li>   h - short 	integer (2 bytes)
   <li>   i - int 	integer (4 bytes)
   <li>   l - long 	integer (8 bytes)
   <li>   f - float 	float (4 bytes)
   <li>   d - double 	float (8 bytes)
   <li>   s - char[] 	string
*/
  public static void main(String[] args) {

    byte[] sidba = new byte[2];
    sidba[0] = 3;
    sidba[1] = 0;

    short stest = (short) 0;
    try {
      stest = ByteHandler.byte2short(sidba);
    }
    catch (ClassCastException ex3) {
      ex3.printStackTrace();
    }
    System.out.println("stest is " + stest);



    ByteHandler bh = new ByteHandler();

    //    bh.testOutput();

    try {

//      bh.testit("ssss");
      bh.setFormat("s100s5hccs3cs10bilfds110hcbilfd");
//      bh.setFormat("shcbilfdshcbilfd");

      int count = 0;
      byte b;
      b = 8;
      //      short s = 260;

      bh.setValue(count++, new String("ABC"));
      bh.setValue(count++, new String("abc"));
      bh.setValue(count++, Short.valueOf((short)260));
      bh.setValue(count++, Character.valueOf('D'));
      bh.setValue(count++, Character.valueOf('d'));
      bh.setValue(count++, new String("EFG"));
      bh.setValue(count++, Character.valueOf('H'));
      bh.setValue(count++, new String("IJ"));
      bh.setValue(count++, Byte.valueOf(b));
      bh.setValue(count++, Integer.valueOf(8000));
      bh.setValue(count++, Long.valueOf(9999999));
      bh.setValue(count++, Float.valueOf(1.1f));
      bh.setValue(count++, Double.valueOf(11111.11111));

      b = 9;
      bh.setValue(count++, new String("ABC, again."));
      bh.setValue(count++, Short.valueOf((short)261));
      bh.setValue(count++, Character.valueOf('B'));
      bh.setValue(count++, Byte.valueOf(b));
      bh.setValue(count++, Integer.valueOf(8001));
      bh.setValue(count++, Long.valueOf(10000000));
      bh.setValue(count++, Float.valueOf(1.2f));
      bh.setValue(count++, Double.valueOf(11111.11112));


      System.out.println("bh size is " + bh.getSize());

      byte[] ba = null;

      try {

        ba = bh.toByteArray();
      }
      catch (Exception ex1) {
        ex1.printStackTrace();
      }

      ByteHandler bhn = new ByteHandler();

//      bhn.setFormat("cbhilfdscbhilfds");
//      bhn.setFormat("shcbilfdshcbilfd");
//      bhn.setFormat("ssccscs");
      bhn.setFormat("s100s5hccs3cs10bilfds110hcbilfd");

      try {
        bhn.parseByteAr(ba);
      }
      catch (Exception ex2) {
        ex2.printStackTrace();
      }

      System.out.println("bhn size is " + bhn.getSize());

      System.out.println(bh.toString());
      System.out.println(bhn.toString() + "\n\n\n");

      //      bh.printBytes();
      //      bhn.printBytes();



      bh.compareBytes(bhn);

    }
    catch (Exception ex) {
      ex.printStackTrace();
    }

  }


}
