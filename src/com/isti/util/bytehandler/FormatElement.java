package com.isti.util.bytehandler;

/**
 * <p>Title: </p>
 *
 * <p>Description: </p>
 *
 * <p>Copyright: Copyright (c) 2005</p>
 *
 * <p>Company: </p>
 *
 * @author not attributable
 * @version 1.0
 */
public class FormatElement {

  char type;
  int stringLen = 0;

  public FormatElement() {
    super();
  }

  /**
   *     turn a string into a format elemetn, if it is of the correct type
   *  <li>   x - pad byte  	no value (1 byte)
    <li>   c - char 	string of length 1 (1 byte)
    <li>   b - signed  char integer (1 byte)
    <li>   h - short 	integer (2 bytes)
    <li>   i - int 	integer (4 bytes)
    <li>   l - long 	integer (8 bytes)
    <li>   f - float 	float (4 bytes)
    <li>   d - double 	float (8 bytes)
    <li>   s - char[] 	string
   * @param s String
   */
  public FormatElement(String s) {
    super();
    this.setType(s);
  }

  public static void main(String[] args) {
    FormatElement fe = new FormatElement();
    fe.setType("i");
    System.out.println(fe.toString());
    fe.setType("s3");
    System.out.println(fe.toString());
  }

  public int getStringLen() {
    return stringLen;
  }

  public char getType() {
    return type;
  }

  /**
   *     turn a string into a format elemetn, if it is of the correct type
   *  <li>   x - pad byte  	no value (1 byte)
    <li>   c - char 	string of length 1 (1 byte)
    <li>   b - signed  char integer (1 byte)
    <li>   h - short 	integer (2 bytes)
    <li>   i - int 	integer (4 bytes)
    <li>   l - long 	integer (8 bytes)
    <li>   f - float 	float (4 bytes)
    <li>   d - double 	float (8 bytes)
    <li>   s - char[] 	string

   * @param val String
   */
  public void setType(String val) {

    this.type = val.charAt(0);

    if (type == 's') {

      StringBuffer sb = new StringBuffer();
      for (int i = 1; i < val.length(); i++) {
        String len = val.substring(i, i+1);
        sb.append(len);
      }
      stringLen = Integer.parseInt(sb.toString());
    }
  }

  public String toString() {
    String ret;
    if (type == 's') {
      ret = type + Integer.toString(stringLen);
    } else {
      ret = String.valueOf(type);
    }
    return ret;
  }
}
