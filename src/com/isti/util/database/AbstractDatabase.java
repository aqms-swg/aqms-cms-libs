//AbstractDatabase.java:  Implements a abstract database connection.
//
//   8/11/2004 -- [KF]  Initial version, previously 'BasicDatabase'.
//  10/15/2004 -- [ET]  Added 'closeDatabaseConnection()' method.
//

package com.isti.util.database;

import java.sql.*;
import java.text.*;
import java.util.*;

public abstract class AbstractDatabase
{
  protected final static String EMPTY_TEXT = DatabaseUtil.EMPTY_TEXT;

  private final static String datePattern = "dd-MMM-yyyy";
  private final static String shortDatePattern = "dd-MM-yyyy";
  private final static String timePattern = "HH:mm:ss";

  private final ConnectionJDBC dbConn;  //database connection
  private QueryJDBC databaseQuery = null;  //database query

  //date format object for creating date strings:
  private SimpleDateFormat dateFormatterObj =
      new SimpleDateFormat(datePattern);

  //short date format object for creating date strings:
  private SimpleDateFormat shortDateFormatterObj =
      new SimpleDateFormat(shortDatePattern);

  //date format object for creating time strings:
  private SimpleDateFormat timeFormatterObj =
      new SimpleDateFormat(timePattern);

  /**
   * Creates a connection to a basic database.
   * @param dbConn database connection
   *
   */
  public AbstractDatabase(ConnectionJDBC dbConn)
  {
    this.dbConn = dbConn;
  }

  /**
   * Gets the database connection
   * @return the database connection
   */
  public ConnectionJDBC getDatabaseConnection()
  {
    return dbConn;
  }

  /**
   * Gets the database query
   * @return the database query
   */
  public synchronized QueryJDBC getDatabaseQuery()
  {
    return databaseQuery;
  }

  /**
   * Save the database query.
   * @param dbQuery database query
   */
  public synchronized void setDatabaseQuery(QueryJDBC dbQuery)
  {
    databaseQuery = dbQuery;
  }

  /**
   * Gets the table name.
   * @return the table name.
   */
  public abstract String getTableName();

  /**
   * Gets the table prefix
   * @return the table prefix
   */
  public abstract String getTablePrefix();

  /**
   * Gets the column names to use in query
   * @return the column names to use in query or empty for all
   */
  public abstract String[] getColumnNames();

  /**
   * Gets the number of columns to use for ordering
   * @return the number of columns to use for ordering
   */
  public abstract int getColumnOrderNumcolumns();

  /**
   * Gets the column prefix
   * @return the column prefix
   */
  public String getColumnPrefix()
  {
    String text = EMPTY_TEXT;
    if (getTablePrefix().length() > 0)
    {
      text = getTablePrefix() + ".";
    }
    return text;
  }

  /**
   * Gets the date formatter.
   * @return the date formatter object.
   */
  public synchronized SimpleDateFormat getDateFormatter()
  {
    return getDateFormatter(false);
  }

  /**
   * Gets the date formatter.
   * @param shortFlag true for short date format
   * @return the date formatter object.
   */
  public synchronized SimpleDateFormat getDateFormatter(boolean shortFlag)
  {
    if (shortFlag)  //if short date format
    {
      return shortDateFormatterObj;
    }
    return dateFormatterObj;
  }

  /**
   * Gets the time formatter.
   * @return the time formatter object.
   */
  public synchronized SimpleDateFormat getTimeFormatter()
  {
    return timeFormatterObj;
  }

  /**
   * Sets the date formatter.
   * @param dateFormatterObj the date formatter object.
   */
  public synchronized void setDateFormatter(SimpleDateFormat dateFormatterObj)
  {
    this.dateFormatterObj = dateFormatterObj;
  }

  /**
   * Sets the time formatter.
   * @param timeFormatterObj the time formatter object.
   */
  public synchronized void setTimeFormatter(SimpleDateFormat timeFormatterObj)
  {
    this.timeFormatterObj = timeFormatterObj;
  }

  /**
   * Gets the select clause
   * @param text select text
   * @return the select clause
   */
  public abstract String getSelectClause(String text);

  /**
   * Gets the from clause
   * @param text from text
   * @return the from clause
   */
  public abstract String getFromClause(String text);

  /**
   * Gets the order by clause
   * @param text order by text
   * @return the order by clause
   */
  public abstract String getOrderByClause(String text);

  /**
   * Executes an SQL statement that returns a single <code>ResultSet</code> object.
   * @param sql typically this is a static SQL <code>SELECT</code> statement
   *
   * @exception ClassNotFoundException if the driver's class could not be found.
   * @exception InstantiationException if the driver could not be instantiated.
   * @exception IllegalAccessException if the driver's class or initializer is
   * not accessible.
   * @exception SQLException if a database access error occurs.
   */
  public void executeQuery(String sql)
      throws ClassNotFoundException, InstantiationException,
  IllegalAccessException, SQLException
  {
    setDatabaseQuery(dbConn.executeQuery(sql));
  }

  /**
   * Gets a list of the column objects.
   * The list contains Map objects with the column name for the key.
   *
   * @return list of column objects.
   */
  public List getColumnObjects()
  {
    QueryJDBC dbQuery = getDatabaseQuery();  //get the database query
    List l = null;
    if (dbQuery != null)  //if query exists
    {
      l = dbQuery.getColumnObjects();
    }
    return l;
  }

  /**
   * Gets the data map for the specified index.
   * @param index database index
   * @return data map
   */
  public Map getMap(int index)
  {
    Map map = (Map)getColumnObjects().get(index);
    return map;
  }

  /**
   * Closes the database connection
   */
  public void closeDatabaseConnection()
  {
    if(dbConn != null)
      dbConn.close();
  }

  /**
   * Gets a string representation of a database element.
   * @param index database element index
   * @return a string representation of a database element.
   */
  public abstract String getString(int index);

  /**
   * Gets a title string representation of a database element.
   * @param index database element index
   * @return a title string representation of a database element.
   */
  public abstract String getTitle(int index);
}