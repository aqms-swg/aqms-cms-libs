//BasicDatabase.java:  Implements a basic database connection.
//
//   1/28/2003 -- [KF]  Initial version.
//   8/11/2004 -- [KF]  Moved base logic to new 'AbstractDatabase' class and
//                      made this class no longer abstract.
//

package com.isti.util.database;

import com.isti.util.UtilFns;

public class BasicDatabase extends AbstractDatabase
{
  /**
   * The column names to use in query for all columns.
   */
  public final static String[] EMPTY_COLUMN_NAMES = new String[0];

  /**
   * Creates a connection to a basic database.
   * @param dbConn database connection
   *
   */
  public BasicDatabase(ConnectionJDBC dbConn)
  {
    super(dbConn);
  }

  /**
   * Gets the table name.
   * It is recommended that all subclasses override this method.
   * @return the table name.
   */
  public String getTableName()
  {
    return "table";
  }

  /**
   * Gets the table prefix
   * Subclasses that need a table prefix should override this method.
   * @return the table prefix
   */
  public String getTablePrefix()
  {
    return UtilFns.EMPTY_STRING;  //no table prefix
  }

  /**
   * Gets the column names to use in query
   * Subclasses that need a subset of columns should override this method.
   * @return the column names to use in query or empty for all
   * @see getColumnOrderNames, getColumnOrderNumcolumns
   */
  public String[] getColumnNames()
  {
    return EMPTY_COLUMN_NAMES;  //all columns
  }

  /**
   * Gets the number of columns to use for ordering
   * Subclasses that want to use a subset of the order columns should override
   * this method.
   * @return the number of columns to use for ordering
   * @see getColumnNames, getColumnOrderNames
   */
  public int getColumnOrderNumcolumns()
  {
    return 0;  //no ordering
  }

  /**
   * Gets the select clause
   * @param text select text
   * @return the select clause
   */
  public String getSelectClause(String text)
  {
    return DatabaseUtil.getSelectClause(text, getColumnNames());
  }

  /**
   * Gets the from clause
   * @param text from text
   * @return the from clause
   */
  public String getFromClause(String text)
  {
    return DatabaseUtil.getFromClause(text, getTableName(), getTablePrefix());
  }

  /**
   * Gets the order by clause
   * @param text order by text
   * @return the order by clause
   */
  public String getOrderByClause(String text)
  {
    return DatabaseUtil.getOrderByClause(
        text, getColumnNames(), getColumnOrderNumcolumns());
  }

  /**
   * Gets a string representation of a database element.
   * @param index database element index
   * @return a string representation of a database element.
   */
  public String getString(int index)
  {
    return getColumnObjects().get(index).toString();
  }

  /**
   * Gets a title string representation of a database element.
   * @param index database element index
   * @return a title string representation of a database element.
   */
  public String getTitle(int index)
  {
    return getString(index);
  }
}