//ConnectionJDBC.java:  Implements a JDBC connection.

package com.isti.util.database;

import java.sql.*;
import java.util.*;
import com.isti.util.gui.IstiDialogPopup;
import java.awt.Component;
import com.isti.util.gui.IstiPasswordPanel;
import com.isti.util.CallBackCompletion;
import com.isti.util.IstiThread;
import com.isti.util.UtilFns;

public class ConnectionJDBC
{
  /** The dialog option string. */
  public static String DIALOG_OPTION_STR = "OK";
  /** The default driver name. */
  protected final static String DEFAULT_DRIVER_NAME =
      "sun.jdbc.odbc.JdbcOdbcDriver";
  /** The default protocol. */
  public final static String DEFAULT_PROTOCOL = "jdbc";
  /** The default subprotocol. */
  public final static String DEFAULT_SUBPROTOCOL = "odbc";
  /** The default subname. */
  public final static String DEFAULT_SUBNAME = null;
  /** The user key. */
  public final static String INFO_USER_KEY = "user";
  /** The password key. */
  public final static String INFO_PASSWORD_KEY = "password";

  //callback information
  private final CallBackCompletion callBack;

  private Connection databaseConnection = null;  //database connection
  private Statement databaseStatement = null;    //database statement

  //connection information
  private final String url;
  private Properties dbInfo;

  //execute lock
  private final Object executeLock = new Object();

  /**
   * Saves the username and password in the info properties.
   * @param info current info or null if none
   * @param userName user name
   * @param password password for the above user name
   * @return info properties
   */
  public static Properties saveUserPassword(
      Properties info, String userName, String password)
  {
    if (info == null)  //if no current info
    {
      info = new Properties();  //create new info
    }
    else
    {
      info = (Properties)info.clone();  //copy the current info
    }

    //if username provided set the user property
    if (userName != null)
    {
      info.setProperty(INFO_USER_KEY, userName);
    }

    //if password provided set the password property
    if (password != null)
    {
      info.setProperty(INFO_PASSWORD_KEY, password);
    }

    return info;  //save the new info
  }

  /**
   * Gets a database url of the form jdbc:subprotocol:subname
   * @param subProtocol sub-protocol
   * @param subName sub-name of the protocol
   *
   * @return database url
   */
  public static String getURL(String subProtocol, String subName)
  {
    return DEFAULT_PROTOCOL+":"+subProtocol+":"+subName;
  }

  /**
   * Gets a database url.
   * @param subProtocol sub-protocol
   * @param subName sub-name of the protocol
   * @param hostName host name
   * @param port TCP/IP listener port
   * @param sid database SID (system identifier)
   *
   * @return database url
   */
  public static String getURL(String subProtocol, String subName,
                                 String hostName, int port, String sid)
  {
    return getURL(subProtocol, subName)+":@"+hostName+":"+port+":"+sid;
  }

  /**
   * Registers the driver with the <code>DriverManager</code>.
   * A newly-loaded driver class should call
   * the method <code>registerDriver</code> to make itself
   * known to the <code>DriverManager</code>.
   *
   * @exception ClassNotFoundException if the driver's class could not be found.
   * @exception InstantiationException if the driver could not be instantiated.
   * @exception IllegalAccessException if the driver's class or initializer is
   * not accessible.
   * @exception SQLException if a database access error occurs.
   */
  public static void registerDriver()
      throws ClassNotFoundException, InstantiationException,
  IllegalAccessException, SQLException
  {
    // Load the driver
    registerDriver(DEFAULT_DRIVER_NAME);
  }

  /**
   * Drops the driver from the <code>DriverManager</code>'s list.
   * Applets can only deregister Drivers from their own classloaders.
   *
   * @exception ClassNotFoundException if the driver's class could not be found.
   * @exception InstantiationException if the driver could not be instantiated.
   * @exception IllegalAccessException if the driver's class or initializer is
   * not accessible.
   * @exception SQLException if a database access error occurs.
   */
  public static void deregisterDriver()
      throws ClassNotFoundException, InstantiationException,
  IllegalAccessException, SQLException
  {
    // Unload the driver
    deregisterDriver(DEFAULT_DRIVER_NAME);
  }

  /**
   * Registers the given driver with the <code>DriverManager</code>.
   * A newly-loaded driver class should call
   * the method <code>registerDriver</code> to make itself
   * known to the <code>DriverManager</code>.
   *
   * @param driver the new JDBC Driver that is to be registered with the
   *               <code>DriverManager</code>
   *
   * @exception ClassNotFoundException if the driver's class could not be found.
   * @exception InstantiationException if the driver could not be instantiated.
   * @exception IllegalAccessException if the driver's class or initializer is
   * not accessible.
   * @exception SQLException if a database access error occurs.
   */
  public static void registerDriver(String driver)
      throws ClassNotFoundException, InstantiationException,
  IllegalAccessException, SQLException
  {
    Class driverClass = Class.forName(driver);
    Object classObj = UtilFns.newInstance(driverClass);
    if (classObj instanceof Driver)
    {
      DriverManager.registerDriver((Driver)classObj);
    }
  }

  /**
   * Drops a Driver from the <code>DriverManager</code>'s list.  Applets can only
   * deregister Drivers from their own classloaders.
   *
   * @param driver the JDBC Driver to drop
   * @exception ClassNotFoundException if the driver's class could not be found.
   * @exception InstantiationException if the driver could not be instantiated.
   * @exception IllegalAccessException if the driver's class or initializer is
   * not accessible.
   * @exception SQLException if a database access error occurs.
   */
  public static void deregisterDriver(String driver)
      throws ClassNotFoundException, InstantiationException,
  IllegalAccessException, SQLException
  {
    Class driverClass = Class.forName(driver);
    Object classObj = UtilFns.newInstance(driverClass);
    if (classObj instanceof Driver)
    {
      DriverManager.deregisterDriver((Driver)classObj);
    }
  }

  /**
   * Connect to a JDBC database.
   * @param url a database url of the form jdbc:subprotocol:subname
   * @param callBack callBack method or null to execute on current thread
   *
   * @exception ClassNotFoundException if the driver's class could not be found.
   * @exception InstantiationException if the driver could not be instantiated.
   * @exception IllegalAccessException if the driver's class or initializer is
   * not accessible.
   * @exception SQLException if a database access error occurs.
   */
  public ConnectionJDBC(String url, CallBackCompletion callBack)
      throws ClassNotFoundException, InstantiationException,
  IllegalAccessException, SQLException
  {
    this(url, null, callBack);
  }

  /**
   * Connect to a JDBC database.
   * @param url a database url of the form jdbc:subprotocol:subname
   * @param userName user name
   * @param passWord password for the above user name
   * @param callBack callBack method or null to execute on current thread
   *
   * @exception ClassNotFoundException if the driver's class could not be found.
   * @exception InstantiationException if the driver could not be instantiated.
   * @exception IllegalAccessException if the driver's class or initializer is
   * not accessible.
   * @exception SQLException if a database access error occurs.
   */
  public ConnectionJDBC(String url, String userName, String passWord,
                        CallBackCompletion callBack)
      throws ClassNotFoundException, InstantiationException,
  IllegalAccessException, SQLException
  {
    this(url, saveUserPassword(null, userName, passWord), callBack);
  }

  /**
   * Connect to a JDBC database.
   * @param url a database url of the form jdbc:subprotocol:subname
   * @param info properties information
   * @param callBack callBack method or null to execute on current thread
   *
   * @exception ClassNotFoundException if the driver's class could not be found.
   * @exception InstantiationException if the driver could not be instantiated.
   * @exception IllegalAccessException if the driver's class or initializer is
   * not accessible.
   * @exception SQLException if a database access error occurs.
   */
  public ConnectionJDBC(String url, Properties info,
                        CallBackCompletion callBack)
  {
    this.url = url;
    this.dbInfo = info;
    this.callBack = callBack;

    if (callBack != null)  //if callback was provided
    {
      startBackgroundThread(null);
    }
  }

  /**
   * Initialize the connection with optional password popup.
   * @param parentComp parent component for password popup or null for no popup.
   * @param popupObj popup object or null for no popup.
   *
   * This method should only be used when a callback is
   * not provided to the constructor.
   * @see ConnectionJDBC
   *
   * @exception ClassNotFoundException if the driver's class could not be found.
   * @exception InstantiationException if the driver could not be instantiated.
   * @exception IllegalAccessException if the driver's class or initializer is
   * not accessible.
   * @exception SQLException if a database access error occurs.
   */
  public void initConnection(
      Component parentComp, IstiDialogPopup popupObj)
      throws ClassNotFoundException, InstantiationException,
  IllegalAccessException, SQLException
  {
    Thread.yield();  //allow other threads to execute

    if (!Thread.currentThread().isInterrupted())  //if thread is not iterrupted
    {
      //if connection is closed
      if (isClosed())
      {
        if (parentComp != null)  //if parent component provided
        {
          promptForPassword(parentComp);  //prompt for password
        }

        if (popupObj != null)  //if popup object exists
        {
          String msgStr = "Connecting to the database";
          popupObj.setMessageStr(msgStr);
          popupObj.pack();     //re-size the popup
          popupObj.show();     //show popup
        }

        createConnection();

        Thread.yield();  //allow other threads to execute
      }
    }

    if (!Thread.currentThread().isInterrupted())  //if thread is not iterrupted
    {
      //if connection existgs and the statement does not exist
      if (getConnection() != null && getStatement() == null)
      {
        if (popupObj != null)  //if popup object exists
        {
          String msgStr = "Initializing the database";
          popupObj.setMessageStr(msgStr);
          popupObj.pack();     //re-size the popup
          popupObj.show();     //show popup
        }

        createStatement();

        Thread.yield();  //allow other threads to execute
      }
    }
  }

  /**
   * Creates the database connection.
   * @exception ClassNotFoundException if the driver's class could not be found.
   * @exception InstantiationException if the driver could not be instantiated.
   * @exception IllegalAccessException if the driver's class or initializer is
   * not accessible.
   * @exception SQLException if a database access error occurs.
   */
  public void createConnection()
      throws ClassNotFoundException, InstantiationException,
  IllegalAccessException, SQLException
  {
    final Properties info = getInfo();
    final Connection dbConnection;

    //if properties information is provided
    if (info != null)
    {
      dbConnection = DriverManager.getConnection(url, info);
    }
    else
    {
      dbConnection = DriverManager.getConnection(url);
    }

    //save the connection
    setConnection(dbConnection);
  }

  /**
   * Creates the database statement.
   * @exception SQLException if a database access error occurs.
   */
  protected void createStatement()
      throws SQLException
  {
    if (!isClosed())  //if connection is open
    {
      //create the database statement
      setStatement(getConnection().createStatement());
    }
  }

  /**
   * Closes the database connection.
   */
  public void close()
  {
    final Statement dbStatement;
    final Connection dbConnection;

    //get the statement and connection and then clear them
    synchronized (this)
    {
      dbStatement = databaseStatement;
      databaseStatement = null;
      dbConnection = databaseConnection;
      databaseConnection = null;
    }

    try
    {
      if (dbStatement != null)  //if statement exists
      {
        dbStatement.close();    //close the statement
      }
    }
    catch (SQLException sqlEx)
    {
    }

    try
    {
      if (dbConnection != null)  //if connection exists
      {
        dbConnection.close();    //close the connection
      }
    }
    catch (SQLException sqlEx)
    {
    }
  }

  /**
   * Tests to see if a Connection is closed.
   *
   * @return true if the connection is closed; false if it's still open
   */
  protected boolean isClosed()
  {
    boolean closedFlag = true;
    try
    {
      final Connection dbConnection = getConnection();
      if (dbConnection != null && !dbConnection.isClosed())
      {
        closedFlag = false;
      }
    }
    catch (SQLException sqlEx)
    {
      close();            //close the database connection
    }
    return closedFlag;
  }

  /**
   * @return the database connection.
   */
  public synchronized Connection getConnection()
  {
    return databaseConnection;
  }

  /**
   * @return the database info.
   */
  protected synchronized Properties getInfo()
  {
    return dbInfo;
  }

  /**
   * @return the database statement.
   */
  public synchronized Statement getStatement()
  {
    return databaseStatement;
  }

  /**
   * Sets the database connection.
   * @param dbConnection database connection
   */
  public synchronized void setConnection(Connection dbConnection)
  {
    databaseConnection = dbConnection;
  }

  /**
   * Sets the database statement.
   * @param dbStatement database statement
   */
  public synchronized void setStatement(Statement dbStatement)
  {
    databaseStatement = dbStatement;
  }

  /**
   * Sets the username and password.
   * @param userName user name
   * @param password password for the above user name
   */
  protected synchronized void setUserPassword(
      String userName, String password)
  {
    final Properties info = saveUserPassword(dbInfo, userName, password);
    dbInfo = info;
  }

  /**
   * Executes an SQL statement that returns a single <code>ResultSet</code> object.
   * @param sql typically this is a static SQL <code>SELECT</code> statement
   * @param parentComp parent component for password popup or null for no popup.
   * @param popupObj popup object or null for no popup.
   * @return query results or null if interrupted or callback is used.
   *
   * If a callback is used the return value ('retVal')
   * will contain the query results.
   *
   * The connection will be initialized if needed.
   * @see initConnection
   *
   * @exception ClassNotFoundException if the driver's class could not be found.
   * @exception InstantiationException if the driver could not be instantiated.
   * @exception IllegalAccessException if the driver's class or initializer is
   * not accessible.
   * @exception SQLException if a database access error occurs.
   */
  protected final QueryJDBC executeQueryNow(
      String sql, Component parentComp, IstiDialogPopup popupObj)
      throws ClassNotFoundException, InstantiationException,
      IllegalAccessException, SQLException
  {
    QueryJDBC query = null;

    synchronized (executeLock)
    {
      if (sql != null)   //if query exists
      {
                   //make sure the connection is initialized:
        initConnection(parentComp, popupObj);

        Thread.yield();  //allow other threads to execute

        if (!Thread.currentThread().isInterrupted())  //if thread is not iterrupted
        {
          if (popupObj != null)  //if popup object exists
          {
            String msgStr = "Accessing the database";
            popupObj.setMessageStr(msgStr);
            popupObj.pack();     //re-size the popup
            popupObj.show();     //show popup
          }

          if(!isClosed())
          {   //database connection is reported as open
            try
            {      //execute the query and return results:
              query = new QueryJDBC(this, sql);
            }
            catch(SQLException ex)
            {      //exception thrown; setup to retry
              query = null;
            }
          }
              //if query failed and database was seen as open then close
              // database and retry (because after a communication problem
              // the connection can be closed but still say that it's open):
          if(query == null)
          {   //query failed or not attempted yet
            close();         //close connection to database
                   //make sure the connection is initialized:
            initConnection(parentComp, popupObj);
                   //execute the query and return results:
            query = new QueryJDBC(this, sql);
          }
        }
      }
      else    //no query; just do initialize
        initConnection(parentComp, popupObj);
    }
    return query;  //return query results
  }

  /**
   * Executes an SQL statement that returns a single <code>ResultSet</code> object.
   * @param sql typically this is a static SQL <code>SELECT</code> statement
   * @param parentComp parent component for password popup or null for no popup.
   * @param popupObj popup object or null for no popup.
   * @return query results or null if interrupted or callback is used.
   *
   * If a callback is used the return value ('retVal')
   * will contain the query results.
   *
   * The connection will be initialized if needed.
   * @see initConnection
   *
   * @exception ClassNotFoundException if the driver's class could not be found.
   * @exception InstantiationException if the driver could not be instantiated.
   * @exception IllegalAccessException if the driver's class or initializer is
   * not accessible.
   * @exception SQLException if a database access error occurs.
   */
  public final QueryJDBC executeQuery(
      String sql, Component parentComp, IstiDialogPopup popupObj)
      throws ClassNotFoundException, InstantiationException,
  IllegalAccessException, SQLException
  {
    QueryJDBC query = null;

    if (callBack != null)               //if callback was provided
    {
      startBackgroundThread(sql);
    }
    else
    {
      query = executeQueryNow(sql, parentComp, popupObj);
    }

    return query;  //return query results
  }

  /**
   * Executes an SQL statement that returns a single <code>ResultSet</code> object.
   * @param sql typically this is a static SQL <code>SELECT</code> statement
   * @return query results or null if interrupted or callback is used.
   *
   * If a callback is used the return value ('retVal')
   * will contain the query results.
   *
   * The connection will be initialized if needed.
   * @see initConnection
   *
   * @exception ClassNotFoundException if the driver's class could not be found.
   * @exception InstantiationException if the driver could not be instantiated.
   * @exception IllegalAccessException if the driver's class or initializer is
   * not accessible.
   * @exception SQLException if a database access error occurs.
   */
  public QueryJDBC executeQuery(String sql)
      throws ClassNotFoundException, InstantiationException,
  IllegalAccessException, SQLException
  {
    return executeQuery(sql, null, null);
  }

  /**
   * Process a callback
   * @param retVal return value object
   * @param errorMsg error message or null if no error.
   */
  protected void processCallback(Object retVal, String errorMsg)
  {
    if (callBack != null)  //if callback was provided
    {
      //callback and clear
      callBack.jobCompleted(retVal, errorMsg);
    }
  }

  /**
   * Creates a password panel.
   * @return password panel
   */
  protected IstiPasswordPanel createPasswordPanel()
  {
    final Properties info = getInfo();
    IstiPasswordPanel passwordPanel = new IstiPasswordPanel();
    passwordPanel.setUsername(info.getProperty(INFO_USER_KEY));
    passwordPanel.setPassword(info.getProperty(INFO_PASSWORD_KEY));
    return passwordPanel;
  }

  /**
   * Creates a password dialog.
   * @return the password dialog
   * @param parentComp parent component for popup.
   * @param passwordPanel password panel
   */
  protected IstiDialogPopup createPasswordDialog(
      Component parentComp, IstiPasswordPanel passwordPanel)
  {
    return IstiPasswordPanel.createLoginDialog(parentComp, passwordPanel);
  }

  /**
   * Bring up password popup.
   * @param parentComp parent component for popup.
   */
  public void promptForPassword(Component parentComp)
  {
    //create a password panel
    IstiPasswordPanel passwordPanel = createPasswordPanel();

    //create a popup for the password panel
    IstiDialogPopup passwordPopup = createPasswordDialog(
        parentComp, passwordPanel);

    //show the password popup
    passwordPopup.show();

    //save the new username and password
    setUserPassword(passwordPanel.getUsername(), passwordPanel.getPassword());
  }

  /**
   * Starts the background thread.
   * @param sql typically this is a static SQL <code>SELECT</code> statement
   */
  protected void startBackgroundThread(String sql)
  {
    IstiDialogPopup popupObj = null;
    //get the parent component
    final Component parentComp = callBack.getParentComponent();

    if (parentComp != null)  //if parent component provided
    {
      final String msgStr = "UNITIALIZED";
      final String titleStr = "Please wait...";

      //create a popup object
      popupObj = new IstiDialogPopup(
          parentComp, msgStr, titleStr,
          IstiDialogPopup.INFORMATION_MESSAGE, DIALOG_OPTION_STR, false);
    }

    //start the background thread
    new BackgroundThread(this, sql, parentComp, popupObj).start();
  }
}


/**
 * Class BackgroundThread defines a thread used to do background tasks.
 */
class BackgroundThread extends IstiThread
{
  //database connection
  private final ConnectionJDBC dbConn;
  //sql statement
  private final String sql;
  //
  private final Component parentComp;
  //popup menu object
  private final IstiDialogPopup popupObj;

  /**
   * Create a background thread.
   * @param dbConn database connection
   * @param sql typically this is a static SQL <code>SELECT</code> statement
   * @param parentComp parent component for password popup or null for no popup.
   * @param popupObj popup object or null for no popup.
   */
  public BackgroundThread(
      ConnectionJDBC dbConn, String sql, Component parentComp,
      IstiDialogPopup popupObj)
  {
    super("ConnectionJDBC.BackgroundThread-" + nextThreadNum());
    this.dbConn = dbConn;
    this.sql = sql;
    this.parentComp = parentComp;
    this.popupObj = popupObj;
  }

  /**
   * Runs the connect thread.
   */
  public void run()
  {
    QueryJDBC query = null;
    String errorMessage = null;

    try
    {
      //execute the query and save the results
      query = dbConn.executeQueryNow(sql, parentComp, popupObj);
    }
    catch (Exception ex)
    {
      errorMessage = ex.toString();
    }

    if (errorMessage != null)  //if there was an error
    {
      dbConn.close();          //close the database connection
    }

    if (popupObj != null)  //if popup object exists
    {
      popupObj.close();    //close the popup object
    }

    dbConn.processCallback(query, errorMessage);  //process the callback
  }
}
