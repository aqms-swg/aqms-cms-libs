//IstiConnectionInfoDialog.java:  Defines a connection information dialog.
//
//  2/14/2008 -- [KF]  Initial release version.
//

package com.isti.util.database;

import java.awt.Component;
import java.sql.SQLException;
import java.sql.Connection;
import com.isti.util.gui.IstiDialogPopup;
import com.isti.util.gui.IstiDialogUtil;

/**
 * Class IstiConnectionInfoDialog defines a connection information dialog.
 */
public class IstiConnectionInfoDialog
    extends IstiDialogPopup
{
  /**
   * Login dialog default title string.
   */
  public static String LOGIN_DIALOG_DEFAULT_TITLE_STRING =
      IstiConnectionInfoPanel.LOGIN_DIALOG_DEFAULT_TITLE_STRING;

  //the connection information panel
  private final IstiConnectionInfoPanel connectionInfoPanel;
  //dialog utility object for popup messages:
  private final IstiDialogUtil dialogUtilObj;

  /**
   * Creates the connection information dialog.
   * @param parentComp the parent component for the popup.
   */
  public IstiConnectionInfoDialog(final Component parentComp)
  {
    this(parentComp, new IstiConnectionInfoPanel(),
         LOGIN_DIALOG_DEFAULT_TITLE_STRING,
         new IstiDialogUtil(parentComp));
    connectionInfoPanel.setHostname("localhost");
  }

  /**
   * Creates the connection information dialog.
   * @param parentComp the parent component for the popup.
   * @param infoPanel the connection information panel.
   * @param dialogUtilObj the dialog utility object or null if none.
   * @param titleStr the title string for popup window.
   */
  public IstiConnectionInfoDialog(
      final Component parentComp, final IstiConnectionInfoPanel infoPanel,
      final String titleStr, final IstiDialogUtil dialogUtilObj)
  {
    super(parentComp, infoPanel, titleStr,
          IstiDialogPopup.OK_CANCEL_OPTION);
    this.connectionInfoPanel = infoPanel;
    setInitialFocusComponent(connectionInfoPanel.getUsernameFieldObj());
    this.dialogUtilObj = dialogUtilObj;
  }

  /**
   * Create the connection.
   * @param hostName the host name.
   * @param port the port or null for the default.
   * @param userName the user name.
   * @param password the password.
   * @param dataBase the database.
   * @return the connection or null if error.
   */
  public Connection createMySQLConnection(
      String hostName, String port, String userName, String password,
      String dataBase)
  {
    try
    {
      MySQLConnectionJDBC.registerDriver();
      final String url = MySQLConnectionJDBC.getUrl(
          hostName, null, port, dataBase);
      final java.util.Properties info = MySQLConnectionJDBC.saveUserPassword(
          null, userName, password);
      MySQLConnectionJDBC mySqlConn = new MySQLConnectionJDBC(url, info, null);
      mySqlConn.createConnection();
      return mySqlConn.getConnection();
    }
    catch (ClassNotFoundException ex)
    {
      showErrorMessage(
          "The MySQL driver's class could not be found: " + ex);
    }
    catch (InstantiationException ex)
    {
      showErrorMessage("The MySQL driver could not be instantiated:" + ex);
    }
    catch (IllegalAccessException ex)
    {
      showErrorMessage(
          "The MySQL driver's class or initializer is not accessible:" + ex);
    }
    catch (SQLException ex)
    {
      showErrorMessage("MySQL database access error:" + ex);
    }
    return null;
  }

  /**
   * Gets the connection information panel.
   * @return the connection information panel.
   */
  public IstiConnectionInfoPanel getConnectionInfoPanel()
  {
    return connectionInfoPanel;
  }

  /**
   * Gets the dialog utility object for popup messages.
   * @return the dialog utility object for popup messages.
   */
  public IstiDialogUtil getDialogUtil()
  {
    return dialogUtilObj;
  }

  /**
   * Get the connection.
   * @return the connection or null if none.
   */
  public Connection getConnection()
  {
    Connection dbConnection = null;

    if (showAndReturnIndex() == IstiDialogPopup.OK_OPTION)
    {
      dbConnection = createMySQLConnection(
          connectionInfoPanel.getHostname(),
          connectionInfoPanel.getPort(),
          connectionInfoPanel.getUsername(),
          connectionInfoPanel.getPassword(),
          connectionInfoPanel.getDatabase());
    }
    return dbConnection;
  }

  /**
   * Show the error message.
   * @param msgStr the error message text to display.
   */
  public void showErrorMessage(String msgStr)
  {
    if (dialogUtilObj != null)
    {
      dialogUtilObj.popupErrorMessage(msgStr);
    }
    else
    {
      System.err.println(msgStr);
    }
  }
}
