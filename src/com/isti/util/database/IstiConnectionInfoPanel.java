//IstiConnectionInfoPanel.java:  Defines a connection information panel.
//
//  2/14/2008 -- [KF]  Initial release version.
//

package com.isti.util.database;

import java.util.Vector;
import java.util.Arrays;
import java.awt.*;
import java.awt.event.*;
import javax.swing.*;
import javax.swing.event.*;
import com.isti.util.gui.MultiLineJLabel;
import com.isti.util.gui.IstiDialogPopup;
import com.isti.util.gui.FilteredJTextField;

/**
 * Class IstiConnectionInfoPanel defines a connection information panel.
 */
public class IstiConnectionInfoPanel
    extends JPanel
{
  protected final JLabel hostnameLabel = new JLabel("Hostname: ");
  protected final JTextField hostnameField;
  protected final JLabel portLabel = new JLabel("Port: ");
  protected final JTextField portField;
  protected final JLabel databaseLabel = new JLabel("Database: ");
  protected final JTextField databaseField;
  protected final JLabel usernameLabel = new JLabel("Username: ");
  protected final JTextField usernameField;
  protected final JLabel passwordLabel = new JLabel("Password: ");
  protected final JPasswordField passwordField = new JPasswordField();
  protected final MultiLineJLabel panelPromptTextObj;
  protected final Vector listenerList = new Vector();

  /**
   * Login dialog default title string.
   */
  public static String LOGIN_DIALOG_DEFAULT_TITLE_STRING = "Login";

  /**
   * Login dialog default option string.
   */
  public static String LOGIN_DIALOG_DEFAULT_OPTION_STRING = "OK";

  /**
   * Login dialog cancel option string.
   */
  public static String LOGIN_DIALOG_CANCEL_OPTION_STRING = "Cancel";

  /**
   * The default number of characters in the username field.
   */
  public final static int USERNAME_COLUMNS = 16;

  /**
   * Creates a connection information panel.
   */
  public IstiConnectionInfoPanel()
  {
    this(null, new JTextField(USERNAME_COLUMNS));
  }

  /**
   * Creates a connection information panel.
   * @param panelPromptStr the "prompt" text to be shown on the panel,
   * or null for none.
   * @see createPasswordPanel
   */
  public IstiConnectionInfoPanel(String panelPromptStr)
  {
    this(panelPromptStr, new JTextField(USERNAME_COLUMNS));
  }

  /**
   * Creates a connection information panel.
   * @param columns the number of the columns for the username field.
   */
  public IstiConnectionInfoPanel(int columns)
  {
    this(null, new JTextField(columns));
  }

  /**
   * Creates a connection information panel.
   * @param panelPromptStr the "prompt" text to be shown on the panel,
   * or null for none.
   * @param columns the number of the columns for the username field.
   */
  public IstiConnectionInfoPanel(String panelPromptStr, int columns)
  {
    this(panelPromptStr, new JTextField(columns));
  }

  /**
   * Creates a connection information panel
   * with restricted character input for the username.
   * @param columns the number of the columns for the field.
   * @param notAllowedChars a String of characters not allowed to be entered
   * into the username field.
   * @see createRestrictedPasswordPanel
   * @see USERNAME_COLUMNS, USERNAME_NOT_ALLOWED_CHARS
   */
  public IstiConnectionInfoPanel(int columns, String notAllowedChars)
  {
    this(null,
         new FilteredJTextField(null, columns, notAllowedChars, false, 0, false));
  }

  /**
   * Creates a connection information panel
   * with restricted character input for the username.
   * @param panelPromptStr the "prompt" text to be shown on the panel,
   * or null for none.
   * @param columns the number of the columns for the field.
   * @param notAllowedChars a String of characters not allowed to be entered
   * into the username field.
   * @see createRestrictedPasswordPanel
   * @see USERNAME_COLUMNS, USERNAME_NOT_ALLOWED_CHARS
   */
  public IstiConnectionInfoPanel(String panelPromptStr, int columns,
                                 String notAllowedChars)
  {
    this(panelPromptStr,
         new FilteredJTextField(null, columns, notAllowedChars, false, 0, false));
  }

  /**
   * Creates a connection information panel.
   * @param usernameField the username text field.
   */
  protected IstiConnectionInfoPanel(JTextField usernameField)
  {
    this(null, usernameField);
  }

  /**
   * Creates a connection information panel.
   * @param panelPromptStr the "prompt" text to be shown on the panel,
   * or null for none.
   * @param usernameField the username text field.
   */
  protected IstiConnectionInfoPanel(String panelPromptStr,
                                    JTextField usernameField)
  {
    hostnameField = new JTextField();
    hostnameLabel.setLabelFor(hostnameField);
    portField = new JTextField();
    portField.setToolTipText("The port or empty for the default port.");
    portLabel.setLabelFor(portField);
    databaseField = new JTextField();
    databaseLabel.setLabelFor(databaseField);
    this.usernameField = usernameField;
    usernameLabel.setLabelFor(usernameField);
    //setup "prompt" text for panel:
    panelPromptTextObj = new MultiLineJLabel(panelPromptStr, 25);
    try
    {
      jbInit();
    }
    catch (Exception e)
    {
      e.printStackTrace();
    }
  }

  /**
   * Requests that the current "initial" component have the keyboard focus.
   */
  public void setInitialFocus()
  {
    usernameField.requestFocus();
  }

  /**
   * Returns the password text.
   * @return the password text.
   */
  public String getPassword()
  {
    final char[] charArr = passwordField.getPassword();
    final String retStr = new String(charArr); //create string from chars
    Arrays.fill(charArr, (char) 0); //clear char array
    return retStr;
  }

  /**
   * Sets the password text.
   * @param passwordText password text to use.
   */
  public void setPassword(String passwordText)
  {
    passwordField.setText(passwordText);
  }

  /**
   * Returns the username text.
   * @return the username text.
   */
  public String getUsername()
  {
    return usernameField.getText();
  }

  /**
   * Sets the username text.
   * @param usernameText username text to use.
   */
  public void setUsername(String usernameText)
  {
    usernameField.setText(usernameText);
  }

  /**
   * Gets the hostname text.
   * @return the hostname text.
   */
  public String getHostname()
  {
    return hostnameField.getText();
  }

  /**
   * Sets the hostname text.
   * @param hostname the hostname text.
   */
  public void setHostname(String hostname)
  {
    hostnameField.setText(hostname);
  }

  /**
   * Sets if the host name is visible.
   * @param b true  if visible, false otherwise.
   */
  public void setHostnameVisible(boolean b)
  {
    hostnameLabel.setVisible(b);
    hostnameField.setVisible(b);
  }

  /**
   * Gets the port text.
   * @return the port text.
   */
  public String getPort()
  {
    return portField.getText();
  }

  /**
   * Sets the port text.
   * @param port the port text.
   */
  public void setPort(String port)
  {
    portField.setText(port);
  }

  /**
   * Sets if the port is visible.
   * @param b true  if visible, false otherwise.
   */
  public void setPortVisible(boolean b)
  {
    portLabel.setVisible(b);
    portField.setVisible(b);
  }

  /**
   * Gets the database text.
   * @return the database text.
   */
  public String getDatabase()
  {
    return databaseField.getText();
  }

  /**
   * Sets the database text.
   * @param database the database text.
   */
  public void setDatabase(String database)
  {
    databaseField.setText(database);
  }

  /**
   * Sets if the database is visible.
   * @param b true  if visible, false otherwise.
   */
  public void setDatabaseVisible(boolean b)
  {
    databaseLabel.setVisible(b);
    databaseField.setVisible(b);
  }

  /**
   * Returns the 'JTextField' object for the username.
   * @return The 'JTextField' object for the username.
   */
  public JTextField getUsernameFieldObj()
  {
    return usernameField;
  }

  /**
   * Initializes components for the panel.
   * @throws Exception if an error occurs.
   */
  private void jbInit() throws Exception
  {
    setLayout(new GridBagLayout());
    this.setBorder(BorderFactory.createEmptyBorder(5, 10, 10, 10));

    final GridBagConstraints constraintsObj = new GridBagConstraints();
    constraintsObj.gridx = constraintsObj.gridy = 0;
    constraintsObj.gridheight = 1;
    constraintsObj.weightx = constraintsObj.weighty = 0.0;

    final String str;
    if ( (str = panelPromptTextObj.getText()) != null && str.length() > 0)
    {
      constraintsObj.gridwidth = 2;
      constraintsObj.insets = new Insets(0, 0, 15, 0); //space after prompt
      add(panelPromptTextObj, constraintsObj);
      constraintsObj.insets = new Insets(0, 0, 0, 0);
      ++constraintsObj.gridy;
    }

    constraintsObj.gridwidth = 1;
    constraintsObj.fill = GridBagConstraints.NONE;
    add(hostnameLabel, constraintsObj);
    ++constraintsObj.gridx;
    constraintsObj.weightx = constraintsObj.weighty = 1.0;
    constraintsObj.fill = GridBagConstraints.HORIZONTAL;
    add(hostnameField, constraintsObj);
    --constraintsObj.gridx;
    ++constraintsObj.gridy;

    constraintsObj.gridwidth = 1;
    constraintsObj.fill = GridBagConstraints.NONE;
    add(portLabel, constraintsObj);
    ++constraintsObj.gridx;
    constraintsObj.weightx = constraintsObj.weighty = 1.0;
    constraintsObj.fill = GridBagConstraints.HORIZONTAL;
    add(portField, constraintsObj);
    --constraintsObj.gridx;
    ++constraintsObj.gridy;

    constraintsObj.gridwidth = 1;
    constraintsObj.fill = GridBagConstraints.NONE;
    add(databaseLabel, constraintsObj);
    ++constraintsObj.gridx;
    constraintsObj.weightx = constraintsObj.weighty = 1.0;
    constraintsObj.fill = GridBagConstraints.HORIZONTAL;
    add(databaseField, constraintsObj);
    --constraintsObj.gridx;
    ++constraintsObj.gridy;

    constraintsObj.gridwidth = 1;
    constraintsObj.fill = GridBagConstraints.NONE;
    add(usernameLabel, constraintsObj);
    ++constraintsObj.gridx;
    constraintsObj.weightx = constraintsObj.weighty = 1.0;
    constraintsObj.fill = GridBagConstraints.HORIZONTAL;
    add(usernameField, constraintsObj);
    --constraintsObj.gridx;
    ++constraintsObj.gridy;

    constraintsObj.weightx = constraintsObj.weighty = 0.0;
    constraintsObj.fill = GridBagConstraints.NONE;
    add(passwordLabel, constraintsObj);
    ++constraintsObj.gridx;
    constraintsObj.weightx = constraintsObj.weighty = 1.0;
    constraintsObj.fill = GridBagConstraints.HORIZONTAL;
    add(passwordField, constraintsObj);

    final UpdateListener updateListener = new UpdateListener();
    usernameField.getDocument().addDocumentListener(updateListener);
    passwordField.getDocument().addDocumentListener(updateListener);
  }

  /**
   * Adds an <code>ActionListener</code> to the panel.
   * @param l the <code>ActionListener</code> to be added.
   */
  public void addActionListener(ActionListener l)
  {
    listenerList.add(l);
  }

  /**
   * Removes an <code>ActionListener</code> from the panel.
   * If the listener is the currently set <code>Action</code>
   * for the panel, then the <code>Action</code>
   * is set to <code>null</code>.
   * @param l the listener to be removed.
   */
  public void removeActionListener(ActionListener l)
  {
    listenerList.remove(l);
  }

  /**
   * Notifies all listeners that have registered interest for
   * notification on this event type.  The event instance
   * is lazily created using the parameters passed into
   * the fire method.
   * @param event  the <code>ChangeEvent</code> object
   * @see EventListenerList
   */
  protected void fireActionPerformed(ActionEvent event)
  {
    Object[] listeners = listenerList.toArray();

    // Process the listeners last to first, notifying
    // those that are interested in this event
    for (int i = listeners.length - 1; i >= 0; i--)
    {
      ( (ActionListener) listeners[i]).actionPerformed(event);
    }
  }

  /**
   * Update listener class.
   */
  protected class UpdateListener
      implements DocumentListener
  {
    /**
     * Gives notification that there was an insert into the document.  The
     * range given by the DocumentEvent bounds the freshly inserted region.
     *
     * @param e the document event
     */
    public void insertUpdate(DocumentEvent e)
    {
      update(e);
    }

    /**
     * Gives notification that a portion of the document has been
     * removed.  The range is given in terms of what the view last
     * saw (that is, before updating sticky positions).
     *
     * @param e the document event
     */
    public void removeUpdate(DocumentEvent e)
    {
      update(e);
    }

    /**
     * Gives notification that an attribute or set of attributes changed.
     *
     * @param e the document event
     */
    public void changedUpdate(DocumentEvent e)
    {
      update(e);
    }

    private void update(DocumentEvent e)
    {
      fireActionPerformed(new ActionEvent(this, 0, e.getType().toString()));
    }
  }

  /**
   * @return login dialog
   * @param parentComp the parent component for the popup.
   */
  public static IstiDialogPopup createLoginDialog(Component parentComp)
  {
    return createLoginDialog(
        parentComp, new IstiConnectionInfoPanel(),
        LOGIN_DIALOG_DEFAULT_TITLE_STRING,
        LOGIN_DIALOG_DEFAULT_OPTION_STRING);
  }

  /**
   * @return login dialog
   * @param parentComp the parent component for the popup.
   * @param msgObj the message object to be shown.
   */
  public static IstiDialogPopup createLoginDialog(Component parentComp,
                                                  Object msgObj)
  {
    return createLoginDialog(
        parentComp, msgObj, LOGIN_DIALOG_DEFAULT_TITLE_STRING,
        LOGIN_DIALOG_DEFAULT_OPTION_STRING);
  }

  /**
   * @return login dialog
   * @param parentComp the parent component for the popup.
   * @param msgObj the message object to be shown.
   * @param titleStr the title string for popup window.
   */
  public static IstiDialogPopup createLoginDialog(Component parentComp,
                                                  Object msgObj,
                                                  String titleStr)
  {
    return createLoginDialog(parentComp, msgObj, titleStr,
                             LOGIN_DIALOG_DEFAULT_OPTION_STRING);
  }

  /**
   * @return login dialog
   * @param parentComp the parent component for the popup.
   * @param msgObj the message object to be shown.
   * @param titleStr the title string for popup window.
   * @param optionObj the Object that defines the button, or null to display
   * no buttons.
   */
  public static IstiDialogPopup createLoginDialog(Component parentComp,
                                                  Object msgObj,
                                                  String titleStr,
                                                  Object optionObj)
  {
    IstiDialogPopup popup =
        new IstiDialogPopup(parentComp, msgObj, titleStr, optionObj);
    return popup;
  }
}
