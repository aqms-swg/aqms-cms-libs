//MySQLConnectionJDBC.java:  Implements an MySQL JDBC connection.
//
//   8/12/2004 -- [KF]  Initial version.
//

package com.isti.util.database;

import java.sql.SQLException;
import java.util.Properties;

import com.isti.util.CallBackCompletion;

public class MySQLConnectionJDBC extends ConnectionJDBC
{
  protected final static String MYSQL_DRIVER_NAME =
      "com.mysql.jdbc.Driver";

  //true if the MySQL driver was loaded
  private static boolean mySQLDriverLoaded = false;

  /**
   * Registers the MySQL driver with the <code>DriverManager</code>.
   * A newly-loaded driver class should call
   * the method <code>registerDriver</code> to make itself
   * known to the <code>DriverManager</code>.
   *
   * @exception ClassNotFoundException if the driver's class could not be found.
   * @exception InstantiationException if the driver could not be instantiated.
   * @exception IllegalAccessException if the driver's class or initializer is
   * not accessible.
   * @exception SQLException if a database access error occurs.
   */
  public static void registerDriver()
      throws ClassNotFoundException, InstantiationException,
  IllegalAccessException, SQLException
  {
    if (!mySQLDriverLoaded)  //if the MySQL driver was not loaded
    {
      // Load MySQL driver
      registerDriver(MYSQL_DRIVER_NAME);
      mySQLDriverLoaded = true;
    }
  }

  /**
   * Drops the MySQL driver from the <code>DriverManager</code>'s list.
   * Applets can only deregister Drivers from their own classloaders.
   *
   * @exception ClassNotFoundException if the driver's class could not be found.
   * @exception InstantiationException if the driver could not be instantiated.
   * @exception IllegalAccessException if the driver's class or initializer is
   * not accessible.
   * @exception SQLException if a database access error occurs.
   */
  public static void deregisterDriver()
      throws ClassNotFoundException, InstantiationException,
  IllegalAccessException, SQLException
  {
    if (mySQLDriverLoaded)  //if the MySQL driver was loaded
    {
      // Unload MySQL driver
      deregisterDriver(MYSQL_DRIVER_NAME);
      mySQLDriverLoaded = false;
    }
  }

  /**
   * Connect to an MySQL JDBC database.
   * @param hostName the host name.
   * @param failOverHostNames the fail over host names or null for none.
   * @param port the port.
   * @param dataBase the database.
   * @param userName the user name.
   * @param passWord password for the above user name
   * @param callBack callBack method or null to execute on current thread
   *
   * @exception ClassNotFoundException if the driver's class could not be found.
   * @exception InstantiationException if the driver could not be instantiated.
   * @exception IllegalAccessException if the driver's class or initializer is
   * not accessible.
   * @exception SQLException if a database access error occurs.
   */
  public MySQLConnectionJDBC(
      String hostName, String[] failOverHostNames, String port, String dataBase,
      String userName, String passWord, CallBackCompletion callBack)
      throws ClassNotFoundException, InstantiationException,
  IllegalAccessException, SQLException
  {
    this(getUrl(hostName, failOverHostNames, port, dataBase),
         saveUserPassword(null, userName, passWord), callBack);
  }

  /**
   * Connect to a MySQL JDBC database.
   * @param url a database url of the form jdbc:subprotocol:subname
   * @param info properties information
   * @param callBack callBack method or null to execute on current thread
   *
   * @exception ClassNotFoundException if the driver's class could not be found.
   * @exception InstantiationException if the driver could not be instantiated.
   * @exception IllegalAccessException if the driver's class or initializer is
   * not accessible.
   * @exception SQLException if a database access error occurs.
   * @see getUrl
   */
  public MySQLConnectionJDBC(String url, Properties info,
                             CallBackCompletion callBack)
  {
    super(url, info, callBack);
  }

  /**
   * Gets a database url.
   * @param hostName the host name.
   * @param failOverHostNames the fail over host names or null for none.
   * @param port the port.
   * @param dataBase the database.
   * @return the database url.
   */
  public static String getUrl(
      String hostName, String[] failOverHostNames, String port, String dataBase)
  {
    String urlString = DEFAULT_PROTOCOL+":mysql://";
    if (hostName != null && hostName.length() > 0)  //if host name was provided
    {
      urlString += hostName;
    }
    if (failOverHostNames != null)  //if failover hostnames were provided
    {
      for (int i = 0; i < failOverHostNames.length; i++)
      {
        //if failover hostname was provided
        final String failOverHostName = failOverHostNames[i];
        if (failOverHostName != null && failOverHostName.length() > 0)
        {
          urlString += ","+failOverHostName;
        }
      }
    }
    if (port != null && port.length() > 0)  //if port was provided
    {
      urlString += ":"+port;
    }
    urlString += "/";
    if (dataBase != null && dataBase.length() > 0)  //if database was provided
    {
      urlString += dataBase;
    }
    return urlString;
  }
}