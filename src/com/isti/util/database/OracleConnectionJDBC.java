//OracleConnectionJDBC.java:  Implements an Oracle JDBC connection.
//
//   1/27/2003 -- [KF]
//   8/10/2004 -- [KF]  Fixed 'deregisterDriver()' method to call
//                      'deregisterDriver(driver)',
//                      made protected static method public.
//

package com.isti.util.database;

import java.sql.SQLException;

import com.isti.util.CallBackCompletion;

public class OracleConnectionJDBC extends ConnectionJDBC
{
  protected final static String ORACLE_DRIVER_NAME =
      "oracle.jdbc.driver.OracleDriver";

  public final static String ORACLE_SUBPROTOCOL = "oracle";

  public final static String ORACALE_SUBNAME = "oci8";
  public final static String ORACALE_THIN_SUBNAME = "thin";

  public final static String ORACLE_URL =
      getURL(ORACLE_SUBPROTOCOL, ORACALE_SUBNAME);

  public final static String ORACLE_THIN_URL =
      getURL(ORACLE_SUBPROTOCOL, ORACALE_THIN_SUBNAME);

  //true if the Oracle driver was loaded
  private static boolean oracleDriverLoaded = false;

  /**
   * Gets a database url of the form jdbc:subprotocol:subname
   * @param subProtocol sub-protocol
   * @param subName sub-name of the protocol
   *
   * @return database url
   */
  public static String getURL(String subProtocol, String subName)
  {
    return DEFAULT_PROTOCOL+":"+subProtocol+":"+subName+":@";
  }

  /**
   * Registers the Oracle driver with the <code>DriverManager</code>.
   * A newly-loaded driver class should call
   * the method <code>registerDriver</code> to make itself
   * known to the <code>DriverManager</code>.
   *
   * @exception ClassNotFoundException if the driver's class could not be found.
   * @exception InstantiationException if the driver could not be instantiated.
   * @exception IllegalAccessException if the driver's class or initializer is
   * not accessible.
   * @exception SQLException if a database access error occurs.
   */
  public static void registerDriver()
      throws ClassNotFoundException, InstantiationException,
  IllegalAccessException, SQLException
  {
    if (!oracleDriverLoaded)  //if the Oracle driver was not loaded
    {
      // Load Oracle driver
      registerDriver(ORACLE_DRIVER_NAME);
      oracleDriverLoaded = true;
    }
  }

  /**
   * Drops the Oracle driver from the <code>DriverManager</code>'s list.
   * Applets can only deregister Drivers from their own classloaders.
   *
   * @exception ClassNotFoundException if the driver's class could not be found.
   * @exception InstantiationException if the driver could not be instantiated.
   * @exception IllegalAccessException if the driver's class or initializer is
   * not accessible.
   * @exception SQLException if a database access error occurs.
   */
  public static void deregisterDriver()
      throws ClassNotFoundException, InstantiationException,
  IllegalAccessException, SQLException
  {
    if (oracleDriverLoaded)  //if the Oracle driver was loaded
    {
      // Unload Oracle driver
      deregisterDriver(ORACLE_DRIVER_NAME);
      oracleDriverLoaded = false;
    }
  }

  /**
   * Connect to an Oracle JDBC database.
   * @param userName user name
   * @param passWord password for the above user name
   * @param callBack callBack method or null to execute on current thread
   *
   * @exception ClassNotFoundException if the driver's class could not be found.
   * @exception InstantiationException if the driver could not be instantiated.
   * @exception IllegalAccessException if the driver's class or initializer is
   * not accessible.
   * @exception SQLException if a database access error occurs.
   */
  public OracleConnectionJDBC(String userName, String passWord,
                              CallBackCompletion callBack)
      throws ClassNotFoundException, InstantiationException,
  IllegalAccessException, SQLException
  {
    this(ORACLE_URL, userName, passWord, callBack);
  }

  /**
   * Connect to an Oracle JDBC database.
   * @param hostName host name
   * @param port TCP/IP listener port
   * @param sid database SID (system identifier)
   * @param userName user name
   * @param passWord password for the above user name
   * @param callBack callBack method or null to execute on current thread
   *
   * @exception ClassNotFoundException if the driver's class could not be found.
   * @exception InstantiationException if the driver could not be instantiated.
   * @exception IllegalAccessException if the driver's class or initializer is
   * not accessible.
   * @exception SQLException if a database access error occurs.
   */
  public OracleConnectionJDBC(String hostName, int port, String sid,
                              String userName, String passWord,
                              CallBackCompletion callBack)
      throws ClassNotFoundException, InstantiationException,
  IllegalAccessException, SQLException
  {
    this(ORACLE_THIN_URL+hostName+":"+port+":"+sid,
         userName, passWord, callBack);
  }

  /**
   * Connect to an Oracle JDBC database.
   * @param url a database url of the form jdbc:subprotocol:subname
   * @param userName user name
   * @param passWord password for the above user name
   * @param callBack callBack method or null to execute on current thread
   *
   * @exception ClassNotFoundException if the driver's class could not be found.
   * @exception InstantiationException if the driver could not be instantiated.
   * @exception IllegalAccessException if the driver's class or initializer is
   * not accessible.
   * @exception SQLException if a database access error occurs.
   */
  public OracleConnectionJDBC(String url, String userName, String passWord,
                              CallBackCompletion callBack)
      throws ClassNotFoundException, InstantiationException,
  IllegalAccessException, SQLException
  {
    super(url, userName, passWord, callBack);
  }
}