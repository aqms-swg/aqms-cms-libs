//QueryJDBC.java:  Implements a JDBC query.
//
//   2/12/2003 -- [KF]
//

package com.isti.util.database;

import java.sql.*;
import java.util.*;
import com.isti.util.UtilFns;

public class QueryJDBC
{
  //query information
  private final String sql;

  //query results
  private final ResultSet dbResultSet;
  private final ResultSetMetaData dbResultSetMetaData;
  private final int columnCount;
  private final String[] columnNames;
  private final Vector columnObjectsList;

  /**
   * Gets the column names
   * @param sql typically this is a static SQL <code>SELECT</code> statement
   * @return the column names or null if not found
   */
  private static String[] getColumnNames(String sql)
  {
    String[] columnNames = null;

    final String sqlUpper = sql.toUpperCase();
    final int selectIndex = sqlUpper.indexOf(DatabaseUtil.SELECT_DISTINCT_TEXT);
    final int fromIndex = sqlUpper.indexOf(DatabaseUtil.FROM_TEXT);

    if (selectIndex >= 0 && fromIndex >= 0)
    {
      String selectStr =
          sql.substring(selectIndex +
          DatabaseUtil.SELECT_DISTINCT_TEXT.length(), fromIndex);
      if (selectStr.length() > 0 && !selectStr.equals("*"))
      {
        Vector v = UtilFns.listStringToVector(selectStr, ',', true);
        final int columnCount = v.size();
        columnNames = (String[])v.toArray(new String[columnCount]);
      }
    }
    return columnNames;
  }

  /**
   * Gets the column names
   * @param dbResultSetMetaData result set meta data
   * @return the column names
   * @exception SQLException if a database access error occurs
   */
  private static String[] getColumnNames(ResultSetMetaData dbResultSetMetaData)
      throws SQLException
  {
    //get column names from the result set meta data
    final int columnCount = dbResultSetMetaData.getColumnCount();
    final String[] columnNames = new String[columnCount];

    //get column names
    for (int i = 0; i < columnCount; i++)
    {
      columnNames[i] = dbResultSetMetaData.getColumnName(i + 1);
    }
    return columnNames;
  }

  /**
   * Determines if the column names are valid
   * @param resultColumnNames result column names
   * @param columnNames column names
   * @return true if the column names are valid
   */
  private static boolean isValidColumnNames(
      String[] resultColumnNames, String[] columnNames)
  {
    if (columnNames == null)
      return false;

    if (columnNames.length != resultColumnNames.length)
    {
      return false;
    }
    else
    {
      for (int i = 0; i < columnNames.length; i++)
      {
        if (columnNames[i].indexOf(resultColumnNames[i]) < 0)
          return false;
      }
    }
    return true;
  }

  /**
   * Gets the column names
   * @param sql typically this is a static SQL <code>SELECT</code> statement
   * @param dbResultSetMetaData result set meta data
   * @return the column names
   * @exception SQLException if a database access error occurs
   */
  private static String[] getColumnNames(
      String sql, ResultSetMetaData dbResultSetMetaData)
      throws SQLException
  {
    //get the column names from the result set meta data
    String[] resultColumnNames = getColumnNames(dbResultSetMetaData);

    //try to get column names from sql statement
    String[] columnNames = getColumnNames(sql);

    //if not valid column names
    if (!isValidColumnNames(resultColumnNames, columnNames))
    {
      //use column names from the result set meta data
      columnNames = resultColumnNames;
    }
    return columnNames;
  }

  /**
   * Execute a query.
   * @param dbConn database connection.
   * @param sql typically this is a static SQL <code>SELECT</code> statement
   * @exception SQLException if a database access error occurs
   */
  public QueryJDBC(ConnectionJDBC dbConn, String sql)
      throws SQLException
  {
    this.sql = sql;  //save the query

    //execute the query
    dbResultSet = dbConn.getStatement().executeQuery(sql);

    //save query results
    dbResultSetMetaData = dbResultSet.getMetaData();
    columnNames = getColumnNames(sql, dbResultSetMetaData);
    columnCount = columnNames.length;
    columnObjectsList = new Vector();

    //go through all of the results
    while (dbResultSet.next())
    {
	  Thread.yield();  //allow other threads to execute

      //exit if interrupted
      if (Thread.currentThread().isInterrupted())
      {
        return;
      }

      Map columnObjects = new Hashtable();
      String columnName;
      Object columnValue;

      for (int i = 0; i < columnCount; i++)
      {
        columnName = columnNames[i];
        columnValue = dbResultSet.getObject(i + 1);

        //add the column
        addColumn(columnObjects, columnName, columnValue);
      }

      //save the column objects in the list
      columnObjectsList.add(columnObjects);
    }
  }

  /**
   * Adds a column to the list.
   * @param columnObjects map of column objects.
   * @param columnName column name
   * @param columnValue column value
   * @return true if the column was added.
   */
  protected static boolean addColumn(
      Map columnObjects, String columnName, Object columnValue)
  {
    try
    {
      columnObjects.put(columnName, columnValue);
      return true;
    }
    catch (Exception ex)
    {
    }

    return false;
  }

  /**
   * Gets information about the database.
   *
   * @exception SQLException if a database access error occurs
   *
   * @return information string or null if not available
   */
  public String getResultSetMetaDataInformation()
      throws SQLException
  {
    String info = null;
    if (dbResultSetMetaData != null)  //if result set meta data exists
    {
      info = "(\n";

      for (int column = 1; column <= columnCount; column++)
      {
        info += " " + dbResultSetMetaData.getColumnName(column);
        info += " " + dbResultSetMetaData.getColumnTypeName(column);
        int precision = dbResultSetMetaData.getPrecision(column);
        if (precision > 0)
          info += "(" + precision + ")";
        if (dbResultSetMetaData.isNullable(column) == ResultSetMetaData.columnNoNulls)
          info += " not null";

        if (column < columnCount)
          info += ",\n";
        else
          info += "\n)";
      }
    }
    return info;
  }

  /**
   * Getss the number of columns in this <code>ResultSet</code> object.
   *
   * @return the number of columns
   */
  public int getColumnCount()
  {
    return columnCount;
  }

  /**
   * Gets the designated column's name.
   * @param columnIndex the first column is 0, the second is 1, ...
   * @return the column name or null if not available
   */
  public String getColumnName(int columnIndex)
  {
    String columnName = null;
    if (columnNames != null)  //if column names exist
    {
      columnName = columnNames[columnIndex];
    }
    return columnName;
  }

  /**
   * Gets a list of the column objects.
   * The list contains Map objects with the column name for the key.
   *
   * @return list of column objects.
   */
  public List getColumnObjects()
  {
    return columnObjectsList;
  }

  /**
   * Gets the map for the specified index.
   * @param index database element index
   * @return the map for the specified index.
   */
  public Map getMap(int index)
  {
    Map map = null;
    if (columnObjectsList != null)
    {
      map = (Map)columnObjectsList.get(index);
    }
    return map;
  }

  /**
   * Gets result information from the query for the specified map element.
   * @param map map element.
   *
   * @return information string
   */
  protected String getResultInformation(Map map)
  {
    String text = "";
    text += map.get(getColumnName(0)).toString();
    for (int i = 1;
         i <  getColumnCount();
         i++)
    {
      text += " " + map.get(getColumnName(i)).toString();
    }
    text += "\n";
    return text;
  }

  /**
   * Gets result information from the query.
   *
   * @return information string
   */
  public String getResultInformation()
  {
    String text = "";

    if (columnObjectsList != null && getColumnCount() > 0)
    {
      Iterator it = columnObjectsList.iterator();

      while (it.hasNext())
      {
        Map map = (Map)it.next();
        text += getResultInformation(map);
      }
    }
    return text;
  }

  /**
   * Gets the size of the query list.
   * @return the size of the query list.
   */
  public int getSize()
  {
    int size = 0;
    if (columnObjectsList != null)
    {
      size = columnObjectsList.size();
    }
    return size;
  }

  /**
   * Gets the sql statement
   * @return the sql statement
   */
  public String getSql()
  {
    return sql;
  }

  /**
   * Gets a string representation of a database element.
   * @param index database element index
   * @return a string or null if not available.
   */
  public String getString(int index)
  {
    String info = null;
    Map map = getMap(index);

    if (map != null)
    {
      info = getResultInformation(map);
    }
    return info;
  }
}