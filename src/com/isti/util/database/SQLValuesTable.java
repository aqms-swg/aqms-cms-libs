//SQLValuesTable.java:  Contains methods for handling SQL values.
//
// 11/27/2007 -- [KF]  Initial version.
//

package com.isti.util.database;

import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.util.List;
import com.isti.util.BaseValueTableModel;
import com.isti.util.ValueTableModel;

public class SQLValuesTable
    extends BaseValueTableModel implements ValueTableModel
{
  /** The column map key index or -1 if none. */
  private int columnMapKeyIndex = -1;
  /** The column map key name or null if none. */
  private String columnMapKeyName = null;

  /**
   * Reads all of the rows.
   * This will attempt to load all of the rows into memory.
   * Any existing error messages are cleared.
   * To clear existing colums and rows the 'clearLists' method should be called.
   * To clear existing rows the 'clearRows' method should be called.
   * @param rs the result set.
   * @return the number of rows read or 0 if error.
   * @throws SQLException if error.
   * @see clearLists
   * @see clearRows
   * @see getErrorMessages
   */
  public int readAll(ResultSet rs) throws SQLException
  {
    int count = 0;
    clearErrorMessageString(); //clear previous error message
    //list of string values
    final List valueList = createList();
    if (!columnsExist()) //if columns not already set up
    {
      createLists(); //create new lists
      if (!setColumnNames(rs.getMetaData(), valueList))
      {
        return 0; //indicate that there is an error
      }
    }
    final int numColumns = getColumnCount();
    while (rs.next())
    {
      readRow(rs, valueList, numColumns);
      ++count;
    }
    if (getErrorMessageFlag()) //if any errors
    {
      return 0; //indicate that there is an error
    }
    return count;
  }

  /**
   * Reads the current row.
   * This will attempt to load the row into memory.
   * @param rs the result set.
   * @throws SQLException if error.
   * @see getErrorMessages
   */
  public void readRow(ResultSet rs) throws SQLException
  {
    readRow(rs, createList(), getColumnCount());
  }

  /**
   * Reads the current row.
   * This will attempt to load the row into memory.
   * @param rs the result set.
   * @param valueList the value list.
   * @param numColumns the number of columns.
   * @throws SQLException if error.
   * @see getErrorMessages
   */
  protected void readRow(ResultSet rs, List valueList, int numColumns) throws
      SQLException
  {
    valueList.clear();
    //read the next list of values
    for (int columnIndex = 0; columnIndex < numColumns; columnIndex++)
    {
      final Object valueObj = getValue(rs, columnIndex);
      valueList.add(valueObj);
    }
    addRow(valueList);
  }

  /**
   * Gets the column map key.
   * @param columnList the list of columns for the row.
   * @return the column map key or null if none.
   */
  protected Object getColumnMapKey(List columnList)
  {
    if (columnMapKeyIndex < 0 && columnMapKeyName != null)
    {
      columnMapKeyIndex = getColumnIndex(columnMapKeyName);
    }
    if (columnMapKeyIndex >= 0)
    {
      final Object columnMapKey = columnList.get(columnMapKeyIndex);
      if (columnMapKey == null)
      {
        return null;
      }
      final String columnMapKeyText = columnMapKey.toString();
      if (columnMapKeyText.length() == 0)
      {
        return null;
      }
      return columnMapKeyText;
    }
    return super.getColumnMapKey(columnList);
  }

  /**
   * Gets the value.
   * @param rs the result set.
   * @param columnIndex the column index.
   * @return the value.
   * @throws SQLException if error.
   */
  protected Object getValue(ResultSet rs, int columnIndex) throws SQLException
  {
    //the first column is 1, the second is 2, ...
    final int dbColumnIndex = columnIndex + 1;
    final Object valueObj = rs.getObject(dbColumnIndex);
    return valueObj;
  }

  /**
   * Resets the result set to before the first row.
   * @param rs the result set.
   * @throws SQLException if error.
   */
  public void reset(ResultSet rs) throws SQLException
  {
    if (!rs.isBeforeFirst())
    {
      rs.beforeFirst();
    }
  }

  /**
   * Sets the column names.
   * @param rsmd the result set meta data.
   * @return true if the column names list was set, false if error.
   * @throws SQLException if error.
   * @see getErrorMessages
   */
  public boolean setColumnNames(ResultSetMetaData rsmd) throws SQLException
  {
    return setColumnNames(rsmd, createList());
  }

  /**
   * Sets the column names.
   * @param rsmd the result set meta data.
   * @param valueList the value list.
   * @return true if the column names list was set, false if error.
   * @throws SQLException if error.
   */
  public boolean setColumnNames(ResultSetMetaData rsmd, List valueList) throws
      SQLException
  {
    final int numColumns = rsmd.getColumnCount();
    for (int columnIndex = 0; columnIndex < numColumns; columnIndex++)
    {
      //the first column is 1, the second is 2, ...
      final int dbColumnIndex = columnIndex + 1;
      final String columnName = rsmd.getColumnName(dbColumnIndex);
      valueList.add(columnName);
      boolean readOnlyFlag = false;
      if (rsmd.isAutoIncrement(dbColumnIndex)) //if auto-increment column
      {
        readOnlyFlag = true;
        //if column map key not yet defined
        if (columnMapKeyIndex < 0 && columnMapKeyName == null)
        {
          setIdColumn(columnIndex);
        }
      }
      else if (rsmd.isReadOnly(dbColumnIndex))
      {
        readOnlyFlag = true;
      }
      if (readOnlyFlag)
      {
        addReadOnlyColumn(columnName);
      }
    }
    return setColumnNames(valueList);
  }

  /**
   * Sets the ID column.
   * @param columnIndex the column index.
   */
  public void setIdColumn(int columnIndex)
  {
    columnMapKeyIndex = columnIndex;
  }

  /**
   * Sets the ID column.
   * @param columnName the column name.
   */
  public void setIdColumn(String columnName)
  {
    //clear the index and determine later by the name
    columnMapKeyIndex = -1;
    columnMapKeyName = columnName;
    addReadOnlyColumn(columnName);
  }

  /**
   * Writes all the rows.
   * @param rs the result set.
   * @throws SQLException if error.
   */
  public void writeAll(ResultSet rs) throws SQLException
  {
    final int numColumns = getColumnCount();
    boolean insertRowFlag = false;
    for (int rowIndex = 0; rowIndex < getRowCount(); rowIndex++)
    {
      if (!insertRowFlag && !rs.next())
      {
        insertRowFlag = true;
        rs.moveToInsertRow();
      }
      writeRow(rs, numColumns, rowIndex, insertRowFlag);
    }
  }

  /**
   * Writes the current row.
   * @param rs the result set.
   * @param insertRowFlag true to insert the row, false to update the row.
   * @throws SQLException if error.
   */
  public void writeRow(ResultSet rs, boolean insertRowFlag) throws SQLException
  {
    writeRow(rs, getColumnCount(), rs.getRow(), insertRowFlag);
  }

  /**
   * Writes the current row.
   * @param rs the result set.
   * @param numColumns the number of columns.
   * @param rowIndex the row index.
   * @param insertRowFlag true to insert the row, false to update the row.
   * @throws SQLException if error.
   */
  protected void writeRow(
      ResultSet rs, int numColumns, int rowIndex, boolean insertRowFlag) throws
      SQLException
  {
    for (int columnIndex = 0; columnIndex < numColumns; columnIndex++)
    {
      final Object valueObj = getValueAt(rowIndex, columnIndex);
      updateValue(rs, valueObj, columnIndex);
    }
    updateRow(rs, insertRowFlag);
  }

  /**
   * Update the row.
   * @param rs the result set.
   * @param insertRowFlag true to insert the row, false to update the row.
   * @throws SQLException if error.
   */
  protected void updateRow(
      ResultSet rs, boolean insertRowFlag) throws SQLException
  {
    if (insertRowFlag)
    {
      rs.insertRow();
    }
    else
    {
      rs.updateRow();
    }
  }

  /**
   * Update the value.
   * @param rs the result set.
   * @param valueObj the value object.
   * @param columnIndex the column index.
   * @throws SQLException if error.
   */
  protected void updateValue(
      ResultSet rs, Object valueObj, int columnIndex) throws SQLException
  {
    //the first column is 1, the second is 2, ...
    final int dbColumnIndex = columnIndex + 1;
    if (isNull(valueObj))
    {
      rs.updateNull(dbColumnIndex);
    }
    else
    {
      rs.updateObject(dbColumnIndex, valueObj);
    }
  }

  /**
   * Update the value.
   * @param rs the result set.
   * @param valueObj the value object.
   * @param columnName the column name.
   * @throws SQLException if error.
   */
  protected void updateValue(
      ResultSet rs, Object valueObj, String columnName) throws SQLException
  {
    if (isNull(valueObj))
    {
      rs.updateNull(columnName);
    }
    else
    {
      rs.updateObject(columnName, valueObj);
    }
  }
}
