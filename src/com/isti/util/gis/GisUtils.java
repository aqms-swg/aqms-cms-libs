//GisUtils.java:  Contains various static utility methods and
//                constants for GIS.
//
//  6/11/2004 -- [ET]  Initial version.
//  8/24/2005 -- [ET]  Added 'sphereDistNew()' method.
//  7/19/2006 -- [ET]  Added 'kmToDegrees()' and 'degreesToKm()' methods.
//

package com.isti.util.gis;

/**
 * Class GisUtils contains various static utility methods and
 * constants for GIS.
 */
public class GisUtils
{
    /** Feet to meters multiplier. */
  public static final double FEET_TO_METER = 0.3048;
    /** Meters to feet multiplier. */
  public static final double METER_TO_FEET = 1.0 / FEET_TO_METER;
    /** Miles to meters multiplier. */
  public static final double MILE_TO_METER = 5280 * FEET_TO_METER;
    /** Meters to miles multiplier. */
  public static final double METER_TO_MILE = 1.0 / MILE_TO_METER;
    /** Miles to kilometers multiplier. */
  public static final double MILE_TO_KM = MILE_TO_METER / 1000;
    /** Kilometers to miles multiplier. */
  public static final double KM_TO_MILE = 1000 / MILE_TO_METER;
    /** Arc-degree to radian multiplier (pi / 180). */
  public static final double DEG_TO_RAD = Math.PI / 180.0;
    /** Radian to arc-degree multiplier (180 / pi). */
  public static final double RAD_TO_DEG = 180.0 / Math.PI;
    /** GRS80 avg. earth radius, meters (6371008.7714). */
  public static final double GEO_RADIUS  = 6371008.7714;
    /** Earth radius in meters at equator (6378137.0). */
  public static final double EARTH_EQUATORIAL_RADIUS_METERS = 6378137.0;
    /** Earth radius in kilometers at equator (6378.1370). */
  public static final double EARTH_EQUATORIAL_RADIUS_KM =
                                    EARTH_EQUATORIAL_RADIUS_METERS / 1000.0;

    //private constructor so that no object instances may be created
    // (static access only)
  private GisUtils()
  {
  }

  /**
   * Calculates the great circle distance along the surface
   * of a sphere from point P1 (lat,lon) to point P2 (lat,lon)
   * on the surface.  Uses sin(c/2) formula, which has
   * high precision for distances from 0 to 180 degrees.
   * Formula 5-3a from J.P.Snyder(1987), USGS Prof.Paper 1395, p30.
   * @param rlat1  point 1 latitude in radians.
   * @param rlon1  point 1 longitude in radians.
   * @param rlat2  point 2 latitude in radians.
   * @param rlon2  point 2 longitude in radians.
   * @param dRadius  radius of the sphere.
   * @return The distance between the points, in meters.
   */
  public static double sphereDist(double rlat1, double rlon1,
                                  double rlat2, double rlon2, double dRadius)
  {
                                            //sin[(lat2-lat1)/2]:
    final double sinLatDif = Math.sin((rlat2 - rlat1)*0.50);
                                            //sin[(lon2-lon1)/2]:
    final double sinLonDif = Math.sin((rlon2 - rlon1)*0.50);
    final double sinsquareCby2 =  sinLatDif * sinLatDif +
                  Math.cos(rlat1) * Math.cos(rlat2) * sinLonDif * sinLonDif;
    return (dRadius * 2.0 * Math.asin(Math.sqrt(sinsquareCby2)));
  }

  /**
   * Calculates the great circle distance along the surface
   * of the earth from point P1 (lat,lon) to point P2 (lat,lon)
   * on the surface.  Uses sin(c/2) formula, which has
   * high precision for distances from 0 to 180 degrees.
   * Formula 5-3a from J.P.Snyder(1987), USGS Prof.Paper 1395, p30.
   * @param dlat1  point 1 latitude in degrees.
   * @param dlon1  point 1 longitude in degrees.
   * @param dlat2  point 2 latitude in degrees.
   * @param dlon2  point 2 longitude in degrees.
   * @return The distance between the points, in meters.
   */
  public static double sphereDist(double dlat1, double dlon1,
                                  double dlat2, double dlon2)
  {
                   //convert degrees to radians:
    return sphereDist(dlat1*DEG_TO_RAD,dlon1*DEG_TO_RAD,dlat2*DEG_TO_RAD,
                                               dlon2*DEG_TO_RAD,GEO_RADIUS);
  }

  /**
   * Calculates azimuth east of north on the surface of a sphere
   * from point P1(lat,lon) to point P2(lat,lon) on the sphere.
   * Uses standard arcTangent formula, with adjustment for proper
   * quadrant.
   * Formula 5-4b from J.P.Snyder(1987), USGS Prof.Paper 1395, p30.
   * @param rlat1 point 1 latitude in radians.
   * @param rlon1 point 1 longitude in radians.
   * @param rlat2 point 2 latitude in radians.
   * @param rlon2 point 2 longitude in radians.
   * @return Azimuth east of north in radians.
   */
  public static double sphereAzim(double rlat1, double rlon1,
                                                 double rlat2, double rlon2)
  {
    final double dLonDif = rlon2 - rlon1;                  // LonP2 - LonP1
    final double cosLatP2 = Math.cos(rlat2);               // cos(LatP2)
    final double a = cosLatP2 * Math.sin(dLonDif);         // atan2 numerator
    final double b = Math.cos(rlat1) * Math.sin(rlat2) -   // atan2 denom
                             cosLatP2 * Math.sin(rlat1) * Math.cos(dLonDif);
    return ptgAtan2(a,b);
  }

  /**
   * Calculates azimuth east of north on the surface of a sphere
   * from point P1(lat,lon) to point P2(lat,lon) on the sphere.
   * Uses standard arcTangent formula, with adjustment for proper
   * quadrant.  Uses degrees instead of radians.
   * Formula 5-4b from J.P.Snyder(1987), USGS Prof.Paper 1395, p30.
   * @param dlat1 point 1 latitude in degrees.
   * @param dlon1 point 1 longitude in degrees.
   * @param dlat2 point 2 latitude in degrees.
   * @param dlon2 point 2 longitude in degrees.
   * @return Azimuth east of north in degrees.
   */
  public static double sphereAzimDeg(double dlat1, double dlon1,
                                                 double dlat2, double dlon2)
  {
    return sphereAzim(dlat1*DEG_TO_RAD,dlon1*DEG_TO_RAD,
                            dlat2*DEG_TO_RAD,dlon2*DEG_TO_RAD) * RAD_TO_DEG;
  }

  /**
   * Atan2 function, with quadrant checks and traps for errors
   * @param a  numerator of arctangent function
   * @param b  denominator of arctangent function
   * @return  arctangent
   */
  public static double ptgAtan2(double a, double b)
  {
    if(a == 0.0 && b == 0.0)
      return 0.0;
    return Math.atan2(a,b);
  }

  /**
   * Calculates the great circle distance along the surface
   * of a sphere from point P1 (lat,lon) to point P2 (lat,lon)
   * on the surface.  Uses formula provided by Bob Simpson (USGS).
   * @param rlat0 point 0 latitude in radians.
   * @param rlon0 point 0 longitude in radians.
   * @param rlat1 point 1 latitude in radians.
   * @param rlon1 point 1 longitude in radians.
   * @param dRadius radius of the sphere.
   * @return The distance between the points, in meters.
   */
  public static double sphereDistNew(double rlat0, double rlon0,
                                 double rlat1, double rlon1, double dRadius)
  {
//  Perl code from Bob Simpson:
//       my ( $cslat0, $snlat0, $cslon0, $snlon0 ) =
//             ( cos($lat0), sin($lat0), cos($lon0), sin($lon0) );
//       my ( $cslat1, $snlat1, $cslon1, $snlon1 ) =
//             ( cos($lat1), sin($lat1), cos($lon1), sin($lon1) );
//       @v0 = ( $cslat0 * $cslon0, $cslat0 * $snlon0, $snlat0 );
//       @v1 = ( $cslat1 * $cslon1, $cslat1 * $snlon1, $snlat1 );
//       my $cosangle = vdot(@v0,@v1);
//       my $dist = acos($cosangle) * $earthradius;
//  sub vdot { return $_[0]*$_[3] + $_[1]*$_[4] + $_[2]*$_[5] ; }

    final double cosAngle = Math.cos(rlat0) * Math.cos(rlon0) *
                                         Math.cos(rlat1) * Math.cos(rlon1) +
                                         Math.cos(rlat0) * Math.sin(rlon0) *
                                         Math.cos(rlat1) * Math.sin(rlon1) +
                                            Math.sin(rlat0)*Math.sin(rlat1);
    return Math.acos(cosAngle) * dRadius;
  }

  /**
   * Calculates the great circle distance along the surface
   * of a sphere from point P1 (lat,lon) to point P2 (lat,lon)
   * on the surface.  Uses formula provided by Bob Simpson (USGS).
   * @param dlat0 point 0 latitude in degrees.
   * @param dlon0 point 0 longitude in degrees.
   * @param dlat1 point 1 latitude in degrees.
   * @param dlon1 point 1 longitude in degrees.
   * @return The distance between the points, in meters.
   */
  public static double sphereDistNew(double dlat0, double dlon0,
                                     double dlat1, double dlon1)
  {
                   //convert degrees to radians:
    return sphereDistNew(dlat0*DEG_TO_RAD,dlon0*DEG_TO_RAD,dlat1*DEG_TO_RAD,
                                               dlon1*DEG_TO_RAD,GEO_RADIUS);
  }

  /**
   * Converts a distance in kilometers to degrees.
   * @param kmVal kilometers value to use.
   * @return The distance value in degrees.
   */
  public static double kmToDegrees(double kmVal)
  {
    return kmVal / EARTH_EQUATORIAL_RADIUS_KM * RAD_TO_DEG;
  }

  /**
   * Converts a distance in degrees to kilometers.
   * @param degVal degrees value to use.
   * @return The distance value in kilometers.
   */
  public static double degreesToKm(double degVal)
  {
    return degVal * DEG_TO_RAD * EARTH_EQUATORIAL_RADIUS_KM;
  }


/*
  //Test program to compare 'sphereDist()' to 'sphereDistNew()' results:
  public static void main(String [] args)
  {
    final double DEG2RAD = 0.17453293e-1;
    final double EARTH_RADIUS = 6371000.0;

//    java.util.Random randomObj = new java.util.Random();
    final Object [] locsArr = new Object []
        {
          Double.valueOf(14.092), Double.valueOf(-91.657), Double.valueOf(14.8330),
                            Double.valueOf(-91.5000), "Quezaltenango, Guatemala",
          Double.valueOf(14.092), Double.valueOf(-91.657), Double.valueOf(14.6330),
                            Double.valueOf(-90.5170), "GUATEMALA CITY, Guatemala"
        };
    double dlat0,dlon0,dlat1,dlon1,result1,result2,diff,distVal,azimVal;
    double minDiff = 99999.0, maxDiff = -1.0;
//    int mileVal;
    LocationDistanceInformation distInfoObj;
//    for(int cnt=0; cnt<5000; ++cnt)
    for(int cnt=0; cnt<2; ++cnt)
    {
//      dlat0 = randomObj.nextDouble()*180.0 - 90.0;
//      dlon0 = randomObj.nextDouble()*360.0 - 180.0;
//      dlat1 = randomObj.nextDouble()*180.0 - 90.0;
//      dlon1 = randomObj.nextDouble()*360.0 - 180.0;
      dlat1 = ((Double)(locsArr[cnt*5])).doubleValue();
      dlon1 = ((Double)(locsArr[cnt*5 + 1])).doubleValue();
      dlat0 = ((Double)(locsArr[cnt*5 + 2])).doubleValue();
      dlon0 = ((Double)(locsArr[cnt*5 + 3])).doubleValue();
      try
      {
        result1 = sphereDist(dlat0,dlon0,dlat1,dlon1);
        result2 = sphereDistNew(dlat0*DEG2RAD,dlon0*DEG2RAD,dlat1*DEG2RAD,
                                                dlon1*DEG2RAD,EARTH_RADIUS);
        diff = Math.abs(result2 - result1);
        if(diff < minDiff)
          minDiff = diff;
        if(diff > maxDiff)
          maxDiff = diff;
        System.out.println("(" + dlat0 + "," + dlon0 + ") -> (" + dlat1 +
                         "," + dlon1 + ") = " + result1 + "m | " + result2 +
                                                    "m diff=" + diff + "m");

        azimVal = sphereAzimDeg(dlat0,dlon0,dlat1,dlon1);
        while(azimVal < 0.0)
          azimVal += 360.0;
        while(azimVal >= 360.0)
          azimVal -= 360.0;

        distInfoObj = new LocationDistanceInformation(result1,azimVal,
                                              ((String)(locsArr[cnt*5+4])));
        distVal = distInfoObj.getDistance();
//        mileVal = (int)(distVal + 0.5);
//        System.out.println("1: " + mileVal + " mile" +
//              ((mileVal!=1)?"s":"") + " (" + (int)(distVal*MILE_TO_KM+0.5) +
//                                     " km) " + distInfoObj.getAzimString() +
//                                   " (" + (int)(distInfoObj.getAzim()+0.5) +
//                                       " deg) of " + distInfoObj.getName());
        System.out.println("1: " +
            (((double)((int)(distVal*100.0+0.5)))/100.0) + " miles" + " (" +
                   (((double)((int)(distVal*MILE_TO_KM*100.0+0.5)))/100.0) +
                                     " km) " + distInfoObj.getAzimString() +
                                   " (" + (int)(distInfoObj.getAzim()+0.5) +
                                       " deg) of " + distInfoObj.getName());

        distInfoObj = new LocationDistanceInformation(result2,azimVal,
                                              ((String)(locsArr[cnt*5+4])));
        distVal = distInfoObj.getDistance();
//        mileVal = (int)(distVal + 0.5);
//        System.out.println("2: " + mileVal + " mile" +
//              ((mileVal!=1)?"s":"") + " (" + (int)(distVal*MILE_TO_KM+0.5) +
//                                     " km) " + distInfoObj.getAzimString() +
//                                   " (" + (int)(distInfoObj.getAzim()+0.5) +
//                                       " deg) of " + distInfoObj.getName());
        System.out.println("2: " +
            (((double)((int)(distVal*100.0+0.5)))/100.0) + " miles" + " (" +
                   (((double)((int)(distVal*MILE_TO_KM*100.0+0.5)))/100.0) +
                                     " km) " + distInfoObj.getAzimString() +
                                   " (" + (int)(distInfoObj.getAzim()+0.5) +
                                       " deg) of " + distInfoObj.getName());

        System.out.println();
      }
      catch(Exception ex)
      {
        System.err.println("(" + dlat0 + "," + dlon0 + ") -> (" + dlat1 +
                                                 "," + dlon1 + "):  " + ex);
        ex.printStackTrace();
      }
    }
//    System.out.println("minDiff = " + minDiff + ", maxDiff = " + maxDiff);
  }
*/

/*
  //Test program to compare 'sphereDist()' to 'sphereDistNew()' results:
  public static void main(String [] args)
  {
    final double DEG2RAD = 0.17453293e-1;
    final double EARTH_RADIUS = 6371000.0;

    java.util.Random randomObj = new java.util.Random();
//    final Object [] locsArr = new Object []
//        {
//          Double.valueOf(14.092), Double.valueOf(-91.657), Double.valueOf(14.8330),
//                            Double.valueOf(-91.5000), "Quezaltenango, Guatemala",
//          Double.valueOf(14.092), Double.valueOf(-91.657), Double.valueOf(14.6330),
//                            Double.valueOf(-90.5170), "GUATEMALA CITY, Guatemala"
//        };
    double dlat0,dlon0,dlat1,dlon1,result1,result2,diff;
    double minDiff = 99999.0, maxDiff = -1.0;
//    int mileVal;
//    LocationDistanceInformation distInfoObj;
    for(int cnt=0; cnt<5000; ++cnt)
//    for(int cnt=0; cnt<2; ++cnt)
    {
      dlat0 = randomObj.nextDouble()*180.0 - 90.0;
      dlon0 = randomObj.nextDouble()*360.0 - 180.0;
      dlat1 = randomObj.nextDouble()*180.0 - 90.0;
      dlon1 = randomObj.nextDouble()*360.0 - 180.0;
//      dlat1 = ((Double)(locsArr[cnt*5])).doubleValue();
//      dlon1 = ((Double)(locsArr[cnt*5 + 1])).doubleValue();
//      dlat0 = ((Double)(locsArr[cnt*5 + 2])).doubleValue();
//      dlon0 = ((Double)(locsArr[cnt*5 + 3])).doubleValue();
      try
      {
        result1 = sphereDist(dlat0,dlon0,dlat1,dlon1);
        result2 = sphereDistNew(dlat0*DEG2RAD,dlon0*DEG2RAD,dlat1*DEG2RAD,
                                                dlon1*DEG2RAD,EARTH_RADIUS);
        diff = Math.abs(result2 - result1);
        if(diff < minDiff)
          minDiff = diff;
        if(diff > maxDiff)
          maxDiff = diff;
        System.out.println("(" + dlat0 + "," + dlon0 + ") -> (" + dlat1 +
                         "," + dlon1 + ") = " + result1 + "m | " + result2 +
                                                    "m diff=" + diff + "m");
      }
      catch(Exception ex)
      {
        System.err.println("(" + dlat0 + "," + dlon0 + ") -> (" + dlat1 +
                                                 "," + dlon1 + "):  " + ex);
        ex.printStackTrace();
      }
    }
    System.out.println("minDiff = " + minDiff + "m, maxDiff = " +
                                                             maxDiff + "m");
  }
*/
}
