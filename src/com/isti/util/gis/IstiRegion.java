//IstiRegion.java:  Region utility class.
//
//  3/30/2004 -- [KF]  Initial version.
//   4/5/2004 -- [KF]  Added optional magnitude name.
//   4/9/2004 -- [KF]  Change default for strings from KM to miles,
//                     only use 3 fraction digits.
//  4/12/2004 -- [KF]  Added NAME_BEGIN_CHAR and NAME_END_CHAR.
//  4/19/2004 -- [KF]  Added 'radiusToString' method.
//  4/21/2004 -- [KF]  Make sure there are 6 elements in the lat/lon
//                     array.
//   5/6/2004 -- [KF]  Add string constructor and 'toString' to
//                     'LatLonPoint'.
//  6/10/2004 -- [ET]  Moved to 'gis' package; moved some methods and
//                     contants to 'GisUtils' class.
//  6/15/2004 -- [ET]  Fixed 'stringToCoord()' method to prevent it from
//                     throwing exceptions in response to unexpected data.
// 12/20/2005 -- [KF]  Changed 'floatNumberToString()' method so that it works
//                     with other locales.
//   3/7/2006 -- [KF]  Added more region options.
//   3/9/2006 -- [KF]  Changed to display up to 3 fraction digits.
//  4/19/2006 -- [ET]  Made default magnitude 3.0 and default maximum-age
//                     4 hours; added extra characters to validator for
//                     'OPID_NAME' option item; added optional parameter
//                     'allowGenericFlag' to method 'parse()'; added
//                     'extractRegionName()' and 'replaceRegionName()'
//                     methods.
//  5/19/2006 -- [ET]  Modified to handle region polygons that bridge the
//                     +/-180 degree longitude line (in 'LatLonPolygon').
//

package com.isti.util.gis;

import diva.util.java2d.Polygon2D;
import com.isti.util.UtilFns;
import com.isti.util.IstiNamedValueInterface;
import com.isti.util.PropItem;
import com.isti.util.propertyeditor.IstiNamedValueComponentPanel;
import com.isti.util.gui.FilteredJTextField;
import com.isti.util.CfgPropValidator;

public class IstiRegion
{
  /**
   * The default conversion factor that will be used for strings.
   * The default is miles.
   */
  public final static double defaultConversionFactor =
                                                     GisUtils.METER_TO_MILE;

  /** The character that comes before the name. */
  public final static String NAME_BEGIN_CHAR = "\"";

  /** The character that comes after the name. */
  public final static String NAME_END_CHAR = NAME_BEGIN_CHAR;

  /** The character that comes before the coordinate. */
  public final static String COORD_BEGIN_CHAR = "(";

  /** The character that comes after the coordinate. */
  public final static String COORD_END_CHAR = ")";

  /** The character that separates coordinates. */
  public final static String COORD_SEP_CHAR = ",";

  /** The character that separates regions. */
  public final static String REGION_SEP_CHAR = ";";

  /** The character that comes before an option. */
  public final static String OPTION_BEGIN_CHAR = "[";

  /** The character that comes after an option. */
  public final static String OPTION_END_CHAR = "]";

    /** String of special characters for 'insertQuoteChars()' method. */
  public static final String DEF_SPECIAL_CHARS_STR =
      UtilFns.DEF_SPECIAL_CHARS_STR + PropItem.sepCh +
      NAME_BEGIN_CHAR + COORD_BEGIN_CHAR + COORD_END_CHAR + COORD_SEP_CHAR +
      REGION_SEP_CHAR + OPTION_BEGIN_CHAR + OPTION_END_CHAR;

  /** The string representation of the region. */
  protected final String regionTextValueStr;

  /** The option ID text for the name. */
  public final static String OPID_NAME = "name";

  /** The option ID text for the minimum magnitude. */
  public final static String OPID_MAG = null;

  /** The option ID text of the minimum depth. */
  public final static String OPID_MINDEPTH = "mindepth";

  /** The option ID text of the maximum depth. */
  public final static String OPID_MAXDEPTH = "maxdepth";

  /** The option ID text of the maximum age. */
  public final static String OPID_MAXAGE = "maxage";

  /** The option ID text of the verified flag. */
  public final static String OPID_VERIFIED = "verified";

  /** The option ID text of the audible flag. */
  public final static String OPID_AUDIBLE = "audible";

  /** The option ID text of the visual flag. */
  public final static String OPID_VISUAL = "visual";

  /** The global alarm option group selection text. */
  public final static String GLOBAL_ALARM_OPTION_GROUP = "Global";

  /** The default magnitude for the region. */
  private final static double DEF_MAGNITUDE_VAL = 3.0;

  /** The default maximum age for the region, in hours. */
  private final static int DEF_MAXAGE_VAL = 4;

  /** The magnitude validator. */
  public static final CfgPropValidator magValidator =
      new CfgPropValidator(Double.valueOf(0.0),Double.valueOf(9.9));
  /** The depth validator. */
  public static final CfgPropValidator depthValidator =
      new CfgPropValidator(Double.valueOf(0.0),Double.valueOf(99999.0));
  /** The age validator. */
  public static final CfgPropValidator ageValidator =
      new CfgPropValidator(Integer.valueOf(0),Integer.valueOf(999999));

  /** The default options. */
  private static final IstiNamedValueInterface[] _defaultOptions =
  {
    /** The region name option (must remain at index 0 for 'nameVal' below. */
    new PropItem(OPID_NAME,UtilFns.EMPTY_STRING,
                       "Name of region").setValidator(new CfgPropValidator(
                   FilteredJTextField.ALPHANUMERIC_CHARS + " _-./'@#$%^&")),
    new PropItem(OPID_MAG,Double.valueOf(DEF_MAGNITUDE_VAL),
                       "Minimum magnitude for alarm").setValidator(magValidator),
    new PropItem(OPID_MINDEPTH,Double.valueOf(0),
                       "Minimum depth for alarm (km)").setValidator(depthValidator),
    new PropItem(OPID_MAXDEPTH,Double.valueOf(0),
                       "Maximum depth for alarm (km)").setValidator(depthValidator),
    new PropItem(OPID_MAXAGE,Integer.valueOf(DEF_MAXAGE_VAL),
                       "Maximum event age (hours)").setValidator(ageValidator),
    new PropItem(OPID_VERIFIED,Boolean.FALSE,
                       "Only verified events trigger alarm"),
    new PropItem(OPID_AUDIBLE,Boolean.TRUE,
          "Enable audible alarms for region").setGroupSelObj(GLOBAL_ALARM_OPTION_GROUP),
    new PropItem(OPID_VISUAL,Boolean.TRUE,
          "Enable visual alarms for region").setGroupSelObj(GLOBAL_ALARM_OPTION_GROUP),
  };
  /**
   * Gets the default options.
   * @return the default options.
   */
  public static IstiNamedValueInterface[] getDefaultOptions()
  {
    return _defaultOptions;
  }
  private static IstiNamedValueInterface[] createOptions()
  {
    final IstiNamedValueInterface[] defaultOptions = getDefaultOptions();
    final IstiNamedValueInterface[] options =
        new IstiNamedValueInterface[defaultOptions.length];
    for (int i = 0; i < defaultOptions.length; i++)
    {
      options[i] = (IstiNamedValueInterface)defaultOptions[i].clone();
    }
    return options;
  }
  private final IstiNamedValueInterface[] options = createOptions();
  private IstiNamedValueInterface nameVal = options[0];

  /**
   * Creates the options component panel.
   * @return the options component panel.
   */
  public static IstiNamedValueComponentPanel createOptionsComponentPanel()
  {
    //options component panel
    final IstiNamedValueComponentPanel optionsComponentPanel =
        new IstiNamedValueComponentPanel();
    optionsComponentPanel.addProperties(getDefaultOptions());
    return optionsComponentPanel;
  }

  /**
   * Circle region.
   */
  public static class Circle extends IstiRegion
  {
    //the mid-point of the circle
    protected final LatLonPoint midPt;
    //the radius of the circle in meters.
    protected final double radius;

    /**
     * Creates a region defined by the circle
     * @param lat the latitude of the point.
     * @param lon the longitude of the point.
     * @param radius the radius of the circle in meters.
     * @return a string representation of the region specifed by the coordinates.
     */
    public Circle(double lat, double lon, double radius)
    {
      super(circleToString(lat, lon, radius));
      this.midPt = new LatLonPoint(lat, lon);
      this.radius = radius;
    }

    /**
     * Creates a copy of a region.
     * @return a copy of the region.
     */
    public Object clone()
    {
      return new Circle(this).copyOptions(this);
    }

    /**
     * Determines if the specified point is inside of the region.
     * @param lat the latitude of the point.
     * @param lon the longitude of the point.
     * @return true if the point is inside the region, false otherwise.
     */
    public boolean contains(double lat, double lon)
    {
      return GisUtils.sphereDist(
                        midPt.getLat(), midPt.getLon(), lat, lon) <= radius;
    }

    /**
     * Gets the latitude point.
     * @return the latitude point.
     */
    public double getLat()
    {
      return midPt.getLat();
    }

    /**
     * Gets the longitude point.
     * @return the longitude point.
     */
    public double getLon()
    {
      return midPt.getLon();
    }

    /**
     * Gets the radius in meters.
     * @return the radius.
     */
    public double getRadius()
    {
      return radius;
    }

    /**
     * Constructs a region.
     * @param circle the circle to copy.
     */
    protected Circle(Circle circle)
    {
      this(circle.midPt.getLat(), circle.midPt.getLon(), circle.radius);
    }

    /**
     * Constructs a region.
     * @param str string representation of the region.
     * @throws InstantiationException if the class cannot be instantiated.
     */
    protected Circle(String str) throws InstantiationException
    {
      super(str);
      this.midPt = stringToCoord(str);
      this.radius = stringToRadius(str);
      if (this.midPt == null || this.radius <= 0.0)
      {
        throw new InstantiationException(
            "Invalid string representation: " + str);
      }
    }
  }


  /**
   * Latitude/longitude point.
   */
  public static class LatLonPoint
  {
    private final double lat, lon;

    /**
     * Creats a latitude/longitude point.
     * @param lat the latitude point.
     * @param lon the longitude point.
     */
    public LatLonPoint(double lat, double lon)
    {
      this.lat = lat;
      this.lon = lon;
    }

    /**
     * Creats a latitude/longitude point.
     * @param s the string representation of the latitude/longitude point.
     */
    public LatLonPoint(String s)
    {
      final LatLonPoint pt = stringToCoord(s);
      if (pt != null)
      {
        lat = pt.lat;
        lon = pt.lon;
      }
      else
      {
        lat = 0.0;
        lon = 0.0;
      }
    }

    /**
     * Gets the latitude point.
     * @return the latitude point.
     */
    public double getLat()
    {
      return lat;
    }

    /**
     * Gets the longitude point.
     * @return the longitude point.
     */
    public double getLon()
    {
      return lon;
    }

    /**
     * Get the string representation of the latitude/longitude point.
     * @return the string representation of the latitude/longitude point.
     */
    public String toString()
    {
      return coordToString(lat, lon);
    }
  }


  /**
   * Latitude/longitude polygon.
   */
  public static class LatLonPolygon
  {
    //the polygon
    private final Polygon2D.Double poly;
    private final boolean fixedLonFlag;

    /**
     * Converts an array of lat/lon points to x/y points.
     * @param llPoints the lat/lon points.
     * @return the x/y points.
     */
    public static double[] latLonToXY(double[] llPoints)
    {
      final int coordsLength = llPoints.length;
      final double[] xyPoints = new double[coordsLength];

      for (int i = 0; i < coordsLength; i += 2)
      {
        //swap for lat/lon to x/y
        xyPoints[i+1] = llPoints[i];
        xyPoints[i] = llPoints[i+1];
      }
      return xyPoints;
    }

    /**
     * Creates a lot/lon polygon.
     * @param llPoints the coordinates, in the format [lat0, lon0, lat1, lon1, ... ].
     */
    public LatLonPolygon(double[] llPoints)
    {
      poly = new Polygon2D.Double(latLonToXY(llPoints));
      poly.closePath();
         //check if polygon bridges +/-180 degree longitude
         // line and fix values if it does:
      boolean fixFlgVal = false;
      final int nPoints;
      if((nPoints=poly.getVertexCount()) > 2)
      {  //at least three points to work with
        double val = poly.getX(0);
        double minVal = val, maxVal = val;
        int i = 1;
        do         //determine minimum & maximum longitude values:
        {     //for each remaining vertex point in polygon
          if((val=poly.getX(i)) < minVal)
            minVal = val;
          else if(val > maxVal)
            maxVal = val;
        }
        while(++i < nPoints);
        if(minVal < -90.0 && maxVal > 90.0)
        {     //polygon bridges +/-180 degree longitude
          fixFlgVal = true;       //indicate longitude values fixed
          i = 0;
          do         //fix negative longitude values:
          {     //for each vertex point in polygon
            if((val=poly.getX(i)) < 0)      //if negative longitude then
              poly.setX(i,val+360.0);       //make it positive >= 180
          }
          while(++i < nPoints);
        }
      }
      fixedLonFlag = fixFlgVal;        //save fixed-longitudes flag
    }

    /**
     * Determines if the specified coordinate is inside of the region.
     * @param lat the latitude of the point.
     * @param lon the longitude of the point.
     * @return true if the point is inside the region, false otherwise.
     */
    public boolean contains(double lat, double lon)
    {
         //if flag and negative longitude then fix value:
      if(fixedLonFlag && lon < 0)
        return poly.contains(lon + 360.0,lat);
      return poly.contains(lon,lat);      //swap for lat/lon to x/y
    }
  }


  /**
   * Polygon region.
   */
  public static class Polygon extends IstiRegion
  {
    //the coordinates, in the format [lat0, lon0, lat1, lon1, ... ]
    private final double[] llPoints;
    //the polygon
    private final LatLonPolygon poly;

    /**
     * Creates a region defined by the specified coordinates.
     * @param llPoints the coordinates, in the format [lat0, lon0, lat1, lon1, ... ].
     * @return a string representation of the region specifed by the coordinates.
     */
    public Polygon(double[] llPoints)
    {
      super(coordsToString(llPoints));
      this.llPoints = llPoints;
      this.poly = new LatLonPolygon(llPoints);
    }

    /**
     * Creates a copy of a region.
     * @return a copy of the region.
     */
    public Object clone()
    {
      return new Polygon(this).copyOptions(this);
    }

    /**
     * Determines if the specified point is inside of the region.
     * @param lat the latitude of the point.
     * @param lon the longitude of the point.
     * @return true if the point is inside the region, false otherwise.
     */
    public boolean contains(double lat, double lon)
    {
      return poly.contains(lat, lon);
    }

    /**
     * Gets the lat/lon points.
     * @return the lat/lon points.
     */
    public double[] getLatLonPoints()
    {
      return llPoints;
    }

    /**
     * Creates a region defined by the specified coordinates.
     * @param poly the polygon to copy.
     * @return a string representation of the region specifed by the coordinates.
     */
    protected Polygon(Polygon poly)
    {
      this(poly.llPoints);
    }

    /**
     * Constructs a region.
     * @param str string representation of the region.
     * @throws InstantiationException if the class cannot be instantiated.
     */
    protected Polygon(String str)  throws InstantiationException
    {
      super(str);
      this.llPoints = stringToCoords(str);
      //if no points found or if less than 3 points (3 lat and 3 lon)
      if (this.llPoints == null || this.llPoints.length < 6)
      {
        throw new InstantiationException(
            "Invalid string representation: " + str);
      }
      this.poly = new LatLonPolygon(llPoints);
    }
  }


  /**
   * Converts the circle and radius to a string with the radius in miles
   * or other unit if the conversion factor has been set.
   * @param lat the latitude of the point.
   * @param lon the longitude of the point.
   * @param radius the radius of the circle in meters.
   * @return a string representation of the region specifed by the coordinates.
   * @see defaultConversionFactor.
   */
  public static String circleToString(double lat, double lon, double radius)
  {
    return coordToString(lat, lon) + radiusToString(radius);
  }

  /**
   * Creates a copy of a region.
   * @return a copy of the region.
   */
  public Object clone()
  {
    return new IstiRegion(regionTextValueStr).copyOptions(this);
  }

  /**
   * Determines if the specified coordinate is inside of the region.
   * @param lat the latitude of the point.
   * @param lon the longitude of the point.
   * @return true if the point is inside the region, false otherwise.
   */
  public boolean contains(double lat, double lon)
  {
    return false;
  }

  /**
   * Converts the coordinates to a string.
   * @param llPoints the coordinates, in the format [lat0, lon0, lat1, lon1, ... ].
   * @return a string representation of the region specifed by the coordinates.
   */
  public static String coordsToString(double[] llPoints)
  {
    final int ptsLength = llPoints.length;
    final StringBuffer s = new StringBuffer();

    for (int i = 0; i < ptsLength;)
    {
      if (i > 0)
        s.append(COORD_SEP_CHAR.charAt(0));
      s.append(coordToString(llPoints[i++], llPoints[i++]));
    }
    return s.toString();
  }

  /**
   * Converts the point to a string.
   * @param lat the latitude of the point.
   * @param lon the longitude of the point.
   * @return a string representation of the point.
   */
  public static String coordToString(double lat, double lon)
  {
    return (COORD_BEGIN_CHAR + floatNumberToString(lat) +
            " " + floatNumberToString(lon) +
            COORD_END_CHAR);
  }

  /**
   * Converts the given float number value to a string
   * where the value displayed will be at least one fractional digit and
   * will always be non-zero as long as the value is greater than zero.
   * @param value the number value.
   * @return A string representing the number.
   */
  public static String floatNumberToString(double value)
  {
    return UtilFns.floatNumberToString(value,3);
  }

  /**
   * Gets the name of this region.
   * @return the name of this region.
   */
  public String getName()
  {
    return nameVal.stringValue();
  }

  /**
   * Returns the region-text string used to created this object.
   * @return The region-text string used to created this object.
   */
  public String getRegionTextValueStr()
  {
    return regionTextValueStr;
  }

  /**
   * Gets the option value with the specified name.
   * @param name the option name.
   * @return the option value or null if not found.
   */
  public IstiNamedValueInterface getOption(String name)
  {
    IstiNamedValueInterface option = null;
    for (int optionIndex = 0; optionIndex < options.length; optionIndex++)
    {
      option = options[optionIndex];
      if (name == option.getName() ||
          (name != null && name.equals(option.getName())))
        return option;
    }
    return null;
  }

  /**
   * Gets the option number value with the specified name.
   * @param opid the option ID.
   * @return the option number value or null if not found.
   */
  public Number getOptionNumberValue(String opid)
  {
    final Object retVal = getOptionValue(opid);
    if (retVal instanceof Number)
      return (Number)retVal;
    return null;
  }

  /**
   * Gets the option string value with the specified name.
   * @param opid the option ID.
   * @return the option string value or null if not found.
   */
  public String getOptionStringValue(String opid)
  {
    final IstiNamedValueInterface option = getOption(opid);
    if (option != null)
    {
      return removeQuoteChars(option.stringValue());
    }
    return null;
  }

  /**
   * Gets the option value with the specified name.
   * @param opid the option ID.
   * @return the option value or null if not found.
   */
  public Object getOptionValue(String opid)
  {
    final IstiNamedValueInterface option = getOption(opid);
    if (option != null)
    {
      return option.getValue();
    }
    return null;
  }

  /**
   * Parses a string representation of a region.
   * @param str a string representation of a region.
   * @param allowGenericFlag true to allow "generic" region object
   * to be created; false for only circle or polygon region object.
   * @return a region object or null if invalid string.
   */
  public static IstiRegion parse(String str, boolean allowGenericFlag)
  {
    try
    {
      return new IstiRegion.Polygon(str);
    }
    catch (InstantiationException ex) {}
    try
    {
      return new IstiRegion.Circle(str);
    }
    catch (InstantiationException ex) {}
    if(allowGenericFlag)
    {    //allowing "generic" region
      try
      {
        return new IstiRegion(str);
      }
      catch (Exception ex) {}
    }
    return null;
  }

  /**
   * Parses a string representation of a region.
   * @param str a string representation of a region.
   * @return a region or null if invalid string.
   */
  public static IstiRegion parse(String str)
  {
    return parse(str,false);
  }

  /**
   * Parses a string representation any number of regions.
   * @param str a string representation of the regions.
   * @return an array of regions or an empty array if invalid string.
   */
  public static IstiRegion[] parseRegions(String str)
  {
    final String subStr[] =
        UtilFns.parseSeparatedSubstrings(str, REGION_SEP_CHAR);
    final int numRegions = subStr.length;
    final IstiRegion[] regions = new IstiRegion[numRegions];

    for (int i = 0; i < numRegions; i++)
    {
      //return empty array if error
      if ((regions[i] = parse(subStr[i])) == null)
        return new IstiRegion[0];
    }
    return regions;
  }

  /**
   * Converts the radious to a string.
   * @param radius the radius of the circle in meters.
   * @return a string representation of the radius.
   */
  public static String radiusToString(double radius)
  {
                                       //convert meters to selected factor:
    return " " + floatNumberToString(radius * defaultConversionFactor);
  }

  /**
   * Converts the string to coordinate.
   * @param str the string representation of the coordinate.
   * @return the coordinate or null if error.
   */
  public static LatLonPoint stringToCoord(String str)
  {
    try
    {
      final int beginIndex = str.indexOf(COORD_BEGIN_CHAR);
      final int endIndex = str.indexOf(COORD_END_CHAR);

      if (beginIndex < 0 || endIndex < 0 || beginIndex >= endIndex - 1)
        return null;

      final String latLonStr = str.substring(beginIndex + 1, endIndex - 1);
      final int latIndex = UtilFns.indexOfWhitespace(latLonStr);

      if (latIndex <= 0 || latIndex >= latLonStr.length() - 1)
        return null;

      final String latStr = latLonStr.substring(0, latIndex - 1);
      final String lonStr = latLonStr.substring(latIndex + 1);
      final double lat = Double.parseDouble(latStr);
      final double lon = Double.parseDouble(lonStr);
      final LatLonPoint pt = new LatLonPoint(lat, lon);
      return pt;
    }
    catch (Exception ex) {}
    return null;
  }

  /**
   * Converts the string to coordinates.
   * @param str the string representation of the coordinates.
   * @return the coordinates or null if error.
   */
  public static double[] stringToCoords(String str)
  {
    final String[] subStr =
        UtilFns.parseSeparatedSubstrings(str, COORD_SEP_CHAR);
    final int numValues = subStr.length;
    final double[] llPoints = new double[numValues*2];
    LatLonPoint coord;

    for (int i = 0, coordIndex = 0; i < numValues; i++)
    {
      //exit if error
      if ((coord = stringToCoord(subStr[i])) == null)
        return null;
      llPoints[coordIndex++] = coord.getLat();
      llPoints[coordIndex++] = coord.getLon();
    }
    return llPoints;
  }

  /**
   * Converts the string containing a radius in miles
   * or other unit if the conversion factor has been set
   * to a radius in meters.
   * @param str the string representation of the coordinates and radius.
   * @return the radius.
   * @see defaultConversionFactor.
   */
  public static double stringToRadius(String str)
  {
    final int beginIndex = str.indexOf(COORD_END_CHAR);

    if (beginIndex < 0)
      return 0.0;

    //skip over coordinates and trim white space
    str = str.substring(beginIndex + 1).trim();

    final int endIndex = UtilFns.indexOfWhitespace(str);
    if (endIndex >= 0)  //if whitespace was found
      str = str.substring(0, endIndex);  //end at white space
    try
    {                   //parse string and convert km to meters:
      return Double.parseDouble(str) / defaultConversionFactor;
    }
    catch (Exception ex) {}
    return 0.0;
  }

  /**
   * Parses the region name from the given region-text string.
   * @param regionTextStr region-text string to parse.
   * @return The region name, or an empty string if none found.
   */
  public static String extractRegionName(String regionTextStr)
  {
         //find surrounding quotes and extract name between quotes:
    final int firstQuoteIndex,lastQuoteIndex;
    return (regionTextStr != null &&
            (firstQuoteIndex=regionTextStr.indexOf(NAME_BEGIN_CHAR)) >= 0 &&
                 (lastQuoteIndex=regionTextStr.lastIndexOf(NAME_END_CHAR)) >
                                                        firstQuoteIndex+1) ?
                 regionTextStr.substring(firstQuoteIndex+1,lastQuoteIndex) :
                                                       UtilFns.EMPTY_STRING;
  }

  /**
   * Replaces the region name in the given region-text string (or enters
   * the given name if the region had none).
   * @param regionTextStr region-text string to use.
   * @param newNameStr new region name.
   * @return A new string containing the region-text with the new name
   * entered into it.
   */
  public static String replaceRegionName(String regionTextStr,
                                                          String newNameStr)
  {
    if(regionTextStr == null)     //if null then change to empty string
      regionTextStr = UtilFns.EMPTY_STRING;
         //find region name with surrounding quotes:
    final int firstQuoteIndex,lastQuoteIndex;
    if((firstQuoteIndex=regionTextStr.indexOf(NAME_BEGIN_CHAR)) >= 0 &&
                 (lastQuoteIndex=regionTextStr.lastIndexOf(NAME_END_CHAR)) >
                                                            firstQuoteIndex)
    {    //surrounding quote found; remove last quote and preceding chars
      regionTextStr = regionTextStr.substring(lastQuoteIndex+1);
    }
         //insert new name at beginning of region-text string:
    return NAME_BEGIN_CHAR + newNameStr + NAME_END_CHAR + ' ' +
                                                       regionTextStr.trim();
  }

  /**
   * Returns a string representation of the region.
   * @return a string representation of the region.
   */
  public final String toString()
  {
    StringBuffer retVal = new StringBuffer();
    final String nameStr = getName();
    if (nameStr != null)
    {
      retVal.append(NAME_BEGIN_CHAR);
      retVal.append(insertQuoteChars(nameStr));
      retVal.append(NAME_END_CHAR);
      retVal.append(' ');
    }
    retVal.append(regionTextValueStr);
    for (int optionIndex = 1; optionIndex < options.length; optionIndex++)
    {
      if (!options[optionIndex].isDefaultValue())
      {
        retVal.append(" " + OPTION_BEGIN_CHAR);
        if (options[optionIndex].getName() != null)
        {
          retVal.append(insertQuoteChars(options[optionIndex].getName()) +
                        PropItem.sepCh);
        }
        retVal.append(insertQuoteChars(options[optionIndex].stringValue()) +
                      OPTION_END_CHAR);
      }
    }
    return retVal.toString();
  }


  /**
   * Constructs a region.
   * @param str string representation of the region.
   */
  protected IstiRegion(String str)
  {
    this.regionTextValueStr = str.trim();

    parseOptions(str);  //parse the options
  }

  /**
   * Copies the opions from the specified region to this region.
   * @param region the region to copy the options from.
   * @return this region.
   */
  protected IstiRegion copyOptions(IstiRegion region)
  {
    for (int optionIndex = 0; optionIndex < options.length; optionIndex++)
    {
      options[optionIndex].setValue(region.options[optionIndex].getValue());
    }
    return this;
  }

  /**
   * Parses the string for options.
   * @param str the string representation of the region.
   */
  protected final void parseOptions(String str)
  {
    final int firstQuoteIndex = str.indexOf(NAME_BEGIN_CHAR);
    if (firstQuoteIndex >= 0)
    {
      final int lastQuoteIndex = str.lastIndexOf(NAME_END_CHAR);
      if (lastQuoteIndex > firstQuoteIndex)  //if region has a name
      {
        final String nameStr = UtilFns.removeQuoteChars(
            str.substring(firstQuoteIndex+1, lastQuoteIndex));
        nameVal.setValueString(nameStr);
      }
    }

    final int optionBeginIndex = str.indexOf(OPTION_BEGIN_CHAR);
    final int optionEndIndex = str.lastIndexOf(OPTION_END_CHAR);
    if (optionBeginIndex < 0 || optionEndIndex < 0)
      return;

    str = str.substring(optionBeginIndex, optionEndIndex);
    final String[] subStr =
        UtilFns.parseSeparatedSubstrings(str, OPTION_END_CHAR);
    final int numOptions = subStr.length;
    String optionStr;

    for (int i = 0; i < numOptions; i++)
    {
      //get the option string and remove whitespace
      optionStr = subStr[i].trim();

      //skip over if empty
      if (optionStr.length() <= 0)
        continue;

      //skip over if doesn't begin with the begin character
      if (!optionStr.startsWith(OPTION_BEGIN_CHAR))
        continue;

      //remove the begin character and strip whitespace
      optionStr = optionStr.substring(1).trim();
      setOptionValue(optionStr);
    }
  }

  /**
   * Sets the option value.
   * @param optionStr the option string.
   * @return true if successful, false if the string could not be
   * converted to the proper type.
   */
  protected boolean setOptionValue(String optionStr)
  {
    IstiNamedValueInterface namedValue;
    String namedValueOptionName;
    for (int optionIndex = 0; optionIndex < options.length; optionIndex++)
    {
      namedValue = options[optionIndex];
      namedValueOptionName = namedValue.getName();

      final String optionName, optionVal;
      final int sepCharIndex = optionStr.indexOf(IstiNamedValueInterface.sepCh);
      if (sepCharIndex >= 0)
      {
        optionName = optionStr.substring(0,sepCharIndex);
        optionVal = optionStr.substring(sepCharIndex+1);
      }
      else
      {
        optionName = null;
        optionVal = optionStr;
      }

      if (optionName == namedValueOptionName ||
          (namedValueOptionName != null && optionStr.startsWith(namedValueOptionName)))
      {
        return namedValue.setValueString(optionVal);
      }
    }
    return false;
  }

  /**
   * Returns a string with a backslash quote character ('\')
   * inserted in front of each occurance of a "special" character
   * in the given source string.
   * @param dataStr the data source string.
   * @return a new string with its "special" characters quoted, or the
   * original string if no matching "special" characters were found.
   */
  public static String insertQuoteChars(String dataStr)
  {
    return UtilFns.insertQuoteChars(dataStr, DEF_SPECIAL_CHARS_STR);
  }

  /**
   * Removes the backslash quote characters ('\') put in by
   * 'insertQuoteChars()'.  The sequences \t, \n, and \r are
   * converted to the ASCII characters tab, newline, and carriage
   * return, respectively.
   * @param dataStr the data source string.
   * @return a new string with the quote characters removed, or the
   * original string if no quote characters were found.
   */
  public static String removeQuoteChars(String dataStr)
  {
    return UtilFns.removeQuoteChars(dataStr);
  }

//  public static void main(String[] args)
//  {
//    String str;
//    str = "\"Test Polygon\" (39.0 -123.0),(39.0 -114.0),(32.0 -114.0),(32.0 -123.0) [1]" +
//        ";\"Test Circle\" (39.0 -123.0) 45 [mindepth=1][maxdepth=1000][verified=true][maxage=30]";
//    IstiRegion[] regions = IstiRegion.parseRegions(str);
//    IstiRegion region1, region2;
//
//    for (int i = 0; i < regions.length; i++)
//    {
//      region1 = regions[i];
//      region2 = (IstiRegion)region1.clone();
//      System.out.println(region2);
//    }
//  }
}
