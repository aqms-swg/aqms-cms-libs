//IstiRegionMgrImpl.java:   An implementation of methods for
//                          managing a collection of GIS regions.
//                          One main collection and any number of
//                          auxiliary collections are supported.
//
//  4/19/2005 -- [ET]
//

package com.isti.util.gis;

import java.util.*;
import com.isti.util.UtilFns;
import com.isti.util.ModIterator;

/**
 * Class IstiRegionMgrImpl is implementation of methods for managing a
 * collection of GIS regions.  One main collection and any number of
 * auxiliary collections are supported.
 */
public class IstiRegionMgrImpl implements IstiRegionMgrIntf
{
  /** The string representation of the regions. */
  protected String regionsTextValueStr = UtilFns.EMPTY_STRING;
  /** Array of region objects for global alarm regions. */
  protected IstiRegion [] globalRegionObjsArray = null;
  /** List of region-text strings for alert recipients. */
  protected Hashtable recipientRegionObjsTable = null;
  /** List of region objects for global and recipient alarm regions. */
  protected List allRegionObjsList = null;

  /**
   * Sets the main collection of regions.
   * @param regionsStr string representation of the regions to be set.
   */
  public void setMainRegionsStr(String regionsStr)
  {
              //save regions-text value (set empty string if null):
    regionsTextValueStr = (regionsStr != null) ? regionsStr.trim() :
                                                       UtilFns.EMPTY_STRING;
    generateRegionObjects();
  }

  /**
   * Returns the main collection of regions.
   * @return A string representation of the regions.
   */
  public String getMainRegionsStr()
  {
    return regionsTextValueStr;
  }

  /**
   * Returns the main collection of regions.
   * @return An array containing the region objects.
   */
  public IstiRegion [] getMainRegionObjs()
  {
    return globalRegionObjsArray;
  }

  /**
   * Updates an auxiliary region.  If a region array exists for the given
   * key then it is replaced.
   * @param keyObj keyObj object associated with region.
   * @param regionsArr array to region objects to be entered, or null or
   * empty array to delete the entry for the given key.
   */
  public void updateAuxRegionsEntry(Object keyObj, IstiRegion [] regionsArr)
  {
    if(regionsArr != null && regionsArr.length > 0)
    {    //given array contains entries
      if(recipientRegionObjsTable == null)            //if not created then
        recipientRegionObjsTable = new Hashtable();   //create table now
      recipientRegionObjsTable.put(keyObj,regionsArr);    //enter into table
    }
    else
    {    //given array contains no entries; remove entry from table
      if(recipientRegionObjsTable == null ||
                            recipientRegionObjsTable.remove(keyObj) == null)
      {  //table has not been created or matching key not found
        return;         //exit method without updating layer
      }
    }
    generateRegionObjects();             //update layer data
  }

  /**
   * Returns a count of the total number of current global and recipient
   * regions.
   * @return A count of the total number of current global and recipient
   * regions.
   */
  public int getAllRegionsCount()
  {
    return (allRegionObjsList != null) ? allRegionObjsList.size() : 0;
  }

  /**
   * Returns a list of region strings, with an entry for each current
   * global and recipient region.
   * @return A new Vector object containing the region strings (empty list
   * if no regions are available).
   */
  public Vector getAllRegionStrsList()
  {
    final ModIterator iterObj = new ModIterator(allRegionObjsList);
    final Vector regionStrsList = new Vector();
    Object obj;
    while(iterObj.hasNext())
    {    //for each region object; add string to list
      if((obj=iterObj.next()) instanceof IstiRegion)
        regionStrsList.add(((IstiRegion)obj).getRegionTextValueStr());
    }
    return regionStrsList;
  }

  /**
   * Returns a list of region-name strings, with an entry for each current
   * global and recipient region.
   * @return A new Vector object containing the region-name strings (empty
   * list if no regions are available).
   */
  public List getAllRegionNamesList()
  {
    final ModIterator iterObj = new ModIterator(allRegionObjsList);
    final Vector regionNamesList = new Vector();
    Object obj;
    while(iterObj.hasNext())
    {    //for each region object; add name string to list
      if((obj=iterObj.next()) instanceof IstiRegion)
        regionNamesList.add(((IstiRegion)obj).getName());
    }
    return regionNamesList;
  }

  /**
   * Appends a numeric-suffix value to the given name string such that
   * the result name does not duplicate any existing region name.
   * @param baseNameStr base name string to use.
   * @return A new region-name string.
   */
  public String checkGenerateRegionName(String baseNameStr)
  {
    if(baseNameStr == null)                 //if null handle given then
      baseNameStr = UtilFns.EMPTY_STRING;   //convert to empty string
    String checkNameStr;
              //get list of region-names (for all regions):
    final List regionNamesList = getAllRegionNamesList();
    final int regionNamesCount;
    if((regionNamesCount=regionNamesList.size()) > 0)
    {    //list of region-names not empty
              //append numeric-suffix value to given name string
              // and check that name is not in use:
      int suffixVal = 1;
      Object obj;
      boolean matchFlag;
      do
      {  //for each numeric-suffix value to be attempted
        checkNameStr = baseNameStr + suffixVal;
        matchFlag = false;
        for(int i=0; i<regionNamesCount; ++i)
        {     //for each entry in region-names list
          if((obj=regionNamesList.get(i)) instanceof String &&
                                 checkNameStr.equalsIgnoreCase((String)obj))
          {   //name fetched from list OK and matches check name
            matchFlag = true;          //indicate match
            break;                     //exit inner loop
          }
        }
      }       //loop if any region-name matched and not too many iterations
      while(matchFlag && ++suffixVal <= 999);
    }
    else      //list of region-names empty; just append '1' to given name
      checkNameStr = baseNameStr + '1';
    return checkNameStr;
  }

  /**
   * Generates region objects via the regions text.
   */
  protected void generateRegionObjects()
  {
         //if regions text contains data then parse into region objects:
    globalRegionObjsArray = (regionsTextValueStr != null &&
                                         regionsTextValueStr.length() > 0) ?
                              IstiRegion.parseRegions(regionsTextValueStr) :
                                                          new IstiRegion[0];
                             //convert array to list
    allRegionObjsList = new Vector(Arrays.asList(globalRegionObjsArray));
         //if any recipient/auxiliary regions then append them to list:
    if(recipientRegionObjsTable != null &&
                                        recipientRegionObjsTable.size() > 0)
    {    //recipient regions table contains entries
      synchronized(recipientRegionObjsTable)
      {  //grab thread lock for recipient regions table
        final Iterator iterObj =
                             recipientRegionObjsTable.entrySet().iterator();
        Object obj;
        IstiRegion [] regionArr;
        while(iterObj.hasNext())
        {     //for each entry in recipient regions table
          if((obj=((Map.Entry)(iterObj.next())).getValue()) instanceof
                                                              IstiRegion [])
          {   //table value is region array
            regionArr = (IstiRegion [])obj;
            for(int i=0; i<regionArr.length; ++i)     //for each array element
              allRegionObjsList.add(regionArr[i]);       //add to regions list
          }
        }
      }
    }
  }
}
