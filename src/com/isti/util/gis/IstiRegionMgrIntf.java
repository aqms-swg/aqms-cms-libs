//IstiRegionMgrIntf.java:   An interface that defines methods for
//                          managing a collection of GIS regions.
//                          One main collection and any number of
//                          auxiliary collections are supported.
//
//  4/19/2005 -- [ET]
//

package com.isti.util.gis;

import java.util.Vector;
import java.util.List;

/**
 * Interface IstiRegionMgrIntf defines methods for managing a collection of
 * GIS regions.  One main collection and any number of auxiliary collections
 * are supported.
 */
public interface IstiRegionMgrIntf
{
  /**
   * Sets the main collection of regions.
   * @param regionsStr string representation of the regions to be set.
   */
  public void setMainRegionsStr(String regionsStr);

  /**
   * Returns the main collection of regions.
   * @return A string representation of the regions.
   */
  public String getMainRegionsStr();

  /**
   * Returns the main collection of regions.
   * @return An array containing the region objects.
   */
  public IstiRegion [] getMainRegionObjs();

  /**
   * Updates an auxiliary region.  If a region array exists for the given
   * key then it is replaced.
   * @param keyObj keyObj object associated with region.
   * @param regionsArr array to region objects to be entered, or null or
   * empty array to delete the entry for the given key.
   */
  public void updateAuxRegionsEntry(Object keyObj, IstiRegion [] regionsArr);

  /**
   * Returns a count of the total number of current global and recipient
   * regions.
   * @return A count of the total number of current global and recipient
   * regions.
   */
  public int getAllRegionsCount();

  /**
   * Returns a list of region strings, with an entry for each current
   * global and recipient region.
   * @return A new Vector object containing the region strings (empty list
   * if no regions are available).
   */
  public Vector getAllRegionStrsList();

  /**
   * Returns a list of region-name strings, with an entry for each current
   * global and recipient region.
   * @return A new Vector object containing the region-name strings (empty
   * list if no regions are available).
   */
  public List getAllRegionNamesList();

  /**
   * Appends a numeric-suffix value to the given name string such that
   * the result name does not duplicate any existing region name.
   * @param baseNameStr base name string to use.
   * @return A new region-name string.
   */
  public String checkGenerateRegionName(String baseNameStr);
}
