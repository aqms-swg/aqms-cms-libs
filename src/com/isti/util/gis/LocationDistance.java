//LocationDistance.java:  Calculates the distance and azimuth between
//                        two points (converted from "distance.c" in
//                        QPager280Source).
//
//   2/3/2003 -- [KF,ET]
//  3/17/2004 -- [KF]  Moved the 'sphereDist' method to the 'IstiRegion'
//                     class.
//  4/27/2004 -- [ET]  Modified 'loadLocationFile()' to trim whitespace
//                     from location names.
//  6/15/2004 -- [ET]  Moved to 'com.isti.util.gis' package; modified
//                     to no longer reference OpenMap classes;
//                     enhanced methods; moved some methods and
//                     contants to 'GisUtils' class.
//   1/4/2006 -- [KF]  Added 'MeasurementUnitsInformation' interface support.
//  8/28/2012 -- [KF]  Modified 'ACCENT_CHARS' to use integer constants.
//  8/29/2012 -- [KF]  Modified to support specifying a charset name.
//

package com.isti.util.gis;

import java.util.*;
import java.io.BufferedReader;

import com.isti.util.FileUtils;
import com.isti.util.UnsyncFifoHashtable;
import com.isti.util.MeasurementUnitsInformation;
import com.isti.util.MeasurementUnits;

/**
 * Class LocationDistance calculates the distance and azimuth between
 * two points.  (Converted from "distance.c" in QPager280Source).
 */
public class LocationDistance implements MeasurementUnitsInformation
{
    /** The charset name. */
  private static String _charsetName = null;
    /** Array of "accented" characters. */
  public static final char [] ACCENT_CHARS = {0xE1, 0xE9, 0xF3, 0xE7, 0xED, 0xF1, 0xCE};
    /** Array of "unaccented" characters. */
  public static final char [] UNACCENT_CHARS = "aeocinI".toCharArray();
    /** Length of "accented/unaccented" character arrays. */
  public static final int ACCENT_CHARS_LEN = ACCENT_CHARS.length;
    /** Arc-degree to radian (pi / 180). */
  public final static double DEGTORAD = Math.PI / 180.0;
    /** Radian to arc-degree (180 / pi). */
  public final static double RADTODEG = 180.0 / Math.PI;
    /** Array of 'LocationInfoBlk' objects loaded from file. */
  protected final LocationInfoBlk [] locationInfoArray;
    /** Number of entries in 'locationInfoArray'. */
  protected final int locationInfoArrLen;
  // current measurement units
  private MeasurementUnits measurementUnits = new MeasurementUnits();

  /**
   * Get the charset name.
   * @return the charset name.
   */
  public static String getCharsetName()
  {
    return _charsetName;
  }

  /**
   * Set the charset name.
   * @param charsetName the charset name.
   */
  public static void setCharsetName(String charsetName)
  {
    _charsetName = charsetName;
  }

  /**
   * Calculates the distance and azimuth between two points.
   * @param locationNoteDataFileName location note data file name.
   */
  public LocationDistance(String locationNoteDataFileName)
  {
              //load location data:
    locationInfoArray = loadLocationFile(locationNoteDataFileName);
    locationInfoArrLen = (locationInfoArray != null) ?
                                               locationInfoArray.length : 0;
  }

  /**
   * Returns the status of the load of data from the location file.
   * @return true if data was valid, false if an error occurred.
   */
  public boolean isDataValid()
  {
    return (locationInfoArray != null);
  }

  /**
   * Returns an array of 'LocationDistanceInformation' objects for the
   * nearest points to the given point.  The spherical distance (in meters)
   * and azimuth value (in degrees) for each point is determined and
   * entered into the returned 'LocationDistanceInformation' objects.
   * @param latVal the latitude for the given point, in degrees.
   * @param lonVal the longitude for the given point, in degrees.
   * @param maxNumPoints the maximum number of points to be returned.
   * @param maxDistance the maximum-distance limit (in meters) for the
   * returned points, or 0.0 for no limit.
   * @return An array of 'LocationDistanceInformation' objects, or an empty
   * array if no points within the maximum-distance limit could be found,
   * or null if an error occurred.
   */
  public LocationDistanceInformation [] getNearestPoints(double latVal,
                        double lonVal, int maxNumPoints, double maxDistance)
  {
    try
    {
      return selectNearestPoints(latVal,lonVal,
               findNearestPointObjs(latVal,lonVal,maxNumPoints,maxDistance),
               maxNumPoints,maxDistance,getMeasurementUnits());
    }
    catch(Exception ex)
    {    //some kind of exception error
      return null;
    }
  }

  /**
   * Returns a 'LocationDistanceInformation' object for the nearest
   * point to the given point.  The spherical distance (in meters)
   * and azimuth value (in degrees) for the point is determined and
   * entered into the returned 'LocationDistanceInformation' object.
   * @param latVal the latitude for the given point, in degrees.
   * @param lonVal the longitude for the given point, in degrees.
   * @param maxDistance the maximum-distance limit (in meters) for the
   * returned points, or 0.0 for no limit.
   * @return A 'LocationDistanceInformation' object, or null if no points
   * within the maximum-distance limit could be found or if an error
   * occurred.
   */
  public LocationDistanceInformation getNearestPoint(double latVal,
                                          double lonVal, double maxDistance)
  {
    final LocationDistanceInformation [] infoArr;
    if((infoArr=getNearestPoints(latVal,lonVal,1,maxDistance)) != null &&
                                                         infoArr.length > 0)
    {    //at least on object returned in array
      return infoArr[0];
    }
    return null;
  }

  /**
   * Returns a 'LocationDistanceInformation' object for the nearest
   * point to the given point.  The spherical distance (in meters)
   * and azimuth value (in degrees) for the point is determined and
   * entered into the returned 'LocationDistanceInformation' object.
   * @param latVal the latitude for the given point, in degrees.
   * @param lonVal the longitude for the given point, in degrees.
   * @return A 'LocationDistanceInformation' object, or null if no points
   * could be found or if an error occurred.
   */
  public LocationDistanceInformation getNearestPoint(
                                                double latVal,double lonVal)
  {
    return getNearestPoint(latVal,lonVal,0.0);
  }

  /**
   * Returns a string containing location-distance information for the
   * nearest point to the given point.
   * @param latVal the latitude for the given point, in degrees.
   * @param lonVal the longitude for the given point, in degrees.
   * @param maxDistance the maximum-distance limit (in meters) for the
   * returned points, or 0.0 for no limit.
   * @return A string containing location-distance information for the
   * nearest point to the given point, or null if no points within the
   * maximum-distance limit could be found or if an error occurred.
   */
  public String getNearestPointString(double latVal,double lonVal,
                                                         double maxDistance)
  {
    final LocationDistanceInformation infoObj;
    return (infoObj=getNearestPoint(latVal,lonVal,maxDistance)) != null ?
                                                  infoObj.toString() : null;
  }

  /**
   * Returns a string containing location-distance information for the
   * nearest point to the given point.
   * @param latVal the latitude for the given point, in degrees.
   * @param lonVal the longitude for the given point, in degrees.
   * @return A string containing location-distance information for the
   * nearest point to the given point, or null if no points could be
   * found or if an error occurred.
   */
  public String getNearestPointString(double latVal,double lonVal)
  {
    return getNearestPointString(latVal,lonVal,0.0);
  }

  /**
   * Gets the measurement units.
   * @return the measurement units.
   */
  public int getMeasurementUnits()
  {
    return measurementUnits.getMeasurementUnits();
  }

  /**
   * Sets the measurement units.
   * @param mu the measurement units.
   */
  public void setMeasurementUnits(int mu)
  {
    measurementUnits.setMeasurementUnits(mu);
  }

  /**
   * Returns a table containing the nearest points to the given point.
   * The returned array is sorted using the distance values, ascending.
   * @param latVal the latitude for the given point, in degrees.
   * @param lonVal the longitude for the given point, in degrees.
   * @param maxNumPoints the maximum number of points to be returned.
   * @param maxDistance the maximum-distance limit (in meters) for the
   * returned points, or 0.0 for no limit.
   * @return A new 'UnsyncFifoHashtable' object containing
   * 'FoundLocationInfo' objects, one for each found point
   * (with the 'distanceValue' field of each object set to the
   * spherical-distance value for that point) and with the
   * table sorted by ascending order using the 'distanceValue'
   * field.
   */
  public UnsyncFifoHashtable findNearestPointObjs(
         double latVal, double lonVal, int maxNumPoints, double maxDistance)
  {
    if(maxNumPoints <= 0)         //if 'maxNumPoints' not positive then
      maxNumPoints = 1;           //make it 1
    final UnsyncFifoHashtable retTableObj = new UnsyncFifoHashtable();
    if(locationInfoArray != null)
    {    //locations list OK
      final UnsyncFifoHashtable tableObj = new UnsyncFifoHashtable();
         //make table size be double the # of points to be returned
         // (with a minimum of 10) to account for measuring differences:
      final int maxTableSize = (maxNumPoints >= 5) ? maxNumPoints*2 : 10;
      double latDiff, lonDiff, distVal, maxDistVal = 0.0;
      int numTableEntries = 0;
      String keyStr;
      LocationInfoBlk blkObj;
         //fill table with the closest 'maxTableSize' number of
         // entries measured using a "flat" distance formula:
      for(int arrIdx=0; arrIdx<locationInfoArrLen; ++arrIdx)
      {  //for each 'LocationInfoBlk' object in array
        blkObj = locationInfoArray[arrIdx];      //get handle to info block
        latDiff = blkObj.latVal - latVal;        //calc latitude difference
        lonDiff = blkObj.lonVal - lonVal;        //calc longitude difference
                   //calculate the square of the "flat" distance:
        distVal = latDiff*latDiff + lonDiff*lonDiff;
        if(numTableEntries < maxTableSize)
        {     //table has less than 'maxTableSize' entries
          keyStr = blkObj.getKeyString();
          if(!tableObj.containsKey(keyStr))
          {   //duplicate not already in table; add entry
            tableObj.putSortByValue(keyStr,      //(sort by distance value)
                                new FoundLocationInfo(blkObj,distVal),true);
            if(distVal > maxDistVal)        //if new max distance value then
              maxDistVal = distVal;         //set max value
            ++numTableEntries;              //increment count
          }
        }
        else if(distVal < maxDistVal)
        {     //table has 'maxTableSize' or more entries and
              // distance value is less than max in table
          keyStr = blkObj.getKeyString();
          if(!tableObj.containsKey(keyStr))
          {   //duplicate not already in table; add entry
            tableObj.putSortByValue(keyStr,      //(sort by distance value)
                                new FoundLocationInfo(blkObj,distVal),true);
                   //remove last entry in table:
            tableObj.removeElementAt(maxTableSize);
                   //set max distance value via new last entry in table:
            maxDistVal = ((FoundLocationInfo)(tableObj.elementAt(
                                            maxTableSize-1))).distanceValue;
          }
        }
      }
         //convert "flat" distance values to spherical-distance values
         // and put entries into new sorted table:
      final int tableObjSize = tableObj.size();

//      System.out.println("DEBUG findNearestPointObjs table after 1st pass:");

      FoundLocationInfo foundInfoObj;
      int tblIdx;
      for(tblIdx=0; tblIdx<tableObjSize; ++tblIdx)
      {  //for each entry in table; set handle to found-info block
        foundInfoObj = (FoundLocationInfo)(tableObj.elementAt(tblIdx));

                        //calculate and save spherical-distance value:
        foundInfoObj.distanceValue = GisUtils.sphereDist(
                                     foundInfoObj.locationInfoBlkObj.latVal,
                      foundInfoObj.locationInfoBlkObj.lonVal,latVal,lonVal);

        if(maxDistance <= 0.0 || foundInfoObj.distanceValue <= maxDistance)
        {     //max-dist limit not given or value is within limit
                                  //add entry to new table (use same key):
          retTableObj.putSortByValue(            //(sort by distance value)
                                  tableObj.keyAt(tblIdx),foundInfoObj,true);
        }
      }
         //reduce size of table to requested number of points:
      tblIdx = retTableObj.size();

      while(tblIdx > maxNumPoints)
        retTableObj.removeElementAt(--tblIdx);
    }
    return retTableObj;
  }

  /**
   * Returns an array of 'LocationDistanceInformation' objects for the
   * nearest points to the given point, using the given table or
   * tables. The given table or tables may contain 'LocationDistance'
   * objects or 'FoundLocationInfo' objects (generated via the
   * 'findNearestPointObjs()' method).  The returned array is sorted
   * using the distance values, ascending.  The spherical distance (in
   * meters) and azimuth value (in degrees) for each point is determined
   * and entered into the returned 'LocationDistanceInformation' objects.
   * @param latVal the latitude for the given point, in degrees.
   * @param lonVal the longitude for the given point, in degrees.
   * @param foundLocationsObj a List object holding 'LocationDistance'
   * objects; or an 'UnsyncFifoHashtable' object or list of
   * 'UnsyncFifoHashtable' objects, with each table holding
   * 'FoundLocationInfo' objects.
   * @param maxNumPoints the maximum number of points to be returned, or
   * 0 for no limit (if 'FoundLocationInfo' objects were given).
   * @param maxDistance the maximum-distance limit (in meters) for the
   * returned points, or 0.0 for no limit.
   * @return An array of 'LocationDistanceInformation' objects, or an empty
   * array if no points within the maximum-distance limit could be found.
   * @throws ClassCastException if the 'foundLocationsObj' parameter
   * does not reference a list of 'LocationDistance' objects, an
   * 'UnsyncFifoHashtable' object or a list of 'UnsyncFifoHashtable'
   * objects, or if any 'UnsyncFifoHashtable' value entry is not a
   * 'FoundLocationInfo' object.
   */
  public static LocationDistanceInformation [] selectNearestPoints(
                     double latVal, double lonVal, Object foundLocationsObj,
             int maxNumPoints, double maxDistance) throws ClassCastException
  {
    return selectNearestPoints(
      latVal,lonVal,foundLocationsObj,maxNumPoints,maxDistance,
      DEFAULT_MEASUREMENT_UNITS);
  }

  /**
   * Returns an array of 'LocationDistanceInformation' objects for the
   * nearest points to the given point, using the given table or
   * tables. The given table or tables may contain 'LocationDistance'
   * objects or 'FoundLocationInfo' objects (generated via the
   * 'findNearestPointObjs()' method).  The returned array is sorted
   * using the distance values, ascending.  The spherical distance (in
   * meters) and azimuth value (in degrees) for each point is determined
   * and entered into the returned 'LocationDistanceInformation' objects.
   * @param latVal the latitude for the given point, in degrees.
   * @param lonVal the longitude for the given point, in degrees.
   * @param foundLocationsObj a List object holding 'LocationDistance'
   * objects; or an 'UnsyncFifoHashtable' object or list of
   * 'UnsyncFifoHashtable' objects, with each table holding
   * 'FoundLocationInfo' objects.
   * @param maxNumPoints the maximum number of points to be returned, or
   * 0 for no limit (if 'FoundLocationInfo' objects were given).
   * @param maxDistance the maximum-distance limit (in meters) for the
   * returned points, or 0.0 for no limit.
   * @param measurementUnits the measurement units.
   * @return An array of 'LocationDistanceInformation' objects, or an empty
   * array if no points within the maximum-distance limit could be found.
   * @throws ClassCastException if the 'foundLocationsObj' parameter
   * does not reference a list of 'LocationDistance' objects, an
   * 'UnsyncFifoHashtable' object or a list of 'UnsyncFifoHashtable'
   * objects, or if any 'UnsyncFifoHashtable' value entry is not a
   * 'FoundLocationInfo' object.
   */
  public static LocationDistanceInformation [] selectNearestPoints(
                     double latVal, double lonVal, Object foundLocationsObj,
             int maxNumPoints, double maxDistance, int measurementUnits)
             throws ClassCastException
  {
         //list to hold 'LocationDistanceInformation' objs to be returned:
    final ArrayList retListObj = new ArrayList();
    if(latVal <= 90.0 && latVal >= -90.0 &&
                                        lonVal <= 180.0 && lonVal >= -180.0)
    {    //given latitude/longitude values are within bounds
      final UnsyncFifoHashtable foundLocsTableObj;
      if(foundLocationsObj instanceof List)
      {  //given object is a List; check for 'LocationDistance' objects
        final List givenListObj = (List)foundLocationsObj;
        final int givenListSize;
        Object obj;
        if((givenListSize=givenListObj.size()) > 0 &&
                      (obj=givenListObj.get(0)) instanceof LocationDistance)
        {     //given List contains 'LocationDistance' objects
          LocationDistance locDistObj = (LocationDistance)obj;
          foundLocationsObj = new ArrayList();
          int i = 0;
          while(true)   //for each 'LocationDistance' object in list, find
          {             // points and add table-of-points to found-list
            ((List)foundLocationsObj).add(locDistObj.findNearestPointObjs(
                                   latVal,lonVal,maxNumPoints,maxDistance));
            if(++i >= givenListSize)   //if no more 'LocationDistance'
              break;                   // objects then exit loop
            locDistObj = (LocationDistance)(givenListObj.get(i));
          }
        }
                        //'foundLocationsObj' references a list
                        // of 'UnsyncFifoHashtable' objects:
        foundLocsTableObj = new UnsyncFifoHashtable();
        final Iterator tablesIterObj = ((List)foundLocationsObj).iterator();
                        //merge tables into a single, sorted table:
        while(tablesIterObj.hasNext())
        {     //for each 'UnsyncFifoHashtable' object in list
                   //add all entries to destination table, in sorted order:
          final Iterator iterObj = ((UnsyncFifoHashtable)(
                               tablesIterObj.next())).entrySet().iterator();
          Map.Entry e;
          Object keyObj;
          while (iterObj.hasNext())
          {   //for each entry in current table
            e = (Map.Entry)(iterObj.next());
                   //if entry with same key not already in table then add:
            if(!foundLocsTableObj.containsKey(keyObj=e.getKey()))
              foundLocsTableObj.putSortByValue(keyObj,e.getValue(),true);
          }
        }
      }
      else    //given object contains a single 'UnsyncFifoHashtable' object
        foundLocsTableObj = (UnsyncFifoHashtable)foundLocationsObj;

      final double latValRad = latVal * DEGTORAD;
      final double lonValRad = lonVal * DEGTORAD;
      int numPointsVal;           //get maximum # of points to process
      if(maxNumPoints <= 0 ||     // (make sure not > table size):
                     (numPointsVal=maxNumPoints) > foundLocsTableObj.size())
      {  //no max specified or not enough points in table
        numPointsVal = foundLocsTableObj.size();      //use table size
      }
      FoundLocationInfo foundLocInfoObj;
      for(int idx=0; idx<numPointsVal; ++idx)
      {  //for each 'FoundLocationInfo' to be processed from table
        foundLocInfoObj =         //set handle to object
                      (FoundLocationInfo)(foundLocsTableObj.elementAt(idx));
        if(maxDistance > 0.0 && foundLocInfoObj.distanceValue > maxDistance)
          break;        //if beyond max-distance limit then exit loop
              //calculate azimuth from found point to given point:
        double azimVal = GisUtils.sphereAzim(
                         foundLocInfoObj.locationInfoBlkObj.latVal*DEGTORAD,
                         foundLocInfoObj.locationInfoBlkObj.lonVal*DEGTORAD,
                                            latValRad,lonValRad) * RADTODEG;
              //make azimuth value be in the 0 to 360 range:
        while(azimVal < 0.0)
          azimVal += 360.0;
        while(azimVal >= 360.0)
          azimVal -= 360.0;
              //create 'LocationDistanceInformation' obj and add to list:
        final LocationDistanceInformation ldi = new LocationDistanceInformation(
            foundLocInfoObj.distanceValue,azimVal,
            foundLocInfoObj.locationInfoBlkObj.nameStr);
        ldi.setMeasurementUnits(measurementUnits);
        retListObj.add(ldi);
      }
    }
              //allocate array of 'LocationDistanceInformation' objects:
    final LocationDistanceInformation [] retArrObj =
                         new LocationDistanceInformation[retListObj.size()];
              //load array of 'LocationDistanceInformation' objects:
    retListObj.toArray(retArrObj);

//    System.out.println("DEBUG selectNearestPoints retArr:");
//    for(int i=0; i<retArrObj.length; ++i)
//      System.out.println("  " + retArrObj[i]);

    return retArrObj;
  }

  /**
   * Replaces "accented" characters in the given string with "unaccented"
   * equivilants.
   * @param nameStr the string to use.
   * @return A string with with "accented" characters replaced, or the
   * original string if no "accented" characters were found.
   */
  public static String getUnaccentedName(String nameStr)
  {
    final char [] charArr = nameStr.toCharArray();
    final int charArrLen = charArr.length;
    boolean changedFlag = false;
    for(int chIdx=0; chIdx<charArrLen; ++chIdx)
    {    //for each character in given string
      for(int acIdx=0; acIdx<ACCENT_CHARS_LEN; ++acIdx)
      {  //for each character in array of "accented" characters
        if(charArr[chIdx] == ACCENT_CHARS[acIdx])
        {     //current character is "accented"
          charArr[chIdx] = UNACCENT_CHARS[acIdx];    //set "unaccented" char
          changedFlag = true;          //indicate data changed
        }
      }
    }
    if(changedFlag)                    //if data changed then
      return new String(charArr);      //return new string
    return nameStr;          //data not changed; return original string
  }

  /**
   * Loads the given file of "lon lat name" entries into a list.
   * Duplicate entries are not added to the list.
   * @param locFileName the name of the file to use.
   * @return An array of 'LocationInfoBlk' objects, or null if an error
   * occurred.
   */
  public static LocationInfoBlk [] loadLocationFile(String locFileName)
  {
    if(locFileName == null || locFileName.length() <= 0)
      return null;           //if no file name data then abort
    final BufferedReader rdrObj;       //open input file:
    if((rdrObj=FileUtils.getBufferedReader(FileUtils.fileMultiOpenInputStream(
        locFileName), getCharsetName())) == null)
      return null;      //if unable to open then abort
              //create table to hold 'LocationInfoBlk' objects,
              // with keys used to detect duplicate entries:
    final HashMap locsTableObj = new HashMap();
    LocationInfoBlk locInfoBlkObj;
    String keyStr;
    LocationInfoBlk [] retArrObj = null;
    try
    {
      String lineBuff;
      int len,p,sPos;
      float lonVal,latVal;
      while((lineBuff=rdrObj.readLine()) != null)
      {  //for each line of data in file
        if((len=lineBuff.length()) > 0 && !lineBuff.startsWith("#") &&
                                                  !lineBuff.startsWith("!"))
        {     //line contains data and does not start with comment character
          p = 0;             //skip through any leading whitespace:
          while(Character.isWhitespace(lineBuff.charAt(p)) && ++p < len);
          if(p < len)
          {   //non-whitespace character found
            sPos = p;        //save start of value position
            while(!Character.isWhitespace(lineBuff.charAt(p)))
              ++p;           //scan through non-whitespace characters
                             //convert longitude value to 'double':
            lonVal = Float.parseFloat(lineBuff.substring(sPos,p));
            while(Character.isWhitespace(lineBuff.charAt(p)))
              ++p;           //scan through whitespace characters
                             //if comma then skip it & whitespace after it:
            if(lineBuff.charAt(p) == ',')
              while(Character.isWhitespace(lineBuff.charAt(++p)));
            sPos = p;        //save start of value position
            while(!Character.isWhitespace(lineBuff.charAt(p)))
              ++p;           //scan through non-whitespace characters
                             //convert latitude value to 'double':
            latVal = Float.parseFloat(lineBuff.substring(sPos,p));
            while(Character.isWhitespace(lineBuff.charAt(p)))
              ++p;           //scan through whitespace characters
                             //if comma then skip it & whitespace after it:
            if(lineBuff.charAt(p) == ',')
              while(Character.isWhitespace(lineBuff.charAt(++p)));

                   //create location-information block:
            locInfoBlkObj = new LocationInfoBlk(
                                latVal,lonVal,lineBuff.substring(p).trim());
            if(!locsTableObj.containsKey(        //create "key" string
                                       keyStr=locInfoBlkObj.getKeyString()))
            {      //entry with same "key" string not already in table
              locsTableObj.put(keyStr,locInfoBlkObj); //add block to table
            }
          }
        }
      }
                        //allocate array of 'LocationInfoBlk' objects:
      retArrObj = new LocationInfoBlk[locsTableObj.size()];
                        //load array of 'LocationInfoBlk' objects:
      locsTableObj.values().toArray(retArrObj);
    }
    catch(Exception ex)
    {         //error processing file
      retArrObj = null;     //setup to return null
    }
    try { rdrObj.close(); }            //close input file
    catch(java.io.IOException ex) {}
    return retArrObj;
  }


  /**
   * Class LocationInfoBlk contains information for a point location.
   */
  public static class LocationInfoBlk
  {
    public final float latVal;
    public final float lonVal;
    public final String nameStr;

    /**
     * Creates a point-location information block.
     * @param latVal the latitude value to use.
     * @param lonVal the longitude value to use.
     * @param nameStr the name string to use.
     */
    public LocationInfoBlk(float latVal, float lonVal,String nameStr)
    {
      this.latVal = latVal;
      this.lonVal = lonVal;
      this.nameStr = nameStr;
    }

    /**
     * Returns a string version of the location information.
     * @return A string version of the location information.
     */
    public String toString()
    {
      return latVal + " " + lonVal + " " + nameStr;
    }

    /**
     * Returns a "key" string version of the location information.
     * Any "accented" characters in the name are replaced with their
     * "unaccented" equivilants and the name character are made all
     * lower case.
     * @return A "key" string version of the location information.
     */
    public String getKeyString()
    {
      return latVal + " " + lonVal + " " +
                                   getUnaccentedName(nameStr).toLowerCase();
    }
  }


  /**
   * Class FoundLocationInfo references a 'LocationInfoBlk' and contains
   * a distance value.
   */
  public static class FoundLocationInfo implements Comparable
  {
    public final LocationInfoBlk locationInfoBlkObj;
    public double distanceValue;

    /**
     * Creates an object that references a 'LocationInfoBlk' and contains
     * a distance value.
     * @param locationInfoBlkObj the 'LocationInfoBlk' object to reference.
     * @param distanceValue the distance value to use.
     */
    public FoundLocationInfo(LocationInfoBlk locationInfoBlkObj,
                                                       double distanceValue)
    {
      this.locationInfoBlkObj = locationInfoBlkObj;
      this.distanceValue = distanceValue;
    }

    /**
     * Compares this object with the specified object for order.  Returns a
     * negative integer, zero, or a positive integer as this object is less
     * than, equal to, or greater than the specified object.  The
     * 'distanceValue' field is used for comparison.
     * @param obj the Object to be compared.
     * @return A negative integer, zero, or a positive integer as this
     * object is less than, equal to, or greater than the specified object.
     * @throws ClassCastException if the specified object's type prevents it
     * from being compared to this Object.
     */
    public int compareTo(Object obj)
    {
      final double objDistVal = ((FoundLocationInfo)obj).distanceValue;
      if(distanceValue < objDistVal)
        return -1;
      if(distanceValue > objDistVal)
        return 1;
      return 0;
    }

    /**
     * Returns true if the given object is a 'FoundLocationInfo' whose
     * 'distanceValue' is equal to the 'distanceValue' in this object.
     * @param obj the 'FoundLocationInfo' to compare to.
     * @return true if the given object is a 'FoundLocationInfo' whose
     * 'distanceValue' is equal to the 'distanceValue' in this object;
     * false if not.
     */
    public boolean equals(Object obj)
    {
      return (obj instanceof FoundLocationInfo &&
                   ((FoundLocationInfo)obj).distanceValue == distanceValue);
    }

    /**
     * Returns a hash-code value derived from the 'distanceValue' in
     * this object.
     * @return A hash-code value derived from the 'distanceValue' in
     * this object.
     */
    public int hashCode()
    {
      return (Double.valueOf(distanceValue)).hashCode();
    }

    /**
     * Returns a string version of the location and distance information.
     * @return A string version of the location and distance information.
     */
    public String toString()
    {
      return ((locationInfoBlkObj != null) ? locationInfoBlkObj.toString() :
              "<null>") + " " +
          MeasurementUnits.getValue(
          distanceValue, DEFAULT_MEASUREMENT_UNITS);
    }
  }
}
