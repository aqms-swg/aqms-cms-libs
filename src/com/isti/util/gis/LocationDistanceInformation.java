//LocationDistanceInformation.java:  Distance information for two points.
//
//  9/10/2002 -- [KF]  Initial version.
//  12/1/2003 -- [ET]  Changed "@returns" tags to "@return".
//  5/19/2004 -- [ET]  Modified 'toString()' method to use the tag
//                     "mile" instead of "miles" when the value is 1.
//   6/8/2004 -- [ET]  Moved from 'QWCommon' project to 'com.isti.util.gis'
//                     package; made conversion constants 'public'.
//   1/4/2006 -- [KF]  Added 'getMeasurementUnits()', 'setMeasurementUnits()',
//                     and 'getDistanceString()' method,
//                     Added 'MeasurementUnitsInformation' interface support.
//  5/19/2008 -- [ET]  Added 'getDistMeters()' method.
//

package com.isti.util.gis;

import com.isti.util.MeasurementUnitsInformation;
import com.isti.util.MeasurementUnits;

public class LocationDistanceInformation implements MeasurementUnitsInformation
{
  // current measurement units
  private MeasurementUnits measurementUnits = new MeasurementUnits();

  // distance  (meters)
  protected double distance;
  // azimuth   (degrees)
  protected double azim;
  // string text name of closest point
  protected String name;

  private static final String sAzim[] =
  { "N", "NNE", "NE", "ENE",
    "E", "ESE", "SE", "SSE",
    "S", "SSW", "SW", "WSW",
    "W", "WNW", "NW", "NNW"
  };
  private static final double DEG_PER_SECTOR = 360.0/sAzim.length;

  /**
   * Constructs the distance information with default values.
   */
  public LocationDistanceInformation()
  {
    this (0.0, 0.0, "");
  }

  /**
   * Constructs the distance information with the specified values.
   * @param distance  distance between the points, in meters.
   * @param azim  azimuth from point to point.
   * @param name  name of the location.
   */
  public LocationDistanceInformation(double distance, double azim, String name)
  {
    this.distance = distance;
    this.azim = azim;
    this.name = name;
  }

  /**
   * Gets the azimuth value.
   * @return the azimuth value.
   */
  public double getAzim()
  {
    return azim;
  }

  /**
   * Gets the azimuth string.
   * @return the azimuth direction string.
   */
  public String getAzimString()
  {
    // find azimuth sector number
    int i = (int)( (getAzim()+DEG_PER_SECTOR/2.0) / DEG_PER_SECTOR );
    if (i >= sAzim.length)
      i = sAzim.length - 1;
    return sAzim[i];
  }

  /**
   * Gets the measurement units.
   * @return the measurement units.
   */
  public int getMeasurementUnits()
  {
    return measurementUnits.getMeasurementUnits();
  }

  /**
   * Sets the measurement units.
   * @param mu the measurement units.
   */
  public void setMeasurementUnits(int mu)
  {
    measurementUnits.setMeasurementUnits(mu);
  }

  /**
   * Returns the distance value in meters.
   * @return The distance value in meters.
   */
  public double getDistMeters()
  {
    return distance;
  }

  /**
   * Gets the distance value in the current units.
   * @return the distance value.
   */
  public double getDistance()
  {
    return measurementUnits.getValue(distance);
  }

  /**
   * Gets the distance string in the current units.
   * @return the distance string.
   */
  public String getDistanceString()
  {
    return measurementUnits.getValueString(distance);
  }

  /**
   * Gets the name of the closest point.
   * @return the name of the closest point.
   */
  public String getName()
  {
    return name;
  }

  /**
   * Gets a string representation of the distance information.
   * @return a string representation of the distance information.
   */
  public String toString()
  {
    return getDistanceString() + " " + getAzimString() + " of " + getName();
  }
}

