//LocationPlaces.java:  Creates the location places.
//
//  11/2/2005 -- [KF,ET]
//  1/12/2006 -- [KF]  Added 'LocationPlacesInterface' interface.
//  1/13/2006 -- [KF]  Added constructor with places directory name parameter.
//  5/19/2008 -- [ET]  Modified to use 'LocationDistanceInformation' method
//                     'getDistMeters()' instead of 'getDistance()'.
//

package com.isti.util.gis;

import java.io.File;
import java.util.Arrays;
import java.util.Comparator;
import java.util.Iterator;
import java.util.List;
import java.util.Vector;

import com.isti.util.LogFile;
import com.isti.util.UtilFns;

/**
 * Class LocationNotes creates the location places.
 */
public class LocationPlaces implements LocationPlacesInterface
{
    /** Default name of directory for places files. */
  public static final String DEF_PLACEFILES_DIR_NAME = "res/places";

    /** The log file object. */
  protected final LogFile logObj;

    /** Array of location-distance-holder objects. */
  protected final LocationDistHolder [] locationDistHolderArr;
    /** List of location-distance objects for "short" messages. */
  protected final List shortMsgLocDistListObj = new Vector();
    /** List of location-distance objects for "short" "quarry" messages. */
  protected final List shortQuarryMsgLocDistListObj = new Vector();
    /** Location note error message string. */
  protected String loadLocNoteErrMsgStr = null;

  /**
   * Creates the location places with the default directory.
   * If there is an error loading the location places the
   * 'getErrorMessageString' method may be used to get the error message string.
   * @see getLocationNoteErrorMessageString.
   */
  public LocationPlaces()
  {
    this(DEF_PLACEFILES_DIR_NAME,LogFile.getGlobalLogObj(true));
  }

  /**
   * Creates the location places.
   * If there is an error loading the location places the
   * 'getErrorMessageString' method may be used to get the error message string.
   * @param placesDirName the name of the directory for place files.
   * @param logFileObj the log file object.
   * @see getLocationNoteErrorMessageString.
   */
  public LocationPlaces(String placesDirName,LogFile logFileObj)
  {
    this(new File(placesDirName),logFileObj);
  }

  /**
   * Creates the location places.
   * If there is an error loading the location places the
   * 'getErrorMessageString' method may be used to get the error message string.
   * @param placesDirectoryObj the directory for place files.
   * @param logFileObj the log file object.
   * @see getLocationNoteErrorMessageString.
   */
  public LocationPlaces(File placesDirectoryObj,LogFile logFileObj)
  {
    //save parameters
    logObj = (logFileObj != null)?logFileObj:LogFile.getNullLogObj();
         //create location-distance objects (given filename, number of
         // points and maximum-distance value) and load into Vector:
    final Vector hldrVec = new Vector();
    final Vector shortMsgVec = new Vector();     //subset for "short" msgs
    LocationDistHolder holderObj;
    hldrVec.add(new LocationDistHolder(
      new File(placesDirectoryObj,"places"),3,500));
    hldrVec.add(new LocationDistHolder(
      new File(placesDirectoryObj,"bigplaces"),1,5000));
    shortMsgVec.addAll(hldrVec);  //use 'small' & 'big' for "short" msgs
    hldrVec.add(new LocationDistHolder(
      new File(placesDirectoryObj,"familiarplaces"),1,5000));
    hldrVec.add(holderObj = new LocationDistHolder(
      new File(placesDirectoryObj,"quarryplaces"),1,20,true));
         //setup location-distance object for "short" "quarry" messages:
    shortQuarryMsgLocDistListObj.add(holderObj.getLocDistObj());
    int vecSize = hldrVec.size();
    int i = 0;
    while(i < vecSize)
    {    //for each 'locations' file loaded; check if loaded OK
      holderObj = (LocationDistHolder)(hldrVec.elementAt(i));
      if(holderObj.getLocDistObj() == null)
      {  //loading of 'locations' file failed
        hldrVec.remove(holderObj);     //remove object from Vector
        --vecSize;                     //decrement size variable
        shortMsgVec.remove(holderObj); //also remove from "short"-msg Vector
                        //enter file name into error-message string:
        loadLocNoteErrMsgStr = ((loadLocNoteErrMsgStr != null) ?
                                (loadLocNoteErrMsgStr + ", ") : "") + "\"" +
                                               holderObj.fileNameStr + "\"";
      }
      else
        ++i;
    }
    if(loadLocNoteErrMsgStr != null)
    {    //'locations' file error; add prompt to list of file names
      loadLocNoteErrMsgStr = "Unable to load 'locations' file(s):  " +
                                                       loadLocNoteErrMsgStr;
    }
    LocationDistHolder [] arrObj;
    try
    {    //convert list of 'LocationDistHolder' objects to an array:
      arrObj = new LocationDistHolder[vecSize];
      hldrVec.toArray(arrObj);
    }
    catch(Exception ex)
    {    //error converting; setup error message
      arrObj = new LocationDistHolder [0];
      loadLocNoteErrMsgStr =
                          "Error processing location-distance list:  " + ex;
    }
    locationDistHolderArr = arrObj;
    try
    {    //setup list of 'LocationDistance' objects for "short" messages:
      vecSize = shortMsgVec.size();
      for(i=0; i<vecSize; ++i)
      {  //for each 'LocationDistHolder' object in "short"-messages list
        shortMsgLocDistListObj.add(    //fetch & add 'LocationDistance' obj
                  ((LocationDistHolder)shortMsgVec.get(i)).getLocDistObj());
      }
    }
    catch(Exception ex)
    {    //some kind of exception error
      if(loadLocNoteErrMsgStr == null)
      {  //no previous error message; build error message
        loadLocNoteErrMsgStr =
                  "Error processing loc-dist list for 'short' msgs:  " + ex;
      }
    }
  }

  /**
   * Gets the error message string.
   * @return the error message string.
   */
  public String getErrorMessageString()
  {
    return loadLocNoteErrMsgStr;
  }

  /**
   * Gets the name of the closest location.
   * @param infoArr the array of 'LocationDistanceInformation' objects.
   * @return the name of the closest location or an empty string if none.
   * @see getLocationDistInfo.
   */
  public static String getClosestLocationName(LocationDistanceInformation[] infoArr)
  {
    return getClosestLocationName(infoArr,UtilFns.EMPTY_STRING);
  }

  /**
   * Gets the name of the closest location.
   * @param infoArr the array of 'LocationDistanceInformation' objects.
   * @param prefixStr the prefix string to prefix the location name.
   * @return the name of the closest location or an empty string if none.
   * @see getLocationDistInfo.
   */
  public static String getClosestLocationName(
      LocationDistanceInformation[] infoArr,String prefixStr)
  {
    return getClosestLocationName(infoArr,prefixStr,UtilFns.EMPTY_STRING);
  }

  /**
   * Gets the name of the closest location.
   * @param infoArr the array of 'LocationDistanceInformation' objects.
   * @param prefixStr the prefix string to prefix the location name.
   * @param defaultStr the default string to use if no location.
   * @return the name of the closest location or default string if none.
   * @see getLocationDistInfo.
   */
  public static String getClosestLocationName(
      LocationDistanceInformation[] infoArr,String prefixStr,String defaultStr)
  {
    String locName = defaultStr;
    if (infoArr != null && infoArr.length > 0)
    {
      locName = prefixStr + infoArr[0].getName();
    }
    return locName;
  }

  /**
   * Returns an array of 'LocationDistanceInformation' objects for the
   * nearest points to the given point.  The spherical distance (in meters)
   * and azimuth value (in degrees) for each point is determined and
   * entered into the returned 'LocationDistanceInformation' objects.
   * @param latVal the latitude of the given point.
   * @param lonVal the longitude of the given point.
   * @param quarryFlag true if the given point references a probable
   * quarry explosion; false if not.
   * @return An array of 'LocationDistanceInformation' objects, or an empty
   * array if no points within the maximum-distance limits could be found,
   * or null if an error occurred.
   */
  public LocationDistanceInformation [] getLocationDistInfo(double latVal,
                                          double lonVal, boolean quarryFlag)
  {
    try
    {
      Vector vec = new Vector();
      LocationDistHolder holderObj;
      LocationDistanceInformation [] infoArr;
      LocationDistanceInformation infoObj;
      for(int hdrIdx=0; hdrIdx<locationDistHolderArr.length; ++hdrIdx)
      {  //for each location-distance-holder object in array
        holderObj = locationDistHolderArr[hdrIdx];
        if(!holderObj.quarryFlag &&
                (infoArr=holderObj.getNearestPoints(latVal,lonVal)) != null)
        {     //not 'quarryplaces' & array of loc-info objects fetched OK
          for(int infIdx=0; infIdx<infoArr.length; ++infIdx)
          {   //for each 'LocationDistance' object in array
                   //check if 'LocationDistance' object with matching
                   // name already exists in Vector:
            if(!locInfocontainsSameName(vec,
                                       (infoObj=infoArr[infIdx]).getName()))
            { //no 'LocationDistance' object in Vector with same name
              vec.add(infoObj);        //add to Vector
            }
          }
        }
      }
              //allocate array to hold 'LocationDistance' objects:
      infoArr = new LocationDistanceInformation[vec.size()];
      vec.toArray(infoArr);       //fill array
      Arrays.sort(infoArr, new Comparator()

          {        //sort list of locations by distance value, ascending
            public int compare(Object obj1, Object obj2)
            {      //implement comparator for distance value
              final double val1 =
                        ((LocationDistanceInformation)obj1).getDistMeters();
              final double val2 =
                        ((LocationDistanceInformation)obj2).getDistMeters();
              if(val1 < val2)
                return -1;
              if(val1 > val2)
                return 1;
              return 0;
            }
          });
      if(quarryFlag)
      {  //given point references a probable quarry explosion
              //convert array back into a Vector:
        vec = new Vector(Arrays.asList(infoArr));
              //find nearest 'quarryplace' location:
        LocationDistanceInformation matchedInfoObj;
        for(int hdrIdx=0; hdrIdx<locationDistHolderArr.length; ++hdrIdx)
        {     //for each location-distance-holder object in array
          holderObj = locationDistHolderArr[hdrIdx];
          if(holderObj.quarryFlag &&
                (infoArr=holderObj.getNearestPoints(latVal,lonVal)) != null)
          {   //is 'quarryplaces' & array of loc-info objects fetched OK
            for(int infIdx=0; infIdx<infoArr.length; ++infIdx)
            {      //for each 'LocationDistance' object in array
              infoObj = infoArr[infIdx];
                   //check if 'LocationDistance' object with matching
                   // name already exists in Vector:
              if((matchedInfoObj=getSameNameLocationInfo(vec,
                                                infoObj.getName())) != null)
              {    //'LocationDistance' object in Vector with same name
                vec.remove(matchedInfoObj);      //remove from Vector
              }
                   //put "quarry" location note at top of list:
              vec.insertElementAt(infoObj,0);
            }
          }
        }
              //allocate new array to hold 'LocationDistance' objects:
        infoArr = new LocationDistanceInformation[vec.size()];
        vec.toArray(infoArr);       //fill array
      }
      return infoArr;
    }
    catch(Exception ex)
    {    //some kind of exception error; log it
      logObj.warning("Error getting location-distance information:  " + ex);
      logObj.warning(UtilFns.getStackTraceString(ex));
    }
    return null;
  }

  /**
   * Checks if any 'LocationDistanceInformation' objects with the given
   * name exist in the given list.  The names are compared with letter-case,
   * whitespace and character "accents" ignored.
   * @param listObj the list of 'LocationDistanceInformation' objects to
   * use.
   * @param nameStr the name string to use.
   * @return true if a matching name was found, false if not.
   */
  public static boolean locInfocontainsSameName(List listObj, String nameStr)
  {
    return (getSameNameLocationInfo(listObj,nameStr) != null);
  }

  /**
   * Returns the first 'LocationDistanceInformation' object with the given
   * name in the given list.  The names are compared with letter-case,
   * whitespace and character "accents" ignored.
   * @param listObj the list of 'LocationDistanceInformation' objects to
   * use.
   * @param nameStr the name string to use.
   * @return The first 'LocationDistanceInformation' object with the given
   * name in the given list.
   */
  public static LocationDistanceInformation getSameNameLocationInfo(
                                               List listObj, String nameStr)
  {
    if(listObj != null && nameStr != null)
    {    //given parameters not null
              //create version of name without "accented" chars or spaces:
      nameStr = LocationDistance.getUnaccentedName(nameStr.trim());
      final Iterator iterObj = listObj.iterator();
      Object obj;
      String str;
      while(iterObj.hasNext())
      {  //for each 'LocationDistanceInformation' object in list
        if((obj=iterObj.next()) instanceof LocationDistanceInformation &&
                 (str=((LocationDistanceInformation)obj).getName()) != null)
        {     //'LocationDistanceInformation' object and name fetched OK
                             //create version of name without "accented"
                             // characters or spaces and compare:
          if(nameStr.equalsIgnoreCase(
                            LocationDistance.getUnaccentedName(str.trim())))
          {   //name same
            return (LocationDistanceInformation)obj;
          }
        }
      }
    }
    return null;        //no matching names
  }

  /**
   * Returns a "short"-message-version 'LocationDistanceInformation'
   * object for the nearest point to the given point.  The
   * spherical distance (in meters) and azimuth value (in
   * degrees) for the point is determined and entered into
   * the returned 'LocationDistanceInformation' object.
   * @param latVal the latitude of the given point.
   * @param lonVal the longitude of the given point.
   * @param quarryFlag true if the given point references a probable
   * quarry explosion; false if not.
   * @return A 'LocationDistanceInformation' object, or null
   * if no point could be found or if an error occurred.
   */
  public LocationDistanceInformation getShortMsgLocDistInfo(double latVal,
                                          double lonVal, boolean quarryFlag)
  {
    try
    {
      final LocationDistanceInformation [] infoArr;
              //select list of location-distance objects based
              // on whether or not 'quarryFlag' is set:
      if((infoArr=LocationDistance.selectNearestPoints(latVal,lonVal,
           (quarryFlag?shortQuarryMsgLocDistListObj:shortMsgLocDistListObj),
                                      1,0.0)) != null && infoArr.length > 0)
      {
        return infoArr[0];
      }
    }
    catch(Exception ex)
    {    //some kind of exception error; log it
      logObj.warning("Error getting loc-dist info for 'short' msg:  " + ex);
      logObj.warning(UtilFns.getStackTraceString(ex));
    }
    return null;
  }

  /**
   * Class LocationDistHolder holds a location-distance object, a
   * number-of-points and a maximum-distance value.
   */
  protected static class LocationDistHolder
  {
    private final String fileNameStr;
    private final int numPoints;
    private final double maxDistance;
    private final boolean quarryFlag;
    private final LocationDistance locDistObj;

    /**
     * Creates a location-distance holder object.
     * @param locFileObj the "locations" file to load.
     * @param numPoints a number-of-points value.
     * @param maxDistKm the maximum-distance limit (in kilometers) for
     * the returned points, or 0.0 for no limit.
     * @param quarryFlag true if the locations are "quarry" places;
     * false if not.
     */
    public LocationDistHolder(File locFileObj, int numPoints,
                                       double maxDistKm, boolean quarryFlag)
    {
      this.fileNameStr = locFileObj.getPath();       //save parameter values
      this.numPoints = numPoints;
      this.maxDistance = maxDistKm * 1000;
      this.quarryFlag = quarryFlag;
      LocationDistance tempDistObj;
      try
      {       //create location-distance object:
        tempDistObj = new LocationDistance(fileNameStr);
      }
      catch(Exception ex)
      {       //some kind of exception error
        tempDistObj = null;            //indicate not loaded
        System.err.println("Error loading 'locations' file \"" +
                                                fileNameStr + "\":  " + ex);
        ex.printStackTrace();          //show error
      }
                        //if file loaded OK then keep object; else null:
      locDistObj = (tempDistObj != null && tempDistObj.isDataValid()) ?
                                                         tempDistObj : null;
    }

    /**
     * Creates a location-distance holder object.
     * @param locFileObj the "locations" file to load.
     * @param numPoints a number-of-points value.
     * @param maxDistKm the maximum-distance limit (in kilometers) for
     * the returned points, or 0.0 for no limit.
     */
    public LocationDistHolder(File locFileObj, int numPoints,
                                                           double maxDistKm)
    {
      this(locFileObj,numPoints,maxDistKm,false);
    }

    /**
     * Returns the location-distance object.
     * @return The location-distance object, or null if the "locations"
     * file could not be loaded.
     */
    public LocationDistance getLocDistObj()
    {
      return locDistObj;
    }

    /**
     * Returns an array of 'LocationDistanceInformation' objects for the
     * nearest points to the given point.  The spherical distance (in meters)
     * and azimuth value (in degrees) for each point is determined and
     * entered into the returned 'LocationDistanceInformation' objects.
     * @param latVal the latitude for the given point, in degrees.
     * @param lonVal the longitude for the given point, in degrees.
     * @return An array of 'LocationDistanceInformation' objects, or an empty
     * array if no points within the maximum-distance limit could be found,
     * or null if an error occurred.
     */
    public LocationDistanceInformation [] getNearestPoints(
                                               double latVal, double lonVal)
    {
      return locDistObj.getNearestPoints(
                                       latVal,lonVal,numPoints,maxDistance);
    }
  }
}
