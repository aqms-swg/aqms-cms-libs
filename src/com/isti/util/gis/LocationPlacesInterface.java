//LocationPlacesInterface.java:  Creates the location places.
//
//  1/12/2005 -- [KF]
//

package com.isti.util.gis;

/**
 * Interface LocationPlacesInterface creates the location places.
 */
public interface LocationPlacesInterface
{
  /**
   * Returns an array of 'LocationDistanceInformation' objects for the
   * nearest points to the given point.  The spherical distance (in meters)
   * and azimuth value (in degrees) for each point is determined and
   * entered into the returned 'LocationDistanceInformation' objects.
   * @param latVal the latitude of the given point.
   * @param lonVal the longitude of the given point.
   * @param quarryFlag true if the given point references a probable
   * quarry explosion; false if not.
   * @return An array of 'LocationDistanceInformation' objects, or an empty
   * array if no points within the maximum-distance limits could be found,
   * or null if an error occurred.
   */
  public LocationDistanceInformation [] getLocationDistInfo(double latVal,
                                          double lonVal, boolean quarryFlag);

  /**
   * Returns a "short"-message-version 'LocationDistanceInformation'
   * object for the nearest point to the given point.  The
   * spherical distance (in meters) and azimuth value (in
   * degrees) for the point is determined and entered into
   * the returned 'LocationDistanceInformation' object.
   * @param latVal the latitude of the given point.
   * @param lonVal the longitude of the given point.
   * @param quarryFlag true if the given point references a probable
   * quarry explosion; false if not.
   * @return A 'LocationDistanceInformation' object, or null
   * if no point could be found or if an error occurred.
   */
  public LocationDistanceInformation getShortMsgLocDistInfo(double latVal,
                                          double lonVal, boolean quarryFlag);
}
