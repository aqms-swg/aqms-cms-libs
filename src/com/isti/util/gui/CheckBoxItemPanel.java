//CheckBoxItemPanel.java:  Defines a panel containing a check-box item
//                         and message text.
//
//  8/10/2007 -- [ET]
//

package com.isti.util.gui;

import java.awt.Component;
import java.awt.BorderLayout;
import java.awt.GridBagLayout;
import java.awt.GridBagConstraints;
import java.awt.Insets;
import javax.swing.JPanel;
import javax.swing.JLabel;
import javax.swing.JCheckBox;
import javax.swing.BorderFactory;

/**
 * Class CheckBoxItemPanel defines a panel containing a check-box item
 * and message text.
 */
public class CheckBoxItemPanel extends JPanel
{
  /** Default horizontal gap value for items on panel layout. */
  public static final int DEF_HORIZ_GAP = 7;
  /** Default vertical gap value for items on panel layout. */
  public static final int DEF_VERT_GAP = 12;
  /** Check-box item for panel. */
  protected final JCheckBox checkBoxItemObj = new JCheckBox();

  /**
   * Creates a check-box item panel.
   * @param msgObj the message object to show above the check-box item;
   * may be a 'String', a 'Component', or null for none.
   * @param promptStr the prompt string to show to the right of the
   * check box.
   * @param bottomObj the object to show below the text-edit field;
   * may be a 'String', a 'Component', or null for none.
   * @param initialStateFlg sets the initial state of the check-box
   * item; true for selected, false for not selected.
   * @param horizGap the horizontal gap to use in the layout.
   * @param vertGap the vertical gap to use in the layout.
   */
  public CheckBoxItemPanel(Object msgObj, String promptStr, Object bottomObj,
                         boolean initialStateFlg, int horizGap, int vertGap)
  {
    setLayout(new BorderLayout(horizGap,vertGap));
                   //add some margin space (if top/bottom objects given):
    setBorder(BorderFactory.createEmptyBorder(
                         ((msgObj!=null)?5:0),0,((bottomObj!=null)?5:0),0));

         //setup message-component object:
    final Component msgCompObj;
    if(msgObj instanceof String)
      msgCompObj = new MultiLineJLabel((String)msgObj);
    else if(msgObj instanceof Component)
      msgCompObj = (Component)msgObj;
    else if(msgObj == null)
      msgCompObj = new JLabel();
    else
    {
      throw new IllegalArgumentException("Message-object parameter must " +
                                      "be of type 'String' or 'Component'");
    }

         //setup text and state for check-box component:
    checkBoxItemObj.setText(promptStr);
    checkBoxItemObj.setSelected(initialStateFlg);

         //setup message-component object:
    final Component bottomCompObj;
    if(bottomObj instanceof String)
      bottomCompObj = new MultiLineJLabel((String)bottomObj);
    else if(bottomObj instanceof Component)
      bottomCompObj = (Component)bottomObj;
    else if(bottomObj == null)
      bottomCompObj = new JLabel();
    else
    {
      throw new IllegalArgumentException("Bottom-object parameter must " +
                                      "be of type 'String' or 'Component'");
    }

              //setup GridBagConstraints to fill horizontally:
    final GridBagConstraints constraintsObj = new GridBagConstraints(0,0,
          1,1,1.0,0.0,GridBagConstraints.WEST,GridBagConstraints.HORIZONTAL,
                                                   new Insets(0,0,0,0),0,0);

              //put message-object into a panel to center it vertically
              // (use GridBagLayout to make panel fill horizontally):
    final JPanel msgPanelObj = new JPanel(new GridBagLayout());
    msgPanelObj.add(msgCompObj,constraintsObj);

         //add text components and check-box item to panel:
    add(msgPanelObj,BorderLayout.NORTH);
    add(checkBoxItemObj,BorderLayout.CENTER);
    add(bottomCompObj,BorderLayout.SOUTH);
  }

  /**
   * Creates a check-box item panel.
   * @param msgObj the message object to show above the check-box item;
   * may be a 'String', a 'Component', or null for none.
   * @param promptStr the prompt string to show to the right of the
   * check box.
   * @param bottomObj the object to show below the text-edit field;
   * may be a 'String', a 'Component', or null for none.
   * @param initialStateFlg sets the initial state of the check-box
   * item; true for selected, false for not selected.
   */
  public CheckBoxItemPanel(Object msgObj, String promptStr, Object bottomObj,
                                                    boolean initialStateFlg)
  {
    this(msgObj,promptStr,bottomObj,initialStateFlg,
                                                DEF_HORIZ_GAP,DEF_VERT_GAP);
  }

  /**
   * Creates a check-box item panel.
   * @param msgObj the message object to show above the check-box item;
   * may be a 'String', a 'Component', or null for none.
   * @param promptStr the prompt string to show to the right of the
   * check box.
   * @param initialStateFlg sets the initial state of the check-box
   * item; true for selected, false for not selected.
   */
  public CheckBoxItemPanel(Object msgObj, String promptStr,
                                                    boolean initialStateFlg)
  {
    this(msgObj,promptStr,null,initialStateFlg,DEF_HORIZ_GAP,DEF_VERT_GAP);
  }

  /**
   * Creates a check-box item panel.
   * @param msgObj the message object to show above the check-box item;
   * may be a 'String', a 'Component', or null for none.
   * @param promptStr the prompt string to show to the right of the
   * check box.
   */
  public CheckBoxItemPanel(Object msgObj, String promptStr)
  {
    this(msgObj,promptStr,null,false,DEF_HORIZ_GAP,DEF_VERT_GAP);
  }

  /**
   * Creates a check-box item panel.
   * @param msgObj the message object to show above the check-box item;
   * may be a 'String', a 'Component', or null for none.
   */
  public CheckBoxItemPanel(Object msgObj)
  {
    this(msgObj,null,null,false,DEF_HORIZ_GAP,DEF_VERT_GAP);
  }

  /**
   * Creates a check-box item panel.
   */
  public CheckBoxItemPanel()
  {
    this(null,null,null,false,DEF_HORIZ_GAP,DEF_VERT_GAP);
  }

  /**
   * Displays this check-box panel in a popup dialog.  This method
   * blocks until the dialog is dismissed by the user (even if the
   * window is non-modal).  The returned value will be one of the
   * "..._OPTION" values.
   * @param parentComp the parent component for the popup.
   * @param titleStr the title string for popup window.
   * @param optionType specifies what default option buttons to
   * automatically create if 'optionsArr' is null or empty; one of:
   * DEFAULT_OPTION, YES_NO_OPTION, YES_NO_CANCEL_OPTION, OK_CANCEL_OPTION
   * or NO_AUTOBUTTONS_OPTION.
   * @param msgType the type of message to be displayed (primarily used
   * to determine the icon from the pluggable Look and Feel):
   * ERROR_MESSAGE, INFORMATION_MESSAGE, WARNING_MESSAGE,
   * QUESTION_MESSAGE, or PLAIN_MESSAGE.
   * @param modalFlag true for modal, false for modeless (allows other
   * windows to run).
   * @return An integer indicating the option chosen by the user,
   * or CLOSED_OPTION if the user closed the dialog without selecting
   * an option.
   */
  public int showPanelInDialog(Component parentComp, String titleStr,
                             int optionType, int msgType, boolean modalFlag)
  {
    final IstiDialogPopup popupObj = new IstiDialogPopup(parentComp,this,
                                     titleStr,msgType,modalFlag,optionType);
    popupObj.setRepackNeededFlag(true);     //setup to repack after shown
         //show dialog and then return value:
    return popupObj.showAndReturnIndex();
  }

  /**
   * Displays this check-box panel in a popup dialog, with "OK" and
   * "Cancel" buttons.  This method blocks until the dialog is dismissed
   * by the user (even if the window is non-modal).  The returned value
   * will be one of the "..._OPTION" values.
   * @param parentComp the parent component for the popup.
   * @param titleStr the title string for popup window.
   * @param msgType the type of message to be displayed (primarily used
   * to determine the icon from the pluggable Look and Feel):
   * ERROR_MESSAGE, INFORMATION_MESSAGE, WARNING_MESSAGE,
   * QUESTION_MESSAGE, or PLAIN_MESSAGE.
   * @param modalFlag true for modal, false for modeless (allows other
   * windows to run).
   * @return An integer indicating the option chosen by the user,
   * or CLOSED_OPTION if the user closed the dialog without selecting
   * an option.
   */
  public int showPanelInDialog(Component parentComp, String titleStr,
                                             int msgType, boolean modalFlag)
  {
    return showPanelInDialog(parentComp,titleStr,
                        IstiDialogPopup.OK_CANCEL_OPTION,msgType,modalFlag);
  }

  /**
   * Displays this check-box panel in a popup dialog, with "OK" and
   * "Cancel" buttons and a "PLAIN_MESSAGE" style of window.  This method
   * blocks until the dialog is dismissed by the user (even if the window
   * is non-modal).  The returned value will be one of the "..._OPTION"
   * values.
   * @param parentComp the parent component for the popup.
   * @param titleStr the title string for popup window.
   * @param modalFlag true for modal, false for modeless (allows other
   * windows to run).
   * @return An integer indicating the option chosen by the user,
   * or CLOSED_OPTION if the user closed the dialog without selecting
   * an option.
   */
  public int showPanelInDialog(Component parentComp, String titleStr,
                                                          boolean modalFlag)
  {
    return showPanelInDialog(parentComp,titleStr,
             IstiDialogPopup.OK_CANCEL_OPTION,IstiDialogPopup.PLAIN_MESSAGE,
                                                                 modalFlag);
  }

  /**
   * Displays this check-box panel in a popup dialog, with "OK" and
   * "Cancel" buttons and a "PLAIN_MESSAGE" style, modal window.  This
   * method blocks until the dialog is dismissed by the user (even if
   * the window is non-modal).  The returned value will be one of the
   * "..._OPTION" values.
   * @param parentComp the parent component for the popup.
   * @param titleStr the title string for popup window.
   * @return An integer indicating the option chosen by the user,
   * or CLOSED_OPTION if the user closed the dialog without selecting
   * an option.
   */
  public int showPanelInDialog(Component parentComp, String titleStr)
  {
    return showPanelInDialog(parentComp,titleStr,
             IstiDialogPopup.OK_CANCEL_OPTION,IstiDialogPopup.PLAIN_MESSAGE,
                                                                      true);
  }

  /**
   * Returns the state of the check-box item.
   * @return true if selected; false if not.
   */
  public boolean getCheckBoxState()
  {
    return checkBoxItemObj.isSelected();
  }

  /**
   * Sets the state of the check-box item.
   * @param flgVal true for selected; false for not.
   */
  public void setCheckBoxState(boolean flgVal)
  {
    checkBoxItemObj.setSelected(flgVal);
  }

  /**
   * Sets the text shown on the check-box item.
   * @param textStr text to be shown.
   */
  public void setCheckBoxText(String textStr)
  {
    checkBoxItemObj.setText(textStr);
  }

  /**
   * Returns the check-box object used by this panel.
   * @return The 'JCheckBox' object used by this panel.
   */
  public JCheckBox getCheckBoxItemObj()
  {
    return checkBoxItemObj;
  }
}
