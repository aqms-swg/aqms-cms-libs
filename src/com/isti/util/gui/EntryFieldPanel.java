//EntryFieldPanel.java:  Defines a single-line entry panel.
//
//  4/26/2004 -- [ET]
//

package com.isti.util.gui;

import java.awt.Component;
import java.awt.BorderLayout;
import java.awt.GridBagLayout;
import java.awt.GridBagConstraints;
import java.awt.Insets;
import javax.swing.*;

/**
 * Class EntryFieldPanel defines a single-line entry panel.
 */
public class EntryFieldPanel extends JPanel
{
  /** Default horizontal gap value. */
  public static final int DEF_HORIZ_GAP = 7;
  /** Default vertical gap value. */
  public static final int DEF_VERT_GAP = 12;
  /** Text-edit field for panel. */
  protected final JTextField textEditFieldObj = new JTextField();

  /**
   * Creates a single-line entry panel.
   * @param msgObj the message object to show above the text-edit field;
   * may be a 'String', a 'Component', or null for none.
   * @param promptObj the prompt object to show to the left of the
   * text-edit field; may be a 'String', a 'Component', or null for none.
   * @param bottomObj the object to show below the text-edit field;
   * may be a 'String', a 'Component', or null for none.
   * @param horizGap the horizontal gap to use in the layout.
   * @param vertGap the vertical gap to use in the layout.
   */
  public EntryFieldPanel(Object msgObj, Object promptObj, Object bottomObj,
                                                  int horizGap, int vertGap)
  {
    setLayout(new BorderLayout(horizGap,vertGap));
                   //add some margin space (if top/bottom objects given):
    setBorder(BorderFactory.createEmptyBorder(
                         ((msgObj!=null)?5:0),0,((bottomObj!=null)?5:0),0));
         //setup message-component object:
    final Component msgCompObj;
    if(msgObj instanceof String)
      msgCompObj = new MultiLineJLabel((String)msgObj);
    else if(msgObj instanceof Component)
      msgCompObj = (Component)msgObj;
    else if(msgObj == null)
      msgCompObj = new JLabel();
    else
    {
      throw new IllegalArgumentException("Message-object parameter must " +
                                      "be of type 'String' or 'Component'");
    }
         //setup prompt-component object:
    final Component promptCompObj;
    if(promptObj instanceof String)
      promptCompObj = new JLabel((String)promptObj);
    else if(promptObj instanceof Component)
      promptCompObj = (Component)promptObj;
    else if(promptObj == null)
      promptCompObj = null;
    else
    {
      throw new IllegalArgumentException("Prompt-object parameter must " +
                                      "be of type 'String' or 'Component'");
    }
         //setup message-component object:
    final Component bottomCompObj;
    if(bottomObj instanceof String)
      bottomCompObj = new MultiLineJLabel((String)bottomObj);
    else if(bottomObj instanceof Component)
      bottomCompObj = (Component)bottomObj;
    else if(bottomObj == null)
      bottomCompObj = new JLabel();
    else
    {
      throw new IllegalArgumentException("Bottom-object parameter must " +
                                      "be of type 'String' or 'Component'");
    }

              //setup GridBagConstraints to fill horizontally:
    final GridBagConstraints constraintsObj = new GridBagConstraints(0,0,
          1,1,1.0,0.0,GridBagConstraints.WEST,GridBagConstraints.HORIZONTAL,
                                                   new Insets(0,0,0,0),0,0);

              //put message-object into a panel to center it vertically
              // (use GridBagLayout to make panel fill horizontally):
    final JPanel msgPanelObj = new JPanel(new GridBagLayout());
    msgPanelObj.add(msgCompObj,constraintsObj);

              //put text-edit-field in panel to prevent vertical fill:
    final JPanel editPanelObj = new JPanel(new GridBagLayout());
    editPanelObj.add(textEditFieldObj,constraintsObj);

         //add components and text-edit-field to panel:
    add(msgPanelObj,BorderLayout.NORTH);
    if(promptCompObj != null)
      add(promptCompObj,BorderLayout.WEST);
    add(editPanelObj,BorderLayout.CENTER);
    add(bottomCompObj,BorderLayout.SOUTH);
  }

  /**
   * Creates a single-line entry panel.
   * @param msgObj the message object to show above the text-edit field;
   * may be a 'String', a 'Component', or null for none.
   * @param promptObj the prompt object to show to the left of the
   * text-edit field; may be a 'String', a 'Component', or null for none.
   * @param bottomObj the object to show below the text-edit field;
   * may be a 'String', a 'Component', or null for none.
   */
  public EntryFieldPanel(Object msgObj, Object promptObj, Object bottomObj)
  {
    this(msgObj,promptObj,bottomObj,DEF_HORIZ_GAP,DEF_VERT_GAP);
  }

  /**
   * Creates a single-line entry panel.
   * @param msgObj the message object to show above the text-edit field;
   * may be a 'String', a 'Component', or null for none.
   * @param promptObj the prompt object to show to the left of the
   * text-edit field; may be a 'String', a 'Component', or null for none.
   */
  public EntryFieldPanel(Object msgObj, Object promptObj)
  {
    this(msgObj,promptObj,null,DEF_HORIZ_GAP,DEF_VERT_GAP);
  }

  /**
   * Creates a single-line entry panel.
   * @param msgObj the message object to show above the text-edit field;
   * may be a 'String', a 'Component', or null for none.
   */
  public EntryFieldPanel(Object msgObj)
  {
    this(msgObj,null,null,DEF_HORIZ_GAP,DEF_VERT_GAP);
  }

  /**
   * Creates a single-line entry panel.
   */
  public EntryFieldPanel()
  {
    this(null,null,null,DEF_HORIZ_GAP,DEF_VERT_GAP);
  }

  /**
   * Displays this entry-line panel in a popup dialog.  This method
   * blocks until the dialog is closed, and then it returns the contents
   * of the text-edit field.
   * @param parentComp the parent component for the popup.
   * @param titleStr the title string for popup window.
   * @param optionType specifies what default option buttons to
   * automatically create if 'optionsArr' is null or empty; one of:
   * DEFAULT_OPTION, YES_NO_OPTION, YES_NO_CANCEL_OPTION, OK_CANCEL_OPTION
   * or NO_AUTOBUTTONS_OPTION.
   * @param msgType the type of message to be displayed (primarily used
   * to determine the icon from the pluggable Look and Feel):
   * ERROR_MESSAGE, INFORMATION_MESSAGE, WARNING_MESSAGE,
   * QUESTION_MESSAGE, or PLAIN_MESSAGE.
   * @param modalFlag true for modal, false for modeless (allows other
   * windows to run).
   * @return The contents of the text-edit field if the user hit the "OK"
   * button; null if not.
   */
  public String showPanelInDialog(Component parentComp, String titleStr,
                               int optionType,int msgType,boolean modalFlag)
  {
    final IstiDialogPopup popupObj = new IstiDialogPopup(parentComp,this,
                                     titleStr,msgType,modalFlag,optionType);
    popupObj.setRepackNeededFlag(true);     //setup to repack after shown
         //setup to have text field get initial keyboard focus:
    popupObj.setInitialFocusComponent(textEditFieldObj);
         //show dialog and then check return value:
    if(popupObj.showAndReturnIndex() != IstiDialogPopup.OK_OPTION)
      return null;      //if not "OK" button then return null
    return textEditFieldObj.getText();      //return text-field contents
  }

  /**
   * Displays this entry-line panel in a popup dialog, with "OK" and
   * "Cancel" buttons.  This method blocks until the dialog is closed,
   * and then it returns the contents of the text-edit field.
   * @param parentComp the parent component for the popup.
   * @param titleStr the title string for popup window.
   * @param msgType the type of message to be displayed (primarily used
   * to determine the icon from the pluggable Look and Feel):
   * ERROR_MESSAGE, INFORMATION_MESSAGE, WARNING_MESSAGE,
   * QUESTION_MESSAGE, or PLAIN_MESSAGE.
   * @param modalFlag true for modal, false for modeless (allows other
   * windows to run).
   * @return The contents of the text-edit field if the user hit the "OK"
   * button; null if not.
   */
  public String showPanelInDialog(Component parentComp, String titleStr,
                                              int msgType,boolean modalFlag)
  {
    return showPanelInDialog(parentComp,titleStr,
                        IstiDialogPopup.OK_CANCEL_OPTION,msgType,modalFlag);
  }

  /**
   * Displays this entry-line panel in a popup dialog, with "OK" and
   * "Cancel" buttons and a "PLAIN_MESSAGE" style of window.  This method
   * blocks until the dialog is closed, and then it returns the contents
   * of the text-edit field.
   * @param parentComp the parent component for the popup.
   * @param titleStr the title string for popup window.
   * @param modalFlag true for modal, false for modeless (allows other
   * windows to run).
   * @return The contents of the text-edit field if the user hit the "OK"
   * button; null if not.
   */
  public String showPanelInDialog(Component parentComp, String titleStr,
                                                          boolean modalFlag)
  {
    return showPanelInDialog(parentComp,titleStr,
             IstiDialogPopup.OK_CANCEL_OPTION,IstiDialogPopup.PLAIN_MESSAGE,
                                                                 modalFlag);
  }

  /**
   * Displays this entry-line panel in a popup dialog, with "OK" and
   * "Cancel" buttons and a "PLAIN_MESSAGE" style, modal window.  This
   * method blocks until the dialog is closed, and then it returns the
   * contents of the text-edit field.
   * @param parentComp the parent component for the popup.
   * @param titleStr the title string for popup window.
   * @return The contents of the text-edit field if the user hit the "OK"
   * button; null if not.
   */
  public String showPanelInDialog(Component parentComp, String titleStr)
  {
    return showPanelInDialog(parentComp,titleStr,
             IstiDialogPopup.OK_CANCEL_OPTION,IstiDialogPopup.PLAIN_MESSAGE,
                                                                      true);
  }

  /**
   * Returns the contents of the text-edit-field object used by this panel.
   * @return The contents of the text-edit-field object used by this panel.
   */
  public String getEditFieldText()
  {
    return textEditFieldObj.getText();
  }

  /**
   * Sets the contents of the text-edit-field object used by this panel.
   * @param str the contents string to use.
   */
  public void setEditFieldText(String str)
  {
    textEditFieldObj.setText(str);
  }

  /**
   * Returns the text-edit-field object used by this panel.
   * @return The 'JTextField' object used by this panel.
   */
  public JTextField getTextEditFieldObj()
  {
    return textEditFieldObj;
  }
}
