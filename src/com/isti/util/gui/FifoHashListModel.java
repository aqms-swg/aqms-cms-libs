//FifoHashListModel.java:  Encasulates a 'FifoHashtable' object and uses
//                         it in a 'ListModel' implementation suitable for
//                         use in a 'JList'.
//
//   3/6/2003 -- [ET]  Initial version.
// 10/14/2003 -- [ET]  Moved from "com.isti.util" to "com.isti.util.gui"
//                     package; modified to be not thread synchronized
//                     and to use 'UnsyncFifoHashtable' internally.
// 10/15/2003 -- [ET]  Renamed 'remove()' to 'removeKey()'.
//  12/7/2005 -- [KF]  Added support for 'ExtendedComparable'.
//

package com.isti.util.gui;

import java.util.Vector;
import java.util.Enumeration;
import java.util.Collection;
import java.util.Comparator;
import java.util.Map;
import java.util.Iterator;
import javax.swing.AbstractListModel;
import com.isti.util.UnsyncFifoHashtable;
import com.isti.util.ExtendedComparable;
import java.util.HashMap;

/**
 * Class FifoHashListModel encasulates an 'UnsyncFifoHashtable' object and
 * uses it in a 'ListModel' implementation suitable for use in a 'JList'.
 * Methods are not thread synchronized.
 */
public class FifoHashListModel extends AbstractListModel
{
  protected final UnsyncFifoHashtable hashtableObj = new UnsyncFifoHashtable();
  protected boolean listenersEnabledFlag = true;
//  protected static final boolean DEBUG_FLG = true;

  /**
   * Returns the current size of the hashtable table data held by this
   * object.
   * @return The size value.
   */
  public int getSize()
  {
//    final int sizeVal = hashtableObj.size();
//    if(DEBUG_FLG)
//    {
//      System.out.println("FifoHashListModelDEBUG:  getSize() returned " +
//                                                                   sizeVal);
//    }
//    return sizeVal;
    return hashtableObj.size();
  }

  /**
   * Returns the element at the given index value.
   * @param indexVal the index value to use.
   * @return The element object.
   * @exception ArrayIndexOutOfBoundsException if the index is negative
   * or not less than the current size of the hashtable data.
   */
  public Object getElementAt(int indexVal)
  {
//    if(DEBUG_FLG)
//    {
//      final int sizeVal = hashtableObj.size();
//      if(indexVal < sizeVal)
//      {
//        System.out.println("FifoHashListModelDEBUG:  getElementAt(" +
//                                           indexVal + "), size=" + sizeVal);
//      }
//      else
//      {
//        System.err.println("FifoHashListModelDEBUG:  *** Out of Bounds " +
//                     "*** getElementAt(" + indexVal + "), size=" + sizeVal);
//      }
//    }
    return hashtableObj.elementAt(indexVal);
  }

  /**
   * Associates the specified value with the specified key in the
   * hashtable data.  If the hashtable previously contained a mapping for
   * this key, the old value is replaced.
   * @param key key with which the specified value is to be associated.
   * @param value value to be associated with the specified key.
   * @return previous value associated with specified key, or <tt>null</tt>
   *	       if there was no mapping for key.  A <tt>null</tt> return can
   *	       also indicate that the HashMap previously associated
   *	       <tt>null</tt> with the specified key.
   */
  public Object put(Object key,Object value)
  {
    final int preIdx = hashtableObj.indexOfKey(key);
    final Object retObj = hashtableObj.put(key,value);     //put into table
    fireItemPut(preIdx,key);      //notify listeners
    return retObj;
  }

  /**
   * Copies all of the mappings from the specified map into the table.
   * These mappings replace any mappings that this table had for any of the
   * keys currently in the specified Map.
   * @param mapObj mappings to be stored in this map.
   */
  public void putAll(Map mapObj)
  {
    final Iterator i = mapObj.entrySet().iterator();
    Map.Entry e;
    while (i.hasNext())
    {
      e = (Map.Entry)i.next();
      hashtableObj.put(e.getKey(),e.getValue());
    }
    if(listenersEnabledFlag)    //if listeners enabled then
      fireContentsChanged();    //notify listeners
  }

  /**
   * Adds a key/value pair to the hashtable data, placing it in sorted
   * order according to the key.  The key should implement the Comparable
   * interface.  Associates the specified value with the specified key in
   * this hashtable.  If the table previously contained a mapping for
   * this key, the old value is replaced.
   * @param key key with which the specified value is to be associated.
   * @param value value to be associated with the specified key.
   * @param sortDirFlag true for ascending sort order, false for
   * descending.
   * @return previous value associated with specified key, or <tt>null</tt>
   *	       if there was no mapping for key.  A <tt>null</tt> return can
   *	       also indicate that the HashMap previously associated
   *	       <tt>null</tt> with the specified key.
   */
  public Object putSort(Object key,Object value,
                                                        boolean sortDirFlag)
  {
    return putSort(key,value,sortDirFlag,ExtendedComparable.NO_SORT_TYPE);
  }

  /**
   * Adds a key/value pair to the hashtable data, placing it in sorted
   * order according to the key.  The key should implement the Comparable
   * interface.  Associates the specified value with the specified key in
   * this hashtable.  If the table previously contained a mapping for
   * this key, the old value is replaced.
   * @param key key with which the specified value is to be associated.
   * @param value value to be associated with the specified key.
   * @param sortDirFlag true for ascending sort order, false for
   * descending.
   * @param sortType the sort type.
   * @return previous value associated with specified key, or <tt>null</tt>
   *	       if there was no mapping for key.  A <tt>null</tt> return can
   *	       also indicate that the HashMap previously associated
   *	       <tt>null</tt> with the specified key.
   */
  public Object putSort(Object key,Object value,
                        boolean sortDirFlag,int sortType)
  {
    final int preIdx = hashtableObj.indexOfKey(key);
    final Object retObj = hashtableObj.putSort(key,value,sortDirFlag,sortType);
    fireItemPut(preIdx,key);      //notify listeners
    return retObj;
  }

  /**
   * Adds a key/value pair to the hashtable table, placing it in sorted
   * order according to the key.  The key should implement the Comparable
   * interface.  Associates the specified value with the specified key in
   * this hashtable.  If the table previously contained a mapping for
   * this key, the old value is replaced.
   * @param key key with which the specified value is to be associated.
   * @param value value to be associated with the specified key.
   * @param sortDirFlag true for ascending sort order, false for
   * descending.
   * @param compObj the comparator to use for sorting the 'key' objects,
   * or null to use the "natural" sort order.
   * @return previous value associated with specified key, or <tt>null</tt>
   *	       if there was no mapping for key.  A <tt>null</tt> return can
   *	       also indicate that the HashMap previously associated
   *	       <tt>null</tt> with the specified key.
   */
  public Object putSort(Object key,Object value,
                                     boolean sortDirFlag,Comparator compObj)
  {
    final int preIdx = hashtableObj.indexOfKey(key);
    final Object retObj = hashtableObj.putSort(       //add to table
                                             key,value,sortDirFlag,compObj);
    fireItemPut(preIdx,key);      //notify listeners
    return retObj;
  }

  /**
   * Copies all of the mappings from the specified map into the table,
   * placing them in sorted order according to the keys.  The keys
   * should implement the Comparable interface.  These mappings replace
   * any mappings that this table had for any of the keys currently in
   * the specified Map.
   * @param mapObj mappings to be stored in this map.
   * @param sortDirFlag true for ascending sort order, false for
   * descending.
   */
  public void putSortAll(Map mapObj,boolean sortDirFlag)
  {
    putSortAll(mapObj,sortDirFlag,ExtendedComparable.NO_SORT_TYPE);
  }

  /**
   * Copies all of the mappings from the specified map into the table,
   * placing them in sorted order according to the keys.  The keys
   * should implement the Comparable interface.  These mappings replace
   * any mappings that this table had for any of the keys currently in
   * the specified Map.
   * @param mapObj mappings to be stored in this map.
   * @param sortDirFlag true for ascending sort order, false for
   * descending.
   * @param sortType the sort type.
   */
  public void putSortAll(Map mapObj,boolean sortDirFlag,int sortType)
  {
    final Iterator i = mapObj.entrySet().iterator();
    Map.Entry e;
    while (i.hasNext())
    {
      e = (Map.Entry)i.next();
      hashtableObj.putSort(e.getKey(),e.getValue(),sortDirFlag,sortType);
    }
    if(listenersEnabledFlag)    //if listeners enabled then
      fireContentsChanged();    //notify listeners
  }

  /**
   * Copies all of the mappings from the specified map into the table,
   * placing them in sorted order according to the keys.  The keys
   * should implement the Comparable interface.  These mappings replace
   * any mappings that this table had for any of the keys currently in
   * the specified Map.
   * @param mapObj mappings to be stored in this map.
   * @param sortDirFlag true for ascending sort order, false for
   * descending.
   * @param compObj the comparator to use for sorting the 'key' objects,
   * or null to use the "natural" sort order.
   */
  public void putSortAll(Map mapObj,boolean sortDirFlag,Comparator compObj)
  {
    final Iterator i = mapObj.entrySet().iterator();
    Map.Entry e;
    while (i.hasNext())
    {
      e = (Map.Entry)i.next();
      hashtableObj.putSort(e.getKey(),e.getValue(),sortDirFlag,compObj);
    }
    if(listenersEnabledFlag)    //if listeners enabled then
      fireContentsChanged();    //notify listeners
  }

  /**
   * Adds a key/value pair to the hashtable data, placing it in sorted
   * order according to the value.  The value should implement the
   * Comparable interface.  Associates the specified value with the
   * specified key in this hashtable.  If the table previously
   * contained a mapping for this key, the old value is deleted and
   * the new value is placed in proper sort order.
   * @param key key with which the specified value is to be associated.
   * @param value value to be associated with the specified key.
   * @param sortDirFlag true for ascending sort order, false for
   * descending.
   * @return previous value associated with specified key, or <tt>null</tt>
   *	       if there was no mapping for key.  A <tt>null</tt> return can
   *	       also indicate that the HashMap previously associated
   *	       <tt>null</tt> with the specified key.
   */
  public Object putSortByValue(Object key,Object value,
                                                        boolean sortDirFlag)
  {
    return putSortByValue(key,value,sortDirFlag,ExtendedComparable.NO_SORT_TYPE);
  }

  /**
   * Adds a key/value pair to the hashtable data, placing it in sorted
   * order according to the value.  The value should implement the
   * Comparable interface.  Associates the specified value with the
   * specified key in this hashtable.  If the table previously
   * contained a mapping for this key, the old value is deleted and
   * the new value is placed in proper sort order.
   * @param key key with which the specified value is to be associated.
   * @param value value to be associated with the specified key.
   * @param sortDirFlag true for ascending sort order, false for
   * descending.
   * @param sortType the sort type.
   * @return previous value associated with specified key, or <tt>null</tt>
   *	       if there was no mapping for key.  A <tt>null</tt> return can
   *	       also indicate that the HashMap previously associated
   *	       <tt>null</tt> with the specified key.
   */
  public Object putSortByValue(Object key,Object value,
                               boolean sortDirFlag,int sortType)
  {
    final int preIdx = hashtableObj.indexOfKey(key);
    final Object retObj =
        hashtableObj.putSortByValue(key,value,sortDirFlag,sortType);
    fireItemPut(preIdx,key);      //notify listeners
    return retObj;
  }

  /**
   * Adds a key/value pair to the hashtable, placing it in sorted
   * order according to the value.  The value should implement the
   * Comparable interface.  Associates the specified value with the
   * specified key in this hashtable.  If the table previously
   * contained a mapping for this key, the old value is deleted and
   * the new value is placed in proper sort order.
   * @param key key with which the specified value is to be associated.
   * @param value value to be associated with the specified key.
   * @param sortDirFlag true for ascending sort order, false for
   * descending.
   * @param compObj the comparator to use for sorting the 'key' objects,
   * or null to use the "natural" sort order.
   * @return previous value associated with specified key, or <tt>null</tt>
   *	       if there was no mapping for key.  A <tt>null</tt> return can
   *	       also indicate that the HashMap previously associated
   *	       <tt>null</tt> with the specified key.
   */
  public Object putSortByValue(Object key,Object value,
                                     boolean sortDirFlag,Comparator compObj)
  {
    final int preIdx = hashtableObj.indexOfKey(key);
    final Object retObj = hashtableObj.putSortByValue(     //add to table
                                             key,value,sortDirFlag,compObj);
    fireItemPut(preIdx,key);      //notify listeners
    return retObj;
  }

  /**
   * Copies all of the mappings from the specified map into the table,
   * placing them in sorted order according to the values.  The values
   * should implement the Comparable interface.  These mappings replace
   * any mappings that this table had for any of the keys currently in
   * the specified Map.
   * @param mapObj mappings to be stored in this map.
   * @param sortDirFlag true for ascending sort order, false for
   * descending.
   */
  public void putSortByValueAll(Map mapObj,boolean sortDirFlag)
  {
    putSortByValueAll(mapObj,sortDirFlag,ExtendedComparable.NO_SORT_TYPE);
  }

  /**
   * Copies all of the mappings from the specified map into the table,
   * placing them in sorted order according to the values.  The values
   * should implement the Comparable interface.  These mappings replace
   * any mappings that this table had for any of the keys currently in
   * the specified Map.
   * @param mapObj mappings to be stored in this map.
   * @param sortDirFlag true for ascending sort order, false for
   * descending.
   * @param sortType the sort type.
   */
  public void putSortByValueAll(Map mapObj,boolean sortDirFlag,int sortType)
  {
    final Iterator i = mapObj.entrySet().iterator();
    Map.Entry e;
    while (i.hasNext())
    {
      e = (Map.Entry)i.next();
      hashtableObj.putSortByValue(e.getKey(),e.getValue(),sortDirFlag,sortType);
    }
    if(listenersEnabledFlag)    //if listeners enabled then
      fireContentsChanged();    //notify listeners
  }

  /**
   * Copies all of the mappings from the specified map into the table,
   * placing them in sorted order according to the values.  The values
   * should implement the Comparable interface.  These mappings replace
   * any mappings that this table had for any of the keys currently in
   * the specified Map.
   * @param mapObj mappings to be stored in this map.
   * @param sortDirFlag true for ascending sort order, false for
   * descending.
   * @param compObj the comparator to use for sorting the 'value' objects,
   * or null to use the "natural" sort order.
   */
  public void putSortByValueAll(Map mapObj,boolean sortDirFlag,
                                                         Comparator compObj)
  {
    final Iterator i = mapObj.entrySet().iterator();
    Map.Entry e;
    while (i.hasNext())
    {
      e = (Map.Entry)i.next();
      hashtableObj.putSortByValue(e.getKey(),e.getValue(),sortDirFlag,
                                                                   compObj);
    }
    if(listenersEnabledFlag)    //if listeners enabled then
      fireContentsChanged();    //notify listeners
  }

  /**
   * Returns the value to which the hashtable data maps the specified key.
   * Returns <tt>null</tt> if the hashtable contains no mapping for this
   * key.  A return value of <tt>null</tt> does not <i>necessarily</i>
   * indicate that the hashtable contains no mapping for the key; it's
   * also possible that the hashtable explicitly maps the key to
   * <tt>null</tt>.  The <tt>containsKey</tt> operation may be used
   * to distinguish these two cases.
   * @return the value to which this hashtable maps the specified key.
   * @param key key whose associated value is to be returned.
   */
  public Object get(Object key)
  {
    return hashtableObj.get(key);
  }

  /**
   * Removes the mapping for the given key from this hashtable if present.
   * @param key key whose mapping is to be removed from the hashtable.
   * @return previous value associated with specified key, or <tt>null</tt>
   *	       if there was no mapping for key.  A <tt>null</tt> return can
   *	       also indicate that the hashtable previously associated
   *         <tt>null</tt> with the specified key.
   */
  public Object removeKey(Object key)
  {
    final int idx = hashtableObj.indexOfKey(key);     //get index for key
//    if(DEBUG_FLG)
//    {
//      System.out.println("FifoHashListModelDEBUG:  removing key (" + idx +
//                                                     "):  \"" + key + "\"");
//    }
    final Object retObj = hashtableObj.remove(key);   //remove key from table
    if(listenersEnabledFlag && idx >= 0)    //if listeners enabled & idx OK
      fireIntervalRemoved(this,idx,idx);    // then notify listeners
    return retObj;
  }

  /**
   * Removes the value and its key from this hashtable data if present.
   * @param obj value to be removed from the hashtable.
   * @return previous key associated with specified value, or <tt>null</tt>
   *	       if the value was not found.  A <tt>null</tt> return can
   *	       also indicate that the hashtable previously associated
   *         <tt>null</tt> with the specified value.
   */
  public Object removeValue(Object obj)
  {
    final int idx = hashtableObj.indexOf(obj);   //get index for value
//    if(DEBUG_FLG)
//    {
//      System.out.println("FifoHashListModelDEBUG:  removing value (" + idx +
//                                                     "):  \"" + obj + "\"");
//    }
    final Object retObj = hashtableObj.removeValue(obj);
    if(listenersEnabledFlag && idx >= 0)    //if listeners enabled & idx OK
      fireIntervalRemoved(this,idx,idx);    // then notify listeners
    return retObj;
  }

  /**
   * Removes the entry at the specified index in the hashtable data.
   * Each entry in the table with an index greater than the
   * specified index is shifted downward to have an index one smaller
   * than it had previously. The size of the table is decreased by 1.
   * @param indexVal the specified index; must be greater than or equal to
   * zero and less than the size of the table.
   * @exception ArrayIndexOutOfBoundsException if the index is negative
   * or not less than the current size of the table.
   */
  public void removeElementAt(int indexVal)
  {
//    if(DEBUG_FLG)
//    {
//      System.out.println("FifoHashListModelDEBUG:  removing element at " +
//                                                                  indexVal);
//    }
    hashtableObj.removeElementAt(indexVal);           //remove element
    if(listenersEnabledFlag)                          //if enabled then
      fireIntervalRemoved(this,indexVal,indexVal);    //notify listeners
  }

  /**
   * Resorts the list with the specified direction and type.
   * @param sortDirFlag true for ascending sort order, false for
   * descending.
   * @param sortType the sort type.
   */
  public void reSortList(final boolean sortDirFlag,final int sortType)
  {
    final HashMap tableMapObj = new HashMap(hashtableObj);
    hashtableObj.clear();                      //clear table
    putSortByValueAll(tableMapObj,sortDirFlag,sortType);
  }

  /**
   * Tests if the specified object is a key in the hashtable data.
   * @param   key   possible key.
   * @return  <code>true</code> if and only if the specified object
   *          is a key in the hashtable data, as determined by the
   *          <tt>equals</tt> method; <code>false</code> otherwise.
   */
  public boolean containsKey(Object key)
  {
    return hashtableObj.containsKey(key);
  }

  /**
   * Returns true if the hashtable data maps one or more keys to this value.
   * This operation is more expensive than the 'containsKey()' method.
   * Note that this method is identical in functionality to 'contains()'
   * (which predates the Map interface).
   * @param value the value to use.
   * @return     <code>true</code> if and only if some key maps to the
   *             <code>value</code> argument in the hashtable data as
   *             determined by the <tt>equals</tt> method;
   *             <code>false</code> otherwise.
   */
  public boolean containsValue(Object value)
  {
    return hashtableObj.containsValue(value);
  }

  /**
   * Tests if some key maps into the specified value in the hashtable data.
   * This operation is more expensive than the 'containsKey()' method.
   * Note that this method is identical in functionality to
   * 'containsValue()' (which is part of the Map interface in
   * the collections framework).
   * @param value the value to use.
   * @return     <code>true</code> if and only if some key maps to the
   *             <code>value</code> argument in the hashtable data as
   *             determined by the <tt>equals</tt> method;
   *             <code>false</code> otherwise.
   */
  public boolean contains(Object value)
  {
    return hashtableObj.containsValue(value);
  }

  /**
   * Searches for the first occurrence of the given argument, testing for
   * equality using the equals method.
   * @param obj the object to find.
   * @return The index of the first occurrence of the argument in the
   * hashtable data; returns -1 if the object is not found.
   */
  public int indexOf(Object obj)
  {
    return hashtableObj.indexOf(obj);
  }

  /**
   * Searches for the first occurrence of the given argument, beginning
   * the search at 'idx', testing for equality using the equals method.
   * @param obj the object to find.
   * @param idx the starting search position.
   * @return The index of the first occurrence of the argument at
   * position 'idx' or later in the hashtable data; returns -1 if
   * the object is not found.
   */
//  public int indexOf(Object obj,int idx)
//  {
//    return hashtableObj.indexOf(obj,idx);
//  }

  /**
   * Searches for the last occurrence of the given argument, testing for
   * equality using the equals method.
   * @param obj the object to find.
   * @return The index of the last occurrence of the argument in the
   * hashtable data; returns -1 if the object is not found.
   */
  public int lastIndexOf(Object obj)
  {
    return hashtableObj.lastIndexOf(obj);
  }

  /**
   * Searches for the last occurrence of the given argument, beginning
   * the search at 'idx', testing for equality using the equals method.
   * @param obj the object to find.
   * @param idx the starting search position.
   * @return The index of the last occurrence of the argument at
   * position 'idx' or earlier in the hashtable data; returns -1 if
   * the object is not found.
   */
//  public int lastIndexOf(Object obj,int idx)
//  {
//    return hashtableObj.lastIndexOf(obj,idx);
//  }

  /**
   * Searches for the first occurrence of the given key, testing for
   * equality using the equals method.
   * @param obj the key to find.
   * @return The index of the first occurrence of the key in the
   * hashtable data; returns -1 if the object is not found.
   */
  public int indexOfKey(Object obj)
  {
    return hashtableObj.indexOfKey(obj);
  }

  /**
   * Searches for the first occurrence of the given key, beginning
   * the search at 'idx', testing for equality using the equals method.
   * @param obj the key to find.
   * @param idx the starting search position.
   * @return The index of the first occurrence of the key at
   * position 'idx' or later in the hashtable data; returns -1 if
   * the object is not found.
   */
//  public int indexOfKey(Object obj,int idx)
//  {
//    return hashtableObj.indexOfKey(obj,idx);
//  }

  /**
   * Searches for the last occurrence of the given key, testing for
   * equality using the equals method.
   * @param obj the key to find.
   * @return The index of the last occurrence of the key in the
   * hashtable data; returns -1 if the object is not found.
   */
  public int lastIndexOfKey(Object obj)
  {
    return hashtableObj.lastIndexOfKey(obj);
  }

  /**
   * Searches for the last occurrence of the given key, beginning
   * the search at 'idx', testing for equality using the equals method.
   * @param obj the key to find.
   * @param idx the starting search position.
   * @return The index of the last occurrence of the key at
   * position 'idx' or earlier in the hashtable data; returns -1 if
   * the object is not found.
   */
//  public int lastIndexOfKey(Object obj,int idx)
//  {
//    return hashtableObj.lastIndexOfKey(obj,idx);
//  }

  /**
   * Returns the key at the specified index in the hashtable data.
   * @param idx the index value to use.
   * @return the key object associated with the given index.
   * @exception ArrayIndexOutOfBoundsException if the index is negative
   * or not less than the current size of the hashtable data.
   */
  public Object keyAt(int idx)
  {
    return hashtableObj.keyAt(idx);
  }

    /**
     * Clears the hashtable data.
     */
  public void clear()
  {
    final int sizeVal;
    if((sizeVal=hashtableObj.size()) > 0)
    {    //hashtable contains data
      hashtableObj.clear();                      //clear table
      if(listenersEnabledFlag)                   //if enabled then
        fireIntervalRemoved(this,0,sizeVal-1);   //notify listeners
    }
  }

  /**
   * Returns an enumeration of the values in the hashtable data.  The values
   * will be enumerated in same order as they were added to the table.
   * The data referenced by the the returned enumeration will not be
   * affected by later modifications to the hashtable.
   * @return An enumeration of the values in the hashtable data.
   */
  public Enumeration elements()
  {
    return hashtableObj.elements();
  }

  /**
   * Returns a new Vector containing the values in the hashtable data.  The
   * values will enumerated in same order as they were added to the table.
   * The data in the the returned Vector will not be affected by later
   * modifications to the hashtable, and the hashtable will not be
   * affected by modifications to the returned Vector.
   * @return A new Vector containing the values in the hashtable data.
   */
  public Vector getValuesVector()
  {
    return hashtableObj.getValuesVector();
  }

  /**
   * Returns a read-only collection view of the values contained in the
   * hashtable data.  The collection does not support any add or remove
   * operations.
   * @return A collection view of the values contained in this map.
   */
  public Collection values()
  {
    return hashtableObj.values();
  }

  /**
   * Sets whether or not attached 'ListDataListener' objects are called
   * when changes occur.
   * @param flgVal true to enable calling of 'ListDataListener' objects,
   * false to disable.
   */
  public void setListenersEnabled(boolean flgVal)
  {
    listenersEnabledFlag = flgVal;
  }

  /**
   * Fires a contents-changed event.
   */
  public void fireContentsChanged()
  {
    final int len;
    super.fireContentsChanged(this,0,  //indicate all elements changed
                               (((len=hashtableObj.size()) > 0) ? len : 0));
  }

  /**
   * Notifies listeners of a 'put' operation.  If the index of the given
   * key has changed then 'removed' and 'added' change events are fired.
   * @param preIdx the previous index of the given key (before the 'put'
   * operation) or -1 if not previously in table.
   * @param key the key object to use.
   */
  protected void fireItemPut(int preIdx,Object key)
  {
    if(listenersEnabledFlag)
    {    //listeners are enabled
      final int idx;    //get index now associated with given key:
      if((idx=hashtableObj.indexOfKey(key)) >= 0)
      {  //key exists in table
        if(preIdx >= 0)
        {     //key existed in table before latest 'put' operation
          if(idx == preIdx)
          {   //index of key not changed
            fireContentsChanged(this,idx,idx);   //notify listeners
            return;
          }
              //index of key changed; notify listeners of removal:
          fireIntervalRemoved(this,preIdx,preIdx);
        }
        fireIntervalAdded(this,idx,idx);         //notify listeners of add
      }
    }
  }
}
