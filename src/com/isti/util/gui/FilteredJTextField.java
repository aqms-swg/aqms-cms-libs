//FilteredJTextField.java:  Extension of 'JTextField' which only allows
//                          certain characters to be entered into it.
//
//   10/3/2000 -- [ET]  Initial release version.
//    2/8/2002 -- [ET]  Added optional 'maxNumChars' parameter to specify
//                      maximum number of characters allowed.
//    7/1/2002 -- [ET]  Added more static "..._CHARS" strings; added
//                      'setMaxValue()' methods and support.
//  10/17/2002 -- [KF]  Added support for HEX numbers: HEX_CHARS.
//  10/23/2002 -- [KF]  Added 'getIntegerValue' method.
//   5/28/2003 -- [KF]  Added constructor with columns and allowed chars.
//    8/5/2004 -- [KF]  Added 'allowFlag' parameter to supply a list of
//                      characters that should not be allowed.
//  12/19/2005 -- [KF]  Added 'getAllowedNumberChars()' method and a
//                      numeric only constructor to support other locales.
//  12/20/2005 -- [KF]  Fixed to support other locales with maximum values.
//  12/23/2005 -- [KF]  Changed to allow the decimal separator character dynamically.
//
//
//=====================================================================
// Copyright (C) 2005 Instrumental Software Technologies, Inc.
//
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions
// are met:
// 1. Redistributions of source code, or portions of this source code,
//    must retain the above copyright notice, this list of conditions
//    and the following disclaimer.
// 2. Redistributions in binary form must reproduce the above copyright
//    notice, this list of conditions and the following disclaimer in
//    the documentation and/or other materials provided with the
//    distribution.
// 3. All advertising materials mentioning features or use of this
//    software must display the following acknowledgment:
//    "This product includes software developed by Instrumental
//    Software Technologies, Inc. (http://www.isti.com)"
// 4. If the software is provided with, or as part of a commercial
//    product, or is used in other commercial software products the
//    customer must be informed that "This product includes software
//    developed by Instrumental Software Technologies, Inc.
//    (http://www.isti.com)"
// 5. The names "Instrumental Software Technologies, Inc." and "ISTI"
//    must not be used to endorse or promote products derived from
//    this software without prior written permission. For written
//    permission, please contact "info@isti.com".
// 6. Products derived from this software may not be called "ISTI"
//    nor may "ISTI" appear in their names without prior written
//    permission of Instrumental Software Technologies, Inc.
// 7. Redistributions of any form whatsoever must retain the following
//    acknowledgment:
//    "This product includes software developed by Instrumental
//    Software Technologies, Inc. (http://www.isti.com/)."
// THIS SOFTWARE IS PROVIDED BY INSTRUMENTAL SOFTWARE
// TECHNOLOGIES, INC. "AS IS" AND ANY EXPRESSED OR IMPLIED
// WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES
// OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
// DISCLAIMED.  IN NO EVENT SHALL INSTRUMENTAL SOFTWARE TECHNOLOGIES,
// INC. OR ITS CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
// INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
// (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
// SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
// HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
// STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
// ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED
// OF THE POSSIBILITY OF SUCH DAMAGE.
//=====================================================================
//  A current version of the software can be found at
//                http://www.isti.com
//  Bug reports and comments should be directed to
//  Instrumental Software Technologies, Inc. at info@isti.com
//=====================================================================
//

package com.isti.util.gui;

import javax.swing.JTextField;
import javax.swing.text.AttributeSet;
import javax.swing.text.BadLocationException;
import javax.swing.text.Document;
import javax.swing.text.PlainDocument;

import com.isti.util.UtilFns;

/**
 * Class FilteredJTextField is an extension of 'JTextField' which only
 * allows certain characters to be entered into it.  Static strings of
 * allowed characters for numeric types (i.e. INTEGER_CHARS) are provided.
 * To construct a text field that only allows valid floating point
 * numeric values (including the 'e' exponent) to be entered, the
 * following constructor could be used:
 *     new FilteredJTextField("",5,SIGNED_EFLOAT_CHARS,true)
 * It will initially be empty and have a column size of 5.  The 'true'
 * flag indicates that only valid numeric values are allowed, so numeric
 * rules (such as only one decimal point, etc) are applied.
 */
public class FilteredJTextField extends JTextField
{
    /** Static string containing the numeric characters '0' through '9'. */
  public static final String INTEGER_CHARS = "0123456789";
  /**
   * Static string containing the upper-case HEX characters 'A' through 'F'.
   */
  public static final String HEX_ALPHA_CAP_CHARS = "ABCDEF";
  /**
   * Static string containing the lower-case HEX characters 'a' through 'f'.
   */
  public static final String HEX_ALPHA_LOW_CHARS = "abcdef";
  /**
   * Static string containing the HEX characters.
   */
  public static final String HEX_CHARS = INTEGER_CHARS +
      HEX_ALPHA_CAP_CHARS + HEX_ALPHA_LOW_CHARS;
    /**
     * Static string containing the numeric characters '0' through '9'
     * and the sign characters '-' and '+'.
     */
  public static final String SIGNED_INT_CHARS = INTEGER_CHARS + "-+";
    /**
     * Static string containing the numeric characters '0' through '9'
     * and the decimal point character '.'.
     */
  public static final String FLOAT_CHARS = INTEGER_CHARS + ".";
    /**
     * Static string containing the numeric characters '0' through '9',
     * the decimal point character '.' and the sign characters '-' and '+'.
     */
  public static final String SIGNED_FLOAT_CHARS = FLOAT_CHARS + "-+";
    /**
     * Static string containing the numeric characters '0' through '9',
     * the decimal point character '.' and the exponent characters 'e'
     * and 'E'.
     */
  public static final String EFLOAT_CHARS = FLOAT_CHARS + "eE";
    /**
     * Static string containing the numeric characters '0' through '9',
     * the decimal point character '.', the exponent characters 'e'
     * and 'E', and the sign characters '-' and '+'.
     */
  public static final String SIGNED_EFLOAT_CHARS =
                                                  SIGNED_FLOAT_CHARS + "eE";
    /**
     * Static string containing the upper-case characters 'A' through 'Z'.
     */
  public static final String ALPHA_CAP_CHARS = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
    /**
     * Static string containing the lower-case characters 'a' through 'z'.
     */
  public static final String ALPHA_LOW_CHARS = "abcdefghijklmnopqrstuvwxyz";
    /**
     * Static string containing the characters 'A' through 'Z' and 'a'
     * through 'z'.
     */
  public static final String ALPHA_CHARS = ALPHA_CAP_CHARS + ALPHA_LOW_CHARS;
    /**
     * Static string containing the characters 'A' through 'Z', 'a'
     * through 'z' and '0' though '9'.
     */
  public static final String ALPHANUMERIC_CHARS = ALPHA_CHARS +
                                           FilteredJTextField.INTEGER_CHARS;
    /**
     * Static string containing the characters needed to specify a time
     * of day ('0' through '9', ':' and '.').
     */
  public static final String TOD_CHARS = ":." +
                                           FilteredJTextField.INTEGER_CHARS;
    /**
     * Static string containing the asterisk wildcard character.
     */
  public static final String WILDCARD_ALL_CHAR = "*";
    /**
     * Static string containing the question-mark wildcard character.
     */
  public static final String WILDCARD_SINGLE_CHAR = "?";
    /**
     * Static string containing asterisk and question-mark wildcard
     * characters.
     */
  public static final String WILDCARD_CHARS = WILDCARD_ALL_CHAR +
                                                       WILDCARD_SINGLE_CHAR;


    /**
     * Gets the allowed number characters.
     * @param isFloatFlag true if floating point number.
     * @param isUnsignedFlag true if unsigned number.
     * @return the allowed number characters.
     */
  public final static String getAllowedNumberChars(
      boolean isFloatFlag,boolean isUnsignedFlag)
  {
    String allowedChars;
    if (isFloatFlag)
    {
      //unsigned or signed float characters allowed
      allowedChars = isUnsignedFlag?FilteredJTextField.EFLOAT_CHARS:
          FilteredJTextField.SIGNED_EFLOAT_CHARS;
    }
    else
    {
      //unsigned or signed characters allowed
      allowedChars = isUnsignedFlag?FilteredJTextField.INTEGER_CHARS:
          FilteredJTextField.SIGNED_INT_CHARS;
    }
    return allowedChars;
  }

    /**
     * Creates a new 'FilteredJTextField' object.  All characters are
     * allowed and the number of columns is zero.
     */
  public FilteredJTextField()
  {
    super();
  }

    /**
     * Creates a new 'FilteredJTextField' object initialized with the
     * given text.  All characters are allowed and the number of columns
     * is zero.
     * @param text text string
     */
  public FilteredJTextField(String text)
  {
    super(text);
  }

    /**
     * Creates a new 'FilteredJTextField' object initialized with the
     * given text.  The number of columns is set to zero.
     * @param text the initial text for the field.
     * @param allowedChars a String of characters allowed to be entered
     * into the field.
     * @param numericFlag true if only valid numeric data is to be allowed.
     */
  public FilteredJTextField(String text,String allowedChars,
                                                        boolean numericFlag)
  {
    super(new FilteredDocument(allowedChars,numericFlag,0),text,0);
  }

    /**
     * Creates a new 'FilteredJTextField' object initialized with the
     * given text.  The number of columns is set to zero.
     * @param text the initial text for the field.
     * @param allowedChars a String of characters allowed to be entered
     * into the field.
     */
  public FilteredJTextField(String text,String allowedChars)
  {
    super(new FilteredDocument(allowedChars,false,0),text,0);
  }

    /**
     * Creates a new 'FilteredJTextField' object initialized with the
     * given text for numeric values.
     * @param text the initial text for the field.
     * @param columns the number of the columns for the field.
     * @param isFloatFlag true if floating point number.
     * @param isUnsignedFlag true if unsigned number.
     */
  public FilteredJTextField(
      String text,int columns,boolean isFloatFlag,boolean isUnsignedFlag)
  {
    super(new FilteredDocument(getAllowedNumberChars(isFloatFlag,isUnsignedFlag),
                               true,0),text,columns);
  }

    /**
     * Creates a new 'FilteredJTextField' object initialized with the
     * given text.
     * @param text the initial text for the field.
     * @param allowedChars a String of characters allowed to be entered
     * into the field.
     * @param columns the number of the columns for the field.
     * @param numericFlag true if only valid numeric data is to be allowed.
     */
  public FilteredJTextField(String text,int columns,String allowedChars,
                                                        boolean numericFlag)
  {
    super(new FilteredDocument(allowedChars,numericFlag,0),text,columns);
  }

    /**
     * Creates a new 'FilteredJTextField' object initialized with the
     * given text.
     * @param text the initial text for the field.
     * @param allowedChars a String of characters allowed to be entered
     * into the field.
     * @param columns the number of the columns for the field.
     */
  public FilteredJTextField(String text,int columns,String allowedChars)
  {
    super(new FilteredDocument(allowedChars,false,0),text,columns);
  }

  /**
   * Creates a new 'FilteredJTextField' object.
   * @param allowedChars a String of characters allowed to be entered
   * into the field.
   * @param columns the number of the columns for the field.
   */
  public FilteredJTextField(int columns,String allowedChars)
  {
    super(new FilteredDocument(allowedChars,false,0),null,columns);
  }

    /**
     * Creates a new 'FilteredJTextField' object.  All characters are
     * allowed and the number of columns is zero.
     * @param maxNumChars maximum number of characters allowed (or 0
     * for no limit).
     */
  public FilteredJTextField(int maxNumChars)
  {
    super(new FilteredDocument(null,false,maxNumChars),null,0);
  }

    /**
     * Creates a new 'FilteredJTextField' object initialized with the
     * given text.  All characters are allowed and the number of columns
     * is zero.
     * @param text text string
     * @param maxNumChars maximum number of characters allowed (or 0
     * for no limit).
     */
  public FilteredJTextField(String text,int maxNumChars)
  {
    super(new FilteredDocument(null,false,maxNumChars),text,0);
  }

    /**
     * Creates a new 'FilteredJTextField' object initialized with the
     * given text.  The number of columns is set to zero.
     * @param text the initial text for the field.
     * @param allowedChars a String of characters allowed to be entered
     * into the field.
     * @param numericFlag true if only valid numeric data is to be allowed.
     * @param maxNumChars maximum number of characters allowed (or 0
     * for no limit).
     */
  public FilteredJTextField(String text,String allowedChars,
                                        boolean numericFlag,int maxNumChars)
  {
    super(new FilteredDocument(allowedChars,numericFlag,maxNumChars),
                                                                    text,0);
  }

    /**
     * Creates a new 'FilteredJTextField' object initialized with the
     * given text.  The number of columns is set to zero.
     * @param text the initial text for the field.
     * @param allowedChars a String of characters allowed to be entered
     * into the field.
     * @param maxNumChars maximum number of characters allowed (or 0
     * for no limit).
     */
  public FilteredJTextField(String text,String allowedChars,int maxNumChars)
  {
    super(new FilteredDocument(allowedChars,false,maxNumChars),text,0);
  }

    /**
     * Creates a new 'FilteredJTextField' object initialized with the
     * given text.
     * @param text the initial text for the field.
     * @param allowedChars a String of characters allowed to be entered
     * into the field.
     * @param columns the number of the columns for the field.
     * @param numericFlag true if only valid numeric data is to be allowed.
     * @param maxNumChars maximum number of characters allowed (or 0
     * for no limit).
     */
  public FilteredJTextField(String text,int columns,String allowedChars,
                                        boolean numericFlag,int maxNumChars)
  {
    super(new FilteredDocument(allowedChars,numericFlag,maxNumChars),
                                                              text,columns);
  }

  /**
   * Creates a new 'FilteredJTextField' object initialized with the
   * given text.
   * @param text the initial text for the field.
   * @param specialChars a String of characters allowed (or not) to be entered
   * into the field.
   * @param columns the number of the columns for the field.
   * @param numericFlag true if only valid numeric data is to be allowed.
   * @param maxNumChars maximum number of characters allowed (or 0
   * for no limit).
   * @param allowFlag true to allow only the special characters, false to not
   * allow the special characters.
   */
  public FilteredJTextField(
      String text,int columns,String specialChars,boolean numericFlag,
      int maxNumChars,boolean allowFlag)
  {
    super(new FilteredDocument(specialChars,numericFlag,maxNumChars,allowFlag),
          text,columns);
  }

    /**
     * Creates a new 'FilteredJTextField' object initialized with the
     * given text.
     * @param text the initial text for the field.
     * @param allowedChars a String of characters allowed to be entered
     * into the field.
     * @param columns the number of the columns for the field.
     * @param maxNumChars maximum number of characters allowed (or 0
     * for no limit).
     */
  public FilteredJTextField(String text,int columns,String allowedChars,
                                                            int maxNumChars)
  {
    super(new FilteredDocument(allowedChars,false,maxNumChars),
                                                              text,columns);
  }

    /**
     * Sets a maximum value to be allowed for numeric entries (only
     * used if the 'numericFlag' is true).
     * @param compObj a "maximum" value object that implements the
     * 'Comparable' interface (or null for no maximum).
     * @return A handle to this object.
     */
  public FilteredJTextField setMaxValue(Comparable compObj)
  {
    Document docObj;    //if using 'FilteredDocument' then set max-value:
    if((docObj=getDocument()) instanceof FilteredDocument)
      ((FilteredDocument)docObj).setMaxValueObj(compObj);
    return this;
  }

  /**
   * Gets the integer value represented by this text field.
   * @return the integer value or 0 if empty or not an integer.
   */
  public int getIntegerValue()
  {
    try
    {
      if (getText() != null && getText().length() > 0)
      {
        return Integer.valueOf(getText()).intValue();
      }
    }
    catch (Exception e)
    {
    }
    return 0;
  }

    /**
     * Returns the maximum value to be allowed for numeric entries, or
     * null if no value has been entered.
     * @return A "maximum" value object that implements the
     * 'Comparable' interface, or null if no maximum.
     */
  public Comparable getMaxValueObj()
  {
    Document docObj;    //if using 'FilteredDocument' then set max-value:
    if((docObj=getDocument()) instanceof FilteredDocument)
      return ((FilteredDocument)docObj).getMaxValueObj();
    return null;
  }

    /**
     * Sets a maximum value to be allowed for numeric entries (only
     * used if the 'numericFlag' is true).
     * @param val maximum value to be allowed.
     * @return A handle to this object.
     */
  public FilteredJTextField setMaxValue(int val)
  {
    Document docObj;    //if using 'FilteredDocument' then set max-value:
    if((docObj=getDocument()) instanceof FilteredDocument)
      ((FilteredDocument)docObj).setMaxValueObj(Integer.valueOf(val));
    return this;
  }

    /**
     * Sets a maximum value to be allowed for numeric entries (only
     * used if the 'numericFlag' is true).
     * @param val maximum value to be allowed.
     * @return A handle to this object.
     */
  public FilteredJTextField setMaxValue(long val)
  {
    Document docObj;    //if using 'FilteredDocument' then set max-value:
    if((docObj=getDocument()) instanceof FilteredDocument)
      ((FilteredDocument)docObj).setMaxValueObj(Long.valueOf(val));
    return this;
  }

    /**
     * Sets a maximum value to be allowed for numeric entries (only
     * used if the 'numericFlag' is true).
     * @param val maximum value to be allowed.
     * @return A handle to this object.
     */
  public FilteredJTextField setMaxValue(short val)
  {
    Document docObj;    //if using 'FilteredDocument' then set max-value:
    if((docObj=getDocument()) instanceof FilteredDocument)
      ((FilteredDocument)docObj).setMaxValueObj(Short.valueOf(val));
    return this;
  }

    /**
     * Sets a maximum value to be allowed for numeric entries (only
     * used if the 'numericFlag' is true).
     * @param val maximum value to be allowed.
     * @return A handle to this object.
     */
  public FilteredJTextField setMaxValue(byte val)
  {
    Document docObj;    //if using 'FilteredDocument' then set max-value:
    if((docObj=getDocument()) instanceof FilteredDocument)
      ((FilteredDocument)docObj).setMaxValueObj(Byte.valueOf(val));
    return this;
  }

    /**
     * Sets a maximum value to be allowed for numeric entries (only
     * used if the 'numericFlag' is true).
     * @param val maximum value to be allowed.
     * @return A handle to this object.
     */
  public FilteredJTextField setMaxValue(double val)
  {
    Document docObj;    //if using 'FilteredDocument' then set max-value:
    if((docObj=getDocument()) instanceof FilteredDocument)
      ((FilteredDocument)docObj).setMaxValueObj(Double.valueOf(val));
    return this;
  }

    /**
     * Sets a maximum value to be allowed for numeric entries (only
     * used if the 'numericFlag' is true).
     * @param val maximum value to be allowed.
     * @return A handle to this object.
     */
  public FilteredJTextField setMaxValue(float val)
  {
    Document docObj;    //if using 'FilteredDocument' then set max-value:
    if((docObj=getDocument()) instanceof FilteredDocument)
      ((FilteredDocument)docObj).setMaxValueObj(Float.valueOf(val));
    return this;
  }
}


/**
 * Class FilteredDocument is an extension of 'PlainDocument' that
 * implements the filtering of input characters.
 */
class FilteredDocument extends PlainDocument
{
  public final String specialChars;       //string of special characters
  public final boolean numericFlag;       //true for valid numeric only
  public final int maxNumChars;           //maximum number of chars allowed
  public final boolean allowFlag;         //only allow the special chars
  private Comparable maxValueObj = null;  //maximum value for numeric

  /**
   * Constructs a plain text document.
   * @param allowedChars a String of characters allowed to be entered
   * into the document.
   * @param numericFlag true for valid numeric only
   * @param maxNumChars maximum number of chars allowed
   */
  public FilteredDocument(String allowedChars,boolean numericFlag,
                                                            int maxNumChars)
  {
    this(allowedChars,numericFlag,maxNumChars,true);
  }

  /**
   * Constructs a plain text document.
   * @param specialChars a String of characters allowed (or not)
   * into the document.
   * @param numericFlag true for valid numeric only
   * @param maxNumChars maximum number of chars allowed
   * @param allowFlag true to allow only the special characters, false to not
   * allow the special characters.
   */
  public FilteredDocument(String specialChars,boolean numericFlag,
                          int maxNumChars,boolean allowFlag)
  {
    this.specialChars = specialChars;
    this.numericFlag = numericFlag;
    this.maxNumChars = maxNumChars;
    this.allowFlag = allowFlag;
  }

  /**
   * Inserts data into the document, filtering it along the way.
   * @param insOffs the starting offset (greater than or equal to zero).
   * @param insStr the string to insert (empty and null strings are
   * ignored).
   * @param aSet the attributes for the inserted content.
   * @exception BadLocationException if the given position is not a
   * valid position within the document.
   */
  public void insertString(int insOffs,String insStr,AttributeSet aSet)
                                               throws BadLocationException
  {
    int p,q,dataLen,offs;
    char ch;
    String dataStr = null;

    final int insLen;
    if(insStr == null || (insLen=insStr.length()) <= 0)
      return;      //if null or empty string then just return

    String cmpString = insStr;
    final char decimalSeparator = UtilFns.getDecimalSeparator();
    if(numericFlag)
    {
      //if decimal separator is not '.'
      if (decimalSeparator != '.')
      {
        //replace '.' with the decimal separator
        insStr = insStr.replace('.', decimalSeparator);
        // but use '.' for checks
        cmpString = cmpString.replace(decimalSeparator,'.');
      }
    }

    dataLen = getLength();        //get length of current data
    if(maxNumChars > 0 && dataLen + insLen > maxNumChars)
      return;      //if length limit and too long then just return
    if(specialChars != null && specialChars.length() > 0)
    {    //string of allowed characters is not empty
      if(numericFlag)
      {       //data must be valid numeric
        if(insOffs >= 0 && insOffs <= dataLen)
        {     //given offset is valid
          offs = insOffs;              //accept value
                   //create string with new data inserted and lower-case:
          dataStr = (getText(0,offs) + insStr + getText(offs,dataLen-offs)).
                                                              toLowerCase();
          dataLen = dataStr.length();       //get new length
        }
        else
        {     //given offset is not valid
          dataStr = insStr;  //just take original string
          offs = dataLen;    //put in valid offset value
        }
      }
      else
      {       //data does not have to be valid numeric
        offs = 0;            //put in dummy value
        dataLen = 0;         //put in dummy value
        dataStr = "";        //put in dummy value
      }
      p = 0;
      do
      {  //for each data character; check if matches allowed character
        if(specialChars.indexOf(ch=cmpString.charAt(p)) < 0 == allowFlag)
          break;        //if not allowed character then exit loop
        if(numericFlag)
        {     //data must be valid numeric
                   //if sign character then it must be at the beginning or
                   // it must be after an 'e' and followed by either
                   // nothing or a number:
          if((ch == '-' || ch == '+') && offs + p > 0 &&
               !(dataStr.substring(offs+p-1,offs+p).equalsIgnoreCase("e") &&
                   (offs+p+1 >= dataLen || FilteredJTextField.INTEGER_CHARS.
                                   indexOf(dataStr.charAt(offs+p+1)) >= 0)))
          {
            break;
          }
          //if a decimal separator then there must not now be two of them:
          if(ch == decimalSeparator &&
             (q=dataStr.indexOf(decimalSeparator)) >= 0 &&
             q < dataLen && dataStr.indexOf(decimalSeparator,q+1) >= 0)
          {
            break;
          }
              //if an 'e' then it must not be at the beginning and
              // it must be preceded by a number and there must not
              // now be two of them:
          if((ch == 'e' || ch == 'E') && (offs + p == 0 ||
                                           FilteredJTextField.INTEGER_CHARS.
                                   indexOf(dataStr.charAt(offs+p-1)) < 0) ||
                                           ((q=dataStr.indexOf('e')) >= 0 &&
                              q < dataLen && dataStr.indexOf('e',q+1) >= 0))
          {
            break;
          }
        }
        ++p;                 //increment to next data character
      }
      while(p < insLen);     //loop if more data
    }
    else      //string of allowed characters is empty
      p = insLen;       //setup to pass through entire string

    if(p > 0)
    {    //new characters were allowed
      if(numericFlag && maxValueObj != null)
      {  //numeric flag set and max-value object exists
        try
        {     //convert new data string to 'Comparable' object:
          final Number numberObj =
              UtilFns.parseNumber(dataStr,maxValueObj.getClass());
          final Comparable compObj;
          if (numberObj instanceof Comparable)
            compObj = (Comparable)numberObj;
          else if(maxValueObj instanceof Integer)
            compObj = Integer.valueOf(dataStr);
          else if(maxValueObj instanceof Long)
            compObj = Long.valueOf(dataStr);
          else if(maxValueObj instanceof Float)
            compObj = Float.valueOf(dataStr);
          else if(maxValueObj instanceof Double)
            compObj = Double.valueOf(dataStr);
          else if(maxValueObj instanceof Byte)
            compObj = Byte.valueOf(dataStr);
          else if(maxValueObj instanceof Short)
            compObj = Short.valueOf(dataStr);
          else compObj = null;
          if(compObj != null && compObj.compareTo(maxValueObj) > 0)
            return;     //if converted OK and out of range then reject
        }
        catch(Exception ex) {}    //if exception error then accept
      }
              //insert new characters:
      super.insertString(insOffs,insStr.substring(0,p),aSet);
    }
  }

    /**
     * Sets a maximum value to be allowed for numeric entries (only
     * used if the 'numericFlag' is true).
     * @param obj a "maximum" value object that implements the
     * 'Comparable' interface (or null for no maximum).
     */
  public void setMaxValueObj(Comparable obj)
  {
    maxValueObj = obj;
  }

    /**
     * Returns the maximum value to be allowed for numeric entries, or
     * null if no value has been entered.
     * @return A "maximum" value object that implements the
     * 'Comparable' interface, or null if no maximum.
     */
  public Comparable getMaxValueObj()
  {
    return maxValueObj;
  }
}
