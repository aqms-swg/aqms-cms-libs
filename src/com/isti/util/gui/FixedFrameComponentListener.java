//FixedFrameComponentListener.java:  Component listener that prevents
//                                   its host component from being moved
//                                   or resized.
//
//  5/23/2007 -- [ET]
//

package com.isti.util.gui;

import java.awt.Rectangle;
import java.awt.Component;
import java.awt.Frame;
import java.awt.event.ComponentAdapter;
import java.awt.event.ComponentEvent;

/**
 * Class FixedFrameComponentListener is a component listener that prevents
 * its host component from being moved or resized.
 */
public class FixedFrameComponentListener extends ComponentAdapter
{
  protected Component hostComponentObj = null;
  protected Rectangle componentBoundRectObj = null;
  protected boolean keepCompMaximizedFlag = false;

  /**
   * Updates the host component to be managed by this listener.
   * @param hostCompObj host 'Component' object.
   * @param rectObj component-bounds-rectangle values to use, or null
   * to use the bounds from the given host 'Component' object.
   * @param keepMaximizedFlag true to keep the host component maximized
   * (if the component is a 'Frame' object); false to not.
   * @return This 'FixedFrameComponentListener' object.
   */
  public FixedFrameComponentListener updateComponent(Component hostCompObj,
                               Rectangle rectObj, boolean keepMaximizedFlag)
  {
    hostComponentObj = hostCompObj;
    componentBoundRectObj = (rectObj != null) ? new Rectangle(rectObj) :
                                                    hostCompObj.getBounds();
    keepCompMaximizedFlag = (hostComponentObj instanceof Frame) ?
                                                  keepMaximizedFlag : false;
    return this;
  }

  /**
   * Updates the host component to be managed by this listener.
   * @param hostCompObj host 'Component' object.
   * @param keepMaximizedFlag true to keep the host component maximized
   * (if the component is a 'Frame' object); false to not.
   * @return This 'FixedFrameComponentListener' object.
   */
  public FixedFrameComponentListener updateComponent(Component hostCompObj,
                                                  boolean keepMaximizedFlag)
  {
    return updateComponent(hostCompObj,null,keepMaximizedFlag);
  }

  /**
   * Updates the host component to be managed by this listener.
   * @param hostCompObj host 'Component' object.
   * @param rectObj component-bounds-rectangle values to use, or null
   * to use the bounds from the given host 'Component' object.
   * @return This 'FixedFrameComponentListener' object.
   */
  public FixedFrameComponentListener updateComponent(Component hostCompObj,
                                                          Rectangle rectObj)
  {
    return updateComponent(hostCompObj,rectObj,false);
  }

  /**
   * Updates the host component to be managed by this listener.
   * @param hostCompObj host 'Component' object.
   * @return This 'FixedFrameComponentListener' object.
   */
  public FixedFrameComponentListener updateComponent(Component hostCompObj)
  {
    return updateComponent(hostCompObj,null,false);
  }

  /**
   * Invoked when the component's position changes.  Sets the component's
   * bounds (or maximize state).
   * @param evtObj component-event object.
   */
  public void componentMoved(ComponentEvent evtObj)
  {
    if(hostComponentObj != null)
    {    //host component OK
              //if keeping host component maximized then maximize frame;
              // otherwise if not at desired position then set position:
      if(keepCompMaximizedFlag)
        GuiUtilFns.maximizeFrame((Frame)hostComponentObj);
      else if(!componentBoundRectObj.equals(hostComponentObj.getBounds()))
        hostComponentObj.setBounds(componentBoundRectObj);
    }
  }

  /**
   * Invoked when the component's size changes.  Sets the component's
   * bounds (or maximize state).
   * @param evtObj component-event object.
   */
  public void componentResized(ComponentEvent evtObj)
  {
    componentMoved(evtObj);
  }
}
