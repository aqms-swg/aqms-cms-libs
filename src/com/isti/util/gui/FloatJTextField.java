//FloatJTextField.java:  Implements the floating value text field.
//
//    9/2/2003 -- [KF]
//

package com.isti.util.gui;

import com.isti.util.UtilFns;

/**
 * Class FloatJTextField implements the floating value text field.
 */
public class FloatJTextField extends FilteredJTextField
{
  /**
   * The default number of digits.
   */
  public final static int DEFAULT_DIGITS = 6;
  /**
   * The default number of columns.
   */
  public final static int DEFAULT_COLUMNS = 2 + DEFAULT_DIGITS;
  private final int columns;
  private final int digits;
  private double value;
  private String valueText;

  /**
   * Constructs a floating value text field.
   */
  public FloatJTextField()
  {
    this(0);
  }

  /**
   * Constructs a floating value text field.
   * @param value the initial value.
   */
  public FloatJTextField(double value)
  {
    this(value, DEFAULT_COLUMNS);
  }

  /**
   * Constructs a floating value text field.
   * @param value the initial value.
   * @param columns the number of the columns for the field.
   */
  public FloatJTextField(double value, int columns)
  {
    this(value, columns, DEFAULT_DIGITS);
  }

  /**
   * Constructs a floating value text field.
   * @param value the initial value.
   * @param columns the number of the columns for the field.
   * @param digits the maximum number of fraction digits to be shown.
   */
  public FloatJTextField(double value, int columns, int digits)
  {
    super(null, columns, FilteredJTextField.FLOAT_CHARS, true, columns);
    this.columns = columns;
    this.digits = digits;
    setValue(value);
  }

  /**
   * Constructs a floating value text field.
   * @param text the initial text for the field.
   */
  public FloatJTextField(String text)
  {
    this(text, DEFAULT_COLUMNS);
  }

  /**
   * Constructs a floating value text field.
   * @param text the initial text for the field.
   * @param columns the number of the columns for the field.
   */
  public FloatJTextField(String text, int columns)
  {
    this(text, columns, DEFAULT_DIGITS);
  }

  /**
   * Constructs a floating value text field.
   * @param text the initial text for the field.
   * @param columns the number of the columns for the field.
   * @param digits the maximum number of fraction digits to be shown.
   */
  public FloatJTextField(String text, int columns, int digits)
  {
    super(text, columns, FilteredJTextField.FLOAT_CHARS, true, columns);
    this.columns = columns;
    this.digits = digits;
    getValue();
  }

  /**
   * Returns the value contained in this component.
   * @return the value.
   */
  public double getValue()
  {
    //get the current text
    final String currentText = super.getText();

    if (!currentText.equals(valueText))  //if text was changed
    {
      //convert the text to a value
      try
      {
        valueText = currentText;
        value = Double.parseDouble(valueText);
      }
      catch (NumberFormatException ex)
      {
        value = 0;
      }
    }
    return value;
  }

  /**
   * Sets the value of this component to the specified value.
   * @param value the new value to be set.
   */
  public void setValue(double value)
  {
    this.value = value;
    valueText = UtilFns.floatNumberToString(
        value, digits, columns, getMaxValueObj());

    super.setText(valueText);
  }
}