//FocusUtils.java:  Defines a set of static utility methods related to
//                  component focus.
//
// 11/23/2002 -- [ET]  Initial version.
// 11/29/2003 -- [ET]  Added 'restoreTabFocusTraversal()' method.
// 10/15/2008 -- [ET]  Added 'requestDefaultFocus()' method.
//

package com.isti.util.gui;

import java.util.Set;
import java.awt.*;
import javax.swing.*;

/**
 * Class FocusUtils defines a set of static utility methods related to
 * component focus.
 */
public class FocusUtils
{
         //object array containing a single 'false' parameter:
  private static final Object [] falseFlagParamObjArray =
                                             new Object[] { Boolean.FALSE };
         //if 'Component' class contains Java-1.4 "setFocusable()" method
         // then set 'compSetFocusableMethodObj' to reference it:
  private static java.lang.reflect.Method compSetFocusableMethodObj = null;
  static
  {           //(this is to allow access to the Java-1.4 "setFocusable()"
    try       // method while retaining Java 1.3 compatibility)
    {              //if method found then setup reference to it:
      compSetFocusableMethodObj = Component.class.getMethod(
                            "setFocusable",(new Class[] { boolean.class }));
    }              //if method not found then leave it at null:
    catch(Exception ex) {}
  }
         //object array containing the parameters "0,null":
  private static final Object [] zeroNullParamObjArray =
                                      new Object[] { Integer.valueOf(0), null };
         //object array containing the parameters "1,null":
  private static final Object [] oneNullParamObjArray =
                                      new Object[] { Integer.valueOf(1), null };
         //if 'Container' class contains Java-1.4 "setFocusTraversal()"
         // method then set 'compSetFocusableMethodObj' to reference it:
  private static java.lang.reflect.Method contSetFocusTraversalMethodObj =
                                                                       null;
  static
  {           //(this is to allow access to the Java-1.4 "setFocusTraversal()"
    try       // method while retaining Java 1.3 compatibility)
    {              //if method found then setup reference to it:
      contSetFocusTraversalMethodObj = Container.class.getMethod(
            "setFocusTraversalKeys",(new Class[] { int.class, Set.class }));
    }              //if method not found then leave it at null:
    catch(Exception ex) {}
  }


    //private constructor so that no object instances may be created
    // (static access only)
  private FocusUtils()
  {
  }

  /**
   * Disables focusability on the given component.  If running under
   * Java 1.4 or later then "setFocusable(false)" is used.
   * @param compObj the component to use.
   */
  public static void setComponentFocusDisabled(Component compObj)
  {
    try   //call the Java-1.4 Component "setFocusable()" method via
    {     // class-reflection to retain Java 1.3 compatibility:
      if(compSetFocusableMethodObj != null)
        compSetFocusableMethodObj.invoke(compObj,falseFlagParamObjArray);
    }
    catch(Exception ex) {}
    if(compObj instanceof JComponent)
      ((JComponent)compObj).setRequestFocusEnabled(false);
  }

  /**
   * Disables focusability on all components held by the given container
   * or any subcontainers of it, except for any component matching the
   * 'keepFocusObj' component or that is any type of menu item.
   * @param contObj the container to use.
   * @param keepFocusObj the component that should retain focusability,
   * or null for none.
   */
  public static void disableFocusComponents(Container contObj,
                                                     Component keepFocusObj)
  {
    try
    {
      final Component [] compArr = contObj.getComponents();
      Component compObj;
      for(int i=0; i<compArr.length; ++i)
      {       //for each component in container; disable focus
        if((compObj=contObj.getComponent(i)) != keepFocusObj &&
                                                          compObj != null &&
          !(compObj instanceof JMenuBar) && !(compObj instanceof JMenuItem))
        {     //component is not the keep-focus component or menu item
          setComponentFocusDisabled(compObj);     //disable focus
                   //if component is also a container then
                   // process it recursively:
          if(compObj instanceof Container)
            disableFocusComponents((Container)compObj,keepFocusObj);
        }
      }
    }
    catch(Exception ex) {}   //if error then just abort
  }

  /**
   * Finds the first component of the preferred class-type among all the
   * components held by the given container or any subcontainers of it.
   * If a matching component is not found then the first focusable
   * component is returned, or null if none could be found.
   * @param contObj the container to use.
   * @param preferredType the preferred class-type to be found.
   * @return A focusable component, or null if none could be found.
   */
  public static Component findFirstFocusableComponent(Container contObj,
                                                        Class preferredType)
  {
    try
    {
      final Component [] compArr = contObj.getComponents();
      Component compObj, cObj, firstFocusableCompObj = null;
      for(int i=0; i<compArr.length; ++i)
      {       //for each component in container
        if((compObj=contObj.getComponent(i)) != null && compObj.isVisible())
        {     //component object fetched OK and is visible
          if(compObj instanceof Container)
          {   //component is also a container; process it recursively
//            System.out.println("  findFirstFocusableComp container:  " +
//                                                        compObj.getClass());
            if((cObj=findFirstFocusableComponent(
                                 (Container)compObj,preferredType)) != null)
            {      //focusable component found
              compObj = cObj;     //use found component
            }
          }
//          System.out.println("  findFirstFocusableComp found:  " +
//                                                        compObj.getClass());
          if(preferredType != null &&
                                   preferredType.equals(compObj.getClass()))
          {   //component of preferred type found
            return compObj;       //return component
          }
          if(firstFocusableCompObj == null && compObj instanceof JComponent
                           && ((JComponent)compObj).isRequestFocusEnabled())
          {   //no previous request-focusable component was found and
              // component is JComponent with request-focus enabled
            firstFocusableCompObj = compObj;     //save component
          }
        }
      }
      return firstFocusableCompObj;    //return first focusable comp found
    }
    catch(Exception ex) {}   //if error then just abort
    return null;
  }

  /**
   * Finds the first focused component held by the given container
   * or any subcontainers of it
   * @param contObj the container to use.
   * @return A focused 'Component', or null if none found.
   */
  public static Component findFocusedComponent(Container contObj)
  {
    try
    {
      final Component [] compArr = contObj.getComponents();
      Component compObj;
      for(int i=0; i<compArr.length; ++i)
      {       //for each component in container; disable focus
        if((compObj=contObj.getComponent(i)) != null)

        {     //component fetched OK
          if(compObj.hasFocus())       //if component has focus then
            return compObj;            //return it
                   //if component is also a container then
                   // process it recursively:
          if(compObj instanceof Container &&
                 (compObj=findFocusedComponent((Container)compObj)) != null)
          {   //component found with focus
            return compObj;            //return it
          }
        }
      }
    }
    catch(Exception ex) {}   //if error then just abort
    return null;
  }

  /**
   * Restores the focus-traversal function of the "Tab" key.  (Some text
   * components like 'JTextArea' redefine the "Tab" key.)  This method
   * only works for Java 1.4 or later.  Under Java 1.3, the focus-traversal
   * function of the "Tab" key may be restored by extending the component
   * to make the 'isManagingFocus()' method return 'false'.
   * @param containerObj the component to use.
   */
  public static void restoreTabFocusTraversal(Container containerObj)
  {
    try  //call the Java-1.4 Component "setFocusTraversalKeys()" method
    {    // via class-reflection to retain Java 1.3 compatibility:
      if(contSetFocusTraversalMethodObj != null)
      {  //if method-object setup then clear forward-traversal keys
        contSetFocusTraversalMethodObj.invoke(
                                        containerObj,zeroNullParamObjArray);
      }
      if(contSetFocusTraversalMethodObj != null)
      {  //if method-object setup then clear backward-traversal keys
        contSetFocusTraversalMethodObj.invoke(
                                        containerObj,oneNullParamObjArray);
      }
    }
    catch(Exception ex) {}
  }

  /**
   * Non-deprecated version of JComponent 'requestDefaultFocus()' method.
   * Requests focus on the given <code>JComponent</code>'s
   * <code>FocusTraversalPolicy</code>'s default <code>Component</code>.
   * If the <code>JComponent</code> is a focus cycle root, then its
   * <code>FocusTraversalPolicy</code> is used. Otherwise, the
   * <code>FocusTraversalPolicy</code> of the <code>JComponent</code>'s
   * focus-cycle-root ancestor is used.
   * @param jCompObj JComponent object to use.
   * @return true if a focus cycle root was found and used.
   */
  public static boolean requestDefaultFocus(JComponent jCompObj)
  {
    final Container nearestRoot = (jCompObj.isFocusCycleRoot()) ? jCompObj :
                                       jCompObj.getFocusCycleRootAncestor();
    if(nearestRoot == null)
      return false;
    final Component comp = nearestRoot.getFocusTraversalPolicy().
                                           getDefaultComponent(nearestRoot);
    if(comp != null)
    {
      comp.requestFocus();
      return true;
    }
    else
      return false;
  }
}
