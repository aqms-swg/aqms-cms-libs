//GuiUtilFns.java:  Defines a set of various static utility methods for
//                  use in GUI applications.
//
// 11/23/2002 -- [ET]  Initial version.
//  12/5/2003 -- [ET]  Added 'setScrollPaneCentered()' method.
//  5/25/2005 -- [ET]  Added 'setEmptyJListWidth()' method.
//  12/1/2005 -- [KF]  Added 'ensureSelectionVisible()' and
//                     'ensureRowVisible()' methods.
//  5/22/2007 -- [ET]  Added "frame" and 'getMaximumWindowBounds()' methods.
//  8/23/2007 -- [KF]  Added 'initColumnSizes()' method.
//  7/31/2009 -- [KF]  Added 'getMaxCharWidth()' and 'setPreferredSize()'
//                     methods.
//   8/4/2009 -- [KF]  Added 'getMaxCharWidthValue()' method.
//  11/2/2009 -- [ET]  Added 'setPreferredSize(Component,Dimension)' method
//                     to retain Java 1.4 compatibility.
//  8/30/2010 -- [ET]  Added 'setButtonsContentAreaFilled()' method.
// 12/21/2017 -- [KF]  Added 'getImageFile' and 'saveImage' methods.
//   3/5/2019 -- [KF]  Added 'getSelectedValuesList()' methods.
//

package com.isti.util.gui;

import java.awt.Component;
import java.awt.Container;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.FontMetrics;
import java.awt.Frame;
import java.awt.Graphics2D;
import java.awt.GraphicsEnvironment;
import java.awt.Rectangle;
import java.awt.Toolkit;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import javax.imageio.ImageIO;
import javax.swing.AbstractButton;
import javax.swing.JComboBox;
import javax.swing.JComponent;
import javax.swing.JFileChooser;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JScrollBar;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.ListModel;
import javax.swing.ListSelectionModel;
import javax.swing.SwingUtilities;
import javax.swing.event.ListDataEvent;
import javax.swing.event.ListDataListener;
import javax.swing.table.TableCellRenderer;
import javax.swing.table.TableColumn;
import javax.swing.table.TableColumnModel;
import javax.swing.table.TableModel;

import com.isti.util.FileUtils;
import com.isti.util.IstiFileFilter;
import com.isti.util.IstiNamedValue;
import com.isti.util.UtilFns;
import com.isti.util.gui.textvalidator.TextValidator;

/**
 * Class GuiUtilFns defines a set of various static utility methods for
 * use in GUI applications.
 */
public class GuiUtilFns
{
  /** The default width character, <em>m</em>. */
  public static final char defaultWidthChar = 'm';

  /**
    * The default pad width for components that sometimes require it,
    * such as 'JComboBox'.
    */
  public static int DEF_PAD_WIDTH = 4;
  private static final String[] STANDARD_IMAGE_FORMATS = {"png", "jpg", "bmp", "gif"};
  private static final IstiFileFilter IMAGE_FILTER_DEFAULT;
  private static final String IMAGE_FORMAT_DEFAULT;
  private static final String[] IMAGE_FILTERS;
  private static final String[] IMAGE_FORMATS;
  private static JLabel utilLabelObj = null;
  private static Font utilFontObj = null;
  private static FontMetrics utilMetricsObj = null;
  private static final String SPACE_STR = " ";
  private static final int DEF_SPACE_WIDTH = 4;
  
  private static int indexOf(String[] sa, String s)
  {
      for (int i = 0; i < sa.length; i++)
          if (s.equals(sa[i]))
              return i;
      return -1;
  }

  static
  {
	  String s;
	  int index;
	  IMAGE_FORMAT_DEFAULT = "png";
	  String[] sa = ImageIO.getWriterFormatNames();
	  List<String> imageFormatList = new ArrayList<String>(STANDARD_IMAGE_FORMATS.length);
	  imageFormatList.add(IMAGE_FORMAT_DEFAULT);
	  for (index = 1; index < STANDARD_IMAGE_FORMATS.length; index++)
	  {
		  s = STANDARD_IMAGE_FORMATS[index];
		  if (indexOf(sa, s) >= 0)
		  {
			  imageFormatList.add(s);
		  }
	  }
	  IMAGE_FORMATS = imageFormatList.toArray(
			  new String[imageFormatList.size()]);
	  IMAGE_FILTERS = new String[IMAGE_FORMATS.length];
	  for (int i = 0; i < IMAGE_FORMATS.length; i++)
	  {
		  IMAGE_FILTERS[i] = "*." + IMAGE_FORMATS[i];
	  }
	  IMAGE_FILTER_DEFAULT = new IstiFileFilter(
			  IMAGE_FILTERS, "Image Files");
  }

         //if 'Component' class contains Java-1.5 "setPreferredSize()"
         // method then setup reference to it:
  private static java.lang.reflect.Method compSetPreferredSizeMethodObj =
                                                                       null;
         //if 'Frame' class contains Java-1.4 "get/setExtendedState()"
         // methods then setup references to them:
  private static java.lang.reflect.Method frameGetExtendedStateMethodObj =
                                                                       null;
  private static java.lang.reflect.Method frameSetExtendedStateMethodObj =
                                                                       null;
         //if 'GraphicsEnvironment' class contains Java-1.4
         // "getMaximumWindowBounds()" method then setup reference to it:
  private static java.lang.reflect.Method getMaximumWindowBoundsMethodObj =
                                                                       null;
  private static int FRAME_MAXIMIZED_BOTH = 6;
  static
  {           //(this is to allow access to the Java-1.4+ methods
    try       // while retaining Java 1.4/1.3 compatibility)
    {              //if methods found then setup references:
      compSetPreferredSizeMethodObj = Component.class.getMethod(
                      "setPreferredSize",(new Class[] { Dimension.class }));
      frameGetExtendedStateMethodObj = Frame.class.getMethod(
    	  "getExtendedState", (Class[]) null);
      frameSetExtendedStateMethodObj = Frame.class.getMethod(
                            "setExtendedState",(new Class[] { int.class }));
      getMaximumWindowBoundsMethodObj = GraphicsEnvironment.class.getMethod(
    	  "getMaximumWindowBounds", (Class<?>[] ) null);
              //get value of static field from 'Frame' class:
      FRAME_MAXIMIZED_BOTH =
                        Frame.class.getField("MAXIMIZED_BOTH").getInt(null);
    }              //if not found then leave variables as-is
    catch(Exception ex) {}
  }


    //private constructor so that no object instances may be created
    // (static access only)
  private GuiUtilFns()
  {
  }

  /**
   * Ensures that the current table selection is visible.
   * @param tableObj the table object.
   */
  public static void ensureSelectionVisible(JTable tableObj)
  {
    final int selIdx; //get index of current selection on list:
    if ( (selIdx = tableObj.getSelectedRow()) >= 0)
    { //an item is selected; make sure it's visible
      ensureRowVisible(tableObj,selIdx);
    }
  }

  /**
   * Ensures that the specified table row is visible.
   * @param tableObj the table object.
   * @param row the row index.
   */
  public static void ensureRowVisible(JTable tableObj, int row)
  {
    tableObj.scrollRectToVisible(
      tableObj.getCellRect(row, 0, false));
  }

  /**
   * Returns the maximum width, in pixels, of the default width character.
   * @param compObj the component to use.
   * @return the maximum width or 0 if none.
   */
  public static int getMaxCharWidth(final Component compObj)
  {
    return getMaxCharWidth(compObj, defaultWidthChar);
  }

  /**
   * Returns the maximum width, in pixels, of the specified width character.
   * @param compObj the component to use.
   * @param widthChar the width character.
   * @return the maximum width or 0 if none.
   */
  public static int getMaxCharWidth(final Component compObj, final char widthChar)
  {
    return compObj.getFontMetrics(compObj.getFont()).charWidth(widthChar);
  }

  /**
   * Returns the maximum width, in pixels, of an allowed character.
   * @param compObj the component to use.
   * @param specialChars the allowed (or not allowed) special characters.
   * @param allowedFlag true to allow only the special characters, false to not
   *          allow the special characters.
   * @return the maximum width or 0 if none.
   */
  public static int getMaxCharWidth(
      final Component compObj, final String specialChars, final boolean allowedFlag)
  {
    return ((Integer)getMaxCharWidthValue(
        compObj, specialChars, allowedFlag).getValue()).intValue();
  }

  /**
   * Returns the maximum width, in pixels, of an allowed character.
   * The return named value has a name that is the character with the maximum width
   * and an <em>Integer</em> width value.
   * @param compObj the component to use.
   * @param specialChars the allowed (or not allowed) special characters.
   * @param allowedFlag true to allow only the special characters, false to not
   *          allow the special characters.
   * @return the maximum width value or 0 if none.
   */
  public static IstiNamedValue getMaxCharWidthValue(
      final Component compObj, final String specialChars, final boolean allowedFlag)
  {
    int maxWidth = 0;
    IstiNamedValue value = createNamedValue(defaultWidthChar, 0);
    if (specialChars == null)
    {
      return value;
    }
    char ch;
    int charWidth;
    final int[] widths = compObj.getFontMetrics(compObj.getFont()).getWidths();

    // if only the special characters are allowed
    if (allowedFlag)
    {
      for (int index = 0; index < specialChars.length(); index++)
      {
        ch = specialChars.charAt(index);
        if (ch < widths.length)
        {
          charWidth = widths[ch];
          if (charWidth > maxWidth)
          {
            maxWidth = charWidth;
            value = createNamedValue(ch, charWidth);
          }
        }
      }
    }
    else
    {
      for (ch = 0; ch < widths.length; ch++)
      {
        if (specialChars.indexOf(ch) < 0 != allowedFlag)
        {
          charWidth = widths[ch];
          if (charWidth > maxWidth)
          {
            maxWidth = charWidth;
            value = createNamedValue(ch, charWidth);
          }
        }
      }
    }
    return value;
  }

  /**
   * Create the named integer value.
   * @param ch the character.
   * @param charWidth the character width.
   * @return the named value.
   */
  private static IstiNamedValue createNamedValue(char ch, int charWidth)
  {
    return new IstiNamedValue(String.valueOf(ch), Integer.valueOf(charWidth));
  }

  /**
   * Returns the maximum width, in pixels, of an allowed character.
   * @param compObj the component to use.
   * @param tv the text validator.
   * @return the maximum width or 0 if none.
   */
  public static int getMaxCharWidth(final Component compObj, final TextValidator tv)
  {
    return getMaxCharWidth(compObj, tv.getSpecialChars(), tv.getAllowedFlag());
  }

  	/**
	 * Get the selected values.
	 * 
	 * @param listObj the 'JList' to use.
	 * @return the selected values list, may be empty but not null.
	 */
  public static List getSelectedValuesList(JList listObj)
  {
      return getSelectedValuesList(listObj, (List) null);
  }

  	/**
	 * Get the selected values.
	 * <p>
	 * NOTE: If a selected values list is provided it is not cleared prior to adding
	 * values.
	 * 
	 * @param listObj        the 'JList' to use.
	 * @param selectedValues the selected values list to add selected values to or
	 *                       null to create a new list if needed.
	 * @return the selected values list, may be empty but not null.
	 */
  public static List getSelectedValuesList(JList listObj, List selectedValues)
  {
      ListSelectionModel sm = listObj.getSelectionModel();
      ListModel dm = listObj.getModel();
      int iMin = sm.getMinSelectionIndex();
      int iMax = sm.getMaxSelectionIndex();

      if (selectedValues == null)
      {
    	  selectedValues = Collections.emptyList();
      }
      if ((iMin >= 0) && (iMax >= 0))
      {
    	  for(int index = iMin; index <= iMax; index++)
    	  {
    		  if (sm.isSelectedIndex(index))
    		  {
    			  if (selectedValues == Collections.emptyList())
    			  {
    				  selectedValues = new ArrayList(); 
    			  }
    			  selectedValues.add(dm.getElementAt(index));
    		  }
    	  }
      }
      return selectedValues;
  }

  /**
   * Returns the width, in pixels, of the given string.
   * @param compObj the component to use, or null to use a default
   * 'JLabel' object.
   * @param fontObj the font to use, or null to use the font from the
   * given component object.
   * @param str the string to use.
   * @param defWidthVal a default width value to be returned if an error
   * occurs while calculating the width.
   * @return The width, in pixels, of the given string.
   */
  public static int getStringWidth(Component compObj, Font fontObj,
                                                 String str,int defWidthVal)
  {
    try
    {
      if(compObj == null)
      {  //component object not given
        if(fontObj == null)
        {     //font object not given
          if(utilMetricsObj != null)   //if metrics obj created then use it
            return utilMetricsObj.stringWidth(str);
          if(utilLabelObj == null)          //if label not yet created then
            utilLabelObj = new JLabel();    //create label object
          if(utilFontObj == null)                     //if no font obj then
            utilFontObj = utilLabelObj.getFont();     //create font object
                                            //create metrics object:
          utilMetricsObj = utilLabelObj.getFontMetrics(utilFontObj);
                                       //calculate and return string width:
          return utilMetricsObj.stringWidth(str);
        }
        if(utilLabelObj == null)            //if label not yet created then
          utilLabelObj = new JLabel();      //create label object
        compObj = utilLabelObj;        //use created label object
      }       //component object was given
      else if(fontObj == null)         //if font object not given then
        fontObj = compObj.getFont();   //get font from component
                                       //calculate and return string width:
      return compObj.getFontMetrics(fontObj).stringWidth(str);
    }
    catch(Exception ex)
    {    //some kind of exception error
      return defWidthVal;         //return default value
    }
  }

  /**
   * Returns the width, in pixels, of the given string.  The font from
   * the given component object is used.
   * @param compObj the component to use, or null to use a default
   * 'JLabel' object.
   * @param str the string to use.
   * @param defWidthVal a default width value to be returned if an error
   * occurs while calculating the width.
   * @return The width, in pixels, of the given string.
   */
  public static int getStringWidth(Component compObj,
                                                String str, int defWidthVal)
  {
    return getStringWidth(compObj,null,str,defWidthVal);
  }

  /**
   * Returns the width, in pixels, of the given string.  A default 'JLabel'
   * object is used.
   * @param fontObj the font to use, or null to use the font from the
   * given component object.
   * @param str the string to use.
   * @param defWidthVal a default width value to be returned if an error
   * occurs while calculating the width.
   * @return The width, in pixels, of the given string.
   */
  public static int getStringWidth(Font fontObj, String str, int defWidthVal)
  {
    return getStringWidth(null,fontObj,str,defWidthVal);
  }

  /**
   * Returns the width, in pixels, of the given string.
   * @param compObj the component to use, or null to use a default
   * 'JLabel' object.
   * @param fontObj the font to use, or null to use the font from the
   * given component object.
   * @param str the string to use.
   * @return The width, in pixels, of the given string; or 0 if an error
   * occurs.
   */
  public static int getStringWidth(Component compObj, Font fontObj,
                                                                 String str)
  {
    return getStringWidth(compObj,fontObj,str,0);
  }

  /**
   * Returns the width, in pixels, of the given string.  A default 'JLabel'
   * object is used.
   * @param fontObj the font to use, or null to use the font from the
   * given component object.
   * @param str the string to use.
   * @return The width, in pixels, of the given string; or 0 if an error
   * occurs.
   */
  public static int getStringWidth(Font fontObj, String str)
  {
    return getStringWidth(null,fontObj,str,0);
  }

  /**
   * Returns the width, in pixels, of the given string.  The font from
   * the given component object is used.
   * @param compObj the component to use, or null to use a default
   * 'JLabel' object.
   * @param str the string to use.
   * @return The width, in pixels, of the given string; or 0 if an error
   * occurs.
   */
  public static int getStringWidth(Component compObj, String str)
  {
    return getStringWidth(compObj,null,str,0);
  }

  /**
   * Returns the width, in pixels, of the given string.  A default 'JLabel'
   * object and font are used.
   * @param str the string to use.
   * @param defWidthVal a default width value to be returned if an error
   * occurs while calculating the width.
   * @return The width, in pixels, of the given string.
   */
  public static int getStringWidth(String str, int defWidthVal)
  {
    return getStringWidth(null,null,str,defWidthVal);
  }

  /**
   * Returns the width, in pixels, of the given string.  A default 'JLabel'
   * object and font are used.
   * @param str the string to use.
   * @return The width, in pixels, of the given string; or 0 if an error
   * occurs.
   */
  public static int getStringWidth(String str)
  {
    return getStringWidth(null,null,str,0);
  }

  /**
   * Generates a spacer string containing the maximum number of space
   * characters that will fit within the given pixel width.  The size
   * of the space character ('spaceCharSize') may be generated via the
   * 'getStringWidth()' method.
   * @param widthVal the pixel width to use.
   * @param spaceCharSize the size (in pixels) of a space character to use,
   * or 0 to use a default size.
   * @return A string containing 0 or more space characters.
   */
  public static String getSpacerStrForWidth(int widthVal, int spaceCharSize)
  {
    if(spaceCharSize <= 0 && (spaceCharSize=getStringWidth(SPACE_STR)) <= 0)
      spaceCharSize = 4;
    return UtilFns.getSpacerString(widthVal/spaceCharSize);
  }

  /**
   * Generates a spacer string containing the maximum number of space
   * characters that will fit within the given pixel width, using the
   * given component and font.
   * @param widthVal the pixel width to use.
   * @param compObj the component to use, or null to use a default
   * 'JLabel' object.
   * @param fontObj the font to use, or null to use the font from the
   * given component object.
   * @param defWidthVal a default width value for the size of space
   * character in pixels (used if an error occurs while determining the
   * size).
   * @return A string containing 0 or more space characters.
   */
  public static String getSpacerStrForWidth(int widthVal, Component compObj,
                                              Font fontObj, int defWidthVal)
  {
    return getSpacerStrForWidth(widthVal,getStringWidth(compObj,
                                            fontObj,SPACE_STR,defWidthVal));
  }

  /**
   * Generates a spacer string containing the maximum number of space
   * characters that will fit within the given pixel width, using the
   * given component and font.
   * @param widthVal the pixel width to use.
   * @param compObj the component to use, or null to use a default
   * 'JLabel' object.
   * @param fontObj the font to use, or null to use the font from the
   * given component object.
   * @return A string containing 0 or more space characters.
   */
  public static String getSpacerStrForWidth(int widthVal, Component compObj,
                                                               Font fontObj)
  {
    return getSpacerStrForWidth(widthVal,getStringWidth(compObj,
                                        fontObj,SPACE_STR,DEF_SPACE_WIDTH));
  }

  /**
   * Generates a spacer string containing the maximum number of space
   * characters that will fit within the given pixel width, using the
   * given component.
   * @param widthVal the pixel width to use.
   * @param compObj the component to use, or null to use a default
   * 'JLabel' object.
   * @param defWidthVal a default width value for the size of space
   * character in pixels (used if an error occurs while determining the
   * size).
   * @return A string containing 0 or more space characters.
   */
  public static String getSpacerStrForWidth(int widthVal, Component compObj,
                                                            int defWidthVal)
  {
    return getSpacerStrForWidth(widthVal,getStringWidth(compObj,
                                               null,SPACE_STR,defWidthVal));
  }

  /**
   * Generates a spacer string containing the maximum number of space
   * characters that will fit within the given pixel width, using the
   * given component.
   * @param widthVal the pixel width to use.
   * @param compObj the component to use, or null to use a default
   * 'JLabel' object.
   * @return A string containing 0 or more space characters.
   */
  public static String getSpacerStrForWidth(int widthVal, Component compObj)
  {
    return getSpacerStrForWidth(widthVal,getStringWidth(compObj,
                                           null,SPACE_STR,DEF_SPACE_WIDTH));
  }

  /**
   * Sets the given scroll-pane to have its scroll-bars centered.  This
   * action is delayed via "SwingUtilities.invokeLater()" to help allow
   * the scroll-pane to get setup.
   * @param paneObj the scroll-pane object to use.
   */
  public static void setScrollPaneCentered(final JScrollPane paneObj)
  {
    SwingUtilities.invokeLater(new Runnable()
        {        //do action after other Swing business is complete
          public void run()
          {
            try
            {           //get horizontal scroll-bar:
              JScrollBar barObj = paneObj.getHorizontalScrollBar();
                        //put scroll-bar at 50%:
              barObj.setValue((barObj.getMaximum()-
                          barObj.getVisibleAmount()+barObj.getMinimum())/2);
                        //get vertical scroll-bar:
              barObj = paneObj.getVerticalScrollBar();
                        //put scroll-bar at 50%:
              barObj.setValue((barObj.getMaximum()-
                          barObj.getVisibleAmount()+barObj.getMinimum())/2);
            }
            catch(Exception ex)
            {
              ex.printStackTrace();
            }
          }
        });
  }

  /**
   * Sets the fixed cell width that the given 'JList' is to have when
   * its contents are empty.  This is accomplished via a 'ListDataListener'
   * that is added to the given list's model, which enters the given fixed
   * cell width when the list is empty and enters '-1' (via the
   * 'setFixedCellWidth()' method) when the list contains entries.
   * As such, if a new list model is set for the 'JList' then the
   * listener will no longer operate.
   * @param listObj the 'JList' to use.
   * @param widthVal the width value to use.
   */
  public static void setEmptyJListWidth(final JList listObj,
                                                         final int widthVal)
  {
    if(listObj.getModel().getSize() <= 0)   //if list initially empty then
      listObj.setFixedCellWidth(widthVal);  //enter the fixed cell width
              //add data listener to list model:
    listObj.getModel().addListDataListener(new ListDataListener()
        {
          private boolean fixedCellWidthFlag =   //tracking flag
                                        (listObj.getModel().getSize() <= 0);
          public void intervalAdded(ListDataEvent evtObj)
          {
            if(fixedCellWidthFlag)
            {      //fixed cell width is setup
              fixedCellWidthFlag = false;        //clear flag
              listObj.setFixedCellWidth(-1);     //clear cell width
            }
          }
          public void intervalRemoved(ListDataEvent evtObj)
          {
            if(!fixedCellWidthFlag && listObj.getModel().getSize() <= 0)
            {      //fixed cell width is not setup and list is empty
              fixedCellWidthFlag = true;              //set flag
              listObj.setFixedCellWidth(widthVal);    //set cell width
            }
          }
          public void contentsChanged(ListDataEvent evtObj)
          {
            if(fixedCellWidthFlag)
            {      //fixed cell width is setup
              if(listObj.getModel().getSize() > 0)
              {    //list is not empty
                fixedCellWidthFlag = false;           //clear flag
                listObj.setFixedCellWidth(-1);        //clear cell width
              }
            }
            else
            {      //fixed cell width is not setup
              if(listObj.getModel().getSize() <= 0)
              {    //list is empty
                fixedCellWidthFlag = true;            //set flag
                listObj.setFixedCellWidth(widthVal);  //set cell width
              }
            }
          }
        });
  }

  /**
   * Returns the "extended" state of the given frame.  If the frame is
   * "maximized" (both horizontally and vertically) then the following test
   * will be true:  (state & Frame.MAXIMIZED_BOTH) == Frame.MAXIMIZED_BOTH
   * This utility method allows programs compiled under Java 1.3 to access
   * the Java >=1.4 method 'Frame.getExtendedState()'.
   * @param frameObj 'Frame' object to use.
   * @return The "extended" state of the given frame.
   */
  public static int getExtendedFrameState(Frame frameObj)
  {
    try
    {
      final Object obj;
      if(frameGetExtendedStateMethodObj != null &&
    	  (obj=frameGetExtendedStateMethodObj.invoke(frameObj, (Object[]) null))
    	  instanceof Integer)
      {
        return ((Integer)obj).intValue();
      }
    }
    catch(Exception ex)
    {    //some kind of exception error
    }
    return frameObj.getState();   //fall back to returning 'getState()'
  }

  /**
   * Sets the "extended" state of the given frame.  This utility method
   * allows programs compiled under Java 1.3 to access the Java >=1.4
   * method 'Frame.setExtendedState()'.
   * @param frameObj 'Frame' object to use.
   * @param stateVal frame-state-bitmap value to use.
   * @return true if successful; false if not.
   */
  public static boolean setExtendedFrameState(Frame frameObj, int stateVal)
  {
    try
    {
      if(frameSetExtendedStateMethodObj != null)
      {
        frameSetExtendedStateMethodObj.invoke(frameObj,
                                (new Object [] { Integer.valueOf(stateVal) }));
        return true;         //indicate success
      }
    }
    catch(Exception ex)
    {    //some kind of exception error
    }
    return false;            //indicate failure
  }

  /*
   * This method picks good column sizes.
   * @param tableObj the table object.
   */
  public static void initColumnSizes(JTable tableObj) {
    TableColumn tableColumn;
    Component comp;
    int width;
    final TableModel model = tableObj.getModel();
    final int maxRow = model.getRowCount();

    final int row = 0;
    final int maxCol = model.getColumnCount();
    final TableCellRenderer headerRenderer =
        tableObj.getTableHeader().getDefaultRenderer();
    final TableColumnModel tableColumnModel =
        tableObj.getColumnModel();

    for (int col = 0; col < maxCol; col++)
    {
      final int modelCol = tableObj.convertColumnIndexToModel(col);
      tableColumn = tableColumnModel.getColumn(col);
      comp = headerRenderer.getTableCellRendererComponent(
          null, tableColumn.getHeaderValue(),
          false, false, 0, 0);
      width = comp.getPreferredSize().width;
      if (row < maxRow) {  //if row exists
        //use the max of header or cell width
        comp = tableObj.getDefaultRenderer(model.getColumnClass(modelCol)).
            getTableCellRendererComponent(
                tableObj, model.getValueAt(row, modelCol),
                false, false, row, col);
        width = Math.max(width, comp.getPreferredSize().width);
      }
      tableColumn.setPreferredWidth(width);
    }
  }

  /**
   * Determines if the given frame is maximized (both horizontally and
   * vertically).
   * @param frameObj 'Frame' object to use.
   * @return true if the given frame is maximized; false if not.
   */
  public static boolean isFrameMaximized(Frame frameObj)
  {
    return ((getExtendedFrameState(frameObj) & FRAME_MAXIMIZED_BOTH) ==
                                                      FRAME_MAXIMIZED_BOTH);
  }

  /**
   * Sets the state of the given frame to "maximized" (both horizontally
   * and vertically).
   * @param frameObj 'Frame' object to use.
   * @return true if successful; false if not.
   */
  public static boolean maximizeFrame(Frame frameObj)
  {
    return setExtendedFrameState(frameObj,FRAME_MAXIMIZED_BOTH);
  }

  /**
   * Returns the value of Frame.MAXIMIZED_BOTH.
   * @return The value of Frame.MAXIMIZED_BOTH.
   */
  public static int getFrameMaximizedBothValue()
  {
    return FRAME_MAXIMIZED_BOTH;
  }

  /**
   * Returns the maximum bounds for centered windows. These bounds account
   * for objects in the native windowing system such as task bars and menu
   * bars. The returned bounds will reside on a single display with one
   * exception: on multi-screen systems where Windows should be centered
   * across all displays, this method returns the bounds of the entire
   * display area.
   * This utility method allows programs compiled under Java 1.3 to access
   * the Java >=1.4 method 'GraphicsEnvironment.getMaximumWindowBounds()'.
   * @return The maximum bounds for centered windows.
   */
  public static Rectangle getMaximumWindowBounds()
  {
    try
    {
      final Object obj;
      if(getMaximumWindowBoundsMethodObj != null &&
    	  (obj=getMaximumWindowBoundsMethodObj.invoke(
    		  GraphicsEnvironment.getLocalGraphicsEnvironment(), (Object[])null))
    	  instanceof Rectangle)
      {
        return (Rectangle)obj;
      }
    }
    catch(Exception ex)
    {    //some kind of exception error
    }
                   //fall back to returning full screen size:
    return new Rectangle(Toolkit.getDefaultToolkit().getScreenSize());
  }
  
  /**
   * Get the image file.
   * @param parent the parent or null if none.
   * @return the image file or null if none.
   */
  public static File getImageFile(Component parent)
  {
	  return getImageFile(parent, IMAGE_FILTER_DEFAULT, IMAGE_FORMAT_DEFAULT);
  }

  /**
   * Get the image file.
   * @param parent the parent or null if none.
   * @param filter the filter.
   * @param imageFormatDefault the image format default.
   * @return the image file or null if none.
   */
  public static File getImageFile(Component parent, IstiFileFilter filter,
		  String imageFormatDefault)
  {
	  IstiFileChooser chooser = new IstiFileChooser();
	  chooser.setDialogType(IstiFileChooser.SAVE_DIALOG);
	  chooser.setDialogTitle("Save Image");
	  chooser.setAcceptAllFileFilterUsed(false);
	  chooser.setFileFilter(filter);
	  chooser.setCurrentDirectory(null);
	  if (chooser.showSaveDialog(parent) != JFileChooser.APPROVE_OPTION)
	  {
		  return null;
	  }
	  File file = chooser.getSelectedFile();
	  if (imageFormatDefault != null &&
			  getImageFileFormatName(file, null) == null)
	  {
		  file = (new File(file.getPath() + "." +
				  imageFormatDefault));
	  }
	  return file;
  }
  
  private static String getImageFileFormatName(File file, String imageFormatDefault)
  {
	  String ext = FileUtils.getFileExtension(file.getPath());
	  if (!UtilFns.isEmpty(ext))
	  {
		  String s;
		  ext = ext.toLowerCase();
		  for (int i = 0; i < IMAGE_FORMATS.length; i++)
		  {
			  s = IMAGE_FORMATS[i];
			  if (ext.matches(s))
			  {
				  return s;
			  }
		  }
	  }
	  return imageFormatDefault;
  }
  
  /**
   * Save the image to the specified file.
   * @param file the file.
   * @param components the components.
   * @throws IOException if an error occurs during writing.
   */
  public static void saveImage(File file, Component ... components)
		  throws IOException
  {
	  if (components == null || components.length == 0)
	  {
		  return;
	  }
	  Component component = components[0];
	  final Dimension size = component.getSize();
	  for (int i = 1; i < components.length; i++)
	  {
		  component = components[i];
		  size.width = Math.max(component.getWidth(), size.width);
		  size.height += component.getHeight();
	  }
	  final BufferedImage image = new BufferedImage(
			  size.width, size.height, BufferedImage.TYPE_INT_RGB);
	  final Graphics2D g = image.createGraphics();
	  int height = 0;
	  for (int i = 0; i < components.length; i++)
	  {
		  component = components[i];
		  if (height != 0)
		  {
			  g.translate(0, height);
		  }
		  height += component.getHeight();
		  component.paint(g);
	  }
	  String formatName = getImageFileFormatName(file, IMAGE_FORMAT_DEFAULT);
	  ImageIO.write(image, formatName, file);
  }

  /**
   * Sets the preferred size of the given component to a constant value.
   * This utility method allows programs compiled under Java <=1.4 to
   * access the Java >=1.5 method 'Component.setPreferredSize()'.
   * @param compObj 'Component' object to use.
   * @param dimObj The new preferred size, or null.
   * @return true if successful; false if not.
   */
  public static boolean setPreferredSize(Component compObj, Dimension dimObj)
  {
    try
    {
      if(compSetPreferredSizeMethodObj != null)
      {
        compSetPreferredSizeMethodObj.invoke(compObj,
                                                (new Object [] { dimObj }));
        return true;         //indicate success
      }
    }
    catch(Exception ex)
    {    //some kind of exception error
    }
    return false;            //indicate failure
  }

  /**
   * Set the preferred size for the component.
   * @param compObj the component.
   * @param maxNumChars the maximum number of characters.
   * @param maxCharWidth the maximum character width.
   */
  public static final void setPreferredSize(
      final Component compObj, final int maxNumChars, final int maxCharWidth)
  {
    int padWidth = 0;
    if (compObj instanceof JComboBox)
    {
      padWidth = DEF_PAD_WIDTH;
    }
    setPreferredSize(compObj, maxNumChars, maxCharWidth, padWidth);
  }

  /**
   * Set the preferred size for the component.
   * @param compObj the component.
   * @param maxNumChars the maximum number of characters.
   * @param maxCharWidth the maximum character width.
   * @param padWidth the pad width or 0 if none.
   */
  public static final void setPreferredSize(
      final Component compObj, final int maxNumChars, final int maxCharWidth,
      final int padWidth)
  {
    final Dimension preferredSize = compObj.getPreferredSize();
    if (maxNumChars != 0)
    {
      preferredSize.width += padWidth + maxNumChars * maxCharWidth;
      if(!setPreferredSize(compObj,preferredSize) &&
                                              compObj instanceof JComponent)
      {  //call via reflection failed and component object is JComponent
                   //use JComponent version of method (Java 1.4):
        ((JComponent)compObj).setPreferredSize(preferredSize);
      }
    }
  }

  /**
   * Set the preferred size for the component.
   * @param compObj the component.
   * @param maxNumChars the maximum number of characters.
   */
  public static final void setPreferredSize(
      final JComponent compObj, final int maxNumChars)
  {
    setPreferredSize(compObj, maxNumChars, getMaxCharWidth(compObj));
  }

  /**
   * Set the preferred size for the component.
   * @param compObj the component.
   * @param tv the text validator.
   */
  public static final void setPreferredSize(
      final Component compObj, final TextValidator tv)
  {
    setPreferredSize(compObj, tv.getMaxNumChars(), getMaxCharWidth(compObj, tv));
  }

  /**
   * Calls the 'setContentAreaFilled()' method on all button objects
   * held by the given container or any subcontainers of it.
   * @param contObj container to use.
   * @param flgVal true for content filled; false for not.
   */
  public static void setButtonsContentAreaFilled(Container contObj,
                                                             boolean flgVal)
  {
    try
    {
      final Component [] compArr = contObj.getComponents();
      Component compObj;
      for(int i=0; i<compArr.length; ++i)
      {  //for each component in container
              //if component is a button object then call its method;
              // if component is a container then process it recursively:
        if((compObj=contObj.getComponent(i)) instanceof AbstractButton)
          ((AbstractButton)compObj).setContentAreaFilled(flgVal);
        else if(compObj instanceof Container)
          setButtonsContentAreaFilled((Container)compObj,flgVal);
      }
    }
    catch(Exception ex) {}   //if error then just abort
  }
}
