//HandJButton.java:  An extension of 'JButton' which configures it to
//                   use the "hand" cursor.
//
//   7/12/2000 -- [ET]
//    9/7/2000 -- [ET]  Added small-button option.
//  10/19/2000 -- [ET]  Added constructors with 'icon' objects.
//
//
//=====================================================================
// Copyright (C) 2000 Instrumental Software Technologies, Inc.
//
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions
// are met:
// 1. Redistributions of source code, or portions of this source code,
//    must retain the above copyright notice, this list of conditions
//    and the following disclaimer.
// 2. Redistributions in binary form must reproduce the above copyright
//    notice, this list of conditions and the following disclaimer in
//    the documentation and/or other materials provided with the
//    distribution.
// 3. All advertising materials mentioning features or use of this
//    software must display the following acknowledgment:
//    "This product includes software developed by Instrumental
//    Software Technologies, Inc. (http://www.isti.com)"
// 4. If the software is provided with, or as part of a commercial
//    product, or is used in other commercial software products the
//    customer must be informed that "This product includes software
//    developed by Instrumental Software Technologies, Inc.
//    (http://www.isti.com)"
// 5. The names "Instrumental Software Technologies, Inc." and "ISTI"
//    must not be used to endorse or promote products derived from
//    this software without prior written permission. For written
//    permission, please contact "info@isti.com".
// 6. Products derived from this software may not be called "ISTI"
//    nor may "ISTI" appear in their names without prior written
//    permission of Instrumental Software Technologies, Inc.
// 7. Redistributions of any form whatsoever must retain the following
//    acknowledgment:
//    "This product includes software developed by Instrumental
//    Software Technologies, Inc. (http://www.isti.com/)."
// THIS SOFTWARE IS PROVIDED BY INSTRUMENTAL SOFTWARE
// TECHNOLOGIES, INC. "AS IS" AND ANY EXPRESSED OR IMPLIED
// WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES
// OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
// DISCLAIMED.  IN NO EVENT SHALL INSTRUMENTAL SOFTWARE TECHNOLOGIES,
// INC. OR ITS CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
// INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
// (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
// SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
// HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
// STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
// ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED
// OF THE POSSIBILITY OF SUCH DAMAGE.
//=====================================================================
//  A current version of the software can be found at
//                http://www.isti.com
//  Bug reports and comments should be directed to
//  Instrumental Software Technologies, Inc. at info@isti.com
//=====================================================================
//

package com.isti.util.gui;

import java.awt.*;
import javax.swing.*;

/**
 * Class HandJButton is an extension of 'JButton' which configures it to
 * use the "hand" cursor.
 */
public class HandJButton extends JButton
{
    /**
     * Creates a button with the given text.
     * @param name button name
     */
  public HandJButton(String name)
  {
    super(name);        //call parent constructor; setup "hand" cursor
    setupButton(false);
  }

    /**
     * Creates a button with the given icon.
     * @param iconObj button icon
     */
  public HandJButton(Icon iconObj)
  {
    super(iconObj);     //call parent constructor; setup "hand" cursor
    setupButton(false);
  }

    /**
     * Creates a button with the given name and icon.
     * @param name button name
     * @param iconObj button icon
     */
  public HandJButton(String name,Icon iconObj)
  {
    super(name,iconObj);     //call parent constructor; setup "hand" cursor
    setupButton(false);
  }

    /** Creates a button with no text. */
  public HandJButton()
  {
    super();            //call parent constructor; setup "hand" cursor
    setupButton(false);
  }

    /**
     * Creates a button with the given text.
     * @param name the name for the button.
     * @param smallFlag if true then the button will be "smaller" than
     * the default size.
     */
  public HandJButton(String name,boolean smallFlag)
  {
    super(name);        //call parent constructor; setup "hand" cursor
    setupButton(smallFlag);
  }

    /**
     * Creates a button with the given text.
     * @param name the name for the button.
     * @param iconObj the icon for the button.
     * @param smallFlag if true then the button will be "smaller" than
     * the default size.
     */
  public HandJButton(String name,Icon iconObj,boolean smallFlag)
  {
    super(name,iconObj);     //call parent constructor; setup "hand" cursor
    setupButton(smallFlag);
  }

    /**
     * Creates a button with the given text.
     * @param iconObj the icon for the button.
     * @param smallFlag if true then the button will be "smaller" than
     * the default size.
     */
  public HandJButton(Icon iconObj,boolean smallFlag)
  {
    super(iconObj);          //call parent constructor; setup "hand" cursor
    setupButton(smallFlag);
  }

    /**
     * Creates a button with no text.
     * @param smallFlag if true then the button will be "smaller" than
     * the default size.
     */
  public HandJButton(boolean smallFlag)
  {
    super();            //call parent constructor; setup "hand" cursor
    setupButton(smallFlag);
  }

    //Initializes special button properties.
  private void setupButton(boolean smallFlag)
  {
    setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
    if(smallFlag)
    {    //make button smaller; get current insets for margins
      final Insets i = (Insets)(getMargin().clone());
      i.left /= 2;                     //reduce insets
      i.top /= 2;
      i.right /= 2;
      i.bottom /= 2;
      setMargin(i);                    //set new insets for margins
    }
  }
}

