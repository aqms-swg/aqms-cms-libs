//HintManager.java:  Defines a hint manager.
//
//=====================================================================
// Copyright (C) 2008 Instrumental Software Technologies, Inc.
//
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions
// are met:
// 1. Redistributions of source code, or portions of this source code,
//    must retain the above copyright notice, this list of conditions
//    and the following disclaimer.
// 2. Redistributions in binary form must reproduce the above copyright
//    notice, this list of conditions and the following disclaimer in
//    the documentation and/or other materials provided with the
//    distribution.
// 3. All advertising materials mentioning features or use of this
//    software must display the following acknowledgment:
//    "This product includes software developed by Instrumental
//    Software Technologies, Inc. (http://www.isti.com)"
// 4. If the software is provided with, or as part of a commercial
//    product, or is used in other commercial software products the
//    customer must be informed that "This product includes software
//    developed by Instrumental Software Technologies, Inc.
//    (http://www.isti.com)"
// 5. The names "Instrumental Software Technologies, Inc." and "ISTI"
//    must not be used to endorse or promote products derived from
//    this software without prior written permission. For written
//    permission, please contact "info@isti.com".
// 6. Products derived from this software may not be called "ISTI"
//    nor may "ISTI" appear in their names without prior written
//    permission of Instrumental Software Technologies, Inc.
// 7. Redistributions of any form whatsoever must retain the following
//    acknowledgment:
//    "This product includes software developed by Instrumental
//    Software Technologies, Inc. (http://www.isti.com/)."
// THIS SOFTWARE IS PROVIDED BY INSTRUMENTAL SOFTWARE
// TECHNOLOGIES, INC. "AS IS" AND ANY EXPRESSED OR IMPLIED
// WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES
// OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
// DISCLAIMED.  IN NO EVENT SHALL INSTRUMENTAL SOFTWARE TECHNOLOGIES,
// INC. OR ITS CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
// INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
// (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
// SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
// HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
// STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
// ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED
// OF THE POSSIBILITY OF SUCH DAMAGE.
//=====================================================================
//  A current version of the software can be found at
//                http://www.isti.com
//  Bug reports and comments should be directed to
//  Instrumental Software Technologies, Inc. at info@isti.com
//=====================================================================
//

package com.isti.util.gui;

import java.awt.*;
import java.awt.event.*;
import java.util.*;
import javax.swing.*;
import javax.swing.table.*;
import java.lang.reflect.Method;
import com.isti.util.HtmlUtilFns;

/**
 * HintManager is a reusable class that provides an implementation for
 * status-bar hints. Using it is straightforward.
 * Just construct an instance of HintManager passing a hint component
 *   (JLabel or JTextField) instance that will be used to display the hints;
 * call the method addHintFor to set a hint for each component you want;
 * and call the method enableHints for the user interface top container
 * (window, frame, dialog box, etc.) after adding all the components.
 */
public class HintManager
    implements FocusListener, MouseListener
{
  /** Mouse event type. */
  public static final int MOUSE_EVENT_TYPE = 0x01;
  /** Focus event type. */
  public static final int FOCUS_EVENT_TYPE = 0x02;
  /** The hint clear text. */
  protected static final String hintClearText = " ";
  private boolean useToolTipTextFlag = true;
  private final Map hintMap;
  private final Object hintComponent;
  private final Method setTextMethodObj;
  /** Additional hint text color or null if none. */
  private Color additionalHintTextColor = null;
  /** Additional hint text bold flag, true for bold, false otherwise. */
  private boolean additionalHintTextBoldFlag = false;

  /**
   * Creates the hint manager.
   * @param hintComponent the hint component (JLabel or JTextField), which
   * must have a 'setText(String)' method.
   * @throws Exception if error.
   */
  public HintManager(Object hintComponent) throws Exception
  {
    hintMap = new WeakHashMap();
    this.hintComponent = hintComponent;
    setTextMethodObj =
        hintComponent.getClass().getMethod(
            "setText", (new Class[]
                        {String.class}));
  }

  /**
   * Adds a hint for the component.
   * @param comp the component.
   * @param hintText the hint text.
   */
  public void addHintFor(Component comp, String hintText)
  {
    hintMap.put(comp, hintText);
  }

  /**
   * Disables hints for the component and all children.
   * @param comp the component.
   */
  public void disableHints(Component comp)
  {
    comp.removeMouseListener(this);
    comp.removeFocusListener(this);
    if (comp instanceof Container)
    {
      Component[] components = ( (Container) comp).getComponents();
      for (int i = 0; i < components.length; i++)
      {
        disableHints(components[i]);
      }
    }
    if (comp instanceof MenuElement)
    {
      MenuElement[] elements = ( (MenuElement) comp).getSubElements();
      for (int i = 0; i > elements.length; i++)
      {
        disableHints(elements[i].getComponent());
      }
    }
  }

  /**
   * Enable hints for the component and all children.
   * The default is for tool tip text to be used if no hint can be found but
   * this may be disabled by calling 'setUseToolTipText(false)' prior to
   * calling this method.
   * @param comp the component.
   * @param type the type of event to listen to, MOUSE_EVENT_TYPE for
   * mouse events and FOCUS_EVENT_TYPE for focus events.
   * @see setUseToolTipText
   */
  public void enableHints(Component comp, int type)
  {
    final String hintText = getHintFor(comp);
    if ( (type & MOUSE_EVENT_TYPE) != 0)
    {
      if (hintText != null)
      {
        comp.addMouseListener(this);
      }
    }
    if ( (type & FOCUS_EVENT_TYPE) != 0)
    {
      if (hintText != null)
      {
        comp.addFocusListener(this);
      }
    }
    if (comp instanceof Container)
    {
      Component[] components = ( (Container) comp).getComponents();
      for (int i = 0; i < components.length; i++)
      {
        enableHints(components[i], type);
      }
    }
    if (comp instanceof MenuElement)
    {
      MenuElement[] elements = ( (MenuElement) comp).getSubElements();
      for (int i = 0; i > elements.length; i++)
      {
        enableHints(elements[i].getComponent(), type);
      }
    }
  }

  /**
   * Determines if tool tip text is used if no hints were found.
   * @return true if tool tip text is used, false otherwise.
   */
  public boolean isUseToolTipText()
  {
    return useToolTipTextFlag;
  }

  /**
   * Sets the additional hint text color attributes.
   * @param color the color or null if none.
   * @param boldFlag true for bold, false otherwise.
   */
  public void setAddtionalHintTextColor(Color color, boolean boldFlag) {
    additionalHintTextColor = color;
    additionalHintTextBoldFlag = boldFlag;
  }

  /**
   * Sets if tool tip text is used if no hints were found.
   * This should be set before enabling hints.
   * @param b true if tool tip text is used, false otherwise.
   * @see enableHints
   */
  public void setUseToolTipText(boolean b)
  {
    useToolTipTextFlag = b;
  }

  /**
   * Gets the hint for the component.
   * @param comp the component.
   * @return the hint text or null if none.
   */
  private String getHintFor(Component comp)
  {
    String hint = (String) hintMap.get(comp);
    if (hint == null)
    {
      if (comp instanceof JLabel)
        hint = (String) hintMap.get( ( (JLabel) comp).getLabelFor());
      else if (comp instanceof JTableHeader)
        hint = (String) hintMap.get( ( (JTableHeader) comp).getTable());
    }
    if (hint == null && useToolTipTextFlag)
    {
      if (comp instanceof JComponent)
      {
        hint = ( (JComponent) comp).getToolTipText();
        hint = addComponentHintText(hint, comp);
      }
    }
    return hint;
  }

  /**
   * Adds help text for the specific component if needed.
   * @param hint the hint text or null if none.
   * @param comp the component.
   * @return the hint text or null if none.
   */
  protected String addComponentHintText(String hint, Component comp)
  {
    if (hint == null) {
      return hint;
    }
    String additionalHintText = null;
    if (comp instanceof JComboBox ||
        (comp = comp.getParent()) instanceof JComboBox)
    {
      if ( ( (JComboBox) comp).isEditable())
      {
        additionalHintText = "to enter a new value press Tab to continue";
      }
    }
    return getHintText(hint, additionalHintText);
  }

  /**
   * Gets the full hint text.
   * @param hint the hint text.
   * @param additionalHintText the additional hint text.
   * @return the hint text or null if none.
   */
  protected String getFullHintText(String hint, String additionalHintText)
  {
    return hint + ", " + additionalHintText;
  }

  /**
   * Gets the hint text.
   * @param hint the hint text.
   * @param additionalHintText the additional hint text or null if none.
   * @return the hint text or null if none.
   */
  protected String getHintText(String hint, String additionalHintText)
  {
    if (additionalHintText != null)
    {
      boolean htmlFlag = false;
      if (additionalHintTextBoldFlag)
      {
        additionalHintText = HtmlUtilFns.getText(
            additionalHintText, HtmlUtilFns.BOLD_ELEMENT);
        htmlFlag = true;
      }
      if (additionalHintTextColor != null)
      {
        additionalHintText =
            HtmlUtilFns.getFontText(additionalHintText,
                                    additionalHintTextColor);
        htmlFlag = true;
      }
      return getHintText(hint, additionalHintText, htmlFlag);
    }
    return hint;
  }

  /**
   * Gets the hint text.
   * @param hint the hint text.
   * @param additionalHintText the additional hint text.
   * @param htmlFlag true to use HTML text, false otherwise.
   * @return the hint text or null if none.
   */
  protected String getHintText(
      String hint, String additionalHintText, boolean htmlFlag)
  {
    String s = getFullHintText(hint, additionalHintText);
    if (htmlFlag)
    {
      s = HtmlUtilFns.getHtmlText(s);
    }
    return s;
  }

  /**
   * Gets the hint clear text.
   * @return the hint clear text.
   */
  protected String getHintClearText()
  {
    return hintClearText;
  }

  /**
   * Sets the hint on the hint component.
   * @param hint the hint text or null if none.
   */
  protected void setHint(String hint)
  {
    if (hint == null || hint.length() == 0)
    {
      hint = getHintClearText();
    }
    try
    {
      setTextMethodObj.invoke(
          hintComponent, new Object[]
          {hint});
    }
    catch (Exception ex)
    {
    }
  }

  public void focusGained(FocusEvent e)
  {
    Component comp = (Component) e.getSource();
    String hint;
    do
    {
      hint = getHintFor(comp);
    }
    while ( (hint == null) && ( (comp = comp.getParent()) != null));
    if (hint != null)
    {
      setHint(hint);
    }
  }

  public void focusLost(FocusEvent e)
  {
    setHint(null);
  }

  public void mouseEntered(MouseEvent e)
  {
    Component comp = (Component) e.getSource();
    String hint;
    do
    {
      hint = getHintFor(comp);
    }
    while ( (hint == null) && ( (comp = comp.getParent()) != null));
    if (hint != null)
    {
      setHint(hint);
    }
  }

  public void mouseExited(MouseEvent e)
  {
    setHint(null);
  }

  public void mouseClicked(MouseEvent e)
  {}

  public void mousePressed(MouseEvent e)
  {}

  public void mouseReleased(MouseEvent e)
  {}
}
