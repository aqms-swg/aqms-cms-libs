//ImageIconLoader.java:  Defines a helper method for loading an image icon
//                       from a URL or file, including a file inside of
//                       a 'jar' archive file.
//
//  10/7/2003 -- [ET]
//

package com.isti.util.gui;

import java.net.URL;
import java.awt.MediaTracker;
import javax.swing.ImageIcon;
import com.isti.util.FileUtils;

/**
 * Class ImageIconLoader defines a helper method for loading an image icon
 * from a URL or file, including a file inside of a 'jar' archive file
 * reference via the 'classpath'.
 */
public class ImageIconLoader
{
    //private constructor so that no object instances may be created
    // (static access only)
  private ImageIconLoader()
  {
  }

  /**
   * Loads an image icon from a URL or file.  The file may also reside
   * inside of a 'jar' archive file reference via the 'classpath'.
   * @param iconURLStr URL or filename for the icon to loaded.
   * @return A new 'ImageIcon' object, or null if the image-icon
   * could not be loaded.
   */
  public static ImageIcon load(String iconURLStr)
  {
    ImageIcon iconObj = null;
    if(iconURLStr != null)
    {    //icon URL string was given
      try
      {       //load image icon using given URL/filename:
        final URL urlObj;                //open image file:
        if((urlObj=FileUtils.fileMultiOpenInputURL(iconURLStr)) != null)
        {     //file opened as URL; create image icon from URL
          iconObj = new ImageIcon(urlObj);
              //if load status not OK then clear handle to icon:
          if(iconObj.getImageLoadStatus() != MediaTracker.COMPLETE)
            iconObj = null;
        }
      }
      catch(Exception ex)
      {  //some kind of exception error
        iconObj = null;      //clear handle to icon
      }
    }
    return iconObj;
  }
}
