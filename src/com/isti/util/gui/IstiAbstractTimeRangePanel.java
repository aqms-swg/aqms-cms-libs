//IstiAbstractTimeRangePanel.java:  Implements an abstract time range panel.
//
//   9/18/2003 -- [KF]
//    8/8/2007 -- [ET]  Added 'setStartTime()' and 'setEndTime()' methods.
//   4/17/2012 -- [KF]  Added 'setEndDate()', 'setMaxDate()', 'setMinDate()'
//                      and 'setStartDate()' methods.
//

package com.isti.util.gui;

import java.util.Date;

import javax.swing.JPanel;

/**
 * Class IstiAbstractTimeRangePanel implements an abstract time range panel.
 */
public abstract class IstiAbstractTimeRangePanel extends JPanel {
  /**
   * 
   */
  private static final long serialVersionUID = 7679014767584925648L;

  /**
   * Gets the end time (number of milliseconds since January 1, 1970, 00:00:00
   * GMT.)
   * @return the end time or 0 if it is greater than or equal to the maximum.
   */
  public abstract long getEndTime();

  /**
   * Gets the end time offset (number of milliseconds before the current time).
   * GMT.)
   * @return the end time offset or 0 if it is greater than or equal to the
   *         maximum.
   */
  public long getEndTimeOffset() {
    return convertTimeToOffset(getEndTime());
  }

  /**
   * Gets the maximum time (number of milliseconds since January 1, 1970,
   * 00:00:00 GMT.)
   * @return the maximum time.
   */
  public abstract long getMaxTime();

  /**
   * Gets the minimum time (number of milliseconds since January 1, 1970,
   * 00:00:00 GMT.)
   * @return the minimum time.
   */
  public abstract long getMinTime();

  /**
   * Gets the start time (number of milliseconds since January 1, 1970, 00:00:00
   * GMT.)
   * @return the start time or 0 if it is less than or equal to the minumum.
   */
  public abstract long getStartTime();

  /**
   * Gets the start time offset (number of milliseconds before the current
   * time). GMT.)
   * @return the start time offset or 0 if it is less than or equal to the
   *         minumum.
   */
  public long getStartTimeOffset() {
    return convertTimeToOffset(getStartTime());
  }

  /**
   * Sets the end date.
   * @param endDate the end date.
   */
  public void setEndDate(Date endDate) {
    setEndTime(endDate.getTime());
  }

  /**
   * Sets the end time.
   * @param endTime the end time.
   */
  public abstract void setEndTime(long endTime);

  /**
   * Sets the maximum date.
   * @param maxDate the maximum date.
   */
  public void setMaxDate(Date maxDate) {
    setMaxTime(maxDate.getTime());
  }

  /**
   * Sets the maximum time (number of milliseconds since January 1, 1970,
   * 00:00:00 GMT.)
   * @param time the maximum time.
   */
  public abstract void setMaxTime(long time);

  /**
   * Sets the minimum date.
   * @param minDate the minimum date.
   */
  public void setMinDate(Date minDate) {
    setMinTime(minDate.getTime());
  }

  /**
   * Sets the minimum time (number of milliseconds since January 1, 1970,
   * 00:00:00 GMT.)
   * @param time the minimum time.
   */
  public abstract void setMinTime(long time);

  /**
   * Sets the start date.
   * @param startDate the start date.
   */
  public void setStartDate(Date startDate) {
    setStartTime(startDate.getTime());
  }

  /**
   * Sets the start time.
   * @param startTime the start time.
   */
  public abstract void setStartTime(long startTime);

  /**
   * Converts the time offset (number of milliseconds before the current time)
   * to a time (number of milliseconds since January 1, 1970, 00:00:00 GMT.)
   * @param timeOffset the time offset.
   * @return the time.
   */
  protected static long convertOffsetToTime(long timeOffset) {
    if (timeOffset == 0)
      return 0;
    return System.currentTimeMillis() - timeOffset;
  }

  /**
   * Converts the time (number of milliseconds since January 1, 1970, 00:00:00
   * GMT) to a time offset (number of milliseconds before the current time.)
   * @param time the time.
   * @return the time offset.
   */
  protected static long convertTimeToOffset(long time) {
    if (time == 0)
      return 0;
    return System.currentTimeMillis() - time;
  }
}
