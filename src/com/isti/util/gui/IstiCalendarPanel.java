//IstiCalendarPanel.java:  Implements a calendar panel that allows selection
//                         of a date.
//
//   8/13/2002 -- [KF]
//   8/15/2002 -- [ET]  Added javadoc comment for class.
//   8/24/2002 -- [KF]  Reduce width of panel and other cleanup.
//   10/2/2002 -- [KF]  Do not allow reordering of columns.
//   9/26/2003 -- [KF]  Use static inner classes 'CalendarTableModel' and
//                      'CalendarTableCellRenderer' instead of outer classes
//                      'IstiTableModel' and 'IstiTableCellRenderer'.
//    4/3/2009 -- [ET]  Replaced call to deprecated Dialog 'show()' method
//                      with 'setVisible()'.
//

package com.isti.util.gui;

import javax.swing.*;
import javax.swing.table.*;
import java.util.*;
import javax.swing.event.*;
import java.awt.*;
import java.awt.event.*;
import javax.swing.plaf.basic.BasicArrowButton;

/**
 * Class IstiCalendarPanel implements a calendar panel that allows selection
 * of a date.
 */
public class IstiCalendarPanel extends JPanel
{
  /** The month text. */
  public final static String MONTH_TEXT[] = new String[]
  {
    "Jan", "Feb", "Mar", "Apr", "May", "Jun",
    "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"
  };
  /** The default minimum year. */
  public final static int MIN_YEAR = 1970;
  /** The default maximum year. */
  public final static int MAX_YEAR = 2070;

  protected final Calendar calendar, workingCalendar;
  protected final Date minTime;
  protected final Date maxTime;
  protected JDialog dialog = null;

  protected final JPanel topPanel = new JPanel();
  protected final JPanel monthPanel = new JPanel(new BorderLayout());
  protected final JPanel yearPanel = new JPanel(new BorderLayout());
  protected final JButton previousMonthButton = new BasicArrowButton(SwingConstants.WEST);
  protected final JButton previousYearButton = new BasicArrowButton(SwingConstants.WEST);

  protected final JComboBox monthComboBox = new JComboBox(MONTH_TEXT);
  protected final YearJComboBox yearComboBox = new YearJComboBox();
  protected final JButton nextYearButton = new BasicArrowButton(SwingConstants.EAST);
  protected final JButton nextMonthButton = new BasicArrowButton(SwingConstants.EAST);

  protected final CalendarTableModel tableModel = new CalendarTableModel();
  protected final JTable table = new JTable(tableModel);

  public IstiCalendarPanel()
  {
    this(null);
  }

  public IstiCalendarPanel(Calendar cal)
  {
    this(cal, null, null);
  }

  public IstiCalendarPanel(Calendar cal, Date minTime, Date maxTime)
  {
    // if no calendar provided use current date and the default time zone
    if (cal == null)
    {
      cal = Calendar.getInstance();
    }

    // save the calendar objects
    this.calendar = cal;
    this.workingCalendar = (Calendar)calendar.clone();
    this.minTime = minTime;
    this.maxTime = maxTime;

    try
    {
      jbInit();
    }
    catch(Exception e)
    {
      e.printStackTrace();
    }
  }

  /**
   * provide the dialog object in order to close it when a selection is made.
   *
   * @param dialog the <code>JDialog</code> used to close the calendar
   */
  public void setDialog(JDialog dialog)
  {
    this.dialog = dialog;
  }

    /**
     * Creates a modal Calendar dialog with a default title
     * and the specified owner frame.
     *
     * @param owner the <code>Component</code> from which the dialog is displayed
     * @param calendar  the <code>Calendar</code> object to use
     */
  public static void createDialog(Component owner, Calendar calendar)
  {
    createDialog(owner, calendar, "Calendar");
  }

  /**
   * Creates a modal Calendar dialog with the specified title
   * and the specified owner frame.
   *
   * @param owner the <code>Component</code> from which the dialog is displayed
   * @param calendar  the <code>Calendar</code> object to use
   * @param title  the <code>String</code> to display in the dialog's
   *			title bar
   */
  public static void createDialog(Component owner, Calendar calendar, String title)
  {
    createDialog(owner, calendar, title, null, null);
  }

    /**
     * Creates a modal Calendar dialog with the specified title
     * and the specified owner frame.
     *
     * @param owner the <code>Component</code> from which the dialog is displayed
     * @param calendar  the <code>Calendar</code> object to use
     * @param title  the <code>String</code> to display in the dialog's
     *			title bar
     * @param minTime the minimum time or null if none.
     * @param maxTime the maximum time or null if none.
     */
  public static void createDialog(Component owner, Calendar calendar, String title,
      Date minTime, Date maxTime)
  {
    // Bring up IstiCalendarPanel
    IstiCalendarPanel panel = new IstiCalendarPanel(calendar, minTime, maxTime);
    SimpleDialog dlg = new SimpleDialog(owner, title, true,
      panel, new JButton("Cancel"));
    panel.setDialog(dlg);
    dlg.setResizable(false);
    dlg.setVisible(true);
  }

  private void jbInit() throws Exception
  {
    // allow selection of rows and columns
    table.setRowSelectionAllowed(true);
    table.setColumnSelectionAllowed(true);

    // do not allow reordering of columns
    table.getTableHeader().setReorderingAllowed(false);

    previousMonthButton.addActionListener(new java.awt.event.ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        final int currentMonth = workingCalendar.get(Calendar.MONTH);
        final int previousMonth;

        if (currentMonth == Calendar.JANUARY)
        {
          final int year = workingCalendar.get(Calendar.YEAR);
          if (year > yearComboBox.minYear)
          {
            workingCalendar.add(Calendar.YEAR, -1);
          }
          previousMonth = Calendar.DECEMBER;
        }
        else
        {
          previousMonth = currentMonth - 1;
        }
        workingCalendar.set(Calendar.MONTH, previousMonth);

        // update the days of the month
        updateDays();
      }
    });
    previousYearButton.addActionListener(new java.awt.event.ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        final int year = workingCalendar.get(Calendar.YEAR);
        if (year > yearComboBox.minYear)
        {
          workingCalendar.add(Calendar.YEAR, -1);
        }
        // update the days of the month
        updateDays();
      }
    });

    // add the selections for the year combo box
    yearComboBox.setCalendar(workingCalendar);
    yearComboBox.setMinTime(minTime);
    yearComboBox.setMaxTime(maxTime);
    yearComboBox.updateYears();

    nextYearButton.addActionListener(new java.awt.event.ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        final int year = workingCalendar.get(Calendar.YEAR);
        if (year < yearComboBox.maxYear)
        {
          workingCalendar.add(Calendar.YEAR, 1);
        }
        // update the days of the month
        updateDays();
      }
    });
    nextYearButton.setHorizontalTextPosition(AbstractButton.LEFT);
    nextMonthButton.addActionListener(new java.awt.event.ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        final int currentMonth = workingCalendar.get(Calendar.MONTH);
        final int nextMonth;

        if (currentMonth == Calendar.DECEMBER)
        {
          nextMonth = Calendar.JANUARY;
          final int year = workingCalendar.get(Calendar.YEAR);
          if (year < yearComboBox.maxYear)
          {
            workingCalendar.add(Calendar.YEAR, 1);
          }
        }
        else
        {
          nextMonth = currentMonth + 1;
        }
        workingCalendar.set(Calendar.MONTH, nextMonth);

        // update the days of the month
        updateDays();
      }
    });
    nextMonthButton.setHorizontalTextPosition(AbstractButton.LEFT);

    monthComboBox.addActionListener(new java.awt.event.ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        workingCalendar.set(Calendar.MONTH, monthComboBox.getSelectedIndex());

        // update the days of the month
        updateDays();
      }
    });
    yearComboBox.addActionListener(new java.awt.event.ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        if (yearComboBox.isInitYearsInProgress())
        {
          return;
        }
        workingCalendar.set(Calendar.YEAR, yearComboBox.getYear());

        // update the days of the month
        updateDays();
      }
    });

    topPanel.setBorder(BorderFactory.createEmptyBorder(0,0,5,0));
    topPanel.setLayout(new FlowLayout(FlowLayout.CENTER, 20, 0));

    // month panel
    monthPanel.add(previousMonthButton, BorderLayout.WEST);
    Font monthFont = monthComboBox.getFont();
    monthComboBox.setFont(new Font(monthFont.getFontName(), monthFont.getStyle(), monthFont.getSize() * 3 / 2));
    monthPanel.add(monthComboBox, BorderLayout.CENTER);
    monthPanel.add(nextMonthButton, BorderLayout.EAST);
    topPanel.add(monthPanel);

    // year panel
    yearPanel.add(previousYearButton, BorderLayout.WEST);
    Font yearFont = yearComboBox.getFont();
    yearComboBox.setFont(new Font(yearFont.getFontName(), yearFont.getStyle(), yearFont.getSize() * 3 / 2));
    yearPanel.add(yearComboBox, BorderLayout.CENTER);
    yearPanel.add(nextYearButton, BorderLayout.EAST);
    topPanel.add(yearPanel);

    // set the size of the view port to the minimum size
    table.setPreferredScrollableViewportSize(table.getMinimumSize());

    // disable column resizing
    for (int i = 0; i < table.getColumnCount(); i++)
    {
      final TableColumn tableColumn = table.getColumnModel().getColumn(i);
      tableColumn.setResizable(false);
    }

    this.setLayout(new BorderLayout());
    this.add(topPanel, BorderLayout.NORTH);
    this.add(new JScrollPane(table), BorderLayout.CENTER);

    // set the default renderer to our own to display values differently
    table.setDefaultRenderer(
        tableModel.getColumnClass(0), new CalendarTableCellRenderer());

    // update the days of the month
    updateDays();

    // set up selection listeners
    ListSelectionModel listSelectionModel = table.getSelectionModel();
    listSelectionModel.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
    listSelectionModel.addListSelectionListener(new ListSelectionListener()
    {
      public void valueChanged(ListSelectionEvent e)
      {
        //Ignore extra messages.
        if (e.getValueIsAdjusting() ||
            !(e.getSource() instanceof ListSelectionModel))
          return;

        ListSelectionModel lsm = (ListSelectionModel)e.getSource();
        if (!lsm.isSelectionEmpty())
        {
           processTimeDateChange();
        }
      }
    });
    TableColumnModel tableColumnModel = table.getColumnModel();
    tableColumnModel.addColumnModelListener(new TableColumnModelListener()
    {
      public void columnMarginChanged(ChangeEvent e) {}
      public void columnMoved(TableColumnModelEvent e) {}
      public void columnRemoved(TableColumnModelEvent e) {}
      public void columnAdded(TableColumnModelEvent e) {}
      public void columnSelectionChanged(ListSelectionEvent e)
      {
        //Ignore extra messages.
        if (e.getValueIsAdjusting() ||
            !(e.getSource() instanceof ListSelectionModel))
          return;

        ListSelectionModel lsm = (ListSelectionModel)e.getSource();
        if (!lsm.isSelectionEmpty())
        {
           processTimeDateChange();
        }
      }
    });
  }

  /** update the days of the month */
  protected void updateDays()
  {
    // clear the current selection
    table.clearSelection();

    final int month = workingCalendar.get(Calendar.MONTH);
    final int year = workingCalendar.get(Calendar.YEAR);

    tableModel.updateData(workingCalendar);

    // set the month and year to the current calendar values
    monthComboBox.setSelectedIndex(month);
    yearComboBox.setYear(year);

    // select the current day if it is the current month and year
    if (month == calendar.get(Calendar.MONTH) &&
        year == calendar.get(Calendar.YEAR))
    {
      final int dayOfMonth = calendar.get(Calendar.DAY_OF_MONTH);
      table.changeSelection(tableModel.getRowIndex(dayOfMonth), tableModel.getColumnIndex(dayOfMonth), false, false);
    }
  }

  /** Process time and/or date change in order to update */
  protected void processTimeDateChange()
  {
    final int row = table.getSelectedRow(); // get the selected week
    final int col = table.getSelectedColumn(); // get the selected day of the week
    final int newDay;

    // if no selection
    if (row < 0 || col < 0)
      return;

    // get the new day from the table
    try
    {
      newDay = Integer.parseInt(tableModel.getValueAt(row, col).toString());
    }
    catch(NumberFormatException e)
    {
      return; // cannot convert day to int
    }

    // if invalid day
    if (newDay <= 0)
      return;

    // get the old and new date
    final int oldMonth = calendar.get(Calendar.MONTH);
    final int oldDay = calendar.get(Calendar.DAY_OF_MONTH);
    final int oldYear = calendar.get(Calendar.YEAR);
    final int newMonth = monthComboBox.getSelectedIndex();
    final int newYear = yearComboBox.getYear();

    // exit if no change
    if (oldMonth == newMonth && oldDay == newDay && oldYear == newYear)
      return;

    // set the new date
    calendar.set(Calendar.MONTH, newMonth);
    calendar.set(Calendar.DAY_OF_MONTH, newDay);
    calendar.set(Calendar.YEAR, newYear);

    // if dialog was provided
    if (dialog != null)
    {
      //Close the dialog
      dialog.setVisible(false);
      dialog.dispose();
    }
  }

  protected static class YearJComboBox extends JComboBox
  {
    /** The minimum year. */
    private int minYear = MIN_YEAR;

    /** The maximum year. */
    private int maxYear = MAX_YEAR;

    /** The TimeZone. */
    private TimeZone timeZone = null;

    /** Init years in progress flag. */
    private boolean initYearsFlag = false;

    /**
     * Creates a year combo box using the local timezone.
     */
    public YearJComboBox()
    {
      updateYears();
    }

    /**
     * Creates a year combo box using the year and the timezone of the calendar.
     * @param value the calendar.
     */
    public YearJComboBox(Calendar value)
    {
      setCalendar(value);
      updateYears();
    }

    /**
     * Creates a year combo box using the given timezone value.
     * @param value the given time zone.
     */
    public YearJComboBox(TimeZone value)
    {
      timeZone = value;
      updateYears();
    }

    /**
     * Gets the selected year.
     * @return the selected year or the minimum year if none.
     */
    public int getYear()
    {
      final int index = getSelectedIndex();
      if (index >= 0)
      {
         return index + minYear;
      }
      return minYear;
    }

    /**
     * Gets the year from the specified date.
     * @param date the Date.
     * @return the year.
     */
    public int getYear(Date date)
    {
      final Calendar cal = getCalendar();
      cal.setTime(date);
      return cal.get(Calendar.YEAR);
    }

    /**
     * Determines if the years are in the process of being initialized.
     * @return true if the years are in the process of being initialized.
     */
    public boolean isInitYearsInProgress()
    {
      return initYearsFlag;
    }

    /**
     * Sets the year and the timezone to that of the calendar.
     * The 'updateYears' method should be called to update the years.
     * @param value the calendar.
     * @see updateYears
     */
    public final void setCalendar(Calendar value)
    {
      setTimeZone(value.getTimeZone());
      setYear(value.get(Calendar.YEAR));
    }

    /**
     * Set the max time.
     * The 'updateYears' method should be called to update the years.
     * @param date the date to set or null to clear.
     * @see updateYears
     */
    public void setMaxTime(Date date)
    {
      setMaxTime(date, false);
    }

    /**
     * Set the max time.
     * @param date the date to set or null to clear.
     * @param updateYearsFlag if true the 'updateYears' method will be called
     * if the maximum year changes, otherwise
     * the 'updateYears' method should be called to update the years.
     * @see updateYears
     */
    public void setMaxTime(Date date, boolean updateYearsFlag)
    {
      final int newMaxYear;
      if (date == null)
      {
        newMaxYear = MAX_YEAR;
      }
      else
      {
        newMaxYear = getYear(date);
      }
      if (maxYear != newMaxYear)
      {
        maxYear = newMaxYear;
        if (updateYearsFlag)
        {
          updateYears();
        }
      }
    }

    /**
     * Set the min time.
     * The 'updateYears' method should be called to update the years.
     * @param date the date to set or null to clear.
     * @see updateYears
     */
    public void setMinTime(Date date)
    {
      setMinTime(date, false);
    }

    /**
     * Set the min time.
     * @param date the date to set or null to clear.
     * @param updateYearsFlag if true the 'updateYears' method will be called
     * if the minimum year changes, otherwise
     * the 'updateYears' method should be called to update the years.
     * @see updateYears
     */
    public void setMinTime(Date date, boolean updateYearsFlag)
    {
      final int newMinYear;
      if (date == null)
      {
        newMinYear = MIN_YEAR;
      }
      else
      {
        newMinYear = getYear(date);
      }
      if (minYear != newMinYear)
      {
        minYear = newMinYear;
        if (updateYearsFlag)
        {
          updateYears();
        }
      }
    }

    /**
     * Sets the time zone with the given time zone value.
     * The 'updateYears' method should be called to update the years.
     * @param value the given time zone.
     * @see updateYears
     */
    public final void setTimeZone(TimeZone value)
    {
      timeZone = value;
    }

    /**
     * Sets the selected year.
     * @param year the year.
     */
    public void setYear(int year)
    {
      if (initYearsFlag)
      {
        return;
      }
      if (year < minYear)
      {
        minYear = year;
        initYears();
      }
      else if (year > maxYear)
      {
        maxYear = year;
        initYears();
      }
      setSelectedIndex(year - minYear);
    }

    /**
     * Updates the years in the combo box.
     */
    public final void updateYears()
    {
      if (initYearsFlag)
      {
        return;
      }
      int currentYear = getYear();
      if (currentYear < minYear)
      {
        currentYear = minYear;
      }
      else if (currentYear > maxYear)
      {
        currentYear = maxYear;
      }
      initYears();
      setYear(currentYear);
    }

    /**
     * Gets the Calendar.
     * @return the Calendar.
     */
    protected Calendar getCalendar()
    {
      final Calendar cal = Calendar.getInstance();
      if (timeZone != null)
      {
        cal.setTimeZone(timeZone);
      }
      return cal;
    }

    /**
     * Initializes the years in the combo box.
     */
    private final void initYears()
    {
      initYearsFlag = true;
      removeAllItems();
      // add the selections for the year combo box
      for (int year = minYear; year <= maxYear; year++)
      {
        addItem(String.valueOf(year));
      }
      initYearsFlag = false;
    }
  }


  /** Table Model for the calendar */
  protected static class CalendarTableModel extends AbstractTableModel
  {
    private static final int MAX_ROW = 6; // max number of weeks in a month
    private static final int MAX_COL = 7; // number of days in a week
    private final static String[] columnNames =
    {
      "S", "M", "T", "W", "T", "F", "S"
    };
    private final int minDayOfMonth = Calendar.getInstance().getMinimum(Calendar.DAY_OF_MONTH);
    private int firstDayOfMonth = minDayOfMonth;
    private int lastDayOfMonth = Calendar.getInstance().getMaximum(Calendar.DAY_OF_MONTH);

    /**
     * update the data for change in the date
     *
     * @param calendar the calendar for the date
     */
    public void updateData(Calendar calendar)
    {
      // set the day of the month to the first day of the month
      calendar.set(Calendar.DAY_OF_MONTH, minDayOfMonth);
      firstDayOfMonth = calendar.get(Calendar.DAY_OF_WEEK);
      lastDayOfMonth = calendar.getActualMaximum(Calendar.DAY_OF_MONTH);

      // notify that the table data has changed
      this.fireTableDataChanged();
    }

    /**
     * the max number of columns (days of the week)
     *
     * @return the max number of columns in the table
     */
    public int getColumnCount()
    {
      return columnNames.length;
    }

    /**
     * the max number of rows (weeks in a month)
     *
     * @return the max number of rows in the table
     */
    public int getRowCount()
    {
      return MAX_ROW;
    }

    /**
     * the column (day of the week) title
     *
     * @param columnIndex the column (day of the week)
     *
     * @return the max number of columns in the table
     */
    public String getColumnName(int columnIndex)
    {
      return columnNames[columnIndex];
    }

    /**
     * gets the index for the day of the month
     *
     * @param dayOfMonth the day of the month
     *
     * @return the day of the month index
     */
    protected int getDayOfMonthIndex(int dayOfMonth)
    {
      return dayOfMonth + (firstDayOfMonth - 1) - minDayOfMonth;
    }

    /**
     * gets the index for the column (day of the week) for the day of the month
     *
     * @param dayOfMonth the day of the month
     *
     * @return the column index
     */
    protected int getColumnIndex(int dayOfMonth)
    {
      final int columnIndex = getDayOfMonthIndex(dayOfMonth) % MAX_COL;
      return columnIndex;
    }

    /**
     * gets the index for the row (week of month) for the day of the month
     *
     * @param dayOfMonth the day of the month
     *
     * @return the row index
     */
    protected int getRowIndex(int dayOfMonth)
    {
      final int rowIndex = getDayOfMonthIndex(dayOfMonth) / MAX_COL;
      return rowIndex;
    }

    /**
     * gets the day of the month specified by the row (week of month)
     * and the column (day of the week).
     *
     * @param rowIndex the row (week of month)
     * @param columnIndex the column (day of the week)
     *
     * @return the day of the month
     */
    public Object getValueAt(int rowIndex, int columnIndex)
    {
      String value = "";

      // exit if before first day of the month
      final int index = rowIndex * MAX_COL + columnIndex + 1;
      if (index < firstDayOfMonth)
        return value;

      // exit if after last day of the month
      final int dayOfMonth = index + 1 -  firstDayOfMonth;
      if (dayOfMonth > lastDayOfMonth)
        return value;

      // convert the day of the month to a string
      value = String.valueOf(dayOfMonth);
      return value;
    }

    /**
     *  Returns the <code>Class</code> of the column
     *
     *  @param columnIndex  the column being queried
     *  @return the Class
     */
    public Class getColumnClass(int columnIndex)
    {
      // always use a string to make sure column size is always the same
      return String.class;
    }
  }


  /**
   * Calendar Calendar Table Cell Renderer
   **/
  protected static class CalendarTableCellRenderer
      extends DefaultTableCellRenderer
  {
    public CalendarTableCellRenderer()
    {
      setHorizontalAlignment(JLabel.CENTER);
    }
  }
}
