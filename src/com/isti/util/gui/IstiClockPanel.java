//IstiClockPanel.java:  Implements a clock panel that displays the current time.
//
//  11/26/2002 -- [KF]  Initial version.
//   3/11/2009 -- [ET]  Added 'setFloatable()' and 'isFloatable()' methods.
//

package com.isti.util.gui;

import javax.swing.*;
import java.util.*;
import java.text.DateFormat;
import java.awt.BorderLayout;
import com.isti.util.UtilFns;
import com.isti.util.menu.IstiMenuPopper;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import java.awt.Dimension;

/**
 * Class IstiClockPanel implements a clock panel that displays the current time.
 */
public class IstiClockPanel extends JPanel implements ActionListener
{
  /**
   * Command to show the all time zones on the display.
   */
  public final static int SHOW_ALL_TIME_ZONES_COMMAND = 0;
  /**
   * Command to show seconds on the display.
   */
  public final static int SHOW_SECONDS_COMMAND = 1;
  /**
   * Command to show the time in 24 hour mode on the display.
   */
  public final static int SHOW_24_HOUR_COMMAND = 2;
  /**
   * Command to show the second time zone on the display
   * if more than one time zone is displayed.
   */
  public final static int SHOW_SECOND_TIME_ZONE_COMMAND = 3;

  /**
   * Value to use when the default time zone is desired.
   */
  public final static TimeZone DEFAULT_TIME_ZONE = null;

  /**
   * Display mode values (displayMode)
   * NOTE: Use bitwise 'or' to use multiple selections.
   */

  /**
   * Display mode value (displayMode):
   * Default mode - only show the selected time zone
   */
  public final static int DISPMODE_DEFAULT = 0;
  /**
   * Display mode value (displayMode):
   * Always show the default time zone
   */
  public final static int DISPMODE_ALWAYS_SHOW_DEFAULT_TIME_ZONE = 0x0001;
  /**
   * Display mode value (displayMode):
   * Show local time zone when the default time zone is selected.
   * NOTE: Normally used with DISPMODE_ALWAYS_SHOW_DEFAULT_TIME_ZONE.
   */
  public final static int DISPMODE_SHOW_LOCAL_WHEN_DEFAULT = 0x0002;

  //default display and action commands for the popup menu
  private final static String[] defaultPopupCommands =
  {
    "Show All Time Zones",
    "Show Seconds",
    "Show 24 Hour",
    "",
  };
  //current display and action commands for the popup menu
  private final static String[] popupCommands =
      new String[defaultPopupCommands.length];
  //enable command flags
  private boolean[] enableCommands = new boolean[defaultPopupCommands.length];
  //tool bar
  private JToolBar toolBar = new JToolBar();
  //tool bar separator size
  private final static Dimension toolBarSeparatorDimension =
      new Dimension(10, 0);
  //delay timer when seconds are used
  private final static int secondsDelayTime = 1000;
  //delay timer when seconds are not used
  private final static int minutesDelayTime = 1000*60;

  //local time zone
  private final static TimeZone localTimeZone = TimeZone.getDefault();
  //timer delay value
  private int timerDelay = minutesDelayTime;
  //time zone to display
  private TimeZone zone = DEFAULT_TIME_ZONE;
  //display mode
  private int displayMode = DISPMODE_DEFAULT;
  //default time zone
  private final TimeZone defaultTimeZone;

  //first timezone clock label
  private final JLabel firstClockLabel = new JLabel();
  //second timezone clock label
  private final JLabel secondClockLabel = new JLabel();
  //first timezone date formatter
  private DateFormat firstDateFormatter;
  //second timezone date formatter
  private DateFormat secondDateFormatter;
  //popup menu
  private final IstiPopupMenu popupMenu = new IstiPopupMenu();
  //menu popper
  private final IstiMenuPopper menuPopper;
  //timer
  private javax.swing.Timer timer;

  /**
   * Creates a clock panel with the default time zone.
   *
   * The 'start' method needs to be called to start the clock after
   * the clock panel is setup.
   * @see start
   */
  public IstiClockPanel()
  {
    this(DEFAULT_TIME_ZONE);
  }

  /**
   * Creates a clock panel with the specified time zone.
   * @param zone time zone for the clock or DEFAULT_TIME_ZONE for default.
   *
   * The 'start' method needs to be called to start the clock after
   * the clock panel is setup.
   * @see start
   */
  public IstiClockPanel(TimeZone zone)
  {
    this(zone, DISPMODE_DEFAULT);
  }

  /**
   * Creates a clock panel with the specified time zone.
   * @param zone time zone for the clock or DEFAULT_TIME_ZONE for default.
   * @param displayMode display mode.
   *
   * The 'start' method needs to be called to start the clock after
   * the clock panel is setup.
   * @see start, DISPMODE_*
   */
  public IstiClockPanel(TimeZone zone, int displayMode)
  {
    this(zone, displayMode, DEFAULT_TIME_ZONE);
  }

  /**
   * Creates a clock panel with the specified time zone.
   * @param zone time zone for the clock or DEFAULT_TIME_ZONE for default.
   * @param displayMode display mode.
   * @param defaultZone default time zone for the clock or DEFAULT_TIME_ZONE
   * for system default.
   *
   * The 'start' method needs to be called to start the clock after
   * the clock panel is setup.
   * @see start, DISPMODE_*
   */
  public IstiClockPanel(TimeZone zone, int displayMode,
                        TimeZone defaultZone)
  {
    defaultTimeZone = defaultZone!=null?defaultZone:localTimeZone;
    setLayout(new BorderLayout());
    menuPopper = IstiMenuPopper.createMenuPopper(popupMenu,
        toolBar);

    //default to all commands enabled
    for (int i = 0; i < defaultPopupCommands.length; i++)
      enableCommands[i] = true;

    init(zone, displayMode);

    add(toolBar, BorderLayout.CENTER);
  }

  /**
   * Initializes the time zone values.
   * @param zone time zone.
   * @param displayMode display mode.
   * @see DISPMODE_*
   */
  public synchronized void init(TimeZone zone, int displayMode)
  {
    //true to always show default time zone
    final boolean showDefaultTimeZone =
        (displayMode & DISPMODE_ALWAYS_SHOW_DEFAULT_TIME_ZONE) != 0;
    //true to show local time zone when the default time zone is selected
    final boolean showLocalTimeZone =
        (displayMode & DISPMODE_SHOW_LOCAL_WHEN_DEFAULT) != 0;

    //if the time zone is not specified use the default
    if (zone == null)
      zone = defaultTimeZone;
    this.zone = zone;
    this.displayMode = displayMode;

    //reset to start
    for (int i = 0; i < defaultPopupCommands.length; i++)
      popupCommands[i] = defaultPopupCommands[i];
    firstDateFormatter = null;
    secondDateFormatter = null;
    toolBar.removeAll();     //remove the components from the tool bar

    toolBar.addSeparator(toolBarSeparatorDimension);  //add a separator

    final TimeZone secondTimeZone;  //second time zone

    //if the specified time zone is not the default and we are displaying it
    if (showDefaultTimeZone && !zone.hasSameRules(defaultTimeZone))
    {
      secondTimeZone = defaultTimeZone;
    }
    //if the specified time zone is not the local and we are displaying it
    else if (showLocalTimeZone && !zone.hasSameRules(localTimeZone))
    {
      secondTimeZone = localTimeZone;
    }
    else
    {
      secondTimeZone = null;
    }

    //set up the first date formatter
    firstDateFormatter = DateFormat.getTimeInstance();
    firstDateFormatter.setTimeZone(zone);
    toolBar.add(firstClockLabel);
    toolBar.addSeparator(toolBarSeparatorDimension);  //add a separator

    //if the second time zone is selected
    if (secondTimeZone != null)
    {
      //allow the second time zone to be shown or not shown
      popupCommands[SHOW_SECOND_TIME_ZONE_COMMAND] =
          "Show "+secondTimeZone.getDisplayName(false, TimeZone.SHORT)+" Time";

      //set up the default date formatter
      secondDateFormatter = DateFormat.getTimeInstance();
      secondDateFormatter.setTimeZone(secondTimeZone);
      toolBar.add(secondClockLabel);
      toolBar.addSeparator(toolBarSeparatorDimension);  //add a separator
    }

    //add the popup commands
    for (int i = 0; i < popupCommands.length; i++)
    {
      addMenuItem(i);
    }

    //set the default seconds
    updateCommandState(SHOW_SECONDS_COMMAND);
  }

  /**
   * Sets the time zone.
   * @param zone time zone.
   */
  public void setTimeZone(TimeZone zone)
  {
    init(zone, displayMode);
    //if clock is running restart it
    restart();
  }

  /**
   * Gets the state for the specified command.
   * @param command the command.
   * @return true if command is enabled.
   * @see SHOW_ALL_TIME_ZONES_COMMAND, SHOW_SECONDS_COMMAND, ...
   */
  public boolean getCommandState(int command)
  {
    JCheckBoxMenuItem mi = getMenuItem(command);
    return mi != null && mi.isSelected();
  }

  /**
   * Disables the specified command.
   * @param command the command.
   * @return true if valid command.
   * @see SHOW_ALL_TIME_ZONES_COMMAND, SHOW_SECONDS_COMMAND, ...
   */
  public boolean disableCommand(int command)
  {
    return setCommandEnable(command, false);
  }

  /**
   * Enables/disables the specified command.
   * @param command the command.
   * @param b true if the command should be enabled.
   * @return true if valid command.
   * @see SHOW_ALL_TIME_ZONES_COMMAND, SHOW_SECONDS_COMMAND, ...
   */
  public boolean setCommandEnable(int command, boolean b)
  {
    JCheckBoxMenuItem mi = getMenuItem(command);
    if (mi != null)
    {
      enableCommands[command] = b;
      setMenuItemEnable(mi, b);
      return true;
    }
    return false;
  }

  /**
   * Gets the menu item for the specified command.
   * @param command the command.
   * @return the menu item or null if none.
   */
  protected JCheckBoxMenuItem getMenuItem(int command)
  {
    JCheckBoxMenuItem mi = null;
    try
    {
      mi = (JCheckBoxMenuItem)popupMenu.getComponent(command);
    }
    catch (Exception ex) {}
    return mi;
  }

  /**
   * Enables/disables the specified menu item.
   * @param mi the menu item.
   * @param b true if the command should be enabled.
   */
  protected void setMenuItemEnable(JCheckBoxMenuItem mi, boolean b)
  {
    //enable if command is enabled and there is text for the command
    mi.setEnabled(b && mi.getText() != null && mi.getText().length() > 0);
    mi.setVisible(mi.isEnabled());
  }

  /**
   * Determines if the specified command is enabled.
   * @param command the command.
   * @return true if the command is enabled.
   */
  public boolean isCommandEnabled(int command)
  {
    return enableCommands[command];
  }

  /**
   * Sets the state for the specified command.
   * @param command the command.
   * @param b command state.
   * @return true if valid command.
   * @see SHOW_ALL_TIME_ZONES_COMMAND, SHOW_SECONDS_COMMAND, ...
   */
  public boolean setCommandState(int command, boolean b)
  {
    JCheckBoxMenuItem mi = getMenuItem(command);
    if (mi != null)
    {
      mi.setSelected(b);
      return updateCommandState(command, b);
    }
    return false;
  }

  /**
   * Enable/disables popup menu.
   * @param b true if popups should be enabled.
   */
  public void setPopupEnabled(boolean b)
  {
    menuPopper.setEnabled(b);
  }

  /**
   * Updates the state for the specified command.
   * @param command the command.
   * @param b command state.
   * @return true if valid command.
   * @see SHOW_ALL_TIME_ZONES_COMMAND, SHOW_SECONDS_COMMAND, ...
   */
  protected synchronized boolean updateCommandState(int command, boolean b)
  {
    switch (command)
    {
      case SHOW_SECONDS_COMMAND:
        if (b)
          timerDelay = secondsDelayTime;
        else
          timerDelay = minutesDelayTime;
        break;
    }

    //update the date format strings
    updateDateFormatString(false);
    updateDateFormatString(true);

    //if clock is running restart it
    restart();
    return true;
  }

  /**
   * Updates the state for the specified command.
   * @param command the command.
   * @return true if valid command.
   * @see SHOW_ALL_TIME_ZONES_COMMAND, SHOW_SECONDS_COMMAND, ...
   */
  protected boolean updateCommandState(int command)
  {
    return updateCommandState(command, getCommandState(command));
  }

  /**
   * Updates the state for the specified command.
   * @param actionCommand the action command string.
   * @return true if valid command.
   * @see popupCommands
   */
  protected boolean updateCommandState(String actionCommand)
  {
    for (int i = 0; i < popupCommands.length; i++)
    {
      if (popupCommands[i].equals(actionCommand))
        return updateCommandState(i);
    }
    return false;
  }

  /**
   * Updates the date format string.
   * @param firstTimeZone true if first time zone should be updated.
   */
  protected void updateDateFormatString(boolean firstTimeZone)
  {
    final DateFormat df;
    if (firstTimeZone)
      df = firstDateFormatter;
    else
      df = secondDateFormatter;
    if (df == null)
      return;

    String dateFormatStr = "";
    //if first time zone or if showing the second time zone
    if (firstTimeZone || getCommandState(SHOW_SECOND_TIME_ZONE_COMMAND))
    {
      //hours
      if (getCommandState(SHOW_24_HOUR_COMMAND))
        dateFormatStr = "HH";
      else
        dateFormatStr = "hh";
      //minutes
      dateFormatStr += ":mm";
      //seconds
      if (getCommandState(SHOW_SECONDS_COMMAND))
        dateFormatStr += ":ss";
      //am/pm
      if (!getCommandState(SHOW_24_HOUR_COMMAND))
        dateFormatStr += " a";
      //timezone
      if (getCommandState(SHOW_ALL_TIME_ZONES_COMMAND) ||
          !df.getTimeZone().hasSameRules(localTimeZone))
        dateFormatStr += " z";
    }
    UtilFns.setDateFormatPattern(df, dateFormatStr);
  }

  /**
   * Adds a menu item to the popup menu.
   * @param command command index.
   */
  protected void addMenuItem(int command)
  {
    JCheckBoxMenuItem mi = getMenuItem(command);
    if (mi == null)  //if menu item not found
    {
      mi = new JCheckBoxMenuItem();
      mi.addActionListener(this);
      mi.setState(true);
      popupMenu.add(mi);
    }

    //get text for the command
    String text = popupCommands[command];
    mi.setActionCommand(text);
    mi.setText(text);
    setMenuItemEnable(mi, isCommandEnabled(command));
  }

  /**
   * Starts the clock.
   */
  public void start()
  {
    if (timer == null)
    {
      timer = new javax.swing.Timer(timerDelay, new ActionListener()
      {
        /**
         * Invoked when an action occurs.
         * @param e action event.
         */
        public void actionPerformed(ActionEvent e)
        {
          updateTime();
        }
      });
    }
    else
      timer.setDelay(timerDelay);
    final Calendar cal = updateTime();
    final int currentMillisecond =
        cal.get(Calendar.SECOND)*1000+cal.get(Calendar.MILLISECOND);
    final int initialDelay = timerDelay-currentMillisecond;
    timer.setInitialDelay(initialDelay >= 0?initialDelay:0);
    timer.start();
  }

  /**
   * Returns <b>true</b> if the clock is running.
   * @see #start
   * @return true if the clock is running.
   */
 public boolean isRunning()
 {
   return timer != null && timer.isRunning();
 }

  /**
   * Restart the clock.
   */
  public void restart()
  {
    if (isRunning())
    {
      timer.stop();
      firstClockLabel.setText(null);
      secondClockLabel.setText(null);
      start();
    }
  }

  /**
   * Stops the clock.
   */
  public void stop()
  {
    if (isRunning())
      timer.stop();
  }

  /**
   * Updates the time on the display.
   * @return current date
   */
  protected Calendar updateTime()
  {
    // get the time and convert it to a date
    Calendar cal = Calendar.getInstance(zone);
    Date date = cal.getTime();

    synchronized(this)
    {
      // format first date and display it
      if (firstDateFormatter != null)
      {
        firstClockLabel.setText(firstDateFormatter.format(date));
      }
      // format second date and display it
      if (secondDateFormatter != null)
      {
        secondClockLabel.setText(secondDateFormatter.format(date));
      }
      toolBar.repaint();
    }
    return cal;
  }

  /**
   * Implements the ActionListener interface.
   * Invoked when an action occurs.
   * @param e action event.
   */
  public void actionPerformed(ActionEvent e)
  {
    updateCommandState(e.getActionCommand());
  }

  /**
   * Sets the "floatable" property for the panel.
   * @param flgVal true to enable; false to disable.
   */
  public void setFloatable(boolean flgVal)
  {
    toolBar.setFloatable(flgVal);
  }

  /**
   * Returns the "floatable" property for the panel.
   * @return true if enabled; false if disabled.
   */
  public boolean isFloatable()
  {
    return toolBar.isFloatable();
  }
}
