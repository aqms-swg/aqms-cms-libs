//IstiColorChooser.java:  Extends the JColorChooser class to add enhancements.
//
//  10/22/2002 -- [KF]
//   11/4/2002 -- [ET]  Modified 'showDialog()' method to block if this
//                      dialog is modal and another modal dialog is showing.
//  11/14/2002 -- [ET]  Took modal "blocking" code back out as it had the
//                      potential to block other AWT activity.
//  11/20/2002 -- [KF]  Move selector GUI to IstiColorSelector.
//  11/26/2002 -- [ET]  Changed 'showDialog()' to make it put focus back
//                      on originating window even if it is a Dialog.
//  10/14/2004 -- [ET]  Added work-around to constructor for Java 1.4.2
//                      preview-panel bug (5029286).
//  11/29/2004 -- [ET]  Added additional work-around to constructor for
//                      preview-panel bug (5029286) for Java 1.5.
//    4/3/2009 -- [ET]  Replaced call to deprecated Dialog 'show()' method
//                      with 'setVisible()'.
//

package com.isti.util.gui;

import java.awt.*;
import java.awt.event.*;
import java.io.Serializable;
import javax.swing.*;
import javax.swing.colorchooser.*;
import javax.swing.event.*;
import com.isti.util.UtilFns;

/**
 * Extends the JColorChooser class to add enhancements:
 * Adds a panel to select the color by name.
 * Replacing the preview panel with a slider
 * that modifies the alpha part of the color.
 */
public class IstiColorChooser extends JColorChooser
{
         //chooser preview panel:
  protected final ChooserPreviewPanel previewPanel;

  /**
   * Creates a color chooser.
   * @param initialColor the initial color.
   */
  public IstiColorChooser(Color initialColor)
  {
    super(initialColor);
    previewPanel = new ChooserPreviewPanel(this, initialColor);

    addChooserPanel(new NameColorChooserPanel());
         //do work-around for Java 1.4.2 preview-panel bug (5029286):
    final Dimension sizeObj = previewPanel.getPreferredSize();
    previewPanel.setSize(sizeObj);
         //do additional work-around for preview-panel bug (5029286) for
         // Java 1.5; add border/inset to make minimum layout size > 0:
    previewPanel.setMinimumSize(sizeObj);
    previewPanel.setBorder(BorderFactory.createEmptyBorder(0,0,1,0));
    setPreviewPanel(previewPanel);
    getSelectionModel().addChangeListener(previewPanel);
  }

  /**
   * Sets the current color of the color chooser to the
   * specified color.
   * @param color the color to be set in the color chooser
   */
  public void setColor(Color color)
  {
    super.setColor(color);
    //reset the preview color
    previewPanel.setColor(color);
  }

  /**
   * Displays a modal dialog that lets you change a color.
   * @param component the parent Component for the dialog.
   * @param title the String title for the window.
   * @param initialColor the initial color.
   * @return the chosen color or null if nothing was chosen.
   */
  public static Color showDialog(Component component,
                                 String title,
                                 Color initialColor)
  {
    final IstiColorChooser pane = new IstiColorChooser(initialColor);
    final JDialog dialogObj = IstiColorChooser.createDialog(component,
       title, true, pane, pane.previewPanel, null);
    pane.setColor(initialColor);
    dialogObj.setVisible(true);
         //find frame or dialog host for the parent component
         // and set the focus onto it:
    final Window windowObj;
    if((windowObj=getWindowForComponent(component)) != null)
      windowObj.requestFocus();
    return pane.previewPanel.getColor();
  }

  /**
   * Returns the specified component's toplevel <code>Frame</code> or
   * <code>Dialog</code>.
   * @param parentComponent the <code>Component</code> to check for a
   *		<code>Frame</code> or <code>Dialog</code>
   * @return the <code>Frame</code> or <code>Dialog</code> that
   *		contains the component, or the default
   *         	frame if the component is <code>null</code>,
   *		or does not have a valid
   *         	<code>Frame</code> or <code>Dialog</code> parent
   */
  private static Window getWindowForComponent(Component parentComponent)
  {
    if(parentComponent == null)
      return JOptionPane.getRootFrame();
    if(parentComponent instanceof Frame || parentComponent instanceof Dialog)
      return (Window)parentComponent;
    return getWindowForComponent(parentComponent.getParent());
  }
}

class NameColorChooserPanel extends AbstractColorChooserPanel
{
  //color list
  private final JList colorList =
      new JList(UtilFns.getColorDisplayNames());

  /**
   * Invoked automatically when the model's state changes.
   * It is also called by <code>installChooserPanel</code> to allow
   * you to set up the initial state of your chooser.
   * Override this method to update your <code>ChooserPanel</code>.
   */
  public void updateChooser()
  {
    //get current color from model
    final Color cc = getColorFromModel();
    //get index for color
    final int index = UtilFns.getColorDisplayNameIndex(cc.getRGB());
    //if the color was found
    if (index >= 0)
      colorList.setSelectedIndex(index);  //select the color
    else
      colorList.clearSelection();         //clear the selection
  }

  /**
   * Builds a new chooser panel.
   */
  protected void buildChooser()
  {
    //add a line border
    this.setBorder(BorderFactory.createLineBorder(Color.black));

    //only allow a single selection
    colorList.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);

    //listen for changes
    colorList.addListSelectionListener(new ListSelectionListener()
    {
      public void valueChanged(ListSelectionEvent e)
      {
        //Ignore extra messages.
        if (e.getValueIsAdjusting())
          return;

        final int selectedIndex = colorList.getSelectedIndex();

        //exit if no selection
        if (selectedIndex < 0)
        {
          return;
        }

        final int value = UtilFns.getColorDisplayNameValue(selectedIndex);
        getColorSelectionModel().setSelectedColor(new Color(value));
      }
    });
    //add color list to the panel
    this.add(colorList);
  }

  /**
   * Returns a string containing the display name of the panel.
   * @return the name of the display panel
   */
  public String getDisplayName()
  {
    return "Name";
  }

  /**
   * Returns the large display icon for the panel.
   * @return the large display icon
   */
  public Icon getSmallDisplayIcon()
  {
    return null;
  }

  /**
   * Returns the small display icon for the panel.
   * @return the small display icon
   */
  public Icon getLargeDisplayIcon()
  {
    return null;
  }
}

/**
 * Preview Panel with the capability to modify the alpha component.
 */
class ChooserPreviewPanel extends JPanel
    implements ActionListener, ChangeListener, Serializable
{
  final IstiColorChooser chooser;
  final ColorDisplayPanel displayPanel;
  final JSlider alphaSlider;
  final FilteredJTextField alphaField;
  Color color = null;
  int   alpha;

  public ChooserPreviewPanel(IstiColorChooser c, Color initialColor)
  {
    chooser = c;
    final Box sliderBox = Box.createHorizontalBox();
    displayPanel = new ColorDisplayPanel();

    final AlphaNumberListener alphaNumberChange = new AlphaNumberListener();

    final JLabel alphaLabel = new JLabel("Alpha");
    alphaLabel.setDisplayedMnemonic(alphaLabel.getText().charAt(0));

    alphaSlider = new JSlider(0, 255);
    alphaSlider.setMajorTickSpacing( 85 );
    alphaSlider.setMinorTickSpacing( 17 );
    alphaSlider.setPaintLabels(true);
    alphaSlider.setPaintTicks(true);
    alphaSlider.addChangeListener(alphaNumberChange);

    alphaField = new FilteredJTextField(
        "Alpha", 4, FilteredJTextField.INTEGER_CHARS, true, 3);
    alphaField.setMaxValue(255);
    final JPanel alphaFieldHolder = new JPanel();
    alphaFieldHolder.add(alphaField);
    alphaField.getDocument().addDocumentListener(alphaNumberChange);

    //set up tool tip text
    final String alphaToolTipText =
        "Alpha value from 0 (completely transparent) to 255 (completely opaque)";
    alphaLabel.setToolTipText(alphaToolTipText);
    alphaSlider.setToolTipText(alphaToolTipText);
    alphaSlider.setToolTipText(alphaToolTipText);

    setColor(initialColor);
    displayPanel.setPreferredSize(new Dimension(80, 40));
    sliderBox.add(displayPanel);
    sliderBox.add(Box.createGlue());
    sliderBox.add(Box.createHorizontalStrut(10));
    sliderBox.add(alphaLabel);
    sliderBox.add(Box.createGlue());
    sliderBox.add(alphaSlider);
    sliderBox.add(Box.createGlue());
    sliderBox.add(alphaFieldHolder);
    sliderBox.add(Box.createGlue());
    add(sliderBox);
  }

  /**
   * ActionListener interface.  Sets the color from the IstiColorChooser.
   * @param e ActionEvent
   */
  public void actionPerformed(ActionEvent e)
  {
    color = chooser.getColor();
    setDisplayPanelColor(color);
  }

  /**
   * ChangeListener interface. Called when the color changes
   * @param e ChangeEvent
   */
  public void stateChanged(ChangeEvent e)
  {
    if(!(e.getSource() instanceof ColorSelectionModel)) return;

    final Color c = ((ColorSelectionModel)e.getSource()).getSelectedColor();
    setDisplayPanelColor(c);
  }

  /**
   * sets the preview color but keeps the existing alpha value.
   * @param c the color
   */
  private void setDisplayPanelColor(Color c)
  {
    setDisplayPanelColor(c, false);
  }

  /**
   * sets the preview alpha value but keeps the existing color.
   * @param a the alpha
   */
  private void setDisplayPanelColor(int a)
  {
    setDisplayPanelColor(displayPanel.getColor(), a);
  }

  /**
   * sets the preview color
   * @param c the color
   * @param a the alpha
   */
  private void setDisplayPanelColor(Color c, int a)
  {
    setDisplayPanelColor(new Color(c.getRed(),c.getGreen(), c.getBlue(), a),
                         true);
  }

  /**
   * sets the preview color
   * @param c the color
   * @param hasalpha <code>true</code> if the alpha bits are valid
   */
  private void setDisplayPanelColor(Color c, boolean hasalpha)
  {
    if(!hasalpha)  //if alpha not valid use existing alpha
      c = new Color(c.getRed(), c.getGreen(), c.getBlue(),
                    alpha);
    displayPanel.setColor(c);
    displayPanel.repaint();

    displayPanel.setToolTipText(c.getRed()+ ", "+c.getGreen()+", "+c.getBlue());
  }

  /**
   * sets the preview color
   * @param c the color
   */
  public void setColor(Color c)
  {
    alpha = c.getAlpha();  //update the alpha
    setDisplayPanelColor(c, true);
    //update the text field and let it update the display panel color
    final String alphaText = Integer.valueOf(alpha).toString();
    alphaField.setText(alphaText);
  }

  /**
   * Get the preview color.
   * @return the color
   */
  public Color getColor()
  {
    if (color != null)
    {
      color =  new Color(color.getRed(),
                         color.getGreen(),
                         color.getBlue(),
                         alpha);
    }
    return color;
  }

  /**
   * Listeners for the alpha value.
   */
  class AlphaNumberListener implements ChangeListener, DocumentListener,
      Serializable
  {
    public void stateChanged(ChangeEvent ce)
    {
      //get the new alpha value
      final int newAlpha = alphaSlider.getValue();
      //if the text field doesn't have the new value
      if (alphaField.getIntegerValue() != newAlpha)
      {
        //update the text field and let it update the display panel color
        final String alphaText = Integer.valueOf(newAlpha).toString();
        alphaField.setText(alphaText);
      }
      else
      {
        //update the alpha
        alpha = newAlpha;
        //update the display panel color
        setDisplayPanelColor(alpha);
      }
    }

    public void insertUpdate(DocumentEvent de)
    {
      updatePanel(de);
    }
    public void removeUpdate(DocumentEvent de)
    {
      updatePanel(de);
    }
    public void changedUpdate(DocumentEvent de)
    {
    }

    private void updatePanel(DocumentEvent de)
    {
      //get the new alpha value
      final int newAlpha = alphaField.getIntegerValue();
      //if the slider doesn't have the new value
      if (alphaSlider.getValue() != newAlpha)
      {
        //update the slider and let it update the display panel color
        alphaSlider.setValue(newAlpha);
      }
      else
      {
        //update the alpha
        alpha = newAlpha;
        //update the display panel color
        setDisplayPanelColor(alpha);
      }
    }
  }
}

// class to display the currently selected color
class ColorDisplayPanel extends JPanel
{
  Color dc = Color.white; // color to display

  /**
   * constructor
   */
  public ColorDisplayPanel()
  {
    this(null);
  }

  /**
   * constructor
   * @param c the color
   */
  public ColorDisplayPanel(Color c)
  {
    setBackground(Color.white);
    if (c != null)
      dc = c;
  }

  /**
   * set the color to tht specified
   * @param c color to paint
   */
  public void setColor(Color c)
  {
    dc = c;
  }

  /**
   * get the color
   * @return the color on null if no color specified.
   */
  public Color getColor()
  {
    return dc;
  }

  /**
   * paints this panel
   * @param g Graphics
   */
  public void paint(Graphics g)
  {
    super.paint(g);

    g.setColor(dc);
    ((Graphics2D)g).fill(g.getClip());
  }
}
