//IstiColorSelector.java:  Defines the color selector.
//
//  11/20/2002 -- [KF]  Get GUI previously in ColorPropertyEditor.
//    3/4/2003 -- [ET]  Increased button width slightly.
//   4/16/2003 -- [KF]  Added support for action listeners.
//   7/28/2004 -- [KF]  Changed button to use hand button.
//  10/14/2004 -- [ET]  Added code to constructor to allow button
//                      background color under Windows XP.
//

package com.isti.util.gui;

import java.awt.Color;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.FontMetrics;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.Vector;

import javax.swing.JButton;
import javax.swing.colorchooser.DefaultColorSelectionModel;

import com.isti.util.UtilFns;

/**
 * Defines the color selector.
 */
public class IstiColorSelector extends DefaultColorSelectionModel
{
  private static final String SIZING_STRING =
           "0x" + UtilFns.toHexString(0, UtilFns.colorPropertyStringLength);
  protected final JButton button;

  /**
   * Creates a color selector.
   * @param c the initial color selection.
   * @param title title for the selector.
   */
  public IstiColorSelector(final Color c, final String title)
  {
    button = new HandJButton(title);
    button.setMargin(new Insets(0,0,0,0));  //no margins around button text
    //get font metrics for button text:
    final FontMetrics metObj = button.getFontMetrics(button.getFont());
    //set button size based on sizing string:
    button.setPreferredSize(new Dimension(
        metObj.stringWidth(SIZING_STRING)+15,metObj.getHeight()+4));

    //make button background work under Windows XP:
    button.setContentAreaFilled(false);
    button.setOpaque(true);

    setSelectedColor(c);

    button.addActionListener(new ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        final Color c = IstiColorChooser.showDialog(button, title,
            getSelectedColor());
        if (c != null)  //if color was changed
        {
          setSelectedColor(c);  //update the color
        }
      }
    });
  }

  /**
   * Gets the selection component.
   * @return component for color selection.
   */
  public Component getSelectionComponent()
  {
    return button;
  }

  /**
   * Sets the model's selected color to <I>color</I>.
   * @param color selected color.
   *
   * Notifies any listeners if the model changes
   *
   * @see   #getSelectedColor
   * @see   #addChangeListener
   */
  public void setSelectedColor(Color color)
  {
    super.setSelectedColor(color);
    //set button background color (make sure it's opaque):
    button.setBackground(UtilFns.createOpaqueColor(color));

    //setup text for button:
    if (color.getRed() + color.getGreen() < 150)
      button.setForeground(Color.white);  //if dark color, use white text
    else
      button.setForeground(Color.black);  //if light color, use black text
    button.setText(UtilFns.getColorDisplayName(color.getRGB()));

    //notify listeners of the change
    fireActionPerformed(
        new ActionEvent(this, ActionEvent.ACTION_PERFORMED, "color selected"));
  }

  protected Vector listenerList = new Vector();

  /**
   * Adds an <code>ActionListener</code> to the button.
   * @param l the <code>ActionListener</code> to be added
   */
  public void addActionListener(ActionListener l)
  {
    listenerList.add(l);
  }

  /**
   * Removes an <code>ActionListener</code> from the button.
   * If the listener is the currently set <code>Action</code>
   * for the button, then the <code>Action</code>
   * is set to <code>null</code>.
   *
   * @param l the listener to be removed
   */
  public void removeActionListener(ActionListener l)
  {
    listenerList.remove(l);
  }

  /**
   * Notifies all listeners that have registered interest for
   * notification on this event type.  The event instance
   * is lazily created using the parameters passed into
   * the fire method.
   *
   * @param event  the <code>ChangeEvent</code> object
   * @see EventListenerList
   */
  protected void fireActionPerformed(ActionEvent event)
  {
    Object[] listeners = listenerList.toArray();

    // Process the listeners last to first, notifying
    // those that are interested in this event
    for (int i = listeners.length-1; i >= 0; i--)
    {
      ((ActionListener)listeners[i]).actionPerformed(event);
    }
  }
}