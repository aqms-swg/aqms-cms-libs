//IstiComboBoxModel.java:  Extension of DefaultComboBoxModel.
//

package com.isti.util.gui;

import java.util.Arrays;
import java.util.List;
import java.util.Vector;
import javax.swing.DefaultComboBoxModel;

/**
 * Class IstiComboBoxModel is an extension of DefaultComboBoxModel.
 */
public class IstiComboBoxModel
    extends DefaultComboBoxModel
{
  /**
   * Elements in the combo box.
   */
  private final List elements;

  /**
   * Creates an IstiComboBoxModel.
   */
  public IstiComboBoxModel()
  {
    this(new Vector());
  }

  /**
   * Creates an IstiComboBoxModel.
   * @param list the list of initial elements.
   */
  public IstiComboBoxModel(List list)
  {
    this(new Vector(list));
  }

  /**
   * Creates an IstiComboBoxModel.
   * @param elements the initial elements.
   */
  public IstiComboBoxModel(Object[] elements)
  {
    this(Arrays.asList(elements));
  }

  /**
   * Creates an IstiComboBoxModel.
   * @param vector the vector of initial elements.
   */
  public IstiComboBoxModel(Vector vector)
  {
    super(vector);
    elements = vector;
  }

  /**
   * Sets the elements.
   * @param list the list of elements.
   */
  public void setElements(List list)
  {
    //if the list is different than the current elements
    if (!elements.equals(list))
    {
      //replace the elements with the contents of the list
      elements.clear();
      if (list != null)
      {
        elements.addAll(list);
      }
      fireIntervalAdded(this, 0, getSize());
    }
  }
}
