//IstiDataTableModel.java:  Defines a table model based on a data model.
//
//   6/18/2009 -- [KF]  Initial version.
//

package com.isti.util.gui;

import java.util.EventObject;

import javax.swing.table.AbstractTableModel;

import com.isti.util.IstiDataModel;
import com.isti.util.IstiEventListener;

/**
 * Class IstiDataTableModel defines a table model based on a data model.
 */
public class IstiDataTableModel extends AbstractTableModel {

  private static final long serialVersionUID = 1L;

  /** The data model. */
  private final IstiDataModel dataModel;

  /**
   * Creates the table model.
   * @param dataModel the data model.
   */
  public IstiDataTableModel(final IstiDataModel dataModel) {
    this.dataModel = dataModel;
    this.dataModel.addEventListener(new IstiEventListener() {
      /**
       * This notification tells listeners an event has occurred.
       * @param e the event object.
       */
      public void notifyEvent(EventObject e) {
        fireTableDataChanged();
      }
    });
  }

  /**
   * Returns the number of columns in the model.
   * @return the number of columns in the model
   * @see #getRowCount
   */
  public int getColumnCount() {
    return dataModel.getColumnCount();
  }

  /**
   * Returns the name of the column at <code>columnIndex</code>. This is used
   * to initialize the table's column header name. Note: this name does not need
   * to be unique; two columns in a table can have the same name.
   * @param columnIndex the index of the column
   * @return the name of the column
   */
  public String getColumnName(int columnIndex) {
    return dataModel.getColumnName(columnIndex);
  }

  /**
   * Returns the number of rows in the model. A table uses this method to
   * determine how many rows it should display. This method should be quick, as
   * it is called frequently during rendering.
   * @return the number of rows in the model
   * @see #getColumnCount
   */
  public int getRowCount() {
    return dataModel.getRowCount();
  }

  /**
   * Returns the value for the cell at <code>columnIndex</code> and
   * <code>rowIndex</code>.
   * @param rowIndex the row whose value is to be queried
   * @param columnIndex the column whose value is to be queried
   * @return the value Object at the specified cell
   */
  public Object getValueAt(int rowIndex, int columnIndex) {
    return dataModel.getValueAt(rowIndex, columnIndex);
  }

  /**
   * Returns true if the cell at <code>rowIndex</code> and
   * <code>columnIndex</code> is editable. Otherwise, <code>setValueAt</code>
   * on the cell will not change the value of that cell.
   * @param rowIndex the row whose value to be queried
   * @param columnIndex the column whose value to be queried
   * @return true if the cell is editable
   * @see #setValueAt
   */
  public boolean isCellEditable(int rowIndex, int columnIndex) {
    return dataModel.isCellEditable(rowIndex, columnIndex);
  }

  /**
   * Sets the value in the cell at <code>columnIndex</code> and
   * <code>rowIndex</code> to <code>aValue</code>.
   * @param aValue the new value
   * @param rowIndex the row whose value is to be changed
   * @param columnIndex the column whose value is to be changed
   * @see #getValueAt
   * @see #isCellEditable
   */
  public void setValueAt(Object aValue, int rowIndex, int columnIndex) {
    dataModel.setValueAt(aValue, rowIndex, columnIndex);
    fireTableCellUpdated(rowIndex, columnIndex);
  }
}
