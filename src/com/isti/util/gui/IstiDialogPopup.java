//IstiDialogPopup.java:  Defines a popup dialog window that is easily
//                       configurable and programatically closeable.
//
//  10/1/2002 -- [ET]  Initial release version.
//  11/1/2002 -- [ET]  Added support for modeless dialogs and dialogs with
//                     no buttons.
// 11/14/2002 -- [ET]  Added 'setVisible()' method; improved 'show()'
//                     method's handling of non-modal dialogs.
// 11/15/2002 -- [ET]  Added option to display an Object and additional
//                     get/set methods.
// 11/26/2002 -- [ET]  Modified constructors to allow objects instead of
//                     just strings to define buttons; added various
//                     convenience methods for manipulating the 'JDialog'
//                     object held by this class.
// 12/10/2002 -- [ET]  Made dialog resizable by default to be consistent in
//                     Java 1.3 & 1.4 (changeable via 'setResizable()').
//   3/7/2003 -- [ET]  Added 'getInputValue()' method.
//  6/19/2003 -- [ET]  Fixed 'showAndWait()' so that it returns when dialog
//                     becomes hidden.
//  6/23/2003 -- [ET]  Fixed 'showAndWait()' so that it returns when dialog
//                     becomes hidden via both 'setVisible(false)' and
//                     'hide()'; fixed so that 'showAndWait()' returns
//                     proper object when non-modal dialog used.
//  6/25/2003 -- [ET]  Added 'waitForDialogShowing()' and 'closeWithWait()'
//                     methods.
//   7/1/2003 -- [ET]  Changed 'waitForDialogShowing()' to
//                     'waitForDialogVisible()'.
//  7/23/2003 -- [ET]  Added option of using default buttons based
//                     on 'optionType' parameter in constructors;
//                     added 'getValue()', 'showOptionDialog()' and
//                     'showMessageDialog()' methods.
// 10/22/2003 -- [ET]  Added support for "enforceDispatchThread" mode where
//                     changes are performed via the event-dispatch thread.
// 10/29/2003 -- [ET]  Added "implements IstiDialogInterface".
//  12/5/2003 -- [ET]  Added support for "parentHeightLimitFlag" option
//                     where the dialog height is not allowed to be larger
//                     than its parent (good for small display screens).
//  3/17/2004 -- [ET]  Added "..._OPTION" return values.
//   4/5/2004 -- [ET]  Added 'setVisibleViaInvokeLater()' method.
//   4/6/2004 -- [KF]  Added 'showOptionDialog()' method.
//   4/7/2004 -- [ET]  Modified to always initialize JOptionPane 'value'
//                     field before showing dialog (to fix issue of
//                     button being ignored on reused non-modal dialog);
//                     renamed non-static 'showOptionDialog()' method to
//                     'showAndReturnIndex()'.
//  4/15/2004 -- [ET]  Added 'createDialog()' method, 'hideDialogOnButtonFlag'
//                     and 'disposeDialogOnButtonFlag' variables to allow
//                     more control over button actions; added
//                     'add/RemovePropertyChangeListener()' methods.
//  4/16/2004 -- [ET]  Added extra 'pack()' to end of constructor (seems to
//                     help layout in some cases); modified 'selectAndShow()'
//                     to only select button if 'initialFocusCompObj'==null;
//                     modified 'setInitialFocus()' to always use
//                     'invokeLater()'.
//  4/27/2004 -- [ET]  Added option of doing an extra 'pack()' after the
//                     dialog is shown (to account for size changes after
//                     layout) via 'setRepackNeededFlag()' method; removed
//                     extra 'pack()' from constructor.
//  4/29/2004 -- [ET]  Took out call to 'paneObj.setValue()' from the
//                     'doSetVisible()' method; modified so that the extra
//                     'pack()' is always done after a slight delay.
//  5/14/2004 -- [ET]  Increased repack-delay time from 10 to 100 ms.
//   8/6/2004 -- [ET]  Added optional dialog-object parameter to
//                     constructor and 'createDialog()' method.
//  9/10/2004 -- [ET]  Added 'addCloseListener()' & 'removeCloseListener()'
//                     and 'CloseListener' class.
// 10/12/2004 -- [ET]  Added 'setOptionPaneValue()' method.
//  2/22/2005 -- [ET]  Modified to allow close ("x") button to clear a
//                     non-modal dialog after it has gotten stuck by being
//                     hidden while minimized; modified 'close()' method
//                     to call 'dispose()' after 'hide()' to make sure
//                     minimized non-modal dialogs don't get stuck.
//   8/2/2007 -- [KF]  Added 'allowDialogClose()' methods.
//  10/7/2008 -- [KF]  Added limit-height-to-the-maximum methods.
//   4/3/2009 -- [ET]  Replaced calls to deprecated Dialog 'show()' and
//                     'hide()' methods with 'setVisible()'.
//  7/23/2010 -- [KF]  Changed to call 'pack()' before 'setVisible(true)'
//                     if it was not already called rather than from
//                     constructor to ensure on event-dispatch thread
//                     without setting 'staticEnforceDispatchThreadFlag'.
//

package com.isti.util.gui;

import java.awt.*;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.awt.event.WindowListener;
import java.awt.event.ComponentListener;
import java.awt.event.ComponentAdapter;
import java.awt.event.ComponentEvent;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import javax.swing.*;
import java.beans.PropertyChangeListener;
import java.beans.PropertyChangeEvent;
import com.isti.util.IstiDialogInterface;

/**
 * Class IstiDialogPopup defines a popup dialog window that is easily
 * configurable and programatically closeable.  The other major advantage
 * of this class over 'JOptionPane' is the ability to use non-modal
 * dialog windows.
 */
public class IstiDialogPopup implements WindowConstants,IstiDialogInterface
{
  protected final JOptionPane paneObj;
  protected final JDialog dlgObj;
  protected String messageString = null;
                 //component that should get the initial focus:
  protected Component initialFocusCompObj;
  protected boolean initialFocusFlag = false;
  protected boolean packNeededFlag = true;
  protected boolean repackNeededFlag = false;
  protected Point repackCheckLocationObj = null;
  protected boolean waitingForShowFlag = false;
  protected boolean hideDialogOnButtonFlag = true;
  protected boolean disposeDialogOnButtonFlag = false;
  protected final Object [] optionsArr;

    //true to enforce changes via dispatch thread (all instances default):
  protected static boolean staticEnforceDispatchThreadFlag = false;
    //true to enforce changes via dispatch thread (this instance):
  protected boolean enforceDispatchThreadFlag =
                                            staticEnforceDispatchThreadFlag;

    //true to limit height to that of parent (all instances default):
  protected static boolean staticParentHeightLimitFlag = false;
    //true to limit height to that of parent (this instance):
  protected boolean parentHeightLimitFlag = staticParentHeightLimitFlag;
  //true to limit height to the maximum (all instances default):
  protected static boolean staticMaxHeightLimitFlag = true;
  //true to limit height to the maximum (this instance):
  protected boolean maxHeightLimitFlag = staticMaxHeightLimitFlag;

  /** Specifies error message type.  Same value as in JOptionPane. */
  public static final int ERROR_MESSAGE = JOptionPane.ERROR_MESSAGE;
  /** Specifies information message type.  Same value as in JOptionPane. */
  public static final int INFORMATION_MESSAGE =
                                            JOptionPane.INFORMATION_MESSAGE;
  /** Specifies warning message type.  Same value as in JOptionPane. */
  public static final int WARNING_MESSAGE = JOptionPane.WARNING_MESSAGE;
  /** Specifies question message type.  Same value as in JOptionPane. */
  public static final int QUESTION_MESSAGE = JOptionPane.QUESTION_MESSAGE;
  /** Specifies plain message with no icon.  Same value as in JOptionPane. */
  public static final int PLAIN_MESSAGE = JOptionPane.PLAIN_MESSAGE;

  /** Value for 'optionType' parameter.  Same value as in JOptionPane. */
  public static final int DEFAULT_OPTION = JOptionPane.DEFAULT_OPTION;
  /** Value for 'optionType' parameter.  Same value as in JOptionPane. */
  public static final int YES_NO_OPTION = JOptionPane.YES_NO_OPTION;
  /** Value for 'optionType' parameter.  Same value as in JOptionPane. */
  public static final int YES_NO_CANCEL_OPTION =
                                           JOptionPane.YES_NO_CANCEL_OPTION;
  /** Value for 'optionType' parameter.  Same value as in JOptionPane. */
  public static final int OK_CANCEL_OPTION = JOptionPane.OK_CANCEL_OPTION;
  /** Value for 'optionType' parameter; specifies no auto-created buttons. */
  public static final int NO_AUTOBUTTONS_OPTION = -99;

  /** Return value if YES is chosen.  Same as in JOptionPane. */
  public static final int YES_OPTION = JOptionPane.YES_OPTION;
  /** Return value if NO is chosen.  Same as in JOptionPane. */
  public static final int NO_OPTION = JOptionPane.NO_OPTION;
  /** Return value if CANCEL is chosen.  Same as in JOptionPane. */
  public static final int CANCEL_OPTION = JOptionPane.CANCEL_OPTION;
  /** Return value if OK is chosen.  Same as in JOptionPane. */
  public static final int OK_OPTION = JOptionPane.OK_OPTION;
  /** Return value for closed without selection.  Same as in JOptionPane. */
  public static final int CLOSED_OPTION = JOptionPane.CLOSED_OPTION;

  /** Value for 'setDefaultCloseOperation()' method. */
  public static final int DISPOSE_ON_CLOSE = JDialog.DISPOSE_ON_CLOSE;
  /** Value for 'setDefaultCloseOperation()' method. */
  public static final int DO_NOTHING_ON_CLOSE = JDialog.DO_NOTHING_ON_CLOSE;
  /** Value for 'setDefaultCloseOperation()' method. */
  public static final int HIDE_ON_CLOSE = JDialog.HIDE_ON_CLOSE;


  /**
   * Creates a popup dialog window.
   * @param parentComp the parent component for the popup.
   * @param givenDialogObj A 'JDialog' to be used, or null to have a
   * new one created.
   * @param msgObj the message object to be shown.
   * @param titleStr the title string for popup window.
   * @param msgType the type of message to be displayed (primarily used
   * to determine the icon from the pluggable Look and Feel):
   * ERROR_MESSAGE, INFORMATION_MESSAGE, WARNING_MESSAGE,
   * QUESTION_MESSAGE, or PLAIN_MESSAGE.
   * @param optionsArr an array of Objects that defines what buttons are
   * shown, or null for default or no buttons, based on 'optionType'.
   * @param initValIdx the index of the 'optionsArr[]' object
   * corresponding to the option to be selected by default.
   * @param modalFlag true for modal, false for modeless (allows other
   * windows to run).
   * @param createButtonFlag if true and the given 'optionsArr' contains a
   * single non-Component item and the given 'modalFlag' is false then the
   * button created for 'optionObj' will dispose the dialog (in addition to
   * hiding it) when used.
   * @param optionType specifies what default option buttons to
   * automatically create if 'optionsArr' is null or empty; one of:
   * DEFAULT_OPTION, YES_NO_OPTION, YES_NO_CANCEL_OPTION, OK_CANCEL_OPTION
   * or NO_AUTOBUTTONS_OPTION.  If default buttons are created and the user
   * selects one then the object value returned by 'show()' or 'getValue()'
   * will be an 'Integer' object wrapping one of the 'JOptionPane' values
   * (such as 'OK_OPTION').
   */
  public IstiDialogPopup(final Component parentComp,JDialog givenDialogObj,
             Object msgObj,String titleStr,int msgType,Object [] optionsArr,
                  int initValIdx,boolean modalFlag,boolean createButtonFlag,
                                                             int optionType)
  {
    this.optionsArr = optionsArr;
         //init changes-via-dispatch-thread flag from static version:
    enforceDispatchThreadFlag = staticEnforceDispatchThreadFlag;
         //init limit-height-to-that-of-parent flag from static version:
    parentHeightLimitFlag = staticParentHeightLimitFlag;
    Object selObj;
    if(optionsArr != null && optionsArr.length > 0)
    {    //options array given
      if(initValIdx < 0 || initValIdx >= optionsArr.length)
        initValIdx = 0;        //if index out of range then set to zero
      selObj = optionsArr[initValIdx];      //set initial option object
      if(createButtonFlag && !modalFlag && optionsArr.length == 1 &&
                                             !(selObj instanceof Component))
      {  //flag set, non-modal and single non-component option item
        final JButton buttonObj;       //create button for item
        if(selObj instanceof Icon)                    //if icon then
          buttonObj = new JButton((Icon)selObj);      //create icon button
        else            //if not icon then create button with text label
          buttonObj = new JButton(selObj.toString());
        final Object paneValueObj = selObj;
        buttonObj.addActionListener(new ActionListener()
            {           //setup button action to hide and dispose dialog
              public void actionPerformed(ActionEvent evtObj)
              {
                paneObj.setValue(paneValueObj);  //set value for pane
                doSetDialogVisible(false);        //clear dialog
                dlgObj.dispose();                //dispose dialog
              }
            });
                        //make button be the one option item used:
        optionsArr[0] = selObj = buttonObj;
      }
    }
    else
    {    //options array not given; setup for default or no buttons
      initValIdx = 0;
              //if no-buttons value then use empty array for no buttons,
              // otherwise use 'null' for default auto-created buttons:
      optionsArr = (optionType == NO_AUTOBUTTONS_OPTION) ?
                                                       new String[0] : null;
      selObj = null;
    }
              //if no-buttons 'optionType' value used then change it to
              // 'DEFAULT_OPTION' to keep JOptionPane happy:
    if(optionType == NO_AUTOBUTTONS_OPTION)
      optionType = JOptionPane.DEFAULT_OPTION;
                        //create option pane object:
    paneObj = new JOptionPane(msgObj,msgType,optionType,null,
                                                         optionsArr,selObj);
                        //create dialog object:
    dlgObj = createDialog(givenDialogObj,parentComp,titleStr);
    dlgObj.setModal(modalFlag);        //set given modal flag value
    initialFocusCompObj =    //find and save initial focus component
               FocusUtils.findFirstFocusableComponent(dlgObj,JButton.class);
//    System.err.println("IstiDialogPopup initialFocusComp:  " +
//                                            ((initialFocusCompObj != null) ?
//                        initialFocusCompObj.getClass().getName() : "null"));
         //listen to window events in order to set the initial focus:
    dlgObj.addWindowListener(new WindowAdapter()
        {
          public void windowClosed(WindowEvent evtObj)
          {   //when window closes, clear the flag
            initialFocusFlag = false;
          }
          public void windowActivated(WindowEvent evtObj)
          {   //the first time the window gets focus set the initial focus
            if (!initialFocusFlag)
            {      //initial focus not yet set
              if(parentHeightLimitFlag)
              {    //flag set to limit dialog height to that of parent comp
                final Window windowObj;
                if((windowObj=getWindowForComponent(parentComp)) != null)
                {  //parent window object found; get bounds of it and dlg
                  final Rectangle windowBoundsObj = windowObj.getBounds();
                  final Rectangle dlgBoundsObj = dlgObj.getBounds();
                  if(dlgBoundsObj.height > windowBoundsObj.height)
                  {     //dialog height greater than parent
                                       //make height same as parent:
                    dlgBoundsObj.height = windowBoundsObj.height;
                                       //make Y-position same as parent:
                    dlgBoundsObj.y = windowObj.getY();
                                       //enter new location & size for dlg:
                    dlgObj.setBounds(dlgBoundsObj);
                  }
                }
              }
              else if (maxHeightLimitFlag)
              {  //flag set to limit dialog height to the maximum
                try
                {
                  final GraphicsEnvironment ge =
                      GraphicsEnvironment.getLocalGraphicsEnvironment();
                  final Rectangle windowBoundsObj = ge.getMaximumWindowBounds();
                  final Rectangle dlgBoundsObj = dlgObj.getBounds();
                  if (dlgBoundsObj.height > windowBoundsObj.height)
                  {
                    dlgBoundsObj.height = windowBoundsObj.height;
                    dlgObj.setBounds(dlgBoundsObj);
                  }
                }
                catch (Exception ex) {}
              }
              setInitialFocus();            //set initial focus component
              initialFocusFlag = true;      //indicate initial focus set
            }
                   //if flag then repeat 'pack()' the first time the window
                   // is shown to account for size changes after layout:
            if(repackNeededFlag)
            {
              repackNeededFlag = false;
              doDelayedRepack(parentComp);
            }
          }
        });
         //also listen for component-hidden event:
    dlgObj.addComponentListener(new ComponentAdapter()
        {     //when dialog hidden, clear the flag
          public void componentHidden(ComponentEvent evtObj)
          {
            initialFocusFlag = false;
          }
        });
         //make dialog resizable (to be consistent in Java 1.3 & 1.4):
    dlgObj.setResizable(true);
  }

  /**
   * Creates a popup dialog window.
   * @param parentComp the parent component for the popup.
   * @param msgObj the message object to be shown.
   * @param titleStr the title string for popup window.
   * @param msgType the type of message to be displayed (primarily used
   * to determine the icon from the pluggable Look and Feel):
   * ERROR_MESSAGE, INFORMATION_MESSAGE, WARNING_MESSAGE,
   * QUESTION_MESSAGE, or PLAIN_MESSAGE.
   * @param optionsArr an array of Objects that defines what buttons are
   * shown, or null for default or no buttons, based on 'optionType'.
   * @param initValIdx the index of the 'optionsArr[]' object
   * corresponding to the option to be selected by default.
   * @param modalFlag true for modal, false for modeless (allows other
   * windows to run).
   * @param createButtonFlag if true and the given 'optionsArr' contains a
   * single non-Component item and the given 'modalFlag' is false then the
   * button created for 'optionObj' will dispose the dialog (in addition to
   * hiding it) when used.
   * @param optionType specifies what default option buttons to
   * automatically create if 'optionsArr' is null or empty; one of:
   * DEFAULT_OPTION, YES_NO_OPTION, YES_NO_CANCEL_OPTION, OK_CANCEL_OPTION
   * or NO_AUTOBUTTONS_OPTION.  If default buttons are created and the user
   * selects one then the object value returned by 'show()' or 'getValue()'
   * will be an 'Integer' object wrapping one of the 'JOptionPane' values
   * (such as 'OK_OPTION').
   */
  public IstiDialogPopup(final Component parentComp,Object msgObj,
            String titleStr,int msgType,Object [] optionsArr,int initValIdx,
                  boolean modalFlag,boolean createButtonFlag,int optionType)
  {
    this(parentComp,null,msgObj,titleStr,msgType,optionsArr,initValIdx,
                                     modalFlag,createButtonFlag,optionType);
  }

  /**
   * Creates a popup dialog window.
   * @param parentComp the parent component for the popup.
   * @param msgObj the message object to be shown.
   * @param titleStr the title string for popup window.
   * @param msgType the type of message to be displayed (primarily used
   * to determine the icon from the pluggable Look and Feel):
   * ERROR_MESSAGE, INFORMATION_MESSAGE, WARNING_MESSAGE,
   * QUESTION_MESSAGE, or PLAIN_MESSAGE.
   * @param optionsArr an array of Objects that defines what buttons are
   * shown, or null for no buttons.
   * @param initValIdx the index of the 'optionsArr[]' object
   * corresponding to the option to be selected by default.
   * @param modalFlag true for modal, false for modeless (allows other
   * windows to run).
   * @param createButtonFlag if true and the given 'optionsArr' contains a
   * single non-Component item and the given 'modalFlag' is false then the
   * button created for 'optionObj' will dispose the dialog (in addition to
   * hiding it) when used.
   */
  public IstiDialogPopup(Component parentComp,Object msgObj,String titleStr,
          int msgType,Object [] optionsArr,int initValIdx,boolean modalFlag,
                                                   boolean createButtonFlag)
  {
    this(parentComp,null,msgObj,titleStr,msgType,optionsArr,initValIdx,
                          modalFlag,createButtonFlag,NO_AUTOBUTTONS_OPTION);
  }

  /**
   * Creates a popup dialog window.
   * @param parentComp the parent component for the popup.
   * @param msgObj the message object to be shown.
   * @param titleStr the title string for popup window.
   * @param msgType the type of message to be displayed (primarily used
   * to determine the icon from the pluggable Look and Feel):
   * ERROR_MESSAGE, INFORMATION_MESSAGE, WARNING_MESSAGE,
   * QUESTION_MESSAGE, or PLAIN_MESSAGE.
   * @param optionsArr an array of Objects that defines what buttons are
   * shown, or null for no buttons.
   * @param initValIdx the index of the 'optionsArr[]' object
   * corresponding to the option to be selected by default.
   * @param modalFlag true for modal, false for modeless (allows other
   * windows to run).
   */
  public IstiDialogPopup(Component parentComp,Object msgObj,String titleStr,
          int msgType,Object [] optionsArr,int initValIdx,boolean modalFlag)
  {
    this(parentComp,null,msgObj,titleStr,msgType,optionsArr,initValIdx,
                                     modalFlag,false,NO_AUTOBUTTONS_OPTION);
  }

  /**
   * Creates a plain-message popup dialog window.
   * @param parentComp the parent component for the popup.
   * @param msgObj the message object to be shown.
   * @param titleStr the title string for popup window.
   * @param optionsArr an array of Objects that defines what buttons are
   * shown, or null for no buttons.
   * @param initValIdx the index of the 'optionsArr[]' object
   * corresponding to the option to be selected by default.
   * @param modalFlag true for modal, false for modeless (allows other
   * windows to run).
   */
  public IstiDialogPopup(Component parentComp,Object msgObj,String titleStr,
                      Object [] optionsArr,int initValIdx,boolean modalFlag)
  {
    this(parentComp,msgObj,titleStr,PLAIN_MESSAGE,optionsArr,initValIdx,
                                                                 modalFlag);
  }

  /**
   * Creates a popup dialog window.  By default the dialog is modal.
   * @param parentComp the parent component for the popup.
   * @param msgObj the message object to be shown.
   * @param titleStr the title string for popup window.
   * @param msgType the type of message to be displayed (primarily used
   * to determine the icon from the pluggable Look and Feel):
   * ERROR_MESSAGE, INFORMATION_MESSAGE, WARNING_MESSAGE,
   * QUESTION_MESSAGE, or PLAIN_MESSAGE.
   * @param optionsArr an array of Objects that defines what buttons are
   * shown, or null for no buttons.
   * @param initValIdx the index of the 'optionsArr[]' object
   * corresponding to the option to be selected by default.
   */
  public IstiDialogPopup(Component parentComp,Object msgObj,String titleStr,
                            int msgType,Object [] optionsArr,int initValIdx)
  {
    this(parentComp,msgObj,titleStr,msgType,optionsArr,initValIdx,true);
  }

  /**
   * Creates a plain-message popup dialog window.  By default the dialog
   * is modal.
   * @param parentComp the parent component for the popup.
   * @param msgObj the message object to be shown.
   * @param titleStr the title string for popup window.
   * @param optionsArr an array of Objects that defines what buttons are
   * shown, or null for no buttons.
   * @param initValIdx the index of the 'optionsArr[]' object
   * corresponding to the option to be selected by default.
   */
  public IstiDialogPopup(Component parentComp,Object msgObj,String titleStr,
                                        Object [] optionsArr,int initValIdx)
  {
    this(parentComp,msgObj,titleStr,PLAIN_MESSAGE,optionsArr,initValIdx,
                                                                      true);
  }

  /**
   * Creates a popup dialog window containing a single button.  If the
   * given 'optionObj' is not a Component and the given 'modalFlag' is
   * false then the button created for 'optionObj' will dispose the
   * dialog (in addition to hiding it) when used.
   * @param parentComp the parent component for the popup.
   * @param msgObj the message object to be shown.
   * @param titleStr the title string for popup window.
   * @param msgType the type of message to be displayed (primarily used
   * to determine the icon from the pluggable Look and Feel):
   * ERROR_MESSAGE, INFORMATION_MESSAGE, WARNING_MESSAGE,
   * QUESTION_MESSAGE, or PLAIN_MESSAGE.
   * @param optionObj the Object that defines the button, or null to display
   * no buttons.
   * @param modalFlag true for modal, false for modeless (allows other
   * windows to run).
   */
  public IstiDialogPopup(Component parentComp,Object msgObj,String titleStr,
                             int msgType,Object optionObj,boolean modalFlag)
  {
    this(parentComp,null,msgObj,titleStr,msgType,((optionObj != null) ?
                     (new Object [] { optionObj }) : null),0,modalFlag,true,
                                                     NO_AUTOBUTTONS_OPTION);
  }

  /**
   * Creates a plain-message popup dialog window containing a single
   * button.
   * @param parentComp the parent component for the popup.
   * @param msgObj the message object to be shown.
   * @param titleStr the title string for popup window.
   * @param optionObj the Object that defines the button, or null to display
   * no buttons.
   * @param modalFlag true for modal, false for modeless (allows other
   * windows to run).
   */
  public IstiDialogPopup(Component parentComp,Object msgObj,String titleStr,
                                         Object optionObj,boolean modalFlag)
  {
    this(parentComp,msgObj,titleStr,PLAIN_MESSAGE,optionObj,modalFlag);
  }

  /**
   * Creates a popup dialog window containing a single button.  By default
   * the dialog is modal.
   * @param parentComp the parent component for the popup.
   * @param msgObj the message object to be shown.
   * @param titleStr the title string for popup window.
   * @param msgType the type of message to be displayed (primarily used
   * to determine the icon from the pluggable Look and Feel):
   * ERROR_MESSAGE, INFORMATION_MESSAGE, WARNING_MESSAGE,
   * QUESTION_MESSAGE, or PLAIN_MESSAGE.
   * @param optionObj the Object that defines the button, or null to display
   * no buttons.
   */
  public IstiDialogPopup(Component parentComp,Object msgObj,String titleStr,
                                               int msgType,Object optionObj)
  {
    this(parentComp,msgObj,titleStr,msgType,optionObj,true);
  }

  /**
   * Creates a plain-message popup dialog window containing a single button.
   * By default the dialog is modal.
   * @param parentComp the parent component for the popup.
   * @param msgObj the message object to be shown.
   * @param titleStr the title string for popup window.
   * @param optionObj the Object that defines the button, or null to display
   * no buttons.
   */
  public IstiDialogPopup(Component parentComp,Object msgObj,String titleStr,
                                                           Object optionObj)
  {
    this(parentComp,msgObj,titleStr,PLAIN_MESSAGE,optionObj,true);
  }

  /**
   * Creates a popup dialog window with default buttons based on the
   * value of 'optionType'.  If the user selects a default button then the
   * object value returned by 'show()' or 'getValue()' will be an
   * 'Integer' object wrapping one of the 'JOptionPane' values (such
   * as 'OK_OPTION').
   * @param parentComp the parent component for the popup.
   * @param msgObj the message object to be shown.
   * @param titleStr the title string for popup window.
   * @param msgType the type of message to be displayed (primarily used
   * to determine the icon from the pluggable Look and Feel):
   * ERROR_MESSAGE, INFORMATION_MESSAGE, WARNING_MESSAGE,
   * QUESTION_MESSAGE, or PLAIN_MESSAGE.
   * @param modalFlag true for modal, false for modeless (allows other
   * windows to run).
   * @param optionType specifies what default option buttons to
   * automatically create if 'optionsArr' is null or empty; one of:
   * DEFAULT_OPTION, YES_NO_OPTION, YES_NO_CANCEL_OPTION, OK_CANCEL_OPTION
   * or NO_AUTOBUTTONS_OPTION.
   */
  public IstiDialogPopup(Component parentComp,Object msgObj,String titleStr,
                               int msgType,boolean modalFlag,int optionType)
  {
    this(parentComp,null,msgObj,titleStr,msgType,null,0,modalFlag,false,optionType);
  }

  /**
   * Creates a modal popup dialog window with default buttons based on the
   * value of 'optionType'.  If the user selects a default button then the
   * object value returned by 'show()' or 'getValue()' will be an
   * 'Integer' object wrapping one of the 'JOptionPane' values (such
   * as 'OK_OPTION').
   * @param parentComp the parent component for the popup.
   * @param msgObj the message object to be shown.
   * @param titleStr the title string for popup window.
   * @param msgType the type of message to be displayed (primarily used
   * to determine the icon from the pluggable Look and Feel):
   * ERROR_MESSAGE, INFORMATION_MESSAGE, WARNING_MESSAGE,
   * QUESTION_MESSAGE, or PLAIN_MESSAGE.
   * @param optionType specifies what default option buttons to
   * automatically create if 'optionsArr' is null or empty; one of:
   * DEFAULT_OPTION, YES_NO_OPTION, YES_NO_CANCEL_OPTION, OK_CANCEL_OPTION
   * or NO_AUTOBUTTONS_OPTION.
   */
  public IstiDialogPopup(Component parentComp,Object msgObj,String titleStr,
                                                 int msgType,int optionType)
  {
    this(parentComp,null,msgObj,titleStr,msgType,null,0,true,false,optionType);
  }

  /**
   * Creates a plain-message popup dialog window with default buttons
   * based on the value of 'optionType'.  If the user selects a default
   * button then the object value returned by 'show()' or 'getValue()'
   * will be an 'Integer' object wrapping one of the 'JOptionPane' values
   * (such as 'OK_OPTION').
   * @param parentComp the parent component for the popup.
   * @param msgObj the message object to be shown.
   * @param titleStr the title string for popup window.
   * @param modalFlag true for modal, false for modeless (allows other
   * windows to run).
   * @param optionType specifies what default option buttons to
   * automatically create if 'optionsArr' is null or empty; one of:
   * DEFAULT_OPTION, YES_NO_OPTION, YES_NO_CANCEL_OPTION, OK_CANCEL_OPTION
   * or NO_AUTOBUTTONS_OPTION.
   */
  public IstiDialogPopup(Component parentComp,Object msgObj,String titleStr,
                                           boolean modalFlag,int optionType)
  {
    this(parentComp,null,msgObj,titleStr,PLAIN_MESSAGE,null,0,modalFlag,false,
                                                                optionType);
  }

  /**
   * Creates a plain-message modal popup dialog window with default buttons
   * based on the value of 'optionType'.  If the user selects a default
   * button then the object value returned by 'show()' or 'getValue()'
   * will be an 'Integer' object wrapping one of the 'JOptionPane' values
   * (such as 'OK_OPTION').
   * @param parentComp the parent component for the popup.
   * @param msgObj the message object to be shown.
   * @param titleStr the title string for popup window.
   * @param optionType specifies what default option buttons to
   * automatically create if 'optionsArr' is null or empty; one of:
   * DEFAULT_OPTION, YES_NO_OPTION, YES_NO_CANCEL_OPTION, OK_CANCEL_OPTION
   * or NO_AUTOBUTTONS_OPTION.
   */
  public IstiDialogPopup(Component parentComp,Object msgObj,String titleStr,
                                                             int optionType)
  {
    this(parentComp,null,msgObj,titleStr,PLAIN_MESSAGE,null,0,true,false,
                                                                optionType);
  }

  /**
   * Sets the static enforce-changes-via-event-dispatch-thread flag
   * that is used as a default on all newly-created instances.
   * @param flgVal true to make changes occur via the event-dispatch
   * thread, false to not.
   */
  public static void setStaticEnforceDispatchThreadFlag(boolean flgVal)
  {
    staticEnforceDispatchThreadFlag = flgVal;
  }

  /**
   * Returns the static enforce-changes-via-event-dispatch-thread flag
   * that is used as a default on all newly-created instances.
   * @return true if changes are made to occur via the event-dispatch
   * thread, false if not.
   */
  public static boolean getStaticEnforceDispatchThreadFlag()
  {
    return staticEnforceDispatchThreadFlag;
  }

  /**
   * Sets the enforce-changes-via-event-dispatch-thread flag.
   * @param flgVal true to make changes occur via the event-dispatch
   * thread, false to not.
   */
  public void setEnforceDispatchThreadFlag(boolean flgVal)
  {
    enforceDispatchThreadFlag = flgVal;
  }

  /**
   * Returns the enforce-changes-via-event-dispatch-thread flag.
   * @return true if changes are made to occur via the event-dispatch
   * thread, false if not.
   */
  public boolean getEnforceDispatchThreadFlag()
  {
    return enforceDispatchThreadFlag;
  }

  /**
   * Sets the static limit-height-to-that-of-parent flag
   * that is used as a default on all newly-created instances.
   * @param flgVal true to limit the height of the dialog to be no
   * taller than its parent component, false to not.
   */
  public static void setStaticParentHeightLimit(boolean flgVal)
  {
    staticParentHeightLimitFlag = flgVal;
  }

  /**
   * Returns the static limit-height-to-that-of-parent flag
   * that is used as a default on all newly-created instances.
   * @return true if the height of the dialog is limited to be no
   * taller than its parent component, false if not.
   */
  public static boolean getStaticParentHeightLimit()
  {
    return staticParentHeightLimitFlag;
  }

  /**
   * Sets the limit-height-to-that-of-parent flag.
   * @param flgVal true to limit the height of the dialog to be no
   * taller than its parent component, false to not.
   */
  public void setParentHeightLimit(boolean flgVal)
  {
    parentHeightLimitFlag = flgVal;
  }

  /**
   * Returns the limit-height-to-that-of-parent flag.
   * @return true if the height of the dialog is limited to be no
   * taller than its parent component, false if not.
   */
  public boolean getParentHeightLimit()
  {
    return parentHeightLimitFlag;
  }

  /**
   * Sets the static limit-height-to-the-maximum flag
   * that is used as a default on all newly-created instances.
   * @param flgVal true to limit the height of the dialog to be no
   * taller than the maximum, false to not.
   */
  public static void setStaticMaxHeightLimit(boolean flgVal)
  {
    staticMaxHeightLimitFlag = flgVal;
  }

  /**
   * Returns the static limit-height-to-the-maximum flag
   * that is used as a default on all newly-created instances.
   * @return true if the height of the dialog is limited to be no
   * taller than the maximum, false if not.
   */
  public static boolean getStaticMaxHeightLimit()
  {
    return staticMaxHeightLimitFlag;
  }

  /**
   * Sets the limit-height-to-the-maximum flag.
   * @param flgVal true to limit the height of the dialog to be no
   * taller than the maximum, false to not.
   */
  public void setMaxHeightLimit(boolean flgVal)
  {
    maxHeightLimitFlag = flgVal;
  }

  /**
   * Returns the limit-height-to-the-maximum flag.
   * @return true if the height of the dialog is limited to be no
   * taller than the maximum, false if not.
   */
  public boolean getMaxHeightLimit()
  {
    return maxHeightLimitFlag;
  }

  /**
   * Creates and returns a new <code>JDialog</code> wrapping
   * the <code>JOptionPane</code> for this <code>IstiDialogPanel</code>
   * centered on the <code>parentComponent</code>
   * in the <code>parentComponent</code>'s frame.
   * <code>title</code> is the title of the returned dialog.
   * The returned <code>JDialog</code> will be set up such that
   * once it is closed, or the user clicks on one of the buttons,
   * the optionpane's value property will be set accordingly and
   * the dialog will be closed.  Each time the dialog is made visible,
   * it will reset the option pane's value property to
   * <code>JOptionPane.UNINITIALIZED_VALUE</code> to ensure the
   * user's subsequent action closes the dialog properly.
   * @param givenDialogObj A 'JDialog' to be used, or null to have a
   * new one created.
   * @param parentComponent determines the frame in which the dialog
   *		is displayed; if the <code>parentComponent</code> has
   *		no <code>Frame</code>, a default <code>Frame</code> is used
   * @param title     the title string for the dialog
   * @return a new <code>JDialog</code> containing this instance
   * @exception HeadlessException if
   *   <code>GraphicsEnvironment.isHeadless</code> returns
   *   <code>true</code>
   * @see java.awt.GraphicsEnvironment#isHeadless
   */
  public final JDialog createDialog(JDialog givenDialogObj,
                                    Component parentComponent, String title)
  {
    final JDialog dialogObj;
    if(givenDialogObj == null)
    {    //dialog object not given
      final Window windowObj = getWindowForComponent(parentComponent);
      if (windowObj instanceof Frame)
        dialogObj = new JDialog((Frame)windowObj, title, true);
      else
        dialogObj = new JDialog((Dialog)windowObj, title, true);
    }
    else      //dialog object given; use it
      dialogObj = givenDialogObj;
    final Container contentPane = dialogObj.getContentPane();

    contentPane.setLayout(new BorderLayout());
    contentPane.add(paneObj, BorderLayout.CENTER);
    dialogObj.pack();
    dialogObj.setLocationRelativeTo(parentComponent);
         //set location to check against if repack is done later:
    repackCheckLocationObj = dialogObj.getLocation();

    dialogObj.addWindowListener(new WindowAdapter()
        {
          private boolean gotFocus = false;
          public void windowClosing(WindowEvent we)
          {
            paneObj.setValue(null);
          }
          public void windowGainedFocus(WindowEvent we)
          {
            // Once window gets focus, set initial focus
            if (!gotFocus)
            {
              paneObj.selectInitialValue();
              gotFocus = true;
            }
          }
        });
    dialogObj.addComponentListener(new ComponentAdapter()
        {
          public void componentShown(ComponentEvent ce)
          {
              // reset value to ensure closing works properly
            paneObj.setValue(JOptionPane.UNINITIALIZED_VALUE);
          }
        });
    paneObj.addPropertyChangeListener(new PropertyChangeListener()
        {
          public void propertyChange(PropertyChangeEvent event)
          {
            // Let the defaultCloseOperation handle the closing
            // if the user closed the window without selecting a button
            // (newValue = null in that case).  Otherwise, close the dialog.
            // (only if dispose or close flag is set)
            if((hideDialogOnButtonFlag || disposeDialogOnButtonFlag) &&
                                                    dialogObj.isVisible() &&
                                             event.getSource() == paneObj &&
                                            (event.getPropertyName().equals(
                                             JOptionPane.VALUE_PROPERTY)) &&
                                              event.getNewValue() != null &&
                                                      event.getNewValue() !=
                                            JOptionPane.UNINITIALIZED_VALUE)
            {
              if (allowDialogClose(event.getNewValue()))
              {
                dialogObj.setVisible(false);
                if (disposeDialogOnButtonFlag)
                  dialogObj.dispose();
              }
              else
              {
                paneObj.setValue(JOptionPane.UNINITIALIZED_VALUE);
              }
            }
            else   //check for condition where a non-modal window
            {      // got stuck by being hidden while minimized and
                   // its close ("x") button was pressed:
              if(!dialogObj.isVisible() && event.getSource() == paneObj &&
                                            (event.getPropertyName().equals(
                                             JOptionPane.VALUE_PROPERTY)) &&
                                             (event.getNewValue() == null ||
                                                      event.getNewValue() ==
                                           JOptionPane.UNINITIALIZED_VALUE))
              {
                dialogObj.dispose();        //dispose to clear-away window
              }
            }
          }
        });
    return dialogObj;
  }

  /**
   * This method is invoked before the dialog is closed by pressing the OK button.
   * This method should be overridden to prevent the dialog from being closed.
   * @return true to allow the dialog to be closed, false otherwise.
   */
  protected boolean allowDialogClose()
  {
    return true;
  }

  /**
   * This method is invoked before the dialog is closed by pressing a button.
   * This method should be overridden to prevent the dialog from being closed.
   * @param value The Object chosen by the user.
   * @return true to allow the dialog to be closed, false otherwise.
   */
  protected boolean allowDialogClose(Object value)
  {
    if (value instanceof Integer && ((Integer)value).intValue() == OK_OPTION)
    {
      return allowDialogClose();
    }
    return true;
  }

  /**
   * Creates and returns a new <code>JDialog</code> wrapping
   * the <code>JOptionPane</code> for this <code>IstiDialogPanel</code>
   * centered on the <code>parentComponent</code>
   * in the <code>parentComponent</code>'s frame.
   * <code>title</code> is the title of the returned dialog.
   * The returned <code>JDialog</code> will be set up such that
   * once it is closed, or the user clicks on one of the buttons,
   * the optionpane's value property will be set accordingly and
   * the dialog will be closed.  Each time the dialog is made visible,
   * it will reset the option pane's value property to
   * <code>JOptionPane.UNINITIALIZED_VALUE</code> to ensure the
   * user's subsequent action closes the dialog properly.
   * @param parentComponent determines the frame in which the dialog
   *		is displayed; if the <code>parentComponent</code> has
   *		no <code>Frame</code>, a default <code>Frame</code> is used
   * @param title     the title string for the dialog
   * @return a new <code>JDialog</code> containing this instance
   * @exception HeadlessException if
   *   <code>GraphicsEnvironment.isHeadless</code> returns
   *   <code>true</code>
   * @see java.awt.GraphicsEnvironment#isHeadless
   */
  public final JDialog createDialog(Component parentComponent, String title)
  {
    return createDialog(null,parentComponent,title);
  }

  /**
   * Configures whether or not the dialog will be automatically hidden
   * after any button is pressed.  If this method is not called then
   * the dialog will default to being automatically hidden after any
   * button is pressed.
   * @param flgVal true to be hidden after any button is pressed; false
   * to remain open (in which case the hiding and disposing of the dialog
   * will need to be handled by the calling code).
   */
  public void setHideDialogOnButtonFlag(boolean flgVal)
  {
    hideDialogOnButtonFlag = flgVal;
  }

  /**
   * Returns a flag indicating whether or not the dialog will be
   * automatically hidden after any button is pressed.
   * @return true if hidden after any button is pressed; false if
   * remaining open (in which case the hiding and disposing of the
   * dialog will need to be handled by the calling code).
   */
  public boolean getHideDialogOnButtonFlag()
  {
    return hideDialogOnButtonFlag;
  }

  /**
   * Configures whether or not the dialog will be automatically hidden
   * and disposed after any button is pressed.  If this method is not
   * called then the dialog will default to not automatically being
   * disposed after a button is pressed (this matches the default for
   * Java 1.4 but not Java 1.3).
   * @param flgVal true to hide and dispose after any button is pressed;
   * false to not dispose (in which case the disposing of the dialog
   * will need to be handled by the calling code).
   */
  public void setDisposeDialogOnButtonFlag(boolean flgVal)
  {
    disposeDialogOnButtonFlag = flgVal;
  }

  /**
   * Returns a flag indicating whether or not the dialog will be
   * automatically closed after any button is pressed.
   * @return true if closed after any button is pressed; false if
   * remaining open (in which case the hiding and disposing of the
   * dialog will need to be handled by the calling code).
   */
  public boolean getDisposeDialogOnButtonFlag()
  {
    return disposeDialogOnButtonFlag;
  }

  /**
   * Configures whether or not the dialog will be repacked the first time
   * it is shown.  The can help the layout of components whose dimension are
   * undetermined until laid out (like 'JTextArea' with word-wrapping
   * enabled), but it can hurt the layout of components whose contents are
   * loaded after the component is shown (like 'ViewHTMLPanel' and some uses
   * of  'JEditorPane').  If this method is not called then the dialog will
   * default to not doing the repack.
   * @param flgVal true to have the dialog repacked the first time it is
   * shown; false to not.
   */
  public void setRepackNeededFlag(boolean flgVal)
  {
    repackNeededFlag = flgVal;
  }

  /**
   * Returns a flag indicating whether or not the dialog will be repacked
   * the first time it is shown.
   * @return true if the dialog is to be repacked the first time it is
   * shown; false if not.
   */
  public boolean getRepackNeededFlag()
  {
    return repackNeededFlag;
  }

  /**
   * Performs a repack after a slight delay (using a worker thread).
   * @param parentComp parent component to be recentered upon after
   * repack, or null for no recentering.
   */
  protected void doDelayedRepack(final Component parentComp)
  {
    (new Thread("doDelayedRepack")
        {
          public void run()
          {
            try { sleep(100); }
            catch(InterruptedException ex) {}
            SwingUtilities.invokeLater(new Runnable()
                {       //use event-dispatch thread for repack
                  public void run()
                  {
                    doPack();
                        //if check-location object exists and location of
                        // dlg hasn't changed then redo centering on parent:
                    if(parentComp != null &&
                                           repackCheckLocationObj != null &&
                                              repackCheckLocationObj.equals(
                                                      dlgObj.getLocation()))
                    {
                      dlgObj.setLocationRelativeTo(parentComp);
                    }
                  }
                });
          }
        }).start();
  }

  /**
   * Adds a PropertyChangeListener to the listener list.
   * The listener is registered for all properties.
   * A PropertyChangeEvent will get fired in response to setting
   * a bound property, such as setFont, setBackground, or setForeground.
   * @param listener  the PropertyChangeListener to be added.
   */
  public void addPropertyChangeListener(PropertyChangeListener listener)
  {
    paneObj.addPropertyChangeListener(listener);
  }

  /**
   * Adds a PropertyChangeListener for a specific property.  The listener
   * will be invoked only when a call on firePropertyChange names that
   * specific property.
   * If listener is null, no exception is thrown and no action is performed.
   * @param propertyName  the name of the property to listen on.
   * @param listener  the PropertyChangeListener to be added.
   */
  public void addPropertyChangeListener(String propertyName,
                                            PropertyChangeListener listener)
  {
    paneObj.addPropertyChangeListener(propertyName,listener);
  }

  /**
   * Removes a PropertyChangeListener from the listener list.
   * This removes a PropertyChangeListener that was registered
   * for all properties.
   * @param listener  the PropertyChangeListener to be removed.
   */
  public void removePropertyChangeListener(PropertyChangeListener listener)
  {
    paneObj.removePropertyChangeListener(listener);
  }

  /**
   * Removes a PropertyChangeListener for a specific property.
   * If listener is null, no exception is thrown and no action is performed.
   * @param propertyName  the name of the property that was listened on.
   * @param listener  the PropertyChangeListener to be removed.
   */
  public void removePropertyChangeListener(String propertyName,
                                            PropertyChangeListener listener)
  {
    paneObj.removePropertyChangeListener(propertyName,listener);
  }

  /**
   * Displays the popup dialog window.  If the dialog is modal then this
   * method blocks until the dialog is dismissed by the user or the
   * 'close()' method is called.
   * @return The Object chosen by the user (UNINITIALIZED_VALUE if the
   * user has not yet made a choice, or null if the user closed the window
   * without making a choice) or the value entered via the 'close()' method.
   */
  public Object show()
  {
    selectAndShow();                   //show the dialog
    return paneObj.getValue();         //fetch and return value
  }

  /**
   * Displays the popup dialog window.  The method blocks until the
   * dialog is dismissed by the user or the 'close()' method is called
   * (even if the window is non-modal).
   * @return The Object chosen by the user (UNINITIALIZED_VALUE if the
   * user has not yet made a choice, or null if the user closed the window
   * without making a choice) or the value entered via the 'close()' method.
   */
  public Object showAndWait()
  {
    if(dlgObj.isModal())          //if modal dialog then
      selectAndShow();            //show dialog (blocks)
    else
    {    //dialog is modeless
      waitingForShowFlag = true;  //set "waiting" flag
      selectAndShow();            //show dialog
         //wait for dialog to be cleared but still process AWT events:
      try
      {
        if(SwingUtilities.isEventDispatchThread())
        {     //current thread is the event dispatch thread
          final EventQueue evtQueueObj =
                                  dlgObj.getToolkit().getSystemEventQueue();
          AWTEvent event;
          Object srcObj;
          while (dlgObj.isVisible())
          {
            event = evtQueueObj.getNextEvent();
            srcObj = event.getSource();
            if (event instanceof ActiveEvent)
            {
              ((ActiveEvent)event).dispatch();
            }
            else if (srcObj instanceof Component)
            {
              ((Component)srcObj).dispatchEvent(event);
            }
          }
        }
        else
        {     //current thread is not the event dispatch thread
                   //create listeners that will call 'notify()' when
                   // the window is cleared; thus exiting the loop
                   // (the component listener gets called when the
                   //  'close()' or dialog 'hide()' method is used,
                   //  whereas the window listener gets called when
                   //  the dialog is closed via a button on the
                   //  dialog or 'setVisible(false)' is used):
          final ComponentAdapter compListenerObj = new ComponentAdapter()
              {
                public void componentHidden(ComponentEvent evtObj)
                {
                  synchronized(dlgObj.getTreeLock())
                  {
                    dlgObj.getTreeLock().notifyAll();
                  }
                }
              };
          final WindowAdapter windowListenerObj = new WindowAdapter()
              {
                public void windowClosed(WindowEvent evtObj)
                {
                  synchronized(dlgObj.getTreeLock())
                  {
                    dlgObj.getTreeLock().notifyAll();
                  }
                }
              };
          dlgObj.addComponentListener(compListenerObj);
          dlgObj.addWindowListener(windowListenerObj);
          synchronized (dlgObj.getTreeLock())
          {   //must own the tree-lock monitor to execute 'wait()'
            while (waitingForShowFlag || dlgObj.isVisible())
            {      //loop while waiting or while the dialog is visible
              try
              {                   //wait for listener to 'notify':
                dlgObj.getTreeLock().wait();
              }
              catch(InterruptedException e)
              {
                break;
              }
            }
          }                       //remove added listeners:
          dlgObj.removeWindowListener(windowListenerObj);
          dlgObj.removeComponentListener(compListenerObj);
        }
      }
      catch (Exception ex)
      {
        ex.printStackTrace();
      }
    }
    return paneObj.getValue();         //fetch and return value
  }

  /**
   * Displays a popup option dialog window.  The method blocks until the
   * dialog is dismissed by the user or the 'close()' method is called
   * (even if the window is non-modal).  The returned value will be one
   * of the "..._OPTION" values, or the index corresponding to the
   * constructor-parameter 'optionsArr[]' entry selected by the user.
   * @return An integer indicating the option chosen by the user,
   * or CLOSED_OPTION if the user closed the dialog without selecting
   * an option.
   */
  public int showAndReturnIndex()
  {
    //display dialog and block until user selection made:
    final Object retObj = showAndWait();
    if(retObj == null)            //if null returned then
      return CLOSED_OPTION;       //return 'closed' value
    if(optionsArr == null || optionsArr.length == 0)
    {    //default options used; return value from 'Integer' object
      return (retObj instanceof Integer) ? ((Integer)retObj).intValue() :
          CLOSED_OPTION;
    }
    //find index corresponding to returned object:
    for(int idx=0; idx<optionsArr.length; ++idx)
    {    //for each option in given array
      if(optionsArr[idx] != null && optionsArr[idx].equals(retObj))
        return idx;     //if option object matched the return index value
    }
    return CLOSED_OPTION;
  }

  /**
   * Sets the visibility of the dialog window.  If the dialog is
   * non-modal then the call is redirected to the dialog object's
   * 'show()' or  'hide()' method (depending on the state of the
   * given flag parameter).
   * @param flgVal true for visible, false for hidden.
   */
  public void setVisible(final boolean flgVal)
  {
              //if not enforcing changes via event-dispatch thread or
              // if this is the event-dispatch thread then do it now:
    if(!enforceDispatchThreadFlag || SwingUtilities.isEventDispatchThread())
      doSetVisible(flgVal);
    else
    {         //do it later via event-dispatch thread:
      SwingUtilities.invokeLater(new Runnable()
          {
            public void run()
            {
              doSetVisible(flgVal);
            }
          });
    }
  }
  
  /**
   * Causes this dialog to be sized to fit the preferred size and layouts
   * of its subcomponents. If the dialog and/or its owner are not yet
   * displayable, both are made displayable before calculating the
   * preferred size. The Window will be validated after the preferredSize
   * is calculated.
   */
  protected void doPack() {
    packNeededFlag = false;
    dlgObj.pack();
  }
  
  /**
   * Sets the visibility of the dialog window.
   * @param flgVal true for visible, false for hidden.
   */
  protected void doSetDialogVisible(boolean flgVal)
  {
    // if visible and pack is needed pack first
    if (flgVal && packNeededFlag) 
    {
      doPack();
    }
    dlgObj.setVisible(flgVal);
  }

  /**
   * Sets the visibility of the dialog window.  If the dialog is
   * non-modal then the call is redirected to the dialog object's
   * 'show()' or  'hide()' method (depending on the state of the
   * given flag parameter).
   * @param flgVal true for visible, false for hidden.
   */
  protected void doSetVisible(boolean flgVal)
  {
    if(dlgObj.isModal())
    {    //modal dialog
      paneObj.selectInitialValue();    //select initial button
      doSetDialogVisible(flgVal);       //set dialog visible
    }
    else
    {    //non-modal dialog
      if(flgVal)             //if flag true then
        selectAndShow();     //show dialog
      else                   //if flag false then
        doSetDialogVisible(false);      //hide dialog
    }
  }

  /**
   * Sets the visibility of the dialog window, using the event-dispatch
   * thread via a call to 'SwingUtilities.invokeLater()'.  This allows
   * a modal dialog to be shown without blocking the invoking thread.
   * @param flgVal true for visible, false for hidden.
   */
  public void setVisibleViaInvokeLater(final boolean flgVal)
  {
         //do it later via event-dispatch thread:
    SwingUtilities.invokeLater(new Runnable()
        {
          public void run()
          {
            doSetVisible(flgVal);
          }
        });
  }

  /**
   * Selects the initial button and calls the dialog's 'show()' method.
   */
  protected void selectAndShow()
  {
              //if not enforcing changes via event-dispatch thread or
              // if dialog is modal or if this is the event-dispatch
              // thread then do it now:
    if(!enforceDispatchThreadFlag || dlgObj.isModal() ||
                                     SwingUtilities.isEventDispatchThread())
    {
                   //make sure JOptionPane 'value' is initialized
                   // (in case JOptionPane object is being reused):
      paneObj.setValue(JOptionPane.UNINITIALIZED_VALUE);
      if(initialFocusCompObj == null)  //if no initial focus component then
        paneObj.selectInitialValue();  //select initial button
      doSetDialogVisible(true);         //show dialog
      waitingForShowFlag = false;      //clear "waiting" flag
    }
    else
    {         //do it later via event-dispatch thread:
      SwingUtilities.invokeLater(new Runnable()
          {
            public void run()
            {
                   //make sure JOptionPane 'value' is initialized
                   // (in case JOptionPane object is being reused):
              paneObj.setValue(JOptionPane.UNINITIALIZED_VALUE);
              if(initialFocusCompObj == null)    //if no init focus comp then
                paneObj.selectInitialValue();    //select initial button
              doSetDialogVisible(true);           //show dialog
              waitingForShowFlag = false;        //clear "waiting" flag
            }
          });
    }
  }

  /**
   * Determines whether the popup dialog should be visible when its
   * parent is visible.
   * @return <code>true</code> if the popup dialog is visible;
   * <code>false</code> otherwise.
   */
  public boolean isVisible()
  {
    return dlgObj.isVisible();
  }

  /**
   * Returns true if the popup dialog window is showing.
   * @return true if the popup dialog window is showing, false if not.
   */
  public boolean isShowing()
  {
    return dlgObj.isShowing();
  }

  /**
   * Waits for the popup dialog window to be visible, up to the given
   * maximum time value.
   * @param maxWaitTimeMs the maximum time to wait, in milliseconds.
   * @return true if the dialog is visible, false if the maximum time was
   * reached without the dialog being visible.
   */
  public boolean waitForDialogVisible(int maxWaitTimeMs)
  {
         //convert timeout to # of 10ms loops, rounded up:
    int count = (maxWaitTimeMs + 5) / 10;
    while(!dlgObj.isVisible())
    {    //wait for dialog to be visible, up to timeout
      if(--count <= 0)       //if too many loops then
        return false;        //return with timeout-indicator falg
      try
      {       //delay 0.01 seconds:
        Thread.sleep(10);
      }
      catch(InterruptedException ex)
      {       //interrupted; setup to stop waiting
        count = 0;
      }
    }    //loop if dialog not showing and not too many loops
    return true;             //return with dialog-showing-indicator flag
  }

  /**
   * Closes the popup dialog window.  This method performs 'hide()'
   * followed by 'dispose()'.  Calling 'hide()' (and not dispose()')
   * on a non-modal dialog that is minimized can result in the dialog
   * becoming "stuck".
   * @param obj the value to be returned via the 'show()' or 'showAndWait()'
   * method.
   */
  public void close(final Object obj)
  {
              //if not enforcing changes via event-dispatch thread or
              // if this is the event-dispatch thread then do it now:
    if(!enforceDispatchThreadFlag || SwingUtilities.isEventDispatchThread())
    {
      paneObj.setValue(obj);
      close();
    }
    else
    {         //do it later via event-dispatch thread:
      SwingUtilities.invokeLater(new Runnable()
          {
            public void run()
            {
              paneObj.setValue(obj);
              close();
            }
          });
    }
  }

  /**
   * Closes the popup dialog window.  This method performs 'hide()'
   * followed by 'dispose()'.  Calling 'hide()' (and not dispose()')
   * on a non-modal dialog that is minimized can result in the dialog
   * becoming "stuck".
   */
  public void close()
  {
    hide();
    SwingUtilities.invokeLater(new Runnable()
        {
          public void run()
          {
            dlgObj.dispose();
          }
        });
  }

  /**
   * Waits (if necessary) for the popup dialog window to be visible and
   * then closes it.
   * @param obj the value to be returned via the 'show()' or 'showAndWait()'
   * method.
   * @param maxWaitTimeMs the maximum time to wait for the popup dialog
   * window to be visible, in milliseconds.
   */
  public void closeWithWait(Object obj,int maxWaitTimeMs)
  {
    waitForDialogVisible(maxWaitTimeMs);
    close(obj);
  }

  /**
   * Waits (if necessary) for the popup dialog window to be visible and
   * then closes it.
   * @param maxWaitTimeMs the maximum time to wait for the popup dialog
   * window to be visible, in milliseconds.
   */
  public void closeWithWait(int maxWaitTimeMs)
  {
    waitForDialogVisible(maxWaitTimeMs);
    close();
  }

  /**
   * Hides the popup dialog window.  Calling 'hide()' (and not dispose()')
   * on a non-modal dialog that is minimized can result in the dialog
   * becoming "stuck".
   */
  public void hide()
  {
              //if not enforcing changes via event-dispatch thread or
              // if this is the event-dispatch thread then do it now:
    if(!enforceDispatchThreadFlag || SwingUtilities.isEventDispatchThread())
      doSetDialogVisible(false);
    else
    {         //do it later via event-dispatch thread:
      SwingUtilities.invokeLater(new Runnable()
          {
            public void run()
            {
              doSetDialogVisible(false);
            }
          });
    }
  }

  /**
   * Disposes the popup dialog window.
   */
  public void dispose()
  {
              //if not enforcing changes via event-dispatch thread or
              // if this is the event-dispatch thread then do it now:
    if(!enforceDispatchThreadFlag || SwingUtilities.isEventDispatchThread())
      dlgObj.dispose();
    else
    {         //do it later via event-dispatch thread:
      SwingUtilities.invokeLater(new Runnable()
          {
            public void run()
            {
              dlgObj.dispose();
            }
          });
    }
  }

  /**
   * Repaints the popup dialog window.
   */
  public void repaint()
  {
              //if not enforcing changes via event-dispatch thread or
              // if this is the event-dispatch thread then do it now:
    if(!enforceDispatchThreadFlag || SwingUtilities.isEventDispatchThread())
      dlgObj.repaint();
    else
    {         //do it later via event-dispatch thread:
      SwingUtilities.invokeLater(new Runnable()
          {
            public void run()
            {
              dlgObj.repaint();
            }
          });
    }
  }

  /**
   * Validates the popup dialog window.
   */
  public void validate()
  {
              //if not enforcing changes via event-dispatch thread or
              // if this is the event-dispatch thread then do it now:
    if(!enforceDispatchThreadFlag || SwingUtilities.isEventDispatchThread())
      dlgObj.validate();
    else
    {         //do it later via event-dispatch thread:
      SwingUtilities.invokeLater(new Runnable()
          {
            public void run()
            {
              dlgObj.validate();
            }
          });
    }
  }

  /**
   * Invalidates the popup dialog window.
   */
  public void invalidate()
  {
              //if not enforcing changes via event-dispatch thread or
              // if this is the event-dispatch thread then do it now:
    if(!enforceDispatchThreadFlag || SwingUtilities.isEventDispatchThread())
      dlgObj.invalidate();
    else
    {         //do it later via event-dispatch thread:
      SwingUtilities.invokeLater(new Runnable()
          {
            public void run()
            {
              dlgObj.invalidate();
            }
          });
    }
  }

  /**
   * Causes this dialog to be sized to fit the preferred size and layouts
   * of its subcomponents. If the dialog and/or its owner are not yet
   * displayable, both are made displayable before calculating the
   * preferred size. The Window will be validated after the preferredSize
   * is calculated.
   */
  public void pack()
  {
              //if not enforcing changes via event-dispatch thread or
              // if this is the event-dispatch thread then do it now:
    if(!enforceDispatchThreadFlag || SwingUtilities.isEventDispatchThread())
      doPack();
    else
    {         //do it later via event-dispatch thread:
      SwingUtilities.invokeLater(new Runnable()
          {
            public void run()
            {
              doPack();
            }
          });
    }
  }

  /**
   * Sets the size of the dialog.
   * @param width the width to use.
   * @param height the height to use.
   */
  public void setSize(final int width,final int height)
  {
              //if not enforcing changes via event-dispatch thread or
              // if this is the event-dispatch thread then do it now:
    if(!enforceDispatchThreadFlag || SwingUtilities.isEventDispatchThread())
      dlgObj.setSize(width,height);
    else
    {         //do it later via event-dispatch thread:
      SwingUtilities.invokeLater(new Runnable()
          {
            public void run()
            {
              dlgObj.setSize(width,height);
            }
          });
    }
  }

  /**
   * Sets the size of the dialog.
   * @param dimObj the dimension of width and height to use.
   */
  public void setSize(final Dimension dimObj)
  {
              //if not enforcing changes via event-dispatch thread or
              // if this is the event-dispatch thread then do it now:
    if(!enforceDispatchThreadFlag || SwingUtilities.isEventDispatchThread())
      dlgObj.setSize(dimObj);
    else
    {         //do it later via event-dispatch thread:
      SwingUtilities.invokeLater(new Runnable()
          {
            public void run()
            {
              dlgObj.setSize(dimObj);
            }
          });
    }
  }

  /**
   * Sets whether the dialog is resizable by the user.
   * @param resizableFlag true if the user can resize this dialog; false
   * otherwise.
   */
  public void setResizable(final boolean resizableFlag)
  {
              //if not enforcing changes via event-dispatch thread or
              // if this is the event-dispatch thread then do it now:
    if(!enforceDispatchThreadFlag || SwingUtilities.isEventDispatchThread())
      dlgObj.setResizable(resizableFlag);
    else
    {         //do it later via event-dispatch thread:
      SwingUtilities.invokeLater(new Runnable()
          {
            public void run()
            {
              dlgObj.setResizable(resizableFlag);
            }
          });
    }
  }

  /**
   * Moves the dialog to a new location. The top-left corner of
   * the new location is specified by the <code>x</code> and <code>y</code>
   * parameters in the coordinate space of this component's parent.
   * @param x The <i>x</i>-coordinate of the new location's
   * top-left corner in the parent's coordinate space.
   * @param y The <i>y</i>-coordinate of the new location's
   * top-left corner in the parent's coordinate space.
   */
  public void setLocation(final int x, final int y)
  {
              //if not enforcing changes via event-dispatch thread or
              // if this is the event-dispatch thread then do it now:
    if(!enforceDispatchThreadFlag || SwingUtilities.isEventDispatchThread())
    {
      dlgObj.setLocation(x,y);
         //clear check location (used if repack is done later):
      repackCheckLocationObj = null;
    }
    else
    {         //do it later via event-dispatch thread:
      SwingUtilities.invokeLater(new Runnable()
          {
            public void run()
            {
              dlgObj.setLocation(x,y);
                   //clear check location (used if repack is done later):
              repackCheckLocationObj = null;
            }
          });
    }
  }

  /**
   * Moves the dialog to a new location. The top-left corner of
   * the new location is specified by point <code>p</code>. Point
   * <code>p</code> is given in the parent's coordinate space.
   * @param ptObj The point defining the top-left corner
   * of the new location, given in the coordinate space of this
   * component's parent.
   */
  public void setLocation(final Point ptObj)
  {
              //if not enforcing changes via event-dispatch thread or
              // if this is the event-dispatch thread then do it now:
    if(!enforceDispatchThreadFlag || SwingUtilities.isEventDispatchThread())
    {
      dlgObj.setLocation(ptObj);
         //clear check location (used if repack is done later):
      repackCheckLocationObj = null;
    }
    else
    {         //do it later via event-dispatch thread:
      SwingUtilities.invokeLater(new Runnable()
          {
            public void run()
            {
              dlgObj.setLocation(ptObj);
                   //clear check location (used if repack is done later):
              repackCheckLocationObj = null;
            }
          });
    }
  }

  /**
   * Sets the location of the dialog relative to the specified
   * component. If the component is not currently showing, the
   * dialog is centered on the screen.
   * @param compObj the component in relation to which the dialog's
   * location is determined.
   */
  public void setLocationRelativeTo(final Component compObj)
  {
              //if not enforcing changes via event-dispatch thread or
              // if this is the event-dispatch thread then do it now:
    if(!enforceDispatchThreadFlag || SwingUtilities.isEventDispatchThread())
    {
      dlgObj.setLocationRelativeTo(compObj);
         //clear check location (used if repack is done later):
      repackCheckLocationObj = null;
    }
    else
    {         //do it later via event-dispatch thread:
      SwingUtilities.invokeLater(new Runnable()
          {
            public void run()
            {
              dlgObj.setLocationRelativeTo(compObj);
                   //clear check location (used if repack is done later):
              repackCheckLocationObj = null;
            }
          });
    }
  }

  /**
   * Requests that the dialog get the input focus, and that the
   * dialog's top-level ancestor become the focused Window.
   */
  public void requestFocus()
  {
              //if not enforcing changes via event-dispatch thread or
              // if this is the event-dispatch thread then do it now:
    if(!enforceDispatchThreadFlag || SwingUtilities.isEventDispatchThread())
      dlgObj.requestFocus();
    else
    {         //do it later via event-dispatch thread:
      SwingUtilities.invokeLater(new Runnable()
          {
            public void run()
            {
              dlgObj.requestFocus();
            }
          });
    }
  }

  /**
   * Specifies whether this dialog should be modal.
   * @param modalFlag true for modal, false for modeless (allows other
   * windows to run).
   */
  public void setModal(boolean modalFlag)
  {
    dlgObj.setModal(modalFlag);
  }

  /**
   * Returns the modal status of the dialog.
   * @return true if modal, false if modeless (allows other windows
   * to run).
   */
  public boolean isModal()
  {
    return dlgObj.isModal();
  }

  /**
   * Sets the operation which will happen by default when the user
   * initiates a "close" on this dialog.  The possible choices are:
   * DO_NOTHING_ON_CLOSE - do not do anything - require the program to
   * handle the operation in the windowClosing method of a registered
   * WindowListener object; HIDE_ON_CLOSE - automatically hide the
   * dialog after invoking any registered WindowListener objects;
   * DISPOSE_ON_CLOSE - automatically hide and dispose the dialog
   * after invoking any registered WindowListener objects.   The
   * value is set to HIDE_ON_CLOSE by default.
   * @param selVal one of the following constants:  DISPOSE_ON_CLOSE,
   * DO_NOTHING_ON_CLOSE, or HIDE_ON_CLOSE.
   */
  public void setDefaultCloseOperation(int selVal)
  {
    dlgObj.setDefaultCloseOperation(selVal);
  }

  /**
   * Sets the foreground color of the dialog.
   * @param colorObj to color object to use.
   */
  public void setForeground(final Color colorObj)
  {
              //if not enforcing changes via event-dispatch thread or
              // if this is the event-dispatch thread then do it now:
    if(!enforceDispatchThreadFlag || SwingUtilities.isEventDispatchThread())
      dlgObj.setForeground(colorObj);
    else
    {         //do it later via event-dispatch thread:
      SwingUtilities.invokeLater(new Runnable()
          {
            public void run()
            {
              dlgObj.setForeground(colorObj);
            }
          });
    }
  }

  /**
   * Returns the foreground color of the dialog.
   * @return the foreground color object.
   */
  public Color getForeground()
  {
    return dlgObj.getForeground();
  }

  /**
   * Sets the background color of the dialog.
   * @param colorObj to color object to use.
   */
  public void setBackground(final Color colorObj)
  {
              //if not enforcing changes via event-dispatch thread or
              // if this is the event-dispatch thread then do it now:
    if(!enforceDispatchThreadFlag || SwingUtilities.isEventDispatchThread())
      dlgObj.setBackground(colorObj);
    else
    {         //do it later via event-dispatch thread:
      SwingUtilities.invokeLater(new Runnable()
          {
            public void run()
            {
              dlgObj.setBackground(colorObj);
            }
          });
    }
  }

  /**
   * Returns the background color of the dialog.
   * @return the background color object.
   */
  public Color getBackground()
  {
    return dlgObj.getBackground();
  }

  /**
   * Sets the message object displayed by this dialog.
   * @param obj the message obj.
   */
  public void setMessageObj(Object obj)
  {
    paneObj.setMessage(obj);
  }

  /**
   * Returns the message object displayed by this dialog.
   * @return The message obj.
   */
  public Object getMessageObj()
  {
    return paneObj.getMessage();
  }

  /**
   * Sets the message object displayed by this dialog to the given string.
   * @param str the message string.
   */
  public void setMessageStr(String str)
  {
    paneObj.setMessage(str);
  }

  /**
   * Returns the message string displayed by this dialog.
   * @return The message String object, or null if a String object
   * could not be retrieved.
   */
  public String getMessageStr()
  {
    final Object obj;
    return ((obj=paneObj.getMessage()) instanceof String) ?
                                                         (String)obj : null;
  }

  /**
   * Sets the title displayed by this dialog.
   * @param str the title string to display.
   */
  public void setTitleStr(final String str)
  {
              //if not enforcing changes via event-dispatch thread or
              // if this is the event-dispatch thread then do it now:
    if(!enforceDispatchThreadFlag || SwingUtilities.isEventDispatchThread())
      dlgObj.setTitle(str);
    else
    {         //do it later via event-dispatch thread:
      SwingUtilities.invokeLater(new Runnable()
          {
            public void run()
            {
              dlgObj.setTitle(str);
            }
          });
    }
  }

  /**
   * Returns the title displayed by this dialog.
   * @return The title string.
   */
  public String getTitleStr()
  {
    return dlgObj.getTitle();
  }

  /**
   * Adds the given "close" listener to the dialog.  The listener's
   * call-back method will be invoked when a dialog instance is closed,
   * hidden or disposed.  See the 'CloseListener' description for more
   * information.
   * @param listenerObj the "close" listener object to add.
   */
  public void addCloseListener(CloseListener listenerObj)
  {
    addWindowListener(listenerObj);
    addComponentListener(listenerObj);
  }

  /**
   * Removes the given "close" listener from the dialog.
   * @param listenerObj the "close" listener object to remove.
   */
  public void removeCloseListener(CloseListener listenerObj)
  {
    removeWindowListener(listenerObj);
    removeComponentListener(listenerObj);
  }

  /**
   * Adds the given window listener to the dialog.
   * @param listenerObj the window listener to add.
   */
  public void addWindowListener(WindowListener listenerObj)
  {
    dlgObj.addWindowListener(listenerObj);
  }

  /**
   * Removes the given window listener from the dialog.
   * @param listenerObj the window listener to remove.
   */
  public void removeWindowListener(WindowListener listenerObj)
  {
    dlgObj.removeWindowListener(listenerObj);
  }

  /**
   * Adds the given component listener to the dialog.
   * @param listenerObj the component listener to add.
   */
  public void addComponentListener(ComponentListener listenerObj)
  {
    dlgObj.addComponentListener(listenerObj);
  }

  /**
   * Removes the given component listener from the dialog.
   * @param listenerObj the component listener to remove.
   */
  public void removeComponentListener(ComponentListener listenerObj)
  {
    dlgObj.removeComponentListener(listenerObj);
  }

  /**
   * Returns the 'JDialog' object used by this object.
   * @return A 'JDialog' object.
   */
  public JDialog getDialogObj()
  {
    return dlgObj;
  }

  /**
   * Returns the 'JOptionPane' object used by this object.
   * @return A 'JOptionPane' object.
   */
  public JOptionPane getOptionPaneObj()
  {
    return paneObj;
  }

  /**
   * Returns the value the user has input.
   * @return The Object the user specified.
   */
  public Object getInputValue()
  {
    return paneObj.getInputValue();
  }

  /**
   * Returns the value the user has selected. UNINITIALIZED_VALUE implies
   * the user has not yet made a choice, null means the user closed the
   * window with out choosing anything. Otherwise the returned value will
   * be one of the options defined in this object.
   * @return The Object chosen by the user, UNINITIALIZED_VALUE if the
   * user has not yet made a choice, or null if the user closed the
   * window without making a choice.
   */
  public Object getValue()
  {
    return paneObj.getValue();
  }

  /**
   * Sets the value that will be returned by the 'getValue()' method.
   * Entering a new value will usually close the dialog.
   * @param obj the value object to set.
   */
  public void setOptionPaneValue(Object obj)
  {
    paneObj.setValue(obj);
  }

  /**
   * Sets a user-defined message string that may be fetched via the
   * 'getMessageString()' method.
   * @param str the message text.
   */
  public void setUserMessageString(String str)
  {
    messageString = str;
  }

  /**
   * Returns the user-defined message string set via the
   * 'setMessageString()' method.
   * @return The message text, or null if none has been set.
   */
  public String getUserMessageString()
  {
    return messageString;
  }

  /**
   * Sets the initial focus component for the dialog and requests that
   * it have focus.
   * @param compObj the component to use.
   *
   */
  public void setInitialFocusComponent(Component compObj)
  {
    this.initialFocusCompObj = compObj;
    setInitialFocus();
  }

  /**
   * Requests that the current "initial" component have focus.
   */
  public void setInitialFocus()
  {
    if (initialFocusCompObj != null)
    {
      SwingUtilities.invokeLater(new Runnable()
          {        //do it later via event-dispatch thread:
            public void run()
            {
              initialFocusCompObj.requestFocus();
            }
          });
    }
  }

  /**
   * Returns the specified component's toplevel <code>Frame</code> or
   * <code>Dialog</code>.
   * @param parentComponent the <code>Component</code> to check for a
   *		<code>Frame</code> or <code>Dialog</code>
   * @return the <code>Frame</code> or <code>Dialog</code> that
   *		contains the component, or the default
   *         	frame if the component is <code>null</code>,
   *		or does not have a valid
   *         	<code>Frame</code> or <code>Dialog</code> parent
   */
  public static Window getWindowForComponent(Component parentComponent)
  {
    if(parentComponent == null)
      return JOptionPane.getRootFrame();
    if(parentComponent instanceof Frame || parentComponent instanceof Dialog)
      return (Window)parentComponent;
    return getWindowForComponent(parentComponent.getParent());
  }


  /**
   * Creates and displays a popup dialog window.
   * @param parentComp the parent component for the popup.
   * @param msgObj the message object to be shown.
   * @param titleStr the title string for popup window.
   * @param optionType specifies what default option buttons to
   * automatically create if 'optionsArr' is null or empty; one of:
   * DEFAULT_OPTION, YES_NO_OPTION, YES_NO_CANCEL_OPTION, OK_CANCEL_OPTION
   * or NO_AUTOBUTTONS_OPTION.
   * @param msgType the type of message to be displayed (primarily used
   * to determine the icon from the pluggable Look and Feel):
   * ERROR_MESSAGE, INFORMATION_MESSAGE, WARNING_MESSAGE,
   * QUESTION_MESSAGE, or PLAIN_MESSAGE.
   * @param optionsArr an array of Objects that defines what buttons are
   * shown, or null for default or no buttons, based on 'optionType'.
   * @param initialValue the object that represents the default selection
   * for the dialog.
   * @param modalFlag true for modal, false for modeless (allows other
   * windows to run).
   * @param waitFlag if false and 'modalFlag' is false then this method
   * will not wait for the dialog window to be dismissed; otherwise this
   * method blocks until the dialog window is dismissed.
   * @return An integer indicating the option chosen by the user,
   * or CLOSED_OPTION if the user closed the dialog without selecting
   * an option or if the method returned before the dialog was dismissed
   * (running non-modal with 'waitFlag'==false).
   */
  public static int showOptionDialog(Component parentComp,Object msgObj,
            String titleStr,int optionType,int msgType,Object [] optionsArr,
                     Object initialValue,boolean modalFlag,boolean waitFlag)
  {
    int initValIdx = 0;      //find index of option matching 'initialValue':
    if(optionsArr != null && initialValue != null)
    {    //options array and initial-value objects given
      for(int i=0; i < optionsArr.length; ++i)
      {  //for each option
        if(optionsArr[i] != null && optionsArr[i].equals(initialValue))
        {     //option not null and option matched
          initValIdx = i;    //set index value
          break;             //exit loop
        }
      }
    }
    return showOptionDialog(parentComp,msgObj,titleStr,optionType,msgType,
                                  optionsArr,initValIdx,modalFlag,waitFlag);
  }

  /**
   * Creates and displays a popup dialog window.
   * @param parentComp the parent component for the popup.
   * @param msgObj the message object to be shown.
   * @param titleStr the title string for popup window.
   * @param optionType specifies what default option buttons to
   * automatically create if 'optionsArr' is null or empty; one of:
   * DEFAULT_OPTION, YES_NO_OPTION, YES_NO_CANCEL_OPTION, OK_CANCEL_OPTION
   * or NO_AUTOBUTTONS_OPTION.
   * @param msgType the type of message to be displayed (primarily used
   * to determine the icon from the pluggable Look and Feel):
   * ERROR_MESSAGE, INFORMATION_MESSAGE, WARNING_MESSAGE,
   * QUESTION_MESSAGE, or PLAIN_MESSAGE.
   * @param optionsArr an array of Objects that defines what buttons are
   * shown, or null for default or no buttons, based on 'optionType'.
   * @param initValIdx the index of the 'optionsArr[]' object
   * corresponding to the option to be selected by default.
   * @param modalFlag true for modal, false for modeless (allows other
   * windows to run).
   * @param waitFlag if false and 'modalFlag' is false then this method
   * will not wait for the dialog window to be dismissed; otherwise this
   * method blocks until the dialog window is dismissed.
   * @return An integer indicating the option chosen by the user,
   * or CLOSED_OPTION if the user closed the dialog without selecting
   * an option or if the method returned before the dialog was dismissed
   * (running non-modal with 'waitFlag'==false).
   */
  public static int showOptionDialog(Component parentComp,Object msgObj,
            String titleStr,int optionType,int msgType,Object [] optionsArr,
                          int initValIdx,boolean modalFlag,boolean waitFlag)
  {
         //create dialog popup object:
    final IstiDialogPopup popupObj = new IstiDialogPopup(parentComp,msgObj,
                                     titleStr,msgType,optionsArr,initValIdx,
                                                modalFlag,false,optionType);
    if(!modalFlag && !waitFlag)
    {    //non-modal and not waiting
      popupObj.show();            //make dialog visible
      return CLOSED_OPTION;       //return value
    }
    return popupObj.showAndReturnIndex();
  }

  /**
   * Creates and displays a modal popup dialog window.  This method blocks
   * until the dialog window is dismissed.
   * @param parentComp the parent component for the popup.
   * @param msgObj the message object to be shown.
   * @param titleStr the title string for popup window.
   * @param optionType specifies what default option buttons to
   * automatically create if 'optionsArr' is null or empty; one of:
   * DEFAULT_OPTION, YES_NO_OPTION, YES_NO_CANCEL_OPTION, OK_CANCEL_OPTION
   * or NO_AUTOBUTTONS_OPTION.
   * @param msgType the type of message to be displayed (primarily used
   * to determine the icon from the pluggable Look and Feel):
   * ERROR_MESSAGE, INFORMATION_MESSAGE, WARNING_MESSAGE,
   * QUESTION_MESSAGE, or PLAIN_MESSAGE.
   * @param optionsArr an array of Objects that defines what buttons are
   * shown, or null for default or no buttons, based on 'optionType'.
   * @param initialValue the object that represents the default selection
   * for the dialog.
   * @return An integer indicating the option chosen by the user,
   * or CLOSED_OPTION if the user closed the dialog without selecting
   * an option.
   */
  public static int showOptionDialog(Component parentComp,Object msgObj,
                                 String titleStr,int optionType,int msgType,
                                   Object [] optionsArr,Object initialValue)
  {
    return showOptionDialog(parentComp,msgObj,titleStr,optionType,msgType,
                                         optionsArr,initialValue,true,true);
  }

  /**
   * Creates and displays a modal popup dialog window.  This method blocks
   * until the dialog window is dismissed.
   * @param parentComp the parent component for the popup.
   * @param msgObj the message object to be shown.
   * @param titleStr the title string for popup window.
   * @param optionType specifies what default option buttons to
   * automatically create if 'optionsArr' is null or empty; one of:
   * DEFAULT_OPTION, YES_NO_OPTION, YES_NO_CANCEL_OPTION, OK_CANCEL_OPTION
   * or NO_AUTOBUTTONS_OPTION.
   * @param msgType the type of message to be displayed (primarily used
   * to determine the icon from the pluggable Look and Feel):
   * ERROR_MESSAGE, INFORMATION_MESSAGE, WARNING_MESSAGE,
   * QUESTION_MESSAGE, or PLAIN_MESSAGE.
   * @param optionsArr an array of Objects that defines what buttons are
   * shown, or null for default or no buttons, based on 'optionType'.
   * @param initValIdx the index of the 'optionsArr[]' object
   * corresponding to the option to be selected by default.
   * @return An integer indicating the option chosen by the user,
   * or CLOSED_OPTION if the user closed the dialog without selecting
   * an option.
   */
  public static int showOptionDialog(Component parentComp,Object msgObj,
                                 String titleStr,int optionType,int msgType,
                                        Object [] optionsArr,int initValIdx)
  {
    return showOptionDialog(parentComp,msgObj,titleStr,optionType,msgType,
                                           optionsArr,initValIdx,true,true);
  }

  /**
   * Creates and displays a popup dialog window with default buttons
   * based on the value of 'optionType'.  This method blocks until the
   * dialog window is dismissed (even if 'modalFlag' is false).
   * @param parentComp the parent component for the popup.
   * @param msgObj the message object to be shown.
   * @param titleStr the title string for popup window.
   * @param optionType specifies what default option buttons to
   * automatically create if 'optionsArr' is null or empty; one of:
   * DEFAULT_OPTION, YES_NO_OPTION, YES_NO_CANCEL_OPTION, OK_CANCEL_OPTION
   * or NO_AUTOBUTTONS_OPTION.
   * @param msgType the type of message to be displayed (primarily used
   * to determine the icon from the pluggable Look and Feel):
   * ERROR_MESSAGE, INFORMATION_MESSAGE, WARNING_MESSAGE,
   * QUESTION_MESSAGE, or PLAIN_MESSAGE.
   * @param modalFlag true for modal, false for modeless (allows other
   * windows to run).
   * @param waitFlag if false and 'modalFlag' is false then this method
   * will not wait for the dialog window to be dismissed; otherwise this
   * method blocks until the dialog window is dismissed.
   * @return An integer indicating the option chosen by the user,
   * or CLOSED_OPTION if the user closed the dialog without selecting
   * an option or if the method returned before the dialog was dismissed
   * (running non-modal with 'waitFlag'==false).
   */
  public static int showOptionDialog(Component parentComp,Object msgObj,
                                 String titleStr,int optionType,int msgType,
                                         boolean modalFlag,boolean waitFlag)
  {
    return showOptionDialog(parentComp,msgObj,titleStr,optionType,msgType,
                                                 null,0,modalFlag,waitFlag);
  }

  /**
   * Creates and displays a modal popup dialog window with default buttons
   * based on the value of 'optionType'.  This method blocks until the
   * dialog window is dismissed.
   * @param parentComp the parent component for the popup.
   * @param msgObj the message object to be shown.
   * @param titleStr the title string for popup window.
   * @param optionType specifies what default option buttons to
   * automatically create if 'optionsArr' is null or empty; one of:
   * DEFAULT_OPTION, YES_NO_OPTION, YES_NO_CANCEL_OPTION, OK_CANCEL_OPTION
   * or NO_AUTOBUTTONS_OPTION.
   * @param msgType the type of message to be displayed (primarily used
   * to determine the icon from the pluggable Look and Feel):
   * ERROR_MESSAGE, INFORMATION_MESSAGE, WARNING_MESSAGE,
   * QUESTION_MESSAGE, or PLAIN_MESSAGE.
   * @return An integer indicating the option chosen by the user,
   * or CLOSED_OPTION if the user closed the dialog without selecting
   * an option.
   */
  public static int showOptionDialog(Component parentComp,Object msgObj,
                                 String titleStr,int optionType,int msgType)
  {
    return showOptionDialog(parentComp,msgObj,titleStr,optionType,msgType,
                                                          null,0,true,true);
  }

  /**
   * Creates and displays a plain-message popup dialog window with default
   * buttons based on the value of 'optionType'.  This method blocks until
   * the dialog window is dismissed (even if 'modalFlag' is false).
   * @param parentComp the parent component for the popup.
   * @param msgObj the message object to be shown.
   * @param titleStr the title string for popup window.
   * @param optionType specifies what default option buttons to
   * automatically create if 'optionsArr' is null or empty; one of:
   * DEFAULT_OPTION, YES_NO_OPTION, YES_NO_CANCEL_OPTION, OK_CANCEL_OPTION
   * or NO_AUTOBUTTONS_OPTION.
   * @param modalFlag true for modal, false for modeless (allows other
   * windows to run).
   * @param waitFlag if false and 'modalFlag' is false then this method
   * will not wait for the dialog window to be dismissed; otherwise this
   * method blocks until the dialog window is dismissed.
   * @return An integer indicating the option chosen by the user,
   * or CLOSED_OPTION if the user closed the dialog without selecting
   * an option or if the method returned before the dialog was dismissed
   * (running non-modal with 'waitFlag'==false).
   */
  public static int showOptionDialog(Component parentComp,Object msgObj,
          String titleStr,int optionType,boolean modalFlag,boolean waitFlag)
  {
    return showOptionDialog(parentComp,msgObj,titleStr,optionType,
                                   PLAIN_MESSAGE,null,0,modalFlag,waitFlag);
  }

  /**
   * Creates and displays a plain-message modal popup dialog window with
   * default buttons based on the value of 'optionType'.  This method
   * blocks until the dialog window is dismissed.
   * @param parentComp the parent component for the popup.
   * @param msgObj the message object to be shown.
   * @param titleStr the title string for popup window.
   * @param optionType specifies what default option buttons to
   * automatically create if 'optionsArr' is null or empty; one of:
   * DEFAULT_OPTION, YES_NO_OPTION, YES_NO_CANCEL_OPTION, OK_CANCEL_OPTION
   * or NO_AUTOBUTTONS_OPTION.
   * @return An integer indicating the option chosen by the user,
   * or CLOSED_OPTION if the user closed the dialog without selecting
   * an option.
   */
  public static int showOptionDialog(Component parentComp,Object msgObj,
                                             String titleStr,int optionType)
  {
    return showOptionDialog(parentComp,msgObj,titleStr,optionType,
                                            PLAIN_MESSAGE,null,0,true,true);
  }

  /**
   * Creates and displays a information-message popup dialog window with
   * a default "OK" button.
   * @param parentComp the parent component for the popup.
   * @param msgObj the message object to be shown.
   * @param titleStr the title string for popup window.
   * @param modalFlag true for modal, false for modeless (allows other
   * windows to run).
   * @param waitFlag if false and 'modalFlag' is false then this method
   * will not wait for the dialog window to be dismissed; otherwise this
   * method blocks until the dialog window is dismissed.
   * @return The value 'JOptionPane.OK_OPTION' if the "OK" button was
   * selected, or CLOSED_OPTION if the user closed the dialog without
   * selecting the button or if the method returned before the dialog
   * was dismissed (running non-modal with 'waitFlag'==false).
   */
  public static int showMessageDialog(Component parentComp,Object msgObj,
                         String titleStr,boolean modalFlag,boolean waitFlag)
  {
    return showOptionDialog(parentComp,msgObj,titleStr,DEFAULT_OPTION,
                                    INFORMATION_MESSAGE,modalFlag,waitFlag);
  }

  /**
   * Creates and displays a modal information-message popup dialog window
   * with a default "OK" button.  This method blocks until the dialog
   * window is dismissed.
   * @param parentComp the parent component for the popup.
   * @param msgObj the message object to be shown.
   * @param titleStr the title string for popup window.
   * @return The value 'JOptionPane.OK_OPTION' if the "OK" button was
   * selected, or CLOSED_OPTION if the user closed the dialog without
   * selecting the button.
   */
  public static int showMessageDialog(Component parentComp,Object msgObj,
                                                            String titleStr)
  {
    return showMessageDialog(parentComp,msgObj,titleStr,true,true);
  }


  /**
   * Class CloseListener defines a call-back method that is to be invoked
   * when a dialog instance is closed, hidden or disposed.  This class
   * implements both the 'WindowListener' and 'ComponentListener'
   * interfaces, and as such may be added to a dialog window via both
   * the 'addWindowListener()' and 'addComponentListener()' methods.
   * When the dialog is closed, hidden or disposed, the user-defined
   * 'dialogClosed()' method is invoked.  If any of the "window...()"
   * or "component...()" methods of this class are overridden, the
   * original method should still be called.
   *
   * The purpose of this class is to make it easier to attach a dialog
   * listener that is reliably invoked when the dialog is dismissed.
   * Various versions of Java (1.3 vs. 1.4) use 'hide()' and 'dispose()'
   * in different ways in response to how the dialog is dismissed, and
   * the methods call different listeners (and combinations of listeners)
   * depending on which method is invoked.  This class allows both types
   * of listeners to be easily monitored.
   */
  public static abstract class CloseListener implements WindowListener,
                                                           ComponentListener
  {
         /** Flag set true after closed and false after opened. */
    protected boolean closeMethodInvokedFlag = false;

    /**
     * This method is invoked when the dialog is closed, hidden or disposed.
     * This method should be overridden by the user.
     * @param evtObj the source event object, either a 'WindowEvent' or
     * a 'ComponentEvent'.
     */
    public abstract void dialogClosed(Object evtObj);

    /**
     * Invoked the first time a window is made visible.
     * This method is part of the 'WindowListener' implementation.
     * @param evtObj the window-event object.
     */
    public void windowOpened(WindowEvent evtObj)
    {
      closeMethodInvokedFlag = false;       //clear "invoked" flag
    }

    /**
     * Invoked when the user attempts to close the window
     * from the window's system menu.  If the program does not
     * explicitly hide or dispose the window while processing
     * this event, the window close operation will be cancelled.
     * This method is part of the 'WindowListener' implementation.
     * @param evtObj the window-event object.
     */
    public void windowClosing(WindowEvent evtObj)
    {
    }

    /**
     * Invoked when a window has been closed as the result
     * of calling dispose on the window.
     * This method is part of the 'WindowListener' implementation.
     * @param evtObj the window-event object.
     */
    public void windowClosed(WindowEvent evtObj)
    {
      if(!closeMethodInvokedFlag)
      {  //close method not yet invoked
        closeMethodInvokedFlag = true;      //set "invoked" flag
        dialogClosed(evtObj);               //invoke close method
      }
    }

    /**
     * Invoked when a window is changed from a normal to a
     * minimized state. For many platforms, a minimized window
     * is displayed as the icon specified in the window's
     * iconImage property.
     * This method is part of the 'WindowListener' implementation.
     * @param evtObj the window-event object.
     */
    public void windowIconified(WindowEvent evtObj)
    {
    }

    /**
     * Invoked when a window is changed from a minimized
     * to a normal state.
     * This method is part of the 'WindowListener' implementation.
     * @param evtObj the window-event object.
     */
    public void windowDeiconified(WindowEvent evtObj)
    {
    }

    /**
     * Invoked when the window is set to be the user's
     * active window, which means the window (or one of its
     * subcomponents) will receive keyboard events.
     * This method is part of the 'WindowListener' implementation.
     * @param evtObj the window-event object.
     */
    public void windowActivated(WindowEvent evtObj)
    {
    }

    /**
     * Invoked when a window is no longer the user's active
     * window, which means that keyboard events will no longer
     * be delivered to the window or its subcomponents.
     * This method is part of the 'WindowListener' implementation.
     * @param evtObj the window-event object.
     */
    public void windowDeactivated(WindowEvent evtObj)
    {
    }

    /**
     * Invoked when the component's size changes.
     * This method is part of the 'ComponentListener' implementation.
     * @param evtObj the component-event object.
     */
    public void componentResized(ComponentEvent evtObj)
    {
    }

    /**
     * Invoked when the component's position changes.
     * This method is part of the 'ComponentListener' implementation.
     * @param evtObj the component-event object.
     */
    public void componentMoved(ComponentEvent evtObj)
    {
    }

    /**
     * Invoked when the component has been made visible.
     * This method is part of the 'ComponentListener' implementation.
     * @param evtObj the component-event object.
     */
    public void componentShown(ComponentEvent evtObj)
    {
      closeMethodInvokedFlag = false;       //clear "invoked" flag
    }

    /**
     * Invoked when the component has been made invisible.
     * This method is part of the 'ComponentListener' implementation.
     * @param evtObj the component-event object.
     */
    public void componentHidden(ComponentEvent evtObj)
    {
      if(!closeMethodInvokedFlag)
      {  //close method not yet invoked
        closeMethodInvokedFlag = true;      //set "invoked" flag
        dialogClosed(evtObj);               //invoke close method
      }
    }
  }
}
