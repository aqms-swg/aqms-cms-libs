//IstiDialogUtil.java:  Provides simplified methods for popup-message-
//                      dialog creation.
//
//  3/17/2004 -- [ET]  Initial version.
//  4/26/2004 -- [ET]  Modified to use a 'MultiLineJLabel' object to
//                     display multi-line message text; added a
//                     'normalizeFlag' parameter option to the
//                     'popupErrorMessage()' method.
//  7/16/2004 -- [ET]  Added 'sizeViaNewlinesFlag' option.
//  9/24/2004 -- [ET]  Added optional 'waitFlag' parameter to
//                     'popupMessage()' method.
//  1/19/2022 -- [KF]  Added 'popupOptionsMessage' method.
//

package com.isti.util.gui;

import java.awt.Component;
import javax.swing.JLabel;
import com.isti.util.LogFile;
import com.isti.util.UtilFns;

/**
 * Class IstiDialogUtil provides simplified methods for
 * popup-message-dialog creation.
 */
public class IstiDialogUtil
{
    /** Default maximum length for popup message text. */
  protected static final int DEF_MAX_JLABEL_LEN = 160;
    /** Default use-JLabel length for popup message text. */
  protected static final int DEF_USE_JLABEL_LEN = 80;
  protected static final String ERROR_STR = "Error";
  protected final Component parentComponent;
  protected final int popupLengthValue;
  protected final boolean useJLabelFlag;
  protected final int useJLabelLength;
  protected boolean sizeViaNewlinesFlag = false;
    /** Thread-synchronization object for popup-message dialogs. */
  protected Object popupMessageSyncObj = new Object();

  /**
   * Creates a dialog-utility object.
   * @param parentComp the parent component for the generated popups.
   * @param popupLength if 'useJLabelFlag'==false then this defines the
   * preferred-size width (in character columns), or 0 for a default
   * width; if 'useJLabelFlag'==true then this defines the maximum
   * allowed length for the popup text, or 0 for a default maximum
   * length (when the popup text is beyond the maximum length it will
   * be truncated and displayed with a trailing "..." string).
   * @param useJLabelFlag true to use a 'JLabel' object to display
   * message text; false to use a 'MultiLineJLabel' object.
   * @param useJLabelLength if the message text is less than or equal
   * to this value then a 'JLabel' object will always be used.
   */
  public IstiDialogUtil(Component parentComp, int popupLength,
                                    boolean useJLabelFlag, int useJLabelLength)
  {
    parentComponent = parentComp;
    this.useJLabelFlag = useJLabelFlag;
    this.useJLabelLength = useJLabelLength;
         //if 'useJLabelFlag'==false then always use the given value;
         // if 'useJLabelFlag'==true then substitute default for zero:
    popupLengthValue = (!useJLabelFlag || popupLength > 0) ?
                                           popupLength : DEF_MAX_JLABEL_LEN;
  }

  /**
   * Creates a dialog-utility object.
   * @param parentComp the parent component for the generated popups.
   * @param popupLength if 'useJLabelFlag'==false then this defines the
   * preferred-size width (in character columns), or 0 for a default
   * width; if 'useJLabelFlag'==true then this defines the maximum
   * allowed length for the popup text, or 0 for a default maximum
   * length (when the popup text is beyond the maximum length it will
   * be truncated and displayed with a trailing "..." string).
   * @param useJLabelFlag true to use a 'JLabel' object to display
   * message text; false to use a 'MultiLineJLabel' object.
   */
  public IstiDialogUtil(Component parentComp, int popupLength,
                                                      boolean useJLabelFlag)
  {
    this(parentComp,popupLength,false,DEF_USE_JLABEL_LEN);
  }

  /**
   * Creates a dialog-utility object.
   * @param parentComp the parent component for the generated popups.
   * @param popupLength the preferred-size width (in character columns),
   * or 0 for a default width.
   */
  public IstiDialogUtil(Component parentComp, int popupLength)
  {
    this(parentComp,popupLength,false,DEF_USE_JLABEL_LEN);
  }

  /**
   * Creates a dialog-utility object.
   * @param parentComp the parent component for the generated popups.
   * @param useJLabelFlag true to use a 'JLabel' object to display
   * message text; false to use a 'MultiLineJLabel' object.
   */
  public IstiDialogUtil(Component parentComp, boolean useJLabelFlag)
  {
    this(parentComp,0,useJLabelFlag,DEF_USE_JLABEL_LEN);
  }

  /**
   * Creates a dialog-utility object.
   * @param parentComp the parent component for the generated popups.
   */
  public IstiDialogUtil(Component parentComp)
  {
    this(parentComp,0,false,DEF_USE_JLABEL_LEN);
  }

  /**
   * Configures whether or not the dialog may be horizontally sized
   * using the newline characters in the message text.
   * @param flgVal if true then when the message text contains newline
   * characters the width of the dialog will be determined by the width
   * of the longest "line" in the text.
   */
  public void setSizeViaNewlinesFlag(boolean flgVal)
  {
    sizeViaNewlinesFlag = flgVal;
  }

  /**
   * Returns a flag indicating whether or not the dialog may be
   * horizontally sized using the newline characters in the message text.
   * @return true if, when the message text contains newline
   * characters, the width of the dialog will be determined by the width
   * of the longest "line" in the text.
   */
  public boolean getSizeViaNewlinesFlag()
  {
    return sizeViaNewlinesFlag;
  }

  /**
   * Displays a message in a popup window with an "OK" button.
   * @param msgStr the error message text to display.
   * @param titleStr the window title to use.
   * @param messageType message type code.
   * @param modalFlag true for a modal dialog, false for non-modal
   * (allows other windows to run).
   * @param waitFlag true to have this method block until the dialog is
   * dismissed (even if non-modal); false to have this method return
   * immediately (if non-modal).
   */
  public void popupMessage(String msgStr, String titleStr, int messageType,
                                        boolean modalFlag, boolean waitFlag)
  {
    if(msgStr != null)
    {    //message string OK
      synchronized(popupMessageSyncObj)
      {  //only allow one thread at a time to attempt popup
        final IstiDialogPopup popupObj = new IstiDialogPopup(
                              parentComponent,createMsgObj(msgStr),titleStr,
                      messageType,modalFlag,IstiDialogPopup.DEFAULT_OPTION);
        popupObj.setRepackNeededFlag(true); //setup to repack after shown
        if(waitFlag)
          popupObj.showAndWait();
        else
          popupObj.show();
      }
    }
  }

  /**
   * Displays a message in a popup window with an "OK" button.
   * @param msgStr the error message text to display.
   * @param titleStr the window title to use.
   * @param messageType message type code.
   * @param modalFlag true for a modal dialog, false for non-modal (in
   * which case this method does not wait for the popup window to be
   * dismissed).
   */
  public void popupMessage(String msgStr, String titleStr, int messageType,
                                                          boolean modalFlag)
  {
    popupMessage(msgStr,titleStr,messageType,modalFlag,false);
  }

  /**
   * Displays an error message in a popup window with an "OK" button.
   * @param logObj the log file to which the message text should also
   * be delivered (with a log level of "warning"), or null for none.
   * @param msgStr the error message text to display.
   * @param titleStr the window title to use.
   * @param modalFlag true for a modal dialog, false for non-modal (in
   * which case this method does not wait for the popup window to be
   * dismissed).
   * @param normalizeFlag true to remove extra whitespace from the
   * message text before showing it in the dialog; false to leave the
   * text unchanged.
   */
  public void popupErrorMessage(LogFile logObj, String msgStr,
                  String titleStr, boolean modalFlag, boolean normalizeFlag)
  {
    if(logObj != null)
      logObj.warning(msgStr);
    popupMessage((normalizeFlag?UtilFns.normalizeString(msgStr):msgStr),
                          titleStr,IstiDialogPopup.ERROR_MESSAGE,modalFlag);
  }

  /**
   * Displays an error message in a popup window with an "OK" button.
   * @param logObj the log file to which the message text should also
   * be delivered (with a log level of "warning"), or null for none.
   * @param msgStr the error message text to display.
   * @param titleStr the window title to use.
   * @param modalFlag true for a modal dialog, false for non-modal (in
   * which case this method does not wait for the popup window to be
   * dismissed).
   */
  public void popupErrorMessage(LogFile logObj, String msgStr,
                                         String titleStr, boolean modalFlag)
  {
    popupErrorMessage(logObj,msgStr,titleStr,modalFlag,false);
  }

  /**
   * Displays an error message in a popup window with an "OK" button.
   * @param msgStr the error message text to display.
   * @param titleStr the window title to use.
   * @param modalFlag true for a modal dialog, false for non-modal (in
   * which case this method does not wait for the popup window to be
   * dismissed).
   * @param normalizeFlag true to remove extra whitespace from the
   * message text before showing it in the dialog; false to leave the
   * text unchanged.
   */
  public void popupErrorMessage(String msgStr, String titleStr,
                                   boolean modalFlag, boolean normalizeFlag)
  {
    popupErrorMessage(null,msgStr,titleStr,modalFlag,normalizeFlag);
  }

  /**
   * Displays an error message in a popup window with an "OK" button.
   * @param msgStr the error message text to display.
   * @param titleStr the window title to use.
   * @param modalFlag true for a modal dialog, false for non-modal (in
   * which case this method does not wait for the popup window to be
   * dismissed).
   */
  public void popupErrorMessage(String msgStr, String titleStr,
                                                          boolean modalFlag)
  {
    popupErrorMessage(null,msgStr,titleStr,modalFlag,false);
  }

  /**
   * Displays an error message in a modal popup window with an "OK" button.
   * @param logObj the log file to which the message text should also
   * be delivered (with a log level of "warning"), or null for none.
   * @param msgStr the error message text to display.
   * @param titleStr the window title to use.
   */
  public void popupErrorMessage(LogFile logObj, String msgStr,
                                                            String titleStr)
  {
    popupErrorMessage(logObj,msgStr,titleStr,true,false);
  }

  /**
   * Displays an error message in a modal popup window with an "OK" button.
   * @param msgStr the error message text to display.
   * @param titleStr the window title to use.
   */
  public void popupErrorMessage(String msgStr, String titleStr)
  {
    popupErrorMessage(null,msgStr,titleStr,true,false);
  }

  /**
   * Displays an error message in a popup window with an "OK" button.
   * @param logObj the log file to which the message text should also
   * be delivered (with a log level of "warning"), or null for none.
   * @param msgStr the error message text to display.
   * @param modalFlag true for a modal dialog, false for non-modal (in
   * which case this method does not wait for the popup window to be
   * dismissed).
   * @param normalizeFlag true to remove extra whitespace from the
   * message text before showing it in the dialog; false to leave the
   * text unchanged.
   */
  public void popupErrorMessage(LogFile logObj, String msgStr,
                                   boolean modalFlag, boolean normalizeFlag)
  {
    popupErrorMessage(logObj,msgStr,ERROR_STR,modalFlag,normalizeFlag);
  }

  /**
   * Displays an error message in a popup window with an "OK" button.
   * @param logObj the log file to which the message text should also
   * be delivered (with a log level of "warning"), or null for none.
   * @param msgStr the error message text to display.
   * @param modalFlag true for a modal dialog, false for non-modal (in
   * which case this method does not wait for the popup window to be
   * dismissed).
   */
  public void popupErrorMessage(LogFile logObj, String msgStr,
                                                          boolean modalFlag)
  {
    popupErrorMessage(logObj,msgStr,ERROR_STR,modalFlag,false);
  }

  /**
   * Displays an error message in a popup window with an "OK" button.
   * @param msgStr the error message text to display.
   * @param modalFlag true for a modal dialog, false for non-modal (in
   * which case this method does not wait for the popup window to be
   * dismissed).
   * @param normalizeFlag true to remove extra whitespace from the
   * message text before showing it in the dialog; false to leave the
   * text unchanged.
   */
  public void popupErrorMessage(String msgStr, boolean modalFlag,
                                                      boolean normalizeFlag)
  {
    popupErrorMessage(null,msgStr,ERROR_STR,modalFlag,normalizeFlag);
  }

  /**
   * Displays an error message in a popup window with an "OK" button.
   * @param msgStr the error message text to display.
   * @param modalFlag true for a modal dialog, false for non-modal (in
   * which case this method does not wait for the popup window to be
   * dismissed).
   */
  public void popupErrorMessage(String msgStr, boolean modalFlag)
  {
    popupErrorMessage(null,msgStr,ERROR_STR,modalFlag,false);
  }

  /**
   * Displays an error message in a modal popup window with an "OK" button.
   * @param logObj the log file to which the message text should also
   * be delivered (with a log level of "warning"), or null for none.
   * @param msgStr the error message text to display.
   */
  public void popupErrorMessage(LogFile logObj, String msgStr)
  {
    popupErrorMessage(logObj,msgStr,ERROR_STR,true,false);
  }

  /**
   * Displays an error message in a modal popup window with an "OK" button.
   * @param msgStr the error message text to display.
   */
  public void popupErrorMessage(String msgStr)
  {
    popupErrorMessage(null,msgStr,ERROR_STR,true,false);
  }

  /**
   * Displays an informational message in a modal popup window with an
   * "OK" button.
   * @param msgStr the error message text to display.
   * @param modalFlag true for a modal dialog, false for non-modal (in
   * which case this method does not wait for the popup window to be
   * dismissed).
   */
  public void popupInfoMessage(String msgStr, boolean modalFlag)
  {
    popupMessage(msgStr,"Information",IstiDialogPopup.INFORMATION_MESSAGE,
                                                                 modalFlag);
  }

  /**
   * Displays an informational message in a modal popup window with an
   * "OK" button.
   * @param msgStr the error message text to display.
   */
  public void popupInfoMessage(String msgStr)
  {
    popupMessage(msgStr,"Information",IstiDialogPopup.INFORMATION_MESSAGE,
                                                                      true);
  }

  /**
   * Displays a user-confirmation popup dialog window with "OK" and
   * "Cancel" buttons.  This method blocks until the user selects an
   * option button or closes the dialog window (even if 'modalFlag'==false).
   * @param msgStr the message text to display.
   * @param titleStr the window title to use.
   * @param messageType the type of message to be displayed (primarily used
   * to determine the icon from the pluggable Look and Feel):
   * ERROR_MESSAGE, INFORMATION_MESSAGE, WARNING_MESSAGE,
   * QUESTION_MESSAGE, or PLAIN_MESSAGE.
   * @param modalFlag true for a modal dialog, false for non-modal.
   * @return An integer indicating the option selected by the user, or
   * IstiDialogPopup.CLOSED_OPTION if the user closed the dialog without
   * selecting an option.
   */
  public int popupOKConfirmMessage(String msgStr, String titleStr,
                                         int messageType, boolean modalFlag)
  {
    if(msgStr != null)
    {    //message string OK
      synchronized(popupMessageSyncObj)
      {  //only allow one thread at a time to attempt popup
                   //show popup, return selection:
        final IstiDialogPopup popupObj = new IstiDialogPopup(
                              parentComponent,createMsgObj(msgStr),titleStr,
                      messageType,modalFlag,IstiDialogPopup.OK_CANCEL_OPTION);
        popupObj.setRepackNeededFlag(true); //setup to repack after shown
        return popupObj.showAndReturnIndex();
      }
    }
    return IstiDialogPopup.CLOSED_OPTION;
  }

  /**
   * Displays a user-confirmation popup dialog window with "OK" and
   * "Cancel" buttons.  This method blocks until the user selects an
   * option button or closes the dialog window (even if 'modalFlag'==false).
   * @param msgStr the message text to display.
   * @param titleStr the window title to use.
   * @param modalFlag true for a modal dialog, false for non-modal.
   * @return An integer indicating the option selected by the user:
   * IstiDialogPopup.OK_OPTION, IstiDialogPopup.CANCEL_OPTION, or
   * IstiDialogPopup.CLOSED_OPTION if the user closed the dialog without
   * selecting an option.
   */
  public int popupOKConfirmMessage(String msgStr, String titleStr,
                                                          boolean modalFlag)
  {
    return popupOKConfirmMessage(msgStr,titleStr,
                                   IstiDialogPopup.PLAIN_MESSAGE,modalFlag);
  }
  
  /**
   * Displays a user options dialog window.
   * 
   * @param msgStr     the message text to display.
   * @param titleStr   the window title to use.
   * @param optionsArr an array of Objects that defines what buttons are shown.
   * @param initValIdx the index of the 'optionsArr[]' object
   * @param modalFlag  true for a modal dialog, false for non-modal.
   * @return An integer indicating the option chosen by the user, or
   *         CLOSED_OPTION if the user closed the dialog without selecting an
   *         option.
   */
  public int popupOptionsMessage(String msgStr, String titleStr, Object[] optionsArr,
      int initValIdx, boolean modalFlag)
  {
    if(msgStr != null)
    {    //message string OK
      synchronized(popupMessageSyncObj)
      {  //only allow one thread at a time to attempt popup
                   //show popup, return selection:
        final IstiDialogPopup popupObj = new IstiDialogPopup(parentComponent,
            msgStr, titleStr, optionsArr, initValIdx, modalFlag);
        popupObj.setRepackNeededFlag(true); //setup to repack after shown
        return popupObj.showAndReturnIndex();
      }
    }
    return IstiDialogPopup.CLOSED_OPTION;
  }

  /**
   * Displays a user-confirmation popup dialog window with "Yes" and
   * "No" buttons.  This method blocks until the user selects an
   * option button or closes the dialog window (even if 'modalFlag'==false).
   * @param msgStr the message text to display.
   * @param titleStr the window title to use.
   * @param messageType the type of message to be displayed (primarily used
   * to determine the icon from the pluggable Look and Feel):
   * ERROR_MESSAGE, INFORMATION_MESSAGE, WARNING_MESSAGE,
   * QUESTION_MESSAGE, or PLAIN_MESSAGE.
   * @param modalFlag true for a modal dialog, false for non-modal.
   * @return An integer indicating the option selected by the user:
   * IstiDialogPopup.YES_OPTION, IstiDialogPopup.NO_OPTION, or
   * IstiDialogPopup.CLOSED_OPTION if the user closed the dialog without
   * selecting an option.
   */
  public int popupYesNoConfirmMessage(String msgStr, String titleStr,
                                         int messageType, boolean modalFlag)
  {
    if(msgStr != null)
    {    //message string OK
      synchronized(popupMessageSyncObj)
      {  //only allow one thread at a time to attempt popup
                   //show popup, return selection:
        final IstiDialogPopup popupObj = new IstiDialogPopup(
                              parentComponent,createMsgObj(msgStr),titleStr,
                       messageType,modalFlag,IstiDialogPopup.YES_NO_OPTION);
        popupObj.setRepackNeededFlag(true); //setup to repack after shown
        return popupObj.showAndReturnIndex();
      }
    }
    return IstiDialogPopup.CLOSED_OPTION;
  }

  /**
   * Displays a user-confirmation popup dialog window with "Yes" and
   * "No" buttons.  This method blocks until the user selects an
   * option button or closes the dialog window (even if 'modalFlag'==false).
   * @param msgStr the message text to display.
   * @param titleStr the window title to use.
   * @param modalFlag true for a modal dialog, false for non-modal.
   * @return An integer indicating the option selected by the user:
   * IstiDialogPopup.YES_OPTION, IstiDialogPopup.NO_OPTION, or
   * IstiDialogPopup.CLOSED_OPTION if the user closed the dialog without
   * selecting an option.
   */
  public int popupYesNoConfirmMessage(String msgStr, String titleStr,
                                                          boolean modalFlag)
  {
    return popupYesNoConfirmMessage(msgStr,titleStr,
                                   IstiDialogPopup.PLAIN_MESSAGE,modalFlag);
  }

  /**
   * Creates a message object using the given string.
   * @param msgStr the message string to use.
   * @return A 'MultiLineJLabel' or 'JLabel' object constructed using the
   * given string.
   */
  public Object createMsgObj(String msgStr)
  {
    if(msgStr != null)
    {    //message string OK
      final Object msgObj;
      if(useJLabelFlag || msgStr.length() <= useJLabelLength)
      {  //using a 'JLabel' object for message text; if too long then trim
        if(popupLengthValue > 0 && msgStr.length() > popupLengthValue)
          msgStr = msgStr.substring(0,popupLengthValue).trim() + "...";
        msgObj = new JLabel(msgStr);   //set message object
      }
      else         //if size-via-newlines allowed and newline character(s)
      {            // found then use JLabel object:
        if(sizeViaNewlinesFlag && msgStr.indexOf('\n') >= 0)
          msgObj = msgStr;        //use the message string itself
        else       //not sizing via newlines; use multi-line object:
          msgObj = new MultiLineJLabel(msgStr,popupLengthValue);
      }
      return msgObj;
    }
    return new JLabel();     //no message string; return empty label
  }
}
