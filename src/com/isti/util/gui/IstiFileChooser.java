//IstiFileChooser.java:  Extends the JFileChooser.
//
//   6/3/2002 -- [KF]  Initial version.
//  6/10/2002 -- [KF]  Clears selection when directory is changed.
//  6/12/2002 -- [KF]  Moved bug fixes into new class: IstiFixedJFileChooser;
//                     added parent to showDialog; dded an AccessoryPanel to
//                     select replace/append selections.
//  6/17/2002 -- [KF]  When appending, removes path if same as previous
//                     path.
//  7/10/2002 -- [KF]  Uses "Append" button that approves selection instead
//                     of selection type.
//  7/19/2002 -- [ET]  Class moved to isti.util.gui and name changed.
//  5/29/2003 -- [KF]  Made approve button text public;
//                     added 'getDefaultDirectory' method.
//  1/20/2004 -- [ET]  Modified 'setSelectedFiles()' to prevent it
//                     from defaulting to user-home directory when
//                     current-selection and default-directory are
//                     empty.
//  4/23/2004 -- [ET]  Modified 'showDialog()' to setup a default dialog
//                     title of "Choose File(s)" when an 'approveButtonText'
//                     parameter is given (was defaulting to the button
//                     text itself).
//  7/16/2004 -- [ET]  Modified 'showDialog()' to make it restore focus
//                     to the 'parent' component on exit.
//  9/16/2004 -- [KF]  Modifed 'setSelectedFiles()' to set the current directory
//                     if the selection is not empty but does not contain a
//                     parent directory.
//  9/17/2004 -- [KF]  Added 'getTitleText()' method and used it to change the
//                     title to "Choose Director{y,ies}" with directories only.
//  2/11/2005 -- [ET]  Added work-around for Java 1.4.2 bug (4711700)
//                     that could result in 'NullPointerException' via
//                     'createFileChooser()' method and protected
//                     constructor.
//   4/6/2005 -- [ET]  Added overridden version of 'setCurrentDirectory()'
//                     to work around Java 1.4.2 bug (4869950); added
//                     explicit initializers for 'currentSelection' and
//                     'windowSize' variables.
//  3/30/2012 -- [KF]  Modified to use null to clear the selection.
//

package com.isti.util.gui;

import java.util.Vector;
import java.io.File;
import java.io.IOException;
import java.awt.Dimension;
import java.awt.Window;
import java.awt.Component;
import javax.swing.JFileChooser;
import javax.swing.JOptionPane;

/**
 * Class IstiFileChooser Extends the JFileChooser.
 */
public class IstiFileChooser extends IstiFixedJFileChooser
{
  public static final String APPROVE_BUTTON_TEXT = "OK";
  protected String currentSelection = null;
  protected Dimension windowSize = null;
  protected boolean appendFlag;
  protected static File defaultDirectory = null;
  protected boolean confirmOverwriteFlag = false;
  protected Component defaultParent = null;

  /**
   * Constructs a file chooser object.
   * @param frameWidth the width of the file chooser window frame.
   * @param frameHeight the height of the file chooser window frame.
   * @param appendButtonFlag specifies that an "Append" button will be
   * added to the panel (that will append selections to those chosen
   * previously).
   */
  protected IstiFileChooser(int frameWidth,int frameHeight,
                         boolean appendButtonFlag)
  {
    setApproveButtonText(APPROVE_BUTTON_TEXT);
    setApproveButtonMnemonic(APPROVE_BUTTON_TEXT.charAt(0));

    // increase the preferred size (if given):
    if(frameWidth > 0 && frameHeight > 0)
      setPreferredSize(new Dimension(frameWidth,frameHeight));

    if(appendButtonFlag)     //if flag then add panel with "Append" button
      setAccessory(new IstiFileChooserAccessoryPanel(this));
  }

  /**
   * Constructs a file chooser object.  No "Append" button is added.
   * @param frameWidth the width of the file chooser window frame.
   * @param frameHeight the height of the file chooser window frame.
   */
  protected IstiFileChooser(int frameWidth,int frameHeight)
  {
    this(frameWidth,frameHeight,false);
  }

  /**
   * Constructs a file chooser object.  The default window frame size
   * is used.
   * @param appendButtonFlag specifies that an "Append" button will be
   * added to the panel (that will append selections to those chosen
   * previously).
   */
  protected IstiFileChooser(boolean appendButtonFlag)
  {
    this(0,0,appendButtonFlag);
  }

  /**
   * Constructs a file chooser object.  The default window frame size
   * is used and no "Append" button is added.
   */
  protected IstiFileChooser()
  {
    this(0,0,false);
  }

  /**
   * Creates and returns a file-chooser object.  This method performs
   * retries to try and work-around the possible occurrence of
   * 'NullPointerException' during construction under Java 1.4.2;
   * see Sun-Java bug 4711700.
   * @param frameWidth the width of the file chooser window frame.
   * @param frameHeight the height of the file chooser window frame.
   * @param appendButtonFlag specifies that an "Append" button will be
   * added to the panel (that will append selections to those chosen
   * previously).
   * @return A new 'IstiFileChooser' object.
   */
  public static IstiFileChooser createFileChooser(int frameWidth,
                                   int frameHeight,boolean appendButtonFlag)
  {
    int c;
    for(c=0; c<1000; ++c)
    {    //attempt to call constructor; loop if exception occurred
      try
      {                      //create and return chooser:
        return new IstiFileChooser(frameWidth,frameHeight,appendButtonFlag);
      }
      catch(Exception ex)
      {  //exception error; show message
//        System.err.println("IstiFileChooser:  Retrying after " +
//                                   "error creating 'JFileChooser':  " + ex);
      }
      try { Thread.sleep(1); }              //delay between attempts
      catch(InterruptedException ex) {}
    }
    System.err.println("IstiFileChooser:  Unable to create " +
             "'JFileChooser' after " + c + " retries; making last attempt");
              //create and return chooser (outside of try/catch block):
    return new IstiFileChooser(frameWidth,frameHeight,appendButtonFlag);
  }

  /**
   * Creates and returns a file-chooser object.  This method performs
   * retries to try and work-around the possible occurrence of
   * 'NullPointerException' during construction under Java 1.4.2;
   * see Sun-Java bug 4711700.
   * @param frameWidth the width of the file chooser window frame.
   * @param frameHeight the height of the file chooser window frame.
   * @return A new 'IstiFileChooser' object.
   */
  public static IstiFileChooser createFileChooser(int frameWidth,
                                                            int frameHeight)
  {
    return createFileChooser(frameWidth,frameHeight,false);
  }

  /**
   * Creates and returns a file-chooser object.  The default window
   * frame size is used.  This method performs retries to try and
   * work-around the possible occurrence of 'NullPointerException'
   * during construction under Java 1.4.2; see Sun-Java bug 4711700.
   * @param appendButtonFlag specifies that an "Append" button will be
   * added to the panel (that will append selections to those chosen
   * previously).
   * @return A new 'IstiFileChooser' object.
   */
  public static IstiFileChooser createFileChooser(boolean appendButtonFlag)
  {
    return createFileChooser(0,0,appendButtonFlag);
  }

  /**
   * Creates and returns a file-chooser object.  The default window frame
   * size is used and no "Append" button is added.  This method performs
   * retries to try and work-around the possible occurrence of
   * 'NullPointerException' during construction under Java 1.4.2;
   * see Sun-Java bug 4711700.
   * @return A new 'IstiFileChooser' object.
   */
  public static IstiFileChooser createFileChooser()
  {
    return createFileChooser(0,0,false);
  }

  /**
   * @return the default parent component.
   */
  public Component getDefaultParent()
  {
    return defaultParent;
  }

  /**
   * @return true if confirm overwrite is enabled.
   */
  public boolean isConfirmOverwriteEnable()
  {
    return confirmOverwriteFlag;
  }

  /**
   * Set the default parent component.
   * @param parent default parent component
   */
  public void setDefaultParent(Component parent)
  {
    defaultParent = parent;
  }

  /**
   * Enables or disable confirm overwrite.
   * @param confirmOverwriteFlag true if overwrite should be confirmed.
   *
   * The default parent is used for the overwrite confirmation dialog.
   * @see also setDefaultParent
   */
  public void setConfirmOverwriteEnable(boolean confirmOverwriteFlag)
  {
    this.confirmOverwriteFlag = confirmOverwriteFlag;
  }

  /**
   * Pops up the file chooser dialog.
   * @param   parent  the parent component of the dialog;
   *			can be <code>null</code>
   * @return  the return state of the file chooser on popdown:
   * <ul>
   * <li>JFileChooser.CANCEL_OPTION
   * <li>JFileChooser.APPROVE_OPTION
   * <li>JFileChooser.ERROR_OPTION if an error occurs or the
   *			dialog is dismissed
   * </ul>
   */
  public int showDialog(Component parent)
  {
    setApproveButtonToolTipText("Use the current selection" +
                                (isMultiSelectionEnabled()?"(s)":""));

    return showDialog(parent, APPROVE_BUTTON_TEXT);
  }

  /**
   * Pops up the file chooser dialog.
   * @param   parent  the parent component of the dialog;
   *			can be <code>null</code>
   * @param   approveButtonText the text of the <code>ApproveButton</code>
   * @return  the return state of the file chooser on popdown:
   * <ul>
   * <li>JFileChooser.CANCEL_OPTION
   * <li>JFileChooser.APPROVE_OPTION
   * <li>JFileChooser.ERROR_OPTION if an error occurs or the
   *			dialog is dismissed
   * </ul>
   */
  public int showDialog(Component parent, String approveButtonText)
  {

    // set the selected files to the specifed starting value or
    // the same as the previous run
    setSelectedFiles(currentSelection);

    // set the size to the same as the previous run
    if (windowSize != null)
    {
      setPreferredSize(windowSize);
    }

    // default to no append
    appendFlag = false;

//    int result = super.showDialog(parent, null);
//    System.err.println("showing dialog ok");
//    if (parent == null)
//      System.err.println("parent is null");

         //if an 'approveButtonText' value is given and there is no
         // current dialog title then put in a default title
         // (needed because 'approveButtonText' makes this a
         //  "custom" dialog that will default to using the
         //  'approveButtonText' value as the title):
    if(approveButtonText != null && getDialogTitle() == null)
    {
      setDialogTitle(getTitleText());
    }

    int result = super.showDialog(parent, approveButtonText);
//    int result = showOpenDialog(parent);

//    System.err.println("result is " + result);

//    if (result == JFileChooser.APPROVE_OPTION)
//      System.err.println("result is approve");
//    if (result == JFileChooser.CANCEL_OPTION)
//      System.err.println("result is cancell");
//    if (result == JFileChooser.ERROR_OPTION)
//      System.err.println("result is error");

    try       //if focus not restored to parent then
    {         //put focus on the parent's window ancestor:
      final Window windowObj;
      if(parent != null && !parent.hasFocus() &&
        (windowObj=IstiDialogPopup.getWindowForComponent(parent)) != null &&
                                                      !windowObj.hasFocus())
      {
        windowObj.requestFocus();
      }
    }
    catch(Exception ex)
    {    //some kind of exception error; ignore and move on
    }    // (focus is non-critical)

    // exit if the user didn't approve a selection
    if (result != JFileChooser.APPROVE_OPTION)  return result;

//    System.err.println("a");

    // exit if the user didn't select anything
    final File selectedFile = getSelectedFile();
    if (selectedFile == null)  return JFileChooser.ERROR_OPTION;

//    System.err.println("b");

    // if the selection should not be appended to the previous value
    if (!appendFlag)
    {
      // clear the previous value
      currentSelection = null;
    }
    if (currentSelection == null)
    {
      currentSelection = "";
    }

    // add separator if the current selection is not empty
    if (currentSelection.length() > 0)
    {
      File lastDir = getLastDirectory(currentSelection);
      File selectionDir = getLastDirectory(selectedFile.getAbsolutePath());

      currentSelection += File.pathSeparator;
      if (lastDir != null && selectionDir != null &&
          lastDir.getAbsolutePath().compareTo(selectionDir.getAbsolutePath()) == 0)
      {
        currentSelection += selectedFile.getName();
      }
      else
      {
        currentSelection += selectedFile.getAbsolutePath();
      }
    }
    else
    {
      currentSelection = selectedFile.getAbsolutePath();
    }

//    System.err.println("c");

    File [] files = getSelectedFiles();

    for (int i = 1; i < files.length; i++)
    {
      currentSelection += File.pathSeparator;
      currentSelection += files[i].getName();
    }

    // save the size
    windowSize = getSize();
//    System.err.println("d");

    return result;
  }

  /**
   * Pops up the file chooser dialog using the default parent.
   * @return  the return state of the file chooser on popdown:
   * <ul>
   * <li>JFileChooser.CANCEL_OPTION
   * <li>JFileChooser.APPROVE_OPTION
   * <li>JFileChooser.ERROR_OPTION if an error occurs or the
   *			dialog is dismissed
   * </ul>
   *
   * @see also setDefaultParent
   */
  public int showDialog()
  {
    return showDialog(defaultParent);
  }

  /**
   * @return the current selection
   */
  public String getCurrentSelection()
  {
    return currentSelection;
  }

  /**
   * Sets the current selection to be used when the dialog is shown
   * @param currentFile current file selection
   */
  public void setCurrentSelection(String currentFile)
  {
    currentSelection = currentFile;
  }

  /**
   * Sets the selected files
   * @param files file selection
   */
  protected void setSelectedFiles(String files)
  {
    // clear selection and set to the current directory
    clearSelection();
              //if given then setup default directory (initially):
    if(defaultDirectory != null)
      setCurrentDirectory(defaultDirectory);

    // leave directory and selection blank if files are not specified
    if (files == null || files.trim().length() <= 0) return;

    File lastDir = getLastDirectory(files);
    if (lastDir != null)
      setCurrentDirectory(lastDir);
    else
      setCurrentDirectory(new File(".").getAbsoluteFile());
  }

  /**
   * @return an array of files from the string
   * @param files file selection
   */
  public static File[] getFileArray(String files)
  {
    if (files == null || files.length() <= 0) return null;

    final int firstIndex = files.indexOf(File.pathSeparatorChar);

    // if path Separator Char not found there is only a single file
    if (firstIndex < 0)
    {
      File [] fileArray = { new File(files) };
      return fileArray;
    }

    // create a vector of files
    Vector vector = new Vector();

    int beginIndex = 0;

    for (int endIndex = firstIndex;
         endIndex >= 0 && beginIndex < files.length();
         beginIndex = endIndex + 1,
         endIndex = files.indexOf(File.pathSeparatorChar, beginIndex))
    {
      vector.add(new File(files.substring(beginIndex, endIndex)));
    }

    vector.add(new File(files.substring(beginIndex)));

    // use the vector to make an array of files
    File [] fileArray = new File [vector.size()];
    for (int i = 0; i < fileArray.length; i++)
    {
      fileArray[i] = ((File)vector.get(i));
    }
    return fileArray;
  }

  /**
   * @return the direcotry of the last entry with a path from the files string
   * @param files file selection
   */
  public static File getLastDirectory(String files)
  {
    return getLastDirectory(getFileArray(files));
  }

  /**
   * @return the directory of the last entry with a path from the file array
   * @param fileArray array of files
   */
  public static File getLastDirectory(File[] fileArray)
  {
    if (fileArray == null || fileArray.length <= 0)  return null;

    for (int i = fileArray.length - 1; i >= 0; i--)
    {
      // get the file from the array
      File file = fileArray[i];
      if (file == null || file.toString().length() <= 0)  continue;

      // get the parent from the file
      file = file.getParentFile();
      if (file == null || file.toString().length() <= 0)  continue;

      // return the parent
      return file;
    }

    // did not find a directory
    return null;
  }

  /**
   * Gets the title text.
   * @return the title text.
   */
  public String getTitleText()
  {
    final String titleText;
    if (getFileSelectionMode()==JFileChooser.DIRECTORIES_ONLY)
      titleText = "Choose Director"+(isMultiSelectionEnabled()?"ies":"y");
    else
      titleText = "Choose File"+(isMultiSelectionEnabled()?"s":"");
    return titleText;
  }

  /**
   * Gets the default directory to use when showing all dialogs.
   * @return the default directory
   */
  public static File getDefaultDirectory()
  {
    return IstiFileChooser.defaultDirectory;
  }

  /**
   * Sets the default directory to use when showing all dialogs.
   * @param defaultDirectory the default directory
   */
  public static void setDefaultDirectory(File defaultDirectory)
  {
    IstiFileChooser.defaultDirectory = defaultDirectory;
  }

  /**
   * Called by the UI when the user hits the Approve button
   * using the default parent (labeled "Open" or "Save", by default).
   * This can also be called by the programmer.
   *
   * @see also setDefaultParent
   */
  public void approveSelection()
  {
    final File selectedFile = getSelectedFile();  //get the selected file

    if (selectedFile == null || selectedFile.getName().length() <= 0)
      return;

    //if confirm overwrite and the selected file exists
    if (confirmOverwriteFlag && selectedFile.exists())
    {
      //show a confirm dialog
      final int retVal = JOptionPane.showConfirmDialog(
          defaultParent,  "File exists: \"" + selectedFile.getName() +
          "\". Do you wish to overwrite it?", "Overwrite File",
          JOptionPane.YES_NO_CANCEL_OPTION, JOptionPane.QUESTION_MESSAGE);

      //if YES was not selected
      if (retVal != JOptionPane.YES_OPTION)
      {
        //if CANCEL was selected
        if (retVal == JOptionPane.CANCEL_OPTION)
          super.cancelSelection();  //cancel the selection
        return;
      }
    }

    super.approveSelection();  //approve the selection
  }

  /**
   * Called when the user hits the Append button.
   */
  public void appendSelection()
  {
    appendFlag = true;
    approveSelection();
  }

  /**
   * Sets the current directory.  This overridden version converts the
   * given directory to a "canonical" version before calling the parent
   * method to work around Java 1.4.2 bug (4869950).  Passing in null
   * sets the file chooser to point to the user's home directory.  If the
   * file passed in as <code>currentDirectory</code> is not a directory,
   * the parent of the file will be used as the currentDirectory.
   * If the parent is not traversable, then it will walk up the parent tree
   * until it finds a traversable directory, or hits the root of the
   * file system.
   * @param dirObj the current directory to point to.
   */
  public void setCurrentDirectory(File dirObj)
  {
    if(dirObj != null)
    {    //given object is not null
      try
      {       //convert to "canonical" version of given path:
        dirObj = dirObj.getCanonicalFile();
      }
      catch(IOException ex)
      {       //exception error; leave directory file object unchanged
      }
    }
    super.setCurrentDirectory(dirObj);
  }
}
