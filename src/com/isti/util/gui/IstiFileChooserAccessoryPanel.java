//IstiFileChooserAccessoryPanel.java:  Defines a file-chooser accessory
//                                     panel that adds an "Append" button
//                                     to the chooser.
//
//  6/12/2002 -- [KF]  Initial version.
//  7/10/2002 -- [KF]  Used "Append" button that approves selection instead
//                     of selection type.
//  7/12/2002 -- [KF]  Decreased the size of the append button
//  7/19/2002 -- [ET]  Class moved to isti.util.gui and name changed;
//                     changed so the panel and button are always enabled.
//

package com.isti.util.gui;

import java.awt.Insets;
import java.awt.event.ActionEvent;

import javax.swing.BorderFactory;
import javax.swing.Box;
import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JPanel;

/**
 * Class IstiFileChooserAccessoryPanel defines a file-chooser accessory
 * panel that adds an "Append" button to the chooser.
 */
public class IstiFileChooserAccessoryPanel extends JPanel
{
  protected final IstiFileChooser fileChooser;
  protected final JButton appendButton = new JButton("Append");
  protected final BoxLayout panelLayout =
                                      new BoxLayout(this, BoxLayout.Y_AXIS);

    /**
     * Construct the File Chooser Accessory panel.
     * @param fileChooser is the RespFileChooser for the Accessory.
     */
  public IstiFileChooserAccessoryPanel(IstiFileChooser fileChooser)
  {
    this.fileChooser = fileChooser;
    try
    {
      jbInit();
    }
    catch(Exception e)
    {
      e.printStackTrace();
    }
  }

  /**
   * Component initialization.
   * @throws Exception
   */
  protected void jbInit() throws Exception
  {
    appendButton.addActionListener(new java.awt.event.ActionListener()
    {              //setup action for "Append" button
      public void actionPerformed(ActionEvent e)
      {
        if (fileChooser != null)
          fileChooser.appendSelection();
      }
    });
    appendButton.setMargin(new Insets(2, 7, 2, 7));
    appendButton.setToolTipText("Append the current selection(s)");

    this.setLayout(panelLayout);
    // add empty border with extra space to center in dialog:
    // createEmptyBorder (top, left, bottom, right)
    this.setBorder(BorderFactory.createEmptyBorder(0, 5, 0, 0));
    this.add(Box.createGlue());
    this.add(appendButton);
  }
}
