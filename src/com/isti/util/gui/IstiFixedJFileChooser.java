//IstiFixedJFileChooser.java:  Extension of JFileChooser that encapsulates
//                             bug fixes to the class.
//
//  6/11/2002 -- [KF]  Initial version.
//  7/19/2002 -- [ET]  Class moved to isti.util.gui and name changed.
//  1/11/2005 -- [ET]  Added work-around for Java 1.4.2 bug (4711700)
//                     that could result in 'NullPointerException'.
//  2/11/2005 -- [ET]  Added new work-around for Java 1.4.2 bug (4711700)
//                     that could result in 'NullPointerException' via
//                     'createFixedFileChooser()' method and protected
//                     constructor.
// 12/13/2005 -- [KF]  Changed to not remove any buttons because
//                     "OK" and "Cancel" were being removed on the Mac.
//  3/30/2012 -- [KF]  Modified to use null to clear the selection.
//

package com.isti.util.gui;

import java.io.File;
import java.beans.PropertyChangeListener;
import java.beans.PropertyChangeEvent;
import javax.swing.*;

/**
 * Class IstiFixedJFileChooser is an extension of JFileChooser that
 * encapsulates bug fixes to the class.
 */
public class IstiFixedJFileChooser extends JFileChooser
                                           implements PropertyChangeListener
{
         //this initial attempt at bug work-around didn't seems to work:
              //set system property to work-around possibility of
//  static      // 'NullPointerException' under Java 1.4.2 (no effect
//  {           // in Java 1.5); see Sun-Java bug 4711700:
//    System.setProperty("swing.disableFileChooserSpeedFix","true");
//  }

  /** Creates a file chooser object. */
  protected IstiFixedJFileChooser()
  {
    addPropertyChangeListener(JFileChooser.SELECTED_FILES_CHANGED_PROPERTY,
      this);
    addPropertyChangeListener(JFileChooser.DIRECTORY_CHANGED_PROPERTY,
      this);
    addPropertyChangeListener(JFileChooser.SELECTED_FILE_CHANGED_PROPERTY,
      this);
  }

  /**
   * Clear selection.
   */
  public void clearSelection()
  {
    setSelectedFile(null);
  }

  /**
   * Creates and returns a file-chooser object.  This method performs
   * retries to try and work-around the possible occurrence of
   * 'NullPointerException' during construction under Java 1.4.2;
   * see Sun-Java bug 4711700.
   * @return A new 'IstiFixedJFileChooser' object.
   */
  public static IstiFixedJFileChooser createFixedFileChooser()
  {
    int c;
    for(c=0; c<1000; ++c)
    {    //attempt to call constructor; loop if exception occurred
      try
      {
        return new IstiFixedJFileChooser();      //create and return chooser
      }
      catch(Exception ex)
      {  //exception error; show message
        System.err.println("IstiFixedJFileChooser:  Retrying after " +
                                   "error creating 'JFileChooser':  " + ex);
      }
      try { Thread.sleep(1); }              //delay between attempts
      catch(InterruptedException ex) {}
    }
    System.err.println("IstiFixedJFileChooser:  Unable to create " +
             "'JFileChooser' after " + c + " retries; making last attempt");
              //create and return chooser (outside of try/catch block):
    return new IstiFixedJFileChooser();
  }

    /**
     * Prevent file chooser FileView to jump to upper-most
     * selected file in muli-selection list.
     * See Bug Id  4354065 for details.
     *
     * Clear selection when directory is changed
     *
     * Clear selection if invalid (directory with files only enabled or
     *                             file with directories only enabled)
     * See Bug Id  4499621 for details.
     * @param e property change event
     */
  public void propertyChange(PropertyChangeEvent e)
  {
    if (e == null)  return;

    if (e.getPropertyName().compareTo(JFileChooser.SELECTED_FILES_CHANGED_PROPERTY) == 0)
    {
      if (e.getNewValue() instanceof java.io.File[])
      {
        File files[] = (java.io.File [])e.getNewValue();

        if (files.length > 0)
        {
          getUI().ensureFileIsVisible(this, files[files.length - 1]);
        }
      }
    }
    else if (e.getPropertyName().compareTo(JFileChooser.DIRECTORY_CHANGED_PROPERTY) == 0)
    {
      File file = getSelectedFile();

      // if selected file exists, is not empty and is a directory
      // NOTE: This test is needed for JDK 1.4 otherwise the program will crash
      if (file != null && file.getName().length() > 0 && file.isDirectory())
      {
        clearSelection(); // clear selection
      }
    }
    else if (e.getPropertyName().compareTo(JFileChooser.SELECTED_FILE_CHANGED_PROPERTY) == 0)
    {
      if (e.getNewValue() instanceof java.io.File)
      {
        File file = (java.io.File)e.getNewValue();

        switch (getFileSelectionMode())
        {
        case JFileChooser.FILES_ONLY:
          if (file.isDirectory())
          {
            clearSelection(); // clear selection
          }
          break;
        case JFileChooser.DIRECTORIES_ONLY:
          if (!file.isDirectory())
          {
            clearSelection(); // clear selection
          }
          break;
        }
      }
    }
  }
}
