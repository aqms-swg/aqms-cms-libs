//IstiFocusManager.java:  Implements the ISTI swing focus manager.
//
//   3/31/2003 -- [KF]
//   5/14/2003 -- [ET]  Added 'addTabKeyXEnabledComp...()' methods to
//                      allow 'Tab'-key translation to be enabled only
//                      on certain components.
//   5/27/2003 -- [KF]  Fix to work under JVM 1.4 and fix adding key characters.
//

package com.isti.util.gui;

import java.awt.Component;
import java.awt.event.FocusEvent;
import java.awt.event.FocusListener;
import java.awt.event.KeyEvent;
import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;

import javax.swing.DefaultFocusManager;

/**
 * Class IstiFocusManager implements the ISTI swing focus manager.
 *
 * This class extends the DefaultFocusManager so that it
 * adds support for adding traversal key codes/chars and
 * adds support for listening to focus traversal changes.
 */
public class IstiFocusManager extends DefaultFocusManager
{
  private final Set tabKeyCharSet = new HashSet();
  private final Set tabKeyCodeSet = new HashSet();
  private Set tabKeyXEnabledCompsList = null;
  private FocusListener traversalFocusListener = null;
  private Component lastFocusedComponent = null;

  /**
   * Returns the traversal focus listener.
   * @return The traversal focus listener or null if none.
   */
  public FocusListener getTraversalFocusListener()
  {
    return traversalFocusListener;
  }

  /**
   * Sets the traversal focus listener.
   * @param fl focus listener or null for none.
   */
  public void setTraversalFocusListener(FocusListener fl)
  {
    traversalFocusListener = fl;
  }

  /**
   * Adds the tab key code to the set.
   * @param keyChar key character.
   */
  public void addTabKeyChar(char keyChar)
  {
    tabKeyCharSet.add(Character.valueOf(keyChar));
  }

  /**
   * Adds the tab key code to the set.
   * @param keyCode key code.
   */
  public void addTabKeyCode(int keyCode)
  {
    tabKeyCodeSet.add(Integer.valueOf(keyCode));
  }

  /**
   * Adds a component to the list of components for which tab keycodes
   * are translated.  If no components are entered then tab keycodes are
   * translated for all components; otherwise tab keycodes are translated
   * only for the entered components.
   * @param compObj the component to be entered.
   */
  public void addTabKeyXEnabledComponent(Component compObj)
  {
    if(tabKeyXEnabledCompsList == null)
      tabKeyXEnabledCompsList = new HashSet();
    tabKeyXEnabledCompsList.add(compObj);
  }

  /**
   * Adds an array of components to the list of components for which
   * tab keycodes are translated.  If no components are entered then
   * tab keycodes are translated for all components; otherwise tab
   * keycodes are translated only for the entered components.
   * @param compArr the array of components to be entered.
   */
  public void addTabKeyXEnabledCompArray(Component [] compArr)
  {
    if(tabKeyXEnabledCompsList == null)
      tabKeyXEnabledCompsList = new HashSet();
    tabKeyXEnabledCompsList.addAll(Arrays.asList(compArr));
  }

  /**
   * This method is called by JComponents when a key event occurs.
   * JComponent gives key events to the focus manager
   * first, then to key listeners, then to the keyboard UI dispatcher.
   * This method should look at the key event and change the focused
   * component if the key event matches the receiver's focus manager
   * hot keys. For example the default focus manager will change the
   * focus if the key event matches TAB or Shift + TAB.
   * The focus manager should call consume() on <b>anEvent</b> if
   * <code>anEvent</code> has been processed.
   * <code>focusedComponent</code> is the component that currently has
   * the focus.
   * Note: FocusManager will receive both KEY_PRESSED and KEY_RELEASED
   * key events. If one event is consumed, the other one should be consumed
   * too.
   * @param focusedComponent focused component
   * @param anEvent key event.
   */
  public void processKeyEvent(Component focusedComponent,KeyEvent anEvent)
  {
         //if list of components empty or component is on list and
         // keycode is in list of keycodes to be translated
         // then translate keycode to 'Tab':
    if((tabKeyXEnabledCompsList == null ||
                      tabKeyXEnabledCompsList.contains(focusedComponent)) &&
               (tabKeyCodeSet.contains(Integer.valueOf(anEvent.getKeyCode())) ||
               tabKeyCharSet.contains(Character.valueOf(anEvent.getKeyChar()))))
    {
      anEvent.setKeyCode(KeyEvent.VK_TAB);  //treat like TAB
    }

    //process the key event
    super.processKeyEvent(focusedComponent, anEvent);

    if (lastFocusedComponent != focusedComponent)  //if focus component changed
    {
      //if traversal focus listener exists
      if (traversalFocusListener != null)
      {
        if (anEvent.getKeyCode() == KeyEvent.VK_TAB || anEvent.getKeyChar() == '\t')
        {
          if (lastFocusedComponent != null)  //if focus lost component exists
          {
            //send out the focus lost event
            traversalFocusListener.focusLost(
                new FocusEvent(lastFocusedComponent, FocusEvent.FOCUS_LOST));
          }
          if (focusedComponent != null)  //if focus gained component exists
          {
            //send out the focus gained event
            traversalFocusListener.focusGained(
                new FocusEvent(focusedComponent, FocusEvent.FOCUS_GAINED));
          }
        }
      }

      //save the last focused component
      lastFocusedComponent = focusedComponent;
    }
  }
}
