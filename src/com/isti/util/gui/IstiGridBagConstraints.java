// IstiGridBagConstraints.java:  Extends the 'GridBagConstraints' class to add
//                               additional constructors.
//
//  8/12/2003 -- [KF]
//

package com.isti.util.gui;

import java.awt.GridBagConstraints;

/**
 * The <code>IstiGridBagConstraints</code> class specifies constraints
 * for components that are laid out using the
 * <code>GridBagLayout</code> class.
 */
public class IstiGridBagConstraints extends GridBagConstraints
{

  /**
   * Creates a <code>GridBagConstraint</code> object with
   * all of its fields set to their default value.
   */
  public IstiGridBagConstraints()
  {
    super();
  }

  /**
   * Creates a <code>GridBagConstraint</code> object with
   * the specified gridx and gridy values
   * and all of the other fields set to their default value.
   * @param gridx	The initial gridx value.
   * @param gridy	The initial gridy value.
   */
  public IstiGridBagConstraints(int gridx, int gridy)
  {
    this();
    this.gridx = gridx;
    this.gridy = gridy;
  }

  /**
   * Creates a <code>GridBagConstraint</code> object with
   * the specified gridx, gridy, gridwidth, and gridheight values
   * and all of the other fields set to their default value.
   * @param gridx	The initial gridx value.
   * @param gridy	The initial gridy value.
   * @param gridwidth	The initial gridwidth value.
   * @param gridheight	The initial gridheight value.
   */
  public IstiGridBagConstraints(int gridx, int gridy,
                                int gridwidth, int gridheight)
  {
    this();
    this.gridx = gridx;
    this.gridy = gridy;
    this.gridwidth = gridwidth;
    this.gridheight = gridheight;
  }
}