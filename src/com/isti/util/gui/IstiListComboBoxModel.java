//IstiListComboBoxModel.java:  Defines a list combo box model.
//
//   6/18/2009 -- [KF]  Initial version.
//

package com.isti.util.gui;

import java.util.List;

import javax.swing.AbstractListModel;
import javax.swing.MutableComboBoxModel;

/**
 * Class IstiListComboBoxModel defines a list combo box model.
 */
public class IstiListComboBoxModel extends AbstractListModel implements
    MutableComboBoxModel {

  private static final long serialVersionUID = 1L;

  /** The list of objects. */
  private final List objects;

  /** The selected object or null if none. */
  private Object selectedObject;

  /**
   * Constructs a station combo box model.
   * @param objects the list of objects.
   * @param selectedObject the selected object.
   */
  public IstiListComboBoxModel(final List objects, final Object selectedObject) {
    this.objects = objects;
    this.selectedObject = selectedObject;
  }

  /**
   * Adds an item at the end of the model.
   * @param obj the <code>Object</code> to be added
   */
  public void addElement(Object obj) {
    objects.add(obj);
    fireIntervalAdded(this, objects.size() - 1, objects.size() - 1);
    if (objects.size() == 1 && selectedObject == null && obj != null) {
      setSelectedItem(obj);
    }
  }

  /**
   * Gets the value at the specified index.
   * @param index the requested index.
   * @return the value at <code>index</code>.
   */
  public Object getElementAt(int index) {
    if (index >= 0 && index < objects.size())
      return objects.get(index);
    else
      return null;
  }

  /**
   * Gets the selected item.
   * @return the selected item or <code>null</code> if there is no selection.
   */
  public Object getSelectedItem() {
    return selectedObject;
  }

  /**
   * Gets the length of the list.
   * @return the length of the list.
   */
  public int getSize() {
    return objects.size();
  }

  /**
   * Adds an item at a specific index.
   * @param obj  the <code>Object</code> to be added
   * @param index  location to add the object
   */
  public void insertElementAt(Object obj, int index) {
    objects.add(index, obj);
    fireIntervalAdded(this, index, index);
  }

  /**
   * Empties the list.
   */
  public void removeAllElements() {
    if (objects.size() > 0) {
      int firstIndex = 0;
      int lastIndex = objects.size() - 1;
      objects.clear();
      selectedObject = null;
      fireIntervalRemoved(this, firstIndex, lastIndex);
    } else {
      selectedObject = null;
    }
  }

  /**
   * Removes an item from the model.
   * @param obj the <code>Object</code> to be removed
   */
  public void removeElement(Object obj) {
    int index = objects.indexOf(obj);
    if (index != -1) {
      removeElementAt(index);
    }
  }

  /**
   * Removes an item at a specific index. The implementation of this method 
   * should notify all registered <code>ListDataListener</code>s that the 
   * item has been removed.
   *
   * @param index  location of object to be removed
   */
  public void removeElementAt(int index) {
    if (getElementAt(index) == selectedObject) {
      if (index == 0) {
        setSelectedItem(getSize() == 1 ? null : getElementAt(index + 1));
      } else {
        setSelectedItem(getElementAt(index - 1));
      }
    }
    objects.remove(index);
    fireIntervalRemoved(this, index, index);
  }

  /**
   * Set the selected item.
   * @param anItem the selected item or <code>null</code> for no selection.
   */
  public void setSelectedItem(Object anItem) {
    if ((selectedObject != null && !selectedObject.equals(anItem))
        || selectedObject == null && anItem != null) {
      selectedObject = anItem;
      fireContentsChanged(this, -1, -1);
    }
  }
}
