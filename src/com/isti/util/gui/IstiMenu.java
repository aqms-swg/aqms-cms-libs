package com.isti.util.gui;

import java.awt.Cursor;
import java.awt.Point;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.event.MouseMotionListener;
import java.util.Enumeration;
import java.util.Vector;

import javax.swing.JList;
import javax.swing.ToolTipManager;

import com.isti.util.FifoHashtable;
import com.isti.util.UtilFns;

/**
 * A Menu-looking widget that is happier than a JMenu to sit on a panel.
 * [HS] 6-23-2003 - Initial version
 * [ET] 6-25-2003 - Made 'optionNameToOptionInfo' declaration include "final".
 * [HS] 7-01-2003 - Added removeMenuItem()
 * [HS] 7-02-2003 - Made non-rollover (and mostly non selectable) items out of
 *                  the items that have no ActionListeners
 * [HS] 7-02-2003 - Added support for a default list of ActionListeners
 * [KF] 2-06-2004 - Added 'getNumMenuItems' method.
 * [KF] 2-22-2021 - Added 'getToolTipText' method and modifiers to the ActionEvent.
 */

public class IstiMenu extends JList
    implements MouseMotionListener, MouseListener {

  private final FifoHashtable optionNameToOptionInfo = new FifoHashtable();
  private Cursor mouseoverCursor = null;
  private static final int ACTION_VECTOR_INDEX = 0;
  private static final int TOOLTIP_INDEX = 1;
  /**
   * a vector of default actions, to be performed if no other actions
   * are available for an item
   */
  private static Vector defaultActionListeners = new Vector();
  
  /**
   * Get the action modifiers for the specified mouse event.
   * @param e the mouse event.
   * @return the action modifiers.
   */
  public static int getActionModifiers(MouseEvent e)
  {
    int mouseModifiers = e.getModifiersEx();
    int actionModifers = 0;
    if ((mouseModifiers & MouseEvent.ALT_DOWN_MASK) != 0) {
      actionModifers |= ActionEvent.ALT_MASK;
    }
    if ((mouseModifiers & MouseEvent.CTRL_DOWN_MASK) != 0) {
      actionModifers |= ActionEvent.CTRL_MASK;
    }
    if ((mouseModifiers & MouseEvent.META_DOWN_MASK) != 0) {
      actionModifers |= ActionEvent.META_MASK;
    }
    if ((mouseModifiers & MouseEvent.SHIFT_DOWN_MASK) != 0) {
      actionModifers |= ActionEvent.SHIFT_MASK;
    }
    return actionModifers;
  }

  /**
   * Create an empty IstiMenu object
   */
  public IstiMenu() {
    addMouseMotionListener(this);
    addMouseListener(this);
    // register to have tooltips
    ToolTipManager.sharedInstance().registerComponent(this);
  }

  /**
   * Add a new ActionListener to the default list.  These listeners
   * are alerted when a menu item without any other ActionListeners
   * is selected.
   * @param listener a new default ActionListener
   */
  public void addDefaultActionListener(ActionListener listener) {
    defaultActionListeners.add(listener);
  }

  /**
   * Remove an ActionListener from the list of default ActionListeners
   * @param listener the ActionListener to remove
   */
  public void removeDefaultActionListener(ActionListener listener) {
    defaultActionListeners.remove(listener);
  }

  /**
   * Add a menu item, and a coresponding ActionListener.
   * @param name the name of the item.
   * @param tooltipStr tooltip text for this option, or null for none.
   * @param listener an ActionListener associated with this item, or null
   * for none.
   */
  public void addMenuItem(String name, String tooltipStr,  ActionListener listener) {
    Object[] optionInfo = { new Vector(), tooltipStr != null ? tooltipStr : "" };
    if(listener != null) {
      ((Vector)optionInfo[ACTION_VECTOR_INDEX]).add(listener);
    }
    optionNameToOptionInfo.put(name, optionInfo);
    updateList();
  }

  /**
   * Remove a menu item from the menu.
   * @param name The menu item to remove
   */
  public void removeMenuItem(String name) {
    // only remove the key if it exists. (if it doesn't exist, we can
    // just silently finish.  The net result is the same.
    if(optionNameToOptionInfo.containsKey(name)) {
      optionNameToOptionInfo.remove(name);
      updateList();
    }
  }

  /**
   * Add a menu item.
   * @param name The menu item.
   * @param tooltipStr tooltip text for this option.
   */
  public void addMenuItem(String name, String tooltipStr) {
    Object[] optionInfo = {new Vector(), tooltipStr != null ? tooltipStr : ""};
    optionNameToOptionInfo.put(name, optionInfo);
    updateList();
  }

  /**
   * Gets the number of items in the menu.
   * @return the number of items in the menu.
   */
  public int getNumMenuItems()
  {
    return optionNameToOptionInfo.size();
  }

  /**
   * Add another ActionListener to a menu item
   * @param name The name of the menu item
   * @param l the action to be performed
   */
  public void addActionListenerToMenuItem(String name, ActionListener l) {
    ((Vector)((Object[])optionNameToOptionInfo.get(name))[ACTION_VECTOR_INDEX]).add(l);
  }
  
  /**
   * Get the tool tip text.
   * @return the tool tip text or an empty string if none.
   */
  public String getToolTipText() {
    final Object value = getSelectedValue();
    if (value != null) {
      String tip = (String) ((Object[]) optionNameToOptionInfo
          .get(value))[TOOLTIP_INDEX];
      if (tip != null) {
        return tip;
      }
    }
    return UtilFns.EMPTY_STRING;
  }


  /**
   * This is where tooltips are implemented.
   * @param e A MouseEvent so we can see where the cursor is
   * @return The string for the desired tooltip
   */
  public String getToolTipText(MouseEvent e) {
    if(getSelectedValue() == null) {
      // We may get called before mouseMoved() gets fired, when the user brings
      // the mouse into the widget.  In this case we have no selected value yet.
      // calling mouseMoved() with our event should fix this.
      mouseMoved(e);
    }
    return getToolTipText();
  }

  /**
   * Update the visual list based on our internal data
   */
  private void updateList() {
    setListData(optionNameToOptionInfo.getKeysVector());
  }

  /**
   * Set the cursor we should use when the mouse enters the menu.
   * @param c A Cursor object
   */
  public void setMouseoverCursor(Cursor c) {
    mouseoverCursor = c;
  }

  /*
   * Below are some methods to make us a MouseMotionListener
   */
  public void mouseDragged(MouseEvent e) {}
  public void mouseMoved(MouseEvent e) {
    int xPos = e.getX();
    int yPos = e.getY();
    int whichElement = locationToIndex(new Point(xPos, yPos));
    String whichItem = (String) getModel().getElementAt(whichElement);
    Vector actionVector = (Vector) ((Object[]) optionNameToOptionInfo.get(whichItem))[ACTION_VECTOR_INDEX];
    if(actionVector.size() > 0) {
      setSelectedIndex(whichElement);
    } else {
      clearSelection();
    }
  }

  /**
   * Method to implement the MouseListener interface.
   * @param e mouse event object.
   */
  public void mouseClicked(MouseEvent e) {
    String selection = (String)getSelectedValue();
    // there may not be a selection
    if(selection != null) {
      ActionEvent triggeredEvent = new ActionEvent(this,
          ActionEvent.ACTION_PERFORMED, selection, getActionModifiers(e));
      if(selection != null) {
        Vector actionVector = (Vector)((Object[]) optionNameToOptionInfo.get(selection))[ACTION_VECTOR_INDEX];
        Enumeration actions = actionVector.elements();
        // if there are no ActionListeners for this item, we'll run the default
        // set of ActionListeners
        if(!actions.hasMoreElements()) {
          actions = defaultActionListeners.elements();
        }
        while(actions.hasMoreElements()) {
          ActionListener thisAction = (ActionListener)actions.nextElement();
          thisAction.actionPerformed(triggeredEvent);
        }
      }
    }
  }

  /**
   * Local setSelectedIndex to avoid non-selectable items from being
   * selected
   * @param index the index of the item being selected
   */
  public void setSelectedIndex(int index) {
    String whichItem = (String) getModel().getElementAt(index);
    Vector actionVector = (Vector) ((Object[]) optionNameToOptionInfo.get(whichItem))[ACTION_VECTOR_INDEX];
    if(actionVector.size() > 0) {
      super.setSelectedIndex(index);
    } else {
      clearSelection();
    }
  }

  /**
   * Give ourselves a hand cursor
   * @param e the mouse event
   */
  public void mouseEntered(MouseEvent e) {
    if(mouseoverCursor != null) {
      setCursor(mouseoverCursor);
    }
  }
  public void mouseExited(MouseEvent e) {
    // the mouse has left the widget
    clearSelection();
  }
  public void mousePressed(MouseEvent e) {}
  public void mouseReleased(MouseEvent e) {}
}