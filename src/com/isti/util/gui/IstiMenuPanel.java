package com.isti.util.gui;


import java.awt.Component;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;

import javax.swing.JPanel;

/**
 * A widget that's menu-like, but wrapped in a full dialog box rather
 * than floating in space like a PopupMenu.<P>
 * [HS] 6-23-2003 - Initial version.
 * [ET] 6-25-2003 - Made 'menuObj' declaration include "final".
 * [HS] 7-02-2003 - Only adding our "close" ActionListener if there is another
 *                  ActionListener as well (no listeners, means non-selectable)
 * [HS] 7-03-2003 - we're now adding a default ActionListener to the menu object
 * [KF] 2-06-2004 - Added 'getNumMenuItems' method.
 */

public class IstiMenuPanel extends JPanel {
  private IstiDialogPopup dlgObj = null;
  private String dialogTitle = null;
  private final IstiMenu menuObj = new IstiMenu();
  private boolean showCloseButton = true;

  /**
   * Create a new IstiMenuPanel with a title.
   * @param title the title of the new IstiMenuPanel object.
   */
  public IstiMenuPanel(String title) {
    dialogTitle = title;
    add(menuObj);
    menuObj.setBackground(getBackground());
    menuObj.addDefaultActionListener(new ActionListener() {
      public void actionPerformed(ActionEvent e) {
        closeDialogBox();
      }
    });
  }

  /**
   * Remove an item from the menu.
   * @param name The item to remove
   */
  public void removeMenuItem(String name) {
    menuObj.removeMenuItem(name);
  }

  /**
   * Add a new menu item to the menu, along with an ActionListener.  An internal
   * action is also added, to close the dialog after selection.
   * @param name the name of the menu item.
   * @param tooltipStr tooltip text for this option, or null for none.
   * @param l An ActionListener representing a desired action (may be null for
   * no action).  No-action items are non-selectable
   */
  public void addMenuItem(String name, String tooltipStr, ActionListener l) {
    if(l != null) {
      menuObj.addMenuItem(name, tooltipStr, l);
      // we only add our own listener if the item is going to be
      // clickable
      menuObj.addActionListenerToMenuItem(name, new ActionListener() {
        public void actionPerformed(ActionEvent e) {
          closeDialogBox();
        }
      });
    } else {
      menuObj.addMenuItem(name, tooltipStr);
    }

  }

  /**
   * Gets the number of items in the menu.
   * @return the number of items in the menu.
   */
  public int getNumMenuItems()
  {
    return menuObj.getNumMenuItems();
  }

  /**
   * A custom show() method.
   * @param parentComponent parent component for the dialog, or null for
   * none.
   */
  public void show(Component parentComponent) {

    if(showCloseButton) {
      dlgObj = new IstiDialogPopup(parentComponent, this, dialogTitle,
                                   (new Object[] {"Close"}), 0, false);
    } else {
      dlgObj = new IstiDialogPopup(parentComponent, this, dialogTitle, null, 0,
                                   false);
    }
    dlgObj.addWindowListener(new WindowAdapter() {
      public void windowDeactivated(WindowEvent evtObj) {
        closeDialogBox();
      }
    });
    menuObj.setSelectedIndex(0);
    menuObj.setVisible(true);
    dlgObj.setVisible(true);
  }

  /**
   * Should the dialog box contain a close button?
   * @param b Set to true for a "Close" button, false otherwize.
   */
  public void setCloseButtonEnabled(boolean b) {
    showCloseButton = b;
  }

  /**
   * Method used to close the dialog box.
   */
  public void closeDialogBox() {
    if(dlgObj != null) {
      dlgObj.hide();
      dlgObj.dispose();
      dlgObj = null;
    }
  }
}

