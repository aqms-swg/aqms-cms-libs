//IstiPasswordDialog.java:  Defines a password dialog.
//
//   8/6/2004 -- [KF]  Initial version.
//  8/11/2004 -- [KF]  Added cancel button and removed forcing entry.
//  10/5/2004 -- [ET]  Added 'setUsernameAsInitalFocus()' and removed
//                     'enableButtons()'; added optional "prompt"-text
//                     parameter and call to 'setRepackNeededFlag(true)'
//                     to constructor.
// 10/12/2004 -- [ET]  Modified to call 'setOptionPaneValue()' and use
//                     a close-listener.
//  3/19/2012 -- [KF]  Added PasswordAuthenticator interface.
//

package com.isti.util.gui;

import java.awt.Component;
import java.awt.event.*;
import java.net.PasswordAuthentication;

import javax.swing.*;

import com.isti.util.PasswordAuthenticator;
import com.isti.util.UtilFns;

/**
 * Class IstiPasswordDialog defines a password dialog.
 */
public class IstiPasswordDialog extends IstiDialogPopup implements PasswordAuthenticator
{
  /**
   * Default title string to login dialog.
   */
  public static String LOGIN_DIALOG_DEFAULT_TITLE_STRING =
                        IstiPasswordPanel.LOGIN_DIALOG_DEFAULT_TITLE_STRING;

  /**
   * Default button-option string for login dialog.
   */
  public static String LOGIN_DIALOG_DEFAULT_OPTION_STRING =
                       IstiPasswordPanel.LOGIN_DIALOG_DEFAULT_OPTION_STRING;

  /**
   * Cancel button-option string for login dialog.
   */
  public static String LOGIN_DIALOG_CANCEL_OPTION_STRING =
                        IstiPasswordPanel.LOGIN_DIALOG_CANCEL_OPTION_STRING;

  private String usernameText = UtilFns.EMPTY_STRING;
  private String passwordText = UtilFns.EMPTY_STRING;
  private final IstiPasswordPanel passwordPanel;

  /**
   * Creates a password dialog with restricted character input for the
   * username.
   * @param parentComp the parent component for the popup.
   */
  public IstiPasswordDialog(Component parentComp)
  {
    this(parentComp, LOGIN_DIALOG_DEFAULT_TITLE_STRING);
  }

  /**
   * Creates a password dialog with restricted character input for the
   * username.
   * @param parentComp the parent component for the popup.
   * @param titleStr the title string for popup window.
   */
  public IstiPasswordDialog(Component parentComp, String titleStr)
  {
    this(parentComp, titleStr,
                         IstiPasswordPanel.createRestrictedPasswordPanel());
  }

  /**
   * Creates a password dialog with restricted character input for the
   * username.
   * @param parentComp the parent component for the popup.
   * @param titleStr the title string for popup window.
   * @param panelPromptStr the "prompt" text to be shown on the panel,
   * or null for none.
   */
  public IstiPasswordDialog(Component parentComp, String titleStr,
                                                      String panelPromptStr)
  {
    this(parentComp, titleStr,
           IstiPasswordPanel.createRestrictedPasswordPanel(panelPromptStr));
  }

  /**
   * Creates a password dialog.
   * @param parentComp the parent component for the popup.
   * @param titleStr the title string for popup window.
   * @param passwordPanel the password panel.
   */
  public IstiPasswordDialog(
      Component parentComp, String titleStr, IstiPasswordPanel passwordPanel)
  {
    this(parentComp, titleStr, passwordPanel,
         new JButton(LOGIN_DIALOG_DEFAULT_OPTION_STRING),
         new JButton(LOGIN_DIALOG_CANCEL_OPTION_STRING));
  }

  /**
   * Creates a password dialog.
   * @param parentComp the parent component for the popup.
   * @param titleStr the title string for popup window.
   * @param passwordPanel the password panel.
   * @param okButton the ok button.
   * @param cancelButton the cancel button.
   */
  protected IstiPasswordDialog(
      final Component parentComp, String titleStr,
      final IstiPasswordPanel passwordPanel, final JButton okButton,
      final JButton cancelButton)
  {
    super(parentComp, passwordPanel, titleStr,
          new Object[] { okButton, cancelButton }, 0);
    this.passwordPanel = passwordPanel;
    setRepackNeededFlag(true);
    okButton.addActionListener(new ActionListener()
        {     //setup action for "OK" button
          public void actionPerformed(ActionEvent e)
          {                       //save the entered values:
            usernameText = passwordPanel.getUsername();
            passwordText = passwordPanel.getPassword();
                   //set option-value for dialog (closes dialog):
            setOptionPaneValue(LOGIN_DIALOG_DEFAULT_OPTION_STRING);
          }
        });
    cancelButton.addActionListener(new ActionListener()
        {     //setup action for "Cancel" button
          public void actionPerformed(ActionEvent e)
          {        //set option-value for dialog (closes dialog):
            setOptionPaneValue(LOGIN_DIALOG_CANCEL_OPTION_STRING);
          }
        });
    addCloseListener(new CloseListener()
        {     //setup action for dialog closed
          public void dialogClosed(Object evtObj)
          {
            if(getValue() != LOGIN_DIALOG_DEFAULT_OPTION_STRING)
            {      //"OK" button not pressed' restore old values
              passwordPanel.setPassword(passwordText);
              passwordPanel.setUsername(usernameText);
            }
          }
        });

//    passwordPanel.addActionListener(new ActionListener()
//        {
//          /**
//           * Invoked when an action occurs.
//           * @param e the action event.
//           */
//          public void actionPerformed(ActionEvent e)
//          {
//            enableButtons();  //enable or disable the buttons
//          }
//        });
//
//    enableButtons();  //enable or disable the buttons
  }

  /**
   * Gets the password panel.
   * @return the password panel.
   */
  public IstiPasswordPanel getPasswordPanel()
  {
    return passwordPanel;
  }

  /**
   * Return the password text.
   * @return the password text.
   */
  public String getPasswordText()
  {
    return passwordText;
  }

  /**
   * Return the username text.
   * @return the username text.
   */
  public String getUsernameText()
  {
    return usernameText;
  }

  /**
   * Sets the password text.
   * @param passwordText the password text.
   */
  public void setPasswordText(String passwordText)
  {
    this.passwordText = passwordText;
    passwordPanel.setPassword(passwordText);
  }

  /**
   * Sets the username text.
   * @param usernameText the username text.
   */
  public void setUsernameText(String usernameText)
  {
    this.usernameText = usernameText;
    passwordPanel.setUsername(usernameText);
  }

  /**
   * Sets the "username" field to have the initial focus.
   */
  public void setUsernameAsInitalFocus()
  {
    setInitialFocusComponent(passwordPanel.getUsernameFieldObj());
  }

  /**
   * Get the password authentication and clear this dialog's password.
   * @return the password authentication or null if none.
   */
  public PasswordAuthentication getPasswordAuthentication()
  {
    showAndWait();
    if (getValue() == LOGIN_DIALOG_DEFAULT_OPTION_STRING)
    {
      PasswordAuthentication pa = passwordPanel.getPasswordAuthentication();
      setPasswordText(UtilFns.EMPTY_STRING);
      return pa;
    }
    return null;
  }

  /**
   * Cancels the popup dialog window.
   */
//  protected void cancel()
//  {
//    //restore the values
//    passwordPanel.setPassword(passwordText);
//    passwordPanel.setUsername(usernameText);
//    close();
//  }

  /**
   * Enables or disables the buttons.
   */
//  protected void enableButtons()
//  {
//  }
}
