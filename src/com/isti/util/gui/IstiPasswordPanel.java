//IstiPasswordPanel.java:  Defines a password panel.
//
//  2/05/2003 -- [KF]  Initial release version.
//  8/02/2004 -- [KF]  Added 'addActionListener' and 'removeActionListener'.
//  8/05/2004 -- [KF]  Added 'createRestrictedPasswordPanel' method and
//                     construtors with columns and not allowed characters.
//  8/11/2004 -- [KF]  Changed final class objects from private to public.
//  10/5/2004 -- [ET]  Added optional panel "prompt" text; added method
//                     'getUsernameFieldObj()'.
// 10/12/2004 -- [ET]  Added 'LOGIN_DIALOG_CANCEL_OPTION_STRING'.
// 10/15/2008 -- [ET]  Took out unneccessary call to 'requestDefaultFocus()'
//                     in constructor.
//  3/19/2012 -- [KF]  Added PasswordAuthenticator interface.
//

package com.isti.util.gui;

import java.net.PasswordAuthentication;
import java.util.Vector;
import java.util.Arrays;
import java.awt.*;
import java.awt.event.*;
import javax.swing.*;
import javax.swing.event.*;
import com.isti.util.BaseProperties;
import com.isti.util.PasswordAuthenticator;

/**
 * Class IstiPasswordPanel defines a password panel.
 */
public class IstiPasswordPanel extends JPanel implements PasswordAuthenticator
{
  protected final JLabel usernameLabel = new JLabel("Username: ");
  protected final JTextField usernameField;
  protected final JLabel passwordLabel = new JLabel("Password: ");
  protected final JPasswordField passwordField = new JPasswordField();
  protected final MultiLineJLabel panelPromptTextObj;
  protected final Vector listenerList = new Vector();

  /**
   * Login dialog default title string.
   */
  public static String LOGIN_DIALOG_DEFAULT_TITLE_STRING = "Login";

  /**
   * Login dialog default option string.
   */
  public static String LOGIN_DIALOG_DEFAULT_OPTION_STRING = "OK";

  /**
   * Login dialog cancel option string.
   */
  public static String LOGIN_DIALOG_CANCEL_OPTION_STRING = "Cancel";

  /**
   * The default number of characters in the username field.
   */
  public final static int USERNAME_COLUMNS = 16;

  /**
   * The default for not allowed characters in the username field.
   */
  public final static String USERNAME_NOT_ALLOWED_CHARS =
      BaseProperties.SEPARATOR_STRING;

  /**
   * Creates a password panel.
   * @see createPasswordPanel
   */
  public IstiPasswordPanel()
  {
    this(null, new JTextField(USERNAME_COLUMNS));
  }

  /**
   * Creates a password panel.
   * @param panelPromptStr the "prompt" text to be shown on the panel,
   * or null for none.
   * @see createPasswordPanel
   */
  public IstiPasswordPanel(String panelPromptStr)
  {
    this(panelPromptStr, new JTextField(USERNAME_COLUMNS));
  }

  /**
   * Creates a password panel.
   * @param columns the number of the columns for the username field.
   */
  public IstiPasswordPanel(int columns)
  {
    this(null, new JTextField(columns));
  }

  /**
   * Creates a password panel.
   * @param panelPromptStr the "prompt" text to be shown on the panel,
   * or null for none.
   * @param columns the number of the columns for the username field.
   */
  public IstiPasswordPanel(String panelPromptStr, int columns)
  {
    this(panelPromptStr, new JTextField(columns));
  }

  /**
   * Creates a password panel with restricted character input for the username.
   * @param columns the number of the columns for the field.
   * @param notAllowedChars a String of characters not allowed to be entered
   * into the username field.
   * @see createRestrictedPasswordPanel
   * @see USERNAME_COLUMNS, USERNAME_NOT_ALLOWED_CHARS
   */
  public IstiPasswordPanel(int columns,String notAllowedChars)
  {
    this(null,
        new FilteredJTextField(null,columns,notAllowedChars,false,0,false));
  }


  /**
   * Creates a password panel with restricted character input for the username.
   * @param panelPromptStr the "prompt" text to be shown on the panel,
   * or null for none.
   * @param columns the number of the columns for the field.
   * @param notAllowedChars a String of characters not allowed to be entered
   * into the username field.
   * @see createRestrictedPasswordPanel
   * @see USERNAME_COLUMNS, USERNAME_NOT_ALLOWED_CHARS
   */
  public IstiPasswordPanel(String panelPromptStr, int columns,
                                                     String notAllowedChars)
  {
    this(panelPromptStr,
        new FilteredJTextField(null,columns,notAllowedChars,false,0,false));
  }

  /**
   * Creates a password panel.
   * @param usernameField the username text field.
   */
  protected IstiPasswordPanel(JTextField usernameField)
  {
    this(null,usernameField);
  }

  /**
   * Creates a password panel.
   * @param panelPromptStr the "prompt" text to be shown on the panel,
   * or null for none.
   * @param usernameField the username text field.
   */
  protected IstiPasswordPanel(String panelPromptStr,
                                                   JTextField usernameField)
  {
    this.usernameField = usernameField;
//    usernameField.requestDefaultFocus();
    usernameField.requestFocus();
              //setup "prompt" text for panel:
    panelPromptTextObj = new MultiLineJLabel(panelPromptStr,25);
    try
    {
      jbInit();
    }
    catch(Exception e)
    {
      e.printStackTrace();
    }
  }

  /**
   * Requests that the current "initial" component have the keyboard focus.
   */
  public void setInitialFocus()
  {
    usernameField.requestFocus();
  }

  /**
   * Returns the password text and clear the password characters.
   * @return the password text.
   */
  public String getPassword()
  {
    final char [] charArr = passwordField.getPassword();
    final String retStr = new String(charArr);   //create string from chars
    Arrays.fill(charArr,(char)0);                //clear char array
    return retStr;
  }

  /**
   * Get the password authentication.
   * @return the password authentication or null if none.
   */
  public PasswordAuthentication getPasswordAuthentication()
  {
    return new PasswordAuthentication(getUsername(), getPasswordChars());
  }

  /**
   * Returns the password characters. For stronger security, it is recommended
   * that the returned character array be cleared after use by setting each
   * character to zero.
   * @return the password characters.
   */
  public char [] getPasswordChars()
  {
    return passwordField.getPassword();
  }

  /**
   * Sets the password text.
   * @param passwordText password text to use.
   */
  public void setPassword(String passwordText)
  {
    passwordField.setText(passwordText);
  }

  /**
   * Returns the username text.
   * @return the username text.
   */
  public String getUsername()
  {
    return usernameField.getText();
  }

  /**
   * Sets the username text.
   * @param usernameText username text to use.
   */
  public void setUsername(String usernameText)
  {
    usernameField.setText(usernameText);
  }

  /**
   * Returns the 'JTextField' object for the username.
   * @return The 'JTextField' object for the username.
   */
  public JTextField getUsernameFieldObj()
  {
    return usernameField;
  }

  /**
   * Initializes components for the panel.
   * @throws Exception if an error occurs.
   */
  private void jbInit() throws Exception
  {
    setLayout(new GridBagLayout());
    this.setBorder(BorderFactory.createEmptyBorder(5, 10, 10, 10));

    final GridBagConstraints constraintsObj = new GridBagConstraints();
    constraintsObj.gridx = constraintsObj.gridy = 0;
    constraintsObj.gridheight = 1;
    constraintsObj.weightx = constraintsObj.weighty = 0.0;

    final String str;
    if((str=panelPromptTextObj.getText()) != null && str.length() > 0)
    {
      constraintsObj.gridwidth = 2;
      constraintsObj.insets = new Insets(0,0,15,0);     //space after prompt
      add(panelPromptTextObj,constraintsObj);
      constraintsObj.insets = new Insets(0,0,0,0);
      ++constraintsObj.gridy;
    }

    constraintsObj.gridwidth = 1;
    constraintsObj.fill = GridBagConstraints.NONE;
    add(usernameLabel,constraintsObj);
    ++constraintsObj.gridx;
    constraintsObj.weightx = constraintsObj.weighty = 1.0;
    constraintsObj.fill = GridBagConstraints.HORIZONTAL;
    add(usernameField,constraintsObj);
    --constraintsObj.gridx;
    ++constraintsObj.gridy;
    constraintsObj.weightx = constraintsObj.weighty = 0.0;
    constraintsObj.fill = GridBagConstraints.NONE;
    add(passwordLabel,constraintsObj);
    ++constraintsObj.gridx;
    constraintsObj.weightx = constraintsObj.weighty = 1.0;
    constraintsObj.fill = GridBagConstraints.HORIZONTAL;
    add(passwordField,constraintsObj);

    final UpdateListener updateListener = new UpdateListener();
    usernameField.getDocument().addDocumentListener(updateListener);
    passwordField.getDocument().addDocumentListener(updateListener);
  }

  /**
   * Adds an <code>ActionListener</code> to the panel.
   * @param l the <code>ActionListener</code> to be added.
   */
  public void addActionListener(ActionListener l)
  {
    listenerList.add(l);
  }

  /**
   * Removes an <code>ActionListener</code> from the panel.
   * If the listener is the currently set <code>Action</code>
   * for the panel, then the <code>Action</code>
   * is set to <code>null</code>.
   * @param l the listener to be removed.
   */
  public void removeActionListener(ActionListener l)
  {
    listenerList.remove(l);
  }

  /**
   * Notifies all listeners that have registered interest for
   * notification on this event type.  The event instance
   * is lazily created using the parameters passed into
   * the fire method.
   * @param event  the <code>ChangeEvent</code> object
   * @see EventListenerList
   */
  protected void fireActionPerformed(ActionEvent event)
  {
    Object[] listeners = listenerList.toArray();

    // Process the listeners last to first, notifying
    // those that are interested in this event
    for (int i = listeners.length-1; i >= 0; i--)
    {
      ((ActionListener)listeners[i]).actionPerformed(event);
    }
  }


  /**
   * Update listener class.
   */
  protected class UpdateListener implements DocumentListener
  {
    /**
     * Gives notification that there was an insert into the document.  The
     * range given by the DocumentEvent bounds the freshly inserted region.
     *
     * @param e the document event
     */
    public void insertUpdate(DocumentEvent e)
    {
      update(e);
    }

    /**
     * Gives notification that a portion of the document has been
     * removed.  The range is given in terms of what the view last
     * saw (that is, before updating sticky positions).
     *
     * @param e the document event
     */
    public void removeUpdate(DocumentEvent e)
    {
      update(e);
    }

    /**
     * Gives notification that an attribute or set of attributes changed.
     *
     * @param e the document event
     */
    public void changedUpdate(DocumentEvent e)
    {
      update(e);
    }

    private void update(DocumentEvent e)
    {
      fireActionPerformed(new ActionEvent(this, 0, e.getType().toString()));
    }
  }

  /**
   * Creates a password panel.
   * @return a new password panel object.
   */
  public static IstiPasswordPanel createPasswordPanel()
  {
    return new IstiPasswordPanel();
  }

  /**
   * Creates a password panel.
   * @param panelPromptStr the "prompt" text to be shown on the panel,
   * or null for none.
   * @return a new password panel object.
   */
  public static IstiPasswordPanel createPasswordPanel(String panelPromptStr)
  {
    return new IstiPasswordPanel(panelPromptStr);
  }

  /**
   * Creates a password panel with restricted character input for the
   * username.
   * @return a new password panel object with restricted character
   * input for the username.
   */
  public static IstiPasswordPanel createRestrictedPasswordPanel()
  {
    return new IstiPasswordPanel(
                              USERNAME_COLUMNS, USERNAME_NOT_ALLOWED_CHARS);
  }

  /**
   * Creates a password panel with restricted character input for the
   * username.
   * @param panelPromptStr the "prompt" text to be shown on the panel,
   * or null for none.
   * @return a new password panel object with restricted character
   * input for the username.
   */
  public static IstiPasswordPanel createRestrictedPasswordPanel(
                                                      String panelPromptStr)
  {
    return new IstiPasswordPanel(
              panelPromptStr, USERNAME_COLUMNS, USERNAME_NOT_ALLOWED_CHARS);
  }

  /**
   * @return login dialog
   * @param parentComp the parent component for the popup.
   */
  public static IstiDialogPopup createLoginDialog(Component parentComp)
  {
    return createLoginDialog(
        parentComp, createPasswordPanel(), LOGIN_DIALOG_DEFAULT_TITLE_STRING,
        LOGIN_DIALOG_DEFAULT_OPTION_STRING);
  }

  /**
   * @return login dialog
   * @param parentComp the parent component for the popup.
   * @param msgObj the message object to be shown.
   */
  public static IstiDialogPopup createLoginDialog(Component parentComp,
      Object msgObj)
  {
    return createLoginDialog(
        parentComp, msgObj, LOGIN_DIALOG_DEFAULT_TITLE_STRING,
        LOGIN_DIALOG_DEFAULT_OPTION_STRING);
  }

  /**
   * @return login dialog
   * @param parentComp the parent component for the popup.
   * @param msgObj the message object to be shown.
   * @param titleStr the title string for popup window.
   */
  public static IstiDialogPopup createLoginDialog(Component parentComp,
      Object msgObj, String titleStr)
  {
    return createLoginDialog(parentComp, msgObj, titleStr,
                             LOGIN_DIALOG_DEFAULT_OPTION_STRING);
  }

  /**
   * @return login dialog
   * @param parentComp the parent component for the popup.
   * @param msgObj the message object to be shown.
   * @param titleStr the title string for popup window.
   * @param optionObj the Object that defines the button, or null to display
   * no buttons.
   */
  public static IstiDialogPopup createLoginDialog(Component parentComp,
      Object msgObj, String titleStr, Object optionObj)
  {
    IstiDialogPopup popup =
        new IstiDialogPopup(parentComp, msgObj, titleStr, optionObj);
    return popup;
  }


//  public static void main(String[] args)
//  {
//    final JFrame mainFrame = new JFrame("IstiPasswordPanelTest");
//
//    //set "look & feel" to be that of host system:
//    SetupLookAndFeel.setLookAndFeel();
//
//    mainFrame.setSize(640,480);        //set frame size
//    mainFrame.addWindowListener(       //setup to exit on window close
//      new WindowAdapter()
//      {  public void windowClosing(WindowEvent e)
//         {
//           System.exit(0);             //exit application
//         }
//      }
//    );
//
//    final JButton modalButtonObj = new JButton("Show");
//    modalButtonObj.addActionListener(new ActionListener()
//         {
//           public void actionPerformed(ActionEvent evtObj)
//           {
//             final IstiPasswordDialog dialogObj = new IstiPasswordDialog(
//                                mainFrame,LOGIN_DIALOG_DEFAULT_TITLE_STRING,
//                                           "Enter your login information:");
//             dialogObj.setUsernameAsInitalFocus();
//             dialogObj.show();
//           }
//         });
//                                       //get content pane for frame:
//    final Container contentPane = mainFrame.getContentPane();
//    contentPane.setLayout(new FlowLayout());               //set layout
//    contentPane.add(modalButtonObj);
//
//                             //get screen size:
//    Dimension d = Toolkit.getDefaultToolkit().getScreenSize();
//                             //put frame at center of screen:
//    mainFrame.setLocation((d.width-mainFrame.getSize().width)/2,
//                                   (d.height-mainFrame.getSize().height)/2);
//    mainFrame.setVisible(true);        //make frame visible
//  }
}
