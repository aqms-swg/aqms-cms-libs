//IstiPopupMenu.java:  Extends the JPopupMenu.
//
//   10/01/2002 -- [KF]
//

package com.isti.util.gui;

import java.awt.Component;
import javax.swing.JPopupMenu;
import java.awt.Point;

/**
 * Class IstiPopupMenu Extends the JPopupMenu.
 */
public class IstiPopupMenu extends JPopupMenu
{
  //last location the popup menu was shown
  private Point lastShowLocation = null;

  /**
   * Displays the popup menu at the position x,y in the coordinate
   * space of the component invoker.
   *
   * @param invoker the component in whose space the popup menu is to appear
   * @param x the x coordinate in invoker's coordinate space at which
   * the popup menu is to be displayed
   * @param y the y coordinate in invoker's coordinate space at which
   * the popup menu is to be displayed
   */
  public void show(Component invoker, int x, int y)
  {
    super.show(invoker, x, y);
    lastShowLocation = new Point(x, y);
  }

  /**
   * Gets the last location in the invoker's coordinate space at which
   * the popup menu was displayed.
   * @return the Point representing the last location or null if not available.
   */
  public Point getLastShowLocation()
  {
    return lastShowLocation;
  }
}