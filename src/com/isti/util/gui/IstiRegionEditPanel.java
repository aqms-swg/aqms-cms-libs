//IstiRegionEditPanel.java:  Defines a region edit panel.
//
//   4/1/2004 -- [KF]  Initial release version.
//   4/5/2004 -- [KF]  Remove inclusive button.
//   4/6/2004 -- [KF]  Use 'showOptionDialog()' method.
//   4/7/2004 -- [ET]  Changed 'showOptionDialog()' reference to
//                     'showAndReturnIndex()'.
//   4/8/2004 -- [KF]  Always use new dialog so that placement is relative to
//                     the parent's current location.
//   4/9/2004 -- [KF]  Only put in commas in the coordinates portion.
//  4/13/2004 -- [KF]  Add region name and magnitude text fields.
//  4/16/2004 -- [KF]  Add "Help" button.
//  4/19/2004 -- [KF]  Only set the pane obj value to 'UNINITIALIZED_VALUE'
//                     when the "Help" button is pressed,
//                     fix magnitude text entry.
//  4/20/2004 -- [KF]  Replace "Add" button with "Add Circle" and "Add Poly".
//  4/22/2004 -- [KF]  Show an error message when the region data is invalid
//                     and re-open the dialog.
//  4/23/2004 -- [KF]  Place region edit panel on the right side of the display.
//  4/29/2004 -- [ET]  Added 'modalCenterFlag' parameter to 'createDialog()'
//                     method.
//  6/10/2004 -- [ET]  Changed 'IstiRegion' reference from "com.isti.util"
//                     to "com.isti.util.gis" package.
//  6/15/2004 -- [ET]  Split 'modalCenterFlag' parameter on 'createDialog()'
//                     method into 'modalFlag' and 'centerFlag'; added
//                     'closeDialog()' and 'requestFocus()' methods.
//  7/15/2004 -- [ET]  Added 'removeAllActivePanelActionListeners()' method.
//   3/9/2006 -- [KF]  Added more region options.
//  4/18/2006 -- [ET]  Modified to allow 'setRegionString()' method to
//                     parse a region-text string containing only a name.
// 11/17/2021 -- [KF]  Added 'isActivePanel' method.
//

package com.isti.util.gui;

import java.awt.BorderLayout;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.beans.PropertyEditor;
import java.util.List;
import java.util.Vector;

import javax.swing.BorderFactory;
import javax.swing.Box;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;

import com.isti.util.IstiNamedValueInterface;
import com.isti.util.PropItemInterface;
import com.isti.util.UtilFns;
import com.isti.util.gis.GisUtils;
import com.isti.util.gis.IstiRegion;
import com.isti.util.propertyeditor.IstiNamedValueComponentPanel;

/**
 * Class IstiRegionEditPanel defines a region edit panel.
 */
public class IstiRegionEditPanel extends JPanel
{
  /** show dialog command */
  public final static String showDialogCommandString = "show dialog";

  /** close dialog command */
  public final static String closeDialogCommandString = "close dialog";

  /** OK option text */
  public final static String OK_OPTION_TEXT = "OK";

  /** Help option text. */
  public final static String HELP_OPTION_TEXT =
                                           IstiRegionPanel.HELP_OPTION_TEXT;

  private static Vector listenerList = new Vector();  //listener list

  /** The character that separates coordinates. */
  private final static String TEXT_AREA_COORD_SEP_CHAR = "\n";

  private IstiDialogPopup dialogObj = null;           //dialog object

  /** Dialog utility object for popup messages. */
  protected final IstiDialogUtil dialogUtilObj = new IstiDialogUtil(this);

  //options component panel
  private final IstiNamedValueComponentPanel optionsComponentPanel =
      new IstiNamedValueComponentPanel();
  private static final int NUM_ROWS = 4;
  private static final int NUM_COLUMNS = 40;
  private final JTextArea textArea =
      new JTextArea(UtilFns.EMPTY_STRING, NUM_ROWS, NUM_COLUMNS);

  //currently active region edit panel sync object
  private static Object activeIstiRegionEditPanelSyncObj = new Object();
  //currently active region edit panel or null if none
  private static IstiRegionEditPanel activeIstiRegionEditPanel = null;
  //circle or polygon
  private boolean circleFlag = false;
  //region panel
  private IstiRegionPanel regionPanel = null;
  //add or edit
  private boolean addFlag = true;
  //circle lat/lon point
  private IstiRegion.LatLonPoint circleLatLonPoint = null;
  //option groups
  private final List optionGroups;

  /**
   * Creates an empty region edit panel with all 'IstiRegion' option groups.
   */
  public IstiRegionEditPanel()
  {
    this(null);
  }

  /**
   * Creates an empty region edit panel.
   * @param optionGroups the 'IstiRegion' option groups or null for all.
   */
  public IstiRegionEditPanel(List optionGroups)
  {
    this(optionGroups,UtilFns.EMPTY_STRING);
  }

  /**
   * Creates a region edit panel.
   * @param optionGroups the 'IstiRegion' option groups or null for all.
   * @param str the region string.
   */
  public IstiRegionEditPanel(List optionGroups,String str)
  {
    this.optionGroups = optionGroups;
    try
    {
      jbInit();         //initialize components
    }                   // (JBuilder likes to have a 'jbInit()' method)
    catch(Exception ex)  //error initializing components
    {
      ex.printStackTrace();       //show stack trace & try to continue
    }
    setRegionString(str);  //set the region string
  }

  /**
   * Performs component initialization.
   * @throws Exception if an error occurs.
   */
  protected final void jbInit() throws Exception
  {
    //place buttons vertically on buttons panel:
    final JPanel buttonsPanel = new JPanel(new GridBagLayout());
    final GridBagConstraints constraintsObj = new GridBagConstraints();
    constraintsObj.anchor = GridBagConstraints.CENTER;
    constraintsObj.fill = GridBagConstraints.HORIZONTAL;
    constraintsObj.insets = new Insets(2,0,2,0);
    constraintsObj.weightx = 1.0;
    constraintsObj.weighty = 0.25;

    constraintsObj.gridx = constraintsObj.gridy = 0;
    constraintsObj.gridwidth = constraintsObj.gridheight = 1;
    ++(constraintsObj.gridy);
    //add struct below buttons to push them up a little:
    ++(constraintsObj.gridy);
    constraintsObj.weighty = 0.75;     //increase spacing around strut
    buttonsPanel.add(Box.createVerticalStrut(1),constraintsObj);
    //put empty border around buttons panel:
    buttonsPanel.setBorder(BorderFactory.createEmptyBorder(10,5,10,10));

    //put GUI list of regions into a scroll pane:
    final JScrollPane listScrollPane = new JScrollPane(textArea,
        JScrollPane.VERTICAL_SCROLLBAR_AS_NEEDED,
        JScrollPane.HORIZONTAL_SCROLLBAR_AS_NEEDED);
    //put scroll pane into a panel:
    final JPanel scrollPanePanel = new JPanel(new BorderLayout());
    scrollPanePanel.add(listScrollPane,BorderLayout.CENTER);

    final JPanel mainPanel = new JPanel(new GridBagLayout());
    constraintsObj.gridy = 0;
    constraintsObj.gridx = 0;
    constraintsObj.gridheight = 1;
    constraintsObj.gridwidth = 2;
    IstiNamedValueInterface defaultOption;
    PropItemInterface defaultItem;
    Object groupSelObj;
    final IstiNamedValueInterface[] defaultOptions =
        IstiRegion.getDefaultOptions();
    for (int i = 0; i < defaultOptions.length; i++)
    {
      defaultOption = defaultOptions[i];
      if (optionGroups != null && defaultOption instanceof PropItemInterface)
      {
        defaultItem = (PropItemInterface)defaultOption;
        groupSelObj = defaultItem.getGroupSelObj();
        if (groupSelObj != null && optionGroups.indexOf(groupSelObj) < 0)
        {
          continue;  //skip the item
        }
      }
      optionsComponentPanel.addProperty(defaultOption);
    }
    ++ (constraintsObj.gridy);
    constraintsObj.gridx = 0;
    mainPanel.add(optionsComponentPanel, constraintsObj);

    constraintsObj.gridwidth = 1;
    ++(constraintsObj.gridy);
    constraintsObj.gridx = 0;
    mainPanel.add(new JLabel("Region data: "),constraintsObj);
    ++(constraintsObj.gridx);
    mainPanel.add(scrollPanePanel,constraintsObj);

    setLayout(new BorderLayout());
    add(mainPanel,BorderLayout.CENTER);
    add(buttonsPanel,BorderLayout.EAST);
  }

  /**
   * Adds an <code>ActionListener</code> to the active panel.
   * @param l the <code>ActionListener</code> to be added
   */
  public static void addActivePanelActionListener(ActionListener l)
  {
    listenerList.add(l);
  }

  /**
   * Add a lat/lon point to the region.
   * @param lat the latitude of the point.
   * @param lon the longitude of the point.
   */
  public void addPoint(double lat, double lon)
  {
    final String text = textArea.getText();
    if (isCircle())
    {
      if (circleLatLonPoint == null)
      {
        circleLatLonPoint = new IstiRegion.LatLonPoint(lat, lon);
        textArea.setText(IstiRegion.coordToString(lat, lon));
      }
      else
      {
        // calculate the radius
        final double radius = GisUtils.sphereDist(
            circleLatLonPoint.getLat(), circleLatLonPoint.getLon(), lat, lon);
        textArea.append(IstiRegion.radiusToString(radius));

        //notify listeners of the change, re-open to erase
        notifyOpenEvent();

        dialogObj.requestFocus();  //request focus on the dialog

        circleLatLonPoint = null;
      }
    }
    else
    {
      if (text.length() > 0 && !text.endsWith(TEXT_AREA_COORD_SEP_CHAR))
        textArea.append(TEXT_AREA_COORD_SEP_CHAR);
      textArea.append(IstiRegion.coordToString(lat, lon));
    }
  }

  /**
   * Add a lat/lon point to the active panel.
   * @param lat the latitude of the point.
   * @param lon the longitude of the point.
   */
  public static void addPointToActivePanel(double lat, double lon)
  {
    synchronized (activeIstiRegionEditPanelSyncObj)
    {
      if (activeIstiRegionEditPanel != null)
      {
        activeIstiRegionEditPanel.addPoint(lat, lon);
      }
    }
  }

  /**
   * Determines if there is an active panel.
   * @return true if active panel, false otherwise.
   */
  public static boolean isActivePanel()
  {
    synchronized (activeIstiRegionEditPanelSyncObj)
    {
      if (activeIstiRegionEditPanel != null)
      {
        return true;
      }
    }
    return false;
  }

  /**
   * Creates a dialog for this Client Configuration Panel.
   * @param regionPanel the region panel.
   * @param addFlag true for add, false for edit.
   * @param parentComp the <code>Component</code> to check for a
   *		<code>Frame</code> or <code>Dialog</code>
   * @param modalFlag true for a modal dialog, false for modeless (allows
   * other windows to run).
   * @param centerFlag true to have the dialog centered over its
   * parent; false to have the dialog on the top-right part of the
   * display screen.
   */
  public void createDialog(final IstiRegionPanel regionPanel,
                                      boolean addFlag, Component parentComp,
                                      boolean modalFlag, boolean centerFlag)
  {
    final int msgType = JOptionPane.PLAIN_MESSAGE;
    final int optionType = IstiDialogPopup.DEFAULT_OPTION;
    final Object [] optionsArr = regionPanel.isHelpActionListenerInstalled()
             ? new Object[] { OK_OPTION_TEXT, "Cancel", HELP_OPTION_TEXT } :
                                  new Object[] { OK_OPTION_TEXT, "Cancel" };
    this.regionPanel = regionPanel;
    this.addFlag = addFlag;

    // create the dialog
    dialogObj = new IstiDialogPopup(
        parentComp, this, UtilFns.EMPTY_STRING, msgType, optionsArr,
        0, modalFlag, false, optionType);
    final JOptionPane paneObj = dialogObj.getOptionPaneObj();
    if (!centerFlag)         //if not centered over parent
    {
      try
      {
        //get display screen size:
        final Dimension screenSizeDimObj = SplashWindow.getDisplayScreenSize();
        dialogObj.setLocation(
            screenSizeDimObj.width - dialogObj.getDialogObj().getWidth(), 2);
      }
      catch(Exception ex)
      {
      }
    }
    dialogObj.setHideDialogOnButtonFlag(false);
    dialogObj.addPropertyChangeListener(new PropertyChangeListener()
    {
      /**
       * This method gets called when a bound property is changed.
       * @param evt A PropertyChangeEvent object describing the event source
       *   	and the property that has changed.
       */

      public void propertyChange(PropertyChangeEvent evt)
      {
        final Object newValue = evt.getNewValue();
        final String prop = evt.getPropertyName();

        if (dialogObj.isVisible()
            && (evt.getSource() == paneObj)
            && (JOptionPane.VALUE_PROPERTY.equals(prop) ||
            JOptionPane.INPUT_VALUE_PROPERTY.equals(prop))) {
          Object value = paneObj.getValue();

          if (value == JOptionPane.UNINITIALIZED_VALUE) {
            //ignore reset
            return;
          }

          if (HELP_OPTION_TEXT.equals(newValue))
          {
            if (regionPanel != null)
              regionPanel.notifyHelpActionListener(
                  IstiRegionEditPanel.this);  //notify the help action listener

            //Reset the JOptionPane's value.
            //If you don't do this, then if the user
            //presses the same button next time, no
            //property change event will be fired.
            paneObj.setValue(JOptionPane.UNINITIALIZED_VALUE);
            return;
          }
          if (OK_OPTION_TEXT.equals(newValue))
          {
            final String str = getRegionString();
            final IstiRegion region = IstiRegion.parse(str);
            if (region == null ||
                !((isCircle() ? region instanceof IstiRegion.Circle :
                region instanceof IstiRegion.Polygon)))
            {
              dialogUtilObj.popupErrorMessage("Invalid region data");

              //Reset the JOptionPane's value.
              //If you don't do this, then if the user
              //presses the same button next time, no
              //property change event will be fired.
              paneObj.setValue(JOptionPane.UNINITIALIZED_VALUE);
              return;
            }
          }
          dialogObj.setVisible(false);
        }
      }
    });
  }

  /**
   * Gets the dialog for this panel.
   * @return The dialog object for this panel, or null if none has been
   * created.
   */
  public IstiDialogPopup getDialog()
  {
    return dialogObj;
  }

  /**
   * Closes the dialog for this panel.
   */
  public void closeDialog()
  {
    if(dialogObj != null)
      dialogObj.close();
  }

  /**
   * Overridden version that also calls 'requestFocus()' on the dialog
   * hosting this panel.
   */
  public void requestFocus()
  {
    if(dialogObj != null)
      dialogObj.requestFocus();
    super.requestFocus();
  }

  /**
   * Gets the region string.
   * @return the region string.
   */
  public String getRegionString()
  {
    String dataText = textArea.getText().trim();
    String str = UtilFns.EMPTY_STRING;

    IstiNamedValueInterface option;
    final IstiNamedValueInterface[] options = optionsComponentPanel.getProperties();
    for (int i = 0; i < options.length; i++)
    {
      option = options[i];
      if (IstiRegion.OPID_NAME.equals(option.getName()))
      {
        final String regionName = option.stringValue();
        if (regionName.length() > 0)
        {
          str += IstiRegion.NAME_BEGIN_CHAR + regionName +
              IstiRegion.NAME_END_CHAR + " ";
        }
        if (dataText.length() > 0)
        {
          dataText = replace(dataText, TEXT_AREA_COORD_SEP_CHAR.charAt(0),
                             IstiRegion.COORD_SEP_CHAR.charAt(0));
          str += dataText;
        }
      }
      else if (!option.isDefaultValue())
      {
        str += " " + IstiRegion.OPTION_BEGIN_CHAR + option.toString() +
            IstiRegion.OPTION_END_CHAR;
      }
    }
    str = str.replace(TEXT_AREA_COORD_SEP_CHAR.charAt(0), ' ');
    return str;
  }

  /**
   * Determines if the edit panel is for a circle or a polygon.
   * @return true for circle, false for polygon.
   */
  public boolean isCircle()
  {
    return circleFlag;
  }

  /**
   * Removes an <code>ActionListener</code> from the active panel.
   * If the listener is the currently set <code>Action</code>
   * for the button, then the <code>Action</code>
   * is set to <code>null</code>.
   *
   * @param l the listener to be removed
   */
  public static void removeActivePanelActionListener(ActionListener l)
  {
    listenerList.remove(l);
  }

  /**
   * Removes all ActionListeners from the active panel.
   */
  public static void removeAllActivePanelActionListeners()
  {
    listenerList.clear();
  }

  /**
   * Set the edit panel for circle or polygon.
   * @param b true for circle, false for polygon.
   */
  public void setCircle(boolean b)
  {
    circleFlag = b;
  }

  /**
   * Sets the region string.
   * @param str the region string.
   */
  public final void setRegionString(String str)
  {
    final IstiRegion regionObj = IstiRegion.parse(str,true);

    if (regionObj != null)
    {
      IstiNamedValueInterface defaultOption;
      PropertyEditor propertyEditor;
      final IstiNamedValueInterface[] defaultOptions =
          optionsComponentPanel.getDefaultProperties();
      for (int optionIndex = 0; optionIndex < defaultOptions.length;
           optionIndex++)
      {
        defaultOption = defaultOptions[optionIndex];
        propertyEditor = optionsComponentPanel.getPropertyEditor(defaultOption);
        if (propertyEditor != null)
        {
          final String optionText =
              regionObj.getOptionStringValue(defaultOption.getName());
          if (optionText != null)
            propertyEditor.setAsText(optionText);
        }
        else
        {
          System.out.println("No editor for: " + defaultOption.getName());
        }
      }
    }

    if (str.length() > 0)
    {
      //remove name from the string
      int nameBeginIndex, nameEndIndex;
      do
      {
        nameBeginIndex = str.indexOf(IstiRegion.NAME_BEGIN_CHAR);
        nameEndIndex = str.lastIndexOf(IstiRegion.NAME_END_CHAR);
        if (nameBeginIndex >= 0 && nameBeginIndex < nameEndIndex)  //if name found
        {
          //remove the name
          str = str.substring(0, nameBeginIndex) + str.substring(nameEndIndex+1);
        }
      }
      while (nameBeginIndex >= 0 || nameEndIndex >= 0);

      //remove all options from the string
      int optionBeginIndex, optionEndIndex;
      do
      {
        optionBeginIndex = str.indexOf(IstiRegion.OPTION_BEGIN_CHAR);
        optionEndIndex = str.indexOf(IstiRegion.OPTION_END_CHAR);
        if (optionBeginIndex >= 0 && optionBeginIndex < optionEndIndex)  //if option found
        {
          //remove the option
          str = str.substring(0, optionBeginIndex) + str.substring(optionEndIndex+1);
        }
      }
      while (optionBeginIndex >= 0 || optionEndIndex >= 0);

      //replace the character that separates coordinates with our own
      str = replace(str, IstiRegion.COORD_SEP_CHAR.charAt(0),
                    TEXT_AREA_COORD_SEP_CHAR.charAt(0));
      if (regionObj != null)
      {
        //set the circle flag if the region is a circle
        setCircle(regionObj instanceof IstiRegion.Circle);
      }
    }
    textArea.setText(str.trim());
  }

  /**
   * Shows the dialog with the Client Configuration Panel
   * after the configuration settings are loaded.
   * Note that the dialog needs to be created with the createDialog method.
   * @see #createDialog()
   * @return An integer indicating the option chosen by the user,
   * or CLOSED_OPTION if the user closed the dialog without selecting
   * an option or if the dialog doesn't exist.
   */
  public int showDialog()
  {
    if(dialogObj == null)
      return IstiDialogPopup.CLOSED_OPTION;

    String titleStr;
    if (addFlag)
      titleStr = "Add";
    else
      titleStr = "Edit";
    if (circleFlag)
      titleStr += " Circle";
    else
      titleStr += " Polygon";
    titleStr += " Region";
    dialogObj.setTitleStr(titleStr);

    synchronized (activeIstiRegionEditPanelSyncObj)
    {
      //make sure there is only 1 active region edit panel
      if (activeIstiRegionEditPanel != null)
      {
        activeIstiRegionEditPanel.getDialog().close();
        activeIstiRegionEditPanel = null;
      }
      activeIstiRegionEditPanel = this;
    }

    //notify listeners of the change
    notifyOpenEvent();

    dialogObj.pack();
    final int retVal = dialogObj.showAndReturnIndex();
    synchronized (activeIstiRegionEditPanelSyncObj)
    {
      activeIstiRegionEditPanel = null;
    }

    //notify listeners of the change
    notifyCloseEvent();
    return retVal;
  }

  /**
   * Clears the region informaton.
   */
  protected void clearRegion()
  {
    if(dialogUtilObj.popupYesNoConfirmMessage(
        ("Are you sure you want to clear the region?"),
        "Confirm Clear",true) != IstiDialogPopup.YES_OPTION)
    {
      //user selected "No"
      return;            //cancel removal
    }
    setRegionString(UtilFns.EMPTY_STRING);
  }

  /**
   * Notifies all listeners that have registered interest for
   * notification on this event type.  The event instance
   * is lazily created using the parameters passed into
   * the fire method.
   *
   * @param event  the <code>ChangeEvent</code> object
   * @see EventListenerList
   */
  protected static void fireActivePanelActionPerformed(ActionEvent event)
  {
    Object[] listeners = listenerList.toArray();

    // Process the listeners last to first, notifying
    // those that are interested in this event
    for (int i = listeners.length-1; i >= 0; i--)
    {
      ((ActionListener)listeners[i]).actionPerformed(event);
    }
  }

  /**
   * Notifies a close event.
   */
  protected void notifyCloseEvent()
  {
    //notify listeners of the change
    fireActivePanelActionPerformed(
        new ActionEvent(this, ActionEvent.ACTION_PERFORMED,
        closeDialogCommandString));
    if (regionPanel != null)
      regionPanel.dialogObj.requestFocus();  //request focus on the dialog
  }

  /**
   * Notifies an open event.
   */
  protected void notifyOpenEvent()
  {
    fireActivePanelActionPerformed(
        new ActionEvent(this, ActionEvent.ACTION_PERFORMED,
        showDialogCommandString));
  }

  /**
   * Replace the old char with the new char in the coordinate part of the
   * specified region string.
   * @param str the region string.
   * @param oldChar the old character.
   * @param newChar the new character.
   * @return the updated region string.
   */
  protected static String replace(String str, char oldChar, char newChar)
  {
    final int coordStart = str.indexOf(IstiRegion.COORD_BEGIN_CHAR);
    final int coordEnd = str.lastIndexOf(IstiRegion.COORD_END_CHAR);
    final String startStr, endStr;
    if (coordStart >= 0 && coordEnd >= 0)
    {
      startStr = str.substring(0, coordStart);
      endStr = str.substring(coordEnd+1);
      str = str.substring(coordStart, coordEnd+1);
    }
    else
    {
      startStr = UtilFns.EMPTY_STRING;
      endStr = UtilFns.EMPTY_STRING;
    }
    str = str.replace(oldChar, newChar);
    return startStr + str + endStr;
  }
}
