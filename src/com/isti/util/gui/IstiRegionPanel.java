//IstiRegionPanel.java:  Defines a region panel.
//
//  3/30/2004 -- [KF]  Initial release version.
//   4/9/2004 -- [KF]  Display region name if single layer is removed.
//  4/12/2004 -- [KF]  Notify listeners of changes immediately.
//  4/16/2004 -- [KF]  Add "Help" button.
//  4/20/2004 -- [KF]  Replace "Add" button with "Add Circle" and "Add Poly".
//  4/23/2004 -- [KF]  Place region edit panel on the right side of the display.
//  4/23/2004 -- [ET]  Added version of 'createDialog()' method without
//                     'regionEditParentComp' parameter; added tooltips to
//                     buttons.
//  4/29/2004 -- [ET]  Modified 'createDialog()' so that the "Close" button
//                     results in 'dispose()' being called instead of
//                     'hide()'; added 'showDialog()' method and option
//                     of using modal dialogs.
//   5/3/2004 -- [ET]  Modified to select newly-added region.
//  6/10/2004 -- [ET]  Changed 'IstiRegion' reference from "com.isti.util"
//                     to "com.isti.util.gis" package.
//  6/15/2004 -- [ET]  Added 'modalFlag' parameter to 'showDialog()' and
//                     'createDialog()' methods; modified 'showDialog()'
//                     to default to non-modal dialog; split
//                     'modalCenterFlag' parameter on IstiRegionEditPanel
//                     'createDialog()' method into 'modalFlag' and
//                     'centerFlag'; added 'isDialogVisible()',
//                     'getDialog()' and 'requestFocus()' methods.
//  4/26/2005 -- [ET]  Fixed 'editSelectedRegion()' to set 'addFlag'
//                     parameter to 'false' in call to regionEditPanel
//                     'createDialog()' to make correct add/edit panel
//                     title be used (bug introduced in 6/15/2004 mods);
//                     added support for editing region entry via mouse
//                     double-click; made "Confirm Removal" dialog
//                     non-modal.
//   3/7/2006 -- [KF]  Added more region options.
//  3/22/2006 -- [ET]  Modified to disable region-edit via mouse double-
//                     click while add/edit-region dialog is active.
//  4/19/2006 -- [ET]  Modified 'editSpecifiedRegion()' to only notify
//                     listeners if region data changed; added
//                     'setIstiRegionMgrObj()' and 'setNewRegionNameStr()'
//                     methods; modified to provide default name when
//                     adding new region; added "Copy" button and
//                     implementation.
//  5/17/2006 -- [ET]  Added empty-JList checks to avoid Java bug
//                     4816818/4178930.
//

package com.isti.util.gui;

import java.util.List;
import java.util.Vector;
import java.awt.BorderLayout;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.event.*;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.Toolkit;
import javax.swing.*;
import java.beans.PropertyChangeListener;
import java.beans.PropertyChangeEvent;
import com.isti.util.UtilFns;
import com.isti.util.gis.IstiRegion;
import com.isti.util.gis.IstiRegionMgrIntf;

/**
 * Class IstiRegionPanel defines a region panel.
 */
public class IstiRegionPanel extends JPanel implements WindowListener
{
    /** Help option text. */
  public static final String HELP_OPTION_TEXT = "Help";
    /** Close option text. */
  public static final String CLOSE_OPTION_TEXT = "Close";
    /** Confirm removal text */
  private static final String CONFIRM_REMOVAL_TEXT = "Confirm Removal";
    /** Default region name text. */
  protected static final String DEF_NEWREGION_NAME = "Region";

  private final Vector panelListenerList = new Vector();   //listener list

  //the display size:
  private final Dimension screenSize = Toolkit.getDefaultToolkit().getScreenSize();

  /** Dialog utility object for popup messages. */
  protected final IstiDialogUtil dialogUtilObj = new IstiDialogUtil(this);

  /** Flag set true if the dialogs are modal. */
  protected boolean dialogModalFlag = false;
  /** Flag set true if the edit dialog should be centered. */
  protected boolean centerEditDialogFlag = false;
  /** Flag set true if region-edit via double-click on list is enabled. */
  protected boolean mouseClickEditFlag = true;
  /** Name to be pre-filled in new regions. */
  protected String newRegionNameStr = DEF_NEWREGION_NAME;

  /** Dialog object for this panel. */
  protected IstiDialogPopup dialogObj = null;

  /** The region edit panel. */
  private IstiRegionEditPanel regionEditPanel = null;

  /** The help action listener. */
  private ActionListener helpActionListener = null;

  /** Action object for the "Add Circle" button. */
  public final AbstractAction addCircleButtonActionObj =
    new AbstractAction("Add Circle")
      {
        public void actionPerformed(ActionEvent evtObj)
        {
          addNewRegion(true);
        }
      };

  /** Action object for the "Add Poly" button. */
  public final AbstractAction addPolyButtonActionObj =
    new AbstractAction("Add Poly")
      {
        public void actionPerformed(ActionEvent evtObj)
        {
          addNewRegion(false);
        }
      };

  /** Action object for the "Edit" button. */
  public final AbstractAction editButtonActionObj =
    new AbstractAction("Edit")
      {
        public void actionPerformed(ActionEvent evtObj)
        {
          editSelectedRegion();
        }
      };

  /** Action object for the "Remove" button. */
  public final AbstractAction removeButtonActionObj =
    new AbstractAction("Remove")
      {
        public void actionPerformed(ActionEvent evtObj)
        {
          removeSelectedRegions();
        }
      };

  /** Action object for the "Copy" button. */
  public final AbstractAction copyButtonActionObj =
    new AbstractAction("Copy")
      {
        public void actionPerformed(ActionEvent evtObj)
        {
          copyToNewRegion();
        }
      };

  //create and setup buttons:
  private final JButton addCircleButton = new JButton(addCircleButtonActionObj);
  private final JButton addPolyButton = new JButton(addPolyButtonActionObj);
  private final JButton editButton = new JButton(editButtonActionObj);
  private final JButton removeButton = new JButton(removeButtonActionObj);
  private final JButton copyButton = new JButton(copyButtonActionObj);

  /** List of regions. */
  private final Vector regionListDataObj = new Vector();
  /** GUI list holding entries for regions. */
  private final JList regionJListObj = new JList();
  /** Region edit panels. */
  private Vector regionEditPanels = new Vector();
  private Component regionEditParentComp = null;  //parent for region edit panel
  //option groups
  private final List optionGroups;
  /** Handle for region-manager object. */
  private IstiRegionMgrIntf istiRegionMgrObj = null;

  /**
   * Creates a region panel with all 'IstiRegion' option groups.
   */
  public IstiRegionPanel()
  {
    this(null);
  }

  /**
   * Creates a region panel.
   * @param optionGroups the 'IstiRegion' option groups or null for all.
   */
  public IstiRegionPanel(List optionGroups)
  {
    this.optionGroups = optionGroups;
    try
    {
      jbInit();         //initialize components
    }                   // (JBuilder likes to have a 'jbInit()' method)
    catch(Exception ex)  //error initializing components
    {
      ex.printStackTrace();       //show stack trace & try to continue
    }
  }

  /**
   * Sets the region-manager object to be used by this panel.
   * @param istiRegionMgrObj region-manger object to use.
   */
  public void setIstiRegionMgrObj(IstiRegionMgrIntf istiRegionMgrObj)
  {
    this.istiRegionMgrObj = istiRegionMgrObj;
  }

  /**
   * Sets the name to be pre-filled when adding a new region.  If this
   * method is not called then the default name "Region" will be used.
   * (When a new region is created a number is appended after the name
   * to make the region names unique.)
   * @param nameStr region name to use.
   */
  public void setNewRegionNameStr(String nameStr)
  {
    newRegionNameStr = nameStr;
  }

  /**
   * Performs component initialization.
   * @throws Exception if an error occurs.
   */
  protected final void jbInit() throws Exception
  {
    this.setPreferredSize(new Dimension(screenSize.width/4, screenSize.height/4));
    //place buttons vertically on buttons panel:
    final JPanel buttonsPanel = new JPanel(new GridBagLayout());
    final GridBagConstraints constraintsObj = new GridBagConstraints();
    constraintsObj.gridx = constraintsObj.gridy = 0;
    constraintsObj.gridwidth = constraintsObj.gridheight = 1;
    constraintsObj.weightx = 1.0;
    constraintsObj.weighty = 0.25;
    constraintsObj.anchor = GridBagConstraints.CENTER;
    constraintsObj.fill = GridBagConstraints.HORIZONTAL;
    constraintsObj.insets = new Insets(2,0,2,0);
    buttonsPanel.add(addCircleButton,constraintsObj);
    ++(constraintsObj.gridy);
    buttonsPanel.add(addPolyButton,constraintsObj);
    ++(constraintsObj.gridy);
    buttonsPanel.add(editButton,constraintsObj);
    ++(constraintsObj.gridy);
    buttonsPanel.add(removeButton,constraintsObj);
    ++(constraintsObj.gridy);
    buttonsPanel.add(copyButton,constraintsObj);
    //add struct below buttons to push them up a little:
    ++(constraintsObj.gridy);
    constraintsObj.weighty = 0.75;     //increase spacing around strut
    buttonsPanel.add(Box.createVerticalStrut(1),constraintsObj);
    //put empty border around buttons panel:
    buttonsPanel.setBorder(BorderFactory.createEmptyBorder(10,5,10,10));

    //put GUI list of regions into a scroll pane:
    final JScrollPane listScrollPane = new JScrollPane(regionJListObj,
        JScrollPane.VERTICAL_SCROLLBAR_AS_NEEDED,
        JScrollPane.HORIZONTAL_SCROLLBAR_AS_NEEDED);
    //put scroll pane into a panel:
    final JPanel scrollPanePanel = new JPanel(new BorderLayout());
    scrollPanePanel.add(listScrollPane,BorderLayout.CENTER);
    //put empty border around scroll-pane panel:
    scrollPanePanel.setBorder(BorderFactory.createEmptyBorder(2,10,10,5));

    setLayout(new BorderLayout());
    add(scrollPanePanel,BorderLayout.CENTER);
    add(buttonsPanel,BorderLayout.EAST);

         //set tooltips for buttons:
    addCircleButton.setToolTipText(
                                "Create and add a new point-radius region");
    addPolyButton.setToolTipText("Create and add a new polygon region");
    editButton.setToolTipText("Edit an existing region entry");
    removeButton.setToolTipText("Remove an existing region entry");
    copyButton.setToolTipText("Copy fields from an existing region");

         //setup mouse listener to do "Edit" in response to double-click:
    regionJListObj.addMouseListener(new MouseAdapter()
        {
          public void mouseClicked(MouseEvent evtObj)
          {
            try
            {
              if(mouseClickEditFlag && evtObj.getClickCount() >= 2)
              {    //edit enabled and mouse clicked at least twice
                final int idx =        //get index for mouse-click position
                      regionJListObj.locationToIndex(evtObj.getPoint());
                final Object obj;      //get region str for index:
                if((obj=regionJListObj.getModel().getElementAt(idx))
                                                          instanceof String)
                {  //region string fetched OK; do edit
                  editSpecifiedRegion((String)obj,idx);
                }
              }
            }
            catch(Exception ex)
            {
              System.err.println("Error processing mouse click on " +
                                                    "regions list:  " + ex);
              ex.printStackTrace();
            }
          }
        });
  }

  /**
   * Adds an <code>ActionListener</code> to the panel.
   * @param l the <code>ActionListener</code> to be added
   */
  public void addActionListener(ActionListener l)
  {
    panelListenerList.add(l);
  }

  /**
   * Creates a dialog for the specified region panel.
   * @param titleStr the title for the region panel.
   * @param regionParentComp the parent for the region panel.
   * @param regionEditParentComp the parent for region edit panel or null
   *                             to use the region panel.
   * @param modalFlag true for a modal dialog, false for modeless (allows
   * other windows to run).
   * @return A new 'IstiDialogPopup' object.
   */
  public IstiDialogPopup createDialog(String titleStr,
                 Component regionParentComp, Component regionEditParentComp,
                                                          boolean modalFlag)
  {
//    this.regionEditParentComp = (regionEditParentComp != null) ?
//                                                regionEditParentComp : this;

    if (regionEditParentComp != null)
    {    //edit-dialog-parent was given; save it
      this.regionEditParentComp = regionEditParentComp;
         //clear centering flag if edit-parent given and non-modal dlg:
      centerEditDialogFlag = modalFlag;
    }
    else
    {    //edit-dialog-parent was given; use this region-panel as parent
      this.regionEditParentComp = this;
      centerEditDialogFlag = true;          //set centering flag
    }
    this.dialogModalFlag = modalFlag;       //save modal flag

    final int msgType = JOptionPane.PLAIN_MESSAGE;
    final int optionType = IstiDialogPopup.DEFAULT_OPTION;
              //setup buttons; help button if listener is installed:
    final Object [] optionsArr = isHelpActionListenerInstalled() ?
                      new Object[] { CLOSE_OPTION_TEXT, HELP_OPTION_TEXT } :
                                         new Object[] { CLOSE_OPTION_TEXT };
    dialogObj = new IstiDialogPopup(
        regionParentComp, this, titleStr, msgType, optionsArr,
        0, modalFlag, false, optionType);
    final JOptionPane paneObj = dialogObj.getOptionPaneObj();
    dialogObj.setDefaultCloseOperation(
        IstiDialogPopup.DISPOSE_ON_CLOSE);
              //if edit-parent given and non-modal dialogs then
              // put this region-panel dialog at upper-left of screen:
    if(!centerEditDialogFlag)
      dialogObj.setLocation(0, 0);
    dialogObj.setHideDialogOnButtonFlag(false);
    dialogObj.addPropertyChangeListener(new PropertyChangeListener()
    {
      /**
       * This method gets called when a bound property is changed.
       * @param evt A PropertyChangeEvent object describing the event source
       *   	and the property that has changed.
       */

      public void propertyChange(PropertyChangeEvent evt)
      {
        final Object newValue = evt.getNewValue();
        final String prop = evt.getPropertyName();

        if (dialogObj.isVisible()
            && (evt.getSource() == paneObj)
            && (JOptionPane.VALUE_PROPERTY.equals(prop) ||
            JOptionPane.INPUT_VALUE_PROPERTY.equals(prop))) {
          Object value = paneObj.getValue();

          if (value == JOptionPane.UNINITIALIZED_VALUE) {
            //ignore reset
            return;
          }

          if (HELP_OPTION_TEXT.equals(newValue))
          {
            //notify the help action listener
            notifyHelpActionListener(IstiRegionPanel.this);
            //Reset the JOptionPane's value.
            //If you don't do this, then if the user
            //presses the same button next time, no
            //property change event will be fired.
            paneObj.setValue(JOptionPane.UNINITIALIZED_VALUE);
            return;
          }
              //use 'dispose()' instead of 'hide()' to make button
              // action match default-window-close action:
          dialogObj.dispose();
        }
      }
    });
    //setup window listener
    dialogObj.addWindowListener(this);
    return dialogObj;
  }

  /**
   * Creates a dialog for the specified region panel.  A non-modal
   * dialog is created.
   * @param titleStr the title for the region panel.
   * @param regionParentComp the parent for the region panel.
   * @param regionEditParentComp the parent for region edit panel or null
   *                             to use the region panel.
   * @return A new 'IstiDialogPopup' object.
   */
  public IstiDialogPopup createDialog(String titleStr,
                 Component regionParentComp, Component regionEditParentComp)
  {
    return createDialog(titleStr,regionParentComp,regionEditParentComp,
                                                                     false);
  }

  /**
   * Creates a dialog for the specified region panel.  The region edit
   * panel uses this region panel as its parent component.
   * @param titleStr the title for the region panel.
   * @param regionParentComp the parent for the region panel.
   * @param modalFlag true for a modal dialog, false for modeless (allows
   * other windows to run).
   * @return A new 'IstiDialogPopup' object.
   */
  public IstiDialogPopup createDialog(String titleStr,
                              Component regionParentComp, boolean modalFlag)
  {
    return createDialog(titleStr,regionParentComp,null,modalFlag);
  }

  /**
   * Creates a dialog for the specified region panel.  The region edit
   * panel uses this region panel as its parent component and a non-modal
   * dialog is created.
   * @param titleStr the title for the region panel.
   * @param regionParentComp the parent for the region panel.
   * @return A new 'IstiDialogPopup' object.
   */
  public IstiDialogPopup createDialog(String titleStr,
                                                 Component regionParentComp)
  {
    return createDialog(titleStr,regionParentComp,null,false);
  }

  /**
   * Shows a dialog for the specified region panel.  The region edit
   * panel uses this region panel as its parent component.
   * @param regionParentComp the parent for the region panel.
   * @param titleStr the title for the region panel.
   * @param modalFlag true for a modal dialog, false for modeless (allows
   * other windows to run).
   */
  public void showDialog(Component regionParentComp, String titleStr,
                                                          boolean modalFlag)
  {
    createDialog(titleStr,regionParentComp,null,modalFlag).show();
  }

  /**
   * Shows a dialog for the specified region panel.  The region edit
   * panel uses this region panel as its parent component and a non-modal
   * dialog is created.
   * @param regionParentComp the parent for the region panel.
   * @param titleStr the title for the region panel.
   */
  public void showDialog(Component regionParentComp, String titleStr)
  {
    createDialog(titleStr,regionParentComp,null).show();
  }

  /**
   * Gets the dialog for this panel.
   * @return The dialog object for this panel, or null if none has been
   * created.
   */
  public IstiDialogPopup getDialog()
  {
    return dialogObj;
  }

  /**
   * Determines if the dialog for this panel is visible.
   * @return true if the dialog for this panel has been created and is
   * visible; false otherwise.
   */
  public boolean isDialogVisible()
  {
    return (dialogObj != null && dialogObj.isVisible());
  }

  /**
   * Overridden version that also calls 'requestFocus()' on the dialog
   * hosting this panel.
   */
  public void requestFocus()
  {
    if(dialogObj != null)
      dialogObj.requestFocus();
    super.requestFocus();
  }

  /**
   * Gets the region string.
   * @return the region string.
   */
  public String getRegionString()
  {
    final StringBuffer s = new StringBuffer();
    final int numRegions = regionListDataObj.size();

    for (int i = 0; i < numRegions; i++)
    {
      if (i > 0)
        s.append(IstiRegion.REGION_SEP_CHAR + " ");
      s.append(regionListDataObj.get(i));
    }
    return s.toString();
  }

  /**
   * Removes an <code>ActionListener</code> from the panel.
   * If the listener is the currently set <code>Action</code>
   * for the button, then the <code>Action</code>
   * is set to <code>null</code>.
   *
   * @param l the listener to be removed
   */
  public void removeActionListener(ActionListener l)
  {
    panelListenerList.remove(l);
  }

  /**
   * Sets the "Help" button action listener.
   * @param l the action listener.
   */
  public void setHelpActionListener(ActionListener l)
  {
    helpActionListener = l;
  }

  /**
   * Sets the region string.
   * @param str the region string.
   */
  public void setRegionString(String str)
  {
    final String subStr[] =
        UtilFns.parseSeparatedSubstrings(str, IstiRegion.REGION_SEP_CHAR);
    final int numRegions = subStr.length;

    regionListDataObj.clear();
    for (int i = 0; i < numRegions; i++)
    {
      regionListDataObj.add(subStr[i].trim());
    }
    regionJListObj.setListData(regionListDataObj);

    if(regionListDataObj.size() > 0)
      selectAndShowListRegion(0);

    //notify listeners of the change
    notifyRegionsUpdated();
  }

  /**
   * Creates, edits and adds a new region object.
   * @param circleFlag true for circle, false for polygon.
   */
  protected void addNewRegion(boolean circleFlag)
  {
    String regionStr;
    if(istiRegionMgrObj != null &&
      (regionStr=istiRegionMgrObj.checkGenerateRegionName(
                       newRegionNameStr)) != null && regionStr.length() > 0)
    {
      regionStr = IstiRegion.NAME_BEGIN_CHAR + regionStr +
                                                   IstiRegion.NAME_END_CHAR;
    }
    else
      regionStr = UtilFns.EMPTY_STRING;

              //setup to add a new region entry:
    openRegionEditPanel(regionStr,regionEditPanels.size());
              //create edit dialog:
    regionEditPanel.createDialog(this, true, regionEditParentComp,
                                      dialogModalFlag,centerEditDialogFlag);
    regionEditPanel.setCircle(circleFlag);
    final int retVal = regionEditPanel.showDialog();
    if (retVal == IstiDialogPopup.OK_OPTION &&
                 (regionStr=regionEditPanel.getRegionString()).length() > 0)
    {    //new region string OK; add it to list
      regionListDataObj.add(regionStr);
      regionJListObj.setListData(regionListDataObj);
                                   //select newly-added region:
      selectAndShowListRegion(regionListDataObj.indexOf(regionStr));
      regionJListObj.repaint();    //update displayed list of regions
      //notify listeners of the change
      notifyRegionsUpdated();
    }
    closeRegionEditPanel();  //close the region edit panel
  }

  /**
   * Edits the region that is currently selected on the GUI list.
   */
  protected void editSelectedRegion()
  {
    final Object obj;   //get and edit currently-selected region object
                   // (check empty list to avoid Java bug 4816818/4178930):
    if(regionJListObj.getModel().getSize() > 0 &&
                  (obj=regionJListObj.getSelectedValue()) instanceof String)
    {
      editSpecifiedRegion((String)obj,regionJListObj.getSelectedIndex());
    }
  }

  /**
   * Edits the region that is currently selected on the GUI list.
   * @param specStr region string to edit.
   * @param specIdx index of given region string, or -1 if adding new
   * region.
   */
  protected void editSpecifiedRegion(String specStr, int specIdx)
  {
    final boolean addFlag = (specIdx < 0);       //setup "add" flag
         //setup to edit (or add if flag) via existing region entry:
    openRegionEditPanel(specStr,
                             (addFlag ? regionEditPanels.size() : specIdx));
                      //create edit dialog:
    regionEditPanel.createDialog(this,addFlag,regionEditParentComp,
                                      dialogModalFlag,centerEditDialogFlag);
    final int retVal = regionEditPanel.showDialog();
    final String editStr;
    if(retVal == IstiDialogPopup.OK_OPTION &&
                 (editStr=regionEditPanel.getRegionString()).length() > 0 &&
                                      (addFlag || !editStr.equals(specStr)))
    {    //edited region string OK and adding or modified; enter into list
      if(addFlag)
      {  //"add" flag set; add new region-text string
        regionListDataObj.add(editStr);
        regionJListObj.setListData(regionListDataObj);
                                       //select newly-added region:
        selectAndShowListRegion(regionListDataObj.indexOf(editStr));
      }
      else    //"add" flag clear; replace region-text string entry
        regionListDataObj.set(specIdx,editStr);
      regionJListObj.repaint();        //update displayed list of regions
      notifyRegionsUpdated();          //notify listeners of change
    }
    closeRegionEditPanel();  //close the region edit panel
  }

  /**
   * Shows a list of regions; then shows an add-region dialog with
   * entries copied from the selected source region.
   */
  protected void copyToNewRegion()
  {
         //get region entry currently selected on list
         // (check empty list to avoid Java bug 4816818/4178930):
    final Object selRegionObj = (regionJListObj.getModel().getSize() > 0) ?
                                   regionJListObj.getSelectedValue() : null;
    String srcRegionStr;
    if(istiRegionMgrObj != null && istiRegionMgrObj.getAllRegionsCount() > 0)
    {    //region manager contains regions; get list of region strings
      final Vector regStrsVec = istiRegionMgrObj.getAllRegionStrsList();
                        //create all-regions list for dialog:
      final JList sourceJListObj = new JList(regStrsVec);
      int selIdx;       //select same region as one selected on panel:
      if((selIdx=regStrsVec.indexOf(selRegionObj)) >= 0)
        sourceJListObj.setSelectedIndex(selIdx);
      else if(regStrsVec.size() > 0)
        sourceJListObj.setSelectedIndex(0);      //default select first
                        //put all-regions list in scroll pane:
      final JScrollPane paneObj = new JScrollPane(sourceJListObj);
                        //set scroll pane to arbitrary size:
      paneObj.setPreferredSize(new Dimension(500,150));
                        //create popup for all-regions list:
      final IstiDialogPopup popupObj = new IstiDialogPopup(
                               this,paneObj,"Select Source Region for Copy",
                                    false,IstiDialogPopup.OK_CANCEL_OPTION);
              //add mouse listener to select region on double-click:
      sourceJListObj.addMouseListener(new MouseAdapter()
          {
            public void mouseClicked(MouseEvent evtObj)
            {      //if double-click then close (with "OK" button):
              if(evtObj.getClickCount() >= 2)
                popupObj.close(Integer.valueOf(IstiDialogPopup.OK_OPTION));
            }
          });
      setButtonsEnabled(false);        //disable buttons on panel
      final Object obj;      //show dialog and wait for result:
      if(popupObj.showAndReturnIndex() != IstiDialogPopup.OK_OPTION ||
                           (selIdx=sourceJListObj.getSelectedIndex()) < 0 ||
                                              selIdx >= regStrsVec.size() ||
                          !((obj=regStrsVec.get(selIdx)) instanceof String))
      {  //not closed via "OK" button or bad selected-index value or
         // unable to fetch value regions-text string entry from list
        setButtonsEnabled(true);       //re-enable buttons on panel
        return;
      }
      srcRegionStr = (String)obj;      //save selected region-text string
    }
    else      //region manager does not contain regions
    {              //use currently-select region entry on panel:
      if(!(selRegionObj instanceof String))
        return;         //if selected-region object not OK then return
      srcRegionStr = (String)selRegionObj;  //save region-text string
    }
    final String oldNameStr = IstiRegion.extractRegionName(srcRegionStr);
    if(istiRegionMgrObj != null && istiRegionMgrObj.getAllRegionsCount() > 0)
    {    //region manager contains regions
      final String newNameStr;
      if(oldNameStr.length() > 0)
      {       //valid region name found; append "_#" to name
        newNameStr =              // (make sure name is unique)
                   istiRegionMgrObj.checkGenerateRegionName(oldNameStr+'_');
      }
      else
      {       //no valid region name found; use default
        newNameStr =              // (make sure name is unique)
                 istiRegionMgrObj.checkGenerateRegionName(newRegionNameStr);
      }
      srcRegionStr = IstiRegion.replaceRegionName(srcRegionStr,newNameStr);
    }
    else if(oldNameStr.length() > 0)
    {    //region manager does not contain regions and region name found
              //put "Copy of " in front of region name:
      srcRegionStr = IstiRegion.replaceRegionName(srcRegionStr,
                                                 ("Copy of " + oldNameStr));
    }
         //use 'edit' method to add region with fields filled in:
    editSpecifiedRegion(srcRegionStr,-1);
  }

  /**
   * Closes the region edit panel.
   */
  protected void closeRegionEditPanel()
  {
    regionEditPanel = null;
    setButtonsEnabled(true);  //enable the buttons
  }

  /**
   * Notifies all listeners that have registered interest for
   * notification on this event type.  The event instance
   * is lazily created using the parameters passed into
   * the fire method.
   *
   * @param event  the <code>ChangeEvent</code> object
   * @see EventListenerList
   */
  protected void fireActionPerformed(ActionEvent event)
  {
    final Object[] listeners = panelListenerList.toArray();

    // Process the listeners last to first, notifying
    // those that are interested in this event
    for (int i = listeners.length-1; i >= 0; i--)
    {
      ((ActionListener)listeners[i]).actionPerformed(event);
    }
  }


  /**
   * Notify the help action listener.
   * @param source  the object that originated the event
   */
  public void notifyHelpActionListener(Object source)
  {
    if (helpActionListener != null)
    {
      helpActionListener.actionPerformed(
          new ActionEvent(source, 0, "Help pressed"));
    }
  }

  /**
   * Returns true if the "Help"-button action listener is installed.
   * @return true if the "Help"-button action listener is installed.
   */
  public boolean isHelpActionListenerInstalled()
  {
    return (helpActionListener != null);
  }

  /**
   * Notify regions updated.
   */
  protected void notifyRegionsUpdated()
  {
    //notify listeners of the change
    fireActionPerformed(
        new ActionEvent(this, ActionEvent.ACTION_PERFORMED, "region updated"));
  }

  /**
   * Opens the region edit panel for the specified region.
   * @param regionStr the region-text string, or an empty string for none.
   * @param regionIndex the region index.
   */
  protected void openRegionEditPanel(
      String regionStr,int regionIndex)
  {
    final Object tableObj;

    if (regionIndex < regionEditPanels.size())
    {
      tableObj = regionEditPanels.get(regionIndex);
    }
    else
    {
      tableObj = null;
      regionEditPanels.setSize(regionIndex+1);
    }

    if (tableObj instanceof IstiRegionEditPanel)
    {
      regionEditPanel = (IstiRegionEditPanel)tableObj;
      regionEditPanel.setRegionString(regionStr);
    }
    else
    {
      regionEditPanel = new IstiRegionEditPanel(optionGroups,regionStr);
      regionEditPanels.set(regionIndex, regionEditPanel);
    }
    setButtonsEnabled(false);  //disable the buttons
  }

  /**
   * Removes the one or more regions that are currently selected on
   * the GUI list.
   */
  protected void removeSelectedRegions()
  {
	// check empty model to avoid Java bug 4816818/4178930:
	if (regionJListObj.getModel().getSize() == 0) // id model is empty
	{
	  return;
	}

	Object obj;
	String regionStr;
	final ListSelectionModel sm = regionJListObj.getSelectionModel();
	final int selMin = sm.getMinSelectionIndex();
	final int selMax = sm.getMaxSelectionIndex();
	if (selMin < 0 || selMax < 0) // if selection is empty
	{
	  return;
	}

	final ListModel dm = regionJListObj.getModel();
	if(!((obj=dm.getElementAt(selMin)) instanceof String))
	  return;            //cancel removal
	regionStr = (String) obj;
	if (selMin != selMax)  //more than one region object selected
	{
	  final String[] selArr = new String[1 + (selMax - selMin)];
	  int numSel = 1;
	  selArr[0] = regionStr;
	  for(int idx = selMin + 1; idx <= selMax; idx++)
	  {
		if (!sm.isSelectedIndex(idx) ||
			!((obj = dm.getElementAt(idx)) instanceof String))
		{
		  continue;
		}
		regionStr = (String) obj;
		selArr[numSel++] = regionStr;
	  }
	  if(dialogUtilObj.popupYesNoConfirmMessage(
		  ("Remove the " + numSel + " selected regions?"),
		  CONFIRM_REMOVAL_TEXT,false) != IstiDialogPopup.YES_OPTION)
	  {
		//user selected "No"
		return;            //cancel removal
	  }
	  for(int idx=0; idx<numSel; ++idx)
	  {     //for each selected region entry; remove it
		regionStr = selArr[idx];
		regionListDataObj.remove(regionStr);
	  }
	}
	else // only one region object selected
	{
	  final IstiRegion region = IstiRegion.parse(regionStr);
	  String regionText;
	  if (region == null ||
		  (regionText = region.getName()).trim().length() <= 0)
	  {
		final int regionNum = regionJListObj.getSelectedIndex() + 1;
		regionText = Integer.toString(regionNum);
	  }
	  //get user confirmation:
	  if(dialogUtilObj.popupYesNoConfirmMessage(
		  ("Remove region \"" + regionText + "\"?"),
		  CONFIRM_REMOVAL_TEXT,false) != IstiDialogPopup.YES_OPTION)
	  {
		//user selected "No"
		return;            //cancel removal
	  }
	  regionListDataObj.remove(regionStr);
	}
	//update GUI list model:
	regionJListObj.setListData(regionListDataObj);
	regionJListObj.repaint();    //update displayed list of regions
	//notify listeners of the change
	notifyRegionsUpdated();
	selectAndShowListRegion(0);   //select first region entry
  }

  /**
   * Selects the given entry on the GUI list of regions and ensures
   * that the entry is visible.
   * @param idx the index of the region entry.
   */
  protected void selectAndShowListRegion(final int idx)
  {
    //do this after other Swing work is complete
    SwingUtilities.invokeLater(new Runnable()
    {
      public void run()
      {
        try
        {
          regionJListObj.setSelectedIndex(idx);
          regionJListObj.ensureIndexIsVisible(idx);
        }
        catch(Exception ex)
        {
          //some kind of exception error; display it and move on
          System.err.println("Error selecting list entry:  " + ex);
          ex.printStackTrace();
        }
      }
    });
  }

  /**
   * Enables or disables the buttons.
   * @param b true to enable, false to disable.
   */
  protected void setButtonsEnabled(boolean b)
  {
    addCircleButton.setEnabled(b);
    addPolyButton.setEnabled(b);
    editButton.setEnabled(b);
    removeButton.setEnabled(b);
    copyButton.setEnabled(b);
    mouseClickEditFlag = b;
  }

//  WindowListener interface

/**
 * Invoked the first time a window is made visible.
 * @param e window event
 */
  public void windowOpened(WindowEvent e) {}

  /**
   * Invoked when the user attempts to close the window
   * from the window's system menu.  If the program does not
   * explicitly hide or dispose the window while processing
   * this event, the window close operation will be cancelled.
   * @param e window event
   */
  public void windowClosing(WindowEvent e) {}

  /**
   * Invoked when a window has been closed as the result
   * of calling dispose on the window.
   * @param e window event
   */
  public void windowClosed(WindowEvent e)
  {
         //if non-modal dialogs and region-edit panel has been created
         // then make sure the region-edit panel closes with this panel:
    if (!dialogModalFlag && regionEditPanel != null)
      regionEditPanel.closeDialog();
  }

  /**
   * Invoked when a window is changed from a normal to a
   * minimized state. For many platforms, a minimized window
   * is displayed as the icon specified in the window's
   * iconImage property.
   * @see java.awt.Frame#setIconImage
   * @param e window event
   */
  public void windowIconified(WindowEvent e) {}

  /**
   * Invoked when a window is changed from a minimized
   * to a normal state.
   * @param e window event
   */
  public void windowDeiconified(WindowEvent e) {}

  /**
   * Invoked when the window is set to be the user's
   * active window, which means the window (or one of its
   * subcomponents) will receive keyboard events.
   * @param e window event
   */
  public void windowActivated(WindowEvent e) {}

  /**
   * Invoked when a window is no longer the user's active
   * window, which means that keyboard events will no longer
   * be delivered to the window or its subcomponents.
   * @param e window event
   */
  public void windowDeactivated(WindowEvent e) {}
}
