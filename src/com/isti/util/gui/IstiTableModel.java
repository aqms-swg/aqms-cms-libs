//IstiTableModel.java:  Extends the TableModel.
//                      This adds support for hiding columns in a table.
//
//  1/25/2008 -- [KF]  Initial version.
//  5/26/2008 -- [KF]  Changed to make the 'getActualColumnIndex' method public.
//  6/19/2009 -- [KF]  Changed to use a Table Model Listener.
//  6/22/2009 -- [KF]  Added column filter and row filter support.
//   7/8/2009 -- [KF]  Removed synchronization.
//

package com.isti.util.gui;

import javax.swing.table.AbstractTableModel;
import javax.swing.table.TableModel;
import javax.swing.event.TableModelEvent;
import javax.swing.event.TableModelListener;

import com.isti.util.BasicColumnFilter;
import com.isti.util.BasicRowFilter;
import com.isti.util.IstiColumnFilter;
import com.isti.util.IstiRowFilter;

/**
 * Class IstiTableModel Extends the TableModel.
 */
public class IstiTableModel implements IstiColumnFilter, IstiRowFilter,
    TableModel, TableModelListener {
  /** The abstract table model or null if none. */
  private final AbstractTableModel abstractTableModel;

  /** The table model. */
  private final TableModel tableModel;

  /** The column index values or null if unknown. */
  private int[] columnIndexValues = null;

  /** visible column count. */
  private int visibleColumnCount;

  /** The row index values or null if unknown. */
  private int[] rowIndexValues = null;

  /** visible row count. */
  private int visibleRowCount;

  /** The column filter or null if none. */
  private IstiColumnFilter columnFilter = null;

  /** The row filter or null if none. */
  private IstiRowFilter rowFilter = null;

  /**
   * Creates a default instance.
   * @param tableModel the table model.
   */
  public IstiTableModel(AbstractTableModel tableModel) {
    this.abstractTableModel = tableModel;
    this.tableModel = tableModel;
    tableModel.addTableModelListener(this);
  }

  /**
   * Adds a listener to the model. The listener will receive notification of
   * updates to the model.
   * @param listener the listener.
   */
  public void addTableModelListener(TableModelListener listener) {
    tableModel.addTableModelListener(listener);
  }

  /**
   * Notifies all listeners that the table's structure has changed.
   */
  public void fireTableStructureChanged() {
    if (abstractTableModel != null) {
      abstractTableModel.fireTableStructureChanged();
    } else {
      tableChanged(new TableModelEvent(this, TableModelEvent.HEADER_ROW));
    }
  }

  /**
   * Gets the actual column index.
   * @param columnIndex the visible column index.
   * @return the actual column index.
   */
  public int getActualColumnIndex(int columnIndex) {
    if (columnFilter == null) {
      return columnIndex;
    }
    if (columnIndexValues == null) {
      updateColumnInfo();
    }
    if (columnIndexValues == null) {
      return columnIndex;
    }
    return columnIndexValues[columnIndex];
  }

  /**
   * Gets the actual row index.
   * @param rowIndex the visible row index.
   * @return the actual row index.
   */
  public int getActualRowIndex(int rowIndex) {
    if (rowFilter == null) {
      return rowIndex;
    }
    if (rowIndexValues == null) {
      updateRowInfo();
    }
    if (rowIndexValues == null) {
      return rowIndex;
    }
    return rowIndexValues[rowIndex];
  }

  /**
   * Returns the <code>Class</code> for all <code>Object</code> instances in
   * the specified column.
   * @param columnIndex the visible column index.
   * @return The class.
   */
  public Class getColumnClass(int columnIndex) {
    columnIndex = getActualColumnIndex(columnIndex);
    return tableModel.getColumnClass(columnIndex);
  }

  /**
   * Returns the number of visible columns in the model.
   * @return The visible column count.
   */
  public int getColumnCount() {
    if (columnFilter == null) {
      return tableModel.getColumnCount();
    }
    if (columnIndexValues == null) {
      updateColumnInfo();
    }
    return visibleColumnCount;
  }

  /**
   * Get the column filter.
   * @return the column filter or null if none.
   */
  public IstiColumnFilter getColumnFilter() {
    return columnFilter;
  }

  /**
   * Returns the name of a column in the model.
   * @param columnIndex the visible column index.
   * @return The column name.
   */
  public String getColumnName(int columnIndex) {
    columnIndex = getActualColumnIndex(columnIndex);
    return tableModel.getColumnName(columnIndex);
  }

  /**
   * Returns the number of rows in the model.
   * @return The row count.
   */
  public int getRowCount() {
    if (rowFilter == null) {
      return tableModel.getRowCount();
    }
    if (rowIndexValues == null) {
      updateRowInfo();
    }
    return visibleRowCount;
  }

  /**
   * Get the row filter.
   * @return the row filter or null if none.
   */
  public IstiRowFilter getRowFilter() {
    return rowFilter;
  }

  /**
   * Gets the underlying table model.
   * @return the table model.
   */
  public TableModel getTableModel() {
    return tableModel;
  }

  /**
   * Returns the value (<code>Object</code>) at a particular cell in the
   * table.
   * @param rowIndex the row index.
   * @param columnIndex the visible column index.
   * @return The value at the specified cell.
   */
  public Object getValueAt(int rowIndex, int columnIndex) {
    rowIndex = getActualRowIndex(rowIndex);
    columnIndex = getActualColumnIndex(columnIndex);
    return tableModel.getValueAt(rowIndex, columnIndex);
  }

  /**
   * Initializes the column information. This method should be called if the
   * number of columns changes. All columns will default to visible as a result
   * of calling this method. This will set the current column filter to a new
   * BasicColumnFilter if it is not already.
   */
  protected void initColumnInfo() {
    columnFilter = new BasicColumnFilter(tableModel.getColumnCount());
  }

  /**
   * Initializes the row information. This method should be called if the number
   * of rows changes. All rows will default to visible as a result of calling
   * this method. This will set the current row filter to a new BasicRowFilter
   * if it is not already.
   */
  protected void initRowInfo() {
    rowFilter = new BasicRowFilter(tableModel.getRowCount());
  }

  /**
   * Returns <code>true</code> if the cell is editable, and <code>false</code>
   * otherwise.
   * @param rowIndex the row index.
   * @param columnIndex the visible column index.
   * @return <code>true</code> if editable, <code>false</code> otherwise.
   */
  public boolean isCellEditable(int rowIndex, int columnIndex) {
    rowIndex = getActualRowIndex(rowIndex);
    columnIndex = getActualColumnIndex(columnIndex);
    return tableModel.isCellEditable(rowIndex, columnIndex);
  }

  /**
   * Determines if the column is visible.
   * @param columnIndex the actual column index.
   * @return true if the column is visible, false otherwise.
   */
  public boolean isColumnVisible(int columnIndex) {
    if (columnFilter == null) {
      return true;
    }
    return columnFilter.isColumnVisible(columnIndex);
  }

  /**
   * Determines if the row is visible.
   * @param rowIndex the actual row index.
   * @return true if the row is visible, false otherwise.
   */
  public boolean isRowVisible(int rowIndex) {
    if (rowFilter == null) {
      return true;
    }
    return rowFilter.isRowVisible(rowIndex);
  }

  /**
   * Removes a listener from the model.
   * @param listener the listener.
   */
  public void removeTableModelListener(TableModelListener listener) {
    tableModel.removeTableModelListener(listener);
  }

  /**
   * Set the column filter.
   * @param columnFilter the column filter or null if none.
   */
  public void setColumnFilter(final IstiColumnFilter columnFilter) {
    this.columnFilter = columnFilter;
    fireTableStructureChanged();
  }

  /**
   * Sets if the column is visible or not. This will set the current column
   * filter to a new BasicColumnFilter if it is not already.
   * @param columnIndex the actual column index.
   * @param b true for visible, false otherwise.
   */
  public void setColumnVisible(final int columnIndex, final boolean b) {
    if (!(columnFilter instanceof BasicColumnFilter)) {
      initColumnInfo();
    }
    final BasicColumnFilter basicColumnFilter = (BasicColumnFilter) columnFilter;
    basicColumnFilter.setColumnVisible(columnIndex, b);
    fireTableStructureChanged();
  }

  /**
   * Set the row filter.
   * @param rowFilter the row filter or null if none.
   */
  public void setRowFilter(final IstiRowFilter rowFilter) {
    this.rowFilter = rowFilter;
    fireTableStructureChanged();
  }

  /**
   * Sets if the row is visible or not. This will set the current row filter to
   * a new BasicRowFilter if it is not already.
   * @param rowIndex the actual row index.
   * @param b true for visible, false otherwise.
   */
  public void setRowVisible(final int rowIndex, final boolean b) {
    if (!(rowFilter instanceof BasicRowFilter)) {
      initRowInfo();
    }
    final BasicRowFilter basicRowFilter = (BasicRowFilter) rowFilter;
    basicRowFilter.setRowVisible(rowIndex, b);
    fireTableStructureChanged();
  }

  /**
   * Sets the value at a particular cell in the table.
   * @param aValue the value (<code>null</code> permitted).
   * @param rowIndex the row index.
   * @param columnIndex the visible column index.
   */
  public void setValueAt(Object aValue, int rowIndex, int columnIndex) {
    rowIndex = getActualRowIndex(rowIndex);
    columnIndex = getActualColumnIndex(columnIndex);
    tableModel.setValueAt(aValue, rowIndex, columnIndex);
  }

  /**
   * This fine grain notification tells listeners the exact range of cells,
   * rows, or columns that changed.
   * @param e the table model event.
   */
  public void tableChanged(final TableModelEvent e) {
    if (e.getFirstRow() == TableModelEvent.HEADER_ROW) {
      columnIndexValues = null;
      visibleColumnCount = 0;
    }
    rowIndexValues = null;
    visibleRowCount = 0;
  }

  /**
   * Updates the column information.
   */
  protected void updateColumnInfo() {
    visibleColumnCount = 0;
    final int columnCount = tableModel.getColumnCount();
    columnIndexValues = new int[columnCount];
    // determine the visible column count
    for (int columnIndex = 0; columnIndex < columnCount; columnIndex++) {
      if (isColumnVisible(columnIndex)) // if the column is visible
      {
        // add the column
        columnIndexValues[visibleColumnCount++] = columnIndex;
      }
    }
  }

  /**
   * Updates the row information.
   */
  protected void updateRowInfo() {
    visibleRowCount = 0;
    final int rowCount = tableModel.getRowCount();
    rowIndexValues = new int[rowCount];
    // determine the visible row count
    for (int rowIndex = 0; rowIndex < rowCount; rowIndex++) {
      if (isRowVisible(rowIndex)) // if the row is visible
      {
        // add the row
        rowIndexValues[visibleRowCount++] = rowIndex;
      }
    }
  }
}
