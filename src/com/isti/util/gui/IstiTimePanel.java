//IstiTimePanel.java:  Implements the time panel.
//
//   8/21/2003 -- [KF]
//

package com.isti.util.gui;

import java.awt.Color;
import java.awt.Component;
import java.awt.FlowLayout;
import java.awt.event.ActionEvent;
import java.util.Calendar;
import java.util.Date;
import java.util.EventObject;
import java.util.TimeZone;

import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.SwingConstants;
import javax.swing.border.TitledBorder;
import javax.swing.event.EventListenerList;

/**
 * Class IstiTimePanel implements the time panel.
 */
public class IstiTimePanel extends JPanel implements UpdateListener
{
  private final TitledBorder borderObj =
      new TitledBorder(BorderFactory.createEtchedBorder(), "Time");
  private final JLabel dayOfMonthLabel = new JLabel();
  private final JLabel monthLabel = new JLabel();
  private final JLabel yearLabel = new JLabel();
  private final Calendar calObj;
  private final JComboBox monthComboBox =
      new JComboBox(IstiCalendarPanel.MONTH_TEXT);
  private final JComboBox dayComboBox = new JComboBox();
  private final IstiCalendarPanel.YearJComboBox yearComboBox =
      new IstiCalendarPanel.YearJComboBox();
  private final JLabel timeLabel = new JLabel();
  private final TimeJTextField timeTextField;
  private final JButton calendarButton = new JButton();

  protected static boolean debugFlag = false;

  private int displayMaxDays = 0;  //display maximum days
  private Object timeUpdateLockObj = new Object();
  private boolean timeUpdateFlag = false;
  private EventListenerList listenerList = new EventListenerList();

  //tool tip text
  protected final String monthTipText;
  protected final String dayOfMonthTextTip;
  protected final String yearTextTip;
  protected final String timeTipText;
  protected final String calendarTipText;

  /**
   * Creates a time panel with the default title for the panel
   * and the calendar popup.
   */
  public IstiTimePanel()
  {
    this(null);
  }

  /**
   * Creates a time panel with the given title for the panel
   * and the calendar popup.
   * @param title the title for the panel and the calendar popup
   * or null for default.
   */
  public IstiTimePanel(String title)
  {
    this(title, TimeJTextField.SHOW_ZERO_TEXT_FLAG_DEFAULT);
  }

  /**
   * Creates a time panel with the given title for the panel
   * and the calendar popup using the provided calendar.
   * @param title the title for the panel and the calendar popup
   * or null for default.
   * @param showZeroTextFlag the show zero test flag,
   * if true spaces in the text are replaced with zeros.
   */
  public IstiTimePanel(String title, boolean showZeroTextFlag)
  {
    this(title, showZeroTextFlag, null);
  }

  /**
   * Creates a time panel with the given title for the panel
   * and the calendar popup using the provided calendar.
   * @param title the title for the panel and the calendar popup
   * or null for default.
   * @param showZeroTextFlag the show zero test flag,
   * if true spaces in the text are replaced with zeros.
   * @param calObj calendar object or null to create a new one.
   */
  public IstiTimePanel(
      String title, boolean showZeroTextFlag, Calendar calObj)
  {
    if (calObj == null)
      calObj = Calendar.getInstance();
    this.calObj = calObj;

    timeTextField = new TimeJTextField(
        TimeJTextField.HOURS_MINS_SECS, showZeroTextFlag, calObj);

    //set tool tip text
    monthTipText = "Month-of-the-year";
    dayOfMonthTextTip = "Day-of-the-month";
    yearTextTip = "Year";
    timeTipText = "Time-of-day value (" +
                  timeTextField.timeFormatterObj.toPattern() + ")";
    calendarTipText = "Show calendar";

    borderObj.setTitleColor(Color.black);
    if (title != null)
    {
      setName(title);
      setTitle(title);
    }

    try
    {
      jbInit();
    }
    catch(Exception e)
    {
      e.printStackTrace();
    }
  }

  /**
   * Gets the maximum time.
   * @return the maximum time.
   */
  public Date getMaxTime()
  {
    return timeTextField.getMaxTime();
  }

  /**
   * Gets the minimum time.
   * @return the minimum time.
   */
  public Date getMinTime()
  {
    return timeTextField.getMinTime();
  }

  /**
   * Get the date that is specified in this object.
   * @return the date that is specified in this object.
   */
  public Date getTime()
  {
    return getCalendar().getTime();
  }

  /**
   * Gets the time zone.
   * @return the time zone object associated with this panel.
   */
  public TimeZone getTimeZone()
  {
    return calObj.getTimeZone();
  }

  /**
   * Gets the title of the time panel.
   * @return the title of the time panel.
   */
  public String getTitle()
  {
    return borderObj.getTitle();
  }

  /**
   * Determines whether this component is enabled. An enabled component
   * can respond to user input and generate events. Components are
   * enabled initially by default. A component may be enabled or disabled by
   * calling its <code>setEnabled</code> method.
   * @return <code>true</code> if the component is enabled;
   * <code>false</code> otherwise.
   * @see #setEnabled
   */
  public boolean isEnabled()
  {
    return timeTextField.isEnabled();
  }

  /**
   * Determines if the labels are shown.
   * @return true if the labels are shown, false otherwise.
   */
  public boolean isShowLabels()
  {
    return dayOfMonthLabel.isVisible();
  }

  /**
   * Enables or disables this component, depending on the value of the
   * parameter <code>b</code>. An enabled component can respond to user
   * input and generate events. Components are enabled initially by default.
   * @param     b   If <code>true</code>, this component is
   *            enabled; otherwise this component is disabled.
   * @see #isEnabled
   */
  public void setEnabled(boolean b)
  {
    timeTextField.setEnabled(b);
    dayComboBox.setEnabled(b);
    monthComboBox.setEnabled(b);
    yearComboBox.setEnabled(b);
    calendarButton.setEnabled(b);
  }

  /**
   * Enables or disables showing the labels.
   * @param b If true the labels are shown; otherwise the labels are not shown.
   */
  public void setShowLabels(boolean b)
  {
    dayOfMonthLabel.setVisible(b);
    monthLabel.setVisible(b);
    yearLabel.setVisible(b);
    timeLabel.setVisible(b);
  }

  /**
   * Sets the maximum time.
   * @param date the date to set or null to clear.
   * @return true if the maximum time is valid and was updated.
   */
  public boolean setMaxTime(Date date)
  {
    final boolean updateFlag = timeTextField.setMaxTime(date);
    if (updateFlag)
    {
      yearComboBox.setMaxTime(timeTextField.getMaxTime(), true);
    }
    return updateFlag;
  }

  /**
   * Sets the minimum time.
   * @param date the date to set or null to clear.
   * @return true if the minimum time is valid and was updated.
   */
  public boolean setMinTime(Date date)
  {
    final boolean updateFlag = timeTextField.setMinTime(date);
    if (updateFlag)
    {
      yearComboBox.setMinTime(timeTextField.getMinTime(), true);
    }
    return updateFlag;
  }

  /**
   * Sets the time.
   * @param date the date to set or null to clear.
   * @return true if the time is valid and was updated.
   */
  public boolean setTime(Date date)
  {
    setTimeUpdateFlag(true);  //time update in progress

    //update the time text field which will also update the shared calendar
    final boolean updatedFlag = timeTextField.setTime(date);
    if (updatedFlag)
    {
      updateDays();  //update the days of the month
      updateDateFields();  //update the date fields to the current date
    }

    setTimeUpdateFlag(false);  //time update completed

    if (updatedFlag)
    {
      fireUpdate(this, new EventObject(this)); //notifty listeners
    }
    return updatedFlag;
  }

  /**
   * Sets the time zone with the given time zone value.
   * @param value the given time zone.
   */
  public void setTimeZone(TimeZone value)
  {
    calObj.setTimeZone(value);
    timeTextField.setTimeZone(value);
    yearComboBox.setTimeZone(value);
    yearComboBox.updateYears();
  }

  /**
   * Sets the title of the time panel.
   * @param title the title for the time panel.
   */
  public void setTitle(String title)
  {
    borderObj.setTitle(title);
  }

  /**
   * Returns a string representation of this component and its values.
   * @return    a string representation of this component.
   */
  public String toString()
  {
    return getTime().toString();
  }

  /**
   * Component initialization
   * @throws Exception
   */
  private void jbInit() throws Exception
  {
    setBorder(BorderFactory.createCompoundBorder(borderObj,
        BorderFactory.createEmptyBorder(4,0,0,0)));
    setLayout(new FlowLayout());

    yearComboBox.setTimeZone(getTimeZone());
    yearComboBox.updateYears();

    updateDays();  //update the days of the month

    updateDateFields();  //update the date fields to the current date

    // now continue with setting up the fields
    monthComboBox.addActionListener(
        new UpdateHandler(monthComboBox, this));
    dayComboBox.addActionListener(
        new UpdateHandler(dayComboBox, this));
    yearComboBox.addActionListener(
        new UpdateHandler(yearComboBox, this));
    timeTextField.addFocusListener(
        new UpdateHandler(timeTextField, this));
    //setup <Enter> key action
    timeTextField.addActionListener(TransferFocusListener.LISTENER);

    // month entry
    monthLabel.setForeground(Color.black);
    monthLabel.setHorizontalAlignment(SwingConstants.CENTER);
    monthLabel.setHorizontalTextPosition(SwingConstants.CENTER);
    monthLabel.setText("Month: ");
    monthLabel.setToolTipText(monthTipText);
    monthComboBox.setToolTipText(monthTipText);

    final JPanel monthPanel = new JPanel();
    monthPanel.setLayout(new FlowLayout());
    monthPanel.add(monthLabel);
    monthPanel.add(monthComboBox, null);
    this.add(monthPanel);

    // day entry
    dayOfMonthLabel.setForeground(Color.black);
    dayOfMonthLabel.setHorizontalAlignment(SwingConstants.CENTER);
    dayOfMonthLabel.setHorizontalTextPosition(SwingConstants.CENTER);
    dayOfMonthLabel.setText("Day: ");
    dayOfMonthLabel.setToolTipText(dayOfMonthTextTip);
    dayComboBox.setToolTipText(dayOfMonthTextTip);

    final JPanel dayPanel = new JPanel();
    dayPanel.setLayout(new FlowLayout());
    dayPanel.add(dayOfMonthLabel);
    dayPanel.add(dayComboBox, null);
    this.add(dayPanel);

    // year entry
    yearLabel.setForeground(Color.black);
    yearLabel.setHorizontalAlignment(SwingConstants.CENTER);
    yearLabel.setHorizontalTextPosition(SwingConstants.CENTER);
    yearLabel.setText("Year: ");
    yearLabel.setToolTipText(yearTextTip);
    yearComboBox.setToolTipText(yearTextTip);
    calendarButton.setText("Calendar");
    calendarButton.setToolTipText(calendarTipText);
    calendarButton.addActionListener(new java.awt.event.ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        calendarButton_actionPerformed(e);
      }
    });

    final JPanel yearPanel = new JPanel();
    yearPanel.setLayout(new FlowLayout());
    yearPanel.add(yearLabel);
    yearPanel.add(yearComboBox, null);
    yearPanel.add(calendarButton, null);
    this.add(yearPanel);

    timeLabel.setForeground(Color.black);
    timeLabel.setHorizontalAlignment(SwingConstants.CENTER);
    timeLabel.setHorizontalTextPosition(SwingConstants.CENTER);
    timeLabel.setText("Time: ");
    timeLabel.setToolTipText(timeTipText);
    timeTextField.setToolTipText(timeTipText);

    final JPanel timePanel = new JPanel();
    timePanel.setLayout(new FlowLayout());
    timePanel.add(timeLabel);
    timePanel.add(timeTextField);
    this.add(timePanel);
  }

  /**
   * Gets the time update flag.
   * @return the time update flag.
   */
  protected boolean isTimeUpdate()
  {
    synchronized (timeUpdateLockObj)
    {
      return timeUpdateFlag;
    }
  }

  /**
   * Sets the time update flag.
   * @param b true if time is being updated, false otherwise.
   */
  protected void setTimeUpdateFlag(boolean b)
  {
    synchronized (timeUpdateLockObj)
    {
      timeUpdateFlag = b;
    }
  }

  /**
   * Update the date fields to the current date.
   */
  protected void updateDateFields()
  {
    monthComboBox.setSelectedIndex(getMonth());
    dayComboBox.setSelectedItem(String.valueOf(getDayOfMonth()));
    yearComboBox.setYear(getYear());
  }

  /**
   * Updates the days of the month.
   */
  protected void updateDays()
  {
    final int maxDay = calObj.getActualMaximum(Calendar.DAY_OF_MONTH);

    // if maximum changed
    if (displayMaxDays != maxDay)
    {
      // if more days than before
      if (maxDay > displayMaxDays)
      {
        // add more days
        for (int dayOfMonth = displayMaxDays + 1;
             dayOfMonth <= maxDay; dayOfMonth++)
        {
          dayComboBox.addItem(String.valueOf(dayOfMonth));
        }
      }
      else
      {
        // remove days
        for (int dayOfMonth = maxDay + 1;
             dayOfMonth <= displayMaxDays; dayOfMonth++)
        {
          dayComboBox.removeItem(String.valueOf(dayOfMonth));
        }
      }

      displayMaxDays = maxDay;  //save the new value
    }
  }

  /**
   * Calendar button processing
   * @param e action event
   */
  void calendarButton_actionPerformed(ActionEvent e)
  {
    // Bring up IstiCalendarPanel
    IstiCalendarPanel.createDialog(
        this, calObj, borderObj.getTitle() + " Calendar", getMinTime(), getMaxTime());

    final int month = getMonth();
    final int date = getDayOfMonth();
    final int year = getYear();

    // update the fields to the selected calendar
    monthComboBox.setSelectedIndex(month);
    dayComboBox.setSelectedItem(String.valueOf(date));
    yearComboBox.setYear(year);
  }

  /**
   * Gets the current month.
   * @return the month
   */
  public int getMonth()
  {
    return calObj.get(Calendar.MONTH);
  }

  /**
   * Gets the current day of the month.
   * @return the day of the month
   */
  public int getDayOfMonth()
  {
    return calObj.get(Calendar.DAY_OF_MONTH);
  }

  /**
   * Gets the current year.
   * @return the year
   */
  public int getYear()
  {
    return calObj.get(Calendar.YEAR);
  }

  /**
   * Gets the calendar for this panel.
   * @return the calendar for this panel.
   */
  public Calendar getCalendar()
  {
    final int month = getMonth();
    final int date = getDayOfMonth();
    final int year = getYear();
    calObj.set(year, month, date);

    timeTextField.updateTime();  //update the time
    return calObj;
  }

  // UpdateListener interface

  /**
   * Adds an <code>UpdateListener</code>. The listener will
   * receive an update event
   * the user finishes making a selection.
   *
   * @param l  the <code>UpdateListener</code> that is to be notified
   */
  public void addUpdateListener(UpdateListener l)
  {
    listenerList.add(UpdateListener.class, l);
  }

  /** Removes an <code>UpdateListener</code>.
   *
   * @param l  the <code>UpdateListener</code> to remove
   */
  public void removeUpdateListener(UpdateListener l)
  {
    listenerList.remove(UpdateListener.class, l);
  }

  /**
   * Notification of an update event.
   * @param component the component.
   * @param e the event or null if none.
   */
  protected void fireUpdate(Component component, Object e)
  {
    // Guaranteed to return a non-null array
    Object[] listeners = listenerList.getListenerList();
    // Process the listeners last to first, notifying
    // those that are interested in this event
    for (int i = listeners.length-2; i>=0; i-=2)
    {
      if (listeners[i]==UpdateListener.class)
        ((UpdateListener)listeners[i+1]).processUpdate(component, e);
    }
  }

  /**
   * Process an update.
   * @param component the component.
   * @param e the event or null if none.
   */
  public void processUpdate(Component component, Object e)
  {
    synchronized (timeUpdateLockObj)
    {
      if (isTimeUpdate())  //exit if time update in progress
        return;
    }

    int year = yearComboBox.getYear();
    int month = monthComboBox.getSelectedIndex();
    int date = Integer.parseInt(dayComboBox.getSelectedItem().toString());

    calObj.set(year, month, date);

    //validate the new time
    boolean inValidFlag;  //true if the new time is invalid
    boolean updateYearFlag = false;  //true if the year was updated
    Date updateTime = null;  //the update time or null if no update needed
    do
    {
      inValidFlag = false;
      final Date newTime = calObj.getTime();
      if (getMinTime() != null && newTime.before(getMinTime()))
      {
        //if year was updated or if already updated the year
        if (component == yearComboBox || updateYearFlag)
        {
          //use min time
          updateTime = getMinTime();
        }
        else
        {
          inValidFlag = true;
          calObj.set(Calendar.YEAR, ++year); //try next year
          updateYearFlag = true;
        }
      }
      if (getMaxTime() != null && newTime.after(getMaxTime()))
      {
        //if year was updated or if already updated the year
        if (component == yearComboBox || updateYearFlag)
        {
          //use max time
          updateTime = getMaxTime();
        }
        else
        {
          inValidFlag = true;
          calObj.set(Calendar.YEAR, --year); //try previous year
          updateYearFlag = true;
        }
      }
    }
    while (inValidFlag);
    if (updateTime != null)  //if the time was updated
    {
      setTime(updateTime);
      return;
    }
    if (updateYearFlag)  //if only the year was updated
    {
      yearComboBox.setYear(year);
    }

    updateDays();  //update the days of the month

    fireUpdate(this, e);  //notifty listeners
  }
}
