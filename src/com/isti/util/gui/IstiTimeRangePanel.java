//IstiTimeRangePanel.java:  Implements the time range panel.
//
//   8/21/2003 -- [KF]
//   1/26/2007 -- [KF]  Added 'setMinMaxPanelVisible',
//                      'getEndDate','getStartDate','setEndDate','setEndTime',
//                      'setStartDate' and 'setStartTime' methods,
//                      Added support for maximum duration value,
//                      Removed potentially hazardous synchronization done
//                      on the event dispatch thread.
//   4/19/2012 -- [KF]  Added 'getDurationChangeType', 'setDurationChangeType',
//                      'setQuickSelectButtonValue' and
//                      'setQuickSelectButtonValues'  methods;
//                      Added constructor with time units argument.
//

package com.isti.util.gui;

import java.awt.Color;
import java.awt.Component;
import java.awt.Font;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.FocusEvent;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.TimeZone;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JSlider;
import javax.swing.border.Border;
import javax.swing.border.TitledBorder;
import javax.swing.event.ChangeEvent;

import com.isti.util.TimeUnits;
import com.isti.util.UtilFns;

/**
 * Class IstiTimeRangePanel implements the time range panel.
 */
public class IstiTimeRangePanel extends IstiAbstractTimeRangePanel implements
    UpdateListener {
  public final static long MS_PER_HOUR = UtilFns.MS_PER_HOUR;

  public final static double FLOAT_MS_PER_HOUR = MS_PER_HOUR;

  /** The default values text separator text, which is a comma. */
  public static final String DEFAULT_VALUES_TEXT_SEPARATOR_TEXT = ",";

  protected final static Color FOREGROUND_COLOR = Color.black;

  protected static boolean showLabelsFlag = false;

  protected static boolean ignoreSlidersWhileValueIsAdjusting = false;

  protected static boolean debugFlag = false;

  public final static String DEFAULT_MIN_MAX_TITLE = "Min/Max";

  public final static String DEFAULT_START_TITLE = "Start";

  public final static String DEFAULT_END_TITLE = "End";

  public final static String DEFAULT_START_END_TITLE = DEFAULT_START_TITLE
      + "/" + DEFAULT_END_TITLE;

  public final static Double DEFAULT_MAX_DURATION_VALUE = null;

  private final TimeUnits offsetUnits;

  private final String minMaxTitle;

  private final String startEndTitle;

  private long minTime = 0;

  private Date minDate = null;

  private long maxTime = 0;

  private Date maxDate = null;

  private long offsetTimeMax = 0;

  private long durationTimeMax = 0;

  // max duration ms or negative for no max
  private long maxDurationMs = -1;

  private final JPanel minMaxPanel = new JPanel();

  private final JPanel startEndPanel = new JPanel();

  private final JSlider minTimeSlider = new TimeSlider("Minimum");

  private final IstiTimePanel minTimePanel = new IstiTimePanel();

  private final JSlider maxTimeSlider = new TimeSlider("Maximum",
      TimeSlider.MAX_VALUE);

  private final FloatJTextField minTimeOffsetText = new FloatJTextField();

  private final FloatJTextField maxTimeOffsetText = new FloatJTextField();

  private final IstiTimePanel maxTimePanel = new IstiTimePanel();

  private final JSlider startTimeSlider = new TimeSlider("Start");

  private final IstiTimePanel startTimePanel = new IstiTimePanel();

  private final FloatJTextField startTimeOffsetText = new FloatJTextField();

  private final JSlider endTimeSlider = new TimeSlider("End",
      TimeSlider.MAX_VALUE);

  private final IstiTimePanel endTimePanel = new IstiTimePanel();

  private final FloatJTextField endTimeOffsetText = new FloatJTextField();

  private final JSlider durationTimeSlider = new TimeSlider("Duration",
      TimeSlider.MAX_VALUE);

  private final FloatJTextField durationTimeOffsetText = new FloatJTextField();

  // duration change type
  private int durationChangeType = DurationChangeType.END_TIME;

  // quick select buttons
  private final JButton[] quickSelectButtons = { new JButton(), new JButton(),
      new JButton() };

  // private final Object processUpdateLock = new Object();
  private boolean processUpdateFlag = true; // start with true until initialized

  // tool tip text
  protected final String maxTimeSliderTipText;

  protected final String minTimeSliderTipText;

  protected final String sliderTipTextSuffix;

  protected final String startTimeSliderTipText;

  protected final String endTimeSliderTipText;

  protected final String durationTimeSliderTipText;

  protected final String offsetTipTextSuffix;

  protected final String minOffsetTipText;

  protected final String maxOffsetTipText;

  protected final String offsetTipText;

  protected final String durationTextTipText;

  /**
   * Constructs a time range panel.
   */
  public IstiTimeRangePanel() {
    this(DEFAULT_MIN_MAX_TITLE, DEFAULT_START_END_TITLE);
  }

  /**
   * Constructs a time range panel.
   * @param minMaxTitle the title for the min/max panel.
   * @param startEndTitle the title for the start/end panel.
   */
  public IstiTimeRangePanel(String minMaxTitle, String startEndTitle) {
    this(minMaxTitle, startEndTitle, null);
  }

  /**
   * Constructs a time range panel.
   * @param minMaxTitle the title for the min/max panel.
   * @param startEndTitle the title for the start/end panel.
   * @param value the given time zone.
   */
  public IstiTimeRangePanel(String minMaxTitle, String startEndTitle,
      TimeZone value) {
    this(minMaxTitle, startEndTitle, value, DEFAULT_START_TITLE,
        DEFAULT_END_TITLE, DEFAULT_MAX_DURATION_VALUE);
  }

  /**
   * Constructs a time range panel.
   * @param minMaxTitle the title for the min/max panel.
   * @param startEndTitle the title for the start/end panel.
   * @param value the given time zone.
   * @param startTitle the title for the start time panel.
   * @param endTitle the title for the end time panel.
   * @param maxDurationValue the max duration value or null for no max.
   */
  public IstiTimeRangePanel(String minMaxTitle, String startEndTitle,
      TimeZone value, String startTitle, String endTitle,
      Double maxDurationValue) {
    this(minMaxTitle, startEndTitle, value, startTitle, endTitle,
        maxDurationValue, TimeUnits.HOUR);
  }

  /**
   * Constructs a time range panel.
   * @param minMaxTitle the title for the min/max panel.
   * @param startEndTitle the title for the start/end panel.
   * @param value the given time zone.
   * @param startTitle the title for the start time panel.
   * @param endTitle the title for the end time panel.
   * @param maxDurationValue the max duration value or null for no max.
   */
  public IstiTimeRangePanel(String minMaxTitle, String startEndTitle,
      TimeZone value, String startTitle, String endTitle,
      Double maxDurationValue, TimeUnits offsetUnits) {
    this.minMaxTitle = minMaxTitle;
    this.startEndTitle = startEndTitle;
    this.offsetUnits = offsetUnits;
    if (value != null) // if the time zone was specified
      setTimeZone(value); // set the time zone
    if (startTitle != null)
      startTimePanel.setTitle(startTitle);
    if (endTitle != null)
      endTimePanel.setTitle(endTitle);
    if (maxDurationValue != null) {
      maxDurationMs = (long) (maxDurationValue.doubleValue() * getMsPerUnit());
    } else {
      maxDurationMs = -1;
    }

    updateTimeOffsets(); // update time offsets

    // set tool tip text
    maxTimeSliderTipText = "Latest available time";
    minTimeSliderTipText = "Earliest available time";
    sliderTipTextSuffix = " from minimum (left) to maximum (right)";
    startTimeSliderTipText = "Start time" + sliderTipTextSuffix;
    endTimeSliderTipText = "End time from" + sliderTipTextSuffix;
    durationTimeSliderTipText = "Duration of start/end time";
    offsetTipTextSuffix = " (in " + offsetUnits.toString(true) + ")";
    minOffsetTipText = "Minimum offset" + offsetTipTextSuffix;
    maxOffsetTipText = "Maximum offset" + offsetTipTextSuffix;
    offsetTipText = "Offset from minimum" + offsetTipTextSuffix;
    durationTextTipText = durationTimeSliderTipText + offsetTipTextSuffix;

    try {
      jbInit();
    } catch (Exception e) {
      e.printStackTrace();
    }

    setShowLabels(showLabelsFlag); // show/hide the labels
  }

  /**
   * Constructs a time range panel.
   * @param value the given time zone.
   */
  public IstiTimeRangePanel(TimeZone value) {
    this(DEFAULT_MIN_MAX_TITLE, DEFAULT_START_END_TITLE, value,
        DEFAULT_START_TITLE, DEFAULT_END_TITLE, DEFAULT_MAX_DURATION_VALUE);
  }

  /**
   * Constructs a time range panel.
   * @param value the given time zone.
   * @param maxDurationValue the max duration value or null for no max.
   */
  public IstiTimeRangePanel(TimeZone value, Double maxDurationValue) {
    this(DEFAULT_MIN_MAX_TITLE, DEFAULT_START_END_TITLE, value,
        DEFAULT_START_TITLE, DEFAULT_END_TITLE, maxDurationValue);
  }

  /**
   * Checks the values and makes sure the start time is not greater than the end
   * time.
   * @param startTimeUpdateFlag true if the start time was updated, false
   *          otherwise.
   * @param endTimeUpdateFlag true if the end time was updated, false otherwise.
   */
  protected void checkValues(boolean startTimeUpdateFlag,
      boolean endTimeUpdateFlag) {
    if (startTimeUpdateFlag) // if start time was updated
    {
      final Date startDate = getStartDate();
      final Date endDate = getEndDate();

      // if start time is greater than the end time
      if (startDate.compareTo(endDate) > 0) {
        // set the end time to the start time
        endTimePanel.setTime(startDate);
      } else if (!isValidDuration(startDate, endDate)) {
        // set the end time to the start time + max duration
        final Date newDate = new Date(startDate.getTime() + maxDurationMs);
        endTimePanel.setTime(newDate);
      }
    }

    if (endTimeUpdateFlag) // end time was updated
    {
      final Date startDate = getStartDate();
      final Date endDate = getEndDate();

      // if end time is less than the start time
      if (endDate.compareTo(startDate) < 0) {
        // set the start time to the end time
        startTimePanel.setTime(endDate);
      } else if (!isValidDuration(startDate, endDate)) {
        // set the start time to the end time - max duration
        final Date newDate = new Date(endDate.getTime() - maxDurationMs);
        startTimePanel.setTime(newDate);
      }
    }
  }

  /**
   * Get the duration change type.
   * @return the duration change type.
   */
  public int getDurationChangeType() {
    return durationChangeType;
  }

  /**
   * Gets the end date.
   * @return the end date.
   */
  public Date getEndDate() {
    return endTimePanel.getTime();
  }

  /**
   * Gets the end time (number of milliseconds since January 1, 1970, 00:00:00
   * GMT.)
   * @return the end time or 0 if it is greater than or equal to the maximum.
   */
  public long getEndTime() {
    long endValue = 0;
    final Date endDate = getEndDate();
    // if the end time is less than the max time
    if (maxDate != null && endDate.compareTo(maxDate) < 0)
      endValue = endDate.getTime();
    return endValue;
  }

  /**
   * Gets the maximum date;
   * @return the maximum date or null if not defined.
   */
  public Date getMaxDate() {
    return maxDate;
  }

  /**
   * Gets the maximum time (number of milliseconds since January 1, 1970,
   * 00:00:00 GMT.)
   * @return the maximum time.
   */
  public long getMaxTime() {
    return maxTime;
  }

  /**
   * Gets the minimum date;
   * @return the minimum date or null if not defined.
   */
  public Date getMinDate() {
    return minDate;
  }

  /**
   * Gets the minimum time (number of milliseconds since January 1, 1970,
   * 00:00:00 GMT.)
   * @return the minimum time.
   */
  public long getMinTime() {
    return minTime;
  }

  /**
   * Get the number of milliseconds per unit as a double.
   * @return the number of milliseconds per unit as a double
   */
  protected double getMsPerUnit() {
    return (double) offsetUnits.getMsPerUnit();
  }

  /**
   * Gets the number of quick select buttons.
   * @return the number of quick select buttons.
   */
  public int getQuickSelectButtonsLength() {
    return quickSelectButtons.length;
  }

  /**
   * Gets the start date.
   * @return the start date.
   */
  public Date getStartDate() {
    return startTimePanel.getTime();
  }

  /**
   * Gets the start time (number of milliseconds since January 1, 1970, 00:00:00
   * GMT.)
   * @return the start time or 0 if it is less than or equal to the minumum.
   */
  public long getStartTime() {
    long startValue = 0;
    final Date startDate = getStartDate();
    // if the start time is greater than the min time
    if (minDate != null && startDate.compareTo(minDate) > 0)
      startValue = startDate.getTime();
    return startValue;
  }

  /**
   * Gets the time zone.
   * @return the time zone object associated with this panel.
   */
  public TimeZone getTimeZone() {
    return minTimePanel.getTimeZone();
  }

  /**
   * Get the value text array.
   * @param valuesText the comma separated values text.
   * @return the value text array.
   */
  protected String[] getValueTextArray(String valuesText) {
    return getValueTextArray(valuesText, DEFAULT_VALUES_TEXT_SEPARATOR_TEXT);
  }

  /**
   * Get the value text array.
   * @param valuesText the values text.
   * @param separatorText the separator text.
   * @return the value text array.
   */
  protected String[] getValueTextArray(String valuesText, String separatorText) {
    return valuesText.split(separatorText);
  }

  /**
   * Determines if the labels are shown.
   * @return true if the labels are shown, false otherwise.
   */
  public boolean isShowLabels() {
    return minTimePanel.isVisible();
  }

  /**
   * Determines if the duration is valid.
   * @param startDate the start date.
   * @param endDate the end date.
   * @return true if the duration is valid, false otherwise.
   */
  private boolean isValidDuration(Date startDate, Date endDate) {
    if (maxDurationMs >= 0) {
      final long durationMs = endDate.getTime() - startDate.getTime();
      if (durationMs > maxDurationMs) {
        return false;
      }
    }
    return true;
  }

  private void jbInit() throws Exception {
    this.setLayout(new GridBagLayout());
    minMaxPanel.setLayout(new GridBagLayout());
    startEndPanel.setLayout(new GridBagLayout());

    // remove the borders from the time panels
    minTimePanel.setBorder(null);
    maxTimePanel.setBorder(null);
    startTimePanel.setBorder(null);
    endTimePanel.setBorder(null);

    // add listeners
    startTimeSlider.addChangeListener(new UpdateHandler(startTimeSlider, this));
    startTimePanel.addUpdateListener(new UpdateHandler(startTimePanel, this));
    startTimeOffsetText.addFocusListener(new UpdateHandler(startTimeOffsetText,
        this));
    // setup <Enter> key action
    startTimeOffsetText.addActionListener(TransferFocusListener.LISTENER);
    endTimeSlider.addChangeListener(new UpdateHandler(endTimeSlider, this));
    durationTimeSlider.addChangeListener(new UpdateHandler(durationTimeSlider,
        this));
    endTimePanel.addUpdateListener(new UpdateHandler(endTimePanel, this));
    endTimeOffsetText.addFocusListener(new UpdateHandler(endTimeOffsetText,
        this));
    // setup <Enter> key action
    endTimeOffsetText.addActionListener(TransferFocusListener.LISTENER);
    durationTimeOffsetText.addFocusListener(new UpdateHandler(
        durationTimeOffsetText, this));
    // setup <Enter> key action
    durationTimeOffsetText.addActionListener(TransferFocusListener.LISTENER);

    // set tool tips
    maxTimeSlider.setToolTipText(maxTimeSliderTipText);
    minTimeSlider.setToolTipText(minTimeSliderTipText);
    startTimeSlider.setToolTipText(startTimeSliderTipText);
    endTimeSlider.setToolTipText(endTimeSliderTipText);
    durationTimeSlider.setToolTipText(durationTimeSliderTipText);
    minTimeOffsetText.setToolTipText(minOffsetTipText);
    maxTimeOffsetText.setToolTipText(maxOffsetTipText);
    startTimeOffsetText.setToolTipText(offsetTipText);
    endTimeOffsetText.setToolTipText(offsetTipText);
    durationTimeOffsetText.setToolTipText(durationTextTipText);

    // disable min/max components
    minTimeSlider.setEnabled(false);
    maxTimeSlider.setEnabled(false);
    minTimePanel.setEnabled(false);
    maxTimePanel.setEnabled(false);
    minTimeOffsetText.setEnabled(false);
    maxTimeOffsetText.setEnabled(false);

    // add the coomponents
    GridBagConstraints constraints = new GridBagConstraints();
    constraints.gridx = 0;
    constraints.gridy = 0;
    minMaxPanel.add(minTimeSlider, constraints);
    constraints.gridx = 1;
    minMaxPanel.add(minTimePanel, constraints);
    constraints.gridx = 2;
    minMaxPanel.add(minTimeOffsetText, constraints);

    constraints.gridx = 0;
    constraints.gridy = 1;
    minMaxPanel.add(maxTimeSlider, constraints);
    constraints.gridx = 1;
    minMaxPanel.add(maxTimePanel, constraints);
    constraints.gridx = 2;
    minMaxPanel.add(maxTimeOffsetText, constraints);

    minMaxPanel.setBorder(new TimeRangeTitledBorder(minMaxTitle));
    constraints.gridx = 0;
    constraints.gridy = 1;
    constraints.gridwidth = 3;
    this.add(minMaxPanel, constraints);

    constraints.gridwidth = 1;
    constraints.gridx = 0;
    constraints.gridy = 0;
    startEndPanel.add(startTimeSlider, constraints);
    constraints.gridx = 1;
    startEndPanel.add(startTimePanel, constraints);
    constraints.gridx = 0;
    startEndPanel.add(startTimeOffsetText, new IstiGridBagConstraints(2, 0));

    constraints.gridx = 0;
    constraints.gridy = 1;
    startEndPanel.add(endTimeSlider, constraints);
    constraints.gridx = 1;
    startEndPanel.add(endTimePanel, constraints);
    constraints.gridx = 2;
    startEndPanel.add(endTimeOffsetText, constraints);

    constraints.gridx = 0;
    constraints.gridy = 2;
    startEndPanel.add(durationTimeSlider, constraints);
    constraints.gridx = 2;
    startEndPanel.add(durationTimeOffsetText, constraints);

    constraints.gridx = 1;
    final JPanel startEndSubPanel = new JPanel();
    ActionListener quickSelectButtonListener = new ActionListener() {
      public void actionPerformed(ActionEvent e) {
        Object source = e.getSource();
        if (source instanceof JButton) {
          String s = ((JButton) source).getText();
          double offsetValue = TimeUnits.getMilliseconds(s, offsetUnits);
          setEndDate(getMaxDate());
          processDurationTimeUpdate(offsetValue, true);
        }
      }
    };
    for (int i = 0; i < getQuickSelectButtonsLength(); i++) {
      JButton button = quickSelectButtons[i];
      button.addActionListener(quickSelectButtonListener);
      button.setVisible(false);
      startEndSubPanel.add(button);
    }
    startEndPanel.add(startEndSubPanel, constraints);

    startEndPanel.setBorder(new TimeRangeTitledBorder(startEndTitle));
    constraints.gridx = 0;
    constraints.gridy = 2;
    constraints.gridwidth = 3;
    this.add(startEndPanel, constraints);
  }

  /**
   * Print the current values.
   * @param title the title.
   */
  protected void printValues(String title) {
    System.out.println(title + ": start = " + startTimePanel + " ("
        + startTimeOffsetText.getText() + ")" + ", end = " + endTimePanel
        + " (" + endTimeOffsetText.getText() + ")" + ", duration = "
        + durationTimeOffsetText.getText());
  }

  /**
   * Process the duration time update.
   * @param offsetValue the offset value in milliseconds.
   */
  private void processDurationTimeUpdate(double offsetValue) {
    final boolean modifyStartTimeFlag;
    switch (durationChangeType) {
    case DurationChangeType.START_TIME:
      modifyStartTimeFlag = true;
      break;
    case DurationChangeType.END_TIME:
    default:
      modifyStartTimeFlag = false;
      break;
    }
    processDurationTimeUpdate(offsetValue, modifyStartTimeFlag);
  }

  /**
   * Process the duration time update.
   * @param offsetValue the offset value in milliseconds.
   * @param modifyStartTimeFlag true to modify the start time, false to modify
   *          the end time.
   */
  private void processDurationTimeUpdate(double offsetValue,
      boolean modifyStartTimeFlag) {
    if (modifyStartTimeFlag) {
      Date startDate = new Date(getEndDate().getTime()
          - Math.round(offsetValue));
      // if start date is equal or after minimum
      if (startDate.compareTo(minTimePanel.getTime()) >= 0) {
        startTimePanel.setTime(startDate);
      } else {
        // set start date to minimum
        startDate = minTimePanel.getTime();
        startTimePanel.setTime(startDate);
        final long startTime = startDate.getTime();
        Date endDate = new Date(startTime + Math.round(offsetValue));
        // if end date is equal or before max time
        if (endDate.compareTo(maxTimePanel.getTime()) <= 0) {
          endTimePanel.setTime(endDate);
        }
      }
    } else {
      Date endDate = new Date(getStartDate().getTime()
          + Math.round(offsetValue));
      // if end date is equal or before max time
      if (endDate.compareTo(maxTimePanel.getTime()) <= 0) {
        endTimePanel.setTime(endDate);
      } else {
        // set end to maximum
        endDate = maxTimePanel.getTime();
        endTimePanel.setTime(endDate);
        final long endTime = endDate.getTime();
        Date startDate = new Date(endTime - Math.round(offsetValue));
        // if start date is equal or after minimum
        if (startDate.compareTo(minTimePanel.getTime()) >= 0) {
          startTimePanel.setTime(startDate);
        }
      }
    }
  }

  /**
   * Process an update.
   * @param component the component.
   * @param e the event or null if none.
   */
  public void processUpdate(Component component, Object e) {
    if (e instanceof ChangeEvent) // if change event
    {
      if (component instanceof JSlider) // if the component is a slider
      {
        // return ignore sliders while value is adjusting is true and
        // the value is adjusting
        if (ignoreSlidersWhileValueIsAdjusting
            && ((JSlider) component).getValueIsAdjusting())
          return;
      }
    } else if (e instanceof FocusEvent) // if focus event
    {
      // return if not focus lost
      if (((FocusEvent) e).getID() != FocusEvent.FOCUS_LOST)
        return;
    }

    // synchronized (processUpdateLock)
    {
      if (processUpdateFlag) // exit if already processing an update
        return;
      processUpdateFlag = true; // start update processing
    }

    if (debugFlag)
      printValues("Before Update");

    boolean startTimeUpdateFlag = false;
    boolean endTimeUpdateFlag = false;
    boolean durationTimeUpdateFlag = false;
    double offsetValue = 0;

    if (component.equals(startTimeSlider)) {
      final int sliderValue = startTimeSlider.getValue();
      offsetValue = (offsetTimeMax * sliderValue) / TimeSlider.MAX_VALUE;
      final Date startDate = new Date(minTime + Math.round(offsetValue));
      startTimePanel.setTime(startDate);
      startTimeOffsetText.setValue(offsetValue / getMsPerUnit());
      startTimeUpdateFlag = true; // start time was updated
    } else if (component.equals(startTimePanel)) {
      final Date startDate = getStartDate();

      if (startDate != null) {
        final long startTime = startDate.getTime();
        offsetValue = startTime - minTime;
        final int sliderValue = (int) ((offsetValue * TimeSlider.MAX_VALUE) / offsetTimeMax);

        // if the slider value was valid
        if (setSliderValue(startTimeSlider, sliderValue)) {
          startTimeOffsetText.setValue(offsetValue / getMsPerUnit());
          startTimeUpdateFlag = true; // start time was updated
        } else {
          if (debugFlag)
            System.out.println("Invalid slider value (" + sliderValue
                + ") for " + component.getName() + " (date=" + startDate + ")");
        }
      }
    } else if (component.equals(startTimeOffsetText)) {
      offsetValue = startTimeOffsetText.getValue();
      offsetValue *= getMsPerUnit();

      final int sliderValue = (int) ((offsetValue * TimeSlider.MAX_VALUE) / offsetTimeMax);
      final Date startDate = new Date(minTime + (long) offsetValue);

      // if the slider value was valid
      if (setSliderValue(startTimeSlider, sliderValue)) {
        startTimePanel.setTime(startDate);
        startTimeUpdateFlag = true; // start time was updated
      } else {
        if (debugFlag)
          System.out.println("Invalid slider value (" + sliderValue + ") for "
              + component.getName() + " (date=" + startDate + ")");
      }
    } else if (component.equals(endTimeSlider)) {
      final int sliderValue = endTimeSlider.getValue();
      offsetValue = (offsetTimeMax * sliderValue) / TimeSlider.MAX_VALUE;
      final Date endDate = new Date(minTime + Math.round(offsetValue));
      endTimePanel.setTime(endDate);
      endTimeOffsetText.setValue(offsetValue / getMsPerUnit());
      endTimeUpdateFlag = true; // end time was updated
    } else if (component.equals(endTimePanel)) {
      final Date endDate = getEndDate();

      if (endDate != null) {
        final long endTime = endDate.getTime();
        offsetValue = endTime - minTime;
        final int sliderValue = (int) ((offsetValue * TimeSlider.MAX_VALUE) / offsetTimeMax);

        // if the slider value was valid
        if (setSliderValue(endTimeSlider, sliderValue)) {
          endTimeOffsetText.setValue(offsetValue / getMsPerUnit());
          endTimeUpdateFlag = true; // end time was updated
        } else {
          if (debugFlag)
            System.out.println("Invalid slider value (" + sliderValue
                + ") for " + component.getName() + " (date=" + endDate + ")");
        }
      }
    } else if (component.equals(endTimeOffsetText)) {
      offsetValue = endTimeOffsetText.getValue();
      offsetValue *= getMsPerUnit();

      final int sliderValue = (int) ((offsetValue * TimeSlider.MAX_VALUE) / offsetTimeMax);
      final Date endDate = new Date(minTime + (long) offsetValue);

      // if the slider value was valid
      if (setSliderValue(endTimeSlider, sliderValue)) {
        endTimePanel.setTime(endDate);
        endTimeUpdateFlag = true; // end time was updated
      } else {
        if (debugFlag)
          System.out.println("Invalid slider value (" + sliderValue + ") for "
              + component.getName() + " (date=" + endDate + ")");
      }
    } else if (component.equals(durationTimeSlider)) {
      final int sliderValue = durationTimeSlider.getValue();
      offsetValue = (durationTimeMax * sliderValue) / TimeSlider.MAX_VALUE;
      setDurationTimeOffsetValue(offsetValue / getMsPerUnit());
      durationTimeUpdateFlag = true; // duration time was updated
    } else if (component.equals(durationTimeOffsetText)) {
      offsetValue = durationTimeOffsetText.getValue();
      offsetValue *= getMsPerUnit();
      final int sliderValue = (int) Math
          .round((offsetValue * (double) TimeSlider.MAX_VALUE)
              / (double) durationTimeMax);
      setSliderValue(durationTimeSlider, sliderValue);
      durationTimeUpdateFlag = true; // duration time was updated
    } else {
      if (debugFlag)
        System.out.println(component.getName());
    }

    if (startTimeUpdateFlag) // if start time was updated
    {
      updateDurationTimes(component); // update the duration times
    }
    if (endTimeUpdateFlag) // if end time was updated
    {
      updateDurationTimes(component); // update the duration times
    }

    // synchronized (processUpdateLock)
    {
      processUpdateFlag = false; // end update processing
    }

    // if duration was updated
    if (durationTimeUpdateFlag) {
      processDurationTimeUpdate(offsetValue);
    }

    // if time was updated
    if (startTimeUpdateFlag || endTimeUpdateFlag) {
      // make sure values are valid
      checkValues(startTimeUpdateFlag, endTimeUpdateFlag);
    }

    if (debugFlag)
      printValues("After Update");
  }

  /**
   * Requests that the panel get the input focus.
   */
  public void requestFocus() {
    super.requestFocus();
    startTimeSlider.requestFocus(); // put focus on the start time slider
  }

  /**
   * Set the duration change type.
   * @param durationChangeType the duration change type.
   */
  public void setDurationChangeType(int durationChangeType) {
    this.durationChangeType = durationChangeType;
  }

  /**
   * Sets the duration time offset value.
   * @param value the duration time offset value.
   */
  private void setDurationTimeOffsetValue(double value) {
    final Double valueObj = Double.valueOf(value);
    final Comparable maxValueObj = durationTimeOffsetText.getMaxValueObj();
    if (maxValueObj instanceof Number && maxValueObj.compareTo(valueObj) < 0) {
      final double currentValue = durationTimeOffsetText.getValue();
      final double newValue = ((Number) maxValueObj).doubleValue();
      if (newValue != currentValue) {
        durationTimeOffsetText.setValue(newValue);
        processDurationTimeUpdate(newValue * getMsPerUnit());
      }
    }
    durationTimeOffsetText.setValue(value);
  }

  /**
   * Sets the end date.
   * @param endDate the end date.
   */
  public void setEndDate(Date endDate) {
    endTimePanel.setTime(endDate);
  }

  /**
   * Sets the end time.
   * @param endTime the end time.
   */
  public void setEndTime(long endTime) {
    endTimePanel.setTime(new Date(endTime));
  }

  /**
   * Sets the maximum date.
   * @param maxDate the maximum date.
   */
  public void setMaxDate(Date maxDate) {
    this.maxDate = maxDate;
    maxTime = maxDate.getTime();
    maxTimePanel.setTime(maxDate);
    startTimePanel.setMaxTime(maxDate);
    endTimePanel.setMaxTime(maxDate);

    updateTimeOffsets(); // update time offsets
  }

  /**
   * Sets the maximum time (number of milliseconds since January 1, 1970,
   * 00:00:00 GMT.)
   * @param time the maximum time.
   */
  public void setMaxTime(long time) {
    setMaxDate(new Date(time));
  }

  /**
   * Sets the minimum date.
   * @param minDate the minimum date.
   */
  public void setMinDate(Date minDate) {
    this.minDate = minDate;
    minTime = minDate.getTime();
    minTimePanel.setTime(minDate);
    startTimePanel.setMinTime(minDate);
    endTimePanel.setMinTime(minDate);

    updateTimeOffsets(); // update time offsets
  }

  /**
   * Sets the min/max panel visible.
   * @param b true for visible, false otherwise.
   */
  public void setMinMaxPanelVisible(boolean b) {
    minMaxPanel.setVisible(b);
  }

  /**
   * Sets the minimum time (number of milliseconds since January 1, 1970,
   * 00:00:00 GMT.)
   * @param time the minimum time.
   */
  public void setMinTime(long time) {
    setMinDate(new Date(time));
  }

  /**
   * Sets the text for the specified quick select button.
   * @param buttonIndex the button index.
   * @param buttonText the button text or null to disable.
   */
  protected void setQuickSelectButtonText(int buttonIndex, String buttonText) {
    JButton button = quickSelectButtons[buttonIndex];
    button.setText(buttonText);
    button.setVisible(buttonText != null);
  }

  /**
   * Sets the value for the specified quick select button.
   * @param buttonIndex the button index.
   * @param value the number of time units. The number must be greater or equal
   *          to 1 to make the button visible.
   */
  public void setQuickSelectButtonValue(int buttonIndex, int value) {
    String buttonText = null;
    if (value >= 1) {
      buttonText = offsetUnits.getTextWithValue(value);
    }
    setQuickSelectButtonText(buttonIndex, buttonText);
  }

  /**
   * Sets the value for the specified quick select button.
   * @param buttonIndex the button index.
   * @param valueText the value text is the number of time units and may include
   *          text for the time units. The number must be greater or equal to 1
   *          to make the button visible.
   */
  public void setQuickSelectButtonValue(int buttonIndex, String valueText) {
    String buttonText = TimeUnits.getTextValueWithTimeUnits(valueText,
        offsetUnits, 1);
    setQuickSelectButtonText(buttonIndex, buttonText);
  }

  /**
   * Set the quick selection button values.
   * @param valuesText the comma separated values text.
   */
  public void setQuickSelectButtonValues(String valuesText) {
    String[] valueTextArray = getValueTextArray(valuesText);
    setQuickSelectButtonValues(valueTextArray);
  }

  /**
   * Set the quick selection button values.
   * @param valueTextArray the value text array.
   */
  public void setQuickSelectButtonValues(String[] valueTextArray) {
    for (int buttonIndex = 0; buttonIndex < valueTextArray.length; buttonIndex++) {
      if (buttonIndex >= getQuickSelectButtonsLength()) {
        break;
      }
      setQuickSelectButtonValue(buttonIndex, valueTextArray[buttonIndex]);
    }
  }

  /**
   * Enables or disables showing the labels.
   * @param b If true the labels are shown; otherwise the labels are not shown.
   */
  public void setShowLabels(boolean b) {
    minTimePanel.setShowLabels(b);
    maxTimePanel.setShowLabels(b);
    startTimePanel.setShowLabels(b);
    endTimePanel.setShowLabels(b);
  }

  /**
   * Sets the start date.
   * @param startDate the start date.
   */
  public void setStartDate(Date startDate) {
    startTimePanel.setTime(startDate);
  }

  /**
   * Sets the start time.
   * @param startTime the start time.
   */
  public void setStartTime(long startTime) {
    startTimePanel.setTime(new Date(startTime));
  }

  /**
   * Sets the time zone with the given time zone value.
   * @param value the given time zone.
   */
  public void setTimeZone(TimeZone value) {
    minTimePanel.setTimeZone(value);
    maxTimePanel.setTimeZone(value);
    startTimePanel.setTimeZone(value);
    endTimePanel.setTimeZone(value);
  }

  /**
   * Update the duration times.
   * @param component the component.
   */
  protected void updateDurationTimes(Component component) {
    final double offsetValue = getEndDate().getTime()
        - getStartDate().getTime();
    final int sliderValue = (int) Math
        .round((offsetValue * (double) TimeSlider.MAX_VALUE)
            / (double) durationTimeMax);
    if (!component.equals(durationTimeSlider))
      durationTimeSlider.setValue(sliderValue);
    if (!component.equals(durationTimeOffsetText))
      setDurationTimeOffsetValue(offsetValue / getMsPerUnit());
  }

  /**
   * Updates the time offsets.
   */
  protected void updateTimeOffsets() {
    final boolean b;

    if (minDate != null && maxDate != null) {
      b = true;

      // determine the offset time maximum
      offsetTimeMax = maxTime - minTime;
      final double offsetTimeMaxHour = offsetTimeMax / getMsPerUnit();
      final long minMaxdiffTimeMs = maxTime - minTime;
      if (maxDurationMs < 0 || minMaxdiffTimeMs <= maxDurationMs) {
        durationTimeMax = minMaxdiffTimeMs;
      } else {
        durationTimeMax = maxDurationMs;
      }

      maxTimeOffsetText.setMaxValue(offsetTimeMaxHour);
      maxTimeOffsetText.setValue(offsetTimeMaxHour);
      startTimeOffsetText.setMaxValue(offsetTimeMaxHour);
      endTimeOffsetText.setMaxValue(offsetTimeMaxHour);
      processUpdateFlag = false; // intialization completed

      processUpdate(startTimePanel, null); // update the start time
      processUpdate(endTimePanel, null); // update the end time
    } else {
      b = false;
    }

    // disable/enable start/end/duration components
    startTimeSlider.setEnabled(b);
    endTimeSlider.setEnabled(b);
    startTimePanel.setEnabled(b);
    endTimePanel.setEnabled(b);
    startTimeOffsetText.setEnabled(b);
    endTimeOffsetText.setEnabled(b);
    durationTimeSlider.setEnabled(b);
    durationTimeOffsetText.setEnabled(b);

    requestFocus(); // reset the default focus component
  }

  /**
   * The duration change type determines how changes to duration are handled.
   */
  public static interface DurationChangeType {
    /** Modify the end time to be the start time plus the duration */
    public static final int END_TIME = 0;

    /** Modify the start time to be the end time minus the duration */
    public static final int START_TIME = 1;
  }

  public static void main(String[] args) {
    boolean debugFlag = false;

    for (int i = 0; i < args.length; i++) {
      if (args[i].toLowerCase().equals("debug"))
        debugFlag = true;
    }

    TimeJTextField.debugFlag = debugFlag;
    IstiTimePanel.debugFlag = debugFlag;
    IstiTimeRangePanel.debugFlag = debugFlag;

    // create and set up the frame
    final JFrame frameObj = new JFrame("IstiTimeRangePanel");

    final IstiTimeRangePanel timeRangePanel = new IstiTimeRangePanel(
        "Data Date/Time Range", "Choose a viewing window Date/Time",
        UtilFns.GMT_TIME_ZONE_OBJ, DEFAULT_START_TITLE, DEFAULT_END_TITLE,
        DEFAULT_MAX_DURATION_VALUE, TimeUnits.DAY);
    timeRangePanel.setDurationChangeType(DurationChangeType.START_TIME);
    timeRangePanel.setQuickSelectButtonValues("1,3,7");

    // set up default start and end time
    final Date startDate;
    final Date endDate;
    final GregorianCalendar calendar = new GregorianCalendar(
        timeRangePanel.getTimeZone());
    calendar.add(GregorianCalendar.HOUR_OF_DAY, -1); // make sure there is at
                                                     // least an hour
    calendar.set(GregorianCalendar.HOUR_OF_DAY, 0);
    calendar.set(GregorianCalendar.MINUTE, 0);
    calendar.set(GregorianCalendar.SECOND, 0);
    calendar.set(GregorianCalendar.MILLISECOND, 0);
    startDate = calendar.getTime();
    calendar.add(GregorianCalendar.HOUR_OF_DAY, 1);
    endDate = calendar.getTime();
    calendar.setTime(new Date(0L));
    calendar.add(GregorianCalendar.MONTH, 1);
    calendar.add(GregorianCalendar.YEAR, -1);
    final Date maxDate = new Date();
    timeRangePanel.setMaxDate(maxDate);
    final Date minDate = calendar.getTime();
    timeRangePanel.setMinDate(minDate);
    timeRangePanel.setEndDate(endDate);
    timeRangePanel.setStartDate(startDate);

    // add main content panel to frame:
    frameObj.getContentPane().add(timeRangePanel);
    frameObj.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
    frameObj.pack();
    frameObj.setVisible(true); // make frame visible
  }

  /**
   * Sets the slider value for the specified slider.
   * @param slider the slider.
   * @param value the value.
   * @return true if the value was valid and was set, false otherwise.
   */
  protected static boolean setSliderValue(JSlider slider, int value) {
    if (value >= slider.getMinimum() && value <= slider.getMaximum()) {
      slider.setValue(value);
      return true;
    }
    return false;
  }
}

/**
 * Time Range Titled Border.
 */
class TimeRangeTitledBorder extends TitledBorder {
  /**
   * Constructs a Time Range Titled Border.
   * @param border the border
   * @param title the title the border should display
   * @param titleJustification the justification for the title
   * @param titlePosition the position for the title
   * @param titleFont the font of the title
   * @param titleColor the color of the title
   */
  TimeRangeTitledBorder(Border border, String title, int titleJustification,
      int titlePosition, Font titleFont, Color titleColor) {
    super(border, title, titleJustification, titlePosition, titleFont,
        titleColor);
  }

  /**
   * Constructs a Time Range Titled Border.
   * @param title the title the border should display
   */
  TimeRangeTitledBorder(String title) {
    this(null, title, TitledBorder.CENTER, TitledBorder.TOP, null,
        IstiTimeRangePanel.FOREGROUND_COLOR);
  }
}

/**
 * Time slider.
 */
class TimeSlider extends JSlider {
  protected final static int MIN_VALUE = 0;

  protected final static int MAX_VALUE = 100;

  /**
   * Constructs a time slider with the minimum value.
   * @param title the title for the slider.
   */
  TimeSlider(String title) {
    this(title, MIN_VALUE);
  }

  /**
   * Constructs a time slider with the specified value.
   * @param title the title for the slider.
   * @param value the slider value.
   */
  TimeSlider(String title, int value) {
    super(MIN_VALUE, MAX_VALUE, value);
    if (title != null)
      setBorder(new TimeRangeTitledBorder(title));
  }
}
