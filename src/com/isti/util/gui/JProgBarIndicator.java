//JProgBarIndicator.java:  Extends the 'JProgressBar' class to be an
//                         implementor of 'ProgressIndicatorInterface'.
//
//  10/3/2003 -- [ET]  Initial version.
// 10/14/2003 -- [ET]  Moved from "com.isti.util" to "com.isti.util.gui"
//                     package.
// 10/16/2003 -- [ET]  Improved javadoc comments.
//

package com.isti.util.gui;

import javax.swing.JProgressBar;
import javax.swing.BoundedRangeModel;
import com.isti.util.ProgressIndicatorInterface;

/**
 * Class JProgBarIndicator extends the 'JProgressBar' class to be an
 * implementor of 'ProgressIndicatorInterface'.
 */
public class JProgBarIndicator extends JProgressBar implements
                                                  ProgressIndicatorInterface
{
 /**
   * Creates a horizontal progress bar.
   * The default orientation for progress bars is
   * <code>JProgressBar.HORIZONTAL</code>.
   * By default, the String is set to <code>null</code> and the
   * StringPainted is not painted.
   * The border is painted by default.
   * Uses the defaultMinimum (0) and defaultMaximum (100).
   * Uses the defaultMinimum for the initial value of the progress bar.
   */
  public JProgBarIndicator()
  {
  }

 /**
   * Creates a progress bar with the specified orientation.
   * By default, the String is set to <code>null</code> and the
   * StringPainted is not painted.
   * The border is painted by default.
   * Uses the defaultMinimum (0) and defaultMaximum (100).
   * Uses the defaultMinimum for the initial value of the progress bar.
   * @param orient the orientation to use, which can be either
   * <code>JProgressBar.VERTICAL</code> or
   * <code>JProgressBar.HORIZONTAL</code>.
   */
  public JProgBarIndicator(int orient)
  {
    super(orient);
  }

  /**
   * Creates a horizontal progress bar, which is the default.
   * By default, the String is set to <code>null</code> and the
   * StringPainted is not painted.
   * The border is painted by default.
   * Uses the specified minimum for the initial value of the progress bar.
   * @param min the minimum value to use.
   * @param max the maximum value to use.
   */
  public JProgBarIndicator(int min, int max)
  {
    super(min, max);
  }

  /**
   * Creates a progress bar using the specified orientation,
   * minimum, and maximum.
   * By default, the String is set to <code>null</code> and the
   * StringPainted is not painted.
   * The border is painted by default.
   * Sets the inital value of the progress bar to the specified minimum.
   * The BoundedRangeModel that sits underneath the progress bar
   * handles any issues that may arrise from improperly setting the
   * minimum, value, and maximum on the progress bar.
   * @param orient the orientation to use, which can be either
   * <code>JProgressBar.VERTICAL</code> or
   * <code>JProgressBar.HORIZONTAL</code>.
   * @param min the minimum value to use.
   * @param max the maximum value to use.
   */
  public JProgBarIndicator(int orient, int min, int max)
  {
    super(orient, min, max);
  }

  /**
   * Creates a horizontal progress bar, the default orientation.
   * By default, the String is set to <code>null</code> and the
   * StringPainted is not painted.
   * The border is painted by default.
   * @param newModel the BoundedRangeModel to use, which holds the
   * minimum, value, and maximum values.
   */
  public JProgBarIndicator(BoundedRangeModel newModel)
  {
    super(newModel);
  }
}
