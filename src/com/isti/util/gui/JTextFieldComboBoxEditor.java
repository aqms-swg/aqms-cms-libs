//JTextFieldComboBoxEditor.java: An implementation of a ComboBoxEditor that
//                               uses a JTextField.
//
//=====================================================================
// Copyright (C) 2007 Instrumental Software Technologies, Inc.
//
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions
// are met:
// 1. Redistributions of source code, or portions of this source code,
//    must retain the above copyright notice, this list of conditions
//    and the following disclaimer.
// 2. Redistributions in binary form must reproduce the above copyright
//    notice, this list of conditions and the following disclaimer in
//    the documentation and/or other materials provided with the
//    distribution.
// 3. All advertising materials mentioning features or use of this
//    software must display the following acknowledgment:
//    "This product includes software developed by Instrumental
//    Software Technologies, Inc. (http://www.isti.com)"
// 4. If the software is provided with, or as part of a commercial
//    product, or is used in other commercial software products the
//    customer must be informed that "This product includes software
//    developed by Instrumental Software Technologies, Inc.
//    (http://www.isti.com)"
// 5. The names "Instrumental Software Technologies, Inc." and "ISTI"
//    must not be used to endorse or promote products derived from
//    this software without prior written permission. For written
//    permission, please contact "info@isti.com".
// 6. Products derived from this software may not be called "ISTI"
//    nor may "ISTI" appear in their names without prior written
//    permission of Instrumental Software Technologies, Inc.
// 7. Redistributions of any form whatsoever must retain the following
//    acknowledgment:
//    "This product includes software developed by Instrumental
//    Software Technologies, Inc. (http://www.isti.com/)."
// THIS SOFTWARE IS PROVIDED BY INSTRUMENTAL SOFTWARE
// TECHNOLOGIES, INC. "AS IS" AND ANY EXPRESSED OR IMPLIED
// WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES
// OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
// DISCLAIMED.  IN NO EVENT SHALL INSTRUMENTAL SOFTWARE TECHNOLOGIES,
// INC. OR ITS CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
// INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
// (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
// SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
// HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
// STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
// ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED
// OF THE POSSIBILITY OF SUCH DAMAGE.
//=====================================================================
//  A current version of the software can be found at
//                http://www.isti.com
//  Bug reports and comments should be directed to
//  Instrumental Software Technologies, Inc. at info@isti.com
//=====================================================================
//

package com.isti.util.gui;

import java.awt.Component;
import java.awt.event.ActionListener;
import javax.swing.ComboBoxEditor;
import javax.swing.JTextField;
import java.lang.reflect.Method;

/**
 * An implementation of a ComboBoxEditor that uses a JTextField.
 */
public class JTextFieldComboBoxEditor
    implements ComboBoxEditor
{
  /** The default number of columns. */
  public final static int DEFAULT_COLUMNS = 9;
  /** The old value or null if none. */
  private Object oldValue = null;

  //the text field
  private final JTextField textField;

  /**
   * Creates the ComboBoxEditor with a new text field.
   */
  public JTextFieldComboBoxEditor()
  {
    this(new JTextField(DEFAULT_COLUMNS));
  }

  /**
   * Creates the ComboBoxEditor with the specified text field.
   * If the number of columns was not specified, the default is used.
   * @param textField the text field.
   * @see DEFAULT_COLUMNS
   */
  public JTextFieldComboBoxEditor(JTextField textField)
  {
    this.textField = textField;
    //if the number of columns was not specified, use the default
    if (textField.getColumns() == 0)
    {
      textField.setColumns(DEFAULT_COLUMNS);
    }
  }

  /**
   * Add an ActionListener. An action event is generated when the edited item
   * changes.
   * @param actionListener the action listener to add.
   */
  public void addActionListener(ActionListener actionListener)
  {
    textField.addActionListener(actionListener);
  }

  /**
   * Return the component that should be added to the tree hierarchy for this
   * editor.
   * @return the editor component.
   */
  public Component getEditorComponent()
  {
    return textField;
  }

  /**
   * Return the edited item.
   * @return the edited item.
   */
  public Object getItem()
  {
    Object newValue = textField.getText();

    if (oldValue != null && ! (oldValue instanceof String))
    {
      // The original value is not a string. Should return the value in it's
      // original type.
      if (newValue.equals(oldValue.toString()))
      {
        return oldValue;
      }
      else
      {
        // convert the new value to the old value's class
        final Class cls = oldValue.getClass();
        try
        {
          final Method method = cls.getMethod("valueOf", new Class[]
                                              {String.class});
          newValue = method.invoke(oldValue, new Object[]
                                   {newValue});
        }
        catch (Exception ex)
        {
          // Fail silently and return the newValue (a String object)
        }
      }
    }
    return newValue;
  }

  /**
   * Gets the text field.
   * @return the text field.
   */
  public JTextField getTextField()
  {
    return textField;
  }

  /**
   * Remove an ActionListener.
   * @param actionListener the action listener to remove.
   */
  public void removeActionListener(ActionListener actionListener)
  {
    textField.removeActionListener(actionListener);
  }

  /**
   * Ask the editor to start editing and to select everything.
   */
  public void selectAll()
  {
    textField.selectAll();
    textField.requestFocus();
  }

  /**
   * Set the item that should be edited. Cancel any editing if necessary.
   * @param anObject the item that should be edited.
   */
  public void setItem(Object anObject)
  {
    String s = null;
    if (anObject != null)
    {
      s = anObject.toString();
      oldValue = anObject;
    }
    if (textField.getText().equals(s))
    {
      return;
    }
    textField.setText(s);
  }
}
