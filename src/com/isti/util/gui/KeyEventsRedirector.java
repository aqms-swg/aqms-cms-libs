//KeyEventsRedirector:  Redirects keyboard events to a specified component.
//
// 11/26/2002 -- [ET]
//

package com.isti.util.gui;

import java.awt.*;
import javax.swing.*;
import java.awt.event.KeyEvent;
import java.awt.event.ActionEvent;
import java.awt.event.FocusAdapter;
import java.awt.event.FocusEvent;

/**
 * Class KeyEventsRedirector redirects keyboard events to a specified
 * component.  This class is useful for situations where a given
 * component always wants to process certain keystrokes, regardless
 * of whether or not the component has the current input focus.
 * To intercept keystrokes, accelerators are defined on menu items
 * held in an invisible menu that is added to a displayed menu bar.
 * If a component (such as the target component or a displayed menu)
 * processes a given keystroke then it will not be received by this
 * redirector.
 */
public class KeyEventsRedirector extends AbstractAction
{
  private final Component targetCompObj;
  private final KeyStroke [] keyStrokesArr;
  private static int instanceCount = 0;
  private boolean inActionMethodFlag = false;
  private KeyEvent receivedKeyEventObj = null;

  /**
   * Creates and installs a redirector object.
   * @param menuBarObj a displayed menu bar.
   * @param targetComponent the target component to sent key events to.
   * @param keyStrokesArr an array of 'keyStroke' objects defining which
   * keys are to be redirected.
   */
  public KeyEventsRedirector(JMenuBar menuBarObj,Component targetComponent,
                                                 KeyStroke [] keyStrokesArr)
  {
    targetCompObj = targetComponent;
    this.keyStrokesArr = keyStrokesArr;
         //create the menu object, giving it unique name:
    final JMenu menuObj =
                     new JMenu(targetCompObj.getName() + (++instanceCount));
    if(keyStrokesArr != null)
    {    //keystrokes array is OK
      JMenuItem menuItemObj;
      for(int idx=0; idx<keyStrokesArr.length; ++idx)
      {  //for each keystroke to be redirected
                   //create menu item using this object's impl of 'Action':
        menuItemObj = new JMenuItem(this);
                   //enter array index value as menu item action command:
        menuItemObj.setActionCommand(Integer.toString(idx));
                   //enter keystroke object as accelerator:
        menuItemObj.setAccelerator(keyStrokesArr[idx]);
        menuObj.add(menuItemObj);           //add item to menu
      }
    }
    menuObj.setVisible(false);         //make menu invisible
    menuObj.setEnabled(false);         //disable menu
    if(menuBarObj != null)             //if menu bar OK then
      menuBarObj.add(menuObj);         //add menu
         //create focus listener to send key event to target
         // component right after it gains focus:
    targetCompObj.addFocusListener(new FocusAdapter()
        {
          public void focusGained(FocusEvent evtObj)
          {
            if(receivedKeyEventObj != null)
            {      //key event is waiting to be sent; send to component
              targetCompObj.dispatchEvent(receivedKeyEventObj);
//              System.out.println("Redirector sent:  " + receivedKeyEventObj);
              receivedKeyEventObj = null;   //clear waiting event
            }
          }
        });
  }

  /**
   * Action-performed method for all menu item accelerators used by this
   * object.
   * @param evtObj the event object for the event.
   */
  public void actionPerformed(ActionEvent evtObj)
  {
    if(inActionMethodFlag)
      return;      //if already in method then abort
    inActionMethodFlag = true;         //indicate inside this method
         //only redirect keystoke if the target component does not have
         // the current input focus, because if it does have focus then
         // it will process keystrokes directly
    if(!targetCompObj.hasFocus())
    {    //component does not have the input focus
      try                    //get array index from menu item action command
      {                      // and fetch corresponding keystroke object:
        final KeyStroke keyStrokeObj =
                 keyStrokesArr[Integer.parseInt(evtObj.getActionCommand())];
//        System.out.println("Redirector received:  " + keyStrokeObj);
                             //build key event obj for keystoke and save it:
        receivedKeyEventObj = new KeyEvent(targetCompObj,
                         KeyEvent.KEY_PRESSED,0,keyStrokeObj.getModifiers(),
                       keyStrokeObj.getKeyCode(),keyStrokeObj.getKeyChar());
        targetCompObj.requestFocus();  //request input focus on component
      }
      catch(Exception ex)
      {       //error occurred; just abort method
      }
    }
    inActionMethodFlag = false;        //indicate method done
  }
}
