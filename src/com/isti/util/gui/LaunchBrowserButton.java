//LaunchBrowserButton.java:  Defines a borderless button that brings
//                           up a webpage when pressed.
//
//  7/18/2002 -- [ET]  Class moved to isti.util.gui.
//  9/27/2002 -- [KF]  Added set link string method.
//  4/28/2003 -- [KF]  Added 'getLaunchBrowser', 'getLinkStr', and
//                     'actionPerformed' methods.
//   9/1/2006 -- [ET]  Modified to have no border under Windows XP
//                     standard desktop theme.
// 10/15/2008 -- [ET]  Renamed 'isFocusableTraverable()' method to
//                     'isFocusable()'.
//

package com.isti.util.gui;

import java.awt.*;
import java.awt.event.*;
import javax.swing.*;
import com.isti.util.LaunchBrowser;

/**
 * Class LaunchBrowserButton defines a borderless button that brings
 * up a webpage when pressed.
 */
public class LaunchBrowserButton extends JButton implements ActionListener
{
  private String urlString = null;
  private final LaunchBrowser launchBrowserObj;

  /**
   * Creates a borderless button that brings up a webpage when pressed.
   * @param labelStr a text label for the button.
   * @param launchBrowserObj 'LaunchBrowser' object used to display URL.
   */
  public LaunchBrowserButton(String labelStr,LaunchBrowser launchBrowserObj)
  {
    super(labelStr);            //create JButton
    this.launchBrowserObj = launchBrowserObj;    //save launcher object
    setLinkStr(null);           //start with no URL link string
    setupButton();              //Setup button attributes and action
  }

    /**
     * Creates a borderless button that brings up a webpage when pressed.
     * @param labelStr a text label for the button.
     * @param iconObj an image for the button.
     * @param launchBrowserObj 'LaunchBrowser' object used to display URL.
     * @param linkStr the URL of the webpage to be displayed when the
     * button is pressed.
     */
  public LaunchBrowserButton(String labelStr,ImageIcon iconObj,
                              LaunchBrowser launchBrowserObj,String linkStr)
  {
    super(labelStr,iconObj);    //create JButton
    this.launchBrowserObj = launchBrowserObj;    //save launcher object
    urlString = linkStr;        //save URL link string
    setupButton();              //Setup button attributes and action
  }

    /**
     * Creates a borderless button that brings up a webpage when pressed.
     * @param labelStr a text label for the button.
     * @param launchBrowserObj 'LaunchBrowser' object used to display URL.
     * @param linkStr the URL of the webpage to be displayed when the
     * button is pressed.
     */
  public LaunchBrowserButton(String labelStr,LaunchBrowser launchBrowserObj,
                                                             String linkStr)
  {
    super(labelStr);            //create JButton
    this.launchBrowserObj = launchBrowserObj;    //save launcher object
    urlString = linkStr;        //save URL link string
    setupButton();              //Setup button attributes and action
  }

    /**
     * Creates a borderless button that brings up a webpage when pressed.
     * @param iconObj an image for the button.
     * @param launchBrowserObj 'LaunchBrowser' object used to display URL.
     * @param linkStr the URL of the webpage to be displayed when the
     * button is pressed.
     */
  public LaunchBrowserButton(ImageIcon iconObj,
                              LaunchBrowser launchBrowserObj,String linkStr)
  {
    super(iconObj);             //create JButton
    this.launchBrowserObj = launchBrowserObj;    //save launcher object
    urlString = linkStr;        //save URL link string
    setupButton();              //Setup button attributes and action
  }

  /**
   * Gets the launch browser.
   * @return the launch browser.
   */
  protected LaunchBrowser getLaunchBrowser()
  {
    return launchBrowserObj;
  }

  /**
   * Gets the URL link string.
   * @return the URL link string.
   */
  protected String getLinkStr()
  {
    return urlString;
  }

  /**
   * Sets the URL link string.
   * @param linkStr the URL of the webpage to be displayed when the
   * button is pressed or null to make the button not visible.
   */
  public void setLinkStr(String linkStr)
  {
    urlString = linkStr;
    setVisible(urlString != null);
  }

  /**
   * Identifies whether or not this component can receive the focus.
   *
   * OVERRIDE and return false to never allow the focus.
   *
   * @return true if this component can receive the focus
   */
  public boolean isFocusable()
  {
    return false;
  }

       //Sets up button attributes and action.
  private void setupButton()
  {
    setBorderPainted(false);         //no border around button
    setFocusPainted(false);          //don't show keyboard focus
    setRequestFocusEnabled(false);   //don't allow KB focus
    setContentAreaFilled(false);     //for no border under Windows XP
    setOpaque(true);                 //for no border under Windows XP
    setCursor(new Cursor(Cursor.HAND_CURSOR));   //set "hand" cursor
    addActionListener(this);         //setup button action
  }

    /**
     * Creates a borderless button that brings up a webpage when pressed.
     * The icon image is tested (via the 'getImageLoadStatus()' method)
     * and only used if it is valid.
     * @param iconObj an image for the button.
     * @param labelStr a text label for the button.
     * @param launchBrowserObj 'LaunchBrowser' object used to display URL.
     * @param linkStr the URL of the webpage to be displayed when the
     * button is pressed.
     * @return a borderless button that brings up a webpage when pressed.
     */
  public static LaunchBrowserButton createButton(String labelStr,
            ImageIcon iconObj,LaunchBrowser launchBrowserObj,String linkStr)
  {
    if(iconObj != null &&
                      iconObj.getImageLoadStatus() != MediaTracker.COMPLETE)
    {    //icon image is OK
      return (labelStr != null) ?
         new LaunchBrowserButton(labelStr,iconObj,launchBrowserObj,linkStr) :
                   new LaunchBrowserButton(iconObj,launchBrowserObj,linkStr);
    }
         //icon image not OK; just use label text string:
    return new LaunchBrowserButton(
           ((labelStr!=null)?labelStr:""),iconObj,launchBrowserObj,linkStr);
  }

    /**
     * Creates a borderless button that brings up a webpage when pressed.
     * The icon image is tested (via the 'getImageLoadStatus()' method)
     * and only used if it is valid.
     * @param iconObj an image for the button.
     * @param launchBrowserObj 'LaunchBrowser' object used to display URL.
     * @param linkStr the URL of the webpage to be displayed when the
     * button is pressed.
     * @return a borderless button that brings up a webpage when pressed.
     */
  public static LaunchBrowserButton createButton(
            ImageIcon iconObj,LaunchBrowser launchBrowserObj,String linkStr)
  {
    return createButton(null,iconObj,launchBrowserObj,linkStr);
  }


  // ActionListener interface

  public void actionPerformed(ActionEvent e)
  {
    //if URL string OK then view in browser
    if(urlString != null && urlString.length() > 0)
      launchBrowserObj.showURL(urlString,"Browser");
  }
}
