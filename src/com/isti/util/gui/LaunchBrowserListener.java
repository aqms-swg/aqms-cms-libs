//LaunchBrowserListener.java:  Defines a listener that brings
//                           up a webpage when a mouse button has been pressed.
//
//   9/7/2004 -- [KF]  Initial version.
//

package com.isti.util.gui;

import java.awt.Component;
import java.awt.Cursor;
import java.awt.event.*;
import javax.swing.*;
import com.isti.util.LaunchBrowser;

/**
 * Class LaunchBrowserListener defines a listener that brings
 * up a webpage when a mouse button has been pressed.
 */
public class LaunchBrowserListener implements MouseListener
{
  //create hand cursor
  private final static Cursor handCursor = new Cursor(Cursor.HAND_CURSOR);

  private final LaunchBrowser launchBrowserObj;
  private Component compObj = null;
  private Cursor compDefaultCursor = null;
  private String linkStr = null;

  /**
   * Creates a listener that brings up a webpage when a mouse button has been
   * pressed.
   * @param launchBrowserObj 'LaunchBrowser' object used to display URL.
   */
  public LaunchBrowserListener(LaunchBrowser launchBrowserObj)
  {
    this(launchBrowserObj,null);
  }

  /**
   * Creates a listener that brings up a webpage when a mouse button has been
   * pressed.
   * @param launchBrowserObj 'LaunchBrowser' object used to display URL.
   * @param compObj the component to listen to or null for none.
   */
  public LaunchBrowserListener(LaunchBrowser launchBrowserObj,Component compObj)
  {
    this(launchBrowserObj,compObj,null);
  }

  /**
   * Creates a listener that brings up a webpage when a mouse button has been
   * pressed.
   * @param launchBrowserObj 'LaunchBrowser' object used to display URL.
   * @param compObj the component to listen to or null for none.
   * @param linkStr the URL of the webpage to be displayed or null for none.
   */
  public LaunchBrowserListener(LaunchBrowser launchBrowserObj,Component compObj,
                               String linkStr)
  {
    this.launchBrowserObj = launchBrowserObj;    //save launcher object
    setLinkStr(linkStr);        //save URL link string
    setComponent(compObj);      //set the component
  }

  /**
   * Gets the component.
   * @return the component.
   */
  public Component getComponent()
  {
    return compObj;
  }

  /**
   * Gets the launch browser.
   * @return the launch browser.
   */
  public LaunchBrowser getLaunchBrowser()
  {
    return launchBrowserObj;
  }

  /**
   * Gets the URL link string.
   * @return the URL link string.
   */
  public String getLinkStr()
  {
    return linkStr;
  }

  /**
   * Sets the component to listen to.
   * @param compObj the component to listen to or null for none.
   */
  public synchronized void setComponent(Component compObj)
  {
    //if a component was already set up
    if (this.compObj != null)
    {
      compObj.setCursor(compDefaultCursor);  //restore the cursor
      compObj.removeMouseListener(this);  //remove the mouse listener

      this.compObj = null;
      compDefaultCursor = null;
    }

    if (compObj != null)  //if a component was specified
    {
      compDefaultCursor = compObj.getCursor();  //save current cursor
      this.compObj = compObj;  //save the component
      compObj.addMouseListener(this);  //add the mouse listener
      if (compObj instanceof JComponent)
      {
        final JComponent jCompObj = (JComponent)compObj;
        jCompObj.setRequestFocusEnabled(false);   //don't allow KB focus
      }
      if (compObj instanceof AbstractButton)
      {
        final AbstractButton buttonObj = (AbstractButton)compObj;
        buttonObj.setBorderPainted(false);  //no border around button
        buttonObj.setFocusPainted(false);   //don't show keyboard focus
      }
    }

    setupCursor();  //set up the cursor
  }

  /**
   * Sets the URL link string.
   * @param linkStr the URL of the webpage to be displayed when the
   * listener is pressed.
   */
  public synchronized void setLinkStr(String linkStr)
  {
    this.linkStr = linkStr;

    setupCursor();  //set up the cursor
  }

  // MouseListener interface

  /**
   * Invoked when the mouse has been clicked on a component.
   * @param e the mouse event.
   */
  public void mouseClicked(MouseEvent e) {}

  /**
   * Invoked when a mouse button has been pressed on a component.
   * @param e the mouse event.
   */
  public void mousePressed(MouseEvent e)
  {
    final String urlStr = linkStr;
    //if URL string OK then view in browser
    if(urlStr != null && urlStr.length() > 0)
      launchBrowserObj.showURL(urlStr,"Browser");
  }

  /**
   * Invoked when a mouse button has been released on a component.
   * @param e the mouse event.
   */
  public void mouseReleased(MouseEvent e) {}

  /**
   * Invoked when the mouse enters a component.
   * @param e the mouse event.
   */
  public void mouseEntered(MouseEvent e) {}

  /**
   * Invoked when the mouse exits a component.
   * @param e the mouse event.
   */
  public void mouseExited(MouseEvent e) {}


  /**
   * Set up the cursor.
   */
  protected void setupCursor()
  {
    if (compObj != null)  //if a component was specified
    {
      if (linkStr == null)  //if no link string
        compObj.setCursor(compDefaultCursor);  //use the default cursor
      else
        compObj.setCursor(handCursor);   //use "hand" cursor
    }
  }
}