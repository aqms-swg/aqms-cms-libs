//LaunchBrowserTextArea.java:  Defines a text area that brings
//                             up a webpage when pressed.
//
//  6/17/2002 -- [KF]  Initial version based on 'LaunchBrowserButton'.
//  7/18/2002 -- [ET]  Class moved to isti.util.gui.
// 10/15/2008 -- [ET]  Renamed 'isFocusableTraverable()' method to
//                     'isFocusable()'.
//

package com.isti.util.gui;

import java.awt.*;
import java.awt.event.*;
import javax.swing.*;
import com.isti.util.LaunchBrowser;

/**
 * Class LaunchBrowserTextArea defines a text area that brings
 * up a webpage when pressed.
 */
public class LaunchBrowserTextArea extends JTextArea
{
  private final String urlString;
  private final LaunchBrowser launchBrowserObj;

    /**
     * Creates a text area that brings up a webpage when pressed.
     * @param textStr a text for the text area.
     * @param launchBrowserObj 'LaunchBrowser' object used to display URL.
     * @param linkStr the URL of the webpage to be displayed when the
     * text area is pressed.
     */
  public LaunchBrowserTextArea(String textStr,
                              LaunchBrowser launchBrowserObj,String linkStr)
  {
    super(textStr);             //create JTextArea
    this.launchBrowserObj = launchBrowserObj;    //save launcher object
    urlString = linkStr;        //save URL link string
    setupTextArea();            //Setup text area attributes and action
  }

  /**
   * OVERRIDE and return false to never allow the focus.
   * @return false to never allow the focus.
   */
  public boolean isFocusable()
  {
    return false;
  }

       //Sets up text area attributes and action.
  private void setupTextArea()
  {
    setEditable(false);              //don't allow edits
    setRequestFocusEnabled(false);   //don't allow KB focus
    setCursor(new Cursor(Cursor.HAND_CURSOR));   //set "hand" cursor
    //setup text area action:
    addMouseListener(new java.awt.event.MouseAdapter()
    {
      public void mouseClicked(MouseEvent e)
      {
        //if URL string OK then view in browser
        if (urlString != null && urlString.length() > 0)
          launchBrowserObj.showURL(urlString,"Browser");
      }
    });
  }
}
