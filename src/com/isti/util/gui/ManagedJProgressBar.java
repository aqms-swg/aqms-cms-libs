//ManagedJProgressBar.java:  Extension of 'JProgressBar' that enforces
//                           the paradigm that list changes should only
//                           happen via the event-dispatch thread.
// 10/16/2003 -- [ET]
//

package com.isti.util.gui;

import javax.swing.JProgressBar;
import javax.swing.BoundedRangeModel;
import com.isti.util.ProgressIndicatorInterface;
import javax.swing.SwingUtilities;

/**
 * Class ManagedJProgressBar is an extension of 'JProgressBar' that enforces
 * the paradigm that list changes should only happen via the event-dispatch
 * thread.
 */
public class ManagedJProgressBar extends JProgressBar implements
                                                  ProgressIndicatorInterface
{

 /**
   * Creates a horizontal progress bar.
   * The default orientation for progress bars is
   * <code>JProgressBar.HORIZONTAL</code>.
   * By default, the String is set to <code>null</code> and the
   * StringPainted is not painted.
   * The border is painted by default.
   * Uses the defaultMinimum (0) and defaultMaximum (100).
   * Uses the defaultMinimum for the initial value of the progress bar.
   */
  public ManagedJProgressBar()
  {
  }

 /**
   * Creates a progress bar with the specified orientation.
   * By default, the String is set to <code>null</code> and the
   * StringPainted is not painted.
   * The border is painted by default.
   * Uses the defaultMinimum (0) and defaultMaximum (100).
   * Uses the defaultMinimum for the initial value of the progress bar.
   * @param orient the orientation to use, which can be either
   * <code>JProgressBar.VERTICAL</code> or
   * <code>JProgressBar.HORIZONTAL</code>.
   */
  public ManagedJProgressBar(int orient)
  {
    super(orient);
  }

  /**
   * Creates a horizontal progress bar, which is the default.
   * By default, the String is set to <code>null</code> and the
   * StringPainted is not painted.
   * The border is painted by default.
   * Uses the specified minimum for the initial value of the progress bar.
   * @param min the minimum value to use.
   * @param max the maximum value to use.
   */
  public ManagedJProgressBar(int min, int max)
  {
    super(min, max);
  }

  /**
   * Creates a progress bar using the specified orientation,
   * minimum, and maximum.
   * By default, the String is set to <code>null</code> and the
   * StringPainted is not painted.
   * The border is painted by default.
   * Sets the inital value of the progress bar to the specified minimum.
   * The BoundedRangeModel that sits underneath the progress bar
   * handles any issues that may arrise from improperly setting the
   * minimum, value, and maximum on the progress bar.
   * @param orient the orientation to use, which can be either
   * <code>JProgressBar.VERTICAL</code> or
   * <code>JProgressBar.HORIZONTAL</code>.
   * @param min the minimum value to use.
   * @param max the maximum value to use.
   */
  public ManagedJProgressBar(int orient, int min, int max)
  {
    super(orient, min, max);
  }

  /**
   * Creates a horizontal progress bar, the default orientation.
   * By default, the String is set to <code>null</code> and the
   * StringPainted is not painted.
   * The border is painted by default.
   * @param newModel the BoundedRangeModel to use, which holds the
   * minimum, value, and maximum values.
   */
  public ManagedJProgressBar(BoundedRangeModel newModel)
  {
    super(newModel);
  }

  /**
   * Sets the model's current value.  If the thread calling this method
   * is not the event-dispatch thread then the action is queued and
   * invoked later by the event-dispatch thread.
   * The underlying BoundedRangeModel will handle any mathematical
   * issues arrising from assigning faulty values.
   * @param val the new value.
   */
  public void setValue(final int val)
  {
              //if this is the event-dispatch thread then do it now:
    if(SwingUtilities.isEventDispatchThread())
      super.setValue(val);
    else
    {         //if this is not the event-dispatch thread then do it later:
      SwingUtilities.invokeLater(new Runnable()
          {
            public void run()
            {
              ManagedJProgressBar.super.setValue(val);
            }
          });
    }
  }

  /**
   * Sets the model's minimum.  The underlying BoundedRangeModel will
   * handle any mathematical issues arrising from assigning faulty values.
   * Notifies any listeners if the data changes.  If the thread calling
   * this method is not the event-dispatch thread then the action is
   * queued and invoked later by the event-dispatch thread.
   * @param val the new minimum value.
   */
  public void setMinimum(final int val)
  {
              //if this is the event-dispatch thread then do it now:
    if(SwingUtilities.isEventDispatchThread())
      super.setMinimum(val);
    else
    {         //if this is not the event-dispatch thread then do it later:
      SwingUtilities.invokeLater(new Runnable()
          {
            public void run()
            {
              ManagedJProgressBar.super.setMinimum(val);
            }
          });
    }
  }

  /**
   * Sets the model's maximum.  The underlying BoundedRangeModel will
   * handle any mathematical issues arrising from assigning faulty values.
   * Notifies any listeners if the data changes.  If the thread calling
   * this method is not the event-dispatch thread then the action is
   * queued and invoked later by the event-dispatch thread.
   * @param val the new maximum value.
   */
  public void setMaximum(final int val)
  {
              //if this is the event-dispatch thread then do it now:
    if(SwingUtilities.isEventDispatchThread())
      super.setMaximum(val);
    else
    {         //if this is not the event-dispatch thread then do it later:
      SwingUtilities.invokeLater(new Runnable()
          {
            public void run()
            {
              ManagedJProgressBar.super.setMaximum(val);
            }
          });
    }
  }

  /**
   * Sets the model's minimum and maximum values.  The underlying
   * BoundedRangeModel will handle any mathematical issues arrising
   * from assigning faulty values.  Notifies any listeners if the
   * data changes.  If the thread calling this method is not the
   * event-dispatch thread then the action is queued and invoked
   * later by the event-dispatch thread.
   * @param minVal the new minimum value.
   * @param maxVal the new maximum value.
   */
  public void setMinMaxValues(final int minVal,final int maxVal)
  {
              //if this is the event-dispatch thread then do it now:
    if(SwingUtilities.isEventDispatchThread())
    {
      super.setMinimum(minVal);
      super.setMaximum(maxVal);
    }
    else
    {         //if this is not the event-dispatch thread then do it later:
      SwingUtilities.invokeLater(new Runnable()
          {
            public void run()
            {
              ManagedJProgressBar.super.setMinimum(minVal);
              ManagedJProgressBar.super.setMaximum(maxVal);
            }
          });
    }
  }


}
