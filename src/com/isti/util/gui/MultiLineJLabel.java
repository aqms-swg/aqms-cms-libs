//MultiLineJLabel.java:  Defines a 'JTextArea' subclass that operates
//                       like a 'JLabel' that can display multiple
//                       lines of text.
//
//  4/22/2004 -- [ET]  Initial version.
//  10/4/2004 -- [ET]  Added no-argument constructor.
// 10/13/2004 -- [ET]  Modified to make component not focusable.
//

package com.isti.util.gui;

import java.awt.Font;
import javax.swing.JTextArea;
import javax.swing.JLabel;

/**
 * Class MultiLineJLabel defines a 'JTextArea' subclass that operates
 * like a 'JLabel' that can display multiple lines of text.  The text
 * is automatically word-wrapped, and the newline ('\n') character may
 * be used to separate lines.  Only text display is supported (no icon).
 * Note that in some cases when this component is used in a dialog,
 * the dialog must perform a 'pack' after it is made visible (the
 * 'IstiDialogPopup' class does this).
 */
public class MultiLineJLabel extends JTextArea
{
  /** The default number of columns (30). */
  public static final int DEF_NUM_COLS = 30;
  /** Default font to be used (same as JLabel font). */
  public static final Font DEF_FONT_OBJ = (new JLabel()).getFont();

  /**
   * Creates a multi-line text component.
   * @param textStr the text to display.
   * @param numCols the number of columns to display, or 0 to use the
   * default number of columns.
   */
  public MultiLineJLabel(String textStr, int numCols)
  {
    super(textStr,0,((numCols>0)?numCols:DEF_NUM_COLS));
    setFont(DEF_FONT_OBJ);        //setup default font
    setEditable(false);           //make area not modifiable
    FocusUtils.setComponentFocusDisabled(this);  //make item not focusable
    setLineWrap(true);            //enable line wrapping
    setWrapStyleWord(true);       //set for word wrapping
    setBackground(null);          //clear background color
  }

  /**
   * Creates a multi-line text component.
   * @param textStr the text to display.
   */
  public MultiLineJLabel(String textStr)
  {
    this(textStr,DEF_NUM_COLS);
  }

  /**
   * Creates an empty multi-line text component.
   */
  public MultiLineJLabel()
  {
    this(null,DEF_NUM_COLS);
  }
}
