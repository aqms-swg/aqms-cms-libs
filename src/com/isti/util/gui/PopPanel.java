//PopPanel.java:  Manages a "popup" panel.
//
//   5/15/2000 -- [ET]  Implemented scrolling-as-needed in message area.
//   7/12/2000 -- [ET]  Initial release version.
//   4/26/2001 -- [ET]  Added 'createShadowedBevelBorder()' method.
//   1/11/2006 -- [KF]  Added constructor with 'bottomPanel' parameter.
//   6/21/2010 -- [ET]  Added 'getPopButtonPressedFlag()' method and
//                      implementation.
//
//
//=====================================================================
// Copyright (C) 2010 Instrumental Software Technologies, Inc.
//
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions
// are met:
// 1. Redistributions of source code, or portions of this source code,
//    must retain the above copyright notice, this list of conditions
//    and the following disclaimer.
// 2. Redistributions in binary form must reproduce the above copyright
//    notice, this list of conditions and the following disclaimer in
//    the documentation and/or other materials provided with the
//    distribution.
// 3. All advertising materials mentioning features or use of this
//    software must display the following acknowledgment:
//    "This product includes software developed by Instrumental
//    Software Technologies, Inc. (http://www.isti.com)"
// 4. If the software is provided with, or as part of a commercial
//    product, or is used in other commercial software products the
//    customer must be informed that "This product includes software
//    developed by Instrumental Software Technologies, Inc.
//    (http://www.isti.com)"
// 5. The names "Instrumental Software Technologies, Inc." and "ISTI"
//    must not be used to endorse or promote products derived from
//    this software without prior written permission. For written
//    permission, please contact "info@isti.com".
// 6. Products derived from this software may not be called "ISTI"
//    nor may "ISTI" appear in their names without prior written
//    permission of Instrumental Software Technologies, Inc.
// 7. Redistributions of any form whatsoever must retain the following
//    acknowledgment:
//    "This product includes software developed by Instrumental
//    Software Technologies, Inc. (http://www.isti.com/)."
// THIS SOFTWARE IS PROVIDED BY INSTRUMENTAL SOFTWARE
// TECHNOLOGIES, INC. "AS IS" AND ANY EXPRESSED OR IMPLIED
// WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES
// OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
// DISCLAIMED.  IN NO EVENT SHALL INSTRUMENTAL SOFTWARE TECHNOLOGIES,
// INC. OR ITS CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
// INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
// (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
// SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
// HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
// STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
// ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED
// OF THE POSSIBILITY OF SUCH DAMAGE.
//=====================================================================
//  A current version of the software can be found at
//                http://www.isti.com
//  Bug reports and comments should be directed to
//  Instrumental Software Technologies, Inc. at info@isti.com
//=====================================================================
//

package com.isti.util.gui;

import java.awt.AWTEvent;
import java.awt.Color;
import java.awt.Component;
import java.awt.Container;
import java.awt.Cursor;
import java.awt.Dimension;
import java.awt.EventQueue;
import java.awt.Font;
import java.awt.FontMetrics;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.MenuComponent;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.SwingUtilities;
import javax.swing.border.Border;
import javax.swing.border.EmptyBorder;
import javax.swing.border.TitledBorder;

/**
 * Class PopPanel manages a "popup" panel, including support for creating
 * and hiding the panel, and for optional thread blocking while the panel
 * is displayed.  Presently, the only type of panel supported is a
 * message-style popup containing a title, a message and an optional button.
 *
 * A method ('waitForButton()') is provided to allow the calling thread to
 * be blocked while the popup panel is displayed.  An alternative approach
 * is to attach a 'ComponentListener' to the panel (via
 * 'addComponentListener()'), which will have its 'componentHidden()'
 * method called after the popup is dismissed.
 *
 * The popup panel created needs to be added to a visible container in
 * order to be displayed.  The 'PopPanelHost' class provides support for
 * pseudo-modal popup functionality.
 */
public class PopPanel extends JPanel
{
  public static final String okButtonText = "OK";     //default button text
  private static final int MIN_CHAR_WIDTH = 30;       //minimum char width
  private static final int MAX_CHAR_WIDTH = 100;      //maximum char width
                        //define amount of extra space around text area:
  private static final int EXTRA_WIDTH = 50;
  private static final int EXTRA_HT_WBUT = 110;       //with button
  private static final int EXTRA_HT_WOBUT = 75;       //without button
                        //width & height per character in text area:
  private static int widthPerChar = 7;      //default value
  private static int heightPerChar = 17;    //default value
  private JButton popButton;           //button on popup
  private boolean popButtonPressedFlag = false;  //true if cleared via button

  static      //determine, for this implementation, the width & height
  {           // per character in the text area:
    final JTextArea txtArea = new JTextArea();   //create a text area
    final Font textAreaFont;
    FontMetrics fMetrics;         //get the font & metrics for text area:
    if((textAreaFont = txtArea.getFont()) != null &&
                  (fMetrics = txtArea.getFontMetrics(textAreaFont)) != null)
    {    //font & font metrics OK
      int val;
      if((val=fMetrics.getHeight()) > 0 && val < 100)
        heightPerChar = val;      //if value OK then save text height
      if((val=fMetrics.charWidth('m')) > 0 && val < 100)
        widthPerChar = val;       //if value OK then save text width
    }
  }

    /**
     * Creates a popup panel with a default character width and an
     * "OK" button.
     * @param message message text for the panel.
     * @param title title text for the panel.
     */
  public PopPanel(String message,String title)
  {
    this(message,title,0,null,okButtonText);
  }

    /**
     * Creates a popup panel with a custom character width and an
     * "OK" button.
     * @param message message text for the panel.
     * @param title title text for the panel.
     * @param charWidth approximate width for the panel, measured in
     * characters.
     */
  public PopPanel(String message,String title,int charWidth)
  {
    this(message,title,charWidth,null,okButtonText);
  }

    /**
     * Creates a popup panel with a default character width and an
     * "OK" button.
     * @param message message text for the panel.
     * @param title title text for the panel.
     * @param buttonCursor if not 'null' then sets the cursor for the
     * button.
     */
  public PopPanel(String message,String title,Cursor buttonCursor)
  {
    this(message,title,0,buttonCursor,okButtonText);
  }

    /**
     * Creates a popup panel with a default character width.
     * @param message message text for the panel.
     * @param title title text for the panel.
     * @param buttonText if not null then a string containing the text
     * for the button; if null then no button is displayed.
     */
  public PopPanel(String message,String title,String buttonText)
  {
    this(message,title,0,null,buttonText);
  }

    /**
     * Creates a popup panel with a custom character width.
     * @param message message text for the panel.
     * @param title title text for the panel.
     * @param charWidth approximate width for the panel, measured in
     * characters.
     * @param buttonText if not null then a string containing the text
     * for the button; if null then no button is displayed.
     */
  public PopPanel(String message,String title,int charWidth,
                                                          String buttonText)
  {
    this(message,title,charWidth,null,buttonText);
  }

    /**
     * Creates a popup panel with a custom character width.
     * @param message message text for the panel.
     * @param title title text for the panel.
     * @param buttonCursor if not 'null' then sets the cursor for the
     * button.
     * @param buttonText if not null then a string containing the text
     * for the button; if null then no button is displayed.
     */
  public PopPanel(String message,String title,Cursor buttonCursor,
                                                          String buttonText)
  {
    this(message,title,0,buttonCursor,buttonText);
  }

    /**
     * Creates a popup panel with a custom character width.
     * @param message message text for the panel.
     * @param title title text for the panel.
     * @param charWidth approximate width for the panel, measured in
     * characters.
     * @param buttonCursor if not 'null' then sets the cursor for the
     * button.
     * @param buttonText if not null then a string containing the text
     * for the button; if null then no button is displayed.
     */
  public PopPanel(String message,String title,int charWidth,
                  Cursor buttonCursor,String buttonText)
  {
   this(message,title,charWidth,buttonCursor,buttonText,null);
  }

    /**
     * Creates a popup panel with a custom character width.
     * @param message message text for the panel.
     * @param title title text for the panel.
     * @param charWidth approximate width for the panel, measured in
     * characters.
     * @param buttonCursor if not 'null' then sets the cursor for the
     * button.
     * @param buttonText if not null then a string containing the text
     * for the button; if null then no button is displayed.
     * @param bottomPanel the bottom panel or null for none.
     */
  public PopPanel(String message,String title,int charWidth,
                  Cursor buttonCursor,String buttonText,JPanel bottomPanel)
  {
    final int msgLen = message.length();
    if(charWidth < 2)
    {    //valid character width not given; generate one based on length
      if((charWidth=msgLen/10) < MIN_CHAR_WIDTH)
        charWidth = MIN_CHAR_WIDTH;         //if too small then set minimum
      else if(charWidth > MAX_CHAR_WIDTH)
        charWidth = MAX_CHAR_WIDTH;         //if too large then set maximum
    }
                             //calculate number of rows needed:
    int sPos=0,ePos,numRows=0;
    do
    {         //for each newline character searched for
      if((ePos=message.indexOf('\n',sPos)) < 0)
        ePos = msgLen;       //if not found then use end of message position
      numRows += (ePos - sPos) / charWidth + 1;  //add to number of rows
      sPos = ePos + 1;       //move to next position after newline
    }
    while(ePos < msgLen);    //loop if more data left to parse

                             //setup desired size of popup panel:
    int preferredWidth = charWidth*widthPerChar+EXTRA_WIDTH;
    int preferredHeight = numRows*heightPerChar+
        ((buttonText!=null)?EXTRA_HT_WBUT:EXTRA_HT_WOBUT);
    if(bottomPanel != null)
    {
      preferredWidth = Math.max(preferredWidth,bottomPanel.getPreferredSize().width);
      preferredHeight += bottomPanel.getPreferredSize().height;
    }
    setPreferredSize(new Dimension(preferredWidth,preferredHeight));

                                      //create text area object:
    final JTextArea textArea = new JTextArea(message);
    textArea.setEditable(false);            //not editable
    textArea.setLineWrap(true);             //enable line wrapping
    textArea.setWrapStyleWord(true);        //keep words together
              //set text area background to be same as outer panel:
    textArea.setBackground(getBackground());

    if(buttonText != null)
    {    //button text string was given
      if(buttonText.length() <= 0)     //if bad length then
       buttonText = okButtonText;      //put in default text
              //setup button:
      popButton = new JButton(buttonText);  //create button
      if(buttonCursor != null)              //if cursor provided then
        popButton.setCursor(buttonCursor);  //set cursor for button
      popButton.addActionListener(     //setup button to shut down panel
        new ActionListener()
        {
          public void actionPerformed(ActionEvent e)
          {
            popButtonPressedFlag = true;         //indicate button pressed
            PopPanel.this.setVisible(false);     //hide popup panel
            stopModal();     //send interrupt in case 'wait()' in progress
          }
        }
      );
    }
    else      //button text string not given
      popButton = null;           //indicate button not created

         //setup outer popup panel:
    setLayout(new GridBagLayout());    //grid bag layout for panel
              //add shadowed bevel border with title:
    setBorder(createShadowedBevelBorder(title));

         //add text panel and button to outer panel:
              //create constraints object:
    GridBagConstraints constraints = new GridBagConstraints();
              //setup constraints for text panel :
    constraints.gridx = constraints.gridy = 0;        //text area on top
    constraints.gridwidth = constraints.gridheight = 1;
    constraints.weightx = constraints.weighty = 1.0;
    constraints.fill = GridBagConstraints.BOTH;
    constraints.anchor = GridBagConstraints.CENTER;
    constraints.insets = new Insets(10,10,5,10);      //space around text
              //put text area into scroll pane, with optional scroll bars:
    final JScrollPane textScrollPane = new JScrollPane(textArea,
                                   JScrollPane.VERTICAL_SCROLLBAR_AS_NEEDED,
                                JScrollPane.HORIZONTAL_SCROLLBAR_AS_NEEDED);
              //setup for no border around outside of scroll pane:
    textScrollPane.setBorder(new EmptyBorder(0,0,0,0));
    add(textScrollPane,constraints);   //add scroll pane with text area
    if(popButton != null)
    {    //button was created above
              //setup constraints for button:
      constraints.gridy = 1;                     //button at lower-center
      constraints.weightx = constraints.weighty = 0.0;
      constraints.fill = GridBagConstraints.NONE;     //don't stretch button
      constraints.insets = new Insets(5,5,5,5);  //put space around button
      add(popButton,constraints);                //add button
    }
    if (bottomPanel != null)
    {
      constraints.gridy++;
      constraints.weightx = constraints.weighty = 0.0;
      constraints.fill = GridBagConstraints.NONE;     //don't stretch panel
      constraints.insets = new Insets(5,5,5,5);  //put space around panel
      add(bottomPanel,constraints);
    }
  }

    /**
     * Method which blocks the calling thread until the popup panel is
     * dismissed.  A local event dispatch loop is used, which relays
     * events while the thread is blocked.
     */
  public void waitForButton()
  {
    startModal();
  }

    /**
     * Determines if the button on this PopPanel was pressed (resulting
     * in the PopPanel being closed).
     * @return true if the button on this PopPanel was pressed; false
     * if not.
     */
  public boolean getPopButtonPressedFlag()
  {
    return popButtonPressedFlag;
  }

    /*
     * Creates a new EventDispatchThread to dispatch events while
     * waiting for the popup panel to close.  Code is taken from
     * 'JInternalFrame.startModal()'.
     */
  synchronized void startModal()
  {
      /* Since all input will be blocked until this dialog is dismissed,
       * make sure its parent containers are visible first (this component
       * is tested below).  This is necessary for JApplets, because
       * because an applet normally isn't made visible until after its
       * start() method returns -- if this method is called from start(),
       * the applet will appear to hang while an invisible modal frame
       * waits for input.
       */
    if (isVisible() && !isShowing())
    {
      Container parent = getParent();
      while (parent != null)
      {
        if (parent.isVisible() == false)
        {
          parent.setVisible(true);
        }
        parent = parent.getParent();
      }
    }

    try
    {
      if (SwingUtilities.isEventDispatchThread())
      {       //thread is an AWT event dispatcher
              //collect and dispatch events from here:
        EventQueue theQueue = getToolkit().getSystemEventQueue();
        while (isVisible())
        {
          // This is essentially the body of EventDispatchThread
          AWTEvent event = theQueue.getNextEvent();
          Object src = event.getSource();
          // can't call theQueue.dispatchEvent, so I pasted it's body here
          /*if (event instanceof ActiveEvent) {
            ((ActiveEvent) event).dispatch();
            } else */ if (src instanceof Component) {
                ((Component) src).dispatchEvent(event);
            } else if (src instanceof MenuComponent) {
                ((MenuComponent) src).dispatchEvent(event);
            } else {
                System.err.println(
                           "PopPanel:  Unable to dispatch event: " + event);
            }
        }
      }
      else    //thread is not an AWT event dispatcher
      {       //wait until panel hidden or 'notify()' called:
        while (isVisible())
          wait();
      }
    }
    catch(InterruptedException e){}
  }


    /*
     * Called after the event dispatching loop created by a previous call
     * to 'startModal()' is no longer needed.  Only has an effect if the
     * thread was not an AWT event dispatching thread and 'wait()' was
     * used in 'startModal()'.  Code is based on
     * 'JInternalFrame.startModal()'.
     */
  synchronized void stopModal()
  {
    try
    {
      notifyAll();           //send interrupt to 'wait()' function
    }
    catch(IllegalMonitorStateException ex)
    {                        //if fails then just show message
      ex.printStackTrace(System.err);
    }
  }

    /**
     * Creates and returns shadowed bevel border.  Useful for dialog-style
     * panels.
     * @param title the title to put on the border.
     * @return border
     */
  public static Border createShadowedBevelBorder(String title)
  {
              //create beveled inner border with title in it:
    final Border innerBorder = BorderFactory.createCompoundBorder(
                                    BorderFactory.createRaisedBevelBorder(),
                                           BorderFactory.createTitledBorder(
                                   BorderFactory.createLoweredBevelBorder(),
                           title,TitledBorder.LEFT,TitledBorder.ABOVE_TOP));
              //add border with "shadow" line around it and return it:
    return BorderFactory.createCompoundBorder(
                    BorderFactory.createMatteBorder(1,1,2,2,Color.darkGray),
                                                               innerBorder);
  }
}
