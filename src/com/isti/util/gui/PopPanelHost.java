//PopPanelHost.java:  Manages a host panel that supports "popup"-panel
//                    functionality.
//
//   10/26/2000 -- [ET]  Improved thread synchronization.
//     4/3/2002 -- [ET]  Added 'isPopupShowing()' method.
//   10/16/2003 -- [KF]  Put main 'showPopupPanel' processing into one method.
//    1/27/2007 -- [ET]  Modified to only access GUI components via
//                       the event-dispatch thread.
//   10/15/2008 -- [ET]  Replaced call to 'JComponent.requestDefaultFocus()'
//                       with call to 'FocusUtils.requestDefaultFocus()'.
//   6/21/2010 -- [ET]  Added 'getPopButtonPressedFlag()' method and
//                      implementation.
//
//
//=====================================================================
// Copyright (C) 2010 Instrumental Software Technologies, Inc.
//
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions
// are met:
// 1. Redistributions of source code, or portions of this source code,
//    must retain the above copyright notice, this list of conditions
//    and the following disclaimer.
// 2. Redistributions in binary form must reproduce the above copyright
//    notice, this list of conditions and the following disclaimer in
//    the documentation and/or other materials provided with the
//    distribution.
// 3. All advertising materials mentioning features or use of this
//    software must display the following acknowledgment:
//    "This product includes software developed by Instrumental
//    Software Technologies, Inc. (http://www.isti.com)"
// 4. If the software is provided with, or as part of a commercial
//    product, or is used in other commercial software products the
//    customer must be informed that "This product includes software
//    developed by Instrumental Software Technologies, Inc.
//    (http://www.isti.com)"
// 5. The names "Instrumental Software Technologies, Inc." and "ISTI"
//    must not be used to endorse or promote products derived from
//    this software without prior written permission. For written
//    permission, please contact "info@isti.com".
// 6. Products derived from this software may not be called "ISTI"
//    nor may "ISTI" appear in their names without prior written
//    permission of Instrumental Software Technologies, Inc.
// 7. Redistributions of any form whatsoever must retain the following
//    acknowledgment:
//    "This product includes software developed by Instrumental
//    Software Technologies, Inc. (http://www.isti.com/)."
// THIS SOFTWARE IS PROVIDED BY INSTRUMENTAL SOFTWARE
// TECHNOLOGIES, INC. "AS IS" AND ANY EXPRESSED OR IMPLIED
// WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES
// OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
// DISCLAIMED.  IN NO EVENT SHALL INSTRUMENTAL SOFTWARE TECHNOLOGIES,
// INC. OR ITS CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
// INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
// (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
// SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
// HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
// STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
// ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED
// OF THE POSSIBILITY OF SUCH DAMAGE.
//=====================================================================
//  A current version of the software can be found at
//                http://www.isti.com
//  Bug reports and comments should be directed to
//  Instrumental Software Technologies, Inc. at info@isti.com
//=====================================================================
//
//

package com.isti.util.gui;

import java.awt.*;
import java.awt.event.*;
import javax.swing.*;

/**
 * Class PopPanelHost manages a host panel that supports "popup"-panel
 * functionality.  The popup panel created is pseudo-modal, in that while
 * no actual events are disabled, all of the components under the popup
 * panel are prevented from receiving events.  The popup panel is a child
 * component of this panel, appears at its center, and is not movable.
 * Only one popup panel may be displayed at a time.
 *
 * The PopPanelHost object should never have its layout manager changed
 * or have components added directly to it--the object returned by
 * 'getMainPanel()' should be used to hold the content and may have
 * its layout manager changed.
 *
 * Support is provided for optional thread blocking while the popup is
 * displayed, and for a component event to be generated when the popup
 * is dismissed.
 *
 * The 'JOptionPane.showInternalMessageDialog()' function tries to provide
 * the functionality of this class, but it only works in a 'JDesktopPane'
 * object using a null layout manager.
 */
public class PopPanelHost extends JLayeredPane
{
  private final JPanel mainPanel;           //main panel for content
  private final EvtJPanel popupHostPanel;   //host panel for popup
  private JPanel pPanel = null;             //popup panel
                                       //component listener used by host:
  private ComponentListener hostCompListener = null;
                                       //component listener given by user:
  private ComponentListener userCompListener = null;
  private boolean displayedFlag = false;    //true while popup displayed
  private Cursor buttonCursor = null;       //cursor for buttons
                                  //constraints object that fills area:
  private final GridBagConstraints fullConstraints;
                                  //constraints object for centered:
  private final GridBagConstraints centerConstraints;
              //flag set true to disable future layout manager changes:
  private boolean freezeLayoutMgrFlag = false;
              //synchronization object for instances of this class:
  private final Object panelHostSyncObj = new Object();
              //cached version of button-pressed flag from 'PopPanel':
  private boolean popButtonPressedFlag = false;

    /**
     * Creates a 'PopPanelHost' object.
     */
  public PopPanelHost()
  {
    this(true);
  }

    /**
     * Creates a 'PopPanelHost' object.
     * @param doubleBuffFlag if true then double-buffering is used.
     */
  public PopPanelHost(boolean doubleBuffFlag)
  {
    setLayout(new GridBagLayout());              //setup layout manager
    freezeLayoutMgrFlag = true;        //disable changes to layout manager
    mainPanel = new JPanel(doubleBuffFlag);      //create main panel
                                       //put main panel on default layer:
    setLayer(mainPanel,JLayeredPane.DEFAULT_LAYER.intValue());
    popupHostPanel = new EvtJPanel(doubleBuffFlag);   //popup host panel
                                       //put popup panel on upper layer:
    setLayer(popupHostPanel,JLayeredPane.POPUP_LAYER.intValue());
                        //enable events on popup host panel to trap them:
    popupHostPanel.evtEnableEvents(AWTEvent.MOUSE_EVENT_MASK|
                  AWTEvent.MOUSE_MOTION_EVENT_MASK|AWTEvent.KEY_EVENT_MASK);
    popupHostPanel.setLayout(new GridBagLayout());    //use grid bag layout
    popupHostPanel.setOpaque(false);   //make popup host panel transparent
    popupHostPanel.setRequestFocusEnabled(false);     //no focus on panel
                        //setup constraints object that fills area:
    fullConstraints = new GridBagConstraints();
    fullConstraints.gridx = fullConstraints.gridy = 0;
    fullConstraints.gridwidth = fullConstraints.gridheight = 1;
    fullConstraints.weightx = fullConstraints.weighty = 1.0;
    fullConstraints.fill = GridBagConstraints.BOTH;
    fullConstraints.anchor = GridBagConstraints.CENTER;
    add(mainPanel,fullConstraints);         //add main panel
                        //setup constraints object for centered:
    centerConstraints = new GridBagConstraints();
    centerConstraints.gridx = centerConstraints.gridy = 0;
    centerConstraints.gridwidth = centerConstraints.gridheight = 1;
              //seems to work a bit better with weights at 1 instead of 0:
    centerConstraints.weightx = centerConstraints.weighty = 1.0;
    centerConstraints.fill = GridBagConstraints.NONE;
    centerConstraints.anchor = GridBagConstraints.CENTER;
  }

    /**
     * Returns main content panel.  User components should added to this
     * panel.
     * @return A handle to a JPanel object.
     */
  public JPanel getMainPanel()
  {
    return mainPanel;
  }

    /**
     * Returns popup host panel.  Access to this panel is usually not
     * necessary.
     * @return A handle to a JPanel object.
     */
  public JPanel getPopupHostPanel()
  {
    return popupHostPanel;
  }

    /**
     * Displays a popup panel with an "OK" button and returns immediately.
     * @param message message text for the panel.
     * @param title title text for the panel.
     */
  public void showPopupPanel(String message,String title)
  {
    showPopupPanel(message,title,0,false,PopPanel.okButtonText,null);
  }


    /**
     * Displays a popup panel with an "OK" button and returns immediately.
     * @param message message text for the panel.
     * @param title title text for the panel.
     * @param popupCharWidth approximate width for the panel, measured in
     * characters; or zero for an automatically generated character width.
     */
  public void showPopupPanel(String message,String title,int popupCharWidth)
  {
    showPopupPanel(message,title,popupCharWidth,false,
                                                PopPanel.okButtonText,null);
  }

    /**
     * Displays a popup panel with an "OK" button.
     * @param message message text for the panel.
     * @param title title text for the panel.
     * @param waitFlag if true then the calling thread is blocked until
     * the popup panel is dismissed; if false then this function returns
     * immediately.
     */
  public void showPopupPanel(String message,String title,boolean waitFlag)
  {
    showPopupPanel(message,title,0,waitFlag,PopPanel.okButtonText,null);
  }

    /**
     * Displays a popup panel with an "OK" button.
     * @param message message text for the panel.
     * @param title title text for the panel.
     * @param popupCharWidth approximate width for the panel, measured in
     * characters; or zero for an automatically generated character width.
     * @param waitFlag if true then the calling thread is blocked until
     * the popup panel is dismissed; if false then this function returns
     * immediately.
     */
  public void showPopupPanel(String message,String title,
                                        int popupCharWidth,boolean waitFlag)
  {
    showPopupPanel(message,title,popupCharWidth,waitFlag,
                                                PopPanel.okButtonText,null);
  }

    /**
     * Displays a popup panel with an "OK" button and returns immediately.
     * @param message message text for the panel.
     * @param title title text for the panel.
     * @param cListener a 'ComponentListener' object that is attached to
     * the popup panel such that its 'componentHidden()' method will be
     * called after the popup is dismissed.
     */
  public void showPopupPanel(String message,String title,
                                                ComponentListener cListener)
  {
    showPopupPanel(message,title,0,false,PopPanel.okButtonText,cListener);
  }

    /**
     * Displays a popup panel with an "OK" button and returns immediately.
     * @param message message text for the panel.
     * @param title title text for the panel.
     * @param popupCharWidth approximate width for the panel, measured in
     * characters; or zero for an automatically generated character width.
     * @param cListener a 'ComponentListener' object that is attached to
     * the popup panel such that its 'componentHidden()' method will be
     * called after the popup is dismissed.
     */
  public void showPopupPanel(String message,String title,
                             int popupCharWidth,ComponentListener cListener)
  {
    showPopupPanel(message,title,popupCharWidth,false,
                                           PopPanel.okButtonText,cListener);
  }

    /**
     * Displays a popup panel and returns immediately.
     * @param message message text for the panel.
     * @param title title text for the panel.
     * @param buttonText if not null then a string containing the text
     * for the button; if null then no button is displayed.
     */
  public void showPopupPanel(String message,String title,String buttonText)
  {
    showPopupPanel(message,title,0,false,buttonText,null);
  }

    /**
     * Displays a popup panel and returns immediately.
     * @param message message text for the panel.
     * @param title title text for the panel.
     * @param popupCharWidth approximate width for the panel, measured in
     * characters; or zero for an automatically generated character width.
     * @param buttonText if not null then a string containing the text
     * for the button; if null then no button is displayed.
     */
  public void showPopupPanel(String message,String title,String buttonText,
                                                         int popupCharWidth)
  {
    showPopupPanel(message,title,popupCharWidth,false,buttonText,null);
  }

    /**
     * Displays a popup panel.
     * @param message message text for the panel.
     * @param title title text for the panel.
     * @param waitFlag if true then the calling thread is blocked until
     * the popup panel is dismissed; if false then this function returns
     * immediately.
     * @param buttonText if not null then a string containing the text
     * for the button; if null then no button is displayed.
     */
  public void showPopupPanel(String message,String title,boolean waitFlag,
                                                          String buttonText)
  {
    showPopupPanel(message,title,0,waitFlag,buttonText,null);
  }

    /**
     * Displays a popup panel.
     * @param message message text for the panel.
     * @param title title text for the panel.
     * @param popupCharWidth approximate width for the panel, measured in
     * characters; or zero for an automatically generated character width.
     * @param waitFlag if true then the calling thread is blocked until
     * the popup panel is dismissed; if false then this function returns
     * immediately.
     * @param buttonText if not null then a string containing the text
     * for the button; if null then no button is displayed.
     */
  public void showPopupPanel(String message,String title,int popupCharWidth,
                                         boolean waitFlag,String buttonText)
  {
    showPopupPanel(message,title,popupCharWidth,waitFlag,buttonText,null);
  }

    /**
     * Displays a popup panel and returns immediately.
     * @param message message text for the panel.
     * @param title title text for the panel.
     * @param buttonText if not null then a string containing the text
     * for the button; if null then no button is displayed.
     * @param cListener a 'ComponentListener' object that is attached to
     * the popup panel such that its 'componentHidden()' method will be
     * called after the popup is dismissed.
     */
  public void showPopupPanel(String message,String title,String buttonText,
                                                ComponentListener cListener)
  {
    showPopupPanel(message,title,0,false,buttonText,cListener);
  }

    /**
     * Displays a popup panel and returns immediately.
     * @param message message text for the panel.
     * @param title title text for the panel.
     * @param popupCharWidth approximate width for the panel, measured in
     * characters; or zero for an automatically generated character width.
     * @param buttonText if not null then a string containing the text
     * for the button; if null then no button is displayed.
     * @param cListener a 'ComponentListener' object that is attached to
     * the popup panel such that its 'componentHidden()' method will be
     * called after the popup is dismissed.
     */
  public void showPopupPanel(String message,String title,int popupCharWidth,
                              String buttonText,ComponentListener cListener)
  {
    showPopupPanel(message,title,popupCharWidth,false,buttonText,cListener);
  }

    /**
     * Displays a generic popup panel with the given title and message.
     * @param message message text for the panel.
     * @param title title text for the panel.
     * @param popupCharWidth approximate width for the panel, measured in
     * characters; or zero for an automatically generated character width.
     * @param waitFlag if true then the calling thread is blocked until
     * the popup panel is dismissed; if false then this function returns
     * immediately.
     * @param buttonText if not null then a string containing the text
     * for the button; if null then no button is displayed.
     * @param cListener a 'ComponentListener' object that is attached to
     * the popup panel such that its 'componentHidden()' method will be
     * called after the popup is dismissed.
     */
  public void showPopupPanel(String message,String title,
                      int popupCharWidth,boolean waitFlag,String buttonText,
                                                ComponentListener cListener)
  {
    synchronized(panelHostSyncObj)
    {    //grab thread-synchronization object for class instances
      final JPanel panel;
      if(popupCharWidth > 0)
      {       //valid character width; use it
        panel = new PopPanel(message,title,popupCharWidth,buttonCursor,
                                                                buttonText);
      }
      else                        //if no char width then use default
      {
        panel = new PopPanel(message,title,buttonCursor,buttonText);
      }
      showPopupPanel(panel,waitFlag,cListener);
    }
  }

    /**
     * Displays the given panel as a popup.  The given panel must have
     * its own means of clearing itself, such as making itself hidden
     * after an 'OK' button is pressed.  A 'ComponentListener' is added
     * to the given panel, which restores access to the main panel after
     * the 'componentHidden()' method is called.
     * @param panel the panel to display as a popup panel.
     */
  public void showPopupPanel(JPanel panel)
  {
    synchronized(panelHostSyncObj)
    {    //grab thread-synchronization object for class instances
      showPopupPanel(panel,false,null);
    }
  }

  /**
   * Displays a generic popup panel with the given title and message.
   * @param panel the panel to display as a popup panel.
   * @param waitFlag if true and the panel is a 'PopPanel' then the
   * calling thread is blocked until the popup panel is dismissed;
   * if false then this function returns immediately.
   * @param cListener a 'ComponentListener' object that is attached to
   * the popup panel such that its 'componentHidden()' method will be
   * called after the popup is dismissed.
   */
  public void showPopupPanel(final JPanel panel, final boolean waitFlag,
                                          final ComponentListener cListener)
  {
    synchronized(panelHostSyncObj)
    {    //grab thread-synchronization object for class instances
              //if this is the event-dispatch thread then do it now:
      if(SwingUtilities.isEventDispatchThread())
      {
        doShowPopupPanel(panel,waitFlag,cListener);
      }
      else
      {       //if this is not the event-dispatch thread then do it later:
        SwingUtilities.invokeLater(new Runnable()
            {
              public void run()
              {
                doShowPopupPanel(panel,waitFlag,cListener);
              }
            });
      }
    }
  }

  /**
   * Performs the work of displaying a generic popup panel with the given
   * title and message.
   * @param panel the panel to display as a popup panel.
   * @param waitFlag if true and the panel is a 'PopPanel' then the
   * calling thread is blocked until the popup panel is dismissed;
   * if false then this function returns immediately.
   * @param cListener a 'ComponentListener' object that is attached to
   * the popup panel such that its 'componentHidden()' method will be
   * called after the popup is dismissed.
   */
  private void doShowPopupPanel(JPanel panel,boolean waitFlag,
                                                ComponentListener cListener)
  {
    if(!displayedFlag)
    {    //popup panel is not already displayed
      displayedFlag = true;            //indicate popup now displayed
      popButtonPressedFlag = false;    //clear cached button-pressed flag
      pPanel = panel;                  //set given panel as popup panel
      userCompListener = cListener;    //save handle to user comp listener
      pPanel.setVisible(true);         //make sure popup is visible

              //get desired size for popup panel and make sure
              // that it's not too large for host panel:
      final Dimension pPanelDim = pPanel.getPreferredSize();
      final Dimension hostDim = getSize();       //get size of host panel
      if(pPanelDim.height > hostDim.height)      //if height too large then
        pPanelDim.height = hostDim.height;       //set height to max
      if(pPanelDim.width > hostDim.width)        //if width too large then
        pPanelDim.width = hostDim.width;         //set width to max

                   //add new popup panel to popup host panel, centered:
      popupHostPanel.add(pPanel,centerConstraints);
                   //add popup host panel, stretched to fill area:
      add(popupHostPanel,fullConstraints);
      popupHostPanel.setVisible(true);      //set popup host panel visible
      popupHostPanel.requestFocus();        //get KB focus off prev button
      FocusUtils.requestDefaultFocus(popupHostPanel);
      validate();                           //refresh display
                   //setup listener to remove popup after it's dismissed:
      pPanel.addComponentListener(hostCompListener=   //save listener handle
        new ComponentAdapter()
        {               //method called after popup panel is hidden:
          public void componentHidden(ComponentEvent e)
          {
            doRemovePopupPanel();      //remove popup panel
          }
        }
      );
                   //if user component listener provided then add it:
      if(userCompListener != null)
        pPanel.addComponentListener(userCompListener);

      if(waitFlag && pPanel instanceof PopPanel) //if wait requested then
        ((PopPanel)pPanel).waitForButton(); //block until panel dismissed

      //(the component listener takes care of removing the popup panel)
    }
  }

    //Does the actual work of removing and deallocating the popup panel.
  private void doRemovePopupPanel()
  {
    if(pPanel != null)
    {    //popup panel handle not null
      if(hostCompListener != null)
      {  //component listener for host was added; remove it
        pPanel.removeComponentListener(hostCompListener);
        hostCompListener = null;       //clear handle
      }
      if(userCompListener != null)
      {  //component listener for user was added; remove it
        pPanel.removeComponentListener(userCompListener);
        userCompListener = null;       //clear handle
      }
      pPanel.setVisible(false);             //hide popup panel
      popupHostPanel.setVisible(false);     //hide host panel
         //remove popup from popup host panel:
      popupHostPanel.remove(pPanel);
         //cache value of button-pressed flag before clearing handle:
      if(pPanel instanceof PopPanel)
        popButtonPressedFlag = ((PopPanel)pPanel).getPopButtonPressedFlag();
      pPanel = null;         //free link to panel
    }
    remove(popupHostPanel);            //remove popup host panel
    validate();                        //refesh display
    displayedFlag = false;             //indicate not displayed
  }

    /** Removes the currently displayed popup panel. */
  public void removePopupPanel()
  {
    removePopupPanel(false);
  }

    /**
     * Removes the currently displayed popup panel.
     * @param removeNowFlag if true then the actual work of removing the
     * popup panel is performed immediately; otherwise it happens via
     * the component-hidden event.
     */
  public void removePopupPanel(boolean removeNowFlag)
  {
    synchronized(panelHostSyncObj)
    {    //grab thread-synchronization object for class instances
      if(removeNowFlag)
      {  //flag set; do popup remove now
              //if this is the event-dispatch thread then do it now:
        if(SwingUtilities.isEventDispatchThread())
        {
          doRemovePopupPanel();
        }
        else
        {     //if this is not the event-dispatch thread then do it later:
          SwingUtilities.invokeLater(new Runnable()
              {
                public void run()
                {
                  doRemovePopupPanel();
                }
              });
        }
      }
      else
      {
              //if this is the event-dispatch thread then do it now:
        if(SwingUtilities.isEventDispatchThread())
        {
          if(pPanel != null)                //if panel OK then
            pPanel.setVisible(false);       //clear it
        }
        else
        {     //if this is not the event-dispatch thread then do it later:
          SwingUtilities.invokeLater(new Runnable()
              {
                public void run()
                {
                  if(pPanel != null)             //if panel OK then
                    pPanel.setVisible(false);    //clear it
                }
              });
        }
      }
    }
  }

    /**
     * Sets the cursor to be used with buttons on the popup panel.
     * @param buttonCursor button cursor
     */
  public void setButtonCursor(Cursor buttonCursor)
  {
    this.buttonCursor = buttonCursor;
  }

    /**
     * Returns the last value sent to 'setButtonCursor()', or null if
     * that function was never called.
     * @return the button cursor
     */
  public Cursor getButtonCursor()
  {
    return buttonCursor;
  }

    /**
     * Returns true if a popup panel is currently showing.
     * @return true if a popup panel is currently showing.
     */
  public boolean isPopupShowing()
  {
    return (pPanel != null);
  }

    /**
     * Determines if the button on the displayed PopPanel was pressed
     * (resulting in the PopPanel being closed).
     * @return true if the button on the displayed PopPanel was pressed;
     * false if not.
     */
  public boolean getPopButtonPressedFlag()
  {
    final JPanel pObj;
    return ((pObj=pPanel) instanceof PopPanel) ?
         ((PopPanel)pObj).getPopButtonPressedFlag() : popButtonPressedFlag;
  }

    /**
     * Override of 'setLayout()' method that throws a runtime exception if
     * an attempt is made to change the layout on a 'PopPanelHost' object.
     * @param mgr layout manager
     */
  public void setLayout(LayoutManager mgr)
  {
    if(!freezeLayoutMgrFlag)           //if flag not set then
      super.setLayout(mgr);            //allow new layout manager
    else
    {
      throw new RuntimeException("Cannot change layout on 'PopPanelHost' " +
                             "object; use 'getMainPanel()' object instead");
    }
  }
}
