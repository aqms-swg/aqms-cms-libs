package com.isti.util.gui;

import java.awt.AWTEvent;
import java.awt.Component;
import java.awt.Container;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.util.ArrayList;
import java.util.List;

import javax.swing.AbstractButton;
import javax.swing.JMenuItem;
import javax.swing.JPopupMenu;
import javax.swing.JViewport;

/**
 * Create the popup handler.
 * <p>
 * The 'processMenuItem' method may be overridden or the
 * 'setMenuItemActionListener' method may be used to process menu item actions.
 */
public class PopupHandler implements ActionListener, MouseListener {
  /** The button list. */
  private final List buttonList = new ArrayList();

  /** The last popup menu invoker. */
  private Component lastPopupMenuInvoker = null;

  /** The last popup menu Y location */
  private int lastPopupMenuY;

  /** The menu item action listener or null if none. */
  private ActionListener menuItemActionListener = null;

  /** The parent component. */
  private final Component parent;

  /** The popup menu */
  private final JPopupMenu popupMenu;

  /**
   * Create the popup handler.
   * @param popupMenu the popup menu.
   * @param parent the component in which the popup menu menu is to be
   *          displayed.
   */
  public PopupHandler(JPopupMenu popupMenu, Component parent) {
    this.popupMenu = popupMenu;
    this.parent = parent;
    parent.addMouseListener(this);
  }

  /**
   * Action listener.
   * @param e the action event.
   */
  public void actionPerformed(ActionEvent e) {
    final Object source = e.getSource();
    if (buttonList.contains(source)) {
      showPopup(e);
    } else if (source instanceof JMenuItem) {
      processMenuItem((JMenuItem) source);
    }
  }

  /**
   * Add a button.
   * @param button the button.
   */
  public void addButton(AbstractButton button) {
    buttonList.add(button);
    button.addActionListener(this);
  }

  /**
   * Adds the menu item.
   * @param menuItem the menu item.
   * @return the menu item.
   */
  public JMenuItem addMenuItem(JMenuItem menuItem) {
    popupMenu.add(menuItem);
    menuItem.addActionListener(this);
    if (menuItemActionListener != null) {
      menuItem.addActionListener(menuItemActionListener);
    }
    return menuItem;
  }

  /**
   * Adds a menu item with the specified text.
   * @param text the menu item text or null for separator.
   * @return the menu item or null if separator.
   */
  public JMenuItem addMenuItem(String text) {
    if (text == null) {
      addSeparator();
      return null;
    }
    return addMenuItem(createMenuItem(text));
  }

  /**
   * Adds menu items with the specified text.
   * @param text the array of menu item text or null for separator.
   */
  public void addMenuItems(String[] text) {
    for (int i = 0; i < text.length; i++) {
      addMenuItem(text[i]);
    }
  }

  /**
   * Appends a new separator at the end of the menu.
   */
  public void addSeparator() {
    popupMenu.addSeparator();
  }

  /**
   * Calculate the popup menu Y location.
   * @param invoker the invoker.
   * @return the popup menu Y location.
   */
  protected int calculatePopupMenuY(Component invoker) {
    boolean belowFlag = false;
    if (popupMenu.getHeight() == 0) {
      popupMenu.show(invoker, 0, 0);
    }
    int popupMenuY = getHeight(invoker);
    final int invokerLocationY = invoker.getLocationOnScreen().y;
    final int parentLocationY = parent.getLocationOnScreen().y;
    final int parentBottom = parentLocationY + getHeight(parent);
    // if invoker is lower than the parent
    if (invokerLocationY > parentBottom) {
      belowFlag = true;
    } else {
      // if invoker is lower than parent bottom
      final int popupBottom = invokerLocationY + popupMenuY
          + popupMenu.getHeight();
      if (popupBottom > parentBottom) {
        belowFlag = true;
      }
    }

    if (belowFlag) {
      popupMenuY = -popupMenu.getHeight() - invoker.getY();
    }
    return popupMenuY;
  }

  /**
   * Create the menu item with the specified text.
   * @param text the text.
   * @return the menu item.
   */
  protected JMenuItem createMenuItem(String text) {
    return new JMenuItem(text);
  }

  /**
   * Get the component height.
   * @param component the component.
   * @return the component height.
   */
  protected int getHeight(Component component) {
    Container container = component.getParent();
    if (container instanceof JViewport) {
      component = container;
    }
    return component.getHeight();
  }

  /**
   * Get the menu item action listener.
   * @return the menu item action listener or null if none.
   */
  public ActionListener getMenuItemActionListener() {
    return menuItemActionListener;
  }

  /**
   * Get the popup menu Y location.
   * @param invoker the invoker.
   * @return the popup menu Y location.
   */
  protected int getPopupMenuY(Component invoker) {
    if (invoker != lastPopupMenuInvoker) {
      lastPopupMenuInvoker = invoker;
      lastPopupMenuY = calculatePopupMenuY(invoker);
    }
    return lastPopupMenuY;
  }

  /**
   * Invoked when the mouse button has been clicked (pressed and released) on a
   * component.
   * @param e the mouse event.
   */
  public void mouseClicked(MouseEvent e) {
  }

  /**
   * Invoked when the mouse enters a component.
   * @param e the mouse event.
   */
  public void mouseEntered(MouseEvent e) {
  }

  /**
   * Invoked when the mouse exits a component.
   * @param e the mouse event.
   */
  public void mouseExited(MouseEvent e) {
  }

  /**
   * Invoked when a mouse button has been pressed on a component.
   * @param e the mouse event.
   */
  public void mousePressed(MouseEvent e) {
    final Object source = e.getSource();
    if (source == parent) {
      showPopup(e);
    }
  }

  /**
   * Invoked when a mouse button has been released on a component.
   * @param e the mouse event.
   */
  public void mouseReleased(MouseEvent e) {
    final Object source = e.getSource();
    if (source == parent) {
      showPopup(e);
    }
  }

  /**
   * Process the menu item.
   * @param menuItem the menu item.
   */
  protected void processMenuItem(JMenuItem menuItem) {
  }

  /**
   * Remove a button.
   * @param button the button.
   * @return true if the button was removed, false otherwise.
   */
  public boolean removeButton(AbstractButton button) {
    button.removeActionListener(this);
    return buttonList.remove(button);
  }

  /**
   * Remove the menu item action listener.
   * @return the menu item action listener that was removed or null if none.
   */
  public ActionListener removeMenuItemActionListener() {
    Component component;
    ActionListener l = menuItemActionListener;
    menuItemActionListener = null;
    for (int i = 0; i < popupMenu.getComponentCount(); i++) {
      component = popupMenu.getComponent(i);
      if (component instanceof JMenuItem) {
        ((JMenuItem) component).removeActionListener(l);
      }
    }
    return l;
  }

  /**
   * Set the menu item action listener.
   * @param l the menu item action listener.
   */
  public void setMenuItemActionListener(ActionListener l) {
    Component component;
    menuItemActionListener = l;
    for (int i = 0; i < popupMenu.getComponentCount(); i++) {
      component = popupMenu.getComponent(i);
      if (component instanceof JMenuItem) {
        ((JMenuItem) component).addActionListener(l);
      }
    }
  }

  /**
   * Show the popup menu if conditions are met.
   * @param e the event or null if none.
   */
  protected void showPopup(AWTEvent e) {
    Component invoker = parent;
    int x = 0;
    int y = 0;
    if (e instanceof MouseEvent) {
      final MouseEvent me = (MouseEvent) e;
      if (!me.isPopupTrigger()) {
        return;
      }
      invoker = me.getComponent();
      x = me.getX();
      y = me.getY();
    } else {
      Object source = e.getSource();
      if (source instanceof Component) {
        invoker = (Component) source;
        y = getPopupMenuY(invoker);
      }
    }
    popupMenu.show(invoker, x, y);
  }
}
