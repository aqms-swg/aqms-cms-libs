//ProgressDialog.java:  A dialog with a progress bar.
//
//   6/3/2004 -- [KF]  Initial version.
//  1/29/2009 -- [KF]  Modified to keep the label when the message string is set.
//  7/23/2010 -- [KF]  Removed call to "pack" from the constructor and
//                     modified 'setMessageStr' to use the dispatch thread.
//

package com.isti.util.gui;

import java.awt.BorderLayout;
import java.awt.Component;
import java.awt.Font;
import javax.swing.*;
import com.isti.util.ProgressIndicatorInterface;

/**
 * Class ProgressDialog is a dialog with a progress bar.
 */
public class ProgressDialog extends IstiDialogPopup
{
  /** Name string for "Cancel" button. */
  public static final String CANCEL_STR = "Cancel";

  /** Button string. */
  private final String buttonStr;

  /** Progress bar shown on dialog. */
  private final ManagedJProgressBar progressBarObj = new ManagedJProgressBar();

  private JLabel msgLabelObj = null;

  /**
   * Creates a modal dialog with a progress bar and a "Cancel" button.
   * @param parentComp the parent component for the popup.
   * @param titleStr the title string for popup window.
   * @param msgStr the message string.
   */
  public ProgressDialog(Component parentComp,String titleStr,String msgStr)
  {
    this(parentComp,titleStr,msgStr,CANCEL_STR,false);
  }

  /**
   * Creates a dialog with a progress bar and an optional "Cancel" button.
   * @param parentComp the parent component for the popup.
   * @param titleStr the title string for popup window.
   * @param msgStr the message string.
   * @param buttonStr the button string, or null to display
   * no buttons.
   * @param modalFlag true for modal, false for modeless (allows other
   * windows to run).
   */
  public ProgressDialog(
      Component parentComp,String titleStr,String msgStr,String buttonStr,
      boolean modalFlag)
  {
    //call super-class constructor with new JPanel object:
    super(parentComp,(new JPanel(new BorderLayout(0,10))),
          titleStr,buttonStr,modalFlag);
    this.buttonStr = buttonStr;
    final Object obj;        //fetch JPanel from super-class
    if((obj=getMessageObj()) instanceof JPanel)
    {
      //JPanel fetched from super-class OK
      final JPanel msgPanelObj = (JPanel)obj;
      msgLabelObj = new JLabel(msgStr);  //create message label
      final Font fontObj = msgLabelObj.getFont(); //inc font size a bit
      msgLabelObj.setFont(fontObj.deriveFont(
          fontObj.getSize2D()+2.0f));
      msgPanelObj.add(msgLabelObj,BorderLayout.CENTER);
      msgPanelObj.add(progressBarObj,BorderLayout.SOUTH);
    }
  }

  /**
   * Returns the text string for the "Cancel" button.
   * @return The text string for the "Cancel" button.
   */
  public String getButtonString()
  {
    return buttonStr;
  }

  /**
   * Returns the message object displayed by this dialog.
   * @return The message obj.
   */
  public final Object getMessageObj()
  {
    return super.getMessageObj();
  }

  /**
   * Returns the message string displayed by this dialog.
   * @return The message String object, or null if a String object
   * could not be retrieved.
   */
  public String getMessageStr()
  {
    if (msgLabelObj == null)
    {
      return super.getMessageStr();
    }
    else
    {
      return msgLabelObj.getText();
    }
  }

  /**
   * Returns the progress-bar object.
   * @return The progress-bar object.
   */
  public ProgressIndicatorInterface getProgressBarObj()
  {
    return progressBarObj;
  }

  /**
   * Sets the message object displayed by this dialog.
   * @param obj the message obj.
   */
  public void setMessageObj(Object obj)
  {
    super.setMessageObj(obj);
    msgLabelObj = null;
  }

  /**
   * Sets the message object displayed by this dialog to the given string.
   * @param str the message string.
   */
  public void setMessageStr(final String str)
  {
    if (msgLabelObj == null)
    {
      super.setMessageStr(str);
    }
    else
    {
      //if not enforcing changes via event-dispatch thread or
      // if this is the event-dispatch thread then do it now:
      if(!enforceDispatchThreadFlag || SwingUtilities.isEventDispatchThread())
        msgLabelObj.setText(str);
      else
      {         //do it later via event-dispatch thread:
        SwingUtilities.invokeLater(new Runnable()
        {
          public void run()
          {
            msgLabelObj.setText(str);
          }
        });
      }
    }
  }
}
