//SetupLookAndFeel.java:  Set the current default look and feel.
//
//    9/8/2004 -- [KF]  Initial version.
//

package com.isti.util.gui;

import javax.swing.UIManager;

/**
 * Class SetupLookAndFeel has a constructor that sets up the "look &
 * feel" for the applet.  Creating an object of this class at the top
 * of a class definition causes all objects created afterward to have
 * to desired "look & feel".  (Putting the 'UIManager.setLookAndFeel()'
 * call into a 'static' block only works the first time an applet is
 * started in a given browser.)
 */
public class SetupLookAndFeel
{
  /**
   * Set the current default look and feel to the native systems look and feel
   * if there is one, otherwise the default cross platform look and feel.
   */
  public SetupLookAndFeel()
  {
    setLookAndFeel();
  }

  /**
   * Set the current default look and feel.
   * @param nativeFlag true for the native systems look and feel if there is one,
   * otherwise the default cross platform look and feel.
   */
  public SetupLookAndFeel(boolean nativeFlag)
  {
    setLookAndFeel(nativeFlag);
  }

  /**
   * Set the current default look and feel.
   * @param className  a string specifying the name of the class that implements
   *        the look and feel
   */
  public SetupLookAndFeel(String className)
  {
    setLookAndFeel(className);
  }

  /**
   * Gets the a string specifying the name of the class that implements
   *        the look and feel.
   * @param nativeFlag true for the native systems look and feel if there is one,
   * otherwise the default cross platform look and feel.
   * @return the a string specifying the name of the class that implements
   *        the look and feel.
   */
  public static String getLookAndFeelClassName(boolean nativeFlag)
  {
    //if native then return the class name of the native systems look and feel
    if (nativeFlag)
      return UIManager.getSystemLookAndFeelClassName();
    //return the class name of the default cross platform look and feel
    return UIManager.getCrossPlatformLookAndFeelClassName();
  }

  /**
   * Set the current default look and feel to the native systems look and feel
   * if there is one, otherwise the default cross platform look and feel.
   */
  public static void setLookAndFeel()
  {
    setLookAndFeel(true);
  }

  /**
   * Set the current default look and feel.
   * @param nativeFlag true for the native systems look and feel if there is one,
   * otherwise the default cross platform look and feel.
   */
  public static void setLookAndFeel(boolean nativeFlag)
  {
    setLookAndFeel(getLookAndFeelClassName(nativeFlag));
  }

  /**
   * Set the current default look and feel using a class name.
   *
   * @param className  a string specifying the name of the class that implements
   *        the look and feel
   */
  public static void setLookAndFeel(String className)
  {
    try
    {
      UIManager.setLookAndFeel(className);
    }
    catch(Exception e) {}
  }
}