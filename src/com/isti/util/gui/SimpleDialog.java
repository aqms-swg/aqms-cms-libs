//SimpleDialog.java:  Extends 'JDialog' in order to provide simple
//                    creation and access.
//
//  6/17/2002 -- [KF]  Initial version.
//  7/10/2002 -- [KF]  Added 'setInitialFocus()' method and support.
//  7/18/2002 -- [ET]  Class moved to isti.util.gui; renamed version of
//                     'setInitialFocus()' method that takes a parameter
//                     to 'setInitialFocusComponent()'.
//  10/3/2002 -- [KF]  Added new constructors with a paramater to control
//                     centering the dialog over the frame.
//  11/4/2002 -- [KF]  Add overridden 'show()' method that blocks if this
//                     dialog is modal and another modal dialog is showing.
// 11/14/2002 -- [ET]  Took overridden 'show()' method back out as it had
//                     the potential to block other AWT activity.
// 12/21/2004 -- [ET]  Removed deprecated 'show()' method.
//

package com.isti.util.gui;

import java.awt.AWTEvent;
import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Component;
import java.awt.FlowLayout;
import java.awt.Frame;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;

import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.JComponent;
import javax.swing.JDialog;
import javax.swing.JPanel;
import javax.swing.WindowConstants;
import javax.swing.border.Border;

/**
 * Class SimpleDialog extends 'JDialog' in order to provide simple
 * creation and access.
 */
public class SimpleDialog extends JDialog implements ActionListener
{
  private JPanel topPanel = new JPanel(new BorderLayout());
  private Component message;    // the Component to display on the dialog
  private JButton closeButton;  // the JButton to close the dialog
                                // the component that has initial focus:
  private JComponent initialFocusComponent;

   /**
     * Returns the specified component's toplevel <code>Frame</code>.
     *
     * @param parentComponent the <code>Component</code> to check for a
     *		<code>Frame</code> or <code>Dialog</code>
     * @return the <code>Frame</code> that contains the component, or
     *         	<code>null</code> if the component is <code>null</code>,
     *		or does not have a valid <code>Frame</code> parent
     */
    public static Frame getFrameForComponent(Component parentComponent)
    {
        if (parentComponent == null)
            return null;
        if (parentComponent instanceof Frame)
            return (Frame)parentComponent;
        return SimpleDialog.getFrameForComponent(parentComponent.getParent());
    }

    /**
     * Creates a modal or non-modal dialog with the specified title
     * and the specified owner frame and centers the dialog over the frame.
     *
     * @param owner the <code>Component</code> from which the dialog is displayed
     * @param title  the <code>String</code> to display in the dialog's
     *			title bar
     * @param modal  true for a modal dialog, false for one that allows
     *               other windows to be active at the same time
     * @param message the <code>Component</code> to display on the dialog
     * @param button  the <code>JButton</code> to close the dialog or
     *                  <code>null</code> if no close button is desired
     *                If the button has no text default text will be provided.
     *                If the button has no parent it is added to the dialog.
     */
  public SimpleDialog(Component owner, String title, boolean modal,
                    Component message, JButton button)
  {
    this(getFrameForComponent(owner), title, modal, message, button, true);
  }

  /**
   * Creates a modal or non-modal dialog with the specified title
   * and the specified owner frame.
   *
   * @param owner the <code>Component</code> from which the dialog is displayed
   * @param title  the <code>String</code> to display in the dialog's
   *			title bar
   * @param modal  true for a modal dialog, false for one that allows
   *               other windows to be active at the same time
   * @param message the <code>Component</code> to display on the dialog
   * @param button  the <code>JButton</code> to close the dialog or
   *                  <code>null</code> if no close button is desired
   *                If the button has no text default text will be provided.
   *                If the button has no parent it is added to the dialog.
   * @param centerFlag true to center the dialog over the frame.
   */
  public SimpleDialog(Component owner, String title, boolean modal,
                      Component message, JButton button, boolean centerFlag)
  {
    this(getFrameForComponent(owner), title, modal, message, button, centerFlag);
  }

  /**
   * Creates a modal or non-modal dialog with the specified title
   * and the specified owner frame and centers the dialog over the frame.
   *
   * @param owner the <code>Frame</code> from which the dialog is displayed
   * @param title  the <code>String</code> to display in the dialog's
   *			title bar
   * @param modal  true for a modal dialog, false for one that allows
   *               other windows to be active at the same time
   * @param message the <code>Component</code> to display on the dialog
   * @param button  the <code>JButton</code> to close the dialog or
   *                  <code>null</code> if no close button is desired
   *                If the button has no text default text will be provided.
   *                If the button has no parent it is added to the dialog.
   */
  public SimpleDialog(Frame owner, String title, boolean modal,
                      Component message, JButton button)
  {
    this(owner, title, modal, message, button, true);
  }

    /**
     * Creates a modal or non-modal dialog with the specified title
     * and the specified owner frame.
     *
     * @param owner the <code>Frame</code> from which the dialog is displayed
     * @param title  the <code>String</code> to display in the dialog's
     *			title bar
     * @param modal  true for a modal dialog, false for one that allows
     *               other windows to be active at the same time
     * @param message the <code>Component</code> to display on the dialog
     * @param button  the <code>JButton</code> to close the dialog or
     *                  <code>null</code> if no close button is desired
     *                If the button has no text default text will be provided.
     *                If the button has no parent it is added to the dialog.
     * @param centerFlag true to center the dialog over the frame.
     */
  public SimpleDialog(Frame owner, String title, boolean modal,
                    Component message, JButton button, boolean centerFlag)
  {
    super(owner, title, modal);
    this.message = message;
    this.closeButton = button;
    this.initialFocusComponent = button;

    try
    {
      jbInit();
    }
    catch(Exception e)
    {
      e.printStackTrace();
    }

    // if centerFlag is true and the owner frame was specified
    if (centerFlag && owner != null)
    {
      // center the dialog over the frame
      setLocationRelativeTo(owner);
    }
  }

  /**
   * Component initialization
   * @throws Exception
   */
  private void jbInit() throws Exception
  {
    this.enableEvents(AWTEvent.WINDOW_EVENT_MASK);
    this.setBackground(topPanel.getBackground());
    this.setDefaultCloseOperation(WindowConstants.DISPOSE_ON_CLOSE);
    this.setForeground(topPanel.getForeground());

    topPanel.setBackground(null);
    // create a default border
    topPanel.setBorder(BorderFactory.createEmptyBorder(10,10,10,10));
    topPanel.setForeground(null);

    // if closeButton was provided
    if (closeButton != null)
    {
      // if no text was supplied use default text
      if (closeButton.getText().length() <= 0)  closeButton.setText("OK");

      // if the closeButton doesn't have a parent
      if (closeButton.getParent() == null)
      {
        // create a panel with a closeButton and add it
        JPanel buttonPanel = new JPanel(new FlowLayout());
        buttonPanel.setBackground(null);
        buttonPanel.setForeground(null);
        closeButton.setBackground(null);
        closeButton.setForeground(null);
        buttonPanel.add(closeButton, null);
        topPanel.add(buttonPanel, BorderLayout.SOUTH);
      }
    }

    // if a message was specified add the message
    if (message != null)  topPanel.add(message, BorderLayout.CENTER);

    // add the panel to the dialog
    getContentPane().add(topPanel, null);

    // listen to the closeButton
    if (closeButton != null)  closeButton.addActionListener(this);

    // size the Window to fit the preferred size and layouts of its subcomponents
    pack();

    // listen to window events in order to set the initial focus
    addWindowListener(new WindowAdapter()
    {
      private boolean gotFocus = false;
      public void windowClosing(WindowEvent we)
      {
        gotFocus = false;
      }
      public void windowActivated(WindowEvent we)
      {
        // Once window gets focus, set initial focus
        if (!gotFocus)
        {
          setInitialFocus();
          gotFocus = true;
        }
      }
    });
  }

  /**
   * Requests that the current "initial" component have the keyboard focus.
   */
  public void setInitialFocus()
  {
    if (initialFocusComponent != null)
    {
      if (closeButton != initialFocusComponent && closeButton != null)
        closeButton.setRequestFocusEnabled(false);
      initialFocusComponent.requestFocus();
    }
  }

  /**
   * Requests that the given component have the keyboard focus.
   * @param component initial focus component
   */
  public void setInitialFocusComponent(JComponent component)
  {
    this.initialFocusComponent = component;
    setInitialFocus();
  }

  /**
   * Closes the dialog on a closeButton event.
   * @param e action event
   */
  public void actionPerformed(ActionEvent e)
  {
    if (closeButton != null && e.getSource() == closeButton)
    {
      //Close the dialog
      setVisible(false);
      dispose();
    }
  }

  /**
   * Sets the background color of this component.
   * @param c The color to become this component's color.
   */
  public void setBackground(Color c)
  {
    super.setBackground(c);
    getContentPane().setBackground(c);
  }

  /**
   * Sets the border of this component.
   *
   * @param border the border to be rendered for this component or
   *               <code>null</code> if no border is desired.
   */
  public void setBorder(Border border)
  {
    if (topPanel != null)
      topPanel.setBorder(border);
  }

  /**
   * Returns the border of this component or null if no border is
   * currently set.
   *
   * @return the border object for this component
   * @see #setBorder
   */
  public Border getBorder()
  {
    if (topPanel != null)
      return topPanel.getBorder();
    return null;
  }

  /**
   * Overridden version sets up focus after displaying the dialog.
   */
//  public void show()
//  {
//    setInitialFocus();       //give initial component the focus
//                             // (helps with some initial-focus issues)
//    super.show();            //call parent show method
//  }
}
