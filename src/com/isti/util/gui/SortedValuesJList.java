//SortedValuesJList.java:  Extends JList to implement a sorted-table model
//                         and to enforce the paradigm that list changes
//                         should only happen via the event-dispatch thread.
//
// 10/22/2003 -- [ET]  Initial version.
//  1/29/2004 -- [ET]  Modified 'doSetColors()' to call 'repaint()' to
//                     make sure display is updated.
//  9/10/2004 -- [KF]  Added constructor with 'sortDirFlag'.
// 10/15/2004 -- [KF]  Added 'put()' and 'putAll()' methods with 'sortFlag'.
//  12/7/2005 -- [KF]  Added support for 'ExtendedComparable'.
//  5/26/2006 -- [ET]  Added 'isSelectionVisible()' method; modified
//                     'put()' method to keep selection visible.
//   6/5/2006 -- [ET]  Modified 'putAll()' and 'removeKey()' methods to
//                     keep selection visible.
//   5/2/2007 -- [KF]  Changed to not process adjusting list selection events.
//

package com.isti.util.gui;

import java.util.Map;
import java.awt.Color;
import java.awt.Point;
import javax.swing.*;
import javax.swing.event.ListSelectionListener;
import javax.swing.event.ListSelectionEvent;
import com.isti.util.ExtendedComparable;

/**
 * Class SortedValuesJList extends JList to implement a sorted-table model
 * and to enforce the paradigm that list changes should only happen
 * via the event-dispatch thread.  Elements in the list are sorted
 * according to the natural ordering of the value objects (not the
 * keys) in the table model.
 */
public class SortedValuesJList extends JList
{
  /** FifoHashtable-based model for list. */
  protected final FifoHashListModel sortedListModelObj =
                                                    new FifoHashListModel();
  /** Direction-of-sort flag for 'FifoHashListModel' used by list (true). */
  protected static final boolean SORT_DIR_FLAG = true;
  /** Direction-of-sort flag for 'FifoHashListModel' used by list (true). */
  private boolean sortDirFlag;
  /** The sort type for 'FifoHashListModel' used by list. */
  private int sortType;
  /** Key object for currently selected item. */
  protected Object selectedKeyObject = null;
  /** Thread-synchronization object for 'selectedKeyObject'. */
  protected final Object selectedKeySyncObj = new Object();
  /** Call-back object used when list selection changes. */
  protected ListSelKeyChangeCallBack listSelKeyChangeCallBackObj = null;

  /**
   * Creates a sorted JList with a sorted-values-table model.  If the
   * given map object is non-null then its elements are added to the
   * table model, sorted according to the natural ordering of the
   * value objects.
   * @param cellRendererObj list-cell renderer to use, or null to
   * use the default list-cell renderer.
   * @param tableMapObj the map object to use, or null for none.
   * @param sortDirFlag true for ascending sort order, false for
   * descending.
   * @param sortType the sort type.
   * @throws ClassCastException if the values in the map are not
   * Comparable, or are not mutually comparable.
   */
  public SortedValuesJList(ListCellRenderer cellRendererObj,Map tableMapObj,
                           boolean sortDirFlag,int sortType)
  {
    this.sortDirFlag = sortDirFlag;
    this.sortType = sortType;
    if(cellRendererObj != null)             //if cell renderer given then
      setCellRenderer(cellRendererObj);     //enter cell renderer
                                  //if table given then
    if(tableMapObj != null)       //enter values from map, sorted ascending
      sortedListModelObj.putSortByValueAll(tableMapObj,sortDirFlag,sortType);
    setModel(sortedListModelObj); //enter model object into JList
              //add listener to track list selection changes:
    addListSelectionListener(new ListSelectionListener()
        {
          public void valueChanged(ListSelectionEvent evtObj)
          {
            if (!evtObj.getValueIsAdjusting())
            {
              processListSelectionChange();
            }
          }
        });
  }

  /**
   * Creates a sorted JList with a sorted-values-table model.  If the
   * given map object is non-null then its elements are added to the
   * table model, sorted according to the natural ordering of the
   * value objects.
   * @param cellRendererObj list-cell renderer to use, or null to
   * use the default list-cell renderer.
   * @param tableMapObj the map object to use, or null for none.
   * @param sortDirFlag true for ascending sort order, false for
   * descending.
   * @throws ClassCastException if the values in the map are not
   * Comparable, or are not mutually comparable.
   */
  public SortedValuesJList(ListCellRenderer cellRendererObj,Map tableMapObj,
                           boolean sortDirFlag)
  {
    this(cellRendererObj,tableMapObj,sortDirFlag,ExtendedComparable.NO_SORT_TYPE);
  }

  /**
   * Creates a sorted JList with a sorted-values-table model.  If the
   * given map object is non-null then its elements are added to the
   * table model, sorted according to the natural ordering of the
   * value objects.
   * @param cellRendererObj list-cell renderer to use, or null to
   * use the default list-cell renderer.
   * @param tableMapObj the map object to use, or null for none.
   * @throws ClassCastException if the values in the map are not
   * Comparable, or are not mutually comparable.
   */
  public SortedValuesJList(ListCellRenderer cellRendererObj,Map tableMapObj)
  {
    this(cellRendererObj,tableMapObj,SORT_DIR_FLAG);
  }

  /**
   * Creates a sorted JList with a sorted-table model.
   * @param cellRendererObj list-cell renderer to use, or null to
   * use the default list-cell renderer.
   */
  public SortedValuesJList(ListCellRenderer cellRendererObj)
  {
    this(cellRendererObj,null);
  }

  /**
   * Creates a sorted JList with a sorted-values-table model.  If the
   * given map object is non-null then its elements are added to the
   * table model, sorted according to the natural ordering of the
   * value objects.
   * @param tableMapObj the map object to use, or null for none.
   * @throws ClassCastException if the values in the map are not
   * Comparable, or are not mutually comparable.
   */
  public SortedValuesJList(Map tableMapObj)
  {
    this(null,tableMapObj);
  }

  /**
   * Creates a sorted JList with a sorted-table model.
   */
  public SortedValuesJList()
  {
    this(null,null);
  }

  /**
   * Gets the direction-of-sort flag for 'FifoHashListModel' used by list.
   * @return true for ascending sort order, false for descending.
   */
  public boolean getSortFlag()
  {
    return sortDirFlag;
  }

  /**
   * Gets the sort type for 'FifoHashListModel' used by list.
   * @return the sort type.
   */
  public int getSortType()
  {
    return sortType;
  }

  /**
   * Resorts the list with the specified direction and type.
   * @param sortDirFlag true for ascending sort order, false for
   * descending.
   * @param sortType the sort type.
   */
  public void reSortList(final boolean sortDirFlag,final int sortType)
  {
              //if this is the event-dispatch thread then do it now:
    if(SwingUtilities.isEventDispatchThread())
    {
      doReSortList(sortDirFlag,sortType);
    }
    else
    {         //if this is not the event-dispatch thread then do it later:
      SwingUtilities.invokeLater(new Runnable()
      {
        public void run()
        {
          doReSortList(sortDirFlag,sortType);
        }
      });
    }
  }

  /**
   * Resorts the list with the specified direction and type.
   * @param sortDirFlag true for ascending sort order, false for
   * descending.
   * @param sortType the sort type.
   */
  private void doReSortList(boolean sortDirFlag,int sortType)
  {
    this.sortDirFlag = sortDirFlag;
    this.sortType = sortType;
    sortedListModelObj.reSortList(sortDirFlag,sortType);
    resyncSelectedItem();       //keep selected item the same
  }

  /**
   * Adds a key/value pair to the list, placing it in sorted
   * order according to the value.  The value should implement the
   * Comparable interface.  If the list previously contained a mapping
   * for this key, the old value is deleted and the new value is placed
   * in proper sort order.  If the thread calling this method is not
   * the event-dispatch thread then the action is queued and invoked
   * later by the event-dispatch thread.
   * @param key key with which the specified value is to be associated.
   * @param value value to be associated with the specified key.
   */
  public void put(final Object key,final Object value)
  {
    put(key,value,value instanceof Comparable);
  }

  /**
   * Adds a key/value pair to the list, optionally placing it in sorted
   * order according to the value.  If sorted the value should implement the
   * Comparable interface.  If the list previously contained a mapping
   * for this key, the old value is deleted and the new value is placed
   * in proper sort order.  If the thread calling this method is not
   * the event-dispatch thread then the action is queued and invoked
   * later by the event-dispatch thread.
   * @param key key with which the specified value is to be associated.
   * @param value value to be associated with the specified key.
   * @param sortFlag true to sort the values, false otherwise.
   */
  public void put(final Object key,final Object value,final boolean sortFlag)
  {
              //if this is the event-dispatch thread then do it now:
    if(SwingUtilities.isEventDispatchThread())
    {
      final boolean selVisFlag = isSelectionVisible();
      if(sortFlag)
        sortedListModelObj.putSortByValue(key,value,sortDirFlag,sortType);
      else
        sortedListModelObj.put(key,value);
      resyncSelectedItem();       //keep selected item the same
      if(selVisFlag)                   //if selection was visible then
        ensureSelectionVisible();      //make sure it's still visible
    }
    else
    {         //if this is not the event-dispatch thread then do it later:
      SwingUtilities.invokeLater(new Runnable()
          {
            public void run()
            {
              final boolean selVisFlag = isSelectionVisible();
              if(sortFlag)
                sortedListModelObj.putSortByValue(key,value,sortDirFlag,sortType);
              else
                sortedListModelObj.put(key,value);
              resyncSelectedItem();    //keep selected item the same
              if(selVisFlag)                //if selection was visible then
                ensureSelectionVisible();   //make sure it's still visible
            }
          });
    }
  }

  /**
   * Copies all of the mappings from the specified map into the list,
   * placing them in sorted order according to the values.  The values
   * should implement the Comparable interface.  These mappings replace
   * any mappings that this list had for any of the keys currently in
   * the specified Map.  If the thread calling this method is not the
   * event-dispatch thread then the action is queued and invoked later
   * by the event-dispatch thread.
   * @param mapObj mappings to be stored in this map.
   */
  public void putAll(final Map mapObj)
  {
    putAll(mapObj,true);
  }

  /**
   * Copies all of the mappings from the specified map into the list,
   * placing them in sorted order according to the values.  The values
   * should implement the Comparable interface.  These mappings replace
   * any mappings that this list had for any of the keys currently in
   * the specified Map.  If the thread calling this method is not the
   * event-dispatch thread then the action is queued and invoked later
   * by the event-dispatch thread.
   * @param mapObj mappings to be stored in this map.
   * @param sortFlag true to sort the values, false otherwise.
   */
  public void putAll(final Map mapObj,final boolean sortFlag)
  {
              //if this is the event-dispatch thread then do it now:
    if(SwingUtilities.isEventDispatchThread())
    {
      final boolean selVisFlag = isSelectionVisible();
      if (sortFlag)
        sortedListModelObj.putSortByValueAll(mapObj,sortDirFlag,sortType);
      else
        sortedListModelObj.putAll(mapObj);
      resyncSelectedItem();       //keep selected item the same
      if(selVisFlag)                   //if selection was visible then
        ensureSelectionVisible();      //make sure it's still visible
    }
    else
    {         //if this is not the event-dispatch thread then do it later:
      if(mapObj.size() > 0)
      {  //map object contains entries
        SwingUtilities.invokeLater(new Runnable()
            {
              public void run()
              {
                final boolean selVisFlag = isSelectionVisible();
                if (sortFlag)
                {
                  sortedListModelObj.putSortByValueAll(
                                               mapObj,sortDirFlag,sortType);
                }
                else
                  sortedListModelObj.putAll(mapObj);
                resyncSelectedItem();       //keep selected item the same
                if(selVisFlag)              //if selection was visible then
                  ensureSelectionVisible(); //make sure it's still visible
              }
            });
      }
    }
  }

  /**
   * Removes the mapping for the given key from the list (if present).
   * If the thread calling this method is not the event-dispatch thread
   * then the action is queued and invoked later by the event-dispatch
   * thread.
   * @param key key whose mapping is to be removed from the hashtable.
   */
  public void removeKey(final Object key)
  {
              //if this is the event-dispatch thread then do it now:
    if(SwingUtilities.isEventDispatchThread())
    {
      final boolean selVisFlag = isSelectionVisible();
      sortedListModelObj.removeKey(key);
      resyncSelectedItem();       //keep selected item the same
      if(selVisFlag)                   //if selection was visible then
        ensureSelectionVisible();      //make sure it's still visible
    }
    else
    {         //if this is not the event-dispatch thread then do it later:
      SwingUtilities.invokeLater(new Runnable()
          {
            public void run()
            {
              final boolean selVisFlag = isSelectionVisible();
              sortedListModelObj.removeKey(key);
              resyncSelectedItem();         //keep selected item the same
              if(selVisFlag)                //if selection was visible then
                ensureSelectionVisible();   //make sure it's still visible
            }
          });
    }
  }

  /**
   * Clears any current list data and enters the items from the given
   * Map object.  If the thread calling this method is not the event-
   * dispatch thread then the action is queued and invoked later by the
   * event-dispatch thread.
   * @param tableMapObj a Map object containing the items to display in
   * the list, or null for no items.
   */
  public void setListData(final Map tableMapObj)
  {
              //if this is the event-dispatch thread then do it now:
    if(SwingUtilities.isEventDispatchThread())
    {
      super.clearSelection();     //clear any current list selection
      sortedListModelObj.clear();
      if(tableMapObj != null)
        sortedListModelObj.putSortByValueAll(tableMapObj,sortDirFlag,sortType);
    }
    else
    {         //if this is not the event-dispatch thread then do it later:
      SwingUtilities.invokeLater(new Runnable()
          {
            public void run()
            {                          //clear any current list selection:
              SortedValuesJList.super.clearSelection();
              sortedListModelObj.clear();
              if(tableMapObj != null)
              {
                sortedListModelObj.putSortByValueAll(tableMapObj,
                                                             sortDirFlag,sortType);
              }
            }
          });
    }
  }

  /**
   * Called after updates to the list have occurred to repaint the list.
   * If the thread calling this method is not the event-dispatch thread
   * then the action is queued and invoked later by the event-dispatch
   * thread.
   */
  public void repaint()
  {
              //if this is the event-dispatch thread then do it now:
    if(SwingUtilities.isEventDispatchThread())
      super.repaint();
    else
    {         //if this is not the event-dispatch thread then do it later:
      SwingUtilities.invokeLater(new Runnable()
          {
            public void run()
            {
              SortedValuesJList.super.repaint();
            }
          });
    }
  }

  /**
   * Requests that this list have the keyboard focus.  If the thread
   * calling this method is not the event-dispatch thread then the
   * action is queued and invoked later by the event-dispatch thread.
   */
  public void requestFocus()
  {
              //if this is the event-dispatch thread then do it now:
    if(SwingUtilities.isEventDispatchThread())
      super.requestFocus();
    else
    {         //if this is not the event-dispatch thread then do it later:
      SwingUtilities.invokeLater(new Runnable()
          {
            public void run()
            {
              SortedValuesJList.super.requestFocus();
            }
          });
    }
  }

  /**
   * Selects the item corresponding to the given key.  If the thread
   * calling this method is not the event-dispatch thread then the
   * action is queued and invoked later by the event-dispatch thread.
   * @param keyObj the key object to use.
   * @param ensureVisibleFlag if true then the selected item will be made
   * visible.
   */
  public void setSelectedKey(final Object keyObj,
                                            final boolean ensureVisibleFlag)
  {
              //if this is the event-dispatch thread then do it now:
    if(SwingUtilities.isEventDispatchThread())
    {
      final int indexVal;
      if((indexVal=sortedListModelObj.indexOfKey(keyObj)) >= 0)
        doSetSelectedIndex(indexVal,ensureVisibleFlag);
    }
    else
    {         //if this is not the event-dispatch thread then do it later:
      SwingUtilities.invokeLater(new Runnable()
          {
            public void run()
            {
              final int indexVal;
              if((indexVal=sortedListModelObj.indexOfKey(keyObj)) >= 0)
                doSetSelectedIndex(indexVal,ensureVisibleFlag);
            }
          });
    }
  }

  /**
   * Selects the item corresponding to the given key.  If the thread
   * calling this method is not the event-dispatch thread then the
   * action is queued and invoked later by the event-dispatch thread.
   * @param keyObj the key object to use.
   */
  public void setSelectedKey(final Object keyObj)
  {
    setSelectedKey(keyObj,false);
  }

  /**
   * Sets the selected index for the list.  This method does not throw an
   * exception if the index value is out of range.  If the thread calling
   * this method is not the event-dispatch thread then the action is
   * queued and invoked later by the event-dispatch thread.
   * @param val the index value.
   * @param ensureVisibleFlag if true then the selected item will be made
   * visible.
   */
  public void setSelectedIndex(final int val,
                                            final boolean ensureVisibleFlag)
  {
              //if this is the event-dispatch thread then do it now:
    if(SwingUtilities.isEventDispatchThread())
      doSetSelectedIndex(val,ensureVisibleFlag);
    else
    {         //if this is not the event-dispatch thread then do it later:
      SwingUtilities.invokeLater(new Runnable()
          {
            public void run()
            {
              doSetSelectedIndex(val,ensureVisibleFlag);
            }
          });
    }
  }

  /**
   * Sets the selected index for the list.  This method does not throw an
   * exception if the index value is out of range.  If the thread calling
   * this method is not the event-dispatch thread then the action is
   * queued and invoked later by the event-dispatch thread.
   * @param val the index value.
   */
  public void setSelectedIndex(final int val)
  {
    setSelectedIndex(val,false);
  }

  /**
   * Sets the selected index for the list.  This method does not throw an
   * exception if the index value is out of range.
   * @param val the index value.
   * @param ensureVisibleFlag if true then the selected item will be made
   * visible.
   */
  protected void doSetSelectedIndex(int val,boolean ensureVisibleFlag)
  {
    super.setSelectedIndex(val);
    if(ensureVisibleFlag)
    {     //flag is set; ensure selected index is visible
      super.ensureIndexIsVisible(val);
      super.repaint();                  //repaint list
    }
  }

  /**
   * Sets the selected index for the list.  This method does not throw an
   * exception if an error occurs.  If the thread calling this method is
   * not the event-dispatch thread then the action is queued and invoked
   * later by the event-dispatch thread.  Setting the 'alwaysQueueFlag'
   * parameter to 'true' can help ensure that this action happens after
   * other changes to the list have completed.
   * @param ensureVisibleFlag if true then the selected item will be made
   * visible.
   * @param alwaysQueueFlag if true then the action will be put on the
   * queue of events to be performed later by the event-dispatch thread;
   * if false then this will only happen if the calling thread is not
   * the event-dispatch thread.
   */
  public void selectLastItemInList(final boolean ensureVisibleFlag,
                                                    boolean alwaysQueueFlag)
  {
              //if flag not set and this is the event-dispatch
              // thread then do it now:
    if(!alwaysQueueFlag && SwingUtilities.isEventDispatchThread())
      doSelectLastItemInList(ensureVisibleFlag);
    else
    {         //if this is not the event-dispatch thread then do it later:
      SwingUtilities.invokeLater(new Runnable()
          {
            public void run()
            {
              doSelectLastItemInList(ensureVisibleFlag);
            }
          });
    }
  }

  /**
   * Sets the selected index for the list.  This method does not throw an
   * exception if an error occurs.  If the thread calling this method is
   * not the event-dispatch thread then the action is queued and invoked
   * later by the event-dispatch thread.
   * @param ensureVisibleFlag if true then the selected item will be made
   * visible.
   */
  public void selectLastItemInList(final boolean ensureVisibleFlag)
  {
    selectLastItemInList(ensureVisibleFlag,false);
  }

  /**
   * Sets the selected index for the list.  This method does not throw an
   * exception if an error occurs.
   * @param ensureVisibleFlag if true then the selected item will be made
   * visible.
   */
  protected void doSelectLastItemInList(boolean ensureVisibleFlag)
  {
    try
    {
      int idx;
      if((idx=sortedListModelObj.getSize()) > 0)      //get size of list
      {  //list not empty
        --idx;          //decrement by 1 for last item on list
        super.setSelectedIndex(idx);        //select last item
        if(ensureVisibleFlag)
        {     //flag is set; ensure selected index is visible
          super.ensureIndexIsVisible(idx);
          super.repaint();                  //repaint list
        }
      }
    }
    catch(Exception ex)
    {    //some kind exception error; just move on
    }    // (list-item selection is generally not that critical)
  }

  /**
   * Ensures that the current list selection is visible.  If the thread
   * calling this method is not the event-dispatch thread then the
   * action is queued and invoked later by the event-dispatch thread.
   */
  public void ensureSelectionVisible()
  {
              //if this is the event-dispatch thread then do it now:
    if(SwingUtilities.isEventDispatchThread())
    {
      final int selIdx;           //get index of current selection on list:
      if((selIdx=getSelectedIndex()) >= 0)
      {  //an item is selected
        super.ensureIndexIsVisible(selIdx);      //make sure it's visible
        super.repaint();                         //repaint list
      }
    }
    else
    {         //if this is not the event-dispatch thread then do it later:
      SwingUtilities.invokeLater(new Runnable()
          {
            public void run()
            {
              final int selIdx;   //get index of current selection on list:
              if((selIdx=getSelectedIndex()) >= 0)
              {  //an item is selected; make sure it's visible
                SortedValuesJList.super.ensureIndexIsVisible(selIdx);
                SortedValuesJList.super.repaint();         //repaint list
              }
            }
          });
    }
  }

  /**
   * Clears the selection.  If the thread calling this method is not
   * the event-dispatch thread then the action is queued and invoked
   * later by the event-dispatch thread.
   */
  public void clearSelection()
  {
              //if this is the event-dispatch thread then do it now:
    if(SwingUtilities.isEventDispatchThread())
      super.clearSelection();
    else
    {         //if this is not the event-dispatch thread then do it later:
      SwingUtilities.invokeLater(new Runnable()
          {
            public void run()
            {
              SortedValuesJList.super.clearSelection();
            }
          });
    }
  }

  /**
   * Returns the object associated with the given index on the list.
   * @param indexVal the index value to use.
   * @return The associated object, or null if none matched.
   */
  public Object getCellAt(int indexVal)
  {
    try
    {
      return sortedListModelObj.getElementAt(indexVal);
    }
    catch(Exception ex)
    {
      return null;
    }
  }

  /**
   * Returns the object associated with the cell closest to the given
   * location.
   * @param pointObj the coordinates to use, relative to the list object.
   * @return The object associated with the cell closest to the given
   * location, or null if none found.
   */
  public Object locationToValue(Point pointObj)
  {
    final int idx = super.locationToIndex(pointObj);  //loc to index
    return (idx >= 0) ? getCellAt(idx) : null;        //index to value
  }

  /**
   * Returns the key for the currently selected item.
   * @return The key object for the currently selected item, or null
   * if not available.
   */
  public Object getSelectedKeyObj()
  {
    synchronized(selectedKeySyncObj)
    {    //only allow access by one thread at a time
      return selectedKeyObject;
    }
  }

  /**
   * Returns the key string for the currently selected item.
   * @return The key string for the currently selected item, or null
   * if not available.
   */
  public String getSelectedKeyString()
  {
    synchronized(selectedKeySyncObj)
    {    //only allow access by one thread at a time
      return (selectedKeyObject instanceof String) ?
                                           (String)selectedKeyObject : null;
    }
  }

  /**
   * Determines whether or not the current list selection is visible.
   * @return true if the current list selection is visible; false if not
   * (or if no list item selected).
   */
  public boolean isSelectionVisible()
  {
    final int selIdx;
    return ((selIdx=getSelectedIndex()) >= 0 &&
       selIdx >= getFirstVisibleIndex() && selIdx <= getLastVisibleIndex());
  }

  /**
   * Sets whether or not 'ListDataListener' objects attached to the list
   * model are called when changes occur.
   * @param flgVal true to enable calling of 'ListDataListener' objects,
   * false to disable.
   */
  public void setListenersEnabled(boolean flgVal)
  {
    sortedListModelObj.setListenersEnabled(flgVal);
  }

  /**
   * Fires a contents-changed event to the list model.
   */
  public void fireContentsChanged()
  {
              //if this is the event-dispatch thread then do it now:
    if(SwingUtilities.isEventDispatchThread())
      sortedListModelObj.fireContentsChanged();
    else
    {         //if this is not the event-dispatch thread then do it later:
      SwingUtilities.invokeLater(new Runnable()
          {
            public void run()
            {
              sortedListModelObj.fireContentsChanged();
            }
          });
    }
  }

  /**
   * Sets the list colors.  If the thread calling this method is not
   * the event-dispatch thread (and 'directFlag'==false) then the
   * action is queued and invoked later by the event-dispatch thread.
   * @param foregroundColor the foreground color to set, or null to leave
   * the color unchanged.
   * @param backgroundColor the background color to set, or null to leave
   * the color unchanged.
   * @param selForegroundColor the selection foreground color to set, or
   * null to leave the color unchanged.
   * @param selBackgroundColor the selection background color to set, or
   * null to leave the color unchanged.
   * @param directFlag if true then the color setting is always done
   * immediately; if false and the thread calling this method is not
   * the event-dispatch thread then the action is queued and invoked
   * later by the event-dispatch thread.
   */
  public void setColors(final Color foregroundColor,
                 final Color backgroundColor,final Color selForegroundColor,
                          final Color selBackgroundColor,boolean directFlag)
  {
              //if flag or if event-dispatch thread then do it now:
    if(directFlag || SwingUtilities.isEventDispatchThread())
    {
      doSetColors(foregroundColor,backgroundColor,selForegroundColor,
                                                        selBackgroundColor);
    }
    else
    {         //otherwise then do it later:
      SwingUtilities.invokeLater(new Runnable()
          {
            public void run()
            {
              doSetColors(foregroundColor,backgroundColor,
                                     selForegroundColor,selBackgroundColor);
            }
          });
    }
  }

  /**
   * Sets the list colors.  If the thread calling this method is not
   * the event-dispatch thread then the action is queued and invoked
   * later by the event-dispatch thread.
   * @param foregroundColor the foreground color to set, or null to leave
   * the color unchanged.
   * @param backgroundColor the background color to set, or null to leave
   * the color unchanged.
   * @param selForegroundColor the selection foreground color to set, or
   * null to leave the color unchanged.
   * @param selBackgroundColor the selection background color to set, or
   * null to leave the color unchanged.
   */
  public void setColors(Color foregroundColor,
                             Color backgroundColor,Color selForegroundColor,
                                                   Color selBackgroundColor)
  {
    setColors(foregroundColor,backgroundColor,selForegroundColor,
                                                  selBackgroundColor,false);
  }

  /**
   * Sets the list colors.
   * @param foregroundColor the foreground color to set, or null to leave
   * the color unchanged.
   * @param backgroundColor the background color to set, or null to leave
   * the color unchanged.
   * @param selForegroundColor the selection foreground color to set, or
   * null to leave the color unchanged.
   * @param selBackgroundColor the selection background color to set, or
   * null to leave the color unchanged.
   */
  protected void doSetColors(Color foregroundColor,
                             Color backgroundColor,Color selForegroundColor,
                                                   Color selBackgroundColor)
  {
              //update colors on the quake list:
    if(foregroundColor != null)
      setForeground(foregroundColor);
    if(backgroundColor != null)
      setBackground(backgroundColor);
    if(selForegroundColor != null)
      setSelectionForeground(selForegroundColor);
    if(selBackgroundColor != null)
      setSelectionBackground(selBackgroundColor);
         //request repaint so that if another color shown on the
         // list is changed it will be updated immediatedly:
    repaint();
  }

  /**
   * Resynchronizes the selected list item by reselecting the
   * selected-key tracker object.  This needs to be done after
   * the list model has been modified.
   */
  protected void resyncSelectedItem()
  {
    setSelectedKey(getSelectedKeyObj());
  }

  /**
   * Processes a change in the selected list item.
   */
  protected void processListSelectionChange()
  {
    Object keyObj;
    try
    {
      final int selIdx;      //get selected index and key for index:
      if((selIdx=super.getSelectedIndex()) >= 0)
        keyObj = sortedListModelObj.keyAt(selIdx);
      else
        keyObj = null;       //if none selected then set key = null
    }
    catch(Exception ex)
    {         //some kind of exception error
      keyObj = null;         //indicate none selected
    }
    boolean changedFlag;
    synchronized(selectedKeySyncObj)
    {    //if changing then set selected-key tracker (and flag):
      if(changedFlag = (keyObj != selectedKeyObject))
        selectedKeyObject = keyObj;
    }
         //if key changed and call-back installed then
         // invoke call-back with updated key object:
    if(changedFlag && listSelKeyChangeCallBackObj != null)
      listSelKeyChangeCallBackObj.newSelectedKeyObj(keyObj);
  }

  /**
   * Sets the ListSelKeyChangeCallBack object whose call-back method
   * is invoked when the selected list item has changed (and its key
   * has changed).
   * @param cbObj the 'ListSelKeyChangeCallBack' object to be used,
   * or null to clear the call-back object.
   */
  public void setListSelKeyChangeCallBack(ListSelKeyChangeCallBack cbObj)
  {
    listSelKeyChangeCallBackObj = cbObj;
  }


  /**
   * Interface ListSelKeyChangeCallBack defines a call-back method that
   * is invoked when the selected list item has changed (and its key has
   * changed).
   */
  public static interface ListSelKeyChangeCallBack
  {
    /**
     * Call-back method that is invoked when the selected list item
     * has changed (and its key has changed).
     * @param keyObj the new key object for the selected list item.
     */
    public void newSelectedKeyObj(Object keyObj);
  }
}
