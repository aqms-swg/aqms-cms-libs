//SplashWindow.java:  Defines a splash window to be shown while a program
//                    is constructing and loading its GUI.
//
//  10/7/2003 -- [ET]  Initial version.
// 12/21/2004 -- [ET]  Renamed 'show()' method to 'showWindow()' to avoid
//                     deprecation warning.
//  7/13/2009 -- [KF]  Changed to allow disposing of the splash window owner by
//                     setting 'createOwnerFlag' to true.
//

package com.isti.util.gui;

import java.awt.*;
import javax.swing.*;
import javax.swing.border.*;

/**
 * Class SplashWindow defines a splash window to be shown while a program
 * is constructing and loading its GUI.  The size of the display screen
 * may also be queried via the static 'getDisplayScreenSize()' method.
 * A simple "Program loading..." splash window may be displayed by using
 * the no-parameter constructor and then calling 'show()' in the 'main()'
 * method of the program.  The 'close()' method should be called just
 * before the program GUI is made visible.  See the various constructors
 * for other possible variations on the window's contents and position.
 */
public class SplashWindow extends JWindow
{
  /**
   * Create the owner flag is true to create the splash window owner,
   * false otherwise. The use of the owner allows the owner to be disposed.
   */
  public static boolean createOwnerFlag = false;
  /** Default width increase for splash screen window (100). */
  public static final int DEF_INC_WIDTH = 100;
  /** Default height increase for splash screen window (25). */
  public static final int DEF_INC_HEIGHT = 25;
  /** Default text string for splash screen window ("Program loading..."). */
  public static final String DEF_TEXT_STR = "Program loading...";

  protected static Dimension screenSizeDimObj = null; //display screen size

  private final Frame owner;  // the owner or null if none
  private boolean disposedFlag = false;  // true if the owner has been disposed

  /**
   * Creates a splash window object.  The window is sized to fit
   * its contents, and then its size is increased by the given values.
   * @param compObj the component to be displayed in the window.
   * @param incWidth the amount to increase the width, in pixels.
   * @param incHeight the amount to increase the height, in pixels.
   */
  public SplashWindow(Component compObj,int incWidth,int incHeight)
  {
    this(createOwner());
    createComponentWindow(compObj,incWidth,incHeight);
  }

  /**
   * Creates a splash window object.  The window is sized to fit
   * its contents, and then its size is increased by the default values
   * 'DEF_INC_WIDTH' and 'DEF_INC_HEIGHT'.
   * @param compObj the component to be displayed in the window.
   */
  public SplashWindow(Component compObj)
  {
    this(compObj,DEF_INC_WIDTH,DEF_INC_HEIGHT);
  }

  /**
   * Creates a splash window object using the given text string and/or
   * icon object.  The window is sized to fit its contents, and then its
   * size is increased by the given values.
   * @param textStr the text to be displayed in the window, or null
   * or none.
   * @param iconObj the icon to be displayed in the window, or null
   * or none.
   * @param horizontalAlignment One of the following constants
   *           defined in <code>SwingConstants</code>:
   *           <code>LEFT</code>,
   *           <code>CENTER</code>,
   *           <code>RIGHT</code>,
   *           <code>LEADING</code> or
   *           <code>TRAILING</code>.
   * @param incWidth the amount to increase the width, in pixels.
   * @param incHeight the amount to increase the height, in pixels.
   */
  public SplashWindow(String textStr,Icon iconObj,int horizontalAlignment,
                                                 int incWidth,int incHeight)
  {
    this(createOwner());
    createLabelWindow(textStr,iconObj,horizontalAlignment,
                                                        incWidth,incHeight);
  }

  /**
   * Creates a splash window object using the given text string and/or
   * icon object.  The window is sized to fit its contents, and then
   * its size is increased by the default values 'DEF_INC_WIDTH' and
   * 'DEF_INC_HEIGHT'.
   * @param textStr the text to be displayed in the window, or null
   * or none.
   * @param iconObj the icon to be displayed in the window, or null
   * or none.
   * @param horizontalAlignment One of the following constants
   *           defined in <code>SwingConstants</code>:
   *           <code>LEFT</code>,
   *           <code>CENTER</code>,
   *           <code>RIGHT</code>,
   *           <code>LEADING</code> or
   *           <code>TRAILING</code>.
   */
  public SplashWindow(String textStr,Icon iconObj,int horizontalAlignment)
  {
    this(textStr,iconObj,horizontalAlignment,
                                              DEF_INC_WIDTH,DEF_INC_HEIGHT);
  }

  /**
   * Creates a splash window object using the given text string and/or
   * icon object.  The window is sized to fit its contents, and then
   * its size is increased by the default values 'DEF_INC_WIDTH' and
   * 'DEF_INC_HEIGHT'.  The text alignment in the window will be 'CENTER'.
   * @param textStr the text to be displayed in the window, or null
   * or none.
   * @param iconObj the icon to be displayed in the window, or null
   * or none.
   */
  public SplashWindow(String textStr,Icon iconObj)
  {
    this(textStr,iconObj,JLabel.CENTER,
                                              DEF_INC_WIDTH,DEF_INC_HEIGHT);
  }

  /**
   * Creates a splash window object using the given text string and/or
   * icon object.  The window is sized to fit its contents, and then its
   * size is increased by the given values.
   * @param textStr the text to be displayed in the window, or null
   * or none.
   * @param iconURLStr URL or filename for the icon to be displayed in
   * the window, or null or none.
   * @param horizontalAlignment One of the following constants
   *           defined in <code>SwingConstants</code>:
   *           <code>LEFT</code>,
   *           <code>CENTER</code>,
   *           <code>RIGHT</code>,
   *           <code>LEADING</code> or
   *           <code>TRAILING</code>.
   * @param incWidth the amount to increase the width, in pixels.
   * @param incHeight the amount to increase the height, in pixels.
   * @param textBackupFlag if true then only show the text string if the
   * icon URL could not be loaded; if false then always show the text
   * string.
   */
  public SplashWindow(String textStr,String iconURLStr,
                         int horizontalAlignment,int incWidth,int incHeight,
                                                     boolean textBackupFlag)
  {
    this(createOwner());
    createLabelWindow(textStr,iconURLStr,horizontalAlignment,
                                         incWidth,incHeight,textBackupFlag);
  }

  /**
   * Creates a splash window object using the given text string and/or
   * icon object.  The window is sized to fit its contents, and then
   * its size is increased by the default values 'DEF_INC_WIDTH' and
   * 'DEF_INC_HEIGHT'.
   * @param textStr the text to be displayed in the window, or null
   * or none.
   * @param iconURLStr URL or filename for the icon to be displayed in
   * the window, or null or none.
   * @param horizontalAlignment One of the following constants
   *           defined in <code>SwingConstants</code>:
   *           <code>LEFT</code>,
   *           <code>CENTER</code>,
   *           <code>RIGHT</code>,
   *           <code>LEADING</code> or
   *           <code>TRAILING</code>.
   * @param textBackupFlag if true then only show the text string if the
   * icon URL could not be loaded; if false then always show the text
   * string.
   */
  public SplashWindow(String textStr,String iconURLStr,
                             int horizontalAlignment,boolean textBackupFlag)
  {
    this(textStr,iconURLStr,horizontalAlignment,
                               DEF_INC_WIDTH,DEF_INC_HEIGHT,textBackupFlag);
  }

  /**
   * Creates a splash window object using the given text string and/or
   * icon object.  The window is sized to fit its contents, and then
   * its size is increased by the default values 'DEF_INC_WIDTH' and
   * 'DEF_INC_HEIGHT'.  The text alignment in the window will be 'CENTER'.
   * @param textStr the text to be displayed in the window, or null
   * or none.
   * @param iconURLStr URL or filename for the icon to be displayed in
   * the window, or null or none.
   * @param textBackupFlag if true then only show the text string if the
   * icon URL could not be loaded; if false then always show the text
   * string.
   */
  public SplashWindow(String textStr,String iconURLStr,
                                                     boolean textBackupFlag)
  {
    this(textStr,iconURLStr,JLabel.CENTER,
                               DEF_INC_WIDTH,DEF_INC_HEIGHT,textBackupFlag);
  }

  /**
   * Creates a splash window object using the given text string and/or
   * icon object.  The window is sized to fit its contents, and then its
   * size is increased by the given values.
   * @param textStr the text to be displayed in the window, or null
   * or none.
   * @param iconURLStr URL or filename for the icon to be displayed in
   * the window, or null or none.
   * @param horizontalAlignment One of the following constants
   *           defined in <code>SwingConstants</code>:
   *           <code>LEFT</code>,
   *           <code>CENTER</code>,
   *           <code>RIGHT</code>,
   *           <code>LEADING</code> or
   *           <code>TRAILING</code>.
   * @param incWidth the amount to increase the width, in pixels.
   * @param incHeight the amount to increase the height, in pixels.
   */
  public SplashWindow(String textStr,String iconURLStr,
                         int horizontalAlignment,int incWidth,int incHeight)
  {
    this(textStr,iconURLStr,horizontalAlignment,
                                                  incWidth,incHeight,false);
  }

  /**
   * Creates a splash window object using the given text string and/or
   * icon object.  The window is sized to fit its contents, and then
   * its size is increased by the default values 'DEF_INC_WIDTH' and
   * 'DEF_INC_HEIGHT'.
   * @param textStr the text to be displayed in the window, or null
   * or none.
   * @param iconURLStr URL or filename for the icon to be displayed in
   * the window, or null or none.
   * @param horizontalAlignment One of the following constants
   *           defined in <code>SwingConstants</code>:
   *           <code>LEFT</code>,
   *           <code>CENTER</code>,
   *           <code>RIGHT</code>,
   *           <code>LEADING</code> or
   *           <code>TRAILING</code>.
   */
  public SplashWindow(String textStr,String iconURLStr,
                                                    int horizontalAlignment)
  {
    this(textStr,iconURLStr,horizontalAlignment,
                                        DEF_INC_WIDTH,DEF_INC_HEIGHT,false);
  }

  /**
   * Creates a splash window object using the given text string and/or
   * icon object.  The window is sized to fit its contents, and then
   * its size is increased by the default values 'DEF_INC_WIDTH' and
   * 'DEF_INC_HEIGHT'.  The text alignment in the window will be 'CENTER'.
   * @param textStr the text to be displayed in the window, or null
   * or none.
   * @param iconURLStr URL or filename for the icon to be displayed in
   * the window, or null or none.
   */
  public SplashWindow(String textStr,String iconURLStr)
  {
    this(textStr,iconURLStr,JLabel.CENTER,
                                        DEF_INC_WIDTH,DEF_INC_HEIGHT,false);
  }

  /**
   * Creates a splash window object using the given text string.  The
   * window is sized to fit its contents, and then its size is increased
   * by the given values.
   * @param textStr the text to be displayed in the window, or null
   * or none.
   * @param horizontalAlignment One of the following constants
   *           defined in <code>SwingConstants</code>:
   *           <code>LEFT</code>,
   *           <code>CENTER</code>,
   *           <code>RIGHT</code>,
   *           <code>LEADING</code> or
   *           <code>TRAILING</code>.
   * @param incWidth the amount to increase the width, in pixels.
   * @param incHeight the amount to increase the height, in pixels.
   */
  public SplashWindow(String textStr,int horizontalAlignment,
                                                 int incWidth,int incHeight)
  {
    this(textStr,(Icon)null,horizontalAlignment,
                                                        incWidth,incHeight);
  }

  /**
   * Creates a splash window object using the given text string.  The
   * window is sized to fit its contents, and then its size is increased
   * by the default values 'DEF_INC_WIDTH' and 'DEF_INC_HEIGHT'.
   * @param textStr the text to be displayed in the window, or null
   * or none.
   * @param horizontalAlignment One of the following constants
   *           defined in <code>SwingConstants</code>:
   *           <code>LEFT</code>,
   *           <code>CENTER</code>,
   *           <code>RIGHT</code>,
   *           <code>LEADING</code> or
   *           <code>TRAILING</code>.
   */
  public SplashWindow(String textStr,int horizontalAlignment)
  {
    this(textStr,(Icon)null,horizontalAlignment,
                                              DEF_INC_WIDTH,DEF_INC_HEIGHT);
  }

  /**
   * Creates a splash window object using the given text string.  The
   * window is sized to fit its contents, and then its size is increased
   * by the default values 'DEF_INC_WIDTH' and 'DEF_INC_HEIGHT'.  The
   * text alignment in the window will be 'CENTER'.
   * @param textStr the text to be displayed in the window, or null
   * or none.
   */
  public SplashWindow(String textStr)
  {
    this(textStr,(Icon)null,JLabel.CENTER,
                                              DEF_INC_WIDTH,DEF_INC_HEIGHT);
  }

  /**
   * Creates a splash window object using the default text string (see
   * 'DEF_TEXT_STR').  The window is sized to fit its contents, and then
   * its size is increased by the default values 'DEF_INC_WIDTH' and
   * 'DEF_INC_HEIGHT'.  The text alignment in the window will be 'CENTER'.
   */
  public SplashWindow()
  {
    this(DEF_TEXT_STR,(Icon)null,JLabel.CENTER,
                                              DEF_INC_WIDTH,DEF_INC_HEIGHT);
  }

  /**
   * Creates the owner.
   * @return the owner.
   */
  protected static final Frame createOwner()
  {
    if (createOwnerFlag)
      return new JFrame();
    return null;
  }

  /**
   * Creates a splash window object using the specified frame.
   * @param owner the owner.
   */
  protected SplashWindow(Frame owner)
  {
    super(owner);
    this.owner = owner;
  }

  /**
   * Creates the splash window object.  The window is sized to fit
   * it contents, and then its size is increased by the given values.
   * @param compObj the component to be displayed in the window.
   * @param incWidth the amount to increase the width, in pixels.
   * @param incHeight the amount to increase the height, in pixels.
   */
  protected final void createComponentWindow(Component compObj,int incWidth,
                                                              int incHeight)
  {
    final Container paneObj = getContentPane();
    paneObj.setLayout(new BorderLayout());       //user Border layout
    paneObj.add(compObj,BorderLayout.CENTER);    //add component to window
    pack();                                      //size window to contents
    final Dimension dimObj = getPreferredSize();
                   //calculate new window size:
    int windowWidth = dimObj.width + incWidth;
    int windowHeight = dimObj.height + incHeight;
                   //calculate centered pos for window:
    final Dimension ssDimObj = getDisplayScreenSize();
    final int windowXPos = ssDimObj.width/2 - windowWidth/2;
    final int windowYPos = ssDimObj.height/2 - windowHeight/2;
                   //set size and center window on screen:
    setBounds(windowXPos,windowYPos,windowWidth,windowHeight);
  }

  /**
   * Creates the splash window object using the given text string and/or
   * icon object.  The window is sized to fit it contents, and then its
   * size is increased by the given values.
   * @param textStr the text to be displayed in the window, or null
   * or none.
   * @param iconObj the icon to be displayed in the window, or null
   * or none.
   * @param horizontalAlignment One of the following constants
   *           defined in <code>SwingConstants</code>:
   *           <code>LEFT</code>,
   *           <code>CENTER</code>,
   *           <code>RIGHT</code>,
   *           <code>LEADING</code> or
   *           <code>TRAILING</code>.
   * @param incWidth the amount to increase the width, in pixels.
   * @param incHeight the amount to increase the height, in pixels.
   */
  protected final void createLabelWindow(String textStr,Icon iconObj,
                         int horizontalAlignment,int incWidth,int incHeight)
  {
                   //create label for splash screen:
    final JLabel labelObj = new JLabel(textStr,iconObj,horizontalAlignment);
    final Font fontObj = labelObj.getFont();     //increase font size
    labelObj.setFont(fontObj.deriveFont(fontObj.getSize2D()+6.0f));
    labelObj.setForeground(Color.blue);     //set text color
    labelObj.setBackground(Color.white);    //set window background
    labelObj.setOpaque(true);               //set label opaque
                                            //setup border:
    labelObj.setBorder(BorderFactory.createBevelBorder(BevelBorder.RAISED,
                                           Color.lightGray,Color.darkGray));
    createComponentWindow(labelObj,incWidth,incHeight);
  }

  /**
   * Creates the splash window object using the given text string and/or
   * icon object.  The window is sized to fit it contents, and then its
   * size is increased by the given values.
   * @param textStr the text to be displayed in the window, or null
   * or none.
   * @param iconURLStr URL or filename for the icon to be displayed in
   * the window, or null or none.
   * @param horizontalAlignment One of the following constants
   *           defined in <code>SwingConstants</code>:
   *           <code>LEFT</code>,
   *           <code>CENTER</code>,
   *           <code>RIGHT</code>,
   *           <code>LEADING</code> or
   *           <code>TRAILING</code>.
   * @param incWidth the amount to increase the width, in pixels.
   * @param incHeight the amount to increase the height, in pixels.
   * @param textBackupFlag if true then only show the text string if the
   * icon URL could not be loaded; if false then always show the text
   * string.
   */
  protected final void createLabelWindow(String textStr,String iconURLStr,
                         int horizontalAlignment,int incWidth,int incHeight,
                                                     boolean textBackupFlag)
  {
    final ImageIcon iconObj = ImageIconLoader.load(iconURLStr);
    if(textBackupFlag && iconObj != null)
      textStr = null;
    createLabelWindow(textStr,iconObj,horizontalAlignment,
                                                        incWidth,incHeight);
  }

  /**
   * Sizes the splash screen window to the size of its contents and then
   * increases its size by the given values.
   * @param incWidth the amount to increase the width, in pixels.
   * @param incHeight the amount to increase the height, in pixels.
   * @param centerFlag true to center the window on the display screen
   * after it is resized.
   */
  public void pack(int incWidth,int incHeight,boolean centerFlag)
  {
    pack();        //size window to contents
    final int windowWidth,windowHeight;
    if(incWidth != 0 || incHeight != 0)
    {    //width or height is to be increased
      final Dimension dimObj = getPreferredSize();
                   //calculate new window size:
      windowWidth = dimObj.width + incWidth;
      windowHeight = dimObj.height + incHeight;
      if(!centerFlag)
      {  //window is not to be centered; just size size and return
        setSize(windowWidth,windowHeight);
        return;
      }
    }
    else
    {    //width or height is not to be increased
      if(!centerFlag)
        return;    //if window is not to be centered then just return
      final Dimension dimObj = getPreferredSize();
                   //fetch new window size:
      windowWidth = dimObj.width;
      windowHeight = dimObj.height;
    }
                                  //calculate centered pos for window:
    final Dimension ssDimObj = getDisplayScreenSize();
    final int windowXPos = ssDimObj.width/2 - windowWidth/2;
    final int windowYPos = ssDimObj.height/2 - windowHeight/2;
                                  //set size and center window on screen:
    setBounds(windowXPos,windowYPos,windowWidth,windowHeight);
  }

  /**
   * Sizes the splash screen window to the size of its contents.
   * @param centerFlag true to center the window on the display screen
   * after it is resized.
   */
  public void pack(boolean centerFlag)
  {
    pack(0,0,centerFlag);
  }

  /**
   * Displays the splash screen window.
   */
  public void showWindow()
  {
    super.setVisible(true);
  }

  /**
   * Hides and disposes the splash screen window.
   */
  public void close()
  {
    dispose();
  }

  /**
   * Hides and disposes the splash screen window.
   */
  public void dispose()
  {
    if (owner != null && !disposedFlag)
    {
      disposedFlag = true;
      owner.dispose();
    }
    else
    {
      super.dispose();
    }
  }

  /**
   * Returns a 'Dimension' object containing the size of the display
   * screen.  If an error occurs then a message is sent to 'stderr'
   * and a size of 800x600 is returned.
   * @return A 'Dimension' object containing the display screen size.
   */
  public static Dimension getDisplayScreenSize()
  {
    if(screenSizeDimObj == null)
    {    //screen size dimension object not yet setup
      try
      {            //save screen size in dimension object:
        screenSizeDimObj = Toolkit.getDefaultToolkit().getScreenSize();
      }
      catch(Exception ex)
      {            //some kind of error; log warning
        System.err.println("Error retrieving screen size:  " + ex);
        screenSizeDimObj = new Dimension(800,600);    //use default size
      }
    }
    return screenSizeDimObj;
  }
}
