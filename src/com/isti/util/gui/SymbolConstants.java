package com.isti.util.gui;

/**
 * Symbol constants
 */
public interface SymbolConstants
{
    /** Circle symbol select value. */
  public static final int CIRCLE_SYMBOL_OBJECT = 0;
    /** Rectangle symbol select value. */
  public static final int RECTANGLE_SYMBOL_OBJECT = 1;
    /** Triangle symbol select value. */
  public static final int TRIANGLE_SYMBOL_OBJECT = 2;
}
