//SymbolIcon.java:  Defines a icon that is a symbol and holds label text.
//
//   3/21/2012 -- [KF]  Initial version based on 'SymbolJLabel'.
//
//
//=====================================================================
// Copyright (C) 2012 Instrumental Software Technologies, Inc.
//
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions
// are met:
// 1. Redistributions of source code, or portions of this source code,
//    must retain the above copyright notice, this list of conditions
//    and the following disclaimer.
// 2. Redistributions in binary form must reproduce the above copyright
//    notice, this list of conditions and the following disclaimer in
//    the documentation and/or other materials provided with the
//    distribution.
// 3. All advertising materials mentioning features or use of this
//    software must display the following acknowledgment:
//    "This product includes software developed by Instrumental
//    Software Technologies, Inc. (http://www.isti.com)"
// 4. If the software is provided with, or as part of a commercial
//    product, or is used in other commercial software products the
//    customer must be informed that "This product includes software
//    developed by Instrumental Software Technologies, Inc.
//    (http://www.isti.com)"
// 5. The names "Instrumental Software Technologies, Inc." and "ISTI"
//    must not be used to endorse or promote products derived from
//    this software without prior written permission. For written
//    permission, please contact "info@isti.com".
// 6. Products derived from this software may not be called "ISTI"
//    nor may "ISTI" appear in their names without prior written
//    permission of Instrumental Software Technologies, Inc.
// 7. Redistributions of any form whatsoever must retain the following
//    acknowledgment:
//    "This product includes software developed by Instrumental
//    Software Technologies, Inc. (http://www.isti.com/)."
// THIS SOFTWARE IS PROVIDED BY INSTRUMENTAL SOFTWARE
// TECHNOLOGIES, INC. "AS IS" AND ANY EXPRESSED OR IMPLIED
// WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES
// OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
// DISCLAIMED.  IN NO EVENT SHALL INSTRUMENTAL SOFTWARE TECHNOLOGIES,
// INC. OR ITS CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
// INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
// (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
// SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
// HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
// STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
// ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED
// OF THE POSSIBILITY OF SUCH DAMAGE.
//=====================================================================
//  A current version of the software can be found at
//                http://www.isti.com
//  Bug reports and comments should be directed to
//  Instrumental Software Technologies, Inc. at info@isti.com
//=====================================================================
//

package com.isti.util.gui;

import java.awt.Color;
import java.awt.Component;
import java.awt.Font;
import java.awt.FontMetrics;
import java.awt.Graphics;
import java.awt.Polygon;

import javax.swing.Icon;

/**
 * Class SymbolIcon is a icon that is a symbol. The symbol may be a filled
 * circle, an empty circle, or a character; all with a given color.
 */
public class SymbolIcon implements Icon, SymbolConstants {
  // font for symbol characters, bold:
  protected static final Font symFontBold = new Font("Dialog", Font.BOLD, 12);

  // font for symbol characters, plain:
  protected static final Font symFontPlain = new Font("Dialog", Font.PLAIN, 12);

  private static int symbolHeight = 15; // symbol height in Graphics

  private static int symbolWidth = 7; // symbol width in Graphics

  private static boolean symSetupFlag = false; // true after above vars setup

  private String labelText = ""; // label text

  private Color colorObj = null; // symbol color

  private Color outlineColorObj = null; // symbol outline color

  private boolean fillFlag = false; // true = fill object symbol

  private String symbolString = null; // symbol character string

  // symbol object
  private int symbolObj = CIRCLE_SYMBOL_OBJECT;

  private boolean boldFlag = false; // true = bold symbol character

  /**
   * Clears any character symbol in use, making a circle symbol be used instead.
   */
  public void clearSymbolChar() {
    setSymbolChar((char) 0);
  }

  /**
   * Returns the icon's height.
   * @return an int specifying the fixed height of the icon.
   */
  public int getIconHeight() {
    return symbolHeight;
  }

  /**
   * Returns the icon's width.
   * @return an int specifying the fixed width of the icon.
   */
  public int getIconWidth() {
    return symbolWidth;
  }

  /**
   * Get the label text.
   * @return the label text.
   */
  public String getLabelText() {
    return labelText;
  }

  /**
   * Gets a flag indicating whether or not a character symbol is in use.
   * @return true if a character symbol is in use, false if a object symbol is
   *         in use.
   */
  public boolean isCharSymbol() {
    return (symbolString != null);
  }

  /**
   * Draw the icon at the specified location. Icon implementations may use the
   * Component argument to get properties useful for painting, e.g. the
   * foreground or background color.
   * @param c the component to which the icon is added.
   * @param g graphics object to use.
   * @param x the X coordinate of the icon's top-left corner.
   * @param y the Y coordinate of the icon's top-left corner.
   * @param fillFlag true if the circle is to be filled; false if not.
   */
  public void paintIcon(Component c, Graphics g, int x, int y) {
    if (colorObj == null) // if no color for symbol then
      return; // just return (no symbol displayed)

    // save the initial color and font
    final Color previousColor = g.getColor();
    final Font previousFont = g.getFont();

    paintSymbol(c, g, x, y, fillFlag); // paint the symbol

    // if drawing filled objects and outline color was specified and is
    // different
    if (fillFlag && outlineColorObj != null
        && !outlineColorObj.equals(colorObj)) {
      g.setColor(outlineColorObj); // set symbol color
      paintSymbol(c, g, x, y, false); // paint the symbol outline
    }

    // restore the color and font
    g.setColor(previousColor);
    g.setFont(previousFont);
  }

  /**
   * Paints the symbol.
   * @param g graphics object to use.
   * @param c the component to which the icon is added.
   * @param x the X coordinate of the icon's top-left corner.
   * @param y the Y coordinate of the icon's top-left corner.
   * @param fillFlag true if the circle is to be filled; false if not.
   */
  protected void paintSymbol(Component c, Graphics g, int x, int y,
      boolean fillFlag) {
    try // trap any exceptions (just in case of null pointer, etc)
    {
      setupSymbol(g);
      g.setColor(colorObj);
      if (symbolHeight > symbolWidth) {
        y += (symbolHeight - symbolWidth - 1) / 2;
      }
      final int width = symbolWidth;
      if (symbolString == null) { // draw object symbol
        if (fillFlag) { // draw filled object:
          switch (this.symbolObj) {
          case TRIANGLE_SYMBOL_OBJECT:
            g.fillPolygon(createTriangle(x, y, width));
            break;
          case RECTANGLE_SYMBOL_OBJECT:
            g.fillRect(x, y, width, width);
            break;
          default:
            g.fillOval(x, y, width, width);
            break;
          }
        } else { // draw object (not filled):
          switch (this.symbolObj) {
          case TRIANGLE_SYMBOL_OBJECT:
            g.drawPolygon(createTriangle(x, y, width));
            break;
          case RECTANGLE_SYMBOL_OBJECT:
            g.drawRect(x, y, width, width);
            break;
          default:
            g.drawOval(x, y, width, width);
            break;
          }
        }
      } else // draw character symbol
      { // calc baseline for symbol text:
        FontMetrics fm = g.getFontMetrics();
        if (fm != null) {
          y += fm.getHeight() - fm.getDescent();
        }
        if (boldFlag) { // show character with bold font
          g.setFont(symFontBold); // setup bold font
          // draw character; add 1 to x pos to line it up:
          g.drawString(symbolString, x + 1, y);
        } else { // show character with plain font
          g.setFont(symFontPlain); // setup plain font
          // draw character:
          g.drawString(symbolString, x, y);
        }
      }
    } catch (Exception ex) {
      ex.printStackTrace();
    } // ignore any exceptions
  }

  /**
   * Sets whether or not the the character symbol is displayed using a bold
   * font.
   * @param flgVal true for bold, false for normal font.
   */
  public void setBoldFlag(boolean flgVal) {
    boldFlag = flgVal;
  }

  /**
   * Sets the color for the symbol.
   * @param colorObj the color to use.
   * @return the color.
   */
  public Color setColorObj(Color colorObj) {
    if (colorObj != null && !colorObj.equals(this.colorObj)) { // color value is
                                                               // changing
      this.colorObj = colorObj; // set new color value
    }
    return this.colorObj;
  }

  /**
   * Sets the color for the symbol.
   * @param colorRGB the color RGB to use.
   * @return the color.
   */
  public Color setColorObj(int colorRGB) {
    return setColorObj(new Color(colorRGB));
  }

  /**
   * Sets whether or not the the object symbol is filled.
   * @param flgVal true if the object is to be filled; false if not.
   */
  public void setFillFlag(boolean flgVal) {
    if (flgVal != fillFlag) { // value is changing
      fillFlag = flgVal; // enter new value
    }
  }

  /**
   * Set the label text.
   * @param labelText the label text.
   */
  public void setLabelText(String labelText) {
    this.labelText = labelText;
  }

  /**
   * Sets the outline color to be used for filled symbols.
   * @param colorObj the color for the symbol outline, or null for no outline.
   */
  public void setOutlineColorObj(Color colorObj) {
    // if change in color value
    if (colorObj != outlineColorObj
        && (colorObj == null || !colorObj.equals(outlineColorObj))) {
      outlineColorObj = colorObj; // set new color value
    }
  }

  /**
   * Sets the character symbol to be used.
   * @param symbolChar the character symbol to use, or '(char)0' to have no
   *          character symbol (and use circle symbol instead).
   */
  public void setSymbolChar(char symbolChar) {
    final String newStr;
    // nonzero character given
    if (symbolChar != (char) 0) {
      // create string consisting of single symbol character:
      newStr = symbolCharToString(symbolChar);
      if (newStr.equals(symbolString))
        return; // if string unchanged then exit method
    } else { // zero character given; setup null string
      if (symbolString == null)
        return; // if string already null then exit method
      newStr = null;
    }
    symbolString = newStr; // enter new string value
    symbolObj = CIRCLE_SYMBOL_OBJECT;
  }

  /**
   * Sets the object symbol to be used.
   * @param symbolObj the object symbol to use.
   */
  public void setSymbolObject(int symbolObj) {
    if (symbolString != null) {
      symbolString = null; // clear symbol string
    } else {
      // exit if no change
      if (this.symbolObj == symbolObj)
        return;
    }
    this.symbolObj = symbolObj; // save the new symbol object
  }

  /**
   * Setup the symbol if needed.
   * @param g graphics object to use.
   */
  protected void setupSymbol(Graphics g) {
    if (!symSetupFlag) { // symbol height and width variables not yet setup
                         // get height of symbol font:
      symbolHeight = g.getFontMetrics(symFontBold).getHeight();
      // get width of symbol font (for 'O'):
      symbolWidth = g.getFontMetrics(symFontBold).charWidth('O') - 1;
      symSetupFlag = true; // indicate variables now setup
    }
  }

  /**
   * Converts the given symbol character to a displayable, single-character
   * string.
   * @param symbolChar the character to use.
   * @return A new String.
   */
  protected String symbolCharToString(char symbolChar) {
    if (symbolChar < ' ' || symbolChar > 127) // if not displayable then
      symbolChar = '?'; // change to question mark
    // create string consisting of single symbol character:
    return new String(new char[] { symbolChar });
  }

  /**
   * Return the label text.
   * @return the label text.
   */
  public String toString() {
    return labelText;
  }

  /**
   * Creates a triangle.
   * @param x the <i>x</i> coordinate of the triangle to be drawn.
   * @param y the <i>y</i> coordinate of the triangle to be drawn.
   * @param width the width of the triangle to be drawn.
   * @return the triangle.
   */
  protected static Polygon createTriangle(int x, int y, int width) {
    final int delta = width / 2;
    final int[] xpoints = { x, x + delta, x + width };
    final int[] ypoints = { y + width, y, y + width };
    final int npoints = 3;
    return new Polygon(xpoints, ypoints, npoints);
  }
}
