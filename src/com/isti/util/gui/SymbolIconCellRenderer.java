package com.isti.util.gui;

import java.awt.Color;
import java.awt.Component;

import javax.swing.DefaultListCellRenderer;
import javax.swing.Icon;
import javax.swing.JList;

public class SymbolIconCellRenderer extends DefaultListCellRenderer implements
    SymbolConstants {
  /** define spacer string to leave blank space where symbol will go */
  protected static final String DEFAULT_SPACER_STRING = "   ";

  /** The symbol icon. */
  private SymbolIcon icon;

  /** The spacer string. */
  private String spacerString = DEFAULT_SPACER_STRING;

  /**
   * Creates a renderer with a symbol icon.
   */
  public SymbolIconCellRenderer() {
    super.setIcon(icon = new SymbolIcon());
  }

  /**
   * Clears any character symbol in use, making a circle symbol be used instead.
   */
  public void clearSymbolChar() {
    icon.clearSymbolChar();
  }

  /**
   * Returns the icon's height.
   * @return an int specifying the fixed height of the icon.
   */
  public int getIconHeight() {
    return icon.getIconHeight();
  }

  /**
   * Returns the icon's width.
   * @return an int specifying the fixed width of the icon.
   */
  public int getIconWidth() {
    return icon.getIconWidth();
  }

  /**
   * Get the label text.
   * @return the label text.
   */
  public String getLabelText() {
    return icon.getLabelText();
  }

  /**
   * Get the label text for the value.
   * @param value the value.
   * @return the label text for the value.
   */
  protected String getLabelText(Object value) {
    return (value == null) ? "" : value.toString();
  }

  /**
   * Return a component that has been configured to display the specified value.
   * That component's paint method is then called to "render" the cell. If it is
   * necessary to compute the dimensions of a list because the list cells do not
   * have a fixed size, this method is called to generate a component on which
   * getPreferredSize can be invoked.
   * @param list The JList we're painting.
   * @param value The value returned by list.getModel().getElementAt(index).
   * @param index The cells index.
   * @param isSelected True if the specified cell was selected.
   * @param cellHasFocus True if the specified cell has the focus.
   * @return A component whose paint() method will render the specified value.
   */
  public Component getListCellRendererComponent(JList list, Object value,
      int index, boolean isSelected, boolean cellHasFocus) {
    final String labelText = getLabelText(value);
    updateLabelText(labelText);
    super.getListCellRendererComponent(list, getText(labelText), index,
        isSelected, cellHasFocus);
    return this;
  }

  /**
   * Gets the spacer string used to create space for the symbol.
   * @return the spacer string used to create space for the symbol.
   */
  public String getSpacerString() {
    return spacerString;
  }

  /**
   * Get the text for the specified label text.
   * @param labelText the label text.
   * @return the text.
   */
  protected String getText(String labelText) {
    return getSpacerString() + labelText;
  }

  /**
   * Gets a flag indicating whether or not a character symbol is in use.
   * @return true if a character symbol is in use, false if a object symbol is
   *         in use.
   */
  public boolean isCharSymbol() {
    return icon.isCharSymbol();
  }

  /**
   * Sets whether or not the the character symbol is displayed using a bold
   * font.
   * @param flgVal true for bold, false for normal font.
   */
  public void setBoldFlag(boolean flgVal) {
    icon.setBoldFlag(flgVal);
  }

  /**
   * Sets the color for the symbol.
   * @param colorObj the color to use.
   * @return the color.
   */
  public Color setColorObj(Color colorObj) {
    return icon.setColorObj(colorObj);
  }

  /**
   * Sets the color for the symbol.
   * @param colorRGB the color RGB to use.
   * @return the color.
   */
  public Color setColorObj(int colorRGB) {
    return icon.setColorObj(colorRGB);
  }

  /**
   * Sets whether or not the the object symbol is filled.
   * @param flgVal true if the object is to be filled; false if not.
   */
  public void setFillFlag(boolean flgVal) {
    icon.setFillFlag(flgVal);
  }

  /**
   * Set the icon.
   * @param icon the icon (ignored if not SymbolIcon.)
   */
  public void setIcon(Icon icon) {
    if (icon instanceof SymbolIcon) {
      setIcon((SymbolIcon) icon);
    }
  }

  /**
   * Set the icon.
   * @param icon the icon (ignored if null.)
   */
  public void setIcon(SymbolIcon icon) {
    if (icon != null) {
      this.icon = icon;
      super.setIcon(icon);
    }
  }

  /**
   * Set the label text.
   * @param labelText the label text.
   */
  public void setLabelText(String labelText) {
    updateLabelText(labelText);
    updateText();
  }

  /**
   * Sets the outline color to be used for filled symbols.
   * @param colorObj the color for the symbol outline, or null for no outline.
   */
  public void setOutlineColorObj(Color colorObj) {
    icon.setOutlineColorObj(colorObj);
  }

  /**
   * Sets the spacer string used to create space for the symbol.
   * @param str a string blanks for spacing.
   */
  public void setSpacerString(String str) {
    spacerString = (str != null) ? str : ""; // save spacer string
    updateText();
  }

  /**
   * Sets the character symbol to be used.
   * @param symbolChar the character symbol to use, or '(char)0' to have no
   *          character symbol (and use circle symbol instead).
   */
  public void setSymbolChar(char symbolChar) {
    icon.setSymbolChar(symbolChar);
  }

  /**
   * Sets the object symbol to be used.
   * @param symbolObj the object symbol to use.
   */
  public void setSymbolObject(int symbolObj) {
    icon.setSymbolObject(symbolObj);
  }

  /**
   * Return the label text.
   * @return the label text.
   */
  public String toString() {
    return icon.toString();
  }

  /**
   * Update the label text.
   * @param labelText the label text.
   */
  protected void updateLabelText(String labelText) {
    icon.setLabelText(labelText);
  }

  /**
   * Update the text.
   */
  protected void updateText() {
    setText(getText(icon.getLabelText())); // enter spacer + label text
  }
}
