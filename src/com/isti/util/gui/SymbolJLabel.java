//SymbolJLabel.java:  Defines a JLabel with a symbol to the left of its
//                    text.
//
//   7/12/2000 -- [ET]  Version 1.00 release.
//  10/17/2000 -- [ET]  Added constructors with no text; other minor
//                      improvements.
//  10/10/2002 -- [ET]  Added 'set' methods.
//  10/14/2002 -- [KF]  Added 'setSymbolSize' method to set the symbol size.
//  10/21/2002 -- [KF]  Added 'setSymbolObject' method to use symbols
//                      other than a circle.
//  11/01/2002 -- [KF]  Fix 'setColorObj' to change color if NOT equal.
//  12/02/2002 -- [KF]  Added 'setOutlineColorObj' method to allow outline.
//    3/4/2002 -- [ET]  Added 'setSymbolXOffset()' method and support.
//  10/08/2004 -- [KF]  Added 'TRIANGLE_SYMBOL_OBJECT'.
//
//
//=====================================================================
// Copyright (C) 2004 Instrumental Software Technologies, Inc.
//
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions
// are met:
// 1. Redistributions of source code, or portions of this source code,
//    must retain the above copyright notice, this list of conditions
//    and the following disclaimer.
// 2. Redistributions in binary form must reproduce the above copyright
//    notice, this list of conditions and the following disclaimer in
//    the documentation and/or other materials provided with the
//    distribution.
// 3. All advertising materials mentioning features or use of this
//    software must display the following acknowledgment:
//    "This product includes software developed by Instrumental
//    Software Technologies, Inc. (http://www.isti.com)"
// 4. If the software is provided with, or as part of a commercial
//    product, or is used in other commercial software products the
//    customer must be informed that "This product includes software
//    developed by Instrumental Software Technologies, Inc.
//    (http://www.isti.com)"
// 5. The names "Instrumental Software Technologies, Inc." and "ISTI"
//    must not be used to endorse or promote products derived from
//    this software without prior written permission. For written
//    permission, please contact "info@isti.com".
// 6. Products derived from this software may not be called "ISTI"
//    nor may "ISTI" appear in their names without prior written
//    permission of Instrumental Software Technologies, Inc.
// 7. Redistributions of any form whatsoever must retain the following
//    acknowledgment:
//    "This product includes software developed by Instrumental
//    Software Technologies, Inc. (http://www.isti.com/)."
// THIS SOFTWARE IS PROVIDED BY INSTRUMENTAL SOFTWARE
// TECHNOLOGIES, INC. "AS IS" AND ANY EXPRESSED OR IMPLIED
// WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES
// OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
// DISCLAIMED.  IN NO EVENT SHALL INSTRUMENTAL SOFTWARE TECHNOLOGIES,
// INC. OR ITS CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
// INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
// (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
// SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
// HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
// STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
// ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED
// OF THE POSSIBILITY OF SUCH DAMAGE.
//=====================================================================
//  A current version of the software can be found at
//                http://www.isti.com
//  Bug reports and comments should be directed to
//  Instrumental Software Technologies, Inc. at info@isti.com
//=====================================================================
//

package com.isti.util.gui;

import java.awt.*;
import javax.swing.*;

/**
 * Class SymbolJLabel is a JLabel with a symbol to the left of its text.
 * The symbol may be a filled circle, an empty circle, or a character; all
 * with a given color.
 */
public class SymbolJLabel extends JLabel implements SymbolConstants
{
    /** Default symbol size value. */
  public static int DEFAULT_SYMBOLSIZE = 0;

         //define spacer string to leave blank space where symbol will go:
  protected static final String DEFAULT_SPACER_STRING = "   ";
  protected static final int SYMBOL_XPOS = 1;      //X-position for symbol
                             //font for symbol characters, bold:
  protected static final Font symFontBold = new Font("Dialog",Font.BOLD,12);
                             //font for symbol characters, plain:
  protected static final Font symFontPlain = new Font("Dialog",Font.PLAIN,12);
  protected String spacerString = DEFAULT_SPACER_STRING;
  protected String labelText = "";          //label text (without spacer)
  protected Color colorObj;                 //symbol color
  protected Color outlineColorObj = null;   //symbol outline color
  protected boolean fillFlag = false;       //true = fill object symbol
  protected String symbolString = null;     //symbol character string
  //symbol object
  protected int     symbolObj = CIRCLE_SYMBOL_OBJECT;
  protected boolean boldFlag = false;       //true = bold symbol character
  protected static int symbolHeight = 17;   //symbol height in Graphics
  protected static int symbolWidth = 8;     //symbol width in Graphics
  protected static boolean symSetupFlag=false;  //true after above vars setup
  protected final Dimension originalDimension;
//  protected final int originalHorizontalAlignment;
//  protected final int originalVerticalAlignment;
  protected int symbolSize = DEFAULT_SYMBOLSIZE;
  protected int symbolXOffsetVal = 0;

  /**
   * Creates a label with a circle symbol.
   * @param labelText the text for the label.
   * @param colorObj the color for the symbol, or null for no symbol.
   * @param fillFlag true if the circle is to be filled; false if not.
   */
  public SymbolJLabel(String labelText,Color colorObj,boolean fillFlag)
  {
    super(DEFAULT_SPACER_STRING + labelText);    //enter spacer + label text
    this.labelText = (labelText != null) ? labelText : ""; //save label text
    this.colorObj = colorObj;          //save symbol color
    this.fillFlag = fillFlag;          //save fill flag
    this.originalDimension = this.getPreferredSize();
//    this.originalHorizontalAlignment = this.getHorizontalAlignment();
//    this.originalVerticalAlignment = this.getVerticalAlignment();
  }

  /**
   * Creates a label with a circle symbol and no text.
   * @param colorObj the color for the symbol, or null for no symbol.
   * @param fillFlag true if the circle is to be filled; false if not.
   */
  public SymbolJLabel(Color colorObj,boolean fillFlag)
  {
    this("",colorObj,fillFlag);
  }

  /**
   * Creates a label with a character symbol.
   * @param labelText the text for the label.
   * @param colorObj the color for the symbol, or null for no symbol.
   * @param symbolChar the character to be used.
   * @param boldFlag true if the symbol character is to be displayed
   * using a bold font; false if not.
   */
  public SymbolJLabel(String labelText,Color colorObj,char symbolChar,
                                                           boolean boldFlag)
  {
    super(DEFAULT_SPACER_STRING + labelText);    //enter spacer + label text
    this.labelText = (labelText != null) ? labelText : ""; //save label text
    this.colorObj = colorObj;          //save symbol color
                   //create string consisting of single symbol character:
    symbolString = symbolCharToString(symbolChar);
    this.boldFlag = boldFlag;          //save bold flag
    this.originalDimension = this.getPreferredSize();
//    this.originalHorizontalAlignment = this.getHorizontalAlignment();
//    this.originalVerticalAlignment = this.getVerticalAlignment();
  }

  /**
   * Creates a label with a character symbol and no text.
   * @param colorObj the color for the symbol, or null for no symbol.
   * @param symbolChar the character to be used.
   * @param boldFlag true if the symbol character is to be displayed
   * using a bold font; false if not.
   */
  public SymbolJLabel(Color colorObj,char symbolChar,boolean boldFlag)
  {
    this("",colorObj,symbolChar,boldFlag);
  }

  /**
   * Sets the text for the label.  The spacer string is prepended.
   * @param str the text string to use.
   */
  public void setLabelText(String str)
  {
    labelText = (str != null) ? str : "";        //save label text
    setText(spacerString + labelText);           //enter spacer + label text
  }

  /**
   * Gets the text for the label (without the spacer string).
   * @return The text string for the label.
   */
  public String getLabelText()
  {
    return labelText;
  }

  /**
   * Sets the size for the symbol.
   * @param symbolSize size of the symbol or DEFAULT_SYMBOLSIZE for default.
   */
  public void setSymbolSize(int symbolSize)
  {
    this.symbolSize = symbolSize;
    final Dimension d;

    if (symbolSize > DEFAULT_SYMBOLSIZE)
    {    //not default symbol size; use as component size
      d = new Dimension(symbolSize,symbolSize);
//      d = (Dimension)originalDimension.clone();
//      d.width = symbolSize;
//      d.height = symbolSize;
//      setHorizontalAlignment(JLabel.CENTER);
//      setVerticalAlignment(JLabel.CENTER);
    }
    else
    {    //default symbol size; use original component size
      d = originalDimension;
//      setHorizontalAlignment(originalHorizontalAlignment);
//      setVerticalAlignment(originalVerticalAlignment);
    }
    setMaximumSize(d);
    setMinimumSize(d);
    setPreferredSize(d);
    updateComponent();            //update the component
  }

  /**
   * Sets the spacer string used to create space for the symbol.
   * @param str a string blanks for spacing.
   */
  public void setSpacerString(String str)
  {
    spacerString = (str != null) ? str : "";     //save spacer string
    setText(spacerString + labelText);           //enter spacer + label text
  }

  /**
   * Gets the spacer string used to create space for the symbol.
   * @return the spacer string used to create space for the symbol.
   */
  public String getSpacerString()
  {
    return spacerString;
  }

  /**
   * Sets the color for the symbol.
   * @param colorObj the color to use.
   */
  public void setColorObj(Color colorObj)
  {
    if(colorObj != null && !colorObj.equals(this.colorObj))
    {    //color value is changing
      this.colorObj = colorObj;   //set new color value
      updateComponent();          //update the component
    }
  }

  /**
   * Sets the outline color to be used for filled symbols.
   * @param colorObj the color for the symbol outline, or null for no outline.
   */
  public void setOutlineColorObj(Color colorObj)
  {
    //if change in color value
    if(colorObj != outlineColorObj &&
       (colorObj == null || !colorObj.equals(outlineColorObj)))
    {
      outlineColorObj = colorObj;      //set new color value
      updateComponent();               //update the component
    }
  }

  /**
   * Sets whether or not the the object symbol is filled.
   * @param flgVal true if the object is to be filled; false if not.
   */
  public void setFillFlag(boolean flgVal)
  {
    if(flgVal != fillFlag)
    {    //value is changing
      fillFlag = flgVal;          //enter new value
      if(!isCharSymbol())
      {  //object symbol is in use
        updateComponent();        //update the component
      }
    }
  }

  /**
   * Sets the character symbol to be used.
   * @param symbolChar the character symbol to use, or '(char)0' to have
   * no character symbol (and use circle symbol instead).
   */
  public void setSymbolChar(char symbolChar)
  {
    final String newStr;
    if(symbolChar != (char)0)
    {    //nonzero character given
                   //create string consisting of single symbol character:
      newStr = symbolCharToString(symbolChar);
      if(newStr.equals(symbolString))
        return;         //if string unchanged then exit method
    }
    else
    {    //zero character given; setup null string
      if(symbolString == null)
        return;         //if string already null then exit method
      newStr = null;
    }
    symbolString = newStr;        //enter new string value
    symbolObj = CIRCLE_SYMBOL_OBJECT;
    updateComponent();            //update the component
  }

  /**
   * Sets the object symbol to be used.
   * @param symbolObj the object symbol to use.
   */
  public void setSymbolObject(int symbolObj)
  {
    if(symbolString != null)
    {
      symbolString = null;        //clear symbol string
    }
    else
    {
      //exit if no change
      if (this.symbolObj == symbolObj)
        return;
    }
    this.symbolObj = symbolObj;   //save the new symbol object
    updateComponent();            //update the component
  }

  /**
   * Updates this component by calling 'revalidate()' and then 'repaint()'.
   */
  protected void updateComponent()
  {
    revalidate();                 //setup to update this component
    repaint();
  }

  /**
   * Clears any character symbol in use, making a circle symbol be used
   * instead.
   */
  public void clearSymbolChar()
  {
    setSymbolChar((char)0);
    updateComponent();            //update the component
  }

  /**
   * Sets whether or not the the character symbol is displayed using a
   * bold font.
   * @param flgVal true for bold, false for normal font.
   */
  public void setBoldFlag(boolean flgVal)
  {
    if(flgVal != boldFlag)
    {    //value is changing
      boldFlag = flgVal;          //enter new value
      if(isCharSymbol())
      {  //character symbol is in use
        updateComponent();        //update the component
      }
    }
  }

  /**
   * Enters an X-axis offset for the symbol.  This is used to enter a
   * relative shift for the horizontal position of symbol.
   * @param offsetVal the X-axis offset value to used, in pixels.
   */
  public void setSymbolXOffset(int offsetVal)
  {
    symbolXOffsetVal = offsetVal;
    updateComponent();            //update the component
  }

  /**
   * Gets a flag indicating whether or not a character symbol is in use.
   * @return true if a character symbol is in use, false if a object symbol
   * is in use.
   */
  public boolean isCharSymbol()
  {
    return (symbolString != null);
  }

  /**
   * Converts the given symbol character to a displayable, single-character
   * string.
   * @param symbolChar the character to use.
   * @return A new String.
   */
  protected String symbolCharToString(char symbolChar)
  {
    if(symbolChar < ' ' || symbolChar > 127)   //if not displayable then
      symbolChar = '?';                        //change to question mark
                 //create string consisting of single symbol character:
    return new String(new char [] {symbolChar});
  }

  /**
   * Calls the parent's paint method and then draws the symbol to the
   * left of the text.
   * @param g graphics object to use.
   */
  public void paint(Graphics g)
  {
    super.paint(g);               //call parent's paint method
    if(colorObj == null)          //if no color for symbol then
      return;                     //just return (no symbol displayed)

    g.setColor(colorObj);       //set symbol color
    paintSymbol(g, fillFlag);   //paint the symbol

    //if drawing filled objects and outline color was specified and is different
    if (fillFlag && outlineColorObj != null && !outlineColorObj.equals(colorObj))
    {
      g.setColor(outlineColorObj);  //set symbol color
      paintSymbol(g, false);        //paint the symbol outline
    }
  }

  /**
   * Paints the symbol.
   * @param g graphics object to use.
   * @param fillFlag true if the circle is to be filled; false if not.
   */
  protected void paintSymbol(Graphics g, boolean fillFlag)
  {
    try       //trap any exceptions (just in case of null pointer, etc)
    {
      if(!symSetupFlag)
      {  //symbol height and width variables not yet setup
                                  //get height of symbol font:
        symbolHeight = g.getFontMetrics(symFontBold).getHeight();
                                  //get width of symbol font (for 'O'):
        symbolWidth = g.getFontMetrics(symFontBold).charWidth('O') - 1;
        symSetupFlag = true;      //indicate variables now setup
      }
      final int width;
      if (symbolSize > DEFAULT_SYMBOLSIZE)
      {
        width = symbolSize;
      }
      else
      {
        width = symbolWidth;
      }
      if(symbolString == null)
      {       //draw object symbol
        final int x;
        if (symbolSize > DEFAULT_SYMBOLSIZE)
          x = ((getWidth()/2-width)/2) + symbolXOffsetVal;
        else
          x = SYMBOL_XPOS + symbolXOffsetVal;
        final int y = (getHeight()-width)/2;
        if(fillFlag)
        {     //draw filled object:
          switch (this.symbolObj)
          {
            case TRIANGLE_SYMBOL_OBJECT:
              g.fillPolygon(createTriangle(x, y, width));
              break;
            case RECTANGLE_SYMBOL_OBJECT:
              g.fillRect(x,y,width,width);
              break;
            default:
              g.fillOval(x,y,width,width);
              break;
          }
        }
        else
        {     //draw object (not filled):
          switch (this.symbolObj)
          {
            case TRIANGLE_SYMBOL_OBJECT:
              g.drawPolygon(createTriangle(x, y, width));
              break;
            case RECTANGLE_SYMBOL_OBJECT:
              g.drawRect(x,y,width,width);
              break;
            default:
              g.drawOval(x,y,width,width);
              break;
          }
        }
      }
      else    //draw character symbol
      {            //calc baseline for symbol text:
        int txtBase = getHeight()/2 + symbolHeight/3;
        if(boldFlag)
        {     //show character with bold font
          g.setFont(symFontBold);       //setup bold font
                   //draw character; add 1 to x pos to line it up:
          g.drawString(symbolString,SYMBOL_XPOS+1,txtBase);
        }
        else
        {     //show character with plain font
          g.setFont(symFontPlain);      //setup plain font
                   //draw character:
          g.drawString(symbolString,SYMBOL_XPOS,txtBase);
        }
      }
    }
    catch(Exception ex) {}   //ignore any exceptions
  }

  /**
   * Creates a triangle.
   * @param         x   the <i>x</i> coordinate
   *                         of the triangle to be drawn.
   * @param         y   the <i>y</i> coordinate
   *                         of the triangle to be drawn.
   * @param         width   the width of the triangle to be drawn.
   * @return the triangle.
   */
  protected static Polygon createTriangle(int x, int y, int width)
  {
    final int delta = width/2;
    final int[] xpoints = {x, x + delta, x + width};
    final int[] ypoints = {y + width, y, y + width};
    final int npoints = 3;
    return new Polygon(xpoints, ypoints, npoints);
  }
}