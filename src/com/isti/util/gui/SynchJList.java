//SynchJList.java:  Extends JList to add extra thread synchronization.
//                  This class is deprecated.
//
//   2/3/2003 -- [ET]  Initial version.
// 10/15/2003 -- [ET]  Deprecated--see notes below.
//

package com.isti.util.gui;

import java.util.Vector;
import java.awt.Dimension;
import java.awt.Component;
import javax.swing.*;

/**
 * Class SynchJList extends JList to add extra thread synchronization.
 * @deprecated Having thread-synchronization blocks on Swing objects is
 * not recommended because if a thread holds the object's lock and then
 * calls a Swing method that requires the AWT-tree-lock a deadlock can
 * occur if the AWT event-dispatch thread holds the AWT-tree-lock and
 * requires the object's lock (for repaint, etc).  See 'SortedValuesJList'
 * for an example of how this should be done.
 */
public class SynchJList extends JList
{

  /**
   * Creates a synchronized JList.
   */
  public SynchJList()
  {
  }

  /**
   * Creates a synchronized JList that displays the elements in the
   * specified, non-null model.
   * @param dataModel   the data model for this list.
   * @exception IllegalArgumentException   if <code>dataModel</code>
   *						is <code>null</code>.
   */
  public SynchJList(ListModel dataModel)
  {
    super(dataModel);
  }

  /**
   * Creates a synchronized JList that displays the elements in the
   * specified array.
   * @param  listData  the array of Objects to be loaded into the data
   * model.
   */
  public SynchJList(Object[] listData)
  {
    super(listData);
  }

  /**
   * Creates a synchronized JList that displays the elements in the
   * specified <code>Vector</code>.
   * @param  listData  the <code>Vector</code> to be loaded into the data
   * model.
   */
  public SynchJList(Vector listData)
  {
    super(listData);
  }

  /**
   * Creates a synchronized JList.
   * @param cellRendererObj the cell renderer to use with the list.
   */
  public SynchJList(ListCellRenderer cellRendererObj)
  {
    setCellRenderer(cellRendererObj);
  }

  /**
   * Requests that this list has the keyboard focus.  The request is
   * delayed, which may help it succeed by making it happen after other
   * Swing business is performed.
   */
  public void requestFocus()
  {
    SwingUtilities.invokeLater(new Runnable()
    {
      //run this after other Swing business is done
      public void run()
      {
        synchronized(SynchJList.this)
        {
          SynchJList.super.requestFocus();
        }
      }
    });
  }

  /**
   * Called after updates to the list have occurred to repaint the list.
   */
  public void handleListUpdate()
  {
    SwingUtilities.invokeLater(new Runnable()
    {
      //run this after other Swing business is done
      public void run()
      {
        repaint();
      }
    });
  }

  /**
   * Sets the selected index for the list.  This method does not throw an
   * exception if the index value is out of range.  This method is thread
   * synchronized.
   * @param val the index value.
   */
  public void setSelectedIndex(int val)
  {
    try
    {
      super.setSelectedIndex(val);
    }
    catch(Exception ex)
    {
    }
  }

  /**
   * Sets the selected index for the list.  The action is delayed to
   * prevent possible Swing problems (race conditions?).  This method
   * does not throw an exception if the index value is out of range.
   * @param val the index value.
   */
  public void setSelectedIndexLater(final int val)
  {
    SwingUtilities.invokeLater(new Runnable()
    {
      //run this after other Swing business is done
      public void run()
      {
        synchronized(SynchJList.this)
        {
          try
          {
            SynchJList.super.setSelectedIndex(val);
          }
          catch(Exception ex)
          {
          }
        }
      }
    });
  }

  /**
   * Ensures that the current list selection is visible.  This method is
   * thread synchronized.
   */
  public synchronized void ensureSelectionVisible()
  {
    final int selIdx;        //get index of current selection on list:
    if((selIdx=getSelectedIndex()) >= 0)
    {    //item is selected
      ensureIndexIsVisible(selIdx);    //make sure it's visible
      repaint();                       //repaint list
    }
  }

  /**
   * Ensures that the current list selection is visible.  The action is
   * delayed to prevent possible Swing problems (race conditions?).
   */
  public void ensureSelectionVisibleLater()
  {
    SwingUtilities.invokeLater(new Runnable()
    {
      //run this after other Swing business is done
      public void run()
      {
        synchronized(SynchJList.this)
        {
          final int selIdx = getSelectedIndex();
          //if selection make sure it is visible
          if(selIdx >= 0)
            ensureIndexIsVisible(selIdx);
          repaint();
        }
      }
    });
  }

  /**
   * Constructs a ListModel from a Vector and then applies 'setModel' to it.
   * This method is thread synchronized.
   * @param vec a Vector containing the items to display in the list.
   */
  public synchronized void setListData(Vector vec)
  {
    super.setListData(vec);
  }

  /**
   * Constructs a ListModel from an array of objects and then applies
   * 'setModel' to it.  This method is thread synchronized.
   * @param arr an array of Objects containing the items to display in
   * the list.
   */
  public synchronized void setListData(Object [] arr)
  {
    super.setListData(arr);
  }

  /**
   * Clears the selection.  This method is thread synchronized.
   */
  public synchronized void clearSelection()
  {
    super.clearSelection();
  }

  /**
   * Returns the first selected index.  This method is thread synchronized.
   * @return The first selected index, or -1 if there is no selection.
   */
  public synchronized int getSelectedIndex()
  {
    return super.getSelectedIndex();
  }

  /**
   * Returns the object associated with the given index on the list.
   * @param indexVal the index value to use.
   * @return The associated object, or null if none matched.
   */
  public synchronized Object getCellAt(int indexVal)
  {
    try
    {
      return super.getModel().getElementAt(indexVal);
    }
    catch(Exception ex)
    {
      return null;
    }
  }

  /**
   * Overridden version of 'paint()' that is synchronized to the JList
   * model object.
   * @param graphicsObj the graphics context object to use.
   */
  public void paint(java.awt.Graphics graphicsObj)
  {
    synchronized(getTreeLock())
    {    //grab the thread lock for the AWT tree now so that we're not
         // waiting for it after we own the list-model thread lock:
      final ListModel modelObj;
      if((modelObj=getModel()) != null)
      {  //model object is OK
        synchronized(modelObj)
        {     //hold thread lock for list model
          super.paint(graphicsObj);
        }
      }
      else    //no model object; just call method
        super.paint(graphicsObj);
    }
  }

  /**
   * Overridden version of 'validate()' that is synchronized to the JList
   * model object.
   */
  public void validate()
  {
    final ListModel modelObj;
    if((modelObj=getModel()) != null)
    {    //model object is OK
      synchronized(modelObj)
      {  //hold thread lock for list model
        super.validate();
      }
    }
    else      //no model object; just call method
      super.validate();
  }

  /**
   * Overridden version of 'getPreferredSize()' that is synchronized to
   * the JList model object.
   * @return A dimension object holding the size.
   */
  public Dimension getPreferredSize()
  {
    final ListModel modelObj;
    if((modelObj=getModel()) != null)
    {    //model object is OK
      synchronized(modelObj)
      {  //hold thread lock for list model
        return super.getPreferredSize();
      }
    }
         //no model object; just call method
    return super.getPreferredSize();
  }

  /**
   * Overridden version of 'getMinimumSize()' that is synchronized to
   * the JList model object.
   * @return A dimension object holding the size.
   */
  public Dimension getMinimumSize()
  {
    final ListModel modelObj;
    if((modelObj=getModel()) != null)
    {    //model object is OK
      synchronized(modelObj)
      {  //hold thread lock for list model
        return super.getMinimumSize();
      }
    }
         //no model object; just call method
    return super.getMinimumSize();
  }

  /**
   * Overridden version of 'getMaximumSize()' that is synchronized to
   * the JList model object.
   * @return A dimension object holding the size.
   */
  public Dimension getMaximumSize()
  {
    final ListModel modelObj;
    if((modelObj=getModel()) != null)
    {    //model object is OK
      synchronized(modelObj)
      {  //hold thread lock for list model
        return super.getMaximumSize();
      }
    }
         //no model object; just call method
    return super.getMaximumSize();
  }

  /**
   * Overridden version of 'getComponent()' that is synchronized to
   * the JList model object.
   * @param indexVal the index of the component to get.
   * @return The corresponding component.
   */
  public Component getComponent(int indexVal)
  {
    final ListModel modelObj;
    if((modelObj=getModel()) != null)
    {    //model object is OK
      synchronized(modelObj)
      {  //hold thread lock for list model
        return super.getComponent(indexVal);
      }
    }
         //no model object; just call method
    return super.getComponent(indexVal);
  }
}
