//TabCloseIcon.java:  Defines an icon to be used to close tabs on a
//                    JTabbedPane.
//
//   7/12/2006 -- [KF]  Initial version.
//   7/19/2006 -- [KF]  Added 'setGroup()', 'getColor()' and 'setColor()'
//                      methods.
//   7/26/2006 -- [ET]  Modified via new 'checkAddListenerToComp()' method
//                      to not use 'getMouseListeners()' method to preserve
//                      Java 1.3 compatibility.
//   1/30/2007 -- [ET]  Modified 'paintIcon()' method to make it disable
//                      focus on 'JTabbedPane' object (to avoid close-icon
//                      causing in 'ArrayIndexOutOfBoundsException' under
//                      Java 1.5); modified 'closeTab()' method to perform
//                      tab-close via 'invokeLater()' (thought it didn't
//                      really fix anything).
//   2/16/2007 -- [ET]  Added work-around for Java bug where
//                      'ArrayIndexOutOfBoundsException' is thrown if
//                      last tab is removed while mouse-rollover effect
//                      is showing on tab (only effects JVMs that support
//                      mouse-rollover highlights on tabs, i.e., Java 1.5
//                      on WinXP with XP theme).
//  11/15/2007 -- [ET]  Added support for marking favorite tabs (that will
//                      not be closed when a "Close All..." popup-menu item
//                      is invoked).
//

package com.isti.util.gui;

import java.util.Set;
import java.util.HashSet;
import java.util.Iterator;
import java.awt.Color;
import java.awt.Component;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.Rectangle;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseAdapter;
import javax.swing.Icon;
import javax.swing.JPopupMenu;
import javax.swing.JMenuItem;
import javax.swing.JCheckBoxMenuItem;
import javax.swing.JTabbedPane;
import javax.swing.SwingUtilities;

/**
 * Class TabCloseIcon defines an icon to be used to close tabs on a
 * JTabbedPane.
 */
public class TabCloseIcon implements Icon, ActionListener
{
  private static final String CLOSE_MENU_TEXT = "Close \"";
  private static final String CLOSE_ALL_EXCEPT_MENU_TEXT =
      "Close all except \"";
  private static final String CLOSE_ALL_MENU_TEXT = "Close all";

  private static final String ClOSE_GROUP_MENU_PREFIX_TEXT = " ";
  private static final String ClOSE_GROUP_MENU_POST_TEXT = " tabs";

  private static final String FAVORITE_TAB_TEXT = "Favorite Tab";

  //the mouse listener for all tab close icons
  private static final TabCloseIconMouseListener mouseListener =
      new TabCloseIconMouseListener();

  private Color colorObj = null;
  private final Icon icon;
  private String group = null;
  private Set groupSet = null;
  private int width = 0;
  private int height = 0;
  private int dx = 0;
  private int dy = 0;
  private JTabbedPane tabbedPane = null;
  private transient Rectangle position = null;
  private boolean favoriteTabFlag = false;
  private static boolean showFavTabMenuItemFlag = false;

  /**
   * Creates a new instance of TabCloseIcon.
   */
  public TabCloseIcon()
  {
    this(null);
  }

  /**
   * Creates a new instance of TabCloseIcon.
   * @param icon the icon or null for the default image.
   */
  public TabCloseIcon(Icon icon)
  {
    this.icon = icon;
    if (icon != null)
    {
      height = icon.getIconHeight();
      width = icon.getIconWidth();
    }
  }

  /**
   * Invoked when a context-popup-menu item is selected.
   * @param evtObj associated action-event object.
   */
  public void actionPerformed(ActionEvent evtObj)
  {
    try
    {
      final Object sourceObj = evtObj.getSource();
      if (!(sourceObj instanceof JMenuItem) || tabbedPane == null)
        return;
      String sourceText = ((JMenuItem)sourceObj).getText();
      if (sourceText.startsWith(CLOSE_ALL_EXCEPT_MENU_TEXT))
      {
        Icon iconObj;
        final String tabNameStr =      //get "except"-tab name string
                   sourceText.substring(CLOSE_ALL_EXCEPT_MENU_TEXT.length(),
                                                   sourceText.length() - 1);
        final int tabCount = tabbedPane.getTabCount();
        for (int idx = tabCount - 1; idx >= 0; idx--)
        {  //for each displayed tab
          if ((iconObj=tabbedPane.getIconAt(idx)) instanceof TabCloseIcon &&
                                 !((TabCloseIcon)iconObj).isFavoriteTab() &&
                             !tabNameStr.equals(tabbedPane.getTitleAt(idx)))
          {  //tab icon is TabCloseIcon, not marked as a favorite tab,
             // and name does not match "except"-tab name
            closeTab(idx);
          }
        }
      }
      else if (sourceText.startsWith(CLOSE_ALL_MENU_TEXT))
      {
        if (sourceText.equals(CLOSE_ALL_MENU_TEXT))
        {
          Icon iconObj;
          final int tabCount = tabbedPane.getTabCount();
          for (int idx = tabCount - 1; idx >= 0; idx--)
          {  //for each displayed tab
            if ((iconObj=tabbedPane.getIconAt(idx)) instanceof
                   TabCloseIcon && !((TabCloseIcon)iconObj).isFavoriteTab())
            {  //tab icon is TabCloseIcon and not marked as a favorite tab
              closeTab(idx);
            }
          }
        }
        else
        {  //selected menu item is "Close All [group]"
          Icon iconObj;
          final String groupStr = sourceText.substring(
                                              CLOSE_ALL_MENU_TEXT.length() +
                                      ClOSE_GROUP_MENU_PREFIX_TEXT.length(),
                                                       sourceText.length() -
                                       ClOSE_GROUP_MENU_POST_TEXT.length());
          final int tabCount = tabbedPane.getTabCount();
          for (int idx = tabCount - 1; idx >= 0; idx--)
          {  //for each displayed tab
            if ((iconObj=tabbedPane.getIconAt(idx)) instanceof TabCloseIcon
                              && !((TabCloseIcon)iconObj).isFavoriteTab() &&
                             groupStr.equals(((TabCloseIcon)iconObj).group))
            {  //tab icon is TabCloseIcon, not marked as a favorite tab,
               // and group name matches target group name
              closeTab(idx);
            }
          }
        }
      }
      else if (sourceText.startsWith(CLOSE_MENU_TEXT))
      {
        final int tabCount = tabbedPane.getTabCount();
        for (int idx = tabCount - 1; idx >= 0; idx--)
        {  //for each displayed tab
          if (tabbedPane.getIconAt(idx) instanceof TabCloseIcon &&
              tabbedPane.getTitleAt(idx).equals(sourceText.substring(
                  CLOSE_MENU_TEXT.length(), sourceText.length() - 1)))
          {
            closeTab(idx);
            break;
          }
        }
      }
      else if (sourceText.equals(FAVORITE_TAB_TEXT))
      {  //"Favorite Tab" checkbox item selected
                   //set favorite-tab flag to new state:
        setFavoriteTabFlag(((JMenuItem)sourceObj).isSelected());
      }
    }
    catch (Exception ex)
    {
      System.err.println("Error processing tab-close menu item:  " + ex);
      ex.printStackTrace();
    }
  }

  /**
   * Draw the icon at the specified location.
   * @param c the component.
   * @param g the graphics.
   * @param x the x position.
   * @param y the y position.
   */
  public void paintIcon(Component c, Graphics g, int x, int y)
  {
    Color previousColor = g.getColor();  //save the initial color
    if (colorObj != null)  //set the color if provided
    {
      g.setColor(colorObj);
    }

    if (icon != null)
    {
      icon.paintIcon(c, g, x, y);
    }
    else
    {
      paintDefaultIcon(c, g, x, y);
    }

    g.setColor(previousColor);  //restore the color

    position = new Rectangle(x, y, getIconWidth(), getIconHeight());
    if (tabbedPane == null && c instanceof JTabbedPane)
    {
      tabbedPane = (JTabbedPane) c;
      try     //disable focus on JTabbedPane object (to prevent Java 1.5
      {       // 'ArrayIndexOutOfBoundsException' via close icon):
        FocusUtils.setComponentFocusDisabled(tabbedPane);
      }
      catch(Exception ex)
      {
      }
      //add the mouse listener to the tabbed pane if not already added
      mouseListener.checkAddListenerToComp(tabbedPane);
    }
  }

  /**
   * Gets the color.
   * @return the color or null for default.
   */
  public Color getColor()
  {
    return colorObj;
  }

  /**
   * Returns the icon's width.
   * @return an int specifying the fixed width of the icon.
   */
  public int getIconWidth()
  {
    return width;
  }

  /**
   * Returns the icon's height.
   * @return an int specifying the fixed height of the icon.
   */
  public int getIconHeight()
  {
    return height;
  }

  /**
   * Sets the color.
   * @param colorObj the color to use.
   */
  public void setColor(Color colorObj)
  {
    this.colorObj = colorObj;
  }

  /**
   * Sets the group text for the tab.
   * @param group the group text.
   * @param groupSet the group set.
   * @return the 'TabCloseIcon'.
   */
  public TabCloseIcon setGroup(String group, Set groupSet)
  {
    this.group = group;
    this.groupSet = groupSet;
    return this;
  }

  /**
   * Sets whether or not this tab is marked as a favorite.  Any tab marked
   * as a favorite will not be closed when a "Close All..." menu item is
   * invoked.
   * @param flgVal true to mark this tab as a favorite; false to not.
   */
  public void setFavoriteTabFlag(boolean flgVal)
  {
    favoriteTabFlag = flgVal;
  }

  /**
   * Determines whether or not this tab is marked as a favorite.  Any tab
   * marked as a favorite will not be closed when a "Close All..." menu
   * item is invoked.
   * @return true if this tab is marked as a favorite; false if not.
   */
  public boolean isFavoriteTab()
  {
    return favoriteTabFlag;
  }

  /**
   * Sets whether or not the specified tab is marked as a favorite.  Any
   * tab marked as a favorite will not be closed when a "Close All..."
   * menu item is invoked.
   * @param tPaneObj tabbed pane hosting the tab.
   * @param compObj component on tab.
   * @param flgVal true to mark tab as a favorite; false to not.
   */
  public static void setFavoriteTabFlagForTab(JTabbedPane tPaneObj,
                                          Component compObj, boolean flgVal)
  {
    final int idx;
    final Icon iconObj;      //find tab and get its 'TabCloseIcon' object:
    if((idx=tPaneObj.indexOfComponent(compObj)) >= 0 &&
                (iconObj=tPaneObj.getIconAt(idx)) instanceof TabCloseIcon)
    {  //matching tab with TabCloseIcon found; set favorite-tab flag
      ((TabCloseIcon)iconObj).setFavoriteTabFlag(flgVal);
    }
  }

  /**
   * Determines whether or not the specified tab is marked as a favorite.
   * Any tab marked as a favorite will not be closed when a "Close All..."
   * menu item is invoked.
   * @param tPaneObj tabbed pane hosting the tab.
   * @param compObj component on tab.
   * @return true if tab is marked as a favorite; false if not (or if
   * no matching tab was found).
   */
  public static boolean getFavoriteTabFlagForTab(JTabbedPane tPaneObj,
                                                          Component compObj)
  {
    final int idx;
    final Icon iconObj;      //find tab and get its 'TabCloseIcon' object:
    if((idx=tPaneObj.indexOfComponent(compObj)) >= 0 &&
                (iconObj=tPaneObj.getIconAt(idx)) instanceof TabCloseIcon)
    {  //matching tab with TabCloseIcon found; set favorite-tab flag
      return ((TabCloseIcon)iconObj).isFavoriteTab();
    }
    return false;
  }

  /**
   * Sets whether or not the "Favorite Tab" menu item will be shown on
   * context popup menus for 'TabCloseIcon' tabs.  This settings applies
   * to all 'TabCloseIcon' tabs.  The default value is 'false' (not shown).
   * @param flgVal true to show the "Favorite Tab" menu item on the
   * context popup menus; false to not show.
   */
  public static void setShowFavTabMenuItemFlag(boolean flgVal)
  {
    showFavTabMenuItemFlag = flgVal;
  }

  /**
   * Determines whether or not the "Favorite Tab" menu item will be shown
   * on context popup menus for 'TabCloseIcon' tabs.  This settings applies
   * to all 'TabCloseIcon' tabs.
   * @return true if the "Favorite Tab" menu item is shown; false if not.
   */
  public static boolean getShowFavTabMenuItemFlag()
  {
    return showFavTabMenuItemFlag;
  }

  /**
   * Close the tab by removing it from the tabbed pane.
   * @param idx the tab index.
   */
  protected void closeTab(final int idx)
  {
    if (tabbedPane == null)
      return;

    try  //if last tab being removed then temporarily add a tab to work
    {    // around Java bug where 'ArrayIndexOutOfBoundsException' is
         // thrown if last tab is removed while mouse-rollover effect is
         // showing on tab (only effects JVMs that support mouse-rollover
         // highlights on tabs; i.e., Java 1.5 on WinXP with XP theme):
      final Component tempCompObj;
      if(idx >= tabbedPane.getTabCount() - 1)
      {  //last tab is being removed
        tempCompObj = new javax.swing.JPanel();
        tabbedPane.addTab("",tempCompObj);
      }
      else    //not last tab being removed
        tempCompObj = null;

      final Component tabComp = tabbedPane.getComponentAt(idx);
      if(tabComp instanceof CloseTab)
      {  //component implements 'CloseTab'; call close method
        ((CloseTab)tabComp).closeTab();
      }
      else if(tabbedPane.getIconAt(idx) instanceof TabCloseIcon)
      {  //component does not implement 'CloseTab'; remove it directly
        tabbedPane.removeTabAt(idx);
      }

      if(tempCompObj != null)
      {  //temporary tab was added; remove it (via 'invokeLater()'
         // to allow remove and validate actions to complete first):
        SwingUtilities.invokeLater(new Runnable()
            {
              public void run()
              {
                try
                {
                  tabbedPane.remove(tempCompObj);
                }
                catch (Exception ex)
                {         //some kind of exception; ignore and move on
                }
              }
            });
      }
    }
    catch (Exception ex)
    {         //some kind of exception; ignore and move on
    }
  }

  /**
   * Determines if the event happened on the icon.
   * @param e the MouseEvent.
   * @return true if the event happened on the icon, false otherwise.
   */
  protected boolean contains(MouseEvent e)
  {
    return position != null && position.contains(e.getX(), e.getY());
  }

  /**
   * Draw the default icon at the specified location.
   * @param c the component.
   * @param g the graphics.
   * @param x the x position.
   * @param y the y position.
   */
  protected void paintDefaultIcon(Component c, Graphics g, int x, int y)
  {
    if (height == 0)
    {
      Font f = c.getFont();
      if (f == null)
        f = new Font("Dialog", Font.BOLD, 12);
      //get width of symbol font (for 'X'):
      width = g.getFontMetrics(f).charWidth('X');
      height = width;
      dx = (width + 2) / 4;
      dy = dx;
    }
    if (!Color.black.equals(g.getColor()))
    {
      g.fillOval(x - dx, y - dy, width + dx * 2, height + dy * 2);
      g.setColor(Color.black);
    }
    else
    {
      g.drawOval(x - dx, y - dy, width + dx * 2, height + dy * 2);
    }
    // top left -> bottom right
    g.drawLine(x, y, x + width, y + height);
    // bottom left -> top right
    g.drawLine(x, y + height, x + width, y);
  }


  /**
   * The close tab interface.
   * If the tab panel implements this interface then it will be used to
   * close the tab, otherwise the tab is removed from the tabbed pane.
   */
  public interface CloseTab
  {
    /**
     * Close the tab by removing it from the tabbed pane.
     */
    public void closeTab();
  }


  /**
   * The mouse listener for all tab close icons.
   */
  protected static class TabCloseIconMouseListener extends MouseAdapter
  {
    private JTabbedPane curTabbedPane = null;  //current tab panel
    private int curTabIdx = -1;  //current tab index
    private TabCloseIcon curTabCloseIcon = null;  //current icon
    private final HashSet hostComponentsSet = new HashSet();

    /**
     * Invoked when a mouse button has been pressed on a component.
     * @param e the MouseEvent.
     */
    public void mousePressed(MouseEvent e)
    {
      if (!getTabInfo(e))  //get the tab information
        return;
      maybeShowPopup(e);
    }

    /**
     * Invoked when a mouse button has been released on a component.
     * @param e the MouseEvent.
     */
    public void mouseReleased(MouseEvent e)
    {
      if (!getTabInfo(e))  //get the tab information
        return;
      maybeShowPopup(e);
      // if this event has not been consumed, left button was pressed, and
      //  the icon contains the event
      if (!e.isConsumed() &&
          SwingUtilities.isLeftMouseButton(e) && curTabCloseIcon.contains(e))
      {
        curTabCloseIcon.closeTab(curTabIdx); //close the tab
        e.consume(); //consume the event
      }
    }

    /**
     * Checks if this listener was previously added to the given component
     * and adds it if not.
     * @param compObj Component object to use.
     */
    public void checkAddListenerToComp(Component compObj)
    {
      if(!hostComponentsSet.contains(compObj))
      {  //this listener not previously added to given component
        hostComponentsSet.add(compObj);     //add to set
        compObj.addMouseListener(this);     //add this listener to component
      }
    }

    /**
     * Adds the menu item to the popup menu.
     * @param pop the popup menu.
     * @param mi the menu item.
     */
    protected void addMenuItem(JPopupMenu pop, JMenuItem mi)
    {
      mi.addActionListener(curTabCloseIcon);
      pop.add(mi);
    }

    /**
     * Adds the menu item to the popup menu.
     * @param pop the popup menu.
     * @param curTitle the current title.
     */
    protected void addMenuItems(JPopupMenu pop, String curTitle)
    {
      addMenuItem(pop, new JMenuItem(CLOSE_MENU_TEXT + curTitle));
      addMenuItem(pop, new JMenuItem(CLOSE_ALL_EXCEPT_MENU_TEXT + curTitle));
      addMenuItem(pop, new JMenuItem(CLOSE_ALL_MENU_TEXT));
      if (curTabCloseIcon.groupSet != null && curTabCloseIcon.groupSet.size() > 0)
      {
        pop.addSeparator();
        final Iterator it = curTabCloseIcon.groupSet.iterator();
        while (it.hasNext())
        {
          addMenuItem(pop, new JMenuItem(
              CLOSE_ALL_MENU_TEXT + ClOSE_GROUP_MENU_PREFIX_TEXT + it.next() +
              ClOSE_GROUP_MENU_POST_TEXT));
        }
      }
      if(showFavTabMenuItemFlag)
      {  //showing of "Favorite Tab" menu item is enabled
        pop.addSeparator();       //put separator before item
                   //create, set and add "Favorite Tab" checkbox item:
        addMenuItem(pop, new JCheckBoxMenuItem(FAVORITE_TAB_TEXT,
                                          curTabCloseIcon.isFavoriteTab()));
      }
    }

    /**
     * Get the tab information.
     * @param e the MouseEvent.
     * @return true if successful, false otherwise.
     */
    protected boolean getTabInfo(MouseEvent e)
    {
      curTabbedPane = null;
      curTabIdx = -1;
      curTabCloseIcon = null;

      final Object source = e.getSource();
      if (! (source instanceof JTabbedPane))
        return false;
      curTabbedPane = (JTabbedPane) source;
      curTabIdx = curTabbedPane.getSelectedIndex();
      if (curTabIdx < 0)
        return false;
      final Icon curIcon = curTabbedPane.getIconAt(curTabIdx);
      if (!(curIcon instanceof TabCloseIcon))
        return false;
      curTabCloseIcon = (TabCloseIcon)curIcon;
      return true;
    }

    /**
     * Show the popup if the event is a popup trigger.
     * The 'getCurrentInfo()' should be called prior to this.
     * @param e the MouseEvent.
     * @see getCurrentInfo
     */
    protected void maybeShowPopup(MouseEvent e)
    {
      if (e.isConsumed() || !e.isPopupTrigger())
        return;

      e.consume();
      final String curTitle = curTabbedPane.getTitleAt(curTabIdx) + "\"";
      JPopupMenu pop = new JPopupMenu();
      addMenuItems(pop, curTitle);
      pop.show(curTabbedPane, e.getX(), e.getY());
    }
  }
}
