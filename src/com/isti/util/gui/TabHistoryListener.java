//TabHistoryListener.java - Implementation of 'ChangeListener' that can
//                          be added to a 'JTabbedPane' object to allow
//                          better control over which tab is selected
//                          after a tab is removed.
//
//    1/14/2002 -- [ET]  Initial version.
//   10/23/2003 -- [ET]  Fixed to work under Java 1.4 (accounts for
//                       container-listener 'componentRemoved()' method
//                       called after JTabbedPane removal).
//
//
//=====================================================================
// Copyright (C) 2000 Instrumental Software Technologies, Inc.
//
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions
// are met:
// 1. Redistributions of source code, or portions of this source code,
//    must retain the above copyright notice, this list of conditions
//    and the following disclaimer.
// 2. Redistributions in binary form must reproduce the above copyright
//    notice, this list of conditions and the following disclaimer in
//    the documentation and/or other materials provided with the
//    distribution.
// 3. All advertising materials mentioning features or use of this
//    software must display the following acknowledgment:
//    "This product includes software developed by Instrumental
//    Software Technologies, Inc. (http://www.isti.com)"
// 4. If the software is provided with, or as part of a commercial
//    product, or is used in other commercial software products the
//    customer must be informed that "This product includes software
//    developed by Instrumental Software Technologies, Inc.
//    (http://www.isti.com)"
// 5. The names "Instrumental Software Technologies, Inc." and "ISTI"
//    must not be used to endorse or promote products derived from
//    this software without prior written permission. For written
//    permission, please contact "info@isti.com".
// 6. Products derived from this software may not be called "ISTI"
//    nor may "ISTI" appear in their names without prior written
//    permission of Instrumental Software Technologies, Inc.
// 7. Redistributions of any form whatsoever must retain the following
//    acknowledgment:
//    "This product includes software developed by Instrumental
//    Software Technologies, Inc. (http://www.isti.com/)."
// THIS SOFTWARE IS PROVIDED BY INSTRUMENTAL SOFTWARE
// TECHNOLOGIES, INC. "AS IS" AND ANY EXPRESSED OR IMPLIED
// WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES
// OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
// DISCLAIMED.  IN NO EVENT SHALL INSTRUMENTAL SOFTWARE TECHNOLOGIES,
// INC. OR ITS CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
// INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
// (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
// SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
// HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
// STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
// ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED
// OF THE POSSIBILITY OF SUCH DAMAGE.
//=====================================================================
//  A current version of the software can be found at
//                http://www.isti.com
//  Bug reports and comments should be directed to
//  Instrumental Software Technologies, Inc. at info@isti.com
//=====================================================================
//
//

package com.isti.util.gui;

import java.awt.Component;
import java.awt.event.ContainerAdapter;
import java.awt.event.ContainerEvent;
import java.util.Vector;

import javax.swing.JTabbedPane;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;

/**
 * Class TabHistoryListener is an implementation of 'ChangeListener' that
 * can be added to a 'JTabbedPane' object to allow better control over
 * which tab is selected after a tab is removed.  It can be configured to
 * have a "history" such that whenever a pane is removed the most recently
 * selected pane is then selected; or so that the first tab is always
 * selected.  In most cases any exceptions that might occur are trapped,
 * reported and discarded (as tab-selection is not considered to be
 * "mission-critical").
 */
public class TabHistoryListener implements ChangeListener
{
  private final JTabbedPane tabbedPaneObj;  //JTabbedPane object to monitor
  private final boolean firstTabFlag;       //true for "first-tab" mode
                   //Vector that mirrors tabs (in same order):
  private final Vector tabsMirrorVec = new Vector();
                   //Vector that tracks tabs in last-selection order:
  private final Vector selTrackingVec = new Vector();
                   //flag true while in 'ComponentRemoved()' method:
  private boolean inComponentRemovedFlag = false;
                   //index of last object moved by 'moveUpSelected()':
  private int lastMovedIdx = -1;

    /**
     * Constructs a 'TabHistoryListener' object.  A 'ContainerListener'
     * object is also created and added to the 'JTabbedPane' being
     * monitored.
     * @param tabbedPaneObj_ the 'JTabbedPane' object to be monitored.
     * @param firstTabFlag_ if true then after a tab is removed the first
     * tab is always selected.
     */
  public TabHistoryListener(JTabbedPane tabbedPaneObj_,boolean firstTabFlag_)
  {
    tabbedPaneObj = tabbedPaneObj_;    //set handle to JTabbedPane obj
    firstTabFlag = firstTabFlag_;      //save flag
    try       //if any exceptions then just show message and abort
    {
      if(!firstTabFlag)
      {  //first-tab not set
        Component compObj;
        final int numTabs = tabbedPaneObj.getTabCount();
        for(int i=0; i<numTabs; ++i)   //build initial Vector of objects
        {     //for each tab; fetch and add to Vectors
          compObj = tabbedPaneObj.getComponentAt(i);
          tabsMirrorVec.add(compObj);
          selTrackingVec.add(compObj);
        }
        moveUpSelected();    //make sure selected tab at beginning of Vector
      }
              //add container listener to handle add/remove tab events:
      tabbedPaneObj.addContainerListener(new ContainerAdapter()
          {
              //method called after tab added:
            public void componentAdded(ContainerEvent e)
            {
              if(!firstTabFlag)
              {    //first-tab flag not set
                synchronized(TabHistoryListener.this)
                {
                  try   //if any exceptions then just show message and abort
                  {
                    final Object obj = e.getChild();
                    tabsMirrorVec.add(obj);       //add to Vector
                    selTrackingVec.add(obj);      //add to Vector
                    moveUpSelected();       //make sure selected tab at beg
//                    System.out.println("DEBUG:  added:  " + e.getChild());
                  }
                  catch(Exception ex)
                  {
                    System.err.println("TabHistoryListener:  " + ex);
                    ex.printStackTrace(System.err);
                  }
                }
              }
            }
              //method called after tab removed:
            public void componentRemoved(ContainerEvent e)
            {
              synchronized(TabHistoryListener.this)
              {
                inComponentRemovedFlag = true;   //indicate inside method
                try     //if any exceptions then just show message and abort
                {
                  final Object removeTabObj;
                  int removeTabNum;
                  if((removeTabObj=e.getChild()) instanceof Component &&
                                        (removeTabNum=tabsMirrorVec.indexOf(
                                                        removeTabObj)) >= 0)
                  {     //removed object is a Component and its index is OK
                    if(!firstTabFlag)
                    {   //first-tab flag not set
                             //if the last displayed tab is being removed
                             // then a 'stageChanged()' event will have
                             // occurred that selected the second-to-last
                             // tab; if so then the effect of the latest
                             // call to 'moveUpSelected()' needs to be
                             // reversed:
                      final int tabCount,newVecSize;
                      if(lastMovedIdx > 0 && removeTabNum >=
                                        (tabCount=tabsMirrorVec.size())-1 &&
                                   (newVecSize=selTrackingVec.size()-1) > 0)
                      {      //element was moved by 'moveUpSelected()' and
                             // last displayed tab is being removed and
                             // sel-order Vector has at least two elements
                                       //get first element in Vector:
                        final Object firstObj = selTrackingVec.firstElement();
                        if(firstObj instanceof Component &&
                                          tabsMirrorVec.indexOf(firstObj) ==
                                                               tabCount - 2)
                        {    //first element is OK and 2nd-to-last tab
                          selTrackingVec.removeElementAt(0);  //remove 1st elem
                                  //get index of where it used to be;
                                  // if out of range then use end of Vector:
                          final int idx = (lastMovedIdx <= newVecSize) ?
                                                  lastMovedIdx : newVecSize;
                                  //insert element in previous position
                          selTrackingVec.add(idx,firstObj);
//                          System.out.println("DEBUG:  Un-moved obj (" +
//                                                   idx + "):  " + firstObj);
                          lastMovedIdx = -1;     //clear saved position
                        }
                      }
                             //remove tab object from mirror Vector:
                      tabsMirrorVec.remove(removeTabObj);
                             //remove tab object from sel-order Vector:
                      if(selTrackingVec.remove(removeTabObj))
                      {      //removed object from Vector OK
//                        System.out.println("DEBUG:  remove (" +
//                                    removeTabNum + "):  " + removeTabObj);
                        final Object obj;
                             //select first tab object in Vector:
                        if(selTrackingVec.size() > 0 && (obj=selTrackingVec.
                                       firstElement()) instanceof Component)
                        {    //obj removed & Component at beg of Vec OK
                                  //find index of new selected tab:
                          int tabNum;
                          if((tabNum=tabsMirrorVec.indexOf(obj)) >= 0)
                          {  //index of new selected tab OK; select it
                            tabbedPaneObj.setSelectedIndex(tabNum);
//                            System.out.println("DEBUG:  selected (" +
//                                                     tabNum + "):  " + obj);
                          }
                        }
                      }
                    }
                    else     //first-tab flag set; select first tab
                      tabbedPaneObj.setSelectedIndex(0);
                  }
                }
                catch(Exception ex)
                {
                  System.err.println("TabHistoryListener:  " + ex);
                  ex.printStackTrace(System.err);
                }
                inComponentRemovedFlag = false;    //indicate method complete
              }
            }
          }
      );
    }
    catch(Exception ex)
    {
      System.err.println("TabHistoryListener:  " + ex);
      ex.printStackTrace(System.err);
    }
  }

    /**
     * Called upon any changes to the selected index of the 'JTabbedPane'
     * object being monitored; overridden to track the currently selected
     * tab.
     * @param e change event
     */
  public void stateChanged(ChangeEvent e)
  {
    if(!firstTabFlag)
    {    //first-tab flag not set
      synchronized(this)
      {       //thread synchronize this method
        if(!inComponentRemovedFlag)
        {     //not currently inside the 'ComponentRemoved()' method
//          System.out.println("DEBUG:  Entered 'stateChanged()'");
          try      //if any exceptions then just show message and abort
          {
            moveUpSelected();     //put selected tab at beginning of Vector
          }
          catch(Exception ex)
          {
            System.err.println("TabHistoryListener:  " + ex);
            ex.printStackTrace(System.err);
          }
//          System.out.println("DEBUG:  Exited 'stateChanged()'");
        }
      }
    }
  }

    //Puts selected tab at beginning of 'selTrackingVec' Vector.
  private void moveUpSelected()
  {
    final Component selObj = tabbedPaneObj.getSelectedComponent();
    int pos;
    if((pos=selTrackingVec.indexOf(selObj)) > 0)
    {    //selected object is in Vector past first element
      lastMovedIdx = pos;              //save index
      selTrackingVec.remove(pos);      //remove object from Vector
      selTrackingVec.add(0,selObj);    //insert object at beginning of Vec
//      System.out.println("DEBUG:  New Vec top:  " + selObj);
    }
    else //selected object is not in Vector past first element
      lastMovedIdx = -1;     //indicate element not moved
  }
}
