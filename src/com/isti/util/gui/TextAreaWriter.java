//TextAreaWriter.java:  Extension of 'Writer' that sends its output to
//                      a 'JTextArea' object.
//
//    5/2/2002 -- [ET]  Initial version.
//   3/22/2005 -- [ET]  Modified to use event-dispatch thread for all
//                      access to the 'JTextArea' object.
//    4/6/2005 -- [ET]  Added support for text size limit.
//
//
//=====================================================================
// Copyright (C) 2005 Instrumental Software Technologies, Inc.
//
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions
// are met:
// 1. Redistributions of source code, or portions of this source code,
//    must retain the above copyright notice, this list of conditions
//    and the following disclaimer.
// 2. Redistributions in binary form must reproduce the above copyright
//    notice, this list of conditions and the following disclaimer in
//    the documentation and/or other materials provided with the
//    distribution.
// 3. All advertising materials mentioning features or use of this
//    software must display the following acknowledgment:
//    "This product includes software developed by Instrumental
//    Software Technologies, Inc. (http://www.isti.com)"
// 4. If the software is provided with, or as part of a commercial
//    product, or is used in other commercial software products the
//    customer must be informed that "This product includes software
//    developed by Instrumental Software Technologies, Inc.
//    (http://www.isti.com)"
// 5. The names "Instrumental Software Technologies, Inc." and "ISTI"
//    must not be used to endorse or promote products derived from
//    this software without prior written permission. For written
//    permission, please contact "info@isti.com".
// 6. Products derived from this software may not be called "ISTI"
//    nor may "ISTI" appear in their names without prior written
//    permission of Instrumental Software Technologies, Inc.
// 7. Redistributions of any form whatsoever must retain the following
//    acknowledgment:
//    "This product includes software developed by Instrumental
//    Software Technologies, Inc. (http://www.isti.com/)."
// THIS SOFTWARE IS PROVIDED BY INSTRUMENTAL SOFTWARE
// TECHNOLOGIES, INC. "AS IS" AND ANY EXPRESSED OR IMPLIED
// WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES
// OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
// DISCLAIMED.  IN NO EVENT SHALL INSTRUMENTAL SOFTWARE TECHNOLOGIES,
// INC. OR ITS CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
// INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
// (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
// SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
// HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
// STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
// ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED
// OF THE POSSIBILITY OF SUCH DAMAGE.
//=====================================================================
//  A current version of the software can be found at
//                http://www.isti.com
//  Bug reports and comments should be directed to
//  Instrumental Software Technologies, Inc. at info@isti.com
//=====================================================================
//

package com.isti.util.gui;

import java.io.Writer;
import java.io.IOException;
import java.awt.Cursor;
import java.awt.event.FocusEvent;
import javax.swing.JTextArea;
import javax.swing.SwingUtilities;
import javax.swing.text.Document;
import javax.swing.text.Caret;
import javax.swing.text.DefaultCaret;


/**
 * Class TextAreaWriter is an extension of 'Writer' that sends its
 * output to a 'JTextArea' object.
 */
public class TextAreaWriter extends Writer
{
    /** Default text size limit, in characters (8000000). */
  public static final int DEF_TEXTSIZE_LIMIT = 8000000;
    /** Number of characters to trim when over limit (8000). */
  public static final int DEF_TEXTTRIM_SIZE = 8000;
    /** Max # of chars to search for newline after trim (400). */
  public static final int DEF_MAXNL_SEARCH = 400;
  private final JTextArea textAreaObj;
  private int textSizeLimit = DEF_TEXTSIZE_LIMIT;
  private int textTrimSize = DEF_TEXTTRIM_SIZE;
  private int maxNewLineSearch = DEF_MAXNL_SEARCH;
  private boolean trimExcReportedFlag = false;

    /**
     * Creates an 'Writer' extension that sends its output to a
     * 'TextArea' object.  The 'setupTextAreaObj()' method is called
     * to setup the given text area object.
     * @param textAreaObj 'TextArea' object
     */
  public TextAreaWriter(JTextArea textAreaObj)
  {
    this(textAreaObj,true);
  }

    /**
     * Creates an 'Writer' extension that sends its output to a
     * 'TextArea' object.
     * @param textAreaObj 'TextArea' object
     * @param setupTextObjFlag if true then 'setupTextAreaObj()' is
     * called to setup the given text area object.
     */
  public TextAreaWriter(JTextArea textAreaObj, boolean setupTextObjFlag)
  {
    this.textAreaObj = textAreaObj;
    if(setupTextObjFlag)          //if flag set then
      setupTextAreaObj();         //setup text area object
  }

    /**
     * Sets up the text area object to be not-editable while still having
     * a visible caret and text cursor.  If the thread calling this method
     * is not the event-dispatch thread then the action is queued and
     * invoked later by the event-dispatch thread.
     */
  public void setupTextAreaObj()
  {
    if(SwingUtilities.isEventDispatchThread())
    {
      doSetupTextAreaObj();
    }
    else
    {         //if this is not the event-dispatch thread then do it later:
      SwingUtilities.invokeLater(new Runnable()
          {
            public void run()
            {
              doSetupTextAreaObj();
            }
          });
    }
  }

  /**
   * Sets up the text area object to be not-editable while still having
   * a visible caret and text cursor.
   */
  public void doSetupTextAreaObj()
  {
                   //create new caret object with overridden focus method:
    final DefaultCaret caretObj = new DefaultCaret()
        {     //new version of focus-gained method that sets the caret
              // visible even if parent-component is not enabled:
          public void focusGained(FocusEvent e)
          {
            if(getComponent() != null && getComponent().isEnabled())
            {
              setVisible(true);
              setSelectionVisible(true);
            }
          }
        };
                   //get current cursor-caret object:
    final Caret oldCaretObj = textAreaObj.getCaret();
    caretObj.setBlinkRate(   //set new caret up to blink like old one
                        (oldCaretObj!=null)?oldCaretObj.getBlinkRate():500);
    textAreaObj.setEditable(false);         //set text area not editable
                   //use text cursor (instead of point cursor):
    textAreaObj.setCursor(Cursor.getPredefinedCursor(Cursor.TEXT_CURSOR));
    textAreaObj.setCaret(caretObj);    //enter new caret object
  }

  /**
   * Sends a portion of an array of characters to the text area.
   * Non-printable values are ignored.  If the thread calling this
   * method is not the event-dispatch thread then the action is
   * queued and invoked later by the event-dispatch thread.
   * @param charsArr array of characters.
   * @param offsetVal offset from which to start writing characters.
   * @param numChars number of characters to write.
   * @throws IOException if an I/O error occurs.
   */
  public void write(char [] charsArr, final int offsetVal,
                                      final int numChars) throws IOException
  {
    if(SwingUtilities.isEventDispatchThread())
    {
      doWrite(charsArr,offsetVal,numChars);
    }
    else
    {    //this is not the event-dispatch thread; do it later
              //create a local copy of the byte array:
      final char [] cpCharsArr = new char[charsArr.length];
      System.arraycopy(charsArr,0,cpCharsArr,0,charsArr.length);
      SwingUtilities.invokeLater(new Runnable()
          {
            public void run()
            {
              try
              {
                doWrite(cpCharsArr,offsetVal,numChars);
              }
              catch(IOException ex)
              {
                System.err.println("Error in 'TextAreaWriter." +
                                          "write(char[],int,int)':  " + ex);
                ex.printStackTrace();
              }
            }
          });
    }
  }

  /**
   * Sends a portion of an array of characters to the text area.
   * Non-printable values are ignored.
   * @param charsArr array of characters.
   * @param offsetVal offset from which to start writing characters.
   * @param numChars number of characters to write.
   * @throws IOException if an I/O error occurs.
   */
  public void doWrite(char [] charsArr, int offsetVal, int numChars)
                                                          throws IOException
  {
    final int endVal;        //check parameter values
    if(offsetVal < 0 || offsetVal > charsArr.length || numChars < 0 ||
                (endVal=offsetVal+numChars) > charsArr.length || endVal < 0)
    {    //parameter out of range; throw exception
      throw new IndexOutOfBoundsException();
    }
    else if(numChars == 0)        //if no characters then
      return;                     //exit method
              //create buffer setup for max number of characters:
    final StringBuffer outBuff = new StringBuffer(numChars);
    char ch;
    for(int i=offsetVal; i<endVal; ++i)
    {    //for each character in array; check it for printability
      if(Character.isLetterOrDigit(ch=charsArr[i]) || ch == '\n' ||
                                     ch == '\t' || (ch >= ' ' && ch <= 126))
      {  //character is letter/digit, LF, tab or within "printable" range
        outBuff.append(ch);       //add character to buffer
      }
    }
              //send string version of output buffer to text area:
    textAreaObj.append(outBuff.toString());
         //enforce text size limit and position caret at end of text:
    doTextLimitAndPosCaret();
  }

  /**
   * Flushes the stream; does nothing in this implementation.
   */
  public void flush()
  {
  }

  /**
   * Closes the stream; does nothing in this implementation.
   */
  public void close()
  {
  }

  /**
   * Sets the text size limit values.  When the number of characters in
   * the text area is greater than 'textSizeLimit' then 'textTrimSize'
   * number of characters will be removed from the beginning of the
   * text area (repeatedly if needed).  After the trim, the characters
   * up to the next newline will also be removed (up to 'maxNewLineSearch'
   * number of characters).
   * @param textSizeLimit text size limit, in characters.
   * @param textTrimSize number of character to trim when over limit.
   * @param maxNewLineSearch maximum number of characters to search for
   * a newline after a trim, or 0 or no search.
   */
  public void setTextSizeLimitValues(int textSizeLimit, int textTrimSize,
                                                       int maxNewLineSearch)
  {
    this.textSizeLimit = textSizeLimit;
    this.textTrimSize = textTrimSize;
    this.maxNewLineSearch = maxNewLineSearch;
  }

  /**
   * Sets the text size limit values.  When the number of characters in
   * the text area is greater than 'textSizeLimit' then 'textTrimSize'
   * number of characters will be removed from the beginning of the
   * text area (repeatedly if needed).  After the trim, the characters
   * up to the next newline will also be removed (up to 'maxNewLineSearch'
   * number of characters).
   * @param textSizeLimit text size limit, in characters.
   * @param textTrimSize number of character to trim when over limit.
   */
  public void setTextSizeLimitValues(int textSizeLimit, int textTrimSize)
  {
    setTextSizeLimitValues(textSizeLimit,textTrimSize,0);
  }

  /**
   * Returns the text trim size.  When the number of characters in
   * the text area is greater than 'textSizeLimit' then 'textTrimSize'
   * number of characters will be removed from the beginning of the
   * text area (repeatedly if needed).
   * @return The text trim size.
   */
  public int getTextTrimSize()
  {
    return textTrimSize;
  }

  /**
   * Returns the text size limit.  When the number of characters in
   * the text area is greater than 'textSizeLimit' then 'textTrimSize'
   * number of characters will be removed from the beginning of the
   * text area (repeatedly if needed).
   * @return The text size limit.
   */
  public int getMaxNewLineSearch()
  {
    return maxNewLineSearch;
  }

  /**
   * Returns the maximum number of characters to search for a newline
   * after a trim.  When the number of characters in the text area is
   * greater than 'textSizeLimit' then 'textTrimSize' number of characters
   * will be removed from the beginning of the text area (repeatedly if
   * needed).  After the trim, the characters up to the next newline will
   * also be removed (up to 'maxNewLineSearch' number of characters).
   * @return The maximum number of characters to search for a newline
   * after a trim, or 0 or no search.
   */
  public int getTextSizeLimit()
  {
    return textSizeLimit;
  }

  /**
   * Enforces the text size limit positions the caret at the end of
   * the text.
   */
  private void doTextLimitAndPosCaret()
  {
    final Document docObj;
    if((docObj=textAreaObj.getDocument()) != null)
    {    //text-area document fetched OK
      int docLen = docObj.getLength();
      try
      {
        if(docLen > textSizeLimit && docLen > textTrimSize*10)
        {     //beyond size limit and much larger than trim size
          int oldDocLen;
          do
          {   //loop while beyond size limit and much larger than trim size
            docObj.remove(0,textTrimSize);
            oldDocLen = docLen;
          }                                 //(make sure length reduced)
          while((docLen=docObj.getLength()) > textSizeLimit &&
                            docLen > textTrimSize*10 && docLen < oldDocLen);
              //attempt to remove characters up to beginning of next line:
          if(maxNewLineSearch > 0)
          {   //max # of chars to search for newline > 0; get chars
            final String checkStr = docObj.getText(0,maxNewLineSearch);
            final int pos;
            if((pos=checkStr.indexOf('\n')) >= 0)
              docObj.remove(0,pos+1);  //if NL found then remove it & before
          }
        }
      }
      catch(Exception ex)
      {  //some kind of exception error; show error message (once)
        if(!trimExcReportedFlag)
        {     //exception error not previously reported
          trimExcReportedFlag = true;       //indicate reported
          System.err.println("Error enforcing text size limit:");
          ex.printStackTrace();
        }
      }
              //put caret at end of data:
      textAreaObj.setCaretPosition(docObj.getLength());
    }
  }
}
