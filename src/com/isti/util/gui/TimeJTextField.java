//TimeJTextField.java:  Extension of 'JTextField' which only allows
//                      time characters to be entered into it.
//
//   8/13/2003 -- [KF]  Initial release version.
//

package com.isti.util.gui;

import javax.swing.*;
import javax.swing.text.*;
import java.text.SimpleDateFormat;
import java.util.*;
import java.awt.Font;
import javax.swing.event.*;
import java.awt.event.FocusListener;
import java.awt.event.FocusEvent;

/**
 * Class TimeJTextField is an extension of 'JTextField' which only
 * allows time characters to be entered into it.
 */
public class TimeJTextField extends JTextField
{
  /**
   * Entry for hours only.
   */
  public final static int HOURS = 2;

  /**
   * Entry for hours and minutes.
   */
  public final static int HOURS_MINS = 5;

  /**
   * Entry for hours, minutes and seconds (default.)
   */
  public final static int HOURS_MINS_SECS = 8;

  /**
   * Default value for the show zero text flag.
   */
  public final static boolean SHOW_ZERO_TEXT_FLAG_DEFAULT = true;

  private final int maxNumChars;           //maximum number of chars allowed

  protected final static String FIELD_SEPARATOR = ":";
  protected final static String MS_FIELD_SEPARATOR = ".";
  protected final static String TIME_PATTERN = "HH:mm:ss.SSS";
  protected final static String EMPTY_TIME_TEXT = "  :  :  .   ";

  //calendar time fields
  protected final static int[] CALENDAR_TIME_FIELDS =
  {
    Calendar.HOUR_OF_DAY,
    Calendar.MINUTE,
    Calendar.SECOND,
    Calendar.MILLISECOND,
  };
  protected final int MAX_CALENDAR_TIME_FIELD;

  //date format object for creating time strings:
  public final SimpleDateFormat timeFormatterObj;

  protected static boolean debugFlag = false;

  private int caretPos = -1;  //caret position

  private final boolean showZeroTextFlag;  //true to show zeros instead of spaces

  private Calendar calObj;  //calendar object
  private Calendar workingCalObj;  //working calendar object
  private Date minDateObj;  //minumum date object
  private Date maxDateObj;  //maximum date object

  /**
   * Creates a new 'TimeJTextField' object.
   * The default is entry for hours, minutes, and seconds ("HH:mm:ss".)
   */
  public TimeJTextField()
  {
    this(HOURS_MINS_SECS);
  }

  /**
   * Creates a new 'TimeJTextField' object.
   * @param maxNumChars the maximum number of characters.
   * This value should be HOURS for hours only ("HH"),
   * HOURS_MINS for hours and minutes ("HH:mm"),
   * or HOURS_MINS_SECS for hours, minutes, and seconds ("HH:mm:ss").
   */
  public TimeJTextField(int maxNumChars)
  {
    this(maxNumChars, SHOW_ZERO_TEXT_FLAG_DEFAULT);
  }

  /**
   * Creates a new 'TimeJTextField' object.
   * @param maxNumChars the maximum number of characters.
   * This value should be HOURS for hours only ("HH"),
   * HOURS_MINS for hours and minutes ("HH:mm"),
   * or HOURS_MINS_SECS for hours, minutes, and seconds ("HH:mm:ss").
   * @param showZeroTextFlag the show zero test flag,
   * if true spaces in the text are replaced with zeros.
   */
  public TimeJTextField(final int maxNumChars, final boolean showZeroTextFlag)
  {
    this(maxNumChars, showZeroTextFlag, null);
  }

  /**
   * Creates a new 'TimeJTextField' object.
   * @param maxNumChars the maximum number of characters.
   * This value should be HOURS for hours only ("HH"),
   * HOURS_MINS for hours and minutes ("HH:mm"),
   * or HOURS_MINS_SECS for hours, minutes, and seconds ("HH:mm:ss").
   * @param showZeroTextFlag the show zero test flag,
   * if true spaces in the text are replaced with zeros.
   * @param calObj calendar object or null to create a new one.
   */
  public TimeJTextField(
      final int maxNumChars, final boolean showZeroTextFlag, Calendar calObj)
  {
    super(maxNumChars + 1);
    this.maxNumChars = maxNumChars;
    this.showZeroTextFlag = showZeroTextFlag;

    MAX_CALENDAR_TIME_FIELD = (maxNumChars+1)/3;
    if (calObj == null)
      calObj = Calendar.getInstance();
    this.calObj = calObj;
    workingCalObj = (Calendar)calObj.clone();

    setDocument(new TimeFilteredDocument(this));
    setTime(null);  //clear the time
    if (showZeroTextFlag)  //if showing zeros instead of spaces
      setText(getCleanText());  //replace spaces with zeros
    //set the caret position so that there will be a change
    setCaretPosition(1);
    setFont(new Font("Monospaced", Font.PLAIN, getFont().getSize()));
    //create time formatter object
    timeFormatterObj =
        new SimpleDateFormat(TIME_PATTERN.substring(0, maxNumChars));

    addCaretListener(new CaretListener()
    {
      /**
       * Called when the caret position is updated.
       *
       * @param e the caret event
       */
      public void caretUpdate(CaretEvent e)
      {
        final int dot = e.getDot();  //get the current position
        final int mark = e.getMark();  //get the current mark
        int nextDot = dot;

        try
        {
          if (nextDot == caretPos - 1)  //if new position is 1 smaller
          {
            //if character at current position is a space
            while (Character.isSpaceChar(getText(nextDot, 1).charAt(0)))
            {
              if (nextDot <= 0)  //exit if at the start
                break;

              //go to the previous position
              --nextDot;
            }

            //if the next position is the start and is a space
            if (nextDot == 0 &&
                Character.isSpaceChar(getText(nextDot, 1).charAt(0)))
            {
              //restore the original position
              nextDot = caretPos;
            }
          }
          else
          {
            //if character at current position is a space
            while (Character.isSpaceChar(getText(nextDot, 1).charAt(0)))
            {
              if (nextDot < maxNumChars)  //if not at the end
                //go to the next position
                ++nextDot;
            }
          }
        }
        catch (Exception ex)
        {
        }

        if (dot != nextDot)  //if position needs to be changed
        {
          if (mark != dot)  //if mark is set
            moveCaretPosition(nextDot);  //move the caret to the new position
          else
            setCaretPosition(nextDot);   //set the new position
        }

        caretPos = nextDot;  //save the new position
      }
    });

    addFocusListener(new FocusListener()
    {
      /**
       * Invoked when a component gains the keyboard focus.
       * @param e the focus event.
       */
      public void focusGained(FocusEvent e)
      {
      }

      /**
       * Invoked when a component loses the keyboard focus.
       * @param e the focus event.
       */
      public void focusLost(FocusEvent e)
      {
        if (showZeroTextFlag)  //if showing zeros instead of spaces
        {
          //get current text
          final String currentText = getText();
          //get clean text
          final String cleanText = getCleanText(currentText);

          if (!cleanText.equals(currentText))  //if text changed
            setText(cleanText);  //use clean text
        }
      }
    });

    //initialize the caret position to the start
    setCaretPosition(0);
  }

  /**
   * Gets the maximum number of characters.
   * @return HOURS for hours only ("HH"),
   * HOURS_MINS for hours and minutes ("HH:mm"),
   * or HOURS_MINS_SECS for hours, minutes, and seconds ("HH:mm:ss").
   */
  public int getMaxNumChars()
  {
    return maxNumChars;
  }

  /**
   * Gets the show zero text flag.
   * @return the show zero text flag,
   * if true spaces in the text are replaced with zeros.
   */
  public boolean isShowZeroText()
  {
    return showZeroTextFlag;
  }

  /**
   * Gets the maximum time.
   * @return the maximum time.
   */
  public Date getMaxTime()
  {
    return maxDateObj;
  }

  /**
   * Gets the minimum time.
   * @return the minimum time.
   */
  public Date getMinTime()
  {
    return minDateObj;
  }

  /**
   * Get the date that is specified in this object.
   * @return the date or null if none entered.
   */
  public Date getTime()
  {
    return getCalendar().getTime();
  }

  /**
   * Gets the time zone.
   * @return the time zone object associated with this panel.
   */
  public TimeZone getTimeZone()
  {
    return calObj.getTimeZone();
  }

  /**
   * Gets the calendar for this panel.
   * @return the calendar for this panel.
   */
  public Calendar getCalendar()
  {
    updateTime();  //update the time
    return calObj;
  }

  /**
   * Gets the date with the milliseconds cleared.
   * @param date the Date.
   * @return the date with the milliseconds cleared.
   */
  public static Date getValidDate(Date date)
  {
    return getValidDate(date, false);
  }

  /**
   * Gets the date with the milliseconds cleared.
   * @param date the Date.
   * @param ceilFlag true for the ceiling value (greater than minimum),
   * false for the floor value (less than maximum.)
   * @return the date with the milliseconds cleared.
   */
  public static Date getValidDate(Date date, boolean ceilFlag)
  {
    if (date != null)
    {
      long updateTime = date.getTime();
      if ( (updateTime % 1000) != 0)
      {
        if (ceilFlag)
        {
          updateTime += 999;
        }
        updateTime /= 1000;
        updateTime *= 1000;
        date = new Date(updateTime);
      }
    }
    return date;
  }

  /**
   * Sets the maximum time.
   * @param date the date to set or null to clear.
   * @return true if the maximum time is valid and was updated.
   */
  public boolean setMaxTime(Date date)
  {
    if (date != null)  //if max date specified
    {
      date = getValidDate(date, false);
      //if min date is defined
      if (minDateObj != null)
      {
        //exit if max date is before the min date
        if (date.before(minDateObj))
          return false;
      }
      //if current time is after the maximum
      if (getTime().after(date))
      {
        setTime(date);  //set to the maximum
      }
    }
    maxDateObj = null;
    if (date != null)
    {
      maxDateObj = date;
    }
    return true;
  }

  /**
   * Sets the minimum time.
   * @param date the date to set or null to clear.
   * @return true if the minimum time is valid and was updated.
   */
  public boolean setMinTime(Date date)
  {
    if (date != null)  //if min date specified
    {
      date = getValidDate(date, true);
      //if max date is defined
      if (maxDateObj != null)
      {
        //exit if min date is after the max date
        if (date.after(maxDateObj))
          return false;
      }
      //if current time is before the minimum
      if (getTime().before(date))
      {
        setTime(date);  //set to the minimum
      }
    }
    minDateObj = null;
    if (date != null)
    {
      minDateObj = date;
    }
    return true;
  }

  /**
   * Sets the time.
   * @param date the date to set or null to clear.
   * @return true if the time is valid and was updated.
   */
  public boolean setTime(Date date)
  {
    if (date != null)
    {
      date = getValidDate(date);
      if (minDateObj != null)
      {
        //exit if date is before the min date
        if (date.before(minDateObj))
          return false;
      }
      if (maxDateObj != null)
      {
        //exit if date is after the max date
        if (date.after(maxDateObj))
          return false;
      }
    }

    if (date != null)
    {
      calObj.setTime(date);
      setText(timeFormatterObj.format(date));

      if (!showZeroTextFlag)  //if not showing zeros
      {
        int i = CALENDAR_TIME_FIELDS.length - 1;
        for (; i >= 0; i--)
        {
          if (calObj.get(CALENDAR_TIME_FIELDS[i]) != 0)
            break;
        }

        //if all time fields are 0, show empty time text
        if (i < 0)
          setText(EMPTY_TIME_TEXT.substring(0, maxNumChars));
      }
    }
    else
    {
      calObj.clear();
      setText(EMPTY_TIME_TEXT.substring(0, maxNumChars));
    }

    if (debugFlag)
      System.out.println(
          "Set date: " + calObj.getTime());
    return true;
  }

  /**
   * Sets the time zone with the given time zone value.
   * @param value the given time zone.
   */
  public void setTimeZone(TimeZone value)
  {
    calObj.setTimeZone(value);
    workingCalObj.setTimeZone(value);
    timeFormatterObj.setTimeZone(value);
  }

  /**
   * Returns a string representation of this component and its values.
   * @return    a string representation of this component.
   */
  public String toString()
  {
    final Date time = getTime();
    if (time != null)
      return timeFormatterObj.format(time);
    return "";
  }

  //non-public methods

  /**
   * Gets a cleaned up version of the text where spaces are replaced with zeros.
   * @return a cleaned up version of the text.
   */
  protected String getCleanText()
  {
    return getCleanText(getText());
  }

  /**
   * Gets a cleaned up version of the text where spaces are replaced with zeros.
   * @param text date text.
   * @return a cleaned up version of the text.
   */
  protected static String getCleanText(String text)
  {
    if (text != null)
      return text.replace(' ', '0');
    return text;
  }

  /**
   * Updates the date that is specified in this object.
   */
  protected void updateTime()
  {
    try
    {
      //parse the time replacing spaces with zeros
      final String cleanText = getCleanText();
      final Date date = timeFormatterObj.parse(cleanText);

      //set the working time calendar to the specified time
      workingCalObj.setTime(date);

      //copy the time fields from the working calendar to the calendar
      for (int i = 0; i < MAX_CALENDAR_TIME_FIELD; i++)
      {
        final int field = CALENDAR_TIME_FIELDS[i];
        final int value = workingCalObj.get(field);
        calObj.set(field, value);
      }

      //get the new time
      final Date newDate = calObj.getTime();

      if (debugFlag)
        System.out.println(
            "New date: " + newDate + " (" + cleanText + ")");

      if (minDateObj != null)
      {
        //exit if date is before the min date
        if (newDate.before(minDateObj))
        {
          if (debugFlag)
            System.out.println(
                "New date is before the min date (" + minDateObj + ")");
          calObj.setTime(minDateObj);
          return;
        }
      }
      if (maxDateObj != null)
      {
        //exit if date is after the max date
        if (newDate.after(maxDateObj))
        {
          if (debugFlag)
            System.out.println(
                "New date is after the max date (" + maxDateObj + ")");
          calObj.setTime(maxDateObj);
          return;
        }
      }
    }
    catch (Exception ex)
    {
    }
  }
}


/**
 * Class TimeFilteredDocument is an extension of 'PlainDocument' that
 * implements the filtering of input characters.
 */
class TimeFilteredDocument extends PlainDocument
{
  //time digits for most digits
  public final static String TIME_DIGITS = " 0123456789";
  //time digits for the first hour digit
  public final static String HOURS_FIRST_DIGITS =
      TIME_DIGITS.substring(0, 4);
  //time digits for the first minutes/seconds digit
  public final static String MINS_SECS_FIRST_DIGITS =
      TIME_DIGITS.substring(0, 7);

  //string of allowed characters
  public final static String[] ALLOWED_CHARS =
  {
    HOURS_FIRST_DIGITS,
    TIME_DIGITS,
    TimeJTextField.FIELD_SEPARATOR,
    MINS_SECS_FIRST_DIGITS,
    TIME_DIGITS,
    TimeJTextField.FIELD_SEPARATOR,
    MINS_SECS_FIRST_DIGITS,
    TIME_DIGITS,
    TimeJTextField.MS_FIELD_SEPARATOR,
    TIME_DIGITS,
    TIME_DIGITS,
    TIME_DIGITS,
  };

  private final TimeJTextField textField;      //text field

  /**
   * Constructs a plain text document.
   * @param textField the text field for the document.
   */
  public TimeFilteredDocument(TimeJTextField textField)
  {
    this.textField = textField;
  }

  /**
   * Constructs a plain text document.
   * @param textField the text field for the document.
   * @param maxNumChars the maximum number of characters.
   * @deprecated maxNumChars is not used
   */
  public TimeFilteredDocument(TimeJTextField textField, int maxNumChars)
  {
    this.textField = textField;
  }

  /**
   * Inserts a character into the document.
   * @param offs the offset.
   * @param ch the character to insert.
   * @return the next offset or -1 if the character was not added.
   * @exception BadLocationException if the given position is not a
   * valid position within the document.
   */
  protected int insertChar(int offs, final char ch)
      throws BadLocationException
  {
    final int currentLen = getLength();  //get the current length
    String str = new String(new char[]{ch});
    final int endIndex;

    //if current text exists and added character is digit
    if (currentLen >= offs && Character.isDigit(ch))
    {
      //get the current text
      final String currentText = getText(0, currentLen);

      char cc;

      //if at end or current digit is not a time digit
      if (currentLen == offs ||
          TIME_DIGITS.indexOf(cc = currentText.charAt(offs)) < 0)
      {
        //exit if at beginning
        if (offs <= 0)
          return -1;
        //get the previous character
        cc = currentText.charAt(--offs);
      }
      else if (Character.isDigit(cc))
      {
        //if not at the beginning and offset is the caret position
        if (offs > 0 && offs == textField.getCaretPosition())
          //get the previous character
          cc = currentText.charAt(--offs);
      }
      else if (Character.isSpaceChar(cc))
      {
        //skip over spaces
        for (int i = offs + 1; i < currentLen &&
             Character.isSpaceChar(currentText.charAt(i)); i++)
        {
          offs++;
        }
      }

      //end at this character (add 1 for substring)
      endIndex = offs + 1;

      //skip over digits
      while (offs > 0 && Character.isDigit(cc))
      {
        cc = currentText.charAt(--offs);
      }

      //if no space before the digits
      if (!Character.isSpaceChar(cc))
        return -1;

      //get the substring
      str = currentText.substring(offs + 1, endIndex) + ch;
    }

    //exit if the string is not valid
    if (!isValidStr(offs, str))
      return -1;

    final int strLen = str.length();

    if (currentLen > offs)
      super.remove(offs, strLen);
    super.insertString(offs, str, null);
    return offs + strLen;
  }

  /**
   * Determines if the character is valid for the specified position.
   * @param offs the offset.
   * @param ch the character to insert.
   * @return true if the character is valid.
   */
  protected static boolean isValidChar(int offs, char ch)
  {
    return offs < ALLOWED_CHARS.length && ALLOWED_CHARS[offs].indexOf(ch) >= 0;
  }

  /**
   * Determines if the string is valid for the specified position.
   * @param offs the offset.
   * @param str the string to insert.
   * @return true if the string is valid.
   */
  protected boolean isValidStr(int offs, String str)
  {
    //for all the characters in the string
    for (int i = str.length() - 1; i >= 0; i--)
    {
      //exit if the character is not valid
      if (!isValidChar(offs + i, str.charAt(i)))
        return false;
    }

    if (offs <= 1)  //if this string contains an hour digit
    {
      final char hourFirstDigit;
      final char hourSecondDigit;

      try
      {
        //get the first hours digit
        if (offs == 0)  //if string contains the first hour digit
          hourFirstDigit = str.charAt(0);
        else
          hourFirstDigit = getText(0, 1).charAt(0);

        //get the second hours digit
        if (offs == 1)
          hourSecondDigit = str.charAt(0);
        else if (offs == 0 && str.length() > 1)
          hourSecondDigit = str.charAt(1);
        else if (getLength() > 1)
          hourSecondDigit = getText(1, 1).charAt(0);
        else
          return true;  //second digit is empty
      }
      catch (Exception ex)
      {
        return false;
      }

      //make sure total hours is less than 24
      if (hourFirstDigit == '2' && hourSecondDigit >= '4')
        return false;
    }

    return true;
  }

  /**
   * Remove the character from the document.
   * @param offs the offset.
   * @return the next offset or -1 if the character was not removed.
   * @exception BadLocationException if the given position is not a
   * valid position within the document.
   */
  protected int removeChar(int offs) throws BadLocationException
  {
    final int currentLen = getLength();  //get the current length

    //if current text exists
    if (currentLen > offs)
    {
      //get the current text
      final String currentText = getText(0, currentLen);

      char cc = currentText.charAt(offs);

      //end at this character (add 1 for substring but skip the last one)
      final int endIndex = offs;

      //if current digit is a digit
      if (Character.isDigit(cc))
      {
        //while the previous character is a digit
        while (offs > 0 && Character.isDigit(currentText.charAt(offs - 1)))
        {
          offs--;   //go to previous digit
        }
      }

      //get the substring
      final String str =  TimeJTextField.EMPTY_TIME_TEXT.charAt(offs) +
                          currentText.substring(offs, endIndex);
      final int strLen = str.length();

      super.remove(offs, strLen);
      super.insertString(offs, str, null);
      return offs + strLen;
    }

    return -1;
  }

  /**
   * Gets the text field for this document.
   * @return the text field for this document.
   */
  public TimeJTextField getTextField()
  {
    return textField;
  }

  /**
   * Inserts data into the document, filtering it along the way.
   * @param offs the starting offset (greater than or equal to zero).
   * @param str the string to insert (empty and null strings are
   * ignored).
   * @param aSet the attributes for the inserted content.
   * @exception BadLocationException if the given position is not a
   * valid position within the document.
   */
  public void insertString(int offs, String str, AttributeSet aSet)
      throws BadLocationException
  {
    final int len;
    if (str == null || (len = str.length()) <= 0)
      return;      //if null or empty string then just return

    for (int i = offs, p = 0; p < len; p++)
    {
      //exit if the character was not added
      if ((i = insertChar(i, str.charAt(p))) < 0)
        break;
    }
  }

  /**
   * Removes some content from the document.
   * Removing content causes a write lock to be held while the
   * actual changes are taking place.  Observers are notified
   * of the change on the thread that called this method.
   * @param offs the starting offset >= 0
   * @param len the number of characters to remove >= 0
   * @exception BadLocationException  the given remove position is not a valid
   *   position within the document
   */
  public void remove(int offs, int len) throws BadLocationException
  {
    for (int i = offs, p = 0; p < len; p++)
    {
      //exit if the character was not removed
      if ((i = removeChar(i)) < 0)
        break;
    }
  }
}
