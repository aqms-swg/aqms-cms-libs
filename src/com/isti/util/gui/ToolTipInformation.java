//ToolTipInformation.java: Defines methods for handling tool tip information.
//
//  8/19/2004 -- [KF]  Initial version.
//

package com.isti.util.gui;

/**
 * Class ToolTipInformation defines methods for handling tool tip information.
 */
public interface ToolTipInformation
{
  /**
   * Determines if the tool tip is enabled or not.
   * @return true if the tool tip is enabled, false otherwise.
   */
  public boolean isToolTipEnabled();

  /**
   * Enables or disables the tool tip.
   * @param b true if the tool tip is enabled, false otherwise.
   */
  public void setToolTipEnabled(boolean b);
}