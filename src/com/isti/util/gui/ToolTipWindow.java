//ToolTipWindow.java:  The tool tip window.
//
//  8/19/2004 -- [KF]  Initial version.
// 10/14/2004 -- [KF]  Added constructor with no specified owner.
// 11/04/2004 -- [KF]  Changed to display "?" if text is null or empty.
//

package com.isti.util.gui;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Component;
import java.awt.Container;
import java.awt.Point;
import java.awt.event.MouseEvent;
import javax.swing.*;

/**
 * Class ToolTipWindow implements the tool tip window.
 */
public class ToolTipWindow extends JWindow
{
  private final JLabel toolTipObj;  //the tool tip

  /**
   * Creates a tool tip window with no specified owner.
   */
  public ToolTipWindow()
  {
    this(null);
  }

  /**
   * Creates a tool tip window.
   * If <code>owner</code> is <code>null</code>, the shared owner
   * will be used.
   * @param owner the frame from which the window is displayed.
   */
  public ToolTipWindow(JFrame owner)
  {
    super(owner);
    //create label for screen:
    toolTipObj = new JLabel();
    toolTipObj.setBorder(BorderFactory.createCompoundBorder(
        BorderFactory.createLineBorder(Color.black),
        BorderFactory.createEmptyBorder(0,2,0,2)));
    final Container paneObj = getContentPane();
    paneObj.setLayout(new BorderLayout());        //user Border layout
    paneObj.add(toolTipObj,BorderLayout.CENTER);  //add tool tip to window
  }

  /**
   * Returns the tool tip text.
   * @return the tool tip text.
   * @see #setText
   */
  public String getText()
  {
    return toolTipObj.getText();
  }

  /**
   * Sets the location to the position of the mouse event.
   * @param e the mouse event.
   */
  public void setLocation(MouseEvent e)
  {
    final Point p = e.getPoint();
    final Object source = e.getSource();
    p.y += getHeight()/2;
    if (source instanceof Component)
    {
      final Point sl = ((Component)source).getLocationOnScreen();
      p.x += sl.x;
      p.y += sl.y;
    }
    setLocation(p);
  }

  /**
   * Sets the tool tip text.
   * @param text the tool tip text.
   */
  public void setText(String text)
  {
    if (text == null || text.length() <= 0)
    {
      text = "?";
    }
    toolTipObj.setText(text);
    pack();                                      //size window to contents
  }
}