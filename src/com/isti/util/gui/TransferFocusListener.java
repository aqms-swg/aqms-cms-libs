//TransferFocusListener.java:  Implements the transfer focus listener.
//
//    9/3/2003 -- [KF]
//

package com.isti.util.gui;

import java.awt.event.*;
import javax.swing.JComponent;

/**
 * Class TransferFocusListener implements the transfer focus listener.
 */
public class TransferFocusListener implements ActionListener
{
  /**
   * Transfer focus listener.
   */
  public final static TransferFocusListener LISTENER =
      new TransferFocusListener();

  /**
   * Invoked when an action occurs.
   * @param e the action event.
   */
  public void actionPerformed(ActionEvent e)
  {
    final Object source = e.getSource();
    if (source instanceof JComponent)
    {
      ((JComponent)source).transferFocus();  //put focus on next field
    }
  }
}