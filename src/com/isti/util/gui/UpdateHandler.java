//UpdateListener.java:  Defines an update listener.
//
//   8/21/2003 -- [KF]
//

package com.isti.util.gui;

import java.awt.Component;
import java.awt.event.*;
import javax.swing.event.*;

/**
 * Class UpdateListener defines an update listener.
 */
public class UpdateHandler
    implements ActionListener, ChangeListener, DocumentListener, FocusListener,
    UpdateListener
{
  private final Component component;
  private final UpdateListener updateListener;

  /**
   * Creates an update listener.
   * @param component the component.
   * @param updateListener the update listener.
   */
  public UpdateHandler(Component component, UpdateListener updateListener)
  {
    //save parameters
    this.component = component;
    this.updateListener = updateListener;
  }


  // ActionListener interface

  /**
   * Invoked when an action occurs.
   *
   * @param e the action event.
   */
  public void actionPerformed(ActionEvent e)
  {
    processUpdate(component, e);
  }


  // ChangeListener interface

  /**
   * Invoked when the target of the listener has changed its state.
   *
   * @param e the change event.
   */
  public void stateChanged(ChangeEvent e)
  {
    processUpdate(component, e);
  }


  // DocumentListener interface

  /**
   * Gives notification that there was an insert into the document.  The
   * range given by the DocumentEvent bounds the freshly inserted region.
   *
   * @param e the document event.
   */
  public void insertUpdate(DocumentEvent e)
  {
    processUpdate(component, e);
  }


  // FocusListener interface

  /**
   * Invoked when a component gains the keyboard focus.
   * @param e the focus event.
   */
  public void focusGained(FocusEvent e)
  {
    processUpdate(component, e);
  }

  /**
   * Invoked when a component loses the keyboard focus.
   * @param e the focus event.
   */
  public void focusLost(FocusEvent e)
  {
    processUpdate(component, e);
  }


  /**
   * Gives notification that a portion of the document has been
   * removed.  The range is given in terms of what the view last
   * saw (that is, before updating sticky positions).
   *
   * @param e the document event
   */
  public void removeUpdate(DocumentEvent e)
  {
    processUpdate(component, e);
  }

  /**
   * Gives notification that an attribute or set of attributes changed.
   *
   * @param e the document event
   */
  public void changedUpdate(DocumentEvent e)
  {
    processUpdate(component, e);
  }


  // UpdateListener interface

  /**
   * Process an update.
   * @param component the component.
   * @param e the event or null if none.
   */
  public void processUpdate(Component component, Object e)
  {
    updateListener.processUpdate(component, e);
  }
}