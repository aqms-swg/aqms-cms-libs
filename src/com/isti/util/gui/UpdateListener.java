//UpdateListener.java:  Defines an update listener.
//
//   8/21/2003 -- [KF]
//

package com.isti.util.gui;

import java.awt.Component;

/**
 * Interface UpdateListener defines an update listener.
 */
public interface UpdateListener extends java.util.EventListener
{
  /**
   * Process an update.
   * @param component the component.
   * @param e the event or null if none.
   */
  public void processUpdate(Component component, Object e);
}