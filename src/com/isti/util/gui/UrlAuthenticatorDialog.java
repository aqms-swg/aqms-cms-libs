package com.isti.util.gui;

import java.awt.Component;

import com.isti.util.PasswordAuthenticator;
import com.isti.util.UrlAuthenticator;

/**
 * URL Authenticator Dialog
 * <p>
 * This class can handle URL password authentication whenever a password
 * protected URL is encountered. All the program needs to do is call
 * <code>UrlAuthenticatorDialog.createDefaultAuthenticator(Component)</code> and
 * after that all URL's requiring password authentication will cause a dialog
 * popup to appear as needed.
 * @see #createDefaultAuthenticator(Component)
 */
public class UrlAuthenticatorDialog extends UrlAuthenticator {

  /**
   * Create the URL authenticator dialog.
   * @param component the parent component for the password dialog.
   */
  public UrlAuthenticatorDialog(Component component) {
    this(component, "Authentication Required");
  }

  /**
   * Create the URL authenticator dialog.
   * @param component the parent component for the password dialog.
   * @param titleStr the title string for popup window.
   */
  public UrlAuthenticatorDialog(Component component, String titleStr) {
    super(createPasswordAuthenticator(component, titleStr));
  }

  /**
   * Create the URL authenticator dialog and set it as the default
   * authenticator.
   * @param component the parent component for the password dialog.
   * @return the URL authenticator dialog.
   */
  public static UrlAuthenticatorDialog createDefaultAuthenticator(
      Component component) {
    UrlAuthenticatorDialog authenticator = new UrlAuthenticatorDialog(component);
    setDefaultAuthenticator(authenticator);
    return authenticator;
  }

  /**
   * Create a password authenticator.
   * @param component the parent component for the password dialog.
   * @param titleStr the title string for popup window.
   * @return the password authenticator.
   */
  public static PasswordAuthenticator createPasswordAuthenticator(
      Component component, String titleStr) {
    IstiPasswordDialog passwordDialog = new IstiPasswordDialog(component,
        titleStr);
    passwordDialog.setUsernameAsInitalFocus();
    return passwordDialog;
  }
}
