//ValueJTableModel.java:  Extends a ValueTableModel for use with a JTable.
//
// 11/20/2007 -- [KF]  Initial version.
//

package com.isti.util.gui;

import com.isti.util.*;
import javax.swing.event.EventListenerList;
import javax.swing.event.TableModelEvent;
import javax.swing.event.TableModelListener;
import javax.swing.table.TableModel;

public class ValueJTableModel
    implements TableModel, ValueTableModel
{
  /**
   * Storage for the listeners registered with this model.
   */
  protected EventListenerList listenerList = new EventListenerList();
  /** The value table model */
  private final ValueTableModel vtm;

  /**
   * Creates the value GUI table model.
   * @param vtm the value table model.
   */
  public ValueJTableModel(ValueTableModel vtm)
  {
    this.vtm = vtm;
  }

  /**
   * Adds a listener to the table model. The listener will receive notification
   * of all changes to the table model.
   *
   * @param listener the listener.
   */
  public void addTableModelListener(TableModelListener listener)
  {
    listenerList.add(TableModelListener.class, listener);
  }

  /**
   * Sends the specified event to all registered listeners.
   *
   * @param event the event to send.
   */
  public void fireTableChanged(TableModelEvent event)
  {
    int index;
    TableModelListener listener;
    Object[] list = listenerList.getListenerList();

    for (index = 0; index < list.length; index += 2)
    {
      listener = (TableModelListener) list[index + 1];
      listener.tableChanged(event);
    }
  }

  /**
   * Sends a {@link TableModelEvent} to all registered listeners to inform
   * them that the table data has changed.
   */
  public void fireTableDataChanged()
  {
    fireTableChanged(new TableModelEvent(this, 0, Integer.MAX_VALUE));
  }

  /**
   * Returns the <code>Class</code> for all <code>Object</code> instances
   * in the specified column.
   *
   * @param columnIndex the column index.
   *
   * @return The class.
   */
  public Class getColumnClass(int columnIndex)
  {
    return vtm.getColumnClass(columnIndex);
  }

  /**
   * Returns the number of columns in the model.
   *
   * @return The column count
   */
  public int getColumnCount()
  {
    return vtm.getColumnCount();
  }

  /**
   * Gets the column index for the specified column name.
   * @param columnName the column name.
   * @return the column index or -1 if none.
   */
  public int getColumnIndex(String columnName)
  {
    return vtm.getColumnIndex(columnName);
  }

  /**
   * Returns the name of a column in the model.
   *
   * @param columnIndex the column index.
   *
   * @return The column name.
   */
  public String getColumnName(int columnIndex)
  {
    return vtm.getColumnName(columnIndex);
  }

  /**
   * Returns the number of rows in the model.
   *
   * @return The row count.
   */
  public int getRowCount()
  {
    return vtm.getRowCount();
  }

  /**
   * Returns the value (<code>Object</code>) at a particular cell in the
   * table.
   *
   * @param rowIndex the row index.
   * @param columnIndex the column index.
   *
   * @return The value at the specified cell.
   */
  public Object getValueAt(int rowIndex, int columnIndex)
  {
    return vtm.getValueAt(rowIndex, columnIndex);
  }

  /**
   * Import the values from the value table model for all rows.
   * @param vtm the value table model.
   */
  public void importValues(ValueTableModel vtm)
  {
    this.vtm.importValues(vtm);
  }

  /**
   * Returns <code>true</code> if the specified cell is editable, and
   * <code>false</code> if it is not.
   *
   * @param rowIndex the row index of the cell.
   * @param columnIndex the column index of the cell.
   *
   * @return true if the cell is editable, false otherwise.
   */
  public boolean isCellEditable(int rowIndex, int columnIndex)
  {
    return vtm.isCellEditable(rowIndex, columnIndex);
  }

  /**
   * Removes a listener from the table model so that it will no longer receive
   * notification of changes to the table model.
   *
   * @param listener the listener to remove.
   */
  public void removeTableModelListener(TableModelListener listener)
  {
    listenerList.remove(TableModelListener.class, listener);
  }

  /**
   * Sets the value at a particular cell in the table.
   *
   * @param aValue the value (<code>null</code> permitted).
   * @param rowIndex the row index.
   * @param columnIndex the column index.
   */
  public void setValueAt(Object aValue, int rowIndex, int columnIndex)
  {
    vtm.setValueAt(aValue, rowIndex, columnIndex);
  }

  private final static String TEST_INPUT =
      "C0,C1,C2,C3\n" +
      "V0,V1,V2,V3\n";

  public static void main(String[] args)
  {
    final DelimiterSeparatedValuesTable dsvt =
        new DelimiterSeparatedValuesTable();
    final ValueJTableModel vgtm = new ValueJTableModel(dsvt);
    dsvt.setEditable(true);

    String inputFileName = null;
    String outputFileName = null;
    if (args.length > 0)
    {
      inputFileName = args[0];
      if (args.length > 1)
      {
        outputFileName = args[1];
      }
    }
    final java.io.Reader reader;
    if (inputFileName != null)
    {
      try
      {
        reader = new java.io.FileReader(inputFileName);
      }
      catch (Exception ex)
      {
        System.err.println("Could not open input file: " + ex);
        return;
      }
    }
    else
    {
      System.out.println(TEST_INPUT);
      reader = new java.io.StringReader(TEST_INPUT);
    }
    java.io.BufferedReader br = new java.io.BufferedReader(reader);
    DelimiterSeparatedValues input = new DelimiterSeparatedValues(
        DelimiterSeparatedValues.STANDARD_COMMENT_TEXT);
    DelimiterSeparatedValues output = new DelimiterSeparatedValues(
//      DelimiterSeparatedValues.DEFAULT_COMMENT_TEXT,
//      DelimiterSeparatedValues.DEFAULT_DELIMITER_CHAR,
//      DelimiterSeparatedValues.DEFAULT_ESCAPED_CHARS,
//      DelimiterSeparatedValues.DEFAULT_ESCAPE_CHAR);
        );
    java.io.Writer w = null;
    if (outputFileName != null)
    {
      try
      {
        w = new java.io.FileWriter(outputFileName);
      }
      catch (Exception ex)
      {
        System.err.println("Could not open output file: " + ex);
        return;
      }
    }

    try
    {
      String errorMessage;
      int count = dsvt.readAll(input, br, true);
      if (count == 0)
      {
        errorMessage = dsvt.getErrorMessageString();
        if (errorMessage.length() > 0)
        {
          dsvt.clearErrorMessageString();
          System.err.println("Error reading input:\n" + errorMessage);
        }
      }
      else
      {
        System.out.println("Found " + count + " lines");
      }
    }
    catch (Exception ex)
    {
      System.err.println(ex);
      ex.printStackTrace();
    }

    //add main content panel to frame:
    final javax.swing.JTable tableObj = new javax.swing.JTable(vgtm);
    final javax.swing.JScrollPane scrollPaneObj =
        new javax.swing.JScrollPane(tableObj);
    //create and set up the frame
    final javax.swing.JFrame frameObj =
        new javax.swing.JFrame("ValueJTableModel");
    frameObj.getContentPane().add(scrollPaneObj);
    frameObj.setDefaultCloseOperation(javax.swing.JFrame.DO_NOTHING_ON_CLOSE);
    frameObj.pack();
    frameObj.setVisible(true); //make frame visible

    final Object windowCloseObj = new Object();
    frameObj.addWindowListener(new java.awt.event.WindowAdapter()
    {
      public void windowClosing(java.awt.event.WindowEvent e)
      {
        synchronized (windowCloseObj)
        {
          windowCloseObj.notifyAll();
        }
      }
    });

    try
    {
      synchronized (windowCloseObj)
      {
        windowCloseObj.wait();
      }
    }
    catch (Exception ex)
    {
      System.err.println(ex);
      ex.printStackTrace();
    }

    if (w != null)
    {
      try
      {
        dsvt.writeAll(output, w, true);
      }
      catch (Exception ex)
      {
        System.err.println(ex);
        ex.printStackTrace();
      }
    }

    if (w != null)
    {
      try
      {
        w.close();
      }
      catch (Exception ex)
      {
        System.err.println("Error closing output file: " + ex);
      }
    }
    if (br != null)
    {
      try
      {
        br.close();
      }
      catch (Exception ex)
      {
        System.err.println("Error closing input file: " + ex);
      }
    }
    frameObj.dispose();
  }
}
