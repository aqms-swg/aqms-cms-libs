//ViewHTMLPanel.java:  Simple HTML viewer panel (capable of viewing HTML
//                     files that reside in a 'jar' archive).
//
//  5/17/2002 -- [ET]  Initial version.
//  7/10/2002 -- [KF]  Made content window have initial keyboard focus
//                     (instead of the "Close" button).
//  7/18/2002 -- [ET]  Class moved to isti.util.gui; added 'showInDialog()'
//                     method.
// 10/24/2002 -- [ET]  Added constructor with button-enable flags.
//  11/4/2002 -- [ET]  Added 'modalFlag' parameter to 'showInDialog()'
//                     method and other support for modeless dialogs.
// 11/24/2002 -- [ET]  Modified to use 'IstiDialogPopup' instead of
//                     'SimpleDialog'.
//  12/5/2002 -- [ET]  Put in work-around in 'showInDialog()' to get the
//                     panel to display in the dialog under Java 1.3.
//   3/5/2003 -- [ET]  Improved 'showInDialog()' work-around to get it to
//                     work when using a modal dialog under Java 1.4.
//   3/7/2003 -- [ET]  Modified 'showInDialog' to support using one or
//                     two specified buttons; added 'getInputValue()'
//                     method.
//  1/30/2004 -- [ET]  Added tooltips to buttons.
//   2/5/2004 -- [ET]  Added 'setPageRef()' method and optional
//                     'initialRefStr' parameter to constructor;
//                     modified to remove any anchor-reference
//                     before launching browser.
//  12/2/2004 -- [ET]  Modified 'setPage()' to handle references with
//                     encoded characters (i.e. "%20").
//  4/26/2005 -- [ET]  Modified 'closeDialog()' to wait for dialog to be
//                     displayed; added method 'getDialogClosedFlag()'.
//  5/10/2006 -- [ET]  Added optional 'remoteLinkCallBackObj' parameter
//                     to constructors; added location-bar implementation
//                     and optional 'locationBarFlag' parameter to
//                     constructors.
//  5/12/2006 -- [ET]  Took out unused flag in constructor.
//  6/30/2010 -- [KF]  Added 'ILaunchBrowser' interface and added 'setCheck()'
//                     and "setDisableBackWhenEmpty()" methods.

package com.isti.util.gui;

import java.util.Vector;
import java.io.InputStream;
import java.io.FileInputStream;
import java.net.URL;
import java.net.MalformedURLException;
import java.awt.Component;
import java.awt.Cursor;
import java.awt.FlowLayout;
import java.awt.BorderLayout;
import java.awt.GridBagLayout;
import java.awt.GridBagConstraints;
import java.awt.Insets;
import java.awt.event.*;
import javax.swing.*;
import javax.swing.event.HyperlinkListener;
import javax.swing.event.HyperlinkEvent;
import javax.swing.text.html.HTMLFrameHyperlinkEvent;
import javax.swing.text.html.HTMLDocument;
import com.isti.util.UtilFns;
import com.isti.util.ILaunchBrowser;
import com.isti.util.LaunchBrowser;
import com.isti.util.URLRefDecoder;
import com.isti.util.CallBackStringFlag;
import java.io.ByteArrayInputStream;

/**
 * Class ViewHTMLPanel implements a simple HTML viewer panel (capable of
 * viewing HTML files that reside in a 'jar' archive).
 */
public class ViewHTMLPanel extends JPanel
{
  private final JButton backButton;
  protected final JEditorPane editorPaneObj;
  protected final JScrollPane scrollPaneObj;
  protected final Vector historyVec = new Vector();
  protected final int xSize,ySize;
  protected final CallBackStringFlag remoteLinkCallBackObj;
  protected final JTextField locationTextFieldObj;
  protected final URL originalUrlObj;
  protected URL currentUrlObj = null;
                   //utility class for launching web browser:
  protected ILaunchBrowser launchBrowserObj = null;
                   //handle for dialog object displayed:
  protected IstiDialogPopup hostDialogObj = null;
                   //handle for user input from dialog:
  protected Object userInputValueObj = null;
                   //flag set true after 'closeDialog()' called:
  protected boolean dialogClosedFlag = false;
  protected boolean checkUrlFlag = true;
  protected boolean disableBackWhenEmptyFlag = false;

    /** Constructor parameter for disabling launch-browser on remote link. */
  public static final CallBackStringFlag REMLINK_NOACTION_CALLBACK =
    new CallBackStringFlag()
      {
          public boolean callBackFlagMethod(String str)
          {
            return false;
          }
      };

  /**
   * Creates a simple HTML viewer panel.
   * @param urlObj the initial URL to display, or null for none.
   * @param xSize the X size for the panel.
   * @param ySize the Y size for the panel.
   * @param launchBrowserObj an existing 'LaunchBrowser' object, or
   * null to have this class create its own.
   * @param backButtonFlag true to show "Back" button.
   * @param launchButtonFlag true to show "Launch Browser" button.
   * @param initialRefStr the initial reference (anchor) to display on
   * the given URL, or null for none.
   * @param remoteLinkCallBackObj a 'CallBackStringFlag' object whose
   * method will be called when a remote or non-HTML link is clicked on,
   * 'REMLINK_NOACTION_CALLBACK' for no action, or null to launch a
   * browser to show the link (default).
   * @param locationBarFlag true to show location bar; false to not
   * (default).
   */
  public ViewHTMLPanel(URL urlObj, int xSize, int ySize,
                     ILaunchBrowser launchBrowserObj, boolean backButtonFlag,
                             boolean launchButtonFlag, String initialRefStr,
                                   CallBackStringFlag remoteLinkCallBackObj,
                                                    boolean locationBarFlag)
  {
    this(urlObj, xSize, ySize, launchBrowserObj, backButtonFlag,
        launchButtonFlag, initialRefStr, remoteLinkCallBackObj,
        locationBarFlag, true);
  }

  /**
   * Creates a simple HTML viewer panel.
   * @param urlObj the initial URL to display, or null for none.
   * @param xSize the X size for the panel.
   * @param ySize the Y size for the panel.
   * @param launchBrowserObj an existing 'LaunchBrowser' object, or
   * null to have this class create its own.
   * @param backButtonFlag true to show "Back" button.
   * @param launchButtonFlag true to show "Launch Browser" button.
   * @param initialRefStr the initial reference (anchor) to display on
   * the given URL, or null for none.
   * @param remoteLinkCallBackObj a 'CallBackStringFlag' object whose
   * method will be called when a remote or non-HTML link is clicked on,
   * 'REMLINK_NOACTION_CALLBACK' for no action, or null to launch a
   * browser to show the link (default).
   * @param locationBarFlag true to show location bar; false to not
   * (default).
   * @param locationBarDisableEditFlag true to disable location bar edits,
   * false to not (default).
   */
  public ViewHTMLPanel(URL urlObj, int xSize, int ySize,
      ILaunchBrowser launchBrowserObj, boolean backButtonFlag,
      boolean launchButtonFlag, String initialRefStr,
      CallBackStringFlag remoteLinkCallBackObj,
      boolean locationBarFlag, boolean locationBarDisableEditFlag)
  {
    this.xSize = xSize;
    this.ySize = ySize;
    if(launchBrowserObj != null)                 //if given then
      this.launchBrowserObj = launchBrowserObj;  //save launch browser
    this.remoteLinkCallBackObj = remoteLinkCallBackObj;
    editorPaneObj = new JEditorPane();
    editorPaneObj.setEditable(false);
    editorPaneObj.addHyperlinkListener(new HyperlinkListener()
        {
          public void hyperlinkUpdate(HyperlinkEvent hEvtObj)
          {
            if (hEvtObj.getEventType() == HyperlinkEvent.EventType.ACTIVATED)
            {
              if (hEvtObj instanceof HTMLFrameHyperlinkEvent)
              {
                final HTMLFrameHyperlinkEvent evt =
                                           (HTMLFrameHyperlinkEvent)hEvtObj;
                final HTMLDocument doc =
                                  (HTMLDocument)editorPaneObj.getDocument();
                doc.processHTMLFrameHyperlinkEvent(evt);
              }
              else
                setPage(hEvtObj.getURL(),true);
            }
          }
        });

    scrollPaneObj = new JScrollPane(editorPaneObj);
    editorPaneObj.addFocusListener(new FocusAdapter()
        {
          public void focusGained(FocusEvent e)
          {
            if (scrollPaneObj != null)
              scrollPaneObj.requestFocus();
          }
        });
//    setPreferredSize(new Dimension(xSize,ySize));
//    setMinimumSize(new Dimension(10,10));

         //create panel for buttons:
    final JPanel buttonsPanel = new JPanel(
                                      new FlowLayout(FlowLayout.LEFT,20,0));
    if(backButtonFlag)
    {    //flag set; create "Back" button
      backButton = new JButton("Back");
      backButton.addActionListener(new ActionListener()
          {                    //setup button action
            public void actionPerformed(ActionEvent evtObj)
            {
              //fetch and remove last URL in history:
              final URL urlObj = removeHistory();
              if(urlObj != null)
              {      //item removed OK; set new page, don't save to history
                doSetPage(urlObj,false);
              }
            }
          });
      if (disableBackWhenEmptyFlag)
      {
        backButton.setEnabled(false);
      }
      backButton.setToolTipText("Return to previous location");
      buttonsPanel.add(backButton);
    }
    else
    {
      backButton = null;
    }
    if(launchButtonFlag)
    {    //flag set; create "Launch Browser" button
      final JButton launchButton = new JButton("Launch Browser");
      launchButton.addActionListener(new ActionListener()
          {                    //setup button action
            public void actionPerformed(ActionEvent evtObj)
            {                       //show current URL in browser:
              if(currentUrlObj != null)
                doLaunchBrowser(currentUrlObj);
            }
          });
      launchButton.setToolTipText("View document in web browser");
      buttonsPanel.add(launchButton);
    }

    setLayout(new BorderLayout());
    add(scrollPaneObj,BorderLayout.CENTER);

    if(locationBarFlag)
    {    //location bar enabled; create text field for location bar
      locationTextFieldObj = new JTextField();
      if (locationBarDisableEditFlag)
      {
        locationTextFieldObj.setEditable(false);
      }
              //create listener invoked via "Enter" key or "Go" button:
      final ActionListener locActionObj = new ActionListener()
          {
            public void actionPerformed(ActionEvent evtObj)
            {
              final String locStr = locationTextFieldObj.getText();
              URL locUrlObj;
              try
              {         //convert text to URL object:
                locUrlObj = new URL(locStr);
              }
              catch(MalformedURLException ex)
              {         //bad URL
                locUrlObj = null;       //indicate error
              }
              if(locUrlObj == null)
              {    //given location text is bad URL
                try
                {       //try location text with "http://" prepended:
                  locUrlObj = new URL("http://" + locStr);
                }
                catch(MalformedURLException ex)
                {       //URL still bad
                }
              }
              boolean retFlag;
              if(locUrlObj != null)
              {    //location text successfully converted to URL object
                try
                {       //view URL object:
                  retFlag = setPage(locUrlObj);
                }
                catch(Exception ex)
                {       //some kind of exception error (shouldn't happen)
                  retFlag = false;          //indicate error
                  ex.printStackTrace();     //dump stack trace to console
                }
              }
              else      //unable to convert location text to URL object
                retFlag = false;       //indicate error
              if(!retFlag)   //if view of location page failed then show msg
                showMessageText("Unable to show location:  " + locStr);
            }
          };            //attach listener to text field and "Go" button:
      locationTextFieldObj.addActionListener(locActionObj);
                        //setup tool-tips for location bar and button:
      locationTextFieldObj.setToolTipText("Location viewed in window");
                        //setup location-bar panel:
      final JPanel locationBarPanel = new JPanel(new GridBagLayout());
      final GridBagConstraints contraints = new GridBagConstraints(0,0,1,1,
                    0.1,1.0,GridBagConstraints.EAST,GridBagConstraints.NONE,
                                                   new Insets(0,0,0,0),0,0);
      locationBarPanel.add(new JLabel("Location:  "),contraints);
      ++(contraints.gridx);       //make edit field take most space:
      contraints.anchor = GridBagConstraints.CENTER;
      contraints.fill = GridBagConstraints.HORIZONTAL;
      contraints.weightx = 1.0;
      contraints.anchor = GridBagConstraints.CENTER;
      locationBarPanel.add(locationTextFieldObj,contraints);
      ++(contraints.gridx);
      contraints.fill = GridBagConstraints.NONE;
      contraints.weightx = 0.1;
      if (locationTextFieldObj.isEditable())
      {
        final JButton goButtonObj = new JButton("Go");  //view-location button
        goButtonObj.addActionListener(locActionObj);
        goButtonObj.setToolTipText("Click to view entered location");
        locationBarPanel.add(goButtonObj,contraints);
      }
              //create panel for buttons and location bar:
      final JPanel topPanel = new JPanel(new BorderLayout());
              //add buttons and location bar to panel:
      topPanel.add(buttonsPanel,BorderLayout.WEST);
      topPanel.add(locationBarPanel,BorderLayout.CENTER);
                   //add space below buttons etc via empty border:
      topPanel.setBorder(BorderFactory.createEmptyBorder(0,0,10,0));
      add(topPanel,BorderLayout.NORTH);     //add panel to top of viewer
    }
    else
    {    //location bar not enabled
      locationTextFieldObj = null;     //indicate not shown
                   //add space below buttons via empty border:
      buttonsPanel.setBorder(BorderFactory.createEmptyBorder(0,0,10,0));
                   //add buttons panel directly to top of viewer:
      add(buttonsPanel,BorderLayout.NORTH);
    }

    originalUrlObj = urlObj;      //save original URL object
    if(urlObj != null && initialRefStr != null)
    {    //initial reference specified
      addHistory(urlObj);     //put original URL into history buffer
                                  //add reference to URL:
      urlObj = UtilFns.setURLReference(urlObj,initialRefStr);
    }
    doSetPage(urlObj,false);
  }

  /**
   * Creates a simple HTML viewer panel.
   * @param urlObj the initial URL to display, or null for none.
   * @param xSize the X size for the panel.
   * @param ySize the Y size for the panel.
   * @param launchBrowserObj an existing 'ILaunchBrowser' object, or
   * null to have this class create its own.
   * @param backButtonFlag true to show "Back" button.
   * @param launchButtonFlag true to show "Launch Browser" button.
   * @param initialRefStr the initial reference (anchor) to display on
   * the given URL, or null for none.
   * @param remoteLinkCallBackObj a 'CallBackStringFlag' object whose
   * method will be called when a remote or non-HTML link is clicked on,
   * 'REMLINK_NOACTION_CALLBACK' for no action, or null to launch a
   * browser to show the link (default).
   */
  public ViewHTMLPanel(URL urlObj, int xSize, int ySize,
                     ILaunchBrowser launchBrowserObj, boolean backButtonFlag,
                             boolean launchButtonFlag, String initialRefStr,
                                   CallBackStringFlag remoteLinkCallBackObj)
  {
    this(urlObj,xSize,ySize,launchBrowserObj,backButtonFlag,
                launchButtonFlag,initialRefStr,remoteLinkCallBackObj,false);
  }

  /**
   * Creates a simple HTML viewer panel.
   * @param urlObj the initial URL to display, or null for none.
   * @param xSize the X size for the panel.
   * @param ySize the Y size for the panel.
   * @param launchBrowserObj an existing 'ILaunchBrowser' object, or
   * null to have this class create its own.
   * @param backButtonFlag true to show "Back" button.
   * @param launchButtonFlag true to show "Launch Browser" button.
   * @param initialRefStr the initial reference (anchor) to display on
   * the given URL, or null for none.
   */
  public ViewHTMLPanel(URL urlObj, int xSize, int ySize,
                     ILaunchBrowser launchBrowserObj, boolean backButtonFlag,
                             boolean launchButtonFlag, String initialRefStr)
  {
    this(urlObj,xSize,ySize,launchBrowserObj,backButtonFlag,
                                 launchButtonFlag,initialRefStr,null,false);
  }

  /**
   * Creates a simple HTML viewer panel.
   * @param urlObj the initial URL to display, or null for none.
   * @param xSize the X size for the panel.
   * @param ySize the Y size for the panel.
   * @param launchBrowserObj an existing 'ILaunchBrowser' object, or
   * null to have this class create its own.
   * @param backButtonFlag true to show "Back" button.
   * @param launchButtonFlag true to show "Launch Browser" button.
   * @param initialRefStr the initial reference (anchor) to display on
   * the given URL, or null for none.
   * @param locationBarFlag true to show location bar; false to not
   * (default).
   */
  public ViewHTMLPanel(URL urlObj, int xSize, int ySize,
                     ILaunchBrowser launchBrowserObj, boolean backButtonFlag,
                             boolean launchButtonFlag, String initialRefStr,
                                                    boolean locationBarFlag)
  {
    this(urlObj,xSize,ySize,launchBrowserObj,backButtonFlag,
                       launchButtonFlag,initialRefStr,null,locationBarFlag);
  }

  /**
   * Creates a simple HTML viewer panel.
   * @param urlObj the initial URL to display, or null for none.
   * @param xSize the X size for the panel.
   * @param ySize the Y size for the panel.
   * @param launchBrowserObj an existing 'ILaunchBrowser' object, or
   * null to have this class create its own.
   * @param backButtonFlag true to show "Back" button.
   * @param launchButtonFlag true to show "Launch Browser" button.
   */
  public ViewHTMLPanel(URL urlObj, int xSize, int ySize,
                     ILaunchBrowser launchBrowserObj, boolean backButtonFlag,
                                                   boolean launchButtonFlag)
  {
    this(urlObj,xSize,ySize,launchBrowserObj,backButtonFlag,
                                          launchButtonFlag,null,null,false);
  }

  /**
   * Creates a simple HTML viewer panel.
   * @param urlObj the initial URL to display, or null for none.
   * @param xSize the X size for the panel.
   * @param ySize the Y size for the panel.
   * @param launchBrowserObj an existing 'ILaunchBrowser' object, or
   * null to have this class create its own.
   * @param backButtonFlag true to show "Back" button.
   * @param launchButtonFlag true to show "Launch Browser" button.
   * @param locationBarFlag true to show location bar; false to not
   * (default).
   */
  public ViewHTMLPanel(URL urlObj, int xSize, int ySize,
                     ILaunchBrowser launchBrowserObj, boolean backButtonFlag,
                           boolean launchButtonFlag,boolean locationBarFlag)
  {
    this(urlObj,xSize,ySize,launchBrowserObj,backButtonFlag,
                                launchButtonFlag,null,null,locationBarFlag);
  }

  /**
   * Creates a simple HTML viewer panel.
   * @param urlObj the initial URL to display, or null for none.
   * @param xSize the X size for the panel.
   * @param ySize the Y size for the panel.
   * @param launchBrowserObj an existing 'ILaunchBrowser' object, or
   * null to have this class create its own.
   * @param backButtonFlag true to show "Back" button.
   * @param launchButtonFlag true to show "Launch Browser" button.
   * @param remoteLinkCallBackObj a 'CallBackStringFlag' object whose
   * method will be called when a remote or non-HTML link is clicked on,
   * 'REMLINK_NOACTION_CALLBACK' for no action, or null to launch a
   * browser to show the link (default).
   * @param locationBarFlag true to show location bar; false to not
   * (default).
   */
  public ViewHTMLPanel(URL urlObj, int xSize, int ySize,
                     ILaunchBrowser launchBrowserObj, boolean backButtonFlag,
                                                   boolean launchButtonFlag,
                                   CallBackStringFlag remoteLinkCallBackObj,
                                                    boolean locationBarFlag)
  {
    this(urlObj,xSize,ySize,launchBrowserObj,backButtonFlag,
               launchButtonFlag,null,remoteLinkCallBackObj,locationBarFlag);
  }

  /**
   * Creates a simple HTML viewer panel.
   * @param urlObj the initial URL to display, or null for none.
   * @param xSize the X size for the panel.
   * @param ySize the Y size for the panel.
   * @param launchBrowserObj an existing 'ILaunchBrowser' object, or
   * null to have this class create its own.
   * @param backButtonFlag true to show "Back" button.
   * @param launchButtonFlag true to show "Launch Browser" button.
   * @param remoteLinkCallBackObj a 'CallBackStringFlag' object whose
   * method will be called when a remote or non-HTML link is clicked on,
   * 'REMLINK_NOACTION_CALLBACK' for no action, or null to launch a
   * browser to show the link (default).
   */
  public ViewHTMLPanel(URL urlObj, int xSize, int ySize,
                     ILaunchBrowser launchBrowserObj, boolean backButtonFlag,
                                                   boolean launchButtonFlag,
                                   CallBackStringFlag remoteLinkCallBackObj)
  {
    this(urlObj,xSize,ySize,launchBrowserObj,backButtonFlag,
                         launchButtonFlag,null,remoteLinkCallBackObj,false);
  }

  /**
   * Creates a simple HTML viewer panel.  "Back" and "Launch Browser"
   * buttons are shown.
   * @param urlObj the initial URL to display, or null for none.
   * @param xSize the X size for the panel.
   * @param ySize the Y size for the panel.
   * @param launchBrowserObj an existing 'ILaunchBrowser' object, or
   * null to have this class create its own.
   * @param initialRefStr the initial reference (anchor) to display on
   * the given URL, or null for none.
   */
  public ViewHTMLPanel(URL urlObj, int xSize, int ySize,
                       ILaunchBrowser launchBrowserObj, String initialRefStr)
  {
    this(urlObj,xSize,ySize,launchBrowserObj,true,true,initialRefStr,null,
                                                                     false);
  }

  /**
   * Creates a simple HTML viewer panel.  "Back" and "Launch Browser"
   * buttons are shown.
   * @param urlObj the initial URL to display, or null for none.
   * @param xSize the X size for the panel.
   * @param ySize the Y size for the panel.
   * @param launchBrowserObj an existing 'ILaunchBrowser' object, or
   * null to have this class create its own.
   */
  public ViewHTMLPanel(URL urlObj, int xSize, int ySize,
                                             ILaunchBrowser launchBrowserObj)
  {
    this(urlObj,xSize,ySize,launchBrowserObj,true,true,null,null);
  }

  /**
   * Shows this 'ViewHTMLPanel' in a dialog window with the given button
   * objects (if none are given then a "Close" button will be used).
   * If 'modalFlag' is true then this method will not return
   * until the dialog window is dismissed.
   * @param parentComponent the parent component for the dialog.
   * @param titleStr the title string for the dialog.
   * @param modalFlag true for a modal dialog, false for a modeless dialog
   * (that allows other windows to be active at the same time).
   * @param button1Obj first button object to use (can be a button
   * component or a string), or null for none.
   * @param button2Obj second button object to use (can be a button
   * component or a string), or null for none.
   * @return The Object chosen by the user (UNINITIALIZED_VALUE if the
   * user has not yet made a choice, or null if the user closed the window
   * without making a choice).
   */
  public Object showInDialog(Component parentComponent, String titleStr,
                    boolean modalFlag, Object button1Obj, Object button2Obj)
  {
    dialogClosedFlag = false;          //indicate dialog not closed
    final Object [] optionsArr;
    if(button1Obj != null)
    {    //first button object given
              //if both button objects given then build options
              // array with both buttons; otherwise just first button:
      if(button2Obj != null)
        optionsArr = new Object [] { button1Obj, button2Obj };
      else
        optionsArr = new Object [] { button1Obj };
    }
    else
    {    //first button object not given
              //if only second button object given then build options
              // with second button; otherwise use "Close" button:
      if(button2Obj != null)
        optionsArr = new Object [] { button2Obj };
      else
        optionsArr = new Object [] { "Close" };
    }
                   //create dialog and put this panel inside of it:
    hostDialogObj = new IstiDialogPopup(parentComponent,this,
                                           titleStr,optionsArr,0,modalFlag);
         //add listener to clear dialog handle after window closed:
    hostDialogObj.addWindowListener(new WindowAdapter()
        {
          public void windowClosed(WindowEvent evtObj)
          {
            hostDialogObj = null;
          }
        });
    if(scrollPaneObj != null)     //if scrollpane OK then give it init focus
      hostDialogObj.setInitialFocusComponent(scrollPaneObj);
         //set size for dialog; add to bottom to account for "Close" button:
    hostDialogObj.setSize(xSize-5,ySize+95);
    hostDialogObj.setLocationRelativeTo(parentComponent);  //recenter dialog
    hostDialogObj.setResizable(true);       //make dialog resizable
         //this work-around is needed under Java 1.3 to get the panel
         // to display in the dialog (also needed under Java 1.4 if
         // the dialog is modal):
    SwingUtilities.invokeLater(new Runnable()
        {
          public void run()
          {                  //resize dialog after it is displayed:
            try { Thread.sleep(100); }      //slight delay seems to help
            catch(InterruptedException ex) {}
            hostDialogObj.setSize(xSize,ySize+100);
            revalidate();                   //revalidate and repaint
            hostDialogObj.validate();       // to make sure panel data
            hostDialogObj.repaint();        // gets displayed OK
          }
        });
                   //show dialog; save and return selection:
    return (userInputValueObj=hostDialogObj.show());
  }

  /**
   * Shows this 'ViewHTMLPanel' in a dialog window with the given button
   * object (if not given then a "Close" button will be used).
   * If 'modalFlag' is true then this method will not return
   * until the dialog window is dismissed.
   * @param parentComponent the parent component for the dialog.
   * @param titleStr the title string for the dialog.
   * @param modalFlag true for a modal dialog, false for a modeless dialog
   * (that allows other windows to be active at the same time).
   * @param button1Obj first button object to use (can be a button
   * component or a string), or null for none.
   * @return The Object chosen by the user (UNINITIALIZED_VALUE if the
   * user has not yet made a choice, or null if the user closed the window
   * without making a choice).
   */
  public Object showInDialog(Component parentComponent, String titleStr,
                                       boolean modalFlag, Object button1Obj)
  {
    return showInDialog(parentComponent,titleStr,modalFlag,button1Obj,null);
  }

  /**
   * Shows this 'ViewHTMLPanel' in a dialog window with a close
   * button.  If 'modalFlag' is true then this method will not return
   * until the dialog window is dismissed.
   * @param parentComponent the parent component for the dialog.
   * @param titleStr the title string for the dialog.
   * @param modalFlag true for a modal dialog, false for a modeless dialog
   * (that allows other windows to be active at the same time).
   */
  public void showInDialog(Component parentComponent, String titleStr,
                                                          boolean modalFlag)
  {
    showInDialog(parentComponent,titleStr,modalFlag,null,null);
  }

  /**
   * Shows this 'ViewHTMLPanel' in a modal dialog window with a close
   * button.  This method does not return until the dialog window is
   * dismissed.
   * @param parentComponent the parent component for the dialog.
   * @param titleStr the title string for the dialog.
   */
  public void showInDialog(Component parentComponent, String titleStr)
  {
    showInDialog(parentComponent,titleStr,true,null,null);
  }

  /**
   * Closes the dialog object hosting this panel.
   */
  public void closeDialog()
  {
    dialogClosedFlag = true;           //indicate dialog closed
    if(hostDialogObj != null)                    //if dlg obj avail then
      hostDialogObj.waitForDialogVisible(5000);  //wait for visible
    final JDialog dlgObj;
    if((dlgObj=getHostDialog()) != null)
      dlgObj.dispose();      //if dialog fetched OK then close it
  }

  /**
   * Sets the URL of the currently viewed page.  The URL is optionally checked
   * to see if it's a remote or non-HTML URL (resulting in a browser
   * launch or 'remoteLinkCallBackObj' invocation if so).
   * @param urlObj URL of page to display.
   * @param historyFlag true to save new URL to history Vector.
   * @return true if successful, false if error.
   * @see #setCheck(boolean)
   */
  public final boolean setPage(URL urlObj, boolean historyFlag)
  {
//    System.out.println("SetPage:  " + urlObj);
    if(urlObj == null)            //if no URL then
      return true;                //just return
    final String str;
    if(checkUrlFlag && !(str=urlObj.toString()).startsWith("jar:") &&
                    (UtilFns.isURLAddress(str) || str.indexOf(".htm") <= 0))
    {    //not a 'jar' URL and is "http://"-type or not ".html" page
      if(remoteLinkCallBackObj == null)     //if no callback then
        return doLaunchBrowser(urlObj);     //launch browser to view URL
                   //invoke call-back with link string:
      return remoteLinkCallBackObj.callBackFlagMethod(str);
    }
    return doSetPage(urlObj,historyFlag);
  }

  /**
   * Sets the URL of the currently viewed page.  The URL is optionally checked
   * to see if it's a remote or non-HTML URL (resulting in a browser
   * launch or 'remoteLinkCallBackObj' invocation if so).  The
   * new URL is saved in the history Vector.
   * @param urlObj URL of page to display.
   * @return true if successful, false if error.
   * @see #setCheck(boolean)
   */
  public boolean setPage(URL urlObj)
  {
    return setPage(urlObj,true);
  }

  /**
   * Sets the URL of the currently viewed page.  The URL is not checked
   * to see if it's a remote or non-HTML URL (no browser launch or
   * 'remoteLinkCallBackObj' invocation).
   * @param urlObj URL of page to display.
   * @param historyFlag true to save new URL to history Vector.
   * @return true if successful, false if error.
   */
  public final boolean doSetPage(URL urlObj, boolean historyFlag)
  {
    if (!checkUrlFlag)  // if not checking for non-HTML
    {
      // set to the default cursor
      final Cursor defaultCursor = Cursor.getPredefinedCursor(Cursor.DEFAULT_CURSOR);
      if (defaultCursor != editorPaneObj.getCursor())
      {
        editorPaneObj.setCursor(defaultCursor);
      }
    }
    if(urlObj == null)            //if no URL then
      return true;                //just return
    try
    {
      if(currentUrlObj == null || !urlObj.equals(currentUrlObj))
      {  //new URL requested
              //check if URL contains a ref with encoded chars (i.e. "%20"):
        final String refStr;
        int p;
        if((refStr=urlObj.getRef()) != null && (p=refStr.indexOf('%')) > 0 &&
                                                    p < refStr.length()-2 &&
                                    Character.isDigit(refStr.charAt(p+1)) &&
                                      Character.isDigit(refStr.charAt(p+2)))
        {     //URL reference contains a "%##" instance
                             //set new page using decoded reference:
          editorPaneObj.setPage(UtilFns.setURLReference(urlObj,
                                             URLRefDecoder.decode(refStr)));
        }
        else
          editorPaneObj.setPage(urlObj);         //set new page
        if(historyFlag && currentUrlObj != null) //if flag & not null then
          addHistory(currentUrlObj);         //add old URL to history
        currentUrlObj = urlObj;                  //save URL for next time
              //if location bar enabled then update it:
        if(locationTextFieldObj != null)
          locationTextFieldObj.setText(urlObj.toString());
      }
      return true;                               //return OK flag
    }
    catch(Exception ex)
    {
      System.err.println("Unable to display page (\"" + urlObj +
                                                             "\"):  " + ex);
      if(currentUrlObj != null)
      {
        try
        {        //try to restore previous page:
          editorPaneObj.setPage(currentUrlObj);
        }
        catch(Exception e) {}
      }
    }
    return false;
  }

  /**
   * Sets the URL of the currently viewed page.  The URL is not checked
   * to see if it's a remote or non-HTML URL (no browser launch or
   * 'remoteLinkCallBackObj' invocation).  The new URL is saved in
   * the history Vector.
   * @param urlObj URL of page to display.
   * @return true if successful, false if error.
   */
  public final boolean doSetPage(URL urlObj)
  {
    return doSetPage(urlObj,true);
  }

  /**
   * Sets the viewed page to a reference (or anchor) on the current
   * page.  The new URL is saved in the history Vector.
   * @param refStr the reference (or anchor) to be viewed.
   * @return true if successful, false if error.
   */
  public boolean setPageRef(final String refStr)
  {
    if(currentUrlObj != null)
    {    //current URL object has been set
              //set reference for URL and enter it:
      return setPage(UtilFns.setURLReference(currentUrlObj,refStr));
    }
    return false;
  }

  /**
   * Displays the given text message in the viewer panel.
   * @param msgStr message text to display.
   * @return true if successful; false if error.
   */
  public boolean showMessageText(String msgStr)
  {
    try
    {
      if(currentUrlObj != null)
      {  //current URL object exists
        if(historyVec.size() <= 0 ||
                            !currentUrlObj.equals(historyVec.lastElement()))
        {     //history list empty or last item not same as current URL
          addHistory(currentUrlObj);         //add URL to history
        }
        currentUrlObj = null;          //clear current URL object
      }
                                  //read message text into panel:
      editorPaneObj.read(new ByteArrayInputStream(
                      ((msgStr != null) ? msgStr.getBytes() : new byte[0])),
                                                        new HTMLDocument());
    }
    catch(Exception ex)
    {    //some kind of exception error; return false
    }
    return false;
  }

  /**
   * Clears the history buffer used by the "Back" button.
   */
  public void clearHistory()
  {
    historyVec.clear();
    processHistoryChange();
  }

  /**
   * Clears the history buffer used by the "Back" button and resets
   * the page back to the original URL.
   */
  public void resetViewer()
  {
    clearHistory();
    setPage(originalUrlObj,false);
  }

  /**
   * Launches a web browser pointing to the given page.
   * @param targetUrlObj the URL object for the page.
   * @return true if no errors are detected.
   */
  protected boolean doLaunchBrowser(URL targetUrlObj)
  {
         //clear any reference (anchor) and get string version of URL:
    String urlStr = UtilFns.setURLReference(targetUrlObj,null).toString();
    if(urlStr.startsWith("jar:"))
    {      //file is inside of jar; try plain filename
      int p;         //find last '/' separator
      if((p=urlStr.lastIndexOf('/')) < 0 || p >= urlStr.length()-1)
      {    //separator not found; show message and exit
        System.err.println("Unable to launch browser to view \"" +
                                                     urlStr + "\"");
        return false;
      }
      urlStr = urlStr.substring(p+1);    //just use filename
      InputStream tempStmObj;
      try
      {         //attempt to open as a local file:
        if((tempStmObj=new FileInputStream(urlStr)) != null)
          tempStmObj.close();  //if stream opened OK then close it
      }
      catch(Exception ex)
      {
        tempStmObj = null;
      }
      if(tempStmObj == null)
      {      //unable to open stream to URL; show message and exit
        System.err.println("Unable to access \"" + urlStr +
                                      "\" before launching browser");
        return false;
      }
    }
    if(launchBrowserObj == null)                 //if not yet created then
      launchBrowserObj = new LaunchBrowser();    //create launch object
    if(launchBrowserObj.showURL(urlStr,"ViewHTMLBrowser"))
    {      //success code returned; close ViewHTMLPanel
      closeDialog();
      return true;
    }
    else
    {      //error code returned; show message
      System.err.println("Error launching browser:  " +
                     ViewHTMLPanel.this.launchBrowserObj.getErrorMessage());
    }
    return false;
  }

  /**
   * Overridden version that also requests focus onto the dialog object
   * the hosts the panel (if currently visible).
   */
  public void requestFocus()
  {
    super.requestFocus();
    final JDialog dlgObj;
    if((dlgObj=getHostDialog()) != null)
      dlgObj.requestFocus();
  }

  /**
   * Sets if the URL should be checked if non-HTML.
   * @param b true if the page should be checked, false otherwise.
   * @see #setPage(URL)
   * @see #setPage(URL, boolean)
   */
  public void setCheck(boolean b)
  {
    checkUrlFlag = b;
  }

  /**
   * Sets if the "Back" button should be disabled when the history is empty.
   * @param b true if the "Back" button should be disabled when the history is
   * empty, false otherwise.
   */
  public void setDisableBackWhenEmpty(boolean b)
  {
    disableBackWhenEmptyFlag = b;
    if (disableBackWhenEmptyFlag)
    {
      processHistoryChange();
    }
    else if (backButton != null)
    {
      backButton.setEnabled(true);
    }
  }

  /**
   * Sets the visibility of this panel and the dialog object
   * hosting this panel.
   * @param flgVal true for visible.
   */
  public void setDialogVisible(boolean flgVal)
  {
    super.setVisible(flgVal);
    final JDialog dlgObj;
    if((dlgObj=getHostDialog()) != null)
      dlgObj.setVisible(flgVal);
  }

  /**
   * Overridden version that only returns 'true' if the dialog object
   * hosting this panel is visible.
   * @return true if the panel is visible.
   */
  public boolean isVisible()
  {
    final JDialog dlgObj;
    if((dlgObj=getHostDialog()) != null && dlgObj.isVisible())
      return super.isVisible();
    return false;
  }

  /**
   * Returns the 'JDialog' object that is hosting this panel.
   * @return the 'JDialog' host, or null if none.
   */
  public JDialog getHostDialog()
  {
    if(hostDialogObj != null)
      return hostDialogObj.getDialogObj();
    Component compObj = ViewHTMLPanel.this;
    do
    {         //find parent JDialog component
      if((compObj=compObj.getParent()) instanceof JDialog)
        return (JDialog)compObj;       //if found then return it
    }
    while(compObj != null);  //loop unless no parent found
    return null;             //if none found then return null
  }

  /**
   * Returns the value the user has input in response to the
   * 'showInDialog()' display.
   * @return the Object the user specified, or null if none available.
   */
  public Object getInputValue()
  {
    return userInputValueObj;
  }

  /**
   * Returns the dialog-closed flag.  The flag is set true if the dialog
   * is closed via the 'closeDialog()' method, which can happen after
   * a browser is launched.
   * @return true if the dialog has been closed via the 'closeDialog()'
   * method; false if not.
   */
  public boolean getDialogClosedFlag()
  {
    return dialogClosedFlag;
  }

  /**
   * Adds a URL to the history.
   * @param urlObj the URL object.
   */
  protected void addHistory(URL urlObj)
  {
    historyVec.add(urlObj);
    processHistoryChange();
  }

  /**
   * Processes a history change.
   */
  protected void processHistoryChange()
  {
    if (backButton != null && disableBackWhenEmptyFlag)
    {
      final boolean b = historyVec.size() > 0;
      if (backButton.isEnabled() != b)
      {
        backButton.setEnabled(b);
      }
    }
  }

  /**
   * Removes the last URL from the history.
   * @return the removed URL or null if none.
   */
  protected URL removeHistory()
  {
    final int index = historyVec.size() - 1;
    if (index >= 0)
    {
      return removeHistory(index);
    }
    return null;
  }

  /**
   * Removes a URL from the history.
   * @param index the index of the URL in the history.
   * @return the removed URL or null if none.
   */
  protected URL removeHistory(int index)
  {
    final Object o = historyVec.remove(index);
    processHistoryChange();
    if (o instanceof URL)
    {
      return (URL) o;
    }
    return null;
  }
}
