//ViewHTMLPanelHandler.java:  Handler for simple HTML viewer panels
//                            (capable of viewing HTML files that reside
//                            in a 'jar' archive).
//
//  9/23/2004 -- [KF]  Initial version.
//  4/26/2005 -- [ET]  Modified 'buildShowViewerDialog()' method to deal
//                     cleanly with the case where the given file is not a
//                     local HTML file and is viewed via a launched browser.
//

package com.isti.util.gui;

import java.util.Hashtable;
import java.net.URL;
import java.awt.Component;
import com.isti.util.UtilFns;
import com.isti.util.FileUtils;
import com.isti.util.LogFile;
import com.isti.util.LaunchBrowser;

/**
 * Class ViewHTMLPanel implements a handler for simple HTML viewer panels
 * (capable of viewing HTML files that reside in a 'jar' archive).
 */
public class ViewHTMLPanelHandler
{
  /** The default for the "showBackLaunchFiles" option for the constructor. */
  public static final String[] DEFAULT_SHOW_BACK_LAUNCH_FILES =
  {
    "Settings"
  };
  private static final String SHOW_FILE = "ShowFile";
  private static final int SHOW_FILE_LEN = SHOW_FILE.length();

  //parameters
  private final IstiDialogUtil dialogUtilObj;
  private final LaunchBrowser launchBrowserObj;
  private final String[] showBackLaunchFiles;

  //view HTML panel table
  private Hashtable viewHTMLPanelTable = new Hashtable();

  /**
   * Creates a handler for simple HTML viewer panels.
   * @param dialogUtilObj the dialog utility object for popup messages or
   * null for none.
   * @param launchBrowserObj an existing 'LaunchBrowser' object, or
   * null to have this class create its own.
   *
   * By default any "Settings" files will automatically show the "Back" and
   * "Launch Browser" buttons.
   */
  public ViewHTMLPanelHandler(
      IstiDialogUtil dialogUtilObj,LaunchBrowser launchBrowserObj)
  {
    this(dialogUtilObj,launchBrowserObj,DEFAULT_SHOW_BACK_LAUNCH_FILES);
  }

  /**
   * Creates a handler for simple HTML viewer panels.
   * @param dialogUtilObj the dialog utility object for popup messages or
   * null for none.
   * @param launchBrowserObj an existing 'LaunchBrowser' object, or
   * null to have this class create its own.
   * @param showBackLaunchFiles files to automatically show "Back" and
   * "Launch Browser" buttons or null for none.
   */
  public ViewHTMLPanelHandler(
      IstiDialogUtil dialogUtilObj,LaunchBrowser launchBrowserObj,
      String[] showBackLaunchFiles)
  {
    //save parameters
    this.dialogUtilObj = dialogUtilObj;
    this.launchBrowserObj = (launchBrowserObj != null) ?
                                     launchBrowserObj : new LaunchBrowser();
    this.showBackLaunchFiles = showBackLaunchFiles;
  }

  /**
   * Displays an HTML-viewer-panel-based dialog window.  If the HTML-viewer
   * panel object has not yet been created then one will be built,
   * otherwise the given panel will be displayed.
   * @param vPanelObj the HTML-viewer panel to use, or null to build
   * a new one.
   * @param fNameStr the name of the HTML file to display.
   * @param parentComponent the parent component for the dialog.
   * @param titleStr the title to use.
   * @param modalFlag true for a modal dialog, false for a modeless dialog
   * (that allows other windows to be active at the same time).
   * @param button1Obj first button object to use (can be a button
   * component or a string), or null for none.
   * @param button2Obj second button object to use (can be a button
   * component or a string), or null for none.
   * @param showBackLaunchFlag true to show "Back" and "Launch Browser"
   * buttons.
   * @param viewRefStr the reference (anchor) to display, null for none.
   * @return If vPanelObj==null then the handle to the new HTML-viewer
   * panel, otherwise vPanelObj.
   */
  public ViewHTMLPanel buildShowViewerDialog(ViewHTMLPanel vPanelObj,
      String fNameStr,Component parentComponent,String titleStr,
      boolean modalFlag,Object button1Obj,Object button2Obj,
      boolean showBackLaunchFlag,String viewRefStr)
  {
    try
    {
      if(vPanelObj == null)
      {  //panel not yet created; build it now
        if((vPanelObj=buildViewerDialog(
                           fNameStr,showBackLaunchFlag,viewRefStr)) == null)
        {     //file not found; log and show error message
          final String msgStr = "Unable to open " +
                                titleStr + " file \"" + fNameStr + "\"";
          if (dialogUtilObj != null)
          {
            dialogUtilObj.popupErrorMessage(
                               LogFile.getGlobalLogObj(false),msgStr,false);
          }
          return null;
        }
              //check if dialog has been closed (which can happen if
              // the given file was displayed via launch-browser):
        if(!vPanelObj.getDialogClosedFlag())
        {     //not already closed; show panel in dialog
          vPanelObj.showInDialog(parentComponent,titleStr,modalFlag,
                                                     button1Obj,button2Obj);
        }
        else  //dialog closed
          vPanelObj = null;       //clear handle to panel
      }
      else    //panel already created
      {                      //clear history and reset to initial page:
        vPanelObj.resetViewer();
        if(viewRefStr != null)                        //if ref given
          vPanelObj.setPageRef(viewRefStr);           //then set view
        if(!vPanelObj.isVisible())
        {     //not already displayed; show dialog
          vPanelObj.showInDialog(parentComponent,titleStr,modalFlag,
                                 button1Obj,button2Obj);
        }
        else                           //if already showing then
          vPanelObj.requestFocus();    //request focus on dialog
      }
    }
    catch(Exception ex)
    {
      LogFile.getGlobalLogObj(true).warning(
          "Error displaying "+titleStr+" file \""+fNameStr+"\":  "+ex);
      LogFile.getGlobalLogObj(true).warning(UtilFns.getStackTraceString(ex));
    }
    return vPanelObj;
  }

  /**
   * Gets the file name for the specified menu name.
   * @param menuNameStr the menu name string.
   * @return the file name.
   */
  public static String getFileNameForMenuName(String menuNameStr)
  {
    if (isShowFileMenuName(menuNameStr))
      return menuNameStr.substring(SHOW_FILE_LEN);
    return menuNameStr;
  }

  /**
   * Determines if the menu name is a show file menu name.
   * @param menuNameStr the menu name string.
   * @return true if the name string is a show file menu name.
   */
  public static boolean isShowFileMenuName(String menuNameStr)
  {
    return menuNameStr.startsWith(SHOW_FILE);
  }

  /**
   * Displays the specified file in a dialog window.
   * @param nameStr the file name string or menu name string.
   * @param titleStr the title to use.
   * @param parentComponent the parent component for the dialog.
   */
  public void showFileDialog(String nameStr,String titleStr,
                             Component parentComponent)
  {
    //make sure the file name isn't a menu name
    final String fNameStr = getFileNameForMenuName(nameStr);
    showFileDialog(fNameStr,titleStr,parentComponent,null);
  }

  /**
   * Displays the specified HTML file in a dialog window.
   * @param fNameStr the name of the HTML file to display.
   * @param titleStr the title to use.
   * @param parentComponent the parent component for the dialog.
   * @param viewRefStr the reference (anchor) to display, null for none.
   */
  public void showFileDialog(
      String fNameStr,String titleStr,Component parentComponent,
      String viewRefStr)
  {
    showFileDialog(fNameStr,titleStr,parentComponent,viewRefStr,false);
  }

  /**
   * Displays the specified HTML file in a dialog window.
   * @param fNameStr the name of the HTML file to display.
   * @param titleStr the title to use.
   * @param parentComponent the parent component for the dialog.
   * @param viewRefStr the reference (anchor) to display, null for none.
   * @param showBackLaunchFlag true to show "Back" and "Launch Browser"
   * buttons.
   */
  public void showFileDialog(
      String fNameStr,String titleStr,Component parentComponent,
      String viewRefStr,boolean showBackLaunchFlag)
  {
    ViewHTMLPanel vPanelObj = (ViewHTMLPanel)viewHTMLPanelTable.get(fNameStr);
    //show dialog window; build it if necessary:
    vPanelObj = buildShowViewerDialog(
        vPanelObj,fNameStr,parentComponent,titleStr,false,null,null,
        showBackLaunchFlag,viewRefStr);
    if(vPanelObj != null)
      viewHTMLPanelTable.put(fNameStr,vPanelObj);
  }

  /**
   * Creates an HTML-viewer-panel-based dialog window.
   * @param fNameStr the name of the HTML file to display.
   * @param showBackLaunchFlag true to show "Back" and "Launch Browser"
   * buttons.
   * @param initialRefStr the initial reference (anchor) to display in
   * the given file, or null for none.
   * @return A new 'ViewHTMLPanel' object, or null if the given HTML file
   * could not be found.
   */
  protected ViewHTMLPanel buildViewerDialog(String fNameStr,
      boolean showBackLaunchFlag,String initialRefStr)
  {
    //if showing back and launch buttons is currently disabled
    if (!showBackLaunchFlag && showBackLaunchFiles != null)
    {
      for (int i = 0; i < showBackLaunchFiles.length; i++)
      {
        //if settings file
        if (fNameStr.indexOf(showBackLaunchFiles[i]) >= 0)
          //show the lauch and back buttons
          showBackLaunchFlag = true;
      }
    }

    final URL urlObj;        //get URL to file:
    if((urlObj=FileUtils.fileMultiOpenInputURL(fNameStr)) != null)
    {    //file found; build viewing panel
      return new ViewHTMLPanel(
          urlObj,750,650,launchBrowserObj,showBackLaunchFlag,showBackLaunchFlag,
          initialRefStr);
    }
    return null;
  }
}
