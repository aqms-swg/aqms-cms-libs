//AbstractValidatedDocument.java:  Extension of 'PlainDocument' that
//implements the validating of input characters.
//
//=====================================================================
//Copyright (C) 2013 Instrumental Software Technologies, Inc.
//
//Redistribution and use in source and binary forms, with or without
//modification, are permitted provided that the following conditions
//are met:
//1. Redistributions of source code, or portions of this source code,
//must retain the above copyright notice, this list of conditions
//and the following disclaimer.
//2. Redistributions in binary form must reproduce the above copyright
//notice, this list of conditions and the following disclaimer in
//the documentation and/or other materials provided with the
//distribution.
//3. All advertising materials mentioning features or use of this
//software must display the following acknowledgment:
//"This product includes software developed by Instrumental
//Software Technologies, Inc. (http://www.isti.com)"
//4. If the software is provided with, or as part of a commercial
//product, or is used in other commercial software products the
//customer must be informed that "This product includes software
//developed by Instrumental Software Technologies, Inc.
//(http://www.isti.com)"
//5. The names "Instrumental Software Technologies, Inc." and "ISTI"
//must not be used to endorse or promote products derived from
//this software without prior written permission. For written
//permission, please contact "info@isti.com".
//6. Products derived from this software may not be called "ISTI"
//nor may "ISTI" appear in their names without prior written
//permission of Instrumental Software Technologies, Inc.
//7. Redistributions of any form whatsoever must retain the following
//acknowledgment:
//"This product includes software developed by Instrumental
//Software Technologies, Inc. (http://www.isti.com/)."
//THIS SOFTWARE IS PROVIDED BY INSTRUMENTAL SOFTWARE
//TECHNOLOGIES, INC. "AS IS" AND ANY EXPRESSED OR IMPLIED
//WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES
//OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
//DISCLAIMED.  IN NO EVENT SHALL INSTRUMENTAL SOFTWARE TECHNOLOGIES,
//INC. OR ITS CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
//INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
//(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
//SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
//HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
//STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
//ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED
//OF THE POSSIBILITY OF SUCH DAMAGE.
//=====================================================================
//A current version of the software can be found at
//http://www.isti.com
//Bug reports and comments should be directed to
//Instrumental Software Technologies, Inc. at info@isti.com
//=====================================================================
//

package com.isti.util.gui.textvalidator;

import javax.swing.text.AttributeSet;
import javax.swing.text.BadLocationException;
import javax.swing.text.PlainDocument;

import com.isti.util.UtilFns;

/**
 * Class AbstractValidatedDocument is an extension of 'PlainDocument' that
 * implements the validating of input characters.
 */
public abstract class AbstractValidatedDocument extends PlainDocument implements
    AllowedCharactersConstants {
  /**
   * Gets the allowed flag.
   * @return true to allow only the special characters, false to not allow the
   *         special characters.
   * @see getSpecialChars()
   */
  public abstract boolean getAllowedFlag();

  /**
   * Gets the number of columns.
   * @return the number of columns or -1 for the default.
   */
  public abstract int getColumns();

  /**
   * Gets the maximum number of characters.
   * @return the maximum number of characters or 0 if none.
   */
  public abstract int getMaxNumChars();

  /**
   * Gets the allowed (or not allowed) special characters. If there are no
   * special characters then all characters are allowed.
   * @return the allowed characters or null for none.
   * @see getAllowedFlag()
   */
  public abstract String getSpecialChars();

  /**
   * Returns the text validator.
   * @return the text validator or null if none.
   */
  public abstract TextValidator getTextValidator();

  /**
   * Inserts data into the document, validating it along the way.
   * @param insOffs the starting offset (greater than or equal to zero).
   * @param insStr the string to insert (empty and null strings are ignored).
   * @param aSet the attributes for the inserted content.
   * @exception BadLocationException if the given position is not a valid
   *              position within the document.
   */
  public void insertString(int insOffs, String insStr, AttributeSet aSet)
      throws BadLocationException {
    int p, q, dataLen, offs;
    char ch, newch;
    final int insLen;
    if (insStr == null || (insLen = insStr.length()) <= 0)
      return; // if null or empty string then just return

    String cmpString = insStr;
    final char decimalSeparator = UtilFns.getDecimalSeparator();
    final boolean numericFlag = isNumeric();
    if (numericFlag) {
      // if decimal separator is not '.'
      if (decimalSeparator != '.') {
        // replace '.' with the decimal separator
        insStr = insStr.replace('.', decimalSeparator);
        // but use '.' for checks
        cmpString = cmpString.replace(decimalSeparator, '.');
      }
    }

    dataLen = getLength(); // get length of current data
    final int maxNumChars = getMaxNumChars();
    if (maxNumChars > 0 && dataLen + insLen > maxNumChars)
      return; // if length limit and too long then just return

    final String dataStr;
    if (insOffs >= 0 && insOffs <= dataLen) { // given offset is valid
      offs = insOffs; // accept value
      // create string with new data inserted and lower-case:
      dataStr = (getText(0, offs) + insStr + getText(offs, dataLen - offs))
          .toLowerCase();
      dataLen = dataStr.length(); // get new length
    } else { // given offset is not valid
      dataStr = insStr; // just take original string
      offs = dataLen; // put in valid offset value
    }

    final String specialChars = getSpecialChars();
    // if string of allowed characters is not empty
    if (specialChars != null && specialChars.length() > 0) {
      final boolean replaceFlag = isReplace();
      p = 0;
      do { // for each data character; check if matches allowed character
        ch = cmpString.charAt(p);
        if (!isAllowed(ch)) { // if character is not allowed
          if (!replaceFlag) { // if not trying to replace characters
            break; // if not allowed character then exit loop
          }
          newch = ch;
          if (Character.isLowerCase(ch)) { // if lower case
            newch = Character.toUpperCase(ch); // try upper case
          } else if (Character.isUpperCase(ch)) { // if upper case
            newch = Character.toLowerCase(ch); // try lower case
          }
          if (newch != ch && isAllowed(newch)) { // if new character is allowed
            // replace character with new character
            insStr = insStr.replace(ch, newch);
            cmpString = cmpString.replace(ch, newch);
          } else {
            break; // if still not allowed character then exit loop
          }
        }
        if (numericFlag) { // data must be valid numeric
          // if sign character then it must be at the beginning or
          // it must be after an 'e' and followed by either
          // nothing or a number:
          if ((ch == '-' || ch == '+')
              && offs + p > 0
              && !(dataStr.substring(offs + p - 1, offs + p).equalsIgnoreCase(
                  "e") && (offs + p + 1 >= dataLen || INTEGER_CHARS
                  .indexOf(dataStr.charAt(offs + p + 1)) >= 0))) {
            break;
          }
          // if a decimal separator then there must not now be two of them:
          if (ch == decimalSeparator
              && (q = dataStr.indexOf(decimalSeparator)) >= 0 && q < dataLen
              && dataStr.indexOf(decimalSeparator, q + 1) >= 0) {
            break;
          }
          // if an 'e' then it must not be at the beginning and
          // it must be preceded by a number and there must not
          // now be two of them:
          if ((ch == 'e' || ch == 'E')
              && (offs + p == 0 || INTEGER_CHARS.indexOf(dataStr.charAt(offs
                  + p - 1)) < 0)
              || ((q = dataStr.indexOf('e')) >= 0 && q < dataLen && dataStr
                  .indexOf('e', q + 1) >= 0)) {
            break;
          }
        }
        ++p; // increment to next data character
      } while (p < insLen); // loop if more data
    } else
      // string of allowed characters is empty
      p = insLen; // setup to pass through entire string

    if (p > 0) { // new characters were allowed
      final TextValidator textValidator = getTextValidator();
      if (textValidator != null) {
        if (!textValidator.isValid(dataStr)) {
          return;
        }
      }
      // insert new characters:
      super.insertString(insOffs, insStr.substring(0, p), aSet);
    }
  }

  /**
   * Determines if the character is allowed.
   * @param ch the character.
   * @return true if the character is allowed, false otherwise.
   */
  protected boolean isAllowed(char ch) {
    return isSpecialChar(ch) == getAllowedFlag();
  }

  /**
   * Determines if the characters represent a numeric value.
   * @return true if numeric value, false otherwise
   */
  public abstract boolean isNumeric();

  /**
   * Determines if trying to replace characters.
   * @return true if trying to replace characters, false otherwise.
   */
  protected boolean isReplace() {
    return isReplace(getSpecialChars(), isNumeric());
  }

  /**
   * Determines if the character is a special character.
   * @param ch the character.
   * @return true if the character is a special character, false otherwise.
   */
  protected boolean isSpecialChar(char ch) {
    final String specialChars = getSpecialChars();
    return specialChars != null && specialChars.indexOf(ch) >= 0;
  }

  /**
   * Sets the text validator.
   * @param tv the text validator or null if none.
   */
  public abstract void setTextValidator(TextValidator tv);

  /**
   * Determines if trying to replace characters.
   * @param specialChars a String of characters allowed (or not) into the
   *          document.
   * @param numericFlag true for valid numeric only, false otherwise.
   * @return true if trying to replace characters, false otherwise.
   */
  protected static boolean isReplace(String specialChars, boolean numericFlag) {
    boolean replaceFlag = false;
    // if not numeric only and there are special characters
    if (!numericFlag && specialChars != null && specialChars.length() > 0) {
      // if not all upper case or not all lower case
      final boolean allUpperCaseFlag = specialChars.indexOf(ALPHA_CAP_CHARS) >= 0;
      final boolean allLowerCaseFlag = specialChars.indexOf(ALPHA_LOW_CHARS) >= 0;
      if (!allUpperCaseFlag || !allLowerCaseFlag) {
        replaceFlag = true; // try to replace characters
      }
    }
    return replaceFlag;
  }
}
