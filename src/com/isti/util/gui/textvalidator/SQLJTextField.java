//SQLJTextField.java:  Extension of 'ValidatedJTextField' which provides
//                     methods to get SQL related values.
//
//=====================================================================
// Copyright (C) 2007 Instrumental Software Technologies, Inc.
//
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions
// are met:
// 1. Redistributions of source code, or portions of this source code,
//    must retain the above copyright notice, this list of conditions
//    and the following disclaimer.
// 2. Redistributions in binary form must reproduce the above copyright
//    notice, this list of conditions and the following disclaimer in
//    the documentation and/or other materials provided with the
//    distribution.
// 3. All advertising materials mentioning features or use of this
//    software must display the following acknowledgment:
//    "This product includes software developed by Instrumental
//    Software Technologies, Inc. (http://www.isti.com)"
// 4. If the software is provided with, or as part of a commercial
//    product, or is used in other commercial software products the
//    customer must be informed that "This product includes software
//    developed by Instrumental Software Technologies, Inc.
//    (http://www.isti.com)"
// 5. The names "Instrumental Software Technologies, Inc." and "ISTI"
//    must not be used to endorse or promote products derived from
//    this software without prior written permission. For written
//    permission, please contact "info@isti.com".
// 6. Products derived from this software may not be called "ISTI"
//    nor may "ISTI" appear in their names without prior written
//    permission of Instrumental Software Technologies, Inc.
// 7. Redistributions of any form whatsoever must retain the following
//    acknowledgment:
//    "This product includes software developed by Instrumental
//    Software Technologies, Inc. (http://www.isti.com/)."
// THIS SOFTWARE IS PROVIDED BY INSTRUMENTAL SOFTWARE
// TECHNOLOGIES, INC. "AS IS" AND ANY EXPRESSED OR IMPLIED
// WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES
// OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
// DISCLAIMED.  IN NO EVENT SHALL INSTRUMENTAL SOFTWARE TECHNOLOGIES,
// INC. OR ITS CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
// INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
// (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
// SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
// HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
// STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
// ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED
// OF THE POSSIBILITY OF SUCH DAMAGE.
//=====================================================================
//  A current version of the software can be found at
//                http://www.isti.com
//  Bug reports and comments should be directed to
//  Instrumental Software Technologies, Inc. at info@isti.com
//=====================================================================
//

package com.isti.util.gui.textvalidator;

import java.sql.Date;
import java.sql.Time;
import java.sql.Timestamp;

/**
 * Class SQLJTextField is an extension of 'ValidatedJTextField' which provides
 * methods to get SQL related values.
 */
public class SQLJTextField
    extends ValidatedJTextField
{
  /**
   * Creates a new 'SQLJTextField' object.
   * @param tv the text validator.
   * @see FilteredDocument
   */
  public SQLJTextField(TextValidator tv)
  {
    super(tv);
  }

  /**
   * Gets the date value represented by this text field.
   * @return the date value.
   * @throws IllegalArgumentException if the date given is not in the JDBC date
   * escape format (yyyy-mm-dd).
   */
  public Date getDateValue()
  {
    return SQLTextUtils.getDateValue(getText());
  }

  /**
   * Gets the time stamp value.
   * @return the Timestamp value or null if error.
   */
  public Timestamp getTimestampValue()
  {
    return SQLTextUtils.getTimestampValue(getText());
  }

  /**
   * Gets the time value represented by this text field.
   * @return the time value.
   * @throws IllegalArgumentException if the date given is not in the JDBC time
   * escape format (hh:mm:ss).
   */
  public Time getTimeValue()
  {
    return SQLTextUtils.getTimeValue(getText());
  }
}
