//SQLTextUtils.java:  Defines utility methods to for SQL text.
//
//=====================================================================
// Copyright (C) 2007 Instrumental Software Technologies, Inc.
//
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions
// are met:
// 1. Redistributions of source code, or portions of this source code,
//    must retain the above copyright notice, this list of conditions
//    and the following disclaimer.
// 2. Redistributions in binary form must reproduce the above copyright
//    notice, this list of conditions and the following disclaimer in
//    the documentation and/or other materials provided with the
//    distribution.
// 3. All advertising materials mentioning features or use of this
//    software must display the following acknowledgment:
//    "This product includes software developed by Instrumental
//    Software Technologies, Inc. (http://www.isti.com)"
// 4. If the software is provided with, or as part of a commercial
//    product, or is used in other commercial software products the
//    customer must be informed that "This product includes software
//    developed by Instrumental Software Technologies, Inc.
//    (http://www.isti.com)"
// 5. The names "Instrumental Software Technologies, Inc." and "ISTI"
//    must not be used to endorse or promote products derived from
//    this software without prior written permission. For written
//    permission, please contact "info@isti.com".
// 6. Products derived from this software may not be called "ISTI"
//    nor may "ISTI" appear in their names without prior written
//    permission of Instrumental Software Technologies, Inc.
// 7. Redistributions of any form whatsoever must retain the following
//    acknowledgment:
//    "This product includes software developed by Instrumental
//    Software Technologies, Inc. (http://www.isti.com/)."
// THIS SOFTWARE IS PROVIDED BY INSTRUMENTAL SOFTWARE
// TECHNOLOGIES, INC. "AS IS" AND ANY EXPRESSED OR IMPLIED
// WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES
// OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
// DISCLAIMED.  IN NO EVENT SHALL INSTRUMENTAL SOFTWARE TECHNOLOGIES,
// INC. OR ITS CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
// INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
// (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
// SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
// HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
// STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
// ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED
// OF THE POSSIBILITY OF SUCH DAMAGE.
//=====================================================================
//  A current version of the software can be found at
//                http://www.isti.com
//  Bug reports and comments should be directed to
//  Instrumental Software Technologies, Inc. at info@isti.com
//=====================================================================
//

package com.isti.util.gui.textvalidator;

import java.sql.Date;
import java.sql.Time;
import java.sql.Timestamp;
import java.util.StringTokenizer;

/**
 * Class SQLTextUtils defines utility methods to for SQL text.
 */
public class SQLTextUtils
{
  /**
   * The default epoch time value.
   * @see getDateValue(String)
   */
  public static long DEFAULT_EPOCH_TIME_VALUE = 0L;
  /**
   * The default time value.
   * @see getTimeValue(String)
   */
  public static Time DEFAULT_TIME_VALUE = new Time(0L);

  /**
   * Gets the date.
   * @param s a String object representing a date in in the format "yyyy-mm-dd".
   * @return the date.
   * @throws IllegalArgumentException if the date given is not in the JDBC date
   * escape format (yyyy-mm-dd).
   */
  public static Date getDateValue(String s)
  {
    return getDateValue(s, DEFAULT_EPOCH_TIME_VALUE);
  }

  /**
   * Gets the date.
   * @param s a String object representing a date in in the format "yyyy-mm-dd".
   * @param defaultEpochTime the epoch time to use if the date text is empty.
   * @return the date.
   * @throws IllegalArgumentException if the date given is not in the JDBC date
   * escape format (yyyy-mm-dd).
   */
  public static Date getDateValue(String s, long defaultEpochTime)
  {
    if (s.length() <= 0)
    {
      return new Date(defaultEpochTime);
    }
    return Date.valueOf(s);
  }

  /**
   * Gets the timestamp value for the specified date and time.
   * @param dateText the date text.
   * @param timeText the time text.
   * @return the Timestamp value or null if error.
   */
  public static Timestamp getTimestampValue(String dateText, String timeText)
  {
    return getTimestampValue(dateText, timeText, DEFAULT_EPOCH_TIME_VALUE);
  }

  /**
   * Gets the timestamp value for the specified date and time.
   * @param dateText the date text.
   * @param timeText the time text.
   * @param defaultEpochTime the epoch time to use if the date text is empty.
   * @return the Timestamp value or null if error.
   */
  public static Timestamp getTimestampValue(
      String dateText, String timeText, long defaultEpochTime)
  {
    final int dateTextLength = dateText.length();
    final int timeTextLength = timeText.length();
    if (dateTextLength <= 0 && timeTextLength <= 0)
    {
      return new Timestamp(defaultEpochTime);
    }
    if (dateTextLength <= 0)
    {
      dateText = new Date(defaultEpochTime).toString();
    }
    if (timeTextLength <= 0)
    {
      timeText = "00:00:00";
    }
    final String s = dateText + " " + timeText;
    try
    {
      return Timestamp.valueOf(s);
    }
    catch (IllegalArgumentException ex)
    {
    }
    return null;
  }

  /**
   * Gets the timestamp value for the specified date and time.
   * @param s the timestamp text.
   * @return the Timestamp value or null if error.
   */
  public static Timestamp getTimestampValue(String s)
  {
    return getTimestampValue(s, DEFAULT_EPOCH_TIME_VALUE);
  }

  /**
   * Gets the timestamp for the specified date and time.
   * @param s the timestamp text.
   * @param defaultEpochTime the epoch time to use if the date text is empty.
   * @return the Timestamp or null if error.
   */
  public static Timestamp getTimestampValue(
      String s, long defaultEpochTime)
  {
    try
    {
      return new TimestampText(s, defaultEpochTime).getTimestampValue();
    }
    catch (Exception ex)
    {
    }
    return null;
  }

  /**
   * Gets the time.
   * @param s a String object representing a time in in the format "hh:mm:ss".
   * @return the time.
   * @throws IllegalArgumentException if the date given is not in the JDBC time
   * escape format (hh:mm:ss).
   */
  public static Time getTimeValue(String s)
  {
    return getTimeValue(s, DEFAULT_TIME_VALUE);
  }

  /**
   * Gets the time.
   * @param s a String object representing a time in in the format "hh:mm:ss".
   * @param defaultTime the time to return if the time text is empty.
   * @return the time.
   * @throws IllegalArgumentException if the date given is not in the JDBC time
   * escape format (hh:mm:ss).
   */
  public static Time getTimeValue(String s, Time defaultTime)
  {
    if (s.length() <= 0)
    {
      return defaultTime;
    }
    return Time.valueOf(s);
  }

  /**
   * Time stamp text.
   */
  public static class TimestampText
  {
    /** The date text. */
    private final String dateText;
    /** The time text. */
    private final String timeText;
    /** The milliseconds text. */
    private final String msText;
    /** The epoch time to use if the date text is empty. */
    private final long defaultEpochTime;

    /**
     * Creates time stamp text.
     * @param s the time stamp text.
     */
    public TimestampText(String s)
    {
      this(s, DEFAULT_EPOCH_TIME_VALUE);
    }

    /**
     * Creates time stamp text.
     * @param s the time stamp text.
     * @param defaultEpochTime the epoch time to use if the date text is empty.
     * @throws IllegalArgumentException if the time stamp text is invalid.
     */
    public TimestampText(String s, long defaultEpochTime) throws
        IllegalArgumentException
    {
      String dateText = "";
      String timeText = "";
      String msText = "";
      if (s != null)
      {
        final StringTokenizer st = new StringTokenizer(
            s, TextSQLTimestampValidator.FIELD_SEPARATOR_CHARS);
        if (st.countTokens() > 3)
        {
          throw new IllegalArgumentException();
        }
        if (st.hasMoreTokens())
        {
          dateText = st.nextToken();
        }
        if (st.hasMoreTokens())
        {
          timeText = st.nextToken();
        }
        if (st.hasMoreTokens())
        {
          msText = st.nextToken();
        }
      }
      this.dateText = dateText;
      this.timeText = timeText;
      this.msText = msText;
      this.defaultEpochTime = defaultEpochTime;
    }

    /**
     * Gets the date text.
     * @return the date text.
     */
    public String getDateText()
    {
      return dateText;
    }

    /**
     * Gets the default epoch time.
     * @return the epoch time to use if the date text is empty.
     */
    public long getDefaultEpochTime()
    {
      return defaultEpochTime;
    }

    /**
     * Gets the milliseconds text.
     * @return the milliseconds text.
     */
    public String getMillisecondsText()
    {
      return msText;
    }

    /**
     * Gets the time stamp value.
     * @return the Timestamp value or null if error.
     */
    public Timestamp getTimestampValue()
    {
      return SQLTextUtils.getTimestampValue(
          dateText, timeText, defaultEpochTime);
    }

    /**
     * Gets the time text.
     * @return the time text.
     */
    public String getTimeText()
    {
      return timeText;
    }
  }
}
