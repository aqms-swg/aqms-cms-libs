//TextNumberValidator.java:  Defines methods to validate text.
//
//=====================================================================
// Copyright (C) 2007 Instrumental Software Technologies, Inc.
//
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions
// are met:
// 1. Redistributions of source code, or portions of this source code,
//    must retain the above copyright notice, this list of conditions
//    and the following disclaimer.
// 2. Redistributions in binary form must reproduce the above copyright
//    notice, this list of conditions and the following disclaimer in
//    the documentation and/or other materials provided with the
//    distribution.
// 3. All advertising materials mentioning features or use of this
//    software must display the following acknowledgment:
//    "This product includes software developed by Instrumental
//    Software Technologies, Inc. (http://www.isti.com)"
// 4. If the software is provided with, or as part of a commercial
//    product, or is used in other commercial software products the
//    customer must be informed that "This product includes software
//    developed by Instrumental Software Technologies, Inc.
//    (http://www.isti.com)"
// 5. The names "Instrumental Software Technologies, Inc." and "ISTI"
//    must not be used to endorse or promote products derived from
//    this software without prior written permission. For written
//    permission, please contact "info@isti.com".
// 6. Products derived from this software may not be called "ISTI"
//    nor may "ISTI" appear in their names without prior written
//    permission of Instrumental Software Technologies, Inc.
// 7. Redistributions of any form whatsoever must retain the following
//    acknowledgment:
//    "This product includes software developed by Instrumental
//    Software Technologies, Inc. (http://www.isti.com/)."
// THIS SOFTWARE IS PROVIDED BY INSTRUMENTAL SOFTWARE
// TECHNOLOGIES, INC. "AS IS" AND ANY EXPRESSED OR IMPLIED
// WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES
// OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
// DISCLAIMED.  IN NO EVENT SHALL INSTRUMENTAL SOFTWARE TECHNOLOGIES,
// INC. OR ITS CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
// INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
// (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
// SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
// HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
// STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
// ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED
// OF THE POSSIBILITY OF SUCH DAMAGE.
//=====================================================================
//  A current version of the software can be found at
//                http://www.isti.com
//  Bug reports and comments should be directed to
//  Instrumental Software Technologies, Inc. at info@isti.com
//=====================================================================
//

package com.isti.util.gui.textvalidator;

/**
 * Class TextNumberValidator defines methods to validate text.
 */
public class TextBasicValidator
    implements TextValidator
{
  /**
   * The allowed flag, true to allow only the special characters, false to not
   * allow the special characters.
   */
  private boolean allowedFlag = true;
  /** The special characters or null for none. */
  private String specialChars = null;
  /** The number of columns or -1 for the default. */
  private int numColumns = -1;
  /** The tool tip text or null if none. */
  private String toolTipText = null;
  /**
   * The numeric flag is true if the characters represent a numeric value,
   * false otherwise.
   */
  private boolean numericFlag = false;
  /** The maximum number of characters or 0 if none. */
  private int maxNumChars = 0;
  /** The allowed number of characters or null if all. */
  private int[] allowedNumChars = null;

  /**
   * Creates a basic text validator.
   */
  public TextBasicValidator()
  {
  }

  /**
   * Creates a basic text validator.
   * This will set the number of columns and the maximum number of characters
   * to the specified amount.
   * @param maxTextLength the maximum number of characters or 0 if none.
   * @see setMaxTextLength
   */
  public TextBasicValidator(int maxTextLength)
  {
    setMaxTextLength(maxTextLength);
  }

  /**
   * Creates a basic text validator.
   * @param allowedNumChars the array of allowed number of characters or null
   * if all.
   */
  public TextBasicValidator(int[] allowedNumChars)
  {
    this.allowedNumChars = allowedNumChars;
    //if allowed number of characters was specified
    if (allowedNumChars != null)
    {
      //determine the maximum
      int numChars;
      int maxTextLength = 0;
      for (int i = 0; i < allowedNumChars.length; i++)
      {
        if ( (numChars = allowedNumChars[i]) > maxTextLength)
        {
          maxTextLength = numChars;
        }
      }
      setMaxTextLength(maxTextLength); //set the maximum
    }
  }

  /**
   * Creates a basic text validator.
   * @param specialChars the the allowed (or not allowed) special characters
   * or null for all.
   * @param numColumns the number of columns or -1 for the default.
   * @param toolTipText the tool tip text or null if none.
   * @param numericFlag true if the characters represent a numeric value,
   * false otherwise.
   * @param maxNumChars the maximum number of characters or 0 if none.
   * @param allowedFlag true to allow only the special characters, false to not
   * allow the special characters.
   */
  public TextBasicValidator(
      String specialChars, int numColumns, String toolTipText,
      boolean numericFlag, int maxNumChars, boolean allowedFlag)
  {
    this.allowedFlag = allowedFlag;
    this.specialChars = specialChars;
    this.numColumns = numColumns;
    this.toolTipText = toolTipText;
    this.numericFlag = numericFlag;
    this.maxNumChars = maxNumChars;
  }

  /**
   * Gets the allowed flag.
   * @return true to allow only the special characters, false to not
   * allow the special characters.
   * @see getSpecialChars()
   */
  public boolean getAllowedFlag()
  {
    return allowedFlag;
  }

  /**
   * Gets the number of columns.
   * @return the number of columns or -1 for the default.
   */
  public int getColumns()
  {
    return numColumns;
  }

  /**
   * Gets the maximum number of characters.
   * @return the maximum number of characters or 0 if none.
   */
  public int getMaxNumChars()
  {
    return maxNumChars;
  }

  /**
   * Gets the allowed (or not allowed) special characters.
   * If there are no special characters then all characters are allowed.
   * @return the allowed characters or null for none.
   * @see getAllowedFlag()
   */
  public String getSpecialChars()
  {
    return specialChars;
  }

  /**
   * Gets the tool tip text.
   * @return the tool tip text or null if none.
   */
  public String getToolTipText()
  {
    return toolTipText;
  }

  /**
   * Determines if the characters represent a numeric value.
   * @return true if numeric value, false otherwise
   */
  public boolean isNumeric()
  {
    return numericFlag;
  }

  /**
   * Determines if the text is valid.
   * The text may be only part of the entire text entry.
   * @param s the text.
   * @return true if the text is valid, false otherwise.
   */
  public boolean isValid(String s)
  {
    return true;
  }

  /**
   * Determines if the text entry is valid.
   * The text should be the entire text entry.
   * @param s the text.
   * @return true if the text is valid, false otherwise.
   */
  public boolean isValidEntry(String s)
  {
    boolean validFlag = isValid(s);
    if (validFlag && allowedNumChars != null)
    {
      validFlag = false;
      final int numChars = s.length();
      for (int i = 0; i < allowedNumChars.length; i++)
      {
        if (numChars == allowedNumChars[i])
        {
          validFlag = true;
          break;
        }
      }
    }
    return validFlag;
  }

  /**
   * Sets the allowed flag.
   * @param b true to allow only the special characters, false to not
   * allow the special characters.
   * @see getAllowedFlag()
   */
  public void setAllowedFlag(boolean b)
  {
    allowedFlag = b;
  }

  /**
   * Sets the number of columns.
   * @param numColumns the number of columns or -1 for the default.
   * @see getColumns()
   */
  public void setColumns(int numColumns)
  {
    this.numColumns = numColumns;
  }

  /**
   * Sets the maximum number of characters.
   * @param maxNumChars the maximum number of characters or 0 if none.
   * @see getMaxNumChars()
   */
  public void setMaxNumChars(int maxNumChars)
  {
    this.maxNumChars = maxNumChars;
  }

  /**
   * Set the maximum text length.
   * This will set the number of columns and the maximum number of characters
   * to the specified amount.
   * @param maxTextLength the maximum text length.
   * @see setColumns, setMaxNumChars
   */
  public final void setMaxTextLength(int maxTextLength)
  {
    this.numColumns = maxTextLength;
    this.maxNumChars = maxTextLength;
  }

  /**
   * Determines if the characters represent a numeric value.
   * @param b true if the characters represent a numeric value,
   * false otherwise.
   * @see isNumeric()
   */
  public void setNumericFlag(boolean b)
  {
    numericFlag = b;
  }

  /**
   * Sets the allowed (or not allowed) special characters.
   * If there are no special characters then all characters are allowed.
   * @param specialChars the the allowed (or not allowed) special characters
   * or null for all.
   * @see getSpecialChars()
   */
  public void setSpecialChars(String specialChars)
  {
    this.specialChars = specialChars;
  }

  /**
   * Sets the tool tip text.
   * @param toolTipText the tool tip text or null if none.
   * @see getToolTipText()
   */
  public void setToolTipText(String toolTipText)
  {
    this.toolTipText = toolTipText;
  }
}
