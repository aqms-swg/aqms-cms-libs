//TextMinMaxValidator.java:  Defines methods to validate min/max text.
//
//=====================================================================
// Copyright (C) 2007 Instrumental Software Technologies, Inc.
//
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions
// are met:
// 1. Redistributions of source code, or portions of this source code,
//    must retain the above copyright notice, this list of conditions
//    and the following disclaimer.
// 2. Redistributions in binary form must reproduce the above copyright
//    notice, this list of conditions and the following disclaimer in
//    the documentation and/or other materials provided with the
//    distribution.
// 3. All advertising materials mentioning features or use of this
//    software must display the following acknowledgment:
//    "This product includes software developed by Instrumental
//    Software Technologies, Inc. (http://www.isti.com)"
// 4. If the software is provided with, or as part of a commercial
//    product, or is used in other commercial software products the
//    customer must be informed that "This product includes software
//    developed by Instrumental Software Technologies, Inc.
//    (http://www.isti.com)"
// 5. The names "Instrumental Software Technologies, Inc." and "ISTI"
//    must not be used to endorse or promote products derived from
//    this software without prior written permission. For written
//    permission, please contact "info@isti.com".
// 6. Products derived from this software may not be called "ISTI"
//    nor may "ISTI" appear in their names without prior written
//    permission of Instrumental Software Technologies, Inc.
// 7. Redistributions of any form whatsoever must retain the following
//    acknowledgment:
//    "This product includes software developed by Instrumental
//    Software Technologies, Inc. (http://www.isti.com/)."
// THIS SOFTWARE IS PROVIDED BY INSTRUMENTAL SOFTWARE
// TECHNOLOGIES, INC. "AS IS" AND ANY EXPRESSED OR IMPLIED
// WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES
// OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
// DISCLAIMED.  IN NO EVENT SHALL INSTRUMENTAL SOFTWARE TECHNOLOGIES,
// INC. OR ITS CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
// INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
// (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
// SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
// HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
// STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
// ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED
// OF THE POSSIBILITY OF SUCH DAMAGE.
//=====================================================================
//  A current version of the software can be found at
//                http://www.isti.com
//  Bug reports and comments should be directed to
//  Instrumental Software Technologies, Inc. at info@isti.com
//=====================================================================
//

package com.isti.util.gui.textvalidator;

import java.lang.reflect.Constructor;

/**
 * Class TextMinMaxValidator defines methods to validate min/max text.
 */
public class TextMinMaxValidator
    extends TextBasicValidator
{

  private final Comparable maxValueObj; //maximum value
  private final Comparable minValueObj; //minimum value

  /**
   * Creates a basic text validator.
   * @param allowedChars the allowed characters or null for all.
   * @param numColumns the number of columns or -1 for the default.
   * @param numericFlag true if the characters represent a numeric value,
   * false otherwise.
   * @param maxNumChars the maximum number of characters or 0 if none.
   * @param min the minimum value or null if none.
   * @param max the maximum value or null if none.
   * @see getToolTipText
   */
  public TextMinMaxValidator(
      String allowedChars, int numColumns,
      boolean numericFlag, int maxNumChars, Comparable min, Comparable max)
  {
    super(allowedChars, numColumns, getToolTipText(min, max),
          numericFlag, maxNumChars, true);
    minValueObj = min;
    maxValueObj = max;
  }

  /**
   * Gets the tool tip text based on the min/max values.
   * @param min the minimum value or null if none.
   * @param max the maximum value or null if none.
   * @return the tool tip text or null if none.
   */
  public final static String getToolTipText(Comparable min, Comparable max)
  {
    if (min == null && max == null)
    {
      return null;
    }
    final StringBuffer sb = new StringBuffer();
    if (min != null)
    {
      sb.append(min);
    }
    if (min != null)
    {
      if (sb.length() > 0)
      {
        sb.append(" , ");
      }
      sb.append(max);
    }
    return sb.toString();
  }

  /**
   * Determines if the text is valid.
   * The text may be only part of the entire text entry.
   * @param s the text.
   * @return true if the text is valid, false otherwise.
   */
  public boolean isValid(String s)
  {
    Comparable compObj = null;
    if (maxValueObj != null)
    { //if max-value object exists
      try
      {
        compObj = getComparable(s, maxValueObj);
        if (compObj != null && compObj.compareTo(maxValueObj) > 0)
          return false; //if converted OK and out of range then reject
      }
      catch (Exception ex)
      {} //if exception error then accept
    }
    if (minValueObj != null)
    { //if min-value object exists
      try
      {
        if (compObj == null ||
            ! (minValueObj.getClass().equals(maxValueObj.getClass())))
        {
          compObj = getComparable(s, minValueObj);
        }
        if (compObj != null && compObj.compareTo(minValueObj) < 0)
          return false; //if converted OK and out of range then reject
      }
      catch (Exception ex)
      {} //if exception error then accept
    }
    return true;
  }

  /**
   * Determines if the text entry is valid.
   * The text should be the entire text entry.
   * @param s the text.
   * @return true if the text is valid, false otherwise.
   */
  public boolean isValidEntry(String s)
  {
    return isValid(s);
  }

  /**
   * Gets the comparable object for the text.
   * @param s the text.
   * @param valueObj the other Comparable value.
   * @return the Comparable object.
   */
  private static Comparable getComparable(String s, Comparable valueObj)
  {
    try
    {
      final Constructor constructor =
          valueObj.getClass().getConstructor(new Class[]
                                             {String.class});
      final Object newInstance = constructor.newInstance(new Object[]
          {s});
      if (newInstance instanceof Comparable)
      {
        return (Comparable) newInstance;
      }
    }
    catch (Exception ex)
    {
    }
    return null;
  }
}
