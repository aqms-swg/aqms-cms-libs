//TextNumberValidator.java:  Defines methods to validate number text.
//
//=====================================================================
// Copyright (C) 2007 Instrumental Software Technologies, Inc.
//
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions
// are met:
// 1. Redistributions of source code, or portions of this source code,
//    must retain the above copyright notice, this list of conditions
//    and the following disclaimer.
// 2. Redistributions in binary form must reproduce the above copyright
//    notice, this list of conditions and the following disclaimer in
//    the documentation and/or other materials provided with the
//    distribution.
// 3. All advertising materials mentioning features or use of this
//    software must display the following acknowledgment:
//    "This product includes software developed by Instrumental
//    Software Technologies, Inc. (http://www.isti.com)"
// 4. If the software is provided with, or as part of a commercial
//    product, or is used in other commercial software products the
//    customer must be informed that "This product includes software
//    developed by Instrumental Software Technologies, Inc.
//    (http://www.isti.com)"
// 5. The names "Instrumental Software Technologies, Inc." and "ISTI"
//    must not be used to endorse or promote products derived from
//    this software without prior written permission. For written
//    permission, please contact "info@isti.com".
// 6. Products derived from this software may not be called "ISTI"
//    nor may "ISTI" appear in their names without prior written
//    permission of Instrumental Software Technologies, Inc.
// 7. Redistributions of any form whatsoever must retain the following
//    acknowledgment:
//    "This product includes software developed by Instrumental
//    Software Technologies, Inc. (http://www.isti.com/)."
// THIS SOFTWARE IS PROVIDED BY INSTRUMENTAL SOFTWARE
// TECHNOLOGIES, INC. "AS IS" AND ANY EXPRESSED OR IMPLIED
// WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES
// OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
// DISCLAIMED.  IN NO EVENT SHALL INSTRUMENTAL SOFTWARE TECHNOLOGIES,
// INC. OR ITS CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
// INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
// (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
// SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
// HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
// STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
// ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED
// OF THE POSSIBILITY OF SUCH DAMAGE.
//=====================================================================
//  A current version of the software can be found at
//                http://www.isti.com
//  Bug reports and comments should be directed to
//  Instrumental Software Technologies, Inc. at info@isti.com
//=====================================================================
//

package com.isti.util.gui.textvalidator;

/**
 * Class TextNumberValidator defines methods to validate number text.
 */
public class TextNumberValidator
    extends TextMinMaxValidator
{
  private static final boolean defaultNumericFlag = true;
  private static final int defaultMaxNumChars = 0;

  /**
   * Creates the number validator.
   * @param isFloatFlag true if floating point number.
   * @param isSignedFlag true if signed number.
   * @param numColumns the number of columns or -1 for the default.
   */
  public TextNumberValidator(
      boolean isFloatFlag, boolean isSignedFlag, int numColumns)
  {
    this(getAllowedNumberChars(isFloatFlag, isSignedFlag),
         numColumns, (Comparable)null, (Comparable)null);
  }

  /**
   * Creates the number validator.
   * @param minNumber the minimum Number value or null if none.
   * The minimum Number value should be an instance of the 'Number' class.
   * @param maxNumber the maximum Number value or null if none.
   * The maximum Number value should be an instance of the 'Number' class.
   * @param numColumns the number of columns or -1 for the default.
   */
  public TextNumberValidator(
      Comparable minNumber, Comparable maxNumber, int numColumns)
  {
    this(getAllowedNumberChars(minNumber, maxNumber), numColumns, minNumber,
         maxNumber);
  }

  /**
   * Creates the number validator.
   * @param minNumber the minimum value.
   * @param maxNumber the maximum value.
   * @param numColumns the number of columns or -1 for the default.
   */
  public TextNumberValidator(double minNumber, double maxNumber, int numColumns)
  {
    this(Double.valueOf(minNumber), Double.valueOf(maxNumber), numColumns);
  }

  /**
   * Creates the number validator.
   * @param minNumber the minimum value.
   * @param maxNumber the maximum value.
   * @param numColumns the number of columns or -1 for the default.
   */
  public TextNumberValidator(long minNumber, long maxNumber, int numColumns)
  {
    this(Long.valueOf(minNumber), Long.valueOf(maxNumber), numColumns);
  }

  /**
   * Creates the number validator.
   * @param allowedChars the allowed characters or null for all.
   * @param numColumns the number of columns or -1 for the default.
   * @param min the minimum value or null if none.
   * @param max the maximum value or null if none.
   */
  public TextNumberValidator(
      String allowedChars, int numColumns, Comparable min,
      Comparable max)
  {
    super(
        allowedChars, numColumns, defaultNumericFlag, defaultMaxNumChars,
        min, max);
  }

  /**
   * Gets the allowed number characters.
   * @param isFloatFlag true if floating point number.
   * @param isSignedFlag true if signed number.
   * @return the allowed number characters.
   */
  public final static String getAllowedNumberChars(
      boolean isFloatFlag, boolean isSignedFlag)
  {
    String allowedChars;
    if (isFloatFlag)
    {
      //unsigned or signed float characters allowed
      allowedChars = isSignedFlag ? SIGNED_EFLOAT_CHARS :
          EFLOAT_CHARS;
    }
    else
    {
      //unsigned or signed characters allowed
      allowedChars = isSignedFlag ? SIGNED_INT_CHARS :
          INTEGER_CHARS;
    }
    return allowedChars;
  }

  /**
   * Gets the allowed number characters.
   * @param minNumber the minimum Number value or null if none.
   * The minimum Number value should be an instance of the 'Number' class.
   * @param maxNumber the maximum Number value or null if none.
   * The maximum Number value should be an instance of the 'Number' class.
   * @return the allowed number characters.
   */
  public final static String getAllowedNumberChars(
      Comparable minNumber, Comparable maxNumber)
  {
    return getAllowedNumberChars(
        isFloat(minNumber, maxNumber), isSigned(minNumber, maxNumber));
  }

  /**
   * Determines if the number can be a floating point value.
   * @param minNumber the minimum Number value or null if none.
   * The minimum Number value should be an instance of the 'Number' class.
   * @param maxNumber the maximum Number value or null if none.
   * The maximum Number value should be an instance of the 'Number' class.
   * @return true if the number can be a floating point value, false otherwise.
   */
  public final static boolean isFloat(Comparable minNumber,
                                      Comparable maxNumber)
  {
    return isFloat(minNumber) || isFloat(maxNumber);
  }

  /**
   * Determines if the number can be a floating point value.
   * @param n the Number value or null if none.
   * The Number value should be an instance of the 'Number' class.
   * @return true if the number can be a floating point value, false otherwise.
   */
  public final static boolean isFloat(Comparable n)
  {
    if (n instanceof Float || n instanceof Double)
    {
      return true;
    }
    return false;
  }

  /**
   * Determines if the number can be a signed value.
   * @param minNumber the minimum Number value or null if none.
   * The minimum Number value should be an instance of the 'Number' class.
   * @param maxNumber the maximum Number value or null if none.
   * The maximum Number value should be an instance of the 'Number' class.
   * @return true if the number can be a signed value, false otherwise.
   */
  public final static boolean isSigned(Comparable minNumber,
                                       Comparable maxNumber)
  {
    //signed if minimum is signed, the maximum does not matter
    return isSigned(minNumber);
  }

  /**
   * Determines if the number can be a signed value.
   * @param n the Number value or null if none.
   * The Number value should be an instance of the 'Number' class.
   * @return true if the number can be a signed value, false otherwise.
   */
  public final static boolean isSigned(Comparable n)
  {
    if (n instanceof Number)
    {
      return ( (Number) n).doubleValue() < 0.0;
    }
    return true;
  }
}
