//TextSQLDateValidator.java:  Defines methods to validate SQL Date text.
//
//=====================================================================
// Copyright (C) 2007 Instrumental Software Technologies, Inc.
//
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions
// are met:
// 1. Redistributions of source code, or portions of this source code,
//    must retain the above copyright notice, this list of conditions
//    and the following disclaimer.
// 2. Redistributions in binary form must reproduce the above copyright
//    notice, this list of conditions and the following disclaimer in
//    the documentation and/or other materials provided with the
//    distribution.
// 3. All advertising materials mentioning features or use of this
//    software must display the following acknowledgment:
//    "This product includes software developed by Instrumental
//    Software Technologies, Inc. (http://www.isti.com)"
// 4. If the software is provided with, or as part of a commercial
//    product, or is used in other commercial software products the
//    customer must be informed that "This product includes software
//    developed by Instrumental Software Technologies, Inc.
//    (http://www.isti.com)"
// 5. The names "Instrumental Software Technologies, Inc." and "ISTI"
//    must not be used to endorse or promote products derived from
//    this software without prior written permission. For written
//    permission, please contact "info@isti.com".
// 6. Products derived from this software may not be called "ISTI"
//    nor may "ISTI" appear in their names without prior written
//    permission of Instrumental Software Technologies, Inc.
// 7. Redistributions of any form whatsoever must retain the following
//    acknowledgment:
//    "This product includes software developed by Instrumental
//    Software Technologies, Inc. (http://www.isti.com/)."
// THIS SOFTWARE IS PROVIDED BY INSTRUMENTAL SOFTWARE
// TECHNOLOGIES, INC. "AS IS" AND ANY EXPRESSED OR IMPLIED
// WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES
// OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
// DISCLAIMED.  IN NO EVENT SHALL INSTRUMENTAL SOFTWARE TECHNOLOGIES,
// INC. OR ITS CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
// INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
// (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
// SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
// HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
// STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
// ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED
// OF THE POSSIBILITY OF SUCH DAMAGE.
//=====================================================================
//  A current version of the software can be found at
//                http://www.isti.com
//  Bug reports and comments should be directed to
//  Instrumental Software Technologies, Inc. at info@isti.com
//=====================================================================
//

package com.isti.util.gui.textvalidator;

import java.sql.Date;
import java.util.GregorianCalendar;

/**
 * Class TextSQLDateValidator defines methods to validate SQL Date text.
 */
public class TextSQLDateValidator
    extends TextBasicValidator
{
  /** Date description text. */
  public static final String DATE_DESCRIPTION_TEXT = "year-month-day";
  /** Date format text. */
  public static final String DATE_FORMAT_TEXT = "yyyy-mm-dd";
  /** Static string containing the separator characters. */
  public static final String SEPARATOR_CHARS = "-";
  /** Static string containing the allowed characters. */
  public static final String ALLOWED_CHARS = INTEGER_CHARS + SEPARATOR_CHARS;
  /** Static string containing the tool tip text. */
  private static final String TOOLTIP_TEXT =
      DATE_DESCRIPTION_TEXT + " (" + DATE_FORMAT_TEXT + ")";
  /** The minimum year. */
  private final int minYear;
  /** The maximum year. */
  private final int maxYear;

  /**
   * Creates the text SQL date validator.
   */
  public TextSQLDateValidator()
  {
    this( -1, -1);
  }

  /**
   * Creates the text SQL date validator.
   * The year in a 'Date' must be between 0 and 8099.
   * @param minYear the minimum year or -1 for no minimum.
   * @param maxYear the maximum year or -1 for no minimum.
   */
  public TextSQLDateValidator(int minYear, int maxYear)
  {
    super(ALLOWED_CHARS, DATE_FORMAT_TEXT.length(), TOOLTIP_TEXT,
          false, DATE_FORMAT_TEXT.length(), true);
    this.minYear = minYear;
    this.maxYear = maxYear;
  }

  /**
   * Determines if the text is valid.
   * The text may be only part of the entire text entry.
   * @param s the text.
   * @return true if the text is valid, false otherwise.
   */
  public boolean isValid(String s)
  {
    return isValidDate(s);
  }

  /**
   * Determines if the text is valid.
   * The text may be only part of the entire text entry.
   * @param s the text.
   * @return true if the text is valid, false otherwise.
   */
  public static boolean isValidDate(String s)
  {
    char c;
    int integerCharCount = 0;
    int integerFieldCount = 0;
    int separatorCount = 0;
    for (int i = 0; i < s.length(); i++)
    {
      c = s.charAt(i);
      if (INTEGER_CHARS.indexOf(c) >= 0)
      {
        ++integerCharCount;
        //if first integer char in the field
        if (integerCharCount == 1)
        {
          ++integerFieldCount;
          //if more than 3 integer fields
          if (integerFieldCount > 3)
          {
            return false; //invalid
          }
        }
        else
        {
          final int maxIntegerCharCount;
          if (integerFieldCount == 1)
          {
            maxIntegerCharCount = 4;
          }
          else
          {
            maxIntegerCharCount = 2;
          }
          if (integerCharCount > maxIntegerCharCount)
          {
            return false; //invalid
          }
        }
      }
      else if (SEPARATOR_CHARS.indexOf(c) >= 0)
      {
        //if not first char and previous char was not an integer
        if (i != 0 && integerCharCount == 0)
        {
          return false; //invalid
        }
        //if more than 2 separators
        if (++separatorCount > 2)
        {
          return false; //invalid
        }
        integerCharCount = 0;
      }
      else
      {
        return false; //invalid
      }
    }
    return true;
  }

  /**
   * Determines if the text entry is valid.
   * The text should be the entire text entry.
   * @param s the text.
   * @return true if the text is valid, false otherwise.
   */
  public boolean isValidEntry(String s)
  {
    try
    {
      final Date d = SQLTextUtils.getDateValue(s);
      if (d != null)
      {
        if (minYear >= 0 || maxYear >= 0)
        {
          final GregorianCalendar g = new GregorianCalendar();
          g.setTime(d);
          final int year = g.get(GregorianCalendar.YEAR);
          if (minYear >= 0 && year < minYear)
          {
            return false;
          }
          if (maxYear >= 0 && year > maxYear)
          {
            return false;
          }
        }
        return true;
      }
    }
    catch (Exception ex)
    {
    }
    return false;
  }
}
