//TextSQLTimestampValidator.java:  Defines methods to validate SQL timestamp text.
//
//=====================================================================
// Copyright (C) 2007 Instrumental Software Technologies, Inc.
//
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions
// are met:
// 1. Redistributions of source code, or portions of this source code,
//    must retain the above copyright notice, this list of conditions
//    and the following disclaimer.
// 2. Redistributions in binary form must reproduce the above copyright
//    notice, this list of conditions and the following disclaimer in
//    the documentation and/or other materials provided with the
//    distribution.
// 3. All advertising materials mentioning features or use of this
//    software must display the following acknowledgment:
//    "This product includes software developed by Instrumental
//    Software Technologies, Inc. (http://www.isti.com)"
// 4. If the software is provided with, or as part of a commercial
//    product, or is used in other commercial software products the
//    customer must be informed that "This product includes software
//    developed by Instrumental Software Technologies, Inc.
//    (http://www.isti.com)"
// 5. The names "Instrumental Software Technologies, Inc." and "ISTI"
//    must not be used to endorse or promote products derived from
//    this software without prior written permission. For written
//    permission, please contact "info@isti.com".
// 6. Products derived from this software may not be called "ISTI"
//    nor may "ISTI" appear in their names without prior written
//    permission of Instrumental Software Technologies, Inc.
// 7. Redistributions of any form whatsoever must retain the following
//    acknowledgment:
//    "This product includes software developed by Instrumental
//    Software Technologies, Inc. (http://www.isti.com/)."
// THIS SOFTWARE IS PROVIDED BY INSTRUMENTAL SOFTWARE
// TECHNOLOGIES, INC. "AS IS" AND ANY EXPRESSED OR IMPLIED
// WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES
// OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
// DISCLAIMED.  IN NO EVENT SHALL INSTRUMENTAL SOFTWARE TECHNOLOGIES,
// INC. OR ITS CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
// INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
// (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
// SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
// HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
// STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
// ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED
// OF THE POSSIBILITY OF SUCH DAMAGE.
//=====================================================================
//  A current version of the software can be found at
//                http://www.isti.com
//  Bug reports and comments should be directed to
//  Instrumental Software Technologies, Inc. at info@isti.com
//=====================================================================
//

package com.isti.util.gui.textvalidator;

import java.sql.Date;
import java.util.GregorianCalendar;
import java.sql.Timestamp;

/**
 * Class TextSQLDateValidator defines methods to validate SQL Date text.
 */
public class TextSQLTimestampValidator
    extends TextBasicValidator
{
  /** The time field separator characters. */
  public static final String TIME_FIELD_SEPARATOR_CHARS = " ";
  /** The millseconds field separator characters. */
  public static final String MS_FIELD_SEPARATOR_CHARS = ".";
  /** The field separator characters. */
  public static final String FIELD_SEPARATOR_CHARS =
      TIME_FIELD_SEPARATOR_CHARS + MS_FIELD_SEPARATOR_CHARS;
  /** Millseconds description text. */
  public static final String MS_DESCRIPTION_TEXT = "ms";
  /** Millseconds format text. */
  public static final String MS_FORMAT_TEXT = "fffffffff";
  /** Timestamp format text. */
  public static final String TIMESTAMP_FORMAT_TEXT =
      TextSQLDateValidator.DATE_FORMAT_TEXT +
      TIME_FIELD_SEPARATOR_CHARS.charAt(0) +
      TextSQLTimeValidator.TIME_FORMAT_TEXT + MS_FIELD_SEPARATOR_CHARS.charAt(0) +
      MS_FORMAT_TEXT;

  /** Static string containing the separator characters. */
  public static final String SEPARATOR_CHARS =
      TextSQLDateValidator.SEPARATOR_CHARS +
      TextSQLTimeValidator.SEPARATOR_CHARS +
      FIELD_SEPARATOR_CHARS;
  /** Static string containing the allowed characters. */
  public static final String ALLOWED_CHARS = INTEGER_CHARS + SEPARATOR_CHARS;
  /** Static string containing the tool tip text. */
  private static final String TOOLTIP_TEXT =
      TextSQLDateValidator.DATE_DESCRIPTION_TEXT +
      TIME_FIELD_SEPARATOR_CHARS.charAt(0) +
      TextSQLTimeValidator.TIME_DESCRIPTION_TEXT +
      MS_FIELD_SEPARATOR_CHARS.charAt(0) +
      MS_DESCRIPTION_TEXT +
      " (" +
      TextSQLDateValidator.DATE_FORMAT_TEXT +
      TIME_FIELD_SEPARATOR_CHARS.charAt(0) +
      TextSQLTimeValidator.TIME_FORMAT_TEXT + MS_FIELD_SEPARATOR_CHARS.charAt(0) +
      MS_FORMAT_TEXT +
      ")";

  /** The minimum year. */
  private final int minYear;
  /** The maximum year. */
  private final int maxYear;

  /**
   * Creates the text SQL date validator.
   */
  public TextSQLTimestampValidator()
  {
    this( -1, -1);
  }

  /**
   * Creates the text SQL date validator.
   * The year in a 'Date' must be between 0 and 8099.
   * @param minYear the minimum year or -1 for no minimum.
   * @param maxYear the maximum year or -1 for no minimum.
   */
  public TextSQLTimestampValidator(int minYear, int maxYear)
  {
    super(ALLOWED_CHARS, TIMESTAMP_FORMAT_TEXT.length(), TOOLTIP_TEXT,
          false, TIMESTAMP_FORMAT_TEXT.length(), true);
    this.minYear = minYear;
    this.maxYear = maxYear;
  }

  /**
   * Determines if the text is valid.
   * The text may be only part of the entire text entry.
   * @param s the text.
   * @return true if the text is valid, false otherwise.
   */
  public boolean isValid(String s)
  {
    try
    {
      final SQLTextUtils.TimestampText timestampText =
          new SQLTextUtils.TimestampText(s);
      if (!TextSQLDateValidator.isValidDate(timestampText.getDateText()))
      {
        return false;
      }
      if (!TextSQLTimeValidator.isValidTime(timestampText.getTimeText()))
      {
        return false;
      }
      if (timestampText.getMillisecondsText().length() > MS_FORMAT_TEXT.length())
      {
        return false;
      }
      return true;
    }
    catch (Exception ex)
    {
    }
    return false;
  }

  /**
   * Determines if the text entry is valid.
   * The text should be the entire text entry.
   * @param s the text.
   * @return true if the text is valid, false otherwise.
   */
  public boolean isValidEntry(String s)
  {
    try
    {
      final Timestamp ts = SQLTextUtils.getTimestampValue(s);
      if (ts != null)
      {
        if (minYear >= 0 || maxYear >= 0)
        {
          final GregorianCalendar g = new GregorianCalendar();
          g.setTime(new Date(ts.getTime()));
          final int year = g.get(GregorianCalendar.YEAR);
          if (minYear >= 0 && year < minYear)
          {
            return false;
          }
          if (maxYear >= 0 && year > maxYear)
          {
            return false;
          }
        }
        return true;
      }
    }
    catch (Exception ex)
    {
    }
    return false;
  }
}
