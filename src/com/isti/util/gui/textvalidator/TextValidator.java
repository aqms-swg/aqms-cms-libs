//TextValidator.java:  Defines methods to validate text.
//
//=====================================================================
// Copyright (C) 2007 Instrumental Software Technologies, Inc.
//
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions
// are met:
// 1. Redistributions of source code, or portions of this source code,
//    must retain the above copyright notice, this list of conditions
//    and the following disclaimer.
// 2. Redistributions in binary form must reproduce the above copyright
//    notice, this list of conditions and the following disclaimer in
//    the documentation and/or other materials provided with the
//    distribution.
// 3. All advertising materials mentioning features or use of this
//    software must display the following acknowledgment:
//    "This product includes software developed by Instrumental
//    Software Technologies, Inc. (http://www.isti.com)"
// 4. If the software is provided with, or as part of a commercial
//    product, or is used in other commercial software products the
//    customer must be informed that "This product includes software
//    developed by Instrumental Software Technologies, Inc.
//    (http://www.isti.com)"
// 5. The names "Instrumental Software Technologies, Inc." and "ISTI"
//    must not be used to endorse or promote products derived from
//    this software without prior written permission. For written
//    permission, please contact "info@isti.com".
// 6. Products derived from this software may not be called "ISTI"
//    nor may "ISTI" appear in their names without prior written
//    permission of Instrumental Software Technologies, Inc.
// 7. Redistributions of any form whatsoever must retain the following
//    acknowledgment:
//    "This product includes software developed by Instrumental
//    Software Technologies, Inc. (http://www.isti.com/)."
// THIS SOFTWARE IS PROVIDED BY INSTRUMENTAL SOFTWARE
// TECHNOLOGIES, INC. "AS IS" AND ANY EXPRESSED OR IMPLIED
// WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES
// OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
// DISCLAIMED.  IN NO EVENT SHALL INSTRUMENTAL SOFTWARE TECHNOLOGIES,
// INC. OR ITS CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
// INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
// (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
// SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
// HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
// STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
// ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED
// OF THE POSSIBILITY OF SUCH DAMAGE.
//=====================================================================
//  A current version of the software can be found at
//                http://www.isti.com
//  Bug reports and comments should be directed to
//  Instrumental Software Technologies, Inc. at info@isti.com
//=====================================================================
//

package com.isti.util.gui.textvalidator;

/**
 * Interface TextValidator defines methods to validate text.
 */
public interface TextValidator
    extends AllowedCharactersConstants
{
  /**
   * Gets the allowed flag.
   * @return true to allow only the special characters, false to not
   * allow the special characters.
   * @see getSpecialChars()
   */
  public boolean getAllowedFlag();

  /**
   * Gets the number of columns.
   * @return the number of columns or -1 for the default.
   */
  public int getColumns();

  /**
   * Gets the maximum number of characters.
   * @return the maximum number of characters or 0 if none.
   */
  public int getMaxNumChars();

  /**
   * Gets the allowed (or not allowed) special characters.
   * If there are no special characters then all characters are allowed.
   * @return the allowed characters or null for none.
   * @see getAllowedFlag()
   */
  public String getSpecialChars();

  /**
   * Gets the tool tip text.
   * @return the tool tip text or null if none.
   */
  public String getToolTipText();

  /**
   * Determines if the characters represent a numeric value.
   * @return true if numeric value, false otherwise
   */
  public boolean isNumeric();

  /**
   * Determines if the text is valid.
   * The text may be only part of the entire text entry.
   * @param s the text.
   * @return true if the text is valid, false otherwise.
   */
  public boolean isValid(String s);

  /**
   * Determines if the text entry is valid.
   * The text should be the entire text entry.
   * @param s the text.
   * @return true if the text is valid, false otherwise.
   */
  public boolean isValidEntry(String s);
}
