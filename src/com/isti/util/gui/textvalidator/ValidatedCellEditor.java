//ValidatedCellEditor.java:  Extension of 'DefaultCellEditor' which only allows
//                          certain characters to be entered into it.
//
//=====================================================================
// Copyright (C) 2009 Instrumental Software Technologies, Inc.
//
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions
// are met:
// 1. Redistributions of source code, or portions of this source code,
//    must retain the above copyright notice, this list of conditions
//    and the following disclaimer.
// 2. Redistributions in binary form must reproduce the above copyright
//    notice, this list of conditions and the following disclaimer in
//    the documentation and/or other materials provided with the
//    distribution.
// 3. All advertising materials mentioning features or use of this
//    software must display the following acknowledgment:
//    "This product includes software developed by Instrumental
//    Software Technologies, Inc. (http://www.isti.com)"
// 4. If the software is provided with, or as part of a commercial
//    product, or is used in other commercial software products the
//    customer must be informed that "This product includes software
//    developed by Instrumental Software Technologies, Inc.
//    (http://www.isti.com)"
// 5. The names "Instrumental Software Technologies, Inc." and "ISTI"
//    must not be used to endorse or promote products derived from
//    this software without prior written permission. For written
//    permission, please contact "info@isti.com".
// 6. Products derived from this software may not be called "ISTI"
//    nor may "ISTI" appear in their names without prior written
//    permission of Instrumental Software Technologies, Inc.
// 7. Redistributions of any form whatsoever must retain the following
//    acknowledgment:
//    "This product includes software developed by Instrumental
//    Software Technologies, Inc. (http://www.isti.com/)."
// THIS SOFTWARE IS PROVIDED BY INSTRUMENTAL SOFTWARE
// TECHNOLOGIES, INC. "AS IS" AND ANY EXPRESSED OR IMPLIED
// WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES
// OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
// DISCLAIMED.  IN NO EVENT SHALL INSTRUMENTAL SOFTWARE TECHNOLOGIES,
// INC. OR ITS CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
// INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
// (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
// SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
// HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
// STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
// ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED
// OF THE POSSIBILITY OF SUCH DAMAGE.
//=====================================================================
//  A current version of the software can be found at
//                http://www.isti.com
//  Bug reports and comments should be directed to
//  Instrumental Software Technologies, Inc. at info@isti.com
//=====================================================================
//

package com.isti.util.gui.textvalidator;

import java.awt.Color;
import java.awt.Component;

import javax.swing.DefaultCellEditor;
import javax.swing.JComboBox;
import javax.swing.JComponent;
import javax.swing.JOptionPane;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.border.LineBorder;

/**
 * Class ValidatedCellEditor is an extension of 'DefaultCellEditor' which only
 * allows certain characters to be entered into it.
 */
public class ValidatedCellEditor extends DefaultCellEditor {

  private static final long serialVersionUID = 1L;

  /** The validated text component. */
  private final ValidatedTextComponent validatedTextComponent;

  /** The old value or null if none. */
  private Object oldValue = null;

  /**
   * Creates a new 'ValidatedCellEditor' object.
   * @param comboBox comboBox a <code>JComboBox</code> object that implements
   *          the <code>ValidatedTextComponent</code> interface.
   * @throws ClassCastException if the <code>JComboBox</code> object does not
   *           implement the <code>ValidatedTextComponent</code> interface.
   */
  public ValidatedCellEditor(final JComboBox comboBox) {
    super(comboBox);
    validatedTextComponent = (ValidatedTextComponent) comboBox;
  }

  /**
   * Creates a new 'ValidatedCellEditor' object.
   * @param textField a <code>JTextField</code> object that implements the
   *          <code>ValidatedTextComponent</code> interface..
   * @throws ClassCastException if the <code>JTextField</code> object does not
   *           implement the <code>ValidatedTextComponent</code> interface.
   */
  public ValidatedCellEditor(final JTextField textField) {
    super(textField);
    validatedTextComponent = (ValidatedTextComponent) textField;
  }

  /**
   * Creates a new 'ValidatedCellEditor' object.
   * @param tv the text validator.
   */
  public ValidatedCellEditor(TextValidator tv) {
    this(new ValidatedJTextField(tv));
  }

  /**
   * Get the editor color.
   * @param validFlag true if the value is valid, false otherwise.
   * @return the editor color.
   */
  protected Color getEditorColor(boolean validFlag) {
    final Color color;
    if (validFlag) {
      color = Color.black;
    } else {
      color = Color.red;
    }
    return color;
  }

  /**
   * Determines if the text entry is valid. The text should be the entire text
   * entry.
   * @param s the text.
   * @param tv the text validator.
   * @return an error message or null if none.
   */
  protected String getErrorMessage(String s, TextValidator tv) {
    String message = null;
    if (!tv.isValidEntry(s)) {
      message = "Entry \"" + s + "\" is invalid";
    }
    return message;
  }

  /**
   * Returns a reference to the editor component.
   * @return the editor <code>JComponent</code>
   * @see #getComponent()
   */
  protected JComponent getJComponent() {
    return editorComponent;
  }

  /**
   * Gets the parent component.
   * @return the parent component.
   */
  protected Component getParentComponent() {
    return null;
  }

  /**
   * Sets an initial <code>value</code> for the editor.
   * @param table the <code>JTable</code> that is asking the editor to edit;
   *          can be <code>null</code>
   * @param value the value of the cell to be edited
   * @param isSelected true if the cell is to be rendered with highlighting
   * @param row the row of the cell being edited
   * @param column the column of the cell being edited
   * @return the component for editing
   */
  public Component getTableCellEditorComponent(JTable table, Object value,
      boolean isSelected, int row, int column) {
    final int rowIndex = table.getSelectedRow();
    final int columnIndex = table.getSelectedColumn();
    if (rowIndex != -1 && columnIndex != -1) {
      oldValue = table.getModel().getValueAt(rowIndex, columnIndex);
    } else {
      oldValue = null;
    }
    validatedTextComponent.setValue(value);
    setEditorColor(value);
    return getJComponent();
  }

  /**
   * Get the validated text component.
   * @return the validated text component.
   */
  protected ValidatedTextComponent getValidatedTextComponent() {
    return validatedTextComponent;
  }

  /**
   * Determines if the text entry is equal to the current value.
   * @param s the text.
   * @return true if the text entry is equal to the current value, false
   *         otherwise.
   */
  protected boolean isEqualEntry(String s) {
    return s.equals(String.valueOf(oldValue));
  }

  /**
   * Determines if the text entry is valid. The text should be the entire text
   * entry.
   * @param s the text.
   * @param showDialogFlag true to show dialog, false otherwise.
   * @return true if the text is valid, false otherwise.
   */
  protected boolean isValidEntry(String s, boolean showDialogFlag) {
    final TextValidator tv = validatedTextComponent.getTextValidator();
    if (tv == null || isEqualEntry(s)) {
      return true;
    }
    return isValidEntry(s, tv, showDialogFlag);
  }

  /**
   * Determines if the text entry is valid. The text should be the entire text
   * entry.
   * @param s the text.
   * @param tv the text validator.
   * @param showDialogFlag true to show dialog, false otherwise.
   * @return true if the text is valid, false otherwise.
   */
  protected boolean isValidEntry(String s, TextValidator tv,
      boolean showDialogFlag) {
    String message = getErrorMessage(s, tv);
    if (message != null) {
      if (showDialogFlag) {
        setEditorColor(false);
        final String title = "Invalid Entry";
        final int option = showOptionDialog(message, title);
        if (option == JOptionPane.CANCEL_OPTION) {
          cancelCellEditing();
          return true;
        }
      }
      return false;
    }
    return true;
  }

  /**
   * Set the editor color.
   * @param validFlag true if the value is valid, false otherwise.
   * @return true if the value is valid, false otherwise.
   */
  protected boolean setEditorColor(boolean validFlag) {
    final Color color = getEditorColor(validFlag);
    setEditorColor(color);
    return validFlag;
  }

  /**
   * Set the editor color.
   * @param color the color.
   */
  protected void setEditorColor(final Color color) {
    getJComponent().setBorder(new LineBorder(color));
  }

  /**
   * Set the editor color.
   * @param value the value.
   * @return true if the value is valid, false otherwise.
   */
  protected boolean setEditorColor(Object value) {
    String s = String.valueOf(value);
    final boolean validFlag = isValidEntry(s, false);
    return setEditorColor(validFlag);
  }

  /**
   * Show the option dialog.
   * @param message the message.
   * @param title the title.
   * @return an integer indicating the option chosen by the user, or
   *         <code>CLOSED_OPTION</code> if the user closed the dialog.
   */
  protected int showOptionDialog(String message, String title) {
    return JOptionPane.showOptionDialog(getParentComponent(), message, title,
        JOptionPane.OK_CANCEL_OPTION, JOptionPane.ERROR_MESSAGE, null, null,
        null);
  }

  /**
   * Determines if editing has stopped.
   * @return true if editing has stopped, false otherwise.
   */
  public boolean stopCellEditing() {
    final String s = String.valueOf(super.getCellEditorValue());
    if (isValidEntry(s, true)) {
      return super.stopCellEditing();
    }
    return false;
  }
}
