//ValidatedDocument.java:  Extension of 'PlainDocument' that
//                        implements the validating of input characters.
//
//=====================================================================
// Copyright (C) 2009 Instrumental Software Technologies, Inc.
//
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions
// are met:
// 1. Redistributions of source code, or portions of this source code,
//    must retain the above copyright notice, this list of conditions
//    and the following disclaimer.
// 2. Redistributions in binary form must reproduce the above copyright
//    notice, this list of conditions and the following disclaimer in
//    the documentation and/or other materials provided with the
//    distribution.
// 3. All advertising materials mentioning features or use of this
//    software must display the following acknowledgment:
//    "This product includes software developed by Instrumental
//    Software Technologies, Inc. (http://www.isti.com)"
// 4. If the software is provided with, or as part of a commercial
//    product, or is used in other commercial software products the
//    customer must be informed that "This product includes software
//    developed by Instrumental Software Technologies, Inc.
//    (http://www.isti.com)"
// 5. The names "Instrumental Software Technologies, Inc." and "ISTI"
//    must not be used to endorse or promote products derived from
//    this software without prior written permission. For written
//    permission, please contact "info@isti.com".
// 6. Products derived from this software may not be called "ISTI"
//    nor may "ISTI" appear in their names without prior written
//    permission of Instrumental Software Technologies, Inc.
// 7. Redistributions of any form whatsoever must retain the following
//    acknowledgment:
//    "This product includes software developed by Instrumental
//    Software Technologies, Inc. (http://www.isti.com/)."
// THIS SOFTWARE IS PROVIDED BY INSTRUMENTAL SOFTWARE
// TECHNOLOGIES, INC. "AS IS" AND ANY EXPRESSED OR IMPLIED
// WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES
// OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
// DISCLAIMED.  IN NO EVENT SHALL INSTRUMENTAL SOFTWARE TECHNOLOGIES,
// INC. OR ITS CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
// INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
// (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
// SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
// HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
// STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
// ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED
// OF THE POSSIBILITY OF SUCH DAMAGE.
//=====================================================================
//  A current version of the software can be found at
//                http://www.isti.com
//  Bug reports and comments should be directed to
//  Instrumental Software Technologies, Inc. at info@isti.com
//=====================================================================
//

package com.isti.util.gui.textvalidator;

/**
 * Class ValidatedDocument is an extension of 'PlainDocument' that
 * implements the validating of input characters.
 */
public class ValidatedDocument
    extends AbstractValidatedDocument implements AllowedCharactersConstants
{
  public final String specialChars; //string of special characters
  public final boolean numericFlag; //true for valid numeric only
  public final int maxNumChars; //maximum number of chars allowed
  public final boolean allowFlag; //only allow the special chars
  private final boolean replaceFlag;  //true to try replacing chars
  private TextValidator textValidator = null; //text validator or null if none

  /**
   * Constructs a plain text document.
   * @param specialChars a String of characters allowed (or not)
   * into the document.
   * @param numericFlag true for valid numeric only, false otherwise.
   * @param maxNumChars maximum number of chars allowed or 0 for none.
   * @param allowFlag true to allow only the special characters, false to not
   * allow the special characters.
   */
  public ValidatedDocument(String specialChars, boolean numericFlag,
                          int maxNumChars, boolean allowFlag)
  {
    this.specialChars = specialChars;
    this.numericFlag = numericFlag;
    this.maxNumChars = maxNumChars;
    this.allowFlag = allowFlag;
    this.replaceFlag = isReplace(specialChars, numericFlag);
  }

  /**
   * Gets the allowed flag.
   * @return true to allow only the special characters, false to not
   * allow the special characters.
   * @see getSpecialChars()
   */
  public boolean getAllowedFlag()
  {
    return allowFlag;
  }
  
  /**
   * Gets the number of columns.
   * @return the number of columns or -1 for the default.
   */
  public int getColumns()
  {
    if (textValidator == null)
    {
      return -1;
    }
    return textValidator.getColumns();
  }
  
  /**
   * Gets the maximum number of characters.
   * @return the maximum number of characters or 0 if none.
   */
  public int getMaxNumChars()
  {
    return maxNumChars;
  }
  
  /**
   * Gets the allowed (or not allowed) special characters.
   * If there are no special characters then all characters are allowed.
   * @return the allowed characters or null for none.
   * @see getAllowedFlag()
   */
  public String getSpecialChars()
  {
    return specialChars;
  }
  
  /**
   * Returns the text validator.
   * @return the text validator or null if none.
   */
  public TextValidator getTextValidator()
  {
    return textValidator;
  }

  /**
   * Determines if the characters represent a numeric value.
   * @return true if numeric value, false otherwise
   */
  public boolean isNumeric()
  {
    return numericFlag;
  }

  /**
   * Determines if trying to replace characters.
   * @return true if trying to replace characters, false otherwise.
   */
  protected boolean isReplace()
  {
    return replaceFlag;
  }
  
  /**
   * Sets the text validator.
   * @param tv the text validator or null if none.
   */
  public void setTextValidator(TextValidator tv)
  {
    textValidator = tv;
  }
}
