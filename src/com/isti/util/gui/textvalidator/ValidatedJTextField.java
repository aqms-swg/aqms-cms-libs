//ValidatedJTextField.java:  Extension of 'JTextField' which only allows
//                          certain characters to be entered into it.
//
//=====================================================================
// Copyright (C) 2009 Instrumental Software Technologies, Inc.
//
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions
// are met:
// 1. Redistributions of source code, or portions of this source code,
//    must retain the above copyright notice, this list of conditions
//    and the following disclaimer.
// 2. Redistributions in binary form must reproduce the above copyright
//    notice, this list of conditions and the following disclaimer in
//    the documentation and/or other materials provided with the
//    distribution.
// 3. All advertising materials mentioning features or use of this
//    software must display the following acknowledgment:
//    "This product includes software developed by Instrumental
//    Software Technologies, Inc. (http://www.isti.com)"
// 4. If the software is provided with, or as part of a commercial
//    product, or is used in other commercial software products the
//    customer must be informed that "This product includes software
//    developed by Instrumental Software Technologies, Inc.
//    (http://www.isti.com)"
// 5. The names "Instrumental Software Technologies, Inc." and "ISTI"
//    must not be used to endorse or promote products derived from
//    this software without prior written permission. For written
//    permission, please contact "info@isti.com".
// 6. Products derived from this software may not be called "ISTI"
//    nor may "ISTI" appear in their names without prior written
//    permission of Instrumental Software Technologies, Inc.
// 7. Redistributions of any form whatsoever must retain the following
//    acknowledgment:
//    "This product includes software developed by Instrumental
//    Software Technologies, Inc. (http://www.isti.com/)."
// THIS SOFTWARE IS PROVIDED BY INSTRUMENTAL SOFTWARE
// TECHNOLOGIES, INC. "AS IS" AND ANY EXPRESSED OR IMPLIED
// WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES
// OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
// DISCLAIMED.  IN NO EVENT SHALL INSTRUMENTAL SOFTWARE TECHNOLOGIES,
// INC. OR ITS CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
// INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
// (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
// SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
// HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
// STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
// ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED
// OF THE POSSIBILITY OF SUCH DAMAGE.
//=====================================================================
//  A current version of the software can be found at
//                http://www.isti.com
//  Bug reports and comments should be directed to
//  Instrumental Software Technologies, Inc. at info@isti.com
//=====================================================================
//

package com.isti.util.gui.textvalidator;

import java.awt.Font;
import javax.swing.JTextField;

import com.isti.util.gui.GuiUtilFns;

/**
 * Class ValidatedJTextField is an extension of 'JTextField' which only
 * allows certain characters to be entered into it.
 */
public class ValidatedJTextField
    extends JTextField implements ValidatedTextComponent
{
  private static final long serialVersionUID = 1L;

  /** validated document or null if none */
  private final AbstractValidatedDocument document;

  /** The column witdh or 0 if unknown. */
  private int columnWidth = 0;
  
  /**
   * Creates a new 'ValidatedJTextField' object.
   * @param document the document or null for the default.
   * @see #getTextValidatorDocument(TextValidator)
   * @see #getValidatedDocument(TextValidator)
   */
  public ValidatedJTextField(AbstractValidatedDocument document)
  {
    super(document, null, getColumns(document));
    this.document = document;
    setTextValidator(document != null ? document.getTextValidator() : null);
  }

  /**
   * Creates a new 'ValidatedJTextField' object.
   * @param tv the text validator or null if none.
   */
  public ValidatedJTextField(TextValidator tv)
  {
    this(getTextValidatorDocument(tv));
  }
  
  /**
   * Returns the column width. The meaning of what a column is can be
   * considered a fairly weak notion for some fonts. This method is used
   * to define the width of a column. This method can be redefined to
   * be some alternative amount
   * @return the column width >= 1
   * @see #getPreferredSize()
   */
  protected int getColumnWidth()
  {
    if (columnWidth == 0)
    {
      int columnWidth = 0;
      // if the default character is not allowed
      if (document != null && !document.isAllowed(GuiUtilFns.defaultWidthChar))
      {
        columnWidth = GuiUtilFns.getMaxCharWidth(
            this, document.getSpecialChars(), document.getAllowedFlag());
      }
      if (columnWidth == 0)
      {
        // use the 'JTextField' column width
        columnWidth = super.getColumnWidth();
      }
      this.columnWidth = columnWidth;
    }
    return columnWidth;
  }

  /**
   * Gets the double value represented by this text field.
   * @return the double value or 0 if empty or not a double.
   */
  public double getDoubleValue()
  {
    try
    {
      if (getText() != null && getText().length() > 0)
      {
        return Double.parseDouble(getText());
      }
    }
    catch (Exception e)
    {
    }
    return 0;
  }

  /**
   * Gets the TextValidatorDocument for the TextValidator.
   * @param tv the text validator or null if none.
   * @return the TextValidatorDocument or null if none.
   */
  public static TextValidatorDocument getTextValidatorDocument(TextValidator tv)
  {
    if (tv == null)
    {
      return null;
    }
    return new TextValidatorDocument(tv);
  }

  /**
   * Gets the ValidatedDocument for the TextValidator.
   * @param tv the text validator or null if none.
   * @return the ValidatedDocument or null if none.
   */
  public static ValidatedDocument getValidatedDocument(TextValidator tv)
  {
    if (tv == null)
    {
      return null;
    }
    return new ValidatedDocument(
        tv.getSpecialChars(), tv.isNumeric(), tv.getMaxNumChars(),
        tv.getAllowedFlag());
  }
  
  /**
   * Gets the integer value represented by this text field.
   * @return the integer value or 0 if empty or not an integer.
   */
  public int getIntegerValue()
  {
    try
    {
      if (getText() != null && getText().length() > 0)
      {
        return Integer.parseInt(getText());
      }
    }
    catch (Exception e)
    {
    }
    return 0;
  }

  /**
   * Returns the text validator.
   * @return the text validator or null if none.
   */
  public TextValidator getTextValidator()
  {
    if (document != null)
    {
      return document.getTextValidator();
    }
    return null;
  }
  
  /**
   * Gets the columns based on the document.
   * @param document the document or null for the default.
   */
  protected static int getColumns(AbstractValidatedDocument document)
  {
    int columns = 0;
    if (document != null)
    {
      columns = document.getColumns();
      if (columns < 0)
      {
        columns = document.getMaxNumChars();
      }
    }
    return columns;
  }

  /**
   * Sets the current font. This removes cached row height and column width so
   * the new font will be reflected. <code>revalidate</code> is called after
   * setting the font.
   * @param f the new font
   */
  public void setFont(Font f)
  {
    super.setFont(f);
    columnWidth = 0;
  }
  
  /**
   * Sets the text validator.
   * @param tv the text validator or null if none.
   * @return A handle to this object.
   */
  public final ValidatedJTextField setTextValidator(TextValidator tv)
  {
    String toolTipText = this.getToolTipText();
    if (toolTipText == null || toolTipText.length() <= 0)
    {
      if (tv != null)
      {
        toolTipText = tv.getToolTipText();
      }
      if (toolTipText != null)
      {
        setToolTipText(toolTipText);
      }
    }
    boolean tvChangedFlag = false;
    if (document != null && document.getTextValidator() != tv)
    {
      document.setTextValidator(tv);
      tvChangedFlag = true;
    }
    final int oldColumns = getColumns();
    final int newColumns = getColumns(document);
    if (oldColumns != newColumns)
    {
      setColumns(newColumns);
    }
    else if (tvChangedFlag)
    {
      invalidate();
      columnWidth = 0;
    }
    return this;
  }

  /**
   * Sets the value.
   * @param val the value.
   */
  public void setValue(int val)
  {
    setText(Integer.toString(val));
  }

  /**
   * Sets the value.
   * @param val the value.
   */
  public void setValue(double val)
  {
    setText(Double.toString(val));
  }

  /**
   * Sets the value.
   * @param val the value or null to clear.
   */
  public void setValue(Object val)
  {
    final String s;
    if (val != null)
    {
      s = val.toString();
    }
    else
    {
      s = null;
    }
    setText(s);
  }
}
