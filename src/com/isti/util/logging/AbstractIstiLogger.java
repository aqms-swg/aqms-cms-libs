//IstiLogger.java:  Defines an abstract ISTI logger.
//
//   7/13/2009 -- [KF]
//   7/14/2009 -- [KF]  Removed 'NO_LEVEL' from 'IstiLogger' interface.
//   3/13/2012 -- [KF]  Added 'error(String str, Throwable t)' method.
//   4/06/2012 -- [KF]  Added 'getMinLevel()' and 'isLoggable()' methods.
//

package com.isti.util.logging;

import com.isti.util.UtilFns;

/**
 * Class MessageLogger defines an abstract ISTI logger. To implement an ISTI
 * logger, the programmer needs only to extend this class and provide an
 * implementation for the logMessage(int level, String str, Throwable t) method.
 */
public abstract class AbstractIstiLogger
    implements IstiLogger
{
  /**
   * Sends the given string to the log file with the message level set
   * to 'DEBUG'.
   * @param str the string to be outputted.
   * @return true if successful, false if an I/O error was detected.
   */
  public boolean debug(String str)
  {
    return println(DEBUG, str);
  }

  /**
   * Sends the given string to the log file with the message level set
   * to 'DEBUG2'.
   * @param str the string to be outputted.
   * @return true if successful, false if an I/O error was detected.
   */
  public boolean debug2(String str)
  {
    return println(DEBUG2, str);
  }

  /**
   * Sends the given string to the log file with the message level set
   * to 'DEBUG3'.
   * @param str the string to be outputted.
   * @return true if successful, false if an I/O error was detected.
   */
  public boolean debug3(String str)
  {
    return println(DEBUG3, str);
  }

  /**
   * Sends the given string to the log file with the message level set
   * to 'DEBUG4'.
   * @param str the string to be outputted.
   * @return true if successful, false if an I/O error was detected.
   */
  public boolean debug4(String str)
  {
    return println(DEBUG4, str);
  }

  /**
   * Sends the given string to the log file with the message level set
   * to 'DEBUG5'.
   * @param str the string to be outputted.
   * @return true if successful, false if an I/O error was detected.
   */
  public boolean debug5(String str)
  {
    return println(DEBUG5, str);
  }

  /**
   * Sends the given string to the log file with the message level set
   * to 'ERROR'.
   * @param str the string to be outputted.
   * @return true if successful, false if an I/O error was detected.
   */
  public boolean error(String str)
  {
    return println(ERROR, str);
  }

  /**
   * Sends the given string to the log file with the message level set
   * to 'ERROR'.
   * @param str the string to be outputted or null if only Throwable is used.
   * @param t Throwable associated with a log message or null if none.
   * @return true if successful, false if an I/O error was detected.
   */
  public boolean error(String str, Throwable t)
  {
    return println(ERROR, str, t);
  }

  /**
   * Returns a String corresponding to the given message level value.
   * @param level the message level.
   * @return a String corresponding to the given message level value, or
   * "level#" if the value is not matched.
   */
  public static String getLevelString(int level)
  {
    final String retStr;
    return ((retStr=getLevelStringNull(level)) != null) ? retStr :
                                                          ("level" + level);
  }

  /**
   * Returns a String corresponding to the given message level value.
   * @param level the message level.
   * @return a String corresponding to the given message level value, or
   * null if the value is not matched.
   */
  public static String getLevelStringNull(int level)
  {
    switch(level)
    {
      case ERROR:
        return ERROR_STR;
      case WARNING:
        return WARNING_STR;
      case INFO:
        return INFO_STR;
      case DEBUG:
        return DEBUG_STR;
      case DEBUG2:
        return DEBUG2_STR;
      case DEBUG3:
        return DEBUG3_STR;
      case DEBUG4:
        return DEBUG4_STR;
      case DEBUG5:
        return DEBUG5_STR;
      case ALL_MSGS:
        return ALL_MSGS_STR;
      case NO_MSGS:
        return NO_MSGS_STR;
      default:
        return null;
    }
  }

  /**
   * Sends the given string to the log file with the message level set
   * to 'INFO'.
   * @param str the string to be outputted.
   * @return true if successful, false if an I/O error was detected.
   */
  public boolean info(String str)
  {
    return println(INFO, str);
  }

  /**
   * Get the minimum level for all output messages.
   * This method should be overridden.
   * @return the minimum level for all output messages.
   * @see #isLoggable(int)
   */
  public int getMinLevel()
  {
    return ALL_MSGS;
  }

  /**
   * Determines if a message of the given level would actually be logged.
   * @param level a message logging level.
   * @return true if the given message level is currently being logged.
   * @see #getMinLevel()
   */
  public boolean isLoggable(int level)
  {
    return level >= getMinLevel();
  }

  /**
   * Gets the integer value that corresponds to the given message level String.
   * A case-insensitive comparison is used.
   * @param str the message level string to interpret.
   * @return the integer value that corresponds to the given message level
   * String or null if the String could not be matched.
   */
  public static Integer levelStringToValue(String str)
  {
    if(str == null)          //if null handle then
      return null;           //return null for no-match
    str = str.trim();        //trim any leading or trailing whitespace
    final int val;      //compare string values:
    if(str.equalsIgnoreCase(ERROR_STR))
      val = ERROR;
    else if(str.equalsIgnoreCase(WARNING_STR))
      val = WARNING;
    else if(str.equalsIgnoreCase(INFO_STR))
      val = INFO;
    else if(str.equalsIgnoreCase(DEBUG_STR))
      val = DEBUG;
    else if(str.equalsIgnoreCase(DEBUG2_STR))
      val = DEBUG2;
    else if(str.equalsIgnoreCase(DEBUG3_STR))
      val = DEBUG3;
    else if(str.equalsIgnoreCase(DEBUG4_STR))
      val = DEBUG4;
    else if(str.equalsIgnoreCase(DEBUG5_STR))
      val = DEBUG5;
    else if(str.equalsIgnoreCase(ALL_MSGS_STR))
      val = ALL_MSGS;
    else if(str.equalsIgnoreCase(NO_MSGS_STR))
      val = NO_MSGS;
    else                     //if no match then
      return null;           //return null for no-match
    return Integer.valueOf(val);      //return Integer holder with value
  }

  /**
   * Sends the given string to the log file.  (And possibly the
   * console--see the 'consoleLevel' parameter on the constructor.)
   * @param level the message level to be used.
   * @param str the string to be outputted.
   * @return true if successful, false if an I/O error was detected.
   */
  public boolean println(int level, String str)
  {
    return println(level, str, null);
  }

  /**
   * Sends the given string to the log file.  (And possibly the
   * console--see the 'consoleLevel' parameter on the constructor.)
   * @param level the message level to be used.
   * @param str the string to be outputted or null if only Throwable is used.
   * @param t Throwable associated with a log message or null if none.
   * @return true if successful, false if an I/O error was detected.
   */
  public boolean println(int level, String str, Throwable t)
  {
    return logMessage(level, getMessage(level, str, t), t);
  }

  /**
   * Sends the given string to the log file.  (And possibly the
   * console--see the 'consoleLevel' parameter on the constructor.)
   * @param str the string to be outputted.
   * @return true if successful, false if an I/O error was detected.
   */
  public boolean println(String str)
  {
    return println(INFO,str);
  }

  /**
   * Sends the given string to the log file with the message level set
   * to 'WARNING'.
   * @param str the string to be outputted.
   * @return true if successful, false if an I/O error was detected.
   */
  public boolean warning(String str)
  {
    return println(WARNING, str);
  }
  
  /**
   * Gets the message for the level.
   * @param level the message level.
   * @return the message.
   */
  protected String getMessage(int level)
  {
    return "[" + getLevelString(level) + "] ";
  }

  /**
   * Gets the message for the level and string. If there is a string and
   * a Throwable then a newline is added to the end of the message.
   * @param level the message level.
   * @param str the string to be outputted or null if only Throwable is used.
   * @param t Throwable associated with a log message or null if none.
   * @return the message.
   */
  protected String getMessage(int level, String str, Throwable t)
  {
    if (str == null)
      str = UtilFns.EMPTY_STRING;
    else if (t != null)
      str += UtilFns.newline;
    //get string name for level (if given):
    final String levelStr = getMessage(level);
    return levelStr + str;
  }

  /**
   * Get the message for the string and the Throwable.
   * @param str the string to be outputted.
   * @param t Throwable associated with a log message or null if none.
   * @return the message.
   */
  protected String getMessage(String str, Throwable t)
  {
    if (t != null)
    {
      str += UtilFns.getStackTraceString(t);
    }
    return str;
  }

  /**
   * Sends the given string to the log file.  (And possibly the
   * console--see the 'consoleLevel' parameter on the constructor.)
   * @param level the message level to be used.
   * @param str the string to be outputted.
   * @param t Throwable associated with a log message or null if none.
   * @return true if successful, false if an I/O error was detected.
   * @see #getMessage(String, Throwable)
   */
  protected abstract boolean logMessage(int level, String str, Throwable t);
}
