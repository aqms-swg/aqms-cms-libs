//IstiConsoleLogger.java:  Defines an ISTI console logger.
//
//  7/14/2009 -- [KF]
//  4/06/2012 -- [KF]  Added 'getMinLevel()' method.
//

package com.isti.util.logging;

import java.io.OutputStream;
import java.io.PrintStream;

/**
 * Class MessageLogger defines an ISTI console logger.
 * Alternatively a different output stream may be used rather than the console
 * by using the 'out' parameter on the constructor or
 * by calling the 'setConsoleOutput()' method.
 * For a GUI program a 'com.isti.util.gui.TextAreaOutputStream' may be used.
 */
public class IstiConsoleLogger
    extends AbstractIstiLogger
{
  /** The minimum level for console output. */
  private int consoleLevel;

  /** The output stream. */
  private PrintStream out = System.out;

  /**
   * Creates the console logger.
   */
  public IstiConsoleLogger()
  {
    this(INFO);
  }

  /**
   * Creates the console logger.
   * @param level the console level to be used.
   */
  public IstiConsoleLogger(int level)
  {
    consoleLevel = level;
  }

  /**
   * Gets the current level for console output messages.  (See the
   * 'consoleLevel' parameter on the constructor.)
   * @return the current level for console output messages
   */
  public int getConsoleLevel()
  {
    return consoleLevel;
  }

  /**
   * Get the minimum level for all output messages.
   * @return the minimum level for all output messages.
   * @see #isLoggable(int)
   */
  public int getMinLevel()
  {
    return consoleLevel;
  }

  /**
   * Get the console output stream.
   * @return the console output stream.
   */
  public PrintStream getConsoleOutput()
  {
    return out;
  }
  
  /**
   * Sets the current level for console output messages.  (See the
   * 'consoleLevel' parameter on the constructor.)
   * @param level the console level to be used.
   */
  public synchronized void setConsoleLevel(int level)
  {
    consoleLevel = level;
  }

  /**
   * Set the console output stream.
   * @param out the console output stream.
   */
  public synchronized void setConsoleOutput(OutputStream out)
  {
    if (out instanceof PrintStream)
    {
      this.out = (PrintStream) out;
    }
    else
    {
      this.out = new PrintStream(out);
    }
  }

  /**
   * Sends the given string to the log file.  (And possibly the
   * console--see the 'consoleLevel' parameter on the constructor.)
   * @param level the message level to be used, or NO_LEVEL for none.
   * @param str the string to be outputted.
   * @param t Throwable associated with a log message or null if none.
   * @return true if successful, false if an I/O error was detected.
   */
  protected boolean logMessage(int level, String str, Throwable t)
  {
    if (level >= getConsoleLevel()) //if level high enough then
    {
      if (t == null)
      {
        out.println(str); //send to stdout
      }
      else
      {
        if (str.length() > 0)
        {
          out.print(str); //send to stdout
        }
        t.printStackTrace(out);
      }
      out.flush();
    }
    return true;
  }
}
