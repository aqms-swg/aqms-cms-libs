//IstiLogger.java:  Defines an ISTI logger.
//
//   7/13/2009 -- [KF]
//   7/14/2009 -- [KF]  Removed 'NO_LEVEL' from 'IstiLogger' interface.
//

package com.isti.util.logging;

/**
 * Interface IstiLogger defines an ISTI logger.
 */
public interface IstiLogger
{
  //defines for log message levels:
  /**
   * A message level value that denotes an error which causes the
   * program to abort (3).
   */
  public static final int ERROR = 3;
  /**
   * A message level value that denotes an unexpected condition,
   * but the program continues (2).
   */
  public static final int WARNING = 2;
  /**
   * A message level value that denotes an informational message (1).
   */
  public static final int INFO = 1;
  /**
   * A message level value that denotes a debug message, for
   * developers only (0).
   */
  public static final int DEBUG = 0;
  /**
   * A message level value that denotes a lower-level debug (2) message,
   * for developers only (-1).
   */
  public static final int DEBUG2 = -1;
  /**
   * A message level value that denotes a lower-level debug (3) message,
   * for developers only (-2).
   */
  public static final int DEBUG3 = -2;
  /**
   * A message level value that denotes a lower-level debug (4) message,
   * for developers only (-3).
   */
  public static final int DEBUG4 = -3;
  /**
   * A message level value that denotes a lower-level debug (5) message,
   * for developers only (-4).
   */
  public static final int DEBUG5 = -4;

  //defines for min/max message level output:
  /**
   * A message level value used to specify that all messages will be
   * retained (-999).
   */
  public static final int ALL_MSGS = -999;
  /**
   * A message level value used to specify that no messages will be
   * retained (999).
   */
  public static final int NO_MSGS = 999;

  //String versions of message level values:
  /**
   * String version of a message level value that denotes an error which
   * causes the program to abort.
   */
  public static final String ERROR_STR = "Error";
  /**
   * String version of a message level value that denotes an unexpected
   * condition, but the program continues.
   */
  public static final String WARNING_STR = "Warning";
  /**
   * String version of a message level value that denotes an
   * informational message.
   */
  public static final String INFO_STR = "Info";
  /**
   * String version of a message level value that denotes a debug message,
   * for developers only.
   */
  public static final String DEBUG_STR = "Debug";
  /**
   * String version of a message level value that denotes a debug (2)
   * message, for developers only.
   */
  public static final String DEBUG2_STR = "Debug2";
  /**
   * String version of a message level value that denotes a debug (3)
   * message, for developers only.
   */
  public static final String DEBUG3_STR = "Debug3";
  /**
   * String version of a message level value that denotes a debug (4)
   * message, for developers only.
   */
  public static final String DEBUG4_STR = "Debug4";
  /**
   * String version of a message level value that denotes a debug (5)
   * message, for developers only.
   */
  public static final String DEBUG5_STR = "Debug5";
  /**
   * String version of a message level value used to specify that all
   * messages will be retained.
   */
  public static final String ALL_MSGS_STR = "ALL_MSGS";
  /**
   * String version of a message level value used to specify that no
   * messages will be retained.
   */
  public static final String NO_MSGS_STR = "NO_MSGS";

  /**
   * Sends the given string to the log file with the message level set
   * to 'DEBUG'.
   * @param str the string to be outputted.
   * @return true if successful, false if an I/O error was detected.
   */
  public boolean debug(String str);

  /**
   * Sends the given string to the log file with the message level set
   * to 'DEBUG2'.
   * @param str the string to be outputted.
   * @return true if successful, false if an I/O error was detected.
   */
  public boolean debug2(String str);

  /**
   * Sends the given string to the log file with the message level set
   * to 'DEBUG3'.
   * @param str the string to be outputted.
   * @return true if successful, false if an I/O error was detected.
   */
  public boolean debug3(String str);

  /**
   * Sends the given string to the log file with the message level set
   * to 'DEBUG4'.
   * @param str the string to be outputted.
   * @return true if successful, false if an I/O error was detected.
   */
  public boolean debug4(String str);

  /**
   * Sends the given string to the log file with the message level set
   * to 'DEBUG5'.
   * @param str the string to be outputted.
   * @return true if successful, false if an I/O error was detected.
   */
  public boolean debug5(String str);

  /**
   * Sends the given string to the log file with the message level set
   * to 'ERROR'.
   * @param str the string to be outputted.
   * @return true if successful, false if an I/O error was detected.
   */
  public boolean error(String str);

  /**
   * Sends the given string to the log file with the message level set
   * to 'INFO'.
   * @param str the string to be outputted.
   * @return true if successful, false if an I/O error was detected.
   */
  public boolean info(String str);

  /**
   * Determines if a message of the given level would actually be logged.
   * @param level a message logging level.
   * @return true if the given message level is currently being logged.
   */
  public boolean isLoggable(int level);

  /**
   * Sends the given string to the log file.  (And possibly the
   * console--see the 'consoleLevel' parameter on the constructor.)
   * @param level the message level to be used.
   * @param str the string to be outputted.
   * @return true if successful, false if an I/O error was detected.
   */
  public boolean println(int level, String str);

  /**
   * Sends the given string to the log file.  (And possibly the
   * console--see the 'consoleLevel' parameter on the constructor.)
   * @param level the message level to be used.
   * @param str the string to be outputted or null if only Throwable is used.
   * @param t Throwable associated with a log message or null if none.
   * @return true if successful, false if an I/O error was detected.
   */
  public boolean println(int level, String str, Throwable t);

  /**
   * Sends the given string to the log file.  (And possibly the
   * console--see the 'consoleLevel' parameter on the constructor.)
   * @param str the string to be outputted.
   * @return true if successful, false if an I/O error was detected.
   */
  public boolean println(String str);

  /**
   * Sends the given string to the log file with the message level set
   * to 'WARNING'.
   * @param str the string to be outputted.
   * @return true if successful, false if an I/O error was detected.
   */
  public boolean warning(String str);
}
