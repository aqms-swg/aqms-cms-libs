package com.isti.util.logging;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.PrintStream;

public class IstiLoggerOutputStream extends ByteArrayOutputStream {
  /**
   * Create a print stream with automatic flush enabled for the log output
   * stream.
   * 
   * @param logOutputStream the log output stream.
   * @return a print stream.
   */
  public static PrintStream createPrintStream(
	  IstiLoggerOutputStream logOutputStream) {
	return new PrintStream(logOutputStream, true);
  }

  /**
   * Set the logger for standard error.
   * 
   * @param logger the logger.
   * @param level the level.
   * @return standard error.
   */
  public static PrintStream logStandardErr(IstiLogger logger, int level) {
	// save standard error
	final PrintStream ps = System.err;
	System.setErr(createPrintStream(new IstiLoggerOutputStream(logger, level)));
	return ps;
  }

  /**
   * Set the logger for standard output.
   * 
   * @param logger the logger.
   * @param level the level.
   * @return standard output.
   */
  public static PrintStream logStandardOut(IstiLogger logger, int level) {
	// save standard output
	final PrintStream ps = System.out;
	System.setOut(createPrintStream(new IstiLoggerOutputStream(logger, level)));
	return ps;
  }

  /** The level. */
  private final int level;

  /** The logger. */
  private final IstiLogger logger;

  /**
   * Create the log output stream.
   * 
   * @param logger the logger.
   * @param level the level.
   */
  public IstiLoggerOutputStream(IstiLogger logger, int level) {
	this.logger = logger;
	this.level = level;
  }

  /**
   * Flush the output stream.
   */
  @Override
  public synchronized void flush() throws IOException {
	super.flush();
	final String str = toString().trim();
	super.reset();

	// ignore if empty
	if (str.length() == 0) {
	  return;
	}

	logger.println(level, str);
  }
}
