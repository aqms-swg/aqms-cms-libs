//CubicSpline.java:  Cubic spline implementation.
//
//  10/25/2005 -- [ET]  Initial version, based on 'spline' module
//                      from GNU 'plotutils' package
//                      (http://www.gnu.org/software/plotutils)
//                      and ported to Java.
//
//   Original notes from GNU 'spline' module:
//
//   Written by Robert S. Maier
//   <rsm@math.arizona.edu>, based on earlier work by Rich Murphey.
//   Copyright (C) 1989-1999 Free Software Foundation, Inc.
//
//   References:
//
//   D. Kincaid and [E.] W. Cheney, Numerical Analysis, Brooks/Cole,
//   2nd. ed., 1996, Section 6.4.
//
//   C. de Boor, A Practical Guide to Splines, Springer-Verlag, 1978,
//   Chapter 4.
//
//   A. K. Cline, "Scalar and Planar-Valued Curve Fitting Using Splines under
//   Tension", Communications of the ACM 17 (1974), 218-223.

package com.isti.util.math;

import com.isti.util.ErrorMessageMgr;

public class CubicSpline extends ErrorMessageMgr
{

  /** Minimum value for magnitude of x, for such functions as x-sinh(x),
      x-tanh(x), x-sin(x), and x-tan(x) to have acceptable accuracy.  If the
      magnitude of x is smaller than this value, these functions of x will
      be computed via power series to accuracy O(x**6). */
  public static double TRIG_ARG_MIN = 0.001;

  /** Maximum value for magnitude of x, beyond which we approximate
      x/sinh(x) and x/tanh(x) by |x|exp(-|x|). */
  public static double TRIG_ARG_MAX = 50.0;


  /**
   * Performs cubic spline interpolation.  Two "source" arrays (abscissa
   * 'X' and ordinate 'Y' values) and an array of "new" abscissa values
   * are given; an array of "destination" ordinate values is generated
   * and returned.  If any "new" abscissa values are outside of the range
   * of "source" abscissa values then the method will return an error
   * message.
   * @param t abscissa "source" array of 'double' values.
   * @param y ordinate "source" array of 'double' values.
   * @param tension tension value for interpolation, use 0.0 as default
   * value:  If tension=0 then a spline with tension reduces to a
   * conventional piecewise cubic spline.  In the limits
   * tension->+infinity and tension->-infinity, a spline with
   * tension reduces to a piecewise linear (`broken line')
   * interpolation.  To oversimplify a bit, 1.0/tension is the
   * maximum abscissa range over which the spline likes to curve,
   * at least when tension>0.  So increasing the tension far above
   * zero tends to make the spline contain short curved sections,
   * separated by sections that are almost straight.  The curved
   * sections will be centered on the user-specified data points.
   * The behavior of the spline when tension<0 is altogether
   * different: it will tend to oscillate, though as
   * tension->-infinity the oscillations are damped out.
   * @param k boundary condition, use 1.0 as default value:  Appears in
   * the two boundary conditions y''[0]=ky''[1] and y''[n]=ky''[n-1].
   * @param xValsArr array of "new" abscissa values to use with
   * interpolation ('double' values).
   * @return A new 'double' array interpolated values; or null if an
   * error occurs (in which case the error message may be fetched via the
   * 'getErrorMessageString()' method).
   */
  public double [] calcSpline(double [] t, double [] y, double tension,
                                              double k, double [] xValsArr)
  {
    if(t.length != y.length)
    {
      setErrorMessageString("Arrays 't[]' and 'y[]' not same length");
      return null;
    }
    final int used = t.length - 1;
    final int numXVals = xValsArr.length;

    if(used < 1 || numXVals <= 0)
    {    /* less than 2 "source" datapoints or no "new" abscissa points */
      return new double[0];       /* don't output anything (null dataset) */
    }

    if(used <= 1)
      k = 0.0;

    if(!isMonotonic(t))
    {
      setErrorMessageString("Abscissa values not monotonic");
      return null;
    }

    final double [] z;            /* array for 2nd derivatives */
         /* compute z, array of 2nd derivatives at each knot */
    if((z=fit(t, y, k, tension, false)) == null)
      return null;
                                  /* last value in 'x' array */
    final double lastXVal = xValsArr[numXVals-1];
    int lastVal = 0;         /* last req'd point = 1st/last data point? */
    if(lastXVal == t[0])
      lastVal = 1;
    else if(lastXVal == t[used])
      lastVal = 2;
                                  /* array to be returned */
    final double [] retValsArr = new double[numXVals];
    int retArrIdx = 0;

    int outRangeCount = 0;   /* number of req'd datapoints out of range */
    double x;
    for(int i = 0; i < numXVals; ++i)
    {    /* for each value in "new" abscissa array */
      x = xValsArr[i];            /* get value from array */

      if(i == numXVals-1)
      {
        /* avoid numerical fuzz */
        if(lastVal == 1)	  /* left end of input */
          x = t[0];
        else if(lastVal == 2)     /* right end of input */
          x = t[used];
      }

      if((x - t[0]) * (x - t[used]) <= 0)
      {               /* calculate value, enter into array, inc index */
        retValsArr[retArrIdx++] = interpolate(t, y, z, x, tension, false);
      }
      else
        outRangeCount++;
    }

    if(outRangeCount > 0)
    {
      setErrorMessageString(outRangeCount + " requested point" +
                                         ((outRangeCount != 1) ? "s" : "") +
                              " could not be computed (out of data range)");
      return null;
    }

    return retValsArr;
  }

  /**
   * Computes the array of second derivatives at the knots, i.e.,
   * internal data points.  The abscissa array t[] and the ordinate array y[]
   * are specified.  On entry, have n+1 >= 2 points in the t and y arrays,
   * numbered 0..n.  The knots are numbered 1..n-1 as in Kincaid and Cheney.
   * In the periodic case, the final knot, i.e., (t[n-1],y[n-1]), has the
   * property that y[n-1]=y[0]; moreover, y[n]=y[1].  The number of points
   * supplied by the user was n+1 in the non-periodic case, and n in the
   * periodic case.  When this function is called, n>=1 in the non-periodic
   * case, and n>=2 in the periodic case.
   *
   * Algorithm: the n-1 by n-1 tridiagonal matrix equation for the vector of
   * 2nd derivatives at the knots is reduced to upper diagonal form.  At that
   * point the diagonal entries (pivots) of the upper diagonal matrix are in
   * the vector u[], and the vector on the right-hand side is v[].  That is,
   * the equation is of the form Ay'' = v, where a_(ii) = u[i], and a_(i,i+1)
   * = alpha[i].  Here i=1..n-1 indexes the set of knots.  The matrix
   * equation is solved by back-substitution for y''[], i.e., for z[].
   *
   * @param t abscissa array of 'double' values.
   * @param y ordinate array of 'double' values.
   * @param k boundary condition, use 1.0 as default value; appears in the
   * two boundary conditions y''[0]=ky''[1] and y''[n]=ky''[n-1].
   * @param tension tension value for interpolation.
   * @param periodic true for periodic; false for non-periodic.
   * @return A new 'double' array of second derivatives; or null if an
   * error occurs (in which case the error message may be fetched via the
   * 'getErrorMessageString()' method).
   */
  public double [] fit(double [] t, double [] y, double k,
                                           double tension, boolean periodic)
  {
    if(t.length != y.length)
    {
      setErrorMessageString(
                  "Arrays 't[]' and 'y[]' not same length in 'fit' method");
      return null;
    }
    final int n = t.length - 1;

    if(n == 1)			/* exactly 2 points, use straight line */
      return new double [] { 0.0, 0.0 };

    final double [] z = new double[n+1];

    final double [] h = new double[n];
    final double [] b = new double[n];
    final double [] u = new double[n];
    final double [] v = new double[n];
    final double [] alpha = new double[n];
    final double [] beta = new double[n];

    double [] uu = null, vv = null, s = null;
    int i;

    if(periodic)
    {
      s = new double[n];
      uu = new double[n];
      vv = new double[n];
    }

    for(i = 0; i <= n - 1 ; ++i)
    {
      h[i] = t[i + 1] - t[i];
      b[i] = 6.0 * (y[i + 1] - y[i]) / h[i]; /* for computing RHS */
    }

    if(tension < 0.0)		/* must rule out sin(tension * h[i]) = 0 */
    {
      for(i = 0; i <= n - 1 ; ++i)
      {
        if(Math.sin(tension * h[i]) == 0.0)
        {
          setErrorMessageString("Specified negative tension value " +
                                             "is singular in 'fit' method");
          return null;
        }
      }
    }
    if(tension == 0.0)
    {
      for(i = 0; i <= n - 1 ; ++i)
      {
        alpha[i] = h[i];	/* off-diagonal = alpha[i] to right */
        beta[i] = 2.0 * h[i];	/* diagonal = beta[i-1] + beta[i] */
      }
    }
    else if(tension > 0.0)
    {   /* `positive' (really real) tension, use hyperbolic trig funcs */
      for(i = 0; i <= n - 1 ; ++i)
      {
        double x = tension * h[i];
        double xabs = ((x < 0.0) ? -x : x);

        if(xabs < TRIG_ARG_MIN)
        { /* hand-compute (6/x^2)(1-x/sinh(x)) and (3/x^2)(x/tanh(x)-1)
             to improve accuracy; here `x' is tension * h[i] */
          alpha[i] = h[i] * sinh_func(x);
          beta[i] = 2.0 * h[i] * tanh_func(x);
        }
        else if(xabs > TRIG_ARG_MAX)
        { /* in (6/x^2)(1-x/sinh(x)) and (3/x^2)(x/tanh(x)-1),
             approximate x/sinh(x) and x/tanh(x) by 2|x|exp(-|x|)
             and |x|, respectively */
          int sign = ((x < 0.0) ? -1 : 1);

          alpha[i] = ((6.0 / (tension * tension))
                   * ((1.0 / h[i]) - tension * 2 * sign * Math.exp(-xabs)));
          beta[i] = ((6.0 / (tension * tension))
                     * (tension - (1.0 / h[i])));
        }
        else
        {
          alpha[i] = ((6.0 / (tension * tension))
                      * ((1.0 / h[i]) - tension / sinh(x)));
          beta[i] = ((6.0 / (tension * tension))
                     * (tension / tanh(x) - (1.0 / h[i])));
        }
      }
    }
    else				/* tension < 0 */
    {   /* `negative' (really imaginary) tension,  use circular trig funcs */
      for(i = 0; i <= n - 1 ; ++i)
      {
        double x = tension * h[i];
        double xabs = ((x < 0.0) ? -x : x);

        if(xabs < TRIG_ARG_MIN)
        { /* hand-compute (6/x^2)(1-x/sin(x)) and (3/x^2)(x/tan(x)-1)
             to improve accuracy; here `x' is tension * h[i] */
          alpha[i] = h[i] * sin_func(x);
          beta[i] = 2.0 * h[i] * tan_func(x);
        }
        else
        {
          alpha[i] = ((6.0 / (tension * tension))
                     * ((1.0 / h[i]) - tension / Math.sin(x)));
          beta[i] = ((6.0 / (tension * tension))
                     * (tension / Math.tan(x) - (1.0 / h[i])));
        }
      }
    }

    if(!periodic && n == 2)
        u[1] = beta[0] + beta[1] + 2 * k * alpha[0];
    else
      u[1] = beta[0] + beta[1] + k * alpha[0];

    v[1] = b[1] - b[0];

    if(u[1] == 0.0)
    {
      setErrorMessageString("As posed, problem of computing spline is " +
                                                "singular in 'fit' method");
      return null;
    }

    if(periodic)
    {
      s[1] = alpha[0];
      uu[1] = 0.0;
      vv[1] = 0.0;
    }

    for(i = 2; i <= n - 1 ; ++i)
    {
      u[i] = (beta[i] + beta[i - 1]
              - alpha[i - 1] * alpha[i - 1] / u[i - 1]
              + ((i == n - 1) ? k * alpha[n - 1] : 0.0));

      if(u[i] == 0.0)
      {
        setErrorMessageString("As posed, problem of computing spline is " +
                                                "singular in 'fit' method");
        return null;
      }

      v[i] = b[i] - b[i - 1] - alpha[i - 1] * v[i - 1] / u[i - 1];

      if(periodic)
      {
        s[i] = - s[i-1] * alpha[i-1] / u[i-1];
        uu[i] = uu[i-1] - s[i-1] * s[i-1] / u[i-1];
        vv[i] = vv[i-1] - v[i-1] * s[i-1] / u[i-1];
      }
    }

    if(!periodic)
    {
      /* fill in 2nd derivative array */
      z[n] = 0.0;
      for(i = n - 1; i >= 1; --i)
        z[i] = (v[i] - alpha[i] * z[i + 1]) / u[i];
      z[0] = 0.0;

      /* modify to include boundary condition */
      z[0] = k * z[1];
      z[n] = k * z[n - 1];
    }
    else		/* periodic */
    {
      z[n-1] = (v[n-1] + vv[n-1]) / (u[n-1] + uu[n-1] + 2 * s[n-1]);
      for(i = n - 2; i >= 1; --i)
        z[i] = ((v[i] - alpha[i] * z[i + 1]) - s[i] * z[n-1]) / u[i];

      z[0] = z[n-1];
      z[n] = z[1];
    }

    return z;
  }

  /**
   * Computes an approximate ordinate value for a given
   * abscissa value, given an array of data points (stored in t[] and y[],
   * containing abscissa and ordinate values respectively), and z[], the
   * array of 2nd derivatives at the knots (i.e. internal data points).
   * On entry, have n+1 >= 2 points in the t, y, z arrays, numbered 0..n.
   * The number of knots (i.e. internal data points) is n-1; they are
   * numbered 1..n-1 as in Kincaid and Cheney.  In the periodic case, the
   * final knot, i.e., (t[n-1],y[n-1]), has the property that y[n-1]=y[0];
   * also, y[n]=y[1].  The number of data points supplied by the user was
   * n+1 in the non-periodic case, and n in the periodic case.  When this
   * function is called, n>=1 in the non-periodic case, and n>=2 in the
   * periodic case.
   */
  public static double interpolate(double [] t, double [] y, double [] z,
                                 double x, double tension, boolean periodic)
  {
    final int n = (t.length <= y.length) ? t.length - 1 : y.length - 1;

    double diff, updiff, reldiff, relupdiff, h;
    double value;
    final boolean is_ascending = (t[n-1] < t[n]);
    int i = 0, k;

    /* in periodic case, map x to t[0] <= x < t[n] */
    if(periodic && (x - t[0]) * (x - t[n]) > 0.0)
      x -= ((int)(Math.floor( (x - t[0]) / (t[n] - t[0]) )) * (t[n] - t[0]));

    /* do binary search to find interval */
    for(k = n - i; k > 1;)
    {
      if(is_ascending ? x >= t[i + (k>>1)] : x <= t[i + (k>>1)])
      {
        i = i + (k>>1);
        k = k - (k>>1);
      }
      else
        k = k>>1;
    }

    /* at this point, x is between t[i] and t[i+1] */
    h = t[i + 1] - t[i];
    diff = x - t[i];
    updiff = t[i+1] - x;
    reldiff = diff / h;
    relupdiff = updiff / h;

    if(tension == 0.0)
    {   /* evaluate cubic polynomial in nested form */
      value =  y[i]
        + diff
          * ((y[i + 1] - y[i]) / h - h * (z[i + 1] + z[i] * 2.0) / 6.0
             + diff * (0.5 * z[i] + diff * (z[i + 1] - z[i]) / (6.0 * h)));
    }
    else if(tension > 0.0)
    {   /* `positive' (really real) tension, use sinh's */
      if(Math.abs(tension * h) < TRIG_ARG_MIN)
      {  /* hand-compute (6/y^2)(sinh(xy)/sinh(y) - x) to improve accuracy;
            here `x' means reldiff or relupdiff and `y' means tension*h */
        value = (y[i] * relupdiff + y[i+1] * reldiff
                 + ((z[i] * h * h / 6.0)
                    * quotient_sinh_func (relupdiff, tension * h))
                 + ((z[i+1] * h * h / 6.0)
                    * quotient_sinh_func (reldiff, tension * h)));
      }
      else if(Math.abs(tension * h) > TRIG_ARG_MAX)
      { /* approximate 1/sinh(y) by 2 sgn(y) exp(-|y|) */
        int sign = ((h < 0.0) ? -1 : 1);

        value = (((z[i] * (Math.exp (tension * updiff - sign * tension * h)
                           + Math.exp (-tension * updiff - sign * tension * h))
                   + z[i + 1] * (Math.exp (tension * diff - sign * tension * h)
                           + Math.exp (-tension * diff - sign * tension*h)))
                  * (sign / (tension * tension)))
                 + (y[i] - z[i] / (tension * tension)) * (updiff / h)
                 + (y[i + 1] - z[i + 1] / (tension * tension)) * (diff / h));
      }
      else
      {
        value = (((z[i] * sinh (tension * updiff)
                   + z[i + 1] * sinh (tension * diff))
                  / (tension * tension * sinh (tension * h)))
                 + (y[i] - z[i] / (tension * tension)) * (updiff / h)
                 + (y[i + 1] - z[i + 1] / (tension * tension)) * (diff / h));
      }
    }
    else
    {   /* `negative' (really imaginary) tension, use sin's */
      if(Math.abs(tension * h) < TRIG_ARG_MIN)
      {  /* hand-compute (6/y^2)(sin(xy)/sin(y) - x) to improve accuracy;
            here `x' means reldiff or relupdiff and `y' means tension*h */
        value = (y[i] * relupdiff + y[i+1] * reldiff
                 + ((z[i] * h * h / 6.0)
                    * quotient_sin_func (relupdiff, tension * h))
                 + ((z[i+1] * h * h / 6.0)
                    * quotient_sin_func (reldiff, tension * h)));
      }
      else
      {
        value = (((z[i] * Math.sin (tension * updiff)
                   + z[i + 1] * Math.sin (tension * diff))
                  / (tension * tension * Math.sin (tension * h)))
                 + (y[i] - z[i] / (tension * tension)) * (updiff / h)
                 + (y[i + 1] - z[i + 1] / (tension * tension)) * (diff / h));
      }
    }
    return value;
  }

  /**
   * Checks whether an array of data points has monotonic values.
   * @param t array of double values to be checked.
   * @return true if the given array is monotonic; false if not.
   */
  public final boolean isMonotonic(double [] t)
  {
    final boolean isAscending;
    int n;

    if((n=t.length-1) <= 0)
      return false;

    if(t[n-1] < t[n])
      isAscending = true;
    else if(t[n-1] > t[n])
      isAscending = false;
    else                               //equality
      return false;

    while(n > 0)
    {
      n--;
      if(isAscending ? (t[n] >= t[n+1]) : (t[n] <= t[n+1]))
        return false;
    }
    return true;
  }

  /**
   * Returns the hyperbolic sine of a <code>double</code> value.
   * The hyperbolic sine of <i>x</i> is defined to be
   * (<i>e<sup>x</sup>&nbsp;-&nbsp;e<sup>-x</sup></i>)/2
   * where <i>e</i> is Euler's number.
   * @param value The number whose hyperbolic sine is to be returned.
   * @return The hyperbolic sine of <code>x</code>.
   */
  public static double sinh(double value)
  {
    return (Math.exp(value) - Math.exp(-value)) / 2.0;
  }

  /**
   * Returns the hyperbolic tangent of a <code>double</code> value.
   * The hyperbolic tangent of <i>x</i> is defined to be
   * (<i>e<sup>x</sup>&nbsp;-&nbsp;e<sup>-x</sup></i>)/
   * (<i>e<sup>x</sup>&nbsp;+&nbsp;e<sup>-x</sup></i>),
   * in other words, sinh(<i>x</i>)/cosh(<i>x</i>).  Note
   * that the absolute value of the exact tanh is always
   * less than 1.
   * @param value The number whose hyperbolic tangent is to be returned.
   * @return The hyperbolic tangent of <code>x</code>.
   */
  public static double tanh(double value)
  {
    final double expVal = Math.exp(value);
    final double expNVal = Math.exp(-value);
    return (expVal - expNVal) / (expVal + expNVal);
  }

  /**
   * These four functions compute (6/x^2)(1-x/sinh(x)),
   * (3/x^2)(x/tanh(x)-1), (6/x^2)(1-x/sin(x)), and (3/x^2)(x/tan(x)-1) via
   * the first three terms of the appropriate power series.  They are used
   * when |x|<TRIG_ARG_MIN, to avoid loss of significance.  Errors are
   * O(x**6).
   */
  public static double sinh_func(double x)
  {
         //use 1-(7/60)x**2+(31/2520)x**4
    return 1.0 - (7.0/60.0)*x*x + (31.0/2520.0)*x*x*x*x;
  }

  /**
   * These four functions compute (6/x^2)(1-x/sinh(x)),
   * (3/x^2)(x/tanh(x)-1), (6/x^2)(1-x/sin(x)), and (3/x^2)(x/tan(x)-1) via
   * the first three terms of the appropriate power series.  They are used
   * when |x|<TRIG_ARG_MIN, to avoid loss of significance.  Errors are
   * O(x**6).
   */
  public static double tanh_func(double x)
  {
         //use 1-(1/15)x**2+(2/315)x**4
    return 1.0 - (1.0/15.0)*x*x + (2.0/315.0)*x*x*x*x;
  }

  /**
   * These four functions compute (6/x^2)(1-x/sinh(x)),
   * (3/x^2)(x/tanh(x)-1), (6/x^2)(1-x/sin(x)), and (3/x^2)(x/tan(x)-1) via
   * the first three terms of the appropriate power series.  They are used
   * when |x|<TRIG_ARG_MIN, to avoid loss of significance.  Errors are
   * O(x**6).
   */
  public static double sin_func(double x)
  {
         //use -1-(7/60)x**2-(31/2520)x**4
    return -1.0 - (7.0/60.0)*x*x - (31.0/2520.0)*x*x*x*x;
  }

  /**
   * These four functions compute (6/x^2)(1-x/sinh(x)),
   * (3/x^2)(x/tanh(x)-1), (6/x^2)(1-x/sin(x)), and (3/x^2)(x/tan(x)-1) via
   * the first three terms of the appropriate power series.  They are used
   * when |x|<TRIG_ARG_MIN, to avoid loss of significance.  Errors are
   * O(x**6).
   */
  public static double tan_func(double x)
  {
         //use -1-(1/15)x**2-(2/315)x**4
    return -1.0 - (1.0/15.0)*x*x - (2.0/315.0)*x*x*x*x;
  }

  /**
   * These two functions compute (6/y^2)(sinh(xy)/sinh(y)-x) and
   * (6/y^2)(sin(xy)/sin(y)-x), via the first three terms of the appropriate
   * power series in y.  They are used when |y|<TRIG_ARG_MIN, to avoid loss
   * of significance.  Errors are O(y**6).
   */
  public static double quotient_sinh_func(double x, double y)
  {
    return ((x*x*x-x) + (x*x*x*x*x/20.0 - x*x*x/6.0 + 7.0*x/60.0)*(y*y)
            + (x*x*x*x*x*x*x/840.0 - x*x*x*x*x/120.0 + 7.0*x*x*x/360.0
               -31.0*x/2520.0)*(y*y*y*y));
  }

  /**
   * These two functions compute (6/y^2)(sinh(xy)/sinh(y)-x) and
   * (6/y^2)(sin(xy)/sin(y)-x), via the first three terms of the appropriate
   * power series in y.  They are used when |y|<TRIG_ARG_MIN, to avoid loss
   * of significance.  Errors are O(y**6).
   */
  public static double quotient_sin_func(double x, double y)
  {
    return (- (x*x*x-x) + (x*x*x*x*x/20.0 - x*x*x/6.0 + 7.0*x/60.0)*(y*y)
            - (x*x*x*x*x*x*x/840.0 - x*x*x*x*x/120.0 + 7.0*x*x*x/360.0
               -31.0*x/2520.0)*(y*y*y*y));
  }


/*
  public static void main(String [] args)
  {
    final double [] t = {
                   1.000000E-02, 2.330000E-02, 2.860000E-02, 3.700000E-02,
                   5.260000E-02, 9.090000E-02, 8.617700E-01, 1.817120E+00,
                   3.000000E+00, 5.646220E+00, 8.000000E+00, 1.000000E+01 };
    final double [] y = {
                   5.666004E+03, 1.776234E+04, 2.245442E+04, 2.976875E+04,
                   4.313147E+04, 7.548593E+04, 7.148388E+05, 1.469329E+06,
                   2.174600E+06, 1.793237E+06, 3.174747E+05, 4.924337E+01 };

    final int numXVals = 101;
    final double interval = (t[t.length-1] - t[0]) / (numXVals - 1);
    final double [] xValsArr = new double[numXVals];
    for(int i=0; i<numXVals; ++i)
      xValsArr[i] = t[0] + i*interval;

    final CubicSpline splineObj = new CubicSpline();
    final double [] retValsArr;
    if((retValsArr=splineObj.calcSpline(t,y,0.0,1.0,xValsArr)) == null)
    {
      System.err.println("Error calculating spline:  " +
                                         splineObj.getErrorMessageString());
      return;
    }

    if(retValsArr.length != xValsArr.length)
    {
      System.err.println("Length of returned array (" + retValsArr.length +
           ") not equal to length of 'xValsarr' (" + xValsArr.length + ")");
      return;
    }

    for(int i=0; i<xValsArr.length; ++i)
    {
      System.out.println(xValsArr[i] + "  " + retValsArr[i]);
    }
  }
*/
}
