//StatFns.java:  Contains various static utility methods for statistics
//               functions.
//
//   5/7/2007 -- [ET]
//

package com.isti.util.math;

import java.util.Arrays;

/**
 * Class StatFns contains various static utility methods for statistics
 * functions.
 */
public class StatFns
{
    //private constructor so that no object instances may be created
    // (static access only)
  private StatFns()
  {
  }

  /**
   * Computes the median of values in the given array.
   * @param dblArr array of 'double' values.
   * @param arrSortedFlag true if given array is in sorted order;
   * false if not.
   * @return Median of values in the given array.
   */
  public static double computeMedian(double [] dblArr, boolean arrSortedFlag)
  {
    final int arrLen = dblArr.length;       //length of given array
    switch (arrLen) {
    case 0:
      return Double.NaN;
    case 1:
      return dblArr[0];
    case 2:
      return (dblArr[0] + dblArr[1]) / 2;
    }
    if(!arrSortedFlag)
    {    //given array is not in sorted order; create copy of array
      final double [] newArr = new double[arrLen];
      System.arraycopy(dblArr,0,newArr,0,arrLen);
      Arrays.sort(newArr);        //put copied array in sorted order
      dblArr = newArr;            //use sorted version of array
    }
    //if even number of values then return average
    // of two middle values; otherwise return middle value:
    final int arrLen2 = arrLen / 2;
    return (arrLen % 2 == 0) ?  (dblArr[arrLen2] + dblArr[arrLen2+1]) / 2 : dblArr[arrLen2];
  }

  /**
   * Computes the median of values in the given array.
   * @param dblArr array of 'double' values.
   * @return Median of values in the given array.
   */
  public static double computeMedian(double [] dblArr)
  {
    return computeMedian(dblArr,false);
  }
}
