//IstiMenuHandler.java:  Implements a Menu Handler for menu actions.
//
//   8/23/2002 -- [KF]
//

package com.isti.util.menu;

import java.awt.AWTEvent;
import java.awt.event.*;

/**
 * Class IstiMenuHandler implements a Menu Handler for menu actions.
 */
public class IstiMenuHandler implements IstiMenuListener, ActionListener,
    ItemListener
{
  /**
   * This method is called when a MenuItem event happens.
   *
   * @param name the name of the menu item.
   * @param displayText the display text for the menu item.
   * @param isSelected true if the menu item is selected.
   * @param e the AWTEvent.
   *
   * OVERRIDE this function to process menu item events.
   */
  public void processMenuItemEvent(String name, String displayText,
                                   boolean isSelected, AWTEvent e)
  {
    if (displayText != null)
    {
      System.out.println(this.getClass() + ".processMenuItemEvent: " +
                         (isSelected?"selected ":"") + displayText);
    }
  }

  /**
   * This method is called when a MenuItem is instantiated.
   *
   * @param name the name of the menu item.
   * @param displayText the display text for the menu item.
   *
   * @return true if the menu item should be initially selected.
   *
   * OVERRIDE this function if you want to set the default for a menu item
   * to be selected.
   */
  public boolean isMenuItemSelected(String name, String displayText)
  {
    return false;
  }

  /**
   * This method is called when a MenuItem is instantiated.
   *
   * @param menuItem the menu item.
   *
   * @return true if the menu item should be initially selected.
   */
  public boolean isMenuItemSelected(Object menuItem)
  {
    String name = null;
    String displayText = null;

    if (menuItem instanceof java.awt.MenuItem)
    {
      final java.awt.MenuItem mi = (java.awt.MenuItem)menuItem;
      name = mi.getName();
      displayText = mi.getLabel();
    }
    else if (menuItem instanceof javax.swing.AbstractButton)
    {
      final javax.swing.AbstractButton ab =
          (javax.swing.AbstractButton)menuItem;
      name = ab.getName();
      displayText = ab.getText();
    }

    return isMenuItemSelected(name, displayText);
  }

  /**
   * This method is called when a MenuItem event happens.
   *
   * @param e the AWTEvent.
   */
  protected void processMenuItemEvent(AWTEvent e)
  {
    String name = null;
    String displayText = null;
    boolean isSelected = false;
    final Object menuItem = e.getSource();

    if (menuItem instanceof java.awt.MenuItem)
    {
      final java.awt.MenuItem mi = (java.awt.MenuItem)menuItem;
      name = mi.getName();
      displayText = mi.getLabel();

      if (menuItem instanceof java.awt.CheckboxMenuItem)
      {
        final java.awt.CheckboxMenuItem cmi =
            (java.awt.CheckboxMenuItem)menuItem;
        isSelected = cmi.getState();
      }
    }
    else if (menuItem instanceof javax.swing.AbstractButton)
    {
      final javax.swing.AbstractButton ab =
          (javax.swing.AbstractButton)menuItem;
      name = ab.getName();
      displayText = ab.getText();
      isSelected = ab.isSelected();
    }

    processMenuItemEvent(name, displayText, isSelected, e);
  }

  /**
   * This method is called when a MenuItem is activated.
   *
   * @param e the ActionEvent.
   */
  public void actionPerformed(ActionEvent e)
  {
    processMenuItemEvent(e);
  }

  /**
   * This method is called when a MenuItem state is changed.
   *
   * @param e the ItemEvent.
   */
  public void itemStateChanged(ItemEvent e)
  {
    processMenuItemEvent(e);
  }
}