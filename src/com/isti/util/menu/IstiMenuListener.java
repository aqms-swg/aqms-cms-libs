//IstiMenuListener.java:  Implements a Menu Listener Interface for menu actions.
//
//   8/28/2002 -- [KF]
//

package com.isti.util.menu;

import java.awt.AWTEvent;

/**
 * Class IstiMenuListener implements a Menu Listener Interface for menu actions.
 */
public interface IstiMenuListener
{
  /**
   * This method is called when a MenuItem is instantiated.
   *
   * @param name the name of the menu item.
   * @param displayText the display text for the menu item.
   *
   * @return true if the menu item should be initially selected.
   */
  public boolean isMenuItemSelected(String name, String displayText);

  /**
   * This method is called when a MenuItem is instantiated.
   *
   * @param menuItem the menu item.
   *
   * @return true if the menu item should be initially selected.
   */
  public boolean isMenuItemSelected(Object menuItem);

  /**
   * This method is called when a MenuItem event happens.
   *
   * @param name the name of the menu item.
   * @param displayText the display text for the menu item.
   * @param isSelected true if the menu item is selected.
   * @param e the AWTEvent.
   */
  public void processMenuItemEvent(String name, String displayText,
                                   boolean isSelected, AWTEvent e);
}