//IstiMenuLoader.java:  Implements a XML menu loader.
//
//   8/23/2002 -- [KF]
//   10/3/2002 -- [KF] - Do not enforce unique names while loading menu items,
//                       Use MNEMONIC for MENU.
//   6/26/2003 -- [KF]   Fixed 'findMenuItem' to work with 'MenuElement' objects.
//   7/23/2004 -- [KF]   Added support for menu bar menu items.
//   7/24/2004 -- [KF]   Modified so that any 'JMenu' with no items and an
//                       all-whitespace name (a separator) is disabled.
//   9/16/2004 -- [KF]   Modified so that space is automatically added before
//                       all menu bar menu items.
//   1/19/2006 -- [KF]   Added 'getTopMenuBarObject()' and
//                       'updateMenuItemShown()' methods.
//    3/9/2006 -- [KF]   Changed to save the 'topMenuBarObject' when loading.
//  10/15/2008 -- [ET]   Renamed 'isFocusableTraverable()' method to
//                       'isFocusable()'.
//   8/30/2010 -- [ET]   Modified 'SpaceMenuItem' class to prevent focus
//                       and mouse-rollover effects.
//

package com.isti.util.menu;

import java.awt.Insets;
import java.awt.event.*;
import java.util.*;
import javax.swing.*;
import org.jdom.Element;
import com.isti.util.IstiXmlLoader;
import com.isti.util.gui.IstiPopupMenu;
import com.isti.util.gui.HandJButton;

/**
 * Class IstiMenuLoader implements a XML menu loader.
 */
public class IstiMenuLoader extends IstiXmlLoader
{
  /** The name string for the top menu bar. */
  public final static String TOP_MENU_BAR_NAME = "TopMenu";
  private Object topMenuBarObject = null;  //the top menu object
  private Hashtable nameHashTable = new Hashtable();       // name hash table
  private ActionListener actionListener = null; // handle for action listener
  private final String rootElementName = IstiMenuTags.ROOT;
  private boolean menuLoadInProgressFlag = false;

  /**
   * Constructs a XML menu loader with no default <code>ActionListener</code>.
   * The <code>setDefaultActionListener</code> method should be called or a
   * <code>Handler</code> should be provided in the XML.
   */
  public IstiMenuLoader()
  {
    this(null);
  }

  /**
   * Constructs a XML menu loader.
   *
   * @param l the default <code>ActionListener</code>
   *
   * @see <code>setDefaultActionListener</code> for <code>ActionListener</code>
   * details.
   */
  public IstiMenuLoader(ActionListener l)
  {
    super();
    setDefaultActionListener(l);
  }

  /**
   * Gets the child menu object.
   * @param childIndex index of child element to return.
   * @param parentObject the parent object for the element.
   *
   * @return     the child menu object.
   */
  public Object getChildMenuObject(int childIndex, Object parentObject)
  {
    return processChildElement(getRootElement(), childIndex, parentObject);
  }

  /**
   * Creates a <code>Frame</code> with the specified top menu object.
   * @param topMenuObject the top menu object to be added to the frame.
   *
   * @return     the top menu object or null if error.
   * Use <code>getErrorMessage</code> to determine error.
   * @see getErrorMessage
   * @see clearErrorMessage
   */
  public java.awt.Frame createFrameWithMenuBar(Object topMenuObject)
  {
    // get screen size:
    final java.awt.Dimension d =
        java.awt.Toolkit.getDefaultToolkit().getScreenSize();
    return createFrameWithMenuBar(topMenuObject, d.width/2, d.height/2);
  }

  /**
   * Creates a <code>Frame</code> with the specified top menu object
   * and resizes this component so that it has width <code>width</code>
   * and <code>height</code>.
   * @param topMenuObject the top menu object to be added to the frame.
   * @param width The new width of this component in pixels.
   * @param height The new height of this component in pixels.
   *
   * @return     the top menu object or null if error.
   * Use <code>getErrorMessage</code> to determine error.
   * @see getErrorMessage
   * @see clearErrorMessage
   */
  public java.awt.Frame createFrameWithMenuBar(Object topMenuObject,
      int width,int height)
  {
    final java.awt.Frame frameObj;
    // if top menu object is an Awt MenuBar
    if (topMenuObject instanceof java.awt.MenuBar)
    {
      // create an Awt Frame and add the menu bar
      frameObj = new java.awt.Frame();
      frameObj.setMenuBar((java.awt.MenuBar)topMenuObject);
    }
    // if top menu object is a Swing JMenuBar
    else if (topMenuObject instanceof javax.swing.JMenuBar)
    {
      // create a Swing JFrame and add the menu bar
      frameObj = new javax.swing.JFrame();
      ((javax.swing.JFrame)frameObj).setJMenuBar(
          (javax.swing.JMenuBar)topMenuObject);
    }
    else
    {
      setErrorMessage("Top menu object is not a MenuBar or JMenuBar: " +
                      topMenuObject.getClass());
      return null;
    }

    //set frame size
    frameObj.setSize(width, height);

    //get screen size:
    final java.awt.Dimension dimObj =
        java.awt.Toolkit.getDefaultToolkit().getScreenSize();

    //put frame at center of screen:
    frameObj.setLocation((dimObj.width-frameObj.getSize().width)/2,
                         (dimObj.height-frameObj.getSize().height)/2);
    return frameObj;
  }

  /**
   * Gets the top menu bar object.
   *
   * @return     the top menu bar object.
   * @see TOP_MENU_BAR_NAME
   */
  public Object getTopMenuBarObject()
  {
    if (topMenuBarObject == null)
    {
      topMenuBarObject = getTopMenuObject(TOP_MENU_BAR_NAME);
    }
    return topMenuBarObject;
  }

  /**
   * Gets the top menu object.
   *
   * @return     the top menu object.
   */
  public Object getTopMenuObject()
  {
    return  processChildElement(getRootElement(), 0, null);
  }

  /**
   * Gets the top menu object.
   * @param index index of top menu object to return.
   *
   * @return     the top menu object.
   */
  public Object getTopMenuObject(int index)
  {
    return  processChildElement(getRootElement(), index, null);
  }

  /**
   * Gets the top menu object.
   * @param name the name of the menu object to be found
   *
   * @return     the top menu object.
   */
  public Object getTopMenuObject(String name)
  {
    return  processChildElement(getRootElement(), IstiMenuTags.NAME, name,
                                null);
  }

  /**
   * Loads the XML file.
   *
   * @param fileName the name of the XML file to load.
   *
   * @return true if the file was loaded
   */
  public boolean loadFile(String fileName)
  {
    final boolean fileLoaded = super.loadFile(fileName, rootElementName);
    return fileLoaded;
  }

  /**
   * Processes an error message.
   *
   * NOTE: Override this function to process error messages.
   *
   * @param      str   the error message.
   */
  protected void processErrorMessage(String str)
  {
    System.out.println(str);
  }

  /**
   * Sets the default <code>ActionListener</code> for the menu.
   * @param l the default <code>ActionListener</code>
   * which may be a standard <code>ActionListener</code> or an
   * <code>IstiMenuListener</code> such as <code>IstiMenuHandler</code>.
   */
  public final void setDefaultActionListener(ActionListener l)
  {
    actionListener = l;
  }


  /**
   * Processes the element for this document.
   * @param element the element to process
   * @param parentObject the parent object for the element
   * @param parentElement the parent element for the element.
   *
   * @return     an object for the element or null if no object is needed.
   */
  protected Object processElement(Element element, Object parentObject,
                                  Element parentElement)
  {
    // if menu separator
    if (element.getName().equalsIgnoreCase(IstiMenuTags.MENU_SEPARATOR))
    {
      if (!parentElement.getName().equalsIgnoreCase(IstiMenuTags.MENU))
      {
        processErrorMessage(
            this.getClass() + ".processElement - " +
            element.getName() +
            " cannot be added to: " + parentElement.getName());
        return null;
      }

      // add a menu separator
      addMenuSeparator(parentObject);
      return null;
    }

    Object menuItem = null;

    // if menu load
    if (element.getName().equalsIgnoreCase(IstiMenuTags.MENU_LOAD))
    {
      menuLoadInProgressFlag = true;

      // load name string
      final String loadNameString =
          element.getAttributeValue(IstiMenuTags.LOAD_NAME);

      // find the specified menu item and add it
      menuItem = loadMenuItem(loadNameString, parentObject, parentElement);

      menuLoadInProgressFlag = false;

      if (menuItem == null)
      {
        processErrorMessage(
            this.getClass() + ".processElement - Failed to load menu item: " +
            loadNameString);
      }
      return null;
    }

    // name string
    final String nameString = element.getAttributeValue(IstiMenuTags.NAME);
    if (nameString == null)
    {
      processErrorMessage(
          this.getClass() + ".processElement - " +
          IstiMenuTags.NAME +
          " string not found for: " +
          element.getName());
      return null;
    }

    // if not menu load make sure the name string is unique
    if (!menuLoadInProgressFlag &&
        nameHashTable.containsKey(nameString))
    {
      processErrorMessage(
          this.getClass() + ".processElement - " +
          IstiMenuTags.NAME +
          " string is not unique: " +
          nameString);
      return null;
    }

    // get class string
    final String classString = element.getAttributeValue(IstiMenuTags.CLASS);

    if (element.getName().equalsIgnoreCase(IstiMenuTags.MENU_BAR) ||
        element.getName().equalsIgnoreCase(IstiMenuTags.POPUP_MENU))
    {
      if (!parentElement.getName().equalsIgnoreCase(IstiMenuTags.ROOT))
      {
        processErrorMessage(
            this.getClass() + ".processElement - " +
            element.getName() +
            " cannot be added to: " + parentElement.getName());
        return null;
      }
      if (classString == null)
      {
        if (element.getName().equalsIgnoreCase(IstiMenuTags.MENU_BAR))
          menuItem = new javax.swing.JMenuBar();
        else
          menuItem = new IstiPopupMenu();
      }
    }
    else if (element.getName().equalsIgnoreCase(IstiMenuTags.MENU))
    {
      if (!parentElement.getName().equalsIgnoreCase(IstiMenuTags.MENU_BAR) &&
          !parentElement.getName().equalsIgnoreCase(IstiMenuTags.POPUP_MENU) &&
          !parentElement.getName().equalsIgnoreCase(IstiMenuTags.MENU))
      {
        processErrorMessage(
            this.getClass() + ".processElement - " +
            element.getName() +
            " cannot be added to: " + parentElement.getName());
        return null;
      }
      if (classString == null)
        menuItem = new javax.swing.JMenu();
    }
    else if (
        element.getName().equalsIgnoreCase(IstiMenuTags.MENU_ITEM) ||
        element.getName().equalsIgnoreCase(IstiMenuTags.CHECKBOX_MENU_ITEM))
    {
      if (parentElement.getName().equalsIgnoreCase(IstiMenuTags.MENU_BAR) &&
          element.getName().equalsIgnoreCase(IstiMenuTags.MENU_ITEM))
      {
        menuItem = new MenuBarMenuItem();
      }
      else if (!parentElement.getName().equalsIgnoreCase(IstiMenuTags.POPUP_MENU) &&
               !parentElement.getName().equalsIgnoreCase(IstiMenuTags.MENU))
      {
        processErrorMessage(
            this.getClass() + ".processElement - " +
            element.getName() +
            " cannot be added to: " + parentElement.getName());
        return null;
      }
      else if (classString == null)
      {
        if (element.getName().equalsIgnoreCase(IstiMenuTags.MENU_ITEM))
          menuItem = new javax.swing.JMenuItem();
        else
          menuItem = new javax.swing.JCheckBoxMenuItem();
      }
    }
    else
    {
      processErrorMessage(
          this.getClass() + ".processElement - Unknown tag: " +
          element.getName());
      return null;
    }

    // if the class was specified
    if (classString != null)
      menuItem = getComponentForClass(classString);

    if (menuItem == null)
    {
      if (this.getErrorFlag())
      {
        processErrorMessage(
            this.getClass() + ".processElement - Menu item creation error: " +
            this.getErrorMessage());
        this.clearErrorMessage();
      }
      else
      {
        processErrorMessage(
            this.getClass() +
            ".processElement - Menu item creation error with element: " +
            element.getName());
      }
      return null;
    }

    //if not menu load
    if (!menuLoadInProgressFlag)
      // add name to the name hash table
      nameHashTable.put(nameString, menuItem);

    //add space before menu bar
    if (menuItem instanceof MenuBarMenuItem)
      addMenuItem(new SpaceMenuItem(), parentObject);

    // add the item to the parent
    addMenuItem(menuItem, parentObject);

    // process element attributes
    processElementAttributes(element, menuItem);

    //if this is a JMenu with no items and all whitespace
    // for a name (a separator) then disable it:
    if (menuItem instanceof JMenu && ((JMenu)menuItem).getItemCount() <= 0 &&
                           ((JMenu)menuItem).getName().trim().length() <= 0)
    {
      ((JMenu)menuItem).setEnabled(false);
      ((JMenu)menuItem).setFocusPainted(false);       //disable focus effect
      ((JMenu)menuItem).setContentAreaFilled(false);  //don't paint pixels
      ((JMenu)menuItem).setOpaque(false);             //transparent
      ((JMenu)menuItem).setRolloverEnabled(false);    //disable mouse-over
      ((JMenu)menuItem).setRequestFocusEnabled(false);     //don't allow focus
    }

    if (nameString.equals(TOP_MENU_BAR_NAME))  //if top menu bar
    {
      topMenuBarObject = menuItem;  //save it
    }

    // return the new menu item
    return menuItem;
  }

  /**
   * Processes element attributes.
   * @param element the element to process
   * @param menuItem the menu item
   */
  protected void processElementAttributes(Element element, Object menuItem)
  {
    // menu item name
    final String nameString = element.getAttributeValue(IstiMenuTags.NAME);
    if (nameString != null)
    {
      if (menuItem instanceof java.awt.MenuComponent)
        ((java.awt.MenuComponent)menuItem).setName(nameString);
      else if (menuItem instanceof java.awt.Component)
        ((java.awt.Component)menuItem).setName(nameString);
      else
      {
        processErrorMessage(
            this.getClass() +
            ".processElementAttributes - Do not know how to set name for: " +
            menuItem);
      }
    }

    // menu item display text for menu or menu item
    if (element.getName().equalsIgnoreCase(IstiMenuTags.MENU) ||
        element.getName().equalsIgnoreCase(IstiMenuTags.MENU_ITEM) ||
        element.getName().equalsIgnoreCase(IstiMenuTags.CHECKBOX_MENU_ITEM))
    {
      String textString = element.getAttributeValue(IstiMenuTags.DISPLAY_TEXT);
      if (textString == null)
        textString = nameString;  // use name since no text was provided
      if (textString != null)
      {
        if (menuItem instanceof java.awt.MenuItem)
          ((java.awt.MenuItem)menuItem).setLabel(textString);
        else if (menuItem instanceof javax.swing.AbstractButton)
          ((javax.swing.AbstractButton)menuItem).setText(textString);
        else
        {
          processErrorMessage(
              this.getClass() +
              ".processElementAttributes - " +
              "Do not know how to set display text for: " +
              menuItem);
        }
      }
    }

    // menu item mnemonic for menu item
    if (element.getName().equalsIgnoreCase(IstiMenuTags.MENU) ||
        element.getName().equalsIgnoreCase(IstiMenuTags.MENU_ITEM) ||
        element.getName().equalsIgnoreCase(IstiMenuTags.CHECKBOX_MENU_ITEM))
    {
      final String mnemonicString =
          element.getAttributeValue(IstiMenuTags.MNEMONIC);
      if (mnemonicString != null)
      {
        if (menuItem instanceof javax.swing.AbstractButton)
        {
          ((javax.swing.AbstractButton)menuItem).setMnemonic(
              mnemonicString.charAt(0));
        }
        else if (!(menuItem instanceof java.awt.MenuItem))
        {     // menu items cannot have a mnemonic
          processErrorMessage(
          this.getClass() + ".processElementAttributes - " +
          "Do not know how to set mnemonic for: " + menuItem);
        }
      }
    }

    // menu item accelerator for menu item
    if (element.getName().equalsIgnoreCase(IstiMenuTags.MENU_ITEM) ||
        element.getName().equalsIgnoreCase(IstiMenuTags.CHECKBOX_MENU_ITEM))
    {
      final String acceleratorString =
          element.getAttributeValue(IstiMenuTags.ACCELERATOR);
      if (acceleratorString != null)
      {
        javax.swing.KeyStroke keystroke;

        // if cannot convert to keystroke
        if ((keystroke =
             javax.swing.KeyStroke.getKeyStroke(acceleratorString)) == null)
        {
          processErrorMessage(this.getClass() + ".processElementAttributes - " +
                              "Cannot convert to keystroke: " +
                              acceleratorString);
        }
        else
        {
          if (menuItem instanceof java.awt.MenuItem)
          {
            final int modifiers = keystroke.getModifiers();

            // use shift modifier if modifiers contain
            // other than the menu shortcut key
            final boolean useShiftModifier = ((modifiers &
                java.awt.Toolkit.getDefaultToolkit().getMenuShortcutKeyMask())
                != modifiers);

            final java.awt.MenuShortcut menuShortcut =
                new java.awt.MenuShortcut(keystroke.getKeyCode(),
                useShiftModifier);

            // set the accelerator for the menu item
            ((java.awt.MenuItem)menuItem).setShortcut(menuShortcut);
          }
          else if (menuItem instanceof javax.swing.JMenuItem)
          {
            // set the accelerator for the menu item
            ((javax.swing.JMenuItem)menuItem).setAccelerator(keystroke);
          }
          else if (menuItem instanceof javax.swing.AbstractButton)
          {
            // ignore the accelerator for the menu item
          }
          else
          {
            processErrorMessage(
                this.getClass() + ".processElementAttributes - " +
                "Do not know how to set the menu accelerator for: " +
                menuItem);
          }
        }
      }
    }

    // menu item handler for all menu items other than top
    ActionListener l = null;
    if (!element.getName().equalsIgnoreCase(IstiMenuTags.MENU_BAR) &&
        !element.getName().equalsIgnoreCase(IstiMenuTags.POPUP_MENU))
    {
      final String handlerString =
          element.getAttributeValue(IstiMenuTags.HANDLER);
      l = addActionListener(menuItem, handlerString);
    }

    // menu item selected for checkbox menu item
    if (element.getName().equalsIgnoreCase(IstiMenuTags.CHECKBOX_MENU_ITEM))
    {
      String selectedString = element.getAttributeValue(IstiMenuTags.SELECTED);
      // if selected string not provided and
      // the action listener is an ISTI Menu Listener
      if (selectedString == null && l instanceof IstiMenuListener)
      {
        // get the selected flag from the ISTI Menu Listener
        final boolean isSelected =
            ((IstiMenuListener)l).isMenuItemSelected(menuItem);
        selectedString = Boolean.valueOf(isSelected).toString();
      }
      if (selectedString != null)
      {
        // convert the string to boolean value
        final boolean selectedFlag = Boolean.valueOf(selectedString).booleanValue();

        if (menuItem instanceof java.awt.CheckboxMenuItem)
          ((java.awt.CheckboxMenuItem)menuItem).setState(selectedFlag);
        else if (menuItem instanceof javax.swing.AbstractButton)
          ((javax.swing.AbstractButton)menuItem).setSelected(selectedFlag);
        else if (!(menuItem instanceof java.awt.MenuItem))
        {     // MenuItem do not have states
          processErrorMessage(
          this.getClass() +
          ".processElementAttributes - Do not know how to set state for: " +
          menuItem);
        }
      }
    }
  }

  /**
   * Adds an action listener to the menu item
   *
   * @param menuItem the menu item
   * @param handlerString the string representation of the handler
   *
   * @return the action listener
   */
  protected ActionListener addActionListener(Object menuItem, String handlerString)
  {
    // MenuBar and JMenuBar cannot have an action listener
    if (menuItem instanceof java.awt.MenuBar ||
        menuItem instanceof javax.swing.JMenuBar)
    {
      return null;
    }

    // default to main action listener
    ActionListener l = actionListener;

    if (handlerString != null)
    {
      Object object = getComponentForClass(handlerString);
      if (object instanceof ActionListener)
      {
        l = (ActionListener)object;
      }
      else if (object == null)
      {
        processErrorMessage(
            this.getClass() +
            ".addActionListener - getComponentForClass: " +
            this.getErrorMessage());
        this.clearErrorMessage();
      }
      else
      {
        processErrorMessage(
            this.getClass() +
            ".addActionListener - Handler does not have an action listener:" +
            handlerString);
      }
    }
    else
    {
      ActionListener[] als = null;
      if (menuItem instanceof java.awt.MenuItem)
      {
        final java.awt.MenuContainer mc =
            ((java.awt.MenuItem)menuItem).getParent();
        if (mc instanceof java.awt.MenuItem)
        {
          als = (ActionListener[])(((java.awt.MenuItem)mc).getListeners(
              ActionListener.class));
        }
        else if (mc != null && !(mc instanceof java.awt.MenuBar))
        {     // Menu Bar does not have action listener
          processErrorMessage(
          this.getClass() + ".addActionListener - " +
          "Do not know how to get action listener for parent: " + mc);
        }
      }
      else if (menuItem instanceof java.awt.Component)
      {
        // get the parent container for this component
        final java.awt.Container mc =
            ((java.awt.Component)menuItem).getParent();

        // if the container is a swing popup menu
        if (mc instanceof javax.swing.JPopupMenu)
        {
          javax.swing.JPopupMenu pm = (javax.swing.JPopupMenu)mc;

          // get the action listener from the invoker class
          final java.awt.Component pmInvoker = pm.getInvoker();
          if (pmInvoker != null)
            als = (ActionListener[])(pmInvoker.getListeners(
                ActionListener.class));
        }
        // if menu component exists and no action listeners found yet
        if (mc != null && als == null)
          als = (ActionListener[])(mc.getListeners(ActionListener.class));
      }
      else
      {
        processErrorMessage(
            this.getClass() + ".addActionListener - " +
            "Do not know how to get action listener for: " + menuItem);
      }

      // if action listener was found
      if (als != null && als.length >= 1)
        l = als[0];  // use the first one
    }

    // add the action listener
    if (menuItem instanceof java.awt.MenuItem)
    {
      // add the action listener for the MenuItem
      ((java.awt.MenuItem)menuItem).addActionListener(l);

      // if AWT check box item
      if (menuItem instanceof java.awt.CheckboxMenuItem)
      { // if the action listener is also an item listener
        if (l instanceof ItemListener)
        {
          // add the item listener for the CheckboxMenuItem
          ((java.awt.CheckboxMenuItem)menuItem).addItemListener(
          (ItemListener)l);
        }
      }
    }
    else if (menuItem instanceof javax.swing.AbstractButton)
    {
      // add the action listener for the AbstractButton
      ((javax.swing.AbstractButton)menuItem).addActionListener(l);
    }
    else
    {
      processErrorMessage(
          this.getClass() +
          ".addActionListener - Do not know how to add action listener for: " +
          menuItem);
    }

    return l;
  }

  /**
   * Adds the menu item to the parent.
   * @param menuItem the menu item
   * @param parentObject the parent object for the menu item
   */
  protected void addMenuItem(Object menuItem, Object parentObject)
  {
    // exit if no parent
    if (parentObject == null) return;

    // if the parent is a MenuBar and menu item is a Menu
    if (parentObject instanceof java.awt.MenuBar &&
        menuItem instanceof java.awt.Menu)
    {
      // add the item to the parent
      ((java.awt.MenuBar)parentObject).add((java.awt.Menu)menuItem);
    }
    // if the parent is a Menu and menu item is a MenuItem
    else if (parentObject instanceof java.awt.Menu &&
             menuItem instanceof java.awt.MenuItem)
    {
      // add the item to the parent
      ((java.awt.Menu)parentObject).add((java.awt.MenuItem)menuItem);
    }
    // if the parent is a java.awt.Container and menu item is a component
    else if (parentObject instanceof java.awt.Container &&
             menuItem instanceof java.awt.Component)
    {
      // add the item to the parent
      ((java.awt.Container)parentObject).add((java.awt.Component)menuItem);
    }
    else
    {
      processErrorMessage(
          this.getClass() + ".addMenuItem - Do not know how to handle parent " +
          parentObject + "and menu item " + menuItem);
    }
  }

  /**
   * Appends a new separator to the end of the menu
   * @param parentObject the parent object for the element
   */
  protected void addMenuSeparator(Object parentObject)
  {
    // exit if no parent
    if (parentObject == null) return;

    // ensure the parent is a Menu
    if (parentObject instanceof java.awt.Menu)
    {
      // append a new separator to the end of the menu
      ((java.awt.Menu)parentObject).addSeparator();
    }
    // or the parent is a JMenu
    else if (parentObject instanceof javax.swing.JMenu)
    {
      // append a new separator to the end of the menu
      ((javax.swing.JMenu)parentObject).addSeparator();
    }
    else
    {
      processErrorMessage(
          this.getClass() + ".addMenuSeparator - " +
          "Do not know how to append a new separator for: " + parentObject);
    }
  }

  /**
   * Loads a menu item.
   * @param name the name of the menu item to add.
   * @param parentObject the parent object for the element
   * @param parentElement the parent element for the element.
   *
   * @return     the object for the menu item.
   */
  protected Object loadMenuItem(String name, Object parentObject,
                                Element parentElement)
  {
    // ensure all parameters are supplied
    if (name == null || parentObject == null || parentElement == null)
      return null;

    final Element element = super.findElement(IstiMenuTags.NAME, name);
    if (element != null) // if element was found
    {
      // process the element
      final Object object = processElement(element, parentObject,
          parentElement);
      if (object != null) // if valid object
        // process the children
        processChildren((Element)element, object);
      return object;
    }
    return null;
  }

  /**
   * Finds a menu item.
   * @param name the name of the item to be found
   *
   * @return     the object for the menu item.
   */
  public Object findMenuItem(String name)
  {
    return findMenuItem(getTopMenuBarObject(),name);
  }

  /**
   * Finds a menu item.
   * @param menuObject the menu object to start with
   * @param name the name of the item to be found
   *
   * @return     the object for the menu item.
   */
  public Object findMenuItem(Object menuObject, String name)
  {
    Object menuItem = null;

    if (menuObject instanceof MenuElement)
      menuItem = findMenuItem((MenuElement)menuObject, name);
    else if (menuObject instanceof java.awt.Component)
      menuItem = findMenuItem((java.awt.Component)menuObject, name);
    else if (menuObject instanceof java.awt.MenuComponent)
      menuItem = findMenuItem((java.awt.MenuComponent)menuObject, name);

    if (menuItem != null)  //if match found
      return menuItem;     //return the match

    if (menuObject instanceof java.awt.MenuBar)
      return findMenuItem((java.awt.MenuBar)menuObject, name);
    else if (menuObject instanceof java.awt.Menu)
      return findMenuItem((java.awt.Menu)menuObject, name);
    return menuItem;
  }

  /**
   * Updates the "visible" status of the given menu item.
   * @param name the name of the item to be found.
   * @param showItemFlag true for item visible; false for item hidden.
   *
   * @return     the object for the menu item or null if not found.
   */
  public Object updateMenuItemShown(
      String name, boolean showItemFlag)
  {
    return updateMenuItemShown(getTopMenuBarObject(),name,showItemFlag);
  }

  /**
   * Updates the "visible" status of the given menu item.
   * @param menuObject the menu object to start with.
   * @param name the name of the item to be found.
   * @param showItemFlag true for item visible; false for item hidden.
   *
   * @return     the object for the menu item or null if not found.
   */
  public Object updateMenuItemShown(
      Object menuObject, String name, boolean showItemFlag)
  {
    Object menuItem = findMenuItem(menuObject, name);
    if (menuItem instanceof java.awt.MenuItem)
    {
      ((java.awt.MenuItem)menuItem).setEnabled(showItemFlag);
    }
    else if (menuItem instanceof javax.swing.JMenuItem)
    {
      ((javax.swing.JMenuItem)menuItem).setVisible(showItemFlag);
      ((javax.swing.JMenuItem)menuItem).setEnabled(showItemFlag);
    }
    else if (menuItem instanceof javax.swing.AbstractButton)
    {
      ((javax.swing.AbstractButton)menuItem).setVisible(showItemFlag);
      ((javax.swing.AbstractButton)menuItem).setEnabled(showItemFlag);
    }
    else if (menuItem != null)
    {
      processErrorMessage(
          this.getClass() + ".setMenuItemShown - " +
          "Do not know how to set shown for: " +
          menuItem);
    }
    return menuItem;
  }

  /**
   * Finds a menu item.
   * @param menuObject the menu object to start with
   * @param name the name of the item to be found
   *
   * @return     the object for the menu item.
   */
  protected Object findMenuItem(java.awt.MenuComponent menuObject, String name)
  {
    // check for match
    if (name.equalsIgnoreCase(menuObject.getName()))
      return menuObject;

    return null;
  }

  /**
   * Finds a menu item.
   * @param menuObject the menu object to start with
   * @param name the name of the item to be found
   *
   * @return     the object for the menu item.
   */
  protected Object findMenuItem(java.awt.Component menuObject, String name)
  {
    // check for match
    if (name.equalsIgnoreCase(menuObject.getName()))
      return menuObject;

    return null;
  }

  /**
   * Finds a menu item.
   * @param menuObject the menu object to start with
   * @param name the name of the item to be found
   *
   * @return     the object for the menu item.
   */
  protected Object findMenuItem(MenuElement menuObject, String name)
  {
    // check for match
    if (name.equalsIgnoreCase(menuObject.getComponent().getName()))
      return menuObject;

    //check sub-elements
    Object menuItem = null;
    MenuElement[] menuElements = menuObject.getSubElements();

    // for each menu item on the menu
    for(int i = 0; i < menuElements.length; i++)
    {
      // no match now check components of this menu object
      if ((menuItem = findMenuItem(menuElements[i], name)) != null)
        break;
    }
    return menuItem;
  }

  /**
   * Finds a menu item.
   * @param menuObject the menu object to start with
   * @param name the name of the item to be found
   *
   * @return     the object for the menu item.
   */
  protected Object findMenuItem(java.awt.MenuBar menuObject, String name)
  {
    Object menuItem = null;

    // for each menu on the menu bar
    for(int i = 0; i < menuObject.getMenuCount(); i++)
    {
      // check for match
      final java.awt.Menu menu = menuObject.getMenu(i);
      if (name.equalsIgnoreCase(menu.getName()))
        return menu;

      // no match now check components of this menu object
      if ((menuItem = findMenuItem(menu, name)) != null)
        break;
    }
    return menuItem;
  }

  /**
   * Finds a menu item.
   * @param menuObject the menu object to start with
   * @param name the name of the item to be found
   *
   * @return     the object for the menu item.
   */
  protected Object findMenuItem(java.awt.Menu menuObject, String name)
  {
    Object menuItem = null;

    // for each menu item on the menu
    for(int i = 0; i < menuObject.getItemCount(); i++)
    {
      // check for match
      final java.awt.MenuItem item = menuObject.getItem(i);
      if (name.equalsIgnoreCase(item.getName()))
        return item;

      // no match now check components of this menu object
      if ((menuItem = findMenuItem(item, name)) != null)
        break;
    }
    return menuItem;
  }

  /**
   * Menu bar menu item.
   */
  protected static class MenuBarMenuItem extends HandJButton
  {
    /**
     * Creates a menu bar menu item.
     */
    public MenuBarMenuItem()
    {
      setBackground(null);
      setBorderPainted(false);
      setFocusPainted(false);
      setContentAreaFilled(false);          //don't paint button pixels
      setOpaque(false);                     //let button be transparent
      setMargin(new Insets(0,0,0,0));       //set new insets for margins
    }

    /**
     * Identifies whether or not this component can receive the focus.
     * @return false to indicate that this component cannot receive
     * the focus.
     */
    public boolean isFocusable()
    {
      return false;
    }
  }

  /**
   * Space menu item.
   */
  protected static class SpaceMenuItem extends JButton
  {
   /**
    * Creates a space menu item.
    */
    public SpaceMenuItem()
    {
      super("      ");
      setEnabled(false);
      setBorderPainted(false);              //don't paint border
      setFocusPainted(false);               //disable focus effect
      setContentAreaFilled(false);          //don't paint button pixels
      setOpaque(false);                     //let button be transparent
      setMargin(new Insets(0,0,0,0));       //set new insets for margins
      setRolloverEnabled(false);            //disable mouse-over effect
      setRequestFocusEnabled(false);        //don't allow focus
    }

    /**
     * Identifies whether or not this component can receive the focus.
     * @return false to indicate that this component cannot receive
     * the focus.
     */
    public boolean isFocusable()
    {
      return false;
    }
  }
}
