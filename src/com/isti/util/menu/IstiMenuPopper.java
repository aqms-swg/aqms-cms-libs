//IstiMenuPopper.java:  Implements the interface between a Component and
// a PopupMenu.
//
//   8/29/2002 -- [KF]
//

package com.isti.util.menu;

import java.awt.event.*;

/**
 * IstiMenuPopper stores a reference to a Component and a PopupMenu.
 * IstiMenuPopper registers itself to listen for MouseEvents
 * on the Component, and when a right-mouse-click occurs, displays the
 * associated popup menu.
 */
public class IstiMenuPopper implements MouseListener
{
  /**
   * True when the object is enabled. An object that is not
   * enabled does not interact with the user.
   *
   * @serial
   * @see #isEnabled
   * @see #setEnabled
   */
  boolean enabled = true;

  private Object popupMenu;

  /**
   * Creates the interface between a Component and a PopupMenu.
   * @param popupMenu the popup menu that will be displayed.
   * @return the menu popper or null if error.
   * @see add
   */
  public static IstiMenuPopper createMenuPopper(java.awt.PopupMenu popupMenu)
  {
    return createMenuPopper ((Object)popupMenu, null);
  }

  /**
   * Creates the interface between a Component and a PopupMenu.
   * @param popupMenu the popup menu that will be displayed.
   * @param component the component that will invoke the popup menu.
   * @return the menu popper or null if error.
   * @see add
   */
  public static IstiMenuPopper createMenuPopper(
      java.awt.PopupMenu popupMenu,
      java.awt.Component component)
  {
    return createMenuPopper((Object)popupMenu, component);
  }

  /**
   * Creates the interface between a JComponent and a JPopupMenu.
   * @param popupMenu the popup menu that will be displayed.
   * @return the menu popper or null if error.
   * @see add
   */
  public static IstiMenuPopper createMenuPopper(javax.swing.JPopupMenu popupMenu)
  {
    return createMenuPopper((Object)popupMenu, null);
  }

  /**
   * Creates the interface between a JComponent and a JPopupMenu.
   * @param popupMenu the popup menu that will be displayed.
   * @param component the component that will invoke the popup menu.
   * @return the menu popper or null if error.
   * @see add
   */
  public static IstiMenuPopper createMenuPopper(
      javax.swing.JPopupMenu popupMenu,
      javax.swing.JComponent component)
  {
    return createMenuPopper((Object)popupMenu, component);
  }

  /**
   * Creates the interface between a Component and a PopupMenu.
   * @param popupMenu the popup menu that will be displayed.
   * @param component the component that will invoke the popup menu.
   * @return the menu popper or null if error.
   * @see add
   */
  protected static IstiMenuPopper createMenuPopper(
      Object popupMenu,
      java.awt.Component component)
  {
    if (popupMenu == null)
      return null;
    return new IstiMenuPopper(popupMenu).add(component);
  }

  /**
   * Adds the component to the menu popper.
   * IstiMenuPopper registers itself to listen for MouseEvents
   * on the Component, and when a right-mouse-click occurs, displays the
   * associated popup menu.
   * @param component the component that will invoke the popup menu.
   * @return the menu popper.
   */
  public IstiMenuPopper add(java.awt.Component component)
  {
    if (component == null)
      return this;

    component.addMouseListener(this);
    // add the popup menu to the component
    if (popupMenu instanceof java.awt.PopupMenu)
      component.add((java.awt.PopupMenu)popupMenu);
    else if (component instanceof javax.swing.JComponent &&
             popupMenu instanceof javax.swing.JPopupMenu)
      ((javax.swing.JComponent)component).add((javax.swing.JPopupMenu)popupMenu);
    return this;
  }

  /**
   * Creates the interface between a Component and a PopupMenu.
   * @param popupMenu the popup menu that will be displayed.
   * @see createMenuPopper
   */
  public IstiMenuPopper(Object popupMenu)
  {
    this.popupMenu = popupMenu;
  }

  /**
   * Determines whether this popup menu is enabled.
   * @see #setEnabled
   * @return true if the popup menu is enabled.
   */
  public boolean isEnabled()
  {
    return enabled;
  }

  /**
   * Sets whether or not this popup menu is enabled.
   * @param enabled true if the popup menu is enabled.
   */
  public void setEnabled(boolean enabled)
  {
    this.enabled = enabled;
  }

  /**
   * Invoked when the mouse has been clicked on a component.
   * @param e the mouse event
   */
  public void mouseClicked(MouseEvent e) { processMouseEvent(e); }

  /**
   * Invoked when a mouse button has been pressed on a component.
   * @param e the mouse event
   */
  public void mousePressed(MouseEvent e) { processMouseEvent(e); }

  /**
   * Invoked when a mouse button has been released on a component.
   * @param e the mouse event
   */
  public void mouseReleased(MouseEvent e) { processMouseEvent(e); }

  /**
   * Invoked when the mouse enters a component.
   * @param e the mouse event
   */
  public void mouseEntered(MouseEvent e) { processMouseEvent(e); }

  /**
   * Invoked when the mouse exits a component.
   * @param e the mouse event
   */
  public void mouseExited(MouseEvent e) { processMouseEvent(e); }

  /**
   * Displays up a popup menu when right mouse click occurs.
   * @param e the mouse event
   */
  protected void processMouseEvent(MouseEvent e)
  {
    //do not process if not enabled
    if (!enabled)
      return;

    if (e.isPopupTrigger())  // if popup-menu trigger event
    {
      java.awt.Component component = e.getComponent();
      // show the popup menu
      if (popupMenu instanceof java.awt.PopupMenu)
        ((java.awt.PopupMenu)popupMenu).show(component, e.getX(), e.getY());
      else if (popupMenu instanceof javax.swing.JPopupMenu)
        ((javax.swing.JPopupMenu)popupMenu).show(component, e.getX(), e.getY());
    }
  }
}