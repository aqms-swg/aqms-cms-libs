//IstiMenuTags.java:  Holds static Strings containing Menu Tag names.
//              tag names.
//
//  8/26/2002 -- [KF]
//

package com.isti.util.menu;

/**
 * Class IstiMenuTags holds static Strings containing Menu Tag names.
 */
public class IstiMenuTags
{
  /**
   * Tag names for the elements.
   */

  // The one and only ROOT element must be at the top.
  public static final String ROOT = "Menus";

  // MENU elements can be added to the MENU_BAR, POPUP_MENU, or another MENU
  public static final String MENU = "Menu";

  // MENU_BAR elements can be added to the ROOT
  public static final String MENU_BAR = "MenuBar";

  // POPUP_MENU elements can be added to the ROOT
  public static final String POPUP_MENU = "PopupMenu";

  // MENU_ITEM elements can be added to the POPUP_MENU or MENU
  public static final String MENU_ITEM = "MenuItem";

  // CHECKBOX_MENU_ITEM elements can be added to the POPUP_MENU or MENU
  public static final String CHECKBOX_MENU_ITEM = "CheckboxMenuItem";

  // MENU_LOAD loads the specified element and all of it's children.
  public static final String MENU_LOAD = "MenuLoad";

  // MENU_SEPARATOR adds a menu separator to the MENU
  public static final String MENU_SEPARATOR = "MenuSeparator";


  /**
   * Tag names for the attributes.
   */

  /**
   * Tag name for LOAD_NAME attribute on MENU_LOAD elements.
   * This is the name of the menu item element to load.
   * The element and all of it's children are loaded.
   */
  public static final String LOAD_NAME = "LoadName";

  /**
   * Tag name for CLASS attribute on all menu item elements.
   * This is the class instantiated for the menu item.
   */
  public static final String CLASS = "Class";

  /**
   * Tag name for NAME attribute on all menu item elements.
   * This is the name of the class instantiated for the menu item.
   *
   * NOTE: This tag must be present and unique.
   */
  public static final String NAME = "Name";

  /**
   * Tag name for DISPLAY_TEXT attribute on MENU, MENU_ITEM, and
   * CHECKBOX_MENU_ITEM elements.
   * This is the display text for the menu item.
   */
  public static final String DISPLAY_TEXT = "DisplayText";

  /**
   * Tag name for MNEMONIC attribute on MENU_ITEM and
   * CHECKBOX_MENU_ITEM elements.
   * This is the keyboard mnemonic for the menu item.
   *
   * NOTE: This is not supported by all menu item classes.
   */
  public static final String MNEMONIC = "Mnemonic";

  /**
   * Tag name for ACCELERATOR attribute on MENU_ITEM and
   * CHECKBOX_MENU_ITEM elements.
   * This is the accelerator key (menu shortcut) for the menu item.
   * <pre>
   * The string has the following syntax:
   * [shift] key
   *  or
   * [shift | control | meta | alt | button1 | button2 | button3 | released] key
   * where key is the KeyEvent keycode name, i.e. the name following "VK_".
   * </pre>
   * NOTE: Some platforms only support a platform specific key (such as control)
   * which is always set and shift (such as with java.awt)
   */
  public static final String ACCELERATOR = "Accelerator";

  /**
   * Tag name for HANDLER attribute on all menu item elements
   * other than MENU_BAR and POPUP_MENU.
   */
  public static final String HANDLER = "Handler";

  /**
   * Tag name for SELECTED attribute on CHECKBOX_MENU_ITEM elements.
   * This indicates if the initial state of the menu item should be selected.
   */
  public static final String SELECTED = "Selected";
}