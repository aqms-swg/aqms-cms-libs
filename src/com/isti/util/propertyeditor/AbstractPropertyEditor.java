//AbstractPropertyEditor.java:  Abstract Property Editor.
//
//  4/16/2003 -- [KF]
//

package com.isti.util.propertyeditor;

import java.beans.PropertyEditorSupport;
import java.awt.event.ActionListener;

/**
 * An Abstract Property Editor.
 */
public abstract class AbstractPropertyEditor extends PropertyEditorSupport
{
  /**
   * Adds an <code>ActionListener</code> to the button.
   * @param l the <code>ActionListener</code> to be added
   */
  public abstract void addActionListener(ActionListener l);

  /**
   * Removes an <code>ActionListener</code> from the button.
   * If the listener is the currently set <code>Action</code>
   * for the button, then the <code>Action</code>
   * is set to <code>null</code>.
   *
   * @param l the listener to be removed
   */
  public abstract void removeActionListener(ActionListener l);
}
