//BasePropertyInspector.java:  Defines a GUI control for editing properties.
//
// 11/23/2002 -- [KF]
//  2/26/2003 -- [ET]  Renamed 'saveConfingSettings()' method to
//                     'commitConfingSettings()' and changed its
//                     return type to 'void'; added output to 'stderr'
//                     to several exception 'catch' blocks.
//  3/11/2003 -- [ET]  Added "all" to tooltips for "Reset" and "Defaults"
//                     buttons.
//   4/4/2003 -- [KF]  Changed the "Reset" and "Defaults" buttons to change
//                     only the current tab's settings,
//                     check for 'JToggleButton' instead of 'JCheckBox'.
//  4/28/2003 -- [ET]  Added parentheses to 'Defaults' button tooltip.
//  4/29/2003 -- [KF]  Added 'getOkButton', 'getCancelButton',
//                     'getCancelButton' and 'getDefaultsButton' methods.
// 10/10/2003 -- [KF]  Added 'loadCurrentConfigSettings' and
//                     'createPanelActionListeners' methods,
//                     do not create scroll pane for tabs without a title
//                     if a scroll pane has already been created.
//   1/8/2004 -- [KF]  Added name parameter to the 'createPropertyEditor'
//                     method.
//   2/5/2004 -- [ET]  Added support for optional "Help" button; added
//                     'getCurrentTabName()' method.
//  4/16/2004 -- [ET]  Added methods 'setSelectedTabIndex()' and
//                     'setSelectedTabName()'.
//  5/21/2004 -- [KF]  Added 'createPropertyComponentPanel' method;
//                     removed old commented out code.
//  7/20/2004 -- [ET]  Added optional 'waitFlag' parameter to 'showDialog()'
//                     method.
//  1/11/2006 -- [KF]  Moved 'PropertyComponentPanel' to new separate class,
//                     Added 'getEditorValue()' method.
//   2/1/2006 -- [KF]  Added 'addTab()' method.
//  3/23/2006 -- [ET]  Added 'setInitialFocusOnFirstCompFlag()' method and
//                     implementation.
//  3/30/2006 -- [ET]  Modified property-change methods to synchronize on
//                     'propChangeListenersSyncObj' instead of 'this';
//                     added 'setDialogCloseListenerObj()' method and
//                     implementation.
//   4/7/2006 -- [ET]  Added 'closeDialog()' method.
//   5/4/2006 -- [ET]  Modified 'closeDialog()' method to only do commit
//                     if dialog is showing.
//  5/31/2006 -- [ET]  Added methods 'getSpecifiedComponentPanel()' and
//                     'addComponentToTab()'.
//  7/12/2006 -- [ET]  Took out duplicate 'PropertyChangeListener' support
//                     because methods can be called while parent
//                     'JComponent' is being constructed (and before
//                     instance variables like 'propChangeListenersSyncObj'
//                     are initialized); added "CHANGE_ALL_PROPSTR" string
//                     and used it in call to 'firePropertyChange()'
//                     in 'commitConfigSettings()' method.
// 10/27/2008 -- [KF]  Fixed 'getEditorValue()' method to avoid null exception,
//                     modified the preferred size to always have scrollbars.
//

package com.isti.util.propertyeditor;

import java.util.*;
import java.beans.PropertyEditor;
import java.awt.*;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import javax.swing.*;
import com.isti.util.FifoHashtable;
import com.isti.util.UtilFns;
import com.isti.util.gui.IstiDialogPopup;
import com.isti.util.gui.ViewHTMLPanel;

/**
 * Class to inspect Properties.
 */
public class BasePropertyInspector extends JPanel
{
  private final JTabbedPane tabbedPaneObj = new JTabbedPane();
  private JScrollPane scrollPane = null;
//  private Vector listeners = null;                    //change listeners
//                                            //thread-sync obj for listeners:
//  private final Object propChangeListenersSyncObj = new Object();
  private IstiDialogPopup dialogObj = null;           //dialog object
                                            //dialog-close listener:
  private IstiDialogPopup.CloseListener dialogCloseListenerObj = null;
                                                      //lower button panel:
  protected final ConfigLowerButtonPanel lowerButtonPanelObj =
                                               new ConfigLowerButtonPanel();
  private Map currentProps = null;                  //current properties
  private Map defaultProps = null;                  //default properties
  //Hashtable containing property keys and their editors.
  private Hashtable editorsTable = new Hashtable();
  private Hashtable allEditors = null;
  private final Map tabTable = new FifoHashtable();  //tab table
  private boolean initialFocusOnFirstCompFlag = false;
  private Component firstFocusComponentObj = null;

  //tab title for items without a table title
  // or use null to skip them over
  private String defaultTabTitle = "Other";
  //save values as text if true
  protected boolean saveAsTextFlag = true;
  //center flag, set to true to center the dialog over the parent
  protected boolean centerFlag = false;

  /** Change all property string for call to 'firePropertyChange()'. */
  public static final String CHANGE_ALL_PROPSTR = "ChangeAll";

  /** The initial horizontal scrollbar policy. */
  protected int initialHsbPolicy =
      ScrollPaneConstants.HORIZONTAL_SCROLLBAR_ALWAYS;
  /** The initial vertical scrollbar policy. */
  protected int initialVsbPolicy =
      ScrollPaneConstants.VERTICAL_SCROLLBAR_ALWAYS;
  /** The normal horizontal scrollbar policy. */
  protected int normalHsbPolicy =
      ScrollPaneConstants.HORIZONTAL_SCROLLBAR_AS_NEEDED;
  /** The normal vertical scrollbar policy. */
  protected int normalVsbPolicy =
      ScrollPaneConstants.VERTICAL_SCROLLBAR_AS_NEEDED;

  /**
   * Creates a JComponent with the properties to be changed.  This
   * component is suitable for inclusion into a GUI.
   *
   * NOTE: The 'addProperties' method must be called before the inspector
   * can be used.
   * @see #addProperties
   */
  protected BasePropertyInspector()
  {
    setLayout(new BorderLayout());

    //add the tab pane to the panel
    this.add(tabbedPaneObj, BorderLayout.CENTER);

    //add the button panel to the bottom
    add(lowerButtonPanelObj, BorderLayout.SOUTH);
  }

  /**
   * Creates a JComponent with the properties to be changed.  This
   * component is suitable for inclusion into a GUI.
   * @param props Properties to be inspected, must not be null.
   */
  public BasePropertyInspector(Map props)
  {
    this();

    //add the property keys
    addProperties(props);
  }

  /**
   * Gets the properties.
   * @return Map of the properties.
   */
  public Map getProperties()
  {
    return currentProps;
  }

  /**
   * Gets the ordered property keys fo the GUI.
   * @return collecion of property keys or null for default property key order.
   */
  protected Collection getPropertyKeys()
  {
    return null;  //use default
  }

  /**
   * Adds the properties to the GUI.
   * @param props Properties to be inspected, must not be null.
   */
  protected final void addProperties(Map props)
  {
    currentProps = props;

    Collection propertyKeys = getPropertyKeys();
    if (propertyKeys == null)  //if no property key collection provided
    {
      //create default property key collection
      propertyKeys = new TreeSet(props.keySet());
    }

    //create the all editors table
    allEditors = new Hashtable(propertyKeys.size());

    //get editor from the table
    Hashtable editors;
    String key = null;
    String tabTitle;
    PropertyComponentPanel cfgPropCompPanel = null;
    Object tableObj;
    Iterator it = propertyKeys.iterator();

    while (it.hasNext())  // iterate properties
    {
      try
      {
        key = (String)it.next();
        tabTitle = getTabTitle(key);

        //if the tab title doesn't exist
        if (tabTitle == null)
        {
          tabTitle = "";
        }

        tableObj = tabTable.get(tabTitle);
        if (tableObj instanceof PropertyComponentPanel)
        {
          cfgPropCompPanel = (PropertyComponentPanel)tableObj;
          editors = (Hashtable)editorsTable.get(cfgPropCompPanel);
        }
        else
        {
          cfgPropCompPanel = createPropertyComponentPanel();
          tabTable.put(tabTitle, cfgPropCompPanel);
          editors = new Hashtable();
          editorsTable.put(cfgPropCompPanel, editors);
        }

        //create the property label
        final Component propertyLabel = createPropertyLabel(key);

        //create the property editor
        final PropertyEditor editor = createPropertyEditor(key);

        //if the property editor was created
        if (editor != null)
        {
          //get the custom editor
          final Component comp = editor.getCustomEditor();
          //get the tool tip
          final String toolTipText = getToolTipText(key);
          //if the tool tip exists
          if (toolTipText != null && toolTipText.length() > 0)
          {
            //set the tool tip for the label
            setToolTipText(propertyLabel, toolTipText);
            //set the tool tip for this component
            setToolTipText(comp, toolTipText);
          }

          //add the property to the panel
          cfgPropCompPanel.addProperty(propertyLabel, comp);

          //save the property editor
          allEditors.put(key, editor);
          editors.put(key, editor);
        }
      }
      catch (Exception ex)        //skip item if error
      {  //some kind of exception error occurred; show message
        System.err.println("BasePropertyInspector.addProperties():  " +
                      "Error adding setting (key=\"" + key + "\"):  " + ex);
        ex.printStackTrace();          //show stack trace
      }
    }

    //for each entry in the tab table
    it = tabTable.keySet().iterator();
    while (it.hasNext())
    {
      //add the tab to the tab pane
      tabTitle = (String)it.next();
      //if tab title exists or scroll pane does not exist
      if (tabTitle.length() > 0 || scrollPane == null)
      {
        cfgPropCompPanel =
            (PropertyComponentPanel)tabTable.get(tabTitle);
        addTab(tabTitle,cfgPropCompPanel);  //add the tab
              //setup component to receive focus when dialog
              // shown (if 'initialFocusOnFirstCompFlag'==true):
        if(firstFocusComponentObj == null && tabTitle.length() > 0)
        {     //component not yet setup and tab was added to pane
          firstFocusComponentObj =     //get "focus" component
                               cfgPropCompPanel.getFirstFocusComponentObj();
        }
      }
    }

    //if only 1 tab and scroll pane exists
    if (tabbedPaneObj.getTabCount() <= 1 && scrollPane != null)
    {
      //add the scroll pane to the panel
      this.add(scrollPane, BorderLayout.CENTER);
      if(firstFocusComponentObj == null && cfgPropCompPanel != null)
      {  //component not yet setup and component panel is OK
        firstFocusComponentObj =       //get "focus" component
                               cfgPropCompPanel.getFirstFocusComponentObj();
      }
    }
  }

  /**
   * Adds the tab with the specified title.
   * @param tabTitle the title.
   * @param tabPanel the panel.
   */
  public void addTab(String tabTitle,JPanel tabPanel)
  {
    // create the palette's scroll pane
    scrollPane = new JScrollPane(tabPanel, normalVsbPolicy, normalHsbPolicy);
    scrollPane.setAlignmentX(Component.LEFT_ALIGNMENT);
    scrollPane.setAlignmentY(Component.TOP_ALIGNMENT);
    if (tabTitle.length() > 0)
      tabbedPaneObj.addTab(tabTitle, scrollPane);
  }

  /**
   * Sets the default properties to the GUI.
   * @param props default properties.
   */
  public void setDefaultProperties(Map props)
  {
    if (props != null && props.size() > 0)
      defaultProps = props;
    lowerButtonPanelObj.defaultsButton.setEnabled(defaultProps != null);
  }

  /**
   * Adds an <code>ActionListener</code> to the "Help" button.
   * @param listenerObj the <code>ActionListener</code> to be added.
   */
  public void addHelpActionListener(ActionListener listenerObj)
  {
    lowerButtonPanelObj.addHelpListener(listenerObj);
  }

  /**
   * Adds a 'ViewHTMLPanel' to be shown in response to the "Help" button.
   * @param panelObj the 'ViewHTMLPanel' to use.
   * @param useTabRefsFlag true to set the name of current tab as an
   * anchor-reference into the view panel, false to simply display the
   * panel.
   * @param parentComponent the parent component for the panel.
   * @param titleStr the title string for the panel.
   * @param modalFlag true for a modal dialog, false for a modeless dialog
   * (that allows other windows to be active at the same time).
   */
  public void addHelpActionViewHTMLPanel(final ViewHTMLPanel panelObj,
               final boolean useTabRefsFlag,final Component parentComponent,
                              final String titleStr,final boolean modalFlag)
  {
    lowerButtonPanelObj.addHelpListener(new ActionListener()
        {
          public void actionPerformed(ActionEvent evtObj)
          {
            if(useTabRefsFlag)
            { //use name of current tab as anchor-reference into document
              final String tabStr;
              if((tabStr=getCurrentTabName()) != null)
                panelObj.setPageRef(tabStr);
            }
            panelObj.showInDialog(parentComponent,titleStr,modalFlag);
          }
        });
  }

  /**
   * Sets whether or not the focus will attempt to be placed on the
   * first available text-field, button, or similar component when
   * the inspector dialog is shown.  If this method is not called then
   * it will default to 'false' (focus not placed).
   * @param flgVal true to attempt to place the focus on the
   * first available text-field, button, or similar component when
   * the inspector dialog is shown.
   */
  public void setInitialFocusOnFirstCompFlag(boolean flgVal)
  {
    initialFocusOnFirstCompFlag = flgVal;
  }

  /**
   * Gets the current value for the key.
   * @param key the property key.
   * @return default value.
   */
  protected Object getCurrentValue(String key)
  {
    if (currentProps != null)
      return currentProps.get(key);
    return null;
  }

  /**
   * Gets the default value for the key.
   * @param key the property key.
   * @return default value.
   */
  protected Object getDefaultValue(String key)
  {
    if (defaultProps != null)
      return defaultProps.get(key);
    return null;
  }

  /**
   * Gets the table title for the key.
   * @param key the property key.
   * @return the tab title or null if key should not be on the inspector.
   */
  protected String getTabTitle(String key)
  {
    return defaultTabTitle;
  }

  /**
   * Creates a property label for the property item.
   * @param key the property key.
   * @return property label.
   */
  protected Component createPropertyLabel(String key)
  {
    return PropertyEditorFactory.createPropertyLabel(key);
  }

  /**
   * Creates a property component panel.
   * @return the property component panel.
   */
  protected PropertyComponentPanel createPropertyComponentPanel()
  {
    return new PropertyComponentPanel();
  }

  /**
   * Creates a property editor for the property item.
   * @param key the property key.
   * @return property editor.
   */
  protected PropertyEditor createPropertyEditor(String key)
  {
    final Object valueObj = currentProps.get(key);
    return PropertyEditorFactory.createPropertyEditor(key, valueObj);
  }

  /**
   * Returns the component panel for the specified tab, or the component
   * panel for the currently-selected tab if none specified.
   * @param tabNameStr name of tab whose component panel should be
   * returned; or null for the selected-tab component.
   * @return The specified component panel, the selected-tab component
   * panel, or null if no component could be found.
   */
  public PropertyComponentPanel getSpecifiedComponentPanel(
                                                          String tabNameStr)
  {
    Component compObj;
    if(tabNameStr != null)
    {    //tab name given
      final int idx;
      compObj = ((idx=tabbedPaneObj.indexOfTab(tabNameStr)) >= 0) ?
                                   tabbedPaneObj.getComponentAt(idx) : null;
    }
    else      //tab name not given; get component for selected tab
      compObj = tabbedPaneObj.getSelectedComponent();
    if(compObj instanceof JScrollPane)
    {    //scroll-pane component found OK; get content panel
      compObj = ((JScrollPane)compObj).getViewport().getView();
              //if content panel fetched OK then return it:
      if(compObj instanceof PropertyComponentPanel)
        return (PropertyComponentPanel)compObj;
    }
    return null;             //panel was not found
  }

  /**
   * Returns the component panel for the currently-selected tab.
   * @return The component panel for the currently-selected tab, or null
   * if no component could be found.
   */
  public PropertyComponentPanel getSelectedComponentPanel()
  {
    return getSpecifiedComponentPanel(null);
  }

  /**
   * Returns the editor for the selected component panel.
   * @return the editor for the selected component panel, or null if none.
   */
  protected Hashtable getSelectedComponentPanelEditors()
  {
    return getSelectedComponentPanelEditors(getSelectedComponentPanel());
  }

  /**
   * Returns the editor for the selected component panel.
   * @param key propert component panel
   * @return the editor for the selected component panel, or null if none.
   */
  protected Hashtable getSelectedComponentPanelEditors(PropertyComponentPanel key)
  {
    if (key != null)  //if the component panel was specified
    {
      Object editors = editorsTable.get(key);  //get the editors for the panel
      if (editors instanceof Hashtable)  //if found
        return (Hashtable)editors;  //return the editors
    }
    return null;  //no editors
  }

  /**
   * Loads all of the current configuration settings and
   * updates the configuration property components.
   */
  public void loadCurrentConfigSettings()
  {
    loadCurrentConfigSettings(true);
  }

  /**
   * Loads the current configuration settings and
   * updates the configuration property components.
   * @param loadAllFlag true to load all tabs
   */
  public void loadCurrentConfigSettings(boolean loadAllFlag)
  {
    String key = null;
    Object value = null;
    PropertyEditor editor;
    final Enumeration e;
    final Hashtable editors;

    //if loading all panels or if there is only 1 tab
    if (loadAllFlag || tabbedPaneObj.getTabCount() <= 1)
    {
      editors = allEditors;  //use all editors
    }
    else
    {
      //get editors for the selected panel
      editors = getSelectedComponentPanelEditors();
    }

    if (editors == null)  //exit if no editors found
      return;

    e = editors.keys();

    while (e.hasMoreElements())
    {
      try
      {
        key = (String)e.nextElement();
        value = getCurrentValue(key);
        editor = (PropertyEditor)allEditors.get(key);
        if (value != null)  //if value was found
          //update the value on the editor
          editor.setValue(value);
      }
      catch (ClassCastException ex)
      {  //exception error occurred; show message
        System.err.println(
                      "BasePropertyInspector.loadCurrentConfigSettings():" +
                                   "  Error loading setting (key=\"" + key +
                                    "\", value=\"" + value + "\"):  " + ex);
        ex.printStackTrace();          //show stack trace
      }
    }
  }

  /**
   * Loads the default configuration settings and
   * updates the configuration property components.
   * @param loadAllFlag true to load all tabs.
   */
  public void loadDefaultConfigSettings(boolean loadAllFlag)
  {
    final Hashtable editors;
    String key = null;
    Object value = null;
    PropertyEditor editor;
    final Enumeration e;

    //if loading all panels or if there is only 1 tab
    if (loadAllFlag || tabbedPaneObj.getTabCount() <= 1)
    {
      editors = allEditors;  //use all editors
    }
    else
    {
      //get editors for the selected panel
      editors = getSelectedComponentPanelEditors();
    }

    if (editors == null)  //exit if no editors found
      return;

    e = editors.keys();

    while (e.hasMoreElements())
    {
      try
      {
        key = (String)e.nextElement();
        value = getDefaultValue(key);
        editor = (PropertyEditor)allEditors.get(key);
        if (value != null)  //if value was found
          //update the value on the editor
          editor.setValue(value);
      }
      catch (ClassCastException ex)
      {  //exception error occurred; show message
        System.err.println(
                      "BasePropertyInspector.loadDefaultConfigSettings():" +
                                   "  Error loading setting (key=\"" + key +
                                    "\", value=\"" + value + "\"):  " + ex);
        ex.printStackTrace();          //show stack trace
      }
    }
  }

  /**
   * Gets the tool tip text for the property item.
   * @param key the property key.
   * @return the tool tip text or null if no tool tip text.
   */
  public String getToolTipText(String key)
  {
    return null;
  }

  /**
   * Sets the tool tip text for the specified component.
   * @param comp  the component.
   * @param text  the string to display; if the text is null,
   *              the tool tip is turned off for this component
   */
  protected void setToolTipText(Component comp, String text)
  {
    //if the component is a Swing panel
    if (comp instanceof JPanel)
    {
      //set the tool tip for all of it's components
      JPanel panel = (JPanel)comp;
      Component[] components = panel.getComponents();
      for (int i = 0; i < components.length; i++)
      {
        setToolTipText(components[i], text);
      }
    }
    //if the component is just a Swing component
    else if (comp instanceof JComponent)
      //set the tool tip
      ((JComponent)comp).setToolTipText(text);
  }

  /**
   * Saves the configuration settings.
   */
  protected void commitConfigSettings()
  {
    boolean propertyChangeFlag = false;

    String key = null;
    PropertyEditor editor;
    Object itemValue = null;
    Object editorValue;
    final Enumeration e = allEditors.keys();
    while (e.hasMoreElements())
    {
      try
      {
        key = (String)e.nextElement();
        editor = (PropertyEditor)allEditors.get(key);
        itemValue = currentProps.get(key);

        //if no editor value
        editorValue = getEditorValue(key,editor,itemValue,saveAsTextFlag);
        if (editorValue == null)
        {
            continue;  //skip this item
        }

        //save the editor value
        setValue(key, editorValue);
        propertyChangeFlag = true;  //will need to fire property change
      }
      catch (Exception ex)
      {  //some kind of exception error occurred; show message
        System.err.println("BasePropertyInspector.commitConfigSettings():" +
                                "  Error committing setting (key=\"" + key +
                                "\", value=\"" + itemValue + "\"):  " + ex);
        ex.printStackTrace();          //show stack trace
      }
    }
         //if change then notify listeners that "everything" has changed
    if (propertyChangeFlag)
      firePropertyChange(CHANGE_ALL_PROPSTR,null,null);
  }

  /**
   * Gets the text for a value. The text will not be null.
   * @param value the value object.
   * @return the text for a value.
   */
  public static String getAsText(Object value)
  {
    String valueText = null;
    if (value != null)
    {
      valueText = value.toString();
    }
    if (valueText == null)
    {
      valueText = UtilFns.EMPTY_STRING;
    }
    return valueText;
  }

  /**
   * Gets the editor value.
   * @param key the property key.
   * @param editor the property editor.
   * @param itemValue the current item value.
   * @param saveAsTextFlag save values as text if true.
   * @return the the editor value or null if no change.
   */
  public static Object getEditorValue(
      String key,PropertyEditor editor,Object itemValue,boolean saveAsTextFlag)
  {
    //if no editor value
    Object editorValue = null;
    if (!saveAsTextFlag) //if values don't have to be saved as text
      editorValue = editor.getValue();
    if (editorValue == null)
    {
      //editor may not support 'getValue' method,
      //use mandatory 'getAsText'
      editorValue = getAsText(editor.getAsText());
      //get text for the current item value
      itemValue = getAsText(itemValue);
    }
    //if there was no change
    if (editorValue.equals(itemValue))
    {
      return null; //no change
    }
    //save the editor value
    return editorValue;
  }

  /**
   * Set the value for the key.
   * @param key the property key.
   * @param value new property value.
   */
  public void setValue(String key, Object value)
  {
    currentProps.put(key, value);
  }

  /**
   * Creates panel action listeners for non-dialog use.
   */
  public void createPanelActionListeners()
  {
    //create and set the action listeners
    lowerButtonPanelObj.addOkListener(new ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        commitConfigSettings();           //commit changes
        setVisible(false);                //hide panel
      }
    });
    lowerButtonPanelObj.addCancelListener(new ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        setVisible(false);                //hide panel
      }
    });
    lowerButtonPanelObj.addResetListener(new ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        loadCurrentConfigSettings(false);  //reload current settings
      }
    });
    lowerButtonPanelObj.addDefaultListener(new ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        loadDefaultConfigSettings(false);  //reload default settings
      }
    });
  }

  /**
   * Creates a dialog for this Client Configuration Panel.
   * @param parent the <code>Component</code> to check for a
   *		<code>Frame</code> or <code>Dialog</code>
   * @param name the name of the menu item.
   * @param displayText the display text for the menu item.
   * @param modalFlag true for a modal dialog, false for a modeless dialog
   * (that allows other windows to be active at the same time).
   */
  public void createDialog(final Component parent, String name,
                          final String displayText, final boolean modalFlag)
  {
    //exit if the dialog was already created
    if (dialogObj != null)
      return;

         // (use menu display text for dialog title)
    dialogObj = new IstiDialogPopup(parent, this, displayText,
                                                           null, modalFlag);
    if (!centerFlag)  //if not centered over parent
    {
      try
      {
        final Frame frame;
        if ((frame=JOptionPane.getFrameForComponent(parent)) != null)
        {
          //put the dialog to the right of the parent
          Dimension parentSize = frame.getSize();
          dialogObj.setLocation(7+parentSize.width, 2);
        }
      }
      catch(Exception ex)
      {
      }
    }
         //add listener to pass-along dialog-closed event to listener
         // setup via 'setDialogCloseListenerObj()' method:
    dialogObj.addCloseListener(new IstiDialogPopup.CloseListener()
        {
          public void dialogClosed(Object evtObj)
          {             //set local copy of 'dialogCloseListenerObj'
                        // to avoid any possible threading issues:
            final IstiDialogPopup.CloseListener listenerObj =
                                                     dialogCloseListenerObj;
            if(listenerObj != null)    //if listener setup then invoke
              listenerObj.dialogClosed(evtObj);
          }
        });

    //create and set the action listeners
    lowerButtonPanelObj.addOkListener(new ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        commitConfigSettings();           //commit changes
        dialogObj.setVisible(false);      //hide dialog
      }
    });
    lowerButtonPanelObj.addCancelListener(new ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        dialogObj.setVisible(false);      //hide dialog
      }
    });
    lowerButtonPanelObj.addResetListener(new ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        loadCurrentConfigSettings(false);  //reload current settings
      }
    });
    lowerButtonPanelObj.addDefaultListener(new ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        loadDefaultConfigSettings(false);  //reload default settings
      }
    });
         //if flag set and component OK then setup to place
         // focus on first suitable component when dialog shown:
    if(initialFocusOnFirstCompFlag && firstFocusComponentObj != null)
      dialogObj.setInitialFocusComponent(firstFocusComponentObj);
  }

  /**
   * Shows the dialog with the Client Configuration Panel
   * after the configuration settings are loaded.
   * Note that the dialog needs to be created with the createDialog method.
   * @param waitFlag true to make this method block until the dialog is
   * closed, even if the dialog is modeless (if the dialog is modal then
   * this flag has no effect).
   * @see #createDialog()
   */
  public void showDialog(boolean waitFlag)
  {
    loadCurrentConfigSettings(true);  //load the configuration settings
    if(dialogObj != null)
    {
      //set the scrollbar policies to initial
      if (scrollPane != null)
      {
        if (initialVsbPolicy != normalVsbPolicy)
          scrollPane.setVerticalScrollBarPolicy(initialVsbPolicy);
        if (initialHsbPolicy != normalHsbPolicy)
          scrollPane.setHorizontalScrollBarPolicy(initialHsbPolicy);
      }
      dialogObj.pack();
      if (scrollPane != null)
      {
        //set the scrollbar policies to normal
        if (initialVsbPolicy != normalVsbPolicy)
          scrollPane.setVerticalScrollBarPolicy(normalVsbPolicy);
        if (initialHsbPolicy != normalHsbPolicy)
          scrollPane.setHorizontalScrollBarPolicy(normalHsbPolicy);
      }
      if(waitFlag)
        dialogObj.showAndWait();
      else
        dialogObj.show();
    }
  }

  /**
   * Shows the dialog with the Client Configuration Panel
   * after the configuration settings are loaded.
   * Note that the dialog needs to be created with the createDialog method.
   * @see #createDialog()
   */
  public void showDialog()
  {
    showDialog(false);
  }

  /**
   * Shows a dialog with a Client Configuration Panel
   * after creating it if necessary.
   * @param parent the <code>Component</code> to check for a
   *		<code>Frame</code> or <code>Dialog</code>
   * @param name the name of the menu item.
   * @param displayText the display text for the menu item.
   * @param modalFlag true for a modal dialog, false for a modeless dialog
   * (that allows other windows to be active at the same time).
   * @param waitFlag true to make this method block until the dialog is
   * closed, even if the dialog is modeless (if the dialog is modal then
   * this flag has no effect).
   */
  public void showDialog(Component parent, String name, String displayText,
                                        boolean modalFlag, boolean waitFlag)
  {
    //create the dialog if necessary
    if (dialogObj == null)
      createDialog(parent, name, displayText, modalFlag);
    //show the dialog (if not already showing)
    if(!dialogObj.isVisible())
      showDialog(waitFlag);
    else                          //if already showing then
      dialogObj.requestFocus();   //request focus for dialog
  }

  /**
   * Shows a dialog with a Client Configuration Panel
   * after creating it if necessary.
   * @param parent the <code>Component</code> to check for a
   *		<code>Frame</code> or <code>Dialog</code>
   * @param name the name of the menu item.
   * @param displayText the display text for the menu item.
   * @param modalFlag true for a modal dialog, false for a modeless dialog
   * (that allows other windows to be active at the same time).
   */
  public void showDialog(Component parent, String name, String displayText,
                                                          boolean modalFlag)
  {
    showDialog(parent, name, displayText, modalFlag, false);
  }

  /**
   * Shows a modal dialog with a Client Configuration Panel
   * after creating it if necessary.
   * @param parent the <code>Component</code> to check for a
   *		<code>Frame</code> or <code>Dialog</code>
   * @param name the name of the menu item.
   * @param displayText the display text for the menu item.
   */
  public void showDialog(Component parent, String name, String displayText)
  {
    showDialog(parent, name, displayText, true);
  }

  /**
   * Closes dialog holding panel (if showing).
   * @param commitFlag true to commit any modified settings (equivalent to
   * the "OK" button); false to discard any modified settings (equivalent to
   * the "Cancel" button).
   */
  public void closeDialog(boolean commitFlag)
  {
    if(dialogObj != null && dialogObj.isVisible())
    {    //dialog object created and is visible
      if(commitFlag)
        commitConfigSettings();
      dialogObj.setVisible(false);
    }
  }

  /**
   * Closes dialog holding panel (if showing).  This method is equivalent
   * to "Cancel" button action.
   */
  public void closeDialog()
  {
    closeDialog(false);
  }

  /**
   * Returns the name of the currently-selected tab.
   * @return The name of the currently-selected tab, or null if there
   * is no currently-selected tab.
   */
  public String getCurrentTabName()
  {
    try
    {
      final int idx;
      if((idx=tabbedPaneObj.getSelectedIndex()) >= 0)
        return tabbedPaneObj.getTitleAt(idx);
    }
    catch(Exception ex)
    {         //some kind of exception error; just return null
    }
    return null;
  }

  /**
   * Selects the currently-displayed tab.
   * @param idxVal the index of the tab to display.
   */
  public void setSelectedTabIndex(int idxVal)
  {
    try
    {         //select tab:
      tabbedPaneObj.setSelectedIndex(idxVal);
    }
    catch(Exception ex)
    {         //some kind of exception error; ignore
    }
  }

  /**
   * Selects the currently-displayed tab.
   * @param tabNameStr the name of the tab to display.
   */
  public void setSelectedTabName(String tabNameStr)
  {
    try
    {         //select tab:
      final int idx;
      if((idx=tabbedPaneObj.indexOfTab(tabNameStr)) >= 0)
        tabbedPaneObj.setSelectedIndex(idx);
    }
    catch(Exception ex)
    {         //some kind of exception error; ignore
    }
  }

  /**
   * Sets up a listener to be called when this panel's dialog is closed.
   * This method may be called before or after the 'createDialog()'
   * or 'showDialog()' method.
   * @param listenerObj 'CloseListener' object to use, or null to clear
   * any previous 'CloseListener' object.
   */
  public void setDialogCloseListenerObj(
                                  IstiDialogPopup.CloseListener listenerObj)
  {
    dialogCloseListenerObj = listenerObj;
  }

  /**
   * Adds a component to the content panel of the specified tab.
   * @param tabNameStr name of tab whose component panel should be
   * used; or null for the selected-tab component.
   * @param labelObj label to be placed before component, or null for
   * none.
   * @param compObj Component to be added.
   * @return true if the component was added; false if the specified
   * tab could not be found.
   */
  public boolean addComponentToTab(String tabNameStr, Component labelObj,
                                                          Component compObj)
  {
    PropertyComponentPanel panelObj;
    if((panelObj=getSpecifiedComponentPanel(tabNameStr)) != null)
    {    //content panel for specified tab fetched OK
      panelObj.addProperty(labelObj,compObj);    //add components
      return true;
    }
    return false;
  }

  /**
   * Adds a component to the content panel of the specified tab.
   * @param tabNameStr name of tab whose component panel should be
   * used; or null for the selected-tab component.
   * @param compObj Component to be added.
   * @return true if the component was added; false if the specified
   * tab could not be found.
   */
  public boolean addComponentToTab(String tabNameStr, Component compObj)
  {
    return addComponentToTab(tabNameStr,null,compObj);
  }

  //----------------------------------------------------------------------
  // Support for PropertyChangeListener.
  //----------------------------------------------------------------------

//  /**
//   * Register a listener for the PropertyChange event.  The class will
//   * fire a PropertyChange value whenever the value is updated.
//   *
//   * @param listener  An object to be invoked when a PropertyChange
//   *		event is fired.
//   */
//  public void addPropertyChangeListener(
//      PropertyChangeListener listener)
//  {
//    synchronized(propChangeListenersSyncObj)
//    {    //grab thread-synchronization object for property change
//      if (listeners == null)
//      {
//        listeners = new Vector();
//      }
//      listeners.addElement(listener);
//    }
//  }
//
//  /**
//   * Remove a listener for the PropertyChange event.
//   *
//   * @param listener  The PropertyChange listener to be removed.
//   */
//  public void removePropertyChangeListener(
//      PropertyChangeListener listener)
//  {
//    synchronized(propChangeListenersSyncObj)
//    {    //grab thread-synchronization object for property change
//      if (listeners == null)
//      {
//        return;
//      }
//      listeners.removeElement(listener);
//    }
//  }
//
//  /**
//   * Report that we have been modified to any interested listeners.
//   *
//   * @param source  The PropertyEditor that caused the event.
//   */
//  public void firePropertyChange(Object source)
//  {
//    Vector targets;
//    synchronized(propChangeListenersSyncObj)
//    {    //grab thread-synchronization object for property change
//      if (listeners == null)
//      {
//        return;
//      }
//      targets = (Vector) listeners.clone();
//    }
//    // Tell our listeners that "everything" has changed.
//    PropertyChangeEvent evt = new PropertyChangeEvent(source, null, null, null);
//
//    for (int i = 0; i < targets.size(); i++)
//    {
//      PropertyChangeListener target =
//                 (PropertyChangeListener)targets.elementAt(i);
//      target.propertyChange(evt);
//    }
//  }


  /**
   * Gets the ok button.
   * @return the ok button.
   */
  protected JButton getOkButton()
  {
    return lowerButtonPanelObj.okButton;
  }

  /**
   * Gets the cancel button.
   * @return the cancel button.
   */
  protected JButton getCancelButton()
  {
    return lowerButtonPanelObj.cancelButton;
  }

  /**
   * Gets the reset button.
   * @return the reset button.
   */
  protected JButton getResetButton()
  {
    return lowerButtonPanelObj.resetButton;
  }

  /**
   * Gets the defaults button.
   * @return the defaults button.
   */
  protected JButton getDefaultsButton()
  {
    return lowerButtonPanelObj.defaultsButton;
  }

  /**
   * Returns the first "focus" component on the panel.  This is usually
   * a text-field, button, or similar component that wants to receive
   * focus after the dialog containing this panel is shown.
   * @return The first "focus" component on the panel, or null if none
   * available.
   */
  public Component getFirstFocusComponentObj()
  {
    return firstFocusComponentObj;
  }


  /**
   * Class ConfigLowerButtonPanel implements the lower button panel.
   */
  protected class ConfigLowerButtonPanel extends JPanel
  {
    protected final String okString =
        UIManager.getString("ColorChooser.okText");
    protected final String cancelString =
        UIManager.getString("ColorChooser.cancelText");
    protected final String resetString =
        UIManager.getString("ColorChooser.resetText");
    protected static final String defaultsString = "Defaults";
    protected static final String helpString = "Help";

    protected final JButton okButton = new JButton(okString);
    protected final JButton cancelButton = new JButton(cancelString);
    protected final JButton resetButton = new JButton(resetString);
    protected final JButton defaultsButton = new JButton(defaultsString);
    protected final JButton helpButton = new JButton(helpString);

    /**
     * Create Lower button panel
     */
    public ConfigLowerButtonPanel()
    {
      this.setLayout(new FlowLayout(FlowLayout.CENTER));
      okButton.setActionCommand("OK");
      okButton.setToolTipText("Accept changes");
      this.add(okButton);

      cancelButton.setActionCommand("cancel");
      cancelButton.setToolTipText("Cancel changes");
      this.add(cancelButton);

      resetButton.setToolTipText("Reset the settings above to the starting values");
      this.add(resetButton);

      defaultsButton.setEnabled(false);
      defaultsButton.setToolTipText(
           "Restore the settings above to the original defaults (where available)");
      this.add(defaultsButton);

      helpButton.setVisible(false);    //hide button until listener added
      helpButton.setToolTipText("Show help information for the settings");
      this.add(helpButton);
    }

    /**
     * Adds an <code>ActionListener</code> to the "OK" button.
     * @param l the <code>ActionListener</code> to be added.
     */
    public void addOkListener(ActionListener l)
    {
      okButton.addActionListener(l);
    }

    /**
     * Adds an <code>ActionListener</code> to the "Cancel" button.
     * @param l the <code>ActionListener</code> to be added.
     */
    public void addCancelListener(ActionListener l)
    {
      cancelButton.addActionListener(l);
    }

    /**
     * Adds an <code>ActionListener</code> to the "Reset" button.
     * @param l the <code>ActionListener</code> to be added.
     */
    public void addResetListener(ActionListener l)
    {
      resetButton.addActionListener(l);
    }

    /**
     * Adds an <code>ActionListener</code> to the "Defaults" button.
     * @param l the <code>ActionListener</code> to be added.
     */
    public void addDefaultListener(ActionListener l)
    {
      defaultsButton.addActionListener(l);
    }

    /**
     * Adds an <code>ActionListener</code> to the "Help" button.
     * @param l the <code>ActionListener</code> to be added.
     */
    public void addHelpListener(ActionListener l)
    {
      helpButton.addActionListener(l);
      helpButton.setVisible(l != null);     //show button if listener given
    }
  }
}
