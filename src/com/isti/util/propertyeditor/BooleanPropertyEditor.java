//BooleanPropertyEditor.java:  Creates a Property Editor for a boolean.
//
//   11/5/2002 -- [KF]
//   4/16/2003 -- [KF]  Add support for action listeners,
//                      add interface type selection and radio button option.
//

package com.isti.util.propertyeditor;

import java.awt.Component;
import java.awt.event.ActionListener;

import javax.swing.AbstractButton;
import javax.swing.ButtonGroup;
import javax.swing.JCheckBox;
import javax.swing.JPanel;
import javax.swing.JRadioButton;
import javax.swing.SwingConstants;

/**
 * A PropertyEditor for a boolean.
 */
public class BooleanPropertyEditor extends AbstractPropertyEditor
{
  /**
   * True/False interface type.
   */
  public static int TRUE_FALSE_BUTTONS = 0;

  /**
   * Checkbox interface type.
   */
  public static int CHECK_BOX = 1;

  /**
   * Radio Button interface type.
   */
  public static int RADIO_BUTTON = 2;

  /**
   * Default interface type.
   */
  public static int defaultInterfaceType = CHECK_BOX;

  /**
   * Default alignment for the action buttons.
   */
  public static int defaultAlignment = SwingConstants.LEFT;

  /**
   * Interface type.
   *
   * @see TRUE_FALSE_BUTTONS, CHECKBOX, USER_CONFIG
   */
  private final int interfaceType;

  private final static String TrueString = Boolean.TRUE.toString();
  private final static String FalseString = Boolean.FALSE.toString();

  /** The GUI components of this editor. */
  private final AbstractButton actionButton;
  private final AbstractButton trueButton;
  private final AbstractButton falseButton;
  private final Component customEditor;

  /**
   * Constructs the default boolean property editor.
   */
  public BooleanPropertyEditor()
  {
    this(defaultInterfaceType);
  }

  /**
   * Constructs the specified type of boolean property editor.
   *
   * @param interfaceType interface type
   * @see TRUE_FALSE_BUTTONS, CHECKBOX, RADIO_BUTTON
   */
  public BooleanPropertyEditor(int interfaceType)
  {
    this.interfaceType = interfaceType;

    if (interfaceType == TRUE_FALSE_BUTTONS)
    {
      final JPanel buttonPanel = new JPanel();
      final ButtonGroup buttonGroup = new ButtonGroup();

      trueButton = new JRadioButton(TrueString);
      falseButton = new JRadioButton(FalseString);

      trueButton.setActionCommand(TrueString);
      falseButton.setActionCommand(FalseString);
      buttonGroup.add(trueButton);
      buttonGroup.add(falseButton);
      buttonPanel.add(trueButton);
      buttonPanel.add(falseButton);

      actionButton = trueButton;
      customEditor = buttonPanel;
    }
    else  if (interfaceType == RADIO_BUTTON)
    {
      final JRadioButton radioButton = new JRadioButton();

      trueButton = falseButton = actionButton = radioButton;
      customEditor = actionButton;
    }
    else
    {
      final JCheckBox checkBoxButton = new JCheckBox();

      trueButton = falseButton = actionButton = checkBoxButton;
      customEditor = actionButton;
    }

    //setup the default alignment for the buttons
    setHorizontalAlignment(defaultAlignment);

    setSelected(true);  //initialize to selected
  }

  /**
   * Returns the horizontal alignment of the boolean property editor.
   * @return the <code>horizontalAlignment</code> property,
   *		one of the following values:
   * <ul>
   * <li>SwingConstants.RIGHT
   * <li>SwingConstants.LEFT
   * <li>SwingConstants.CENTER
   * <li>SwingConstants.LEADING
   * <li>SwingConstants.TRAILING
   * </ul>
   */
  public int getHorizontalAlignment()
  {
    return actionButton.getHorizontalAlignment();
  }

  /**
   * @return the interface type.
   * @see TRUE_FALSE_BUTTONS, CHECKBOX, RADIO_BUTTON
   */
  public int getInterfaceType()
  {
    return interfaceType;
  }

  /**
   * Determines whether this boolean property editor is enabled.
   * A boolean property editor may be enabled or disabled by
   * calling its <code>setEnabled</code> method.
   * @return <code>true</code> if the component is enabled;
   * <code>false</code> otherwise.
   * @see #setEnabled
   */
  protected boolean isEnabled()
  {
    return actionButton.isEnabled();
  }

  /**
   * Returns the state of the boolean property editor.
   * @return true if selected, otherwise false
   */
  protected boolean isSelected()
  {
    return actionButton.isSelected();
  }

  /**
   * Enables (or disables) the boolean property editor.
   * @param b  true to enable the boolean property editor, otherwise false
   */
  protected void setEnabled(boolean b)
  {
    trueButton.setEnabled(b);
    if (falseButton != trueButton)
    {
      falseButton.setEnabled(b);
    }
  }

  /**
   * Sets the horizontal alignment of the boolean property editor.
   * @param alignment  one of the following values:
   * <ul>
   * <li>SwingConstants.RIGHT (the default)
   * <li>SwingConstants.LEFT
   * <li>SwingConstants.CENTER
   * <li>SwingConstants.LEADING
   * <li>SwingConstants.TRAILING
   * </ul>
   * @beaninfo
   *        bound: true
   *         enum: LEFT     SwingConstants.LEFT
   *               CENTER   SwingConstants.CENTER
   *               RIGHT    SwingConstants.RIGHT
   *               LEADING  SwingConstants.LEADING
   *               TRAILING SwingConstants.TRAILING
   *    attribute: visualUpdate true
   *  description: The horizontal alignment of the icon and text.
   */
  protected void setHorizontalAlignment(int alignment)
  {
    trueButton.setHorizontalAlignment(alignment);
    if (falseButton != trueButton)
    {
      falseButton.setHorizontalAlignment(alignment);
    }
  }

  /**
   * Sets the state of the boolean property editor.
   * Note that this method does not trigger an <code>actionEvent</code>.
   * Call <code>doClick</code> to perform a programatic action change.
   *
   * @param b  true if the button is selected, otherwise false
   */
  protected void setSelected(boolean b)
  {
    trueButton.setSelected(b);
    if (falseButton != trueButton)
    {
      falseButton.setSelected(!b);
    }
  }

  /**
   * Sets the state of the boolean property editor and optionally
   * trigger an <code>actionEvent</code>.
   *
   * @param b  true if the button is selected, otherwise false
   * @param doClick true if an <code>actionEvent</code> should be triggered.
   */
  protected void setSelected(boolean b, boolean doClick)
  {
    if (doClick)
    {
      if (isSelected() != b)  //if value change
      {
        if (b)
          trueButton.doClick();
        else
          falseButton.doClick();
      }
    }
    else
    {
      setSelected(b);
    }
  }

  /**
   * Determines whether the propertyeditor can provide a custom editor.
   * @return true
   */
  public boolean supportsCustomEditor()
  {
    return true;
  }

  /**
   * Returns the editor GUI.
   * @return component for editor.
   */
  public Component getCustomEditor()
  {
    return customEditor;
  }

  /**
   * Sets value.
   * @param someObj value
   */
  public void setValue(Object someObj)
  {
    if (someObj == null)
      return;

    final boolean b = Boolean.valueOf(someObj.toString()).booleanValue();

    setSelected(b, true);
  }

  /**
   * Sets value as text.
   * @param text value
   */
  public void setAsText(String text)
  {
    if (text == null)
      return;
    setValue(text);
  }

  /**
   * Returns value.
   * @return the value
   */
  public Object getValue()
  {
    return Boolean.valueOf(isSelected());
  }

  /**
   * Returns value as a String.
   * @return the value as a string
   */
  public String getAsText()
  {
    return getValue().toString();
  }

  /**
   * Adds an <code>ActionListener</code> to the button.
   * @param l the <code>ActionListener</code> to be added
   */
  public void addActionListener(ActionListener l)
  {
    actionButton.addActionListener(l);
  }

  /**
   * Removes an <code>ActionListener</code> from the button.
   * If the listener is the currently set <code>Action</code>
   * for the button, then the <code>Action</code>
   * is set to <code>null</code>.
   *
   * @param l the listener to be removed
   */
  public void removeActionListener(ActionListener l)
  {
    actionButton.removeActionListener(l);
  }
}
