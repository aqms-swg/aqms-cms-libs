//CfgPropertyComponentPanel.java:  Implements a property component panel.
//
//  1/11/2006 -- [KF]  Initial version.
//

package com.isti.util.propertyeditor;

import java.awt.Component;
import java.beans.PropertyEditor;
import java.util.Enumeration;
import java.util.Hashtable;

import com.isti.util.CfgPropItem;

/**
 * Class CfgPropertyComponentPanel implements a property component panel.
 */
public class CfgPropertyComponentPanel extends PropertyComponentPanel
{
  private Hashtable editors = new Hashtable();  //table of editors

  /**
   * Adds the property to the panel.
   * @param item the configuration property item.
   */
  public void addProperty(CfgPropItem item)
  {
    //create the property label
    final Component propertyLabel =
        PropertyEditorFactory.createPropertyLabel(item);
    //create the property editor
    final PropertyEditor editor =
        PropertyEditorFactory.createPropertyEditor(item);
    //add the property to the panel
    super.addProperty(propertyLabel, editor);
    editors.put(item, editor);
  }

  /**
   * Adds the property to the panel.
   * @param propertyLabel the property label.
   * @param propertyeditor the property editor.
   * @deprecated replaced by <code>addProperty(CfgPropItem)</code>
   */
  public void addProperty(Component propertyLabel, PropertyEditor propertyeditor)
  {
    super.addProperty(propertyLabel, propertyeditor);
  }

  /**
   * Adds the property to the panel.
   * @param propertyLabel the property label.
   * @param propertyeditor the property editor.
   * @deprecated replaced by <code>addProperty(CfgPropItem)</code>
   */
  public void addProperty(Component propertyLabel, Component propertyeditor)
  {
    super.addProperty(propertyLabel, propertyeditor);
  }

  /**
   * Saves the configuration settings.
   * @return true if there was a change, false otherwise.
   */
  public boolean saveConfigSettings()
  {
    Object key, value;
    CfgPropItem item;
    PropertyEditor editor;
    boolean changeFlag = false;
    Enumeration e = editors.keys();
    while (e.hasMoreElements())
    {
      if ((key=e.nextElement()) instanceof CfgPropItem &&
          (value=editors.get(key)) instanceof PropertyEditor)
      {
        item = (CfgPropItem)key;
        editor = (PropertyEditor)value;
        value = CfgPropertyInspector.getEditorValue(item,editor);
        if (value != null)  //if value changed
        {
          if (item.setValue(value)) //if the value was set
            changeFlag = true;
        }
      }
    }
    return changeFlag;
  }
}
