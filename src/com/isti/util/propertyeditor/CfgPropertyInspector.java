//CfgPropertyInspector.java:  Defines a GUI control for editing a 'CfgPropItem'
//                            objects.
//
// 11/21/2002 -- [KF]
// 10/10/2003 -- [KF]  Add option to set the loaded flag when setting values.
//   1/8/2004 -- [KF]  Add name parameter to the 'createPropertyEditor' method.
//   2/5/2004 -- [ET]  Changed 'buttonPanel' reference to
//                     'lowerButtonPanelObj'; modified to use
//                     'BaseProperties' instead of 'CfgProperties'.
//   1/5/2005 -- [ET]  Modified to support 'ItemGroupSelector' as an
//                     item's group-select object.
//  1/11/2006 -- [KF}  Moved 'createPropertyLabel()' and 'createPropertyEditor()'
//                     base logic to the 'PropertyEditorFactory' class,
//                     Added 'getEditorValue()' method.
//  7/26/2006 -- [KF]  Changed to not display ignored items.
//

package com.isti.util.propertyeditor;

import java.util.Collection;
import java.beans.PropertyEditor;
import java.awt.Component;
import com.isti.util.BaseProperties;
import com.isti.util.CfgPropItem;
import com.isti.util.CfgPropItem.ItemGroupSelector;

/**
 * Class to inspect Properties.
 */
public class CfgPropertyInspector extends BasePropertyInspector
{
  /**
   * Set loaded flag.
   * If true sets the loaded flag when setting values.
   */
  public static boolean setLoadedFlag = false;
  //default save as text flag
  private static final boolean defaultSaveAsTextFlag = false;  //don't save as text
  private final BaseProperties propertiesObj;    //properties
  private final boolean startupItems;  //true if only startup items should be used.

  /**
   * Creates a JComponent with the properties to be changed.  This
   * component is suitable for inclusion into a GUI.
   * @param settingsProps Configuration properties object for Settings.
   */
  public CfgPropertyInspector(BaseProperties settingsProps)
  {
    this(settingsProps, false);
  }

  /**
   * Creates a JComponent with the properties to be changed.  This
   * component is suitable for inclusion into a GUI.
   * @param settingsProps Configuration properties object for Settings.
   * @param startupItems true if only startup items should be used.
   */
  public CfgPropertyInspector(BaseProperties settingsProps,
                                                       boolean startupItems)
  {
    super();
    saveAsTextFlag = defaultSaveAsTextFlag;
    centerFlag = true;            //center dialog over parent
    this.propertiesObj = settingsProps;
    this.startupItems = startupItems;
    //add the settings properties
    addProperties(settingsProps);
    //enable the defaults button
    lowerButtonPanelObj.defaultsButton.setEnabled(propertiesObj != null);
  }

  /**
   * Gets the ordered property keys fo the GUI.
   * @return collecion of property keys or null for default property key order.
   */
  protected Collection getPropertyKeys()
  {
    //use FIFO order from 'BaseProperties'
    return propertiesObj.keySet();
  }

  /**
   * Gets the default value for the key.
   * @param key the property key.
   * @return default value.
   */
  protected Object getDefaultValue(String key)
  {
    if (propertiesObj == null)    //if no properties
      return null;                //return no default
    CfgPropItem item = propertiesObj.get(key);
    if (item == null)   //if item not found
      return null;      //return no default
    return item.getDefaultValue();
  }

  /**
   * Gets the editor value.
   * @param item the configuration property item.
   * @param editor the property editor.
   * @return the the editor value or null if no change.
   */
  public static Object getEditorValue(CfgPropItem item,PropertyEditor editor)
  {
    return BasePropertyInspector.getEditorValue(
      item.getName(),editor,item.getValue(),defaultSaveAsTextFlag);
  }

  /**
   * Gets the table title for the key.
   * @param key the property key.
   * @return the tab title or null if key should not be on the inspector.
   */
  protected String getTabTitle(String key)
  {
    final CfgPropItem item = propertiesObj.get(key);
              //if item is ignored then return null (key not used):
    if (item.getIgnoreItemFlag())
      return null;
              //if using only startup items and item is not a
              // startup item then return null (key not used):
    if(startupItems && !item.isStartupConfigFlag())
      return null;

    final Object groupSelObj = item.getGroupSelObj();

         //if the group selection object is a string then
         // use the group select object for a tab title
    if (groupSelObj instanceof String)
      return (String)groupSelObj;
         //if 'ItemGroupSelector' then return its select string:
    if (groupSelObj instanceof ItemGroupSelector)
      return ((ItemGroupSelector)groupSelObj).getSelectStr();
    return null;  //key should not be on the inspector
  }

  /**
   * Creates a property label for the property item.
   * @param key the property key.
   * @return property label.
   */
  protected Component createPropertyLabel(String key)
  {
    final CfgPropItem item = propertiesObj.get(key);
    return PropertyEditorFactory.createPropertyLabel(item);
  }

  /**
   * Creates a property editor for the property item.
   * @param key the property key.
   * @return property editor.
   */
  protected PropertyEditor createPropertyEditor(String key)
  {
    final CfgPropItem item = propertiesObj.get(key);
    return PropertyEditorFactory.createPropertyEditor(item);
  }

  /**
   * Set the value for the key.
   * @param key the property key.
   * @param value new property value.
   */
  public void setValue(String key, Object value)
  {
    final CfgPropItem item = propertiesObj.get(key);
    item.setValue(value);
    if (setLoadedFlag)  //if setting load flag
      item.setLoadedFlag(true);
  }
}
