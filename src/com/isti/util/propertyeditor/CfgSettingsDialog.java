//CfgSettingsDialog.java:  Manages a configuration-settings dialog.
//
//  1/19/2006 -- [KF]  Initial version.
//   4/5/2006 -- [ET]  Modified 'showSettingsManualDialog()' to remove
//                     spaces from tab-name string value; fixed parameter
//                     usage in 'showSettingsDialog(String)' method.
//  5/31/2006 -- [ET]  Added 'clearCfgPropertyInspector()' method.
//

package com.isti.util.propertyeditor;

import java.awt.Component;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import com.isti.util.UtilFns;
import com.isti.util.ProgramInformationInterface;
import com.isti.util.gui.ViewHTMLPanelHandler;

/**
 * Class CfgSettingsDialog manages a configuration-settings dialog.
 */
public class CfgSettingsDialog
{
  //the program information
  private final ProgramInformationInterface progInfo;
  //the configuration property inspector
  private CfgPropertyInspector cfgPropertyInspector = null;

  /**
   * Creates a manager for a configuration-settings dialog.
   * @param progInfo the program information.
   */
  public CfgSettingsDialog(ProgramInformationInterface progInfo)
  {
    this.progInfo = progInfo;
  }

  /**
   * Creates a new instance of the "Settings" dialog.
   */
  public void createSettingsDialog()
  {
    cfgPropertyInspector = new CfgPropertyInspector(
                                        progInfo.getProgramCfgProperties());
              //setup "Help" button action:
    cfgPropertyInspector.addHelpActionListener(new ActionListener()
        {
          public void actionPerformed(ActionEvent evtObj)
          {
            //get name of currently-displayed tab and show
            // "Settings Manual" dialog, referenced to the tab name:
            if(cfgPropertyInspector != null)
            {
              showSettingsManualDialog(cfgPropertyInspector,
                                  cfgPropertyInspector.getCurrentTabName());
            }
          }
        });
  }

  /**
   * Gets the 'CfgPropertyInspector' object.
   * @return the 'CfgPropertyInspector' object or null if none.
   */
  public CfgPropertyInspector getCfgPropertyInspector()
  {
    return cfgPropertyInspector;
  }

  /**
   * Clears the 'CfgPropertyInspector' object.  This will force the property
   * inspector to be recreated the next time 'showSettingsDialog()' or
   * 'setupSettingsDialog()' is called.
   */
  public void clearCfgPropertyInspector()
  {
    cfgPropertyInspector = null;
  }

  /**
   * Gets the 'ProgramInformationInterface' object.
   * @return the 'ProgramInformationInterface' object.
   */
  public ProgramInformationInterface getProgramInformation()
  {
    return progInfo;
  }

  /**
   * Sets up the "Settings" dialog.
   */
  public void setupSettingsDialog()
  {
    if(cfgPropertyInspector == null)
    {
      createSettingsDialog();  //create the "Settings" dialog
    }
  }

  /**
   * Displays the "Settings" dialog.
   * @param tabNameStr the name of the tab to be displayed, or null
   * to display the default tab.
   */
  public final void showSettingsDialog(String tabNameStr)
  {
    showSettingsDialog(tabNameStr,false);
  }

  /**
   * Displays the "Settings" dialog.
   * @param waitFlag true to make this method block until the dialog is
   * closed.
   */
  public void showSettingsDialog(boolean waitFlag)
  {
    showSettingsDialog(null,waitFlag);
  }

  /**
   * Displays the "Settings" dialog.
   */
  public void showSettingsDialog()
  {
    showSettingsDialog(null,false);
  }

  /**
   * Displays the "Settings" dialog.
   * @param tabNameStr the name of the tab to be displayed, or null
   * to display the default tab.
   * @param waitFlag true to make this method block until the dialog is
   * closed.
   */
  public final void showSettingsDialog(String tabNameStr, boolean waitFlag)
  {
    setupSettingsDialog();
    if(tabNameStr != null)
      cfgPropertyInspector.setSelectedTabName(tabNameStr);
    final String settingsNameStr = progInfo.getSettingsConfigGroupName();
    cfgPropertyInspector.showDialog(
        progInfo.getProgramFrameObj(),
        settingsNameStr,settingsNameStr,false,waitFlag);
  }

  /**
   * Displays the "Help|SettingsManual" dialog window.
   * @param parentComponent the parent component for the dialog.
   * @param tabStr the name of the tab to use as the reference (anchor)
   * into the view.
   */
  public void showSettingsManualDialog(Component parentComponent,
                                                              String tabStr)
  {
    final ViewHTMLPanelHandler viewHTMLPanelHandler =
                                         progInfo.getViewHTMLPanelHandler();
    if(viewHTMLPanelHandler == null)
      return;
    final String settingsManualFilename =
                                       progInfo.getSettingsManualFilename();
    if(settingsManualFilename == null)
      return;
    final String settingsManualTitleString =
                                    progInfo.getSettingsManualTitleString();
    if(settingsManualTitleString == null)
      return;
    viewHTMLPanelHandler.showFileDialog(settingsManualFilename,
                                  settingsManualTitleString,parentComponent,
                       ((tabStr!=null)?UtilFns.stripChar(tabStr,' '):null));
  }                                    //(remove spaces from tab name)
}
