//ColorPropertyEditor.java:  Creates a Property Editor for a color.
//
//  10/29/2002 -- [KF]
//  10/31/2002 -- [KF]  Use 'colorToPropertyString' and 'parseColor' for
//                      converting Color property values.
//   11/6/2002 -- [ET]  Setup preferred size for color button.
//   11/8/2002 -- [ET]  Modified to hold the current color value in a local
//                      variable (instead of in the button background) and
//                      modified to make sure that the editor-button
//                      background color is always opaque (alpha = 255).
//  11/20/2002 -- [KF]  Move selector GUI to IstiColorSelector.
//   4/16/2003 -- [KF]  Add support for action listeners.
//

package com.isti.util.propertyeditor;

import java.awt.Color;
import java.awt.Component;
import java.awt.event.ActionListener;

import com.isti.util.UtilFns;
import com.isti.util.gui.IstiColorSelector;

/**
 * A PropertyEditor for a color.
 */
public class ColorPropertyEditor extends AbstractPropertyEditor
{
  public static final String DEF_TITLE_STR = "Select Color";

  /** The GUI component of this editor. */
  protected final IstiColorSelector colorSelector =
      new IstiColorSelector(Color.white, DEF_TITLE_STR);

  /**
   * Determines whether the propertyeditor can provide a custom editor.
   * @return true
   */
  public boolean supportsCustomEditor()
  {
    return true;
  }

  /**
   * Returns the editor GUI component.
   * @return component for editor.
   */
  public Component getCustomEditor()
  {
    return colorSelector.getSelectionComponent();
  }

  /**
   * Sets the color value.
   * @param someObj a Color object or a String object containing a valid
   * color name.
   * @throws IllegalArgumentException if an invalid color name is given.
   */
  public void setValue(Object someObj)
  {
    if (someObj == null)
      return;

    final Color colorValueObj;

    if (someObj instanceof Color)
    {
      colorValueObj = (Color)someObj;
    }
    else
    {
      final String text = someObj.toString();
      try
      {
        colorValueObj = UtilFns.parseColor(text);
      }
      catch (NumberFormatException nfe)
      {
        throw new IllegalArgumentException();
      }
    }
    colorSelector.setSelectedColor(colorValueObj);
  }

  /**
   * Sets value as text.
   * @param text value
   */
  public void setAsText(String text)
  {
    if (text == null)
      return;
    setValue(text);
  }

  /**
   * Returns value.
   * @return the value
   */
  public Object getValue()
  {
    return colorSelector.getSelectedColor();
  }

  /**
   * Returns value as a String.
   * @return the value as a string
   */
  public String getAsText()
  {
    return UtilFns.colorToPropertyString((Color)getValue());
  }

  /**
   * Adds an <code>ActionListener</code> to the button.
   * @param l the <code>ActionListener</code> to be added
   */
  public void addActionListener(ActionListener l)
  {
    colorSelector.addActionListener(l);
  }

  /**
   * Removes an <code>ActionListener</code> from the button.
   * If the listener is the currently set <code>Action</code>
   * for the button, then the <code>Action</code>
   * is set to <code>null</code>.
   *
   * @param l the listener to be removed
   */
  public void removeActionListener(ActionListener l)
  {
    colorSelector.removeActionListener(l);
  }
}