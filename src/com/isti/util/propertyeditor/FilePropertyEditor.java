//FilePropertyEditor.java:  Creates a Property Editor for files.
//
//    1/8/2004 -- [KF]
//   7/28/2004 -- [KF]  Changed button to use hand button.
//   9/14/2004 -- [KF]  Added 'getFileChooser()' method and default button text.
//   9/17/2004 -- [KF]  Modified to support directories.
//   2/11/2005 -- [ET]  Modified to use 'IstiFileChooser.createFileChooser()'.
//   2/16/2005 -- [ET]  Modified to substitute directory name in place of
//                      "." or ".." on editor button text and to convert
//                      absolute to relative paths when possible.
//

package com.isti.util.propertyeditor;

import java.io.File;
import java.io.IOException;
import java.awt.Component;
import java.awt.BorderLayout;
import java.awt.Insets;
import java.awt.FontMetrics;
import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.*;
import com.isti.util.gui.HandJButton;
import com.isti.util.gui.IstiFileChooser;


/**
 * A PropertyEditor for a files.
 */
public class FilePropertyEditor extends AbstractPropertyEditor
{
  private static final String SIZING_STRING = "CISN_Display_banner.gif";
  protected final static int MAX_BUTTON_TEXT = SIZING_STRING.length();
  protected String fileText = "";
  protected final IstiFileChooser fileChooser =
                                        IstiFileChooser.createFileChooser();
  protected static String curWorkingDirDotStr = ".";
         //setup 'File' object for current-working directory:
  protected static final File currentWorkingDirFileObj =
            new File(curWorkingDirDotStr).getAbsoluteFile().getParentFile();
         //setup string containing current-working directory path:
  protected static final String currentWorkingDirPathString =
                        currentWorkingDirFileObj.getPath() + File.separator;
         //setup length of current-working directory path string:
  protected static final int curWorkingDirPathStringLen =
                                       currentWorkingDirPathString.length();

  /** The GUI component of this editor. */
  protected final JPanel panelObj = new JPanel(new BorderLayout());
  protected final JButton buttonObj = new HandJButton();

  /**
   * Creates a Property Editor for a files.
   */
  public FilePropertyEditor()
  {
    this(false);
  }

  /**
   * Creates a Property Editor for a files.
   * @param directoryFlag true for directories only, false otherwise.
   */
  public FilePropertyEditor(boolean directoryFlag)
  {
    if (directoryFlag)  //if directories only
    {
      fileChooser.setFileSelectionMode(JFileChooser.DIRECTORIES_ONLY);
    }

    buttonObj.addActionListener(new ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        fileChooser.setCurrentSelection(getAsText());
        if (fileChooser.showDialog() == JFileChooser.APPROVE_OPTION)
        {
          setAsText(fileChooser.getCurrentSelection());
        }
      }
    });

    buttonObj.setMargin(new Insets(0,0,0,0));  //no margins around button text
    //get font metrics for button text:
    final FontMetrics metObj = buttonObj.getFontMetrics(buttonObj.getFont());
    //set button size based on sizing string:
    buttonObj.setPreferredSize(new Dimension(
        metObj.stringWidth(SIZING_STRING)+15,metObj.getHeight()+4));
    fileChooser.setDefaultParent(getCustomEditor());

    panelObj.add(buttonObj, BorderLayout.CENTER);
  }

  /**
   * Determines whether the propertyeditor can provide a custom editor.
   * @return true
   */
  public boolean supportsCustomEditor()
  {
    return true;
  }

  /**
   * Returns the editor GUI.
   * @return component for editor.
   */
  public Component getCustomEditor()
  {
    return panelObj;
  }

  /**
   * Returns the file chooser.
   * @return the file chooser.
   */
  public JFileChooser getFileChooser()
  {
    return fileChooser;
  }

  /**
   * Sets value.
   * @param someObj value
   */
  public void setValue(Object someObj)
  {
    if (someObj == null)
      return;
    fileText = someObj.toString();          //save given file path value
    File fTextFileObj = new File(fileText);
         //if equal to CWD then substitute ".":
    if(currentWorkingDirFileObj.equals(fTextFileObj))
      fileText = curWorkingDirDotStr;
         //if pathname starts with current directory then
         // remove directory-path from pathname:
    if(fileText.startsWith(currentWorkingDirPathString))
    {
      fileText = fileText.substring(curWorkingDirPathStringLen);
      fTextFileObj = new File(fileText);
    }
         //setup title for editor button:
    String buttonText;
    if (fileText.length() > 0)
    {    //file path is not empty
      try     //get 'File' object containing "canonical" path
      {       // (no trailing ".")
        fTextFileObj = fTextFileObj.getCanonicalFile();
      }
      catch(IOException ex)
      {       //error with 'getCanonicalFile()'; use 'getAbsoluteFile()'
        fTextFileObj = fTextFileObj.getAbsoluteFile();
      }
              //get last name in path for button text
              // (if ends with '/' then use full path to show root name):
      if((buttonText=fTextFileObj.getName()).endsWith(File.pathSeparator))
        buttonText = fTextFileObj.getPath();
    }
    else      //file path is empty; use dialog title for button title
      buttonText = fileChooser.getTitleText();
    if (buttonText.length() > MAX_BUTTON_TEXT)   //limit button text length
      buttonText = buttonText.substring(0, MAX_BUTTON_TEXT-3) + "...";
    buttonObj.setText(buttonText);     //enter button text
  }

  /**
   * Sets value as text.
   * @param text value
   */
  public void setAsText(String text)
  {
    if (text == null)
      return;
    setValue(text);
  }

  /**
   * Returns value.
   * @return the value
   */
  public Object getValue()
  {
    return getAsText();
  }

  /**
   * Returns value as a String.
   * @return the value as a string
   */
  public String getAsText()
  {
    return fileText;
  }

  /**
   * Adds an <code>ActionListener</code> to the button.
   * @param l the <code>ActionListener</code> to be added
   */
  public void addActionListener(ActionListener l)
  {
    fileChooser.addActionListener(l);
  }

  /**
   * Removes an <code>ActionListener</code> from the button.
   * If the listener is the currently set <code>Action</code>
   * for the button, then the <code>Action</code>
   * is set to <code>null</code>.
   *
   * @param l the listener to be removed
   */
  public void removeActionListener(ActionListener l)
  {
    fileChooser.removeActionListener(l);
  }
}
