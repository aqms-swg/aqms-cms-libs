//IstiNamedValueComponentPanel.java:  Implements a property component panel.
//
//   3/2/2006 -- [KF]  Initial version.
//

package com.isti.util.propertyeditor;

import java.awt.Component;
import java.beans.PropertyEditor;

import com.isti.util.FifoHashtable;
import com.isti.util.IstiNamedValueInterface;

/**
 * Class IstiNamedValueComponentPanel implements a property component panel.
 */
public class IstiNamedValueComponentPanel extends PropertyComponentPanel
{
  private FifoHashtable editors = new FifoHashtable(); //table of editors

  /**
   * Adds the properties to the panel.
   * @param items the named value items.
   */
  public void addProperties(IstiNamedValueInterface[] items)
  {
    for (int i = 0; i < items.length; i++)
    {
      addProperty(items[i]);
    }
  }

  /**
   * Adds the property to the panel.
   * @param item the named value item.
   */
  public void addProperty(IstiNamedValueInterface item)
  {
    //create the property label
    final Component propertyLabel =
        PropertyEditorFactory.createPropertyLabel(item);
    //create the property editor
    final PropertyEditor editor =
        PropertyEditorFactory.createPropertyEditor(item);
    editor.setValue(item.getValue());
    //add the property to the panel
    super.addProperty(propertyLabel, editor);
    editors.put(item, editor);
  }

  /**
   * Adds the property to the panel.
   * @param propertyLabel the property label.
   * @param propertyeditor the property editor.
   * @deprecated replaced by <code>addProperty(IstiNamedValueInterface)</code>
   */
  public void addProperty(Component propertyLabel, PropertyEditor propertyeditor)
  {
    super.addProperty(propertyLabel, propertyeditor);
  }

  /**
   * Adds the property to the panel.
   * @param propertyLabel the property label.
   * @param propertyeditor the property editor.
   * @deprecated replaced by <code>addProperty(IstiNamedValueInterface)</code>
   */
  public void addProperty(Component propertyLabel, Component propertyeditor)
  {
    super.addProperty(propertyLabel, propertyeditor);
  }

  /**
   * Gets the default properties.
   * @return the default properties.
   */
  public IstiNamedValueInterface[] getDefaultProperties()
  {
    final IstiNamedValueInterface[] defaultItems =
        new IstiNamedValueInterface[editors.size()];
    editors.keySet().toArray(defaultItems);
    return defaultItems;
  }

  /**
   * Gets the current properties.
   * @return the current properties.
   */
  public IstiNamedValueInterface[] getProperties()
  {
    final IstiNamedValueInterface[] defaultItems = getDefaultProperties();
    final int itemsLength = defaultItems.length;
    final IstiNamedValueInterface[] items = new IstiNamedValueInterface[itemsLength];
    for (int i = 0; i < itemsLength; i++)
    {
      final PropertyEditor editor = (PropertyEditor)editors.get(defaultItems[i]);
      items[i] = (IstiNamedValueInterface)defaultItems[i].clone();
      items[i].setValueString(editor.getAsText());
    }
    return items;
  }

  /**
   * Gets the property editor for the specifed default value item.
   * @param defaultItem the default value item.
   * @return the PropertyEditor.
   */
  public PropertyEditor getPropertyEditor(IstiNamedValueInterface defaultItem)
  {
    return (PropertyEditor)editors.get(defaultItem);
  }
}
