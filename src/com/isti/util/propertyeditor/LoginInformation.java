//LoginInformation.java:  Manages login information.
//
//    8/5/2004 -- [KF]  Initial version.
//   8/11/2004 -- [KF]  Changed to not encrypt empty password.
//   10/1/2004 -- [ET]  Added methods 'enterGUILoginPropertyEditor()',
//                      'showLoginDialog()' and 'loginAttemptFailed()'.
//   10/5/2004 -- [ET]  Removed 'enterGUILoginPropertyEditor()' method;
//                      modified 'showLoginDialog()' to detect property
//                      editor that extends 'ShowDialogPropertyEditor'.
//  10/12/2004 -- [ET]  Removed 'getEncryptedPassword()' method;
//                      modified 'setPropertyEditor()' to set value
//                      on editor; fixed issue where constructor would
//                      call overridable 'createEncryptedPassword()'
//                      method; modified methods 'showLoginDialog()' and
//                      'loginAttemptFailed()' to return boolean.
//  10/13/2004 -- [ET]  Added 'setLoginDialogMsgStrs()' method and default
//                      values for its strings.
//  10/25/2004 -- [ET]  Added methods 'clearEncryptedPassword()' and
//                      'clearLoginInfoText()'.
//   11/1/2004 -- [ET]  Modified 'getLoginInfoText()' to make sure
//                      encrypted password cannot be ommitted because
//                      plain-text password is not available.
//  11/22/2005 -- [ET]  Added methods 'copyLoginInfoResources()' and
//                      'setPasswordText()'.
//

package com.isti.util.propertyeditor;

import java.beans.PropertyEditor;
import com.isti.util.UtilFns;
import com.isti.util.BaseProperties;
import com.isti.util.JCrypt;

/**
 * Class LoginInformation manages login information.  GUI
 * programs that want a login-information dialog should execute
 * "setPropertyEditor(new LoginPropertyEditor())".
 */
public class LoginInformation implements PropertyEditorInformation
{
  //the separator string
  private static final String SEPARATOR_STRING =
      BaseProperties.SEPARATOR_STRING;
  //special characters
  private static final String SPECIAL_CHARS_STR =
      BaseProperties.SPECIAL_CHARS_STR;
  //the property editor
  private PropertyEditor propertyEditorObj = null;
  //the username text
  private final String usernameText;
  //the password text
  private String passwordText;
  //true if 'passwordText' is encrypted, false if it's plain text
  private boolean isEncryptedFlag;
  //the encrypted password
  private String encryptedPassword = null;
  //the text the represents the login information
  private String loginInfoText = null;
  //text always shown in login dialog:
  private static String loginDialogMsgStr = "Enter the login information " +
                                "to be used when connecting to the server:";
  //text shown in failed-login dialog before error message:
  private static String failMsgPrefixStr =
                                     "Unable to connect to the server:  \"";
  //text shown in failed-login dialog after error message:
  private static String failMsgSuffixStr = "\" \n\n";

  /**
   * Creates emtpy login information.
   */
  public LoginInformation()
  {
    this(UtilFns.EMPTY_STRING, UtilFns.EMPTY_STRING);
  }

  /**
   * Creates login information.
   * @param loginInfoStr the text the represents the login information.
   */
  public LoginInformation(String loginInfoStr)
  {
    final int separatorIndex = loginInfoStr.indexOf(SEPARATOR_STRING);
    if (separatorIndex >= 0)
    {
      usernameText = loginInfoStr.substring(0, separatorIndex-1);
      passwordText = loginInfoStr.substring(
            separatorIndex+SEPARATOR_STRING.length(),loginInfoStr.length());
    }
    else
    {
      usernameText = loginInfoStr;
      passwordText = UtilFns.EMPTY_STRING;
    }
    isEncryptedFlag = true;
  }

  /**
   * Creates login information.
   * @param usernameText the user name text.
   * @param passwordText the password text (not encrypted).
   */
  public LoginInformation(String usernameText, String passwordText)
  {
    this(usernameText, passwordText, passwordText.length() <= 0);
  }

  /**
   * Creates login information.
   * @param usernameText the user name text.
   * @param passwordText the password text.
   * @param isEncryptedFlag true if the password text is encrypted.
   */
  public LoginInformation(
      String usernameText, String passwordText, boolean isEncryptedFlag)
  {
    this.usernameText = usernameText;
    this.passwordText = passwordText;
    this.isEncryptedFlag = isEncryptedFlag;
  }

  /**
   * Indicates whether some other object is "equal to" this one.
   * @param   obj   the reference object with which to compare.
   * @return  <code>true</code> if this object is the same as the obj
   *          argument; <code>false</code> otherwise.
   */
  public boolean equals(Object obj)
  {
    if (!(obj instanceof LoginInformation))
      return false;
    return equals((LoginInformation)obj);
  }

  /**
   * Indicates whether some other login information object is "equal to" this one.
   * @param   loginInformationObj   the reference object with which to compare.
   * @return  <code>true</code> if this object is the same as the obj
   *          argument; <code>false</code> otherwise.
   */
  public boolean equals(LoginInformation loginInformationObj)
  {
    if (loginInformationObj == null)
      return false;
    return getLoginInfoText().equals(loginInformationObj.getLoginInfoText());
  }

  /**
   * Clears the encrypted password.  This will force the password to be
   * regenerated the next time 'getEncryptedPasswordText()' is called.
   */
  public void clearEncryptedPassword()
  {
    encryptedPassword = null;
  }

  /**
   * Gets the encrypted password.
   * @return the encrypted password.
   */
  public String getEncryptedPasswordText()
  {
    if(encryptedPassword == null)
    {    //encrypted password not yet generated
              //if encypted then use given pwd, else encrypt it:
      encryptedPassword = isEncryptedFlag ? passwordText :
                                                  createEncryptedPassword();
    }
    return encryptedPassword;
  }

  /**
   * Enters the password text.
   * @param pwdStr password text to enter.
   * @param isEncFlg true if encrypted, false if not.
   */
  protected void setPasswordText(String pwdStr, boolean isEncFlg)
  {
    passwordText = pwdStr;
    isEncryptedFlag = isEncFlg;
  }

  /**
   * Gets the password text.
   * @return the password text.
   */
  public String getPasswordText()
  {
    return passwordText;
  }

  /**
   * Gets the username text.
   * @return the username text.
   */
  public String getUsernameText()
  {
    return usernameText;
  }

  /**
   * Determines if the password is encrypted.
   * @return true if the password is encrypted, false otherwise.
   */
  public boolean isEncrypted()
  {
    return isEncryptedFlag;
  }

  /**
   * Determines if the value string is already quoted.
   * @return true if the value string is already quoted, false otherwise.
   * @see insertQuoteChars
   */
  public boolean isValueStringQuoted()
  {
    return true;
  }

  /**
   * Sets the property editor for the information.
   * @param propertyEditorObj the property editor for the information.
   */
  public void setPropertyEditor(PropertyEditor propertyEditorObj)
  {
    this.propertyEditorObj = propertyEditorObj;
    if(propertyEditorObj != null)
    {    //property editor was given
      propertyEditorObj.setValue(this);     //enter this obj into editor
      if(propertyEditorObj instanceof ShowDialogPropertyEditor)
      {  //property editor supports "showDialog" methods
              //enter login-dialog message string into property editor:
        ((ShowDialogPropertyEditor)propertyEditorObj).setLoginDialogMsgStr(
                                                         loginDialogMsgStr);
      }
    }
  }

  /**
   * Copies resources from the given login-information object into
   * this object.
   * @param infoObj the 'LoginInformation' object from which resources
   * should be copied.
   */
  public void copyLoginInfoResources(LoginInformation srcInfoObj)
  {
    setPropertyEditor(srcInfoObj.propertyEditorObj);
  }

  /**
   * Gets the property editor for the information.
   * @return the property editor for the information.
   */
  public PropertyEditor getPropertyEditor()
  {
    return propertyEditorObj;
  }

  /**
   * Displays the login dialog.
   * @return true if the user closed the dialog by selected the "OK"
   * button; false if not.
   */
  public boolean showLoginDialog()
  {
    return showLoginDialog(null);
  }

  /**
   * Displays the login dialog.
   * @param panelPromptStr the "prompt" text to be shown on the panel,
   * or null for none.
   * @return true if the user closed the dialog by selected the "OK"
   * button; false if not.
   */
  public boolean showLoginDialog(String panelPromptStr)
  {
    final PropertyEditor pEditObj;
    if((pEditObj=getPropertyEditor()) instanceof ShowDialogPropertyEditor)
    {    //property editor supports the 'showEditorDialog()' method
              //show dialog; return flag value:
      return ((ShowDialogPropertyEditor)pEditObj).showEditorDialog(
                                                            panelPromptStr);
    }
    return false;
  }

  /**
   * Called to indicate that a login attempt has failed.  If a
   * 'LoginPropertyEditor' object was entered via the 'setPropertyEditor()'
   * method then the login dialog will be shown.
   * @param messageStr the error-message text to use.
   * @return true if the login dialog was shown and the user closed the
   * dialog by selected the "OK" button; false if not.
   */
  public boolean loginAttemptFailed(String messageStr)
  {
         //if message string given then put in prefix and suffix strings:
    if(messageStr != null && messageStr.length() > 0)
      messageStr = failMsgPrefixStr + messageStr + failMsgSuffixStr;
    return showLoginDialog(messageStr);
  }

  /**
   * Clears the cached value for the string representation of the object.
   * This will force the representation to be regenerated the next time
   * 'getLoginInfoText()' is called.
   */
  public void clearLoginInfoText()
  {
    loginInfoText = null;
  }

  /**
   * Returns a string representation of the object.
   * @return a string representation of the object.
   */
  public String getLoginInfoText()
  {
    if(loginInfoText == null)
    {    //login text not yet generated; do it now
      loginInfoText = "\"" + insertQuoteChars(usernameText) + "\"";
      final String pwdStr;
      if((pwdStr=getEncryptedPasswordText()).length() > 0)
      {  //password contains data; add to string
        loginInfoText += SEPARATOR_STRING + "\"" +
                                            insertQuoteChars(pwdStr) + "\"";
      }
    }
    return loginInfoText;
  }

  /**
   * Returns a string representation of the object.
   * @return a string representation of the object.
   */
  public String toString()
  {
    return getLoginInfoText();
  }

  /**
   * Creates the encrypted password.
   * @return the encrypted password.
   */
  protected String createEncryptedPassword()
  {
    return JCrypt.crypt(usernameText, passwordText);
  }

  /**
   * Returns a string with a backslash quote character ('\')
   * inserted in front of each occurance of a "special" character
   * in the given source string.  If they are included in the string
   * of "special" characters, the ASCII characters tab, newline, and
   * carriage return are written as \t, \n, and \r, respectively.
   * @param dataStr the data source string.
   * @return a new string with its "special" characters quoted, or the
   * original string if no matching "special" characters were found.
   */
  protected static String insertQuoteChars(String dataStr)
  {
    return UtilFns.insertQuoteChars(dataStr, SPECIAL_CHARS_STR);
  }

  /**
   * Modifies the message strings to be shown in the login dialog.
   * If used, this method should be called before any instances of
   * login-information objects are created.
   * @param loginDialogMsgStr text always shown in login dialog.
   * @param failMsgPrefixStr text shown in failed-login dialog before
   * error message.
   * @param failMsgSuffixStr text shown in failed-login dialog after
   * error message.
   */
  public static void setLoginDialogMsgStrs(String loginDialogMsgStr,
                           String failMsgPrefixStr, String failMsgSuffixStr)
  {
    LoginInformation.loginDialogMsgStr = loginDialogMsgStr;
    LoginInformation.failMsgPrefixStr = failMsgPrefixStr;
    LoginInformation.failMsgSuffixStr = failMsgSuffixStr;
  }
}
