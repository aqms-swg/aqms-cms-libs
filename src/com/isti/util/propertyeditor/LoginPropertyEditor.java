//LoginPropertyEditor.java:  Creates a Property Editor for a login
//                           username and password.
//
//    8/5/2004 -- [KF]  Initial version.
//   8/11/2004 -- [KF]  Changed text, increased the size of the button,
//                      made the username field have the initial focus.
//   10/1/2004 -- [ET]  Added 'showLoginDialog()' method.
//   10/5/2004 -- [ET]  Modified to use 'setUsernameAsInitalFocus()';
//                      changed final class objects from 'public' to
//                      'protected'; added optional 'panelPromptStr'
//                      parameter to 'showLoginDialog()' method; renamed
//                      'showLoginDialog()' to 'showEditorDialog()';
//                      modified to extend 'ShowDialogPropertyEditor'.
//  10/12/2004 -- [ET]  Added optional 'parentCompObj' parameter to
//                      constructor; added 'get/setDialogModalFlag()'
//                      methods and made non-modal the default for the
//                      password dialog; changed ".error()" log outputs
//                      to ".warning()"; modified 'createLoginInformation()'
//                      to copy over property editor; modified to only
//                      allow one password dialog shown at a time; modified
//                      'showEditorDialog()' to return boolean.
//  10/13/2004 -- [ET]  Added "get/setLoginDialogMsgStr()" methods.
//  11/22/2005 -- [ET]  Modified to use 'copyLoginInfoResources()' in
//                      'createLoginInformation()' method.
//

package com.isti.util.propertyeditor;

import java.util.Vector;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.FontMetrics;
import java.awt.Insets;
import java.awt.event.*;
import javax.swing.*;
import com.isti.util.UtilFns;
import com.isti.util.CfgPropItem;
import com.isti.util.LogFile;
import com.isti.util.gui.HandJButton;
import com.isti.util.gui.IstiPasswordDialog;

/**
 * A PropertyEditor for a login username and password.
 */
public class LoginPropertyEditor extends ShowDialogPropertyEditor
{
  /** The sizing string. */
  private static final String SIZING_STRING = "LoginPropertyEditorSize";
  /** The default title string. */
  public static final String DEF_TITLE_STR =  "Login Information";
  /** The default button string. */
  private static final String DEF_BUTTON_STR = DEF_TITLE_STR;
  /** The GUI component of this editor. */
  private final JButton editorButtonObj = new HandJButton(DEF_BUTTON_STR);
  /** The login information. */
  private LoginInformation editorLoginInformationObj = null;
  /** The listener list. */
  protected final Vector listenerList = new Vector();
  /** Handle to currently-displayed password dialog. */
  private IstiPasswordDialog currentPwdDialogObj = null;
  /** Default parent component for dialog, or null for none. */
  private Component parentComponentObj = null;
  /** Dialog modal flag, true = modal, false = non-modal (modeless). */
  private boolean dialogModalFlag = false;
  //message text to be shown in login dialog:
  private String loginDialogMsgStr = null;


  /**
   * Creates a login property editor.
   */
  public LoginPropertyEditor()
  {
    this(null);
  }

  /**
   * Creates a login property editor.
   * @param parentCompObj the default parent component for the password
   * dialog, or null for none.
   */
  public LoginPropertyEditor(Component parentCompObj)
  {
    parentComponentObj = parentCompObj;             //setup parent component
    editorButtonObj.setMargin(new Insets(0,0,0,0)); //no margins around text
    //get font metrics for button text:
    final FontMetrics metObj =
                  editorButtonObj.getFontMetrics(editorButtonObj.getFont());
    //set button size based on sizing string:
    editorButtonObj.setPreferredSize(new Dimension(
        metObj.stringWidth(SIZING_STRING)+15,metObj.getHeight()+4));
    //setup show-dialog action for button:
    editorButtonObj.addActionListener(new ActionListener()
        {
          public void actionPerformed(ActionEvent e)
          {
            showEditorDialog(editorButtonObj,null);
          }
        });
  }

  /**
   * Sets the dialog modal flag.  This determines whether or not the
   * password dialod will be modal.  The default is non-modal (allows
   * other windows to operate).
   * @param flgVal true for modal, false for non-modal (modeless).
   */
  public void setDialogModalFlag(boolean flgVal)
  {
    dialogModalFlag = flgVal;          //setup flag for future dialogs
  }

  /**
   * Returns the dialog modal flag.  This indicates whether or not the
   * password dialod will be modal.
   * @return true for modal, false for non-modal (modeless).
   */
  public boolean getDialogModalFlag()
  {
    return dialogModalFlag;
  }

  /**
   * Displays the login dialog.
   * @return true if the user closed the dialog by selected the "OK"
   * button; false if not.
   */
  public boolean showEditorDialog()
  {
    return showEditorDialog(null,null);
  }

  /**
   * Displays the login dialog.
   * @param panelPromptStr the "prompt" text to be shown on the panel,
   * or null for none.
   * @return true if the user closed the dialog by selected the "OK"
   * button; false if not.
   */
  public boolean showEditorDialog(String panelPromptStr)
  {
    return showEditorDialog(null,panelPromptStr);
  }

  /**
   * Displays the login dialog.
   * @param parentComp the parent component to use, or null to use the
   * default parent component.
   * @param panelPromptStr the "prompt" text to be shown on the panel,
   * or null for none.
   * @return true if the user closed the dialog by selected the "OK"
   * button; false if not.
   */
  public boolean showEditorDialog(Component parentComp, String panelPromptStr)
  {
    boolean retFlag = false;
    if(currentPwdDialogObj == null)
    {    //dialog not currently displayed
      if(loginDialogMsgStr != null)
      {  //message string was setup
              //add message str to dialog (append to prompt str if given):
        panelPromptStr = (panelPromptStr != null) ?
                   (panelPromptStr + loginDialogMsgStr) : loginDialogMsgStr;
      }
         //create the login-information dialog:
      currentPwdDialogObj = createDialog(((parentComp != null) ?
                          parentComp : parentComponentObj), panelPromptStr);
         //set up the dialog:
      setupDialog(getLoginInformation(),currentPwdDialogObj);
      if(currentPwdDialogObj.showAndWait() ==
                      IstiPasswordDialog.LOGIN_DIALOG_DEFAULT_OPTION_STRING)
      {  //user selected the "OK" button; fetch and enter values from dialog
        setLoginInformation(currentPwdDialogObj.getUsernameText(),
                                     currentPwdDialogObj.getPasswordText());
        retFlag = true;           //indicate "OK" button was selected
      }
      currentPwdDialogObj = null;      //indicate dialog not displayed
    }
    else      //dialog is currently displayed
      currentPwdDialogObj.requestFocus();        //put focus on dialog
    return retFlag;
  }

  /**
   * Adds an <code>ActionListener</code> to the button.
   * @param l the <code>ActionListener</code> to be added
   */
  public void addActionListener(ActionListener l)
  {
    listenerList.add(l);
  }

  /**
   * Creates up the dialog.
   * @param parent the parent of the dialog or null for none.
   * @param panelPromptStr the "prompt" text to be shown on the panel,
   * or null for none.
   * @return the dialog.
   */
  public IstiPasswordDialog createDialog(Component parent,
                                                      String panelPromptStr)
  {
    final IstiPasswordDialog dialogObj = new IstiPasswordDialog(
                                       parent,DEF_TITLE_STR,panelPromptStr);
    dialogObj.setModal(dialogModalFlag);    //setup modal status
    return dialogObj;
  }

  /**
   * Creates up the dialog.
   * @param parent the parent of the dialog or null for none.
   * @return the dialog.
   */
  public IstiPasswordDialog createDialog(Component parent)
  {
    return createDialog(parent,null);
  }

  /**
   * Creates login information for the specified text.
   * @param loginInformationObj the current login information.
   * @param text the text.
   * @return the new login information or null if error.
   */
  public static LoginInformation createLoginInformation(
      LoginInformation loginInformationObj, String text)
  {
    //if login information exists
    if (loginInformationObj != null)
    {
      //Find the constructor with a String parameter and
      //make a new instance of the object.
      try
      {
        final Class valueClass = loginInformationObj.getClass();
        final Class[] parameterTypes = {String.class};
        final java.lang.reflect.Constructor c =
            valueClass.getConstructor(parameterTypes);
        final Object [] initargs = {text};
        final LoginInformation newLoginInfoObj =
                                (LoginInformation)(c.newInstance(initargs));
                   //copy resources into new login-info object:
        newLoginInfoObj.copyLoginInfoResources(loginInformationObj);
        return newLoginInfoObj;
      }
      catch (Exception ex)
      {
        //some kind of exception error; log it
        LogFile.getGlobalLogObj(true).warning(
            "Error creating login information object:  " + ex);
        LogFile.getGlobalLogObj(true).warning(
                                           UtilFns.getStackTraceString(ex));
        return null;
      }
    }
    return new LoginInformation(text);
  }

  /**
   * Creates login information for the specified user name and password.
   * @param loginInformationObj the current login information.
   * @param usernameText the user name text.
   * @param passwordText the password text.
   * @return the new login information or null if error.
   */
  public static LoginInformation createLoginInformation(
      LoginInformation loginInformationObj, String usernameText,
      String passwordText)
  {
    //if login information exists
    if (loginInformationObj != null)
    {
      //Find the constructor with two String parameters and
      //make a new instance of the object.
      try
      {
        final Class valueClass = loginInformationObj.getClass();
        final Class[] parameterTypes = {String.class, String.class};
        final java.lang.reflect.Constructor c =
            valueClass.getConstructor(parameterTypes);
        final Object [] initargs = {usernameText, passwordText};
        final LoginInformation newLoginInfoObj =
                                (LoginInformation)(c.newInstance(initargs));
                   //copy resources into new login-info object:
        newLoginInfoObj.copyLoginInfoResources(loginInformationObj);
        return newLoginInfoObj;
      }
      catch (Exception ex)
      {
        //some kind of exception error; log it
        LogFile.getGlobalLogObj(true).warning(
            "Error creating login information object:  " + ex);
        LogFile.getGlobalLogObj(true).warning(
                                           UtilFns.getStackTraceString(ex));
        return null;
      }
    }
    return new LoginInformation(usernameText, passwordText);
  }

  /**
   * Returns value as a String.
   * @return the value as a string
   */
  public String getAsText()
  {
    return getLoginInformation().toString();
  }

  /**
   * Returns the editor GUI component.
   * @return component for editor.
   */
  public Component getCustomEditor()
  {
    return editorButtonObj;
  }

  /**
   * Gets the login information.
   * @return the login information.
   */
  public LoginInformation getLoginInformation()
  {
    //create the login information if needed
    if (editorLoginInformationObj == null)
    {
      editorLoginInformationObj = new LoginInformation();
    }
    return editorLoginInformationObj;
  }

  /**
   * Gets the password text.
   * @return the password text.
   */
  public String getPasswordText()
  {
    return getLoginInformation().getPasswordText();
  }

  /**
   * Gets the username text.
   * @return the username text.
   */
  public String getUsernameText()
  {
    return getLoginInformation().getUsernameText();
  }

  /**
   * Returns value.
   * @return the value
   */
  public Object getValue()
  {
    return getLoginInformation();
  }

  /**
   * Removes an <code>ActionListener</code> from the button.
   * If the listener is the currently set <code>Action</code>
   * for the button, then the <code>Action</code>
   * is set to <code>null</code>.
   *
   * @param l the listener to be removed
   */
  public void removeActionListener(ActionListener l)
  {
    listenerList.remove(l);
  }

  /**
   * Sets value as text.
   * @param text value
   */
  public void setAsText(String text)
  {
    setValue(createLoginInformation(editorLoginInformationObj, text));
  }

  /**
   * Sets up the dialog.
   * @param loginInformationObj the login information.
   * @param dialogObj the dialog object.
   * @see createDialog
   */
  public static void setupDialog(
      LoginInformation loginInformationObj, IstiPasswordDialog dialogObj)
  {
         //setup to make the username field have the initial focus:
    dialogObj.setUsernameAsInitalFocus();
         //enter the username value into the dialog:
    dialogObj.setUsernameText(loginInformationObj.getUsernameText());
         //enter the password value into the dialog (if not encrypted):
    dialogObj.setPasswordText(loginInformationObj.isEncrypted() ?
              UtilFns.EMPTY_STRING : loginInformationObj.getPasswordText());
  }

  /**
   * Sets the login information.
   * @param loginInformationObj the login information.
   */
  public void setValue(LoginInformation loginInformationObj)
  {
    //exit if no information or no change
    if (loginInformationObj == null ||
        loginInformationObj.equals(editorLoginInformationObj))
    {
      return;
    }

    editorLoginInformationObj = loginInformationObj;  //save the information

    final String usernameText = loginInformationObj.getUsernameText();
    if (usernameText.length() > 0)
    {
      //set the button text to the username
      editorButtonObj.setText(usernameText);
    }
    else
    {
      //set the button text to the default
      editorButtonObj.setText(DEF_BUTTON_STR);
    }

    //notify listeners of the change
    fireActionPerformed(
        new ActionEvent(this, ActionEvent.ACTION_PERFORMED,
        "value set"));
  }

  /**
   * Sets the login information.
   * @param someObj the login information.
   */
  public void setValue(Object someObj)
  {
    if (someObj == null)
      return;

    if (someObj instanceof LoginInformation)
    {
      setValue((LoginInformation)someObj);
    }
    else if (someObj instanceof CfgPropItem)
    {
      setValue(((CfgPropItem)someObj).getValue());
    }
    else
    {
      setAsText(someObj.toString());
    }
  }

  /**
   * Determines whether the propertyeditor can provide a custom editor.
   * @return true
   */
  public boolean supportsCustomEditor()
  {
    return true;
  }

  /**
   * Notifies all listeners that have registered interest for
   * notification on this event type.  The event instance
   * is lazily created using the parameters passed into
   * the fire method.
   *
   * @param event  the <code>ChangeEvent</code> object
   * @see EventListenerList
   */
  protected void fireActionPerformed(ActionEvent event)
  {
    Object[] listeners = listenerList.toArray();

    // Process the listeners last to first, notifying
    // those that are interested in this event
    for (int i = listeners.length-1; i >= 0; i--)
    {
      ((ActionListener)listeners[i]).actionPerformed(event);
    }
  }

  /**
   * Sets the login information.
   * @param usernameText the user name text.
   * @param passwordText the password text.
   */
  protected void setLoginInformation(String usernameText, String passwordText)
  {
    setValue(createLoginInformation(editorLoginInformationObj, usernameText,
                                    passwordText));
  }

  /**
   * Sets a message string to be shown in the login dialog.  If a prompt
   * string is given via the 'showEditorDialog()' method then this
   * message string will be appended to the prompt string.
   * @param str the message string to show, or null for none.
   */
  public void setLoginDialogMsgStr(String str)
  {
    loginDialogMsgStr = str;
  }

  /**
   * Returns the message string to be shown in the login dialog.
   * @return The message string to be shown in the login dialog, or
   * null if none was given.
   */
  public String getLoginDialogMsgStr()
  {
    return loginDialogMsgStr;
  }
}
