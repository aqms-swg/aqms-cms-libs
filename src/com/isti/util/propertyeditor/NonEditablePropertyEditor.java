//NonEditablePropertyEditor.java:  Creates a Property Editor for a
//                                 non-editable property.
//
//  11/25/2002 -- [KF]
//

package com.isti.util.propertyeditor;

/**
 * A PropertyEditor that doesn't let you edit the property.
 */
public class NonEditablePropertyEditor extends StringPropertyEditor
{
  /**
   * Creates A PropertyEditor for a non-editable property.
   */
  public NonEditablePropertyEditor()
  {
    textField.setBackground(null);
    textField.setEditable(false);
  }
}