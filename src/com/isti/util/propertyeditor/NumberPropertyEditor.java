//NumberPropertyEditor.java:  Creates a Property Editor for a number.
//
//   11/5/2002 -- [KF]
//   4/16/2003 -- [KF]  Add support for action listeners.
//  12/20/2005 -- [KF]  Changed to use the 'parseNumber()' method to support
//                      other locales.
//

package com.isti.util.propertyeditor;

import java.awt.Component;
import java.awt.event.ActionListener;

import javax.swing.JPanel;

import com.isti.util.UtilFns;
import com.isti.util.gui.FilteredJTextField;

/**
 * A PropertyEditor for a number.
 */
public class NumberPropertyEditor extends AbstractPropertyEditor
{
  /** The GUI component of this editor. */
  protected final FilteredJTextField textField;

  private Class valueClass = Number.class;
  protected final Object [] initargs = new Object[1];

  /**
   * Creates A PropertyEditor for a number.
   */
  public NumberPropertyEditor()
  {
    this(Integer.valueOf(0), false, null);
  }

  /**
   * Creates A PropertyEditor for a number.
   * @param valueObj the value of the item.
   */
  public NumberPropertyEditor(Object valueObj)
  {
    this(valueObj, false, null);
  }

  /**
   * Creates A PropertyEditor for a number.
   * @param valueObj the value of the item.
   * @param isUnsignedFlag true if unsigned number.
   * @param maxValue the maximum value for the item or null if not needed.
   * @return property editor.
   * @see com.isti.util.gui.FilteredJTextField
   */
  public NumberPropertyEditor(
      Object valueObj,
      boolean isUnsignedFlag,
      Comparable maxValue)
  {
    //if the value is not null
    if (valueObj != null)
    {
      valueClass = valueObj.getClass();
    }

    final boolean isFloatFlag =
        valueObj instanceof Float || valueObj instanceof Double;
    //create and setup edit field:
    textField = new FilteredJTextField(null,
                                       PropertyEditorFactory.
                                       NUMERIC_TEXTFIELD_SIZE,
                                       isFloatFlag, isUnsignedFlag);
    //if item's validator had max value then enter it:
    if (maxValue != null)
      textField.setMaxValue(maxValue);
  }

  /**
   * Determines whether the propertyeditor can provide a custom editor.
   * @return true
   */
  public boolean supportsCustomEditor()
  {
    return true;
  }

  /**
   * Returns the editor GUI.
   * @return component for editor.
   */
  public Component getCustomEditor()
  {
    final JPanel panel = new JPanel();
    panel.add(textField);
    return panel;
  }

  /**
   * Sets value.
   * @param someObj value
   */
  public void setValue(Object someObj)
  {
    if (someObj == null)
      return;

    textField.setText(someObj.toString());
  }

  /**
   * Sets value as text.
   * @param text value
   */
  public void setAsText(String text)
  {
    if (text == null)
      return;
    setValue(text);
  }

  /**
   * Returns value.
   * @return the value
   */
  public Object getValue()
  {
    final String str = getAsText();
    final Number numberObj = UtilFns.parseNumber(str,valueClass);
    if (numberObj != null)
      return numberObj;
    //failed to convert to a number
    return str;
  }

  /**
   * Returns value as a String.
   * @return the value as a string
   */
  public String getAsText()
  {
    return textField.getText();
  }

  /**
   * Adds an <code>ActionListener</code> to the button.
   * @param l the <code>ActionListener</code> to be added
   */
  public void addActionListener(ActionListener l)
  {
    textField.addActionListener(l);
  }

  /**
   * Removes an <code>ActionListener</code> from the button.
   * If the listener is the currently set <code>Action</code>
   * for the button, then the <code>Action</code>
   * is set to <code>null</code>.
   *
   * @param l the listener to be removed
   */
  public void removeActionListener(ActionListener l)
  {
    textField.removeActionListener(l);
  }
}
