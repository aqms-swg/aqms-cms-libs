//PropertyComponentPanel.java:  Implements a property component panel.
//
//  1/11/2006 -- [KF]  Moved from the 'BasePropertyInspector' class.
//  3/23/2006 -- [ET]  Added methods 'getFirstPropEditorCompObj()',
//                     'getFirstFocusComponentObj()' and
//                     'getFirstFocusChildCompObj()'.
//

package com.isti.util.propertyeditor;

import java.awt.Component;
import java.awt.BorderLayout;
import java.awt.GridBagLayout;
import java.awt.GridBagConstraints;
import java.awt.Insets;
import javax.swing.JPanel;
import javax.swing.JLabel;
import javax.swing.JToggleButton;
import javax.swing.BorderFactory;
import java.beans.PropertyEditor;

/**
 * Class PropertyComponentPanel implements a property component panel.
 */
public class PropertyComponentPanel extends JPanel
{
  //panel for items, laid out via GridBag:
  private final JPanel itemsPanel = new JPanel(new GridBagLayout());
  private final GridBagConstraints constraints = new GridBagConstraints();
  private Component firstPropEditorCompObj = null;
  private Component firstFocusComponentObj = null;

  /**
   * Creates a property component panel.
   */
  public PropertyComponentPanel()
  {
    //setup empty space around item:
    setBorder(BorderFactory.createEmptyBorder(10, 10, 5, 10));
    setLayout(new BorderLayout()); //border layout for host panel
    add(itemsPanel, BorderLayout.NORTH); //start items at top of panel
    constraints.anchor = GridBagConstraints.WEST; //left flush items
    constraints.gridx = constraints.gridy = 0; //start top-left
    constraints.insets = new Insets(2, 1, 1, 1); //space around items
  }

  /**
   * Adds the property to the panel.
   * @param propertyLabel the property label.
   * @param propertyEditor the property editor.
   */
  public void addProperty(Component propertyLabel, PropertyEditor propertyEditor)
  {
    if (propertyEditor.supportsCustomEditor())
      addProperty(propertyLabel, propertyEditor.getCustomEditor());
  }

  /**
   * Adds the property to the panel.
   * @param propertyLabel the property label.
   * @param propertyEditor the property editor.
   */
  public void addProperty(Component propertyLabel, Component propertyEditor)
  {
    constraints.gridx = 0; //back to first column
    if (! (propertyEditor instanceof JToggleButton &&
           propertyLabel instanceof JLabel))
    { //editor item not a JToggleButton (or label item not a JLabel)
      if (propertyLabel != null)
      { //prompt label was given
        constraints.gridwidth = 1; //each item spans 1 column
        itemsPanel.add(propertyLabel, constraints); //add prompt label
        ++constraints.gridx; //move to next column
        itemsPanel.add(propertyEditor, constraints); //add property editor
      }
      else
      { //prompt label not given
        constraints.gridwidth = 2; //item spans 2 columns
        itemsPanel.add(propertyEditor, constraints); //add property editor
      }
    }
    else
    { //editor item is a JToggleButton and label item is a JLabel
      String promptStr = ( (JLabel) propertyLabel).getText(); //get label
      if (promptStr != null)
      { //prompt-label text not null
        promptStr = promptStr.trim(); //trim any trailing spaces
        final int len = promptStr.length(); //get length of string
        //if ends with colon then trim colon:
        if (len > 0 && promptStr.charAt(len - 1) == ':')
          promptStr = promptStr.substring(0, len - 1);
        //enter prompt-label text into check-box button object:
        ( (JToggleButton) propertyEditor).setText(promptStr);
      }
      constraints.gridwidth = 2; //item spans 2 columns
      itemsPanel.add(propertyEditor, constraints); //add property editor
    }
    ++constraints.gridy; //move to next row
              //if first one then save handle to property editor component:
    if(firstPropEditorCompObj == null)
      firstPropEditorCompObj = propertyEditor;
  }

  /**
   * Returns the first "custom" property-editor component entered into
   * the panel.
   * @return The first "custom" property-editor component entered into
   * the panel, or null if none available.
   */
  public Component getFirstPropEditorCompObj()
  {
    return firstPropEditorCompObj;
  }

  /**
   * Returns the first "focus" component on the panel.  This is usually
   * a text-field, button, or similar component that wants to receive
   * focus after a dialog containing this panel is shown.
   * @return The first "focus" component on the panel, or null if none
   * available.
   */
  public Component getFirstFocusComponentObj()
  {
    try
    {
      if(firstFocusComponentObj == null && firstPropEditorCompObj != null)
      {  //component not already fetched and first-prop-editor available
        firstFocusComponentObj =       //find "focus" child component
                          getFirstFocusChildCompObj(firstPropEditorCompObj);
      }
      return firstFocusComponentObj;
    }
    catch(Exception ex)
    {    //some kind of exception error
      return null;      //just return null
    }
  }

  /**
   * Returns the first "focus" child component of the given component.
   * The first branch of the component children of the given component
   * is recursively searched until a text-field, button, or similar
   * component is found or the end of the branch is reached.
   * @param compObj parent Component object to use.
   * @return The first "focus" child component of the given component, or
   * the given component if no suitable child componet could be found.
   */
  public static Component getFirstFocusChildCompObj(Component compObj)
  {
    if(compObj instanceof javax.swing.text.JTextComponent ||
                            compObj instanceof javax.swing.AbstractButton ||
                                compObj instanceof java.awt.TextComponent ||
                                         compObj instanceof java.awt.Button)
    {    //given component is text or button object
      return compObj;        //return given component
    }
         //get array of child comonents:
    final Component [] compArr;
    if(compObj instanceof javax.swing.JComponent)
      compArr = ((javax.swing.JComponent)compObj).getComponents();
    else if(compObj instanceof java.awt.Container)
      compArr = ((java.awt.Container)compObj).getComponents();
    else                     //if not container component then
      return compObj;        //just return given component
    if(compArr == null || compArr.length <= 0 || compArr[0] == null)
      return compObj;        //if no valid children then return given comp
         //do recursive call with first child component:
    return getFirstFocusChildCompObj(compArr[0]);
  }
}
