//PropertyEditorFactory.java:  Creates a Property Editor.
//
//  11/6/2002 -- [KF]
//   1/8/2004 -- [KF]  Added name parameter to the 'createPropertyEditor'
//                     method and support for file property editors.
//   4/8/2004 -- [KF]  Added support for region property editors.
//  6/10/2004 -- [ET]  Removed 'IstiRegion' import.
//  8/04/2004 -- [KF]  Added support of login editor (PropertyEditorInformation).
//  9/14/2004 -- [KF]  Added support for directory values.
//  9/16/2004 -- [ET]  Modified to setup "Choose Directory" title for
//                     chooser dialog for "directory" property editor.
//  9/17/2004 -- [KF]  Modifed to use 'FilePropertyEditor(true)' for directory
//                     values.
//  1/11/2006 -- [KF}  Added 'createPropertyLabel()' and 'createPropertyEditor()'
//                     methods previously in the 'CfgPropertyInspector' class.
//   3/6/2006 -- [KF]  Added support for 'IstiNamedValueInterface' and
//                     for allowed characters.
//   5/11/2012 -- [KF]  Added support to use max integer value to specify the
//                      number of characters for string values.
//

package com.isti.util.propertyeditor;

import java.awt.Component;
import java.beans.PropertyEditor;

import javax.swing.JComponent;
import javax.swing.JLabel;

import com.isti.util.CfgPropValidator;
import com.isti.util.IstiNamedValueInterface;
import com.isti.util.PropItemInterface;
import com.isti.util.UtilFns;
import com.isti.util.gui.FilteredJTextField;

/**
 * PropertyEditorFactory is used to create a Property Editor based on
 * the type of the property value.
 */
public class PropertyEditorFactory
{
  //the number of columns in text fields
  public static final int NUMERIC_TEXTFIELD_SIZE = 15;
  public static final int STRING_TEXTFIELD_SIZE = 15;

  /** Don't let anyone instantiate this class */
  private PropertyEditorFactory() {}

  /**
   * Creates a property label for the property item.
   * A pretty version of name will be used.
   * @param name the name of the item.
   * @return property label.
   */
  public static Component createPropertyLabel(
      String name)
  {
    return createPropertyLabel(name, null);
  }

  /**
   * Creates a property label for the property item.
   * @param name the name of the item.
   * @param descriptionStr description string
   * or null if pretty version of name should be used.
   * @return property label.
   */
  public static Component createPropertyLabel(
      String name,
      String descriptionStr)
  {
    final String compString;
    //if the description string exists
    if (descriptionStr != null && descriptionStr.length() > 0)
      //use the description string
      compString = descriptionStr;
    //if the description string does not exist
    else
      //use a pretty version of the name
      compString = UtilFns.getPrettyString(name,false);

    final JComponent label = new JLabel(compString + ":  ");
    return label;
  }

  /**
   * Creates a property label for the property item.
   * @param item the property item.
   * @return property label.
   */
  public static Component createPropertyLabel(IstiNamedValueInterface item)
  {
    final String descriptionStr;
    if (item instanceof PropItemInterface)
      descriptionStr = ((PropItemInterface)item).getDescriptionStr();
    else
      descriptionStr = null;
    return createPropertyLabel(item.getName(),descriptionStr);
  }

  /**
   * Creates a property editor for the property item.
   * @param name the name of the item.
   * @param valueObj the value of the item.
   * @return property editor.
   * @see com.isti.util.gui.FilteredJTextField
   */
  public static PropertyEditor createPropertyEditor(
      String name,
      Object valueObj)
  {
    return createPropertyEditor(
        name,
        valueObj,
        false,
        null,
        null);
  }

  /**
   * Creates a property editor for the property item.
   * @param name the name of the item.
   * @param valueObj the value of the item.
   * @param isUnsignedFlag true if unsigned number.
   * @param maxValue the maximum value for the item or null if not needed.
   * @return property editor.
   * @see com.isti.util.gui.FilteredJTextField
   */
  public static PropertyEditor createPropertyEditor(
      String name,
      Object valueObj,
      boolean isUnsignedFlag,
      Comparable maxValue)
  {
    return createPropertyEditor(
        name,
        valueObj,
        isUnsignedFlag,
        maxValue,
        null);
  }

  /**
   * Creates a property editor for the property item.
   * @param validValuesArr valid values array.
   * @return property editor.
   * @see com.isti.util.gui.FilteredJTextField
   */
  public static PropertyEditor createPropertyEditor(Object [] validValuesArr)
  {
    return createPropertyEditor(
        null,
        null,
        false,
        null,
        validValuesArr);
  }

  /**
   * Creates a property editor for the property item.
   * @param name the name of the item.
   * @param valueObj the value of the item.
   * @param isUnsignedFlag true if unsigned number.
   * @param maxValue the maximum value for the item or null if not needed.
   * @param validValuesArr valid values array or null if not needed.
   * @return property editor.
   * @see com.isti.util.gui.FilteredJTextField
   */
  public static PropertyEditor createPropertyEditor(
      String name,
      Object valueObj,
      boolean isUnsignedFlag,
      Comparable maxValue,
      Object [] validValuesArr)
  {
    final PropertyEditor propertyeditor;

    if(validValuesArr != null && validValuesArr.length > 0)
    {
      //validator uses array of valid values; build combo-box with values
      propertyeditor = new ValueArrayPropertyEditor(validValuesArr);
    }
    else
    {
      if (valueObj instanceof Number)
      {
        propertyeditor = new NumberPropertyEditor(
            valueObj, isUnsignedFlag, maxValue);
      }
      else if(valueObj instanceof Boolean)
      {
        propertyeditor = new BooleanPropertyEditor();
      }
      else if (valueObj instanceof java.awt.Color)
      {
        //setup color selection panel
        propertyeditor = new ColorPropertyEditor();
      }
      else if (valueObj instanceof PropertyEditorInformation)
      {
        propertyeditor =
            ((PropertyEditorInformation)valueObj).getPropertyEditor();
      }
      else if (valueObj instanceof String && maxValue instanceof Integer)
      {
        propertyeditor = new StringPropertyEditor(new FilteredJTextField(
            ((Integer) maxValue).intValue()));
      }
      else if (name != null)
      {
        final String lowerName = name.toLowerCase();
        if (lowerName.endsWith("file") && valueObj instanceof String)
          propertyeditor = new FilePropertyEditor();
        else if (lowerName.endsWith("directory") && valueObj instanceof String)
          propertyeditor = new FilePropertyEditor(true);
        else if (lowerName.endsWith("region") || lowerName.endsWith("regions"))
          propertyeditor = new PropertyEditorLauncher(
              new RegionPropertyEditor(), UtilFns.getPrettyString(name));
        else
          propertyeditor = new StringPropertyEditor();
      }
      else
        propertyeditor = new StringPropertyEditor();
    }

    return propertyeditor;
  }

  /**
   * Creates a property editor for the property item.
   * @param item the property item.
   * @return property editor.
   */
  public static PropertyEditor createPropertyEditor(IstiNamedValueInterface item)
  {
    final Object auxObj;
    final CfgPropValidator validatorObj;

    if (item instanceof PropItemInterface)
    {
      final PropItemInterface propItem = (PropItemInterface)item;
      auxObj = propItem.getAuxiliaryObj();
      validatorObj = propItem.getValidator();
    }
    else
    {
      auxObj = null;
      validatorObj = null;
    }

    if (auxObj instanceof PropertyEditor)  //if aux object is a property editor
      return (PropertyEditor)auxObj;  //use the property editor

    //setup child components:
    final Object [] validValuesArr;
    final boolean isUnsignedFlag;
    final Comparable maxValueObj;

    if(validatorObj != null)  //if item contains a validator object
    {
      if (validatorObj.allowedChars != null)  //if allowed characters exist
        return new StringPropertyEditor(
        new FilteredJTextField(UtilFns.EMPTY_STRING,validatorObj.allowedChars));
      validValuesArr = validatorObj.validValuesArr;   //save values array
      //set flag if min >= 0:
      isUnsignedFlag = validatorObj.isMinimumNonNegative();
      maxValueObj = validatorObj.maxValueCompObj;     //save max value obj
    }
    else
    {    //item does not contain a validator object
      validValuesArr = null;           //no values array
      isUnsignedFlag = false;          //no min value
      maxValueObj = null;              //no max value
    }
    return createPropertyEditor(
        item.getName(),
        item.getValue(),
        isUnsignedFlag,
        maxValueObj,
        validValuesArr);
  }
}
