//PropertyEditorInformation.java:  Interface for Property Editor information.
//
//   8/05/2004 -- [KF]  Initial version.
//

package com.isti.util.propertyeditor;

import java.beans.PropertyEditor;

/**
 * Property editor information.
 */
public interface PropertyEditorInformation
{
  /**
   * Gets the property editor for the information.
   * @return the property editor for the information.
   */
  public PropertyEditor getPropertyEditor();

  /**
   * Determines if the value string is already quoted.
   * @return true if the value string is already quoted, false otherwise.
   * @see insertQuoteChars
   */
  public boolean isValueStringQuoted();

  /**
   * Sets the property editor for the information.
   * @param propertyEditor the property editor for the information.
   */
  public void setPropertyEditor(PropertyEditor propertyEditor);
}