//PropertyEditorLauncher.java:  Creates a Property Editor launcher.
//
//    4/8/2004 -- [KF]
//

package com.isti.util.propertyeditor;

import java.awt.Component;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.Vector;

import javax.swing.JButton;
import javax.swing.JOptionPane;

import com.isti.util.gui.IstiDialogPopup;

/**
 * A PropertyEditor launcher.
 */
public class PropertyEditorLauncher extends AbstractPropertyEditor
{
  private final JButton button;
  private final AbstractPropertyEditor propertyEditor;
  private Object valueObj = null;
  private String valueStr = null;
  private Vector listenerList = new Vector();
  private IstiDialogPopup dialogObj = null;

  /**
   * Creates a PropertyEditor launcher.
   * @param propertyEditor the property editor that will be launched.
   * @param displayText the display text.
   */
  public PropertyEditorLauncher(
      AbstractPropertyEditor propertyEditor, String displayText)
  {
    this.propertyEditor = propertyEditor;
    button = new JButton("Edit");
    final Component parentComp = button;
    final Object msgObj = propertyEditor.getCustomEditor();
    final String titleStr = displayText;
    final int msgType = JOptionPane.PLAIN_MESSAGE;
    final boolean modalFlag = false;
    final int optionType = IstiDialogPopup.OK_CANCEL_OPTION;

    button.addActionListener(new ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        if (dialogObj == null || !dialogObj.isVisible())
        {
          //make sure the value is correct
          PropertyEditorLauncher.this.propertyEditor.setAsText(valueStr);
          PropertyEditorLauncher.this.propertyEditor.setValue(valueObj);
          dialogObj = new IstiDialogPopup(
              parentComp, msgObj, titleStr, msgType, null, 0, modalFlag, false,
              optionType);
          final int retVal = dialogObj.showAndReturnIndex();
          if (retVal == JOptionPane.OK_OPTION)
          {
            updateValue();  //updates the value object and string
          }
          dialogObj = null;
        }
        else
        {
          dialogObj.requestFocus();
        }
      }
    });
  }

  /**
   * Determines whether the propertyeditor can provide a custom editor.
   * @return true
   */
  public boolean supportsCustomEditor()
  {
    return propertyEditor.supportsCustomEditor();
  }

  /**
   * Returns the editor GUI.
   * @return component for editor.
   */
  public Component getCustomEditor()
  {
    return button;
  }

  /**
   * Sets value.
   * @param someObj value
   */
  public void setValue(Object someObj)
  {
    propertyEditor.setValue(someObj);
    updateValue();  //updates the value object and string
  }

  /**
   * Sets value as text.
   * @param text value
   */
  public void setAsText(String text)
  {
    propertyEditor.setAsText(text);
    updateValue();  //updates the value object and string
  }

  /**
   * Returns value.
   * @return the value
   */
  public Object getValue()
  {
    return valueObj;
  }

  /**
   * Returns value as a String.
   * @return the value as a string
   */
  public String getAsText()
  {
    return valueStr;
  }

  /**
   * Adds an <code>ActionListener</code> to the property dditor.
   * @param l the <code>ActionListener</code> to be added
   */
  public void addActionListener(ActionListener l)
  {
    listenerList.add(l);
  }

  /**
   * Removes an <code>ActionListener</code> from the property editor.
   * If the listener is the currently set <code>Action</code>
   * for the button, then the <code>Action</code>
   * is set to <code>null</code>.
   *
   * @param l the listener to be removed
   */
  public void removeActionListener(ActionListener l)
  {
    listenerList.remove(l);
  }

  /**
   * Notifies all listeners that have registered interest for
   * notification on this event type.  The event instance
   * is lazily created using the parameters passed into
   * the fire method.
   *
   * @param event  the <code>ChangeEvent</code> object
   * @see EventListenerList
   */
  protected void fireActionPerformed(ActionEvent event)
  {
    Object[] listeners = listenerList.toArray();

    // Process the listeners last to first, notifying
    // those that are interested in this event
    for (int i = listeners.length-1; i >= 0; i--)
    {
      ((ActionListener)listeners[i]).actionPerformed(event);
    }
  }

  /**
   * Updates the value object and string.
   */
  protected void updateValue()
  {
    valueObj = propertyEditor.getValue();
    valueStr = propertyEditor.getAsText();
    //notify listeners of the change
    fireActionPerformed(
        new ActionEvent(this, ActionEvent.ACTION_PERFORMED, "update"));
  }
}