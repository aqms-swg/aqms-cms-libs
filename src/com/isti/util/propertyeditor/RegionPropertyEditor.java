//RegionPropertyEditor.java:  Creates a Property Editor for a region.
//
//   3/30/2004 -- [KF]
//

package com.isti.util.propertyeditor;

import java.awt.Component;
import java.awt.event.*;
import com.isti.util.gui.IstiRegionPanel;

/**
 * A PropertyEditor for a region.
 */
public class RegionPropertyEditor extends AbstractPropertyEditor
{
  /** The GUI component of this editor. */
  private final IstiRegionPanel panel = new IstiRegionPanel();

  /**
   * Creates A PropertyEditor for a region.
   */
  public RegionPropertyEditor()
  {
  }

  /**
   * Creates A PropertyEditor for a region.
   * @param valueObj the value of the item.
   */
  public RegionPropertyEditor(Object valueObj)
  {
    setValue(valueObj);
  }

  /**
   * Creates A PropertyEditor for a region.
   * @param text the value of the item.
   */
  public RegionPropertyEditor(String text)
  {
    setAsText(text);
  }

  /**
   * Adds an <code>ActionListener</code> to this editor.
   * @param l the <code>ActionListener</code> to be added
   */
  public void addActionListener(ActionListener l)
  {
    panel.addActionListener(l);
  }

  /**
   * Returns value as a String.
   * @return the value as a string
   */
  public String getAsText()
  {
    return panel.getRegionString();
  }

  /**
   * Returns the editor GUI.
   * @return component for editor.
   */
  public Component getCustomEditor()
  {
    return panel;
  }

  /**
   * Returns value.
   * @return the value
   */
  public Object getValue()
  {
    return getAsText();
  }

  /**
   * Removes an <code>ActionListener</code> from this editor.
   * If the listener is the currently set <code>Action</code>
   * for the button, then the <code>Action</code>
   * is set to <code>null</code>.
   *
   * @param l the listener to be removed
   */
  public void removeActionListener(ActionListener l)
  {
    panel.removeActionListener(l);
  }

  /**
   * Sets value as text.
   * @param text value
   */
  public void setAsText(String text)
  {
    if (text == null)
      return;

    panel.setRegionString(text);
  }

  /**
   * Sets value.
   * @param someObj value
   */
  public void setValue(Object someObj)
  {
    if (someObj == null)
      return;

    setAsText(someObj.toString());
  }

  /**
   * Determines whether the propertyeditor can provide a custom editor.
   * @return true
   */
  public boolean supportsCustomEditor()
  {
    return true;
  }
}