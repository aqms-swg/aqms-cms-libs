//ShowDialogPropertyEditor.java:  Defines a property editor that supports
//                                'showEditorDialog()' methods.
//
//  10/5/2004 -- [ET]  Initial version.
// 10/12/2004 -- [ET]  Modified 'showEditorDialog()' to return boolean.
// 10/13/2004 -- [ET]  Added "get/setLoginDialogMsgStr()" methods.
//

package com.isti.util.propertyeditor;

/**
 * Class ShowDialogPropertyEditor defines a property editor that supports
 * 'showEditorDialog()' methods.
 */
public abstract class ShowDialogPropertyEditor extends AbstractPropertyEditor
{
  /**
   * Displays the property-editor dialog.
   * @return true if the user closed the dialog by selected the "OK"
   * button; false if not.
   */
  public abstract boolean showEditorDialog();

  /**
   * Displays the property-editor dialog.
   * @param panelPromptStr the "prompt" text to be shown on the panel,
   * or null for none.
   * @return true if the user closed the dialog by selected the "OK"
   * button; false if not.
   */
  public abstract boolean showEditorDialog(String panelPromptStr);

  /**
   * Sets a message string to be shown in the login dialog.  If a prompt
   * string is given via the 'showEditorDialog()' method then this
   * message string will be appended to the prompt string.
   * @param str the message string to show, or null for none.
   */
  public abstract void setLoginDialogMsgStr(String str);

  /**
   * Returns the message string to be shown in the login dialog.
   * @return The message string to be shown in the login dialog, or
   * null if none was given.
   */
  public abstract String getLoginDialogMsgStr();
}
