//StringPropertyEditor.java:  Creates a Property Editor for a string.
//
//   11/5/2002 -- [KF]
//   4/16/2003 -- [KF]  Added support for action listeners.
//    3/1/2006 -- [KF]  Added constructor with text field parameter.
//

package com.isti.util.propertyeditor;

import java.awt.Component;
import java.awt.event.ActionListener;

import javax.swing.JPanel;
import javax.swing.JTextField;

/**
 * A PropertyEditor for a string.
 */
public class StringPropertyEditor extends AbstractPropertyEditor
{
  /** The GUI component of this editor. */
  protected final JTextField textField;

  /**
   * Create the PropertyEditor for a string.
   */
  public StringPropertyEditor()
  {
    this(new JTextField());
  }

  /**
   * Create the PropertyEditor for a string.
   * @param textField the text field.
   */
  public StringPropertyEditor(JTextField textField)
  {
    this.textField = textField;
    textField.setColumns(PropertyEditorFactory.STRING_TEXTFIELD_SIZE);
  }

  /**
   * Determines whether the propertyeditor can provide a custom editor.
   * @return true
   */
  public boolean supportsCustomEditor()
  {
    return true;
  }

  /**
   * Returns the editor GUI.
   * @return component for editor.
   */
  public Component getCustomEditor()
  {
    final JPanel panel = new JPanel();
    panel.add(textField);
    return panel;
  }

  /**
   * Sets value.
   * @param someObj value
   */
  public void setValue(Object someObj)
  {
    if (someObj == null)
      return;

    textField.setText(someObj.toString());
  }

  /**
   * Sets value as text.
   * @param text value
   */
  public void setAsText(String text)
  {
    if (text == null)
      return;
    setValue(text);
  }

  /**
   * Returns value.
   * @return the value
   */
  public Object getValue()
  {
    return getAsText();
  }

  /**
   * Returns value as a String.
   * @return the value as a string
   */
  public String getAsText()
  {
    return textField.getText();
  }

  /**
   * Adds an <code>ActionListener</code> to the button.
   * @param l the <code>ActionListener</code> to be added
   */
  public void addActionListener(ActionListener l)
  {
    textField.addActionListener(l);
  }

  /**
   * Removes an <code>ActionListener</code> from the button.
   * If the listener is the currently set <code>Action</code>
   * for the button, then the <code>Action</code>
   * is set to <code>null</code>.
   *
   * @param l the listener to be removed
   */
  public void removeActionListener(ActionListener l)
  {
    textField.removeActionListener(l);
  }
}
