//ValueArrayPropertyEditor.java:  Creates a Property Editor for a value array.
//
//  10/29/2002 -- [KF]
//   4/16/2003 -- [KF]  Add support for action listeners.
//   1/13/2004 -- [KF]  Add work around for JVM 1.3 bug (see setSelectedItem).
//   2/12/2004 -- [KF]  Fix problem where first selection is blank and
//                      remove the previous work around.
//  11/04/2004 -- [KF]  Added support for list.
//   2/10/2021 -- [KF]  Modified to work with non-String values.
//

package com.isti.util.propertyeditor;

import java.awt.Component;
import java.awt.event.ActionListener;
import java.util.Vector;

import javax.swing.DefaultComboBoxModel;
import javax.swing.JComboBox;
import javax.swing.JPanel;

import com.isti.util.UtilFns;

/**
 * A PropertyEditor for a value array.
 */
public class ValueArrayPropertyEditor extends AbstractPropertyEditor
{
  /** The GUI component of this editor. */
  protected final JComboBox comboBox;
  private final boolean useListFlag;  //true to use list

  /**
   * Creates A PropertyEditor for a value array.
   */
  public ValueArrayPropertyEditor()
  {
    comboBox = new JComboBox();
    useListFlag = true;  //use list
  }

  /**
   * Creates A PropertyEditor for a value array.
   * @param validValuesArr valid values array.
   */
  public ValueArrayPropertyEditor(Object [] validValuesArr)
  {
    comboBox = new JComboBox(validValuesArr);
    useListFlag = false;  //use single value
  }

  /**
   * Determines whether the propertyeditor can provide a custom editor.
   * @return true
   */
  public boolean supportsCustomEditor()
  {
    return true;
  }

  /**
   * Returns the editor GUI.
   * @return component for editor.
   */
  public Component getCustomEditor()
  {
    final JPanel panel = new JPanel();
    panel.add(comboBox);
    return panel;
  }

  /**
   * Sets value.
   * @param someObj value
   */
  public void setValue(Object someObj)
  {
    if (someObj == null)
      return;

    //select as string
    setAsText(someObj.toString());
  }

  /**
   * Sets value as text.
   * @param text value
   */
  public void setAsText(String text)
  {
    if (text == null)
      return;

    if (useListFlag)  //if using list
    {
      //use the list and use the default of the first item selected
      final Object[] items = UtilFns.listStringToVector(text).toArray();
      comboBox.setModel(new DefaultComboBoxModel(items));
      return;
    }

    //try to select the item
    comboBox.setSelectedItem(text);

    //exit if selection matches
    if (comboBox.getSelectedItem().equals(text))
      return;

    //look for the text in the combo box items
    for (int i = 0; i < comboBox.getItemCount(); i++)
    {
      //if an item with the text was found
      final Object someObj = comboBox.getItemAt(i);
      if (someObj.toString().equalsIgnoreCase(text))
      {
        //set that item and exit
        comboBox.setSelectedItem(someObj);
        return;
      }
    }
  }

  /**
   * Returns value.
   * @return the value
   */
  public Object getValue()
  {
    final Object selectedItem = comboBox.getSelectedItem();
    if (useListFlag)  //if using list
    {
      //add all items to the list and
      // put selected item first so that it remains selected
      final int itemCount = comboBox.getItemCount();
      Vector items = new Vector(itemCount);
      Object item;
      items.add(selectedItem);
      for (int i = 0; i < itemCount; i++)
      {
        item = comboBox.getItemAt(i);
        if (!item.equals(selectedItem))
          items.add(item);
      }
      return UtilFns.enumToListString(items.elements());
    }
    return selectedItem;
  }

  /**
   * Returns value as a String.
   * @return the value as a string
   */
  public String getAsText()
  {
    return getValue().toString();
  }

  /**
   * Adds an <code>ActionListener</code> to the button.
   * @param l the <code>ActionListener</code> to be added
   */
  public void addActionListener(ActionListener l)
  {
    comboBox.addActionListener(l);
  }

  /**
   * Removes an <code>ActionListener</code> from the button.
   * If the listener is the currently set <code>Action</code>
   * for the button, then the <code>Action</code>
   * is set to <code>null</code>.
   *
   * @param l the listener to be removed
   */
  public void removeActionListener(ActionListener l)
  {
    comboBox.removeActionListener(l);
  }
}