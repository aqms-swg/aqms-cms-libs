//EventQueue.java:  Defines event-queue access methods.
//
//   4/6/2004 -- [ET]  Module imported from OpenORB.
//

/*
* Copyright (C) The Community OpenORB Project. All rights reserved.
*
* This software is published under the terms of The OpenORB Community Software
* License version 1.0, a copy of which has been included with this distribution
* in the LICENSE.txt file.
*/

package com.isti.util.queue;

/**
 * An event queue is an object into which events are pushed and pulled.
 * Pushing an event is adding an event to this queue, pulling an event
 * is retrieving an event from  this queue.
 *
 * Events in the queue can be either Untyped, Structured, StructuredSequence
 * and Type events.
 *
 * @author Olivier Modica
 */
public interface EventQueue
{

    /**
     * Push an event into the queue
     */
    void pushEvent( java.lang.Object event );

    /**
     * Pull an event from the queue
     */
    java.lang.Object pullEvent();

    /**
    * Return the current queue size
    */
    int getQueueSize();

    /**
     * Indicate if the event queue is empty
     */
    boolean isEmpty();
}
