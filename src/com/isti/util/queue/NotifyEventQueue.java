//NotifyEventQueue.java:  A FIFO event queue with notify-thread support.
//                        See the 'com.isti.util.test.NotifyEvtQueueTest'
//                        class for an example usage.
//
//   4/6/2004 -- [ET]  Created module by combining and extending OpenORB
//                     classes.
//  4/13/2004 -- [SH]  Added the following methods:
//                     waitForNotify()
//                     pushEventBackNoNotify()
//                     pushEventNoNotify()
//                     checkForEvent().
//  4/29/2004 -- [ET]  Fixed the 'waitForEvent()' method.
//  5/19/2004 -- [ET]  Added optional wait-time parameter to
//                     'waitForNotify()' method.
//  6/17/2005 -- [KF]  Added optional wait-time parameter to
//                     'waitForEvent()' and 'checkForEvent()' methods.
// 12/12/2006 -- [ET]  Improvement to comment for 'finishRunning()'.
//  6/13/2008 -- [ET]  Added 'clearEvents()' method.
//

package com.isti.util.queue;

import java.util.Vector;

/**
 * Class NotifyEventQueue is a FIFO event queue with notify-thread support.
 * Subclasses need to define a 'run()' method with a processing loop that
 * checks 'finishRunning()' and uses 'waitForEvent()' to retrieve events.
 * See the 'com.isti.util.test.NotifyEvtQueueTest' class for an example
 * usage.
 */
public abstract class NotifyEventQueue extends NotifyThread
{
  /**
   * Fifo event queue implemented with a vector.
   */
  protected Vector m_queue = new Vector();


  /**
   * Class NotifyEventQueue is a FIFO event queue with notify-thread
   * support.
   * @param idStr Identification string for this queue and its processing
   * thread.
   */
  public NotifyEventQueue(String idStr)
  {
    super(idStr);
  }

  /**
   * Pushes an event object into the queue.
   * @param event the event object to use.
   */
  public void pushEvent( Object event )
  {
    synchronized ( m_queue )
    {
      m_queue.addElement( event );          //add event-object to Vector
      m_queue.notifyAll();                  //notify waiting thread
    }
  }

  /**
    * Pushes an event object back into the queue at location 0
    * @param event the event object to use.
    */
   public void pushEventBackNoNotify( Object event )
   {
     synchronized ( m_queue )
     {
       m_queue.add(0, event );          //push event-object back to Vector
     }
   }

  /**
   * Pushes an event object into the queue but don't issue any notifications
   * @param event the event object to use.
   */
  public void pushEventNoNotify( Object event )
  {
    //    System.err.println("queue size is " + this.getQueueSize());
    synchronized ( m_queue )
    {
      m_queue.addElement( event );          //add event-object to Vector
    }
    //    System.err.println("queue size is now " + this.getQueueSize());
  }

  /**
   * Pulls an event from the queue.  If the queue is empty then this
   * method will return null.
   * @return the event object, or null if none are available.
   */
  public Object pullEvent()
  {
    synchronized ( m_queue )
    {
      if ( m_queue.size() == 0 )
      {
        return null;
      }
      return m_queue.remove( 0 );
    }
  }

  /**
   * Indicates if the event queue is empty.
   * @return the number of event objects in the queue.
   */
  public int getQueueSize()
  {
    return m_queue.size();
  }

  /**
   * Indicates if the event queue is empty.
   * @return true if the queue is empty; false if not.
   */
  public boolean isEmpty()
  {
    return m_queue.size() == 0;
  }

  /**
   * Notifies our event processing thread that the queue has events
   * to process.
   * Invokes notify() on the queue object.
   */
  public void notifyThread()
  {
    synchronized ( m_queue )
    {
      m_queue.notifyAll();
    }
  }

  /**
   * Waits for the queue to have events.
   * Invokes wait() on the queue object if running.
   *
   * @return  Event if available, null otherwise.
   */
  public Object waitForEvent()
  {
    return waitForEvent(0);
  }

  /**
   * Waits for the queue to have events.
   * Invokes wait() on the queue object if running.
   * @param waitTimeMs the maximum number of milliseconds to wait for
   * the thread-notify, or 0 to wait indefinitely.
   *
   * @return  Event if available, null otherwise.
   */
  public Object waitForEvent(long waitTimeMs)
  {
    return checkForEvent(waitTimeMs) ? pullEvent() : null;
  }

  /**
   * Waits for a thread-notify on the queue, up to the given number
   * of milliseconds.
   * Invokes wait() on the queue object if running.
   * @param waitTimeMs the maximum number of milliseconds to wait for
   * the thread-notify, or 0 to wait indefinitely.
   * @return The number of elements in the queue.
   */
  public int waitForNotify(long waitTimeMs)
  {
    // synchronize on the queue
    synchronized ( m_queue )
    {
      if ( isRunning() )
      {
        try
        {
          m_queue.wait(waitTimeMs);
        }
        catch ( final InterruptedException iex )
        {
            // ignore
        }
      }
      return m_queue.size();
    }
  }

  /**
   * Waits for a thread-notify on the queue.
   * Invokes wait() on the queue object if running.
   * @return The number of elements in the queue.
   */
  public int waitForNotify()
  {
    return waitForNotify(0);
  }

  /**
   * Waits for the queue to have events.
   * Invokes wait() on the queue object if running.
   * @return true if the queue is not empty, false if empty..
   */
  public boolean checkForEvent()
  {
    return checkForEvent(0);
  }

  /**
   * Waits for the queue to have events.
   * Invokes wait() on the queue object if running.
   * @param waitTimeMs the maximum number of milliseconds to wait for
   * the thread-notify, or 0 to wait indefinitely.
   * @return true if the queue is not empty, false if empty..
   */
  public boolean checkForEvent(long waitTimeMs)
  {
    // synchronize on the queue
    synchronized ( m_queue )
    {
      if ( m_queue.isEmpty() && isRunning() )
      {
        try
        {
          m_queue.wait(waitTimeMs);
        }
        catch ( final InterruptedException iex )
        {
            // ignore
        }
      }

      return !(isEmpty());
      //      return m_queue.isEmpty() ? null : pullEvent();
    }
  }


  /**
   * Indicates if processing thread should exit.
   * @return true if processing thread should exit; false if not.
   */
  public boolean finishRunning()
  {
    if ( shouldFinishWork() )
    {
      synchronized ( m_queue )
      {
        if ( m_queue.isEmpty() )
        {
          return true;
        }
      }
    }
    return !isRunning();
  }

  /**
   * Stops the thread.
   */
  public void stopThread()
  {
    super.stopThread();

    // wake the thread up if waiting for an event
    notifyThread();
  }

  /**
   * Finishes pending work and stops the thread.
   */
  public void finishWorkAndStopThread()
  {
    super.finishWorkAndStopThread();

    // wake the thread up if waiting for an event
    notifyThread();
  }

  /**
   * Clears all events from the queue.
   */
  public void clearEvents()
  {
    synchronized ( m_queue )
    {
      m_queue.clear();
    }
  }
}
