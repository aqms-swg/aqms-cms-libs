//NotifyThread.java:  Defines a thread with notify-support methods.
//
//   4/6/2004 -- [ET]  Module imported from OpenORB.
//  5/14/2004 -- [ET]  Made 'isRunning()' method public.
//  3/15/2005 -- [SH]  m_finishWork initialized to false by SBH on 3/15/05
// 12/12/2006 -- [ET]  Added 'setDaemonThread()' method.
//

/*
 * Copyright (C) The Community OpenORB Project. All rights reserved.
 *
 * This software is published under the terms of The OpenORB Community Software
 * License version 1.0, a copy of which has been included with this distribution
 * in the LICENSE.txt file.
 */

package com.isti.util.queue;

//import org.apache.avalon.framework.logger.Logger;

/**
 * Class NotifyThread defines a thread with notify-support methods.
 * Original author:  Shawn Boyce.
 */
public abstract class NotifyThread implements Runnable
{

    private Thread m_thread;
    private boolean m_daemonFlag = false;
    private final String m_id;
    protected final Object m_stateLock = new byte[0];

    /** Should this thread stop running. */
    private boolean m_isRunning = false;

    /** Should this thread finish pending work and stop running. */
    private boolean m_finishWork = false;

    /**
     * logger
     */
//    private final Logger m_logger;


    /**
     * Creates a thread with notify-support methods.
     * @param id name of thread.
     */
    public NotifyThread( final String id /* , final Logger logger */ )
    {
        m_id = id;
//        m_logger = logger.getChildLogger( id );
    }

    /**
     * Determines if this thread has been stopped.
     * @return false if 'stopThread()' has been called; true if not.
     */
    public boolean isRunning()
    {
        synchronized ( m_stateLock )
        {
            return m_isRunning;
        }
    }

    /**
     * Marks this thread as either a daemon thread or a user thread.  The
     * Java Virtual Machine exits when the only threads running are all
     * daemon threads.  This method must be called before the thread is
     * started (via the 'startThread()' method).
     * @param flgVal true for a daemon thread; false for user thread.
     */
    public void setDaemonThread( final boolean flgVal )
    {
        m_daemonFlag = flgVal;
    }

    /**
     * Sets the running status of the thread.
     * @param value true for running; false for stopped.
     */
    protected void setRunning( final boolean value )
    {
        synchronized ( m_stateLock )
        {
            m_isRunning = value;
        }
    }

    /**
     * Determines if 'finishWorkAndStopThread()' has been called.
     * @return true if 'finishWorkAndStopThread()' has been called;
     * false if not.
     */
    protected boolean shouldFinishWork()
    {
        synchronized ( m_stateLock )
        {
            return m_finishWork;
        }
    }

    /**
     * Starts the thread.
     */
    public void startThread()
    {
//        getLogger().debug( "Started." );

        setRunning( true );
        m_thread = new Thread( this, m_id );
        m_thread.setDaemon( m_daemonFlag );
        m_thread.start();
    }

    /**
     * Resumes the thread.
     */
    public void resumeThread()
    {
//        getLogger().debug( "Resumed." );
        notifyAll();
    }

    /**
     * Suspends the thread.
     */
    public void suspendThread()
    {
//        getLogger().debug( "Suspended." );
        try
        {
            wait();
        }
        catch ( final InterruptedException ex )
        {
            // ignore
        }
    }

    /**
     * Stops the thread.
     */
    public void stopThread()
    {
//        getLogger().debug( "Stopped." );
        m_thread = null;
        setRunning( false );
    }

    /**
     * Finish pending work and stop the thread
     */
    public void finishWorkAndStopThread()
    {
//        getLogger().debug( "Pending work finished and stopped." );

        synchronized ( m_stateLock )
        {
            m_finishWork = true;
        }
    }

//    public final Logger getLogger()
//    {
//        return m_logger;
//    }

    /**
     * Returns the thread name.
     * @return The thread name.
     */
    public final String toString()
    {
        return m_id;
    }
}
