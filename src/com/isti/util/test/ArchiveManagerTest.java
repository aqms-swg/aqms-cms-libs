package com.isti.util.test;

import java.io.IOException;
import java.util.Calendar;
import java.util.Date;
import java.util.Vector;

import com.isti.util.Archivable;
import com.isti.util.ArchivableData;
import com.isti.util.ArchiveManager;
import com.isti.util.ExtendedArchiveManager;
import com.isti.util.FlatDailyArchiveManager;
import com.isti.util.LogFile;
import com.isti.util.UtilFns;

/**
 * Test program for ArchiveManager classes.
 *  7/15/2003 -- [HS,ET]
 *  9/26/2003 -- [KF]  Add optional extended archive test.
 */
public class ArchiveManagerTest
{
  /**
   * Extended date-formatter pattern string.
   */
  public static final String EXT_DATE_FORMAT_STR = "yyyyMMdd_HHmm";

  private static final String archiveRootDirName = "archive";

  private final int LONG_TEST_MINUTES = 10;
  private final int MAX_TESTNUM = 60 * LONG_TEST_MINUTES;
  private ArchiveManager am = null;
  private final long timestamp;
  private int testNum = 1;

  public ArchiveManagerTest(boolean extendedFlag)
  {
    LogFile.initGlobalLogObj(
        archiveRootDirName+"/test.log", LogFile.ALL_MSGS, LogFile.NO_MSGS);

    final Calendar startCalObj = Calendar.getInstance();
    startCalObj.set(2003, 1, 1);
    final Calendar endCalObj = Calendar.getInstance();

    timestamp =
        endCalObj.getTime().getTime()/UtilFns.MS_PER_SECOND;

    try
    {
      am = new FlatDailyArchiveManager(
          ArchivableData.class,archiveRootDirName,"test.txt");

      am.setLogFile(LogFile.getGlobalLogObj());  //set the log file

      am.setLeaveOutputStreamsOpenFlag(false);  //close output streams

      am.setPurgeIntoArchiveFlag(true);         //set purge-into flag

      archiveItem(
          new ArchivableData("this is past current time",
          timestamp + 1));

      if (!extendedFlag)
      {
        archiveItem(
            new ArchivableData("this is a test",
            timestamp - 4));
        archiveItem(
            new ArchivableData("test 2",
            timestamp - 3));
        archiveItem(
            new ArchivableData("this is test 3",
            timestamp - 2));
        archiveItem(
            new ArchivableData("and finally, test 4",
            timestamp - 1));
        archiveOldItem();  //try an item that is too old
        archiveItem(new ArchivableData(null, timestamp));

        final Vector v = am.getArchivedItemsVector(
            startCalObj.getTime(), endCalObj.getTime());

        printElements(v);

        am.purgeArchive(new Date());  //purge the archive
      }
      else
      {
        if (am instanceof ExtendedArchiveManager)
        {
          ExtendedArchiveManager eam = (ExtendedArchiveManager)am;
          eam.setArchiveItemQueueEnabled(true);
          eam.setArchivePurgeAge(5 * UtilFns.MS_PER_MINUTE);
        }
        if (am instanceof FlatDailyArchiveManager)
        {
          FlatDailyArchiveManager fdam = (FlatDailyArchiveManager)am;
          fdam.setDateFormatObj(
            UtilFns.createDateFormatObj(
            EXT_DATE_FORMAT_STR, UtilFns.GMT_TIME_ZONE_OBJ));
        }

        Vector v;

        try
        {
          archiveItem();

          archiveOldItem();  //try an item that is too old

		  Thread.sleep(UtilFns.MS_PER_SECOND);

          for (int i = Integer.MAX_VALUE;
               ((v=am.getArchivedItemsVector(null, null)).size()) > 0;
               i++)
          {
            if (i >= 60)
            {
              i = 0;

              printElements(v);
            }

            archiveItem();

			Thread.sleep(UtilFns.MS_PER_SECOND);
          }
        }
        catch (Exception ex)
        {
          LogFile.getGlobalLogObj().error("Exception in archive loop:  " + ex);
        }
      }
    }
    catch (NoSuchMethodException ex)
    {
      LogFile.getGlobalLogObj().error("No such method (constructor)");
      ex.printStackTrace();
    }
    catch (IOException ex)
    {
      LogFile.getGlobalLogObj().error("IOException:  " + ex);
    }
    catch (Exception ex)
    {
      LogFile.getGlobalLogObj().error("Exception:  " + ex);
    }

    System.out.println("Test complete");

    if (am != null)       //if archive exists
      am.closeArchive();  //close the archive

    LogFile.getGlobalLogObj().close();  //close the log file
  }

  /**
   * Attempts to insert an item that is too old into the archive.
   * @throws IOException if the archive file can not be written to.
   * @return true if the item was added, false otherwise.
   */
  protected boolean archiveOldItem() throws IOException
  {
    return archiveItem(
        new ArchivableData("this is too old",
        timestamp - 4 * UtilFns.SECONDS_PER_MINUTE));
  }

  /**
   * Inserts an item into the archive.
   * @param item the item to be archived.
   * @throws IOException if the archive file can not be written to.
   * @return true if the item was added, false otherwise.
   */
  protected boolean archiveItem(Archivable item) throws IOException
  {
    return am.archiveItem(item);
  }

  /**
   * Inserts an item into the archive.
   * @throws IOException if the archive file can not be written to.
   * @return true if the item was added, false otherwise.
   */
  protected boolean archiveItem() throws IOException
  {
    if (testNum  > MAX_TESTNUM)
    {
      return false;
    }

    final long timestamp = System.currentTimeMillis()/UtilFns.MS_PER_SECOND;
    return archiveItem(new ArchivableData("test " + testNum++, timestamp));
  }

  /**
   * Gets the archive text.
   * @param m the archive data.
   * @return the archive text.
   */
  protected String archiveText(ArchivableData m)
  {
    return m.toString();
  }

  /**
   * Prints the elements.
   * @param v the vector of elements.
   */
  public void printElements(Vector v)
  {
    final int numItems = v.size();
    long currentTime = 0;
    long lastTime = 0;

    System.out.println("Number of items: " + numItems);

    for (int i = 0; i < numItems; i++)
    {
      ArchivableData m = (ArchivableData)(v.get(i));

      if ((currentTime = m.getArchiveTime()) < lastTime)
        LogFile.getGlobalLogObj().warning(
            "Invalid archive time: " + archiveText(m));

      System.out.println(archiveText(m));
      lastTime = currentTime;
    }
  }

  public static void main(String[] args)
  {
    boolean extendedFlag = false;
    new ArchiveManagerTest(extendedFlag);
  }
}