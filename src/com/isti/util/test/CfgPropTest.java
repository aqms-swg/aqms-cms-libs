//CfgPropTest.java:  Test program for 'CfgProperties' class.
//
//   7/12/2000 -- [ET]
//  10/19/2002 -- [ET]  Moved to 'test' package.
//
//
//=====================================================================
// Copyright (C) 2000 Instrumental Software Technologies, Inc.
//
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions
// are met:
// 1. Redistributions of source code, or portions of this source code,
//    must retain the above copyright notice, this list of conditions
//    and the following disclaimer.
// 2. Redistributions in binary form must reproduce the above copyright
//    notice, this list of conditions and the following disclaimer in
//    the documentation and/or other materials provided with the
//    distribution.
// 3. All advertising materials mentioning features or use of this
//    software must display the following acknowledgment:
//    "This product includes software developed by Instrumental
//    Software Technologies, Inc. (http://www.isti.com)"
// 4. If the software is provided with, or as part of a commercial
//    product, or is used in other commercial software products the
//    customer must be informed that "This product includes software
//    developed by Instrumental Software Technologies, Inc.
//    (http://www.isti.com)"
// 5. The names "Instrumental Software Technologies, Inc." and "ISTI"
//    must not be used to endorse or promote products derived from
//    this software without prior written permission. For written
//    permission, please contact "info@isti.com".
// 6. Products derived from this software may not be called "ISTI"
//    nor may "ISTI" appear in their names without prior written
//    permission of Instrumental Software Technologies, Inc.
// 7. Redistributions of any form whatsoever must retain the following
//    acknowledgment:
//    "This product includes software developed by Instrumental
//    Software Technologies, Inc. (http://www.isti.com/)."
// THIS SOFTWARE IS PROVIDED BY INSTRUMENTAL SOFTWARE
// TECHNOLOGIES, INC. "AS IS" AND ANY EXPRESSED OR IMPLIED
// WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES
// OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
// DISCLAIMED.  IN NO EVENT SHALL INSTRUMENTAL SOFTWARE TECHNOLOGIES,
// INC. OR ITS CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
// INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
// (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
// SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
// HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
// STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
// ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED
// OF THE POSSIBILITY OF SUCH DAMAGE.
//=====================================================================
//  A current version of the software can be found at
//                http://www.isti.com
//  Bug reports and comments should be directed to
//  Instrumental Software Technologies, Inc. at info@isti.com
//=====================================================================
//

package com.isti.util.test;

import java.io.*;
import java.util.*;
import com.isti.util.CfgProperties;
import com.isti.util.UtilFns;
import com.isti.util.CfgPropItem;
import com.isti.util.BooleanCp;

/**
 * Class CfgProperties:  Test program for 'CfgProperties' class.
 */
public class CfgPropTest
{

  public static void main(String[] args)
  {
         //create a table of properties with names & default values:
    CfgProperties cfgProps = new CfgProperties();
    CfgPropItem integerProp = cfgProps.add("IntegerProp",  //required item
                                                      Integer.valueOf(99),true);
    cfgProps.add("FloatProp",
                                                    Float.valueOf((float)98.0));
    CfgPropItem doubleProp = cfgProps.add("DoubleProp",
                                                  Double.valueOf((double)97.0));
    cfgProps.add("LongProp",Long.valueOf(96L));
    CfgPropItem shortProp = cfgProps.add("ShortProp",Short.valueOf((short)95));
    cfgProps.add("ByteProp",Byte.valueOf((byte)94));
    cfgProps.add("BooleanProp",Boolean.valueOf(true));
    cfgProps.add("BooleanCpProp",
                                                      new BooleanCp(false));
    CfgPropItem stringProp = cfgProps.add("StringProp",    //required item
                                              "Some \"string\" data.",true);
    CfgPropItem pdlFilterWarnProp = cfgProps.add("pdlFilterWarn", UtilFns.EMPTY_STRING);

         //load configuration file:
    FileInputStream inStm;
    try
    {
      inStm = new FileInputStream("CfgPropTest.ini");
      if(!cfgProps.load(inStm))
      {
        System.err.println("Error loading configuration file:");
        System.err.println(cfgProps.getErrorMessage());
      }
      inStm.close();
    }
    catch(Exception ex)
    {
      ex.printStackTrace(System.err);
      return;
    }

    System.out.println("Configuration properties items:");
    Enumeration e = cfgProps.elements();
    Object obj;
    CfgPropItem item;
    while(e.hasMoreElements())
    {
      if((obj=e.nextElement()) != null && obj instanceof CfgPropItem)
      {
        item = (CfgPropItem)obj;
        System.out.println(" " + (item.getLoadedFlag()?"1":"0") + " " +
                item.getName() + " = " + item.getValue() + "  [default = " +
                                              item.getDefaultValue() + "]");
      }
      else
        System.err.println("Error reading item element");
    }

         //use some of the properties items individually:
    System.out.println();
    System.out.println("integerProp = " + integerProp.intValue());
    System.out.println("doubleProp = " + doubleProp.doubleValue());
    System.out.println("stringProp = " + stringProp.toString());
    System.out.println("pdlFilterWarnProp = {");
    for (String s : pdlFilterWarnProp.stringArrayValue()) {
      System.out.print("\'");
      System.out.print(s);
      System.out.println("\'");
    }
    System.out.println("}");

         //change some of the properties items:
    if(!integerProp.setValue(0) ||
       !doubleProp.setValue((double)3.1415926536) ||
       !shortProp.setValue(Short.valueOf((short)1111)) ||
       !stringProp.setValue("new string value"))
    {
      System.err.println("Error setting properties item values");
    }

         //create new configuration file; only store items that were
         // loaded from file:
    try
    {
      FileOutputStream outStm = new FileOutputStream("CfgPropTest_new.ini");
      cfgProps.store(outStm,"CfgPropTest Initialization Test File",true);
      outStm.close();
    }
    catch(Exception ex)
    {
      ex.printStackTrace(System.err);
      return;
    }
  }
}

