//CircularBufferTest.java:  Test program for the 'CircularBuffer' class.
//
//  1/30/2006 - [ET]
//

package com.isti.util.test;

import com.isti.util.CircularBuffer;

/**
 * Class CircularBufferTest is a test program for the 'CircularBuffer'
 * class.
 */
public class CircularBufferTest
{
  public static void main(String [] args)
  {
    final CircularBuffer cBuffObj = new CircularBuffer(10);

    cBuffObj.add("one");
    cBuffObj.add("two");
    cBuffObj.add("three");
    cBuffObj.add("four");
    cBuffObj.add(Integer.valueOf(5));
    System.out.print("buff 1-5 objs: ");
    Object [] objArr = cBuffObj.getBufferItems();
    for(int i=0; i<objArr.length; ++i)
      System.out.print(" " + objArr[i]);
    System.out.println();
    System.out.print("buff 1-5 strs: ");
    String [] strArr = cBuffObj.getStrBufferItems();
    for(int i=0; i<strArr.length; ++i)
      System.out.print(" " + strArr[i]);
    System.out.println();

    cBuffObj.add("six");
    cBuffObj.add("seven");
    cBuffObj.add("eight");
    cBuffObj.add("nine");
    cBuffObj.add(Integer.valueOf(10));
    System.out.print("buff 1-10 objs: ");
    objArr = cBuffObj.getBufferItems();
    for(int i=0; i<objArr.length; ++i)
      System.out.print(" " + objArr[i]);
    System.out.println();
    System.out.print("buff 1-10 strs: ");
    strArr = cBuffObj.getStrBufferItems();
    for(int i=0; i<strArr.length; ++i)
      System.out.print(" " + strArr[i]);
    System.out.println();

    cBuffObj.add("eleven");
    cBuffObj.add(Integer.valueOf(12));
    System.out.print("buff 1-12 objs: ");
    objArr = cBuffObj.getBufferItems();
    for(int i=0; i<objArr.length; ++i)
      System.out.print(" " + objArr[i]);
    System.out.println();
    System.out.print("buff 1-12 strs: ");
    strArr = cBuffObj.getStrBufferItems();
    for(int i=0; i<strArr.length; ++i)
      System.out.print(" " + strArr[i]);
    System.out.println();

    for(int i=13; i<=18; ++i)
      cBuffObj.add(Integer.toString(i));
    System.out.print("buff 1-18 objs: ");
    objArr = cBuffObj.getBufferItems();
    for(int i=0; i<objArr.length; ++i)
      System.out.print(" " + objArr[i]);
    System.out.println();
    System.out.print("buff 1-18 strs: ");
    strArr = cBuffObj.getStrBufferItems();
    for(int i=0; i<strArr.length; ++i)
      System.out.print(" " + strArr[i]);
    System.out.println();

    for(int i=19; i<=99; ++i)
      cBuffObj.add(Integer.toString(i));
    System.out.print("buff 1-99 objs: ");
    objArr = cBuffObj.getBufferItems();
    for(int i=0; i<objArr.length; ++i)
      System.out.print(" " + objArr[i]);
    System.out.println();
    System.out.print("buff 1-99 strs: ");
    strArr = cBuffObj.getStrBufferItems();
    for(int i=0; i<strArr.length; ++i)
      System.out.print(" " + strArr[i]);
    System.out.println();

    System.out.println("buff as str:  " + cBuffObj.getBufferItemsAsString(","));

    System.out.println("Total items = " + cBuffObj.getTotalItemsCount());
  }
}
