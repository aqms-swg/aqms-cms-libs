//DownloadFileTest.java:  Test program for FileDownloader.
//
//   6/4/2004 -- [KF]  Initial version.
//  8/11/2004 -- [ET]  Changed 'DownloadFile' to 'FileDownloader'.
//  1/29/2009 -- [KF]  Modified to test new features.
//  3/25/2019 -- [KF]  Added support for downloading multiple files.
//

package com.isti.util.test;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.io.File;
import java.io.IOException;
import java.net.URL;
import java.util.List;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JProgressBar;

import com.isti.util.CallBackStringParam;
import com.isti.util.FileDownloader;
import com.isti.util.LogFile;
import com.isti.util.ProgressIndicatorInterface;
import com.isti.util.gui.ProgressDialog;
import com.isti.util.gui.SplashWindow;

public class DownloadFileTest extends FileDownloader implements CallBackStringParam
{
  private final static String upgradeRootDirName = "DownloadFileTest";

  private final static String dialogTitleStr = "Downloading";
  private final static String dialogMsgStr = "Checking data...";
  public static void main(String[] args)
  {
    try
    {
      LogFile.initGlobalLogObj(
          null, LogFile.NO_MSGS, LogFile.ALL_MSGS);
      File outDirObj = new File(upgradeRootDirName);
      final JFrame frame = new JFrame();
      frame.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
      frame.addWindowListener(new WindowAdapter()
      {
        /**
         * Invoked when the user attempts to close the window
         * from the window's system menu.  If the program does not
         * explicitly hide or dispose the window while processing
         * this event, the window close operation will be cancelled.
         * @param e window event
         */
        public void windowClosing(WindowEvent e)
        {
          //exit program when window frame is closed
          System.exit(0);
        }
      });
      final JPanel panel = new JPanel(new BorderLayout());
      frame.getContentPane().add(panel);
      final Dimension screenSizeDimObj = SplashWindow.getDisplayScreenSize();
      frame.setLocation(screenSizeDimObj.width/4, screenSizeDimObj.height/4);
      frame.setSize(screenSizeDimObj.width/2, screenSizeDimObj.height/2);
      frame.setVisible(true);
      final ProgressDialog downloadingPopupObj =
          new ProgressDialog(frame, dialogTitleStr, dialogMsgStr);
      final boolean unzipDownloadedFileFlag = true;
      final DownloadFileTest fd;
      if (args.length == 0)
      {
        String fileNameStr = "http://www.isti.com/libs/zips/JDOM_b9_rc1.zip";
        fd = new DownloadFileTest(
            fileNameStr, outDirObj, downloadingPopupObj,
            downloadingPopupObj.getProgressBarObj(), unzipDownloadedFileFlag);
      }
      else
      {
        final List<URL> urlList = createUrlList(args);
        fd = new DownloadFileTest(
            urlList, outDirObj, downloadingPopupObj,
            downloadingPopupObj.getProgressBarObj(), unzipDownloadedFileFlag);
        if (args.length > 1)
        {
          fd.setOutputFirstDirFlag(true);
        }
      }
      LogFile.getGlobalLogObj().debug(fd.getFileNameStr());
      fd.setBackupFile(true);
      fd.setCheckLastModified(true);
      fd.setDeleteFileAfterUnzip(false);
      fd.start();
    }
    catch (Exception ex)
    {
      ex.printStackTrace();
    }
  }

  private final ProgressDialog dialogObj;

  private final ProgressIndicatorInterface progressBarObj;

  public DownloadFileTest(List<URL> urlList, File outputDir,
      ProgressDialog dialogObj, ProgressIndicatorInterface progressBarObj,
      boolean unzipDownloadedFileFlag) throws Exception
  {
    super(urlList, outputDir, null, dialogObj, progressBarObj,
        unzipDownloadedFileFlag);
    this.dialogObj = dialogObj;
    this.progressBarObj = progressBarObj;
    setCallBackObj(this);
    if (progressBarObj instanceof JProgressBar)
    {
      ((JProgressBar) progressBarObj).setIndeterminate(true);
    }
  }

  public DownloadFileTest(String fileNameStr, File outputDir,
             ProgressDialog dialogObj,
             ProgressIndicatorInterface progressBarObj,
             boolean unzipDownloadedFileFlag) throws Exception
  {
    super(fileNameStr, outputDir, null, dialogObj,
          progressBarObj, unzipDownloadedFileFlag);
    this.dialogObj = dialogObj;
    this.progressBarObj = progressBarObj;
    setCallBackObj(this);
    if (progressBarObj instanceof JProgressBar)
    {
      ((JProgressBar)progressBarObj).setIndeterminate(true);
    }
  }

  /**
   * Method called by the implementing class.
   * @param str string parameter to be passed.
   */
  public void callBackMethod(String str)
  {
    LogFile.getGlobalLogObj().debug("DownloadFileTest - call back: " + str);
  }

  /**
   * Initialize the progress bar.
   */
  protected void initProgressBar()
  {
    super.initProgressBar();
    if (progressBarObj instanceof JProgressBar)
    {
      ((JProgressBar)progressBarObj).setIndeterminate(false);
    }
    dialogObj.setMessageStr("Downloading data...");
  }

  /**
   * Unzip the file.
   * @param outputFile the output file.
   * @return the ZIP file object or null if none.
   * @exception IOException if an I/O error has occurred.
   */
  protected Object unzipZipFile(File file, File outputDir) throws IOException
  {
    if (progressBarObj instanceof JProgressBar)
    {
      ((JProgressBar)progressBarObj).setIndeterminate(true);
    }
    dialogObj.setMessageStr("Unzipping data...");
    return super.unzipZipFile(file, outputDir);
  }
}
