//EntryFieldPanelTest.java:  Test/demo program for 'EntryFieldPanel' class.
//
//   4/21/2004 -- [ET]
//    9/8/2004 -- [KF]  Use 'SetupLookAndFeel' class.
//

package com.isti.util.test;

import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;

import com.isti.util.gui.EntryFieldPanel;
import com.isti.util.gui.SetupLookAndFeel;

/**
 * Class EntryFieldPanelTest is a test/demo program for the 'EntryFieldPanel'
 * class.
 */
public class EntryFieldPanelTest
{
  /**
   * Program entry point.
   * @param args string array of command-line parameters.
   */
  public static void main(String [] args)
  {
    //set "look & feel" to be that of host system:
    SetupLookAndFeel.setLookAndFeel();
    final JFrame mainFrame = new JFrame();       //create frame; set title
    mainFrame.setTitle("EntryFieldPanel Test");
    mainFrame.setSize(640,480);        //set frame size
    mainFrame.addWindowListener(       //setup to exit on window close
      new WindowAdapter()
      {  public void windowClosing(WindowEvent e)
         {
           System.exit(0);             //exit application
         }
      }
    );
                             //get screen size:
    final Dimension dimObj = Toolkit.getDefaultToolkit().getScreenSize();
                             //put frame at center of screen:
    mainFrame.setLocation((dimObj.width-mainFrame.getSize().width)/2,
                              (dimObj.height-mainFrame.getSize().height)/2);
    mainFrame.setVisible(true);        //make frame visible

    final String button1Str = "MultiLineJLabel";
    final JButton button1Obj = new JButton(button1Str);
    final JButton button2Obj = new JButton("Simple JLabel");
    final ActionListener listenerObj = new ActionListener()
        {     //setup button action to show entry-line panel:
          public void actionPerformed(ActionEvent evtObj)
          {
//            final String msgStr = "This is the place to enter some test text";
            final String msgStr = "This is the place to enter some test text" +
                              " This is the place to enter some test text" +
                              " This is the place to enter some test text" +
                               " This is the place to enter some test text";
                   //if first button then use the string (which will end up
                   // using a 'MultiLineJLabel'); else use a JLabel object:
            final Object msgObj =
                              button1Str.equals(evtObj.getActionCommand()) ?
                              (Object)msgStr : (Object)(new JLabel(msgStr));
            final EntryFieldPanel entryPanelObj = new EntryFieldPanel(
                                                             msgObj,"Text:",
                              "This is where text below\nthe field can go");
//            entryPanelObj.setEditFieldText("Initial contents");
            final String retStr = entryPanelObj.
                           showPanelInDialog(mainFrame,"Test Entry Dialog");
            System.out.println("Response:  " + retStr);
          }
        };
    button1Obj.addActionListener(listenerObj);
    button2Obj.addActionListener(listenerObj);
    final JPanel panelObj = new JPanel(new FlowLayout());
    panelObj.add(button1Obj);
    panelObj.add(button2Obj);
    mainFrame.getContentPane().add(panelObj);
    mainFrame.getContentPane().validate();
    button1Obj.doClick();

//    (new IstiDialogUtil(mainFrame)).popupOKConfirmMessage(
//            "Some test text some test text Some test text some test text " +
//            "Some test text some test text Some test text some test text " +
//            "Some test text some test text Some test text some test text " +
//            "Some test text some test text Some test text some test text " +
//              "Some test text some test text Some test text some test text",
//                                                "Test IstiDialogUtil",true);
  }
}
