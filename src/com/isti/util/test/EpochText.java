package com.isti.util.test;

import java.util.Date;
import java.text.DateFormat;
import java.util.TimeZone;
//import java.util.Calendar;
import com.isti.util.UtilFns;

/**
 * Class EpochText is a test program that takes a numeric epoch time
 * value (in seconds or milliseconds since 1/1/1970 GMT) and displays
 * the date/time in human-readable text.
 */
public class EpochText
{
  public static void main(String [] args)
  {
    final long MAX_SECS_VAL = 100000000000L;
    final DateFormat dateFormatterLocal =
            DateFormat.getDateTimeInstance(DateFormat.FULL,DateFormat.FULL);
    final DateFormat dateFormatterGMT =
            DateFormat.getDateTimeInstance(DateFormat.FULL,DateFormat.FULL);
    dateFormatterGMT.setTimeZone(TimeZone.getTimeZone("GMT"));

    final String argStr = (args.length > 0) ? args[0] : null;

    String str;
    long timeVal;
    Date dateObj;
    do
    {
      if(argStr == null)
      {
        System.out.print("Epoch> ");
        if((str=UtilFns.getUserConsoleString()) == null ||
            str.length() <= 0 || str.startsWith("Q") || str.startsWith("q"))
        {
          break;
        }
      }
      else
        str = argStr;
      try
      {
        if((timeVal=Long.parseLong(str)) <= MAX_SECS_VAL)
          timeVal *= 1000;        //if small enough then convert secs to ms
        dateObj = new Date(timeVal);
        System.out.println(dateFormatterLocal.format(dateObj));
        System.out.println(dateFormatterGMT.format(dateObj));
      }
      catch(Exception ex)
      {
        ex.printStackTrace();
      }
    }
    while(argStr == null);


//    Calendar calObj = Calendar.getInstance(TimeZone.getTimeZone("GMT"));
//    System.out.println("now (ms) = " + calObj.getTime().getTime() + " " + calObj);
//    calObj.set(3000,0,1,0,0,0);
//    calObj.set(Calendar.MILLISECOND,0);
//
//    System.out.println("3000 (ms) = " + calObj.getTime().getTime());
//    System.out.println("3000 (sec) = " + calObj.getTime().getTime()/1000);
  }
}
