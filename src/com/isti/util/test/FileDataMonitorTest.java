//FileDataMonitorTest.java:  Test program for FileDataMonitor and
//                           FileMonPollingThread.
//
//  3/18/2011 -- [ET]
//

package com.isti.util.test;

import java.util.Date;

import com.isti.util.FileMonPollingThread;
import com.isti.util.UtilFns;

public class FileDataMonitorTest
{
  private static FileMonPollingThread fileMonPollingThreadObj = null;

  public static void main(String [] args)
  {
    int timeOutMs = 5000;
    long maxReadBytesLimit = 65535;
    final String fNameStr = (args.length > 0) ? args[0] :
             "http://www.cisn.org/software/QWClient/ClientVersionsInfo.xml";
    if(args.length > 1)
    {
      timeOutMs = Integer.parseInt(args[1]);
      if(args.length > 2)
        maxReadBytesLimit = Long.parseLong(args[2]);
    }
    System.out.println("Monitoring file:  " + fNameStr);
    System.out.println("Enter 'Q' <Enter> to exit");

    fileMonPollingThreadObj = new FileMonPollingThread(
                             fNameStr,1000,timeOutMs,maxReadBytesLimit,true,
      new FileMonPollingThread.FileDataCallBack()
        {     //define call-back method invoked when file data changed:
          public void callBackMethod(boolean flagVal, byte [] byteArr)
          {
            if(flagVal)
            {
              if(byteArr != null)
              {
                System.out.println();
                System.out.println("File data for URL:  " +
                            fileMonPollingThreadObj.getFileDataMonitorObj().
                                                     getFileAccessUrlObj());
                String retStr = new String(byteArr);
                if(retStr.length() > 5000)
                {
                  retStr = "Successfully read " + retStr.length() +
                                                              " characters";
                }
                System.out.println(retStr);
                System.out.println("[Last Modified = " + new Date(
                            fileMonPollingThreadObj.getFileDataMonitorObj().
                                        getFileLastModifiedTimeMs()) + "]");
              }
              else
              {  //recovery after error indicated
                System.err.println("Successfully checked monitored file " +
                                                      "after access error");
              }
            }
            else
            {  //error reading file data; show error message
              System.err.println("FileDataMonitor error:  " +
                            fileMonPollingThreadObj.getFileDataMonitorObj().
                                                   getErrorMessageString());
            }
          }
        });
    fileMonPollingThreadObj.start();

    doInputLoop();
  }

  /** Loops until 'Q' is entered by user, then sets terminate flag. */
  private static void doInputLoop()
  {
    String str = null;
    while ( true )
    {
      if((str=UtilFns.getUserConsoleString()) != null)
      {  //user string fetched from console OK
        if(str.equalsIgnoreCase("Q"))
        {     //exit command entered by user
          fileMonPollingThreadObj.terminate();
          return;
        }
      }
      else
      {   //error fetching user string from console
        try         //delay a bit before trying again:
        { Thread.sleep(3000); }
        catch(InterruptedException ex) {}
      }
    }
  }


/*   old version:
public class FileDataMonitorTest
{
  private static boolean terminateFlag = false;
  private static final Object waitNotifyObject = new Object();

  public static void main(String [] args)
  {
    int timeOutMs = 5000;
    long maxReadBytesLimit = 65535;
    final String fNameStr = (args.length > 0) ? args[0] :
             "http://www.cisn.org/software/QWClient/ClientVersionsInfo.xml";
    if(args.length > 1)
    {
      timeOutMs = Integer.parseInt(args[1]);
      if(args.length > 2)
        maxReadBytesLimit = Long.parseLong(args[2]);
    }
    final FileDataMonitor fileDataMonitorObj =
                                              new FileDataMonitor(fNameStr);
    System.out.println("Monitoring file:  " + fNameStr);
    System.out.println("Enter 'Q' <Enter> to exit");
         //run input loop in new thread so user can enter 'Q' to exit:
    (new Thread()
        {
          public void run()
          {
            doInputLoop();
          }
        }).start();
    byte [] retArr;
    String retStr;
    while(!terminateFlag)
    {
      if((retArr=fileDataMonitorObj.checkReadFileData(
               timeOutMs,maxReadBytesLimit)) != FileDataMonitor.ERROR_ARRAY)
      {
        if(retArr != null)
        {
          System.out.println();
          System.out.println("File data for URL:  " +
                                  fileDataMonitorObj.getFileAccessUrlObj());
          retStr = new String(retArr);
          if(retStr.length() > 5000)
            retStr = "Successfully read " + retStr.length() + " characters";
          System.out.println(retStr);
          System.out.println("[Last Modified = " + new Date(
                     fileDataMonitorObj.getFileLastModifiedTimeMs()) + "]");
        }
      }
      else
      {
        System.err.println("FileDataMonitor error:  " +
                                fileDataMonitorObj.getErrorMessageString());
      }
      synchronized(waitNotifyObject)
      {
        try { waitNotifyObject.wait(1000); }
        catch(InterruptedException ex) {}
      }
    }
  }
*/
}
