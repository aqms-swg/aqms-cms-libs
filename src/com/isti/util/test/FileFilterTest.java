//FileFilterTest.java:  Tests the 'IstiFileFilter' class by using it
//                      to find specified files.
//
// 10/30/2006 -- [ET]
//

package com.isti.util.test;

import java.io.File;
import com.isti.util.IstiFileFilter;
import com.isti.util.FileUtils;

/**
 * Class FileFilterTest tests the 'IstiFileFilter' class by using it
 * to find specified files.
 */
public class FileFilterTest
{
  public static void main(String [] args)
  {
    final File fileObj = new File(".");
    final IstiFileFilter filterObj = new IstiFileFilter(new String[]
                                       { "File*", "*.txt", "*.bat" }, null);
    filterObj.setAcceptDirectories(true);
    final File [] fileArr = FileUtils.listAllFiles(fileObj,filterObj);
    for(int i=0; i<fileArr.length; ++i)
    {
      System.out.println(fileArr[i]);
    }
  }
}
