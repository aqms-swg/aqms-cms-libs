//FindDupLocs.java:  Finds duplicate locations in "LocationNoteData" files.
//
//   6/1/2004 -- [ET]
//

package com.isti.util.test;

import java.io.*;

import com.isti.util.gis.LocationDistance;

public class FindDupLocs
{
  protected static final char [] ACCENT_CHARS = LocationDistance.ACCENT_CHARS;
  protected static final char [] UNACCENT_CHARS = LocationDistance.UNACCENT_CHARS;
  protected static final int ACCENT_CHARS_LEN = ACCENT_CHARS.length;

  public FindDupLocs(String [] args)
  {
    try
    {
      final String fNameStr = (args.length > 0) ? args[0] :
                            "D:\\ISTI\\QuakeWatch\\local\\datafiles\\t.txt";
      final BufferedReader rdrObj =
                               new BufferedReader(new FileReader(fNameStr));
      final PrintWriter wtrObj =
             new PrintWriter(new BufferedWriter(new FileWriter("out.txt")));
      String orgStr, cmpStr, nameStr, prevNameStr;
      String prevOrgStr = "", prevCmpStr = "",
             outStr = null, nextOutStr = null;
      int p,len;
      boolean keepFirstFlag;
      while((orgStr=rdrObj.readLine()) != null)
      {
        p = len = orgStr.length();
        while(p > 0 && orgStr.charAt(p-1) <= ' ')
          --p;
        if(p < len)
          orgStr = orgStr.substring(0,p);
        cmpStr = orgStr;
        for(int i=0; i<ACCENT_CHARS_LEN; ++i)
          cmpStr = cmpStr.replace(ACCENT_CHARS[i],UNACCENT_CHARS[i]);
        if(cmpStr.length() > 20)
        {
          cmpStr = cmpStr.substring(19);
          if(cmpStr.equalsIgnoreCase(prevCmpStr))
          {
            if((p=prevCmpStr.indexOf(',')) < 0)
              p = prevCmpStr.length();
            prevNameStr = prevCmpStr.substring(0,p);
            if((p=cmpStr.indexOf(',')) < 0)
              p = cmpStr.length();
            nameStr = cmpStr.substring(0,p);
            if(isAllUpperCase(prevNameStr) && !isAllUpperCase(nameStr))
              keepFirstFlag = true;
            else if(isAllUpperCase(nameStr) && !isAllUpperCase(prevNameStr))
              keepFirstFlag = false;
            else
            {
              keepFirstFlag = true;
              for(int i=0; i<ACCENT_CHARS_LEN; ++i)
              {
                if(prevOrgStr.indexOf((int)(ACCENT_CHARS[i])) >= 0)
                {
                  keepFirstFlag = false;
                  break;
                }
              }
              if(!keepFirstFlag)
              {
                for(int i=0; i<ACCENT_CHARS_LEN; ++i)
                {
                  if(orgStr.indexOf((int)(ACCENT_CHARS[i])) >= 0)
                  {
                    keepFirstFlag = true;
                    break;
                  }
                }
              }
            }
            System.out.println();
            System.out.println((keepFirstFlag?"Keep:  ":"Drop:  ") +
                                                                prevOrgStr);
            System.out.println((keepFirstFlag?"Drop:  ":"Keep:  ") +
                                                                    orgStr);
            if(keepFirstFlag)
              nextOutStr = null;
            else
            {
              outStr = null;
              nextOutStr = orgStr;
            }
          }
          else
            nextOutStr = orgStr;
        }
        else
          nextOutStr = orgStr;
        prevOrgStr = orgStr;
        prevCmpStr = cmpStr;
        if(outStr != null)
          wtrObj.println(outStr);
        outStr = nextOutStr;
        nextOutStr = null;
      }
      if(outStr != null)
        wtrObj.println(outStr);
      wtrObj.close();
      rdrObj.close();
    }
    catch(Exception ex)
    {
      ex.printStackTrace();
    }
  }

  //Returns true if alphabetic characters in string are upper case.
  private static boolean isAllUpperCase(String str)
  {
    if(str == null)
      return false;
    final int len = str.length();
    char ch;
    for(int p=0; p<len; ++p)
    {    //for each character in string
      if(Character.isLetter(ch=str.charAt(p)) && !Character.isUpperCase(ch))
      {  //is letter and is not upper-case
        return false;
      }
    }
    return true;
  }

  public static void main(String [] args)
  {
    new FindDupLocs(args);
  }
}
