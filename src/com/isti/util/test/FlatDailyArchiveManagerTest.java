//FlatDailyArchiveManagerTest.java:  Test program for FlatDailyArchiveManager.
//
//  5/18/2004 -- [KF]
//

package com.isti.util.test;

import java.util.Calendar;
import java.util.TimeZone;

import com.isti.util.ArchivableData;
import com.isti.util.FlatDailyArchiveManager;
import com.isti.util.IstiThread;
import com.isti.util.LogFile;
import com.isti.util.UtilFns;

public class FlatDailyArchiveManagerTest
{
  private final static int numArchives = 1000;
  private final FlatDailyArchiveManager[] archives;
  private final static String archiveRootDirName = "archiveTest";

  /**
   * Class FlatDailyArchiveManagerTest test program for FlatDailyArchiveManager.
   */
  public FlatDailyArchiveManagerTest()
  {
    archives = new FlatDailyArchiveManager[numArchives];
    for (int archiveIndex = 0; archiveIndex < numArchives; archiveIndex++)
    {
      archives[archiveIndex] = null;
      new ArchiveWriteThread(archiveIndex).start();
    }
  }

  public void archiveItem(int archiveIndex, ArchivableData item)
  {
    if (archives[archiveIndex] == null)
    {
      try
      {
      archives[archiveIndex] = new FlatDailyArchiveManager(
          item.getClass(), archiveRootDirName, "test" + archiveIndex + ".txt");
      }
      catch (Exception ex)
      {
        LogFile.getGlobalLogObj().warning("Error creating archive: " + ex);
        LogFile.getGlobalLogObj().debug(UtilFns.getStackTraceString(ex));
        return;
      }
    }
    try
    {
      archives[archiveIndex].archiveItem(item);
    }
    catch (Exception ex)
    {
      LogFile.getGlobalLogObj().warning("Error archiving item: " + ex);
      LogFile.getGlobalLogObj().debug(UtilFns.getStackTraceString(ex));
    }
  }

  public class ArchiveWriteThread extends IstiThread
  {
    private final Calendar calObj =
        Calendar.getInstance(TimeZone.getTimeZone("GMT"));
    private int archiveCount = 0;
    private final int archiveIndex;
    private long timestamp;

    public ArchiveWriteThread(int archiveIndex)
    {
      super("ArchiveWriteThread" + archiveIndex);
      this.archiveIndex = archiveIndex;
      calObj.clear();
      calObj.set(2003, Calendar.APRIL, 30, 23, 50, 0);
      timestamp = calObj.getTime().getTime()/1000;
    }

    /**
     * Runs the worker thread.
     */
    public void run()
    {
      while (!isTerminated())  //loop while thread not terminated
      {
        try
        {     //wait until time to run again:
          sleep(100);
        }
        catch(InterruptedException ex)
        {
          LogFile.getGlobalLogObj().warning("Archive thread interrupted: " + ex);
          LogFile.getGlobalLogObj().debug(UtilFns.getStackTraceString(ex));
          return;
        }

        if (isTerminated())         //if terminated then
          return;                   //exit thread

        if (++archiveCount > 20)
          return;

        final ArchivableData item =
            new ArchivableData("test " + archiveCount,
            timestamp + archiveCount * UtilFns.SECONDS_PER_MINUTE);
        try
        {
          archiveItem(archiveIndex, item);
        }
        catch (Exception ex)
        {
          LogFile.getGlobalLogObj().warning("Error archiving item: " + ex);
          LogFile.getGlobalLogObj().debug(UtilFns.getStackTraceString(ex));
        }
      }
    }
  }

  public static void main(String[] args)
  {
    LogFile.initGlobalLogObj(
        archiveRootDirName + "/test.log", LogFile.ALL_MSGS, LogFile.ALL_MSGS);
    new FlatDailyArchiveManagerTest();
  }
}