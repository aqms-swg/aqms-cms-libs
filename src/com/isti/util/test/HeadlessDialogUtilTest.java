//HeadlessDialogUtilTest.java:  Test program for using 'IstiDialogUtil'
//                              with no parent component.
//
//  6/15/2011 -- [ET]
//

package com.isti.util.test;

import javax.swing.JOptionPane;
import com.isti.util.gui.SetupLookAndFeel;
import com.isti.util.gui.IstiDialogUtil;

/**
 * Class HeadlessDialogUtilTest is a test program for using 'IstiDialogUtil'
 * with no parent component.
 */
public class HeadlessDialogUtilTest
{
    //private constructor so that no object instances may be created
    // (static access only)
  private HeadlessDialogUtilTest()
  {
  }

  /**
   * Runs test program.
   * @param args command-line arguments.
   */
  public static void main(String[] args)
  {
    //set "look & feel" to be that of host system:
    SetupLookAndFeel.setLookAndFeel();

    final IstiDialogUtil dialogUtilObj = new IstiDialogUtil(null);
    final String titleStr = "HeadlessDialogUtilTest";
    final String msgStr = "Test data for dialog:\nClicking on the " +
            "'Settings' item in the 'Tools' menu brings up the 'Settings'" +
          " dialog window.  The window contains several 'tabbed' panels, " +
          "one of which may be selected at a time.  These tabs and their " +
          "items are described in the sections below.\n\nEnd of test data.";
    dialogUtilObj.popupMessage(msgStr,titleStr,
                                JOptionPane.INFORMATION_MESSAGE,false,true);
    System.exit(0);
  }
}
