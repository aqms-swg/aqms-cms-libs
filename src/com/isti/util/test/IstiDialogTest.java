//IstiDialogTest.java:  Test program for dialogs.
//
//  8/11/2004 -- [ET]  Initial version.
//   9/8/2004 -- [KF]  Use 'SetupLookAndFeel' class.
// 12/21/2004 -- [ET]  Removed reference to 'hide()' to avoid deprecation
//                     warning.
//  2/22/2005 -- [ET]  Added "timed..." checkbox items.
//

package com.isti.util.test;

import java.awt.*;
import java.awt.event.*;
import javax.swing.*;
import java.beans.PropertyChangeListener;
import java.beans.PropertyChangeEvent;
import com.isti.util.gui.IstiDialogPopup;
import com.isti.util.gui.SetupLookAndFeel;

/**
 * Class IstiDialogTest is a test program for dialogs.
 */
public class IstiDialogTest
{
  protected static final JFrame mainFrame = new JFrame("IstiDialogTest");
  protected static JCheckBox timedDisposeCheckObj;
  protected static JCheckBox timedHideCheckObj;
  protected static JCheckBox timedCloseCheckObj;

    //private constructor so that no object instances may be created
    // (static access only)
  private IstiDialogTest()
  {
  }

  public static void main(String[] args)
  {
    //set "look & feel" to be that of host system:
    SetupLookAndFeel.setLookAndFeel();

    timedDisposeCheckObj = new JCheckBox("Timed Dispose");
    timedHideCheckObj = new JCheckBox("Timed Hide");
    timedCloseCheckObj = new JCheckBox("Timed Close");

    mainFrame.setSize(640,480);        //set frame size
    mainFrame.addWindowListener(       //setup to exit on window close
      new WindowAdapter()
      {  public void windowClosing(WindowEvent e)
         {
           System.exit(0);             //exit application
         }
      }
    );

    final JButton modalButtonObj = new JButton("Modal");
    modalButtonObj.addActionListener(new ActionListener()
         {
           public void actionPerformed(ActionEvent evtObj)
           {
             showTestDialog(true);
           }
         });
    final JButton nonModalButtonObj = new JButton("Non-Modal");
    nonModalButtonObj.addActionListener(new ActionListener()
         {
           public void actionPerformed(ActionEvent evtObj)
           {
             showTestDialog(false);
           }
         });

                                       //get content pane for frame:
    final Container contentPane = mainFrame.getContentPane();
    contentPane.setLayout(new FlowLayout());               //set layout
    contentPane.add(modalButtonObj);
    contentPane.add(Box.createHorizontalStrut(10));
    contentPane.add(nonModalButtonObj);
    contentPane.add(Box.createHorizontalStrut(25));
    contentPane.add(timedDisposeCheckObj);
    contentPane.add(Box.createHorizontalStrut(5));
    contentPane.add(timedHideCheckObj);
    contentPane.add(Box.createHorizontalStrut(5));
    contentPane.add(timedCloseCheckObj);

                             //get screen size:
    Dimension d = Toolkit.getDefaultToolkit().getScreenSize();
                             //put frame at center of screen:
    mainFrame.setLocation((d.width-mainFrame.getSize().width)/2,
                                   (d.height-mainFrame.getSize().height)/2);
    mainFrame.setVisible(true);        //make frame visible
  }

  /**
   * Displays the test dialog.
   * @param modalFlag true for modal dialog, false for non-modal (modeless).
   */
  protected static void showTestDialog(boolean modalFlag)
  {
    System.out.println();
    final String titleStr = "Test Dialog";
    final JDialog dialogObj = new JDialog(mainFrame,titleStr,true)
        {
          public void dispose()
          {
            System.out.println("Dialog 'dispose()' method called");
            super.dispose();
          }
//          public void hide()
//          {
//            System.out.println("Dialog 'hide()' method called");
//            super.hide();
//          }
        };
    final IstiDialogPopup popupObj = new IstiDialogPopup(mainFrame,dialogObj,
          "This is a test dialog.  Hit 'Yes' to call 'dispose()' or 'No' " +
                                         "to just hide the dialog",titleStr,
                                       IstiDialogPopup.PLAIN_MESSAGE,null,0,
                             modalFlag,false,IstiDialogPopup.YES_NO_OPTION);
    popupObj.addWindowListener(new WindowListener()
        {
          public void windowOpened(WindowEvent e)
          {
            System.out.println("Dialog 'windowOpened()' called");
          }
          public void windowClosing(WindowEvent e)
          {
            System.out.println("Dialog 'windowClosing()' called");
          }
          public void windowClosed(WindowEvent e)
          {
            System.out.println("Dialog 'windowClosed()' called");
          }
          public void windowIconified(WindowEvent e)
          {
            System.out.println("Dialog 'windowIconified()' called");
          }
          public void windowDeiconified(WindowEvent e)
          {
            System.out.println("Dialog 'windowDeiconified()' called");
          }
          public void windowActivated(WindowEvent e)
          {
            System.out.println("Dialog 'windowActivated()' called");
          }
          public void windowDeactivated(WindowEvent e)
          {
            System.out.println("Dialog 'windowDeactivated()' called");
          }
        });
    popupObj.addComponentListener(new ComponentListener()
        {
          public void componentResized(ComponentEvent e)
          {
            System.out.println("Dialog 'componentResized()' called");
          }
          public void componentMoved(ComponentEvent e)
          {
            System.out.println("Dialog 'componentMoved()' called");
          }
          public void componentShown(ComponentEvent e)
          {
            System.out.println("Dialog 'componentShown()' called");
          }
          public void componentHidden(ComponentEvent e)
          {
            System.out.println("Dialog 'componentHidden()' called");
          }
        });
    popupObj.addPropertyChangeListener(new PropertyChangeListener()
        {
          public void propertyChange(PropertyChangeEvent evt)
          {
            System.out.println("Dialog propery change, name=\"" +
                 evt.getPropertyName() + "\", oldVal=" + evt.getOldValue() +
                                           ", newVal=" + evt.getNewValue());
          }
        });

    if(timedDisposeCheckObj.isSelected() || timedHideCheckObj.isSelected()
                                         || timedCloseCheckObj.isSelected())
    {
      (new Thread()
          {
            public void run()
            {
              try { sleep(5000); }
              catch(InterruptedException ex) {}
              SwingUtilities.invokeLater(new Runnable()
                  {
                    public void run()
                    {
                      if(timedDisposeCheckObj.isSelected())
                        popupObj.dispose();
                      if(timedHideCheckObj.isSelected())
                        popupObj.hide();
                      if(timedCloseCheckObj.isSelected())
                        popupObj.close();
                    }
                  });
            }
          }).start();
    }

    System.out.println("Calling dialog 'show()'");
    final int retVal = popupObj.showAndReturnIndex();
    System.out.println("Returned from dialog 'show()', retVal=" + retVal);
    if(retVal == IstiDialogPopup.YES_OPTION)
      popupObj.dispose();
  }
}
