//IstiEncryptionUtilsTest.java:  Test program for IstiEncryptionUtils.
//
//  8/30/2004 -- [KF]
//   4/4/2007 -- [KF]  Changed to work with OpenSSL generated private keys.
//

package com.isti.util.test;

import java.io.*;
import java.security.*;
import java.security.cert.*;
import com.isti.util.*;

public class IstiEncryptionUtilsTest
{
  public static void main(String[] args)
  {
    LogFile.initGlobalLogObj(LogFile.getConsoleLogObj());

    boolean errorFlag = false;

    //check directories
    // CA files may be found at:
    // http://www.isti.com/libs/zips/CA.zip
    final File caDir = new File("../CA");
    final File privateDir = new File(caDir, "private");
    final File crlDir = new File(caDir, "crl");
    if (!isDirectory(caDir))
    {
      errorFlag = true;
      System.exit(-1);
    }
    if (!isDirectory(privateDir))
    {
      errorFlag = true;
    }
    if (!isDirectory(crlDir))
    {
      errorFlag = true;
    }
    if (errorFlag)
    {
      System.exit(-2);
    }

    //check files
    final File certificateOfAuthorityFile = new File(caDir,"cacert.pem");
    final File privKeyFile = new File(privateDir,"server-key.pk8");
    final File certificateFile = new File(caDir, "server-cert.crt");
    final File crlFile = new File(crlDir, "ehpca.crl");

    if (!certificateOfAuthorityFile.exists())
    {
      errorFlag = true;
      System.err.println("CoA file does not exist: " + certificateOfAuthorityFile);
    }
    if (!privKeyFile.exists())
    {
      errorFlag = true;
      System.err.println("Private key file does not exist: " + privKeyFile);
    }
    if (!certificateFile.exists())
    {
      errorFlag = true;
      System.err.println("Certificate file does not exist: " + certificateFile);
    }
    if (!crlFile.exists())
    {
      errorFlag = true;
      System.err.println("CRL file does not exist: " + crlFile);
    }
    if (errorFlag)
    {
      System.exit(1);
    }

    final X509Certificate certificate =
        generateX509Certificate(certificateFile);
    final X509Certificate certificateOfAuthority =
        generateX509Certificate(certificateOfAuthorityFile);
    final X509CRL crl = IstiEncryptionUtils.generateX509CRL(crlFile);

    if (certificate == null || certificateOfAuthority == null || crl == null)
    {
      errorFlag = true;
    }
    if (errorFlag)
    {
      System.out.println("Could not read certificate files");
      System.exit(2);
    }

    if (!IstiEncryptionUtils.verifyX509Information(
        certificate, certificateOfAuthority, crl))
    {
      System.out.println("X.509 information is not valid");
      System.exit(3);
    }

    final PrivateKey privKey = IstiEncryptionUtils.generatePrivateKey(privKeyFile);
    if (privKey == null)
      System.out.println("Could not generate private key");

    final String msgText =
        "<DataMessage Action=\"Update\" TimeReceived=\"2004-08-20T00:30:37.903Z\" FeederMsgNum=\"231096\" SourceHost=\"qdds1.wr.usgs.gov\" SrcHostMsgID=\"309949\"><Product Type=\"LinkURL(Waveform_nc)\" MsgTypeCode=\"LI\" ProdTypeCode=\"Waveform_nc\" Value=\"http://quake.wr.usgs.gov/waveforms/wavesall/nc40160328.rsec0.html\" Comment=\"Waveforms\"><Identifier EventIDKey=\"nc40160328\" DataSource=\"NC\" Version=\"01\" /></Product></DataMessage>";
    final String sigText = IstiEncryptionUtils.generateSignatureText(
        msgText, privKey);

    final boolean validFlag = IstiEncryptionUtils.isValidSignatureText(
        msgText, sigText, certificate);

    if (!validFlag)
    {
      LogFile.getGlobalLogObj(true).warning(
          "Signature \"" + sigText + "\" is not valid.");
      System.exit(4);
    }
    else
    {
      LogFile.getGlobalLogObj(true).info(
          "Signature is valid.");
    }

    System.exit(0);
  }

  /**
   * Generates a X.509 certificate object and initializes it with
   * the data read from the file.
   *
   * <p>The given file must contain a single certificate.
   *
   * <p>The X.509 certificate provided in the file must be
   * DER-encoded and may be supplied in binary or printable (Base64) encoding.
   * If the certificate is provided in Base64 encoding, it must be bounded at
   * the beginning by -----BEGIN CERTIFICATE-----, and must be bounded at
   * the end by -----END CERTIFICATE-----.
   *
   * <p>Note that if the given input stream does not support
   * {@link java.io.InputStream#mark(int) mark} and
   * {@link java.io.InputStream#reset() reset}, this method will
   * consume the entire input stream.
   *
   * @param certificateFile the certificate file.
   *
   * @return a certificate object initialized with the data
   * from the file.
   *
   * @exception CertificateException on parsing errors.
   */
  public static X509Certificate generateX509Certificate(File certificateFile)
  {
    System.out.println("Generating a X.509 certificate: " + certificateFile);
    return IstiEncryptionUtils.generateX509Certificate(certificateFile);
  }

  /**
   * Test if the specified file is a directory.
   * @param fileObj the file.
   * @return true if the specified file is a directorym false otherwise.
   */
  public static boolean isDirectory(File fileObj)
  {
    if (fileObj.isDirectory())
      return true;
    if (fileObj.exists())
      System.err.println("File is not a directory: " + fileObj);
    else
      System.err.println("Directory does not exist: " + fileObj);
    return false;
  }
}
