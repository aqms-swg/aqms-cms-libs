//IstiSoundTest.java:  Test program for IstiSound.
//
// 12/22/2004 -- [ET]
//

package com.isti.util.test;

import java.io.ByteArrayInputStream;
import java.io.File;

import com.isti.util.FileUtils;
import com.isti.util.IstiSound;
import com.isti.util.UtilFns;

public class IstiSoundTest
{
  private IstiSound soundObj;

  /**
   * Class IstiSoundTest test program for IstiSound.
   */
  public IstiSoundTest()
  {
    final File file1Obj = new File("../QuakeWatch/QWClient/res/alarm.wav");
    if(!file1Obj.exists())
    {
      System.err.println("Unable to find test file \"" + file1Obj.getPath() +
                                                                      "\"");
      System.exit(1);
      return;
    }
    final File file2Obj = new File("D:/SOUNDS/WAV/HDoh1.WAV");
    if(!file2Obj.exists())
    {
      System.err.println("Unable to find test file \"" + file2Obj.getPath() +
                                                                      "\"");
      System.exit(1);
      return;
    }
    final ByteArrayInputStream btStm1,btStm2;
    try
    {
//      aInStm1 = AudioSystem.getAudioInputStream(file1Obj);
//      aInStm2 = AudioSystem.getAudioInputStream(file2Obj);
      btStm1 = new ByteArrayInputStream(
                   FileUtils.readMultiOpenFileToBuffer(file1Obj.getPath()));
      btStm2 = new ByteArrayInputStream(
                   FileUtils.readMultiOpenFileToBuffer(file2Obj.getPath()));
      soundObj = new IstiSound(btStm1);
    }
    catch(Exception ex)
    {
      System.err.println("Error creating sound object:  " + ex);
      ex.printStackTrace();
      System.exit(1);
      return;
    }
    while(true)
    {
      System.out.println("Hit <Enter> to play sound or 'Q' to quit");
      if(UtilFns.getUserConsoleString().equalsIgnoreCase("Q"))
      {
        System.exit(0);
        return;
      }
      soundObj.play(true);

      btStm1.reset();


//      try { Thread.sleep(2000); }
//      catch(InterruptedException ex) {}

      reopenAndPlay(btStm2);
      reopenAndPlay(btStm1);
      reopenAndPlay(btStm2);
      reopenAndPlay(btStm1);
      reopenAndPlay(btStm2);
      reopenAndPlay(btStm1);
    }

/*
    try
    {
      soundObj = new IstiSound(file1Obj);
    }
    catch(Exception ex)
    {
      System.err.println("Error creating sound object:  " + ex);
      ex.printStackTrace();
      System.exit(1);
      return;
    }
    while(true)
    {
      System.out.println("Hit <Enter> to play sound or 'Q' to quit");
      if(UtilFns.getUserConsoleString().equalsIgnoreCase("Q"))
      {
        System.exit(0);
        return;
      }
      soundObj.play(true);
//      try { Thread.sleep(2000); }
//      catch(InterruptedException ex) {}

      reopenAndPlay(file2Obj);
      reopenAndPlay(file1Obj);
      reopenAndPlay(file2Obj);
      reopenAndPlay(file1Obj);
      reopenAndPlay(file2Obj);
      reopenAndPlay(file1Obj);
    }

*/
  }


  private void reopenAndPlay(ByteArrayInputStream btStmObj)
  {
    try
    {
      soundObj.reopen(btStmObj);
    }
    catch(Exception ex)
    {
      System.err.println("Error reopening sound object:  " + ex);
      ex.printStackTrace();
      System.exit(1);
      return;
    }
    soundObj.play(true);
    try { Thread.sleep(50); }
    catch(InterruptedException ex) {}
    btStmObj.reset();
  }

/*
  private void reopenAndPlay(File fileObj)
  {
    try
    {
      soundObj.reopen(fileObj);
    }
    catch(Exception ex)
    {
      System.err.println("Error reopening sound object:  " + ex);
      ex.printStackTrace();
      System.exit(1);
      return;
    }
    soundObj.play(true);
//    try { Thread.sleep(50); }
//    catch(InterruptedException ex) {}
  }
*/

/*
  private void directPlaySoundFile(File fileObj)
                                       throws UnsupportedAudioFileException,
                                        IOException,LineUnavailableException
  {
    final AudioInputStream sourceObj = AudioSystem.getAudioInputStream(fileObj);
    final DataLine.Info info =
        new DataLine.Info(SourceDataLine.class, sourceObj.getFormat());
    final SourceDataLine lineObj = (SourceDataLine)AudioSystem.getLine(info);
    lineObj.open(sourceObj);
  }
*/

  public static void main(String[] args)
  {
     new IstiSoundTest();
  }
}
