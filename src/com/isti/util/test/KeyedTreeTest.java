//KeyedTreeTest.java:  Test program for 'KeyedTreeTable'.
//
//  5/28/2004 -- [ET]
//

package com.isti.util.test;

import com.isti.util.UtilFns;
import com.isti.util.KeyedTreeTable;

/**
 * Class KeyedTreeTest is a test program for 'KeyedTreeTable'.
 */
public class KeyedTreeTest
{
  protected final KeyedTreeTable keyedTreeTableObj =
                                                   new KeyedTreeTable(false);

  /**
   * Test program for 'KeyedTreeTable'.
   */
  public KeyedTreeTest(String [] args)
  {
    addToTable("firstKey","k1value1");
    addToTable("firstKey","k1value2");
    addToTable("firstKey","k1value3");
    addToTable("secondKey","k2value1");
    addToTable("secondKey","k2value2");
    addToTable("secondKey","k2value3");
    addToTable("thirdKey","k3value1");
    addToTable("thirdKey","k3value2");
    addToTable("thirdKey","k3value3");
    System.out.println("Initial table:");
    showTable();

    System.out.println();
    if(keyedTreeTableObj.addEntry("secondKey","k2value2"))
    {
      System.out.println("dup addToTable(\"secondKey\",\"k2value2\"):");
      showTable();
    }
    else
      System.out.println("dup addToTable(\"secondKey\",\"k2value2\") failed");

    System.out.println();
    if(keyedTreeTableObj.removeEntry("secondKey","k2value2"))
    {
      System.out.println("removeEntry(\"secondKey\",\"k2value2\"):");
      showTable();
    }
    else
      System.out.println("removeEntry() failed");

    System.out.println();
    if(keyedTreeTableObj.removeEntriesForKey("thirdKey"))
    {
      System.out.println("removeEntriesForKey(\"thirdKey\"):");
      showTable();
    }
    else
      System.out.println("removeEntriesForKey() failed");

    UtilFns.sleep(250);
    final long timeVal = System.currentTimeMillis();
    addToTable("fourthKey","k4value1");
    addToTable("fourthKey","k4value2");
    addToTable("fourthKey","k4value3");
    addToTable("fourthKey","k4value4");
    addToTable("fourthKey","k4value5");
    keyedTreeTableObj.removeOldEntries(timeVal,2);
    System.out.println();
    System.out.println("removeOldEntries(" + timeVal + ",2):");
    showTable();

    keyedTreeTableObj.removeOldEntries(timeVal,10);
    System.out.println();
    System.out.println("removeOldEntries(" + timeVal + ",10):");
    showTable();
  }

  /**
   * Adds an entry to the table.  Display an error message if unable
   * to add the entry.
   * @param keyObj the 'key' object to use.
   * @param valueObj the 'value' object to use.
   */
  protected void addToTable(Object keyObj, Object valueObj)
  {
    if(!keyedTreeTableObj.addEntry(keyObj,valueObj))
    {
      System.err.println("Unable to add entry, key=\"" + keyObj +
                                          "\", value=\"" + valueObj + "\"");
    }
  }

  /**
   * Dumps the table contents to the console.
   */
  protected void showTable()
  {
    System.out.println("Values:  " +
                                   keyedTreeTableObj.getValuesDisplayStr());
    System.out.println("Table:");
    System.out.println(keyedTreeTableObj.getTableDisplayStr());
  }

  /** Program entry point. */
  public static void main(String [] args)
  {
    new KeyedTreeTest(args);
  }
}
