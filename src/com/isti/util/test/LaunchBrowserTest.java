//LaunchBrowserTest.java:  Test program for 'LaunchBrowser' class.
//
//  3/30/2006 -- [ET]
//

package com.isti.util.test;

import com.isti.util.LaunchBrowser;

/**
 * Class LaunchBrowserTest is a test program for the 'LaunchBrowser' class.
 */
public class LaunchBrowserTest
{
  /**
   * Executing method for program.
   * @param args array of command-line parameters.
   */
  public static void main(String [] args)
  {
    System.out.println(
              "LaunchBrowserTest:  Test program for 'LaunchBrowser' class");
    if(args.length <= 0 || args[0].startsWith("-h") ||
                                                  args[0].startsWith("--h"))
    {
      System.out.println(
                       "  Usage:  LaunchBrowserTest URL [prefix] [suffix]");
      System.out.println("    where 'prefix' = launch command prefix");
      System.out.println("    where 'suffix' = launch command suffix");
      System.exit(0);
      return;
    }
    final String prefixStr = (args.length >= 2) ? args[1] : null;
    final String suffixStr = (args.length >= 3) ? args[2] : null;
    final LaunchBrowser launchBrowserObj = new LaunchBrowser(
                                                       prefixStr,suffixStr);
    System.out.println("OS detected:  " +
                                      launchBrowserObj.getOSSelectString());
    if(!launchBrowserObj.showURL(args[0]))
    {
      System.err.println("Error showing URL:  " +
                                        launchBrowserObj.getErrorMessage());
    }
    System.out.println("Command:  " +
                                   launchBrowserObj.getLastShowURLCmdStr());
  }
}
