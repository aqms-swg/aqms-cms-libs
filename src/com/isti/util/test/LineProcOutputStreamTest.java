package com.isti.util.test;

import java.io.IOException;
import java.io.PrintStream;
import com.isti.util.LineProcOutputStream;
import com.isti.util.UtilFns;

/**
 * Test program for 'LineProcOutputStream'.
 *   7/9/2003 -- [ET].
 */

public class LineProcOutputStreamTest extends LineProcOutputStream
{
  public LineProcOutputStreamTest()
  {
    final String testData = "12345\nLine with DOS line-end\r\nLine " +
                                "with Unix line-end\nLine with no line-end";
    for(int p=0; p<testData.length(); ++p)
    {
      System.out.println("Sending:  '" + UtilFns.insertQuoteChars(
            testData.substring(p,p+1),UtilFns.DEF_SPECIAL_CHARS_STR) + "'");
      write(testData.charAt(p));
    }
    System.out.println("Sending 'flush()'");
    flush();
    System.out.println("Sending 'flush()'");
    flush();


    final PrintStream printStmObj = new PrintStream(this);
    System.out.println("Sending \"testdata\" via 'print'");
    printStmObj.print(testData);
    System.out.println("Sending 'flush()'");
    flush();
    System.out.println("Sending 'flush()'");
    flush();

    System.out.println("Sending 'println'");
    printStmObj.println("Data via 'println'");
    System.out.println("Sending 'flush()'");
    flush();
  }

  public static void main(String[] args)
  {
    try
	{
	  new LineProcOutputStreamTest().close();
	}
	catch (IOException ex)
	{
	  ex.printStackTrace();
	}
  }

  public void callBackMethod(String str)
  {
    System.out.println("Received:  \"" +
        UtilFns.insertQuoteChars(str,UtilFns.DEF_SPECIAL_CHARS_STR) + "\"");
  }
}
