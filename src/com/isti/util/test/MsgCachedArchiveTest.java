//MsgCachedArchiveTest.java:  Test program for the IstiMessageCachedArchive
//                            class.
//
//   9/9/2003 -- [ET]  Initial version.
// 10/24/2003 -- [ET]  Changed reference from 'ArchivableMsgObjEntry' to
//                     'BasicArchivableMsgObjEntry'.
//

package com.isti.util.test;

import java.util.*;
import java.io.*;
import java.text.SimpleDateFormat;
import org.jdom.Element;
import org.jdom.Attribute;
import com.isti.util.UtilFns;
import com.isti.util.LogFile;
import com.isti.util.IstiXmlUtils;
import com.isti.util.Archivable;
import com.isti.util.FlatDailyArchiveManager;
import com.isti.util.IstiTimeObjectCache;
import com.isti.util.IstiMessageCachedArchive;

/**
 * Class MsgCachedArchiveTest is a test program for the
 * IstiMessageCachedArchive class.  If one or more filename parameters
 * are given then the program creates a new archive from the messages
 * in the files (removing duplicate messages and renumbering message
 * numbers as needed).  The 'testArchive' directory should be empty (or
 * non-existent) before the new archive is created.  If no parameters
 * are given then the program loads in and performs tests on an
 * existing archive in the 'testArchive' directory.
 */
public class MsgCachedArchiveTest
{
  private final static String ARCHIVE_DIRSTR = "testArchive";
  private final static String MSG_START_STR = "<" + MsgTag.QW_MESSAGE;
  private final LogFile logObj = new LogFile("MsgCachedArchiveTest.log",
                                  LogFile.DEBUG4,LogFile.INFO,false,true);
  IstiMessageCachedArchive msgCachedArchiveObj;
  PrintWriter wtrObj;

  public MsgCachedArchiveTest(String [] args)
  {
    if(args.length > 0)
      readFilesIntoArchive(args);
    else
      testExistingArchive();
    logObj.close();
  }

  public void readFilesIntoArchive(String [] args)
  {
    Arrays.sort(args);

//    final PrintWriter wtrObj;
//    try
//    {
//      wtrObj = new PrintWriter(new BufferedWriter(
//                            new FileWriter("MsgCachedArchiveTestOut.txt")));
//    }
//    catch(Exception ex)
//    {
//      System.err.println("Unable to open output file:  " + ex);
//      return;
//    }

    final FlatDailyArchiveManager archiveMgrObj;
    try
    {
      archiveMgrObj = new FlatDailyArchiveManager(QWServerMsgRecord.class,
                                             ARCHIVE_DIRSTR,"QWEvents.txt");
    }
    catch(Exception ex)
    {
      logObj.error("Unable to create archive manager:  " + ex);
      return;
    }

    msgCachedArchiveObj = new IstiMessageCachedArchive(archiveMgrObj,
                                            logObj,5000L,1000*24*60*60,0,0);

    String lastFileNameStr = "";
    BufferedReader rdrObj;
    String lineStr;
    int p,lineCount;
    QWServerMsgRecord recObj;
    long recObjMsgNum, updatedMsgNum = 0;
    Element elementObj;
    Attribute attrObj;
    for(int i=0; i<args.length; ++i)
    {
      if(lastFileNameStr.equals(args[i]))
      {
        logObj.info("Skipping repeated filename \"" + args[i] + "\"");
        continue;
      }
      lastFileNameStr = args[i];
      logObj.info("Processing file (" + (i+1) + ") \"" + args[i] + "\"");
      try
      {
        rdrObj = new BufferedReader(new FileReader(args[i]));
      }
      catch(Exception ex)
      {
        rdrObj = null;
        logObj.warning("Error opening input file:  " + ex);
      }
      if(rdrObj != null)
      {
        try
        {
          lineCount = 0;
          while((lineStr=rdrObj.readLine()) != null)
          {
            ++lineCount;
            if((p=lineStr.indexOf(MSG_START_STR)) >= 0)
            {
              try
              {
                recObj = new QWServerMsgRecord(lineStr.substring(p),
                                                   (Archivable.Marker)null);
                while(!msgCachedArchiveObj.isAddMessageQueueEmpty());
                if(!msgCachedArchiveObj.cacheContains(recObj))
                {
                  recObjMsgNum = recObj.getMsgNum();
                  if(updatedMsgNum <= 0 && recObjMsgNum > 0)
                    updatedMsgNum = recObjMsgNum;
                  else
                  {
                    if(++updatedMsgNum != recObjMsgNum)
                    {
                      try
                      {      //update message-number to keep them sequential:
                        elementObj = (Element)recObj.getDataObj();
                        if((attrObj=elementObj.getAttribute(
                                                MsgTag.MSG_NUMBER)) != null)
                        {
                          attrObj.setValue(Long.toString(updatedMsgNum));
                          recObj.updateArchivedForm();
                        }
                      }
                      catch(Exception ex)
                      {
                        logObj.warning(
                           "Error updating message number in record:  " + ex);
                      }
                    }
                  }
                  msgCachedArchiveObj.addArchivableMsgObj(recObj);
                }
                else
                {
                  logObj.debug2("MsgCachedArchiveTest:  Entry not added " +
                                "because matching msg-object-key already " +
                                  "in cache (msgNum=" + recObj.getMsgNum() +
                          ", msgDate=\"" + recObj.getArchiveDate() + "\")");
                  logObj.debug3("MsgCachedArchiveTest:  keyStr = \"" +
                                                 recObj.getKeyStr() + "\"");
                }
//                wtrObj.println(recObj.toArchivedForm());
//                wtrObj.println("  keyStr:  " + recObj.getDataStr());
              }
              catch(Exception ex)
              {
                logObj.warning("Error processing line " + lineCount +
                                                                ":  " + ex);
              }
            }
          }
        }
        catch(Exception ex)
        {
          logObj.warning("Error processing file:  " + ex);
        }
        try {rdrObj.close(); }
        catch(IOException ex) {}
      }
    }
    msgCachedArchiveObj.close();
  }


  public void testExistingArchive()
  {
    final FlatDailyArchiveManager archiveMgrObj;
    try
    {
      archiveMgrObj = new FlatDailyArchiveManager(QWServerMsgRecord.class,
                                             ARCHIVE_DIRSTR,"QWEvents.txt");
    }
    catch(Exception ex)
    {
      logObj.error("Unable to create archive manager:  " + ex);
      return;
    }

    try
    {
      wtrObj = new PrintWriter(new BufferedWriter(
                            new FileWriter("MsgCachedArchiveTestOut.txt")));
    }
    catch(Exception ex)
    {
      System.err.println("Unable to open output file:  " + ex);
      return;
    }

    final QWServerMsgRecord firstItemObj = (QWServerMsgRecord)
                             (archiveMgrObj.getOldestItemInEntireArchive());
    final QWServerMsgRecord lastItemObj = (QWServerMsgRecord)
                             (archiveMgrObj.getNewestItemInEntireArchive());
    logObj.info("First item in archive:  " + UtilFns.timeMillisToString(
                             firstItemObj.getArchiveDate().getTime(),true) +
                                           ", " + firstItemObj.getMsgNum());
    logObj.info(" Last item in archive:  " + UtilFns.timeMillisToString(
                              lastItemObj.getArchiveDate().getTime(),true) +
                                            ", " + lastItemObj.getMsgNum());

              //setup cache age to hold 3 day's worth of available data:
    final long cacheRemoveAge = System.currentTimeMillis() -
             lastItemObj.getArchiveDate().getTime() + 3L*UtilFns.MS_PER_DAY;
    logObj.info("Now minus cacheRemoveAge = " + UtilFns.timeMillisToString(
                           System.currentTimeMillis()-cacheRemoveAge,true));
    msgCachedArchiveObj = new IstiMessageCachedArchive(archiveMgrObj,
                               logObj,5000L,(int)(cacheRemoveAge/1000),0,0);

    logObj.info("Starting preload of cache from archive");
    msgCachedArchiveObj.preloadCacheFromArchive();
    logObj.info("Finished preload of cache from archive");
    logObj.info("Cache contents after preload:");
    showVecData(msgCachedArchiveObj.getMessageCacheObj().getAllObjects());

              //fetch 1 day's worth near beginning of archive data:
    final long inArchiveStartTime = firstItemObj.getArchiveDate().getTime()
                                                       + UtilFns.MS_PER_DAY;
    final long inArchiveEndTime = inArchiveStartTime + UtilFns.MS_PER_DAY;
    wtrObj.println();
    wtrObj.println("TEST:  1 day's worth near beginning of archive data:");
    logObj.info("TEST:  1 day's worth near beginning of archive data");
    fetchAndShowVecData(inArchiveStartTime,inArchiveEndTime);

              //fetch 1 day's worth near beg of archive data (maxCount=10):
    wtrObj.println();
    wtrObj.println(
            "TEST:  1 day's worth near beg of archive data (maxCount=10):");
    logObj.info(
             "TEST:  1 day's worth near beg of archive data (maxCount=10)");
    fetchAndShowVecData(inArchiveStartTime,0,inArchiveEndTime,0,10);

              //fetch 1 day's worth spanning cache and archive data:
    final long spanStartTime = lastItemObj.getArchiveDate().getTime() -
                                                    7L*UtilFns.MS_PER_DAY/2;
    final long spanEndTime = spanStartTime + UtilFns.MS_PER_DAY;
    wtrObj.println();
    wtrObj.println("TEST:  1 day's worth spanning cache and archive data:");
    logObj.info("TEST:  1 day's worth spanning cache and archive data");
    fetchAndShowVecData(spanStartTime,spanEndTime);

              //fetch 1 day's worth spanning cache and archive (maxCount=10):
    wtrObj.println();
    wtrObj.println(
          "TEST:  1 day's worth spanning cache and archive (maxCount=10):");
    logObj.info(
           "TEST:  1 day's worth spanning cache and archive (maxCount=10)");
    fetchAndShowVecData(spanStartTime,0,spanEndTime,0,10);

              //fetch 1 day's worth from middle of cache data:
    final long inCacheStartTime = lastItemObj.getArchiveDate().getTime() -
                                                      2L*UtilFns.MS_PER_DAY;
    final long inCacheEndTime = inCacheStartTime + UtilFns.MS_PER_DAY;
    wtrObj.println();
    wtrObj.println("TEST:  1 day's worth from middle of cache data:");
    logObj.info("TEST:  1 day's worth from middle of cache data");
    Vector vec = fetchAndShowVecData(inCacheStartTime,inCacheEndTime);

              //fetch 1 day's worth from cache using message numbers:
    final int vecSize;
    final Object obj1,obj2;
    if(vec != null && (vecSize=vec.size()) > 0 &&
         (obj1=vec.elementAt(0)) instanceof QWServerMsgRecord &&
               (obj2=vec.elementAt(vecSize-1)) instanceof QWServerMsgRecord)
    {
      final QWServerMsgRecord rec1Obj = (QWServerMsgRecord)obj1;
      final QWServerMsgRecord rec2Obj = (QWServerMsgRecord)obj2;
      wtrObj.println();
      wtrObj.println("TEST:  1 day's worth from cache using msgNums:");
      logObj.info("TEST:  1 day's worth from cache using msgNums");
      fetchAndShowVecData(rec1Obj.getTimeGenerated(),rec1Obj.getMsgNum(),
                          rec2Obj.getTimeGenerated(),rec2Obj.getMsgNum(),0);
      wtrObj.println();
      wtrObj.println(
            "TEST:  1 day's worth from cache using msgNums (maxCount=10):");
      logObj.info(
             "TEST:  1 day's worth from cache using msgNums (maxCount=10)");
      vec = fetchAndShowVecData(rec1Obj.getTimeGenerated(),
                             rec1Obj.getMsgNum(),rec2Obj.getTimeGenerated(),
                                                    rec2Obj.getMsgNum(),10);
      if(vec instanceof IstiTimeObjectCache.VectorWithCount)
      {
        logObj.info("  Requested count = " +
                     ((IstiTimeObjectCache.VectorWithCount)vec).getCount());
      }
      else
      {
        logObj.warning("  Vector returned by 'fetchAndShowVecData()' " +
                                           "not of type 'VectorWithCount'");
      }
    }
    else
      logObj.info("Tests via message numbers not performed; no data");

              //fetch 1 day's worth 12 hours before max-request-age limit:
    final long reqLimitStartTime = firstItemObj.getArchiveDate().getTime()
                                                       + UtilFns.MS_PER_DAY;
    final long maxRequestAgeTime = reqLimitStartTime + UtilFns.MS_PER_DAY/2;
    final long maxRequestAgeMS = System.currentTimeMillis() -
                                                          maxRequestAgeTime;
    wtrObj.println(
             "TEST:  1 day's worth 12 hours before max-request-age limit:");
    logObj.info("TEST:  1 day's worth 12 hours before max-request-age limit");
    logObj.info("maxRequestAgeTime = " + UtilFns.timeMillisToString(
                        maxRequestAgeTime,true) + ", maxRequestAgeSecs = " +
                                               (int)(maxRequestAgeMS/1000));
    msgCachedArchiveObj.setMaxRequestAgeSecs((int)(maxRequestAgeMS/1000));
    fetchAndShowVecData(inArchiveStartTime,inArchiveEndTime);

              //test removal of old data from cache:
    long newCacheRemoveAge;
    if((newCacheRemoveAge=cacheRemoveAge-UtilFns.MS_PER_DAY) <= 0)
      newCacheRemoveAge = 1000;
    msgCachedArchiveObj.setCacheRemoveAgeSecs((int)(newCacheRemoveAge/1000));
    logObj.info("Now minus newCacheRemoveAge = " +
                                                 UtilFns.timeMillisToString(
                        System.currentTimeMillis()-newCacheRemoveAge,true));
    msgCachedArchiveObj.removeOldObjects();
    logObj.info("Cache contents after 'removeOldObjects()':");
    showVecData(msgCachedArchiveObj.getMessageCacheObj().getAllObjects());

    wtrObj.close();
    msgCachedArchiveObj.close();
  }

  private Vector fetchAndShowVecData(long startTime,long beginMsgNum,
                                   long endTime,long endMsgNum,int maxCount)
  {
    try
    {
      final String msgStr = "Fetching from \"" +
                       UtilFns.timeMillisToString(startTime,true) + "\" (" +
                                                   beginMsgNum + ") to \"" +
                                  UtilFns.timeMillisToString(endTime,true) +
                                                   "\" (" + endMsgNum + ")";
      logObj.info(msgStr);
      wtrObj.println(msgStr);
      final Vector vec;
      final long sTimeVal = System.currentTimeMillis();
      showVecData(vec=msgCachedArchiveObj.getMessages(
                         startTime,beginMsgNum,endTime,endMsgNum,maxCount));
      final long eTimeVal = System.currentTimeMillis();
      logObj.info("  elasped time = " + (eTimeVal - sTimeVal) + " ms");
      if(vec.size() > 0)
      {
        long msgNum = ((QWServerMsgRecord)(vec.elementAt(0))).getMsgNum();
        QWServerMsgRecord recObj;
        final Iterator iterObj = vec.iterator();
        while(iterObj.hasNext())
        {
          wtrObj.println((recObj=
                     (QWServerMsgRecord)(iterObj.next())).toArchivedForm());
          if(recObj.getMsgNum() != msgNum)
          {
            logObj.warning("Non-sequential message number, expected=" +
                      msgNum + ", found=" + recObj.getMsgNum() + " (date=" +
                UtilFns.timeMillisToString(recObj.getTimeGenerated(),true) +
                                                                       ")");
            msgNum = recObj.getMsgNum();
          }
          ++msgNum;
        }
      }
      return vec;
    }
    catch(Exception ex)
    {
      logObj.warning("fetchAndShowVecData() error:  " + ex);
      return null;
    }
  }

  private Vector fetchAndShowVecData(long startTime,long endTime)
  {
    return fetchAndShowVecData(startTime,0,endTime,0,0);
  }

  private void showVecData(Vector vec)
  {
    try
    {
      final int vecSize = vec.size();
      logObj.info("Number of items:  " + vecSize);
      if(vecSize > 0)
      {
        QWServerMsgRecord recObj = (QWServerMsgRecord)(vec.elementAt(0));
        logObj.info("First item:  " + UtilFns.timeMillisToString(
                                   recObj.getArchiveDate().getTime(),true) +
                                                 ", " + recObj.getMsgNum());
        recObj = (QWServerMsgRecord)(vec.elementAt(vecSize-1));
        logObj.info(" Last item:  " + UtilFns.timeMillisToString(
                                   recObj.getArchiveDate().getTime(),true) +
                                                 ", " + recObj.getMsgNum());
      }
    }
    catch(Exception ex)
    {
      logObj.warning("showVecData() error:  " + ex);
    }
  }

  public static void main(String [] args)
  {
    new MsgCachedArchiveTest(args);
  }


  public static class QWServerMsgRecord extends
                         IstiMessageCachedArchive.BasicArchivableMsgObjEntry
  {
    final Element topElementObj;
    private /*final*/ String archiveFormStr;

    /**
     * Creates a message-record object.
     * @param timeGenerated the time the message was generated.
     * @param msgObj message object.
     * @param msgStr message string (used as a key into the cache-table).
     * @param msgNum message number.
     * @throws QWRecordException if an error occurs while creating
     * the data record.
     */
/*
    public QWServerMsgRecord(long timeGenerated,Object msgObj,
                         String msgStr,long msgNum) throws QWRecordException
    {
      super(timeGenerated,msgObj,msgStr,msgNum);
      if(!(msgObj instanceof Element))
      {  //given object not of type 'Element'; throw exception error
        throw new QWRecordException(
                          "Message object parameter not of type 'Element'");
      }
      topElementObj = (Element)msgObj;
                   //convert message Element object to string:
      archiveFormStr = elemToStr(topElementObj);
    }
*/

    /**
     * Creates a message-record object.
     * @param timeGenerated the time the message was generated.
     * @param qwMsgElement the 'QWmessage' element to use.
     * @param dataEventElement the 'DataMessage' element to used to generate
     * the key string into the cache.
     * @param msgNum message number.
     * @throws QWRecordException if an error occurs while creating
     * the data record.
     */
/*
    public QWServerMsgRecord(long timeGenerated,Element qwMsgElement,
              Element dataEventElement,long msgNum) throws QWRecordException
    {
      this.timeGenerated = timeGenerated;        //save time-generated value
      topElementObj = qwMsgElement;              //save 'QWmessage' element
                   //create cache-key from 'DataMessage' element:
      keyStr = dataElemToKeyStr(dataEventElement);
      this.msgNum = msgNum;                      //save message-number value
                   //convert message Element object to string:
      archiveFormStr = elemToStr(topElementObj);
    }
*/

    /**
     * Creates a record of data from the archived-string data for an XML
     * message.  This constructor is needed to have this class implement
     * the 'Archivable' interface.
     * @param archivedDataStr the archived-string data to use.
     * @param mkrObj Archivable.Marker parameter to indicate that this
     * constructor is used to dearchive a record.
     * @throws QWRecordException if an error occurs while creating
     * the data record.
     */
    public QWServerMsgRecord(String archivedDataStr,
                          Archivable.Marker mkrObj) throws QWRecordException
    {
      try
      {              //convert string to XML element:
        topElementObj = IstiXmlUtils.stringToElement(archivedDataStr);
      }
      catch(Exception ex)
      {              //if error then convert to 'QWRecordException':
        throw new QWRecordException(ex.toString());
      }
      dataObj = topElementObj;    //save element as data object
                   //convert message Element object back to a string:
      archiveFormStr = elemToStr(topElementObj);

              //setup data string that will be used as key into cache:
      Element elementObj;         //get "DataMessage" element:
                   //get "DataMessage" element; if found then convert to
                   // key string; otherwise use entire element:
      keyStr = ((elementObj=topElementObj.getChild(MsgTag.DATA_MESSAGE)) !=
                      null) ? dataElemToKeyStr(elementObj) : archiveFormStr;

              //get "TimeGenerated" attribute:
      String str;
      if((str=topElementObj.getAttributeValue(MsgTag.TIME_GENERATED)) ==
                                                                       null)
      {       //error fetching attribute; throw exception error
        throw new QWRecordException("Unable to find \"" +
                                    MsgTag.TIME_GENERATED + "\" attribute");
      }
      try
      {       //convert date string to 'Date' object and then time value:
        timeGenerated = QWUtils.xmlDateFormatterObj.parse(str).getTime();
      }
      catch(Exception ex)
      {       //error converting; throw exception error
        throw new QWRecordException("Error parsing \"" +
                          MsgTag.TIME_GENERATED + "\" attribute value (\"" +
                                                               str + "\")");
      }

              //get "MsgNumber" attribute:
      if((str=topElementObj.getAttributeValue(MsgTag.MSG_NUMBER)) == null)
      {       //error fetching attribute; throw exception error
        throw new QWRecordException("Unable to find \"" +
                                        MsgTag.MSG_NUMBER + "\" attribute");
      }
      try
      {       //convert string to long-integer value:
        msgNum = Long.parseLong(str);
      }
      catch(Exception ex)
      {       //error converting; throw exception error
        throw new QWRecordException("Error parsing \"" +
                MsgTag.MSG_NUMBER + "\" attribute value (\"" + str + "\")");
      }
    }

    /**
     * Returns the archivable representation for this object.  When the
     * object is recreated from the string, it should be identical to the
     * original.
     * @return A String representing the archived form of the object.
     */
    public String toArchivedForm()
    {
      return archiveFormStr;
    }

    /**
     * Compares the given 'QWServerMsgRecord' for "equality" using the
     * 'keyStr' field (if available).
     * @param obj the 'QWServerMsgRecord' object to compare.
     * @return true if the given 'QWServerMsgRecord' object is "equal"
     * to this object.
     */
    public boolean equals(Object obj)
    {
      if(obj instanceof QWServerMsgRecord)
      {
        if(keyStr == null)
          return super.equals(obj);
        return keyStr.equals(((QWServerMsgRecord)obj).keyStr);
      }
      return false;
    }

    /**
     * Updates the archivable representation for this object.
     * @throws QWRecordException if an error occurred.
     */
    public void updateArchivedForm() throws QWRecordException
    {
      archiveFormStr = elemToStr(topElementObj);
    }

    /**
     * Converts an Element object to a string.
     * @param elementObj the Element object to use.
     * @return A new converted string.
     * @throws QWRecordException if an error occurred.
     */
    public static String elemToStr(Element elementObj)
                                                    throws QWRecordException
    {
      try
      {            //convert Element object to a string and return it:
        return IstiXmlUtils.elementToString(elementObj);
      }
      catch(Exception ex)
      {            //if error then convert to 'QWRecordException':
        throw new QWRecordException(
                              "Error converting element to string:  " + ex);
      }
    }

    /**
     * Creates a key string that will be used as a key into the cache.
     * The string consists of the value of the "Action" attribute (if
     * found) and all child elements.
     * @param elementObj the 'DataMessage' element object to use.
     * @return The generated key string.
     * @throws QWRecordException if an error occurred.
     */
    public static String dataElemToKeyStr(Element elementObj)
                                                    throws QWRecordException
    {
      String str;
                   //if "Action" attribute found then use
                   // its value as a prefix string:
      if((str=elementObj.getAttributeValue(MsgTag.ACTION)) != null)
        str += ",";               //append comma separator
      else                   //if "Action" attribute not found then
        str = "";            //no prefix string
      final List listObj = elementObj.getChildren();
      if(listObj.size() > 0)
      {  //"DataMessage" element contains child elements
        final Iterator iterObj = listObj.iterator();
        Object obj;
        while(iterObj.hasNext())
        {     //for each child element of "DataMessage" element
          if((obj=iterObj.next()) instanceof Element)      //if type OK then
            str += elemToStr((Element)obj);      //add string form of element
          else
          {        //if unexpected type then throw exception error
            throw new QWRecordException("Non-element object type " +
                                      "returned by 'getChildren()' method");
          }
        }
      }
      else    //"DataMessage" element does not contain child elements
        str += elemToStr(elementObj);       //just add "DataMessage" element
      return str;
    }
  }
}

/**
 * QWRecordException defines an exception thrown while constructing
 * a QuakeWatch data record.
 */
class QWRecordException extends Exception
{
  /**
   * Creates an exception thrown while constructing a 'QWEventRecord'.
   * @param msgStr a descriptive message for the exception.
   */
  public QWRecordException(String msgStr)
  {
    super(msgStr);
  }
}


/**
 * Class MsgTag holds static Strings containing QuakeWatch XML message
 * tag names.
 */
class MsgTag
{
    /** Version number string for XML message format. */
  public static final String VERSION_NUMBER = "1.0";

    /** Value for "Yes" on flag attributes. */
  public static final String YES = "Y";
    /** Value for "No" on flag attributes. */
  public static final String NO = "N";

    /** Tag name for Value attribute on various elements. */
  public static final String VALUE = "Value";
    /** Tag name for MsgTypeCode attribute on various elements. */
  public static final String MSG_TYPE_CODE = "MsgTypeCode";
    /** Tag name for Type attribute on various elements. */
  public static final String TYPE = "Type";
    /** Tag name for NumStations attribute on various elements. */
  public static final String NUM_STATIONS = "NumStations";
    /** Tag name for Comment attribute on various elements. */
  public static final String COMMENT = "Comment";

    /** Value for "New" used in DataMessage.Action. */
  public static final String NEW = "New";
    /** Value for "Update" used in DataMessage.Action. */
  public static final String UPDATE = "Update";
    /** Value for "Delete" used in DataMessage.Action. */
  public static final String DELETE = "Delete";
    /** Value for "Trump" used in DataMessage.Action. */
  public static final String TRUMP = "Trump";
    /** Value for "Earthquake" used in Event.Type. */
  public static final String EARTHQUAKE = "Earthquake";
    /** Value for "TextComment" used in Product.Type. */
  public static final String TEXT_COMMENT = "TextComment";
  /** Value for "ShakeMapURL" used in Product.Type. */
  public static final String SHAKEMAP_URL = "ShakeMapURL";
  /** Value for "FocalMech1URL" used in Product.Type. */
  public static final String FOCAL_MECH1_URL = "FocalMech1URL";
  /** Value for "FocalMech2URL" used in Product.Type. */
  public static final String FOCAL_MECH2_URL = "FocalMech2URL";
  /** Value for "RealTimeMechURL" used in Product.Type. */
  public static final String REALTIME_MECH_URL = "RealTimeMechURL";
  /** Value for "MomentTensorURL" used in Product.Type. */
  public static final String MOMENT_TENSOR_URL = "MomentTensorURL";
  /** Value for "DidYouFeelIt" used in Product.Type. */
  public static final String DIDYOU_FEELIT = "DidYouFeelIt";
  /** Value for "LinkURL" used in Product.Type. */
  public static final String LINK_URL = "LinkURL";

                        //values for CUBIC message-type ID strings:
  public static final String EQUAKE_MSGSTR = "E ";    //Earthquake
  public static final String DELQK_MSGSTR = "DE";     //Delete Earthquake
  public static final String TRUMP_MSGSTR = "TR";     //Trump event
  public static final String TEXT_MSGSTR = "TX";      //Text Comment
  public static final String LINK_MSGSTR = "LI";      //Link to Add-on

    /** Tag name for the QWmessage element. */
  public static final String QW_MESSAGE = "QWmessage";
    /** Tag name for MsgNumber attribute of QWmessage element. */
  public static final String MSG_NUMBER = "MsgNumber";
    /** Tag name for MsgVersion attribute of QWmessage element. */
  public static final String MSG_VERSION = "MsgVersion";
    /** Tag name for ServerRevStr attribute of QWmessage element. */
  public static final String SERVER_REV_STR = "ServerRevStr";
    /** Tag name for ServerIDName attribute of QWmessage element. */
  public static final String SERVER_ID_NAME = "ServerIDName";
    /** Tag name for TimeGenerated attribute of QWmessage element. */
  public static final String TIME_GENERATED = "TimeGenerated";
    /** Tag name for MsgSource attribute of QWmessage element. */
  public static final String MSG_SOURCE = "MsgSource";
    /** Tag name for Signature attribute of QWmessage element. */
  public static final String SIGNATURE = "Signature";

    /** Tag name for the DataMessage element. */
  public static final String DATA_MESSAGE = "DataMessage";
    /** Tag name for Action attribute of DataMessage element. */
  public static final String ACTION = "Action";
    /** Tag name for TimeReceived attribute of DataMessage element. */
  public static final String TIME_RECEIVED = "TimeReceived";
    /** Tag name for FeederMsgNum attribute of DataMessage element. */
  public static final String FEEDER_MSG_NUM = "FeederMsgNum";
    /** Tag name for SrcHostMsgID attribute of DataMessage element. */
  public static final String SRC_HOST_MSG_ID = "SrcHostMsgID";
    /** Tag name for SourceHost attribute of DataMessage element. */
  public static final String SOURCE_HOST = "SourceHost";

    /** Tag name for the Identifier element. */
  public static final String IDENTIFIER = "Identifier";
    /** Tag name for EventIDKey attribute of Identifier element. */
  public static final String EVENT_ID_KEY = "EventIDKey";
    /** Tag name for DataSource attribute of Identifier element. */
  public static final String DATA_SOURCE = "DataSource";
    /** Tag name for Authoritative attribute of Identifier element. */
  public static final String AUTHORITATIVE = "Authoritative";
    /** Tag name for Version attribute of Identifier element. */
  public static final String VERSION = "Version";

    /** Tag name for the Magnitude element. */
  public static final String MAGNITUDE = "Magnitude";
    /** Tag name for NumReadings attribute of Magnitude element. */
  public static final String NUM_READINGS = "NumReadings";
    /** Tag name for MagMethod attribute of Magnitude element. */
  public static final String MAG_METHOD = "MagMethod";
    /** Tag name for MagError attribute of Magnitude element. */
  public static final String MAG_ERROR = "MagError";

    /** Tag name for the Event element. */
  public static final String EVENT = "Event";
    /** Tag name for Time attribute of Event element. */
  public static final String TIME = "Time";
    /** Tag name for Latitude attribute of Event element. */
  public static final String LATITUDE = "Latitude";
    /** Tag name for Longitude attribute of Event element. */
  public static final String LONGITUDE = "Longitude";
    /** Tag name for Depth attribute of Event element. */
  public static final String DEPTH = "Depth";
    /** Tag name for HorizontalError attribute of Event element. */
  public static final String HORIZONTAL_ERROR = "HorizontalError";
    /** Tag name for VerticalError attribute of Event element. */
  public static final String VERTICAL_ERROR = "VerticalError";
    /** Tag name for RMSTimeError attribute of Event element. */
  public static final String RMS_TIME_ERROR = "RMSTimeError";
    /** Tag name for AzimuthalGap attribute of Event element. */
  public static final String AZIMUTHAL_GAP = "AzimuthalGap";
    /** Tag name for LocationMethod attribute of Event element. */
  public static final String LOCATION_METHOD = "LocationMethod";
    /** Tag name for Verified attribute of Event element. */
  public static final String VERIFIED = "Verified";
    /** Tag name for NumPhases attribute of Event element. */
  public static final String NUM_PHASES = "NumPhases";
    /** Tag name for MinDistance attribute of Event element. */
  public static final String MIN_DISTANCE = "MinDistance";
    /** Tag name for Quarry attribute of Event element. */
  public static final String QUARRY = "Quarry";

    /** Tag name for the Product element. */
  public static final String PRODUCT = "Product";
    /** Tag name for ProdTypeCode attribute of Product element. */
  public static final String PROD_TYPE_CODE = "ProdTypeCode";

    /** Tag name for the StatusMessage element. */
  public static final String STATUS_MESSAGE = "StatusMessage";
    /** Tag name for MsgType attribute of StatusMessage element. */
  public static final String MSG_TYPE = "MsgType";
    /** Tag name for MsgData attribute of StatusMessage element. */
  public static final String MSG_DATA = "MsgData";
    /** Type value for MsgType attribute of StatusMessage element. */
  public static final String STARTUP = "Startup";
    /** Type value for MsgType attribute of StatusMessage element. */
  public static final String ALIVE = "Alive";
    /** Type value for MsgType attribute of StatusMessage element. */
  public static final String STATUS = "Status";
    /** Type value for MsgType attribute of StatusMessage element. */
  public static final String TERMINATING = "Terminating";


    /** Tag name for the QWresend element. */
  public static final String QW_RESEND = "QWresend";
    /** Tag name for NumRequested attribute of QWresend element. */
  public static final String NUM_REQUESTED = "NumRequested";
}


/**
 * Class QWUtils contains a collection of static utility fields and methods
 * for QuakeWatch.
 */
class QWUtils
{
    /** Date format object for parsing and formatting date/time strings. */
  public static final SimpleDateFormat xmlDateFormatterObj =
                       new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'");
    /** Timezone object set to "GMT". */
  public static final TimeZone gmtTimeZone = TimeZone.getTimeZone("GMT");

  static      //static initialization block; executes only once
  {                     //set time zone for data formatters to GMT:
    xmlDateFormatterObj.setTimeZone(gmtTimeZone);
  }
}
