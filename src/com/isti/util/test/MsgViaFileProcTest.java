//MsgViaFileProcTest.java:  Test program for the 'MsgViaFileProcessor'
//                          class.
// 11/19/2003 -- [ET]
//

package com.isti.util.test;

import java.io.IOException;
import com.isti.util.UtilFns;
import com.isti.util.MsgViaFileProcessor;

/**
 * Class MsgViaFileProcTest.java is a test program for the
 * 'MsgViaFileProcessor' class.
 */
public class MsgViaFileProcTest
{
  private static int messageNumber = 0;

  public static void main(String[] args) throws IOException
  {
    System.out.println("MsgViaFileProcTest Program");
    final MsgViaFileProcessor.ViaFileProperties viaFilePropObj =
                                new MsgViaFileProcessor.ViaFileProperties();
    viaFilePropObj.inputPollDelayMSProp.setValue(1000);
//    viaFilePropObj.storageAgeSecsProp.setValue(
//                                      (int)(10*UtilFns.SECONDS_PER_MINUTE));
    viaFilePropObj.storageAgeSecsProp.setValue(10);
    final MsgViaFileProcessor msgViaFileProcObj = new MsgViaFileProcessor(
                    viaFilePropObj,new MsgViaFileProcessor.ProcMsgCallBack()
        {
          public String processMsg(String fileNameStr,String msgStr)
          {
            if(fileNameStr != null)
            {
              ++messageNumber;
              System.out.print("Received (file=\"" + fileNameStr +
                           "\", msgNum=" + messageNumber + "):  " + msgStr);
              if(!msgStr.endsWith(UtilFns.newline))
                System.out.println();
              return Integer.toString(messageNumber);
            }
            System.err.println("Error:  " + msgStr);
            return null;
          }
        });
    System.out.println("Starting processing...");
    msgViaFileProcObj.startProcessing();
  }
}
