//NotifyEvtQueueTest.java:  Test program to demonstrate 'NotifyEventQueue'
//                          usage.
//
//   4/6/2004 -- [ET]
//

package com.isti.util.test;

import com.isti.util.queue.NotifyEventQueue;
import com.isti.util.UtilFns;

public class NotifyEvtQueueTest
{
  /** Queue object. */
  protected final TestQueue eventQueueObj = new TestQueue();
  /** Flag set true to terminate main thread. */
  protected boolean terminateFlag = false;


  /**
   * Creates the test program.
   */
  public NotifyEvtQueueTest()
  {
    System.out.println(
                   "Test Program for NotifyEventQueue, enter 'Q' to quit:");
    eventQueueObj.startThread();       //start the queue-processing thread

      //create new thread to process user input:
    (new Thread()
        {
          public void run()
          {
            String str;
            while(true)
            {
              if((str=UtilFns.getUserConsoleString()) != null)
              {   //user string fetched from console OK
                if(str.equalsIgnoreCase("Q"))
                  break;         //if keyword then exit loop
              }
              else
              {   //error fetching user string from console
                            //delay a bit before trying again:
                try { Thread.sleep(3000); }
                catch(InterruptedException ex) {}
              }
            }
            System.out.println("Terminating threads");
                                       //terminate event-queue thread:
            eventQueueObj.finishWorkAndStopThread();
            terminateFlag = true;      //set flag to terminate main thread
            synchronized(NotifyEvtQueueTest.this)
            {
              NotifyEvtQueueTest.this.notifyAll();
            }
          }
        }).start();

         //run loop to push events out to the queue:
    int eventCount = 0;
    System.out.println("Main loop starting");
    while(!terminateFlag)
    {
      synchronized(this)
      {
        try { wait(1000); }
        catch(InterruptedException ex) {}
      }
      if(terminateFlag)
        break;

      eventQueueObj.pushEvent("Event #" + (++eventCount));
    }
    System.out.println("Main loop exiting");
  }

  /**
   * Program entry point.
   * @param args string array of command-line arguments.
   */
  public static void main(String [] args)
  {
    new NotifyEvtQueueTest();
  }


  /**
   * Class TestQueue implements the queue operations.
   */
  private class TestQueue extends NotifyEventQueue
  {
    /**
     * Creates the queue.
     */
    public TestQueue()
    {
      super("TestQueue");
    }

    /**
     * Executing method for queue.
     */
    public void run()
    {
      System.out.println("Event queue processing loop starting");
      Object obj;
      while (!finishRunning())
      {
        if((obj=waitForEvent()) != null)
          System.out.println("Received event:  " + obj);
      }
      System.out.println("Event queue processing loop exiting");
    }
  }
}
