//ParseNumberTest.java:  Test for parse-number method in UtilFns.
//
//  2/16/2011 -- [ET]
//

package com.isti.util.test;

import com.isti.util.UtilFns;

/**
 * Class ParseNumberTest is a test for the parse-number method in UtilFns.
 */
public class ParseNumberTest
{
  public static void main(String[] args)
  {
    Object obj;
    String str;
    while(true)
    {
      System.out.print("ParseNumber> ");
      if((str=UtilFns.getUserConsoleString()) == null ||
            str.length() <= 0 || str.startsWith("Q") || str.startsWith("q"))
      {
        break;
      }
      try
      {
        if((obj=UtilFns.parseNumber(str)) != null)
        {
          System.out.println("  parsed:  " + obj + ", class:  " +
                                                            obj.getClass());
        }
        else
          System.out.println("  parseNumber() returned null");
      }
      catch(Exception ex)
      {
        ex.printStackTrace();
        UtilFns.sleep(1000);
      }
    }
  }
}
