//ParseStringsTest.java:  Test for parse-strings method in UtilFns.
//
//  9/3/2010 -- [ET]
//

package com.isti.util.test;

import java.io.IOException;
import com.isti.util.UtilFns;
import com.isti.util.ProcessUtilFns;

/**
 * Class ParseStringsTest is a test for parse-strings method in UtilFns.
 */
public class ParseStringsTest
{
  public static void main(String [] args)
                                    throws IOException, InterruptedException
  {
    Thread.sleep(100);
    if(args.length > 0)
    {
      System.out.println();
      System.out.println("args array:");
      for(int i=0; i<args.length; ++i)
        System.out.println("  [" + i + "]:  " + args[i]);
      return;
    }

    String testStr = "java -cp classes;jars/isti.util.jar " +
                                    "com.isti.util.test.ParseStringsTest " +
                                   "javaCmdStr -Xmx64m -jar QWClient.jar " +
         "-instBackupDir=\"C:\\Documents and Settings\\et\\.cisndisplay\\" +
                                                 "client2974554636\\1.52\"";
    System.out.println();
    System.out.println("Input string:  " + testStr);
    String [] strArr = UtilFns.parseSeparatedSubstrings(testStr,
                                                       " ",'\"',true,false);
    System.out.println("Output array:");
    for(int i=0; i<strArr.length; ++i)
      System.out.println("  [" + i + "]:  " + strArr[i]);

    ProcessUtilFns.exec(strArr,System.err,System.out);
    Thread.sleep(3000);

    testStr = "java -cp classes;jars/isti.util.jar " +
                                    "com.isti.util.test.ParseStringsTest " +
                                   "javaCmdStr -Xmx64m -jar QWClient.jar " +
         "\"-instBackupDir=C:\\Documents and Settings\\et\\.cisndisplay\\" +
                                                 "client2974554636\\1.52\"";
    System.out.println();
    System.out.println("Input string:  " + testStr);
    strArr = UtilFns.parseSeparatedSubstrings(testStr," ",'\"',true,true);
    System.out.println("Output array:");
    for(int i=0; i<strArr.length; ++i)
      System.out.println("  [" + i + "]:  " + strArr[i]);

    ProcessUtilFns.exec(strArr,System.err,System.out);
    Thread.sleep(1000);
  }
}
