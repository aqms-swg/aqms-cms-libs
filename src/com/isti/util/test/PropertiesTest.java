package com.isti.util.test;

import java.io.File;
import java.io.FileInputStream;
import java.io.InputStream;
import java.util.Properties;

import com.isti.util.UtilFns;

public class PropertiesTest {
  public static void main(String[] args) {
    File propFile = new File("test.properties");
    try {
      Properties props1 = new Properties();
      Properties props2 = new Properties();
      try (InputStream is = new FileInputStream(propFile)) {
        props1.load(is);
      }
      UtilFns.load(props2, propFile, null);

      String key = "windows_directory_back";
      System.out.println(props2.get(key));
      props1.remove(key);
      props2.remove(key);
      
      System.out.println(UtilFns.equals(props1, props2));
    } catch (Exception ex) {
      ex.printStackTrace();
    }
  }
}
