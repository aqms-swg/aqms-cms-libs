package com.isti.util.test;

import java.net.InetAddress;

import com.isti.util.UtilFns;

public class PublicIPTest
{
  public static String getHostAddress(InetAddress inetAddress)
  {
    if (inetAddress == null)
    {
      return null;
    }
    return inetAddress.getHostAddress();
  }
  
  public static void main(String[] args)
  {
    InetAddress inetAddress;
    if (args.length == 0)
    {
      inetAddress = UtilFns.getPublicIP(); 
    }
    else
    {
      if (args.length == 1 && args[0].equalsIgnoreCase("ALL"))
      {
        for (String s : UtilFns.PUBLIC_IP_HOSTS)
        {
          System.out.printf("%s=%s\n", s, getHostAddress(UtilFns.getPublicIP(s)));
        }
        return;
      }
      inetAddress = UtilFns.getPublicIP(args);
    }
    if (inetAddress == null)
    {
      System.err.println("Could not determine the Public IP address");
      System.exit(1);
    }
    System.out.println(getHostAddress(inetAddress));
  }
}
