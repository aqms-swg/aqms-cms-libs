//ReadFileTailTest.java:  Test program for 'FileUtils.readFileTail()'
//                        method.
//
//  1/30/2006 - [ET]
//

package com.isti.util.test;

import com.isti.util.FileUtils;


/**
 * Class ReadFileTailTest is a test program for the
 * 'FileUtils.readFileTail()' method.
 */
public class ReadFileTailTest
{
  public static void main(String [] args)
  {
    try
    {
      FileUtils.readFileTail(args[1],Long.parseLong(args[0]),System.out);
    }
    catch(Exception ex)
    {
      ex.printStackTrace();
    }
  }
}
