
package com.isti.util.test;

import java.net.InetAddress;
import com.isti.util.UtilFns;

public class ShowIPAddr
{
  public static void main(String [] args)
  {
    String localHostStr="";       //set ID string to default value
    try
    {         //get name/address of local machine:
      InetAddress localHostAddr = InetAddress.getLocalHost();
      System.out.println("Local host address:  " + localHostAddr);
      System.out.println("  hostName=\"" + localHostAddr.getHostName() +
                     "\", hostAddress=\"" + localHostAddr.getHostAddress() +
                                                        "\", getByName=\"" +
        InetAddress.getByName(localHostAddr.getHostName()).getHostName() +
                                                                      "\"");

      System.out.println("  hostNameViaAddr=\"" +
                     InetAddress.getByName(localHostAddr.getHostAddress()) +
                                             "\", hostNameViaLocalhost=\"" +
                   InetAddress.getByName(InetAddress.getByName("localhost").
                                                  getHostAddress()) + "\"");

              //enter numeric address of host (for now):
      localHostStr = localHostAddr.getHostAddress();
      InetAddress [] addrArr =         //get all addresses for host
                      InetAddress.getAllByName(localHostAddr.getHostName());

              //check through (possibly) multiple host addresses until one
              // is found that does not match any address prefixes in the
              // 'ignoreIPAddrs' list (as these are "internal" addresses):
      int i,pos;
      String str;
      for(pos=0; pos<addrArr.length; ++pos)
      {       //for each local host address
        if(addrArr[pos] != null && (str=addrArr[pos].getHostAddress()) != null)
        {     //string version of IP address fetched OK
          System.out.println("Found address:  " + str + " \"" +
                                         addrArr[pos].getHostName() + "\"");
          for(i=0; i<UtilFns.FAKE_IP_ADDRS.length; ++i)
          {        //for each entry in the "ignore" list of prefixes
            if(str.startsWith(UtilFns.FAKE_IP_ADDRS[i]))
            {      //beginning of IP address matches "ignore" prefix
              str = null;         //indicate IP address is rejected
              break;              //exit inner loop
            }
          }
          if(str != null)
          {        //IP address was not rejected
            localHostStr = str;        //accept IP address
            break;                     //exit outer loop
          }
        }
      }
    }
    catch(Exception ex) {}   //if error then keep default ID string
    System.out.println("Preferred address:  " + localHostStr);
  }
}
