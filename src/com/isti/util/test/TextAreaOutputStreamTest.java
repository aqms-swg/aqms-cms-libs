//TextAreaOutputStreamTest.java:  Test program for 'TextAreaOutputStream'.
//
//    5/2/2002 -- [ET]
//  10/19/2002 -- [ET]  Moved to 'test' package.
//    9/8/2004 -- [KF]  Use 'SetupLookAndFeel' class.
//

package com.isti.util.test;

import java.io.PrintStream;
import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.awt.*;
import java.awt.event.*;
import javax.swing.*;
import com.isti.util.gui.TextAreaOutputStream;
import com.isti.util.gui.SetupLookAndFeel;

/**
 * Class TextAreaOutputStreamTest is a test program for
 * 'TextAreaOutputStream'.
 */
public class TextAreaOutputStreamTest
{
    //private constructor so that no object instances may be created
    // (static access only)
  private TextAreaOutputStreamTest()
  {
  }

    /**
     * Runs a test program that demonstrates using a 'TextAreaOutputStream'
     * object.  A single command-line parameter is the name of a file to
     * be displayed in the text area.  Capturing and restoring of the
     * system stdout/stderr streams is also demonstrated.
     * @param args arguments
     */
  public static void main(String[] args)
  {
    //set "look & feel" to be that of host system:
    SetupLookAndFeel.setLookAndFeel();

         //take command-line parameter as filename (or use default):
    final String fileName = (args.length > 0) ? args[0] :
                      "src/com/isti/util/test/TextAreaOutputStreamTest.java";

    final JTextArea textAreaObj = new JTextArea();    //create text area
              //put empty-space border around text area for margin:
    textAreaObj.setBorder(BorderFactory.createEmptyBorder(2,5,2,5));
              //put text area into scroll pane:
    final JScrollPane scrollPaneObj = new JScrollPane(textAreaObj);
    final JFrame mainFrame = new JFrame();       //create frame; set title
    mainFrame.setTitle("TextAreaOutputStreamTest <" + fileName + ">");
    mainFrame.setSize(640,480);        //set frame size
    mainFrame.addWindowListener(       //setup to exit on window close
      new WindowAdapter()
      {  public void windowClosing(WindowEvent e)
         {
           System.exit(0);             //exit application
         }
      }
    );
                                       //get content pane for frame:
    final Container contentPane = mainFrame.getContentPane();
    contentPane.setLayout(new BorderLayout());             //set layout
    contentPane.add(scrollPaneObj,BorderLayout.CENTER);    //add panel

                             //get screen size:
    Dimension d = Toolkit.getDefaultToolkit().getScreenSize();
                             //put frame at center of screen:
    mainFrame.setLocation((d.width-mainFrame.getSize().width)/2,
                                   (d.height-mainFrame.getSize().height)/2);
    mainFrame.setVisible(true);        //make frame visible

              //create TextAreaOutputStream and wrap PrintStream around it:
    final PrintStream textStmObj = new PrintStream(   //(enable auto-flush)
                                new TextAreaOutputStream(textAreaObj),true);
              //save current 'stdout' and 'stderr' stream objects:
    final PrintStream sysOutSaveStream = System.out;
    final PrintStream sysErrSaveStream = System.err;
    System.setOut(textStmObj);         //redirect to text area streams
    System.setErr(textStmObj);

    BufferedReader fileStmObj = null;
    try
    {                   //open input file:
      fileStmObj = new BufferedReader(new FileReader(fileName));
    }
    catch(Exception ex)
    {                   //error opening file; show message
      System.err.println("Error opening input file \"" + fileName + "\"");
    }
    if(fileStmObj != null)
    {    //file opened OK
      String lineStr;
      try
      {
        while((lineStr=fileStmObj.readLine()) != null)
        {     //for each line in file
          System.out.println(lineStr);      //show line of data
        }
      }
      catch(Exception ex)
      {       //error detected; setup error message
        System.err.println("Error reading from input file(\"" + fileName +
                                                             "\"):  " + ex);
      }
      try { fileStmObj.close(); }      //close input file
      catch(IOException ex) {}
    }
    System.setOut(sysOutSaveStream);   //restore original system streams
    System.setErr(sysErrSaveStream);
    if(textStmObj.checkError())        //if stream errors then show message
      System.err.println("Error sending output to text area stream");
    textStmObj.close();                //close text area output stream
  }
}
