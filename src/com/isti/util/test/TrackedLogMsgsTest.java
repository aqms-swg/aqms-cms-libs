//TrackedLogMsgsTest.java:  Test program for tracked messages in
//                          'LogFile' class.
//
//  1/31/2006 - [ET]
//

package com.isti.util.test;

import java.util.Date;
import com.isti.util.UtilFns;
import com.isti.util.LogFile;

/**
 * Class TrackedLogMsgsTest is a test program for tracked messages in
 * 'LogFile' class.
 */
public class TrackedLogMsgsTest
{
  public static void main(String [] args)
  {
    final LogFile logObj =
                         new LogFile(null,LogFile.NO_MSGS,LogFile.ALL_MSGS);
    logObj.setupMessageTracking(LogFile.INFO,10);
    int ageMs;
    for(int setIdx=1; setIdx<=20; ++setIdx)
    {
      UtilFns.sleep(500);
      logObj.info("Set " + setIdx + " INFO message");
      UtilFns.sleep(500);
      logObj.debug("Set " + setIdx + " DEBUG message");
      UtilFns.sleep(500);
      logObj.warning("Set " + setIdx + " WARNING message");
      UtilFns.sleep(500);
      ageMs = ((setIdx % 3) == 1) ? 2000 : (((setIdx % 3) == 2) ? 5000 : 0);
      System.out.println(
                       "Tracked msgs(" + ageMs + ") {" + new Date() + "}:");
      System.out.println(
                      logObj.getTrackedMsgsAsString(UtilFns.newline,ageMs));
      System.out.println("[" + setIdx + "]");
    }
    logObj.close();
  }
}
