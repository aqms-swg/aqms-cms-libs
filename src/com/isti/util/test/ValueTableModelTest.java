package com.isti.util.test;

import java.awt.BorderLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.Reader;
import java.io.Writer;
import java.sql.ResultSet;
import javax.swing.*;
import com.isti.util.DelimiterSeparatedValues;
import com.isti.util.DelimiterSeparatedValuesTable;
import com.isti.util.database.MySQLConnectionJDBC;
import com.isti.util.database.SQLValuesTable;
import com.isti.util.IstiFileFilter;
import com.isti.util.gui.GuiUtilFns;
import com.isti.util.gui.ValueJTableModel;

public class ValueTableModelTest
{
  public static void main(String[] args)
  {
    String hostName = "localhost";
    String dataBase = null;
    String[] failOverHostNames = null;
    String port = null; //use the default port
    String tableName = null;
    String userName = null;
    String password = null;
    final JFileChooser chooser = new JFileChooser();
    chooser.setFileSelectionMode(JFileChooser.FILES_ONLY);
    final IstiFileFilter filter = new IstiFileFilter(
        new String[]
        {"*.csv"}, "CSV");
    chooser.addChoosableFileFilter(filter);
    chooser.setCurrentDirectory(new File(".").getAbsoluteFile());

    if (args.length > 1)
    {
      dataBase = args[0];
      tableName = args[1];
      if (args.length > 2)
      {
        userName = args[2];
        if (args.length > 3)
        {
          password = args[3];
        }
      }
    }
    else
    {
      System.out.println(
          "ValueTableModelTest dataBase tableName [username password]");
      return;
    }

    ResultSet rs = null;
    final SQLValuesTable svt = new SQLValuesTable();
    try
    {
      MySQLConnectionJDBC.registerDriver();
      final String url = MySQLConnectionJDBC.getUrl(
          hostName, failOverHostNames, port, dataBase);
      final java.util.Properties info =
          MySQLConnectionJDBC.saveUserPassword(null, userName, password);
      final MySQLConnectionJDBC conn = new MySQLConnectionJDBC(url, info, null);

      conn.createConnection();
      final String sql = "SELECT * FROM " + tableName;
      rs = conn.getConnection().createStatement(
          ResultSet.TYPE_SCROLL_INSENSITIVE,
          ResultSet.CONCUR_UPDATABLE).executeQuery(sql);
      final int count = svt.readAll(rs);
      if (count == 0)
      {
        final String errorMessage = svt.getErrorMessageString();
        if (errorMessage != null && errorMessage.length() > 0)
        {
          svt.clearErrorMessageString();
          System.err.println("Error reading SQL:\n" + errorMessage);
        }
      }
      else
      {
        System.out.println("Found " + count + " lines");
      }
    }
    catch (Exception ex)
    {
      System.err.println(ex);
      ex.printStackTrace();
    }

    svt.setEditable(true); //default to editable cells
    final ValueJTableModel vgtm = new ValueJTableModel(svt);
    //add main content panel to frame:
    final JTable tableObj = new JTable(vgtm);
    final JScrollPane scrollPaneObj =
        new JScrollPane(tableObj);
    //create and set up the frame
    final JFrame frameObj =
        new JFrame("ValueTableModelTest");
    final JPanel panelObj = new JPanel(new BorderLayout());
    panelObj.add(scrollPaneObj, BorderLayout.CENTER);
    final JPanel bottomPanelObj = new JPanel();
    final JButton importButton = new JButton("Import");
    importButton.addActionListener(new ActionListener()
    {
      public void actionPerformed(ActionEvent evt)
      {
        final String titleText = "Import";
        chooser.setDialogTitle(titleText);
        final int returnVal = chooser.showDialog(frameObj, titleText);
        if (returnVal == JFileChooser.APPROVE_OPTION)
        {
          final File inputFile = chooser.getSelectedFile();
          if (!inputFile.exists())
          {
            System.err.println("Import file does not exist: " + inputFile);
            return;
          }
          System.out.println("You chose to import from this file: " +
                             inputFile.getName());
          final DelimiterSeparatedValuesTable dsvt =
              new DelimiterSeparatedValuesTable();
          BufferedReader br = null;
          try
          {
            final Reader reader = new FileReader(inputFile);
            br = new BufferedReader(reader);
            final DelimiterSeparatedValues input = new DelimiterSeparatedValues(
                DelimiterSeparatedValues.STANDARD_COMMENT_TEXT);
            String errorMessage;
            int count = dsvt.readAll(input, br, true);
            if (count == 0)
            {
              errorMessage = dsvt.getErrorMessageString();
              if (errorMessage != null && errorMessage.length() > 0)
              {
                dsvt.clearErrorMessageString();
                System.err.println("Error reading input:\n" + errorMessage);
              }
            }
            else
            {
              svt.importValues(dsvt);
              vgtm.fireTableDataChanged();
            }
          }
          catch (Exception ex)
          {
            System.err.println(ex);
            ex.printStackTrace();
          }
          if (br != null)
          {
            try
            {
              br.close();
            }
            catch (Exception ex)
            {
              System.err.println("Error closing input file: " + ex);
            }
          }
        }
      }
    });
    bottomPanelObj.add(importButton);
    final JButton exportButton = new JButton("Export");
    exportButton.addActionListener(new ActionListener()
    {
      public void actionPerformed(ActionEvent evt)
      {
        final String titleText = "Export";
        chooser.setDialogTitle(titleText);
        final int returnVal = chooser.showDialog(frameObj, titleText);
        if (returnVal == JFileChooser.APPROVE_OPTION)
        {
          final File outputFile = chooser.getSelectedFile();
          System.out.println("You chose to export to this file: " +
                             outputFile.getName());
          final DelimiterSeparatedValuesTable dsvt =
              new DelimiterSeparatedValuesTable();
          dsvt.importValues(svt);
          Writer w = null;
          try
          {
            w = new FileWriter(outputFile);
            final DelimiterSeparatedValues output =
                new DelimiterSeparatedValues();
            dsvt.writeAll(output, w, true);
            w.close();
          }
          catch (Exception ex)
          {
            System.err.println(ex);
            ex.printStackTrace();
          }
          if (w != null)
          {
            try
            {
              w.close();
            }
            catch (Exception ex)
            {
              System.err.println("Error closing output file: " + ex);
            }
          }
        }
      }
    });
    bottomPanelObj.add(exportButton);
    panelObj.add(bottomPanelObj, BorderLayout.SOUTH);
    frameObj.getContentPane().add(panelObj);
    frameObj.setDefaultCloseOperation(JFrame.DO_NOTHING_ON_CLOSE);
    frameObj.pack();
    frameObj.setVisible(true); //make frame visible

    final Object windowCloseObj = new Object();
    frameObj.addWindowListener(new WindowAdapter()
    {
      public void windowOpened(WindowEvent e)
      {
        GuiUtilFns.initColumnSizes(tableObj);
      }

      public void windowClosing(WindowEvent e)
      {
        synchronized (windowCloseObj)
        {
          windowCloseObj.notifyAll();
        }
      }
    });

    try
    {
      synchronized (windowCloseObj)
      {
        windowCloseObj.wait();
      }
    }
    catch (Exception ex)
    {
      System.err.println(ex);
      ex.printStackTrace();
    }

    if (rs != null)
    {
      try
      {
        svt.reset(rs); //reset the result set
        svt.writeAll(rs); //write the db
      }
      catch (Exception ex)
      {
        System.err.println(ex);
        ex.printStackTrace();
      }
    }

    frameObj.dispose();
  }
}
