package com.isti.util.test;

import java.util.Properties;

import com.isti.util.IstiVersion;
import com.isti.util.updatechecker.XMLUpdateCheckerClient;

public class XMLUpdateCheckerTest {

  public static void main(String[] args) {
    Properties httpHeaders = new Properties();
    String version = "2.1.4";
    String updateURL = "http://www.seis.sc.edu/GEE/Dynamic/UpdateChecker.xml";
    httpHeaders.put("User-Agent", "TestUpdateChecker/" + version);
    XMLUpdateCheckerClient updateChecker = new XMLUpdateCheckerClient(
        new IstiVersion(version), updateURL, httpHeaders);
    System.out.println("updateChecker: version=" + version
        + ", update_version=" + updateChecker.getVersion() + ", update="
        + updateChecker.isUpdateAvailable());
  }
}
