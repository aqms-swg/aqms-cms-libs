//BasicUpdateCheckerClient.java - Defines an update checker client.
//
//  8/20/2003 -- [KF]  Initial version.
//

package com.isti.util.updatechecker;

import com.isti.util.*;

/**
 * Class BasicUpdateCheckerClient defines an update checker client.
 */
public class BasicUpdateCheckerClient
{
  protected final IstiVersion version;
  protected final BasicUpdateCheckerServer server;

  /**
   * Constructs an update checker client.
   * @param version the version.
   * @param server the update checker server.
   */
  public BasicUpdateCheckerClient(
      IstiVersion version, BasicUpdateCheckerServer server)
  {
    this.version = version;
    this.server = server;
  }

  /**
   * Gets the admin email address.
   * @return the admin email address or null if unknown.
   */
  public String getAdminEmailAddress()
  {
    return server.getAdminEmailAddress();
  }

  /**
   * Gets a list of updates.
   * @return an array of updates.
   */
  public UpdateInformation [] getUpdates()
  {
    return server.getUpdates(version);
  }

  /**
   * Gets the version.
   * @return the version.
   */
  public IstiVersion getVersion()
  {
    return version;
  }

  /**
   * Determines if there is an update available.
   * @return true if there is an update available, false otherwise.
   */
  public boolean isUpdateAvailable()
  {
    return server.isUpdateAvailable(version);
  }

  /**
   * Returns a string representation of the object.
   * @return  a string representation of the object.
   */
  public String toString()
  {
    return "version=" + version +
        ", server: " + server;
  }
}