//BasicUpdateCheckerServer.java - Defines an update checker server.
//
//  8/20/2003 -- [KF]  Initial version.
//

package com.isti.util.updatechecker;

import java.util.*;
import com.isti.util.*;

/**
 * Class BasicUpdateCheckerServer defines an update checker server.
 */
public class BasicUpdateCheckerServer
{
  private final String adminEmailAddress;
  private final UpdateInformation [] updates;
  private final Vector updateList = new Vector();
  private final UpdateInformation [] emptyUpdates = new UpdateInformation [0];
  private final int numUpdates;

  /**
   * Constructs an update checker server.
   * @param adminEmailAddress the admin email address or null if unknown.
   * @param updates an array of updates.
   */
  public BasicUpdateCheckerServer(
      String adminEmailAddress, UpdateInformation [] updates)
  {
    numUpdates = updates.length;
    Arrays.sort(updates);
    this.adminEmailAddress = adminEmailAddress;
    this.updates = updates;
    this.updateList.addAll(Arrays.asList(updates));
  }

  /**
   * Gets the admin email address.
   * @return the admin email address or null if unknown.
   */
  public String getAdminEmailAddress()
  {
    return adminEmailAddress;
  }

  /**
   * Gets a list of updates for the specified version.
   * @param version the version.
   * @return an array of updates.
   */
  public UpdateInformation [] getUpdates(IstiVersion version)
  {
    int fromIndex = 0;

    if (numUpdates > 0)  //if there are updates
    {
      UpdateInformation update;
      do
      {
        update = (UpdateInformation)updateList.get(fromIndex);
      }
      while (update.getVersion().compareTo(version) <= 0 &&
             ++fromIndex < numUpdates);
    }
    return (UpdateInformation [])updateList.subList(
        fromIndex, numUpdates).toArray(emptyUpdates);
  }

  /**
   * Determines if there is an update available for the specified version.
   * @param version the version.
   * @return true if there is an update available, false otherwise.
   */
  public boolean isUpdateAvailable(IstiVersion version)
  {
    return getUpdates(version).length > 0;
  }

  /**
   * Returns a string representation of the object.
   * @return  a string representation of the object.
   */
  public String toString()
  {
    return "admin Email address="+adminEmailAddress +
        ", " + toString(updates);
  }

  /**
   * Returns a string representation of the updates.
   * @param updates an array of updates.
   * @return  a string representation of the updates.
   */
  public static String toString(UpdateInformation[] updates)
  {
    final int numUpdates = updates.length;
    String str;

    if (numUpdates <= 0)
    {
      str = "No update available.";
    }
    else
    {
      int i = 0;
      str = "update[" + i + "]=" + updates[i];
      for (i++; i < numUpdates; i++)
      {
        str += ", update["+i+"]=" + updates[i];
      }
    }

    return str;
  }
}