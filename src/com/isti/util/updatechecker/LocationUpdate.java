//LocationUpdate.java - Defines methods for doing an update.
//
//  8/20/2003 -- [KF]  Initial version.
//

package com.isti.util.updatechecker;

import com.isti.util.*;

/**
 * Class LocationUpdate defines methods for doing an update.
 */
public class LocationUpdate implements UpdateAction
{
  private final String description;
  private final String location;
  private final LaunchBrowser launchBrowserObj = new LaunchBrowser();

  /**
   * Constructs a location update.
   * @param description the description of the update.
   * @param location the location of the installer.
   */
  public LocationUpdate(String description, String location)
  {
    this.description = description.trim();
    this.location = location.trim();
  }

  /**
   * Gets a description string for option display.
   * @return a description string for option display.
   */
  public String getDescription()
  {
    return description;
  }

  /**
   * Gets the location of the installer.
   * @return the location of the installer.
   */
  public String getLocation()
  {
    return location;
  }

  /**
   * Run the update.
   * @return true on success, false otherwise.
   */
  public boolean run()
  {
    return launchBrowserObj.showURL(location);
  }

  /**
   * Returns a string representation of the object.
   * @return  a string representation of the object.
   */
  public String toString()
  {
    String str = "description=" + description + ", location=" + location;
    return str;
  }
}