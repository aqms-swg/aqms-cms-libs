//UpdateAction.java - Defines methods for doing an update.
//
//  8/20/2003 -- [KF]  Initial version.
//

package com.isti.util.updatechecker;

/**
 * Interface UpdateAction defines methods for doing an update.
 */
public interface UpdateAction
{
  /**
   * Gets a description string for option display.
   * @return a description string for option display.
   */
  public String getDescription();

  /**
   * Run the update.
   * @return true on success, false otherwise.
   */
  public boolean run();
}