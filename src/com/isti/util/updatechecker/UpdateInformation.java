//UpdateInformation.java - Defines information needed for doing an update.
//
//  8/20/2003 -- [KF]  Initial version.
//

package com.isti.util.updatechecker;

import com.isti.util.*;

/**
 * Class UpdateInformation defines information needed for doing an update.
 */
public class UpdateInformation implements Comparable
{
  private IstiVersion version;
  private String versionDate;
  private String features;
  private String importantUpdateReason;
  private UpdateAction [] updateActions;

  /**
   * Constructs update information.
   * @param versionString the version string.
   * @param versionDate the version date.
   * @param features the features.
   * @param importantUpdateReason the fundamental reason to install this update.
   * @param updateActions the update actions.
   */
  public UpdateInformation(
      String versionString, String versionDate, String features,
      String importantUpdateReason, UpdateAction [] updateActions)
  {
    this(new IstiVersion(versionString), versionDate, features,
         importantUpdateReason, updateActions);
  }

  /**
   * Constructs update information.
   * @param version the version.
   * @param versionDate the version date.
   * @param features the features.
   * @param importantUpdateReason the fundamental reason to install this update.
   * @param updateActions the update actions.
   */
  public UpdateInformation(
      IstiVersion version, String versionDate, String features,
      String importantUpdateReason, UpdateAction [] updateActions)
  {
    this.version = version;
    this.versionDate = versionDate;
    this.features = features;
    this.importantUpdateReason = importantUpdateReason;
    this.updateActions = updateActions;
  }

  /**
   * Gets the version.
   * @return the version.
   */
  public IstiVersion getVersion()
  {
    return version;
  }

  /**
   * Gets the version date.
   * @return the version date.
   */
  public String getVersionDate()
  {
    return versionDate;
  }

  /**
   * Gets the update actions.
   * @return an array of update actions.
   */
  public UpdateAction [] getUpdateActions()
  {
    return updateActions;
  }

  /**
   * Gets the features.
   * @return the features.
   */
  public String getFeatures()
  {
    return features;
  }

  /**
   * Gets the fundamental reason to install this update.
   * @return the fundamental reason to install this update.
   */
  public String getImportantUpdateReason()
  {
    return importantUpdateReason;
  }

  /**
   * Returns a string representation of the object.
   * @return  a string representation of the object.
   */
  public String toString()
  {
    String str = "version="+version +
                 ", date="+versionDate +
                 ", features="+features +
                 ", reason="+importantUpdateReason;
    for (int i = 0; i < updateActions.length; i++)
    {
      str += ", action["+i+"]="+updateActions[i];
    }
    return str;
  }

  //Comparable interface

  /**
   * Compares this UpdateInformation with the specified UpdateInformation.
   * If the specified Object is a UpdateInformation, this function behaves like
   * <code>compareTo(UpdateInformation)</code>.  Otherwise, it throws a
   * <code>ClassCastException</code> (as Versions are comparable
   * only to other Versions).
   *
   * @param   o the <code>Object</code> to be compared.
   * @return  the value <code>0</code> if the versions are equal;
   *            a value less than <code>0</code> if this version
   *            is less than the specified version;
   *            and a value greater than <code>0</code> if this version
   *            is greater than the specified version.
   * @exception <code>ClassCastException</code> if the argument is not a
   *		  <code>UpdateInformation</code>.
   */
  public int compareTo(java.lang.Object o)
  {
    return compareTo((UpdateInformation)o);
  }

  /**
   * Compares this UpdateInformation with the specified UpdateInformation.
   * @param   o the <code>Object</code> to be compared.
   * @return  the value <code>0</code> if the versions are equal;
   *            a value less than <code>0</code> if this version
   *            is less than the specified version;
   *            and a value greater than <code>0</code> if this version
   *            is greater than the specified version.
   */
  public int compareTo(UpdateInformation o)
  {
    return version.compareTo(o.version);
  }
}