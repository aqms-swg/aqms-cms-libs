//XMLUpdateCheckerClient.java - Defines an update checker client.
//
//  8/20/2003 -- [KF]  Initial version.
//   3/8/2007 -- [KF]  Added 'getErrorFlag()' and 'getErrorMessage()' methods,
//                     removed the output to the console.
//  2/01/2012 -- [KF]  Added support to set URL connection request properties.
//

package com.isti.util.updatechecker;

import java.util.Properties;

import com.isti.util.IstiVersion;

/**
 * Class XMLUpdateChecker defines an update checker client.
 */
public class XMLUpdateCheckerClient extends BasicUpdateCheckerClient
{
  protected static boolean debug = false;  //true for debug, false otherwise.
  protected final XMLUpdateCheckerServer xmlServer;

  /**
   * Constructs an update checker.
   * @param versionString the version string.
   * @param updateLocationString the update location string.
   * @see #getErrorFlag()
   * @see #getErrorMessage()
   */
  public XMLUpdateCheckerClient(
      String versionString, String updateLocationString)
  {
    this(new IstiVersion(versionString), updateLocationString);
  }

  /**
   * Constructs an update checker.
   * @param version the version.
   * @param updateLocationString the update location string.
   * @see #getErrorFlag()
   * @see #getErrorMessage()
   */
  public XMLUpdateCheckerClient(
      IstiVersion version, String updateLocationString)
  {
    this(version, new XMLUpdateCheckerServer(updateLocationString));
  }

  /**
   * Constructs an update checker.
   * @param version the version.
   * @param updateLocationString the update location string.
   * @param urlRequestProps the request properties to use for URL connections
   *                        or null if none.
   * @see #getErrorFlag()
   * @see #getErrorMessage()
   */
  public XMLUpdateCheckerClient(
      IstiVersion version, String updateLocationString, Properties urlRequestProps)
  {
    this(version, new XMLUpdateCheckerServer(updateLocationString, urlRequestProps));
  }

  /**
   * Constructs an update checker client.
   * @param version the version.
   * @param xmlServer the XML update checker server.
   */
  public XMLUpdateCheckerClient(
      IstiVersion version, XMLUpdateCheckerServer xmlServer)
  {
    super(version, xmlServer);
    this.xmlServer = xmlServer;
  }

  /**
   * Returns true if an error was detected.  The error message may be
   * fetched via the 'getErrorMessage()' method.
   * @return true if an error was detected.
   */
  public boolean getErrorFlag()
  {
    return xmlServer.getErrorFlag();
  }

  /**
   * Returns message string for last error (or 'No error' if none).
   * @return the error message.
   */
  public String getErrorMessage()
  {
    return xmlServer.getErrorMessage();
  }

  public static void main(String[] args)
  {
    String versionString = "0.0.2";

    for (int i = 0; i < args.length; i++)
    {
      if (args[i].toLowerCase().equals("debug"))
        debug = true;
      else
        versionString = args[i];
    }

    final String localUpdateLocationString =
        "src/com/isti/util/updatechecker/UpdateChecker.xml";

    XMLUpdateCheckerClient updateChecker =
        new XMLUpdateCheckerClient(
        versionString, localUpdateLocationString);

    if (debug)
    {
      System.out.println(updateChecker);

      System.out.println(
          "Update is" + (updateChecker.isUpdateAvailable()?"":" not") +
          " available");
    }

    UpdateInformation []updates = updateChecker.getUpdates();

    if (debug)
    {
      System.out.println(BasicUpdateCheckerServer.toString(updates));
    }
    else
    {
      String installer = "No updates available";

      if (updates.length > 0)
      {
        UpdateAction[] actions = updates[updates.length-1].getUpdateActions();

        if (actions.length <= 0)
        {
          installer = "No installers available";
        }
        else
        {
          if (actions[0] instanceof LocationUpdate)
          {
            installer = ((LocationUpdate)actions[0]).getLocation();
          }
          else
          {
            installer = actions[0].getDescription();
          }
        }
      }

      System.out.println(
          "\nInstaller:\n" + installer);
    }
  }
}
