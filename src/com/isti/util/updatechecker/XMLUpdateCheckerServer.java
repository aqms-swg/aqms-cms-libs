//XMLUpdateCheckerServer.java - Defines an update checker server.
//
//  8/20/2003 -- [KF]  Initial version.
//   3/8/2007 -- [KF]  Added 'getErrorFlag()' and 'getErrorMessage()' methods,
//                     removed the output to the console.
//  2/01/2012 -- [KF]  Added support to set URL connection request properties.
//

package com.isti.util.updatechecker;

import java.util.*;

import org.jdom.Element;
import com.isti.util.*;

/**
 * Class XMLUpdateCheckerServer defines an update checker server.
 */
public class XMLUpdateCheckerServer extends BasicUpdateCheckerServer
{
  /** Value for "UpdateChecker" tag. */
  public static final String UPDATE_CHECKER_TAG = "UpdateChecker";
  /** Tag name for AdminEmail element. */
  public static final String ADMIN_EMAIL = "AdminEmail";
  /** Tag name for Update element. */
  public static final String UPDATE = "Update";
  /** Tag name for Version attribute. */
  public static final String VERSION = "Version";
  /** Tag name for Date attribute. */
  public static final String DATE = "Date";
  /** Tag name for Features element. */
  public static final String FEATURES = "Features";
  /** Tag name for Reason element. */
  public static final String REASON = "Reason";
  /** Tag name for UpdateAction element. */
  public static final String UPDATE_ACTION = "UpdateAction";
  /** Tag name for Description attribute. */
  public static final String DESCRIPTION = "Description";
  /** Tag name for UpdateLocation element. */
  public static final String UPDATE_LOCATION = "UpdateLocation";
  //XML utils object
  protected static final IstiXmlUtils istiXmlUtilsObj = new IstiXmlUtils();

  /**
   * Constructs an update checker server.
   * @param updateLocationString the update location string.
   * @see #getErrorFlag()
   * @see #getErrorMessage()
   */
  public XMLUpdateCheckerServer(String updateLocationString)
  {
    this(updateLocationString, (Properties)null);
  }

  /**
   * Constructs an update checker server.
   * @param updateLocationString the update location string.
   * @param urlRequestProps the request properties to use for URL connections
   *                        or null if none.
   * @see #getErrorFlag()
   * @see #getErrorMessage()
   */
  public XMLUpdateCheckerServer(
      String updateLocationString, Properties urlRequestProps)
  {
    this(createRootElement(updateLocationString, urlRequestProps));
  }

  /**
   * Constructs an update checker server.
   * @param rootElement root element.
   */
  public XMLUpdateCheckerServer(Element rootElement)
  {
    super(createAdminEmailAddress(rootElement),
          createUpdates(rootElement));
  }

  /**
   * Returns true if an error was detected.  The error message may be
   * fetched via the 'getErrorMessage()' method.
   * @return true if an error was detected.
   */
  public boolean getErrorFlag()
  {
    return istiXmlUtilsObj.getErrorFlag();
  }

  /**
   * Returns message string for last error (or 'No error' if none).
   * @return the error message.
   */
  public String getErrorMessage()
  {
    return istiXmlUtilsObj.getErrorMessage();
  }

  /**
   * Creates the root element.
   * @param updateLocationString the update location string.
  * @return the root element.
   */
  protected static Element createRootElement(String updateLocationString)
  {
    return createRootElement(updateLocationString, (Properties)null);
  }

  /**
   * Creates the root element.
   * @param updateLocationString the update location string.
   * @param urlRequestProps the request properties to use for URL connections
   *                        or null if none.
  * @return the root element.
   */
  protected static Element createRootElement(String updateLocationString,
      Properties urlRequestProps)
  {
    Element rootElement = null;

    //load file into JDOM Element objects:
    if(!istiXmlUtilsObj.loadFile(updateLocationString, UPDATE_CHECKER_TAG, urlRequestProps))
    {
      return rootElement;  //abort method
    }

    rootElement = istiXmlUtilsObj.getRootElement();

    return rootElement;  //abort method
  }

  /**
   * Gets the text for the child with the specified name.
   * @param e the element.
   * @param name the name of the child.
   * @return the text or null if not found.
   */
  protected static String getChildText(Element e, String name)
  {
    String text = null;
    Element child = e.getChild(name);
    if (child != null)
      text = child.getText();
    return text;
  }

  /**
   * Creates the admin email address.
   * @param rootElement root element.
   * @return the admin email address or null if unknown.
   */
  protected static String createAdminEmailAddress(Element rootElement)
  {
    if (rootElement == null)
      return null;

    return getChildText(rootElement, ADMIN_EMAIL);
  }

  /**
   * Creates a list of updates for the specified version string.
   * @param rootElement root element.
   * @return an array of updates.
   */
  protected static UpdateInformation [] createUpdates(Element rootElement)
  {
    if (rootElement == null)
      return new UpdateInformation[0];

    List children = rootElement.getChildren(UPDATE);
    UpdateInformation [] updates = new UpdateInformation[children.size()];
    for (int i = 0; i < children.size(); i++)
    {
      Element currentElement = (Element)children.get(i);

      final String versionString = currentElement.getAttributeValue(VERSION);
      final String versionDate = currentElement.getAttributeValue(DATE);
      final String features = getChildText(currentElement, FEATURES);
      final String reasons = getChildText(currentElement, REASON);

      UpdateAction [] updateActions = createUpdateActions(currentElement);

      updates[i] = new UpdateInformation(versionString, versionDate, features,
          reasons, updateActions);
    }
    return updates;
  }

  /**
   * Creates update actions.
   * @param updateElement the update element.
   * @return an array of update actions.
   */
  protected static UpdateAction [] createUpdateActions(Element updateElement)
  {
    List children = updateElement.getChildren(UPDATE_ACTION);
    UpdateAction [] actions = new UpdateAction[children.size()];
    for (int i = 0; i < children.size(); i++)
    {
      Element currentElement = (Element)children.get(i);

      String description = currentElement.getAttributeValue(DESCRIPTION);
      String location = getChildText(currentElement, UPDATE_LOCATION);
      actions[i] = new LocationUpdate(description, location);
    }
    return actions;
  }
}
